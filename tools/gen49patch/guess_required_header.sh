#!/bin/bash
#
# Hiromasa YOSHIMOTO <y@momonga-linux.org>
#

if [ $# -ne 1 ] ; then
    cat<<EOF > /dev/stderr
usage:
    $0   function
EOF
    exit 1
fi

case "$1" in
R_OK|execl|execlp|execv|execvp|fork)
	echo unistd.h
	;;
getopt|getarg)
	echo unistd.h
	;;
gid_t|uid_t)
	echo unistd.h
	echo sys/types.h
	;;
getuid|getppid|isatty)
	echo unistd.h
	;;
lseek64)
	echo unistd.h
	;;
fdopen)
	echo stdio.h
	;;
dup|dup2|dup3|sleep|usleep|unlink|close|chdir|fchdir|getcwd|access|read|write|select)
	echo unistd.h
	;;
EXIT_SUCCESS)
	echo stdlib.h
	;;
*int64_t|*int32_t|intptr_t)
	echo stdint.h
	;;
*INT_MAX|*INT_MIN|*LONG_MAX|*LONG_MIN|*CHAR_MAX|*CHAR_MIN|PATH_MAX)
	echo limits.h
	;;
std::time_t)
	echo ctime
	;;
auto_ptr)
	echo memory
	;;
numeric_limits)
	echo limits
	;;
abs|find_if|min|max|transform|count|reverse)
	echo algorithm
	;;
*)
man 3 $1 2> /dev/null | colcrt  \
    | grep "#include <.*>" \
    | sed -e 's,#include,,' -e 's,<,,' -e 's,>,,' -e 's,[ ]*,,' \
    | sort | uniq 
	;;
esac
exit 0
