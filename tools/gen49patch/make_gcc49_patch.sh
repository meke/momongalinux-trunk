#!/bin/bash
#
#  Hiromasa YOSHIMOTO <y@momonga-linux.org>
#

BIN=`dirname $0`
BIN=`readlink -f $BIN`
BIN=${BIN:-.}

LANG=C 

LOG=`mktemp /tmp/$$.XXXXXXX` || exit 1

CMD=${@:-make}

if [ "make"x == "$CMD"x ]; then
    if [ ! -f GNUmakefile ] && [ ! -f makefile ] && [ ! -f Makefile ]; then
	for x in `uname -m`-momonga-linux build; do
	    if [ -f "$x"/Makefile ]; then
		CMD=" make -C $x"
		break	       
	    fi
	done
    fi
fi
$CMD  2>&1 | tee  $LOG

gawk -vBIN=$BIN -f $BIN/sub_make_gcc49_patch.awk  $LOG


rm $LOG
