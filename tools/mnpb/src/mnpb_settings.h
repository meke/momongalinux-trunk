#ifndef MNPB_SETTINGS_H
#define MNPB_SETTINGS_H

#include <glib.h>
#include <gio/gio.h>
#include <sys/stat.h>

gchar* get_arch ();

gchar* get_svntree ();
gboolean set_svntree (const gchar* svntree);
void init_svntree ();

gchar* get_mnpb_dir ();
gboolean set_mnpb_dir (const gchar* mnpb_dir);
void init_rpmbuild_dir ();

gchar* get_proxy_server ();
gboolean set_proxy_server (const gchar* server);
gchar* get_proxy_port ();
gboolean set_proxy_port (const gchar* prt);
gboolean get_use_proxy ();
gboolean set_use_proxy (const gboolean use_proxy);
void init_proxy ();

void init_rpmmacros ();

#endif /* MNPB_SETTINGS_H */
