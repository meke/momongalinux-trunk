#include <config.h>

#include "mnpb_settings.h"
#include "mnpb_i18n.h"
#include "mnpb_ui.h"
#include "mnpb_check.h"

int
main (int argc, char* argv[])
{

	/* initialization */
	gtk_init (&argc, &argv);

	/* internationalization */
	init_i18n ();

	/* directory check and create missing one */
	init_rpmbuild_dir ();

	/* init rpmmacros */
	init_rpmmacros ();

	/* set momonga svn tree */
	init_svntree ();

	/* check svn command */
	if (!check_svn ())
	{
		return 1;
	}

	/* check wget command, but on error this output warning only */
	check_wget ();

	/* init g_print () handler */
	g_set_print_handler (print_to_textview);

	/* ui init */
	init_ui ();

	/* main routine */
	gtk_main();

	return 0;
}

