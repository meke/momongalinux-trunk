#include <config.h>

#include "mnpb-install_ui.h"
#include "mnpb_settings.h"
#include "mnpb_i18n.h"

int
main (int argc, char* argv[])
{

	/* initialization */
	gtk_init (&argc, &argv);

	/* internationalization */
	init_i18n ();

	/* ui init */
	init_ui ();

	/* main routine */
	gtk_main();
	return 0;
}

