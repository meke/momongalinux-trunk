#ifndef MNPB_UI_H
#define MNPB_UI_H

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <config.h>

void init_ui ();
void print_to_textview (const gchar* text);
#endif /* MNPB_UI_H */
