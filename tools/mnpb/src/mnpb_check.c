#include "mnpb_check.h"

/* check svn command */
gboolean
check_svn ()
{
	gint status;
	gchar *output;
	gchar *error;
	GError *err = NULL;

	g_spawn_command_line_sync ("svn ls http://svn.momonga-linux.org/svn/pkgs/", 
				&output, &error, &status, &err);
	g_assert_no_error (err);
	if (status)
	{
		g_print (_("Error In SVN check\n"));
		g_print (_("STDOUT:\t%s"), output);
		g_print (_("STDERR:\t%s"), error);
		return FALSE;
	}
	return TRUE;
}

/* check wget command */
gboolean
check_wget ()
{
	gint status;
	gchar *output;
	gchar *error;
	gchar *command_line;
	gchar *proxy;
	GError *err = NULL;

	if (get_use_proxy ())
	{
		proxy = g_strconcat (" --execute=http_proxy=",
						get_proxy_server (), ":", get_proxy_port (), " ", NULL);
	}
	else
	{
		proxy = g_strconcat ("", NULL);
	}
	command_line = g_strconcat ("wget -q ", proxy,
									" --spider http://www.momonga-linux.org/", NULL);
	g_free (proxy);
	g_spawn_command_line_sync (command_line, &output, &error, &status, &err);
	g_free (command_line);
	g_assert_no_error (err);
	if (status)
	{
		g_print (_("Error In Wget check.\nYou can set proxy.\n"));
		g_print (_("STDOUT:\t%s"), output);
		g_print (_("STDERR:\t%s"), error);
		return FALSE;
	}
	return TRUE;
}

