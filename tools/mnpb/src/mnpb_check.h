#ifndef MNPB_CHECK_H
#define MNPB_CHECK_H

#include <glib.h>
#include <glib/gi18n.h>
#include <config.h>

gboolean check_svn ();
gboolean check_wget ();

#endif /* MNPB_CHECK_H */
