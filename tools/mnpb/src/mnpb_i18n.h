#ifndef MNPB_I18N_H
#define MNPB_I18N_H

#include <glib.h>
#include <glib/gi18n.h>
#include <config.h>

void init_i18n ();

#endif /* MNPB_I18N_H */
