#include "mnpb-install_ui.h"
#include "mnpb_settings.h"
#define TIMEOUT 100 /* GtkProgresBar Time out */

/* Global Variable */
GtkBuilder *builder;

/* add item to treeview */
static void
add_item (gchar *text, gchar *arch)
{
	GtkTreeIter iter;
	GtkListStore *store;
	GtkTreeView *treeview;

	treeview = GTK_TREE_VIEW (gtk_builder_get_object (builder, "treeview1"));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (treeview));
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter, 0, text, 1, arch, -1);
}

/* check directory and add item */
static void
list_dir_pkgs (gchar *dir_name, gchar *arch)
{
	GDir *dir;
	GError *error = NULL;
	gchar *pkgname;

	dir = g_dir_open (dir_name, 0, &error);
	g_assert_no_error (error);
	while (1)
	{
		pkgname = g_strdup (g_dir_read_name (dir));
		if (pkgname == NULL)
		{
			break;
		}
		add_item (pkgname, arch);
		g_free (pkgname);
	}
	g_dir_close (dir);
}

/* init treeview */
static void
init_treeview ()
{
	gchar *dir_name;
	gchar *arch;

	/* get arch_dir */
	arch = g_strdup (get_arch ());
	dir_name = g_build_filename (get_mnpb_dir (), "RPMS", arch, NULL);
	if (g_file_test (dir_name, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))
	{
		list_dir_pkgs (dir_name, arch);
	}
	g_free (dir_name);
	g_free (arch);

	/* get noarch dir */
	arch = "noarch";
	dir_name = g_build_filename (get_mnpb_dir (), "RPMS", "noarch", NULL);
	if (g_file_test (dir_name, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))
	{
		list_dir_pkgs (dir_name, "noarch");
	}
	g_free (dir_name);
}

/* set toolbuttons sensitive */
static void
set_tool_buttons_sensitive (gboolean sensitive)
{
	GtkWidget *install_button, *delete_button;

	install_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton1"));
	delete_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton2"));

	gtk_widget_set_sensitive (install_button, sensitive);
	gtk_widget_set_sensitive (delete_button, sensitive);
}

/* progressbar step */
gboolean
cb_step_pbar (gboolean *end)
{
	GtkProgressBar *pbar;

	pbar = GTK_PROGRESS_BAR (gtk_builder_get_object (builder, "progressbar1"));
	if (*end)
	{
		gtk_widget_show (GTK_WIDGET (pbar));
		gtk_progress_bar_pulse (pbar);
	}
	else
	{
		gtk_widget_hide (GTK_WIDGET (pbar));
	}
	return *end;
}

/* install package */
void
cb_install_foreach (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, GString *pkgs)
{
	gchar *package, *arch;
	gchar *pkg_fullname;

	gtk_tree_model_get (model, iter, 0, &package, 1, &arch, -1);

	pkg_fullname = g_build_filename (get_mnpb_dir (), "RPMS", arch, package, NULL);
	g_string_append_printf (pkgs, " %s", pkg_fullname);
	g_free (package);
	g_free (arch);
	g_free (pkg_fullname);
}

void
cb_child_watch (GPid pid, gint status, GMainLoop *loop)
{
	g_main_loop_quit (loop);
	g_spawn_close_pid (pid);
}

G_MODULE_EXPORT void
cb_install (GtkAction *action)
{
	GtkTreeView *treeview;
	GtkTreeSelection *selection;
	GString *pkgs;
	gchar* command_line;
	gchar** args;

	GMainLoop *loop;
	GPid pid;
	GError *error = NULL;
	static gboolean end;

	set_tool_buttons_sensitive (FALSE);
	end = TRUE;
	g_timeout_add (TIMEOUT, (GSourceFunc) cb_step_pbar, &end);

	treeview = GTK_TREE_VIEW (gtk_builder_get_object (builder, "treeview1"));
	selection = gtk_tree_view_get_selection (treeview);
	pkgs = g_string_new ("");
	gtk_tree_selection_selected_foreach (selection, 
		(GtkTreeSelectionForeachFunc) cb_install_foreach, pkgs);

	command_line = g_strconcat ("pkexec yum install -y ", pkgs->str, NULL);
	g_shell_parse_argv (command_line, NULL, &args, &error);
	g_assert_no_error (error);
	g_free (command_line);

	g_spawn_async_with_pipes (NULL, args, NULL, G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
		NULL, NULL, &pid, NULL, NULL, NULL, &error);
	g_assert_no_error (error);
	g_string_free (pkgs, TRUE);

	loop = g_main_loop_new (NULL, TRUE);
	g_child_watch_add (pid, (GChildWatchFunc)cb_child_watch, loop);
	g_main_loop_run (loop);

	end = FALSE;
	set_tool_buttons_sensitive (TRUE);
}

/* delete package */
void
cb_delete_foreach (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	gchar *package, *arch;
	gchar *pkg_fullname;

	gtk_tree_model_get (model, iter, 0, &package, 1, &arch, -1);
	pkg_fullname = g_build_filename (get_mnpb_dir (), "RPMS", arch, package, NULL);
	g_remove (pkg_fullname);
	g_free (package);
	g_free (arch);
	g_free (pkg_fullname);
}

G_MODULE_EXPORT void
cb_delete (GtkAction *action)
{
	GtkTreeView *treeview;
	GtkTreeSelection *selection;
	GtkListStore *store;
	static gboolean end;

	set_tool_buttons_sensitive (FALSE);
	end = TRUE;
	g_timeout_add (TIMEOUT, (GSourceFunc) cb_step_pbar, &end);

	treeview = GTK_TREE_VIEW (gtk_builder_get_object (builder, "treeview1"));
	selection = gtk_tree_view_get_selection (treeview);
	gtk_tree_selection_selected_foreach (selection, cb_delete_foreach, NULL);
	store = GTK_LIST_STORE (gtk_builder_get_object (builder, "liststore1"));
	gtk_list_store_clear (store);
	init_treeview ();

	end = FALSE;
	set_tool_buttons_sensitive (TRUE);
}

G_MODULE_EXPORT void
cb_quit (GtkAction *action)
{
	gtk_main_quit ();
}

void
init_ui ()
{
	GtkWindow *window;
	GtkTreeSortable *sortable;
	GtkProgressBar *pbar;

	builder = gtk_builder_new ();
	gtk_builder_add_from_file (builder, PKGDATADIR "/mnpb-install.ui", NULL);
	window = GTK_WINDOW (gtk_builder_get_object (builder, "window1"));
	sortable = GTK_TREE_SORTABLE (gtk_builder_get_object (builder, "liststore1"));

	/* add rpms to treeview */
	init_treeview ();

	/* sort list */
	gtk_tree_sortable_set_sort_column_id (sortable, 0, GTK_SORT_ASCENDING);

	/* signale init */
	gtk_builder_connect_signals (builder, NULL);

	/* visible window */
	gtk_widget_show_all (GTK_WIDGET (window));

	/* hide progressbar default */
	pbar = GTK_PROGRESS_BAR (gtk_builder_get_object (builder, "progressbar1"));
	gtk_widget_hide (GTK_WIDGET (pbar));
}

