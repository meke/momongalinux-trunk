#include "mnpb_settings.h"

/* Get Build Architecture */

gchar*
get_arch ()
{
#ifdef __i386__
	static gchar *arch = "i686";
#elif __x86_64__
	static gchar *arch = "x86_64";
#else
	static gchar *arch = "none";
#endif
	return arch;
}

/* Get SVN Tree */
gchar*
get_svntree ()
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_get_string (settings, "svntree");
}

/* Set SVN Tree */
gboolean
set_svntree (const gchar* svntree)
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_set_string (settings, "svntree", svntree);
}

/* Init SVN Tree */
void
init_svntree ()
{
	gchar *svntree;
	const gchar *svn_root = "http://svn.momonga-linux.org/svn/pkgs/";
	gchar *release;
	gchar *codename;
	gint status;
	gchar *output;
	gchar *error;
	gchar **split;
	GError	*err = NULL;

	svntree  = get_svntree ();
	if (g_utf8_strlen (svntree , 0) == 0)
	{
		/* Get momonga codename */
		g_spawn_command_line_sync ("lsb_release -c", &output, &error, &status, &err);
		g_assert_no_error (err);
		split = g_strsplit (output, "\t", -1);
		codename = g_strchomp (g_strdup (split[1]));
		g_strfreev (split);

		if (!g_strcmp0 (codename, "Sinji"))
		{
			/* get development tree */
			svntree = g_strconcat (svn_root, "trunk/pkgs/", NULL);
			g_free (codename);
		}
		else
		{
			/* Get momonga release */
			g_spawn_command_line_sync ("lsb_release -r", &output, &error, &status, &err);
			g_assert_no_error (err);
			split = g_strsplit (output, "\t", -1);
			release = g_strchomp (g_strdup (split[1]));
			g_strfreev (split);

			/* get stable tree */
			svntree = g_strconcat (svn_root, "branches/STABLE_", release, "/pkgs/", NULL);
			g_free (release);
			g_free (codename);
		}
		set_svntree (svntree);
	}
}

/* Get mnpb Direcroty */
gchar*
get_mnpb_dir ()
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_get_string (settings, "mnpb-rootdir");
}

/* Set mnpb Direcroty */
gboolean
set_mnpb_dir (const gchar* mnpb_dir)
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_set_string (settings, "mnpb-rootdir", mnpb_dir);
}


/* Create mnpb Directories */
void
init_rpmbuild_dir ()
{
	gchar *mnpb_root_dir;
	const gchar *user_data_dir;
	gchar *mnpb_dir;

	gchar *rpmdir_list[] = {"SOURCES", 
				"BUILD", 
				"BUILDROOT", 
				"RPMS", 
				"SPECS", 
				"SRPMS",
				NULL};

	gchar *dirname;
	gint i;

	mnpb_dir = get_mnpb_dir ();
	if (g_utf8_strlen (mnpb_dir, 0) == 0)
	{
		mnpb_dir = "mnpb";
		user_data_dir = g_get_user_data_dir ();
		mnpb_root_dir = g_build_filename (user_data_dir, mnpb_dir, NULL);
	}
	else
	{
		mnpb_root_dir = g_strdup (mnpb_dir);
	}
	if (!g_file_test (mnpb_root_dir, G_FILE_TEST_IS_DIR | G_FILE_TEST_EXISTS))
	{
		g_mkdir_with_parents (mnpb_root_dir, S_IRUSR | S_IWUSR | S_IXUSR);
	}
	i = 0;
	while (rpmdir_list[i] != NULL)
	{
		dirname = g_build_filename (mnpb_root_dir, rpmdir_list[i], NULL);
		if (!g_file_test (dirname, G_FILE_TEST_IS_DIR | G_FILE_TEST_EXISTS))
		{
			g_mkdir_with_parents (dirname, S_IRUSR | S_IWUSR | S_IXUSR);
		}
		g_free (dirname);
		i++;
	}
	set_mnpb_dir (mnpb_root_dir);
	g_free (mnpb_root_dir);
}

/* get proxy server */
gchar*
get_proxy_server ()
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_get_string (settings, "proxy-server");
}

/* set proxy server */
gboolean
set_proxy_server (const gchar* server)
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_set_string (settings, "proxy-server", server);
}

/* get proxy port */
gchar*
get_proxy_port ()
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_get_string (settings, "proxy-port");
}

/* set proxy port */
gboolean
set_proxy_port (const gchar* port)
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_set_string (settings, "proxy-port", port);
}

/* get use proxy */
gboolean
get_use_proxy ()
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_get_boolean (settings, "use-proxy");
}

/* set use proxy */
gboolean
set_use_proxy (const gboolean use_proxy)
{
	GSettings *settings;

	settings = g_settings_new ("org.momonga.mnpb");
	return g_settings_set_boolean (settings, "use-proxy", use_proxy);
}

/* init proxy server */
void
init_proxy ()
{
	return;
}

/* Init rpmrc */
#define BUFSIZE 1024

void
init_rpmmacros ()
{
	gchar *rpmmacros;
	gchar *contents;

	rpmmacros = g_build_filename (get_mnpb_dir (), "rpmmacros", NULL);

	if (!g_file_test (rpmmacros, G_FILE_TEST_EXISTS))
	{
		contents = g_strconcat ("%_topdir ", get_mnpb_dir (), NULL);
		g_file_set_contents (rpmmacros, contents, -1, NULL);
		g_free (contents);
	}
	g_free (rpmmacros);
}

