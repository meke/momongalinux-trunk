#include "mnpb_settings.h"
#include "mnpb_util.h"

typedef struct _PipeLoop
{
	GMainLoop *loop;
	gint	count;
	gint	status;
} PipeLoop;

/* call back for spawn_async_with_pipe */
/* waiting for exit child process */
void
cb_child_watch (GPid pid, gint status, PipeLoop *ploop)
{
	ploop->status = status;
	ploop->count--;
	if (ploop->count == 0)
	{
		g_main_loop_quit (ploop->loop);
	}
	g_spawn_close_pid (pid);
}

/* output child process's stdout */
gboolean
cb_out_watch (GIOChannel *channel, GIOCondition cond, PipeLoop *ploop)
{
	gchar *string;
	gsize size;
	GError *error = NULL;

	if (cond == G_IO_HUP)
	{
		g_io_channel_unref (channel);
		ploop->count--;
		if (ploop->count == 0)
		{
			g_main_loop_quit (ploop->loop);
		}
		return FALSE;
	}
	g_io_channel_read_line (channel, &string, &size, NULL, &error);
	g_assert_no_error (error);
	g_print (string);
	g_free (string);
	return TRUE;
}

/* output child process's stdout */
gboolean
cb_err_watch( GIOChannel *channel, GIOCondition cond, PipeLoop *ploop)
{
	gchar *string;
	gsize  size;
	GError *error = NULL;

	if (cond == G_IO_HUP)
	{
		g_io_channel_unref (channel);
		ploop->count--;
		if (ploop->count == 0)
		{
			g_main_loop_quit (ploop->loop);
		}
		return FALSE;
	}

	g_io_channel_read_line (channel, &string, &size, NULL, &error);
	g_assert_no_error (error);
	g_print (string);
	g_free (string );

	return TRUE;
}

/* execute command line */
#define AUTH_TRY 3
gboolean
execute_command_line (gchar* command_line)
{
	PipeLoop ploop;
	GError *error = NULL;
	gint out, err;
	GPid pid;
	GIOChannel *out_ch, *err_ch;
	gchar **args;
	gint auth_count = 0;
	gint status;

	g_shell_parse_argv (command_line, NULL, &args, &error);
	g_assert_no_error (error);

	auth_count = 0;
	while (auth_count < AUTH_TRY)
	{
		g_spawn_async_with_pipes (NULL, args, NULL,
			G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD, NULL,
			NULL, &pid, NULL, &out, &err, &error);
		g_assert_no_error (error);

		ploop.loop = g_main_loop_new (NULL, TRUE);
		ploop.count = 0;
		g_child_watch_add (pid, (GChildWatchFunc)cb_child_watch, &ploop);
		ploop.count++;

		out_ch = g_io_channel_unix_new (out);
		err_ch = g_io_channel_unix_new (err);

		g_io_add_watch (out_ch, G_IO_IN | G_IO_HUP, (GIOFunc)cb_out_watch, &ploop);
		ploop.count++;
		g_io_add_watch (err_ch, G_IO_IN | G_IO_HUP, (GIOFunc)cb_err_watch, &ploop);
		ploop.count++;

		g_main_loop_run (ploop.loop);

		/* pkexec return 127 for authenticate error */
		if (g_strcmp0 (args[0], "pkexec") == 0 && WEXITSTATUS (ploop.status) == 127)
		{
			auth_count++;
		}
		else
		{
			break;
		}
	}
	g_strfreev (args);

	if (ploop.status == 0)
	{
		return TRUE;
	}
	return FALSE;
}

/* get from svn server */
gchar*
get_spec_from_svn (gchar* package_name)
{
	gchar *root_directory;
	gchar *command_line;
	gboolean ret;
	gchar *spec_file_name;
	gchar *spec_base_name;
	gchar *from, *to;


	/* set workdir to mnpbroot */
	root_directory = get_mnpb_dir ();
	g_chdir (root_directory);

	/* get SPEC file from svn */
	command_line = g_strconcat ("svn export --force ", get_svntree (), 
		package_name, " SOURCES", NULL);
	ret = execute_command_line (command_line);
	g_free (command_line);

	/* move spec file to SPECS direcroty */
	spec_file_name = g_strconcat (package_name, ".spec", NULL);
	from = g_build_filename (root_directory, "SOURCES", spec_file_name, NULL);
	to = g_build_filename (root_directory, "SPECS", spec_file_name, NULL);
	g_free (spec_file_name);

	command_line = g_strconcat ("mv " , from, " ", to, NULL);
	g_free (from);
	g_free (to);

	ret = execute_command_line (command_line);
	g_free (command_line);

	spec_base_name = g_strconcat (package_name, ".spec", NULL);
	spec_file_name = g_build_filename (root_directory, "SPECS", spec_base_name, NULL);
	g_free (spec_base_name);
	return spec_file_name;
}


/* SPEC RESULTS are Require, Source, Package, and empty line */
#define REQ "Require="
#define SRC "Source="
#define PKG "Package="
gboolean
parse_spec (gchar* spec_file_name, mnpb_spec *spec)
{
	gint status;
	gchar *output, *error;
	GError *err = NULL;
	gchar *command_line;
	gchar *parse_result;
	gchar **lines;
	gint i;
	GRegex *regex;

	command_line = g_strconcat (PKGLIBEXECDIR, "/mnpb_parse_spec ", spec_file_name, NULL);
	g_spawn_command_line_sync (command_line, &output, &error, &status, &err);
	g_assert_no_error (err);
	g_free (command_line);

	if (status)
	{
		g_print (_("Error: spec file parsing %s\n"), spec_file_name);
		return FALSE;
	}

	lines = g_strsplit_set (output, "\n", -1);
	i = 0;
	while (g_strcmp0 (lines[i], ""))
	{
		if (g_strrstr (lines[i], REQ) != NULL)
		{
			regex = g_regex_new ("Require=", 0, 0, NULL);
			spec->req = g_regex_replace_literal (regex, lines[i], -1, 0, " ", 0, NULL);
			g_regex_unref (regex);
		}
		else if (g_strrstr (lines[i], SRC) != NULL)
		{
			gchar *tmp;

			regex = g_regex_new (SRC, 0, 0, NULL);
			tmp = g_regex_replace_literal (regex, lines[i], -1, 0, "", 0, NULL);
			g_regex_unref (regex);

			regex = g_regex_new ("http://dl\.sourceforge\.net",
						0, 0, NULL);
			spec->src = g_regex_replace_literal (regex, tmp, -1, 0,
						"http://jaist.dl.sourceforge.net", 0, NULL);
			g_regex_unref (regex);
			g_free (tmp);
		}
		else if (g_strrstr (lines[i], PKG) != NULL)
		{
			regex = g_regex_new (PKG, 0, 0, NULL);
			spec->pkg = g_regex_replace_literal (regex, lines[i], -1, 0, "", 0, NULL);
			g_regex_unref (regex);
		}
		i++;
	}
	g_strfreev (lines);

	return TRUE;
}

/* install requred package */
gboolean
install_req (gchar *req)
{
	gchar *command_line;
	gboolean ret;

	command_line = g_strconcat ("rpm -q build-momonga ", req, NULL);
	g_print (_("Check Required Packages:\t%s"), command_line);

	ret = execute_command_line (command_line);
	g_free (command_line);
	if (ret)
	{
		return TRUE;
	}
	else
	{
		command_line = g_strconcat ("pkexec ",
						"yum install -y build-momonga ", req, NULL);
		g_print (_("Install Required Packages:\t%s"), command_line);

		ret = execute_command_line (command_line);
		g_free (command_line);
		if (ret)
		{
			g_print ("\n");
			return TRUE;
		}
		else
		{
			g_print (_("Error In Installing Required Packages\n"));
			return FALSE;
		}
	}
}

/* get source */
gboolean
get_sources (gchar *src)
{
	gchar *command_line;
	gboolean ret;
	gchar *proxy;

	/* no nosource */
	if (g_utf8_strlen (src, 1) == 0)
	{
		g_print (_("No Sources Download\n"));
		return TRUE;
	}

	if (get_use_proxy ())
	{
		proxy = g_strconcat (" --execute=http_proxy=",
						get_proxy_server (), ":", get_proxy_port (), " ", NULL);
	}
	else
	{
		proxy = g_strconcat ("", NULL);
	}
	command_line = g_strconcat ("wget --inet4-only ", proxy, 
					" -P SOURCES ", src, NULL);
	g_free (proxy);
	g_print (_("Get Source from the Internet:\t%s"), command_line);

	ret = execute_command_line (command_line);
	g_free (command_line);

	if (ret)
	{
		g_print ("\n");
		return TRUE;
	}
	else
	{
		g_print (_("Error In Get Sources With Wget\n"));
		return FALSE;
	}
}

/* check sha256sum */
gboolean
check_sha256sum (gchar *src)
{
	gchar *sources;
	gchar *contents;
	gchar **lines;

	gchar **src_list;
	gint i, j;
	gchar **checksum_item;
	gchar *edited = "", *tmp;
	gchar *filename;
	gchar *src_dir;

	gchar *command_line;
	gboolean ret;

	/* no nosource */
	if (g_utf8_strlen (src, 1) == 0)
	{
		g_print (_("No Sources Need CheckSum\n"));
		return TRUE;
	}

	/* get sources file contents */
	sources = g_build_filename (get_mnpb_dir (), "SOURCES", "sources", NULL);
	if (!g_file_test (sources, G_FILE_TEST_EXISTS))
	{
		return TRUE;
	}
	g_file_get_contents (sources, &contents, NULL, NULL);

	/* compare sources and src */
	lines = g_strsplit (contents, "\n", -1);
	src_list = g_strsplit (src, " ", -1);
	for (i = 0;;i++)
	{
		if (g_strcmp0 (lines[i], "") == 0)
		{
			break;
		}
		for (j = 0;;j++)
		{
			if (g_strcmp0 (src_list[j], "") == 0)
			{
				break;
			}
			filename = g_path_get_basename (src_list[j]);
			if (g_strrstr (lines[i], filename) != NULL)
			{
				tmp = g_strdup (edited);
				checksum_item = g_strsplit (lines[i], " ", -1);
				edited = g_strconcat (checksum_item[0], "  ", filename, "\n", NULL);
				g_strfreev (checksum_item);
				g_free (tmp);
			}
		}
	}
	g_strfreev (lines);
	g_strfreev (src_list);
		
	/* set sources file contents */
	g_file_set_contents (sources, edited, -1, NULL);
	g_free (edited);

	/* change directory SOURCES and sha256sum check */
	src_dir = g_build_filename (get_mnpb_dir (), "SOURCES", NULL);
	g_chdir (src_dir);
	g_free (src_dir);

	command_line = "sha256sum -c sources";
	ret = execute_command_line (command_line);

	src_dir = get_mnpb_dir ();
	g_chdir (src_dir);
	if (!ret)
	{
		g_print (_("SHA256SUM ERROR\n"));
		return FALSE;
	}
	return TRUE;
}

/* build package */
gboolean
build_package (gchar *spec)
{
	gchar *command_line;
	gboolean ret;

	command_line = g_strconcat ("rpmbuild -bb --clean ",
			"--rcfile /usr/lib/rpm/rpmrc:",
				"/usr/lib/rpm/momonga/rpmrc",
				" ",
			"--macros /usr/lib/rpm/macros:",
				"/usr/lib/rpm/platform/", get_arch (), "-linux/macros:",
				"/usr/lib/rpm/momonga/macros:",
				"/etc/rpm/macros.*:",
				"/etc/rpm/macros:",
				"/etc/rpm/", get_arch (), "-linux/macros:",
				"~/.rpmmacros:",
				get_mnpb_dir (), "/rpmmacros"
				" ",
				spec, NULL);
	g_print (_("Build Package:\t%s"), command_line);

	ret = execute_command_line (command_line);
	g_free (command_line);

	if (ret)
	{
		g_print ("\n");
		return TRUE;
	}
	else
	{
		g_print (_("Error In Build Package\n"));
		return FALSE;
	}
}

/* clean up SOURCE dir */
void
clean_sources ()
{
	GDir *src_dir;
	gchar *src_dir_name;
	gchar *f_name;
	gchar *del_f_name;
		
	src_dir_name = g_build_filename (get_mnpb_dir (), "SOURCES", NULL);
	src_dir = g_dir_open (src_dir_name, 0, NULL);
	while (1)
	{
		f_name = g_strdup (g_dir_read_name (src_dir));
		if (f_name == NULL)
		{
			break;
		}
		del_f_name = g_build_filename (src_dir_name, f_name, NULL);
		g_remove (del_f_name);
		g_free (del_f_name);
		g_free (f_name);
	}
	g_dir_close (src_dir);
	g_free (src_dir_name);
}
		
/* install package */
gboolean
install_package (gchar *pkg)
{
	gchar *command_line;
	gboolean ret;
	gchar **pkgs;
	GString *packages;	
	gint i;

	pkgs = g_strsplit (pkg, " ", -1);

	packages = g_string_new ("");
	for (i = 0; g_strcmp0 (pkgs[i], ""); i++)
	{
		g_string_append_printf (packages, "%s/RPMS/%s ", get_mnpb_dir (), pkgs[i]);
	}

	command_line = g_strconcat ("pkexec ",
			"yum install -y ", packages->str, NULL);
	g_string_free (packages, TRUE);
	g_print (_("Install Built Package:\t%s\n"), command_line);

	ret = execute_command_line (command_line);
	g_free (command_line);

	if (ret)
	{
		g_print ("\n");
		return TRUE;
	}
	else
	{
		g_print (_("Error In Install Package\n"));
		return FALSE;
	}
}

