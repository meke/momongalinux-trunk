#include "mnpb_ui.h"
#include "mnpb_settings.h"
#include "mnpb_util.h"

/* Global Variable */
GtkBuilder *builder;

/* redraw */
static void
redraw ()
{
		while (gtk_events_pending())
		{
			gtk_main_iteration();
		}
}

/* GPrintFunc for handler */
void
print_to_textview (const gchar* text)
{
	GtkTextView *textview;
	GtkTextBuffer *buffer;
	GtkTextIter iter;

	if (g_utf8_validate (text, -1, NULL))
	{
		textview = GTK_TEXT_VIEW (gtk_builder_get_object (builder, "textview1"));
		buffer = gtk_text_view_get_buffer (textview);
		gtk_text_buffer_get_end_iter (buffer, &iter);
		gtk_text_buffer_insert (buffer, &iter, text, -1);
		gtk_text_view_scroll_to_iter (textview, &iter, 0, FALSE, 0, 0);
		redraw ();
	}
}

/* set statusbar text */
static void
set_statusbar_text (const gchar* text)
{
	GtkStatusbar *statusbar;

	statusbar = GTK_STATUSBAR (gtk_builder_get_object (builder, "statusbar1"));
	gtk_statusbar_pop (statusbar, 0);
	gtk_statusbar_push (statusbar, 0, text);
}

/* Show Done Dialog Box */
static void
mnpb_done ()
{
	GtkWidget *dialog;
	GtkWidget *parent;

	parent = GTK_WIDGET (gtk_builder_get_object (builder, "window1"));
	dialog = gtk_message_dialog_new (GTK_WINDOW (parent), 0, GTK_MESSAGE_INFO, 
		GTK_BUTTONS_OK, _("Install Done"));
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

/* Ask install */
static gboolean
mnpb_is_install ()
{
	GtkWidget *dialog;
	GtkWidget *parent;
	gint ret;

	parent = GTK_WIDGET (gtk_builder_get_object (builder, "window1"));
	dialog = gtk_message_dialog_new (GTK_WINDOW (parent), 0, GTK_MESSAGE_QUESTION, 
		GTK_BUTTONS_YES_NO, _("Do you want to install Built Packages?"));
	ret = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
	if (ret == GTK_RESPONSE_YES)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/* add item to treeview */
static void
add_item (GtkTreeView *treeview, gchar *text)
{
	GtkTreeIter iter;
	GtkListStore *store;

	store = GTK_LIST_STORE (gtk_tree_view_get_model (treeview));
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter, 0, text, -1);
}

/* create nonfree list */
static void
create_nonfree_list (GtkTreeView *treeview)
{
	GFile *file;
	GInputStream *in;
	GFileInputStream *fin;
	GDataInputStream *din;
	gsize length;
	gchar *line;
	gchar *arch;
	gchar **list;

	file = g_file_new_for_path (PKGDATADIR "/nonfree.csv");
	in = (GInputStream *)g_file_read (file, NULL, NULL);
	din = g_data_input_stream_new (in);
	arch = get_arch ();
	while (line = g_data_input_stream_read_line (din, &length, NULL, NULL))
	{
		list = g_strsplit (line, ",", 0);
		if ((g_strcmp0 (list[1], arch) == 0) || (g_strcmp0 (list[2], arch) == 0))
		{
			add_item (treeview, list[0]);
		}
		g_strfreev (list);
	}
	g_object_unref (in);
}

/* get selected package name from GtkTreeView */
static gchar*
get_package_name_from_list ()
{
	GtkTreeView *treeview;
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *package_name;

	/* get Widget GtkTreeVew and GtkTreeSelection */
	treeview = GTK_TREE_VIEW (gtk_builder_get_object (builder, "treeview1"));
	selection = gtk_tree_view_get_selection (treeview);

	/* get selected iter and iter value as a string */
	if (gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		gtk_tree_model_get (model, &iter, 0, &package_name, -1);
		return package_name;
	}
	else
	{
		return NULL;
	}
}

/* set tool button sensitive */
static void
set_tool_buttons_sensitive (gboolean sensitive)
{
	GtkWidget *build_button, *logsave_button, *install_button, *setting_button, *quit_button;

	build_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton1"));
	logsave_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton2"));
	install_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton3"));

	setting_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton4"));
	quit_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton5"));

	gtk_widget_set_sensitive (build_button, sensitive);
	gtk_widget_set_sensitive (logsave_button, sensitive);
	gtk_widget_set_sensitive (install_button, sensitive);
	gtk_widget_set_sensitive (setting_button, sensitive);
	gtk_widget_set_sensitive (quit_button, sensitive);
}

/* Call Back Functions */
G_MODULE_EXPORT void
cb_exec (GtkAction *action)
{
	gchar *selected;
	gchar *spec;
	mnpb_spec pspec;
	gboolean ret, install;

	/* toolbutton unsensitive */
	set_tool_buttons_sensitive (FALSE);

	/* get package name */
	selected = get_package_name_from_list ();
	if (selected == NULL)
	{
		g_print (_("No Package is selected\n"));
		set_tool_buttons_sensitive (TRUE);
		return;
	}
	/* download and parse spec */
	set_statusbar_text (_("Download SPEC file"));
	redraw ();
	spec = get_spec_from_svn (selected);

	/* parse spec file */
	if (!parse_spec (spec, &pspec))
	{
		set_tool_buttons_sensitive (TRUE);
		g_free (spec);
		return;
	}

	/* install requred package */
	set_statusbar_text (_("Install Required Packages"));
	redraw ();
	ret = install_req (pspec.req);
	if (!ret)
	{
		g_print (_("Error Install Required Package\n"));
		set_tool_buttons_sensitive (TRUE);
		g_free (spec);
		g_free (pspec.req);
		g_free (pspec.src);
		g_free (pspec.pkg);
		return;
	}

	/* get source */
	set_statusbar_text (_("Download Source files"));
	redraw ();
	ret = get_sources (pspec.src);
	if (!ret)
	{
		g_print (_("Error Getting Source Files\n"));
		set_tool_buttons_sensitive (TRUE);
		g_free (spec);
		g_free (pspec.req);
		g_free (pspec.src);
		g_free (pspec.pkg);
		return;
	}

	/* check sha256sum */
	ret = check_sha256sum (pspec.src);
	if (!ret)
	{
		g_print (_("Error CheckSum Source Files\n"));
		set_tool_buttons_sensitive (TRUE);
		g_free (spec);
		g_free (pspec.req);
		g_free (pspec.src);
		g_free (pspec.pkg);
		return;
	}

	/* build package */
	set_statusbar_text (_("Build Package"));
	redraw ();
	ret = build_package (spec);
	if (!ret)
	{
		g_print (_("Error Build Package\n"));
		set_tool_buttons_sensitive (TRUE);
		g_free (spec);
		g_free (pspec.req);
		g_free (pspec.src);
		g_free (pspec.pkg);
		return;
	}
	set_statusbar_text (_("Build Done"));
	redraw ();

	/* clean up SOURCE dir */
	clean_sources ();

	/* Now Not Install */
	install = mnpb_is_install ();

	if (install)
	{
		/* install package */
		set_statusbar_text (_("Install Package"));
		redraw ();
		ret = install_package (pspec.pkg);
		if (!ret)
		{
			g_print (_("Error Install Package\n"));
			set_tool_buttons_sensitive (TRUE);
			g_free (spec);
			g_free (pspec.req);
			g_free (pspec.src);
			g_free (pspec.pkg);
			return;
		}
		mnpb_done ();
		set_statusbar_text (_("Install Done"));
		redraw ();
	}

	/* end */
	set_tool_buttons_sensitive (TRUE);
	g_free (spec);
	g_free (pspec.req);
	g_free (pspec.src);
	g_free (pspec.pkg);
}

G_MODULE_EXPORT void
cb_log_save (GtkAction *action)
{
	GtkWidget *dialog;
	GtkWindow *parent;
	GtkTextView *textview;
	GtkTextBuffer *buffer;
	gchar *filename;
	gchar *log;
	GtkTextIter start, end;

	parent = GTK_WINDOW (gtk_builder_get_object (builder, "window1"));
	dialog = gtk_file_chooser_dialog_new ("Save Log File", parent,
											GTK_FILE_CHOOSER_ACTION_SAVE,
											GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
											GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
											NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	{

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		textview = GTK_TEXT_VIEW (gtk_builder_get_object (builder, "textview1"));
		buffer = gtk_text_view_get_buffer (textview);

		gtk_text_buffer_get_start_iter (buffer, &start);
		gtk_text_buffer_get_end_iter (buffer, &end);
		log = gtk_text_buffer_get_text (buffer, &start, &end, TRUE);
		g_file_set_contents (filename, log, -1, NULL);
		g_free (filename);
		g_free (log);
	}
	gtk_widget_destroy (dialog);
}

G_MODULE_EXPORT void
cb_install (GtkAction *action)
{
	GError *err = NULL;
	gchar *command_line;


	command_line = g_build_filename (PKGLIBEXECDIR, "mnpb-install", NULL);
	g_spawn_command_line_sync (command_line, NULL, NULL, NULL, &err);
	g_assert_no_error (err);
	g_free (command_line);
}

G_MODULE_EXPORT void
cb_proxy_toggled  (GtkAction *action)
{
	GtkWidget *use_proxy;
	GtkWidget *proxy_server;
	GtkWidget *proxy_port;
	gboolean status;

	use_proxy = GTK_WIDGET (gtk_builder_get_object (builder, "checkbutton1"));
	proxy_server = GTK_WIDGET (gtk_builder_get_object (builder, "entry1"));
	proxy_port = GTK_WIDGET (gtk_builder_get_object (builder, "entry2"));
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (use_proxy)))
	{
		gtk_widget_set_sensitive (proxy_server, TRUE);
		gtk_widget_set_sensitive (proxy_port, TRUE);
	}
	else
	{
		gtk_widget_set_sensitive (proxy_server, FALSE);
		gtk_widget_set_sensitive (proxy_port, FALSE);
	}
}

G_MODULE_EXPORT void
cb_setting (GtkAction *action)
{
	GtkWidget *parent;
	GtkWidget *dialog;
	GtkWidget *use_proxy;
	GtkWidget *proxy_server;
	GtkWidget *proxy_port;
	gint result;

	GtkWidget *msg;

	parent = GTK_WIDGET (gtk_builder_get_object (builder, "window1"));
	dialog = GTK_WIDGET (gtk_builder_get_object (builder, "dialog1"));
	use_proxy = GTK_WIDGET (gtk_builder_get_object (builder, "checkbutton1"));
	proxy_server = GTK_WIDGET (gtk_builder_get_object (builder, "entry1"));
	proxy_port = GTK_WIDGET (gtk_builder_get_object (builder, "entry2"));

	/* get proxy setting */
	if (get_use_proxy ())
	{
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (use_proxy), TRUE);
		gtk_widget_set_sensitive (proxy_server, TRUE);
		gtk_widget_set_sensitive (proxy_port, TRUE);
		gtk_entry_set_text (GTK_ENTRY (proxy_server), get_proxy_server ());
		gtk_entry_set_text (GTK_ENTRY (proxy_port), get_proxy_port ());
	}
	else
	{
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (use_proxy), FALSE);
		gtk_widget_set_sensitive (proxy_server, FALSE);
		gtk_widget_set_sensitive (proxy_port, FALSE);
		gtk_entry_set_text (GTK_ENTRY (proxy_server), "");
		gtk_entry_set_text (GTK_ENTRY (proxy_port), "");
	}
	result = gtk_dialog_run (GTK_DIALOG (dialog));

	/* set proxy */
	if (result != GTK_RESPONSE_CANCEL)
	{
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (use_proxy)) && 
			gtk_entry_get_text_length (GTK_ENTRY (proxy_server)) != 0 &&
			gtk_entry_get_text_length (GTK_ENTRY (proxy_port)) != 0)
		{
			set_use_proxy (TRUE);
			set_proxy_server (gtk_entry_get_text (GTK_ENTRY (proxy_server)));
			set_proxy_port (gtk_entry_get_text (GTK_ENTRY (proxy_port)));
		}
		else
		{
			set_use_proxy (FALSE);
			set_proxy_server ("");
			set_proxy_port ("");
		}
	}

	/* end */
	gtk_widget_hide (dialog);
}

G_MODULE_EXPORT void
cb_quit (GtkAction *action)
{
	gtk_main_quit ();
}

G_MODULE_EXPORT void
cb_about (GtkAction *action)
{
	GtkWidget *about_dlg;

	about_dlg = GTK_WIDGET (gtk_builder_get_object (builder, "aboutdialog1"));
	gtk_dialog_run (GTK_DIALOG (about_dlg));
	gtk_widget_hide (about_dlg);
}

/* Call from main () */

void
init_ui ()
{
	GtkWindow *window;
	GtkTreeView *treeview;
	GPtrArray *nonfree_list;
	GtkWidget *logsave_button;
	GtkWidget *install_button;

	builder = gtk_builder_new ();
	gtk_builder_add_from_file (builder, PKGDATADIR "/mnpb.ui", NULL);
	window = GTK_WINDOW (gtk_builder_get_object (builder, "window1"));
	treeview = GTK_TREE_VIEW (gtk_builder_get_object (builder, "treeview1"));
	logsave_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton2"));
	install_button = GTK_WIDGET (gtk_builder_get_object (builder, "toolbutton3"));

	/* Log Save Button unsensitive */
	gtk_widget_set_sensitive (logsave_button, FALSE);

	/* read nonfree.csv */
	create_nonfree_list (treeview);

	/* signale init */
	gtk_builder_connect_signals (builder, NULL);

	/* visible window */
	gtk_widget_show_all (GTK_WIDGET (window));
}

