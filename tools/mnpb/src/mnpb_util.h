#ifndef MNPB_UTIL_H
#define MNPB_UTIL_H

#include <glib.h>
#include <glib/gi18n.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <config.h>

/* get spec file from svn */
gchar* get_spec_from_svn (gchar* spec_name);

/* result parsed spec file */
typedef struct _mnpb_spec {
	gchar *req;
	gchar *src;
	gchar *pkg;
} mnpb_spec;

/* parse spec file */
gboolean parse_spec (gchar* spec_file_name, mnpb_spec *spec);

/* install requred package */
gboolean install_req (gchar* req);

/* get source */
gboolean get_sources (gchar* src);

/* check sha256sum */
gboolean check_sha256sum (gchar *src);

/* build package */
gboolean build_package (gchar *spec);

/* clean up SOURCE dir */
void clean_sources ();

/* install package */
gboolean install_package (gchar *pkg);

#endif /* MNPB_UTIL_H */

