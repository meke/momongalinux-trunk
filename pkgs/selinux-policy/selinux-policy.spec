%global momorel 1
%global _usr /usr

%define distro redhat
%define polyinstatiate n
%define monolithic n
%if %{?BUILD_TARGETED:0}%{!?BUILD_TARGETED:1}
%define BUILD_TARGETED 1
%endif
%if %{?BUILD_MINIMUM:0}%{!?BUILD_MINIMUM:1}
%define BUILD_MINIMUM 1
%endif
%if %{?BUILD_MLS:0}%{!?BUILD_MLS:1}
%define BUILD_MLS 1
%endif
%define POLICYVER 29
%define POLICYCOREUTILSVER 2.3
%define CHECKPOLICYVER 2.3
%define libsepolver 2.3
Summary: SELinux policy configuration
Name: selinux-policy
Version: 3.13.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
Source: serefpolicy-%{version}.tgz
Source1: modules-targeted-base.conf 
Source31: modules-targeted-contrib.conf
Source2: booleans-targeted.conf
Source3: Makefile.devel
Source4: setrans-targeted.conf
Source5: modules-mls-base.conf
Source32: modules-mls-contrib.conf
Source6: booleans-mls.conf
Source8: setrans-mls.conf
Source14: securetty_types-targeted
Source15: securetty_types-mls
#Source16: modules-minimum.conf
Source17: booleans-minimum.conf
Source18: setrans-minimum.conf
Source19: securetty_types-minimum
Source20: customizable_types
Source21: config.tgz
Source22: users-mls
Source23: users-targeted
Source25: users-minimum
Source26: file_contexts.subs_dist
Source27: selinux-policy.conf
Source28: permissivedomains.pp
Source29: serefpolicy-contrib-%{version}.tgz
Source30: booleans.subs_dist

Patch0: policy-rawhide-base.patch
Patch1: policy-rawhide-contrib.patch
Patch2: policy-rawhide-base-user_tmp.patch
Patch3: policy-rawhide-contrib-user_tmp.patch

Url: http://oss.tresys.com/repos/refpolicy/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: python gawk checkpolicy >= %{CHECKPOLICYVER} m4 policycoreutils-python >= %{POLICYCOREUTILSVER} bzip2 
Requires(pre): policycoreutils >= %{POLICYCOREUTILSVER} libsemanage >= 2.0.46
Requires(post): bzip2 coreutils gawk
Requires: checkpolicy >= %{CHECKPOLICYVER} m4 
Obsoletes: selinux-policy-devel <= %{version}-%{release}
Provides: selinux-policy-devel = %{version}-%{release}

%description 
SELinux Base package

%files 
%defattr(-,root,root,-)
%dir %{_datadir}/selinux
%dir %{_datadir}/selinux/packages
%dir %{_sysconfdir}/selinux
%ghost %config(noreplace) %{_sysconfdir}/selinux/config
%ghost %{_sysconfdir}/sysconfig/selinux
%{_usr}/lib/tmpfiles.d/selinux-policy.conf
%attr(0755, root, root) %dir %{_rpmconfigdir}
%attr(0755, root, root) %dir %{_rpmconfigdir}/macros.d
%{_rpmconfigdir}/macros.d/macros.selinux-policy

%package sandbox
Summary: SELinux policy sandbox
Group: System Environment/Base
Requires(pre): selinux-policy-base = %{version}-%{release}

%description sandbox
SELinux sandbox policy used for the policycoreutils-sandbox package

%files sandbox
%defattr(-,root,root,-)
%verify(not md5 size mtime) /usr/share/selinux/packages/sandbox.pp

%post sandbox
rm -f /etc/selinux/*/modules/active/modules/sandbox.pp.disabled 2>/dev/null
semodule -n -i /usr/share/selinux/packages/sandbox.pp
if /usr/sbin/selinuxenabled ; then
    /usr/sbin/load_policy
fi;
exit 0

%preun sandbox
semodule -n -d sandbox 2>/dev/null
if /usr/sbin/selinuxenabled ; then
    /usr/sbin/load_policy
fi;exit 0

%package devel
Summary: SELinux policy devel
Group: System Environment/Base
Requires(pre): selinux-policy = %{version}-%{release}
Requires: m4 checkpolicy >= %{CHECKPOLICYVER}
Requires: /usr/bin/make
Requires(post): policycoreutils-devel >= %{POLICYCOREUTILSVER}

%description devel
SELinux policy development and man page package

%files devel
%defattr(-,root,root,-)
%{_mandir}/man*/*
%{_mandir}/ru/*/*
%dir %{_datadir}/selinux/devel
%dir %{_datadir}/selinux/devel/include
%{_datadir}/selinux/devel/include/*
%dir %{_datadir}/selinux/devel/html
%{_datadir}/selinux/devel/html/*html
%{_datadir}/selinux/devel/html/*css
%{_datadir}/selinux/devel/Makefile
%{_datadir}/selinux/devel/example.*
%{_datadir}/selinux/devel/policy.*

%post devel
selinuxenabled && /usr/bin/sepolgen-ifgen 2>/dev/null
exit 0


%package doc
Summary: SELinux policy documentation
Group: System Environment/Base
Requires(pre): selinux-policy = %{version}-%{release}
Requires: xdg-utils

%description doc
SELinux policy documentation package

%files doc
%defattr(-,root,root,-)
#%%doc %{_datadir}/doc/%{name}-%{version}
%doc %{_datadir}/doc/%{name}
%attr(755,root,root) %{_datadir}/selinux/devel/policyhelp

%define makeCmds() \
make UNK_PERMS=%4 NAME=%1 TYPE=%2 DISTRO=%{distro} UBAC=n DIRECT_INITRC=%3 MONOLITHIC=%{monolithic} MLS_CATS=1024 MCS_CATS=1024 bare \
make UNK_PERMS=%4 NAME=%1 TYPE=%2 DISTRO=%{distro} UBAC=n DIRECT_INITRC=%3 MONOLITHIC=%{monolithic} MLS_CATS=1024 MCS_CATS=1024  conf \
cp -f selinux_config/booleans-%1.conf ./policy/booleans.conf \
cp -f selinux_config/users-%1 ./policy/users \
#cp -f selinux_config/modules-%1-base.conf  ./policy/modules.conf \

%define makeModulesConf() \
cp -f selinux_config/modules-%1-%2.conf  ./policy/modules-base.conf \
cp -f selinux_config/modules-%1-%2.conf  ./policy/modules.conf \
if [ %3 == "contrib" ];then \
	cp selinux_config/modules-%1-%3.conf ./policy/modules-contrib.conf; \
	cat selinux_config/modules-%1-%3.conf >> ./policy/modules.conf; \
fi; \

%define installCmds() \
make UNK_PERMS=%4 NAME=%1 TYPE=%2 DISTRO=%{distro} UBAC=n DIRECT_INITRC=%3 MONOLITHIC=%{monolithic} MLS_CATS=1024 MCS_CATS=1024 SEMOD_EXP="/usr/bin/semodule_expand -a" base.pp \
make validate UNK_PERMS=%4 NAME=%1 TYPE=%2 DISTRO=%{distro} UBAC=n DIRECT_INITRC=%3 MONOLITHIC=%{monolithic} MLS_CATS=1024 MCS_CATS=1024 SEMOD_EXP="/usr/bin/semodule_expand -a" modules \
make UNK_PERMS=%4 NAME=%1 TYPE=%2 DISTRO=%{distro} UBAC=n DIRECT_INITRC=%3 MONOLITHIC=%{monolithic} DESTDIR=%{buildroot} MLS_CATS=1024 MCS_CATS=1024 install \
make UNK_PERMS=%4 NAME=%1 TYPE=%2 DISTRO=%{distro} UBAC=n DIRECT_INITRC=%3 MONOLITHIC=%{monolithic} DESTDIR=%{buildroot} MLS_CATS=1024 MCS_CATS=1024 install-appconfig \
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/selinux/%1/logins \
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/selinux/%1/policy \
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/selinux/%1/modules/active/modules \
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/selinux/%1/contexts/files \
touch %{buildroot}/%{_sysconfdir}/selinux/%1/modules/semanage.read.LOCK \
touch %{buildroot}/%{_sysconfdir}/selinux/%1/modules/semanage.trans.LOCK \
rm -rf %{buildroot}%{_sysconfdir}/selinux/%1/booleans \
touch %{buildroot}%{_sysconfdir}/selinux/%1/policy/policy.%{POLICYVER} \
touch %{buildroot}%{_sysconfdir}/selinux/%1/contexts/files/file_contexts.subs \
install -m0644 selinux_config/securetty_types-%1 %{buildroot}%{_sysconfdir}/selinux/%1/contexts/securetty_types \
install -m0644 selinux_config/file_contexts.subs_dist %{buildroot}%{_sysconfdir}/selinux/%1/contexts/files \
install -m0644 selinux_config/setrans-%1.conf %{buildroot}%{_sysconfdir}/selinux/%1/setrans.conf \
install -m0644 selinux_config/customizable_types %{buildroot}%{_sysconfdir}/selinux/%1/contexts/customizable_types \
touch %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/seusers \
touch %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/file_contexts.local \
touch %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/nodes.local \
touch %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/users_extra.local \
touch %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/users.local \
touch %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/file_contexts.homedirs.bin \
touch %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/file_contexts.bin \
cp %{SOURCE30} %{buildroot}%{_sysconfdir}/selinux/%1 \
bzip2 -c %{buildroot}/%{_datadir}/selinux/%1/base.pp  > %{buildroot}/%{_sysconfdir}/selinux/%1/modules/active/base.pp \
rm -f %{buildroot}/%{_datadir}/selinux/%1/base.pp  \
for i in %{buildroot}/%{_datadir}/selinux/%1/*.pp; do bzip2 -c $i > %{buildroot}/%{_sysconfdir}/selinux/%1/modules/active/modules/`basename $i`; done \
rm -f %{buildroot}/%{_datadir}/selinux/%1/*pp*  \
mkdir -p %{buildroot}%{_datadir}/selinux/packages \
/usr/sbin/semodule -s %1 -n -B -p %{buildroot}; \
/usr/bin/sha512sum %{buildroot}%{_sysconfdir}/selinux/%1/policy/policy.%{POLICYVER} | cut -d' ' -f 1 > %{buildroot}%{_sysconfdir}/selinux/%1/.policy.sha512; \
rm -rf %{buildroot}%{_sysconfdir}/selinux/%1/contexts/netfilter_contexts  \
rm -rf %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/policy.kern \
ln -sf /etc/selinux/%1/policy/policy.%{POLICYVER}  %{buildroot}%{_sysconfdir}/selinux/%1/modules/active/policy.kern \
%nil

%define fileList() \
%defattr(-,root,root) \
%dir %{_datadir}/selinux/%1 \
%dir %{_sysconfdir}/selinux/%1 \
%config(noreplace) %{_sysconfdir}/selinux/%1/setrans.conf \
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/seusers \
%dir %{_sysconfdir}/selinux/%1/logins \
%dir %{_sysconfdir}/selinux/%1/modules \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/semanage.read.LOCK \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/semanage.trans.LOCK \
%dir %attr(700,root,root) %dir %{_sysconfdir}/selinux/%1/modules/active \
%dir %{_sysconfdir}/selinux/%1/modules/active/modules \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/commit_num \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/base.pp \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/file_contexts \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/file_contexts.homedirs \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/file_contexts.template \
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/seusers.final \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/netfilter_contexts \
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/users_extra \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/homedir_template \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/modules/*.pp \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/modules/active/policy.kern \
%ghost %{_sysconfdir}/selinux/%1/modules/active/*.local \
%ghost %{_sysconfdir}/selinux/%1/modules/active/*.bin \
%ghost %{_sysconfdir}/selinux/%1/modules/active/seusers \
%dir %{_sysconfdir}/selinux/%1/policy/ \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/policy/policy.%{POLICYVER} \
%{_sysconfdir}/selinux/%1/.policy.sha512 \
%dir %{_sysconfdir}/selinux/%1/contexts \
%config %{_sysconfdir}/selinux/%1/contexts/customizable_types \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/securetty_types \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/dbus_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/x_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/default_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/virtual_domain_context \
%config %{_sysconfdir}/selinux/%1/contexts/virtual_image_context \
%config %{_sysconfdir}/selinux/%1/contexts/lxc_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/systemd_contexts \
%config %{_sysconfdir}/selinux/%1/contexts/sepgsql_contexts \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/default_type \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/failsafe_context \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/initrc_context \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/removable_context \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/userhelper_context \
%dir %{_sysconfdir}/selinux/%1/contexts/files \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/contexts/files/file_contexts \
%verify(not md5 size mtime) %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.homedirs \
%ghost %{_sysconfdir}/selinux/%1/contexts/files/*.bin \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.local \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/files/file_contexts.subs \
%{_sysconfdir}/selinux/%1/contexts/files/file_contexts.subs_dist \
%{_sysconfdir}/selinux/%1/booleans.subs_dist \
%config %{_sysconfdir}/selinux/%1/contexts/files/media \
%dir %{_sysconfdir}/selinux/%1/contexts/users \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/root \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/guest_u \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/xguest_u \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/user_u \
%config(noreplace) %{_sysconfdir}/selinux/%1/contexts/users/staff_u 

%define relabel() \
. %{_sysconfdir}/selinux/config; \
FILE_CONTEXT=%{_sysconfdir}/selinux/%1/contexts/files/file_contexts; \
/usr/sbin/selinuxenabled; \
if [ $? = 0  -a "${SELINUXTYPE}" = %1 -a -f ${FILE_CONTEXT}.pre ]; then \
     /sbin/fixfiles -C ${FILE_CONTEXT}.pre restore 2> /dev/null; \
     rm -f ${FILE_CONTEXT}.pre; \
fi; \
if /sbin/restorecon -e /run/media -R /root /var/log /var/run /etc/passwd* /etc/group* /etc/*shadow* 2> /dev/null;then \
    continue; \
fi; \
if /sbin/restorecon -R /home/*/.config 2> /dev/null;then \
    continue; \
fi;

%define preInstall() \
if [ $1 -ne 1 ] && [ -s /etc/selinux/config ]; then \
     . %{_sysconfdir}/selinux/config; \
     FILE_CONTEXT=%{_sysconfdir}/selinux/%1/contexts/files/file_contexts; \
     if [ "${SELINUXTYPE}" = %1 -a -f ${FILE_CONTEXT} ]; then \
        [ -f ${FILE_CONTEXT}.pre ] || cp -f ${FILE_CONTEXT} ${FILE_CONTEXT}.pre; \
     fi; \
     touch /etc/selinux/%1/.rebuild; \
     if [ -e /etc/selinux/%1/.policy.sha512 ]; then \
        POLICY_FILE=`ls /etc/selinux/%1/policy/policy.* | sort | head -1` \
        sha512=`sha512sum $POLICY_FILE | cut -d ' ' -f 1`; \
	checksha512=`cat /etc/selinux/%1/.policy.sha512`; \
	if [ "$sha512" == "$checksha512" ] ; then \
		rm /etc/selinux/%1/.rebuild; \
	fi; \
   fi; \
fi;

%define postInstall() \
. %{_sysconfdir}/selinux/config; \
(cd /etc/selinux/%2/modules/active/modules; rm -f vbetool.pp l2tpd.pp shutdown.pp amavis.pp clamav.pp gnomeclock.pp nsplugin.pp matahari.pp xfs.pp kudzu.pp kerneloops.pp execmem.pp openoffice.pp ada.pp tzdata.pp hal.pp hotplug.pp howl.pp java.pp mono.pp moilscanner.pp gamin.pp audio_entropy.pp audioentropy.pp iscsid.pp polkit_auth.pp polkit.pp rtkit_daemon.pp ModemManager.pp telepathysofiasip.pp ethereal.pp passanger.pp qemu.pp qpidd.pp pyzor.pp razor.pp pki-selinux.pp phpfpm.pp consoletype.pp ctdbd.pp fcoemon.pp isnsd.pp rgmanager.pp corosync.pp aisexec.pp pacemaker.pp pkcsslotd.pp smstools.pp ) \
if [ -e /etc/selinux/%2/.rebuild ]; then \
   rm /etc/selinux/%2/.rebuild; \
   /usr/sbin/semodule -B -n -s %2; \
fi; \
[ "${SELINUXTYPE}" == "%2" ] && selinuxenabled && load_policy; \
if [ %1 -eq 1 ]; then \
   /sbin/restorecon -R /root /var/log /run 2> /dev/null; \
else \
%relabel %2 \
fi;

%define modulesList() \
awk '$1 !~ "/^#/" && $2 == "=" && $3 == "module" { printf "%%s.pp ", $1 }' ./policy/modules-base.conf > %{buildroot}/%{_datadir}/selinux/%1/modules-base.lst \
awk '$1 !~ "/^#/" && $2 == "=" && $3 == "base" { printf "%%s.pp ", $1 }' ./policy/modules-base.conf > %{buildroot}/%{_datadir}/selinux/%1/base.lst \
if [ -e ./policy/modules-contrib.conf ];then \
	awk '$1 !~ "/^#/" && $2 == "=" && $3 == "module" { printf "%%s.pp ", $1 }' ./policy/modules-contrib.conf > %{buildroot}/%{_datadir}/selinux/%1/modules-contrib.lst; \
fi;

%description
SELinux Reference Policy - modular.
Based off of reference policy: Checked out revision  2.20091117

%build

%prep 
%setup -n serefpolicy-contrib-%{version} -q -b 29
%patch1 -p1
%patch3 -p1
contrib_path=`pwd`
%setup -n serefpolicy-%{version} -q
%patch0 -p1
%patch2 -p1
refpolicy_path=`pwd`
cp $contrib_path/* $refpolicy_path/policy/modules/contrib

%install
rm -rf %{buildroot}

mkdir selinux_config
for i in %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4} %{SOURCE5} %{SOURCE6} %{SOURCE8} %{SOURCE14} %{SOURCE15} %{SOURCE17} %{SOURCE18} %{SOURCE19} %{SOURCE20} %{SOURCE21} %{SOURCE22} %{SOURCE23} %{SOURCE25} %{SOURCE26} %{SOURCE31} %{SOURCE32};do
 cp $i selinux_config
done
tar zxvf selinux_config/config.tgz
# Build targeted policy
%{__rm} -fR %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/selinux
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
touch %{buildroot}%{_sysconfdir}/selinux/config
touch %{buildroot}%{_sysconfdir}/sysconfig/selinux
mkdir -p %{buildroot}%{_usr}/lib/tmpfiles.d/
cp %{SOURCE27} %{buildroot}%{_usr}/lib/tmpfiles.d/

# Always create policy module package directories
mkdir -p %{buildroot}%{_datadir}/selinux/{targeted,mls,minimum,modules}/

# Install devel
make clean
%if %{BUILD_TARGETED}
# Build targeted policy
# Commented out because only targeted ref policy currently builds
mkdir -p %{buildroot}%{_datadir}/selinux/targeted
cp %{SOURCE28} %{buildroot}/%{_datadir}/selinux/targeted
%makeCmds targeted mcs n allow
%makeModulesConf targeted base contrib
%installCmds targeted mcs n allow
mv %{buildroot}/%{_sysconfdir}/selinux/targeted/modules/active/modules/sandbox.pp %{buildroot}/usr/share/selinux/packages
%modulesList targeted 
%endif

%if %{BUILD_MINIMUM}
# Build minimum policy
# Commented out because only minimum ref policy currently builds
mkdir -p %{buildroot}%{_datadir}/selinux/minimum
cp %{SOURCE28} %{buildroot}/%{_datadir}/selinux/minimum
%makeCmds minimum mcs n allow
%makeModulesConf targeted base contrib
%installCmds minimum mcs n allow
rm -f %{buildroot}/%{_sysconfdir}/selinux/minimum/modules/active/modules/sandbox.pp
%modulesList minimum
%endif

%if %{BUILD_MLS}
# Build mls policy
%makeCmds mls mls n deny
%makeModulesConf mls base contrib
%installCmds mls mls n deny
%modulesList mls
%endif

mkdir -p %{buildroot}%{_mandir}
cp -R  man/* %{buildroot}%{_mandir}
make UNK_PERMS=allow NAME=targeted TYPE=mcs DISTRO=%{distro} UBAC=n DIRECT_INITRC=n MONOLITHIC=%{monolithic} DESTDIR=%{buildroot} PKGNAME=%{name} MLS_CATS=1024 MCS_CATS=1024 install-docs
make UNK_PERMS=allow NAME=targeted TYPE=mcs DISTRO=%{distro} UBAC=n DIRECT_INITRC=n MONOLITHIC=%{monolithic} DESTDIR=%{buildroot} PKGNAME=%{name} MLS_CATS=1024 MCS_CATS=1024 install-headers
mkdir %{buildroot}%{_datadir}/selinux/devel/
mv %{buildroot}%{_datadir}/selinux/targeted/include %{buildroot}%{_datadir}/selinux/devel/include
install -m 644 selinux_config/Makefile.devel %{buildroot}%{_datadir}/selinux/devel/Makefile
install -m 644 doc/example.* %{buildroot}%{_datadir}/selinux/devel/
install -m 644 doc/policy.* %{buildroot}%{_datadir}/selinux/devel/
echo  "xdg-open file:///usr/share/doc/selinux-policy/html/index.html"> %{buildroot}%{_datadir}/selinux/devel/policyhelp
chmod +x %{buildroot}%{_datadir}/selinux/devel/policyhelp
/usr/bin/sepolicy manpage -a -p %{buildroot}/usr/share/man/man8/ -w -r %{buildroot}
mkdir %{buildroot}%{_datadir}/selinux/devel/html
htmldir=`compgen -d %{buildroot}%{_datadir}/man/man8/`
mv ${htmldir}/* %{buildroot}%{_datadir}/selinux/devel/html
mv %{buildroot}%{_datadir}/man/man8/index.html %{buildroot}%{_datadir}/selinux/devel/html
mv %{buildroot}%{_datadir}/man/man8/style.css %{buildroot}%{_datadir}/selinux/devel/html
rm -rf ${htmldir}

mkdir -p %{buildroot}%{_rpmconfigdir}/macros.d
echo '%%_selinux_policy_version %{version}-%{release}' > %{buildroot}%{_rpmconfigdir}/macros.d/macros.selinux-policy

rm -rf selinux_config

%clean
%{__rm} -fR %{buildroot}

%post
if [ ! -s /etc/selinux/config ]; then
#
#     New install so we will default to targeted policy
#
echo "
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=enforcing
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected. 
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted 

" > /etc/selinux/config

     ln -sf ../selinux/config /etc/sysconfig/selinux 
     restorecon /etc/selinux/config 2> /dev/null || :
else
     . /etc/selinux/config
     # if first time update booleans.local needs to be copied to sandbox
     [ -f /etc/selinux/${SELINUXTYPE}/booleans.local ] && mv /etc/selinux/${SELINUXTYPE}/booleans.local /etc/selinux/targeted/modules/active/
     [ -f /etc/selinux/${SELINUXTYPE}/seusers ] && cp -f /etc/selinux/${SELINUXTYPE}/seusers /etc/selinux/${SELINUXTYPE}/modules/active/seusers
fi
exit 0

%postun
if [ $1 = 0 ]; then
     setenforce 0 2> /dev/null
     if [ ! -s /etc/selinux/config ]; then
          echo "SELINUX=disabled" > /etc/selinux/config
     else
          sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
     fi
fi
exit 0

%if %{BUILD_TARGETED}
%package targeted
Summary: SELinux targeted base policy
Provides: selinux-policy-base = %{version}-%{release}
Group: System Environment/Base
Obsoletes: selinux-policy-targeted-sources < 2
Requires(pre): policycoreutils >= %{POLICYCOREUTILSVER}
Requires(pre): coreutils
Requires(pre): selinux-policy = %{version}-%{release}
Requires: selinux-policy = %{version}-%{release}
Conflicts:  audispd-plugins <= 1.7.7-1
Obsoletes: mod_fcgid-selinux <= %{version}-%{release}
Obsoletes: cachefilesd-selinux <= 0.10-1
Conflicts:  seedit
Conflicts:  389-ds-base < 1.2.7, 389-admin < 1.1.12

%description targeted
SELinux Reference policy targeted base module.

%pre targeted
%preInstall targeted

%post targeted
%postInstall $1 targeted
exit 0

%triggerin -- pcre
selinuxenabled && semodule -nB
exit 0

%triggerpostun -- selinux-policy-targeted < 3.12.1-74
rm -f /etc/selinux/*/modules/active/modules/sandbox.pp.disabled 2>/dev/null
exit 0

%triggerpostun targeted -- selinux-policy-targeted < 3.12.1-75
restorecon -R -p /home
exit 0

%files targeted
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/selinux/targeted/contexts/users/unconfined_u
%config(noreplace) %{_sysconfdir}/selinux/targeted/contexts/users/sysadm_u 
%{_datadir}/selinux/targeted/base.lst
%{_datadir}/selinux/targeted/modules-base.lst
%{_datadir}/selinux/targeted/modules-contrib.lst
%fileList targeted
%endif

%if %{BUILD_MINIMUM}
%package minimum
Summary: SELinux minimum base policy
Provides: selinux-policy-base = %{version}-%{release}
Group: System Environment/Base
Requires(post): policycoreutils-python >= %{POLICYCOREUTILSVER}
Requires(pre): coreutils
Requires(pre): selinux-policy = %{version}-%{release}
Requires: selinux-policy = %{version}-%{release}
Conflicts:  seedit

%description minimum
SELinux Reference policy minimum base module.

%pre minimum
%preInstall minimum
if [ $1 -ne 1 ]; then
   /usr/sbin/semodule -s minimum -l 2>/dev/null | awk '{ if ($3 != "Disabled") print $1; }' > /usr/share/selinux/minimum/instmodules.lst
fi

%post minimum
contribpackages=`cat /usr/share/selinux/minimum/modules-contrib.lst`
basepackages=`cat /usr/share/selinux/minimum/modules-base.lst`
if [ $1 -eq 1 ]; then
for p in $contribpackages; do
	touch /etc/selinux/minimum/modules/active/modules/$p.disabled
done
for p in $basepackages apache.pp dbus.pp inetd.pp kerberos.pp mta.pp nis.pp; do
	rm -f /etc/selinux/minimum/modules/active/modules/$p.disabled
done
/usr/sbin/semanage import -S minimum -f - << __eof
login -m  -s unconfined_u -r s0-s0:c0.c1023 __default__
login -m  -s unconfined_u -r s0-s0:c0.c1023 root
__eof
/sbin/restorecon -R /root /var/log /var/run 2> /dev/null
/usr/sbin/semodule -B -s minimum
else
instpackages=`cat /usr/share/selinux/minimum/instmodules.lst`
for p in $contribpackages; do
    touch /etc/selinux/minimum/modules/active/modules/$p.disabled
done
for p in $instpackages apache dbus inetd kerberos mta nis; do
    rm -f /etc/selinux/minimum/modules/active/modules/$p.pp.disabled
done
/usr/sbin/semodule -B -s minimum
%relabel minimum
fi
exit 0

%files minimum
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/selinux/minimum/contexts/users/unconfined_u
%config(noreplace) %{_sysconfdir}/selinux/minimum/contexts/users/sysadm_u 
%{_datadir}/selinux/minimum/base.lst
%{_datadir}/selinux/minimum/modules-base.lst
%{_datadir}/selinux/minimum/modules-contrib.lst
%fileList targeted
%fileList minimum
%endif

%if %{BUILD_MLS}
%package mls 
Summary: SELinux mls base policy
Group: System Environment/Base
Provides: selinux-policy-base = %{version}-%{release}
Obsoletes: selinux-policy-mls-sources < 2
Requires: policycoreutils-newrole >= %{POLICYCOREUTILSVER} setransd
Requires(pre): policycoreutils >= %{POLICYCOREUTILSVER}
Requires(pre): coreutils
Requires(pre): selinux-policy = %{version}-%{release}
Requires: selinux-policy = %{version}-%{release}
Conflicts:  seedit

%description mls 
SELinux Reference policy mls base module.

%pre mls 
%preInstall mls

%post mls 
%postInstall $1 mls

%files mls
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/selinux/mls/contexts/users/unconfined_u
%{_datadir}/selinux/mls/base.lst
%{_datadir}/selinux/mls/modules-base.lst
%{_datadir}/selinux/mls/modules-contrib.lst
%fileList mls
%endif

%changelog
* Mon May 26 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13.1-1m)
- update 3.13.1

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-11m)
- import fedora patch(3.10.0-81)

* Fri Feb 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-10m)
- import fedora patch(3.10.0-76)

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-9m)
- import fedora patch(3.10.0-75)

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-8m)
- import fedora patch(3.10.0-73)

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-7m)
- import fedora patch(3.10.0-71)

* Sat Dec 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-6m)
- import fedora patch(3.10.0-67)

* Tue Nov 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-5m)
- import fedora patch(3.10.0-56)

* Mon Nov 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-4m)
- import fedora patch(3.10.0-55)

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-3m)
- import fedora patch(3.10.0-51)

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-2m)
- import fedora patch(3.10.0-40)

* Sun Oct 16 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.10.0-1m)
- sync Fedora

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9.16-4m)
- update patch

* Wed Jul 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.16-3m)
- add BuildRequires

* Mon Jul  4 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.9.16-2m)
- modify Requires

* Sun Jul  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9.16-1m)
- update 3.9.16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.19-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.19-9m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.19-8m)
- merge fedora selinux-policy(3.7.19-69)

* Thu Oct  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.7.19-7m)
- fix contexts of %%{_libdir}/kde4/libexec/*
- BTS ID: 306
- http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=306
- fix up fc for updates of Momonga Linux 7 without subversion and sudo

* Sun Sep 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.19-6m)
- allow_postfix_local_write_mail_spool=true

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.19-5m)
- full rebuild for mo7 release

* Mon Aug 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.19-4m)
- merge fedora selinux-policy(3.7.19-49)

* Sun Jul  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.19-3m)
- merge fedora selinux-policy(3.7.19-33)

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.19-2m)
- merge fedora selinux-policy(3.7.19-23)

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.19-1m)
- update 3.7.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.12-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.12-5m)
- fix %%post script to avoid "selinuxenabled: command not found" warning

* Thu Jul 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.12-4m)
- update policy data (fedora selinux-policy-3.6.12-62)

* Tue Jul  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.12-3m)
- add policy-Mo6.patch
-- support nilfs2,reiser4

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.12-2m)
- update policy data (fedora selinux-policy-3.6.12-50)

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.12-1m)
- sync sith Fedora 11 (3.6.12-46)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.1-12m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.1-11m)
- update Patch10 for fuzz=0

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.1-10m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Fri Oct 10 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-9m)
- sync 3.3.1-101.f9
- allow unlabbled partition mount 

* Thu Oct  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-8m)
- add postgresql patch(booleans-targeted.conf)

* Tue Sep  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-7m)
- sync 3.3.1-89.fc9

* Tue Aug 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.1-6m)
- sync 3.3.1-84.fc9

* Fri Jul 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.1-5m)
- merge from 3.3.1-77.fc9

* Sun Jun  1 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.1-4m)
- merge from 3.3.1-55.fc9

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.1-3m)
- merge from 3.3.1-51.fc9

* Wed Apr 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.1-2m)
- modify %%files to avoid conflicting
- add %%defattr(-,root,root)

* Sun Apr 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.1-1m)
- version up 3.3.1
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.4-10m)
- rebuild against gcc43

* Fri Feb  1 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.4-9m)
- update modules-targeted.conf policy-20070501.patch from upsteam
- modify serefpolicy-2.6.4-fc-momonga.patch serefpolicy-2.6.4-te-momonga.patch
  for libFLAC.so.*

* Sat Aug 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.4-8m)
- add serefpolicy-2.6.4-fc-momonga.patch
- add serefpolicy-2.6.4-te-momonga.patch

* Sat Aug 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.4-7m)
- add serefpolicy-2.6.4-execstack.patch

* Sat Aug 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.4-6m)
- modify post script

* Thu Aug 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.4-5m)
- sync Fedora(selinux-policy-2.6.4-37)

* Mon Aug  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.4-4m)
- update patch1 (for Momonga 4)

* Sun Aug  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.4-3m)
- sync Fedora(selinux-policy-2.6.4-31)

* Tue Jul 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.4-2m)
- sync Fedora(selinux-policy-2.6.4-26)
- enable momonga patch

* Mon Jun 11 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.4-1m)
- sync Fedora

* Fri May 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.1-1m)
- update 2.6.1

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.6-1m)
- update 2.5.6

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.3-1m)
- update 2.5.3
- update serefpolicy-2.5.3-momonga.patch

* Tue Jan 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.6-2m)
- update serefpolicy-2.4.6-momonga.patch

* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.6-1m)
- update 2.4.6 

* Mon Aug 21 2006 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (2.2.43-6m)
- update serefpolicy-2.2.43-momonga.patch for make_reiser4 

* Sun Aug 13 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.43-5m)
- update policy-20060505.patch

* Wed Aug  2 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.43-4m)
- update serefpolicy-2.2.43-momonga.patch for libtermcap

* Fri Jul 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.43-3m)
- update serefpolicy-2.2.43-momonga.patch for x264

* Fri Jul 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.43-2m)
- update serefpolicy-2.2.43-momonga.patch for cdparanoia, pwlib and avifile

* Sun Jun 18 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.43-1m)
- update 2.2.43
- apply serefpolicy-2.2.43-momonga.patch

* Sat May 27 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.40-1m)
- import from fc5

* Wed May 17 2006 Dan Walsh <dwalsh@redhat.com> 2.2.40-1.fc5
- Bump for fc5

* Tue May 16 2006 Dan Walsh <dwalsh@redhat.com> 2.2.40-1
- Update from upstream

* Mon May 15 2006 Dan Walsh <dwalsh@redhat.com> 2.2.39-2
- Fixes for amavis

* Mon May 15 2006 Dan Walsh <dwalsh@redhat.com> 2.2.39-1
- Update from upstream

* Fri May 12 2006 Dan Walsh <dwalsh@redhat.com> 2.2.38-6
- Allow auditctl to search all directories

* Thu May 11 2006 Dan Walsh <dwalsh@redhat.com> 2.2.38-5
- Add acquire service for mono.

* Thu May 11 2006 Dan Walsh <dwalsh@redhat.com> 2.2.38-4
- Turn off allow_execmem boolean
- Allow ftp dac_override when allowed to access users homedirs

* Wed May 10 2006 Dan Walsh <dwalsh@redhat.com> 2.2.38-3
- Clean up spec file
- Transition from unconfined_t to prelink_t

* Mon May 8 2006 Dan Walsh <dwalsh@redhat.com> 2.2.38-2
- Allow execution of cvs command

* Mon May 8 2006 Dan Walsh <dwalsh@redhat.com> 2.2.38-1.fc5
- Bump for fc5

* Fri May 5 2006 Dan Walsh <dwalsh@redhat.com> 2.2.38-1
- Update to upstream

* Wed May 3 2006 Dan Walsh <dwalsh@redhat.com> 2.2.37-1
- Update to upstream

* Mon May 1 2006 Dan Walsh <dwalsh@redhat.com> 2.2.36-2.fc5
- Bump for fc5

* Mon May 1 2006 Dan Walsh <dwalsh@redhat.com> 2.2.36-2
- Fix libjvm spec

* Tue Apr 25 2006 Dan Walsh <dwalsh@redhat.com> 2.2.36-1
- Update to upstream

* Tue Apr 25 2006 James Antill <jantill@redhat.com> 2.2.35-2
- Add xm policy
- Fix policygentool

* Mon Apr 24 2006 Dan Walsh <dwalsh@redhat.com> 2.2.35-1
- Update to upstream
- Fix postun to only disable selinux on full removal of the packages

* Fri Apr 21 2006 Dan Walsh <dwalsh@redhat.com> 2.2.34-3.fc5
- Bump for fc5

* Fri Apr 21 2006 Dan Walsh <dwalsh@redhat.com> 2.2.34-3
- Allow mono to chat with unconfined

* Thu Apr 20 2006 Dan Walsh <dwalsh@redhat.com> 2.2.34-2
- Allow procmail to sendmail
- Allow nfs to share dosfs

* Thu Apr 20 2006 Dan Walsh <dwalsh@redhat.com> 2.2.34-1
- Update to latest from upstream
- Allow selinux-policy to be removed and kernel not to crash

* Tue Apr 18 2006 Dan Walsh <dwalsh@redhat.com> 2.2.33-1
- Update to latest from upstream
- Add James Antill patch for xen
- Many fixes for pegasus

* Sat Apr 15 2006 Dan Walsh <dwalsh@redhat.com> 2.2.32-2
- Add unconfined_mount_t
- Allow privoxy to connect to httpd_cache
- fix cups labeleing on /var/cache/cups

* Sat Apr 15 2006 Dan Walsh <dwalsh@redhat.com> 2.2.32-1.fc5
- Bump for fc5

* Fri Apr 14 2006 Dan Walsh <dwalsh@redhat.com> 2.2.32-1
- Update to latest from upstream

* Fri Apr 14 2006 Dan Walsh <dwalsh@redhat.com> 2.2.31-1
- Update to latest from upstream
- Allow mono and unconfined to talk to initrc_t dbus objects

* Tue Apr 11 2006 Dan Walsh <dwalsh@redhat.com> 2.2.30-2
- Change libraries.fc to stop shlib_t form overriding texrel_shlib_t

* Tue Apr 11 2006 Dan Walsh <dwalsh@redhat.com> 2.2.30-1
- Fix samba creating dirs in homedir
- Fix NFS so its booleans would work

* Mon Apr 10 2006 Dan Walsh <dwalsh@redhat.com> 2.2.29-6
- Allow secadm_t ability to relabel all files
- Allow ftp to search xferlog_t directories
- Allow mysql to communicate with ldap
- Allow rsync to bind to rsync_port_t

* Mon Apr 10 2006 Russell Coker <rcoker@redhat.com> 2.2.29-5
- Fixed mailman with Postfix #183928
- Allowed semanage to create file_context files.
- Allowed amanda_t to access inetd_t TCP sockets and allowed amanda_recover_t
  to bind to reserved ports.  #149030
- Don't allow devpts_t to be associated with tmp_t.
- Allow hald_t to stat all mountpoints.
- Added boolean samba_share_nfs to allow smbd_t full access to NFS mounts.
  #169947
- Make mount run in mount_t domain from unconfined_t to prevent mislabeling of
  /etc/mtab.
- Changed the file_contexts to not have a regex before the first ^/[a-z]/
  whenever possible, makes restorecon slightly faster.
- Correct the label of /etc/named.caching-nameserver.conf
- Now label /usr/src/kernels/.+/lib(/.*)? as usr_t instead of
  /usr/src(/.*)?/lib(/.*)? - I don't think we need anything else under /usr/src
  hit by this.
- Granted xen access to /boot, allowed mounting on xend_var_lib_t, and allowed
  xenstored_t rw access to the xen device node.

* Tue Apr 4 2006 Dan Walsh <dwalsh@redhat.com> 2.2.29-4
- More textrel_shlib_t file path fixes
- Add ada support

* Tue Apr 4 2006 Dan Walsh <dwalsh@redhat.com> 2.2.29-3.fc5
- Bump for fc5

* Mon Apr 3 2006 Dan Walsh <dwalsh@redhat.com> 2.2.29-3
- Get auditctl working in MLS policy

* Mon Apr 3 2006 Dan Walsh <dwalsh@redhat.com> 2.2.29-2
- Add mono dbus support
- Lots of file_context fixes for textrel_shlib_t in FC5
- Turn off execmem auditallow since they are filling log files

* Fri Mar 30 2006 Dan Walsh <dwalsh@redhat.com> 2.2.29-1
- Update to upstream

* Thu Mar 30 2006 Dan Walsh <dwalsh@redhat.com> 2.2.28-3
- Allow automount and dbus to read cert files

* Thu Mar 30 2006 Dan Walsh <dwalsh@redhat.com> 2.2.28-2
- Fix ftp policy
- Fix secadm running of auditctl

* Mon Mar 27 2006 Dan Walsh <dwalsh@redhat.com> 2.2.28-1
- Update to upstream

* Wed Mar 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.27-1
- Update to upstream

* Wed Mar 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.25-3.fc5
- Bump for fc5
- Fixes for hplip and klogd

* Wed Mar 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.25-3
- Fix policyhelp

* Wed Mar 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.25-2.fc5
- Rebuild for FC5

* Wed Mar 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.25-2
- Fix pam_console handling of usb_device
- dontaudit logwatch reading /mnt dir

* Fri Mar 17 2006 Dan Walsh <dwalsh@redhat.com> 2.2.24-1
- Update to upstream

* Wed Mar 15 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-19
- Get transition rules to create policy.20 at SystemHigh

* Tue Mar 14 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-18
- Allow secadmin to shutdown system
- Allow sendmail to exec newalias

* Tue Mar 14 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-17
- MLS Fixes
	dmidecode needs mls_file_read_up
- add ypxfr_t
- run init needs access to nscd
- udev needs setuid
- another xen log file
- Dontaudit mount getattr proc_kcore_t

* Tue Mar 14 2006 Karsten Hopp <karsten@redhat.de> 2.2.23-16
- fix buildroot usage (#185391)

* Thu Mar 9 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-15
- Get rid of mount/fsdisk scan of /dev messages
- Additional fixes for suspend/resume

* Thu Mar 9 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-14
- Fake make to rebuild enableaudit.pp

* Thu Mar 9 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-13
- Get xen networking running.

* Thu Mar 9 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-12
- Fixes for Xen
- enableaudit should not be the same as base.pp
- Allow ps to work for all process

* Thu Mar  9 2006 Jeremy Katz <katzj@redhat.com> - 2.2.23-11
- more xen policy fixups

* Wed Mar  8 2006 Jeremy Katz <katzj@redhat.com> - 2.2.23-10
- more xen fixage (#184393)

* Wed Mar 8 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-9
- Fix blkid specification
- Allow postfix to execute mailman_que

* Wed Mar 8 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-8
- Blkid changes
- Allow udev access to usb_device_t
- Fix post script to create targeted policy config file

* Wed Mar 8 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-7
- Allow lvm tools to create drevice dir

* Tue Mar 7 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-5
- Add Xen support

* Mon Mar 6 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-4
- Fixes for cups
- Make cryptosetup work with hal

* Sun Mar 5 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-3
- Load Policy needs translock

* Sat Mar 4 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-2
- Fix cups html interface

* Sat Mar 4 2006 Dan Walsh <dwalsh@redhat.com> 2.2.23-1
- Add hal changes suggested by Jeremy
- add policyhelp to point at policy html pages

* Mon Feb 27 2006 Dan Walsh <dwalsh@redhat.com> 2.2.22-2
- Additional fixes for nvidia and cups

* Mon Feb 27 2006 Dan Walsh <dwalsh@redhat.com> 2.2.22-1
- Update to upstream
- Merged my latest fixes
- Fix cups policy to handle unix domain sockets

* Sat Feb 25 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-9
- NSCD socket is in nscd_var_run_t needs to be able to search dir

* Fri Feb 24 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-8
- Fixes Apache interface file

* Fri Feb 24 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-7
- Fixes for new version of cups

* Fri Feb 24 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-6
- Turn off polyinstatiate util after FC5

* Fri Feb 24 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-5
- Fix problem with privoxy talking to Tor

* Thu Feb 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-4
- Turn on polyinstatiation

* Thu Feb 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-3
- Don't transition from unconfined_t to fsadm_t

* Thu Feb 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-2
- Fix policy update model.

* Thu Feb 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.21-1
- Update to upstream

* Wed Feb 22 2006 Dan Walsh <dwalsh@redhat.com> 2.2.20-1
- Fix load_policy to work on MLS
- Fix cron_rw_system_pipes for postfix_postdrop_t
- Allow audotmount to run showmount

* Tue Feb 21 2006 Dan Walsh <dwalsh@redhat.com> 2.2.19-2
- Fix swapon
- allow httpd_sys_script_t to be entered via a shell
- Allow httpd_sys_script_t to read eventpolfs

* Tue Feb 21 2006 Dan Walsh <dwalsh@redhat.com> 2.2.19-1
- Update from upstream

* Tue Feb 21 2006 Dan Walsh <dwalsh@redhat.com> 2.2.18-2
- allow cron to read apache files

* Tue Feb 21 2006 Dan Walsh <dwalsh@redhat.com> 2.2.18-1
- Fix vpnc policy to work from NetworkManager

* Mon Feb 20 2006 Dan Walsh <dwalsh@redhat.com> 2.2.17-2
- Update to upstream
- Fix semoudle polcy

* Thu Feb 16 2006 Dan Walsh <dwalsh@redhat.com> 2.2.16-1
- Update to upstream 
- fix sysconfig/selinux link

* Wed Feb 15 2006 Dan Walsh <dwalsh@redhat.com> 2.2.15-4
- Add router port for zebra
- Add imaze port for spamd
- Fixes for amanda and java

* Tue Feb 14 2006 Dan Walsh <dwalsh@redhat.com> 2.2.15-3
- Fix bluetooth handling of usb devices
- Fix spamd reading of ~/
- fix nvidia spec

* Tue Feb 14 2006 Dan Walsh <dwalsh@redhat.com> 2.2.15-1
- Update to upsteam

* Mon Feb 13 2006 Dan Walsh <dwalsh@redhat.com> 2.2.14-2
- Add users_extra files

* Fri Feb 10 2006 Dan Walsh <dwalsh@redhat.com> 2.2.14-1
- Update to upstream

* Fri Feb 10 2006 Dan Walsh <dwalsh@redhat.com> 2.2.13-1
- Add semodule policy

* Tue Feb 7 2006 Dan Walsh <dwalsh@redhat.com> 2.2.12-1
- Update from upstream


* Mon Feb 6 2006 Dan Walsh <dwalsh@redhat.com> 2.2.11-2
- Fix for spamd to use razor port

* Fri Feb 3 2006 Dan Walsh <dwalsh@redhat.com> 2.2.11-1
- Fixes for mcs
- Turn on mount and fsadm for unconfined_t

* Wed Feb 1 2006 Dan Walsh <dwalsh@redhat.com> 2.2.10-1
- Fixes for the -devel package

* Wed Feb 1 2006 Dan Walsh <dwalsh@redhat.com> 2.2.9-2
- Fix for spamd to use ldap

* Fri Jan 27 2006 Dan Walsh <dwalsh@redhat.com> 2.2.9-1
- Update to upstream

* Fri Jan 27 2006 Dan Walsh <dwalsh@redhat.com> 2.2.8-2
- Update to upstream
- Fix rhgb, and other Xorg startups

* Thu Jan 26 2006 Dan Walsh <dwalsh@redhat.com> 2.2.7-1
- Update to upstream

* Thu Jan 26 2006 Dan Walsh <dwalsh@redhat.com> 2.2.6-3
- Separate out role of secadm for mls

* Thu Jan 26 2006 Dan Walsh <dwalsh@redhat.com> 2.2.6-2
- Add inotifyfs handling

* Thu Jan 26 2006 Dan Walsh <dwalsh@redhat.com> 2.2.6-1
- Update to upstream
- Put back in changes for pup/zen

* Tue Jan 24 2006 Dan Walsh <dwalsh@redhat.com> 2.2.5-1
- Many changes for MLS 
- Turn on strict policy

* Mon Jan 23 2006 Dan Walsh <dwalsh@redhat.com> 2.2.4-1
- Update to upstream

* Wed Jan 18 2006 Dan Walsh <dwalsh@redhat.com> 2.2.3-1
- Update to upstream
- Fixes for booting and logging in on MLS machine

* Wed Jan 18 2006 Dan Walsh <dwalsh@redhat.com> 2.2.2-1
- Update to upstream
- Turn off execheap execstack for unconfined users
- Add mono/wine policy to allow execheap and execstack for them
- Add execheap for Xdm policy

* Wed Jan 18 2006 Dan Walsh <dwalsh@redhat.com> 2.2.1-1
- Update to upstream
- Fixes to fetchmail,

* Tue Jan 17 2006 Dan Walsh <dwalsh@redhat.com> 2.1.13-1
- Update to upstream

* Tue Jan 17 2006 Dan Walsh <dwalsh@redhat.com> 2.1.12-3
- Fix for procmail/spamassasin
- Update to upstream
- Add rules to allow rpcd to work with unlabeled_networks.

* Sat Jan 14 2006 Dan Walsh <dwalsh@redhat.com> 2.1.11-1
- Update to upstream
- Fix ftp Man page

* Fri Jan 13 2006 Dan Walsh <dwalsh@redhat.com> 2.1.10-1
- Update to upstream

* Wed Jan 11 2006 Jeremy Katz <katzj@redhat.com> - 2.1.9-2
- fix pup transitions (#177262)
- fix xen disks (#177599)

* Tue Jan 10 2006 Dan Walsh <dwalsh@redhat.com> 2.1.9-1
- Update to upstream

* Tue Jan 10 2006 Dan Walsh <dwalsh@redhat.com> 2.1.8-3
- More Fixes for hal and readahead

* Mon Jan 9 2006 Dan Walsh <dwalsh@redhat.com> 2.1.8-2
- Fixes for hal and readahead

* Mon Jan 9 2006 Dan Walsh <dwalsh@redhat.com> 2.1.8-1
- Update to upstream
- Apply 
* Fri Jan 7 2006 Dan Walsh <dwalsh@redhat.com> 2.1.7-4
- Add wine and fix hal problems

* Thu Jan 6 2006 Dan Walsh <dwalsh@redhat.com> 2.1.7-3
- Handle new location of hal scripts

* Thu Jan 5 2006 Dan Walsh <dwalsh@redhat.com> 2.1.7-2
- Allow su to read /etc/mtab

* Wed Jan 4 2006 Dan Walsh <dwalsh@redhat.com> 2.1.7-1
- Update to upstream

* Tue Jan 3 2006 Dan Walsh <dwalsh@redhat.com> 2.1.6-24
- Fix  "libsemanage.parse_module_headers: Data did not represent a module." problem

* Tue Jan 3 2006 Dan Walsh <dwalsh@redhat.com> 2.1.6-23
- Allow load_policy to read /etc/mtab

* Mon Jan 2 2006 Dan Walsh <dwalsh@redhat.com> 2.1.6-22
- Fix dovecot to allow dovecot_auth to look at /tmp

* Mon Jan 2 2006 Dan Walsh <dwalsh@redhat.com> 2.1.6-21
- Allow restorecon to read unlabeled_t directories in order to fix labeling.

* Fri Dec 30 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-20
- Add Logwatch policy

* Wed Dec 28 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-18
- Fix /dev/ub[a-z] file context

* Tue Dec 27 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-17
- Fix library specification
- Give kudzu execmem privs

* Thu Dec 22 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-16
- Fix hostname in targeted policy

* Wed Dec 21 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-15
- Fix passwd command on mls

* Wed Dec 21 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-14
- Lots of fixes to make mls policy work

* Tue Dec 20 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-13
- Add dri libs to textrel_shlib_t
- Add system_r role for java
- Add unconfined_exec_t for vncserver
- Allow slapd to use kerberos

* Mon Dec 19 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-11
- Add man pages

* Fri Dec 16 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-10
- Add enableaudit.pp

* Fri Dec 16 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-9
- Fix mls policy

* Fri Dec 16 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-8
- Update mls file from old version

* Thu Dec 15 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-5
- Add sids back in
- Rebuild with update checkpolicy

* Thu Dec 15 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-4
- Fixes to allow automount to use portmap
- Fixes to start kernel in s0-s15:c0.c255

* Wed Dec 14 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-3
- Add java unconfined/execmem policy 

* Wed Dec 14 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-2
- Add file context for /var/cvs
- Dontaudit webalizer search of homedir

* Tue Dec 13 2005 Dan Walsh <dwalsh@redhat.com> 2.1.6-1
- Update from upstream

* Tue Dec 13 2005 Dan Walsh <dwalsh@redhat.com> 2.1.4-2
- Clean up spec
- range_transition crond to SystemHigh

* Mon Dec 12 2005 Dan Walsh <dwalsh@redhat.com> 2.1.4-1
- Fixes for hal
- Update to upstream

* Mon Dec 12 2005 Dan Walsh <dwalsh@redhat.com> 2.1.3-1
- Turn back on execmem since we need it for java, firefox, ooffice
- Allow gpm to stream socket to itself

* Mon Dec 12 2005 Jeremy Katz <katzj@redhat.com> - 2.1.2-3
- fix requirements to be on the actual packages so that policy can get
  created properly at install time

* Sun Dec  10 2005 Dan Walsh <dwalsh@redhat.com> 2.1.2-2
- Allow unconfined_t to execmod texrel_shlib_t

* Sat Dec  9 2005 Dan Walsh <dwalsh@redhat.com> 2.1.2-1
- Update to upstream 
- Turn off allow_execmem and allow_execmod booleans
- Add tcpd and automount policies

* Fri Dec  8 2005 Dan Walsh <dwalsh@redhat.com> 2.1.1-3
- Add two new httpd booleans, turned off by default
	* httpd_can_network_relay
	* httpd_can_network_connect_db

* Fri Dec  8 2005 Dan Walsh <dwalsh@redhat.com> 2.1.1-2
- Add ghost for policy.20

* Thu Dec  8 2005 Dan Walsh <dwalsh@redhat.com> 2.1.1-1
- Update to upstream
- Turn off boolean allow_execstack

* Thu Dec  8 2005 Dan Walsh <dwalsh@redhat.com> 2.1.0-3
- Change setrans-mls to use new libsetrans
- Add default_context rule for xdm

* Thu Dec  8 2005 Dan Walsh <dwalsh@redhat.com> 2.1.0-2.
- Change Requires to PreReg for requiring of policycoreutils on install

* Wed Dec  7 2005 Dan Walsh <dwalsh@redhat.com> 2.1.0-1.
- New upstream release

* Wed Dec  7 2005 Dan Walsh <dwalsh@redhat.com> 2.0.11-2.
Add xdm policy

* Tue Dec  6 2005 Dan Walsh <dwalsh@redhat.com> 2.0.11-1.
Update from upstream

* Fri Dec  2 2005 Dan Walsh <dwalsh@redhat.com> 2.0.9-1.
Update from upstream

* Fri Dec  2 2005 Dan Walsh <dwalsh@redhat.com> 2.0.8-1.
Update from upstream

* Fri Dec  2 2005 Dan Walsh <dwalsh@redhat.com> 2.0.7-3
- Also trigger to rebuild policy for versions up to 2.0.7.

* Tue Nov 29 2005 Dan Walsh <dwalsh@redhat.com> 2.0.7-2
- No longer installing policy.20 file, anaconda handles the building of the app.

* Tue Nov 29 2005 Dan Walsh <dwalsh@redhat.com> 2.0.6-2
- Fixes for dovecot and saslauthd

* Wed Nov 23 2005 Dan Walsh <dwalsh@redhat.com> 2.0.5-4
- Cleanup pegasus and named 
- Fix spec file
- Fix up passwd changing applications

* Tue Nov 21 2005 Dan Walsh <dwalsh@redhat.com> 2.0.5-1
-Update to latest from upstream

* Tue Nov 21 2005 Dan Walsh <dwalsh@redhat.com> 2.0.4-1
- Add rules for pegasus and avahi

* Mon Nov 21 2005 Dan Walsh <dwalsh@redhat.com> 2.0.2-2
- Start building MLS Policy

* Fri Nov 18 2005 Dan Walsh <dwalsh@redhat.com> 2.0.2-1
- Update to upstream

* Wed Nov 9 2005 Dan Walsh <dwalsh@redhat.com> 2.0.1-2
- Turn on bash

* Wed Nov 9 2005 Dan Walsh <dwalsh@redhat.com> 2.0.1-1
- Initial version
