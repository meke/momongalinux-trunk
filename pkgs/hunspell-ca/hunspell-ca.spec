%global momorel 4

Name: hunspell-ca
Summary: Catalan hunspell dictionaries
Version: 2.2.0
Release: %{momorel}m%{?dist}
Source: http://www.softcatala.org/diccionaris/actualitzacions/OOo/catalan.oxt
Group: Applications/Text
URL: http://www.softcatala.org/wiki/Projectes/Corrector_ortogr%C3%A0fic
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Catalan hunspell dictionaries.

%prep
%setup -q -c

%build
tr -d '\r' < dictionaries/catalan.aff > ca_ES.aff
touch -r dictionaries/catalan.aff ca_ES.aff
tr -d '\r' < dictionaries/catalan.dic > ca_ES.dic
touch -r dictionaries/catalan.dic ca_ES.dic

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p ca_ES.dic ca_ES.aff $RPM_BUILD_ROOT/%{_datadir}/myspell
pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
ca_ES_aliases="ca_AD ca_FR ca_IT"
for lang in $ca_ES_aliases; do
        ln -s ca_ES.aff $lang.aff
        ln -s ca_ES.dic $lang.dic
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc LICENSES-en.txt LLICENCIES-ca.txt       
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-1m)
- sync with Fedora 13 (2.2.0-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090311-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090311-1m)
- update to 20090311

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20060508-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20060508-1m)
- import from Fedora to Momonga

* Mon Jul 09 2007 Caolan McNamara <caolanm@redhat.com> - 0.20060508-1
- latest version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20021015-1
- initial version
