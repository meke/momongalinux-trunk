%global momorel 13
Summary: a package for creating and reading constant databases
Name: freecdb
Version: 0.62
Release: %{momorel}m%{?dist}
License: Public Domain
Group: Applications/Databases
Source0: http://ftp.debian.org/debian/pool/main/f/freecdb/%{name}_%{version}.tar.gz
URL: http://packages.debian.org/stable/utils/freecdb.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 5.8.2

%description
freecdb is a small, fast and reliable utility set and subroutine library for creating and
reading constant databases. The database structure is tuned for fast reading:

 - Successful lookups take normally just two disk accesses.
 - Unsuccessful lookups take only one disk access.
 - Small disk space and memory size requirements; a database
   uses 2048 bytes for the header and 24 bytes per record.
 - Maximum database size is 4GB; individual record size is not
   otherwise limited.
 - Portable file format.
 - Fast creation of new databases.
 - No locking, updates are atomical.

This package contains both the utilities and the development files.

%package devel
Summary: freecdb development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
freecdb libraries and headers.

%prep
%setup -q

%build
make CFLAGS="%{optflags}" LIBDIR=%{_libdir}

%install
test %{buildroot} != "/" && rm -rf %{buildroot}

make DESTDIR=%{buildroot} LIBDIR=%{buildroot}%{_libdir} install-dirs
make DESTDIR=%{buildroot} LIBDIR=%{buildroot}%{_libdir} install

rm debian/rules

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README debian/*
%{_bindir}/*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root)
%{_libdir}/lib*
%{_includedir}/*
%{_mandir}/man3/*

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-13m)
- add source (latest version is 0.75)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.62-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.62-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.62-6m)
- rebuild against gcc43

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62-5m)
- revised spec for debuginfo

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.62-4m)
- enable x86_64.

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.62-3m)
- remove Epoch from BuildPrereq

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.62-2m)
- rebuild against perl-5.8.2

* Sat Oct 18 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.62-1m)
- update to 0.62

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.61-11m)
- rebuild against perl-5.8.0

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (0.61-10k)
- revised spec.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.61-8k)
- /usr/bin/pod2man -> perl in BuildPreReq.

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (0.61-6k)
- No docs could be excutable :-p

* Tue Nov 12 2001 Tsutomu Yasuda <tom@kondara.org>
- (0.61-4k)
  included man pages

* Thu Nov 01 2001 Kenta MURATA <muraken2@nifty.com>
- (0.61-2k)
- first release.
