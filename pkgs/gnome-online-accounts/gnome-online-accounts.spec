%global		momorel 1
Name:		gnome-online-accounts
Version:	3.7.1
Release: 	%{momorel}m%{?dist}
Summary:	Provide online accounts information

Group:		System Environment/Libraries
License:	LGPLv2+
URL:		https://live.gnome.org/OnlineAccounts
Source0:	http://download.gnome.org/sources/gnome-online-accounts/3.7/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires:	glib2-devel >= 2.32
BuildRequires:	gtk3-devel >= 3.5.1
BuildRequires:	gobject-introspection-devel
BuildRequires:	gtk-doc
BuildRequires:	intltool
BuildRequires:	webkitgtk3-devel
BuildRequires:	json-glib-devel
BuildRequires:	libgnome-keyring-devel
BuildRequires:	libnotify-devel
BuildRequires:	libsoup-devel >= 2.38
BuildRequires:	rest-devel
BuildRequires:	libxml2-devel
BuildRequires:  libsecret-devel

%description
gnome-online-accounts provides interfaces so applications and
libraries in GNOME can access the user's online accounts.

%package devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig
Requires:	gobject-introspection-devel

%description devel
The gnome-online-accounts-devel package contains libraries and header
files for developing applications that use gnome-online-accounts.

%prep
%setup -q

%build
%configure \
  --disable-static \
  --enable-gtk-doc \
  --enable-exchange \
  --enable-facebook \
  --enable-windows-live
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT/%{_libdir}/*.la $RPM_BUILD_ROOT/%{_libdir}/control-center-1/panels/*.la

%find_lang %{name}

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%doc NEWS COPYING
%{_libdir}/girepository-1.0/Goa-1.0.typelib
%{_libdir}/libgoa-1.0.so.0
%{_libdir}/libgoa-1.0.so.0.0.0
%{_libdir}/libgoa-backend-1.0.so.0
%{_libdir}/libgoa-backend-1.0.so.0.0.0
%{_prefix}/libexec/goa-daemon
%{_datadir}/dbus-1/services/org.gnome.OnlineAccounts.service
%{_datadir}/gnome-online-accounts/goa*.css
%{_datadir}/icons/hicolor/*/apps/goa-*.png
%{_datadir}/man/man8/goa-daemon.8.*

%files devel
%{_includedir}/goa-1.0/
%{_libdir}/goa-1.0/include/goaconfig.h
%{_libdir}/libgoa-1.0.so
%{_libdir}/libgoa-backend-1.0.so
%{_datadir}/gir-1.0/Goa-1.0.gir
%{_libdir}/pkgconfig/goa-1.0.pc
%{_libdir}/pkgconfig/goa-backend-1.0.pc
%{_datadir}/gtk-doc/html/goa/

%changelog
* Mon Jun  2 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.7.1-1m)
- update to 3.7.1

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-2m)
- add BuildRequires

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-3m)
- rebuild for glib 2.33.2

* Sun Oct 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-2m)
- disable-twitter & disable-yahoo

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0.1-1m)
- update to 3.2.0.1

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- initial build
