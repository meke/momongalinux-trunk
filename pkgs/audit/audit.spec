%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: User space tools for 2.6 kernel auditing
Name: audit
Version: 2.3.7
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://people.redhat.com/sgrubb/audit/
Source0: http://people.redhat.com/sgrubb/audit/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext-devel intltool libtool swig python-devel
BuildRequires: tcp_wrappers-devel
BuildRequires: kernel-headers >= 2.6.30

BuildRequires: libtool swig
BuildRequires: python-devel >= %{pythonver}
BuildRequires: automake >= 1.9
BuildRequires: autoconf >= 2.59
BuildRequires: python-devel >= %{pythonver}
BuildRequires: gettext-devel intltool
BuildRequires: intltool >= 0.36.0
BuildRequires: perl-XML-Parser
BuildRequires: libprelude-devel >= 0.9.16
BuildRequires: libcap-ng-devel 
Requires: %{name}-libs = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}
Requires(post): systemd-units systemd-sysv chkconfig coreutils
Requires(preun): systemd-units
Requires(postun): systemd-units coreutils

%description
The audit package contains the user space utilities for
storing and searching the audit records generate by
the audit subsystem in the Linux 2.6 kernel.

%package libs
Summary: Dynamic library for libaudit
License: LGPLv2+
Group: Development/Libraries

%description libs
The audit-libs package contains the dynamic libraries needed for 
applications to use the audit framework.

%package libs-devel
Summary: Header files and static library for libaudit
License: LGPLv2+
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: kernel-headers >= 2.6.18

%description libs-devel
The audit-libs-devel package contains the static libraries and header 
files needed for developing applications that need to use the audit 
framework libraries.

%package libs-python
Summary: Python bindings for libaudit
License: LGPLv2+
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}

%description libs-python
The audit-libs-python package contains the bindings so that libaudit
and libauparse can be used by python.

%package -n audispd-plugins
Summary: Plugins for the audit event dispatcher
License: GPLv2+
Group: System Environment/Daemons
BuildRequires: openldap-devel
BuildRequires: checkpolicy selinux-policy-devel
Requires: %{name} = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}
Requires: openldap

%description -n audispd-plugins
The audispd-plugins package provides plugins for the real-time
interface to the audit system, audispd. These plugins can do things
like relay events to remote machines or analyze events for suspicious
behavior.

%prep
%setup -q

%build
%configure --with-python=yes --with-prelude --with-libwrap --enable-gssapi-krb5=no --with-libcap-ng=yes --enable-systemd
%make %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}

mkdir -p %{buildroot}/etc/{sysconfig,audispd/plugins.d,rc.d/init.d,init.d}
mkdir -p %{buildroot}/%{_mandir}/{man5,man8}
mkdir -p %{buildroot}/%{_libdir}/audit
mkdir -p %{buildroot}/%{_var}/log/audit
mkdir -p %{buildroot}/%{_var}/spool/audit
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/%{_libdir}
# Remove these items so they don't get picked up.
rm -f %{buildroot}/%{_libdir}/python?.?/site-packages/_audit.a
rm -f %{buildroot}/%{_libdir}/python?.?/site-packages/_audit.la
rm -f %{buildroot}/%{_libdir}/python?.?/site-packages/_auparse.a
rm -f %{buildroot}/%{_libdir}/python?.?/site-packages/_auparse.la
rm -f %{buildroot}/%{_libdir}/python?.?/site-packages/auparse.a
rm -f %{buildroot}/%{_libdir}/python?.?/site-packages/auparse.la

# On platforms with 32 & 64 bit libs, we need to coordinate the timestamp
touch -r ./audit.spec $RPM_BUILD_ROOT/etc/libaudit.conf
touch -r ./audit.spec $RPM_BUILD_ROOT/usr/share/man/man5/libaudit.conf.5.bz2
%ifarch x86_64
mkdir  %{buildroot}/lib/
%endif
rm -rf %{buildroot}%{_initscriptdir}

%ifnarch ppc ppc64
%check
make check
%endif

%clean
rm -rf --preserve-root %{buildroot}

%post
%systemd_post auditd.service

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable auditd.service > /dev/null 2>&1 || :
    /bin/systemctl stop auditd.service > /dev/null 2>&1 || :
fi

%postun libs -p /sbin/ldconfig

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart auditd.service >/dev/null 2>&1 || :
fi

%triggerun -- audit  < 2.1.2-2m
/usr/bin/systemd-sysv-convert --save auditd >/dev/null 2>&1 ||:
/bin/systemctl --no-reload enable auditd.service >/dev/null 2>&1 ||:
/sbin/chkconfig --del auditd >/dev/null 2>&1 || :
/bin/systemctl try-restart auditd.service >/dev/null 2>&1 || :

%files libs
%defattr(-,root,root)
%attr(755,root,root) %{_libdir}/libaudit.*
%attr(755,root,root) %{_libdir}/libauparse.*
%config(noreplace) %attr(640,root,root) /etc/libaudit.conf
%{_mandir}/man5/libaudit.conf.5.*

%files libs-devel
%defattr(-,root,root)
%doc contrib/skeleton.c contrib/plugin
%{_libdir}/libaudit.a
%{_libdir}/libauparse.a
%{_libdir}/libaudit.so
%{_libdir}/libauparse.so
%{_includedir}/libaudit.h
%{_includedir}/auparse.h
%{_includedir}/auparse-defs.h
%{_mandir}/man3/*

%files libs-python
%defattr(-,root,root)
%attr(755,root,root) %{python_sitearch}/_audit.so
%attr(755,root,root) %{python_sitearch}/auparse.so
%{python_sitearch}/audit.py*

%files
%defattr(-,root,root,-)

%doc  README COPYING ChangeLog contrib/capp.rules contrib/nispom.rules contrib/lspp.rules contrib/stig.rules init.d/auditd.cron
%attr(644,root,root) %{_mandir}/man8/audispd.8.*
%attr(644,root,root) %{_mandir}/man8/auditctl.8.*
%attr(644,root,root) %{_mandir}/man8/auditd.8.*
%attr(644,root,root) %{_mandir}/man8/aureport.8.*
%attr(644,root,root) %{_mandir}/man8/ausearch.8.*
%attr(644,root,root) %{_mandir}/man8/autrace.8.*
%attr(644,root,root) %{_mandir}/man8/aulast.8.*
%attr(644,root,root) %{_mandir}/man8/aulastlog.8.*
%attr(644,root,root) %{_mandir}/man8/auvirt.8.*
%attr(644,root,root) %{_mandir}/man8/augenrules.8.*
%attr(644,root,root) %{_mandir}/man8/ausyscall.8.*
%attr(644,root,root) %{_mandir}/man5/auditd.conf.5.*
%attr(644,root,root) %{_mandir}/man7/audit.rules.7.*
%attr(644,root,root) %{_mandir}/man5/audispd.conf.5.*
%attr(644,root,root) %{_mandir}/man5/ausearch-expression.5.*
%attr(750,root,root) %{_sbindir}/auditctl
%attr(750,root,root) %{_sbindir}/auditd
%attr(755,root,root) %{_sbindir}/ausearch
%attr(755,root,root) %{_sbindir}/aureport
%attr(750,root,root) %{_sbindir}/autrace
%attr(750,root,root) %{_sbindir}/audispd
%attr(750,root,root) %{_sbindir}/augenrules
%attr(755,root,root) %{_bindir}/aulast
%attr(750,root,root) %{_bindir}/aulastlog
%attr(755,root,root) %{_bindir}/ausyscall
%attr(755,root,root) %{_bindir}/auvirt
%attr(640,root,root) %{_unitdir}/auditd.service
%attr(750,root,root) %dir %{_libexecdir}/initscripts/legacy-actions/auditd
%attr(750,root,root) %{_libexecdir}/initscripts/legacy-actions/auditd/resume
%attr(750,root,root) %{_libexecdir}/initscripts/legacy-actions/auditd/rotate
%attr(750,root,root) %{_libexecdir}/initscripts/legacy-actions/auditd/stop
%attr(750,root,root) %{_libexecdir}/initscripts/legacy-actions/auditd/restart
%attr(750,root,root) %{_libexecdir}/initscripts/legacy-actions/auditd/condrestart
%attr(750,root,root) %{_var}/log/audit
%attr(750,root,root) %dir /etc/audit
%attr(750,root,root) %dir /etc/audisp
%attr(750,root,root) %dir /etc/audisp/plugins.d
%attr(750,root,root) %dir %{_libdir}/audit
%config(noreplace) %attr(640,root,root) /etc/audit/auditd.conf
%config(noreplace) %attr(640,root,root) /etc/audit/rules.d/audit.rules
##%%config(noreplace) %attr(640,root,root) /etc/sysconfig/auditd
%config(noreplace) %attr(640,root,root) /etc/audisp/audispd.conf
%config(noreplace) %attr(640,root,root) /etc/audisp/plugins.d/af_unix.conf

%files -n audispd-plugins
%defattr(-,root,root,-)
%attr(640,root,root) /etc/audisp/plugins.d/syslog.conf
%attr(644,root,root) %{_mandir}/man8/audispd-zos-remote.8.*
%attr(644,root,root) %{_mandir}/man5/zos-remote.conf.5.*
%config(noreplace) %attr(640,root,root) /etc/audisp/plugins.d/audispd-zos-remote.conf
%config(noreplace) %attr(640,root,root) /etc/audisp/zos-remote.conf
%attr(750,root,root) %{_sbindir}/audispd-zos-remote
%config(noreplace) %attr(640,root,root) /etc/audisp/plugins.d/au-prelude.conf
%config(noreplace) %attr(640,root,root) /etc/audisp/audisp-prelude.conf
%attr(750,root,root) %{_sbindir}/audisp-prelude
%attr(644,root,root) %{_mandir}/man5/audisp-prelude.conf.5.*
%attr(644,root,root) %{_mandir}/man8/audisp-prelude.8.*
%config(noreplace) %attr(640,root,root) /etc/audisp/audisp-remote.conf
%config(noreplace) %attr(640,root,root) /etc/audisp/plugins.d/au-remote.conf
%attr(750,root,root) %{_sbindir}/audisp-remote
%attr(644,root,root) %{_mandir}/man5/audisp-remote.conf.5.*
%attr(644,root,root) %{_mandir}/man8/audisp-remote.8.*

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.7-1m)
- update 2.3.7

* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-4m)
- support UserMove env

* Sat Dec  7 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-3m)
- fix build on i686

* Fri Dec 06 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-2m)
- remove old auditd.service

* Sun Nov 17 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2

* Mon Mar 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2
- auditd.service temporarily in both /usr/lib/systemd/ and /lib/systemd 

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.3-2m)
- update systemd service

* Wed Aug 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.3-1m)
- update 2.1.3

* Thu Jul 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.2-1m)
- update 2.1.2

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.1-1m)
- update 2.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-7m)
- full rebuild for mo7 release

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-6m)
- explicitly link libpthread (Patch1)
- drop GSSAPI support

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.4-5m)
- use Requires

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-4m)
- apply glibc212 patch

* Wed Feb  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-3m)
- add BR libcap-ng-devel

* Thu Dec 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- use python_sitearch

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-1m)
- update to 2.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.8-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.7.8-2m)
- rebuild against python-2.6.1-1m

* Sun Nov  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.8-1m)
- update to 1.7.8
- sync to RedHat http://people.redhat.com/sgrubb/audit/

* Fri May 23 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.7.4-2m)
- revised %%files section for x86_64

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.4-1m)
- update 1.7.4

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.3-7m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.3-6m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.3-5m)
- rebuild against gcc43

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-4m)
- rebuild against openldap-2.4.8

* Wed Jan  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-3m)
- modify Requires of audispd-plugins

* Sun Dec 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-2m)
- adjust duplicate files

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3
- update system-config-audit to 0.4.5-1m
- add audispd-plugins package
- sync with Fedora devel

* Sat Aug 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-3m)
- modify %%files section
-- use %%{_sysconfdir}, %%{_initscriptdir} and so on

* Wed Aug 15 2007 Nishio Futoshi <futoshi@momonga-linux>
- (1.5.5-2m)
- add patch2 for new intltool

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-1m)
- update to 1.5.5
- sync with FC-devel
- add sub package system-config-audit
- import two patches from FC

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.4-1m)
- update to 1.5.4

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3
- NoSource and adjust %%files section

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.2-2m)
- add Obsoletes: audit-audispd-plugins, it has no %%files

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-1m)
- update 1.5.2

* Sun Mar 11 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.5-3m)
- add python libdir macro and modify python libdir

* Sat Mar 10 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5-2m)
- modify libdir

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-1m)
- update 1.5
- create audit-audispd-plugins package

* Mon Feb 26 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.1-2m)
- modify pydir

* Tue Feb 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-1m)
- update 1.4.1 

* Thu Feb 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-2m)
- remove Requires: kernel-headers for the moment

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-1m)
- update 1.4 

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-4m)
- delete pyc pyo

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-3m)
- fix python modue install path

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-2m)
- rebuild against python-2.5

* Sat Jul 29 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.5-1m)
- version up

* Fri May 12 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.5-1m)
- version up

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.3-2m)
- rebuild against swig

* Sun Feb 12 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.3-1m)
- import from fc-devel

* Thu Jan 5 2006 Steve Grubb <sgrubb@redhat.com> 1.1.3-1
- Add timestamp to daemon_config messages (#174865)
- Add error checking of year for aureport & ausearh
- Treat af_unix sockets as files for searching and reporting
- Update capp & lspp rules to combine syscalls for higher performance
- Adjusted the chkconfig line for auditd to start a little earlier
- Added skeleton program to docs for people to write their own dispatcher with
- Apply patch from Ulrich Drepper that optimizes resource utilization
- Change ausearch and aureport to unlocked IO

* Thu Dec 5 2005 Steve Grubb <sgrubb@redhat.com> 1.1.2-1
- Add more message types

* Wed Nov 30 2005 Steve Grubb <sgrubb@redhat.com> 1.1.1-1
- Add support for alpha processors
- Update the audisp code
- Add locale code in ausearch and aureport
- Add new rule operator patch
- Add exclude filter patch
- Cleanup make files
- Add python bindings

* Wed Nov 9 2005 Steve Grubb <sgrubb@redhat.com> 1.1-1
- Add initial version of audisp. Just a placeholder at this point
- Remove -t from auditctl

* Mon Nov 7 2005 Steve Grubb <sgrubb@redhat.com> 1.0.12-1
- Add 2 more summary reports
- Add 2 more message types

