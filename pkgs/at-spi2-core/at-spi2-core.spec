%global		momorel 1
Name:           at-spi2-core
Version:        2.6.1
Release: %{momorel}m%{?dist}
Summary:        Protocol definitions and daemon for D-Bus at-spi

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.linuxfoundation.org/en/AT-SPI_on_D-Bus
Source0:        http://download.gnome.org/sources/at-spi2-core/2.6/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires:  dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  glib2-devel
BuildRequires:  gobject-introspection-devel
BuildRequires:  libXtst-devel
BuildRequires:  libXevie-devel
BuildRequires:  libXext-devel
BuildRequires:  libXi-devel
BuildRequires:  autoconf automake libtool
BuildRequires:  intltool

Requires:       dbus

%description
at-spi allows assistive technologies to access GTK-based
applications. Essentially it exposes the internals of applications for
automation, so tools such as screen readers, magnifiers, or even
scripting interfaces can query and interact with GUI controls.

This version of at-spi is a major break from previous versions.
It has been completely rewritten to use D-Bus rather than
ORBIT / CORBA for its transport protocol.

%package devel
Summary: Development files and headers for at-spi2-core
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The at-spi2-core-devel package includes the header files and
API documentation for libatspi.

%prep
%setup -q

%build
%configure --with-dbus-daemondir=/bin

sed -i -e 's+sys_lib_dlsearch_path_spec="/lib /usr/lib+sys_lib_dlsearch_path_spec="/lib /usr/lib /lib64 /usr/lib64+' configure

make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT

%{find_lang} %{name}

rm $RPM_BUILD_ROOT%{_libdir}/libatspi.la

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files -f %{name}.lang
%doc COPYING AUTHORS README
%{_libexecdir}/at-spi2-registryd
%{_datadir}/dbus-1/services/org.a11y.atspi.Registry.service
%{_sysconfdir}/at-spi2
%{_sysconfdir}/xdg/autostart/at-spi-dbus-bus.desktop
%{_libdir}/libatspi.so.*
%{_libdir}/girepository-1.0/Atspi-2.0.typelib
%{_libexecdir}/at-spi-bus-launcher
%{_datadir}/dbus-1/services/org.a11y.Bus.service


%files devel
%{_libdir}/libatspi.so
%{_datadir}/gtk-doc/html/libatspi
%{_datadir}/gir-1.0/Atspi-2.0.gir
%{_includedir}/at-spi-2.0
%{_libdir}/pkgconfig/atspi-2.pc

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.1-1m)
- update to 2.6.1

* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.92-1m)
- update to 2.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.91-1m)
- update to 2.5.91

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.5-1m)
- update to 2.5.5

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.3-1m)
- reimport from fedora

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-2m)
- fix build failure with glib 2.33

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.92-1m)
- update to 2.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.91-1m)
- update to 2.1.91

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Sat May 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-2m)
- welcome at-spi

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-2m)
- obsoletes at-spi

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.8-3m)
- full rebuild for mo7 release

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-2m)
- explicitly link libdl (Patch0)

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.8-1m)
- update to 0.1.8

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.6-1m)
- update to 0.1.6

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5-1m)
- initial build
