%global momorel 2

%define _use_internal_dependency_generator 0

%define contentdir       %{_localstatedir}/www/%{name}
%define libdir           %{_localstatedir}/lib/mrtg

Summary:   Multi Router Traffic Grapher
Name:      mrtg
Version:   2.16.4
Release:   %{momorel}m%{?dist}
URL:       http://oss.oetiker.ch/mrtg/
Source0:   http://oss.oetiker.ch/mrtg/pub/mrtg-%{version}.tar.gz
NoSource:  0
Source1:   http://oss.oetiker.ch/mrtg/pub/mrtg-%{version}.tar.gz.md5.gpg
NoSource:  1
Source2:   mrtg.cfg
Source3:   filter-requires-mrtg.sh
Source4:   mrtg.crond.in
Source5:   mrtg-httpd.conf
Patch0:    mrtg-2.15.0-lib64.patch
Patch1:    mrtg-2.10.5-norpath.patch
License:   GPL
Group:     Applications/Internet
Requires:  cronie initscripts
Requires(post): initscripts
Requires(postun): initscripts
Requires: grep cronie coreutils
BuildRequires: gd-devel, libpng-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define __find_requires %{SOURCE3}

%description
The Multi Router Traffic Grapher (MRTG) is a tool to monitor the traffic
load on network-links. MRTG generates HTML pages containing PNG
images which provide a LIVE visual representation of this traffic.

%prep
%setup -q
%patch0 -p1 -b .lib64
%patch1 -p1

for i in doc/mrtg-forum.1 doc/mrtg-squid.1; do
	iconv -f iso-8859-1 -t utf-8 < "$i" > "${i}_"
	mv "${i}_" "$i"
done

%build
%configure
# Don't link rateup statically, don't link to indirect dependencies
# LIBS derived from autodetected by removing -Wl,-B(static|dynamic), -lpng, -lz
make LIBS='-lgd -lm'
find contrib -type f -exec \
	%{__perl} -e 's,^#!/\s*\S*perl\S*,#!%{__perl},gi' -p -i \{\} \;
find contrib -name "*.pl" -exec %{__perl} -e 's;\015;;gi' -p -i \{\} \;
find contrib -type f | xargs chmod a-x

%install
rm -rf   %{buildroot}
make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/mrtg
mkdir -p %{buildroot}%{_sysconfdir}/cron.d
mkdir -p %{buildroot}%{_localstatedir}/lib/mrtg
mkdir -p %{buildroot}%{_localstatedir}/lock/mrtg
mkdir -p %{buildroot}%{contentdir}
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d

install -m 644 images/*   %{buildroot}%{contentdir}/
sed 's,@CONTENTDIR@,%{contentdir},g; s,@LIBDIR@,%{_localstatedir}/lib/mrtg,g' \
	%{SOURCE2} > %{buildroot}%{_sysconfdir}/mrtg/mrtg.cfg
chmod 644 %{buildroot}%{_sysconfdir}/mrtg/mrtg.cfg
sed -e 's,@bindir@,%{_bindir},g; s,@sysconfdir@,%{_sysconfdir},g;' \
	-e 's,@localstatedir@,%{_localstatedir},g' %{SOURCE4} \
	> %{buildroot}%{_sysconfdir}/cron.d/mrtg
chmod 644 %{buildroot}%{_sysconfdir}/cron.d/mrtg

install -m 644 %{SOURCE5} %{buildroot}%{_sysconfdir}/httpd/conf.d/mrtg.conf.dist

for i in mrtg cfgmaker indexmaker; do
	%{__perl} -pi -e 's;\@\@lib\@\@;%{_lib};g' %{buildroot}%{_bindir}/$i
done

%{__perl} -pi -e 's;\@\@lib\@\@;%{_lib};g' %{buildroot}%{_mandir}/man1/*.1

%clean
rm -rf %{buildroot}

# Tell crond to reload its configuration.
%post
/sbin/service crond condrestart 2>&1 > /dev/null || :

%postun
/sbin/service crond condrestart 2>&1 > /dev/null || :

# Add a trigger to remove the system crontab version of our tasks.
%triggerun -- mrtg <= 2.9.17-12
grep -v '* * * * root %{_bindir}/mrtg' /etc/crontab > /etc/crontab.new &&\
cat /etc/crontab.new > /etc/crontab &&
rm /etc/crontab.new

%files
%defattr(-,root,root)
%doc contrib CHANGES COPYING COPYRIGHT README THANKS
%dir %{_sysconfdir}/mrtg
%config(noreplace) %{_sysconfdir}/mrtg/mrtg.cfg
%config(noreplace) %{_sysconfdir}/cron.d/mrtg
%config(noreplace) %{_sysconfdir}/httpd/conf.d/mrtg.conf.dist
%{contentdir}
%{_bindir}/*
%{_libdir}/mrtg2
%exclude %{_libdir}/mrtg2/Pod
%{_mandir}/*/*
%exclude %{_datadir}/mrtg2/icons
%exclude %{_datadir}/doc/mrtg2
%dir %{_localstatedir}/lib/mrtg
%dir %{_localstatedir}/lock/mrtg

%changelog
* Thu Mar 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.16.4-2m)
- remove Requires(triggerrun) section
-- Obso rpm-4.11

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.16.4-1m)
- update 2.16.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.16.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.16.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.2-6m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.16.2-5m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 31 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.16.2-3m)
- stop cron

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16.2-2m)
- rebuild against rpm-4.6

* Thu Jul 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.2-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.15.2-2m)
- rebuild against perl-5.10.0-1m

* Wed May 16 2007 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (2.15.2-1m)
- update to 2.15.2
- revised URL, Source1

* Wed Sep 27 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (2.14.7-1m)
- update to 2.14.7
- revised Patch0: mrtg-2.14.7-lib64.patch for 2.14.7
- revised Source0,1 URI

* Sun Jul 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.5-1m)
- update to 2.14.5
- rename config file

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.13.2-1m)
- update to 2.13.2

* Tue Jan 31 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (2.13.1-1m)
- update to 2.13.1

* Tue Aug  2 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.12.2-1m)
- update to 2.12.2

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.12.1-3m)
- rebuild against zlib-1.2.3 (CAN-2005-1849)

* Tue Jul 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.1-2m)
- rebuild against zlib-devel 1.2.2-2m, which fixes CAN-2005-2096.

* Wed May 18 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.12.1-1m)
- update to 2.12.1

* Sat Jan  8 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.11.1-1m)
- update to 2.11.1

* Sun Aug  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.15-1m)
- minor bugfixes

* Wed Jun  9 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.14-1m)
- minor bugfixes

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.13-2m)
- revised spec for enabling rpm 4.2.

* Mon Jan 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.13-1m)
- minor bugfixes

* Mon Jan 05 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.10.12-1m)
- update to 2.10.12

* Sat Dec 06 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.10.11-1m)
- update to 2.10.11

* Wed Dec 03 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.10.8-1m)
- update to 2.10.8

* Sun Nov 23 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.10.5-1m)
- update to 2.10.5
- use %%NoSource, %%make
- remove mrtg-2.9.18-manpath.patch

* Sun May 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.9.29-1m)

* Fri Dec 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.9.25-2m)
- cancel 'AutoReq: 0'

* Sun Nov 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.9.25-1m)

* Wed Aug 07 2002 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.9.22-1m)
- update to 2.9.22

* Thu Jul 25 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (2.9.21-1m)
- update to 2.9.21

* Mon May 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.9.18-0.0011002k)
- update to 2.9.18pre11

* Wed Apr 02 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.9.18-0.0004002k)
- update to 2.9.18pre4
- Pod:: ika ha nigiranai

* Wed Apr 02 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.9.18-0.0003002k)
- update to 2.9.18pre3
- rework manpath patch

* Mon Oct 29 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.9.17-4k)
- rebuild against libpng.so.3

* Wed Oct 17 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.9.17-2k)
- update to 2.9.17

* Mon Apr  2 2001 Toru Hoshina <toru@df-usa.com>
- (2.9.10-3k)

* Thu Jan 25 2001 Motonobu Ichimura <famao@kondara.org>
- (2.9.7-3k)
- first release

