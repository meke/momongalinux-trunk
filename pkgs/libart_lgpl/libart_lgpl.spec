%global momorel 4

Summary: A library for high-performance 2D graphics.
Name: libart_lgpl
Version: 2.3.21
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.3/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
Provides: libart

%description
Libart supports a very powerful imaging model, basically the same as
SVG and the Java 2D API. It includes all PostScript imaging
operations, and adds antialiasing and alpha-transparency.

Libart is also highly tuned for incremental rendering. It contains
data structures and algorithms suited to rapid, precise computation of
Region of Interest, as well as a two-phase rendering pipeline
optimized for interactive display.

%package devel
Summary: A library for high-performance 2D graphics.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libart supports a very powerful imaging model, basically the same as
SVG and the Java 2D API. It includes all PostScript imaging
operations, and adds antialiasing and alpha-transparency.

Libart is also highly tuned for incremental rendering. It contains
data structures and algorithms suited to rapid, precise computation of
Region of Interest, as well as a two-phase rendering pipeline
optimized for interactive display.

%prep
%setup -q

%build
%configure --disable-static 
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libart_lgpl_2.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-, root, root)
%doc COPYING test*.c
%{_bindir}/libart2-config
%{_libdir}/libart_lgpl_2.so
%{_libdir}/pkgconfig/libart-2.0.pc
%{_includedir}/libart-2.0

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.21-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.21-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.21-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.21-1m)
- update to 2.3.21

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.20-7m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.20-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.20-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.20-4m)
- define __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.20-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.20-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.20-1m)
- update to 2.3.20
- delete patch0 (merged)

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.19-3m)
- %%NoSource -> NoSource

* Sun Mar  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.19-2m)
- import libart-2.3.19-header.patch from Fedora devel (fix kdelibs build problem)
 +* Thu Mar 01 2007 Behdad Esfahbod <besfahbo@edhat.com> - 2.3.19-2
 +- Add upstreamed patch libart-2.3.19-header.patch
 +- Resolves: #230571

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.19-1m)
- update to 2.3.19

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.17-4m)
- delete libtool library

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.17-3m)
- comment out unnessesaly autoreconf and make check

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.17-2m)
- enable shared

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.17-1m)
- version up.
- GNOME 2.12.1 Desktop

* Tue Dec 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.3.16-3m)
- add auto commands before %%build for fix missing lib*.so problem

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.3.16-2m)
- includes test sources.
- changes Sumamry and Description.
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.3.16-1m)
- version 2.3.16

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.14-1m)
- version 2.3.14

* Tue Jul 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.13-1m)
- version 2.3.13

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.12-1m)
- version 2.3.12

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.11-1m)
- version 2.3.11

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.10-1m)
- version 2.3.10

* Tue Jun 25 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.9-2k)
- version 2.3.9

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.8-2k)
- version 2.3.8

* Fri Jan 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.8-2k)
- release version

* Mon Jan  7 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.8-0.02002010701k)
- cvs version 2.3.8

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (2.3.7-2k)
- port from Jirai
- version 2.3.7

* Tue Sep 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (2.3.5-3k)
- created spec file
