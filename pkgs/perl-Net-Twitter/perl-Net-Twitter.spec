%global         momorel 2

Name:           perl-Net-Twitter
Version:        4.01004
Release:        %{momorel}m%{?dist}
Summary:        Perl interface to the Twitter API
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-Twitter/
Source0:        http://www.cpan.org/authors/id/M/MM/MMIMS/Net-Twitter-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Crypt-SSLeay >= 0.5
BuildRequires:  perl-Data-Visitor
BuildRequires:  perl-DateTime >= 0.51
BuildRequires:  perl-DateTime-Format-Strptime >= 1.09
BuildRequires:  perl-Devel-StackTrace
BuildRequires:  perl-Digest-SHA
BuildRequires:  perl-Encode
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-JSON-Any >= 1.21
BuildRequires:  perl-JSON-XS
BuildRequires:  perl-libnet
BuildRequires:  perl-libwww-perl >= 2.032
BuildRequires:  perl-Module-Install-AutoManifest
BuildRequires:  perl-Module-Install-Repository
BuildRequires:  perl-Moose >= 0.9
BuildRequires:  perl-MooseX-MultiInitArg
BuildRequires:  perl-namespace-autoclean >= 0.09
BuildRequires:  perl-Net-OAuth >= 0.25
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Time-HiRes
BuildRequires:  perl-Try-Tiny >= 0.03
BuildRequires:  perl-URI >= 1.4
Requires:       perl-Crypt-SSLeay >= 0.5
Requires:       perl-Data-Visitor
Requires:       perl-DateTime >= 0.51
Requires:       perl-DateTime-Format-Strptime >= 1.09
Requires:       perl-Devel-StackTrace
Requires:       perl-Digest-SHA
Requires:       perl-Encode
Requires:       perl-HTML-Parser
Requires:       perl-JSON-Any >= 1.21
Requires:       perl-JSON-XS
Requires:       perl-libnet
Requires:       perl-libwww-perl >= 2.032
Requires:       perl-Module-Install-AutoManifest
Requires:       perl-Module-Install-Repository
Requires:       perl-Moose >= 0.9
Requires:       perl-MooseX-MultiInitArg
Requires:       perl-namespace-autoclean >= 0.09
Requires:       perl-Net-OAuth >= 0.25
Requires:       perl-Scalar-Util
Requires:       perl-Time-HiRes
Requires:       perl-Try-Tiny >= 0.03
Requires:       perl-URI >= 1.4
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides a perl interface to the Twitter APIs. See
http://dev.twitter.com/doc for a full description of the Twitter APIs.

%prep
%setup -q -n Net-Twitter-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Net/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01004-2m)
- rebuild against perl-5.20.0

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01004-1m)
- update to 4.01004

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01003-1m)
- update to 4.01003

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01002-1m)
- update to 4.01002
- rebuild against perl-5.18.2

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01000-1m)
- update to 4.01000

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00007-1m)
- update to 4.00007
- rebuild against perl-5.18.1

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00006-1m)
- update to 4.00006

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00005-2m)
- rebuild against perl-5.18.0

* Fri Apr 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00005-1m)
- update to 4.00005

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00004-1m)
- update to 4.00004
- rebuild against perl-5.16.3

* Thu Feb 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00003-1m)
- update to 4.00003

* Mon Feb 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00002-1m)
- update to 4.00002

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00001-1m)
- update to 4.00001

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.18004-2m)
- rebuild against perl-5.16.2

* Tue Oct 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.18004-1m)
- update to 3.18004

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.18003-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.18003-1m)
- update to 3.18003
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.18001-2m)
- rebuild against perl-5.14.2

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.18001-1m)
- update to 3.18001

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.17001-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.17001-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.17001-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.17001-1m)
- update to 3.17001

* Wed Mar 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.17000-1m)
- update to 3.17000

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16000-1m)
- update to 3.16000

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15000-1m)
- update to 3.15000

* Tue Feb  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14003-1m)
- update to 3.14003

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.14002-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14002-1m)
- update to 3.14002

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14001-1m)
- update to 3.14001

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14000-1m)
- update to 3.14000

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13009-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13009-1m)
- update to 3.13009

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13007-2m)
- full rebuild for mo7 release

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13007-1m)
- update to 3.13007

* Sat Jun 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13006-1m)
- update to 3.13006

* Sat Jun 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13004-1m)
- update to 3.13004

* Sun May 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.13003-2m)
- BuildRequires: perl-MooseX-MultiInitArg >= 0.01-6m

* Sat May 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13003-1m)
- update to 3.13003

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13001-2m)
- rebuild against perl-5.12.1

* Wed May 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13001-1m)
- update to 3.13001

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12000-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12000-1m)
- update to 3.12000

* Sun Mar 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11012-1m)
- update to 3.11012

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11011-1m)
- update to 3.11011

* Thu Mar 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11009-1m)
- update to 3.11009

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11008-1m)
- update to 3.11008

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11007-1m)
- update to 3.11007

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11006-1m)
- update to 3.11006

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11004-1m)
- update to 3.11004

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11003-1m)
- update to 3.11003

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11002-1m)
- update to 3.11002

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11001-1m)
- update to 3.11001

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11000-1m)
- update to 3.11000

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10003-1m)
- update to 3.10003

* Wed Dec  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10001-1m)
- update to 3.10001

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.09000-1m)
- update to 3.09000

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.08000-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.08000-1m)
- update to 3.08000

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.07004-1m)
- update to 3.07004

* Thu Oct 22 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.07003-2m)
- add BR version

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.07003-1m)
- update to 3.07003

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.07002-1m)
- update to 3.07002

* Wed Sep 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.07000-1m)
- update to 3.07000

* Wed Sep 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.07000-1m)
- update to 3.07000

* Wed Sep 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.07000-1m)
- update to 3.07000

* Sat Sep 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.06000-1m)
- update to 3.06000

* Sat Sep 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.05003-1m)
- update to 3.05003

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.05002-1m)
- update to 3.05002

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.05001-1m)
- update to 3.05001

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.05000-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.05000-1m)
- update to 3.05000

* Wed Aug  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.04006-1m)
- update to 3.04006

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.04003-1m)
- update to 3.04003

* Tue Jun 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.03001-1m)
- update to 3.03001

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.03000-1m)
- update to 3.03000
- modify BuildRequires: and Requires:

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-1m)
- update to 2.12

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Sat Feb 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Sat Feb 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-1m)
- update to 2.09

* Sat Feb 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-1m)
- update to 2.08

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Tue Feb 10 2009 NARITA Koichi <pulsar@momonga-linnu.org>
- (2.06-1m)
- update to 2.06

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.01-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Thu Jan  9 2009 NARITA Koichi <pulsar@momonga-Linux.org>
- (1.23-1m)
- update to 1.23

* Fri Dec 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Thu Dec 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Thu Jun 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.06-2m)
- rebuild against gcc43

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.06-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
