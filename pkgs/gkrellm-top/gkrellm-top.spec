%global momorel 4
%global gkplugindir %{_libdir}/gkrellm2/plugins

Summary:	GKrellM plugin which shows 3 most CPU intensive processes
Name:		gkrellm-top
Version:	2.2.13
Release:	%{momorel}m%{?dist}
License:	GPL+
Group:		Applications/System
URL:		http://gkrelltop.sourceforge.net/
Source:		http://downloads.sf.net/gkrelltop/gkrelltop_%{version}.orig.tar.gz
Patch0:		gkrelltop-2.2.13-optflags.patch
Requires:	gkrellm >= 2.2.0
BuildRequires:	gkrellm-devel >= 2.2.0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
A GKrellM plugin which displays the top three CPU intensive processes in a
small window inside GKrellM, similar to wmtop. Useful to check out anytime
what processes are consuming most CPU power on your machine.

%prep
%setup -q -n gkrelltop-%{version}.orig
%patch0 -p1 -b .optflags

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
install -D -m 755 gkrelltop.so $RPM_BUILD_ROOT%{gkplugindir}/gkrelltop.so
install -D -m 755 gkrelltopd.so $RPM_BUILD_ROOT%{gkplugindir}/gkrelltopd.so

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README
%{gkplugindir}/gkrelltop.so
%{gkplugindir}/gkrelltopd.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.13-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.13-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.13-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.13-1m)
- update to 2.2.13

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.11-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.11-1m)
- import from Fedora to Momonga

* Sun Feb 10 2008 Robert Scheck <robert@fedoraproject.org> 2.2.11-1
- Upgrade to 2.2.11

* Wed Dec 12 2007 Robert Scheck <robert@fedoraproject.org> 2.2.10-1
- Upgrade to 2.2.10
- Initial spec file for Fedora and Red Hat Enterprise Linux
