%global momorel	1
%global octpkg octcdf

Name:           octave-%{octpkg}
Version:        1.1.4
Release:        %{momorel}m%{?dist}
Summary:        A NetCDF interface for octave
Group:          Applications/Engineering
License:        GPLv2+
URL:            http://octave.sourceforge.net/octcdf/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  octave-devel  
BuildRequires:  netcdf-devel

Requires:       octave(api) = %{octave_api}
Requires(post): octave
Requires(postun): octave
Obsoletes:      octave-forge <= 20090607


%description
A NetCDF interface for octave.

%prep
%setup -q -n %{octpkg}

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install

%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)
%{octpkglibdir}

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo
%{octpkgdir}/@ncatt
%{octpkgdir}/@ncdim
%{octpkgdir}/@ncfile
%{octpkgdir}/@ncvar

%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4
- build against octave-3.6.1-1m

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-1m)
- initial import from fedora
