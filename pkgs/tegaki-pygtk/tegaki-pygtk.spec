%global momorel 6
#%%define py_ver %(python -c 'import sys;print(sys.version[0:3])')
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: The Base user interface library for the Tegaki project
Name: tegaki-pygtk
Version: 0.3.1
Release: %{momorel}m%{?dist}
URL: http://www.tegaki.org/
Source0: http://www.tegaki.org/releases/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
License: LGPL
Group: Development/Languages
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gtk2
Requires: python >= %{pyver}
Requires: tegaki-python
BuildRequires: python-devel >= %{pyver}
BuildRequires: gtk2-devel

%description
he Base user interface library for the Tegaki project.
 
%prep
%setup -q -n %{name}-%{version}

%build

%install
rm -rf --preserve-root %{buildroot}
%{__python} setup.py install --root %{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(644, root, root, 755)
%doc AUTHORS COPYING README ChangeLog* TODO
%{_datadir}/tegaki/icons/handwriting.png
%dir %{python_sitelib}/tegakigtk/
%{python_sitelib}/tegakigtk/*
%{python_sitelib}/tegaki*.egg-info

%changelog
* Wed May  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-3m)
- full rebuild for mo7 release

* Sat Apr 10 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3.1-2m)
- user python_sitelib instead of python_sitearch

* Sat Apr 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-1m)
- initial Momonga spec file
