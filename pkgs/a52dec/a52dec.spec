%global momorel 12
Summary:   A library for handling encrypted dvds.
Name:      a52dec
Version:   0.7.4
Release: %{momorel}m%{?dist}
URL: 	   http://liba52.sourceforge.net/
License:   GPL
Group:     Applications/Multimedia
Source0:    http://liba52.sourceforge.net/files/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource:  0
Requires(post): libselinux-utils

%description
liba52 is a free library for decoding ATSC A/52 streams. It is released
under the terms of the GPL license. The A/52 standard is used in a
variety of applications, including digital television and DVD. It is
also known as AC-3.

%package devel
Summary: %{name} development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel includes development files for %{name}

%prep
%setup -q

%build
libtoolize -c -f
aclocal
autoconf
%configure --enable-shared=yes
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall
cp doc/liba52.txt .

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
if test -f %{_sbindir}/getenforce ; then
  result=$(%{_sbindir}/getenforce)
  if test "x${result}" != "xDisabled" ; then
    chcon -t textrel_shlib_t %{_libdir}/liba52.so.0.0.0
  fi
fi

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HISTORY NEWS README TODO liba52.txt
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/man*/*

%files devel
%defattr(-,root,root)
%{_includedir}/a52dec
%{_libdir}/*.so
%{_libdir}/*.a

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.4-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.4-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.4-10m)
- add Requires(post): libselinux-utils

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.4-9m)
- full rebuild for mo7 release

* Thu Aug 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.4-8m)
- add chcon hack
- [BTS 282]

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.4-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.4-5m)
- rebuild against gcc43

* Tue May 15 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.4-4m)
- --enable-shared=yes

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.4-3m)
- delete libtool library

* Wed Jan 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.4-2m)
- add defattr to files devel section

* Mon Jul 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.4-1m)
- minor feature enhancements

* Fri Jun 14 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.7.3-2k)
  update to 0.7.3

* Fri Mar 22 2002 Toru Hoshina <t@kondara.org>
- (0.7.2-4k)
- add alpha support...

* Sat Mar  9 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.2-2k)
- down to 0.7.2, for ogle

* Wed Mar  6 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.3-2k)
- up to 0.7.3

* Fri Jan 18 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.2-3k)
- kondarize

* Tue Dec 18 2001 Martin Norbaeck <d95mback@dtek.chalmers.se>
- Update to version 0.7.2

* Thu Sep 20 2001 Martin Norbaeck <d95mback@dtek.chalmers.se>
- Added missing .la files
- Building statically
* Thu Sep 20 2001 Martin Norbaeck <d95mback@dtek.chalmers.se>
- Initial version
