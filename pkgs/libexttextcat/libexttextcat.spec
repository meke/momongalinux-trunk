%global momorel 1

Name: libexttextcat
Version: 3.2.0
Release: %{momorel}m%{?dist}
Summary: Text categorization library

Group: System Environment/Libraries
License: BSD
URL: http://www.freedesktop.org/wiki/Software/libexttextcat
Source: http://dev-www.libreoffice.org/src/libexttextcat/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Obsoletes: libtextcat < 3.2.0
Provides: libtextcat = %{version}

%description
%{name} is an N-Gram-Based Text Categorization library primarily
intended for language guessing.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Obsoletes: libtextcat-devel < 3.2.0
Provides: libtextcat-devel = %{version}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package tools
Summary: Tool for creating custom document fingerprints
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}

%description tools
The %{name}-tools package contains the createfp program that allows
you to easily create your own document fingerprints.


%prep
%setup -q


%build
%configure --disable-static --disable-werror
%make


%install
rm -rf %{buildroot}
%makeinstall

rm -f %{buildroot}/%{_libdir}/*.la


%check
make check


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc LICENSE README*
%{_libdir}/%{name}.so.*
%{_datadir}/%{name}


%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc


%files tools
%defattr(-,root,root,-)
%{_bindir}/createfp


%changelog
* Thu May 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0-1m)
- Initial Commit Momonga Linux
