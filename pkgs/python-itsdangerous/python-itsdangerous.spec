%global momorel 2

%global upstream_name itsdangerous

%global python3 1

Name:           python-%{upstream_name}
Version:        0.23
Release:        %{momorel}m%{?dist}
Summary:        Python library for passing trusted data to untrusted environments
License:        BSD
Group:          Development/Libraries
URL:            http://pythonhosted.org/itsdangerous/
Source0:        http://pypi.python.org/packages/source/i/%{upstream_name}/%{upstream_name}-%{version}.tar.gz
NoSource:	0
BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  python-setuptools
%if %{python3}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%endif

%description
Itsdangerous is a Python library for passing data through untrusted 
environments (for example, HTTP cookies) while ensuring the data is not 
tampered with.

Internally itsdangerous uses HMAC and SHA1 for signing by default and bases the 
implementation on the Django signing module. It also however supports JSON Web 
Signatures (JWS).

%if %{python3}
%package -n python3-%{upstream_name}
Summary:        Python 3 library for passing trusted data to untrusted environments

%description -n python3-%{upstream_name}
Itsdangerous is a Python 3 library for passing data through untrusted 
environments (for example, HTTP cookies) while ensuring the data is not 
tampered with.

Internally itsdangerous uses HMAC and SHA1 for signing by default and bases the 
implementation on the Django signing module. It also however supports JSON Web 
Signatures (JWS).
%endif

%prep
%setup -q -n %{upstream_name}-%{version}
rm -r *.egg-info

%if %{python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build
%{__python} setup.py build

%if %{python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif

%install
%if %{python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif

%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%check
PYTHONPATH=%{buildroot}%{python_sitelib} %{__python} tests.py

%if %{python3}
pushd %{py3dir}
PYTHONPATH=%{buildroot}%{python3_sitelib} %{__python3} tests.py
popd
%endif

%files
%doc LICENSE CHANGES README
%{python_sitelib}/%{upstream_name}.py*
%{python_sitelib}/%{upstream_name}*.egg-info

%if %{python3}
%files -n python3-%{upstream_name}
%doc LICENSE CHANGES README
%{python3_sitelib}/%{upstream_name}.py
%{python3_sitelib}/%{upstream_name}*.egg-info
%{python3_sitelib}/__pycache__/%{upstream_name}*
%endif

%changelog
* Tue Mar 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23-2m)
- enable python3 support

* Tue Mar 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23-1m)
- Initial commit Momonga Linux

* Thu Aug 15 2013 Dan Callaghan <dcallagh@redhat.com> - 0.23-1
- new upstream release 0.23 (no code changes, only packaging fixes)

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.22-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Tue Jul 09 2013 Dan Callaghan <dcallagh@redhat.com> - 0.22-1
- new upstream release 0.22

* Tue Jun 18 2013 Dan Callaghan <dcallagh@redhat.com> - 0.21-3
- disable Python 3 subpackage on Fedora 17

* Mon Jun 17 2013 Dan Callaghan <dcallagh@redhat.com> - 0.21-2
- $RPM_BUILD_ROOT -> %%{buildroot}

* Fri Jun 14 2013 Dan Callaghan <dcallagh@redhat.com> - 0.21-1
- updated to upstream release 0.21
- added Python 3 subpackage

* Wed Nov 16 2011 Dan Callaghan <dcallagh@redhat.com> - 0.11-1
- initial version
