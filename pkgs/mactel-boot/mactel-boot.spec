%global momorel 1

Name:		mactel-boot
Version:	0.9
Release:	%{momorel}m%{?dist}
Summary:	Intel Mac boot files

Group:		System Environment/Base
License:	GPLv2+
URL:		http://www.codon.org.uk/~mjg59/mactel-boot/
Source:		http://www.codon.org.uk/~mjg59/mactel-boot/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

#ExclusiveArch:	x86_64

Requires:	coreutils

%description
Files for booting Fedora on Intel-based Apple hardware using EFI.

%prep
%setup -q

%build
make PRODUCTVERSION="Momonga %{momonga}" %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
install -D -m 644 SystemVersion.plist $RPM_BUILD_ROOT/boot/efi/System/Library/CoreServices/SystemVersion.plist
echo "This file is required for booting" >$RPM_BUILD_ROOT/boot/efi/mach_kernel
touch $RPM_BUILD_ROOT/boot/efi/System/Library/CoreServices/boot.efi
touch $RPM_BUILD_ROOT/boot/efi/.VolumeIcon.icns

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc GPL
%doc Copyright
%{_mandir}/man1/hfs-bless.1.*
/boot/efi/mach_kernel
/boot/efi/System/Library/CoreServices/SystemVersion.plist
/usr/sbin/hfs-bless
%attr(0755, root, root) %ghost /boot/efi/System/Library/CoreServices/boot.efi
%attr(0644, root, root) %ghost /boot/efi/.VolumeIcon.icns

%triggerin -- grub-efi
if [ x`df -T /boot/efi | tail -n +2 | awk '{print $2}'` = x"hfsplus" ]; then
   hfs-bless /boot/efi/System/Library/CoreServices/boot.efi
fi

%triggerin -- grub2-efi
if [ x`df -T /boot/efi | tail -n +2 | awk '{print $2}'` = x"hfsplus" ]; then
   hfs-bless /boot/efi/System/Library/CoreServices/boot.efi
fi

%post
if [ x`df -T /boot/efi | tail -n +2 | awk '{print $2}'` = x"hfsplus" ]; then
   if [ -f /boot/efi/EFI/redhat/grub2-efi/grub.efi ]; then
	ln -sf ../../../EFI/redhat/grub2-efi/grub.efi /boot/efi/System/Library/CoreServices/boot.efi;
   elif [ -f /boot/efi/EFI/redhat/grub.efi ]; then
	ln -sf ../../../EFI/redhat/grub.efi /boot/efi/System/Library/CoreServices/boot.efi;
   fi

   if [ -f /usr/share/pixmaps/bootloader/fedora.icns ]; then
	cp /usr/share/pixmaps/bootloader/fedora.icns /boot/efi/.VolumeIcon.icns
   fi
fi

%changelog
* Wed Sep 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9-1m)
- Initial Commit Momonga Linux
