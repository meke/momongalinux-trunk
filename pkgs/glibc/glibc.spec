%global momorel 2
# valgrind and gdb requires glibc-debuginfo
%global _enable_debug_packages 1
%global glibc_release_url http://ftp.gnu.org/gnu/glibc/
%global silentrules PARALLELMFLAGS=

%define glibcsrcdir glibc-2.19-195-gd71aeee
%define glibcversion 2.19.90
### glibc.spec.in follows:
%define run_glibc_tests 0
%define auxarches athlon alphaev6
%define xenarches i686 athlon
%ifarch %{xenarches}
%define buildxen 1
%define xenpackage 0
%else
%define buildxen 0
%define xenpackage 0
%endif
%ifarch ppc ppc64
%define buildpower6 1
%else
%define buildpower6 0
%endif
%define rtkaioarches %{ix86} x86_64 ia64 ppc ppc64 s390 s390x
%define biarcharches %{ix86} x86_64 ppc ppc64
%define debuginfocommonarches %{biarcharches} alpha alphaev6
%define multiarcharches ppc ppc64 %{ix86} x86_64 %{sparc}
%define systemtaparches %{ix86} x86_64 ppc ppc64 s390 s390x

Summary: The GNU libc libraries
Name: glibc
Version: %{glibcversion}
Release: %{momorel}m%{?dist}
# GPLv2+ is used in a bunch of programs, LGPLv2+ is used for libraries.
# Things that are linked directly into dynamically linked programs
# and shared libraries (e.g. crt files, lib*_nonshared.a) have an additional
# exception which allows linking it into any kind of programs or shared
# libraries without restrictions.
License: LGPLv2+ and "LGPLv2+ with exceptions" and GPLv2+
Group: System Environment/Libraries
URL: http://www.gnu.org/software/glibc/
Source0: %{?glibc_release_url}%{glibcsrcdir}.tar.xz
Source1: build-locale-archive.c
Source2: glibc_post_upgrade.c
Source3: libc-lock.h
Source4: nscd.conf
Source7: nsswitch.conf
Source8: power6emul.c

#
# Patches that are highly unlikely to ever be accepted upstream.
#

# Configuration twiddle, not sure there's a good case to get upstream to
# change this.
Patch0001: %{name}-fedora-nscd.patch

Patch0003: %{name}-fedora-ldd.patch

Patch0004: %{name}-fedora-ppc-unwind.patch

# Build info files in the source tree, then move to the build
# tree so that they're identical for multilib builds
Patch0005: %{name}-rh825061.patch

# Horrible hack, never to be upstreamed.  Can go away once the world
# has been rebuilt to use the new ld.so path.
Patch0006: %{name}-arm-hardfloat-3.patch

# Needs to be sent upstream
Patch0008: %{name}-fedora-getrlimit-PLT.patch
Patch0009: %{name}-fedora-include-bits-ldbl.patch

# Needs to be sent upstream
Patch0029: %{name}-rh841318.patch

# All these were from the glibc-fedora.patch mega-patch and need another
# round of reviewing.  Ideally they'll either be submitted upstream or
# dropped.
Patch0012: %{name}-fedora-linux-tcsetattr.patch
Patch0014: %{name}-fedora-nptl-linklibc.patch
Patch0015: %{name}-fedora-localedef.patch
Patch0016: %{name}-fedora-i386-tls-direct-seg-refs.patch
Patch0019: %{name}-fedora-nis-rh188246.patch
Patch0020: %{name}-fedora-manual-dircategory.patch
Patch0024: %{name}-fedora-locarchive.patch
Patch0025: %{name}-fedora-streams-rh436349.patch
Patch0028: %{name}-fedora-localedata-rh61908.patch
Patch0030: %{name}-fedora-uname-getrlimit.patch
Patch0031: %{name}-fedora-__libc_multiple_libcs.patch
Patch0032: %{name}-fedora-elf-rh737223.patch
Patch0033: %{name}-fedora-elf-ORIGIN.patch
Patch0034: %{name}-fedora-elf-init-hidden_undef.patch

# Needs to be sent upstream.
# Support mangling and demangling null pointers.
Patch0037: %{name}-rh952799.patch

# rtkaio and c_stubs.  Note that despite the numbering, these are always
# applied first.
Patch0038: %{name}-rtkaio.patch
Patch0039: %{name}-c_stubs.patch

# Remove non-ELF support in rtkaio
Patch0040: %{name}-rh731833-rtkaio.patch
Patch0041: %{name}-rh731833-rtkaio-2.patch
Patch0042: %{name}-rh970865.patch

# ARM: Accept that some objects marked hard ABI are now not because of a
#      binutils bug.
Patch0044: %{name}-rh1009145.patch

# Allow applications to call pthread_atfork without libpthread.so.
Patch0046: %{name}-rh1013801.patch

Patch0047: %{name}-nscd-sysconfig.patch

##############################################################################
#
# Patches from upstream
#
##############################################################################

# None!

##############################################################################
#
# Patches submitted, but not yet approved upstream.
#
##############################################################################
#
# Each should be associated with a BZ.
# Obviously we're not there right now, but that's the goal
#

# http://sourceware.org/ml/libc-alpha/2012-12/msg00103.html
Patch2007: %{name}-rh697421.patch

Patch2011: %{name}-rh757881.patch

Patch2013: %{name}-rh741105.patch

# Upstream BZ 14247
Patch2023: %{name}-rh827510.patch

# Upstream BZ 13028
Patch2026: %{name}-rh841787.patch

# Upstream BZ 14185
Patch2027: %{name}-rh819430.patch

Patch2031: %{name}-rh1070416.patch

Patch2033: glibc-aarch64-tls-fixes.patch

Patch10001: %{name}-locale.patch


Patch10000: glibc-2.19-564-g14642b8.patch.xz

##############################################################################
# End of glibc patches.
##############################################################################

Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: glibc-profile < 2.4
Obsoletes: nss_db
Provides: /sbin/ldconfig
# The dynamic linker supports DT_GNU_HASH
Provides: rtld(GNU_HASH)
Provides: /usr/sbin/glibc_post_upgrade.%{_target_cpu}
Requires: glibc-common = %{version}-%{release}
# Require libgcc in case some program calls pthread_cancel in its %%post
Requires(pre): basesystem, libgcc
# This is for building auxiliary programs like memusage, nscd
# For initial glibc bootstraps it can be commented out
BuildRequires: gd-devel libpng-devel zlib-devel texinfo, libselinux-devel >= 1.33.4-3
BuildRequires: audit-libs-devel >= 1.1.3, sed >= 3.95, libcap-devel, gettext, nss-devel
BuildRequires: autoconf >= 2.69
BuildRequires: /bin/ps, /bin/kill, /bin/awk
%ifarch %{systemtaparches}
BuildRequires: systemtap-sdt-devel
%endif
# This is to ensure that __frame_state_for is exported by glibc
# will be compatible with egcs 1.x.y
BuildRequires: gcc >= 3.2
%define enablekernel 2.6.32
Conflicts: kernel < %{enablekernel}
%define target %{_target_cpu}-redhat-linux
%ifarch i386
%define nptl_target_cpu i486
%else
%define nptl_target_cpu %{_target_cpu}
%endif
%ifarch %{multiarcharches}
# Need STT_IFUNC support
%ifarch ppc ppc64
BuildRequires: binutils >= 2.20.51.0.2
Conflicts: binutils < 2.20.51.0.2
%else
BuildRequires: binutils >= 2.19.51.0.10
Conflicts: binutils < 2.19.51.0.10
%endif
# Earlier releases have broken support for IRELATIVE relocations
Conflicts: prelink < 0.4.2
%else
# Need AS_NEEDED directive
# Need --hash-style=* support
BuildRequires: binutils >= 2.17.50.0.2-5
%endif
BuildRequires: gcc >= 3.2.1-5
%ifarch ppc s390 s390x
BuildRequires: gcc >= 4.1.0-0.17
%endif
%if 0%{?_enable_debug_packages}
BuildRequires: elfutils >= 0.152
BuildRequires: rpm >= 4.2-0.56
%endif
%define __find_provides %{_builddir}/%{glibcsrcdir}/find_provides.sh
%define _filter_GLIBC_PRIVATE 1

%description
The glibc package contains standard libraries which are used by
multiple programs on the system. In order to save disk space and
memory, as well as to make upgrading easier, common system code is
kept in one place and shared between programs. This particular package
contains the most important sets of shared libraries: the standard C
library and the standard math library. Without these two libraries, a
Linux system will not function.

%if %{xenpackage}
%package xen
Summary: The GNU libc libraries (optimized for running under Xen)
Group: System Environment/Libraries
Requires: glibc = %{version}-%{release}, glibc-utils = %{version}-%{release}

%description xen
The standard glibc package is optimized for native kernels and does not
perform as well under the Xen hypervisor.  This package provides alternative
library binaries that will be selected instead when running under Xen.

Install glibc-xen if you might run your system under the Xen hypervisor.
%endif

%package devel
Summary: Object files for development using standard C libraries.
Group: Development/Libraries
Requires(pre): /sbin/install-info
Requires(pre): %{name}-headers
Requires: %{name}-headers = %{version}-%{release}
Requires: %{name} = %{version}-%{release}

%description devel
The glibc-devel package contains the object files necessary
for developing programs which use the standard C libraries (which are
used by nearly all programs).  If you are developing programs which
will use the standard C libraries, your system needs to have these
standard object files available in order to create the
executables.

Install glibc-devel if you are going to develop programs which will
use the standard C libraries.

%package static
Summary: C library static libraries for -static linking.
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
The glibc-static package contains the C library static libraries
for -static linking.  You don't need these, unless you link statically,
which is highly discouraged.

%package headers
Summary: Header files for development using standard C libraries.
Group: Development/Libraries
Provides: %{name}-headers(%{_target_cpu})
%ifarch x86_64
# If both -m32 and -m64 is to be supported on AMD64, x86_64 glibc-headers
# have to be installed, not i586 ones.
Obsoletes: %{name}-headers(i586)
Obsoletes: %{name}-headers(i686)
%endif
Requires(pre): kernel-headers
Requires: kernel-headers >= 2.2.1, %{name} = %{version}-%{release}
BuildRequires: kernel-headers >= 2.6.22

%description headers
The glibc-headers package contains the header files necessary
for developing programs which use the standard C libraries (which are
used by nearly all programs).  If you are developing programs which
will use the standard C libraries, your system needs to have these
standard header files available in order to create the
executables.

Install glibc-headers if you are going to develop programs which will
use the standard C libraries.

%package common
Summary: Common binaries and locale data for glibc
Provides: /usr/sbin/build-locale-archive
Requires: %{name} = %{version}-%{release}
Requires: tzdata >= 2003a
Group: System Environment/Base

%description common
The glibc-common package includes common binaries for the GNU libc
libraries, as well as national language (locale) support.

%package -n nscd
Summary: A Name Service Caching Daemon (nscd).
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}
Requires: libselinux >= 1.17.10-1, audit-libs >= 1.1.3
Requires(pre): coreutils, shadow-utils
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units, 

%description -n nscd
Nscd caches name service lookups and can dramatically improve
performance with NIS+, and may help with DNS as well.

%package utils
Summary: Development utilities from GNU C library
Group: Development/Tools
Requires: %{name} = %{version}-%{release}

%description utils
The glibc-utils package contains memusage, a memory usage profiler,
mtrace, a memory leak tracer and xtrace, a function call tracer
which can be helpful during program debugging.

If unsure if you need this, don't install this package.

%if 0%{?_enable_debug_packages}
%define debug_package %{nil}
%define __debug_install_post %{nil}
%global __debug_package 1

%package debuginfo
Summary: Debug information for package %{name}
Group: Development/Debug
AutoReqProv: no
Requires: filesystem
%ifarch %{debuginfocommonarches}
Requires: glibc-debuginfo-common = %{version}-%{release}
%else
%ifarch %{ix86} %{sparc}
Obsoletes: glibc-debuginfo-common
%endif
%endif

%description debuginfo
This package provides debug information for package %{name}.
Debug information is useful when developing applications that use this
package or when debugging this package.

This package also contains static standard C libraries with
debugging information.  You need this only if you want to step into
C library routines during debugging programs statically linked against
one or more of the standard C libraries.
To use this debugging information, you need to link binaries
with -static -L%{_prefix}/lib/debug%{_prefix}/%{_libdir} compiler options.

%ifarch %{debuginfocommonarches}

%package debuginfo-common
Summary: Debug information for package %{name}
Group: Development/Debug
AutoReqProv: no

%description debuginfo-common
This package provides debug information for package %{name}.
Debug information is useful when developing applications that use this
package or when debugging this package.

%endif
%endif

%prep
%setup -q -n %{glibcsrcdir} 
# upstream patch
%patch10000 -p1
%patch10001 -p1

# Patch order is important as some patches depend on other patches and
# therefore the order must not be changed.  First two are rtkaio and c_stubs;
# we maintain this only in Fedora.
%patch0038 -p1
%patch0039 -p1
%patch0001 -p1
%patch0003 -p1
%patch0004 -p1
%patch0005 -p1
%patch0006 -p1
%patch2007 -p1
%patch0008 -p1
%patch0009 -p1
%patch2011 -p1
%patch0012 -p1
%patch2013 -p1
%patch0014 -p1
%patch0015 -p1
%patch0016 -p1
%patch0019 -p1
%patch0020 -p1
%patch2023 -p1
%patch0024 -p1
%patch0025 -p1
%patch2026 -p1
%patch2027 -p1
%patch0028 -p1
%patch0029 -p1
%patch0030 -p1
%patch0031 -p1
%patch0032 -p1
%patch0033 -p1
%patch0034 -p1
%patch0037 -p1
%patch0040 -p1
%patch0041 -p1
%patch0042 -p1
%patch0044 -p1
%patch0046 -p1
%patch2031 -p1
%patch0047 -p1
%patch2033 -p1

##############################################################################
# A lot of programs still misuse memcpy when they have to use
# memmove. The memcpy implementation below is not tolerant at
# all.
rm -f sysdeps/alpha/alphaev6/memcpy.S
%if %{buildpower6}
# On powerpc32, hp timing is only available in power4/power6
# libs, not in base, so pre-power4 dynamic linker is incompatible
# with power6 libs.
rm -f sysdeps/powerpc/powerpc32/power4/hp-timing.[ch]
%endif

find . -type f -size 0 -o -name "*.orig" -exec rm -f {} \;
cat > find_provides.sh <<EOF
#!/bin/sh
/usr/lib/rpm/find-provides | grep -v GLIBC_PRIVATE
exit 0
EOF
chmod +x find_provides.sh
touch `find . -name configure`
touch locale/programs/*-kw.h

%build
GCC=gcc
GXX=g++
%ifarch %{ix86}
BuildFlags="-march=%{_target_cpu} -mtune=generic"
%endif
%ifarch i686
BuildFlags="-march=i686 -mtune=generic"
%endif
%ifarch i386 i486 i586
BuildFlags="$BuildFlags -mno-tls-direct-seg-refs"
%endif
%ifarch x86_64
BuildFlags="-mtune=generic"
%endif
%ifarch alphaev6
BuildFlags="-mcpu=ev6"
%endif
%ifarch sparc
BuildFlags="-fcall-used-g6"
GCC="gcc -m32"
GXX="g++ -m32"
%endif
%ifarch sparcv9
BuildFlags="-mcpu=ultrasparc -fcall-used-g6"
GCC="gcc -m32"
GXX="g++ -m32"
%endif
%ifarch sparcv9v
BuildFlags="-mcpu=niagara -fcall-used-g6"
GCC="gcc -m32"
GXX="g++ -m32"
%endif
%ifarch sparc64
BuildFlags="-mcpu=ultrasparc -mvis -fcall-used-g6"
GCC="gcc -m64"
GXX="g++ -m64"
%endif
%ifarch sparc64v
BuildFlags="-mcpu=niagara -mvis -fcall-used-g6"
GCC="gcc -m64"
GXX="g++ -m64"
%endif
%ifarch ppc64
BuildFlags="-mno-minimal-toc"
GCC="gcc -m64"
GXX="g++ -m64"
%endif

BuildFlags="$BuildFlags -fasynchronous-unwind-tables"
# Add -DNDEBUG unless using a prerelease
case %{version} in
  *.*.9[0-9]*) ;;
  *)
     BuildFlags="$BuildFlags -DNDEBUG"
     ;;
esac
EnableKernel="--enable-kernel=%{enablekernel}"
echo "$GCC" > Gcc
AddOns=`echo */configure | sed -e 's!/configure!!g;s!\(linuxthreads\|nptl\|rtkaio\|powerpc-cpu\)\( \|$\)!!g;s! \+$!!;s! !,!g;s!^!,!;/^,\*$/d'`
%ifarch %{rtkaioarches}
AddOns=,rtkaio$AddOns
%endif

build()
{
        builddir=build-%{target}${1:+-$1}
        ${1+shift}
        rm -rf $builddir
        mkdir $builddir
        pushd $builddir
        build_CFLAGS="$BuildFlags -g -O3 $*"
        # Some configure checks can spuriously fail for some architectures if
        # unwind info is present
        configure_CFLAGS="$build_CFLAGS -fno-asynchronous-unwind-tables"
        ../configure CC="$GCC" CXX="$GXX" CFLAGS="$configure_CFLAGS" \
                --prefix=%{_prefix} \
                --enable-add-ons=nptl$AddOns \
                --with-headers=%{_prefix}/include $EnableKernel --enable-bind-now \
                --build=%{target} \
%ifarch %{multiarcharches}
                --enable-multi-arch \
%endif
                --enable-obsolete-rpc \
%ifarch %{systemtaparches}
                --enable-systemtap \
%endif
%ifarch ppc64p7
                --with-cpu=power7 \
%endif
                --enable-lock-elision \
                --disable-profile --enable-nss-crypt ||
                { cat config.log; false; }

        make %{?_smp_mflags} -r CFLAGS="$build_CFLAGS" %{silentrules}
        popd
}

build

%if %{buildxen}
build nosegneg -mno-tls-direct-seg-refs
%endif

%if %{buildpower6}
(
platform=`LD_SHOW_AUXV=1 /bin/true | sed -n 's/^AT_PLATFORM:[[:blank:]]*//p'`
if [ "$platform" != power6 ]; then
  mkdir -p power6emul/{lib,lib64}
  $GCC -shared -O2 -fpic -o power6emul/%{_libdir}/power6emul.so fedora/power6emul.c -Wl,-z,initfirst
%ifarch ppc
  gcc -shared -nostdlib -O2 -fpic -m64 -o power6emul/lib64/power6emul.so -xc - </dev/null
%endif
%ifarch ppc64
  gcc -shared -nostdlib -O2 -fpic -m32 -o power6emul/lib/power6emul.so -xc - < /dev/null
%endif
  export LD_PRELOAD=`pwd`/power6emul/\$LIB/power6emul.so
fi
AddOns="$AddOns --with-cpu=power6"
GCC="$GCC -mcpu=power6"
GXX="$GXX -mcpu=power6"
build_nptl linuxnptl-power6
)
%endif

pushd build-%{target}
$GCC -static -L. -Os -g %{SOURCE2} \
        -o glibc_post_upgrade.%{_target_cpu} \
        '-DLIBTLS="/%{_libdir}/tls/"' \
        '-DGCONV_MODULES_DIR="%{_libdir}/gconv"' \
        '-DLD_SO_CONF="/etc/ld.so.conf"' \
        '-DICONVCONFIG="%{_sbindir}/iconvconfig.%{_target_cpu}"'
popd

%install
rm -rf --preserve-root $RPM_BUILD_ROOT

# Reload compiler and build options that were used during %%build.
GCC=`cat Gcc`

# Cleanup any previous installs...
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
make -j1 install_root=$RPM_BUILD_ROOT \
        install -C build-%{target} %{silentrules}
# If we are not building an auxiliary arch then install all of the supported
# locales.
%ifnarch %{auxarches}
pushd build-%{target}
make %{?_smp_mflags} install_root=$RPM_BUILD_ROOT \
        install-locales -C ../localedata objdir=`pwd`
popd
%endif

librtso=`basename $RPM_BUILD_ROOT/%{_libdir}/librt.so.*`

%ifarch %{rtkaioarches}
rm -f $RPM_BUILD_ROOT{,%{_prefix}}/%{_libdir}/librtkaio.*
rm -f $RPM_BUILD_ROOT%{_prefix}/%{_libdir}/librt.so.*
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/rtkaio
mv $RPM_BUILD_ROOT/%{_lib}/librtkaio-*.so $RPM_BUILD_ROOT/%{_libdir}/rtkaio/
rm -f $RPM_BUILD_ROOT/%{_lib}/$librtso
ln -sf `basename $RPM_BUILD_ROOT/%{_libdir}/librt-*.so` $RPM_BUILD_ROOT/%{_libdir}/$librtso
ln -sf `basename $RPM_BUILD_ROOT/%{_libdir}/rtkaio/librtkaio-*.so` $RPM_BUILD_ROOT/%{_libdir}/rtkaio/$librtso
%endif

%if %{buildxen}
%define nosegneg_subdir_base i686
%define nosegneg_subdir i686/nosegneg
%define nosegneg_subdir_up ../..
cd build-%{target}-nosegneg
destdir=$RPM_BUILD_ROOT/%{_libdir}/%{nosegneg_subdir}
mkdir -p $destdir
for lib in libc math/libm nptl/libpthread rt/librt nptl_db/libthread_db
do
  libbase=${lib#*/}
  libbaseso=$(basename $RPM_BUILD_ROOT/%{_libdir}/${libbase}-*.so)
  # Only install if different from base lib
  if cmp -s ${lib}.so ../build-%{nptl_target_cpu}-linuxnptl/${lib}.so; then
    ln -sf %{nosegneg_subdir_up}/$libbaseso $destdir/$libbaseso
  else
    cp -a ${lib}.so $destdir/$libbaseso
  fi
  ln -sf $libbaseso $destdir/$(basename $RPM_BUILD_ROOT/%{_libdir}/${libbase}.so.*)
done
%ifarch %{rtkaioarches}
destdir=$RPM_BUILD_ROOT/%{_libdir}/rtkaio/%{nosegneg_subdir}
mkdir -p $destdir
librtkaioso=$(basename $RPM_BUILD_ROOT/%{_libdir}/librt-*.so | sed s/librt-/librtkaio-/)
if cmp -s rtkaio/librtkaio.so ../build-%{nptl_target_cpu}-linuxnptl/rtkaio/librtkaio.so; then
  ln -s %{nosegneg_subdir_up}/$librtkaioso $destdir/$librtkaioso
else
  cp -a rtkaio/librtkaio.so $destdir/$librtkaioso
fi
ln -sf $librtkaioso $destdir/$librtso
%endif
cd ..
%endif

%if %{buildpower6}
cd build-%{target}-power6
destdir=$RPM_BUILD_ROOT/%{_libdir}/power6
mkdir -p ${destdir}
for lib in libc math/libm nptl/libpthread rt/librt nptl_db/libthread_db
do
  libbase=${lib#*/}
  libbaseso=$(basename $RPM_BUILD_ROOT/%{_libdir}/${libbase}-*.so)
  cp -a ${lib}.so $destdir/$libbaseso
  ln -sf $libbaseso $destdir/$(basename $RPM_BUILD_ROOT/%{_libdir}/${libbase}.so.*)
done
mkdir -p ${destdir}x
pushd ${destdir}x
ln -sf ../power6/*.so .
cp -a ../power6/*.so.* .
popd
%ifarch %{rtkaioarches}
destdir=$RPM_BUILD_ROOT/%{_libdir}/rtkaio/power6
mkdir -p $destdir
librtkaioso=$(basename $RPM_BUILD_ROOT/%{_libdir}/librt-*.so | sed s/librt-/librtkaio-/)
cp -a rtkaio/librtkaio.so $destdir/$librtkaioso
ln -sf $librtkaioso $destdir/$librtso
mkdir -p ${destdir}x
pushd ${destdir}x
ln -sf ../power6/*.so .
cp -a ../power6/*.so.* .
popd
%endif
cd ..
%endif

# Remove the files we don't want to distribute
rm -f $RPM_BUILD_ROOT%{_prefix}/%{_libdir}/libNoVersion*
rm -f $RPM_BUILD_ROOT/%{_libdir}/libNoVersion*

# NPTL <bits/stdio-lock.h> is not usable outside of glibc, so include
# the generic one (#162634)
cp -a bits/stdio-lock.h $RPM_BUILD_ROOT%{_prefix}/include/bits/stdio-lock.h
# And <bits/libc-lock.h> needs sanitizing as well.
cp -a %{SOURCE3} $RPM_BUILD_ROOT%{_prefix}/include/bits/libc-lock.h

if [ -d $RPM_BUILD_ROOT%{_prefix}/info -a "%{_infodir}" != "%{_prefix}/info" ]; then
  mkdir -p $RPM_BUILD_ROOT%{_infodir}
  mv -f $RPM_BUILD_ROOT%{_prefix}/info/* $RPM_BUILD_ROOT%{_infodir}
  rm -rf $RPM_BUILD_ROOT%{_prefix}/info
fi

bzip2 -9vf $RPM_BUILD_ROOT%{_infodir}/libc*

install -p -m 644 %{SOURCE7} $RPM_BUILD_ROOT/etc/nsswitch.conf

%ifnarch %{auxarches}
mkdir -p $RPM_BUILD_ROOT/etc/default
install -p -m 644 nis/nss $RPM_BUILD_ROOT/etc/default/nss
%endif

# Include ld.so.conf
echo 'include ld.so.conf.d/*.conf' > $RPM_BUILD_ROOT/etc/ld.so.conf
truncate -s 0 $RPM_BUILD_ROOT/etc/ld.so.cache
chmod 644 $RPM_BUILD_ROOT/etc/ld.so.conf
mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d
%ifnarch %{auxarches}
mkdir -p $RPM_BUILD_ROOT/etc/sysconfig
truncate -s 0 $RPM_BUILD_ROOT/etc/sysconfig/nscd
truncate -s 0 $RPM_BUILD_ROOT/etc/gai.conf
%endif

# Include %{_libdir}/gconv/gconv-modules.cache
truncate -s 0 $RPM_BUILD_ROOT%{_libdir}/gconv/gconv-modules.cache
chmod 644 $RPM_BUILD_ROOT%{_libdir}/gconv/gconv-modules.cache

# NPTL <bits/stdio-lock.h> is not usable outside of glibc, so include
# the generic one (#162634)
cp -a bits/stdio-lock.h $RPM_BUILD_ROOT%{_prefix}/include/bits/stdio-lock.h
# And <bits/libc-lock.h> needs sanitizing as well.
cp -a %{SOURCE3} $RPM_BUILD_ROOT%{_prefix}/include/bits/libc-lock.h

# XXX: What is this for?
ln -sf libbsd-compat.a $RPM_BUILD_ROOT%{_libdir}/libbsd.a

# Install the upgrade program
install -m 700 build-%{target}/glibc_post_upgrade.%{_target_cpu} \
  $RPM_BUILD_ROOT%{_prefix}/sbin/glibc_post_upgrade.%{_target_cpu}

# Strip all of the installed object files.
strip -g $RPM_BUILD_ROOT%{_libdir}/*.o

# XXX: Ugly hack for buggy rpm. What bug? BZ? Is this fixed?
ln -f ${RPM_BUILD_ROOT}%{_sbindir}/iconvconfig{,.%{_target_cpu}}

%if 0%{?_enable_debug_packages}
mkdir -p $RPM_BUILD_ROOT%{_prefix}/lib/debug%{_prefix}/%{_lib}
cp -a $RPM_BUILD_ROOT%{_prefix}/%{_lib}/*.a \
  $RPM_BUILD_ROOT%{_prefix}/lib/debug%{_prefix}/%{_lib}/
rm -f $RPM_BUILD_ROOT%{_prefix}/lib/debug%{_prefix}/%{_lib}/*_p.a
%endif

# rquota.x and rquota.h are now provided by quota
rm -f $RPM_BUILD_ROOT%{_prefix}/include/rpcsvc/rquota.[hx]

touch -r %{SOURCE2} $RPM_BUILD_ROOT/etc/ld.so.conf
touch -r sunrpc/etc.rpc $RPM_BUILD_ROOT/etc/rpc

pushd build-%{target}
$GCC -Os -g -o build-locale-archive %{SOURCE1} \
        ../build-%{target}/locale/locarchive.o \
        ../build-%{target}/locale/md5.o \
        -I. -DDATADIR=\"%{_datadir}\" -DPREFIX=\"%{_prefix}\" \
        -L../build-%{target} \
        -Wl,--allow-shlib-undefined \
        -B../build-%{target}/csu/ -lc -lc_nonshared
install -m 700 build-locale-archive $RPM_BUILD_ROOT%{_prefix}/sbin/build-locale-archive
popd

rm -f ${RPM_BUILD_ROOT}/%{_libdir}/libnss1-*
rm -f ${RPM_BUILD_ROOT}/%{_libdir}/libnss-*.so.1

# Ugly hack for buggy rpm
ln -f ${RPM_BUILD_ROOT}%{_sbindir}/iconvconfig{,.%{_target_cpu}}

# In F7+ this is provided by rpcbind rpm
rm -f $RPM_BUILD_ROOT%{_sbindir}/rpcinfo

##############################################################################
# Install info files
##############################################################################

# Move the info files if glibc installed them into the wrong location.
if [ -d $RPM_BUILD_ROOT%{_prefix}/info -a "%{_infodir}" != "%{_prefix}/info" ]; then
  mkdir -p $RPM_BUILD_ROOT%{_infodir}
  mv -f $RPM_BUILD_ROOT%{_prefix}/info/* $RPM_BUILD_ROOT%{_infodir}
  rm -rf $RPM_BUILD_ROOT%{_prefix}/info
fi

# Compress all of the info files.
#gzip -9nvf $RPM_BUILD_ROOT%{_infodir}/libc*

##############################################################################
# Install locale files
##############################################################################

# Create archive of locale files
%ifnarch %{auxarches}
olddir=`pwd`
pushd ${RPM_BUILD_ROOT}%{_prefix}/lib/locale
rm -f locale-archive
# Intentionally we do not pass --alias-file=, aliases will be added
# by build-locale-archive.
$olddir/build-%{target}/elf/ld.so \
        --library-path $olddir/build-%{target}/ \
        $olddir/build-%{target}/locale/localedef \
        --prefix ${RPM_BUILD_ROOT} --add-to-archive \
        *_*
rm -rf *_*
mv locale-archive{,.tmpl}
popd
%endif

##############################################################################
# Install configuration files for services
##############################################################################

install -p -m 644 %{SOURCE7} $RPM_BUILD_ROOT/etc/nsswitch.conf

%ifnarch %{auxarches}
mkdir -p $RPM_BUILD_ROOT/etc/default
install -p -m 644 nis/nss $RPM_BUILD_ROOT/etc/default/nss

# This is for ncsd - in glibc 2.2
install -m 644 nscd/nscd.conf $RPM_BUILD_ROOT/etc
mkdir -p $RPM_BUILD_ROOT%{_tmpfilesdir}
install -m 644 %{SOURCE4} %{buildroot}%{_tmpfilesdir}
mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/system
install -m 644 nscd/nscd.service nscd/nscd.socket $RPM_BUILD_ROOT/usr/lib/systemd/system
%endif

# Include ld.so.conf
echo 'include ld.so.conf.d/*.conf' > $RPM_BUILD_ROOT/etc/ld.so.conf
truncate -s 0 $RPM_BUILD_ROOT/etc/ld.so.cache
chmod 644 $RPM_BUILD_ROOT/etc/ld.so.conf
mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d
%ifnarch %{auxarches}
mkdir -p $RPM_BUILD_ROOT/etc/sysconfig
truncate -s 0 $RPM_BUILD_ROOT/etc/sysconfig/nscd
truncate -s 0 $RPM_BUILD_ROOT/etc/gai.conf
%endif

# Include %{_libdir}/gconv/gconv-modules.cache
truncate -s 0 $RPM_BUILD_ROOT%{_libdir}/gconv/gconv-modules.cache
chmod 644 $RPM_BUILD_ROOT%{_libdir}/gconv/gconv-modules.cache

##############################################################################
# Misc...
##############################################################################

# NPTL <bits/stdio-lock.h> is not usable outside of glibc, so include
# the generic one (#162634)
cp -a bits/stdio-lock.h $RPM_BUILD_ROOT%{_prefix}/include/bits/stdio-lock.h
# And <bits/libc-lock.h> needs sanitizing as well.
cp -a %{SOURCE3} $RPM_BUILD_ROOT%{_prefix}/include/bits/libc-lock.h

# XXX: What is this for?
ln -sf libbsd-compat.a $RPM_BUILD_ROOT%{_libdir}/libbsd.a

# Install the upgrade program
install -m 700 build-%{target}/glibc_post_upgrade.%{_target_cpu} \
  $RPM_BUILD_ROOT%{_prefix}/sbin/glibc_post_upgrade.%{_target_cpu}

# Strip all of the installed object files.
strip -g $RPM_BUILD_ROOT%{_libdir}/*.o

# XXX: Ugly hack for buggy rpm. What bug? BZ? Is this fixed?
ln -f ${RPM_BUILD_ROOT}%{_sbindir}/iconvconfig{,.%{_target_cpu}}

##############################################################################
# Install debug copies of unstripped static libraries
# - This step must be last in order to capture any additional static
#   archives we might have added.
##############################################################################

# If we are building a debug package then copy all of the static archives
# into the debug directory to keep them as unstripped copies.
%if 0%{?_enable_debug_packages}
mkdir -p $RPM_BUILD_ROOT%{_prefix}/lib/debug%{_libdir}
cp -a $RPM_BUILD_ROOT%{_libdir}/*.a \
        $RPM_BUILD_ROOT%{_prefix}/lib/debug%{_libdir}/
rm -f $RPM_BUILD_ROOT%{_prefix}/lib/debug%{_libdir}/*_p.a
%endif

##############################################################################
# Build the file lists used for describing the package and subpackages.
##############################################################################
# There are 11 file lists:
# * rpm.fileslist
#       - Master file list. Eventually, after removing files from this list
#         we are left with the list of files for the glibc package.
# * common.filelist
#       - Contains the list of flies for the common subpackage.
# * utils.filelist
#       - Contains the list of files for the utils subpackage.
# * nscd.filelist
#       - Contains the list of files for the nscd subpackage.
# * devel.filelist
#       - Contains the list of files for the devel subpackage.
# * headers.filelist
#       - Contains the list of files for the headers subpackage.
# * static.filelist
#       - Contains the list of files for the static subpackage.
# * nosegneg.filelist
#       - Contains the list of files for the xen subpackage.
# * debuginfo.filelist
#       - Contains the list of files for the glibc debuginfo package.
# * debuginfocommon.filelist
#       - Contains the list of files for the glibc common debuginfo package.
#

{
  find $RPM_BUILD_ROOT \( -type f -o -type l \) \
       \( \
         -name etc -printf "%%%%config " -o \
         -name gconv-modules \
         -printf "%%%%verify(not md5 size mtime) %%%%config(noreplace) " -o \
         -name gconv-modules.cache \
         -printf "%%%%verify(not md5 size mtime) " \
         , \
         ! -path "*/lib/debug/*" -printf "/%%P\n" \)
  find $RPM_BUILD_ROOT -type d \
       \( -path '*%{_prefix}/share/*' ! -path '*%{_infodir}' -o \
          -path "*%{_prefix}/include/*" \
       \) -printf "%%%%dir /%%P\n"
} | {

  # primary filelist
  SHARE_LANG='s|.*/share/locale/\([^/_]\+\).*/LC_MESSAGES/.*\.mo|%lang(\1) &|'
  LIB_LANG='s|.*/lib/locale/\([^/_]\+\)|%lang(\1) &|'
  # rpm does not handle %lang() tagged files hardlinked together accross
  # languages very well, temporarily disable
  LIB_LANG=''
  sed -e "$LIB_LANG" -e "$SHARE_LANG" \
      -e '\,/etc/\(localtime\|nsswitch.conf\|ld\.so\.conf\|ld\.so\.cache\|default\|rpc\|gai\.conf\),d' \
      -e '\,/%{_lib}/lib\(pcprofile\|memusage\)\.so,d' \
      -e '\,bin/\(memusage\|mtrace\|xtrace\|pcprofiledump\),d'
} | sort > rpm.filelist

mkdir -p $RPM_BUILD_ROOT%{_libdir}
mv -f $RPM_BUILD_ROOT/%{_lib}/lib{pcprofile,memusage}.so $RPM_BUILD_ROOT%{_libdir}

# The xtrace and memusage scripts have hard-coded paths that need to be
# translated to a correct set of paths using the $LIB token which is
# dynamically translated by ld.so as the default lib directory.
for i in $RPM_BUILD_ROOT%{_prefix}/bin/{xtrace,memusage}; do
  sed -e 's~=/%{_lib}/libpcprofile.so~=%{_libdir}/libpcprofile.so~' \
      -e 's~=/%{_lib}/libmemusage.so~=%{_libdir}/libmemusage.so~' \
      -e 's~='\''/\\\$LIB/libpcprofile.so~='\''%{_prefix}/\\$LIB/libpcprofile.so~' \
      -e 's~='\''/\\\$LIB/libmemusage.so~='\''%{_prefix}/\\$LIB/libmemusage.so~' \
      -i $i
done

# Put the info files into the devel file list.
grep '%{_infodir}' < rpm.filelist | grep -v '%{_infodir}/dir' > devel.filelist

# Put the stub headers into the devel file list.
grep '%{_prefix}/include/gnu/stubs-[32164]\+\.h' < rpm.filelist >> devel.filelist || :

# Put the include files into headers file list.
grep '%{_prefix}/include' < rpm.filelist |
        egrep -v '%{_prefix}/include/(linuxthreads|gnu/stubs-[32164]+\.h)' \
        > headers.filelist

# Remove partial (lib*_p.a) static libraries, include files, and info files from
# the core glibc package.
sed -i -e '\|%{_libdir}/lib.*_p.a|d' \
       -e '\|%{_prefix}/include|d' \
       -e '\|%{_infodir}|d' rpm.filelist

# Put some static files into the devel package.
grep '%{_libdir}/lib.*\.a' < rpm.filelist \
  | grep '/lib\(\(c\|pthread\|nldbl\)_nonshared\|bsd\(\|-compat\)\|g\|ieee\|mcheck\|rpcsvc\)\.a$' \
  >> devel.filelist

# Put the rest of the static files into the static package.
grep '%{_libdir}/lib.*\.a' < rpm.filelist \
  | grep -v '/lib\(\(c\|pthread\|nldbl\)_nonshared\|bsd\(\|-compat\)\|g\|ieee\|mcheck\|rpcsvc\)\.a$' \
  > static.filelist

# Put all of the object files and *.so (not the versioned ones) into the
# devel package.
grep '%{_libdir}/.*\.o' < rpm.filelist >> devel.filelist
grep '%{_libdir}/lib.*\.so' < rpm.filelist >> devel.filelist

# Remove all of the static, object, unversioned DSOs, old linuxthreads stuff,
# and nscd from the core glibc package.
sed -i -e '\|%{_libdir}/lib.*\.a|d' \
       -e '\|%{_libdir}/.*\.o|d' \
       -e '\|%{_libdir}/lib.*\.so|d' \
       -e '\|%{_libdir}/linuxthreads|d' \
       -e '\|nscd|d' rpm.filelist

# All of the bin and certain sbin files go into the common package.
# We explicitly exclude certain sbin files that need to go into
# the core glibc package for use during upgrades.
grep '%{_prefix}/bin' < rpm.filelist >> common.filelist
grep '%{_prefix}/sbin/[^gi]' < rpm.filelist >> common.filelist
# All of the files under share go into the common package since
# they should be multilib-independent.
grep '%{_prefix}/share' < rpm.filelist | \
  grep -v -e '%{_prefix}/share/zoneinfo' -e '%%dir %{prefix}/share' \
       >> common.filelist

# Remove the bin, locale, some sbin, and share from the
# core glibc package. We cheat a bit and use the slightly dangerous
# /usr/sbin/[^gi] to match the inverse of the search that put the
# files into common.filelist. It's dangerous in that additional files
# that start with g, or i would get put into common.filelist and
# rpm.filelist.
sed -i -e '\|%{_prefix}/bin|d' \
       -e '\|%{_prefix}/lib/locale|d' \
       -e '\|%{_prefix}/sbin/[^gi]|d' \
       -e '\|%{_prefix}/share|d' rpm.filelist


# Add the binary to build localse to the common subpackage.
echo '%{_prefix}/sbin/build-locale-archive' >> common.filelist

# The nscd binary must go into the nscd subpackage.
echo '%{_prefix}/sbin/nscd' > nscd.filelist

# The memusage and pcprofile libraries are put back into the core
# glibc package even though they are only used by utils package
# scripts..
cat >> rpm.filelist <<EOF
%{_libdir}/libmemusage.so
%{_libdir}/libpcprofile.so
EOF

# Add the utils scripts and programs to the utils subpackage.
cat > utils.filelist <<EOF
%{_prefix}/bin/memusage
%{_prefix}/bin/memusagestat
%{_prefix}/bin/mtrace
%{_prefix}/bin/pcprofiledump
%{_prefix}/bin/xtrace
EOF

rm -rf $RPM_BUILD_ROOT%{_prefix}/share/zoneinfo

# Make sure %config files have the same timestamp
touch -r %{SOURCE2} $RPM_BUILD_ROOT/etc/ld.so.conf
touch -r sunrpc/etc.rpc $RPM_BUILD_ROOT/etc/rpc

# We allow undefined symbols in shared libraries because the libraries
# referenced at link time here, particularly ld.so, may be different than
# the one used at runtime.  This is really only needed during the ARM
# transition from ld-linux.so.3 to ld-linux-armhf.so.3.
pushd build-%{target}
$GCC -Os -g -o build-locale-archive %{SOURCE1} \
	../build-%{target}/locale/locarchive.o \
	../build-%{target}/locale/md5.o \
	-I. -DDATADIR=\"%{_datadir}\" -DPREFIX=\"%{_prefix}\" \
	-L../build-%{target} \
	-Wl,--allow-shlib-undefined \
	-B../build-%{target}/csu/ -lc -lc_nonshared
install -m 700 build-locale-archive $RPM_BUILD_ROOT%{_prefix}/sbin/build-locale-archive
popd

# the last bit: more documentation
rm -rf documentation
mkdir documentation
cp crypt/README.ufc-crypt documentation/README.ufc-crypt
cp timezone/README documentation/README.timezone
cp ChangeLog{,.15,.16} documentation
bzip2 -9 documentation/ChangeLog*
cp posix/gai.conf documentation/

%ifarch s390x
# Compatibility symlink
mkdir -p $RPM_BUILD_ROOT/lib
ln -sf /%{_libdir}/ld64.so.1 $RPM_BUILD_ROOT/lib/ld64.so.1
%endif

# Leave a compatibility symlink for the dynamic loader on armhfp targets,
# at least until the world gets rebuilt
%ifarch armv7hl armv7hnl
ln -sf /lib/ld-linux-armhf.so.3 $RPM_BUILD_ROOT/lib/ld-linux.so.3
%endif

# Increase timeouts
%if %{run_glibc_tests}

# Increase timeouts
export TIMEOUTFACTOR=16
parent=$$
echo ====================TESTING=========================
cd build-%{target}
( make %{?_smp_mflags} -k check %{silentrules} 2>&1
  sleep 10s
  teepid="`ps -eo ppid,pid,command | awk '($1 == '${parent}' && $3 ~ /^tee/) { print $2 }'`"
  [ -n "$teepid" ] && kill $teepid
) | tee check.log || :
find . -name '*.test-result' | xargs cat;
cd ..
%if %{buildxen}
echo ====================TESTING -mno-tls-direct-seg-refs=============
cd build-%{target}-nosegneg
( make %{?_smp_mflags} -k check %{silentrules} 2>&1
  sleep 10s
  teepid="`ps -eo ppid,pid,command | awk '($1 == '${parent}' && $3 ~ /^tee/) { print $2 }'`"
  [ -n "$teepid" ] && kill $teepid
) | tee check.log || :
find . -name '*.test-result' | xargs cat;
cd ..
%endif
%if %{buildpower6}
echo ====================TESTING -mcpu=power6=============
cd build-%{target}-power6
( if [ -d ../power6emul ]; then
    export LD_PRELOAD=`cd ../power6emul; pwd`/\$LIB/power6emul.so
  fi
  make %{?_smp_mflags} -k check %{silentrules} 2>&1
  sleep 10s
  teepid="`ps -eo ppid,pid,command | awk '($1 == '${parent}' && $3 ~ /^tee/) { print $2 }'`"
  [ -n "$teepid" ] && kill $teepid
) | tee check.log || :
find . -name '*.test-result' | xargs cat;
cd ..
%endif
echo ====================TESTING DETAILS=================
for i in `sed -n 's|^.*\*\*\* \[\([^]]*\.out\)\].*$|\1|p' build-*-linux*/check.log`; do
  echo =====$i=====
  cat $i || :
  echo ============
done
echo ====================TESTING END=====================
PLTCMD='/^Relocation section .*\(\.rela\?\.plt\|\.rela\.IA_64\.pltoff\)/,/^$/p'
echo ====================PLT RELOCS LD.SO================
readelf -Wr $RPM_BUILD_ROOT/%{_libdir}/ld-*.so | sed -n -e "$PLTCMD"
echo ====================PLT RELOCS LIBC.SO==============
readelf -Wr $RPM_BUILD_ROOT/%{_libdir}/libc-*.so | sed -n -e "$PLTCMD"
echo ====================PLT RELOCS END==================

%endif

pushd $RPM_BUILD_ROOT/%{_libdir}/
$GCC -r -nostdlib -o libpthread.o -Wl,--whole-archive ./libpthread.a
rm libpthread.a
ar rcs libpthread.a libpthread.o
rm libpthread.o
popd

%if 0%{?_enable_debug_packages}


# FIXME 
rm -rf $RPM_BUILD_ROOT/usr/src/debug/$RPM_BUILD_ROOT

# The #line directives gperf generates do not give the proper
# file name relative to the build directory.
pushd locale
ln -s programs/*.gperf .
popd
pushd iconv
ln -s ../locale/programs/charmap-kw.gperf .
popd

# Print some diagnostic information in the builds about the
# getconf binaries.
# XXX: Why do we do this?
ls -l $RPM_BUILD_ROOT%{_prefix}/bin/getconf
ls -l $RPM_BUILD_ROOT%{_prefix}/libexec/getconf
eu-readelf -hS $RPM_BUILD_ROOT%{_prefix}/bin/getconf \
        $RPM_BUILD_ROOT%{_prefix}/libexec/getconf/*

find_debuginfo_args='--strict-build-id -g'
%ifarch %{debuginfocommonarches}
find_debuginfo_args="$find_debuginfo_args \
        -l common.filelist \
        -l utils.filelist \
        -l nscd.filelist \
        -p '.*/(sbin|libexec)/.*' \
        -o debuginfocommon.filelist \
        -l rpm.filelist \
        -l nosegneg.filelist"
%endif
eval /usr/lib/rpm/find-debuginfo.sh \
        "$find_debuginfo_args" \
        -o debuginfo.filelist

# List all of the *.a archives in the debug directory.
list_debug_archives()
{
        local dir=%{_prefix}/lib/debug%{_libdir}
        find $RPM_BUILD_ROOT$dir -name "*.a" -printf "$dir/%%P\n"
}

%ifarch %{debuginfocommonarches}

# Remove the source files from the common package debuginfo.
sed -i '\#^%{_prefix}/src/debug/#d' debuginfocommon.filelist

# Create a list of all of the source files we copied to the debug directory.
find $RPM_BUILD_ROOT%{_prefix}/src/debug \
     \( -type d -printf '%%%%dir ' \) , \
     -printf '%{_prefix}/src/debug/%%P\n' > debuginfocommon.sources

%ifarch %{biarcharches}

# Add the source files to the core debuginfo package.
cat debuginfocommon.sources >> debuginfo.filelist

%else

%ifarch %{ix86}
%define basearch i686
%endif
%ifarch sparc sparcv9
%define basearch sparc
%endif

# The auxarches get only these few source files.
auxarches_debugsources=\
'/(generic|linux|%{basearch}|nptl(_db)?)/|/%{glibcsrcdir}/build|/dl-osinfo\.h'

# Place the source files into the core debuginfo pakcage.
egrep "$auxarches_debugsources" debuginfocommon.sources >> debuginfo.filelist

# Remove the source files from the common debuginfo package.
egrep -v "$auxarches_debugsources" \
  debuginfocommon.sources >> debuginfocommon.filelist

%endif # %{biarcharches}

# Add the list of *.a archives in the debug directory to
# the common debuginfo package.
list_debug_archives >> debuginfocommon.filelist

# It happens that find-debuginfo.sh produces duplicate entries even
# though the inputs are unique. Therefore we sort and unique the
# entries in the debug file lists. This avoids the following warnings:
# ~~~
# Processing files: glibc-debuginfo-common-2.17.90-10.fc20.x86_64
# warning: File listed twice: /usr/lib/debug/usr/sbin/build-locale-archive.debug
# warning: File listed twice: /usr/lib/debug/usr/sbin/nscd.debug
# warning: File listed twice: /usr/lib/debug/usr/sbin/zdump.debug
# warning: File listed twice: /usr/lib/debug/usr/sbin/zic.debug
# ~~~
sort -u debuginfocommon.filelist > debuginfocommon2.filelist
mv debuginfocommon2.filelist debuginfocommon.filelist

%endif # %{debuginfocommonarches}

# Remove any duplicates output by a buggy find-debuginfo.sh.
sort -u debuginfo.filelist > debuginfo2.filelist
mv debuginfo2.filelist debuginfo.filelist

%endif # 0%{?_enable_debug_packages}

# Remove the `dir' info-heirarchy file which will be maintained
# by the system as it adds info files to the install.
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

%ifarch %{auxarches}

# Delete files that we do not intended to ship with the auxarch.
echo Cutting down the list of unpackaged files
sed -e '/%%dir/d;/%%config/d;/%%verify/d;s/%%lang([^)]*) //;s#^/*##' \
        common.filelist devel.filelist static.filelist headers.filelist \
        utils.filelist nscd.filelist \
%ifarch %{debuginfocommonarches}
        debuginfocommon.filelist \
%endif
        | (cd $RPM_BUILD_ROOT; xargs --no-run-if-empty rm -f 2> /dev/null || :)

%else

mkdir -p $RPM_BUILD_ROOT/var/{db,run}/nscd
touch $RPM_BUILD_ROOT/var/{db,run}/nscd/{passwd,group,hosts,services}
touch $RPM_BUILD_ROOT/var/run/nscd/{socket,nscd.pid}

%endif # %{auxarches}

%ifnarch %{auxarches}
truncate -s 0 $RPM_BUILD_ROOT/%{_prefix}/lib/locale/locale-archive
%endif

mkdir -p $RPM_BUILD_ROOT/var/cache/ldconfig
truncate -s 0 $RPM_BUILD_ROOT/var/cache/ldconfig/aux-cache


#%pre -p <lua>
#-- Check that the running kernel is new enough
#required = '%{enablekernel}'
#rel = posix.uname("%r")
#if rpm.vercmp(rel, required) < 0 then
#  error("FATAL: kernel too old", 0)
#end

%post -p /usr/sbin/glibc_post_upgrade.%{_target_cpu}

%postun -p /sbin/ldconfig

%triggerin common -p <lua> -- glibc
if posix.stat("%{_prefix}/lib/locale/locale-archive.tmpl", "size") > 0 then
  pid = posix.fork()
  if pid == 0 then
    posix.exec("%{_prefix}/sbin/build-locale-archive")
  elseif pid > 0 then
    posix.wait(pid)
  end
end

%post common -p <lua>
if posix.access("/etc/ld.so.cache") then
  if posix.stat("%{_prefix}/lib/locale/locale-archive.tmpl", "size") > 0 then
    pid = posix.fork()
    if pid == 0 then
      posix.exec("%{_prefix}/sbin/build-locale-archive")
    elseif pid > 0 then
      posix.wait(pid)
    end
  end
end

%triggerin common -p <lua> -- tzdata
function update (filename, new_data)
  local fd = io.open(filename)
  if not fd then return end
  local data = fd:read("*a")
  fd:close()
  if not data then return end
  -- Don't update the file unnecessarily.
  if data == new_data then return end
  local tempfilename = filename .. ".tzupdate"
  fd = io.open(tempfilename, "w")
  if not fd then return end
  fd:write(new_data)
  fd:close()
  posix.chmod(tempfilename, 0644)
  if not os.rename(tempfilename, filename) then
    os.remove(tempfilename)
  end
end
fd = io.open("/etc/sysconfig/clock")
if not fd then return end
zonename = nil
for l in fd:lines() do
  zone = string.match(l, "^[ \t]*ZONE[ \t]*=[ \t]*\"?([^ \t\n\"]*)");
  if zone then
    zonename = "/usr/share/zoneinfo/" .. zone
    break
  end
end
fd:close()
if not zonename then return end
fd = io.open(zonename)
if not fd then return end
data = fd:read("*a")
fd:close()
if not data then return end
update("/etc/localtime", data)
update("/var/spool/postfix/etc/localtime", data)

%post devel
/sbin/install-info %{_infodir}/libc.info %{_infodir}/dir > /dev/null 2>&1 || :

%pre headers
# this used to be a link and it is causing nightmares now
if [ -L %{_prefix}/include/scsi ] ; then
  rm -f %{_prefix}/include/scsi
fi

%preun devel
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/libc.info %{_infodir}/dir > /dev/null 2>&1 || :
fi

%post utils -p /sbin/ldconfig

%postun utils -p /sbin/ldconfig

%pre -n nscd
getent group nscd >/dev/null || /usr/sbin/groupadd -g 28 -r nscd
getent passwd nscd >/dev/null ||
  /usr/sbin/useradd -M -o -r -d / -s /sbin/nologin \
		    -c "NSCD Daemon" -u 28 -g nscd nscd

%post -n nscd
if test $1 -eq 1; then
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun -n nscd
if test $1 -eq 0; then
  /bin/systemctl --no-reload disable nscd.service > /dev/null 2>&1 || :
  /bin/systemctl stop nscd.service > /dev/null 2>&1 || :
fi

%postun -n nscd
if test $1 = 0; then
  /usr/sbin/userdel nscd > /dev/null 2>&1 || :
fi
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if test $1 -ge 1; then
  /bin/systemctl try-restart nscd.service >/dev/null 2>&1 || :
fi

%if %{xenpackage}
%post xen -p /sbin/ldconfig
%postun xen -p /sbin/ldconfig
%endif

%clean
rm -rf "$RPM_BUILD_ROOT"
rm -f *.filelist*

%files -f rpm.filelist
%defattr(-,root,root)
%ifarch %{rtkaioarches}
%dir /%{_libdir}/rtkaio
%endif
%if %{buildxen} && !%{xenpackage}
%dir /%{_libdir}/%{nosegneg_subdir_base}
%dir /%{_libdir}/%{nosegneg_subdir}
%ifarch %{rtkaioarches}
%dir /%{_libdir}/rtkaio/%{nosegneg_subdir_base}
%dir /%{_libdir}/rtkaio/%{nosegneg_subdir}
%endif
%endif
%if %{buildpower6}
%dir /%{_libdir}/power6
%dir /%{_libdir}/power6x
%ifarch %{rtkaioarches}
%dir /%{_libdir}/rtkaio/power6
%dir /%{_libdir}/rtkaio/power6x
%endif
%endif
%ifarch s390x
/lib/ld64.so.1
%endif
%ifarch ia64
%if "%{_lib}" == "lib64"
/lib/ld-linux-ia64.so.2
%endif
%endif
#%%verify(not md5 size mtime) %config(noreplace) /etc/localtime
%verify(not md5 size mtime) %config(noreplace) /etc/nsswitch.conf
%verify(not md5 size mtime) %config(noreplace) /etc/ld.so.conf
%verify(not md5 size mtime) %config(noreplace) /etc/rpc
%dir /etc/ld.so.conf.d
%dir %{_prefix}/libexec/getconf
%dir %{_prefix}/%{_lib}/gconv
%dir %attr(0700,root,root) /var/cache/ldconfig
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/cache/ldconfig/aux-cache
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /etc/ld.so.cache
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /etc/gai.conf
%doc README NEWS INSTALL BUGS PROJECTS CONFORMANCE
%doc COPYING COPYING.LIB LICENSES
%doc hesiod/README.hesiod

%if %{xenpackage}
%files -f nosegneg.filelist xen
%defattr(-,root,root)
%dir /%{_libdir}/%{nosegneg_subdir_base}
%dir /%{_libdir}/%{nosegneg_subdir}
%endif

%ifnarch %{auxarches}
%files -f common.filelist common
%defattr(-,root,root)
%attr(0644,root,root) %verify(not md5 size mtime) %{_prefix}/lib/locale/locale-archive.tmpl
%attr(0644,root,root) %verify(not md5 size mtime mode) %ghost %config(missingok,noreplace) %{_prefix}/lib/locale/locale-archive
%dir %attr(755,root,root) /etc/default
%verify(not md5 size mtime) %config(noreplace) /etc/default/nss
%doc documentation/*
%exclude %dir %{_datadir}/locale
%exclude %dir %{_datadir}/locale/??
%exclude %dir %{_datadir}/locale/??_??
%exclude %dir %{_datadir}/locale/??/LC_MESSAGES
%exclude %dir %{_datadir}/locale/??_??/LC_MESSAGES

%files -f devel.filelist devel
%defattr(-,root,root)
%{_infodir}/libc.info*

%files -f static.filelist static
%defattr(-,root,root)

%files -f headers.filelist headers
%defattr(-,root,root)

%files -f utils.filelist utils
%defattr(-,root,root)

%files -f nscd.filelist -n nscd
%defattr(-,root,root)
%config(noreplace) /etc/nscd.conf
%{_unitdir}/nscd.service
%{_unitdir}/nscd.socket
/usr/lib/tmpfiles.d/nscd.conf
%dir %attr(0755,root,root) /var/run/nscd
%dir %attr(0755,root,root) /var/db/nscd
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/nscd.pid
%attr(0666,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/socket
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/passwd
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/group
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/hosts
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/services
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/db/nscd/passwd
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/db/nscd/group
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/db/nscd/hosts
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/db/nscd/services
%ghost %config(missingok,noreplace) /etc/sysconfig/nscd
%endif

%if 0%{?_enable_debug_packages}
%files debuginfo -f debuginfo.filelist
%defattr(-,root,root)
%exclude %dir %{_prefix}/lib/debug
%exclude %dir %{_prefix}/lib/debug/.build-id
%exclude %dir %{_prefix}/lib/debug/.build-id/??
%exclude %dir %{_prefix}/lib/debug%{_prefix}
%exclude %dir %{_prefix}/lib/debug%{_libdir}
%exclude %dir %{_prefix}/src/debug
%ifarch %{debuginfocommonarches}
%ifnarch %{auxarches}
%files debuginfo-common -f debuginfocommon.filelist
%defattr(-,root,root)
%endif
%endif
%endif

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.19.90-2m)
- update 2.19-564-g14642b8

* Sat Mar 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.19.90-1m)
- update 2.19.90

* Thu Jan 30 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.90-4m)
- fix locale archive. see BTS #494.

* Tue Jan 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.90-3m)
- add glibc-2.18-332-810.patch.xz

* Thu Nov 28 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.90-2m)
- [Momonga-devel.ja:05066] increase release

* Thu Nov 14 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.90-1m)
- update 2.18.90 

* Wed Jul 24 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.17.90-3m)
- revive rpc.h

* Wed Jul 24 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.17.90-2m)
- fix build on i686

* Wed Jul 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.17.90-1m)
- update 2.18-pre
-- support Haswell Transaction Memory

* Sun Sep  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15-8m)
- import fedora patch

* Thu Apr 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15-7m)
- import fedora patch

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15-6m)
- import patch
- remove nscd SOURCES. use glibc include files

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15-5m)
- import patch

* Mon Feb 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15-4m)
- revert glibc-rh769421.patch
-- this patch caused any program
- import fedora glibc patches

* Wed Feb 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15-3m)
- import patch

* Sat Feb  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15-2m)
- import patch

* Tue Jan  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15-1m)
- update 2.15

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90.999-7m)
- update glibc-2.14-394

* Fri Nov 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90.999-6m)
- update glibc-2.14-518

* Mon Nov 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90.999-5m)
- update glibc-2.14-498-g98591e5

* Fri Nov 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90.999-4m)
- update glibc-2.14-418-gb2ea1df

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90.999-3m)
- add memcpy_32_small_sizes.patch

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90.999-2m)
- add glibc-no-leaf-attribute.patch

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90.999-1m)
- update glibc-2.14-379-g49a43d8

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90-3m)
- update glibc-2.14-346-ga843a20

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90-2m)
- add patch
-- https://bugzilla.redhat.com/show_bug.cgi?id=737223
- Obsoletes nss_db

* Wed Oct  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.90-1m)
- update 2.14.90
- add systemd service.
- add tmpfiles.d file

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14-4m)
- release a directory provided by filesystem

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14-3m)
- import fedora 2.14-4

* Mon Jun 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14-2m)
- import fedora 2.14-3

* Thu Jun  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14-1m)
- update 2.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.90-7m)
- rebuild for new GCC 4.6

* Thu Mar 10 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.90-6m)
- fix up, sorry

* Wed Mar  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.90-5m)
- touch up

* Sun Feb 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.90-4m)
- merge fixes from glibc-2.13.90-4 of fedora 15

* Sat Feb 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.90-3m)
- fix debuginfo package

* Thu Feb 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.90-2m)
- fix elfutils issue

* Thu Feb 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.90-1m)
- update 2.13
- enable debuginfo packages for valgrind and gdb

* Wed Dec 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.2-1m)
- update 2.12.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.1-3m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.1-2m)
- fix build failure; import a patch from gentoo
-- see http://bugs.gentoo.org/show_bug.cgi?id=331995

* Tue Nov  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.1-1m)
- update 2.12.1 (fedora-2.12.1-4)
- [SECURITY] CVE-2010-3847, CVE-2010-3856

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12-5m)
- full rebuild for mo7 release

* Fri Jul 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12-4m)
- merge the following Fedora 13 changes
-- * Tue Jul  6 2010 Andreas Schwab <schwab@redhat.com> - 2.12-3
-- - Update from 2.12 branch
--  - Fix use of extend_alloca in NIS
--  - Fix a couple of __REDIRECT () __THROW occurrences
-- - Workaround assembler bug sneaking in nopl (#579838) 
- import RHEL6 beta2 refresh patches
-- * Tue Jun 29 2010 Andreas Schwab <schwab@redhat.com> - 2.12-1.4
-- - Fix setxid race handing exiting threads (#607461)
-- 
-- * Tue Jun  1 2010 Andreas Schwab <schwab@redhat.com> - 2.12-1.3
-- - Avoid strict aliasing issues (#594194)
-- - Fix scope handling during dl_close (#593686)

* Thu Jun 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12-3m)
- merge the following Fedora 13 changes
-- * Tue Jun  1 2010 Andreas Schwab <schwab@redhat.com> - 2.12-2
-- - Update from 2.12 branch
--   - Correct x86 CPU family and model check (BZ#11640, #596554)
--   - Don't crash on unresolved weak symbol reference
--   - Implement recvmmsg also as socketcall
--   - sunrpc: Fix spurious fall-through
--   - Make <sys/timex.h> compatible with C++ (#593762)
--   - Enable IDN support in getent
--   - Fix race in free sanity check (#594784)
--   - Fix lookup of collation sequence value during regexp matching
--   - Fix name of tt_RU.UTF-8@iqtelif locale (#589138)
--   - Handle too-small buffers in Linux getlogin_r (BZ#11571, #589946)
-- - Fix users and groups creation in nscd %%post script
-- - Require coreutils instead of sh-utils
-- - Fix typo causing missing directory ownership

* Sun May 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12-2m)
- fix pthread debugging issue

* Thu May  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12-1m)
- sync with Fedora 13 (2.12-1)

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.90-4m)
- sync with Fedora 13 (2.11.90-20)

* Sat Mar 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.90-3m)
- import fixes from Fedora 13 (2.11.90-16)
-- fix bug in memcmp
   see https://bugzilla.redhat.com/show_bug.cgi?id=574210

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.90-2m)
- sync with Fedora 13 (2.11.90-14)
- BuildRequires: libselinux-devel >= 2.0.91-1m

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.90-1m)
- sync with Fedora 13 (2.11.90-12)

* Wed Feb  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.1-1m)
- update 2.11.1 merge from Fedora glibc-2.11.1

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11-3m)
- rebuild against audit-2.0.4

* Wed Nov 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11-2m)
- sync with Fedora 12
-- * Thu Nov  5 2009 Andreas Schwab <schwab@redhat.com> - 2.11-2
-- - Fix readahead on powerpc32.
-- - Fix R_PPC64_{JMP_IREL,IRELATIVE} handling.
-- - Fix preadv, pwritev and fallocate for -D_FILE_OFFSET_BITS=64 (#533063).

* Tue Nov  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11-1m)
- sync with Fedora 12
-- * Mon Nov  2 2009 Andreas Schwab <schwab@redhat.com> - 2.11-1
-- - Update to 2.11 release.
-- - Disable multi-arch support on PowerPC again since binutils is too old.
-- - Fix crash in tzdata-update due to use of multi-arch symbol (#532128).
-- 
-- * Fri Oct 30 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-27
-- - Update from master.
--   - Fix races in setXid implementation (BZ#3270).
--   - Implement IFUNC for PPC and enable multi-arch support.
--   - Implement mkstemps/mkstemps64 and mkostemps/mkostemps64 (BZ#10349).
--   - Fix IA-64 and S390 sigevent definitions (BZ#10446).
--   - Fix memory leak in NIS grp database handling (BZ#10713).
--   - Print timestamp in nscd debug messages (BZ#10742).
--   - Fix mixing IPv4 and IPv6 name server in resolv.conf.
--   - Fix range checks in coshl.
--   - Implement SSE4.2 optimized strchr and strrchr.
--   - Handle IFUNC symbols in dlsym (#529965).
--   - Misc fixes (BZ#10312, BZ#10315, BZ#10319, BZ#10391, BZ#10425,
--     BZ#10540, BZ#10553, BZ#10564, BZ#10609, BZ#10692, BZ#10780,
--     BZ#10717, BZ#10784, BZ#10789, BZ#10847
-- - No longer build with -fno-var-tracking-assignments.

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-6m)
- release /usr/share/locale, /usr/share/locale/*, /usr/share/locale/*/LC_MESSAGES
- sync with Fedora 12
-- * Mon Oct 19 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-26
-- - Update from master.
--   - Add ____longjmp_chk for sparc.
-- - Avoid installing the same libraries twice.
-- 
-- * Mon Oct 12 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-25
-- - Update from master
--   - Fix descriptor leak when calling dlopen with RTLD_NOLOAD (#527409).
--   - Fix week-1stday in C locale.
--   - Check for integer overflows in formatting functions.
--   - Fix locale program error handling (#525363).
-- 
-- * Mon Sep 28 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-24
-- - Update from master.
--   - Fix missing reloc dependency (#517001).

* Tue Oct 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-8m)
- grab /usr/share/locale/*/LC_MESSAGES/libc.mo

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-7m)
- fix %%files
- Provides: /usr/sbin/glibc_post_upgrade.%%{_target_cpu}
- Provides: /usr/sbin/build-locale-archive
- Provides: /usr/sbin/tzdata-update

* Sat Sep 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-6m)
- sync with Rawhide
-- * Mon Sep 21 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-23
-- - Update from master.

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-5m)
- sync with Rawhide
- BuildRequires: gcc >= 4.4.1-4m for -fno-var-tracking-assignments
-- * Mon Sep 14 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-22
-- - Update from master.
--   - Fix endless loop in localedef.
--   - Fix __longjmp_chk on s390/s390x.
-- - Fix exit codes in nscd start script (#521848).
-- - Build with -fno-var-tracking-assignments for now (#523172).
-- 
-- * Mon Sep  7 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-21
-- - Update from master.
--   - Fix strstr/strcasestr on i386 (#519226).

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-4m)
- sync with Rawhide
-- * Thu Sep  3 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-20
-- - Update from master.
--   - Fix strstr/strcasestr/fma/fmaf on x86_64 (#519226).
--   - Fix lookup of group names in hesiod initgroups (#520472).

* Sat Sep  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-3m)
- BuildRequires: gcc >= 4.4.0
-- 2.10.90 needs gcc44 on i686, or ld-linux.so.2 will be broken.
   (but build success with gcc43 on x86_64)

* Thu Sep  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-2m)
- sync with Rawhide
-- * Wed Sep  2 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-19
-- - Update from master.
--   - Fix x86_64 bits/mathinline.h for -m32 compilation.

* Wed Sep  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.90-1m)
- sync with Rawhide
- drop USAGI patches, since they are too old to be here
-- Rawhide changes
-- * Tue Sep  1 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-18
-- - Update from master.
--   - fix parse error in <bits/mathinline.h> (#520209).
-- 
-- * Thu Aug 27 2009 Roland McGrath <roland@redhat.com> - 2.10.90-17
-- - Update from master.
-- 
-- * Wed Aug 26 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-16
-- - Update from master.
--   - handle AVX saving on x86-64 in interrupted symbol lookups (#519081).
-- 
-- * Mon Aug 24 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-15
-- - Update from master.
--   - fix fortify failure with longjmp from alternate stack (#512103).
-- - Add conflict with prelink (#509655).
-- 
-- * Mon Aug 17 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-14
-- - Update from master.
--   - fix pthread_cond_signal (#516469)
-- 
-- * Mon Aug 10 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-13
-- - Update from master.
--   - fix rehashing of unique symbols (#515677)
-- - Fix spurious messages with --excludedocs (#515948)
-- 
-- * Mon Aug  3 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-12
-- - Update from master.
--   - fix fortify failure with longjmp from alternate stack (#512103)
-- 
-- * Thu Jul 30 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-11
-- - Update from master.
-- - Don't package debuginfo files in glibc-devel.
-- 
-- * Tue Jul 28 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-10
-- - Update from master.
--   * fix memory ordering in pthread_mutex_unlock (BZ#10418)
--   * implement RES_USE_DNSSEC option in resolver (#205842)
--   * fix hang in ldd -r (#513945)
-- 
-- * Mon Jul 27 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-9
-- - Update from master.
-- 
-- * Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.10.90-8.1
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild
-- 
-- * Fri Jul 24 2009 Jakub Jelinek <jakub@redhat.com> - 2.10.90-7.1
-- - Fix up pthread_cond_timedwait on x86_64 with old kernels.
-- 
-- * Thu Jul 23 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-7
-- - Update from master.
-- - Build with -DNDEBUG unless using a prerelease.
-- 
-- * Thu Jul 23 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-6
-- - Rebuilt with binutils-2.19.51.0.14-29.fc12 to fix static binaries
-- 
-- * Wed Jul 22 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-5
-- - Update from master.
-- - Undefine __i686 on x86 to fix build.
-- 
-- * Mon Jul 20 2009 Andreas Schwab <schwab@redhat.com> - 2.10.90-4
-- - Update from master.
-- - Don't build separate i686 package.
-- 
-- * Wed Jul  8 2009 Andreas Schwab <schwab@redhat.com> 2.10.90-3
-- - Reenable setuid on pt_chown.
-- 
-- * Thu Jul  2 2009 Andreas Schwab <aschwab@redhat.com> 2.10.90-2
-- - Update from master.
-- 
-- * Thu Jun 26 2009 Andreas Schwab <aschwab@redhat.com> 2.10.90-1
-- - Update from master.
-- - Enable multi-arch support on x86/x86-64.
-- - Add requires glibc-headers to glibc-devel (#476295).
-- - Implement second fallback mode for DNS requests (#505105).
-- - Don't generate invalid POSIX TZ string for Asia/Dhaka timezone (#506941).
-- - Allow backtrace through __longjmp_chk on powerpc.
-- 
-- * Fri May 22 2009 Jakub Jelinek <jakub@redhat.com> 2.10.1-2
-- - fix accept4 on architectures other than i?86/x86_64
-- - robustify nscd client code during server GC
-- - fix up nscd segfaults during daemon shutdown
-- - fix memchr on ia64 (BZ#10162)
-- - replace the Sun RPC license with the BSD license, with the explicit
--   permission of Sun Microsystems
-- - fix up powerpc long double errno reporting
-- 
-- * Sun May 10 2009 Jakub Jelinek <jakub@redhat.com> 2.10.1-1
-- - fix up getsgent_r and getsgnam_r exports on i?86 and ppc
-- 
-- * Sat May  9 2009 Jakub Jelinek <jakub@redhat.com> 2.10-2
-- - update from trunk
--   - glibc 2.10 release
--   - fix memchr on x86_64 (#499689)
-- 
-- * Mon Apr 27 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-22
-- - update from trunk
--   - further localedef fixes
-- - fix build-locale-archive
-- 
-- * Fri Apr 24 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-21
-- - update from trunk
--   - fix localedef
--   - fix SHIFT_JIS iconv EILSEQ handling (#497267)
--   - misc fixes (BZ#10093, BZ#10100)
-- 
-- * Fri Apr 24 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-20
-- - update from trunk
--   - fix p{read,write}v{,64} (#497429, #497434)
--   - fix strfmon (#496386)
-- 
-- * Thu Apr 16 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-19
-- - update from trunk
--   - fix dlopen from statically linked binaries (#495830)
-- 
-- * Thu Apr 16 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-18
-- - update from trunk
--   - fix fallocate
-- 
-- * Wed Apr 15 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-17
-- - update from trunk
--   - if threads have very small stack sizes, use much smaller buffer
--     in __get_nprocs when called from within malloc (#494631)
-- 
-- * Tue Apr 14 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-16
-- - update from trunk
-- 
-- * Thu Apr  9 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-15
-- - rebuilt with fixed gcc to avoid miscompilation of i586 memmove
-- - reenable experimental malloc again
-- 
-- * Wed Apr  8 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-14
-- - update from trunk
-- - temporarily disable experimental malloc
-- 
-- * Tue Apr  7 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-13
-- - update from trunk
--   - fix strverscmp (#494457)
-- - configure with --enable-nss-crypt
-- 
-- * Wed Apr  1 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-12
-- - update from trunk
-- - configure with --enable-experimental-malloc
-- 
-- * Fri Mar 20 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-11
-- - update from trunk
--   - POSIX 2008 prototype adjustments for scandir{,64}, alphasort{,64} and
--     versionsort{,64}
--   - fix libthread_db (#491197)
-- 
-- * Tue Mar 10 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-10
-- - update from trunk
--   - fix atexit/__cxa_atexit
-- 
-- * Mon Mar  9 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-9
-- - update from trunk
--   - POSIX 2008 support: -D_XOPEN_SOURCE=700 and -D_POSIX_C_SOURCE=200809L
-- - move libnldbl_nonshared.a on ppc*/s390*/sparc* back to glibc-devel
-- 
-- * Fri Feb 27 2009 Roland McGrath <roland@redhat.com> - 2.9.90-8.1
-- - fix libthread_db (#487212)
-- 
-- * Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.9.90-8
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
-- 
-- * Wed Feb 18 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-7
-- - update from trunk
-- - adjust for i586 + i686 from i386 + i686 build
-- - split static libraries into glibc-static subpackage
-- - ld -r the whole libpthread.a together to avoid endless issues with
--   -static ... -lpthread
-- - require 2.6.18 and later kernel
-- 
-- * Wed Feb  4 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-3
-- - update from trunk
--   - ISO C++ compliant strchr etc. with GCC 4.4+
--   - AT_RANDOM support
-- 
-- * Thu Jan  8 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-2
-- - update from trunk
-- 
-- * Fri Jan  2 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-1
-- - update from trunk (#478314)
--
-- T4R changes
-- * Sun Mar 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.9.90-1m)
-- - update to 2.9.90 (20090320T1944)
-- -- it seems that make check does not do well when i686 and %%{buildxen}
-- -- drop Patch199, merged upstream
-- - %%define auxarches athlon sparcv9v sparc64v alphaev6
-- - %%define xenarches i686 athlon
-- - %%global enablekernel 2.6.18
-- - change %{nptl_target_cpu}-redhat-linux to %{nptl_target_cpu}-momonga-linux
-- - hold /var/cache/ldconfig
-- - import following changes from Rawhide
-- -- * Wed Feb 18 2009 Jakub Jelinek <jakub@redhat.com> 2.9.90-7
-- -- - ld -r the whole libpthread.a together to avoid endless issues with
-- --   -static ... -lpthread

* Sat Apr 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-5m)
- merge the following Fedora 10 change
-- * Mon Dec  8 2008 Jakub Jelinek <jakub@redhat.com> 2.9-3
-- - NIS hostname lookup fixes (#473073, #474800, BZ#7058)
-- - fix unsetenv (#472941)
- correct install-info
- %%global auxarches athlon sparcv9 alphaev6
- %%global xenarches i686 athlon

* Sun Feb 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9-4m)
- support newer binutils(binutils-2.19.51.0.2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-3m)
- rebuild against rpm-4.6

* Thu Dec 11 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-2m)
- use %%{?_smp_mflags} instead of $numprocs

* Sun Nov 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9-1m)
- update 2.9

* Thu Jul 17 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8-6m)
- import glibc-fedora.patch (2.8-8.fc9)

* Tue May  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8-5m)
- import glibc-fedora.patch (2.8-3.fc9)

* Sun Apr 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8-4m)
- release %%{_prefix}/lib/locale
- it's provided by filesystem

* Sun Apr 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8-3m)
- release directories owned by filesystem

* Thu Apr 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8-2m)
- add BuildRequires: libcap >= 2.08-1m

* Mon Apr 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8-1m)
- update 2.8-20080412T0741(2.8-1.fc9)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.90-4m)
- rebuild against gcc43

* Sun Mar 30 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.90-3m)
- update 2.7.90-20080328T1347(2.7.90-13.fc9

* Thu Mar  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.90-2m)
- update 2.7.90-20080305T0857

* Tue Mar  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.90-1m)
- update 2.7.90
- add Optimized memset for x86-64 patch
-- http://sources.redhat.com/ml/libc-alpha/2008-02/msg00045.html

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7-4m)
- add glibc-gas_fnstsw.patch. need newer binutils

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7-3m)
- import glibc-2.7-mtrace-perl-5.10.0.patch from mandrake
  to avoid $* error when using perl 5.10.0 with mtrace

* Fri Feb  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7-2m)
- backport gcc43 support from cvs trunk
-- see http://sourceware.org/bugzilla/show_bug.cgi?id=5442

* Sun Oct 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7-1m)
- update 2.7-20071017T2029
- merge 2.6-5m change

* Mon Oct 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.90-3m)
- update 2.6.90-20071011T1636

* Sat Sep 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.90-2m)
- update 2.6.90-20070920T0007

* Tue Sep  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.90-1m)
- update 2.6.90-20070827T2032

* Mon Jul  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6-3m)
- sync with F-7 (glibc-2_6-4)
-* Sun Jul  8 2007 Jakub Jelinek <jakub redhat com> 2.6-4
-- filter <built-in> pseudo-files from debuginfo source lists (#245714)
-- fix sscanf when errno is EINTR before the call (BZ#4745)
-- save/restore errno around reading /etc/default/nss (BZ#4702)
-- fix LD_HWCAP_MASK handling
-- disable workaround for #210748, instead backport
-  ld.so locking fixes from the trunk (#235026)
-- new x86_64 memcpy
-- don't write uninitialized padding bytes to nscd socket
-- fix dl{,v}sym, dl_iterate_phdr and dlopen if some library is
-  mapped into ld.so's inter-segment hole on x86_64 (#245035, #244545)
-- fix LD_AUDIT=a:b program (#180432)
-- don't crash on pseudo-zero long double values passed to
-  *printf on i?86/x86_64/ia64 (BZ#4586)
-- fix *printf %La and strtold with some hexadecimal floating point
-  constants on ppc/ppc64
-- fix nextafterl on ppc/ppc64
-- fix sem_timedwait on i?86 and x86_64

* Sat Jun  2 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-2m)
- sync with F-7 (glibc-2_6-3)
--* Thu May 24 2007 Jakub Jelinek <jakub@redhat.com> 2.6-3
--- don't use %%config(missingok) for locale-archive.tmpl,
--  instead of removing it altogether truncate it to zero
--  size (#240697)
--- add a workaround for #210748
--* Mon May 21 2007 Jakub Jelinek <jakub@redhat.com> 2.6-2
--- restore malloc_set_state backwards compatibility (#239344)
--- fix epoll_pwait (BZ#4525)
--- fix printf with unknown format spec or positional arguments
--  and large width and/or precision (BZ#4514)
--- robust mutexes fix (BZ#4512)

* Fri May 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6-1m)
- update 2.6

* Wed Apr 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.90-5m)
- update 20070416

* Sun Mar  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.90-3m)
- update 20070211

* Mon Jan  1 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.90-2m)
- add x86_64 optimize patch
-- patch500: 6901_all_2.4-new-libm-20061210.patch.bz2
-- patch501: 6901_all_2.4-amd64-strings-20061210.patch.bz2
- ppc: no build power6
- ppc & ppc64 disable any make check 

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.90-1m)
- update 20061219

* Sat Oct  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-2m)
- merge from TUPPA4RI branch

* Tue Oct  3 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-1m)
- update 2.5

* Fri Sep  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-10m)
- update 20060905

* Tue Aug 15 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-9m)
- add glibc-gnu-hash.patch(Patch1000)
-- support DT_GNU_HASH

* Sat Jul  8 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-8m)
- add EXCLUDE_FROM_STRIP environment variable.

* Wed Jun 28 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-7m)
- add x86_64-string optimeze patch
-- libc-string-x86_64-head.diff.gz

* Sat Jun 10 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-6m)
- _enable_debug_packages 0
-- no build -debuginfo packages

* Wed Jun 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-5m)
- delete '%{_prefix}/include$' from headers.filelist
- delete '%{_prefix}/lib/locale' from common.filelist
- delete '%{_prefix}/include/net$' from headers.filelist
- re-add '%{_prefix}/include/net$' from headers.filelist

* Sun May 28 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-4m)
- Sync FC5-update
- 
- * Fri May 12 2006 Jakub Jelinek <jakub@redhat.com> 2.4.8
- - update from CVS
-   - fix tcgetattr (#177965)
-   - fix <sys/queue.h> (#191264)
- 
- * Fri May  5 2006 Jakub Jelinek <jakub@redhat.com> 2.4-7
- - update from CVS
-   - some NIS+ fixes
-   - allow overriding rfc3484 address sorting tables for getaddrinfo
-     through /etc/gai.conf (sample config file included in %%doc directory)
-   - SETENT_BATCH_READ /etc/default/nss option for speeding up
-     some usages of NIS+ (#188246)
-   - move debug state change notification (#179208)
-   - fix ldd script if one of the dynamic linkers is not installed (#190259)
-   - fix a typo in nscd.conf (#190085)
-   - fix handling of SIGHUP in nscd when some caches are disabled (#189978)
-   - make nscd paranoia mode working with non-root server-user (#189779)
-   - fix getaddrinfo (#190002)
-   - add auto-propagate nscd.conf options (#177154)
-   - fix nscd auditing (#169148)
- 
- * Mon Apr 24 2006 Jakub Jelinek <jakub@redhat.com> 2.4-6
- - update from CVS
-   - NIS+ fixes
-   - don't segfault on too large argp key values (#189545)
-   - getaddrinfo fixes for RFC3484 (#188364)

* Thu Mar 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-3m)
- update glibc-20060328T0900
-- +* Tue Mar 28 2006 Jakub Jelinek <jakub redhat com> 2.4-5
-- update from CVS
-  - pshared robust mutex support
-  - fix btowc and bwtoc in C++ (#186410)
-  - fix NIS+ (#186592)
-  - don't declare __wcsto*l_internal for non-GCC or if not -O1+ (#185667)
-- don't mention nscd failures on 2.0 kernels (#185335)

* Thu Mar 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- add libm-x86-64-head.diff.gz
-   http://sources.redhat.com/ml/libc-alpha/2006-03/msg00147.html

* Sat Mar 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-1m)
- update 2.4

* Sat Feb 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.90-6m)
- sync to trunk patch

* Fri Feb 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.90-5m)
- sync to fc dev (glibc-2.3.90-38)

* Sun Feb 19 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.90-4m)
- add Xen support

* Sat Feb 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.90-3m)
- merge fc spec

* Sat Feb 18 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.3.90-2m)
- sync to fc dev (glibc-2.3.90-37)

* Sat Jan 14 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.6-1m)
- update 2.3.6
- import fedora patch
-- Patch0: glibc-2.3.6-fedora.patch

* Thu Dec 29 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.5-4m)
- sync FC-4
-  Patch4: glibc-20050524-20050727.patch
-  Patch5: glibc-zh_TW.patch
-  Patch6: glibc-20050727-20050815.patch

* Thu Nov  3 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.3.5-3m)
- BuildRequires: binutils >= 2.15.94.0.2 for AS_NEEDED directive

* Tue Nov  1 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.3.5-2m)
- remove nptl-check patch (Patch1)

* Fri Oct 21 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.3.5-1m)
- update to 2.3.5

* Mon Oct 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (kossori)
- make check hungup on ppc64

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.3.4-3m)
- fix nscd.filelist
- enable alpha, mipsel

* Wed Jul 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.4-2m)
- rebuild glibc to build rpm with nptl-devel

* Fri Jan 29 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.4-1m)
- update to 2.3.4

* Mon Jan 10 2005 mutecat <mutecat@momonga-linux.org>
- (2.3.3-7m)
- arrange ppc.

* Sat Nov 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.3.3-6m)
- enable nptl/tls
- sync with Fedora (2.3.3-84)
- Patch104 was merged

* Sat Nov  6 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.3-5m)
- add Patch104 from http://sources.redhat.com/bugzilla/show_bug.cgi?id=424

* Mon Nov  1 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.3-4m)
- remove Conflicts kernel < 2.2.0 at nscd

* Fri Oct 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-3m)
- add patch4 for waitpid().

* Sun Oct  3 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.3.3-2m)
- sync with Fedora (2.3.3-63)

* Fri Sep 17 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.3.3-1m)
- update to 2.3.3
- sync with Fedora (2.3.3-51)
- still disable NPTL / TLS

* Sat Jul 24 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.3.2-13m)
- add patch15 from cvs (fixes for find_collation_sequence_value() and check_node_accept_bytes() in regexec.c).

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.2-12m)
- delete %%global _ipv6 1, use usagi_ipv6
- add BuildRequires: momonga-rpmmacros for use usagi_ipv6 macro
- use %%if %%{usagi_ipv6} 

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (2.3.2-11m)
- %%{_initscriptdir}

* Fri Mar  5 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.3.2-10m)
- install usagi libinet6 headers
- update usagi to 20040301

* Wed Mar  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.3.2-9m)
- add glibc-2.3.2-suppress-ldconfig-warning.patch to suppress warning "lib*.so is too small, not checked."

* Tue Jan 27 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.3.2-8m)
- update usagi to 20040119

* Sun Jan 11 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.3.2-7m)
- move /etc/ld.so.conf to glibc-common

* Sat Jan 10 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.3.2-6m)
- move /sbin/ldconfig to glibc-common
- where is change log of 5m...

* Thu Dec 18 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.3.2-4m)
- remove glibc-Versions.patch
- remove glibc-2.3.2-cp932-2.diff.gz(cp932 patch was merged into redhat's source)
- sync with glibc-2.3.2-101.1(fedora)
-* Tue Nov 11 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-101.1
-- fix getifaddrs (CAN-2003-0859)
-- fix ftw fd leak
-- fix linuxthreads sigaction (#108634)
-- fix glibc 2.0 stdio compatibility
-- fix uselocale (LC_GLOBAL_LOCALE)
-- speed up stdio locking in non-threaded programs on IA-32
-- try to maintain correct order of cleanups between those
-  registered with __attribute__((cleanup))
-  and with LinuxThreads style pthread_cleanup_push/pop (#108631)
-- fix segfault in regex (#109606)
-- fix RE_ICASE multi-byte handling in regex
-- fix pthread_exit in libpthread.a (#109790)
-
-* Mon Oct 27 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-101
-- update from CVS
-  - fix ld.so --verify (and ldd)
-
-* Mon Oct 27 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-100
-- update from CVS
-  - fix sprof (#103727)
-  - avoid infinite loops in {,f}statvfs{,64} with hosed mounts file
-  - prevent dlopening of executables
-  - fix glob with GLOB_BRACE and without GLOB_NOESCAPE
-  - fix locale printing of word values on 64-bit big-endian arches
-    (#107846)
-  - fix getnameinfo and getaddrinfo with reverse IPv6 lookups
-    (#101261)
-
-* Wed Oct 22 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-99
-- update from CVS
-  - dl_iterate_phdr in libc.a on arches other than IA-64
-  - LD_DEBUG=statistics prints number of relative relocations
-  - fix hwcap computation
-- NPTL is now part of upstream glibc CVS
-- include {st,xh,zu}_ZA{,.UTF-8} locales
-
-* Sat Oct  4 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-98
-- update from CVS
-  - fix close, pause and fsync (#105348)
-  - fix pthread_once on IA-32
-- implement backtrace () on IA-64, handle -fomit-frame-pointer
-  in AMD64 backtrace () (#90402)
-
-* Tue Sep 30 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-97
-- update from CVS
-  - fix <sys/sysmacros.h> with C++ or -ansi or -pedantic C
-  - fix mknod/ustat return value when given bogus device number (#105768)
-
-* Fri Sep 26 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-96
-- rebuilt
-
-* Fri Sep 26 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-95
-- fix IA-64 getcontext
-
-* Thu Sep 25 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-94
-- update from CVS
-- fix syslog with non-C non-en_* locales (#61296, #104979)
-- filter GLIBC_PRIVATE symbols from glibc provides
-- fix NIS+
-
-* Thu Sep 25 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-93
-- update from CVS
-- assume 2.4.21 kernel features on RHEL/ppc*, so that
-  {make,set,get,swap}context works
-- backout execstack support for RHEL
-- build rtkaio on amd64 too
-
-* Wed Sep 24 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-92
-- update from CVS
-  - execstack/noexecstack support
-  - build nscd as PIE
-- move __libc_stack_end back to @GLIBC_2.1
-- build against elfutils >= 0.86 to fix stripping on s390x
-
-* Mon Sep 22 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-91
-- rebuilt
-
-* Mon Sep 22 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-90
-- update from CVS
-  - NPTL locking change (#102682)
-- don't jump around lock on amd64
-
-* Thu Sep 18 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-89
-- fix open_memstream/syslog (#104661)
-
-* Thu Sep 18 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-88
-- update from CVS
-  - retrieve affinity in pthread_getattr_np
-  - fix pthread_attr_[gs]etaffinity_np
-  - handle hex and octal in wordexp
-
-* Wed Sep 17 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-87
-- update from CVS
-  - truncate instead of round in utimes when utimes syscall is not available
-  - don't align stack in every glibc function unnecessarily on IA-32
-  - make sure threads have their stack 16 byte aligned on IA-32
-  - move sched_[sg]etaffinity to GLIBC_2.3.3 symbol version (#103231)
-  - fix pthread_getattr_np for the initial thread (#102683)
-  - avoid linuxthreads signal race (#104368)
-- ensure all gzip invocations are done with -n option
-
-* Fri Sep 12 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-86
-- update from CVS
-- avoid linking in libgcc_eh.a unnecessarily
-- change ssize_t back to long int on s390 -m31, unless
-  gcc 2.95.x is used
-
-* Wed Sep 10 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-85
-- update from CVS
-  - fix IA-64 memccpy (#104114)
-
-* Tue Sep  9 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-84
-- update from CVS
-  - undo broken amd64 signal context changes
-
-* Tue Sep  9 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-83
-- update from CVS
-- change *nlink_t, *ssize_t and *intptr_t types on s390 -m31 to
-  {unsigned,} int
-- change *u_quad_t, *quad_t, *qaddr_t, *dev_t, *ino64_t, *loff_t,
-  *off64_t, *rlim64_t, *blkcnt64_t, *fsblkcnt64_t, *fsfilcnt64_t
-  on 64-bit arches from {unsigned,} long long int {,*} to
-  {unsigned,} long int {,*} to restore binary compatibility
-  for C++ functions using these types as arguments
-
-* Sun Sep  7 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-82
-- rebuilt
-
-* Sat Sep  6 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-81
-- update from CVS
-  - fix tc[gs]etattr/cf[gs]et[io]speed on ppc (#102732)
-  - libio fixes
-
-* Thu Sep  4 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-80
-- update from CVS
-  - fix IA-64 cancellation when mixing __attribute__((cleanup ()))
-    and old-style pthread_cleanup_push cleanups
-
-* Tue Sep  2 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-79
-- updated from CVS
-  - lots of cancellation fixes
-  - fix posix_fadvise* on ppc32
-  - TLS layout fix
-  - optimize stdio cleanups (#103354)
-  - sparcv9 NPTL
-  - include sigset, sighold, sigrelse, sigpause and sigignore prototypes
-    in signal.h even if -D_XOPEN_SOURCE_EXTENDED (#103269)
-  - fix svc_getreqset on 64-bit big-endian arches
-  - return ENOSYS in linuxthreads pthread_barrierattr_setpshared for
-    PTHREAD_PROCESS_SHARED
-  - add pthread_cond_timedwait stubs to libc.so (#102709)
-- split glibc-devel into glibc-devel and glibc-headers to ensure
-  amd64 /usr/include always wins on amd64/i386 bi-arch installs
-- increase PTHREAD_STACK_MIN on alpha, ia64 and sparc*
-- get rid of __syscall_* prototypes and stubs in sysdeps/unix/sysv/linux
-- run make check also with linuxthreads (on IA-32 non-FLOATING_STACKS)
-  ld.so and NPTL (on IA-32 also FLOATING_STACKS linuxthreads) libraries
-  and tests
-
-* Tue Aug 25 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-78
-- include dl-osinfo.h only in glibc-debuginfo-2*.rpm, not
-  in glibc-debuginfo-common*
-
-* Mon Aug 25 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-77
-- update from CVS
-  - fix glibc 2.0 libio compatibility (#101385)
-  - fix ldconfig with /usr/lib/lib*.so symlinks (#102853)
-  - fix assert.h (#102916, #103017)
-  - make ld.so.cache identical between IA-32 and AMD64 (#102887)
-  - fix static linking of large IA-64 binaries (#102586)
-- avoid using floating point regs in lazy binding code on ppc64 (#102763)
-
-* Fri Aug 22 2003 Roland McGrath <roland@redhat.com> 2.3.2-76
-- add td_thr_tls_get_addr changes missed in initial nptl_db rewrite
-
-* Sun Aug 17 2003 Roland McGrath <roland@redhat.com> 2.3.2-74
-- nptl_db rewrite not yet in CVS
-
-* Thu Aug 14 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-72
-- update from CVS
-  - fix rtkaio aio_fsync{,64}
-  - update rtkaio for !BROKEN_THREAD_SIGNALS
-  - fix assert macro when used on pointers
-
-* Wed Aug 13 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-71
-- update from CVS
-
-* Tue Aug 12 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-70
-- update from CVS
-- disable CLONE_STOPPED for now until it is resolved
-- strip crt files
-- fix libio on arches with no < GLIBC_2.2 support (#102102, #102105)
-- fix glibc-debuginfo to include all nptl and nptl_db sources
-
-* Thu Aug  7 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-69
-- update from CVS
-  - fix pthread_create@GLIBC_2.0 (#101767)
-- __ASSUME_CLONE_STOPPED on all arches but s390* in RHEL
-
-* Sun Aug  3 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-68
-- update from CVS
-  - only use CLONE_STOPPED if kernel supports it, fix setting of thread
-    explicit scheduling (#101457)
-
-* Fri Aug  1 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-67
-- update from CVS
-  - fix utimes and futimes if kernel doesn't support utimes syscall
-  - fix s390 ssize_t type
-  - fix dlerror when called before any dlopen/dlsym
-  - update IA-64 bits/sigcontext.h (#101344)
-  - various warning fixes
-  - fix pthread.h comment typos (#101363)
-
-* Wed Jul 30 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-66
-- update from CVS
-- fix dlopen of libraries using TLS IE/LE models
-
-* Tue Jul 29 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-65
-- update from CVS
-  - fix timer_create
-  - use __extension__ before long long typedefs in <bits/types.h> (#100718)
-
-* Mon Jul 28 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-64
-- update from CVS
-  - fix wcpncpy (#99462)
-  - export _res@GLIBC_2.0 even from NPTL libc.so (__res_state ()
-    unlike __errno_location () or __h_errno_location () was introduced
-    in glibc 2.2)
-  - fix zic bug on 64-bit platforms
-  - some TLS handling fixes
-  - make ldconfig look into alternate ABI dirs by default (#99402)
-- move %{_datadir}/zoneinfo to tzdata package, so that it can be
-  errataed separately from glibc
-- new add-on - rtkaio
-- prereq libgcc, as glibc now relies on libgcc_s.so.1 for pthread_cancel
-
-* Tue Jul 15 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-63
-- fix thread cancellation on ppc64
-
-* Sat Jul 12 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-62
-- update from CVS
-  - fix thread cancellation on ppc32, s390 and s390x
-
-* Thu Jul 10 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-61
-- update from CVS
-  - build libc_nonshared.a with -fPIC instead of -fpic
-- fix ppc64 PIE support
-- add cfi directives to NPTL sysdep-cancel.h on ppc/ppc64/s390/s390x
-
-* Tue Jul  8 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-60
-- update from CVS
-
-* Thu Jul  3 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-59
-- update from CVS
-- on IA-64 use different symbols for cancellation portion of syscall
-  handlers to make gdb happier
-
-* Thu Jun 26 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-58
-- update from CVS
-  - nss_compat supporting LDAP etc.

* Sat Aug 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.2-3m)
- add glibc-2.2.3-localedef-disable-normalize.patch for xinitrc

* Sat Jul 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.2-2m)
- add cp932
   See: http://www2d.biglobe.ne.jp/~msyk/software/glibc/README.ja

* Sun Jul 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.2-1m)
- usagi: force glibc23

* Sun Jul 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.2-0.0.5m)
- sync with RawHide(glibc-2.3.2-57)
-  changes are:
-  * Tue Jun 24 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-57
-  - update from CVS
-  
-  * Thu Jun 19 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-56
-  - fix condvars and semaphores in ppc* NPTL
-  - fix test-skeleton.c reporting of timed-out tests (#91269)
-  - increase timeouts for tests during make check
-  
-  * Wed Jun 18 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-55
-  - make ldconfig default to both /lib+/usr/lib and /lib64+/usr/lib64
-    on bi-ABI architectures (#97557)
-  - disable FUTEX_REQUEUE on ppc* temporarily
-  
-  * Wed Jun 18 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-54
-  - update from CVS
-  - fix glibc_post_upgrade on ppc
-  
-  * Tue Jun 17 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-53
-  - update from CVS
-  - fix localedef (#90659)
-  - tweak linuxthreads for librt cancellation
-  
-  * Mon Jun 16 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-52
-  - update from CVS

* Sun Jun 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.2-0.0.4m)
- use %%{?_smp_mflags} instead of $numprocs
- changes in 2.3.2-33.9 are canceled
- sync with RawHide(glibc-2.3.2-51)
-  changes are:
-  * Thu Jun 12 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-51
-  - update from CVS
-  - fix <gnu/stubs.h> (#97169)
-
-  * Wed Jun 11 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-50
-  - update from CVS
-
-  * Tue Jun 10 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-49
-  - update from CVS
-    - fix pthread_cond_signal on IA-32 (#92080, #92253)
-    - fix setegid (#91567)
-  - don't prelink -R libc.so on any architecture, it prohibits
-    address randomization
-
-  * Fri Jun  5 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-48
-  - update from CVS
-    - fix IA-64 NPTL build
-
-  * Thu Jun  5 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-47
-  - update from CVS
-  - PT_GNU_STACK segment in binaries/executables and .note.GNU-stack
-    section in *.[oa]
-
-  * Sun Jun  1 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-46
-  - update from CVS
-  - enable NPTL on AMD64
-  - avoid using trampolines in localedef
-
-  * Fri May 29 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-45
-  - enable NPTL on IA-64
-
-  * Fri May 29 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-44
-  - update from CVS
-  - enable NPTL on s390 and s390x
-  - make __init_array_start etc. symbols in elf-init.oS hidden undefined
-
-  * Thu May 29 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-43
-  - update from CVS
-
-  * Fri May 23 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-42
-  - update from CVS
-
-  * Tue May 20 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-41
-  - update from CVS
-  - use NPTL libs if uname -r contains nptl substring or is >= 2.5.69
-    or set_tid_address syscall is available instead of checking
-    AT_SYSINFO dynamic tag
-
-  * Thu May 15 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-40
-  - update from CVS
-
-  * Wed May 14 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-39
-  - update from CVS
-    - fix for prelinking of libraries with no dependencies
-
-  * Tue May 13 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-38
-  - update from CVS
-  - enable NPTL on ppc and ppc64
-
-  * Tue May  6 2003 Matt Wilson <msw@redhat.com> 2.3.2-37
-  - rebuild
-
-  * Sun May  4 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-36
-  - update from CVS
-
-  * Sat May  3 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-35
-  - update from CVS
-    - make -jN build fixes
-
-  * Fri May  2 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-34
-  - update from CVS
-   avoid using trampolines in iconvconfig for now


* Tue May 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.2-0.0.3m)
- Provides: locale-utf
- Provides: %%{_sbindir}/glibc_post_upgrade
- Provides: %%{_sbindir}/build-locale-archive
- rebuild against binutils-2.14.90.0.1-1m
- use own find_requires
- remove Conflicts: kernel < 2.2.0

* Fri May 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.2-0.0.2m)
- /etc/rc.d/init.d/nscd -> /etc/init.d/nscd

* Tue Apr 29 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.2-0.0.1m)
- import from rawhide(glibc-2.3.2-33.9)
- change basearch from i386 to i586
- add usagi headers and libinet6

* Sat Apr 26 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-33.9
- rebuilt for RHL 9
- move __libc_fork, __libc_stack_end, __libc_wait and __libc_waitpid
  from GLIBC_PRIVATE symver for the errata
- put back xdr_ypall@GLIBC_2.1.2
- undo _NSIG changes

* Sat Apr 26 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-33
- update from CVS

* Fri Apr 25 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-32
- update from CVS
- more ppc TLS fixes

* Wed Apr 23 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-31
- update from CVS
  - nscd fixes
  - fix Bahrain spelling (#56298)
  - fix Ukrainian collation (#83973)
  - accept trailing spaces in /etc/ld.so.conf (#86032)
  - perror fix (#85994)
  - fix localedef (#88978)
  - fix getifaddrs (#89026)
  - fix strxfrm (#88409)
- fix ppc TLS
- fix getaddrinfo (#89448)
- don't print warning about errno, h_errno or _res if
  LD_ASSUME_KERNEL=2.4.1 or earlier

* Tue Apr 15 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-30
- update from CVS
- fix prelink on ppc32
- add TLS support on ppc32 and ppc64
- make sure on -m64 arches all helper binaries are built with this
  option

* Mon Apr 14 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-29
- update from CVS
  - fix strxfrm (#88409)
- use -m64 -mno-minimal-toc on ppc64
- conflict with kernels < 2.4.20 on ppc64 and < 2.4.0 on x86_64
- link glibc_post_upgrade against newly built libc.a

* Sun Apr 13 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-28
- update from CVS
  - fix NPTL pthread_detach and already terminated, but not yet
    joined thread (#88219)
  - fix bug-regex4 testcase (#88118)
  - reenable prelink support broken in 2.3.2-13
  - fix register_printf_function (#88052)
  - fix double free with fopen using ccs= (#88056)
  - fix potential access below $esp in {set,swap}context (#88093)
  - fix buffer underrun in gencat -H (#88099)
  - avoid using unitialized variable in tst-tgmath (#88101)
  - fix gammal (#88104)
  - fix iconv -c
  - fix xdr_string (PR libc/4999)
  - fix /usr/lib/nptl/librt.so symlink
  - avoid running NPTL cleanups twice in some cases
  - unblock __pthread_signal_cancel in linuxthreads, so that
    linuxthreads threaded programs work correctly if spawned
    from NPTL threaded programs
  - fix sysconf _SC_{NPROCESSORS_{CONF,ONLN},{,AV}PHYS_PAGES}
- remove /lib/i686 directory before running ldconfig in glibc post
  during i686 -> i386 glibc "upgrades" (#88456)

* Wed Apr  2 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-22
- update from CVS
  - add pthread_atfork to libpthread.a

* Tue Apr  1 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-21
- update from CVS
- make sure linuxthreads pthread_mutex_lock etc. is not a cancellation
  point

* Sat Mar 29 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-20
- update from CVS
- if kernel >= 2.4.1 doesn't support NPTL, fall back to
  /lib/i686 libs on i686, not stright to /lib

* Fri Mar 28 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-19
- update from CVS
  - timers fixes

* Thu Mar 27 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-18
- update from CVS
- fix NPTL pthread_cond_timedwait
- fix sysconf (_SC_MONOTONIC_CLOCK)
- use /%%{_lib}/tls instead of /lib/tls on x86-64
- add /%{_lib}/tls/librt*so* and /%{_lib}/i686/librt*so*
- display content of .out files for all make check failures

* Wed Mar 26 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-17
- update from CVS
  - kernel POSIX timers support

* Sat Mar 22 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-16
- update from CVS
  - export __fork from glibc again
- fix glibc-compat build in NPTL
- fix c_stubs
- fix some more atomic.h problems
- don't check abi in glibc-compat libs

* Fri Mar 21 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-15
- update from CVS
- build glibc-compat (for glibc 2.0 compatibility) and c_stubs add-ons
- condrestart sshd in glibc_post_upgrade so that the user can
  log in remotely and handle the rest (#86339)
- fix a typo in glibc_post_upgrade on sparc

* Tue Mar 18 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-14
- update from CVS
- change i686/athlon libc.so.6 base to 0x00e80000

* Mon Mar 17 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-13
- update from CVS
  - hopefully last fix for condvar problems

* Fri Mar 14 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-12
- fix bits/syscall.h creation on x86-64

* Thu Mar 13 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-11
- update from CVS

* Wed Mar 12 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-10
- update from CVS

* Tue Mar 11 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-9
- update from CVS
- fix glibc-debug description (#85111)
- make librt.so a symlink again, not linker script

* Tue Mar  4 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-8
- update from CVS
- remove the workarounds for broken software accessing GLIBC_PRIVATE
  symbols

* Mon Mar  3 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-7
- update from CVS

* Sun Mar  2 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-6
- fix TLS IE/LE model handling in dlopened libraries
  on TCB_AT_TP arches

* Thu Feb 25 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-5
- update from CVS

* Tue Feb 25 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-4
- update from CVS

* Mon Feb 24 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-3
- update from CVS
- only warn about errno, h_errno or _res for binaries, never
  libraries
- rebuilt with gcc-3.2.2-4 to use direct %gs TLS access insn sequences

* Sun Feb 23 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-2
- update from CVS

* Sat Feb 22 2003 Jakub Jelinek <jakub@redhat.com> 2.3.2-1
- update from CVS

* Thu Feb 20 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-51
- update from CVS

* Wed Feb 19 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-50
- update from CVS

* Wed Feb 19 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-49
- update from CVS
- remove nisplus and nis from the default nsswitch.conf (#67401, #9952)

* Tue Feb 18 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-48
- update from CVS

* Sat Feb 15 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-47
- update from CVS

* Fri Feb 14 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-46
- update from CVS
  - pthread_cond* NPTL fixes, new NPTL testcases

* Thu Feb 13 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-45
- update from CVS
- include also linuxthreads FLOATING_STACKS libs on i686 and athlon:
  LD_ASSUME_KERNEL=2.2.5 to LD_ASSUME_KERNEL=2.4.0 is non-FLOATING_STACKS lt,
  LD_ASSUME_KERNEL=2.4.1 to LD_ASSUME_KERNEL=2.4.19 is FLOATING_STACKS lt,
  later is NPTL
- enable TLS on alpha/alphaev6
- add BuildPreReq: /usr/bin/readlink

* Tue Feb 11 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-44
- update from CVS
  - pthread_once fix

* Mon Feb 10 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-43
- update from CVS
- vfork fix on s390
- rebuilt with binutils 2.13.90.0.18-5 so that accesses to errno
  don't bind locally (#83325)

* Thu Feb 06 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-42
- update from CVS
- fix pthread_create after vfork+exec in linuxthreads

* Wed Feb 05 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-41
- update from CVS

* Thu Jan 30 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-40
- update from CVS

* Wed Jan 29 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-39
- update from CVS
- enable TLS on s390{,x} and sparc{,v9}

* Fri Jan 17 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-38
- update from CVS
- initialize __environ in glibc_post_upgrade to empty array,
  so that it is not NULL
- compat symlink for s390x /lib/ld64.so.1
- enable glibc-profile on x86-64
- only include libNoVersion.so on IA-32, Alpha and Sparc 32-bit

* Thu Jan 16 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-37
- update from CVS
  - nscd fixes, *scanf fix
- fix %%nptlarches noarch build (#81909)
- IA-64 TLS fixes

* Tue Jan 14 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-36
- update from CVS
- rework -debuginfo subpackage, add -debuginfo-common
  subpackage on IA-32, Alpha and Sparc (ie. auxiliary arches)
- fix vfork in libc.a on PPC32, Alpha, Sparc
- fix libio locks in linuxthreads libc.so if libpthread.so
  is dlopened later (#81374)

* Mon Jan 13 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-35
- update from CVS
  - dlclose bugfixes
- fix NPTL libpthread.a
- fix glibc_post_upgrade on several arches

* Sat Jan 11 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-34
- update from CVS
- TLS support on IA-64

* Wed Jan  8 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-33
- fix vfork in linuxthreads (#81377, #81363)

* Tue Jan  7 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-32
- update from CVS
- don't use TLS libs if kernel doesn't set AT_SYSINFO
  (#80921, #81212)
- add ntp_adjtime on alpha (#79996)
- fix nptl_db (#81116)

* Sun Jan  5 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-31
- update from CVS
- support all architectures again

* Fri Jan  3 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-30
- fix condvar compatibility wrappers
- add ugly hack to use non-TLS libs if a binary is seen
  to have errno, h_errno or _res symbols in .dynsym

* Fri Jan  3 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-29
- update from CVS
  - fixes for new condvar

* Thu Jan  2 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-28
- new NPTL condvar implementation plus related linuxthreads
  symbol versioning updates

* Thu Jan  2 2003 Jakub Jelinek <jakub@redhat.com> 2.3.1-27
- update from CVS
- fix #include <sys/stat.h> with -D_BSD_SOURCE or without
  feature set macros
- make *sigaction, sigwait and raise the same between
  -lpthread -lc and -lc -lpthread in linuxthreads builds

* Tue Dec 31 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-26
- fix dlclose

* Sun Dec 29 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-25
- enable sysenter by default for now
- fix endless loop in ldconfig

* Sat Dec 28 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-24
- update from CVS

* Fri Dec 27 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-23
- update from CVS
  - fix ptmalloc_init after clearenv (#80370)

* Sun Dec 22 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-22
- update from CVS
- add IA-64 back
- move TLS libraries from /lib/i686 to /lib/tls

* Thu Dec 19 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-21
- system(3) fix for linuxthreads
- don't segfault in pthread_attr_init from libc.so
- add cancellation tests from nptl to linuxthreads

* Wed Dec 18 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-20
- fix up lists of exported symbols + their versions
  from the libraries

* Wed Dec 18 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-19
- fix --with-tls --enable-kernel=2.2.5 libc on IA-32

* Wed Dec 18 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-18
- update from CVS
  - fix NPTL hanging mozilla
  - initialize malloc in mALLOPt (fixes problems with squid, #79957)
  - make linuxthreads work with dl_dynamic_weak 0
  - clear dl_dynamic_weak everywhere

* Tue Dec 17 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-17
- update from CVS
  - NPTL socket fixes, flockfile/ftrylockfile/funlockfile fix
  - kill -debug sub-package, rename -debug-static to -debug
  - clear dl_dynamic_weak for NPTL

* Mon Dec 16 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-16
- fix <bits/mathinline.h> and <bits/nan.h> for C++
- automatically generate NPTL libpthread wrappers

* Mon Dec 16 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-15
- update from CVS
  - all functions which need cancellation should now be cancellable
    both in libpthread.so and libc.so
  - removed @@GLIBC_2.3.2 cancellation wrappers

* Fri Dec 13 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-14
- update from CVS
  - replace __libc_lock_needed@GOTOFF(%ebx) with
    %gs:offsetof(tcbhead_t, multiple_threads)
  - start of new NPTL cancellation wrappers

* Thu Dec 12 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-13
- update from CVS
- use inline locks in malloc

* Tue Dec 10 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-12
- update from CVS
  - support LD_ASSUME_KERNEL=2.2.5 in statically linked programs

* Mon Dec  9 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-11
- update from CVS
- rebuilt with gcc-3.2.1-2

* Fri Dec  6 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-10
- update from CVS
- non-nptl --with-tls --without-__thread FLOATING_STACKS libpthread
  should work now
- faster libc locking when using nptl
- add OUTPUT_FORMAT to linker scripts
- fix x86_64 sendfile (#79111)

* Wed Dec  4 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-9
- update from CVS
  - RUSCII support (#78906)
- for nptl builds add BuildRequires
- fix byteswap.h for non-gcc (#77689)
- add nptl-devel package

* Tue Dec  3 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-8
- update from CVS
  - make --enable-kernel=2.2.5 --with-tls --without-__thread
    ld.so load nptl and other --with-__thread libs
- disable nptl by default for now

* Wed Nov 27 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-7
- update from CVS
- restructured redhat/Makefile and spec, so that src.rpm contains
  glibc-<date>.tar.bz2, glibc-redhat-<date>.tar.bz2 and glibc-redhat.patch
- added nptl

* Fri Nov  8 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-6
- update from CVS
  - even more regex fixes
- run sed testsuite to check glibc regex

* Thu Oct 24 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-5
- fix LD_DEBUG=statistics and LD_TRACE_PRELINKING in programs
  using libpthread.so.

* Thu Oct 24 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-4
- update from CVS
  - fixed %a and %A in *printf (#75821)
  - fix re_comp memory leaking (#76594)

* Tue Oct 22 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-3
- update from CVS
  - some more regex fixes
- fix libpthread.a (#76484)
- fix locale-archive enlarging

* Fri Oct 18 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-2
- update from CVS
  - don't need to use 128K of stacks for DNS lookups
  - regex fixes
  - updated timezone data e.g. for this year's Brasil DST
    changes
  - expand ${LIB} in RPATH/RUNPATH/dlopen filenames

* Fri Oct 11 2002 Jakub Jelinek <jakub@redhat.com> 2.3.1-1
- update to 2.3.1 final
  - support really low thread stack sizes (#74073)
- tzdata update

* Wed Oct  9 2002 Jakub Jelinek <jakub@redhat.com> 2.3-2
- update from CVS
  - handle low stack limits
  - move s390x into */lib64

* Thu Oct  3 2002 Jakub Jelinek <jakub@redhat.com> 2.3-1
- update to 2.3 final
  - fix freopen on libstdc++ <= 2.96 stdin/stdout/stderr (#74800)

* Sun Sep 29 2002 Jakub Jelinek <jakub@redhat.com> 2.2.94-3
- don't prelink -r libc.so on ppc/x86-64/sparc*, it doesn't
  speed things up, because they are neither REL arches, nor
  ELF_MACHINE_REL_RELATIVE
- fix sparc64 build

* Sun Sep 29 2002 Jakub Jelinek <jakub@redhat.com> 2.2.94-2
- update from CVS

* Sat Sep 28 2002 Jakub Jelinek <jakub@redhat.com> 2.2.94-1
- update from CVS
- prelink on ppc and x86-64 too
- don't remove ppc memset
- instead of listing on which arches to remove glibc-compat
  list where it should stay

* Fri Sep  6 2002 Jakub Jelinek <jakub@redhat.com> 2.2.93-5
- fix wcsmbs functions with invalid character sets (or malloc
  failures)
- make sure __ctype_b etc. compat vars are updated even if
  they are copy relocs in the main program

* Thu Sep  5 2002 Jakub Jelinek <jakub@redhat.com> 2.2.93-4
- fix /lib/libnss1_dns.so.1 (missing __set_h_errno definition
  leading to unresolved __set_h_errno symbol)

* Wed Sep  4 2002 Jakub Jelinek <jakub@redhat.com> 2.2.93-3
- security fix - increase dns-network.c MAXPACKET to at least
  65536 to avoid buffer overrun. Likewise glibc-compat
  dns-{host,network}.c.

* Tue Sep  3 2002 Jakub Jelinek <jakub@redhat.com> 2.2.93-2
- temporarily add back __ctype_b, __ctype_tolower and __ctype_toupper to
  libc.a and export them as @@GLIBC_2.0 symbols, not @GLIBC_2.0
  from libc.so - we have still lots of .a libraries referencing
  __ctype_{b,tolower,toupper} out there...

* Tue Sep  3 2002 Jakub Jelinek <jakub@redhat.com> 2.2.93-1
- update from CVS
  - 2.2.93 release
  - use double instead of single indirection in isXXX macros
  - per-locale wcsmbs conversion state

* Sat Aug 31 2002 Jakub Jelinek <jakub@redhat.com> 2.2.92-2
- update from CVS
  - fix newlocale/duplocale/uselocale
- disable profile on x86_64 for now

* Sat Aug 31 2002 Jakub Jelinek <jakub@redhat.com> 2.2.92-1
- update from CVS
  - 2.2.92 release
  - fix gettext after uselocale
  - fix locales in statically linked threaded programs
  - fix NSS

* Thu Aug 29 2002 Jakub Jelinek <jakub@redhat.com> 2.2.91-1
- update from CVS
  - 2.2.91 release
  - fix fd leaks in locale-archive reader (#72043)
- handle EROFS in build-locale-archive gracefully (#71665)

* Wed Aug 28 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-27
- update from CVS
  - fix re_match (#72312)
- support more than 1024 threads

* Fri Aug 23 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-26
- update from CVS
  - fix i386 build

* Thu Aug 22 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-25
- update from CVS
  - fix locale-archive loading hang on some (non-primary) locales
    (#72122, #71878)
  - fix umount problems with locale-archives when /usr is a separate
    partition (#72043)
- add LICENSES file

* Fri Aug 16 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-24
- update from CVS
  - only mmap up to 2MB of locale-archive on 32-bit machines
    initially
  - fix fseek past end + fread segfault with mmaped stdio
- include <sys/debugreg.h> which is mistakenly not included
  in glibc-devel on IA-32

* Fri Aug 16 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-23
- don't return normalized locale name in setlocale when using
  locale-archive

* Thu Aug 15 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-22
- update from CVS
  - optimize for primary system locale
- localedef fixes (#71552, #67705)

* Wed Aug 14 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-21
- fix path to locale-archive in libc reader
- build locale archive at glibc-common %post time
- export __strtold_internal and __wcstold_internal on Alpha again
- workaround some localedata problems

* Tue Aug 13 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-20
- update from CVS
- patch out set_thread_area for now

* Fri Aug  9 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-19
- update from CVS
- GB18030 patch from Yu Shao
- applied Debian patch for getaddrinfo IPv4 vs. IPv6
- fix regcomp (#71039)

* Sun Aug  4 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-18
- update from CVS
- use /usr/sbin/prelink, not prelink (#70376)

* Thu Jul 25 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-17
- update from CVS

* Thu Jul 25 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-16
- update from CVS
  - ungetc fix (#69586)
  - fseek errno fix (#69589)
  - change *etrlimit prototypes for C++ (#68588)
- use --without-tls instead of --disable-tls

* Thu Jul 11 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-15
- set nscd user's shell to /sbin/nologin (#68369)
- fix glibc-compat buffer overflows (security)
- buildrequire prelink, don't build glibc's own copy of it (#67567)
- update from CVS
  - regex fix (#67734)
  - fix unused warnings (#67706)
  - fix freopen with mmap stdio (#67552)
  - fix realloc (#68499)

* Tue Jun 25 2002 Bill Nottingham <notting@redhat.com> 2.2.90-14
- update from CVS
  - fix argp on long words
  - update atime in libio

* Sat Jun 22 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-13
- update from CVS
  - a thread race fix
  - fix readdir on invalid dirp

* Wed Jun 19 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-12
- update from CVS
  - don't use __thread in headers
- fix system(3) in threaded apps
- update prelink, so that it is possible to prelink -u libc.so.6.1
  on Alpha

* Fri Jun  7 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-11
- update from CVS
  - fix __moddi3 (#65612, #65695)
  - fix ether_line (#64427)
- fix setvbuf with mmap stdio (#65864)
- --disable-tls for now, waiting for kernel
- avoid duplication of __divtf3 etc. on IA-64
- make sure get*ent_r and _IO_wfile_jumps are exported (#62278)

* Tue May 21 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-10
- update from CVS
  - fix Alpha pthread bug with gcc 3.1

* Fri Apr 19 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-35
- fix nice

* Mon Apr 15 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-34
- add relocation dependencies even for weak symbols (#63422)
- stricter check_fds check for suid/sgid binaries
- run make check at %%install time

* Sat Apr 13 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-33
- handle Dec 31 1969 in mktime for timezones west of GMT (#63369)
- back out do-lookup.h change (#63261, #63305)
- use "memory" clobber instead all the fancy stuff in i386/i686/bits/string.h
  since lots of compilers break on it
- fix sparc build with gcc 3.1
- fix spec file for athlon

* Tue Apr  9 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-32
- fix debugging of threaded apps (#62804)
- fix DST for Estonia (#61494)
- document that pthread_mutexattr_?etkind_np are deprecated
  and pthread_mutexattr_?ettype should be used instead in man
  pages (#61485)
- fix libSegFault.so undefined externals

* Fri Apr  5 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-31
- temporarily disable prelinking ld.so, as some statically linked
  binaries linked against debugging versions of old glibcs die on it
  (#62352)
- fix <semaphore.h> for -std=c99 (#62516)
- fix ether_ntohost segfault (#62397)
- remove in glibc_post_upgrade on i386 all /lib/i686/libc-*.so,
  /lib/i686/libm-*.so and /lib/i686/libpthread-*.so, not just current
  version (#61633)
- prelink -r on alpha too

* Thu Mar 28 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-30
- update GB18030 iconv module (Yu Shao)

* Tue Mar 26 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-29
- features.h fix

* Tue Mar 26 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-28
- update from CVS
  - fix nscd with huge groups
  - fix nis to not close fds it shouldn't
- rebuilt against newer glibc-kernheaders to use the correct
  PATH_MAX
- handle .athlon.rpm glibc the same way as .i686.rpm
- add a couple of .ISO-8859-15 locales (#61922)
- readd temporarily currencies which were superceeded by Euro
  into the list of accepted currencies by localedef to make
  standard conformance testsuites happy
- temporarily moved __libc_waitpid back to make Sun JDK happy
- use old malloc code
- prelink i686/athlon ld.so and prelink -r i686/athlon libc.so

* Thu Mar 14 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-27
- update from CVS
  - fix DST handling for southern hemisphere (#60747)
  - fix daylight setting for tzset (#59951)
  - fix ftime (#60350)
  - fix nice return value
  - fix a malloc segfault
- temporarily moved __libc_wait, __libc_fork and __libc_stack_end
  back to what they used to be exported at
- censorship (#60758)

* Thu Feb 28 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-26
- update from CVS
- use __attribute__((visibility(...))) if supported, use _rtld_local
  for ld.so only objects
- provide libc's own __{,u}{div,mod}di3

* Wed Feb 27 2002 Jakub Jelinek <jakub@redhat.com> 2.2.5-25
- switch back to 2.2.5, mmap stdio needs work

* Mon Feb 25 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-8
- fix two other mmap stdio bugs (#60228)

* Thu Feb 21 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-7
- fix yet another mmap stdio bug (#60145)

* Tue Feb 19 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-6
- fix mmap stdio bug (seen on ld as File truncated error, #60043)
- apply Andreas Schwab's fix for pthread sigwait
- remove /lib/i686/ libraries in glibc_post_upgrade when
  performing i386 glibc install

* Thu Feb 14 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-5
- update to CVS
- added glibc-utils subpackage
- disable autoreq in glibc-debug
- readd %%lang() to locale files

* Fri Feb  7 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-4
- update to CVS
- move glibc private symbols to GLIBC_PRIVATE symbol version

* Wed Jan  9 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-3
- fix a sqrt bug on alpha which caused SHN_UNDEF $__full_ieee754_sqrt..ng
  symbol in libm

* Tue Jan  8 2002 Jakub Jelinek <jakub@redhat.com> 2.2.90-2
- add debug-static package

* Mon Dec 31 2001 Jakub Jelinek <jakub@redhat.com> 2.2.90-1
- update from CVS
- remove -D__USE_STRING_INLINES
- add debug subpackage to trim glibc and glibc-devel size

* Wed Oct  3 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-19
- fix strsep

* Fri Sep 28 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-18
- fix a ld.so bug with duplicate searchlists in l_scope
- fix erfcl(-inf)
- turn /usr/lib/librt.so into linker script

* Wed Sep 26 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-17
- fix a ld.so lookup bug after lots of dlopen calls
- fix CMSG_DATA for non-gcc non-ISOC99 compilers (#53984)
- prelinking support for Sparc64

* Fri Sep 21 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-16
- update from CVS to fix DT_SYMBOLIC
- prelinking support for Alpha and Sparc

* Tue Sep 18 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-15
- update from CVS
  - linuxthreads now retries if -1/EINTR is returned from
    reading or writing to thread manager pipe (#43742)
- use DT_FILTER in librt.so (#53394)
  - update glibc prelink patch so that it handles filters
- fix timer_* with SIGEV_NONE (#53494)
- make glibc_post_upgrade work on PPC (patch from Franz Sirl)

* Mon Sep 10 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-14
- fix build on sparc32
- 2.2.4-13 build for some reason missed some locales
  on alpha/ia64

* Mon Sep  3 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-13
- fix iconvconfig

* Mon Sep  3 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-12
- add fam to /etc/rpc (#52863)
- fix <inttypes.h> for C++ (#52960)
- fix perror

* Mon Aug 27 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-11
- fix strnlen(x, -1)

* Mon Aug 27 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-10
- doh, <bits/libc-lock.h> should only define __libc_rwlock_t
  if __USE_UNIX98.

* Mon Aug 27 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-9
- fix bits/libc-lock.h so that gcc can compile
- fix s390 build

* Fri Aug 24 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-8
- kill stale library symlinks in ldconfig (#52350)
- fix inttypes.h for G++ < 3.0
- use DT_REL*COUNT

* Wed Aug 22 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-7
- fix strnlen on IA-64 (#50077)

* Thu Aug 16 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-6
- glibc 2.2.4 final
- fix -lpthread -static (#51672)

* Fri Aug 10 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-5
- doh, include libio/tst-swscanf.c

* Fri Aug 10 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-4
- don't crash on catclose(-1)
- fix wscanf %[] handling
- fix return value from swprintf
- handle year + %U/%W week + week day in strptime

* Thu Aug  9 2001 Jakub Jelinek <jakub@redhat.com> 2.2.4-3
- update from CVS to
  - fix strcoll (#50548)
  - fix seekdir (#51132)
  - fix memusage (#50606)
- don't make gconv-modules.cache %%config file, just don't verify
  its content.

* Mon Aug  6 2001 Jakub Jelinek <jakub@redhat.com>
- fix strtod and *scanf (#50723, #50724)

* Sat Aug  4 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
  - fix iconv cache handling
- glibc should not own %{_infodir}, %{_mandir} nor %{_mandir}/man3 (#50673)
- add gconv-modules.cache as emtpy config file (#50699)
- only run iconvconfig if /usr is mounted read-write (#50667)

* Wed Jul 25 2001 Jakub Jelinek <jakub@redhat.com>
- move iconvconfig from glibc-common into glibc subpackage,
  call it from glibc_post_upgrade instead of common's post.

* Tue Jul 24 2001 Jakub Jelinek <jakub@redhat.com>
- turn off debugging printouts in iconvconfig

* Tue Jul 24 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
  - fix IA-32 makecontext
  - make fflush(0) thread-safe (#46446)

* Mon Jul 23 2001 Jakub Jelinek <jakub@redhat.com>
- adjust prelinking DT_* and SHT_* values in elf.h
- update from CVS
  - iconv cache
  - make iconv work in SUID/SGID programs (#34611)

* Fri Jul 20 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
  - kill non-pic code in libm.so
  - fix getdate
  - fix some locales (#49402)
- rebuilt with binutils-2.11.90.0.8-5 to place .interp section
  properly in libBrokenLocale.so, libNoVersion.so and libanl.so
- add floating stacks on IA-64, Alpha, Sparc (#49308)

* Mon Jul 16 2001 Jakub Jelinek <jakub@redhat.com>
- make /lib/i686 directory owned by glibc*.i686.rpm

* Mon Jul  9 2001 Jakub Jelinek <jakub@redhat.com>
- remove rquota.[hx] headers which are now provided by quota (#47141)
- add prelinking patch

* Thu Jul  5 2001 Jakub Jelinek <jakub@redhat.com>
- require sh-utils for nscd

* Mon Jun 25 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS (#43681, #43350, #44663, #45685)
- fix ro_RO bug (#44644)

* Wed Jun  6 2001 Jakub Jelinek <jakub@redhat.com>
- fix a bunch of math bugs (#43210, #43345, #43346, #43347, #43348, #43355)
- make rpc headers -ansi compilable (#42390)
- remove alphaev6 optimized memcpy, since there are still far too many
  broken apps which call memcpy where they should call memmove
- update from CVS to (among other things):
  - fix tanhl bug (#43352)

* Tue May 22 2001 Jakub Jelinek <jakub@redhat.com>
- fix #include <signal.h> with -D_XOPEN_SOURCE=500 on ia64 (#35968)
- fix a dlclose reldeps handling bug
- some more profiling fixes
- fix tgmath.h

* Thu May 17 2001 Jakub Jelinek <jakub@redhat.com>
- make ldconfig more quiet
- fix LD_PROFILE on i686 (#41030)

* Wed May 16 2001 Jakub Jelinek <jakub@redhat.com>
- fix the hardlink program, so that it really catches all files with
  identical content
- add a s390x clone fix

* Wed May 16 2001 Jakub Jelinek <jakub@redhat.com>
- fix rpc for non-threaded apps using svc_fdset and similar variables (#40409)
- fix nss compatibility DSO versions for alphaev6
- add a hardlink program instead of the shell 3x for plus cmp -s/link
  which takes a lot of time during build
- rework BuildPreReq and Conflicts with gcc, so that
  it applies only where it has to

* Fri May 11 2001 Jakub Jelinek <jakub@redhat.com>
- fix locale name of ja_JP in UTF-8 (#39783)
- fix re_search_2 (#40244)
- fix memusage script (#39138, #39823)
- fix dlsym(RTLD_NEXT, ) from main program (#39803)
- fix xtrace script (#39609)
- make glibc conflict with glibc-devel 2.2.2 and below (to make sure
  libc_nonshared.a has atexit)
- fix getconf LFS_CFLAGS on 64bitters
- recompile with gcc-2.96-84 or above to fix binary compatibility problem
  with __frame_state_for function (#37933)

* Fri Apr 27 2001 Jakub Jelinek <jakub@redhat.com>
- glibc 2.2.3 release
  - fix strcoll (#36539)
- add BuildPreReqs (#36378)

* Wed Apr 25 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS

* Fri Apr 20 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
  - fix sparc64, ia64
  - fix some locale syntax errors (#35982)

* Wed Apr 18 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS

* Wed Apr 11 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS

* Fri Apr  6 2001 Jakub Jelinek <jakub@redhat.com>
- support even 2.4.0 kernels on ia64, sparc64 and s390x
- include UTF-8 locales
- make gconv-modules %%config(noreplace)

* Fri Mar 23 2001 Jakub Jelinek <jakub@redhat.com>
- back out sunrpc changes

* Wed Mar 21 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
  - fix ia64 build
  - fix pthread_getattr_np

* Fri Mar 16 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
  - run atexit() registered functions at dlclose time if they are in shared
    libraries (#28625)
  - add pthread_getattr_np API to make JVM folks happy

* Wed Mar 14 2001 Jakub Jelinek <jakub@redhat.com>
- require 2.4.1 instead of 2.4.0 on platforms where it required 2.4 kernel
- fix ldd behaviour on unresolved symbols
- remove nonsensical ldconfig warning, update osversion for the most
  recent library with the same soname in the same directory instead (#31703)
- apply selected patches from CVS
- s390x spec file changes from Florian La Roche

* Wed Mar  7 2001 Jakub Jelinek <jakub@redhat.com>
- fix gencat (#30894)
- fix ldconfig changes from yesterday, fix LD_ASSUME_KERNEL handling

* Tue Mar  6 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
- make pthread_attr_setstacksize consistent before and after pthread manager
  is started (#28194)
- pass back struct sigcontext from pthread signal wrapper (on ia32 only so
  far, #28493)
- on i686 ship both --enable-kernel 2.2.5 and 2.4.0 libc/libm/libpthread,
  make ld.so pick the right one

* Sat Feb 17 2001 Preston Brown <pbrown@redhat.com>
- glib-common doesn't require glibc, until we can figure out how to get out of dependency hell.

* Sat Feb 17 2001 Jakub Jelinek <jakub@redhat.com>
- make glibc require particular version of glibc-common
  and glibc-common prerequire glibc.

* Fri Feb 16 2001 Jakub Jelinek <jakub@redhat.com>
- glibc 2.2.2 release
  - fix regex REG_ICASE bug seen in ksymoops

* Sat Feb 10 2001 Jakub Jelinek <jakub@redhat.com>
- fix regexec leaking memory (#26864)

* Fri Feb  9 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
  - fix ia64 build with gnupro
  - make regex 64bit clean
  - fix tgmath make check failures on alpha

* Tue Feb  6 2001 Jakub Jelinek <jakub@redhat.com>
- update again for ia64 DF_1_INITFIRST

* Fri Feb  2 2001 Jakub Jelinek <jakub@redhat.com>
- update from CVS
  - fix getaddrinfo (#25437)
  - support DF_1_INITFIRST (#25029)

* Wed Jan 24 2001 Jakub Jelinek <jakub@redhat.com>
- build all auxiliary arches with --enablekernel 2.4.0, those wanting
  to run 2.2 kernels can downgrade to the base architecture glibc.

* Sat Jan 20 2001 Jakub Jelinek <jakub@redhat.com>
- remove %%lang() flags from %%{_prefix}/lib/locale files temporarily

* Sun Jan 14 2001 Jakub Jelinek <jakub@redhat.com>
- update to 2.2.1 final
  - fix a pthread_kill_other_threads_np breakage (#23966)
  - make static binaries using dlopen work on ia64 again
- fix a typo in glibc-common group

* Wed Jan 10 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- devel requires glibc = %%{version}
- noreplace /etc/nscd.conf

* Wed Jan 10 2001 Jakub Jelinek <jakub@redhat.com>
- some more security fixes:
  - don't look up LD_PRELOAD libs in cache for SUID apps
    (because that bypasses SUID bit checking on the library)
  - place output files for profiling SUID apps into /var/profile,
    use O_NOFOLLOW for them
  - add checks for $MEMUSAGE_OUTPUT and $SEGFAULT_OUTPUT_NAME
- hardlink identical locale files together
- add %%lang() tags to locale stuff
- remove ko_KR.utf8 for now, it is provided by locale-utf8 package

* Mon Jan  8 2001 Jakub Jelinek <jakub@redhat.com>
- add glibc-common subpackage
- fix alphaev6 memcpy (#22494)
- fix sys/cdefs.h (#22908)
- don't define stdin/stdout/stderr as macros for -traditional (#22913)
- work around a bug in IBM JDK (#22932, #23012)
- fix pmap_unset when network is down (#23176)
- move nscd in rc.d before netfs on shutdown
- fix $RESOLV_HOST_CONF in SUID apps (#23562)

* Fri Dec 15 2000 Jakub Jelinek <jakub@redhat.com>
- fix ftw and nftw

* Wed Dec 13 2000 Jakub Jelinek <jakub@redhat.com>
- fix fcvt (#22184)
- ldd /lib/ld-linux.so.2 is not crashing any longer again (#22197)
- fix gencat

* Mon Dec 11 2000 Jakub Jelinek <jakub@redhat.com>
- fix alpha htonl and alphaev6 stpcpy

* Sat Dec  9 2000 Jakub Jelinek <jakub@redhat.com>
- update to CVS to:
  - fix getnameinfo (#21934)
  - don't stomp on memory in rpath handling (#21544)
  - fix setlocale (#21507)
- fix libNoVersion.so.1 loading code (#21579)
- use auxarches define in spec file for auxiliary
  architectures (#21219)
- remove /usr/share directory from filelist (#21218)

* Sun Nov 19 2000 Jakub Jelinek <jakub@redhat.com>
- update to CVS to fix getaddrinfo

* Fri Nov 17 2000 Jakub Jelinek <jakub@redhat.com>
- update to CVS to fix freopen
- remove all alpha workarounds, not needed anymore

* Wed Nov 15 2000 Jakub Jelinek <jakub@redhat.com>
- fix dladdr bug on alpha/sparc32/sparc64
- fix Makefiles so that they run static tests properly

* Tue Nov 14 2000 Jakub Jelinek <jakub@redhat.com>
- update to CVS to fix ldconfig

* Thu Nov  9 2000 Jakub Jelinek <jakub@redhat.com>
- update to glibc 2.2 release

* Mon Nov  6 2000 Jakub Jelinek <jakub@redhat.com>
- update to CVS to:
  - export __sysconf@@GLIBC_2.2 (#20417)

* Fri Nov  3 2000 Jakub Jelinek <jakub@redhat.com>
- merge to 2.1.97

* Mon Oct 30 2000 Jakub Jelinek <jakub@redhat.com>
- update to CVS, including:
  - fix WORD_BIT/LONG_BIT definition in limits.h (#19088)
  - fix hesiod (#19375)
  - set LC_MESSAGES in zic/zdump for proper error message output (#19495)
  - fix LFS fcntl when used with non-LFS aware kernels (#19730)

* Thu Oct 19 2000 Jakub Jelinek <jakub@redhat.com>
- fix alpha semctl (#19199)
- update to CVS, including:
  - fix glibc headers for Compaq non-gcc compilers
  - fix locale alias handling code (#18832)
  - fix rexec on little endian machines (#18886)
- started writing changelog again

* Thu Aug 10 2000 Adrian Havill <havill@redhat.com>
- added ja ujis alias for backwards compatibility
