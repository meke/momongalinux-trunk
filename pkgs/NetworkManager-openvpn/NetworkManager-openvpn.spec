%global momorel 2
%define nm_version          0.9.8.10
%define dbus_version        1.1
%define gtk2_version        2.10.0
%define openvpn_version     2.1
%define shared_mime_version 0.16-3

%define snapshot %{nil}
%define realversion 0.9.8.4

Summary: NetworkManager VPN integration for OpenVPN
Name: NetworkManager-openvpn
Version: 0.9.8.4
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.gnome.org/projects/NetworkManager/
Group: System Environment/Base
Source0: http://ftp.gnome.org/pub/GNOME/sources/NetworkManager-openvpn/0.9/%{name}-%{realversion}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel                 >= %{gtk2_version}
BuildRequires: dbus-devel                 >= %{dbus_version}
BuildRequires: NetworkManager-devel       >= %{nm_version}
BuildRequires: NetworkManager-glib-devel  >= %{nm_version}
BuildRequires: glib2-devel
BuildRequires: GConf2-devel
BuildRequires: libgnomeui-devel
BuildRequires: gnome-keyring-devel
BuildRequires: libglade2-devel
BuildRequires: libpng-devel
BuildRequires: perl-XML-Parser
BuildRequires: libtool intltool gettext
BuildRequires: perl
BuildRequires: gnome-common
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires: gtk2             >= %{gtk2_version}
Requires: dbus             >= %{dbus_version}
Requires: NetworkManager   >= %{nm_version}
Requires: openvpn          >= %{openvpn_version}
Requires: shared-mime-info >= %{shared_mime_version}
Requires: GConf2
Requires: gnome-keyring

%description
This package contains software for integrating the OpenVPN VPN software
with NetworkManager and the GNOME desktop.

%prep
%setup -q 

%build
CFLAGS="$RPM_OPT_FLAGS -Wno-error=deprecated-declarations" \
%configure --disable-static --disable-dependency-tracking
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"

rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.la

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
/usr/bin/update-desktop-database %{_datadir}/applications &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
      /usr/bin/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
/sbin/ldconfig
/usr/bin/update-desktop-database %{_datadir}/applications &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
      /usr/bin/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS ChangeLog README
%{_libdir}/NetworkManager/lib*.so*
%{_libexecdir}/nm-openvpn-auth-dialog
%{_sysconfdir}/dbus-1/system.d/nm-openvpn-service.conf
%{_sysconfdir}/NetworkManager/VPN/nm-openvpn-service.name
%{_libexecdir}/nm-openvpn-service
%{_libexecdir}/nm-openvpn-service-openvpn-helper
# %{_datadir}/gnome-vpn-properties/openvpn/nm-openvpn-dialog.glade
%{_datadir}/gnome-vpn-properties/openvpn/nm-openvpn-dialog.ui

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.4-2m)
- rebuild against NetworkManager

* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.4-1m)
- update to 0.9.8.4

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.2-1m)
- update to 0.9.8.2

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6.0-1m)
- update 0.9.6.0

* Sun Jul 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.5.95-2m)
- add -Wno-error=deprecated-declarations option

* Sun Jul 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.5.95-1m)
- update 0.9.5.95
- remove 'fedora' value

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3.997-1m)
- reimport from fedora

* Sun Mar 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4.0-1m)
- update 0.9.4.0

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3.995-1m)
- update 0.9.3.995

* Thu Nov 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2.0-1m)
- update 0.9.2.0

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1.95-1m)
- update 0.9.1.95

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update to 0.9.0

* Sun Jul 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.9997-1m)
- update to 0.8.9997

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.999-1m)
- update to 0.8.999

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.4-1m)
- update 0.8.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- update 0.8.2

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-4m)
- build fix

* Thu Sep  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-3m)
- [BUG FIX] fix %%post and %%postun

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1

* Mon May 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0.997-1m)
- update 0.8.0.997

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-1m)
- update 0.8

* Fri Nov 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-1m)
- update 0.7.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- add autoreconf. need libtool-2.2.x

* Tue Apr 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0.99-1m)
- update 0.7.0.99

* Mon Mar  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0.97-1m)
- update 0.7.0.97

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against rpm-4.6

* Sun Dec 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- update 0.7.0-release

* Sun Nov 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.svn4326.1m)
- update svn4326

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.svn4027.1m)
- update svn4027

* Sat Jul 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.svn3846.1m)
- update svn3846

* Sun May 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.0-9.svn3549.2m)
- modify Requires

* Sat May 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.0-9.svn3549.1m)
- import from Fedora

* Wed Apr 09 2008 Dan Williams <dcbw@redhat.com> 1:0.7.0-9.svn3549
- Update for compat with new NM bits

* Mon Mar 03 2008 Tim Niemueller <tim@niemueller.de> 1:0.7.0-9.svn3302
- Mute %post and %postun scripts

* Fri Feb 08 2008 Tim Niemueller <tim@niemueller.de> 1:0.7.0-8.svn3302
- Update to latest SVN snapshot
- Fixes rhbz#429816 (port was not saved correctly)
- Respects DNS search string from OpenVPN server

* Fri Jan 18 2008 Tim Niemueller <tim@niemueller.de> 1:0.7.0-7.svn3169
- Use install -p during "make install" to fix #342701

* Thu Dec 13 2007 Tim Niemueller <tim@niemueller.de> 1:0.7.0-6.svn3169
- Update to latest SVN snapshot

* Thu Dec  6 2007 Dan Williams <dcbw@redhat.com> 1:0.7.0-5.svn3140
- Update to latest SVN snapshot to get stuff working

* Fri Nov 23 2007 Tim Niemueller <tim@niemueller.de> 1:0.7.0-4.svn3047
- BuildRequire libtool and glib2-devel since we call autogen.sh now

* Fri Nov 23 2007 Tim Niemueller <tim@niemueller.de> 1:0.7.0-3.svn3047
- Fixed #320941
- Call autogen, therefore BuildRequire gnome-common
- Use plain 3047 from repo and use a patch, we cannot use trunk at the
  moment since it is in flux and incompatible with NM available for F8

* Wed Oct 31 2007 Tim Niemueller <tim@niemueller.de> 1:0.7.0-2.svn3047.fc8
- BuildRequire gettext

* Tue Oct 30 2007 Tim Niemueller <tim@niemueller.de> 1:0.7.0-1.svn3047.fc8
- Upgrade to trunk, needed to be compatible with NM 0.7.0, rebuild for F-8

* Fri Sep 15 2006 Tim Niemueller <tim@niemueller.de> 0.3.2-7
- Rebuild for FC6

* Sat Aug 19 2006 Tim Niemueller <tim@niemueller.de> 0.3.2-5
- Added perl-XML-Parser as a build requirement, needed for intltool

* Tue Aug 15 2006 Tim Niemueller <tim@niemueller.de> 0.3.2-4
- Added instructions how to build the source package
- removed a rm line

* Wed Aug 09 2006 Tim Niemueller <tim@niemueller.de> 0.3.2-3
- Added URL

* Fri Aug 04 2006 Tim Niemueller <tim@niemueller.de> 0.3.2-2
- Upgrade to current upstream version (0.3.2 on 0.6 branch)

* Mon Jul 10 2006 Tim Niemueller <tim@niemueller.de> 0.3.2-1
- Upgraded to 0.3.2 for 0.6 branch

* Tue Dec 06 2005 Tim Niemueller <tim@niemueller.de> 0.3-1
- Initial revision based on NetworkManager-vpnc spec

