%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-bisect
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        OCaml code coverage tool

Group:          Development/Libraries
License:        GPLv3+
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

URL:            http://bisect.x9c.fr/
Source0:        http://bisect.x9c.fr/distrib/bisect-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-Makefile.patch

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel

%global __ocaml_requires_opts -i Asttypes -i Parsetree


%description
Bisect is a code coverage tool for the Objective Caml language. It is
a camlp4-based tool that allows to instrument your application before
running tests. After application execution, it is possible to generate
a report in HTML format that is the replica of the application source
code annotated with code coverage information.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n bisect-%{version}
%patch0 -p1


%build
make PATH_OCAML_PREFIX=%{_prefix} all


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_libdir}/ocaml/bisect

install -m 0755 _build/src/report/report.byte \
  $RPM_BUILD_ROOT%{_bindir}/bisect-report
#strip $RPM_BUILD_ROOT%%{_bindir}/bisect-report

install -m 0755 _build/src/report/report.native \
  $RPM_BUILD_ROOT%{_bindir}/bisect-report.opt
strip $RPM_BUILD_ROOT%{_bindir}/bisect-report.opt

install -m 0644 _build/src/library/*.{cmi,cmo,cmx} \
  $RPM_BUILD_ROOT%{_libdir}/ocaml/bisect

install -m 0644 _build/src/report/*.{cmi,cmo,cmx} \
  $RPM_BUILD_ROOT%{_libdir}/ocaml/bisect

install -m 0644 _build/src/syntax/*.{cmi,cmo} \
  $RPM_BUILD_ROOT%{_libdir}/ocaml/bisect

install -m 0644 _build/src/threads/*.{cmi,cmo,cmx} \
  $RPM_BUILD_ROOT%{_libdir}/ocaml/bisect

install -m 0644 _build/*.{a,cmi,cmo,cmx,cma,cmxa} \
  $RPM_BUILD_ROOT%{_libdir}/ocaml/bisect


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/bisect-report
%{_bindir}/bisect-report.opt
%{_libdir}/ocaml/bisect
%if %opt
%exclude %{_libdir}/ocaml/bisect/*.a
%exclude %{_libdir}/ocaml/bisect/*.cmxa
%exclude %{_libdir}/ocaml/bisect/*.cmx
%endif


%files devel
%defattr(-,root,root,-)
%doc CHANGES COPYING README VERSION doc/bisect.pdf ocamldoc
%if %opt
%{_libdir}/ocaml/bisect/*.a
%{_libdir}/ocaml/bisect/*.cmxa
%{_libdir}/ocaml/bisect/*.cmx
%endif


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- update to 1.0
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.1.1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-0.4.alpha
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Nov 26 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0-0.3.alpha
- Rebuild for OCaml 3.11.0+rc1.

* Wed Nov 19 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0-0.2.alpha
- Rebuild for OCaml 3.11.0

* Sun Aug 24 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0-0.1.alpha
- Initial RPM release.
