%global momorel 1

%global xfce4ver 4.10.0
%global tunarver 1.4.0

%global major 0.6

Name:           ristretto
Version:        0.6.3
Release:        %{momorel}m%{?dist}
Summary:        Image-viewer for the Xfce desktop environment

Group:          User Interface/Desktops
License:        GPL
URL:            http://goodies.xfce.org/projects/applications/ristretto/
Source0:        http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  Thunar-devel >= %{tunarver}
BuildRequires:  libexif-devel >= 0.6.16
BuildRequires:  dbus-devel >= 1.0.2
BuildRequires:  desktop-file-utils, gettext
# Requires:       shared-mime-info

%description
Ristretto is a fast and lightweight image-viewer for the Xfce desktop 
environment.

%prep
%setup -q

%build
%configure LIBS="-lX11"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}
desktop-file-install --vendor=                                  \
        --dir %{buildroot}%{_datadir}/applications              \
        --add-mime-type=image/tiff                              \
        --add-mime-type=image/x-bmp                             \
        --add-mime-type=image/x-png                             \
        --add-mime-type=image/x-pcx                             \
        --add-mime-type=image/x-tga                             \
        --add-mime-type=image/xpm                               \
        %{buildroot}%{_datadir}/applications/%{name}.desktop
#        --delete-original                                       \

%post
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.3-1m)
- update
- build against xfce4-4.10.0

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.93-1m)
- update to 0.0.93
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.22-6m)
- rebuild for new GCC 4.6

* Wed Feb 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.22-5m)
- no NoSource

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.22-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.22-3m)
- full rebuild for mo7 release

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.22-2m)
- rebuild against xfce4 4.6.2

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.22-1m)
- update to 0.0.22

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.21-3m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.21-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.21-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20-1m)
- update 0.0.20

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.14-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.14-1m)
- import to Momonga from fedora
- update to 0.0.14

* Tue Nov 20 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.12-1
- Update to 0.0.12

* Wed Nov 07 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.11-1
- Update to 0.0.11
- BuildRequire dbus-glib-devel

* Sat Nov 03 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.10-1
- Update to 0.0.10
- Add more mimetypes: tiff, x-bmp, x-png, x-pcx, x-tga and xpm
- Correct build requirements

* Fri Oct 19 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.9-1
- Update to 0.0.9

* Sun Oct 14 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.8-1
- Update to 0.0.8

* Sun Sep 30 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.7-1
- Update to 0.0.7

* Sun Sep 09 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.4-1
- Update to 0.0.4

* Wed Sep 05 2007 Christoph Wickert <fedora christoph-wickert de> - 0.0.3-1
- Initial RPM package
