%global momorel 6

%define fontname sil-doulos
%define archivename DoulosSIL
%define docversion 4.100

Name:           %{fontname}-fonts
Version:        4.104
Release:        %{momorel}m%{?dist}
Summary:        Doulos SIL fonts

Group:          User Interface/X
License:        OFL
URL:            http://scripts.sil.org/DoulosSILFont
Source0:        %{archivename}%{version}.zip
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

# Obsoleting and providing the old RPM name
Obsoletes:      doulos-fonts < 4.104-2

%description
Doulos SIL provides glyphs for a wide range of Latin and Cyrillic
characters. Doulos's design is similar to the design of the Times-like
fonts, but only has a single regular face. It is intended for use alongside
other Times-like fonts where a range of styles (italic, bold) are not
needed.


%prep
%setup -q -n %{archivename}
sed -i 's/\r$//' *.txt


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}


%clean
rm -fr %{buildroot}


%_font_pkg *.ttf

%doc FONTLOG.txt OFL.txt OFL-FAQ.txt README.txt
%doc DoulosSIL%{docversion}FontDocumentation.pdf

%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.104-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.104-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.104-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.104-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.104-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.104-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.104-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jan 29 2009 Roozbeh Pournader <roozbeh@gmail.com> - 4.104-3
- Cleanup, remove Provides (Nicholas Mailhot)

* Thu Jan 29 2009 Roozbeh Pournader <roozbeh@gmail.com> - 4.104-2
- Update to new F11 fonts policy, renaming from doulos-fonts

* Tue Jan 13 2009 Roozbeh Pournader <roozbeh@gmail.com> - 4.104-1
- Update main font to 4.104 (with Unicode 5.1 support)
- Remove Doulos Literacy (not maintained anymore)
- Last update before conversion to new fonts policy

* Tue Dec 11 2007 Roozbeh Pournader <roozbeh@farsiweb.info> - 4.100-1
- Update main font to 4.100, keep old Doulos Literacy which is latest
- Change license tag to OFL

* Thu Nov 09 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 4.0.14-4
- Include Doulos Literacy
- Don't include font cache files (fontconfig mechanism is changed)

* Wed Nov 08 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 4.0.14-3
- Add dist tag

* Wed Nov 08 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 4.0.14-2
- Rebuild to make package available in Rawhide again

* Sat Feb 18 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 4.0.14-1
- Initial packaging, based on gentium-fonts
