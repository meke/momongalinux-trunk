%global momorel 6

%global plugdir %{_libdir}/gkrellm2/plugins
%global datadir %{_datadir}/gkrellm2

Name:           gkrellm-weather
Version:        2.0.7
Release:        %{momorel}m%{?dist}
Summary:        Weather plugin for GKrellM

Group:          Applications/System
License:        GPLv2+
URL:            http://makovick.googlepages.com/gkrellmplugins
Source0:        http://makovick.googlepages.com/gkrellweather-%{version}.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Provides:       gkrellweather = %{version}-%{release}
BuildRequires:  perl
BuildRequires:  gtk2-devel
BuildRequires:  gkrellm-devel >= 2
BuildRequires:  gkrellm >= 2
BuildRequires:  gettext
Requires:       gkrellm >= 2

%description
%{summary}.


%prep
%setup -q -n gkrellweather-%{version}
perl -pi -e \
  's|/usr/.+/GrabWeather\b|%{datadir}/GrabWeather| ;
   s|PREFIX "/bin/GrabWeather|"%{datadir}/GrabWeather| ' \
  gkrellweather.c
perl -pi -e \
  "s|^(CFLAGS\\s*=.+)\$|\$1 $RPM_OPT_FLAGS|" \
  Makefile


%build
CFLAGS="$RPM_OPT_FLAGS" make %{?_smp_mflags} enable_nls=1


%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 755 gkrellweather.so $RPM_BUILD_ROOT%{plugdir}/gkrellweather.so
install -Dpm 755 GrabWeather $RPM_BUILD_ROOT%{datadir}/GrabWeather
make install INSTALL_PREFIX=$RPM_BUILD_ROOT LOCALEDIR=%{_datadir}/locale \
  -C po enable_nls=1
%find_lang gkrellweather


%clean
rm -rf $RPM_BUILD_ROOT


%files -f gkrellweather.lang
%defattr(-,root,root,-)
%doc ChangeLog COPYING README
%{datadir}/GrabWeather
%{plugdir}/gkrellweather.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-2m)
- rebuild against rpm-4.6

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-1m)
- import from Fedora to Momonga

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.0.7-6
- Autorebuild for GCC 4.3

* Sat Feb 16 2008 Adam Goode <adam@spicenitz.org> - 2.0.7-5
- Take orphaned package
- Update URL

* Thu Aug 16 2007 Ville Skytta <ville.skytta at iki.fi> - 2.0.7-4
- License: GPLv2+

* Wed Aug 30 2006 Ville Skytta <ville.skytta at iki.fi> - 2.0.7-3
- Rebuild.

* Wed Feb 15 2006 Ville Skytta <ville.skytta at iki.fi> - 2.0.7-2
- Rebuild, cosmetics.

* Thu Mar 17 2005 Ville Skytta <ville.skytta at iki.fi> - 2.0.7-1
- 2.0.7.
- Provide upstream name.

* Fri Nov 19 2004 Ville Skytta <ville.skytta at iki.fi> - 0:2.0.6-2
- Clean up specfile and convert to UTF-8.

* Sun Apr 13 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.0.6-0.fdr.1
- First Fedora release.
