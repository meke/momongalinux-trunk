#! /bin/sh

`rpm -E %__find_provides` "$@" | egrep -v "(libX11|libXrender|libXext)\.so"
