%global momorel 1

%{!?_x_libraries:%global _x_libraries %{_libdir}}
%{!?_x_bindir:%global _x_bindir %{_bindir}}

%define _pkglibdir %{_libdir}/nx
%define _pkgdatadir %{_datadir}/nx
%define _pkglibexecdir %{_libexecdir}/nx

Summary:	Proxy system for X11
Name:		nx
Version:	3.5.0
Release:	%{momorel}m%{?dist}
# MIT on the X11 bits
License:	GPLv2 and MIT
Group:		Applications/Internet
URL:		http://www.nomachine.com
# Compression Libraries and Proxy Sources
Source0:	http://64.34.173.142/download/%{version}/sources/nxproxy-%{version}-1.tar.gz
Source1:	http://64.34.173.142/download/%{version}/sources/nxcomp-%{version}-2.tar.gz
Source2:	http://64.34.173.142/download/%{version}/sources/nxcompext-%{version}-1.tar.gz
Source3:	http://64.34.173.142/download/%{version}/sources/nxssh-%{version}-2.tar.gz
# Shadowing Libraries
Source4:	http://64.34.173.142/download/%{version}/sources/nxcompshad-%{version}-2.tar.gz
# X11 Support Programs and Libraries
Source5:	http://64.34.173.142/download/%{version}/sources/nx-X11-%{version}-2.tar.gz
Source6:	http://64.34.173.142/download/%{version}/sources/nxauth-%{version}-1.tar.gz
# X11 Agent Sources
Source7:	http://64.34.173.142/download/%{version}/sources/nxagent-%{version}-9.tar.gz
# NX Example Scripts
Source8:	http://64.34.173.142/download/%{version}/sources/nxscripts-%{version}-1.tar.gz
Source9:	nxwrapper.in
Source10:	docs.tar.bz2
Source11:	nxfind-provides.sh
NoSource:	0
NoSource:	1
NoSource:	2
NoSource:	3
NoSource:	4
NoSource:	5
NoSource:	6
NoSource:	7
NoSource:	8

%define _use_internal_dependency_generator 0
%define __find_provides sh %{SOURCE11}

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gcc-c++ 
BuildRequires:	libstdc++-devel 
BuildRequires:	momonga-release
BuildRequires:	expat-devel 
BuildRequires:	audiofile-devel 
BuildRequires:	openssl-devel >= 1.0.0
BuildRequires:	libjpeg-devel >= 8a
BuildRequires:	libpng-devel
BuildRequires:	libXt-devel 
BuildRequires:	xorg-x11-proto-devel 
BuildRequires:	libXp-devel 
BuildRequires:	imake
BuildRequires:	libXdamage-devel 
BuildRequires:	libXrandr-devel 
BuildRequires:	libXtst-devel
# Better mention what we really require on a file basis.
# Requires:	xorg-x11-utils

# Fails with
# Core.o: relocation R_X86_64_32S against `vtable for CorePoller' can not be used when making a shared object; recompile with -fPIC
# but Core.o has been built with -fPIC :(
#ExcludeArch: x86_64

%description
NX provides a proxy system for the X Window System.

%prep
%setup -q -c %{name}-%{version} -T -a0 -a1 -a2 -a3 -a4 -a5 -a6 -a7 -a8
perl -pi -e's,-L/usr/X11R6/lib,-L%{_x_libraries},' nxcompshad/Makefile.in
export CFLAGS="%{optflags}"
%ifarch x86_64 ppc64
grep -rl -- -fPIC . | xargs perl -pi -e's,-fPIC,-fPIC -DPIC,'
export CFLAGS="$CFLAGS -fPIC -DPIC"
%endif
cat >> nx-X11/config/cf/host.def << EOF
#ifdef i386Architecture
#undef DefaultGcc2i386Opt
#define DefaultGcc2i386Opt $CFLAGS -fno-strict-aliasing
#endif
#ifdef AMD64Architecture
#undef DefaultGcc2AMD64Opt $CFLAGS -fno-strict-aliasing
#endif
#define DefaultGcc2AMD64Opt
#ifdef MipsArchitecture
#undef DefaultGcc2MipsOpt
#define DefaultGcc2MipsOpt $CFLAGS -fno-strict-aliasing
#endif
#ifdef s390xArchitecture
#undef OptimizedCDebugFlags
#define OptimizedCDebugFlags $CFLAGS -fno-strict-aliasing
#endif
EOF
export CXXFLAGS="$CFLAGS"
perl -pi -e"s|CXXFLAGS=.-O.*|CXXFLAGS=\"$CXXFLAGS\"|" */configure

%build
export CFLAGS="%{optflags}"
%ifarch x86_64 ppc64
export CFLAGS="$CFLAGS -fPIC -DPIC"
%endif
export CXXFLAGS="$CFLAGS"

# The commented parts show how the build would proceed step by step.
# This information is important in case someone wants to split this package
# (which would be the proper thing to do).
# Within the commented area the make World invocation does all for
# you. It isn't placed by accident in the middle of the commented
# build instructions, as this is where the X11 libs would be built

# build Compression Library and Proxy
for i in nxcomp nxcompshad nxproxy; do
  pushd $i; ./configure; make CCFLAGS="$CFLAGS"; popd
done
# build X11 Support Libraries and Agents
make -C nx-X11 World
%if 0
# build Extended Compression Library
pushd nxcompext
  ./configure; make
popd
%endif
pushd nxssh
./configure --without-zlib-version-check; make
popd
pushd nxproxy
./configure
make CCFLAGS="$CFLAGS"
popd

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_pkglibdir}
mkdir -p %{buildroot}%{_pkglibexecdir}
mkdir -p %{buildroot}%{_mandir}/man1

# install X11 Support Libraries and Agents
install -p -m 0755 nx-X11/lib/X11/libX11.so.* \
   nx-X11/lib/Xext/libXext.so.* \
   nx-X11/lib/Xrender/libXrender.so.* \
   %{buildroot}%{_pkglibdir}
install -p -m 0755 nx-X11/programs/Xserver/nxagent \
  %{buildroot}%{_pkglibexecdir}
# nxwrapper
sed -e's,@PKGLIBDIR@,%{_pkglibdir},g' \
    -e's,@PKGLIBEXECDIR@,%{_pkglibexecdir},g' \
  < %{SOURCE9} > %{buildroot}%{_pkglibexecdir}/nxwrapper
chmod 0755 %{buildroot}%{_pkglibexecdir}/nxwrapper
ln -s %{_pkglibexecdir}/nxwrapper %{buildroot}%{_bindir}/nxagent
# install Compression Libraries and Proxy
install -p -m 0755 nxcomp/libXcomp.so.* %{buildroot}%{_pkglibdir}
install -p -m 0755 nxcompext/libXcompext.so.* %{buildroot}%{_pkglibdir}
install -p -m 0755 nxcompshad/libXcompshad.so.* %{buildroot}%{_pkglibdir}
install -p -m 0755 nxssh/nxssh %{buildroot}%{_pkglibexecdir}
ln -s %{_pkglibexecdir}/nxwrapper %{buildroot}%{_bindir}/nxssh
install -p -m 0755 nxproxy/nxproxy %{buildroot}%{_pkglibexecdir}
ln -s %{_pkglibexecdir}/nxwrapper %{buildroot}%{_bindir}/nxproxy
# install scripts
mkdir -p %{buildroot}%{_defaultdocdir}/%{name}-%{version}
cp -a nxscripts %{buildroot}%{_defaultdocdir}/%{name}-%{version}
# documentation and license
tar xjf %{SOURCE10} -C %{buildroot}%{_defaultdocdir}/%{name}-%{version}
install -m 644 nxcomp/LICENSE %{buildroot}%{_defaultdocdir}/%{name}-%{version}
for i in nxcomp; do
  mkdir %{buildroot}%{_defaultdocdir}/%{name}-%{version}/$i
done
install -p -m 0644 nxcomp/README \
  %{buildroot}%{_defaultdocdir}/%{name}-%{version}/nxcomp/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc %{_defaultdocdir}/%{name}-%{version}
%{_bindir}/*
%{_pkglibdir}
%{_pkglibexecdir}

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-1m)
- update to 3.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.0-4m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-3m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-2m)
- rebuild against openssl-1.0.0

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-1m)
- update to 3.4.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-5m)
- apply glibc210 patch

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0-4m)
- rebuild against libjpeg-7

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-2m)
- rebuild against rpm-4.6

* Fri Nov  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- import from Fedora devel

* Tue Aug 26 2008 TOm "spot" Callaway <tcallawa@redhat.com> - 3.2.0-31
- fix license tag

* Mon Aug 25 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 3.2.0-30
- Update nxagent to fix some keyboard mapping on 64 bits and broken
  repaint on ADSL type lines.

* Tue Aug 12 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 3.2.0-29
- Remove old patches.

* Sun Aug 10 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 3.2.0-28
- Upstream updates to nxcomp, nxcompshad, nx-X11, nxagent.

* Sat Apr 12 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 3.2.0-27
- Update to 3.2.0.

* Sat Apr  5 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 3.1.0-26
- Update to maintenance release #2.
- Add nxssh.
- mv nxwrapper to the libexec dir.

* Wed Jan  2 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 3.1.0-25
- Update to 3.1.0.
- add nxcompshad, nxauth; remove nxviewer, nxdesktop.
- add -fPIC for ppc64.
- Propagate %%{optflags} for x86_64, too.

* Fri Jun  1 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 2.1.0-22
- Sync with ATrpms' nxfindprovides helper.

* Wed May 23 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 2.1.0-21.1
- Fix typo in nxwrapper.in (PKGEXECDIR -> PKGLIBEXECDIR).

* Tue May 22 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 2.1.0-20
- readded Source11 to filter find-provides from showing libraries that
  should not be provided to the system. BZ#194652 & 240835.

* Mon Feb 19 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 2.1.0-18
- Update to 2.1.0 (4th? maintenance release).

* Tue Jan 17 2006 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to maintenance release.

* Sat Jul 30 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to nx 1.5.0. final.

* Mon Feb 14 2005 Rick Stout <zipsonic[AT]gmail.com> - 4:1.4.0
- updated nx-X11, nxproxy, nxcomp, nxagent and nxdesktop
- released to address X11 security concerns.

* Tue Nov 09 2004 Rick Stout <zipsonci[AT]gmail.com> - 3:1.4.0
- updated to 1.4.0 final

* Mon Oct 11 2004 Rick Stout <zipsonic[AT]gmail.com> - 2:1.4.0
- Changed naming structure to be more friendly (lowercase)

* Fri Oct 07 2004 Rick Stout <zipsonic[AT]gmail.com> - 1:1.4.0
- Updated package dependencies

* Wed Sep 29 2004 Rick Stout <zipsonic[AT]gmail.com> - 0:1.4.0
- Initial Fedora RPM release.
- Updated SuSE package to work with Fedora
