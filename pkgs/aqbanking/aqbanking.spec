%global momorel 1
%global qtdir %{_libdir}/qt3

Summary: A library for online banking functions and financial data import/export
Name: aqbanking
Version: 5.0.25
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.aquamaniac.de/aqbanking/
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch2: aqbanking-5.0.2-pkgconfig.patch
Patch3: aqbanking-5.0.25-conflict.patch
BuildRequires: gmp-devel >= 5.0.1-2m
BuildRequires: gwenhywfar-devel >= 4.3.3
BuildRequires: libofx-devel >= 0.9.1
BuildRequires: gettext
BuildRequires: libtool
Obsoletes: g2banking
Obsoletes: aqhbci
Obsoletes: python-aqbanking
Obsoletes: qbanking
Obsoletes: kbanking
Obsoletes: q4banking

%description 
The intention of AqBanking is to provide a middle layer between the
program and the various Online Banking libraries (e.g. AqHBCI). The
first backend which is already supported is AqHBCI, a library which
implements a client for the German HBCI (Home Banking Computer
Interface) protocol. Additionally, Aqbanking provides various plugins
to simplify import and export of financial data. Currently there are
import plugins for the following formats: DTAUS (German financial
format), SWIFT (MT940 and MT942).

%package devel
Summary: Development headers for Aqbanking
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gwenhywfar-devel
Requires: pkgconfig
# for %%{_datadir}/aclocal
Requires: automake
Obsoletes: g2banking-devel
Obsoletes: aqhbci-devel
Obsoletes: qbanking-devel
Obsoletes: kbanking-devel
Obsoletes: q4banking-devel

%description devel
This package contains aqbanking-config and header files for writing and
compiling programs using Aqbanking.

%prep
%setup -q

%patch2 -p1 -b .pkgconfig
%patch3 -p1 -b .conflict

# hack to nuke rpaths, slighly less ugly than using overriding LIBTOOL below
%if "%{_libdir}" != "/usr/lib"
sed -i -e 's|"/lib /usr/lib|"/%{_lib} %{_libdir}|' configure
%endif

%build
%configure --disable-static

## not smp_mflags safe
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# get rid of *.la files
find %{buildroot} -name *.la -exec rm -f {} \;
find %{buildroot} -name *.a -exec rm -f {} \;

pushd tutorials
make clean
rm -rf .deps
rm -f Makefile*
popd

# for doc
mkdir -p %{buildroot}/%{_datadir}/doc/%{name}-%{version}
mv %{buildroot}/%{_datadir}/doc/{aqbanking,aqhbci} %{buildroot}/%{_datadir}/doc/%{name}-%{version}
mv AUTHORS README COPYING ChangeLog NEWS %{buildroot}/%{_datadir}/doc/%{name}-%{version}

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc %{_datadir}/doc/%{name}-%{version}
%{_libdir}/libaq*.so.*
%dir %{_libdir}/aqbanking
%dir %{_libdir}/aqbanking/plugins/
%dir %{_libdir}/aqbanking/plugins/*/
%{_libdir}/aqbanking/plugins/*/bankinfo/
%{_libdir}/aqbanking/plugins/*/imexporters/
%{_libdir}/aqbanking/plugins/*/providers/
%{_libdir}/gwenhywfar/plugins/*/dbio/*
%dir %{_datadir}/aqbanking
%dir %{_datadir}/aqbanking/aqbanking
%{_datadir}/aqbanking/backends/
%{_datadir}/aqbanking/bankinfo/
%{_datadir}/aqbanking/dialogs/
%{_datadir}/aqbanking/imexporters/
%{_bindir}/aqbanking-cli
%{_bindir}/aqhbci-tool4

%files devel
%defattr(-,root,root)
%doc doc/0[12]* tutorials
%{_bindir}/hbcixml3
%{_bindir}/aqbanking-config
%{_libdir}/libaq*.so
%{_includedir}/aq*/
%{_libdir}/pkgconfig/aqbanking.pc
%{_datadir}/aclocal/aqbanking.m4
%{_datadir}/aqbanking/aqbanking/typemaker2
%{_datadir}/aqbanking/typemaker2

%changelog
* Thu Sep 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.25-1m)
- update to 5.0.25

* Tue Jan  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.21-1m)
- update to 5.0.21
- Obsoletes: q(4)banking(-devel)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.4-5m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.4-4m)
- rebuild against gmp-5.0.1

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.4-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to 4.2.4
- add support qt4 (q4banking)
- renove support python (python-aqbanking)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 13 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.3-3m)
- add patch for gcc44, generated by gen44patch(v1)

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.8.3-2m)
- BuildRequires: gwenhywfar-devel >= 3.8.2

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.3-1m)
- update to 3.8.3
-- remove g2banking and kbanking
- build with --disable-static

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.3-5m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.3-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.3-3m)
- rebuild against python-2.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.3-2m)
- rebuild against gcc43

* Mon Mar 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.3-1m)
- version 2.3.3

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-2m)
- rebuild with kdelibs3

* Thu Jul 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-1m)
- import from Fedora for kmymoney-0.8.7

* Wed Jul 11 2007 Bill Nottingham <notting@redhat.com> - 2.3.2-1
- update to 2.3.2

* Mon Jun 25 2007 Bill Nottingham <notting@redhat.com> - 2.2.9-3
- fix some build bogosity

* Wed Jun 20 2007 Bill Nottingham <notting@redhat.com> - 2.2.9-2
- add a dist tag

* Mon Mar 19 2007 Bill Nottingham <notting@redhat.com> - 2.2.9-1
- update to 2.2.9

* Wed Jan 17 2007 Bill Nottingham <notting@redhat.com> - 2.1.0-14
- fix docdir, obsoletes for aqhbci-devel, and %%clean

* Tue Jan 16 2007 Bill Nottingham <notting@redhat.com> - 2.1.0-13
- fix docs
- add PyXML buildreq

* Mon Jan 15 2007 Bill Nottingham <notting@redhat.com> - 2.1.0-12
- fix missing %%defattrs
- fix %%excludes
- other cleanups from review
- use %%{_python_sitelib}
- require automake
- twiddle aqhbci obsoletes

* Sat Jan 13 2007 Bill Nottingham <notting@redhat.com> - 2.1.0-11
- split into a variety of packages

* Thu Dec  7 2006 Jeremy Katz <katzj@redhat.com> - 2.1.0-10
- rebuild for python 2.5

* Thu Sep  7 2006 Bill Nottingham <notting@redhat.com> - 2.1.0-9
- rebuild for fixed debuginfo (#205248)

* Fri Sep  1 2006 Bill Nottingham <notting@redhat.com> - 2.1.0-8
- fix multilib conficts (#205204)

* Mon Aug 28 2006 Bill Nottingham <notting@redhat.com> - 2.1.0-4
- rebuild against latest libofx

* Tue Aug  1 2006 Bill Nottingham <notting@redhat.com> - 2.1.0-3
- reenable visibility

* Fri Jul 14 2006 Bill Nottingham <notting@redhat.com> - 2.1.0-2
- port *-config to pkgconfig
- don't use -fvisibility=hidden

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 2.1.0-1.1
- rebuild

* Tue Jul 11 2006 Bill Nottingham <notting@redhat.com> - 2.1.0-1
- update to 2.1.0

* Mon Jun 12 2006 Bill Nottingham <notting@redhat.com> - 1.8.1beta-5
- buildreq autoconf, libtool

* Tue May 30 2006 Bill Nottingham <notting@redhat.com> - 1.8.1beta-4
- add gettext buildreq (#193348)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.8.1beta-3.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Karsten Hopp <karsten@redhat.de> 1.8.1beta-3
- buildrequire libofx-devel instead of libofx (pulls in libofx)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.8.1beta-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Sun Jan 22 2006 Bill Nottingham <notting@redhat.com> 1.8.1beta-2
- add an obsolete (#178554)

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Mar  7 2005 Bill Nottingham <notting@redhat.com> 1.0.4beta-2
- rebuild

* Wed Feb  9 2005 Bill Nottingham <notting@redhat.com> 1.0.4beta-1
- initial packaging, adopt upstream specfile
