%global momorel 1

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?python_ver: %global python_ver %(%{__python} -c "import sys ; print sys.version[:3]")}

Summary: Wireless settings python bindings
Name: python-iwlib
Version: 1.1
Release: %{momorel}m%{?dist}
URL: http://git.fedorahosted.org/git/python-iwlib.git
Source: http://fedorahosted.org/released/python-iwlib/%{name}-%{version}.tar.bz2
NoSource: 0
License: GPLv2
Group: System Environment/Libraries
ExcludeArch: s390 s390x
BuildRequires: python-devel >= 2.7
BuildRequires: wireless-tools-devel
Requires: wireless-tools
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Python bindings for the iwlib kernel interface,
that provides functions to examine the wireless network devices
installed on the system.

%prep
%setup -q

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}
mkdir -p %{buildroot}%{_sbindir}
chmod 755 %{buildroot}%{python_sitearch}/iwlib.so

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%{python_sitearch}/iwlib.so
%if "%{python_ver}" >= "2.5"
%{python_sitearch}/*.egg-info
%endif

%changelog
* Sun Feb 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-1m)
- update 1.1

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1-1m)
- import from Fedora

* Thu Jan 07 2010 Jiri Popelka <jpopelka@redhat.com> - 0.1-2
- Use %%global instead of %%define.

* Mon Jul 27 2009 Jiri Popelka <jpopelka@redhat.com> - 0.1-1
- Get iwlib code from rhpl 0.222-1
