%global momorel 1
%global qt_module qtdoc

Summary: Main Qt5 Reference Documentation
Name:    qt5-%{qt_module}
Version: 5.2.1
Release: %{momorel}m%{?dist}

License: GFDL
Group: Documentation
Url: http://qt-project.org/
Source0: http://download.qt-project.org/official_releases/qt/5.2/%{version}/submodules/%{qt_module}-opensource-src-%{version}.tar.xz
NoSource: 0
BuildArch: noarch

BuildRequires: qt5-qtbase-devel >= %{version}
BuildRequires: qt5-qttools-devel >= %{version}

%description
QtDoc contains the main Qt Reference Documentation, which includes
overviews, Qt topics, and examples not specific to any Qt module.

%prep
%setup -q -n %{qt_module}-opensource-src-%{version}%{?pre:-%{pre}}

%build
qmake-qt5
make docs %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
make install_docs INSTALL_ROOT=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE.GPL LICENSE.LGPL LGPL_EXCEPTION.txt
%{_qt5_docdir}/qtdoc.qch
%{_qt5_docdir}/qtdoc/

%changelog
* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-1m)
- import from Fedora

* Wed Feb 05 2014 Rex Dieter <rdieter@fedoraproject.org> 5.2.1-1
- 5.2.1

* Thu Dec 12 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-1
- 5.2.0

* Fri Dec 06 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-0.11.rc1
- BR: qt5-qtbase-devel

* Mon Dec 02 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-0.10.rc1
- 5.2.0-rc1

* Thu Oct 24 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-0.2.beta1
- 5.2.0-beta1

* Thu Oct 10 2013 Rex Dieter <rdieter@fedoraproject.org> 5.2.0-0.1.alpha
- 5.2.0-alpha

* Mon Sep 30 2013 Rex Dieter <rdieter@fedoraproject.org> 5.1.1-2
- License: GFDL

* Sun Sep 22 2013 Rex Dieter <rdieter@fedoraproject.org> 5.1.1-1
- Initial packaging
