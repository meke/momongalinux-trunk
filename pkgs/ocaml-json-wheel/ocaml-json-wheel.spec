%global momorel 8
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-json-wheel
Version:        1.0.6
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for parsing JSON

Group:          Development/Libraries
License:        Modified BSD
URL:            http://martin.jambon.free.fr/json-wheel.html
Source0:        http://martin.jambon.free.fr/json-wheel-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-ocamlnet-devel >= 3.4.1-1m
BuildRequires:  ocaml-pcre-devel >= 6.2.3-1m
BuildRequires:  pcre-devel >= 8.31


%description
JSON library for OCaml following RFC 4627.

If you use this library, consider installing ocaml-json-static, the
syntax extension to the language which makes using JSON much easier.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n json-wheel-%{version}


%build
make

strip jsoncat


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
mkdir -p $RPM_BUILD_ROOT%{_bindir}

make BINDIR=$RPM_BUILD_ROOT%{_bindir} install

# Remove *.cmo and *.o files.  These aren't needed for
# anything because they are included in the *.cma/*.a.
rm $RPM_BUILD_ROOT%{_libdir}/ocaml/json-wheel/*.cmo
rm $RPM_BUILD_ROOT%{_libdir}/ocaml/json-wheel/*.o


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/json-wheel
%if %opt
%exclude %{_libdir}/ocaml/json-wheel/*.a
%exclude %{_libdir}/ocaml/json-wheel/*.cmxa
%exclude %{_libdir}/ocaml/json-wheel/*.cmx
%endif
%exclude %{_libdir}/ocaml/json-wheel/*.mli
%exclude %{_libdir}/ocaml/json-wheel/*.ml
%{_bindir}/jsoncat


%files devel
%defattr(-,root,root,-)
%doc LICENSE Changes README html
%if %opt
%{_libdir}/ocaml/json-wheel/*.a
%{_libdir}/ocaml/json-wheel/*.cmxa
%{_libdir}/ocaml/json-wheel/*.cmx
%endif
%{_libdir}/ocaml/json-wheel/*.mli
%{_libdir}/ocaml/json-wheel/*.ml


%changelog
* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-8m)
- rebuild against pcre-8.31

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-7m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-3m)
- rebuild against ocaml-3.11.2
- rebuild against ocaml-ocamlnet-2.2.9-9m
- rebuild against ocaml-pcre-6.1.0-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.4-5
- Rebuild for OCaml 3.10.2

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.4-4
- Remove ExcludeArch ppc64.

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.4-3
- Don't distribute the *.cmo and *.o files.
- Better way to install jsoncat in the right directory.

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.4-2
- Missing BR ocaml-pcre-devel.
- Missing BR pcre-devel.

* Thu Feb 28 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.4-1
- Initial RPM release.
