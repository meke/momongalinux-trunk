%global momorel 2
%global srcname %{name}-%{version}
%global geckover 1.9.2
%global pythonver 2.7
%global pyver %(%{__python} -c "import sys ; print sys.version[:3]")
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%global srcname miro-%{version}

Summary: Open-source, non-profit video player and podcast client
Name: Miro
Version: 3.5.1
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
License: GPLv2+
URL: http://www.getmiro.com/
Source0: http://ftp.osuosl.org/pub/pculture.org/miro/src/%{srcname}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: %{ix86} x86_64 ppc ppc64
BuildRequires: python-devel >= %{pythonver}
BuildRequires: Pyrex
BuildRequires: pygtk2-devel
BuildRequires: gettext
BuildRequires: atk-devel
BuildRequires: cairo-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel
BuildRequires: glibc-devel
BuildRequires: gtk2-devel
BuildRequires: libX11-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdamage-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libpng-devel
BuildRequires: pango-devel
Requires: python-abi = %{pyver}
Requires: pywebkitgtk
Requires: dbus-python
Requires: gnome-python2-gconf
Requires: gstreamer-python
Requires: rb_libtorrent-python
Provides: miro = %{version}-%{release}
Obsoletes: miro < %{version}-%{release}

%description
Miro is a free, open source internet tv and video player.

%prep
%setup -q -n %{srcname}

%build
cd linux && CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf %{buildroot}

# remove shebangs from scripts
cd linux && \
  find build/lib* -name '*.py' -exec sed -i "1{/^#!/d}" {} \; && \
  %{__python} setup.py install -O1 --skip-build --root %{buildroot}

%find_lang miro
 
%clean
rm -rf %{buildroot}

%post
update-desktop-database %{_datadir}/applications &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
update-desktop-database %{_datadir}/applications &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%posttrans
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files -f linux/miro.lang
%defattr(-,root,root,-)
%doc CREDITS README license.txt
%{_bindir}/miro
%{_bindir}/miro.real
%{python_sitearch}/miro
%{python_sitearch}/miro-*.egg-info
%{_datadir}/miro
%{_datadir}/applications/miro.desktop
%{_datadir}/icons/hicolor/*/apps/miro.png
%{_datadir}/mime/packages/miro.xml
%{_datadir}/pixmaps/miro.xpm
%{_datadir}/man/man1/miro.1*
%{_datadir}/man/man1/miro.real.1*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.1-2m)
- rebuild for glib 2.33.2

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.5.1-1m)
- update to 3.5.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.2-2m)
- full rebuild for mo7 release

* Fri Jun 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.4-2m)
- rebuild agaisnt xulrunner-1.9.2

* Wed Jan 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.2-6m)
- change BR from xine-lib to xine-lib-devel

* Sun Nov 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.2-5m)
- rebuild agaisnt boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.2-3m)
- rebuild agaisnt boost-1.40.0

* Thu Oct  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.2-2m)
- add patch for boost-1.40
- switch to use rb_libtorrent

* Thu Aug  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Mon Jul 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-0.1.1m)
- update to 2.5-rc1

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-0.0.20090629.1m)
- update to nightly (20090629)

* Sun Jun 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5-0.0.20090610.5m)
- fix up Provides and Obsoletes

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5-0.0.20090610.4m)
- rename miro to Miro

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-0.0.20090610.3m)
- rebuild against xulrunner-1.9.1

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5-0.0.20090610.2m)
- rebuild against xulrunner-1.9.0.11-1m

* Thu Jun 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-0.0.20090610.1m)
- merge from T4R
-- 
-- * Thu Jun 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.5-0.0.20090610.1m)
-- - update to 20090610's nightly 
-- 
-- * Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0.4-1m)
-- - update to 2.0.4
-- 
-- * Sat Mar 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0.3-1m)
-- - update to 2.0.3
-- -- import Patch1 from Rawhide (2.0.3-1)
-- -- Recently miro does not work. So I can not merge to trunk.
-- -- I suspect that xine-lib is guilty, but have not had the evidence.
-- -- http://bugs.gentoo.org/131527
-- -- https://bugzilla.redhat.com/show_bug.cgi?id=470568
-- -- http://bugzilla.pculture.org/show_bug.cgi?id=11435
-- -- http://getsatisfaction.com/participatoryculturefoundation/topics/miro_segfaults_after_each_video
-- 
-- * Mon Mar 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0.2-2m)
-- - replace Patch5
-- 
-- * Mon Mar  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0.2-1m)
-- - update to 2.0.2
-- 
-- * Wed Feb 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0.1-1m)
-- - update to 2.0.1
-- 
-- * Tue Feb 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0-0.3.2m)
-- 
-- * Mon Feb  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0-0.3.1m)
-- - update to 2.0-rc3 which supports python26, but not tested upstream
-- 
-- * Sat Feb  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0-0.2.1m)
-- - update to 2.0-rc2
-- - add lib64 patch (Patch5)
-- 
-- * Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0-0.1.1m)
-- - update to 2.0-rc1

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-16m)
- rebuilt for boost-1.39.0

* Tue Apr 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-15m)
- rebuild against xulrunner-1.9.0.8-1m

* Wed Apr 22 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.7-14m)
- rebuild against xulrunner-1.9.0.9-1m

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-13m)
- rebuild against openssl-0.9.8k

* Mon Mar 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-12m)
- rebuild against xulrunner-1.9.0.8-1m

* Tue Feb 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-11m)
- rebuild against xulrunner-1.9.0.6-1m

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-10m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-9m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-8m)
- rebuild against boost-1.37.0
-- enable Patch3

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.7-7m)
- rebuild against python-2.6.1-1m

* Wed Dec 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-6m)
- rebuild against xulrunner-1.9.0.5-1m

* Thu Nov 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-5m)
- rebuild against xulrunner-1.9.0.4-1m
- add BuildRequires: qt-devel

* Mon Oct 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-4m)
- revise BuildRequires and Reqiures

* Mon Oct 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-3m)
- rebuild against xulrunner-1.9.0.3-1m
- update Patch2: Miro-gcc43.patch
- add Patch3: Miro-1.2.6-boost.patch -> roll back for compile error...

* Wed Sep 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-2m)
- rebuild against xulrunner-1.9.0.2-1m

* Mon Sep 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-6m)
- rebuild against xulrunner-1.9.0.1-1m

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-5m)
- rebuild against openssl-0.9.8h-1m

* Thu May 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-4m)
- import xulrunner patch from Fedora

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-3m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-2m)
- rebuild against firefox-3

* Tue Apr 29 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Mon Apr 28 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.1-2m)
- enable egg-info

* Fri Apr 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Thu Apr 17 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1-9m)
- rebuild against firefox-2.0.0.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-8m)
- rebuild against gcc43

* Thu Mar 27 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.1-7m)
- rebuild against firefox-2.0.0.13

* Fri Feb 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- add patch for gcc43

* Fri Feb 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-5m)
- adjust %%files for the newest python
- BR: python-devel >= 2.5.1-7m

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild against firefox-2.0.0.12

* Fri Feb  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-3m)
- modify %%files

* Thu Feb  7 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-2m)
- add gcc43.patch 
- add -fpermissive for gcc43

* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Sat Dec  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-3m)
- rebuild against firefox-2.0.0.11

* Wed Nov 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-2m)
- rebuild against firefox-2.0.0.10

* Wed Nov 14 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri Nov  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.9-2m)
- rebuild against firefox-2.0.0.9

* Thu Nov  1 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.9-1m)
- update to 0.9.9.9

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9-5m)
- rebuild against firefox-2.0.0.8

* Thu Sep 27 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-4m)
- replace %%{_datadir}/mime/packages/miro.xml

* Wed Sep 19 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-3m)
- rebuild against firefox-2.0.0.7-1m

* Sun Sep  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-2m)
- add Requires: gnome-python2-extras-gtkmozembed
  thx <http://pc11.2ch.net/test/read.cgi/linux/1188293074/34>

* Wed Sep  5 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-1m)
- update to 0.9.9

* Sun Aug 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.1-3m)
- add BuildArch ppc ppc64

* Thu Aug 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.1-2m)
- rebuild against boost-1.34.1-1m

* Thu Aug  2 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.1-1m)
- update to 0.9.8.1

* Thu Aug  2 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-1m)
- import to Momonga
- add Patch0: miro-0.9.8-dbus-fix.patch
  <https://develop.participatoryculture.org/trac/democracy/ticket/7270>
