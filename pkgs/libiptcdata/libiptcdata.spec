%global momorel 5

%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Name: libiptcdata
Version: 1.0.4
Release: %{momorel}m%{?dist}
Summary: IPTC tag library

Group: Development/Libraries
License: LGPLv2+
URL: http://libiptcdata.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/libiptcdata/libiptcdata-%{version}.tar.gz
NoSource: 0         
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: gtk-doc
BuildRequires: python-devel >= 2.7

%description
libiptcdata is a library for parsing, editing, and saving IPTC data
stored inside images.  IPTC is a standard for encoding metadata such
as captions, titles, locations, etc. in the headers of an image file.
libiptcdata also includes a command-line utility for modifying the
metadata.

%package devel
Summary: Headers and libraries for libiptcdata application development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: gtk-doc

%description devel
The libiptcdata-devel package contains the libraries and include files
that you can use to develop libiptcdata applications.

%package python
Summary: Python bindings for libiptcdata
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
BuildRequires: python-devel

%description python
The libiptcdata-python package contains a Python module that allows Python
applications to use the libiptcdata API for reading and writing IPTC
metadata in images.

%prep
%setup -q

%build
gtkdocize --copy
autoreconf -vfi
%configure --enable-gtk-doc --enable-python --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} INSTALL="%{__install} -c -p" install
rm -f %{buildroot}%{_libdir}/%{name}.la
rm -f %{buildroot}%{python_sitearch}/iptcdata.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%files devel
%defattr(-,root,root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/libiptcdata
%{_datadir}/gtk-doc/html/libiptcdata

%files python
%defattr(-,root,root)
%doc python/README
%doc python/examples/*
%{python_sitearch}/*.so

%changelog
* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-5m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linnux.org>
- (1.0.4-1m)
- update to 1.0.4

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-5m)
- --enable-gtk-doc

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-4m)
- delete __libtoolize hack

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-3m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (1.0.2-5m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.2-3m)
- rebuild against python-2.6.1-2m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-1m)
- import from f7 to Momonga

* Tue May 15 2007 David Moore <dcm@acm.org> 1.0.2-1
- New upstream version

* Fri Mar 23 2007 David Moore <dcm@acm.org> 1.0.1-1
- New upstream version

* Thu Mar 22 2007 David Moore <dcm@acm.org> 1.0.0-2
- Fixed URL, removed INSTALL file, fixed python path and timestamps

* Wed Mar 21 2007 David Moore <dcm@acm.org> 1.0.0-1
- Updated spec file to better match Fedora guidelines

* Sun Jan 28 2007 David Moore <dcm@acm.org>
- Added libiptcdata-python package

* Wed Apr 12 2006 David Moore <dcm@acm.org>
- Removed *.mo from spec file since there are no translations yet

* Mon Feb 28 2005 David Moore <dcm@acm.org>
- Initial version
