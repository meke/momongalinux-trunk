%global momorel 11

Summary: Database-specific drivers for libdbi
Name: libdbi-drivers
Version: 0.8.3
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: LGPL
URL: http://libdbi-drivers.sourceforge.net/
Source: http://prdownloads.sourceforge.net/libdbi-drivers/%{name}-%{version}-1.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libdbi >= 0.8
BuildRequires: libdbi-devel >= 0.8

%description
libdbi implements a database-independent abstraction layer in C, similar to the
DBI/DBD layer in Perl. Writing one generic set of code, programmers can
leverage the power of multiple databases and multiple simultaneous database
connections by using this framework.

libdbi-drivers contains the database-specific plugins needed to connect
libdbi to particular database servers.

%package -n libdbi-dbd-mysql
Summary: MySQL plugin for libdbi
Group: Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires: mysql
BuildRequires: mysql-devel >= 5.5.10, openssl-devel

%description -n libdbi-dbd-mysql
This plugin provides connectivity to MySQL database servers through the
libdbi database independent abstraction layer. Switching a program's plugin
does not require recompilation or rewriting source code.

%package -n libdbi-dbd-pgsql
Summary: PostgreSQL plugin for libdbi
Group: Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires: postgresql-libs
BuildRequires: postgresql-devel, krb5-devel, openssl-devel

%description -n libdbi-dbd-pgsql
This plugin provides connectivity to PostgreSQL database servers through the
libdbi database independent abstraction layer. Switching a program's plugin
does not require recompilation or rewriting source code.

%package -n libdbi-dbd-sqlite
Summary: SQLite plugin for libdbi
Group: Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires: sqlite >= 3
BuildRequires: sqlite-devel

%description -n libdbi-dbd-sqlite
This plugin provides access to an embedded SQL engine using libsqlite3 through
the libdbi database independent abstraction layer. Switching a program's plugin
does not require recompilation or rewriting source code.

%clean 
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%prep
# stupid packaging of 0.8.1a --- unpacks into directory libdbi-drivers-0.8.1
# %setup -q -n %{name}-%{version}
%setup -q -n %{name}-0.8.3-1

%build
# configure is broken, must pass both --with-*sql-libdir _AND_
# --with-*sql-incdir in order for --with-*sql-libdir to be used
%configure --with-mysql --with-pgsql --with-sqlite3 \
	--with-mysql-libdir=%{_libdir}/mysql \
	--with-mysql-incdir=%{_includedir} \
	--with-pgsql-libdir=%{_libdir} \
	--with-pgsql-incdir=%{_includedir} \
	--with-sqlite3-libdir=%{_libdir} \
	--with-sqlite3-incdir=%{_includedir} \
	--with-dbi-libdir=%{_libdir} \
	--with-dbi-incdir=%{_includedir}/dbi

make %{?_smp_mflags}

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

# more klugery for mis-packaging of 0.8.1a
mv $RPM_BUILD_ROOT%{_docdir}/libdbi-drivers-0.8.3-1 $RPM_BUILD_ROOT%{_docdir}/libdbi-drivers-0.8.3

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS
%doc ChangeLog
%doc COPYING
%doc README
%dir %{_libdir}/dbd

%files -n libdbi-dbd-mysql
%defattr(-,root,root)
%{_libdir}/dbd/libdbdmysql.*

%files -n libdbi-dbd-pgsql
%defattr(-,root,root)
%{_libdir}/dbd/libdbdpgsql.*

%files -n libdbi-dbd-sqlite
%defattr(-,root,root)
%{_libdir}/dbd/libdbdsqlite3.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3-11m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.3-10m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.3-8m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.8.3-5m)
- define __libtoolize (build fix)

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3-4m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3-2m)
- rebuild against gcc43

* Sat Mar 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.3-1m)
- update to 0.8.3-1

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1a-1m)
- import from fc-devel to Momonga

* Mon Dec 11 2006 Tom Lane <tgl@redhat.com> 0.8.1a-2
- Enable building of sqlite driver
Resolves: #184568
- Rebuild needed anyway for Postgres library update

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.8.1a-1.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.8.1a-1.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.8.1a-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Sat Nov 12 2005 Tom Lane <tgl@redhat.com> 0.8.1a-1
- Update to version 0.8.1a.

* Tue Apr 12 2005 Tom Lane <tgl@redhat.com> 0.7.1-3
- Rebuild for Postgres 8.0.2 (new libpq major version).

* Fri Mar 11 2005 Tom Lane <tgl@redhat.com> 0.7.1-2
- Packaging improvements per discussion with sopwith.

* Thu Mar 10 2005 Tom Lane <tgl@redhat.com> 0.7.1-1
- Import new libdbi version, splitting libdbi-drivers into a separate SRPM
  so we can track new upstream packaging.
