%global momorel 1

Summary: Access control list utilities
Name: acl
Version: 2.2.52
Release: %{momorel}m%{?dist}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gawk
BuildRequires: gettext
BuildRequires: libattr-devel >= 2.4.32
BuildRequires: libtool
#BuildRequires: autoconf
Requires: libacl = %{version}-%{release}
Source0: http://download.savannah.gnu.org/releases-noredirect/acl/acl-%{version}.src.tar.gz
NoSource: 0

# fix a typo in setfacl(1) man page (#675451)
Patch1: 0001-acl-2.2.49-bz675451.patch

# prepare the test-suite for SELinux and arbitrary umask
Patch3: 0003-acl-2.2.52-tests.patch

# Install the libraries to the appropriate directory
Patch4: 0004-acl-2.2.52-libdir.patch

# fix SIGSEGV of getfacl -e on overly long group name
Patch5: 0005-acl-2.2.52-getfacl-segv.patch

License: GPLv2+
Group: System Environment/Base
URL: http://acl.bestbits.at/

%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This package contains the getfacl and setfacl utilities needed for
manipulating access control lists.

%package -n libacl
Summary: Dynamic library for access control list support
License: LGPLv2+
Group: System Environment/Libraries
Requires(post): glibc-common
Requires(postun): glibc-common

%description -n libacl
This package contains the libacl.so dynamic library which contains
the POSIX 1003.1e draft standard 17 functions for manipulating access
control lists.

%package -n libacl-devel
Summary: Access control list static libraries and headers
License: LGPLv2+
Group: Development/Libraries
Requires: libacl = %{version}-%{release}, libattr-devel

%description -n libacl-devel
This package contains static libraries and header files needed to develop
programs which make use of the access control list programming interface
defined in POSIX 1003.1e draft standard 17.

%prep
%setup -q

%patch1 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1

%build
touch .census
# acl abuses libexecdir
%configure 
#make LIBTOOL="libtool --tag=CC" %{?_smp_mflags}

# uncomment to turn on optimizations
# sed -i 's/-O2/-O0/' libtool include/builddefs
# unset CFLAGS

make %{?_smp_mflags} LIBTOOL="libtool --tag=CC"

%if %{do_test}
%check
if ./setfacl/setfacl -m u:`id -u`:rwx .; then
    make tests || exit $?
    if test 0 = `id -u`; then
        make root-tests || exit $?
    fi
else
    echo '*** ACLs are probably not supported by the file system,' \
         'the test-suite will NOT run ***'
fi
%endif

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}
make install-dev DESTDIR=%{buildroot}
make install-lib DESTDIR=%{buildroot}

# get rid of libacl.a and libacl.la
rm -f $RPM_BUILD_ROOT/%{_lib}/libacl.a
rm -f $RPM_BUILD_ROOT/%{_lib}/libacl.la
rm -f $RPM_BUILD_ROOT%{_libdir}/libacl.a
rm -f $RPM_BUILD_ROOT%{_libdir}/libacl.la

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post -n libacl -p /sbin/ldconfig

%postun -n libacl -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc doc/COPYING*
%{_bindir}/chacl
%{_bindir}/getfacl
%{_bindir}/setfacl
%{_datadir}/doc/acl
%{_mandir}/man1/chacl.1*
%{_mandir}/man1/getfacl.1*
%{_mandir}/man1/setfacl.1*
%{_mandir}/man5/acl.5*

%files -n libacl-devel
%defattr(-,root,root)
%{_libdir}/libacl.so
%{_libdir}/libacl.so
%{_includedir}/acl
%{_includedir}/sys/acl.h
%{_mandir}/man3/acl_*

%files -n libacl
%defattr(-,root,root)
%{_libdir}/libacl.so.*

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.52-1m)
- update 2.2.52

* Sat Sep 24 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.51-2m)
- fix typo in %%install
- add %%do_test switch
-- make check fails when SELinux is disabled

* Sat Aug 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.51-1m)
- version up 2.2.51

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.49-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.49-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.49-2m)
- full rebuild for mo7 release

* Sun Jan 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.49-1m)
- [SECURITY] CVE-2009-4411
- update to 2.2.49
- import some patches from Rawhide (2.2.49-3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.47-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.47-5m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.47-4m)
- update Patch13 for fuzz=0 from Rawhide (2.2.47-3)
- License: GPLv2+

* Tue Apr 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.47-3m)
- import 5 patches from Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.47-2m)
- rebuild against gcc43

* Sat Feb 24 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.2.47-1m)
- update 2.2.47

* Tue Jan 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.45-1m)
- update 2.2.45

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.39-3m)
- BuildRequires: attr-devel -> libattr-devel
- libacl-devel Requires: attr-devel -> libattr-devel

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.39-2m)
- files attr fix

* Fri Dec 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.39-1m)
- update
- chmod 0755
- update BuildRequires: attr-devel >= 2.4.32

* Sun May 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.34-1m)
- update to 2.2.34

* Sun Dec 25 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.28-3m)
- no NoSource

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.28-2m)
- get rid of *.la files, libtool doesn't handle well .a and .so.*
  in separates directories
- BuildRequires: attr-devel >= 2.4.20-2m

* Tue May 17 2005 Kazuya Iwamoto <iwapi@momonga-linux.org>
- (2.2.28-1m)
- update to 2.2.28

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (2.2.27-2m)
- enable x86_64.
- libexec isn't appropriate.

* Wed Oct 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.27-1m)
  update to 2.2.27

* Sat Sep  4 2004 TAKAHASHI Tamotsu <tamo>
- (2.2.23-1m)
- import
- get the source from sgi

* Thu Aug 19 2004 Phil Knirsch <pknirsch@redhat.com> 2.2.23-2
- Make libacl.so.* executable.

* Thu Aug 19 2004 Phil Knirsch <pknirsch@redhat.com> 2.2.23-1
- Update to latest upstream version.

* Sun Aug  8 2004 Alan Cox <alan@redhat.com> 2.2.7-7
- Close bug #125300 (Steve Grubb: build requires libtool,gettext)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Mar 31 2004 Stephen C. Tweedie <sct@redhat.com> 2.2.7-5
- Add missing %defattr

* Tue Mar 30 2004 Stephen C. Tweedie <sct@redhat.com> 2.2.7-3
- Add /usr/include/acl to files manifest
- Fix location of doc files, add main doc dir to files manifest

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Aug  5 2003 Elliot Lee <sopwith@redhat.com> 2.2.7-2
- Fix libtool invocation

* Tue Jun  3 2003 Stephen C. Tweedie <sct@redhat.com> 2.2.7-1
- Update to acl-2.2.7

* Wed Mar 26 2003 Michael K. Johnson <johnsonm@redhat.com> 2.2.3-2
- include patch from Jay Berkenbilt to print better error messages

* Tue Jan 28 2003 Michael K. Johnson <johnsonm@redhat.com> 2.2.3-1
- udpate/rebuild

* Sat Jan  4 2003 Jeff Johnson <jbj@redhat.com> 2.0.11-7
- set execute bits on library so that requires are generated.

* Tue Nov 19 2002 Elliot Lee <sopwith@redhat.com> 2.0.11-5
- Correct patch in previous fix so that shared libraries go in /lib* 
  instead of /usr/lib*

* Tue Nov 19 2002 Elliot Lee <sopwith@redhat.com> 2.0.11-4
- Fix multilibbing

* Wed Sep 11 2002 Than Ngo <than@redhat.com> 2.0.11-3
- Added fix to install libs in correct directory on 64bit machine

* Thu Aug 08 2002 Michael K. Johnson <johnsonm@redhat.com> 2.0.11-2
- Made the package only own the one directory that is unique to it:
  /usr/include/acl

* Mon Jun 24 2002 Michael K. Johnson <johnsonm@redhat.com> 2.0.11-1
- Initial Red Hat package
  Made as few changes as possible relative to upstream packaging to
  make it easier to maintain long-term.  This means that some of
  the techniques used here are definitely not standard Red Hat
  techniques.  If you are looking for an example package to fit
  into Red Hat Linux transparently, this would not be the one to
  pick.
- acl-devel -> libacl-devel
