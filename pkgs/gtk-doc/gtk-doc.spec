Summary: GTK+ DocBook Documentation Generator
Name: gtk-doc

%global momorel 1

Version: 1.20
Release: %{momorel}m%{?dist}
Group: Applications/Text
License: GPLv3+ and GFDL
URL: http://www.gtk.org/gtk-doc/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.20/%{name}-%{version}.tar.xz
NoSource: 0

#BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: docbook-dtds >= 4.2
BuildRequires: docbook-style-dsssl
BuildRequires: docbook-style-xsl
BuildRequires: libxslt >= 1.1.20
BuildRequires: openjade
BuildRequires: xml-common
BuildRequires: rarian-devel
BuildRequires: perl
BuildRequires: python
BuildRequires: glib2-devel
BuildRequires: gnome-doc-utils-devel

Requires(pre): docbook-style-dsssl
Requires(pre): docbook-style-xsl
Requires(pre): libxslt >= 1.1.20
Requires(pre): xml-common

%description
gtk-doc is a set of perl scripts that generate API reference documention in
DocBook format.  It can extract documentation from source code comments in a
manner similar to java-doc.  It is used to generate the documentation for
GLib, Gtk+, and GNOME.

%prep
%setup -q

%build
%configure --disable-static \
	--enable-silent-rules \
	--disable-scrollkeeper
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README doc
%{_bindir}/*
%{_datadir}/pkgconfig/*.pc
%{_datadir}/%{name}/*
%{_datadir}/aclocal/*.m4
%{_datadir}/sgml/%{name}
%{_datadir}/help/*/gtk-doc-manual
##%{_datadir}/omf/gtk-doc-manual

%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.20-1m)
- up[date to 1.20

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.18-2m)
- rebuild for glib 2.33.2

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Tue Feb  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.15-5m)
- add more docment files

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.15-4m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.15-3m)
- import bug fix patch from upstream
-- see https://bugzilla.gnome.org/show_bug.cgi?id=627223

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.15-2m)
- update to 1.15 again

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14-3m)
- full rebuild for mo7 release

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-2m)
- version down to 1.14

* Mon Jul  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.13-1m)
- update to 1.13
- 1.12 is better, maybe 

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 12 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11-5m)
- add BuildPrereq

* Wed May 13 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.11-4m)
- add autoreconf

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-3m)
- apply filename with spaces patch (Patch0) for gimp-2.6.6
- License: GPLv3+ and GFDL

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-2m)
- rebuild against rpm-4.6

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11-1m)
- update to 1.11
- remove all patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9-2m)
- %%NoSource -> NoSource

* Tue Oct  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8-2m)
- unhold /usr/share/gtk-doc

* Wed Feb 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8-1m)
- update to 1.8

* Mon Jan  1 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.7-2m)
- add BuildPrereq: scrollkeeper

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7-1m)
- update to 1.7

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6-2m)
- delete duplicated files

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Wed Apr  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Sun Nov 20 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-3m)
- use %{_libdir}

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4-2m)
- comment out unnessesaly autoreconf and make check

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4-1m)
- reject patch2.
- version up.
- GNOME 2.12.1 Desktop

* Tue Nov 1 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
-  (1.3-3m)
- rebuild against libxslt-1.1.12-3m

* Sun Apr 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-2m)
- import dcl patch (gtk-doc.dcl: Allow ':' in names. (#169087))

* Tue Apr 14 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-1m)
- update 1.3

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2-2m)
- adjustment BuildPreReq

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2-1m)
- version 1.2
- exchanges from Patch0 to Patch1
- GNOME 2.6 Desktop

* Wed Aug 13 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.1-7m)
- add BuildPrereq: docbook-dtds (sorry for several commit...)

* Wed Aug 13 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.1-6m)
- add BuildPrereq: libxslt

* Wed Aug 13 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.1-5m)
- add BuildPrereq: openjade

* Tue Jul 22 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-4m)
- add BuildPrereq: xml-common and add Prereq: xml-common

* Mon Jun 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1-3m)
- fix file list

* Fri May 23 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-2m)
- add BuildPrereq: docbook-dtds,docbook-style-xsl

* Fri Apr 18 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1-1m)
- version 1.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0-1m)
- version 1.0

* Wed Nov 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10-1m)
- version 0.10

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.9-2k)
- version 0.9

* Fri Feb  1 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-8k)
- rebuild against for openjade-1.3.1

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.7-6k)
- s/Copyright/License/

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.7-4k)
- nigittenu

* Wed Sep 26 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.7-3k)
- k

* Fri Apr 27 2001 Toshio Kuratomi <badger@prtr-13.ucsc.edu>
- Merge in some of the features of the redhat spec file.

* Wed Nov 15 2000 John Gotts <jgotts@linuxsavvy.com>
- Minor updates for 0.4.
* Thu Aug 26 1999 John E. Gotts <jgotts@engin.umich.edu>
- Created spec file.
