%global momorel 4
%global require_ibus_version 1.4.99.20121006

Summary: The M17N engine for IBus platform
Name: ibus-m17n
Version: 1.3.2
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://code.google.com/p/ibus/
Group: System Environment/Libraries
Source0: http://ibus.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext-devel
BuildRequires: ibus-devel  >= %{reqiure_ibus_version}
BuildRequires: m17n-lib-devel
BuildRequires: pkgconfig
Requires: ibus >= %{require_ibus_version}
Requires: m17n-lib
Requires: iok > 1.3.1

%description
M17N engine for IBus input platform. It allows input of many launguaes using
the input table maps from m17n-db.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la file
rm -f %{buildroot}%{python_sitearch}/_m17n.la

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libexecdir}/ibus-engine-m17n
%{_libexecdir}/ibus-setup-m17n
%{_datadir}/ibus-m17n
%{_datadir}/ibus/component/*

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.2-4m)
- rebuild against ibus-1.4.99.20121006

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-3m)
- rebuild for ibus-1.3.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-2m)
- rebuild for new GCC 4.5

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090617-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090617-1m)
- update to 1.2.0.20090617

* Sat May  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.20090211.1m)
- version 1.1.0

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080819.1m)
- update to 20080819 git snapshot

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080810.1m)
- initial package for Momonga Linux
