%global         momorel 5

Name:           perl-BSD-Resource
Version:        1.2907
Release:        %{momorel}m%{?dist}
Summary:        BSD process resource limit and priority functions
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/BSD-Resource/
Source0:        http://www.cpan.org/authors/id/J/JH/JHI/BSD-Resource-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
getrusage

%prep
%setup -q -n BSD-Resource-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog META.json README
%{perl_vendorarch}/BSD
%{perl_vendorarch}/auto/BSD
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2907-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2907-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2907-3m)
- rebuild against perl-5.18.1

* Tue Jul 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2907-2m)
- all tests are successful

* Mon Jul 15 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2907-1m)
- update to 1.2907, 1.2906 is missing

* Fri Jul 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2906-1m)
- update to 1.2906

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-15m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-14m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-13m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-12m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-11m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2904-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2904-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2904-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-2m)
- rebuild against perl-5.12.0

* Mon Mar 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2904-1m)
- update to 1.2904

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2903-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2903-2m)
- rebuild against perl-5.10.1

* Sat Apr  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2903-1m)
- update to 1.2903

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2902-1m)
- update to 1.2902

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2901-2m)
- rebuild against rpm-4.6

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2901-1m)
- update to 1.2901

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.28-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.28-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.28-2m)
- use vendor

* Thu Jun  1 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.28-1m)
- update to 1.28

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.25-1m)
- update to 1.25

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.24-2m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.24-1m)
- version up to 1.24
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.23-6m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.23-5m)
- remove Epoch from BuildRequires

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.23-1m)
- update to 1.23
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.22-3m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.22-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Tue Jun 17 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.22-1m)
- update to 1.22

* Sat Dec 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.21-1m)

* Wed Nov 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.20-1m)

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.19-1m)
- rebuild against perl-5.8.0

* Sun Sep 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.17-1m)

* Mon Sep 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.16-1m)

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.15-3m)
- fix %files

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.15-2m)
- remove BuildRequires: gcc2.95.3

* Thu Jul 25 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.15-1m)
- version up 1.15

* Mon Mar  4 2002 Shingo Akagaki <dora@kondara.org>
- (1.14-2k)
- create
