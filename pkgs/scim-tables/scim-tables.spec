%global momorel 10
%global scimver 1.4.9
%global scim_major_version 1.4.0

Summary: SCIM Generic Table IMEngine and its data files
Name: scim-tables
Version: 0.5.9

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# ${HOME}/rpm/specopt/uim.specopt and edit it.

## Configuration
%{?!build_indic_tables: %global build_indic_tables 0}
%{?!build_jk_tables:    %global build_jk_tables    0}

Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://www.scim-im.org/projects/imengines/
Source0: http://dl.sourceforge.net/sourceforge/scim/%{name}-%{version}.tar.gz
NoSource: 0
Source1: CangJie5.png
Patch0: %{name}-0.5.7-2.bz217639.patch
Patch1: %{name}-0.5.7-5.bz232860.patch
Patch2: %{name}-0.5.7-gcc43.patch
Patch3: %{name}-%{version}-CangJie5-icon.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: scim >= %{scimver}
BuildRequires: scim-devel >= %{scimver}
BuildRequires: coreutils
BuildRequires: expat-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libpng-devel
BuildRequires: libxcb-devel
BuildRequires: pango-devel
BuildRequires: zlib-devel
Obsoletes: skim-scim-tables

%description
This package includes Generic Table IMEngine for SCIM and many data files for it.

%package amharic
Summary: SCIM tables for Amharic
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description amharic
This package contains scim-tables files for Amharic input.

%package arabic
Summary: SCIM tables for Arabic
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description arabic
This package contains scim-tables files for Arabic input.

%if %{build_indic_tables}
%package bengali
Summary: SCIM tables for Bengali
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description bengali
This package contains scim-tables files for Bengali input.
%endif

%package chinese
Summary: SCIM tables for Chinese
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description chinese
This package contains scim-tables files for Chinese input.

%package chinese-extra
Summary: Additional SCIM tables for Chinese
Group: Applications/System
# for ZhuYin.png
Requires: %{name}-chinese = %{version}-%{release}

%description chinese-extra
This package contains additional less used scim-tables files for Chinese input.

%if %{build_indic_tables}
%package gujarati
Summary: SCIM tables for Gujarati
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description gujarati
This package contains scim-tables files for Gujarati input.

%package hindi
Summary: SCIM tables for Hindi
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description hindi
This package contains scim-tables files for Hindi input.
%endif

%if %{build_jk_tables}
%package japanese
Summary: SCIM tables for Japanese
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description japanese
This package contains scim-tables files for Japanese.
%endif

%if %{build_indic_tables}
%package kannada
Summary: SCIM tables for Kannada
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description kannada
This package contains scim-tables files for Kannada input.
%endif

%if %{build_jk_tables}
%package korean
Summary: SCIM tables for Korean
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description korean
This package contains scim-tables files for Korean.
%endif

%if %{build_indic_tables}
%package malayalam
Summary: SCIM tables for Malayalam scripts
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description malayalam
This package contains scim-tables files for Malayalam languages.
%endif

%package nepali
Summary: SCIM tables for Nepali
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description nepali
This package contains scim-tables files for Nepali input.

%if %{build_indic_tables}
%package punjabi
Summary: SCIM tables for Punjabi
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description punjabi
This package contains scim-tables files for Punjabi input.
%endif

%package russian
Summary: SCIM tables for Russian
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description russian
This package contains scim-tables files for Russian input.

%if %{build_indic_tables}
%package tamil
Summary: SCIM tables for Tamil
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description tamil
This package contains scim-tables files for Tamil input.
%endif

%package thai
Summary: SCIM tables for Thai
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description thai
This package contains scim-tables files for Thai input.

%if %{build_indic_tables}
%package telugu
Summary: SCIM tables for Telugu
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description telugu
This package contains scim-tables files for Telugu input.
%endif

%package ukrainian
Summary: SCIM tables for Ukrainian
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description ukrainian
This package contains scim-tables files for Ukrainian input.

%package vietnamese
Summary: SCIM tables for Vietnamese
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description vietnamese
This package contains scim-tables files for Vietnamese input.

%package additional
Summary: Other miscellaneous SCIM tables
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description additional
This package contains some miscellaneous scim-tables.

%package -n skim-%{name}
Summary: skim-scim-tables is a scim-tables settings plugin for SKIM
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: skim >= %{skimver}

%description -n skim-%{name}
skim-scim-tables is a scim-tables settings plugin for SKIM.

%prep
%setup -q

%patch0 -p1 -b .1-217639
%patch1 -p1 -b .2-232860
%patch2 -p1 -b .gcc43~
%patch3 -p1 -b .icon

%build
%configure --enable-static=no
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install CangJie5 icon
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/scim/icons/

%if ! %{build_indic_tables}
rm -f %{buildroot}%{_datadir}/scim/{icons,tables}/{Bengali,Gujarati,Hindi,Kannada,Malayalam,Punjabi,Tamil,Telugu}-*
%endif

%if !%{build_jk_tables}
rm -f %{buildroot}%{_datadir}/scim/{icons,tables}/{Hangul,Hanja,HIRAGANA,KATAKANA,Nippon}*
%endif

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/scim-1.0/%{scim_major_version}/{IMEngine,SetupUI}/table*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_bindir}/scim-make-table
%{_libdir}/scim-1.0/%{scim_major_version}/IMEngine/table.so
%{_libdir}/scim-1.0/%{scim_major_version}/SetupUI/table-imengine-setup.so
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/scim-make-table.1*
%{_datadir}/scim/icons/table.png
%dir %{_datadir}/scim/tables

%files amharic
%defattr(-,root,root)
%{_datadir}/scim/icons/Amharic.png
%{_datadir}/scim/tables/Amharic.bin

%files arabic
%defattr(-,root,root)
%{_datadir}/scim/icons/Arabic.png
%{_datadir}/scim/tables/Arabic.bin

%if %{build_indic_tables}
%files bengali
%defattr(-,root,root)
%{_datadir}/scim/icons/Bengali-inscript.png
%{_datadir}/scim/icons/Bengali-probhat.png
%{_datadir}/scim/tables/Bengali-inscript.bin
%{_datadir}/scim/tables/Bengali-probhat.bin
%endif

%files chinese
%defattr(-,root,root)
%doc tables/zh/README-*.txt
%{_datadir}/scim/icons/Array30.png
%{_datadir}/scim/icons/CangJie3.png
%{_datadir}/scim/icons/CangJie5.png
%{_datadir}/scim/icons/CantonHK.png
%{_datadir}/scim/icons/Quick.png
%{_datadir}/scim/icons/Wubi.png
%{_datadir}/scim/icons/ZhuYin.png
%{_datadir}/scim/tables/CangJie3.bin
%{_datadir}/scim/tables/Array30.bin
%{_datadir}/scim/tables/CangJie5.bin
%{_datadir}/scim/tables/CantonHK.bin
%{_datadir}/scim/tables/Quick.bin
%{_datadir}/scim/tables/Wubi.bin
%{_datadir}/scim/tables/ZhuYin.bin

%files chinese-extra
%defattr(-,root,root)
%doc tables/zh/README-*.txt
%{_datadir}/scim/icons/CNS11643.png
%{_datadir}/scim/icons/CangJie.png
%{_datadir}/scim/icons/Cantonese.png
%{_datadir}/scim/icons/Dayi.png
%{_datadir}/scim/icons/EZ.png
%{_datadir}/scim/icons/Erbi.png
%{_datadir}/scim/icons/Erbi-QS.png
%{_datadir}/scim/icons/Jyutping.png
%{_datadir}/scim/icons/Simplex.png
%{_datadir}/scim/icons/Stroke5.png
%{_datadir}/scim/icons/Wu.png
%{_datadir}/scim/icons/Ziranma.png
%{_datadir}/scim/tables/CNS11643.bin
%{_datadir}/scim/tables/CangJie.bin
%{_datadir}/scim/tables/Cantonese.bin
%{_datadir}/scim/tables/Dayi3.bin
%{_datadir}/scim/tables/EZ-Big.bin
%{_datadir}/scim/tables/Erbi.bin
%{_datadir}/scim/tables/Erbi-QS.bin
%{_datadir}/scim/tables/Jyutping.bin
%{_datadir}/scim/tables/Simplex.bin
%{_datadir}/scim/tables/Stroke5.bin
%{_datadir}/scim/tables/Wu.bin
%{_datadir}/scim/tables/ZhuYin-Big.bin
%{_datadir}/scim/tables/Ziranma.bin

%if %{build_indic_tables}
%files gujarati
%defattr(-,root,root)
%{_datadir}/scim/icons/Gujarati-inscript.png
%{_datadir}/scim/icons/Gujarati-phonetic.png
%{_datadir}/scim/tables/Gujarati-inscript.bin
%{_datadir}/scim/tables/Gujarati-phonetic.bin

%files hindi
%defattr(-,root,root)
%{_datadir}/scim/icons/Hindi-inscript.png
%{_datadir}/scim/icons/Hindi-phonetic.png
%{_datadir}/scim/tables/Hindi-inscript.bin
%{_datadir}/scim/tables/Hindi-phonetic.bin
%endif

%if %{build_jk_tables}
%files japanese
%defattr(-,root,root)
%doc tables/ja/kanjidic*
%{_datadir}/scim/icons/HIRAGANA.png
%{_datadir}/scim/icons/KATAKANA.png
%{_datadir}/scim/icons/Nippon.png
%{_datadir}/scim/tables/HIRAGANA.bin
%{_datadir}/scim/tables/KATAKANA.bin
%{_datadir}/scim/tables/Nippon.bin
%endif

%if %{build_indic_tables}
%files kannada
%defattr(-,root,root)
%{_datadir}/scim/icons/Kannada-inscript.png
%{_datadir}/scim/icons/Kannada-kgp.png
%{_datadir}/scim/tables/Kannada-inscript.bin
%{_datadir}/scim/tables/Kannada-kgp.bin
%endif

%if %{build_jk_tables}
%files korean
%defattr(-,root,root)
%{_datadir}/scim/icons/Hangul.png
%{_datadir}/scim/icons/Hanja.png
%{_datadir}/scim/tables/Hangul.bin
%{_datadir}/scim/tables/HangulRomaja.bin
%{_datadir}/scim/tables/Hanja.bin
%endif

%if %{build_indic_tables}
%files malayalam
%defattr(-,root,root)
%{_datadir}/scim/icons/Malayalam-inscript.png
%{_datadir}/scim/tables/Malayalam-inscript.bin
%endif

%files nepali
%defattr(-,root,root)
%{_datadir}/scim/icons/Nepali.png
%{_datadir}/scim/tables/Nepali_*.bin

%if %{build_indic_tables}
%files punjabi
%defattr(-,root,root)
%{_datadir}/scim/icons/Punjabi-inscript.png
%{_datadir}/scim/icons/Punjabi-jhelum.png
%{_datadir}/scim/icons/Punjabi-phonetic.png
%{_datadir}/scim/tables/Punjabi-inscript.bin
%{_datadir}/scim/tables/Punjabi-jhelum.bin
%{_datadir}/scim/tables/Punjabi-phonetic.bin
%endif

%files russian
%defattr(-,root,root)
%{_datadir}/scim/icons/RussianTraditional.png
%{_datadir}/scim/icons/Yawerty.png
%{_datadir}/scim/tables/RussianTraditional.bin
%{_datadir}/scim/tables/Translit.bin
%{_datadir}/scim/tables/Yawerty.bin

%if %{build_indic_tables}
%files tamil
%defattr(-,root,root)
%{_datadir}/scim/icons/Tamil-inscript.png
%{_datadir}/scim/icons/Tamil-phonetic.png
%{_datadir}/scim/icons/Tamil-remington.png
%{_datadir}/scim/tables/Tamil-inscript.bin
%{_datadir}/scim/tables/Tamil-phonetic.bin
%{_datadir}/scim/tables/Tamil-remington.bin
%endif

%files thai
%defattr(-,root,root)
%{_datadir}/scim/icons/Thai.png
%{_datadir}/scim/tables/Thai.bin

%if %{build_indic_tables}
%files telugu
%defattr(-,root,root)
%{_datadir}/scim/icons/Telugu-inscript.png
%{_datadir}/scim/tables/Telugu-inscript.bin
%endif

%files ukrainian
%defattr(-,root,root)
%{_datadir}/scim/tables/Ukrainian-Translit.bin

%files vietnamese
%defattr(-,root,root)
%{_datadir}/scim/icons/Viqr.png
%{_datadir}/scim/tables/Viqr.bin

%files additional
%defattr(-,root,root)
%{_datadir}/scim/icons/IPA-X-SAMPA.png
%{_datadir}/scim/icons/LaTeX.png
%{_datadir}/scim/tables/IPA-X-SAMPA.bin
%{_datadir}/scim/tables/LaTeX.bin

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.9-10m)
- rebuild for glib 2.33.2

* Wed Jan 25 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.9-9m)
- re-enable optflags
- Obsoletes: skim-scim-tables

* Tue Jan 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.9-8m)
- Obsoletes: scim-skim-tables

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.9-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.9-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.9-5m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.9-4m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.9-2m)
- change BuildRequires: skim to skim-devel

* Mon May 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.9-1m)
- version 0.5.9
- add 2 packages scim-tables-chinese-extra and scim-tables-ukrainian
- import CangJie5.png from Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.7-8m)
- rebuild against rpm-4.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.7-7m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.7-6m)
- rebuild against gcc43

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.7-5m)
- rebuild with skim-1.4.4-0.20060301.7m

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.7-4m)
- specify KDE3 headers and libs
- modify BuildRequires

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.7-3m)
- %%NoSource -> NoSource

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.7-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Wed Apr 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.7-1m)
- initial package for Momonga Linux
- import 2 patches from Fedora
