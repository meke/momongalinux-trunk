%global momorel	2
%global octpkg image

Name:           octave-%{octpkg}
Version:        1.0.15
Release:        %{momorel}m%{?dist}
Summary:        Image processing for Octave
Group:          Applications/Engineering
License:        GPLv2+
URL:            http://octave.sourceforge.net/image/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  octave-devel  

Requires:       octave(api) = %{octave_api}
Requires(post): octave
Requires(postun): octave


Obsoletes:      octave-forge <= 20090607


%description
The Octave-forge Image package provides functions for processing images.
The package also provides functions for feature extraction, image
statistics, spatial and geometric transformations, morphological
operations, linear filtering, and much more.

%prep
%setup -q -n %{octpkg}-%{version}
chmod -x src/*.c

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install

%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)
%{octpkglibdir}

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo


%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.15-2m)
- rebuild against octave-3.6.1-1m

* Fri Sep 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.15-1m)
- update to 1.0.15

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.14-1m)
- initial import from fedora
