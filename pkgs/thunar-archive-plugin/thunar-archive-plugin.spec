%global momorel 1

%global xfce4ver 4.11.0
%global thunarver 1.6.3

%global major 0.3

Name:           thunar-archive-plugin
Version:        0.3.1
Release:        %{momorel}m%{?dist}
Summary:        Archive plugin for the Thunar file manager
Group:          User Interface/Desktops
License:        LGPL
URL:            http://foo-projects.org/~benny/projects/thunar-archive-plugin/
#Source0:        http://download.berlios.de/xfce-goodies/%{name}-%{version}.tar.bz2
Source0:        http://archive.xfce.org/src/thunar-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
#BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  Thunar-devel >= %{thunarver}
BuildRequires:  libxml2-devel, gettext
Requires:       Thunar >= %{thunarver}, xarchiver >= 0.2

%description
The Thunar Archive Plugin allows you to create and extract archive files using 
the file context menus in the Thunar file manager. Starting with version 0.2.0, 
the plugin provides a generic scripting interface for archive managers. 

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT transform='s,x,x,'

# We need to install file-roller.tap as gnome-file-roller.tap, because the name 
# has to match the basename of the desktop-file in %{_datadir}/applications.
rm -f $RPM_BUILD_ROOT%{_libexecdir}/thunar-archive-plugin/file-roller.tap
install -p -m 755 scripts/file-roller.tap \
   $RPM_BUILD_ROOT%{_libexecdir}/thunar-archive-plugin/gnome-file-roller.tap

find $RPM_BUILD_ROOT -type f -name "*.la" -exec rm -f {} ';'
%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README THANKS NEWS
%doc scripts/template.tap
%{_libdir}/thunarx-2/thunar-archive-plugin.so
%{_libexecdir}/thunar-archive-plugin/*.tap
%{_datadir}/icons/hicolor/16x16/apps/tap-*.png

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1
- build against xfce4-4.11.0

* Mon Oct 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-4m)
- BR: Thunar-devel >= 1.5.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.0-3m)
- rebuild against Thunar 1.5.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.0-2m)
- rebuild against xfce4-4.10.0

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.4-12m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.4-11m)
- rebuild against xfce4-4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-10m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-8m)
- define libtoolize

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.4-7m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4-6m)
- rebuild against rpm-4.6

* Thu Sep 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4-5m)
- correct filenames

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.4-4m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.4-3m)
- rebuild against xfce4 4.4.2

* Sun Apr 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4-2m)
- remove %%{_libexecdir}/thunar-archive-plugin from %%files
  it's provided by xarchiver

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.4-1m)
- import from fc-devel to Momonga
- use rm -f at remove file-roller.tap if ! exist that file

* Mon Jan 22 2007 Christoph Wickert <fedora christoph-wickert de> - 0.2.4-2
- Rebuild for Thunar 0.8.0.

* Sat Jan 20 2007 Christoph Wickert <fedora christoph-wickert de> - 0.2.4-1
- Update to 0.2.4.

* Sun Nov 12 2006 Christoph Wickert <fedora christoph-wickert de> - 0.2.2-2
- Require xarchiver.
- Shorten %%description.
- Use thunarver macro.
- Include template.tap to %%doc.

* Sat Nov 11 2006 Christoph Wickert <fedora christoph-wickert de> - 0.2.2-1
- Update to 0.2.2.

* Wed Sep 13 2006 Christoph Wickert <fedora christoph-wickert de> - 0.2.0-1
- Initial Fedora Extras Version.
