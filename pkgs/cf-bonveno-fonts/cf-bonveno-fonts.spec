%global momorel 6

%define	fontname	cf-bonveno
%define fontconf	60-%{fontname}.conf


Name:		%{fontname}-fonts
Version:	1.1
Release:	%{momorel}m%{?dist}
Summary:	A fun font by Barry Schwartz

Group:		User Interface/X
License:	GPLv2+
URL:		http://home.comcast.net/~crudfactory/cf3/bonveno.xhtml
Source0:	http://home.comcast.net/~crudfactory/cf3/fonts/BonvenoCF-1.1.zip
Source1:	%{name}-fontconfig.conf
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch: 	noarch
BuildRequires:	fontforge, fontpackages-devel
Requires:	fontpackages-filesystem


%description
A set of fun fonts from the crud factory.

%prep
%setup -q -c


for txt in COPYING README ; do
	sed 's/\r//' $txt > $txt.new
	touch -r $txt $txt.new
	mv $txt.new $txt
done

%build
fontforge -lang=ff -script "-" BonvenoCF*.sfd <<EOF
i = 1 
while ( i < \$argc )
	Open (\$argv[i], 1)
	Generate (\$fontname + ".ttf")
	PrintSetup (5) 
	PrintFont (0, 0, "", \$fontname + "-sample.pdf")
	Close()
	i++ 
endloop
EOF

%install
rm -fr %{buildroot}

install -dm 755 %{buildroot}%{_fontdir}
install -pm 644 *.ttf  %{buildroot}%{_fontdir}

install -m 755 -d %{buildroot}%{_fontconfig_templatedir} \
		%{buildroot}%{_fontconfig_confdir}

install -m 644 -p %{SOURCE1} \
		%{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} \
	%{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -rf %{buildroot}

%_font_pkg -f %{fontconf} *.ttf


%doc  COPYING* README*

%dir %{_fontdir}/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- import from Fedora

* Mon Mar 16 2009 Ankur Sinha <ankursinha at fedoraproject.org> - 1.1-8
- Changed according to #490371

* Sun Mar 15 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net> - 1.1-5
- Make sure F11 font packages have been built with F11 fontforge

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 20 2009 Ankur Sinha <ankursinha@fedoraproject.org>
- 1.1-3
- changed font config file according to files provided in the fontpackages-devel package.
* Sun Jan 4 2009 Ankur Sinha <ankursinha@fedoraproject.org>
- 1.1-2
- rebuilt as per new font packaging guidelines
* Mon Dec 15 2008 Ankur Sinha <ankursinha@fedoraproject.org>
- 1.1-1
- rebuilt on fedora 10 (#457955 at bugzilla)

