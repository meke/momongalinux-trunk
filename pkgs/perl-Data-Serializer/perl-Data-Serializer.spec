%global         momorel 3

Name:           perl-Data-Serializer
Version:        0.60
Release:        %{momorel}m%{?dist}
Summary:        Data::Serializer Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Data-Serializer/
Source0:        http://www.cpan.org/authors/id/N/NE/NEELY/Data-Serializer-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-AutoLoader
BuildRequires:  perl-Bencode
#BuildRequires:  perl-Compress-PPMd
BuildRequires:  perl-Config-General
BuildRequires:  perl-Convert-Bencode
BuildRequires:  perl-Convert-Bencode_XS
BuildRequires:  perl-Crypt-Blowfish
BuildRequires:  perl-Crypt-CBC
BuildRequires:  perl-Data-Denter
BuildRequires:  perl-Data-Dumper >= 2.08
BuildRequires:  perl-Data-Taxi
BuildRequires:  perl-Digest-SHA
BuildRequires:  perl-FreezeThaw
BuildRequires:  perl-IO
BuildRequires:  perl-IO-Compress
BuildRequires:  perl-JSON
BuildRequires:  perl-JSON-XS
BuildRequires:  perl-MIME-Base64
BuildRequires:  perl-Module-Build
BuildRequires:  perl-PathTools
BuildRequires:  perl-PHP-Serialization
BuildRequires:  perl-Storable
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-XML-Dumper
BuildRequires:  perl-XML-Simple
BuildRequires:  perl-YAML
BuildRequires:  perl-YAML-Syck
Requires:       perl-AutoLoader
Requires:       perl-Bencode
#Requires:       perl-Compress-PPMd
Requires:       perl-Config-General
Requires:       perl-Convert-Bencode
Requires:       perl-Convert-Bencode_XS
Requires:       perl-Crypt-Blowfish
Requires:       perl-Crypt-CBC
Requires:       perl-Data-Denter
Requires:       perl-Data-Dumper >= 2.08
Requires:       perl-Data-Taxi
Requires:       perl-Digest-SHA
Requires:       perl-FreezeThaw
Requires:       perl-IO
Requires:       perl-IO-Compress
Requires:       perl-JSON
Requires:       perl-JSON-XS
Requires:       perl-MIME-Base64
Requires:       perl-PHP-Serialization
Requires:       perl-Storable
Requires:       perl-XML-Dumper
Requires:       perl-XML-Simple
Requires:       perl-YAML
Requires:       perl-YAML-Syck
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Provides a unified interface to the various serializing modules currently
available. Adds the functionality of both compression and encryption.

%prep
%setup -q -n Data-Serializer-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes examples README
%{perl_vendorlib}/Data/Serializer
%{perl_vendorlib}/Data/Serializer.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-2m)
- rebuild against perl-5.18.2

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-9m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-8m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-7m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-6m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-5m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-4m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.57-2m)
- rebuild for new GCC 4.6

* Tue Jan 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Sat Jan 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Sat Jan 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Fri Jan 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Tue Jan 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.49-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.49-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.49-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-2m)
- rebuild against perl-5.10.1

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.48-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.44-2m)
- rebuild against gcc43

* Sat Mar 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Sat Dec 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Sat Oct 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.36-2m)
- use vendor

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.36-1m)
- spec file was autogenerated
