%global momorel 1

Name: keychain
Version: 2.7.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
Source0: http://www.funtoo.org/archive/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Requires: openssh-clients
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary: an OpenSSH key manager
Url: http://www.funtoo.org/wiki/Keychain
BuildArchitectures: noarch

%description
Keychain is an OpenSSH key manager, typically run from ~/.bash_profile.  When
run, it will make sure ssh-agent is running; if not, it will start ssh-agent.
It will redirect ssh-agent's output to ~/.ssh-agent-[hostname], so that cron
jobs that need to use ssh-agent keys can simply source this file and make the
necessary passwordless ssh connections.  In addition, when keychain runs, it
will check with ssh-agent and make sure that the ssh RSA/DSA keys that you
specified on the keychain command line have actually been added to ssh-agent.
If not, you are prompted for the appropriate passphrases so that they can be
added by keychain.

%prep
%setup -q

%install
rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_bindir}
%__install keychain %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/keychain
%doc COPYING.txt ChangeLog README.rst VERSION

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.8-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.8-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.8-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.8-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.8-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.8-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.8-3m)
- %%NoSource -> NoSource

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.8-2m)
- %%NoSource -> NoSource

* Wed Nov 08 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.8-1m)
- update to 2.6.8

* Mon May 22 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.6.2-1m)
- update to 2.6.2

* Sun Aug 22 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.3.5-1m)
  revised source url
  update to 2.3.5

* Mon Nov 24 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.3-1m)
- upgrade 2.0.3

* Sat Sep 14 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.2-2m)
- fix version number printed in help message

* Thu Aug 29 2002 Junichiro Kita <kita@momonga-linux.org>
- (2.0.2-1m)
- ver up

* Sun Mar 10 2002 kitaj <kita@kitaj.no-ip.com>
- initial build
