%global momorel 1
%global iconssylver 3.0
%global rcver beta1
%global prever 1
%global srcname %{name}-%{version}%{?prever:%{rcver}}


# 1 to enable gpgme support
%global gpgme 1

# 1 to enable openldap support
%global openldap 1

# 1 to enable openssl support
%global openssl 1

# 1 to enable compface support
%global compface 0

# 1 to enable jpilot support
%global jpilot 0

# 1 to enable gtkspell support
%global gtkspell 0

# 1 to enable auto update check support
%global updatecheck 0

Summary: a GTK+ based, lightweight, and fast e-mail client
Name: sylpheed

### default configuration
# If you'd like to change this configuration, please copy it to
# ${HOME}/rpm/specopt/sylpheed.specopt and edit it.

## Configuration
##### for version 3.1, temporary disabled #####
%{?!replace_icons:      %global replace_icons              1}

Version: 3.5.0
Release: %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: Applications/Internet
URL: http://sylpheed.sraoss.jp/
Source0: http://sylpheed.sraoss.jp/%{name}/v3.5%{?prever:beta}/%{srcname}.tar.bz2
Source1 : http://www.coonsden.com/dl0ads/Sylpheed%{iconssylver}_icon-set.tar.gz
Patch0: %{name}-3.1.0-stock_spam_16.patch
Patch1: %{name}-2.2.0beta1-momonga.patch
Patch2: %{name}-foldername-hack.patch
Patch11: %{name}-0.8.2-get_specific_account.patch
Patch12: %{name}-3.1.1-use-firefox-new-tab.patch
Patch13: %{name}-3.1.0beta7-change_search_default.patch
NoSource: 0
Requires(post): coreutils
Requires(post): desktop-file-utils
Requires(post): gtk2
Requires(post): /sbin/ldconfig
Requires(postun): coreutils
Requires(postun): desktop-file-utils
Requires(postun): gtk2
Requires(postun): /sbin/ldconfig
Requires: gdk-pixbuf
Requires: glib1
Requires: gtk2
Requires: libjconv
BuildRequires: ImageMagick
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gdk-pixbuf-devel >= 0.13.0
BuildRequires: glib1-devel
BuildRequires: gtk2-devel
BuildRequires: libX11-devel
BuildRequires: libXcursor-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libjconv
BuildRequires: momonga-rpmmacros >= 20040310-6m
BuildRequires: readline-devel

%if %{gpgme}
Requires: gpgme
BuildRequires: gpgme-devel
BuildRequires: libgpg-error-devel
%endif

%if %{openldap}
Requires: cyrus-sasl
Requires: gdbm
Requires: openldap
Requires: pam
BuildRequires: cyrus-sasl-devel
BuildRequires: gdbm-devel
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: pam-devel

%endif

%if %{openssl}
Requires: openssl
BuildRequires: openssl-devel >= 1.0.0
%endif

%if %{compface}
BuildRequires: faces-devel
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Sylpheed is an e-mail client (and news reader) based on GTK+, running on
X Window System, and aiming for
 * Quick response
 * Simple, graceful, and well-polished interface
 * Easy configuration
 * Intuitive operation
 * Abundant features
The appearance and interface are similar to some popular e-mail clients for
Windows, such as Outlook Express, Becky!, and Datula. The interface is also
designed to emulate the mailers on Emacsen, and almost all commands are
accessible with the keyboard.

The messages are managed by MH format, and you'll be able to use it together
with another mailer based on MH format (like Mew). You can also utilize
fetchmail or/and procmail, and external programs on receiving (like inc or
imget).

%package devel
Summary: Development files for sylpheed
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This program is an X based fast email client which has features like:

o user-friendly and intuitive interface
o integrated NetNews client (partially implemented)
o ability of keyboard-only operation
o Mew/Wanderlust-like key bind
o multipart MIME
o unlimited multiple account handling
o message queueing
o assortment function
o XML-based address book

See /usr/share/doc/sylpheed*/README for more information.

this package contains development files only

%prep
%if %{replace_icons}
%setup -q -n %{srcname} -a 1
#setup -q -n #{name}-#{version} -a 1
# fix permission
find Sylpheed%{iconssylver}_icon-set/src/icons -type f -exec chmod 644 {} \;
# preparing for stock_spam_16
%patch0 -p0 -b .install-stock_spam_16
## for 3.4.0-beta6 or later
cp src/icons/html.h Sylpheed%{iconssylver}_icon-set/src/icons
pushd Sylpheed%{iconssylver}_icon-set/src/icons
convert -scale 16x16 stock_spam.png stock_spam_16.png
gdk-pixbuf-csource --name=stock_spam_16 stock_spam_16.png > stock_spam_16.h
popd
# clean up upstream sources
rm -rf src/icons
# install sophisticated icons
mv Sylpheed%{iconssylver}_icon-set/src/icons src/icons
# move icon's COPYING
mv Sylpheed%{iconssylver}_icon-set/COPYING COPYING.icons
# move icons readme
mv Sylpheed%{iconssylver}_icon-set/readme.txt README.icons
%else
%setup -q -n %{srcname}
%endif

%patch1 -p1 -b .momonga
%patch2 -p1 -b .foldername~

%patch12 -p1 -b .firefox-new-tab~
%patch13 -p1 -b .search

%build
%if ! %{_ipv6}
IPV6="--disable-ipv6"
%endif

%if %{openldap}
LDAP="--enable-ldap"
%endif

%if ! %{gpgme}
GPG="--disable-gpgme"
%endif

%if ! %{openssl}
OPENSSL="--disable-ssl"
%endif

%if ! %{compface}
FASE="--disable-compface"
%endif

%if %{jpilot}
JPILOT="--enable-jpilot"
%endif

%if ! %{gtkspell}
GTKSPELL="--disable-gtkspell"
%endif

%if ! %{updatecheck}
UPDATECHECK="--disable-updatecheck"
%endif

%configure $OPENSSL $IPV6 $LDAP $GPG $FASE $JPILOT $GTKSPELL $UPDATECHECK
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall transform='s,x,x,'

# get rid of la file
find %{buildroot} -name \*.la -exec rm {} \;

# install and link icons
mkdir -p mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48,64x64,128x128}/apps
convert -scale 16x16 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 22x22 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
convert -scale 32x32 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
install -m 644 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
install -m 644 %{name}-64x64.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
install -m 644 %{name}-128x128.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
rm -f %{buildroot}%{_datadir}/pixmaps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png
ln -s ../icons/hicolor/64x64/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}-64x64.png
ln -s ../icons/hicolor/128x128/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}-128x128.png

# Install menu entries
mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= \
    --add-category=Network \
    --add-mime-type=x-scheme-handler/mailto \
    --dir %{buildroot}%{_datadir}/applications \
    --delete-original \
    %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
%{_bindir}/update-desktop-database &> /dev/null || :

%postun
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
%{_bindir}/update-desktop-database &> /dev/null || :

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog* INSTALL* LICENSE NEWS* README* TODO*
%{_bindir}/%{name}
%{_libdir}/libsylph*.so.*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}*.png

%files devel
%defattr(-,root,root,-)
%doc PLUGIN*
%{_includedir}/%{name}
%{_libdir}/libsylph*.so

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-0.1.1m)
- update to 3.5.0beta1

* Fri Apr  4 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1
- includes an important bug fix

* Mon Mar 31 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-1m)
- update to 3.4.0

* Wed Mar 26 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.9.1m)
- update to 3.4rc

* Thu Mar 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.8.1m)
- update to 3.4beta8

* Fri Nov 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.7.1m)
- update to 3.4beta7

* Tue Oct 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.6.1m)
- update to 3.4beta6

* Wed Aug 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.5.1m)
- update to 3.4beta5

* Wed May 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.4.1m)
- update to 3.4beta4

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.3.1m)
- update to 3.4beta3

* Fri Feb 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.2.1m)
- update to 3.4beta2

* Thu Jan 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-0.1.1m)
- update to 3.4beta1

* Fri Nov  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-1m)
- update to 3.3.0

* Wed Oct 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-0.3.1m)
- update to 3.3rc

* Fri Sep 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-0.2.1m)
- update to 3.3beta2

* Fri Jul 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-0.1.1m)
- update to 3.3beta1

* Fri Jun 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.9.1m)
- update to 3.2beta9

* Fri Jun  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.8.1m)
- update to 3.2beta8

* Fri Jun  1 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.0-0.7.3m)
- add mimetype entry to desktop file

* Sun Apr 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.7.2m)
- remove CPPFLAGS, fixed by upstream

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.7.1m)
- update to 3.2beta7

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-0.6.2m)
- build fix

* Wed Feb 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.6.1m)
- update to 3.2beta6

* Wed Dec 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.5.1m)
- update to 3.2beta5

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.4.1m)
- update to 3.2beta4

* Sat Sep 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.3.1m)
- update to 3.2beta3

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.2.1m)
- update to 3.2beta2

* Sat Jul  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.1.1m)
- update to 3.2beta1

* Sun May  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-2m)
- rebuild for new GCC 4.6

* Mon Jan 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0

* Mon Jan 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.8.1m)
- update to 3.1rc

* Mon Jan 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.7.1m)
- update to 3.1beta7

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.6.1m)
- update to 3.1beta6

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.5.1m)
- update to 3.1beta5

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.4.1m)
- update to 3.1beta4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-0.3.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-0.3.3m)
- full rebuild for mo7 release

* Fri Aug 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-0.3.2m)
- revive Johan's cool-icons

* Wed Aug 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.3.1m)
- update to 3.1beta3

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.2.1m)
- update to 3.1beta2

* Thu Jul  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.1.2m)
- disable auto update check

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-0.1.1m)
- update to 3.1beta1

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.2-3m)
- update icon theme to version 2, thanks JW and Mr. Johan Spee
- add %%post and %%postun

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.2-2m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.2-1m)
- version 3.0.2

* Wed Mar 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-1m)
- version 3.0.1
- add PLUGIN* to %%doc of devel

* Wed Feb 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Fri Feb 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.9.1m)
- update to 3.0.0rc

* Fri Feb 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.8.1m)
- update to 3.0.0beta8

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.7.1m)
- update to 3.0.0beta7

* Thu Jan 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.6.1m)
- update to 3.0.0beta6

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.5.1m)
- update to 3.0.0beta5

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-0.4.2m)
- delete __libtoolize hack

* Thu Dec 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.0-0.4.1m)
- update to 3.0.0beta4
- update foldername-hack.patch

* Fri Nov 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.0-0.3.1m)
- update to 3.0.0beta3

* Tue Nov 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.2.1m)
- update to 3.0.0beta2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.1.1m)
- update to 3.0.0beta1

* Thu Aug 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-1m)
- version 2.7.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-1m)
- update to 2.7.0
- separate devel package

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.0-4m)
- define __libtoolize (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-2m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Tue Dec  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-0.3.1m)
- update to 2.6.0rc
- remove patch for gtk-2.14.4
- add patch to build fix

* Mon Nov 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.0-0.2.1m)
- update to 2.6.0beta2

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-0.1.1m)
- update to 2.6.0beta1

* Sun Nov  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-8m)
- add patch for gtk 2.14.4

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-7m)
- update to 2.5.0

* Fri Jun  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-6m)
- update to 2.5.0rc2

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-5m)
- rebuild against openssl-0.9.8h-1m

* Tue May 20 2008 NARITA Koichi <pulsar@momonga-iinux.org>
- (2.5.0-4m)
- update to 2.5.0rc

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-3m)
- update to 2.5.0beta3, but momorel is 3m...

* Mon Apr 14 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.5.0-0.2.1m)
- up to 2.5.0beta2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.0-0.1.2m)
- rebuild against gcc43

* Fri Feb 29 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.5.0-0.1.1m)
- up to 2.5.0beta1

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.8-2m)
- rebuild against openldap-2.4.8

* Mon Dec 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-1m)
- update to 2.4.8

* Thu Oct  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.7-1m)
- update to 2.4.7

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-1m)
- update to 2.4.6

* Fri Aug 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5
- including CVE-2007-2958 security fix, remove patch50

* Sun Aug 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-2m)
- [SECURITY] CVE-2007-2958
- import security fix patch from FC-devel

* Fri Jul 20 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.4-1m)
- up to 2.4.4

* Sat Jun 30 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.3-1m)
- up to 2.4.3

* Fri May 18 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.2-1m)
- up to 2.4.2

* Sun May  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-2m)
- add Source1

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.1-1m)
- version 2.4.1

* Tue May  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-3m)
- clean up %%doc

* Tue May  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-2m)
- add new icon theme, thanks JW and Mr. Johan Spee
- use specopt and set replace_icons 1, test new icon's look and feel
- clean up spec file

* Sat Apr 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Thu Apr 19 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.0-0.9.1m)
- up to 2.4.0rc

* Thu Apr 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-0.8.1m)
- update to 2.4.0beta8

* Tue Apr  3 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.0-0.7.1m)
- up to 2.4.0beta7

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-0.6.2m)
- BuildRequires: freetype2-devel -> freetype-devel

* Thu Mar 29 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.0-0.6.1m)
- up to 2.4.0beta6

* Fri Mar 16 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.0-0.5.1m)
- up to 2.4.0beta5

* Thu Feb 15 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.0-0.4.1m)
- up to 2.4.0beta4

* Thu Feb  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-0.3.1m)
- update to 2.4.0beta3

* Sat Feb  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-0.2.1m)
- update to 2.4.0beta2

* Tue Jan 30 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-0.1.2m)
- add --disable-gtkspell

* Tue Jan 30 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.0-0.1.1m)
- up to 2.4.0beta1

* Wed Jan 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Sun Dec 24 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Tue Dec 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-0.8.1m)
- update to 2.3.0rc

* Thu Dec 14 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-0.7.1m)
- update to 2.3.0beta7

* Thu Dec  7 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-0.6.1m)
- update to 2.3.0beta6

* Tue Dec  5 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.3.0-0.5.2m)
- change default condition of mail search

* Fri Nov 10 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-0.5.1m)
- update to 2.3.0beta5

* Tue Oct 31 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-0.4.1m)
- update to 2.3.0beta4

* Tue Oct 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-0.3.1m)
- update to 2.3.0beta3

* Wed Oct  4 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-0.2.1m)
- update to 2.3.0beta2

* Fri Sep 29 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-0.1.1m)
- update to 2.3.0beta1

* Fri Sep 22 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.9-1m)
- update to 2.2.9
- change URL

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.7-2m)
- rebuild against expat-2.0.0-1m

* Mon Jul 31 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.7-1m)
- update to 2.2.7

* Tue Jun 27 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.6-2m)
- now IPAPGothic/Mincho is default font

* Sat Jun 10 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.6-1m)
- update to 2.2.6

* Tue May 30 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.5-1m)
- update to 2.2.5

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.4-2m)
- rebuild against openssl-0.9.8a

* Wed Mar 29 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.2.4-1m)
- update to 2.2.4

* Tue Mar 14 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.2.3-1m)
- update to 2.2.3

* Fri Mar 03 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.2-1m)
- update to 2.2.2

* Mon Feb 27 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.1-1m)
- update to 2.2.1

* Tue Feb 14 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.2.0-1m)
- version 2.2.0

* Thu Feb 09 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.9.1m)
- update tp 2.2.0rc
- delete sylpheed-2.2.0beta8-external-editor.patch

* Tue Feb 07 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.8.2m)
- add sylpheed-2.2.0beta8-external-editor.patch (from [sylpheed-jp:03306])

* Mon Feb 06 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.8.1m)
- update to 2.2.0beta8
- delete quick-search patch

* Sat Feb 04 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.7.2m)
- apply quick-search patch (from [sylpheed-jp:03302])

* Tue Jan 31 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.7.1m)
- update to 2.2.0beta7

* Wed Jan 25 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.6.1m)
- update to 2.2.0beta6

* Fri Jan 20 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.5.1m)
- update to 2.2.0beta5

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.0-0.4.2m)
- rebuild against openldap-2.3.11

* Fri Jan 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-0.4.1m)
- update to 2.2.0beta4

* Wed Jan 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-0.3.1m)
- update to 2.2.0beta3

* Tue Dec 27 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.2.1m)
- update to 2.2.0beta2

* Fri Dec 23 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.0-0.1.2m)
- change Requires section from "gtk+ >= 2.4" to "gtk+ >= 2.6.0"

* Thu Dec 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-0.1.1m)
- update to 2.2.0beta1
- update patches
- change License from GPL to LGPL

* Tue Nov  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-1m)
- version 2.0.4
- [SECURITY] ldif.c: ldif_get_line(): fixed buffer overflow

* Wed Oct 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-1m)
- version 2.0.3

* Thu Sep 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- version 2.0.2

* Fri Aug 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-1m)
- version 2.0.1

* Fri Jul 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1m)
- version 2.0.0

* Fri Jul 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-0.7.1m)
- update to 2.0.0rc

* Thu Jul 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-0.6.1m)
- update to 2.0.0beta6
- move sylpheed-*.png from %%doc to %%{_datadir}/pixmaps/

* Sat Jul  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-0.5.1m)
- update to 2.0.0beta5

* Mon Jul  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-0.4.1m)
- update to 2.0.0beta4

* Sat Jun 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-0.3.1m)
- update to 2.0.0beta3

* Fri Jun 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-0.2.1m)
- update to 2.0.0beta2

* Mon Jun 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-0.1.1m)
- update to 2.0.0beta1

* Sun May 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.12-1m)
- update to 1.9.12

* Thu May 19 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.9.11-1m)
- up to 1.9.11

* Sat May 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.10-1m)
- update to 1.9.10
- update Patch0: momonga.patch
- remove Patch3: desktop.patch
- modify %%doc

* Wed Apr 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.9.9-1m)
- up to 1.9.9

* Wed Apr 13 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.9.8-1m)
- up to 1.9.8

* Sun Apr  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.7-1m)
- update to 1.9.7

* Wed Mar 30 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.6-1m)
- upgrade

* Fri Mar 25 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.4-1m)
- up to 1.0.4
- [SECURITY] Buffer Overflow http://www.tmtm.org/cgi-bin/w3ml/sylpheed-jp/msg/2876

* Sat Mar 05 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- move sylpheed.desktop to %%{_datadir}/applications/

* Wed Feb 02 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Fri Dec 24 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Wed Dec 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-0.10m)
- update to 1.0.0rc
- update Patch12

* Sat Oct 30 2004 kourin <kourin@momonga-linux.org>
- (1.0.0-0.1m)
- update to 1.0.0beta1

* Tue Jul 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.99-1m)
- update to 0.9.99

* Tue Jul 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.12-2m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Thu Jun 17 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.12-1m)
- update to 0.9.12

* Tue Jun  1 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.11-2m)
- disable jpilot

* Sun May 30 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.11-1m)
- update to 0.9.11

* Mon Mar 1 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.10-1m)
- update to 0.9.10

* Thu Jan 29 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9-1m)
- update to 0.9.9

* Mon Dec 15 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.8a-1m)
- update to 0.9.8a

* Sun Dec 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.7-3m)
- rebuild against openldap

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-2m)
- set %%define compface 0
- reported by ToshiOkada [Momonga-devel.ja:02282]
- s/%%define/%%global/g

* Wed Oct 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Mon Sep  22 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.6-1m)
  update to 0.9.6

* Sat Sep  6 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.5-1m)
  update to 0.9.5

* Tue Aug 19 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.4-2m)
- use mozilla-remote instead of "mozilla -remote openURL()" as default browser

* Mon Jul  28 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.4-1m)
  update to 0.9.4

* Fri Jul  4 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.3-1m)
  update to 0.9.3

* Tue Jul  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.2-2m)
- rebuild against cyrus-sasl-2.x

* Fri Jun  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.2-1m)
  update to 0.9.2

* Fri May 16 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.0-1m)
- update to 0.9.0

* Wed May 14 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.9.0-0.1m)
- update to 0.9.0pre1

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.8.11-2m)
  rebuild against openssl 0.9.7a

* Sat Mar  8 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.11-1m)
-  update to 0.8.11

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.10-2m)
- rebuild against for gdbm

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.8.10-1m)
  update to 0.8.10

* Mon Feb  3 2003 smbd <smbd@momonga-linux.org>
- (0.8.9-1m)
- up to 0.8.9

* Sat Nov 16 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.8.6-1m)
- up to 0.8.6

* Fri Oct  4 2002 smbd <smbd@momonga-linux.org>
- (0.8.5-1m)
- up to 0.8.5

* Wed Oct  1 2002 smbd <smbd@momonga-linux.org>
- (0.8.4-1m)
- up to 0.8.4

* Sun Sep 15 2002 smbd <smbd@momonga-linux.org>
- (0.8.3-1m)
- up to 0.8.3

* Tue Aug 27 2002 smbd <smbd@momonga-linux.org>
- (0.8.2-2m)
- apply patch modified by Toshi http://www.neverland.to/cgi-bin/diary/toshi/junk.rb?date=20020827#p03

* Tue Aug 27 2002 smbd <smbd@momonga-linux.org>
- (0.8.2-1m)
- up to 0.8.2

* Fri Jul 26 2002 smbd <smbd@momonga-linux.org>
- (0.8.1-1m)
- up to 0.8.1

* Tue Jul 23 2002 smbd <smbd@momonga-linux.org>
- (0.8.0-2m)
- add some patchs from sylpheed-jp

* Mon Jul 15 2002 smbd <smbd@momonga-linux.org>
-(0.8.0-1m)
- up to 0.8.0

* Mon Jul  1 2002 smbd <smbd@momonga-linux.org>
- (0.7.8-1m)
- up to 0.7.8
- enable gpgme

* Mon Jun 10 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.7-1k)
- up to 0.7.7

* Sun May 12 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.6-1k)
- up to 0.7.6
- add other size icons

* Wed Apr 24 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.5-6k)
- use official icon

* Sun Apr 21 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.5-4k)
- modify immediate_execution

* Sun Apr 21 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.5-2k)
- up to 0.7.5

* Fri Apr  5 2002 Toru Hoshina <t@kondara.org>
- (0.7.4-4k)
- procmsg_read_mark_file() sizeof fixed.

* Sat Mar  9 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.4-2k)
- up to 0.7.4

* Sun Mar  3 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.3-2k)
- up to 0.7.3

* Sun Feb 17 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.2-2k)
- up to 0.7.2

* Wed Jan  9 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.0-4k)
- revise sylpheed.desctop, thanks i_nakai! (http://nq10.gaiax.com/~member/monthlydiary.cgi?i_nakai@moomoo:200201#20020108-01)

* Tue Jan  8 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.7.0-2k)
- up to 0.7.0

* Sun Dec 16 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.6.6-2k)
- up to 0.6.6

* Thu Dec 13 2001 Toru Hoshina <t@kondara.org>
- (0.6.5-6k)
- Do not use size_t for packed binary to keep platform independency.
- dor some platforms, 'sizeof' would be dangerous...

* Wed Nov 14 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6.5-4k)
- zh_CN.GB2312 => zh_CN
- zh_TW.Big5 => zh_TW
-
* Thu Nov 08 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6.5-2k)
- up to 0.6.5

* Mon Oct 29 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6.4-4k)
- add BuildRequires: libjconv

* Thu Oct 24 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6.4-2k)
- up to 0.6.4
- modify spec file

* Tue Oct 16 2001 Masaru Sato <masachan@kondara.org>
- (0.6.3-4k)
- up to 4k for Mary errata

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- (0.6.3-2k)
- up to 0.6.3

* Sat Oct 06 2001 Masaru Sato <masachan@kondara.org>
- (0.6.2-4k)
- add configure option enable-ldap
- fix Requires: and BuildRequires:

* Fri Sep 21 2001 Toru Hoshina <t@kondara.org>
- (0.6.2-2k)
- up to 0.6.2

* Mon Sep 03 2001 Kusunoki Masanori <nori@kondara.org>
- (0.6.1-2k)
- up to 0.6.1

* Fri Aug 31 2001 Kusunoki Masanori <nori@kondara.org>
- (0.6.0-3k)
- up to 0.6.0
- enable ssl

* Tue Aug 21 2001 Motonobu Ichimura <famao@kondara.org>
- (0.5.3-3k)
- up to 0.5.3

* Tue Aug 07 2001 Motonobu Ichimura <famao@kondara.org>
- (0.5.2-3k)
- up to 0.5.2

* Mon Jul 16 2001 Motonobu Ichimura <famao@kondara.org>
- (0.5.0-3k)
- up to 0.5.0

* Fri May 11 2001 KUSUNOKI Masanori <nori@kondara.org>
- (0.4.66-2k)
- up to 0.4.66

* Thu May 10 2001 Akira Higuchi <a@kondara.org>
- (0.4.65-6k)
- changed title font

* Wed May  2 2001 Akira Higuchi <a@kondara.org>
- (0.4.65-4k)
- misc-fixed -> alias-fixed in prefs_common.c
- foldername-hack.patch

* Mon May 01 2001 Masanori Kusunoki <nori@kondara.org>
- (0.4.65-2k)
- up to 0.4.65

* Mon Apr 23 2001 Motonobu Ichimura <famao@kondara.org>
- (0.4.64-2k)
- up to 0.4.64

* Mon Mar 19 2001 Akira Higuchi <a@kondara.org>
- (0.4.62-5k)
- bug fixes in sylpheed-kondara.patch

* Mon Mar 12 2001 Kusunoki Masanori <nori@kondara.org>
- (0.4.62-3k)
- up to 0.4.62
- merged sylpheed-kondara.patch

* Tue Mar  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4.9-5k)
- errased IPv6 function with %{_ipv6} macro

* Wed Dec 20 2000 Motonobu Ichimura <famao@kondara.org>
- up to 0.4.9
- merged sylpheed-kondara.patch

* Fri Nov 20 2000 Akira Higuchi <a@kondara.org>
- changed default fonts
- fixed a minor bug in compose.c

* Thu Nov  9 2000 Tsutomu Yasuda <tom@kondara.org>
- update to 0.4.4

* Thu Sep 28 2000 Akira Higuchi <a@kondara.org>
- add alpha patch

* Thu Sep 28 2000 Masanori Kusunoki <nori@kondara.org>
- GNOME Icon Added. (Thanks for Yoichi Imai <yoichi@silver-forest.com>)

* Thu Sep 28 2000 Masanori Kusunoki <nori@kondara.org>
- Version 0.4.1.
- Enable IPv6.
- Disable Multithread. (Because conflict with IPv6 support.)
- %files updated.

* Thu Sep 28 2000 Masanori Kusunoki <nori@kondara.org>
- version 0.4.0

* Wed Sep 27 2000 Motonobu Ichimura <famao@kondara.org>
- version 0.4.0
- Wow!
- enable IPv6 support

* Tue Sep 26 2000 Akira Higuchi <a@kondara.org>
- version 0.3.99
- removed kondara patch, which is not needed anymore.

* Mon Sep 4 2000 Masanori Kusunoki <nori@kondara.org>
- version 0.3.28
- build against glibc-2.1.93.

* Sat Aug  5 2000 Akira Higuchi <a@kondara.org>
- version 0.3.24

* Thu Jul 27 2000 Akira Higuchi <a@kondara.org>
- fixed the bug that some guabage characters are added when we use
base64 for message body.

* Mon Jul 10 2000 Akira Higuchi <a@kondara.org>
- added support for many charsets

* Wed Jul 05 2000 HamHam <HamHam@kondara.org>
- version 0.3.21

* Fri Jun 30 2000 HamHam <HamHam@kondara.org>
- version 0.3.20

* Mon Jun 26 2000 HamHam <HamHam@kondara.org>
- version 0.3.19

* Sun Jun 18 2000 HamHam <HamHam@kondara.org>
- version 0.3.18

* Thu Jun 15 2000 HamHam <HamHam@kondara.org>
- version 0.3.17

* Mon Jun 12 2000 HamHam <HamHam@kondara.org>
- version 0.3.16

* Sun Jun 11 2000 HamHam <HamHam@kondara.org>
- version 0.3.15

* Tue Jun 06 2000 HamHam <HamHam@kondara.org>
- version 0.3.14

* Mon Jun 05 2000 HamHam <HamHam@kondara.org>
- version 0.3.13

* Sat Jun 03 2000 HamHam <HamHam@kondara.org>
- version 0.3.12

* Fri Jun 02 2000 HamHam <HamHam@kondara.org>
- version 0.3.11

* Sat May 20 2000 HamHam <HamHam@kondara.org>
- version 0.3.10

* Mon May 15 2000 HamHam <HamHam@kondara.org>
- version 0.3.8

* Thu May 11 2000 HamHam <HamHam@kondara.org>
- version 0.3.7a

* Wed May 10 2000 HamHam <HamHam@kondara.org>
- version 0.3.7
- remove Japanese description tag

* Sun May 07 2000 HamHam <HamHam@kondara.org>
- version 0.3.6a

* Wed May 03 2000 HamHam <HamHam@kondara.org>
- version 0.3.5

* Mon May 01 2000 HamHam <HamHam@kondara.org>
- version 0.3.4

* Mon Apr 24 2000 HamHam <HamHam@kondara.org>
- version 0.3.3

* Sun Apr 16 2000 HamHam <HamHam@kondara.org>
- version 0.3.2

* Fri Apr 14 2000 HamHam <HamHam@kondara.org>
- version 0.3.1

* Sun Apr 09 2000 HamHam <HamHam@kondara.org>
- version 0.3.0

* Wed Apr 05 2000 HamHam <HamHam@kondara.org>
- version 0.2.9

* Sat Mar 18 2000 HamHam <HamHam@kondara.org>
- version 0.2.8

* Wed Mar 15 2000 HamHam <HamHam@kondara.org>
- version 0.2.7

* Sat Mar 04 2000 HamHam <HamHam@kondara.org>
- version 0.2.6

* Sat Feb 26 2000 HamHam <HamHam@kondara.org>
- version 0.2.5

* Fri Feb 25 2000 HamHam <HamHam@kondara.org>
- kondarized :P

* Thu Feb 24 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.2.4

* Tue Feb 22 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.2.3

* Sun Feb 20 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.2.2

* Sat Feb 19 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.2.1

* Sat Feb 12 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.2.0pre8

* Sat Feb 12 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.2.0pre7

* Wed Feb 5 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.2.0pre3

* Wed Feb 5 2000 Yoichi Imai <yoichi@silver-forest.com>
- append "TODO.jp"

* Wed Feb 5 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.2.0pre1

* Wed Feb 4 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.23

* Wed Feb 2 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.21

* Tue Feb 1 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.20

* Tue Jan 25 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.19

* Sun Jan 23 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.17

* Sun Jan 16 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.15

* Sat Jan 15 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.14

* Fri Jan 14 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.13

* Thu Jan 13 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.12

* Wed Jan 12 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.11

* Tue Jan 11 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.10

* Mon Jan 10 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.8

* Sat Jan 8 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.7

* Fri Jan 7 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.6

* Thu Jan 6 2000 Yoichi Imai <yoichi@silver-forest.com>
- update for 0.1.5

* Sat Jan 1 2000 Yoichi Imai <yoichi@silver-forest.com>
- first release for version 0.1.0
