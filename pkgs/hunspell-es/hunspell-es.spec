%global momorel 5

Name: hunspell-es
Summary: Spanish hunspell dictionaries
%define upstreamid 20081215
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://es.openoffice.org/files/documents/73/3001/es_ANY.zip
Group: Applications/Text
URL: http://es.openoffice.org/programa/diccionario.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv3+ or GPLv3+ or MPLv1.1
BuildArch: noarch

Requires: hunspell

%description
Spanish (Spain, Mexico, etc.) hunspell dictionaries.

%prep
%setup -q -c -n hunspell-es

%build
chmod -x *

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p es_ANY.dic $RPM_BUILD_ROOT/%{_datadir}/myspell/es_ES.dic
cp -p es_ANY.aff $RPM_BUILD_ROOT/%{_datadir}/myspell/es_ES.aff

pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
es_ES_aliases="es_AR es_BO es_CL es_CO es_CR es_CU es_DO es_EC es_GT es_HN es_MX es_NI es_PA es_PE es_PR es_PY es_SV es_US es_UY es_VE"

for lang in $es_ES_aliases; do
	ln -s es_ES.aff $lang.aff
	ln -s es_ES.dic $lang.dic
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README.txt Changelog.txt GPLv3.txt MPL-1.1.txt LGPLv3.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20081215-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20081215-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20081215-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20081215-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20081215-1m)
- update to 20081215

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20051031-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20051031-1m)
- import from Fedora to Momonga

* Mon Aug 20 2007 Caolan McNamara <caolanm@redhat.com> - 0.20051031-1
- latest version
- clarify license version

* Thu Aug 09 2007 Caolan McNamara <caolanm@redhat.com> - 0.20050510-2
- clarify license version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20050510-1
- initial version
