%global momorel 1
Name:		telepathy-haze
Version:	0.7.0
Release: %{momorel}m%{?dist}
Summary:	A multi-protocol Libpurple connection manager for Telepathy

Group:		Applications/Communications
License:	GPLv2+
URL:		http://developer.pidgin.im/wiki/Telepathy

Source0:	http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource: 0

BuildRequires:	libpurple-devel >= 2.7
BuildRequires:	telepathy-glib-devel >= 0.15.1
BuildRequires:  libxslt
  
Requires:	telepathy-filesystem    

%description
telepathy-haze is a connection manager built around libpurple, the core of
Pidgin (formerly Gaim), as a Summer of Code project under the Pidgin umbrella.
Ultimately, any protocol supported by libpurple will be supported by
telepathy-haze; for now, XMPP, MSN and AIM are known to work acceptably, and
others will probably work too.


%prep
%setup -q


%build
%configure
%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_datadir}/telepathy/managers/haze.manager


%files
%defattr(-,root,root,-)
%doc COPYING NEWS
%{_libexecdir}/telepathy-haze
%{_datadir}/dbus-1/services/*.haze.service
%{_mandir}/man8/telepathy-haze.8*

%changelog
* Sun Nov 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-1m)
- reimport from fedora

* Wed Jul 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-2m)
- rebuild for new GCC 4.5

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-1m)
- import from Fedora devel and update to 0.3.6

* Sat Feb 20 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.3.3-2
- Drop manager file, so Empathy can connect to ICQ. (#566968)

* Mon Jan 25 2010 Peter Gordon <peter@thecodergeek.com> - 0.3.3-1
- Update to new upstream release (0.3.3).
- Remove upstream Yahoo! IM fix:
  - no-yahoo-japan.patch
- Bump minimum telepathy-glib version.

* Thu Aug 13 2009 Peter Gordon <peter@thecodergeek.com> - 0.3.1-3
- Add upstream patch to remove the Yahoo! Japan from the manager profile,
  which fixes Yahoo! IM connectivity issues as reported in GNOME bug #591381.
  + no-yahoo-japan.patch
- Resolves: Fedora bug #514998 (Yahoo! Instant Messenger accounts do not work
  in empathy).

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Jun 18 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.3.1-1
- Update to 0.3.1.

* Sun Apr 12 2009 Peter Gordon <peter@thecodergeek.com> - 0.3.0-1
- Update to new upstream release (0.3.0)

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Sep 12 2008 Peter Gordon <peter@thecodergeek.com> - 0.2.1-1
- Update to new upstream release (0.2.1)

* Wed Aug 13 2008 Peter Gordon <peter@thecodergeek.com> - 0.2.0-3
- Remove the mission-control subpackage in favor of using the profiles from
  Empathy upstream.

* Tue Jul 29 2008 Peter Gordon <peter@thecodergeek.com> - 0.2.0-2
- Fix the ICQ Mission Control profile to properly use the "icq" configuration
  UI, rather than the one for jabber.
- Resolves: bug #456565 (Wrong entry in haze-icq.profile)

* Sat Mar 01 2008 Peter Gordon <peter@thecodergeek.com> - 0.2.0-1
- Update to new upstream release (0.2.0)
- Add ICQ Mission-Control profile.

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.1.4-3
- Autorebuild for GCC 4.3

* Sun Dec 16 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.4-2
- Add patch from upstream Darcs to fix bug 425870 (bad apostrophe escaping with
  Yahoo! messages).
  + fix-yahoo-apostrophe-escaping.patch

* Thu Nov 22 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.4-1
- Update to new upstream build-fix release (0.1.4).
- Add Yahoo! IM support to the mission-control profiles, with default
  login/server information taken from Pidgin/Finch.

* Wed Nov 14 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.3-1
- Update to new upstream release (0.1.3), which fixes the accidental
  ~/.purple directory deletion with Pidgin 2.3.0+ (among other notable fixes
  and enhancements).
- Drop compile fix with recent telepathy-glib releases (fixed upstream):
  - fix-deprecated-tp_debug-call.patch

* Tue Nov 13 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.2-4
- Add patch to fix build error due to calling deprecated
  tp_debug_set_flags_from_env function.
  + fix-deprecated-tp_debug-call.patch

* Sun Nov 11 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.2-3
- Fix Source0 URL.

* Thu Nov 01 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.2-2
- Add haze-msn.profile (based on the profile provided by the PyMSN-based
  Butterfly connection manager) so that MC-using applications (such as
  Empathy) can use the libpurple-based MSN connection manager instead of
  Butterfly, at the user's option. 
  
* Sun Sep 16 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.2-1
- Update to new upstream release (0.1.2), which fixes sending messages with
  <, >, and & characters and properly cleans up zombie children.   

* Fri Aug 17 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.1-1
- Update to new upstream release (0.1.1), which fixes segfaults when closing
  text and list channels, and some potential g_free corruptions.
- Sync %%description with upstream release announcements.

* Mon Aug 13 2007 Peter Gordon <peter@thecodergeek.com> - 0.1.0-1
- Initial packaging for Fedora.







