%global momorel 8
%global srcrel 00370b5
%global srcname mishaaq-kcm_touchpad
%global src1name ksynaptics
%global src1ver 0.3.3
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m

Summary: Synaptics driver based touchpads kcontrol module for KDE
Name: kcm_touchpad
Version: 0.3.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://kde-apps.org/content/show.php/kcm_touchpad?content=113335
Source0: http://download.github.com/%{srcname}-%{srcrel}.tar.gz
# NoSource: 0
Source1: http://qsynaptics.sourceforge.net/%{src1name}-%{src1ver}.tar.bz2
NoSource: 1
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-0.3.0-fix-build.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): gtk2
Requires(postun): gtk2
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: oxygen-icons
Requires: xorg-x11-drv-synaptics
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: coreutils
## reserve for new version
# BuildRequires: libsynaptics-devel
# BuildRequires: gettext
BuildRequires: xorg-x11-drv-synaptics-devel

%description
This is configuration control panel module for synaptics-driven touchpads.
Based on ksynaptics by Stefan Kombrink (qsynaptics.sourceforge.net). 

%prep
%setup -q -n %{srcname}-%{srcrel} -a 1

%patch0 -p1 -b .desktop-ja
%patch1 -p1 -b .fix-build

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install icons
pushd %{src1name}-%{src1ver}/%{src1name}
mkdir -p %{buildroot}%{_kde4_iconsdir}/oxygen/{16x16,32x32,48x48,64x64,128x128}/apps
install hi16-app-ksynaptics.png %{buildroot}%{_kde4_iconsdir}/oxygen/16x16/apps/preferences-desktop-touchpad.png
install hi32-app-ksynaptics.png %{buildroot}%{_kde4_iconsdir}/oxygen/32x32/apps/preferences-desktop-touchpad.png
install hi48-app-ksynaptics.png %{buildroot}%{_kde4_iconsdir}/oxygen/48x48/apps/preferences-desktop-touchpad.png
install hi64-app-ksynaptics.png %{buildroot}%{_kde4_iconsdir}/oxygen/64x64/apps/preferences-desktop-touchpad.png
install hi128-app-ksynaptics.png %{buildroot}%{_kde4_iconsdir}/oxygen/128x128/apps/preferences-desktop-touchpad.png

# clean up
rm -rf %{buildroot}%{_kde4_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :

%postun
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :

%files
%defattr(-, root, root)
%doc AUTHORS LICENSE README
%{_kde4_libdir}/kde4/%{name}.so
%{_kde4_datadir}/kde4/services/touchpad.desktop
%{_kde4_iconsdir}/oxygen/*/apps/preferences-desktop-touchpad.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-7m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-5m)
- full rebuild for mo7 release

* Mon Aug 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-4m)
- adapt touchpad.desktop to KDE 4.5 specification

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-3m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-2m)
- touch up spec file

* Sat Jan 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-1m)
- version 0.3.1

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- version 0.3.0
- update fix-build.patch

* Sun Oct 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-1m)
- version 0.2.1
- add %%doc
- License: GPLv2

* Sat Oct 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-1m)
- initial package for Momonga Linux
- branch from ksynaptics
- apply desktop.patch and fix-build.patch
- import icons from ksynaptics-0.3.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-8m)
- rebuild against rpm-4.6

* Sat Oct 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-7m)
- change Requires from synaptics to xorg-x11-drv-synaptics

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.3-5m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-3m)
- %%NoSource -> NoSource

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-2m)
- add autostart-false.patch

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-1m)
- initial package for Momonga Linux
