%global momorel 1
%global qtver 3.3.7
%global qtdir %{_libdir}/qt3

Summary: ICC Profiler
Name: lprof
Version: 1.11.4.1
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://lprof.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-lcms.patch
Patch2: argyll-typo.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: ImageMagick
BuildRequires: desktop-file-utils
BuildRequires: libdrm-devel
BuildRequires: libtiff-devel
BuildRequires: scons
BuildRequires: vigra-devel

%description
LPROF is the only open source ICC profiler with a graphical user
interface. It can be used to create profiles for cameras, scanners, and
monitors, and fills a necessary niche in the emerging open source color
management effort.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p0 -b .lcms~
%patch2 -p0 -b .fix-typo~

%build
export QTDIR=%{qtdir} QTLIB=%{qtdir}/lib

scons %{?_smp_mflags} PREFIX=%{_prefix} ccflags="%{optflags}" cxxflags="%{optflags}"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_prefix}
scons install PREFIX=%{buildroot}%{_prefix} %{?_smp_mflags}

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48}/apps
convert -scale 16x16 data/icons/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 data/icons/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
install -m 644 data/icons/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/
rm -f %{buildroot}%{_datadir}/pixmaps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING KNOWN_BUGS README*
%{_bindir}/icc2it8
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/pixmaps/%{name}.png

%changelog
* Fri Jul  5 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.11.4.1-1m)
- initial package for Momonga Linux
- import some parts of spec and patches from opensuse