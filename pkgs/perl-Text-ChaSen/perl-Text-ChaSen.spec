%global momorel 25

Summary: ChaSen Perl Module
Name: perl-Text-ChaSen
Version: 1.04
Release: %{momorel}m%{?dist}
Source0: http://www.cpan.org/modules/by-module/Text/Text-ChaSen-%{version}.tar.gz 
NoSource: 0
License:  see "README"
URL: http://www.daionet.gr.jp/~knok/chasen/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group: Applications/System
Requires: chasen
BuildRequires: chasen-devel
BuildRequires: perl >= 5.8.5
Obsoletes: chasen-perl

%description
ChaSen Perl Module.

%prep
%setup -q -n Text-ChaSen-%{version}

%build
CFLAGS="%optflags" perl Makefile.PL INSTALLDIRS=vendor
make


%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm %{buildroot}%{perl_vendorarch}/auto/Text/ChaSen/.packlist

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{_mandir}/man3/*
%{perl_vendorarch}/Text/ChaSen.pm
%{perl_vendorarch}/auto/Text/ChaSen

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-25m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-24m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-23m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-22m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-21m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-20m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-19m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-18m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-17m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-16m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-15m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-13m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-12m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.04-11m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-10m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-9m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-7m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.04-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.04-5m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-4m)
- rebuild againsr chasen-2.4.2-1m

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.04-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.04-2m)
- use vendor

* Thu Jan 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04
- delete ChaSen.pm-1.03-pod-fix.diff
- change source URI to CPAN

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.03-14m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.03-13m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.03-12m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.03-11m)
- remove Epoch from BuildRequires

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.03-10m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.03-9m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.03-8m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.03-7m)
- rebuild against perl-5.8.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.3-6k)
- rebuild against for perl-5.6.1

* Tue Apr 10 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.03-3k)
- add a patch
