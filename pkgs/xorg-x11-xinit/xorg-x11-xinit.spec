%global momorel 1
%define pkgname xinit

Summary:   X.Org X11 X Window System xinit startup scripts
Name:      xorg-x11-%{pkgname}
Version:   1.3.2
Release: %{momorel}m%{?dist}
License:   MIT
Group:     User Interface/X
URL:       http://www.x.org

Source0:  ftp://ftp.x.org/pub/individual/app/%{pkgname}-%{version}.tar.bz2
NoSource: 0
Source10: xinitrc-common
Source11: xinitrc
Source12: Xclients
Source13: Xmodmap
Source14: Xresources
# NOTE: Xsession is used by xdm/kdm/gdm and possibly others, so we keep it
#       here instead of the xdm package.
Source16: Xsession
Source17: localuser.sh
Source18: xinit-compat.desktop
Source19: xinit-compat

# Fedora specific patches

Patch1: xinit-1.0.2-client-session.patch
Patch3: xinit-1.0.9-unset.patch

BuildRequires: pkgconfig
BuildRequires: libX11-devel
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: dbus-devel
BuildRequires: libtool
BuildRequires: xorg-x11-util-macros
# NOTE: startx needs xauth in order to run, but that is not picked up
#       automatically by rpm.  (Bug #173684)
Requires: xorg-x11-xauth
# next two are for localuser.sh
Requires: coreutils
Requires: xorg-x11-server-utils

Obsoletes: xorg-x11-xinit-base
Provides:  xorg-x11-xinit-base

Obsoletes: xorg-x11-xinit-additional
Obsoletes: xorg-x11-xinit-french
Obsoletes: xorg-x11-xinit-english
Obsoletes: xorg-x11-xinit-spanish
Obsoletes: xorg-x11-xinit-german

%package session
Summary: Display manager support for ~/.xsession and ~/.Xclients
Group: User Interface/X

%description
X.Org X11 X Window System xinit startup scripts

%description session
Allows legacy ~/.xsession and ~/.Xclients files to be used from display managers

%prep
%setup -q -n %{pkgname}-%{version}
%patch1 -p1 -b .client-session
%patch3 -p1 -b .unset

%build
autoreconf
%configure
# FIXME: Upstream should default to XINITDIR being this.  Make a patch to
# Makefile.am and submit it in a bug report or check into CVS.
make XINITDIR=%{_sysconfdir}/X11/xinit

%install
# FIXME: Upstream should default to XINITDIR being this.  Make a patch to
# Makefile.am and submit it in a bug report or check into CVS.
make install DESTDIR=%{buildroot} XINITDIR=%{_sysconfdir}/X11/xinit
install -p -m644 -D %{SOURCE18} %{buildroot}%{_datadir}/xsessions/xinit-compat.desktop

# Install Red Hat custom xinitrc, etc.
{
    mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit

    install -p -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc-common

    for script in %{SOURCE11} %{SOURCE12} %{SOURCE16} ; do
        install -p -m 755 $script %{buildroot}%{_sysconfdir}/X11/xinit/${script##*/}
    done

    install -p -m 644 %{SOURCE13} %{buildroot}%{_sysconfdir}/X11/Xmodmap
    install -p -m 644 %{SOURCE14} %{buildroot}%{_sysconfdir}/X11/Xresources

    mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d
    install -p -m 755 %{SOURCE17} %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d/localuser.sh

    mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/Xclients.d

    mkdir -p %{buildroot}%{_libexecdir}
    install -p -m 755 %{SOURCE19} %{buildroot}%{_libexecdir}
}

%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog
%{_bindir}/startx
%{_bindir}/xinit
%dir %{_sysconfdir}/X11/xinit
%{_sysconfdir}/X11/xinit/xinitrc
%{_sysconfdir}/X11/xinit/xinitrc-common
%config(noreplace) %{_sysconfdir}/X11/Xmodmap
%config(noreplace) %{_sysconfdir}/X11/Xresources
%dir %{_sysconfdir}/X11/xinit/Xclients.d
%{_sysconfdir}/X11/xinit/Xclients
%{_sysconfdir}/X11/xinit/Xsession
%dir %{_sysconfdir}/X11/xinit/xinitrc.d
%{_sysconfdir}/X11/xinit/xinitrc.d/*
%{_mandir}/man1/startx.1*
%{_mandir}/man1/xinit.1*

%files session
%defattr(-, root, root,-)
%{_libexecdir}/xinit-compat
%{_datadir}/xsessions/xinit-compat.desktop

%changelog
* Sat Jul 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-1m)
- reimport from fedora
- fix for BTS 447 issue

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-6m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-5m)
- set default XIM to "ibus-qt3" ("IBus with Qt3")

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-4m)
- use ruby18 (Patch101)

* Wed Aug  4 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.1-3m)
- change background color

* Sun Jul 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-2m)
- change no_NO string 
-- Latin-1 to ASCII

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-1m)
- udpate 1.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 23 2009 SANUKI <sanuki@momonga-linux.org>
- (1.1.0-4m)
- change background color

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-3m)
- rebuild against rpm-4.6

* Sat Sep 13 2008 SANUKI <sanuki@momonga-linux.org>
- (1.1.0-2m)
- change background color

* Fri Jun 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9

* Tue Apr 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-5m)
- fix xinitrc-momonga-ck.patch
- probably ConsoleKit is working fully now

* Sat Apr 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-4m)
- remove DBUS_LAUNCH from xinitrc-common

* Sat Apr 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-3m)
- re-import ConsoleKit support from Fedora
- add ConsoleKit support to Xsession and xinitrc-common

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.8-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-1m)
- update 1.0.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-2m)
- %%NoSource -> NoSource

* Wed Sep 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-1m)
- update 1.0.5

* Tue Jul 31 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.3-9m)
- modify Xclients for startx
- change background color (Mo4 color)

* Wed Jul 11 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.3-8m)
- update xinitrc-momonga-20070709.tar.bz2
-- marge Patch2(xinit-1.0.2-gnome_dbus.patch)
-- change background color (wakatake iro)

* Mon Jul  9 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.3-7m)
- update xinitrc-momonga-20070709.tar.bz2
-- update Xsession, xinitrc-momonga
-- marge Patch3(xinit-1.0.3-kde_dbus_launch.patch)

* Sun Jul  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-6m)
- remove gdm-default-desktop.patch

* Tue Jun 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-5m)
- add gdm-default-desktop.patch for Beta1
- update kde_dbus_launch.patch

- * Sat Jun  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (1.0.3-5m)
- - import xinit-1.0.2-2-poke-ck.patch from Fedora
-  +* Mon Apr 02 2007 David Zeuthen <davidz@redhat.com> 1.0.2-16
-  +- Add ConsoleKit support (#233183)

* Sun May 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-4m)
- update xinitrc-momonga-20060714.tar.bz2
-- make-langinfo.sh : use locale command
-- create list-langage.rb
--- Durty hack. Please remake this script

* Mon Feb 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-3m)
- update kde_dbus_launch.patch (add $DBUS_LAUNCH to "echo" section)

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-2m)
- add $DBUS_LAUNCH to startkde of Xsession, amarok-yauap needs it

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Thu Sep 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-15m)
- bugfix http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=183
-- gnome-settions-daemon need dbus-launch (startx)
-- add patch2

* Fri Jul 14 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.2-14m)
- change background color (xinitrc-common)

* Sat Jul  1 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.2-13m)
- update xinitrc-common, for set SSH_ASKPASS
- update Xsession, Bug Fix .xinitrc/hook execute

* Wed Jun 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-12m)
- update xinitrc-momonga, for twm

* Tue Jun 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-11m)
- Security advisory http://xorg.freedesktop.org/releases/X11R7.1/patches/

* Fri Jun 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.2-10m)
- update xinitrc-momonga, enable ssh-agent for runlevel3 user

* Wed Jun  7 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.2-9m)
- update Source1 (modify xinitrc-momonga)

* Mon Jun  5 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.2-8m)
- update Source1 (modify xinitrc-momonga)

* Sun Jun  4 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.2-7m)
- update Source1
- change X11 start scripts

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-6m)
- delete duplicated dir
- add xorg-x11-xinit-base package

* Sun May 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-5m)
- rewind Source1

* Sun May 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-4m)
- update Source1
-- delete ssh agent from /etc/X11/xinit/xinitrc

* Sun May 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-3m)
- update Source1
-- use sdr in each login

* Sun Apr 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-2m)
- Obsoletes xinitrc
-- Source1 and Patch0 from xinitrc

* Wed Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-4m)
- delete somescript Xmodmap, Xresources, and so on.
- delete To.Alter

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- import to Momonga

* Thu Feb 16 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-2
- Change Conflicts to Obsoletes for xorg-x11 and XFree86 (#181414)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated to xinit 1.0.1 from X11R7.0

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated to xinit 1.0.0 from X11R7 RC4.
- Changed manpage dir from man1x to man1 to match upstream default.

* Tue Nov 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-6
- Add "Requires: xauth" for startx, to fix bug (#173684)

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> 0.99.3-5
- Do not provide xinit anymore, gdm has been fixed and that breaks things
  with the obsoletes

* Sat Nov 12 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-4
- Added Xsession script from xinitrc, as it is very similar codebase, which
  shares "xinitrc-common" anyway, and all of the display managers use it.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-3
- Updated to xinit 0.99.3 from X11R7 RC2.

* Mon Nov 7 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-3
- Added "Provides: xinitrc = 5.0.0-1" for temporary compatibility between
  monolithic and modular X.  This will be removed however for FC5.

* Mon Oct 31 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Import custom Red Hat xinit scripts from xinitrc package.
- Obsolete xinitrc package, as we include the scripts/configs here now.
- Fix all scripts/configs to avoid the now obsolete /usr/X11R6 prefix.

* Mon Oct 31 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated to xinit 0.99.2 from X11R7 RC1.
- Change manpage location to 'man1x' in file manifest.

* Wed Oct  5 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Use Fedora-Extras style BuildRoot tag.
- Update BuildRequires to use new library package names.
- Tidy up spec file a bit.

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
