%global momorel 6

%define fontname vollkorn
%define fontconf 64-%{fontname}.conf

%define archivename Vollkorn

Name:           %{fontname}-fonts
Version:        1.008
Release:        %{momorel}m%{?dist}
Summary:        A serif latin font with good readability

Group:          User Interface/X
License:        "CC-BY"
URL:            http://www.grafikfritze.de/?p=43
#http://www.grafikfritze.de/download.php?download=1
Source0:        %{fontname}.otf  
Source1:        %{name}-fontconfig.conf
Source2:        %{archivename}.pdf
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
Serif latin OTF font by Friedrich Althausen with focus on good readability.
Because of this it is relatively dark and strong and has emphasized serifs. 
Vollkorn will especially work good in reading-sizes with its familiar and 
smooth overall picture.

%prep
%setup -q -c -T
install -m 0644 -p %{SOURCE2} .

%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{SOURCE0} %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.otf

%doc *.pdf

%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.008-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.008-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.008-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.008-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.008-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.008-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.008-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 02 2009 Paul Lange <palango@gmx.de> - 1.008-2
move vollkorn.otf file out of the cvs repo

* Fri Jan 30 2009 Paul Lange <palango@gmx.de> - 1.008-1
- initial packaging
