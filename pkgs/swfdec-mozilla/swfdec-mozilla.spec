%global momorel 5

Name:		swfdec-mozilla
Version:	0.9.2
Release:	%{momorel}m%{?dist}
Summary:	Mozilla/Gecko player Flash plugin using swfdec
 
Group:		Applications/Internet
License:	LGPLv2+
URL:		http://swfdec.freedesktop.org/
Source0:	http://swfdec.freedesktop.org/download/%{name}/0.8/%{name}-%{version}.tar.gz
#NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gtk2-devel
BuildRequires:	swfdec-gtk-devel >= %{version}

Requires:	mozilla-filesystem

%description
swfdec is a library for rendering Adobe Flash animations. Currently it handles
most Flash 3, 4 and many Flash 7 videos. This is the viewer plugin for Firefox
and other Gecko-based browsers such as Epiphany and Galeon. 

%prep
%setup -q

%build
%configure --disable-static --with-plugin-dir=%{_libdir}/mozilla/plugins
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

%clean
rm -rf $RPM_BUILD_ROOT

%post
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun 
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README
%{_libdir}/mozilla/plugins/*.so
%{_datadir}/icons/*/*/apps/swfdec-mozilla.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-2m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-1m)
- update to 0.8.0 for swfdec-0.8.0

* Thu Jun 26 2008 Yohsuke Ooi <meke@n.email.ne.jp>
- (0.7.2-1m)
- update 0.7.2

* Mon May  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-2m)
- Requires: mozilla-filesystem

* Sun May  4 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.0-1m)
- import from Fedora

* Wed Feb 20 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.6.0-1
- Update to 0.6.0.

* Fri Feb  8 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.5.90-2
- Rebuild for gcc-4.3.

* Tue Jan 29 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.5.90-1
- Update to 0.5.90.

* Wed Dec 19 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.5-2
- Drop BR on alsa-libs-devel.
- Rebuild for pulseaudio-enabled swfdec.

* Mon Dec 17 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.5-1
- Update to 0.5.5.

* Sat Nov 17 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.4-2
- Update license tag.
- Drop BR on firefox-devel.

* Thu Nov 15 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.4-1
- Update to 0.5.4.

* Fri Oct 12 2007  Peter Gordon <peter@thecodergeek.com> - 0.5.3-1
- Update to new upstream release (0.5.3)

* Wed Oct 10 2007  Peter Gordon <peter@thecodergeek.com> - 0.5.2-1
- Update to new upstream release (0.5.2)

* Wed Aug 15 2007  Peter Gordon <peter@thecodergeek.com> - 0.5.1-1
- Update to new upstream release (0.5.1)

* Thu Jul  5 2007 kwizart <kwizart at gmail.com> - 0.4.5-2
- Fix directory ownership
- Add missing BR ( swfdec-gtk-devel )

* Thu Jul  5 2007 kwizart <kwizart at gmail.com> - 0.4.5-1
- Update to 0.4.5
- Use --with-plugin-dir

* Sat Mar 31 2007 Peter Gordon <peter@thecodergeek.com> - 0.4.3-3
- Remove %%post and %%postun ldconfig calls (not necessary for this package).
- Remove empty ChangeLog from %%doc. 

* Sun Mar 25 2007 Peter Gordon <peter@thecodergeek.com> - 0.4.3-2
- Fix firefox dependency versioning.

* Sat Mar 24 2007 Peter Gordon <peter@thecodergeek.com> - 0.4.3-1
- Initial packaging for Fedora/Livna
