%global momorel 4

%global  git_commit 61655c8
%global cluster jruby

# Prevent brp-java-repack-jars from being run.
%define __jar_repack %{nil}

Name:           bytelist
Version:        1.0.3
Release:        %{momorel}m%{?dist}
Summary:        A java library for lists of bytes

Group:          Development/Libraries
License:        CPL or GPLv2+ or LGPLv2+
URL:            http://github.com/%{cluster}/%{name}
Source0:        %{url}/tarball/%{version}/%{cluster}-%{name}-%{git_commit}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  ant
BuildRequires:  ant-junit
BuildRequires:  java-devel
BuildRequires:  jcodings
BuildRequires:  jpackage-utils
BuildRequires:  junit

Requires:       java
Requires:       jcodings
Requires:       jpackage-utils


%description
A small java library for manipulating lists of bytes.


%prep
%setup -q -n %{cluster}-%{name}-%{git_commit}

find -name '*.class' -exec rm -f '{}' \;
find -name '*.jar' -exec rm -f '{}' \;


%build
echo "See %{url} for more info about the %{name} project." > README.txt

export CLASSPATH=$(build-classpath junit jcodings)
%__mkdir_p lib
%ant


%install
%__rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_javadir}

%__cp -p lib/%{name}-1.0.1.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
pushd %{buildroot}%{_javadir}/
  %__ln_s %{name}-%{version}.jar %{name}.jar
popd


%check
export CLASSPATH=$(build-classpath junit jcodings)
%ant test


%clean
%__rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_javadir}/*
%doc README.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- full rebuild for mo7 release

* Fri Feb 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- import from Fedora 13

* Tue Feb 09 2010 Victor G. Vasilyev <victor.vasilyev@sun.com> - 1.0.3-2
- Fix the clean up code in the prep section
- Fix typo
- Save changelog

* Thu Jan 28 2010 Victor G. Vasilyev <victor.vasilyev@sun.com> - 1.0.3-1
- 1.0.3
- Remove gcj bits
- New URL
- Use macros in all sections of the spec
- Add README.txt generated on the fly

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-0.2.svn9177
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb 15 2009 Conrad Meyer <konrad@tylerc.org> - 1.0.1-0.1.svn9177
- Bump to SVN HEAD.

* Sun Feb 15 2009 Conrad Meyer <konrad@tylerc.org> - 1.0-1
- Bump to 1.0 release.

* Tue Apr 22 2008 Conrad Meyer <konrad@tylerc.org> - 0.1-0.2.svn6558
- Do not include version in jar filename.
- Run tests in check section.

* Tue Apr 22 2008 Conrad Meyer <konrad@tylerc.org> - 0.1-0.1.svn6558
- Initial RPM.
