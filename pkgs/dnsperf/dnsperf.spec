%global momorel 17

Summary: Benchmarking authorative and recursing DNS servers
Name: dnsperf
Version: 1.0.1.0
Release: %{momorel}m%{?dist}
License: MIT/X
Url: http://www.nominum.com/services/measurement_tools.php
Source: ftp://ftp.nominum.com/pub/nominum/dnsperf/%{version}/dnsperf-src-%{version}-1.tar.gz
NoSource: 0
Group: Applications/Internet
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: bind-devel >= 9.9.5-1m, libcap-devel, bzip2, openssl-devel >= 1.0.0
BuildRequires: krb5-devel, libxml2-devel
Requires: gnuplot pcapy python-dns

%description
This is dnsperf, a collection of DNS server performance testing tools.
For more information, see the dnsperf(1) and resperf(1) man pages.

%prep
%setup -q -n dnsperf-src-%{version}-1
%configure  

%build
%{__make} CFLAGS="$RPM_OPT_FLAGS" %{?_smp_mflags}

%install
rm -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install
bzip2 -9 examples/queryfile-example-100thousand
echo "For a better test, use ftp://ftp.nominum.com/pub/nominum/dnsperf/data/queryfile-example-3million.gz" > examples/README.largetest
install contrib/queryparse/queryparse %{buildroot}/%{_bindir}
install -D -m 644 contrib/queryparse/queryparse.1 %{buildroot}/%{_mandir}/man1/queryparse.1
gzip %{buildroot}/%{_mandir}/man1/queryparse.1

%clean
rm -rf ${RPM_BUILD_ROOT}

%files 
%defattr(-,root,root,-)
%doc README RELEASE_NOTES doc examples/*
%{_bindir}/*
%{_mandir}/*/*

%changelog
* Thu Feb  6 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1.0-17m)
- rebuild against bind-9.9.5-1m

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1.0-16m)
- rebuild against bind-9.9.2-1m

* Wed Jun 06 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.0-15m)
- rebuild against bind-9.9.1-1m

* Sun Mar 18 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.0-14m)
- rebuild against bind-9.9.0-1m

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.0-13m)
- rebuild against bind-9.8.1-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1.0-12m)
- rebuild for new GCC 4.6

* Tue Feb 15 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.0-11m)
- rebuild against bind-9.7.3-1m

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1.0-10m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1.0-9m)
- rebuild against bind-9.7.2-2m

* Tue Sep 14 2010 ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1.0-8m)
- rebuild against bind-9.7.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1.0-7m)
- full rebuild for mo7 release

* Fri Jul 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.0-6m)
- rebuild against bind-9.7.1-P2

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1.0-5m)
- rebuild against openssl-1.0.0

* Thu Mar 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.0-4m)
- rebuild against bind-9.7.0

* Wed Nov 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.0-3m)
- rebuild against bind

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1.0-1m)
- import from Fedora

* Mon Mar 30 2009 Adam Tkac <atkac redhat com> - 1.0.1.0-8
- rebuild against new bind-libs

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jan 15 2009 Tomas Mraz <tmraz@redhat.com> - 1.0.1.0-6
- rebuild with new openssl
- seems to require libxml2-devel to build now

* Mon Nov 10 2008 Adam Tkac <atkac redhat com> - 1.0.1.0-5
- rebuild against new bind-libs

* Wed Oct 31 2008 Paul Wouters <paul@xelerance.com> - 1.0.1.0-4
- Changed license from BSD to MIT

* Wed Oct 22 2008 Paul Wouters <paul@xelerance.com> - 1.0.1.0-3
- Fixed missing buildrequires
- Pass proper CFLAGS to gcc
- Fix Group

* Tue Oct 21 2008 Paul Wouters <paul@xelerance.com> - 1.0.1.0-2
- Fixed libpcap vs libcap confusion

* Mon Oct 20 2008 Paul Wouters <paul@xelerance.com> - 1.0.1.0-1
- Initial Fedora package
