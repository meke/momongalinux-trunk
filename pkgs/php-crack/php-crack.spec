%global momorel 15
%global php_ver 5.4.1

%global pkg_name crack

Summary: "Good Password" Checking Utility: Keep your user passwords reasonably safe from dictionary based attacks.
Name: php-%{pkg_name}
Version: 0.4
Release: %{momorel}m%{?dist}
License: Artistic
Group: Development/Languages
URL: http://pecl.php.net/package/crack
Source0: http://pecl.php.net/get/%{pkg_name}-%{version}.tgz 
NoSource: 0
Patch0: crack-0.4-lib64.patch
Patch1: %{name}-%{version}-php54.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source1: %{pkg_name}.ini

Requires: cracklib
BuildRequires: cracklib
Requires: php >= %{php_ver}
BuildRequires: php-devel >= %{php_ver}

%description
This package provides an interface to the cracklib (libcrack) libraries that come standard on most unix-like distributions. This allows you to check passwords against dictionaries of words to ensure some minimal level of password security.

%prep
# Setup main module
%setup -q -n %{pkg_name}-%{version}
%if %{_lib} == "lib64"
%patch0 -p1 -b .lib64~
%endif
%patch1 -p1 -b .php54

%{_bindir}/phpize
%configure

%build
# Build main module
%make

%install
rm -rf %{buildroot}
%makeinstall INSTALL_ROOT=%{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/php.d
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/php.d/%{pkg_name}.ini

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc  CREDITS
%config(noreplace) %{_sysconfdir}/php.d/%{pkg_name}.ini
%dir %{_libdir}/php/modules/%{pkg_name}.so

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-15m)
- rebuild against php-5.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-12m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-9m)
- rebuild against rpm-4.6

* Mon May 05 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.4-8m)
- rebuild against php 5.2.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-6m)
- %%NoSource -> NoSource

* Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4-5m)
- rebuild against php-5.2.1

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.4-4m)
- add crack.ini

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.4-3m)
- rebuild against PHP 5.1.4

* Tue Jan 13 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.4-2m)
- rebuild against PHP 5.1.2

* Tue Jan 10 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.4-1m)
- version up

* Sat Dec 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.1-6m)
- rebuild against php-5.1.1-1m

* Sat Nov  5 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.1-5m)
- erbuild against php-5.0.5-8m

* Tue Apr  5 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.1-4m)
- rebuild against php-5.0.4

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.1-3m)
- enable x86_64.

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-2m)
- rebuild against php-5.0.3

* Tue Oct 12 2004 Yasuo Ohgaki <yoghaki@ohgaki.net>
- (0.1-1m)
- Initial version
