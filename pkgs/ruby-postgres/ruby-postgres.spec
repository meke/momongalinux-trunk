%global momorel 1
%global rbname pg

Summary: The extension for PostgreSQL access
Name: ruby-postgres
Version: 0.12.2
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: Ruby
URL: http://bitbucket.org/ged/ruby-pg/wiki/Home
Source0: http://bitbucket.org/ged/ruby-pg/downloads/pg-%{version}.tar.xz
#NoSource: 0
Patch0: pg-0.9.0-buildfixes.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: rubygem-rake-compiler >= 0.7.1-1m
BuildRequires: postgresql-devel >= 8.2.3
BuildRequires: openssl-devel >= 1.0.0

Obsoletes: postgres
Provides: postgres

%description
This is the extension library to access PostgreSQL database from Ruby.
This library works with PostgreSQL 6.4/6.5;  probably works with 6.3 or
earlier with slight modification, but not tested at all.

%prep
%setup -q -n %{rbname}-%{version}
%patch0 -p1

# remove test scripts
# tests need running postgresql-server
rm spec/*.rb

%build
ruby -Ku -I. /usr/bin/rake

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

# pg-0.9.0 unsupport DESTDIR...
# ruby -Ku -I. /usr/bin/rake install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{ruby_sitearchdir}/
cp lib/*.rb %{buildroot}%{ruby_sitelibdir}
cp lib/*.so %{buildroot}%{ruby_sitearchdir}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc BSD GPL LICENSE Contributors.rdoc
%doc README.rdoc README.ja.rdoc History.rdoc
%{ruby_sitearchdir}/pg_ext.so
%{ruby_sitelibdir}/pg.rb

%changelog
* Sun Jan  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.2-1m)
- update 0.12.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-4m)
- full rebuild for mo7 release

* Tue Aug 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-3m)
- BuildRequires: rubygem-rake-compiler >= 0.7.1-1m

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-2m)
- remove some multibyte chars

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1.20051221-12m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1.20051221-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1.20051221-10m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1.20051221-9m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1.20051221-8m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1.20051221-7m)
- rebuild against gcc43

* Tue Mar 25 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.7.1.20051221-6m)
- fix build

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1.20051221-5m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.1.20051221-4m)
- rebuild against ruby-1.8.6-4m

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1.20051221-3m)
- rebuild against postgresql-8.2.3

* Thu Apr 13 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.1.20051221-2m)
- rebuild against openssl-0.9.8a

* Wed Feb 15 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.1.20051221-1m)
- add ssl support

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.7.1-8m)
- rebuild against postgresql-8.1.0

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.7.1-7m)
- /usr/lib/ruby

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (0.7.1-6m)
- rebuild against postgresql-8.0.2.

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.1-5m)
- rebuild against ruby-1.8.2

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.7.1-4m)
- rebuild against postgresql-7.4.1

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.7.1-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.7.1-2m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.7.1-1m)
- version up.

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.7.0-3m)
  rebuild against 0.9.7a

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.0-2m)
- rebuild against for postgresql

* Thu Nov 07 2002 Kenta MURATA <muraken@momonga-linux.org>
- (0.7.0-1m)
- version up to 0.7.0.

* Thu May 16 2002 Masaru Sato <masachan@kondara.org>
- (0.6.5-4k)
- rebuild against postgresql-7.2.1-8k

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.6.5-2k)
- update to 0.6.5

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (0.6.1-8k)
- rebuild against postgresql 7.1.3-4k.
- add buildprereq.

* Sat Oct 27 2001 Toru Hoshina <t@kondara.org>
- (0.6.1-6k)
- rebuild against postgresql 7.1.3.

* Thu Nov 30 2000 AYUHANA Tomonori <l@kondara.org>
- (0.6.1-1k)
- version 0.6.1
- name change postgres to ruby-postgres
- it should be suitable to both ruby 1.4.x and 1.6.x.
- add URL:
- add BuildRequires: postgresql-devel

* Mon Mar 13 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, Distribution, description )

* Tue Dec  7 1999 Tenkou N. Hattori <tnh@kondara.org>
- alpha surrpoted.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Oct 26 1999 KUSUNOKI Masanori <masanori@linux.or.jp>
- first release
