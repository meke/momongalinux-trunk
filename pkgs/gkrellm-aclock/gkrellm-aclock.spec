%global momorel 6
%global gkplugindir %{_libdir}/gkrellm2/plugins

Summary: Analog clock plugin for GKrellM
Name: gkrellm-aclock
Version: 0.3.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://www.geocities.com/m_muthukumar/gkrellaclock.html
Source: http://www.geocities.com/m_muthukumar/gkrellaclock-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gkrellm >= 2.2.0
BuildRequires: gkrellm-devel >= 2.2.0
BuildRequires: gkrellm >= 2.2.0

%description
Analog clock plugin for GKrellM, the GNU Krell Monitor.


%prep
%setup -q -n gkrellAclock-%{version}


%build
%{__make} clean
%{__make} CFLAGS="%{optflags}"


%install
%{__rm} -rf %{buildroot}
%{__install} -D -m 0755 gkrellaclock.so \
    %{buildroot}%{gkplugindir}/gkrellaclock.so


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{gkplugindir}/gkrellaclock.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-2m)
- rebuild against rpm-4.6

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.3.4-5
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 Matthias Saou <http://freshrpms.net/> 0.3.4-4
- Rebuild for new BuildID feature.

* Sun Aug  5 2007 Matthias Saou <http://freshrpms.net/> 0.3.4-3
- Update License field.

* Fri Jun 22 2007 Matthias Saou <http://freshrpms.net/> 0.3.4-2
- Remove dist, as it might be a while before the next rebuild.

* Mon Aug 28 2006 Matthias Saou <http://freshrpms.net/> 0.3.4-1
- Update to 0.3.4.
- FC6 rebuild.
- Add "make clean" since the sources contain a leftover i386 *.o file.

* Mon Mar  6 2006 Matthias Saou <http://freshrpms.net/> 0.3.3-4
- FC5 rebuild.

* Wed Feb  8 2006 Matthias Saou <http://freshrpms.net/> 0.3.3-3
- Rebuild for new gcc/glibc.

* Wed Aug 17 2005 Matthias Saou <http://freshrpms.net/> 0.3.3-2
- Rebuild against new libcairo.

* Tue Jul 12 2005 Matthias Saou <http://freshrpms.net/> 0.3.3-1
- Initial RPM release, split out from my old gkrellm-plugins package.

