%global momorel 4

Name: libwvstreams
Version: 4.6.1
Release: %{momorel}m%{?dist}
Summary: WvStreams is a network programming library written in C++
Source0: http://wvstreams.googlecode.com/files/wvstreams-%{version}.tar.gz
NoSource: 0

Patch1: wvstreams-4.2.2-multilib.patch
Patch2: wvstreams-4.5-noxplctarget.patch
Patch3: wvstreams-4.6.1-make.patch
Patch4: wvstreams-4.6.1-statinclude.patch
Patch5: wvstreams-4.6.1-gcc.patch

URL: http://alumnit.ca/wiki/index.php?page=WvStreams
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel >= 1.0.0, pkgconfig, zlib-devel
BuildRequires: gcc-c++ >= 3.4.1-1m
License: LGPLv2+

%description
WvStreams aims to be an efficient, secure, and easy-to-use library for
doing network applications development.

%package devel
Summary: Development files for WvStreams.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
WvStreams aims to be an efficient, secure, and easy-to-use library for
doing network applications development.  This package contains the files
needed for developing applications which use WvStreams.

%prep
%setup -q -n wvstreams-%{version}
%patch1 -p1 -b .multilib
%patch2 -p1 -b .xplctarget
%patch3 -p1 -b .make
%patch4 -p1 -b .statinclude
%patch5 -p1 -b .gcc

%build
#  --with-fam              FAM
#  --with-fftw             FFTW
#  --with-bdb              Berkeley DB 1.x (or compatible)
#  --with-gdbm             GDBM
#  --with-ogg              Ogg
#  --with-openssl          OpenSSL
#  --with-pam              PAM
#  --with-tcl              Tcl
#  --with-swig             SWIG
#  --with-qt               Qt
#  --with-speex            Speex
#  --with-vorbis           Vorbis
#  --with-xplc             XPLC
#  --with-zlib             zlib

touch configure
%configure --with-pam --with-openssl --without-fam --without-fftw --without-gdbm --without-pgg --without-tcl --without-swig --without-qt --without-speex --without-vorbis --with-xplc --disable-static

make COPTS="$RPM_OPT_FLAGS -fPIC -fpermissive" CXXOPTS="$RPM_OPT_FLAGS -fPIC -fpermissive" VERBOSE=1

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
chmod 755 %{buildroot}%{_libdir}/*.so.*
rm -fr %{buildroot}/usr/bin

pushd %{buildroot}
rm -f \
   ./etc/uniconf.conf \
   .%{_bindir}/uni \
   .%{_libdir}/pkgconfig/liboggspeex.pc \
   .%{_libdir}/pkgconfig/liboggvorbis.pc \
   .%{_libdir}/pkgconfig/libwvfft.pc \
   .%{_libdir}/pkgconfig/libwvqt.pc \
   .%{_sbindir}/uniconfd \
   .%{_mandir}/man8/uni.8* \
   .%{_mandir}/man8/uniconfd.8* \
   .%{_var}/lib/uniconf/uniconfd.ini

popd

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog LICENSE README README.TESTS
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/wvstreams
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/valgrind/*.supp
%{_libdir}/pkgconfig/*.pc

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.1-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to 4.6.1
- sync with Fedora devel

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.1-5m)
- rebuild against readline6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.1-4m)
- rebuild against openssl-1.0.0

* Fri Feb 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.1-3m)
- apply glibc212 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.1-1m)
- update to 4.5.1
- import patches from Fedora 11 (4.5.1-5)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.1-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.1-4m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.1-3m)
- update Patch1 for fuzz=0
- License: LGPLv2+

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.4.1-2m)
- rebuild against db4-4.7.25-1m

* Tue Jul 22 2008 NARTIA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to 4.4.1, sync with Fedora

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-4m)
- rebuild against gcc43

* Fri Feb 15 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.2-3m)
- add patch for gcc43

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.2.2-2m)
- rebuild against db4-4.6.21

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.2-1m)
- update 4.2.2

* Thu Mar  1 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.75.0-6m)
- Patch11: libwvstreams-3.75.0-kh.patch

* Sat Apr 15 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (3.75.0-5m)
- don't apply patch5

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.75.0-4m)
- rebuild against openssl-0.9.8a

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.75.0-3m)
- add gcc-4.1 patch
-- Patch10: libwvstreams-3.75.0-gcc41.patch

* Sun Jan  9 2005 Toru Hoshina <t@momonga-linux.org>
- (3.75.0-2m)
- openssl/des.h and libwvstreams-3.75.0 wasn't match.

* Sun Aug 22 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.75.0-1m)
- update to 3.75 (rebuild against gcc-c++-3.4.1)
- sync with Fedora (libwvstreams-3.75.0-2)
- add BuildRequires: gcc-c++

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.70-2m)
  rebuild against openssl 0.9.7a

* Sun Aug 18 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (3.70-1m)
- pakuri from RawHide (libwvstreams-3.70-5)

* Sat Aug 10 2002 Elliot Lee <sopwith@redhat.com>
- rebuilt with gcc-3.2 (we hope)

* Mon Jul 22 2002 Tim Powers <timp@redhat.com>
- rebuild using gcc-3.2-0.1

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun May 26 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon May 20 2002 Nalin Dahyabhai <nalin@redhat.com> 3.70-1
- patch to build with gcc 3.x
- build with -fPIC

* Wed Apr 10 2002 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.70

* Wed Mar 27 2002 Nalin Dahyabhai <nalin@redhat.com> 3.69-1
- pull in from upstream tarball

* Wed Feb 27 2002 Nalin Dahyabhai <nalin@redhat.com>
- merge the main and -devel packages into one .spec file
- use globbing to shorten the file lists
- don't define name, version, and release as macros (RPM does this by default)
- use the License: tag instead of Copyright: (equivalent at the package level,
  but License: reflects the intent of the tag better)
- use a URL to point to the source of the source tarball
- add BuildRequires: openssl-devel (libwvcrypto uses libcrypto)
- move the buildroot to be under %%{_tmppath}, so that it can be moved by
  altering RPM's configuration

* Tue Jan 29 2002 Patrick Patterson <ppatters@nit.ca>
- Initial Release of WvStreams
