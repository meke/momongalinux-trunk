%global momorel 4

%define		svndate		2013.09.16
%define		svnrev		1328054
%define		internalver	9.2.3
#%%define		internalver	2012.05.21

Name:		open-vm-tools
Version:	0
Release:	0.%{svndate}.%{momorel}m%{?dist}
Summary:	Open Virtual Machine Tools

#Source0:	http://dl.sourceforge.net/project/%{name}/%{name}/stable-9.2.x/open-vm-tools-%{internalver}-%{svnrev}.tar.gz
Source0:http://jaist.dl.sourceforge.net/project/%{name}/%{name}/Development%20Snapshots/%{name}-%{svndate}-%{svnrev}.tar.gz
NoSource:	0
Source1:	tools.conf
Source2:	xautostart.conf
Source3:	vmware-guestd.pam
Source4:	%{name}.init
Source5:	%{name}.sysconfig
Source8:	xorg.conf
Source9:	vmxnet
Source11:	vmware-user.desktop
Source12:	dkms.conf.in
Patch1:		open-vm-tools-9.2.0-glib232.patch
Group:		System Environment/Daemons
License:	LGPLv2
URL:		http://open-vm-tools.sourceforge.net/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	atk-devel
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	cairo-devel
BuildRequires:	cairomm-devel
BuildRequires:	desktop-file-utils
BuildRequires:	fontconfig-devel
BuildRequires:	freetype-devel
BuildRequires:	fuse-devel
BuildRequires:	glib2-devel
BuildRequires:	glibc-devel
BuildRequires:	glibmm-devel
BuildRequires:	gtk2-devel
BuildRequires:	gtkmm-devel
BuildRequires:	libICE-devel
BuildRequires:	libSM-devel
BuildRequires:	libX11-devel
BuildRequires:	libXScrnSaver-devel
BuildRequires:	libXcomposite-devel
BuildRequires:	libXcursor-devel
BuildRequires:	libXdamage-devel
BuildRequires:	libXext-devel
BuildRequires:	libXfixes-devel
BuildRequires:	libXi-devel
BuildRequires:	libXinerama-devel
BuildRequires:	libXrandr-devel
BuildRequires:	libXrender-devel
BuildRequires:	libXtst-devel
BuildRequires:	libdnet-devel
BuildRequires:	libicu-devel >= 52
BuildRequires:	libnotify-devel
BuildRequires:	libsigc++-devel
BuildRequires:	libstdc++-devel
BuildRequires:	libtool
BuildRequires:	liburiparser-devel
BuildRequires:	pango-devel
BuildRequires:	pangomm-devel
BuildRequires:	procps-ng-devel >= 3.3.9
BuildRequires:	zlib-devel
BuildRequires:	kernel-devel
Requires:	pam

%description
The Open Virtual Machine Tools (open-vm-tools) are the open source
implementation of VMware Tools. They are a set of guest operating system
virtualization components that enhance performance and user experience of
virtual machines.

%package	gtk
Summary:	Open Virtual Machine Tools common package
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description gtk
The Open Virtual Machine Tools (open-vm-tools) are the open source
implementation of VMware Tools. They are a set of guest operating system
virtualization components that enhance performance and user experience of
virtual machines.

%package	docs
Summary:	Open Virtual Machine Tools documentation package
Group:		Documentation
Requires:	%{name} = %{version}-%{release}

%description docs
The Open Virtual Machine Tools (open-vm-tools) are the open source
implementation of VMware Tools. They are a set of guest operating system
virtualization components that enhance performance and user experience of
virtual machines.

%package	dkms
BuildArch:	noarch
Summary:	dkms source for VMware guest systems driver
Group:		System Environment/Base
Requires:	%{name} = %{version}-%{release}
Requires:	dkms

%description dkms
This package provides the dkms support for %{name} kernel modules.

%prep
%setup -q -n %{name}-%{svndate}-%{svnrev}

%patch1 -p1 -b .glib232~

cp -a %{SOURCE8} .

%build
export CFLAGS="$CFLAGS -Wno-error=unused-variable"
%configure --without-root-privileges --without-kernel-modules --disable-tests
%make


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# remove static and libtool libraries
rm -f %{buildroot}%{_libdir}/*.a %{buildroot}%{_libdir}/*.la  %{buildroot}%{_libdir}/open-vm-tools/plugins/common/*.la

# change mount.vmhgfs location
mv -f %{buildroot}%{_sbindir}/mount.vmhgfs %{buildroot}/sbin/mount.vmhgfs

# Common
install -D -m0755 %{SOURCE9} %{buildroot}%{_sysconfdir}/modprobe.d/vmxnet.conf

# vmtoolsd
install -D -m0600 %{SOURCE1} %{buildroot}%{_sysconfdir}/vmware-tools/tools.conf
install -D -m0755 %{SOURCE4} %{buildroot}%{_sysconfdir}/init.d/%{name}
install -D -m0644 %{SOURCE5} %{buildroot}%{_sysconfdir}/sysconfig/%{name}

# User
install -D -m0600 %{SOURCE2} %{buildroot}%{_sysconfdir}/vmware-tools/xautostart.conf
install -D -m0644 %{SOURCE11} %{buildroot}%{_sysconfdir}/xdg/autostart/vmware-user.desktop
install -D -m0644 %{SOURCE11} %{buildroot}%{_datadir}/applications/vmware-user.desktop

# fix up vmware-user.desktop to avoid displaying "Lost & Found" in menu of KDE
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category System \
  %{buildroot}%{_datadir}/applications/vmware-user.desktop

rm -rf %{buildroot}%{_datadir}/open-vm-tools/tests

# prepare dkms stuff
bash ./modules/linux/dkms.sh . %{buildroot}/usr/src
# replace dkms.conf so that we temporarily disable vmxnet module
cat %{SOURCE12} \
    | sed -e 's,@@SRCNAME@@,%{name},g' -e 's,@@VERSION@@,%{svndate},g' \
    > %{buildroot}/usr/src/%{name}-%{svndate}/dkms.conf

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add open-vm-tools
/sbin/ldconfig

%postun -p /sbin/ldconfig

%preun
if [ "$1" = 0 ]
then
    /sbin/service open-vm-tools stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del open-vm-tools
fi

%post dkms
/usr/sbin/dkms add -m %{name} -v %{svndate} || :
/usr/sbin/dkms build -m %{name} -v %{svndate} || :
/usr/sbin/dkms install -m %{name} -v %{svndate} || :

%preun dkms
/usr/sbin/dkms remove -m %{name} -v %{svndate} --all || :

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README xorg.conf
%dir %{_sysconfdir}/vmware-tools
%{_sysconfdir}/vmware-tools/poweroff-vm-default
%{_sysconfdir}/vmware-tools/poweron-vm-default
%{_sysconfdir}/vmware-tools/resume-vm-default
%{_sysconfdir}/vmware-tools/statechange.subr
%{_sysconfdir}/vmware-tools/suspend-vm-default
%{_sysconfdir}/vmware-tools/vm-support
%{_sysconfdir}/vmware-tools/scripts/vmware/network
%config(noreplace) %{_sysconfdir}/modprobe.d/vmxnet.conf
%{_sysconfdir}/init.d/%{name}
%{_sysconfdir}/pam.d/vmtoolsd
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%{_libdir}/libguestlib.so*
%{_libdir}/libvmtools.so*
%{_libdir}/libhgfs.so*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/plugins
%dir %{_libdir}/%{name}/plugins/vmsvc
%{_libdir}/%{name}/plugins/vmsvc/libguestInfo.so
#%%{_libdir}/%{name}/plugins/vmsvc/libhgfsServer.so
%{_libdir}/%{name}/plugins/vmsvc/libpowerOps.so
%{_libdir}/%{name}/plugins/vmsvc/libtimeSync.so
#%%{_libdir}/%{name}/plugins/vmsvc/libvix.so
%{_libdir}/%{name}/plugins/vmsvc/libvmbackup.so
%dir %{_libdir}/%{name}/plugins/vmusr
#%%{_libdir}/%{name}/plugins/vmusr/libhgfsServer.so
%{_libdir}/%{name}/plugins/vmusr/libresolutionSet.so
#%%{_libdir}/%{name}/plugins/vmusr/libvix.so
#%{_libdir}/%{name}/plugins/vmusr/libvixUser.so
%{_libdir}/%{name}/plugins/vmusr/libdesktopEvents.so
%{_libdir}/%{name}/plugins/vmusr/libdndcp.so
#%{_libdir}/%{name}/plugins/vmusr/libunity.so
%dir %{_libdir}/%{name}/plugins/common
%{_libdir}/%{name}/plugins/common/libhgfsServer.so
%{_libdir}/%{name}/plugins/common/libvix.so
%{_datadir}/open-vm-tools/messages/*/toolboxcmd.vmsg
%{_datadir}/open-vm-tools/messages/*/vmtoolsd.vmsg

/sbin/mount.vmhgfs
%{_bindir}/vmtoolsd
%{_bindir}/vmware-checkvm
%{_bindir}/vmware-hgfsclient
%{_bindir}/vmware-rpctool
%{_bindir}/vmware-toolbox-cmd
%{_bindir}/vmware-vmblock-fuse
%{_bindir}/vmware-xferlogs
#%%{_datadir}/%{name}
%{_libdir}/pkgconfig/vmguestlib.pc
%{_includedir}/vmGuestLib

%files gtk
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/vmware-tools/xautostart.conf
%config(noreplace) %{_sysconfdir}/vmware-tools/tools.conf
%{_sysconfdir}/xdg/autostart/vmware-user.desktop
#%{_bindir}/vmware-toolbox
#%{_bindir}/vmware-user
%{_bindir}/vmware-user-suid-wrapper
%{_datadir}/applications/vmware-user.desktop
#%{_datadir}/open-vm-tools/messages/*/toolbox.vmsg

%files docs
%defattr(-,root,root,-)
%{_datadir}/doc/%{name}

%files dkms
%defattr(-,root,root,-)
%dir /usr/src/%{name}-%{svndate}
/usr/src/%{name}-%{svndate}/dkms.conf
/usr/src/%{name}-%{svndate}/vmblock/
/usr/src/%{name}-%{svndate}/vmci/
/usr/src/%{name}-%{svndate}/vmhgfs/
/usr/src/%{name}-%{svndate}/vmsync/
/usr/src/%{name}-%{svndate}/vmxnet/
/usr/src/%{name}-%{svndate}/vsock/

%changelog
* Thu Mar 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.2013.09.16-3m)
- rebuild against icu-52

* Wed Jan 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.2013.09.16-2m)
- rebuild against procps-ng-3.3.9

* Fri Nov 29 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.2013.09.16-1m)
- update to 2013.09.16

* Fri Nov 29 2013 Ichiro Nakai <ichiro@n.email.ne.jp> 
- (0-0.2013.04.16-2m)
- rebuild against procps-ng

* Tue Sep 10 2013 Ichiro Nakai <ichiro@n.email.ne.jp> 
- (0-0.2013.04.16-1m)
- update to 2013.04.16
- kernel module cannot yet be built
- PLEASE REMOVE package dkms to build and install other dkms kernel modules

* Thu Oct 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.2012.08.01-1m)
- update to the latest version
- fix open-vm-tools.init script

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.2012.05.21-1m)
- merge from T4R/dkms-test/open-vm-tools
-- switch to use dkms
-- add patch for glib 2.31+

* Tue Nov  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-0.2011.10.26-1m)
- update 2011.10.26

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0-0.2011.01.24.4m)
- rebuild against icu-4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.2011.01.24.3m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.2011.01.24.2m)
- add %%config(noreplace) to some config files

* Mon Feb  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-0.2011.01.24.1m)
- update 2011.01.24

* Sun Jan 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.2010.12.19.1m)
- update to fix build failure 

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.2010.06.16.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.2010.06.16.3m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-2010.06.16-2m)
- rebuild against procps-3.2.8
- remove tests

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-2010.06.16-1m)
- update 2010.06.16-268169

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (0-0.2010.03.20.4m)
- rebuild against icu-4.2.1

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0-2010.03.20-3m)
- add %%dir

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-2010.03.20-2m)
- explicitly link libdl

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-2010.03.20-1m)
- update 2010.03.20-243334

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-2010.02.23-1m)
- update 2010.02.23-236320

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0-0.2010.01.19.3m)
- rename vmxnet to vmxnet.conf

* Mon Jan 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0-0.2010.01.19.2m)
- fix build on x86_64 by autoreconf

* Sun Jan 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-0.2010.01.19.1m)
- update 2010.01.19-226760

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-0.2009.11.16.1m)
- update 2009.11.16-210370

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.2009.10.15.3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.2009.10.15.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-0.2009.10.15.1m)
- update 2009.10.15-201664

* Tue Aug 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0-0.2009.07.22.2m)
- [BUG FIX] fix broken KDE menu
- fix up vmware-user.desktop to avoid displaying "Lost & Found" in menu of KDE

* Tue Jul 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-0.2009.07.22.1m)
- update 2009.07.22-179896

* Mon Jul 27 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (0-0.2009.06.18.3m)
- revised vmware-user.desktop file for gnome-session-properties

* Fri Jul 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.2009.06.18.2m)
- revise initscript and sysconfig (Source4,5)

* Fri Jul 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.2009.06.18.1m)
- update to 2009.06.18-172495
- add --without-root-privileges --without-kernel-modules
- remove --with-kernel-release="%%{kernel}"
- use make install

* Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.2009.03.18.1m)
- update to 2009.03.18-154848

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.2008.12.23.2m)
- rebuild against rpm-4.6

* Fri Jan 9 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.2008.12.23.1m)
- update to 2008.12.23

* Tue Oct 4 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (0.0.2008.10.10.1m)
- updated to 2008.10.10

* Tue Sep 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2008.09.03.3m)
- add BuildRequires:  kernel-devel

* Sun Sep 28 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.2008.09.03.2m)
- fix build for chroot

* Sun Sep 14 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.2008.09.03.1m)
- based open-vm-tools-2008.09.03-114782.tar.gz
- initial import for Momonga

* Sat Jun 28 2008 Johnny Hughes <johnny@centos.org> - 0.0.2008.6.20
- upgraded to upstream version open-vm-tools-2008.06.20-100027.tar.gz
- eliminated the open-vm-tools-common package and rolled those files
  into open-vm-tools

* Sat May 31 2008  Johnny Hughes <johnny@centos.org> - 0.0.2008.05.15 
- upgraded to upstream version open-vm-tools-2008.05.15-93241.tar.gz
- added vmware-user.desktop

* Sat May 10 2008 Johnny Hughes <johnny@centos.org> - 0.0.2008.05.02
- updated to upstream version open-vm-tools-2008.05.02-90473.tar.gz
- added a perl search to use the included procps headers
- added a perl search to disable treat warnings as errors for gcc in
  lib/guestInfo/
- used the --without-procps and --without-icu options for centos-4

* Mon Mar 31 2008 Johnny Hughes <johnny@centos.org> - 0.0.2008.03.19
- updated to upstream version open-vm-tools-2008.03.19-82724.tar.gz
- added /etc/modprobe.d/vmxnet for CentOS-5
- added --without-dnet to the configure line

* Thu Feb  7 2008 Johnny Hughes <johnny@centos.org> - 0.0.2008.01.23
- upgraded to upstream version open-vm-tools-2008.01.23-74039.tar.gz
- added an xorg.conf file that works properly for CentOS-4 or CentOS-5 clients
 
* Sat Dec 29 2007 Johnny Hughes <johnny@centos.org> - 0.0.2007.11.21 
- upgraded to upstream version open-vm-tools-2007.11.21-64693.tar.gz
- added building and install for xferlogs
- modified to build on CentOS-4 and CentOS-5
- added COPYING, README, ChangeLog, Source6, Source7 to docs

* Tue Oct 30 2007 Sean Dilda <sean@duke.edu> - 0.0.2007.10.08.2
- Modify init script to only run on VMs (as determined by vmware-checkvm)

* Fri Oct 26 2007 Sean Dilda <sean@duke.edu> - 0-0.2007.10.08.1
- Split off a -gtk package with all the X deps
- Add BR for gtk2-devel
- Change require from open-vm-tools-kmods to open-vm-tools-kmod
- Update init script - use sysconfig file to know what modules to load
- Update init script - unload loaded modules on stop
- Make vmware-guestd executable
- Add init script in %post


* Wed Oct 10 2007 Brandon Holbrook <fedora at theholbrooks.org> - 0-0.1.2007.10.08
- Bump version
- Fix missing BR

* Wed Sep 12 2007 Brandon Holbrook <fedora at theholbrooks.org> - 0-0.1.2007.09.04
- Initial Build

