%global         momorel 6

Name:           perl-MooseX-Role-WithOverloading
Version:        0.13
Release:        %{momorel}m%{?dist}
Summary:        Roles which support overloading
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Role-WithOverloading/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/MooseX-Role-WithOverloading-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-aliased
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Moose >= 0.94
BuildRequires:  perl-MooseX-Types
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-namespace-clean
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-XSLoader
Requires:       perl-aliased
Requires:       perl-Moose >= 0.94
Requires:       perl-MooseX-Types
Requires:       perl-namespace-autoclean
Requires:       perl-namespace-clean
Requires:       perl-XSLoader
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
MooseX::Role::WithOverloading allows you to write a Moose::Role which
defines overloaded operators and allows those operator overloadings to be
composed into the classes/roles/instances it's compiled to, while plain
Moose::Roles would lose the overloading.

%prep
%setup -q -n MooseX-Role-WithOverloading-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE README
%{perl_vendorarch}/auto/MooseX/Role/WithOverloading
%{perl_vendorarch}/MooseX/Role/WithOverloading*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-2m)
- rebuild against perl-5.16.3

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sun Jan  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sun Nov 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09-2m)
- rebuild for new GCC 4.6

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.08-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-2m)
- rebuild against perl-5.12.1

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-2m)
- rebuild against perl-5.12.0

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
