%global momorel 8

Summary: Spanish man pages from the Linux Documentation Project
Name: man-pages-es
Version: 1.55
Release: %{momorel}m%{?dist}
# These man pages come under various copyrights.
# All are freely distributable when the nroff source is included.
License: see "README"
Group: Documentation
URL: http://ditec.um.es/~piernas/manpages-es/
%define extra_name %{name}-extra
%define extra_ver 0.8a
%define extra_pkg_name %{extra_name}-%{extra_ver}
Source0: http://ditec.um.es/~piernas/manpages-es/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: http://ditec.um.es/~piernas/manpages-es/%{extra_pkg_name}.tar.gz
NoSource: 1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
Requires: man
Requires: filesystem

%description
This package contains the translation into Spanish of the English
man-pages package. It is a beta release, so you can still find a lot
of bugs.  Contributions are welcome. For any doubt or suggestion about
this release, send an e-mail to Juan Piernas Canovas <piernas at
ditec.um.es>. In order to collaborate with the project, please visit
http://es.tldp.org.

#%%package extra
#Summary: Extra manual pages - Spanish versions
#Group: Documentation
#Requires: %{name} = %{version}-%{release}
#
#%%description extra
#This package contains the eighth release of the Linux extra man pages
#in Spanish. Note it is an alpha release, so you can find a lot of bugs.
#These man pages are from several packages and applications. See PAQUETES
#file for more information about packages.

%prep
%setup -q -a 1

for i in README LEEME.extra PAQUETES PROYECTO; do
    iconv -f ISO8859-15 -t UTF-8 %{extra_pkg_name}/$i -o %{extra_pkg_name}/$i.utf8
    mv  %{extra_pkg_name}/$i.utf8 %{extra_pkg_name}/$i
done
mv %{extra_pkg_name}/README %{extra_pkg_name}/README.extra

# Bug 388391
rm %{extra_pkg_name}/man1/mc.1
rm %{extra_pkg_name}/man1/newgrp.1
# Bug 226124
for i in man3/dlopen.3 man5/acct.5 man5/host.conf.5 man5/resolver.5 man8/ld.so.8; do
    iconv -f UTF-8 -t UTF-8 $i -o $i.utf8
    mv $i.utf8 $i
    rm %{extra_pkg_name}/$i
done

for i in man-pages-1.55.Announce man7/iso_8859-7.7; do
    iconv -f ISO8859-15 -t UTF-8 $i -o $i.utf8
    mv $i.utf8 $i
done

for j in 1 1x 2 3 4 5 6 7 8 9; do
    for i in `find %{extra_pkg_name} -type f -name \*.$j`; do
        iconv -f ISO8859-15 -t UTF-8 $i -o $i.utf8
        mv $i.utf8 $i
    done
done

%build

%install
rm -rf %{buildroot}
make install MANDIR=%{buildroot}%{_mandir}/es
make -C %{extra_pkg_name} MANDIR=%{buildroot}%{_mandir}/es
rm -f %{buildroot}%{_mandir}/es/LEEME.extra
rm -f %{buildroot}%{_mandir}/es/PAQUETES
rm -f %{buildroot}%{_mandir}/es/PROYECTO
#[Bug 427684] man-pages fileconflict
#- Fix the conflict with vipw.8 and vigr.8(in shadow-utils)
rm -f %{buildroot}%{_mandir}/es/man8/vipw.8
rm -f %{buildroot}%{_mandir}/es/man8/vigr.8
  
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LEEME man-pages-1.55.Announce README CHANGES-1.28-1.55 CAMBIOS-1.28-1.55
%{_mandir}/es/man?/*

%changelog
* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.55-8m)
- release some directories provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.55-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.55-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.55-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.55-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.55-3m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.55-2m)
- remove vigr.8 to avoid conflicting with shadow-utils

* Wed Jul 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.55-1m)
- update to 1.55 (sync fedora)
- do not make extra package

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.28-4m)
- rebuild against gcc43

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.28-3m)
- remove %%{_mandir}/es from %%files, it's already provided by man

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.28-2m)
- sync FC

* Thu Nov 20 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (1.28-1m)
- version up to 1.28
- add URL:
- change Source0 location

* Wed Nov 12 2003 zunda <zunda at freeshell.org>
- (0.6a-1m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)


* Wed Sep 20 2000 Toru Hoshina <t@kondara.org>
- merge from pinstripe.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Mon Jun 19 2000 Matt Wilson <msw@redhat.com>
- defattr root

* Sun Jun 12 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%{_mandir} and %%{_tmppath}

* Mon May 15 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
