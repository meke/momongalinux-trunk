%global momorel 1

Summary:  Extract headers from rpm in a old yum repository
Name:     yum-arch
Version:  2.2.2
Release:  %{momorel}m%{?dist}
License:  GPLv2+
Group:    System Environment/Base
Source0:  http://linux.duke.edu/projects/yum/download/2.2/yum-%{version}.tar.gz
NoSource: 0

Patch1:   yum-arch-folder.patch
Patch2:   yum-arch-python26.patch

URL:      http://linux.duke.edu/yum/

BuildArch: noarch
BuildRequires: python, gettext

Requires:  python, rpm-python, rpm >= 0:4.1.1, libxml2-python
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Extract headers from rpm in a old yum repository.

This package only provides the old yum-arch command from yum-%{version}
It should be used to generate repository informations for Fedora Core  < 3
and RedHat Enterprise Linux < 4.

%prep
%setup -q -n yum-%{version}
%patch1 -p0 -b .folder
%patch2 -p1 -b .p26

# to avoid rpmlint warnings
for source in *.py {repomd,rpmUtils,yum}/*.py; do
    sed -i -e '/^#!\/usr/d' $source
done

%build
make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-, root, root, -)
%doc README AUTHORS COPYING TODO INSTALL ChangeLog
%{_datadir}/%{name}
%{_bindir}/%{name}
%{_mandir}/man8/%{name}*

%changelog
* Sun Sep 25 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.2-1m)
- import from Fedora

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.2-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Aug 11 2010 David Malcolm <dmalcolm@redhat.com> - 2.2.2-10
- recompiling .py files against Python 2.7 (rhbz#623421)

* Sun Feb 14 2010 Remi Collet <Fedora@FamilleCollet.com> - 2.2.2-9
- improve python 2.6 patch (fix FTBFS #564994)

* Sat Sep 26 2009 Remi Collet <Fedora@FamilleCollet.com> - 2.2.2-8
- fix python 2.6 warnings (#521869)

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat May 16 2009 Remi Collet <Fedora@FamilleCollet.com> - 2.2.2-6
- fix python 2.6 issue

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Dec 01 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 2.2.2-4
- Rebuild for Python 2.6

* Mon Aug 11 2008 Jason L Tibbitts III <tibbs@math.uh.edu> - 2.2.2-3
- Fix license tag.

* Sun Feb 18 2007 Remi Collet <Fedora@FamilleCollet.com> - 2.2.2-2
- from package review (#229123) 
  own /usr/share/yum-arch
  del shellbang in libs

* Sat Feb 17 2007 Remi Collet <Fedora@FamilleCollet.com> - 2.2.2-1
- initial spec for Extras
