%global momorel 17

# Note that this is NOT a relocatable package
Summary:   X Set Root Image
Name:      xsri
Version:   2.1.0
Release: %{momorel}m%{?dist}
License:   GPL
Group:     System Environment/Base
#Source0:   %{name}-%{PACKAGE_VERSION}.tar.gz
Source0:   %{name}-%{version}.tar.gz
Patch0: xsri-2.1.0-gtk222.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel
BuildRequires: popt
Obsoletes: xbanner

%description
X Set Root Image - displays images on the root window with 
parameters. Does nothing much more.

%prep
%setup
%patch0 -p1 -b .gtk222~

%build
autoreconf
%configure LIBS="-lX11 -lm"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc README
%{_bindir}/*

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-17m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-15m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.0-14m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-13m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-12m)
- fix build

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-8m)
- rebuild against gcc43

* Sat Apr 10 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.0-7m)
- import from Fedora.

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> 1:2.1.0-4
- rebuild on all arches

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Feb 11 2002 Owen Taylor <otaylor@redhat.com>
- Version 2.1.0

* Wed Aug 15 2001 Owen Taylor <otaylor@redhat.com>
- Version 2.0.3

* Fri Aug 03 2001 Owen Taylor <otaylor@redhat.com>
- Version 2.0.2

* Sun Jul 08 2001 Owen Taylor <otaylor@redhat.com>
- Version 2.0.1

* Sun Jul 08 2001 Owen Taylor <otaylor@redhat.com>
- Epoch is 1

* Sun Jul 08 2001 Owen Taylor <otaylor@redhat.com>
- Version 2.0.0

* Sun Jul  8 2001 Owen Taylor <otaylor@redhat.com>
- Completely new version

* Tue Apr 6 1999 The Rasterman <raster@redhat.com>
- made rpm
