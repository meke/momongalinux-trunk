Summary: FastCGI library for Ruby
Name: ruby-fcgi

%global momorel 4
%global abi_ver 1.9.1
%global src_name fcgi

Version: 0.8.8
Release: %{momorel}m%{?dist}
Group: Applications/Text
License: Ruby
URL: http://raa.ruby-lang.org/list.rhtml?name=fcgi
Source0: http://rubyforge.org/frs/download.php/69127/%{src_name}-%{version}.tgz
NoSource: 0
#Source0: http://www.moonwolf.com/ruby/archive/%{name}-%{version}.tar.gz
#Patch0:  ruby-fcgi-ruby19.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: libfcgi-devel >= 1.8.0

%description
FastCGI library for Ruby. It merges matz's C version(fcgi.so) & Eli's
pure ruby version(fastcgi.rb)

%prep
%setup -q -n %{src_name}-%{version}
chmod 644 README*

# %patch0 -p1 -b .ruby19

%build
ruby setup.rb config \
    --bin-dir=%{buildroot}%{_bindir} \
    --rb-dir=%{buildroot}%{ruby_sitelibdir} \
    --so-dir=%{buildroot}%{ruby_sitearchdir}

ruby setup.rb setup

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
ruby setup.rb install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README*
%{ruby_sitelibdir}/fcgi.rb
%{ruby_sitearchdir}/fcgi.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.8-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.8-1m)
- update 0.8.8

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.5-8m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.5-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.5-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.5-5m)
- rebuild against gcc43

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.5-4m)
- rebuild against ruby-1.8.6-4m

* Mon Feb  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.5-3m)
- cancel NoSource

* Mon Nov 22 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.8.5-2m)
- revised spec file to follow the kossori changes of released archive

* Fri Oct  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.5-1m)
- version up

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.4-2m)
- rebuild against ruby-1.8.2

* Mon Apr  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.4-1m)
- initial import to Momonga
