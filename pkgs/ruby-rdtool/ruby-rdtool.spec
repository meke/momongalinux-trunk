%global momorel 19
%global rbname rdtool

Summary: RD formatter
Name: ruby-%{rbname}
Version: 0.6.22
Release: %{momorel}m%{?dist}
Group: Applications/Text
License: GPL
URL: http://trac.moonwolf.com/%{rbname}/wiki/
Source0: http://www.moonwolf.com/ruby/archive/%{rbname}-%{version}.tar.gz 
NoSource: 0
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby-devel >= 1.9.2-0.992.11m
BuildRequires: rubygem-racc, ruby-amstd
BuildRequires: emacs >= %{_emacs_version}
Requires: ruby, ruby-amstd
Obsoletes: rdtool
Provides: rdtool
Obsoletes: rd-mode-xemacs

%description
RD document formatter & library. RD is the standard documentation
format in the commmunity of Object Oriented Scripting Language: Ruby.

%package -n emacs-rd-mode
Group: Applications/Editors
Summary: Major mode for RD editing on Emacs
Requires: emacs >= %{_emacs_version}
Obsoletes: rd-mode-emacs
Obsoletes: elisp-rd-mode

%description -n emacs-rd-mode
Major mode for RD editing.

%prep
%setup -q -n rdtool-%{version}

%build
ruby setup.rb config \
    --bindir=%{_bindir} \
    --rbdir=%{ruby_sitelibdir} \
    --sodir=%{ruby_sitearchdir}
ruby setup.rb setup
(cd utils && %{_emacs_bytecompile} rd-mode.el)

%install
rm -rf %{buildroot}
ruby setup.rb config \
    --bindir=%{buildroot}%{_bindir} \
    --rbdir=%{buildroot}%{ruby_sitelibdir} \
    --sodir=%{buildroot}%{ruby_sitearchdir}
ruby setup.rb install
(cd utils
  %{__mkdir_p} %{buildroot}%{_emacs_sitelispdir}/rd
  %{__install} -m 644 rd-mode.el %{buildroot}%{_emacs_sitelispdir}/rd/
  %{__install} -m 644 rd-mode.elc %{buildroot}%{_emacs_sitelispdir}/rd/
)

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/rd2
%{_bindir}/rdswap
%{ruby_sitelibdir}/rd
%doc README.html README.ja.html README.rd README.rd.ja HISTORY
%doc doc/rd-draft.rd doc/rd-draft.rd.ja

%files -n emacs-rd-mode
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/rd
%{_emacs_sitelispdir}/rd

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.22-19m)
- rebuild for emacs-24.1

* Wed Dec 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.22-18m)
- change BuildRequires from ruby-racc to rubygem-racc

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.22-17m)
- rename elisp-rd-mode to emacs-rd-mode

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.22-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.22-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.22-14m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.22-13m)
- rebuild against new ruby for mod_ruby

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.22-12m)
- rebuild against ruby-1.9.2

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.22-11m)
- rebuild against emacs-23.2

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.22-10m)
- rename rd-mode-emacs to elisp-rd-mode
- kill rd-mode-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.22-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.22-8m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.22-7m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.22-6m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.22-5m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.22-4m)
- rebuild against xemacs-21.5.29

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.22-3m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.22-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.22-1m)
- update 0.6.22

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.20-6m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.20-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.20-4m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.20-2m)
- rebuild against ruby-1.8.6-4m

* Tue Oct 31 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.20-1m)
- update to 0.6.20
  obsolete Patch0: %%{rbname}-0.6.13-momonga.patch
  obsolete Patch1: %%{rbname}-0.6.13-output-format-visitor.rb-for-ruby-1.8.patch
- rebuild against emacs-22.0.90
- change License from GPL to Ruby
- change URI

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.13-2m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Wed Aug 20 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.6.13-1m)
- version up
- apply Patch1: rdtool-0.6.13-output-format-visitor.rb-for-ruby-1.8.patch

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.6.11-5m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.6.11-4m)
- rebuild against ruby-1.8.0.

* Wed Nov 13 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.6.11-3m)
- requires ruby-racc-runtime, not ruby-racc.
- use rbconfig.

* Mon May 20 2002 Junichiro Kita<kita@kondara.org>
- (0.6.11-2k)
- ver up

* Tue Nov 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6.10-10k)
- grab directory

* Thu Nov  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6.10-8k)
- apply rdtool-0.6.10-rd2html-lib.rb.patch
- never use %triggerin

* Thu Oct 25 2001 Kenta MURATA <muraken2@nifty.com>
- (1.6.10-6k)
- use %triggerin section for rd-mode-emacsen

* Fri Oct 19 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6.10-4k)
- add rdtool.rdblockparser.ry.patch

* Mon Oct  8 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6.10-2k)
- add utils/rdswap.rb
- merge 'rd-mode-emacs' package
- make 'rd-mode-xemacs' package

* Wed Mar 28 2001 Kenta MURATA <muraken2@nifty.com>
- (1.6.7-3k)
- version 1.6.7

* Thu Oct 19 2000 Toru Hoshina <toru@df-usa.com>
- it should be suitable to both ruby 1.4.x and 1.6.x.

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against ruby 1.6.1.

* Tue Sep 12 2000 Toru Hoshina <t@kondara.org>
- renamed ruby-rdtool from rdtool.

* Sat Jun 18 2000 AYUHANA Tomonori <l@kondara.org>
- add ruby-amstd at BuildPreReq
- add -q at %setup

* Sat Jun 10 2000 OZAWA -Crouton- Sakuro <crouton@duelists.org>
- Upstream new version.

* Sun Apr 23 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Copyright, Group, BuildRoot, Summary )

* Thu Apr 20 2000 Toru Hoshina <t@kondara.org>
- add BuildPreReq tag for racc.
- No architecture dependency.

* Sat Apr  8 2000 OZAWA -Crouton- Sakuro <crouton@duelists.org>
- First release for Kondara.

