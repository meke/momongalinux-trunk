%global momorel 5

Name: hardinfo
Version: 0.5.1
Release: %{momorel}m%{?dist}
Summary: System Profiler and Benchmark      

Group: Applications/System     
License: GPLv2        
URL: http://hardinfo.berlios.de/            
Source0:  http://download.berlios.de/hardinfo/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel
BuildRequires:  libsoup-devel >= 2.4.1-2m
BuildRequires:  desktop-file-utils
BuildRequires:  pciutils
BuildRequires:  libtasn1-devel

Requires:  pciutils 
Requires:  gcc
Requires:  glx-utils
Requires:  xorg-x11-utils

%description
HardInfo can gather information about a system's hardware and operating system,
perform benchmarks, and generate printable reports either in HTML or in plain 
text formats

%prep
%setup -q

%build
export LIBDIR=%{_libdir}
%configure
make %{?_smp_mflags} ARCHOPTS="%{optflags}"

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT LIBDIR=%{_libdir} 
desktop-file-install --vendor="" --delete-original        \
  --dir=${RPM_BUILD_ROOT}%{_datadir}/applications         \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/hardinfo.desktop

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/hardinfo
%{_libdir}/hardinfo
%{_datadir}/hardinfo
%{_datadir}/applications/hardinfo.desktop
%doc LICENSE

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2.3-3m)
- rebuild against rpm-4.6

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2.3-2m)
- rebuild against gnutls-2.4.1 (libsoup)

* Tue Jul 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.2.3-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2.2-4m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2.2-3m)
- rebuild against libsoup-2.4.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2.2-2m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2.2-1m)
- import to Momonga from Fedora devel

* Fri Aug 03 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-14
- Revert patch doesn't work 

* Fri Aug 03 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-13
- Update License tag

* Thu Aug 02 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-12
- Try again to fix RH #249794

* Tue Jul 31 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-11
- Enable debug output

* Mon Jul 30 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-10
- Fix changelog date ....

* Mon Jul 30 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-9
- Try to fix RH #249794 (upstream patch)

* Fri Jul 27 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-8
- Fix build

* Fri Jul 27 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-7
- bump release

* Fri Jul 27 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-6
- Specfile cleanups

* Fri Jul 27 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-5
- Add better sensors fix from upstream

* Thu Jul 26 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-4
- Fix and reenable sensor reading
- Remove zlib requires
- Fix group

* Wed Jul 25 2007 Jesse Keating <jkeating@redhat.com> - 0.4.2.2-3
- Rebuild for RH #249435

* Mon Jul 23 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-2
- In Fedora (human)uids start with 500 not 1000

* Mon Jul 23 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.4.2.2-1
- Initial Build
