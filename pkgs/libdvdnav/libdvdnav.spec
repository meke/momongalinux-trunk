%global momorel 1

Summary: a library for DVD navigation
Name: libdvdnav
Version: 4.2.0
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: GPLv2+
URL: http://www.mplayerhq.hu/
Source0: http://dvdnav.mplayerhq.hu/releases/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen
BuildRequires: libdvdread-devel >= %{version}
BuildRequires: pkgconfig

%description
libdvdnav provides support to applications wishing to make use of advanced
DVD features.

%package devel
Summary: DVD Navigation library headers and support files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libdvdread-devel >= %{version}
Requires: pkgconfig

%description devel
libdvdnav provides support to applications wishing to make use of advanced
DVD features.

This package contains the C headers and support files for compiling 
applications with libdvdnav.

%prep
%setup -q

%build
./configure2 \
	--disable-opts \
	--extra-cflags="%{optflags}" \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--shlibdir=%{_libdir} \
	--enable-shared \
	--disable-strip

%make

pushd doc
doxygen doxy.conf
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install dvdnav.m4
mkdir -p %{buildroot}%{_datadir}/aclocal
install -m 644 m4/dvdnav.m4 %{buildroot}%{_datadir}/aclocal/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog DEVELOPMENT-POLICY.txt
%doc INSTALL NEWS README TODO
%{_libdir}/libdvdnav.so.*
%{_libdir}/libdvdnavmini.so.*

%files devel
%defattr(-,root,root)
%doc doc/html
%{_bindir}/dvdnav-config
%{_includedir}/dvdnav
%{_libdir}/pkgconfig/dvdnav.pc
%{_libdir}/pkgconfig/dvdnavmini.pc
%{_libdir}/libdvdnav.a
%{_libdir}/libdvdnav.so
%{_libdir}/libdvdnavmini.so
%{_datadir}/aclocal/dvdnav.m4

%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.0-1m)
- update 4.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.3-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.3-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.3-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.3-2m)
- add Requires: pkgconfig to libdvdnav-devel

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.3-1m)
- version 4.1.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.2-2m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.2-1m)
- switch to new upstream including libdvdread
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.10-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.10-4m)
- %%NoSource -> NoSource

* Wed Apr 25 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.1.10-3m)
- symlink include/*.h -> include/dvdnav/*.h

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.10-2m)
- delete libtool library

* Wed Oct  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.10-1m)
- version 0.1.10
- add a package libdvdnav-devel
- change Group: from Development/Libraries to System Environment/Libraries
- import libdvdnav-0.1.10-fixdvdcrash.patch.bz2 from cooker
 +* Fri Oct 22 2004 Frederic Crozat <fcrozat@mandrakesoft.com> 0.1.10-2mdk
 +- Patch0 (CVS): fix crash with DVD without first play chain
- use %%configure and %%make

* Sat Dec 20 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.1.9-1m)
- version 0.1.9

* Sat Jun 28 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.1.3-2m)
- libtoolize for libtool...

* Sun Aug  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.3-1m)

* Sat Jul 13 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.2-1m)

* Sun Jun 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.1-2m)
- add 'BuildPreReq: libdvdread-devel'

* Sat Jun 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.1-1m)
