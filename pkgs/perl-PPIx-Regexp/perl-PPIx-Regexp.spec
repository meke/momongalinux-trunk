%global         momorel 2

Name:           perl-PPIx-Regexp
Version:        0.036
Release:        %{momorel}m%{?dist}
Summary:        Represent a regular expression of some sort
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/PPIx-Regexp/
Source0:        http://www.cpan.org/authors/id/W/WY/WYANT/PPIx-Regexp-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Module-Build
BuildRequires:  perl-PPI >= 1.117
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Test-Simple >= 0.4
Requires:       perl-List-MoreUtils
Requires:       perl-PPI >= 1.117
Requires:       perl-Scalar-Util
Requires:       perl-Task-Weaken
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The purpose of the PPIx-Regexp package is to parse regular expressions in a
manner similar to the way the PPI package parses Perl. This class forms the
root of the parse tree, playing a role similar to PPI::Document.

%prep
%setup -q -n PPIx-Regexp-%{version}

find . -type f | xargs perl -p -i -e 's|/usr/local/bin/perl|/usr/bin/perl|'

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes eg LICENSES README xt
%{perl_vendorlib}/PPIx/Regexp
%{perl_vendorlib}/PPIx/Regexp.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.036-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.036-1m)
- update to 0.36
- rebuild against perl-5.18.2

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.035-1m)
- update to 0.035

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.034-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.034-2m)
- rebuild against perl-5.18.0

* Sun May 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.034-1m)
- update to 0.034

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.033-2m)
- rebuild against perl-5.16.3

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.033-1m)
- update to 0.033

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.032-1m)
- update to 0.032

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.031-1m)
- update to 0.031

* Thu Jan 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.030-1m)
- update to 0.030

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.029-1m)
- update to 0.029

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.028-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.028-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.028-1m)
- update to 0.028
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.026-1m)
- update to 0.026

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.025-1m)
- update to 0.025

* Sun Dec 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.024-1m)
- update to 0.024

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.023-1m)
- update to 0.023

* Fri Nov 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.022-1m)
- update to 0.022

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.021-2m)
- rebuild against perl-5.14.2

* Mon Aug  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.021-1m)
- update to 0.021

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.020-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.020-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.020-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.020-1m)
- update to 0.020

* Thu Feb 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.018-1m)
- update to 0.018

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.017-1m)
- update to 0.017

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.016-1m)
- update to 0.016

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.015-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.015-1m)
- update to 0.015

* Fri Oct 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.014-1m)
- update to 0.014

* Mon Oct 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.013-1m)
- update to 0.013

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.011-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.011-1m)
- update to 0.011

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.008-2m)
- full rebuild for mo7 release

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.008-1m)
- update to 0.008

* Wed Jun 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.007-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
