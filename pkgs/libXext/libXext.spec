%global momorel 1

Summary: X.Org X11 libXext runtime library
Name: libXext
Version: 1.3.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-proto-devel >= 7.5-0.6m
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: fop >= 0.95
Buildrequires: xorg-x11-sgml-doctools >= 1.7
BuildRequires: xmlto-tex

%description
X.Org X11 libXext runtime library

%package devel
Summary: X.Org X11 libXext development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3

Requires: libX11-devel
Requires: xorg-x11-proto-devel >= 7.5

%description devel
X.Org X11 libXext development package

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README INSTALL ChangeLog
%{_libdir}/libXext.so.6
%{_libdir}/libXext.so.6.4.0
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root,-)
%{_libdir}/libXext.so
%{_libdir}/pkgconfig/xext.pc
%{_includedir}/X11/extensions/MITMisc.h
%{_includedir}/X11/extensions/XEVI.h
%{_includedir}/X11/extensions/XLbx.h
%{_includedir}/X11/extensions/XShm.h
%{_includedir}/X11/extensions/Xag.h
%{_includedir}/X11/extensions/Xcup.h
%{_includedir}/X11/extensions/Xdbe.h
%{_includedir}/X11/extensions/Xext.h
%{_includedir}/X11/extensions/Xge.h
%{_includedir}/X11/extensions/dpms.h
%{_includedir}/X11/extensions/extutil.h
#%{_includedir}/X11/extensions/lbxbuf.h
#%{_includedir}/X11/extensions/lbxbufstr.h
#%{_includedir}/X11/extensions/lbximage.h
%{_includedir}/X11/extensions/multibuf.h
%{_includedir}/X11/extensions/security.h
%{_includedir}/X11/extensions/shape.h
%{_includedir}/X11/extensions/sync.h
%{_includedir}/X11/extensions/xtestext1.h
%{_mandir}/man3/DBE.3*
%{_mandir}/man3/DPMSCapable.3*
%{_mandir}/man3/DPMSDisable.3*
%{_mandir}/man3/DPMSEnable.3*
%{_mandir}/man3/DPMSForceLevel.3*
%{_mandir}/man3/DPMSGetTimeouts.3*
%{_mandir}/man3/DPMSGetVersion.3*
%{_mandir}/man3/DPMSInfo.3*
%{_mandir}/man3/DPMSQueryExtension.3*
%{_mandir}/man3/DPMSSetTimeouts.3*
%{_mandir}/man3/XShape.3*
%{_mandir}/man3/XShapeCombineMask.3*
%{_mandir}/man3/XShapeCombineRectangles.3*
%{_mandir}/man3/XShapeCombineRegion.3*
%{_mandir}/man3/XShapeCombineShape.3*
%{_mandir}/man3/XShapeGetRectangles.3*
%{_mandir}/man3/XShapeInputSelected.3*
%{_mandir}/man3/XShapeOffsetShape.3*
%{_mandir}/man3/XShapeQueryExtension.3*
%{_mandir}/man3/XShapeQueryExtents.3*
%{_mandir}/man3/XShapeQueryVersion.3*
%{_mandir}/man3/XShapeSelectInput.3*
%{_mandir}/man3/XcupGetReservedColormapEntries.3*
%{_mandir}/man3/XcupQueryVersion.3*
%{_mandir}/man3/XcupStoreColors.3*
%{_mandir}/man3/XdbeAllocateBackBufferName.3*
%{_mandir}/man3/XdbeBeginIdiom.3*
%{_mandir}/man3/XdbeDeallocateBackBufferName.3*
%{_mandir}/man3/XdbeEndIdiom.3*
%{_mandir}/man3/XdbeFreeVisualInfo.3*
%{_mandir}/man3/XdbeGetBackBufferAttributes.3*
%{_mandir}/man3/XdbeGetVisualInfo.3*
%{_mandir}/man3/XdbeQueryExtension.3*
%{_mandir}/man3/XdbeSwapBuffers.3*
%{_mandir}/man3/Xevi.3*
%{_mandir}/man3/XeviGetVisualInfo.3*
%{_mandir}/man3/XeviQueryExtension.3*
%{_mandir}/man3/XeviQueryVersion.3*
%{_mandir}/man3/Xmbuf.3*
%{_mandir}/man3/XmbufChangeBufferAttributes.3*
%{_mandir}/man3/XmbufChangeWindowAttributes.3*
%{_mandir}/man3/XmbufCreateBuffers.3*
%{_mandir}/man3/XmbufCreateStereoWindow.3*
%{_mandir}/man3/XmbufDestroyBuffers.3*
%{_mandir}/man3/XmbufDisplayBuffers.3*
%{_mandir}/man3/XmbufGetBufferAttributes.3*
%{_mandir}/man3/XmbufGetScreenInfo.3*
%{_mandir}/man3/XmbufGetVersion.3*
%{_mandir}/man3/XmbufGetWindowAttributes.3*
%{_mandir}/man3/XmbufQueryExtension.3*
%{_mandir}/man3/XShm.3*
%{_mandir}/man3/XShmAttach.3*
%{_mandir}/man3/XShmCreateImage.3*
%{_mandir}/man3/XShmCreatePixmap.3*
%{_mandir}/man3/XShmDetach.3*
%{_mandir}/man3/XShmGetEventBase.3*
%{_mandir}/man3/XShmGetImage.3*
%{_mandir}/man3/XShmPixmapFormat.3*
%{_mandir}/man3/XShmPutImage.3*
%{_mandir}/man3/XShmQueryExtension.3*
%{_mandir}/man3/XShmQueryVersion.3*
%{_datadir}/doc/%{name}

%changelog
* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-6m)
- add BuildRequires

* Sun Jun 26 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (1.3.0-5m)
- add BR fop, xorg-x11-sgml-doctools

* Mon May  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- update to 1.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-2m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1

* Wed Oct  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-1m)
- update 1.1

* Fri Feb 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-2m)
- BR: xorg-x11-proto-devel >= 7.5

* Tue Feb 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.5-1m)
- update 1.0.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-2m)
- rebuild against gcc43

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update 1.0.4

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- %%NoSource -> NoSource

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-1m)
- update 1.0.1

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-4m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-3.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-3.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-3.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 31 2006 Mike A. Harris <mharris@redhat.com> 1.0.0-3
- Added "Requires: xorg-x11-proto-devel >= 7.0-1" to devel package (#173713)
- Added "libX11-devel" to devel package (#176078)

* Mon Jan 23 2006 Mike A. Harris <mharris@redhat.com> 1.0.0-2
- Bumped and rebuilt

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated libXext to version 1.0.0 from X11R7 RC4

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Updated libXext to version 0.99.3 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated libXext to version 0.99.2 from X11R7 RC2
- Changed 'Conflicts: XFree86-devel, xorg-x11-devel' to 'Obsoletes'
- Changed 'Conflicts: XFree86-libs, xorg-x11-libs' to 'Obsoletes'

* Fri Oct 21 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated to libXext-0.99.1 from the X11R7 RC1 release.
- Added manpages that were absent in X11R7 RC0, and updated the file lists
  to find them in section "man3x".

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Renamed package to remove xorg-x11 from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
