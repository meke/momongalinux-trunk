%global         momorel 3

Name:           p11-kit
Version:        0.20.2
Release:        %{momorel}m%{?dist}
Summary:        Library for loading and sharing PKCS#11 modules

Group:          Development/Libraries
License:        BSD
URL:            http://p11-glue.freedesktop.org/p11-kit.html
Source0:        http://p11-glue.freedesktop.org/releases/p11-kit-%{version}.tar.gz
NoSource:	0
Source1:        trust-extract-compat
%description
p11-kit provides a way to load and enumerate PKCS#11 modules, as well
as a standard configuration setup for installing PKCS#11 modules in
such a way that they're discoverable.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package trust
Summary:        System trust module from %{name}
Requires:       %{name} = %{version}-%{release}
Requires(post):   chkconfig
Requires(postun): chkconfig
Conflicts:        nss < 3.14.3-9

%description trust
The %{name}-trust package contains a system trust PKCS#11 module which
contains certificate anchors and black lists.


# solution taken from icedtea-web.spec
%define multilib_arches ppc64 sparc64 x86_64 ppc64le
%ifarch %{multilib_arches}
%define alt_ckbi  libnssckbi.so.%{_arch}
%else
%define alt_ckbi  libnssckbi.so
%endif


%prep
%setup -q


%build
%configure --disable-static \
           --enable-doc \
           --with-trust-paths=%{_sysconfdir}/pki/ca-trust/source:%{_datadir}/pki/ca-trust-source \
           --with-hash-impl=freebl \
           --disable-silent-rules
%make V=1


%install
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}/pkcs11/modules
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/pkcs11/*.la
install -p -m 755 %{SOURCE1} %{buildroot}%{_libdir}/p11-kit/
# Install the example conf with %%doc instead
rm %{buildroot}%{_sysconfdir}/pkcs11/pkcs11.conf.example


%post -p /sbin/ldconfig

%post trust
%{_sbindir}/update-alternatives --install %{_libdir}/libnssckbi.so \
        %{alt_ckbi} %{_libdir}/pkcs11/p11-kit-trust.so 20

%postun -p /sbin/ldconfig

%postun trust
if [ $1 -eq 0 ] ; then
        # package removal
        %{_sbindir}/update-alternatives --remove %{alt_ckbi} %{_libdir}/pkcs11/p11-kit-trust.so
fi


%files
%doc AUTHORS COPYING NEWS README
%dir %{_sysconfdir}/pkcs11
%dir %{_sysconfdir}/pkcs11/modules
%{_bindir}/p11-kit
%{_libdir}/libp11-kit.so.*
%{_libdir}/p11-kit-proxy.so
%{_mandir}/man1/trust.1.*
%{_mandir}/man8/p11-kit.8.*
%{_mandir}/man5/pkcs11.conf.5.*

%files devel
%{_includedir}/p11-kit-1/
%{_libdir}/libp11-kit.so
%{_libdir}/pkgconfig/p11-kit-1.pc
%doc %{_datadir}/gtk-doc/html/p11-kit

%files trust
%{_bindir}/trust
%dir %{_libdir}/pkcs11
%{_libdir}/pkcs11/p11-kit-trust.so
%{_datadir}/p11-kit/modules/p11-kit-trust.module
%{_libdir}/p11-kit/trust-extract-compat

%changelog
* Mon Apr  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20.2-3m)
- change prioity (update-alternatives)

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20.2-2m)
- define alt_ckbi correctly...

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20.2-1m)
- update to 0.20.2

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Tue Dec  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9-1m)
- update to 0.9

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Sun Sep 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-2m)
- delete conflict dir

* Mon Aug 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4-1m)
- update 0.4

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-1m)
- Initial commit Momonga-Linux

* Fri Jul 29 2011 Kalev Lember <kalevlember@gmail.com> - 0.3-1
- Update to 0.3
- Upstream rewrote the ASL 2.0 bits, which makes the whole package
  BSD-licensed

* Tue Jul 12 2011 Kalev Lember <kalevlember@gmail.com> - 0.2-1
- Initial RPM release
