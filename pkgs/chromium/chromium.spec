%global momorel 1

# We need policy first.
%global selinux 0

# Useful for debugging
%global noisy 1

# Really, we want this on, but for testing...
%global sharedbuild 0

## Maybe, someday, I'll be able to flip these on. ##

# Chromium depends on forked NSS features not found in upstream.
%global nss 0

# Chromium depends on system libsrtp
%global system_libsrtp 0

Name:		chromium
Version:	36.0.1985.98
Release:	%{momorel}m%{?dist}
Summary:	A WebKit powered web browser
# Licensing Overview
# 
# ffmpeg headers are LGPLv2+
# libjingle is BSD

# TODO: Proper list of licenses
License:	BSD and LGPLv2+
Group:		Applications/Internet

# Just the code changes, none of the "makefile" changes
Patch0:		chromium-33.0.1725.0-codechanges-system-nss-nspr.patch

# These are the conditionals that upstream has added:
# bzip2, jpeg, png, zlib, xml, xslt, libevent, v8, icu, speex, flac, minizip

# webkit needs to call nss to pull in nspr headers
Patch10:        chromium-14.0.827.10-system-nss.patch

# Courgette not in source tree
Patch15:        chromium-23.0.1271.95-no-courgette.patch

# Don't try to build non-existent test sources
Patch19:        chromium-29.0.1518.2-no-test-sources.patch

# Fix ffmpeg wrapper compile
Patch25:        chromium-25.0.1364.172-ffmpeg-no-pkgconfig.patch

# Use system libjingle
Patch29:        chromium-20.0.1132.47-system-libjingle.patch

# Use system copy of FLAC and speex
Patch40:        chromium-25.0.1364.172-system-flac-speex.patch

# Fix gcc46 issues
Patch41:        chromium-25.0.1364.172-gcc46.patch

# Pull in nss for nspr for chrome_common
Patch43:        chromium-19.0.1084.56-chrome-common-nss.patch

# Use system nss in base
Patch45:        chromium-33.0.1725.0-prtimefix.patch

# Fix compile against external ffmpegsumo
# This is a bit hacky, but I didn't want to re-learn GYP.
Patch53:        chromium-13.0.782.112-ffmpegsumo-compile-fix.patch

# Get nacl going
Patch55:        chromium-23.0.1271.95-build-nacl-irt.patch

# Add missing include for header that provides OVERRIDE definition
Patch57:        chromium-20.0.1132.47-OVERRIDE-define-fix.patch

# Use --no-keep-memory in ldflags to ensure the final exec doesn't run
# out of memory while linking.
Patch62:        chromium-20.0.1132.47-ldflags-no-keep-memory.patch

# Use system jsoncpp
Patch66:        chromium-25.0.1364.172-system-jsoncpp.patch

# Use system webrtc
Patch67:        chromium-25.0.1364.172-system-webrtc.patch

# Use system libusb1
Patch71:        chromium-25.0.1364.172-system-libusb.patch

# Chromium contains a local implementation of a string byte sink needed when ICU is compiled without
# support for std::string which is the case on Android. However, Fedora's system ICU has std::string.
Patch72:        chromium-21.0.1180.81-only-droid.patch

# http://code.google.com/p/gperftools/issues/detail?id=444
Patch73:        chromium-21.0.1180.81-glibc216.patch

# Taken from upstream webkit
# http://trac.webkit.org/changeset/124099
Patch74:        changeset_124099.diff

# Don't unpack nacl-sources. We've got it.
Patch75:        chromium-25.0.1364.172-more-naclfixes.patch

Patch78:        chromium-25.0.1364.172-stillmorenaclfixes.patch

Patch79:        chromium-25.0.1364.172-fixvars.patch

Patch80:        chromium-25.0.1364.172-nacl-nostamp.patch

# Thanks to Gentoo
Patch81:        chromium-25.0.1364.172-no-pnacl.patch

# From WebKit Bugzilla
# https://bugs.webkit.org/show_bug.cgi?id=113454
Patch82:        webkit-bug-113454-gcc48.patch

Patch83:        chromium-25.0.1364.172-system-opus.patch

Patch84:        chromium-25.0.1364.172-system-png.patch

Patch85:        chromium-25.0.1364.172-speex-fix.patch

Patch86:        chromium-25.0.1364.172-system-vpx.patch

Patch87:        chromium-25.0.1364.172-system-libyuv.patch

Patch1000:	chromium-33.0.1750.70-nogn.patch
Patch1001:	chromium-36.0.1985.67-opus.patch

# OLD: Use chromium-daily-tarball.sh to generate tarball from svn.
# New: Use chromium-latest.py to generate clean tarball from released build tarballs, found here:
# http://build.chromium.org/buildbot/official/
#Source0:	chromium-%{version}-clean.tar.lzma
Source0:	http://gsdview.appspot.com/chromium-browser-official/chromium-%{version}.tar.xz
NoSource: 0
# Custom build tools for chromium, hammer is a fancy front-end for scons
Source1:	http://src.chromium.org/svn/trunk/tools/depot_tools.tar.gz
Source2:	chromium-browser.sh
Source3:	chromium-browser.desktop
# We don't actually use this in the build, but it is included so you can make the tarball.
Source4:	chromium-daily-tarball.sh
Source5:	chromium-browser.xml
# Set default prefs
Source6:	master_preferences
# Also, only used if you want to reproduce the clean tarball.
Source7:	chromium-latest.py

# Nicer icon generated via inkscape from chromium.ai
# http://src.chromium.org/viewvc/chrome/trunk/src/chrome/app/theme/chromium/chromium.ai?view=log&pathrev=81848
Source9:        chromium-12-256x256.svg

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	bzip2-devel, libevent-devel, libjpeg-devel, libpng-devel
#BuildRequires:	bzip2-devel, libevent-devel >= 2.0.10, libjpeg-devel, libpng-devel
BuildRequires:	libxslt-devel, nss-devel, nspr-devel, minizip-devel
BuildRequires:	gcc-c++, bison, flex, gtk2-devel, atk-devel
BuildRequires:	fontconfig-devel, GConf2-devel, dbus-devel, alsa-lib-devel
BuildRequires:	desktop-file-utils, gperf, libselinux-devel
BuildRequires:	nss-devel >= 3.12.3, expat-devel
BuildRequires:	dbus-glib-devel, libXScrnSaver-devel
BuildRequires:	cups-devel

BuildRequires:  libicu-devel >= 4.6
BuildRequires:  ffmpeg-devel >= 0.6.1-0.20110514
BuildRequires:  libvpx-devel >= 1.0.0
%if 0%{?system_libsrtp}
BuildRequires:  libsrtp-devel
%endif
BuildRequires:  perl >= 5.14.0
BuildRequires:  glibc-devel >= 2.14

# NaCl needs these
BuildRequires:  libstdc++-devel, openssl-devel

BuildRequires:	sqlite-devel

# On x86_64, it also needs these:
# %ifarch x86_64
# BuildRequires:	glibc-devel(x86-32), libgcc(x86-32)
# %endif

%if 0%{?selinux}
BuildRequires:	libselinux-devel
%endif

ExclusiveArch:	%{ix86} arm x86_64

# GTK modules it expects to find for some reason.
Requires:	libcanberra

# We pick up an automatic requires on the library, but we need the version check
# because the nss shared library is unversioned.
# This is to prevent someone from hitting http://code.google.com/p/chromium/issues/detail?id=26448
Requires:	nss%{_isa} >= 3.12.3
Requires:	nss-mdns%{_isa}

# We want to make sure we have the chromium-libs
Requires:	chromium-libs = %{version}-%{release}

%description
Chromium is an open-source web browser, powered by WebKit.

%package libs
Summary:	Shared libraries for chromium
Group:		System Environment/Libraries

%description libs
This package contains the shared libraries that chromium depends on. Some of 
these libraries are unique to chromium, others are forked versions of system 
libraries.

%prep
echo "####### BUILD OPTIONS #######"
echo "Using system NSS: %{nss}"
echo "Built-in SELinux support: %{selinux}"
echo "Verbose Build: %{noisy}"
%setup -q -n chromium-%{version} -a 1
cp %{SOURCE4} .

# Patch in code changes for system libs
#%%patch0 -p1 -b .system-code

# Courgette not in source tree
%patch15 -p1 -b .no-courgette

# Don't try to build non-existent test sources
#%%patch19 -p1 -b .notests

# Fix ffmpeg wrapper
# use system ffmpeg
#%patch25 -p1 -b .no-pkgconfig

%if 0%{?jingle}
# Use system libjingle
#%patch29 -p1 -b .jingle
%endif

#%patch40 -p1 -b .system-flac

#%patch41 -p1 -b .gcc46

%patch43 -p1 -b .nss

#%%patch45 -p1 -b .base-nss

#%patch53 -p1 -b .ffmpegsumo

#%patch55 -p1 -b .buildnacl

%patch57 -p1 -b .OVERRIDE

#%patch62 -p1 -b .ldflags

#%patch66 -p1 -b .system-jsoncpp

#%patch67 -p1 -b .system-webrtc

#%patch71 -p1 -b .system-libusb

%patch72 -p1 -b .only-droid 

#%%patch73 -p1 -b .glibc216

# %patch74 -p1 -b .124099

# %patch75 -p1 -b .naclfixes

#%patch78 -p1 -b .stillmorenaclfixes

#%patch79 -p1 -b .nogypv8sh

# %patch80 -p1 -b .nostamp

#%patch81 -p1 -b .nopnacl

#%patch82 -p1 -b .gcc48

#%patch83 -p1 -b .system-opus 

#%patch84 -p1 -b .system-png    

#%%patch85 -p1 -b .speexfix

#%patch86 -p1 -b .system-vpx

#%patch87 -p1 -b .system-libyuv

#%%patch1000 -p1 -b .nogn

%patch1001 -p1 -b .opus

# icu.pc to icu-i18n,pc
sed -i 's|icu)|icu-i18n)|g' build/linux/system.gyp

# Scrape out incorrect optflags and hack in the correct ones
%if 0%{?sharedbuild}
PARSED_OPT_FLAGS=`echo \'$RPM_OPT_FLAGS -DUSE_SYSTEM_LIBEVENT -fPIC -fno-strict-aliasing -fno-ipa-cp -Wno-error=unused-but-set-variable -Wno-error=c++0x-compat -Wno-error=uninitialized -Wno-error=deprecated-declarations %{?narrowingflag} -Wno-error=unused-local-typedefs -Wno-unused-local-typedefs -Wno-error=int-to-pointer-cast \' | sed "s/ /',/g" | sed "s/',/', '/g"`
%else
PARSED_OPT_FLAGS=`echo \'$RPM_OPT_FLAGS -DUSE_SYSTEM_LIBEVENT -fno-strict-aliasing -fno-ipa-cp -Wno-error=unused-but-set-variable -Wno-error=c++0x-compat -Wno-error=uninitialized -Wno-error=deprecated-declarations %{?narrowingflag} -Wno-error=unused-local-typedefs -Wno-unused-local-typedefs -Wno-error=int-to-pointer-cast \' | sed "s/ /',/g" | sed "s/',/', '/g"`
%endif
PARSED_OPT_FLAGS=`echo \'$RPM_OPT_FLAGS -DGLIB_COMPILATION -DUSE_SYSTEM_LIBEVENT -DUSE_MMX -DUSE_SSE -DUSE_SSE2 -fvisibility=hidden -fPIC -fno-strict-aliasing -fno-ipa-cp -Wno-error=unused-but-set-variable -Wno-error=c++0x-compat -Wno-error=uninitialized -Wno-error=int-to-pointer-cast -Wno-error -I/usr/include/ffmpeg/ -lgthread-2.0 \' | sed "s/ /',/g" | sed "s/',/', '/g"`
for i in build/common.gypi; do
        sed -i "s|'-march=pentium4',||g" $i
        sed -i "s|'-mfpmath=sse',||g" $i
        sed -i "s|'-O<(debug_optimize)',||g" $i
        sed -i "s|'-m32',||g" $i
        sed -i "s|'-fno-exceptions',|$PARSED_OPT_FLAGS|g" $i
done

# Also, set the sandbox paths correctly.
# We set use_system_ffmpeg so chromium doesn't try to build ffmpeg source that we strip out

./build/gyp_chromium -f make build/all.gyp \
			       --depth . \
                               -Dlinux_sandbox_path=%{_libdir}/chromium-browser/chrome-sandbox \
			       -Dlinux_sandbox_chrome_path=%{_libdir}/chromium-browser/chromium-browser \
%ifarch x86_64
                               -Dtarget_arch=x64 \
%endif
                               -Dpython_ver=2.7 \
                               -Dsystem_libdir=%{_lib} \
			                   -Dlinux_use_gold_binary=0 \
                               -Dlinux_use_gold_flags=0 \
                               -Duse_system_flac=1 \
                               -Duse_system_speex=1 \
		               -Duse_system_libpng=1 \
                               -Duse_system_harfbuzz=1 \
		 	       -Duse_system_bzip2=1 \
			       -Duse_system_zlib=1 \
			       -Duse_system_libjpeg=1 \
                               -Duse_system_libxslt=1 \
                               -Duse_system_libsrtp=1 \
                               -Duse_system_libxml=1 \
                               -Duse_system_libyuv=1 \
		               -Duse_system_ffmpeg=1 \
                               -Duse_system_vpx=1 \
                               -Duse_system_opus=1 \
		               -Duse_system_expat=1 \
		               -Duse_system_libmtp=1 \
		               -Duse_system_yasm=1 \
		               -Duse_system_libexif=1 \
		               -Duse_system_protobuf=1 \
                               -Duse_system_nspr=1 \
                               -Duse_system_sqlite=0 \
%if 0%{?nss}
                               -Duse_system_ssl=1 \
%endif
                               -Duse_system_libevent=1 \
%if 0%{?selinux}
                               -Dselinux=1 \
%endif
%if 0%{?sharedbuild}
                               -Dlibrary=shared_library \
                               -Drelease_extra_cflags=-fPIC \
%endif
                               -Dproprietary_codecs=1 \
                               -Ddisable_nacl=1 \
							   -Ddisable_glibc=1 \
							   -Ddisable_newlib_untar=1 \
							   -Ddisable_pnacl_untar=1 \
							   -Dlinux_link_gsettings=1 \
							   -Dlinux_link_libpci=1 \
							   -Dlinux_link_libspeechd=1 \
							   -Dusb_ids_path=/usr/share/hwdata/usb.ids \
                               -Djavascript_engine=v8

%build
%if 0%{?selinux}
%if 0%{?noisy}
make -r %{?_smp_mflags} chrome BUILDTYPE=Release V=1
%else
make -r %{?_smp_mflags} chrome BUILDTYPE=Release
%endif
%else
%if 0%{?noisy}
make -r %{?_smp_mflags} chrome chrome_sandbox BUILDTYPE=Release V=1
# make -r %{?_smp_mflags} chrome chrome_sandbox BUILDTYPE=Release V=1
%else
make -r %{?_smp_mflags} chrome chrome_sandbox BUILDTYPE=Release
%endif
%endif

# If we're building sandbox without SELINUX, add "chrome_sandbox" here.
# %if 0%{?selinux}
# %if 0%{?noisy}
# ../../depot_tools/hammer --mode=Release --verbose chrome
# %else
# ../../depot_tools/hammer --mode=Release chrome
# %endif
# %else
# %if 0%{?noisy}
# ../../depot_tools/hammer --mode=Release --verbose chrome chrome_sandbox
# %else
# ../../depot_tools/hammer --mode=Release chrome chrome_sandbox
# %endif
# %endif

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
sed -i 's/@@__LIBRARY_PATH__@@/%{_lib}/g' %{SOURCE2}
cp -a %{SOURCE2} %{buildroot}%{_bindir}/chromium-browser
export BUILDTARGET=`cat /etc/momonga-release`
sed -i "s|@@BUILDTARGET@@|$BUILDTARGET|g" %{buildroot}%{_bindir}/chromium-browser
# x86_64 capable systems need this
sed -i "s|/usr/lib/chromium|%{_libdir}/chromium|g" %{buildroot}%{_bindir}/chromium-browser
mkdir -p %{buildroot}%{_libdir}/chromium-browser/
mkdir -p %{buildroot}%{_mandir}/man1/
pushd out/Release
cp -a *.pak locales resources %{buildroot}%{_libdir}/chromium-browser/
cp -a icudtl.dat %{buildroot}%{_libdir}/chromium-browser/
# nacl
# cp -a nacl_irt_*.nexe nacl_helper* libppGoogleNaClPluginChrome.so %{buildroot}%{_libdir}/chromium-browser/
#chmod -x %{buildroot}%{_libdir}/chromium-browser/nacl_irt_*.nexe %{buildroot}%{_libdir}/chromium-browser/nacl_helper_bootstrap*
cp -a chrome %{buildroot}%{_libdir}/chromium-browser/chromium-browser
%if 0%{?sharedbuild}
cp -a lib.target/lib*.so %{buildroot}%{_libdir}/chromium-browser/
cp -a lib.host/lib*.so %{buildroot}%{_libdir}/chromium-browser/
%endif
# If we're building without SELINUX support, uncomment this line.
%if 0%{?selinux}
# Do nothing. Sandboxing is in the selinux policy and core binary.
%else
cp -a chrome_sandbox %{buildroot}%{_libdir}/chromium-browser/chrome-sandbox
%endif
cp -a chrome.1 %{buildroot}%{_mandir}/man1/chrome.1
cp -a chrome.1 %{buildroot}%{_mandir}/man1/chromium-browser.1
mkdir -p %{buildroot}%{_libdir}/chromium-browser/plugins/
popd

mkdir -p %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/
cp -a %{SOURCE9} %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/chromium-browser.svg
# cp -a chrome/app/theme/chromium/product_logo_48.png %{buildroot}%{_datadir}/pixmaps/chromium-browser.png

mkdir -p %{buildroot}%{_datadir}/applications/
desktop-file-install --vendor= --dir %{buildroot}%{_datadir}/applications %{SOURCE3}

mkdir -p %{buildroot}%{_datadir}/gnome-control-center/default-apps/
cp -a %{SOURCE5} %{buildroot}%{_datadir}/gnome-control-center/default-apps/

# Make the dirtree for managed policies
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/policies/managed/

# Install the master_preferences file
mkdir -p %{buildroot}%{_sysconfdir}/%{name}-browser/
install -m 0644 %{SOURCE6} %{buildroot}%{_sysconfdir}/%{name}-browser/default

# This enables HTML5 video if you have ffmpeg installed, you naughty naughty user.
pushd %{buildroot}%{_libdir}/chromium-browser
touch %{buildroot}%{_libdir}/libavcodec.so.52
ln -s %{_libdir}/libavcodec.so.52 libavcodec.so.52
touch %{buildroot}%{_libdir}/libavformat.so.52
ln -s %{_libdir}/libavformat.so.52 libavformat.so.52
touch %{buildroot}%{_libdir}/libavutil.so.50
ln -s %{_libdir}/libavutil.so.50 libavutil.so.50
popd

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc chromium-daily-tarball.sh
%{_bindir}/chromium-browser
#%%{_libdir}/chromium-browser/chrome.pak
#%%{_libdir}/chromium-browser/chrome_remote_desktop.pak
%{_libdir}/chromium-browser/icudtl.dat
%{_libdir}/chromium-browser/chrome_100_percent.pak
%{_libdir}/chromium-browser/chromium-browser
# Uncomment this line if building without SELINUX
%if 0%{?selinux}
# Do nothing. Sandboxing is in the selinux policy and core binary.
%else
# These unique permissions are intentional and necessary for the sandboxing
%attr(4555, root, root) %{_libdir}/chromium-browser/chrome-sandbox
%endif
%{_libdir}/chromium-browser/locales/
%{_libdir}/chromium-browser/plugins/
%{_libdir}/chromium-browser/resources/
%{_libdir}/chromium-browser/content_resources.pak
%{_libdir}/chromium-browser/resources.pak
%{_libdir}/chromium-browser/keyboard_resources.pak
#%{_libdir}/chromium-browser/theme_resources_standard.pak
#%{_libdir}/chromium-browser/ui_resources_standard.pak
# %%{_libdir}/chromium-browser/themes/
%{_mandir}/man1/chrom*
#%{_datadir}/pixmaps/chromium-browser.png
%{_datadir}/icons/hicolor/scalable/apps/chromium-browser.svg
%{_datadir}/applications/*.desktop
%{_datadir}/gnome-control-center/default-apps/chromium-browser.xml
%{_sysconfdir}/chromium-browser/default

%files libs
%defattr(-,root,root,-)
%dir %{_libdir}/chromium-browser/
# These are dummy symlinks. You'll have to install ffmpeg-libs to get them for real.
%{_libdir}/chromium-browser/libavcodec.so.52
%{_libdir}/chromium-browser/libavutil.so.50
%{_libdir}/chromium-browser/libavformat.so.52
%if 0%{?sharedbuild}
# These are real.
%{_libdir}/chromium-browser/liballocator.so
%{_libdir}/chromium-browser/libapp_base.so
%{_libdir}/chromium-browser/libappcache.so
%{_libdir}/chromium-browser/libbase.so
%{_libdir}/chromium-browser/libbase_i18n.so
%{_libdir}/chromium-browser/libbase_static.so
%{_libdir}/chromium-browser/libblob.so
%{_libdir}/chromium-browser/libbrowser.so
%{_libdir}/chromium-browser/libcacheinvalidation.so
%{_libdir}/chromium-browser/libcacheinvalidation_proto_cpp.so
#%{_libdir}/chromium-browser/libchrome_gpu.so
%{_libdir}/chromium-browser/libchromoting_base.so
%{_libdir}/chromium-browser/libchromoting_client.so
%{_libdir}/chromium-browser/libchromoting_host.so
%{_libdir}/chromium-browser/libchromoting_jingle_glue.so
%{_libdir}/chromium-browser/libchromoting_protocol.so
%{_libdir}/chromium-browser/libchromotocol_proto_lib.so
%{_libdir}/chromium-browser/libcld.so
%{_libdir}/chromium-browser/libcommon.so
%{_libdir}/chromium-browser/libcommon_constants.so
%{_libdir}/chromium-browser/libcommon_net.so
%{_libdir}/chromium-browser/libcontent_browser.so
%{_libdir}/chromium-browser/libcontent_common.so
%{_libdir}/chromium-browser/libcontent_gpu.so
%{_libdir}/chromium-browser/libcontent_plugin.so
%{_libdir}/chromium-browser/libcontent_ppapi_plugin.so
%{_libdir}/chromium-browser/libcontent_renderer.so
%{_libdir}/chromium-browser/libcontent_worker.so
%{_libdir}/chromium-browser/libcpu_features.so
%{_libdir}/chromium-browser/libcrcrypto.so
%{_libdir}/chromium-browser/libdatabase.so
%{_libdir}/chromium-browser/libdebugger.so
%{_libdir}/chromium-browser/libdefault_plugin.so
%{_libdir}/chromium-browser/libdiffer_block.so
%{_libdir}/chromium-browser/libdiffer_block_sse2.so
%{_libdir}/chromium-browser/libdynamic_annotations.so
%{_libdir}/chromium-browser/libfileapi.so
# Note: This is a dummy lib. There isn't any ffmpeg code in here.
%{_libdir}/chromium-browser/libffmpeg.so
%{_libdir}/chromium-browser/libflac.so
#%{_libdir}/chromium-browser/libgfx.so
%{_libdir}/chromium-browser/libgl.so
%{_libdir}/chromium-browser/libglue.so
%{_libdir}/chromium-browser/libgoogleurl.so
%{_libdir}/chromium-browser/libgtest.so
%{_libdir}/chromium-browser/libharfbuzz.so
#%{_libdir}/chromium-browser/libharfbuzz_interface.so
#%{_libdir}/chromium-browser/libhttp_listen_socket.so
%{_libdir}/chromium-browser/libhttp_server.so
%{_libdir}/chromium-browser/libhunspell.so
%{_libdir}/chromium-browser/libiccjpeg.so
%{_libdir}/chromium-browser/libicudata.so
%{_libdir}/chromium-browser/libicui18n.so
%{_libdir}/chromium-browser/libicuuc.so
%{_libdir}/chromium-browser/libin_memory_url_index_cache_proto_cpp.so
%{_libdir}/chromium-browser/libipc.so
%{_libdir}/chromium-browser/libil.so
%{_libdir}/chromium-browser/libinstaller_util.so
%{_libdir}/chromium-browser/libjingle.so
%{_libdir}/chromium-browser/libjingle_p2p.so
%{_libdir}/chromium-browser/libjingle_glue.so
%{_libdir}/chromium-browser/libmedia.so
%{_libdir}/chromium-browser/libmodp_b64.so
# This isn't a shared lib anymore. A shame.
# %{_libdir}/chromium-browser/libnacl.so
%{_libdir}/chromium-browser/libnet.so
%{_libdir}/chromium-browser/libnet_base.so
%{_libdir}/chromium-browser/libnotifier.so
%{_libdir}/chromium-browser/libomx_wrapper.so
%{_libdir}/chromium-browser/libots.so
#%{_libdir}/chromium-browser/libpcre.so
#%{_libdir}/chromium-browser/libplugin.so
#%{_libdir}/chromium-browser/libppapi_plugin.so
%{_libdir}/chromium-browser/libpolicy.so
%{_libdir}/chromium-browser/libprinting.so
%{_libdir}/chromium-browser/libprofile_import.so
%{_libdir}/chromium-browser/libprotobuf_full_do_not_use.so
%{_libdir}/chromium-browser/libprotobuf_lite.so
%{_libdir}/chromium-browser/libquota.so
%{_libdir}/chromium-browser/librenderer.so
%{_libdir}/chromium-browser/libsandbox.so
%{_libdir}/chromium-browser/libsdch.so
%{_libdir}/chromium-browser/libservice.so
%{_libdir}/chromium-browser/libskia.so
%{_libdir}/chromium-browser/libskia_opts.so
#%{_libdir}/chromium-browser/libspeex.so
%if 0%{?sqlite}
# Do nothing. We're using system-y goodness.
%else
%{_libdir}/chromium-browser/libsqlite3.so
%endif
%if 0%{?system_libsrtp}
# Do nothing
%else
%{_libdir}/chromium-browser/libsrtp.so
%endif
%if 0%{?nss}
# Do nothing. We're using system-y goodness.
%else
%{_libdir}/chromium-browser/libssl.so
#%{_libdir}/chromium-browser/libssl_host_info.so
%endif
%{_libdir}/chromium-browser/libsurface.so
%{_libdir}/chromium-browser/libsymbolize.so
%{_libdir}/chromium-browser/libsync.so
%{_libdir}/chromium-browser/libsyncapi.so
%{_libdir}/chromium-browser/libsync_notifier.so
%{_libdir}/chromium-browser/libsync_proto_cpp.so
%{_libdir}/chromium-browser/libtess.so
%{_libdir}/chromium-browser/libtrace_proto_lib.so
%{_libdir}/chromium-browser/libundoview.so
%{_libdir}/chromium-browser/libui_base.so
%{_libdir}/chromium-browser/libui_gfx.so
%{_libdir}/chromium-browser/libutility.so
%{_libdir}/chromium-browser/libv8_base.so
%{_libdir}/chromium-browser/libv8_nosnapshot.so
%{_libdir}/chromium-browser/libv8_snapshot.so
%{_libdir}/chromium-browser/libwebcore_bindings.so
%{_libdir}/chromium-browser/libwebcore_html.so
%{_libdir}/chromium-browser/libwebcore_platform.so
%{_libdir}/chromium-browser/libwebcore_remaining.so
%{_libdir}/chromium-browser/libwebcore_rendering.so
%{_libdir}/chromium-browser/libwebcore_svg.so
%{_libdir}/chromium-browser/libwebkit.so
%{_libdir}/chromium-browser/libwebkit_gpu.so
%{_libdir}/chromium-browser/libwebkit_user_agent.so
#%{_libdir}/chromium-browser/libwebp.so
%{_libdir}/chromium-browser/libwebp_dec.so
%{_libdir}/chromium-browser/libwebp_enc.so
#%{_libdir}/chromium-browser/libworker.so
%{_libdir}/chromium-browser/libwtf.so
%{_libdir}/chromium-browser/libxdg_mime.so
%{_libdir}/chromium-browser/libyarr.so
%{_libdir}/chromium-browser/libyuv_convert.so
%{_libdir}/chromium-browser/libyuv_convert_sse2.so
%endif
%exclude %{_libdir}/libavcodec.so.52
%exclude %{_libdir}/libavutil.so.50
%exclude %{_libdir}/libavformat.so.52

%changelog
* Sat Jun 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (36.0.1985.98-1m)
- update 36.0.1985.98

* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (36.0.1985.67-1m)
- update 36.0.1985.67

* Wed Mar 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (33.0.1750.149-1m)
- update 33.0.1750.149

* Fri Feb 14 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (33.0.1750.91-1m)
- update 33.0.1750.91
- auto detect PepperFlash(install chromium-pepper-flash)

* Mon Feb 10 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (33.0.1750.70-1m)
- update 33.0.1750.70

* Mon Dec 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (33.0.1725.0-1m)
- update 33.0.1725.0

* Sun Nov 17 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (31.0.1650.57-1m)
- update 31.0.1650.57

* Sat Nov 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (31.0.1650.39-1m)
- update 31.0.1650.39

* Fri Oct 04 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (31.0.1650.8-1m)
- update 31.0.1650.8

* Mon Sep 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (30.0.1599.52-1m)
- update 30.0.1599.52

* Wed Sep 04 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (30.0.1599.26-1m)
- update 30.0.1599.26

* Wed Aug 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (30.0.1599.12-1m)
- update 30.0.1599.12

* Mon Jul 22 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (29.0.1547.27-1m)
- update source

* Sun Jun 16 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (29.0.1539.0-1m)
- update source

* Mon Jun  3 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (29.0.1522.6-1m)
- update chromium29 branch

* Sun May 26 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (29.0.1518.2-1m)
- update chromium29 branch

* Mon Aug  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (20.0.1132.59-2m)
- fix theme broken

* Sun Aug  5 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (20.0.1132.59-1m)
- update chromium20

* Sun Apr 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (17.0.963.792-1m)
- update chromium17

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (15.0.874.121-3m)
- build fix

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15.0.874.121-2m)
- rebuild against libvpx-1.0.0

* Thu Dec  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (15.0.874.121-1m)
- update Chromium15

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (14.0.835.203-1m)
- update Chromium14
-- static link binary only,,,

* Sun Aug 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (12.0.742.130-3m)
- fix build failure with icu-4.6
- fix build failure with linux-3.x
- fix build failure with cups 1.5.0

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (12.0.742.130-2m)
- rebuild against icu-4.6

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.130-1m)
- update 12.0.742.130

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.120-1m)
- update 12.0.742.120

* Thu Jun 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.113-1m)
- update 12.0.742.113
- [SECURITY]    CVE-2011-2345 CVE-2011-2346 CVE-2011-2347
- CVE-2011-2348 CVE-2011-2349 CVE-2011-2350 CVE-2011-2351

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.105-1m)
- update 12.0.742.105

* Sun Jun 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (12.0.742.91-2m)
- enable to build with glibc-2.14

* Mon Jun  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.91-1m)
- update 12.0.742.91

* Sun Jun  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.77-1m)
- update 12.0.742.77

* Wed May 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.68-1m)
- update 12.0.742.68

* Tue May 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.59-1m)
- update 12.0.742.59
- fix build error

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.56-1m)
- update 12.0.742.56

* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.0.742.46-1m)
- update 12.0.742.46

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0.648.205-4m)
- rebuild against perl-5.14.0-0.2.1m

* Wed Apr 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.0.648.205-3m)
- update gcc46.patch to enable build

* Wed Apr 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.0.648.205-2m)
- apply gcc46.patch
- I can not build this package yet, fix me

* Sat Apr 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.0.648.205-1m)
- update 10.0.648.205
- [SECURITY] CVE-2011-1300 CVE-2011-1301 CVE-2011-1302

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.0.648.204-2m)
- rebuild for new GCC 4.6

* Sun Mar 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.0.648.204-1m)
- update 10.0.648.204

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0.648.201-2m)
- rebuild against libevent-2.0.10

* Fri Mar 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.0.648.201-1m)
- update 10.0.648.201

* Sun Mar 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.0.648.134-1m)
- update 10.0.648.134

* Tue Mar  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.0.648.124-1m)
- update 10.0.648.124

* Mon Feb 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.0.648.105-1m)
- update 10.0.648.105

* Mon Feb 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.597.102-1m)
- update 9.0.597.102
- fixed any Security Issue

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.597.94-1m)
- update 9.0.597.94
- fixed any Security Issue

* Fri Feb  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.597.88-1m)
- update 9.0.597.88

* Tue Feb  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.597.84-1m)
- update 9.0.597.84

* Sat Jan 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.597.78-1m)
- update 9.0.597.78

* Sun Jan 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.597.69-2m)
- use shared library

* Sun Jan 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.597.69-1m)
- update 9.0.597.69
-- but static build only

* Wed Jan  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.597.54-1m)
- update 9.0.597.54
-- but static build only

* Thu Dec  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.342-1m)
- update 8.0.552.342

* Mon Nov 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.328-1m)
- update 8.0.552.328
- add gcc-4.5 patch 

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.552.327-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.327-1m)
- update 8.0.552.327

* Sun Nov 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.321-1m)
- update 8.0.552.321

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.205-1m)
- update 8.0.552.205

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.104-1m)
- update 8.0.552.104

* Sun Nov  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.48-1m)
- update 8.0.552.48

* Wed Nov  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.40-1m)
- update 8.0.552.40

* Sun Oct 31 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.25-1m)
- update 8.0.552.25

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.18-1m)
- update 8.0.552.18

* Thu Oct 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.11-1m)
- update 8.0.552.11
- add system_libsrtp flag

* Wed Oct 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.7-1m)
- update 8.0.552.7

* Thu Oct 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.1-1m)
- update 8.0.552.1
- build shared library

* Wed Oct 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.552.0-1m)
- update 8.0.552.0
-- static link binary...

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.0.487.0-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.0.487.0-1m)
- update 6.0.487
- Obsoletes v8, v8-devel

* Tue Jun  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0.417.0-2m)
- fix build on i686, %%global sharedbuild 1

* Thu Jun  1 2010 Masanobu Sato  <satoshiga@momonga-linux.org>
- (6.0.417.0-1m)
- update and sync to Fedora

* Fri May 14 2010 Masanobu Sato  <satoshiga@momonga-linux.org>
- (6.0.399.0-1m)
- update and sync to Fedora

* Fri May 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.220.0-5m)
- rebuild against icu-4.2.1

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.220.0-4m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.220.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 14 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.0.220.0-2m)
- revised 64bit-plugin-path.patch

* Wed Oct  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.220.0-1m)
- sync

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.212.0-1m)
- change release

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.212.0-0.1.20090916svn26424.1m)
- initial build on Momonga
-- http://spot.fedorapeople.org/chromium/F12/

* Wed Sep 16 2009 Tom "spot" Callaway <tcallawa@redhat.com> 4.0.212.0-0.1.20090916svn26424
- 20090916svn26424
- revert http://src.chromium.org/viewvc/chrome/trunk/src/chrome/common/sqlite_utils.cc?r1=24321&r2=25633
  to stop crashes when typing in url bar

* Thu Sep 10 2009 Tom "spot" Callaway <tcallawa@redhat.com> 4.0.208.0-0.1.20090910svn25958
- 20090910svn25958

* Wed Sep 9 2009 Tom "spot" Callaway <tcallawa@redhat.com> 4.0.208.0-0.1.20090909svn25824
- 20090909svn25824
- drop hardcoded Requires on bug-buddy (fixes issue where it is being obsoleted by abrt in rawhide)
- disable webkit deopt, flash bug is fixed now
- use system libicu

* Thu Aug 27 2009 Tom "spot" Callaway <tcallawa@redhat.com> 4.0.204.0-0.1.20090827svn24640
- 20090827svn24640

* Tue Aug 25 2009 Tom "spot" Callaway <tcallawa@redhat.com> 4.0.203.0-0.1.20090824svn24148
- 20090824svn24148
- find proper plugin dir on x86_64
- pass --enable-user-scripts (instead of old --enable-greasemonkey)

* Tue Aug 18 2009 Tom "spot" Callaway <tcallawa@redhat.com> 4.0.202.0-0.1.20090818svn23628
- 20090818svn23628

* Fri Aug 14 2009 Tom "spot" Callaway <tcallawa@redhat.com> 4.0.202.0-0.1.20090814svn23460
- 20090814svn23460

* Wed Aug 12 2009 Tom "spot" Callaway <tcallawa@redhat.com> 4.0.202.0-0.1.20090812svn23201
- Bump to 4.0.202 (we're not tracking 3.0, no one can tell me exactly how to manage that)

* Mon Aug 10 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.198.0-0.1.20090810svn22925
- 20090810svn22925

* Fri Aug  7 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.198.0-0.1.20090807svn22807
- 20090807svn22807

* Wed Aug  5 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.198.0-0.1.20090805svn22496
- 20090805svn22496

* Mon Aug  3 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.197.0-0.1.20090803svn22262
- 20090803svn22262

* Fri Jul 31 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.197.0-0.1.20090731svn22188
- 20090731svn22188

* Thu Jul 30 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.197.0-0.1.20090730svn22105
- 20090730svn22105

* Mon Jul 27 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.196.0-0.1.20090727svn21648
- 20090727svn21648

* Fri Jul 24 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.196.0-0.1.20090724svn21567
- 20090724svn21567
- drop ffmpeg binaries (only code remaining is headers, doesn't infringe patents)
- package up manpage

* Mon Jul 20 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.195.0-0.1.20090720svn21073
- 20090720svn21073

* Thu Jul 16 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.195.0-0.1.20090716svn20889
- 20090716svn20889

* Wed Jul 15 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.195.0-0.1.20090715svn20726
- 20090715svn20726

* Mon Jul 13 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.194.0-0.1.20090713svn20473
- 20090713svn20473

* Sat Jul 11 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.194.0-0.1.20090711svn20464
- 20090711svn20464
- fix sandboxing up to match code changes (no longer need to be read-only, doesn't need /var/run/chrome-sandbox)

* Wed Jul  8 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.193.0-0.1.20090708svn20141
- 20090708svn20141
- support LinuxZygote sandboxing

* Sat Jul  4 2009 Tom "spot" Callaway <tcallawa@redhat.com> 3.0.192.0-0.1.20090704svn19929
- 20090704svn19929
- hack in correct optflags

* Sun Jun 28 2009 Tom "spot" Callaway <tcallawa@redhat.com> 
- 20090628svn19474

* Fri Jun 26 2009 Tom "spot" Callaway <tcallawa@redhat.com>
- 20090626svn19370

* Thu Jun 25 2009 Tom "spot" Callaway <tcallawa@redhat.com> 
- 3.0.191.0 20090625svn19237

* Thu Jun 18 2009 Tom "spot" Callaway <tcallawa@redhat.com>
- 3.0.190.0 20090618svn18706

* Mon Jun 8 2009 Tom "spot" Callaway <tcallawa@redhat.com>
- 20090608svn17870

* Sat Jun 6 2009 Tom "spot" Callaway <tcallawa@redhat.com>
- 20090606svn17834
