%global momorel 5

Name:           remmina
Version:        0.8.1
Release:        %{momorel}m%{?dist}
Summary:        Remote Desktop Client

Group:          Applications/Internet
License:        GPLv2+ and MIT
URL:            http://remmina.sourceforge.net
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  intltool, gtk2-devel, libvncserver-devel
BuildRequires:  libjpeg-devel, gnutls-devel, libssh-devel 
#BuildRequires:  avahi-ui-devel, vte-devel, unique-devel
BuildRequires:  avahi, vte028-devel, unique-devel
BuildRequires:  gettext, desktop-file-utils      

Requires:  rdesktop, xorg-x11-server-Xephyr

Provides: grdc = %{version}
Obsoletes: grdc < 0.6.1

%description
Remmina is a remote desktop client written in GTK+, aiming to be useful 
for system administrators and travellers, who need to work with lots
of remote computers in front of either large monitors or tiny netbooks.

Remmina supports multiple network protocols in an integrated and consistant
 user interface. Currently RDP, VNC, XDMCP and SSH are supported.


%prep
%setup -q


%build
%configure --enable-vnc=dl
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

desktop-file-install --vendor="" --delete-original \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications        \
  $RPM_BUILD_ROOT/%{_datadir}/applications/%{name}.desktop

%find_lang %{name}

# remove devel staff files
rm -rf %{buildroot}%{_includedir}

%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README 
%{_bindir}/%{name}
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/%{name}/

%changelog
* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-5m)
- fix BuildRequires; use vte028

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Mon Jul 19 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.4-1m)
- import from Fedora

* Tue Mar 16 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.7.4-2
- Add patch to fix DSO issue

* Sat Feb 27 2010 Damien Durand <splinux@fedoraproject.org> 0.7.4-1
- Update to 0.7.4
- Fix License tag

* Sun Feb 14 2010 Damien Durand <splinux@fedoraproject.org> 0.7.3-1
- Upstream release
- Add rdesktop, xorg-x11-server-Xephyr in Requires
- Add grdc in Provides/Obsoletes
- Add --enable-vnc=dl in %configure
- Remove unneeded README.LibVNCServer
- Fix "icons/hicolor" path

* Thu Jan 07 2010 Damien Durand <splinux@fedoraproject.org> 0.7.2-2
- Fix Summary
- Split BuildRequires

* Thu Jan 07 2010 Damien Durand <splinux@fedoraproject.org> 0.7.2-1
- Initial release
