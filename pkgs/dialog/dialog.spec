%global         momorel 1

Summary:        A utility for creating TTY dialog boxes.
%define         ver 1.2
%define         datever 20140219
Name:           dialog
Version:        %{ver}.%{datever}
Release:        %{momorel}m%{?dist}
License:        GPL
Group:          Applications/System
URL:            http://invisible-island.net/dialog/
Source0:        ftp://invisible-island.net/pub/%{name}/%{name}-%{ver}-%{datever}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ncurses-devel >= 5.7-5m

%description
Dialog is a utility that allows you to show dialog boxes (containing
questions or messages) in TTY (text mode) interfaces.  Dialog is called
from within a shell script.  The following dialog boxes are implemented:
yes/no, menu, input, message, text, info, checklist, radiolist, and
gauge.  

Install dialog if you would like to create TTY dialog boxes.

%prep
%setup -q -n dialog-%{ver}-%{datever}

%build
%configure --enable-nls --with-ncursesw

%make

%install
rm -rf %{buildroot}
fgrep -l -r perl samples|xargs rm -f

%makeinstall

find %{buildroot} -name '*.a' | xargs rm -f

%find_lang %name

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING dialog.lsm README samples
%{_prefix}/bin/dialog
%{_mandir}/man1/dialog.*
#%%{_datadir}/locale/*/*/*

%changelog
* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.20140219-1m)
- update to 1.2-20140219

* Sun Sep 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.20130928-1m)
- update to 1.2-20130928

* Sun Sep 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.20130902-1m)
- update to 1.2-20130902

* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.20130523-1m)
- update to 1.2-20130523

* Sun Jan 13 2013 NARITA Koichi <pulsarpmomonga-linux.org>
- (1.2.20121230-1m)
- update to 1.2-20121230

* Fri Oct 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20120706-1m)
- update to 1.1-20120706

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20120215-1m)
- update to 1.1-20120215

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.20111020-1m)
- update to 1.1-20111020

* Fri Jul 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20110707-1m)
- update to 1.1-20110707

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.20110302-2m)
- rebuild for new GCC 4.6

* Sat Mar 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20110302-1m)
- update to 1.1-20110302

* Thu Jan 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.20110118-1m)
- update to 1.1-20110118

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.20100428-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.20100428-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20090428-1m)
- update to 1.1-20090428

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.20100119-2m)
- use BuildRequires

* Tue Jan 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20100119-1m)
- update to 1.1-20100119

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.20080819-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NARITA Koichi <pulsar@momonga-linux org>
- (1.1.20080819-3m)
- enable to build with ncurses-5.7-5m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.20080819-2m)
- rebuild against rpm-4.6

* Sun Aug 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20080819-1m)
- update to 1.1-20080819

* Sun Aug  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20080727-1m)
- update to 1.1-20080727

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.20080316-2m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20080316-1m)
- update to 1.1-20080316

* Sat Nov  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20071028-1m)
- update to 1.1-20071028

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20070930-1m)
- update to 1.1-20070930
- do not use %%NoSource macro

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20070704-1m)
- update to 1.1-20070704

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20070604-1m)
- update to 1.1-20070604
- delete unused source and patch

* Mon Mar 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20070325-1m)
- update t0 1.1-20070325

* Mon Mar 06 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.0.20060221-1m)
- update to 1.0-20060221

* Sun Feb 12 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.0.20060126-1m)
- update to 1.0-20060126

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.0.20051207-1m)
- update to 1.0-20051207
- modify patch0

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.20040731-3m)
- enable x86_64.

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.20040731-2m)
- rebuild against ncurses-5.4-1m.

* Sat Aug 21 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.20040731-1m)
  update to 1.0-20040731

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9b-6m)
- rebuild against ncurses 5.3.

* Tue Apr 27 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9b-5m)
- upgrade 0.9b-20040421

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9b-4m)
- revised spec for enabling rpm 4.2.

* Wed Nov 26 2003 Masahiro Takahata <takahata@momonga-linux.org>
- upgrade 0.9b-20031002

* Tue Jan 21 2003 Masahiro Takahata <takahata@momonga-linux.org>
- upgrade 0.9b-20020814

* Wed Nov 15 2000 MATSUDA, Daiki <dyky@df-usa.com>
- corrected %doc files

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Apr  5 2000 Bill Nottingham <notting@redhat.com>
- rebuild against current ncurses/readline

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Thu Jan 20 2000 Bill Nottingham <notting@redhat.com>
- fix loop patch for reading from pipe

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 14)

* Fri Dec 18 1998 Bill Nottingham <notting@redhat.com>
- build for 6.0

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Thu May 7 1998 Michael Maher <mike@redhat.com> 
- Added Sean Reifschneider <jafo@tummy.com> patches for 
  infinite loop problems.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 15 1998 Erik Troan <ewt@redhat.com>
- built against new ncurses

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
