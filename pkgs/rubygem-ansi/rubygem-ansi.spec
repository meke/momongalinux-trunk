# Generated from ansi-1.4.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname ansi

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: ANSI at your fingertips!
Name: rubygem-%{gemname}
Version: 1.4.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubyworks.github.com/ansi
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
The ANSI project is a superlative collection of ANSI escape code related
libraries
enabling ANSI colorization and stylization of console output. Byte for byte
ANSI is the best ANSI code library available for the Ruby programming
language.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/HISTORY.rdoc
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/QED.rdoc
%doc %{geminstdir}/COPYING.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-1m)
- update 1.4.2

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.1-1m)
- update 1.4.1

* Mon Nov  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update 1.4.0

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- update 1.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- Initial package for Momonga Linux
