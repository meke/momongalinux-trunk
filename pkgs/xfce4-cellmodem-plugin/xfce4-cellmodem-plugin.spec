%global momorel 12

%global xfce4ver 4.10.0
%global major 0.0

Name:           xfce4-cellmodem-plugin
Version:        0.0.5
Release:        %{momorel}m%{?dist}
Summary:        A cellmodem plugin for the Xfce panel

Group:          Applications/System
License:        GPL
URL:            http://goodies.xfce.org/projects/panel-plugins/xfce4-cellmodem-plugin
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.gz
NoSource:	0
# From Debian: fix a typo in Makefile.am that breaks linking
# http://git.xfce.org/panel-plugins/xfce4-cellmodem-plugin/commit/?id=404093d6e2f22064166b806711d5cb5d9dd935d2
Patch0:         %{name}-0.0.5-typo-linking.patch
# From Debian: fix underlinking
# http://bugzilla.xfce.org/show_bug.cgi?id=6952
Patch1:         %{name}-0.0.5-explicit-linking-to-libxfcegui4.patch
# contains all translations from 
# http://translations.xfce.org/projects/p/xfce4-cellmodem-plugin/c/master/
# as of 2011-04-08
Patch2:         %{name}-0.0.5-update-translations.patch
# fixes path of the desktop file for xfce4-panel 4.7
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  intltool
BuildRequires:  libusb-devel >= 0.1.12
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  pciutils-devel
BuildRequires:  xfce4-dev-tools >= %{xfce4ver}
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
BuildRequires:  zlib-devel
Requires:       xfce4-panel >= %{xfce4ver}


%description
The cellmodem plugin is a monitoring plugin for cellular modems. It reports 
provider and signal quality for GPRS/UMTS(3G)/HSDPA(3.5G) modem cards. It 
works with (mostly) all cards which support an out-of-band channel for 
monitoring purposes. Current features include:
* Display the current network type (GPRS/UMTS)
* Display the current signal level
* Configure the maximum signal level
* Configure the low and critical signal level
* Asking for PIN if modem needs it
* Quick visual feedback on modem and registration status via LEDs

%prep
%setup -q
%patch0 -p1 -b .typo
%patch1 -p1 -b .explicit-linking
%patch2 -p1 -b .update-translations

# Fix icon in 'Add new panel item' dialog
sed -i 's|Icon=xfce-mouse|Icon=phone|g' panel-plugin/cellmodem.desktop.in.in

%build
# Xfce has its own autotools-running-script thingy, if you use autoreconf
# it'll fall apart horribly
xdt-autogen

%configure --disable-static
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(644,root,root,755)
%doc AUTHORS ChangeLog NEWS README
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/xfce4/panel-plugins/cellmodem.desktop

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.5-12m)
- build against xfce4-4.10.0

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.5-11m)
- import patches from Fedora

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-10m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.5-7m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.5-6m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.5-4m)
- rebuild against xfce4-4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.5-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.5-1m)
- initial momonga package based on PLD
