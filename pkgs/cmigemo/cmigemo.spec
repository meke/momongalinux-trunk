%global         momorel 6
%global         svndate 20070702

%define		skkdicdir	%{_datadir}/skk
%define		skkcoding	EUC-JP

Name:		cmigemo
Version:	1.3c
Release:	0.0.%{svndate}.%{momorel}m%{?dist}
Summary:	C interface of Ruby/Migemo Japanese incremental search tool

Group:		System Environment/Libraries
License:	MIT
URL:		http://www.kaoriya.net/
Source0:	http://www.kaoriya.net/dist/var/cmigemo-%{version}-MIT.tar.bz2
Patch0:		cmigemo-1.3c-MIT-ignore-random-string.patch
Patch1:		cmigemo-1.3c-MIT-dont-escape.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  skk-jisyo

%description
C/Migemo is a C interface of Ruby/Migemo, a Japanese incremental search tool
by Romaji.

%package	devel
Summary:	Development files for cmigemo
Group:		Development/Libraries

Requires:	%{name} = %{version}-%{release}

%description	devel
This package  contains libraries and header files for
developing applications that use cmigemo.

%prep
%setup -q -n %{name}-%{version}-MIT
%patch0 -p1 -b .random
%patch1 -p1 -b .escape

# Change default command for configure
%{__sed} -i.command \
	-e 's|curl|true|' \
	-e 's|nkf|true|' \
	-e 's|install\"|install -p"|' \
	configure

# use iconv instead of nkf
%{__sed} -i.nkf \
	-e 's|^\(FILTER_CP932[ \t][ \t]*=\).*|\1 iconv -f %{skkcoding} -t SJIS|' \
	-e 's|^\(FILTER_EUCJP[ \t][ \t]*=\).*|\1 iconv -f SJIS -t EUC-JP|' \
	compile/config.mk.in

# make cmigemo original data dir
%{__sed} -i.dir \
	-e 's|/share/migemo|/share/cmigemo|' \
	compile/config.mk.in config.mk

# ( don't create unnecessary backup file for document...)
%{__sed} -i \
	-e 's|/usr/local/share/migemo|%{_datadir}/cmigemo|' \
	doc/README_j.txt tools/migemo.vim

# remove unneeded rpath
%{__sed} -i.rpath \
	-e 's|^\(LDFLAGS_MIGEMO[ \t][ \t]*=\).*|\1 |' \
	compile/Make_gcc.mak

# 64 bits libdir
%{__sed} -i.bits \
	-e 's|\$(prefix)/lib|$(prefix)/%{_lib}|' \
	config.mk compile/config.mk.in compile/config_default.mk

# Also install zen2han
%{__sed} -i.han \
	-e 's|^\(.*\)\(han2zen\)\(.*\)$|\1\2\3\n\1zen2han\3|' \
	compile/unix.mak

%{__chmod} 0644 tools/*

%build
%configure

# parallel make unsafe
%{__make} gcc CC="gcc $RPM_OPT_FLAGS"

%{__cat} %{skkdicdir}/SKK-JISYO.L | gzip > dict/SKK-JISYO.L.gz
%{__make} gcc-dict
( cd dict ; %{__make} utf-8 )

%install
%{__rm} -rf $RPM_BUILD_ROOT
%{__make} gcc-install prefix=$RPM_BUILD_ROOT%{_prefix}

# remove unneeded document
%{__rm} -rf $RPM_BUILD_ROOT%{_prefix}/doc/

# make documentation directory
%{__rm} -rf doc_install
%{__mkdir} doc_install
cd doc
for f in *txt ; do \
	iconv -f SJIS -t UTF-8 $f > ../doc_install/$f && \
		touch -r $f ../doc_install/$f || \
		%{__cp} -p $f ../doc_install/$f
done
%{__cp} -p LICENSE ../doc_install/
cd ..

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc doc_install/*
%doc tools/

%{_bindir}/%{name}
%{_libdir}/libmigemo.so.*

%{_datadir}/cmigemo/

%files	devel
%defattr(-,root,root,-)
%{_includedir}/migemo.h
%{_libdir}/libmigemo.so

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3c-0.0.20070702.6m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3c-0.0.20070702.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3c-0.0.20070702.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3c-0.0.20070702.3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3c-0.0.20070702.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-0.0.20070702.1m)
- import from Fedora 11
- momonganize

* Tue Feb 24 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp>
- F-11: Mass rebuild

* Tue Jan 20 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3-0.7.c_MIT
- Also install zen2han (JD 6 comment 976)

* Sat Feb  9 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp>
- Rebuild against gcc43

* Wed Aug 22 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3-0.6.c_MIT.dist.1
- Mass rebuild (buildID or binutils issue)

* Wed Jul 11 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3-0.6.c_MIT
- Re-enable Migemo autocompletion

* Sun May 26 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3-0.5.c_MIT
- Don't make special character escaped.

* Sat May 26 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3-0.4.c_MIT
- Suppress completent for too random string.

* Sun May 20 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3-0.3.c_MIT
- Don't create unnecessary document backup

* Sun May 20 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3-0.2.c_MIT
- 64 bits fix

* Sat May 19 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3-0.1.c_MIT
- Initial packaging.
