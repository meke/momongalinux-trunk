%global momorel 4

Summary: a ruby module for reading and writing zip files
Name: ruby-zip
Version: 0.9.4
Release: %{momorel}m%{?dist}
License: Ruby
Group: Development/Libraries
Source0: http://dl.sourceforge.net/sourceforge/rubyzip/rubyzip-%{version}.tgz 
NoSource: 0
URL: http://rubyzip.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby
BuildArch: noarch

%description
ruby-zip is a ruby module for reading and writing zip files.

%prep
%setup -q -n rubyzip-%{version}

%build
cd test
ruby -I. alltests.rb || exit 0

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{ruby_libdir}/zip
install -p -m 644 lib/zip/*.rb %{buildroot}%{ruby_libdir}/zip

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root)
%doc ChangeLog NEWS README samples
%{ruby_libdir}/zip

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-1m)
- update 0.9.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.8-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.8-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.8-2m)
- %%NoSource -> NoSource

* Wed Jul  6 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.8-1m)
- initial import to Momonga
