%global momorel 8

%global rbname rpm
%global reldir 66904

Summary: An interface to access RPM database for Ruby-2.0
Name: ruby-%{rbname}
Version: 1.3.1
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPLv2+
URL: http://rubyforge.org/projects/ruby-rpm/
Source0: ruby-rpm-%{version}.tar.xz
#Source0: http://rubyforge.org/frs/download.php/%{reldir}/ruby-rpm-%{version}.tgz 
#NoSource: 0

#Patch0: 0001-Remove-constants-which-no-longer-exist-in-4.9.0.patch
#Patch1: 0002-Port-other-things-to-rpm-4.9.patch
#Patch2: 0003-Link-with-librpmbuild.patch

Patch0: 0001-Remove-constants-which-no-longer-exist-in-4.9.0.patch
Patch1: 0002-Port-other-things-to-rpm-4.9.patch
Patch2: 0003-Link-with-librpmbuild.patch
Patch3: 0001-Stop-handling-rpmcliPackagesTotal-rpmShowProgress-no.patch
Patch4: 0002-Use-ruby-st.h-instead-of-bare-st.h.patch
patch5: 0003-Link-against-librpmio.patch

Patch100: ruby-rpm-1.3.1-memleak.patch
Patch101: ruby-rpm-1.2.3-specfile.patch
Patch102: ruby-rpm-1.2.3-specfile-rpm410-compat.patch
Patch103: ruby-rpm-1.3.1-ruby19.patch
Patch104: ruby-rpm-1.3.1-rpmrc_free.patch
Patch1000: ruby-rpm-1.3.1-no_yard.patch
Patch1001: ruby-rpm-1.3.1-cflags.patch
Patch1002: ruby-rpm-1.3.1-rpm410.patch
Patch1003: ruby-rpm-1.3.1-rpm411.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby  >= 1.9.3
BuildRequires: rpm-devel
BuildRequires: popt-devel >= 1.9.1-1m
BuildRequires: libdb-devel

Conflicts: mph-get <= 0.92.6.1
Obsoletes: ruby-rpm-devel

%description
Ruby-rpm is an interface to access RPM database for Ruby-2.0

%prep
%setup -q -n ruby-rpm-%{version}

%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1

%patch100 -p1 -b .memleak
%patch101 -p1 -b .specfile
%patch102 -p1 -b .specfile-compat
#%%patch103 -p1 -b .compat
%patch104 -p1 -b .rpmrc_free

%patch1000 -p1 -b .no_yard
%patch1001 -p1 -b .cflags
#%%patch1002 -p1 -b .rpm410
%patch1003 -p1 -b .rpm411


%build
export RPM_OPT_FLAGS="$RPM_OPT_FLAGS -Wno-error=cpp "
rake compile

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

# install file
mkdir -p %{buildroot}%{ruby_sitearchdir}
cp -r lib/* %{buildroot}%{ruby_sitearchdir}

# install header file
%{__install} -m 0644 ext/rpm/ruby-rpm.h %{buildroot}%{ruby_sitearchdir}

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%check
#rake test

%files
%defattr(-,root,root,-)
%doc README.rdoc COPYING CHANGELOG.rdoc doc
%{ruby_sitearchdir}/rpm.rb
%{ruby_sitearchdir}/rpm.so
%{ruby_sitearchdir}/rpm/
%{ruby_sitearchdir}/ruby-rpm.h

%changelog
* Thu Mar 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-8m)
- add patch

* Thu Dec 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-7m)
- fix resource leak crash. 
-- ruby-rpm broken rpm-4.9/4.10/4.11

* Thu Dec 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-6m)
- add rpm-4.11 patch

* Tue Nov 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-5m)
- update rpm-4.9/4.10 patches.
-- maybe fixed speclint error

* Mon Sep  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-4m)
- update rpm-4.9 patches. from Arch linux
-- maybe support rpm-4.10

* Thu May 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-3m)
- maybe support rpm-4.9
-- https://gitorious.org/ruby-rpm/ruby-rpm/merge_requests/3
- not test yet

* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-2m)
- no run yard

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Mon Sep  5 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-5m)
- modify spec for db-4.8

* Wed Aug 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-4m)
- fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- update 1.3.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-6m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-5m)
- rename tto ruby19-rpm to ruby-rpm

* Thu Jul 29 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-4m)
- fix BuildRequires

* Wed Jul 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-3m)
- rebuild against ruby19-1.9.2-0.992.5m

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-2m)
- rebuild against rpm-4.8.0

* Tue Feb 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4
- drop rpm64, lib64 and ia64 patches

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-30m)
- rebuild against db-4.8.26

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-29m)
- support db-4.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-28m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.3-27m)
- BuildRequires: popt-devel, instead of popt

* Mon Oct 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-26m)
- rebuila against ruby19-1.9.2-0.1-20091012

* Mon Jul 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-25m)
- rebuila against ruby19-1.9.2

* Sat Apr 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-24m)
- BuildRequires: db4-devel >= 4.7.25

* Fri Apr 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-23m)
- rebuild against rpm-4.7
- modify rpm46 patch (Patch200) for db4-4.7.25

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-22m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-21m)
- rebuild against ruby19-1.9.1-rc2

* Tue Jan 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-20m)
- use compat-db45 to harmonize db4 with rpm46

* Sun Jan 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-19m)
- apply Patch201 which adds RPM::Spec#specfile method

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-18m)
- work with rpm44 and rpm46
- apply rpm46 patch
-- this patch includes Patch4 which modifies db4 check codes in extconf.rb
- License: GPLv2+

* Tue Oct 28 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-17m)
- rebuild against ruby19-1.9.1-preview
-- if broke OmoiKondara then "ruby ../tools/OmoiKondara -i ruby19-rpm"

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-16m)
- rebuild against db4-4.7.25-1m

* Mon Oct 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-15m)
- add db-4.7 support 
- rebuild against ruby-1.9.0-5
-- if broke OmoiKondara then "ruby ../tools/OmoiKondara -i ruby19-rpm" 

* Tue Aug 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-13m)
- rebuild against ruby-1.9.0-4
-- if broke OmoiKondara then "ruby ../tools/OmoiKondara -i ruby19-rpm" 

* Sat Jul 26 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-12m)
- rename ruby19-rpm

* Sat Jun 14 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-11m)
- add patch that fixes memory leak

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-10m)
- rebuild against ruby19-1.9.0-10m

* Wed Apr 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-9m)
- no need ruby18.
- add db-4.6 patch 

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-7m)
- %%NoSource -> NoSource

* Tue Dec 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-6m)
- Re: support newer ruby-1.9

* Tue Dec 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-5m)
- support newer ruby-1.9

* Thu Nov  8 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.3-4m)
- modify reldir, md5sum

* Sun Oct 21 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.3-3m)
- rebuild against db4-4.6.21

* Wed Aug 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-2m)
- fix install .so path

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-1m)
- initial commit

