#!/bin/sh

GREEN=$'\e[0;32;1m'
RED=$'\e[0;31;1m'
BLACK=$'\e[0;30m'

function echo_msg() {
  echo 
  echo $GREEN \#\#\#\#\# $@ \#\#\#\#\# $BLACK
}

RUBY_RPM=ruby-rpm-1.3.1
SOURCE=$RUBY_RPM.tar.xz
RUBY=ruby
RUBY_LIB_DIR=`$RUBY -rrbconfig -e 'puts RbConfig::CONFIG["sitearchdir"]'`

rm -rf $RUBY_RPM

# extract and move
echo_msg extrace $SOURCE
tar Jxf $SOURCE
pushd $RUBY_RPM 2>&1 > /dev/null

echo_msg "extra local libs"
tar Jxf ../rake-locallib.tar.xz

sed -i 's/^gem.*//g' Rakefile

# Patched 
for i in `egrep \^Patch ../ruby-rpm.spec | awk '{print $2}'`
do
	echo Patch $i
	patch -p1 < ../$i
done

# build
echo_msg "build libs"
rake compile --libdir vendor/bundler-*/lib  --libdir vendor/rake-compiler-*/lib 

# copy system rpm.so
echo_msg "sudo mv $RUBY_LIB_DIR/rpm.so{,.orig}"
sudo mv $RUBY_LIB_DIR/rpm.so{,.orig}

echo_msg "sudo cp lib/rpm.so $RUBY_LIB_DIR/"
sudo cp lib/rpm.so $RUBY_LIB_DIR/

popd $RUBY_RPM 2>&1 > /dev/null

echo_msg "checking installed library"
ruby <<EOF
begin
require 'rpm'
print "$GREEN Success: ruby-rpm installed $BLACK\n"
rescue LoadError
  print "$RED Error: ruby-rpm not found $BLACK\n"
end
EOF
