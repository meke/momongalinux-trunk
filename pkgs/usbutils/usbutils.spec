%global momorel 1

Summary: Linux USB utilities
Name: usbutils
Version: 006
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://www.linux-usb.org/
Source0: http://www.kernel.org/pub/linux/utils/usb/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: %{name}-%{version}-hwdata.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: hwdata
BuildRequires: autoconf
BuildRequires: libtool
BuildRequires: libusb-devel >= 0.1.8
Obsoletes: usbutils-devel

%description
This package contains an utility for inspecting
devices connected to an USB bus.

%prep
%setup -q
%patch0 -p1 -b .hwdata

autoreconf -fi

%build
%configure --sbindir=%{_sbindir} --datadir=%{_datadir}/hwdata --disable-usbids
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

# usb.ids is shipped in hwdata; nuke and adjust .pc file
sed -i 's|usbids=/usr/share/usb.ids|usbids=/usr/share/hwdata/usb.ids|' %{buildroot}%{_datadir}/pkgconfig/usbutils.pc

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/*
%{_datadir}/pkgconfig/usbutils.pc
%{_mandir}/man1/*
%{_mandir}/man8/*

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (006-1m)
- update to 006

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.87-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.87-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.87-2m)
- full rebuild for mo7 release

* Fri Mar 19 2010 Shigeru Yamzaki <muradaikan@moonga-linux.org>
- (0.87-1m)
- update to 0.87
- modify spec for Source0 URL and new files

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.82-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.82-1m)
- version 0.82
- move lsusb from %%{_sbindir}/ to /sbin/
- add usbutils.pc to main package, should be split to usbutils-devel?

* Wed May 27 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.73-3m)
- libtool build fix

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.73-2m)
- rebuild against rpm-4.6

* Sat Jul 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.73-1m)
- sync with Fedora devel (0.73-2)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.71-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.71-2m)
- %%NoSource -> NoSource

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.71-1m)
- update to 0.71
- import some patches from FC
- delete unused patches
- change URL and Source0 URI

* Thu Feb  3 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.11-4m)
- enable x86_64.

* Sat Nov 27 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.11-3m)
- add 2 patches from Fedora
- usbutils-0214.patch to fix various brokenness
- usbutils-0.11-hidcc.patch for unknown HID Country Code entries in usb.ids

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.11-2m)
- revised spec for rpm 4.2.

* Tue Oct 28 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (0.11-1m)
- Version up

* Sun Jul 21 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (0.9-8m)
- Add Obsoletes: usbutils-devel

* Sun Jul 21 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (0.9-7m)
- m
- usbutils-0.9-hwdata.patch - RawHide (0.9-7)
- devel is obsolete

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.9-6k)
- s/Copyright/License/

* Fri Dec  7 2001 Shingo Akagaki <dora@kondara.org>
- (0.9-4k)
- devel require base

* Thu Dec  6 2001 Shingo Akagaki <dora@kondara.org>
- (0.9-2k)
- k

* Tue Jun 12 2001 Thomas Sailer <t.sailer@alumni.ethz.ch>
- rev 0.8

* Tue Sep 14 1999 Thomas Sailer <sailer@ife.ee.ethz.ch>
- Initial build
