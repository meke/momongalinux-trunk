%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

%global python_ver %(%{__python} -c "import sys ; print sys.version[:3]")
%global python_sitelib  %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")
%global ruby_ver 1.9.3
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global ruby_sitearch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")

## falcon (still) not functional
%define kross_falcon 0
## java needs love
%define kross_java 1
%define kross_ruby 1

Name: kross-interpreters
Version: %{kdever}
Release: %{momorel}m%{?dist}
Summary: Kross interpreters
License: LGPLv2+
Group: Development/Languages
URL: http://developer.kde.org/language-bindings/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}%{?svnrel:svn%{svnrel}}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: kdelibs-devel >= %{version}
%if 0%{?kross_falcon}
BuildRequires: Falcon-devel
%endif
%if 0%{?kross_java}
BuildRequires: java-devel
%endif
%if 0%{?kross_ruby}
BuildRequires: ruby-devel ruby
%endif
BuildRequires: python-devel

%if ! 0%{?kross_falcon}
Obsoletes: kross-falcon
%endif
%if ! 0%{?kross_java}
Obsoletes: kross-java
%endif
%if ! 0%{?kross_ruby}
Obsoletes: kross-ruby
%endif
Obsoletes: php-qt
Obsoletes: php-qt-devel

%description
%{summary}.

%package -n kross-python
Summary:  Kross plugin for python
Group: Development/Languages
Requires: kdelibs >= %{version}
Provides: kross(python) = %{version}-%{release}
Requires: %{name} = %{version}-%{release}

%description -n kross-python
Python plugin for the Kross archtecture in KDE.

%if 0%{?kross_falcon}
%package -n kross-falcon
Summary:  Kross plugin for falcon
Group: Development/Languages
Requires: Falcon
Requires: kdelibs >= %{version}
Provides: kross(falcon) = %{version}-%{release}
Requires: %{name} = %{version}-%{release}

%description -n kross-falcon
Falcon plugin for the Kross archtecture in KDE.
%endif

%if 0%{?kross_java}
%package -n kross-java
Summary:  Kross plugin for java
Group: Development/Languages
Requires: kdelibs >= %{version}
Provides: kross(java) = %{version}-%{release}
Requires: %{name} = %{version}-%{release}

%description -n kross-java
Java plugin for the Kross archtecture in KDE.
%endif

%if 0%{?kross_ruby}
%package -n kross-ruby
Summary:  Kross plugin for ruby
Group: Development/Languages
Requires: ruby
Requires: kdelibs >= %{version}
Provides: kross(ruby) = %{version}-%{release}
Requires: %{name} = %{version}-%{release}

%description -n kross-ruby
Ruby plugin for the Kross architecture in KDE.
%endif

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
%if 0%{?kross_falcon}
   -DENABLE_KROSSFALCON:BOOL=ON \
%else
   -DBUILD_falcon:BOOL=OFF \
%endif
%if 0%{?kross_java}
   -DENABLE_KROSSJAVA:BOOL=ON \
%else
   -DBUILD_java:BOOL=OFF \
%endif
%if 0%{?kross_ruby}
   -DRUBY_EXECUTABLE:FILEPATH=%{_bindir}/ruby \
   -DRUBY_LIBRARY:FILEPATH=%{_libdir}/libruby.so \
   -DRUBY_ARCH_DIR:PATH=%{ruby_sitelib} \
   -DRUBY_SITEARCH_DIR:PATH=%{ruby_sitearch} \
%else
   -DBUILD_ruby:BOOL=OFF \
%endif
    ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
# intentionally left blank (for now)

%files -n kross-python
%{_kde4_libdir}/kde4/krosspython.so

%if 0%{?kross_falcon}
%files -n kross-falcon
%{_kde4_libdir}/kde4/krossfalcon.so
%endif

%if 0%{?kross_java}
%files -n kross-java
%{_kde4_libdir}/kde4/krossjava.so
%{_kde4_libdir}/kde4/kross
%endif

%if 0%{?kross_ruby}
%files -n kross-ruby
%{_kde4_libdir}/kde4/krossruby.so
%endif

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@mom0nga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- import From Fedora for kdebindings-4.7.0

* Tue Jul 26 2011 Jaroslav Reznik <jreznik@redhat.com> 4.7.0-1
- 4.7.0

* Fri Jul 15 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-1
- 4.6.95

* Wed Jul 06 2011 Than Ngo <than@redhat.com> - 4.6.90-1
- first Fedora RPM
