%global         momorel 1
%global         kdever 4.12.97
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         ftpdirver %{version}
%global         sourcedir %{release_dir}/%{name}/%{ftpdirver}/src

Name:           libkgapi
Version:        2.1.1
Release:        %{momorel}m%{?dist}
Summary:        Library to access to Google services
Group:          Development/Libraries
License:        GPLv2+
URL:            http://www.progdan.cz/2012/05/libkgoogle-libkgapi
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kdepimlibs-devel
BuildRequires:  kdelibs-devel
BuildRequires:  qjson-devel
Requires:       kdepimlibs >= %{kdever}

%description
Library to access to Google services, this package is needed by kdepim-runtime
to build akonadi-google resources.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kdepimlibs-devel

%description devel
Libraries and header files for developing applications that use akonadi-google
resources.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc LICENSE README examples/
%{_libdir}/libkgapi2.so.2*

%files devel
%{_kde4_includedir}/LibKGAPI2
%{_kde4_includedir}/libkgapi2
%{_libdir}/libkgapi2.so
%{_libdir}/cmake/LibKGAPI2

%changelog
* Tue Apr  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sun Dec  8 2013 NARITA Koichi <pulsar[momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Dec 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Mon Oct 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Tue Aug 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- import from Fedora

* Wed Jun 20 2012 Rex Dieter <rdieter@fedoraproject.org>
- 0.4.0-5
- -devel: tighten subpkg dep via %%_isa, Req: kdepimlibs-devel
- Parsing token page failed (kde#301240)

* Sun Jun 10 2012 Rex Dieter <rdieter@fedoraproject.org> 0.4.0-4
- -devel: track files closer
- pkgconfig-style deps

* Thu Jun 07 2012 Mario Santagiuliana <fedora@marionline.it> 0.4.0-3
- Update spec file following Gregor Tätzner request:
https://bugzilla.redhat.com/show_bug.cgi?id=817622#c8

* Thu May 31 2012 Mario Santagiuliana <fedora@marionline.it> 0.4.0-2
- Update spec file following Rex Dieter and Kevin Kofler suggestion
- Add obsolete and provide for devel subpkg

* Thu May 31 2012 Mario Santagiuliana <fedora@marionline.it> 0.4.0-1
- Update to new version 0.4.0
- Update to new licence GPLv2+
- Update to new name libkgapi
- Add obsolete and provide libkgoogle

* Wed May 30 2012 Mario Santagiuliana <fedora@marionline.it> 0.3.2-1.20120530gitf18d699
- Update spec comment to new git repository
- Update to new version 0.3.2
- Snapshot f18d699d9ef7ceceff06c2bb72fc00f34811c503

* Mon Apr 30 2012 Mario Santagiuliana <fedora@marionline.it> 0.3.1-1.20120430gitefb3215
- Rename package from akonadi-google to libkgoogle
- Update spec file
- Snapshot efb32159c283168cc2ab1a39e6fa3c8a30fbc941

* Mon Apr 30 2012 Mario Santagiuliana <fedora@marionline.it> 0.3.1-1
- New version 0.3.1

* Thu Apr 01 2012 Mario Santagiuliana <fedora@marionline.it> 0.3-1.20120402git3e0a93e
- New version 0.3
- Update to git snapshot 3e0a93e1b24cd7b6e394cf76d153c428246f9fa9
- Obsolete akonadi-google-tasks
- Fix error in changelog

* Thu Mar 01 2012 Mario Santagiuliana <fedora@marionline.it> 0.2-12.20120301git41cd7c5
- Update to git snapshot 41cd7c5d6e9cfb62875fd21f8a920a235b7a7d9c

* Wed Jan 20 2012 Mario Santagiuliana <fedora@marionline.it> 0.2-11.20120121gitbe021c6
- Update to git snapshot be021c6f12e6804976dcac203a1864686a219c26

* Wed Jan 20 2012 Mario Santagiuliana <fedora@marionline.it> 0.2-10.20120120git11bf6ad
- Update spec file follow comment 1:
https://bugzilla.redhat.com/show_bug.cgi?id=783317#c1
- Update to git snapshot 11bf6ad40dd93eda1f880a99d592009ea3ff47ac
- Include LICENSE

* Thu Jan 19 2012 Mario Santagiuliana <fedora@marionline.it> 0.2-9.20120119git754771b
- Create spec file for Fedora Review
- Source package create from git snapshot 754771b6081b194aedf750fac76a9af2709a5de3

* Wed Nov 16 2011 Dan Vratil <dan@progdan.cz> 0.2-8.1
- Initial SPEC
