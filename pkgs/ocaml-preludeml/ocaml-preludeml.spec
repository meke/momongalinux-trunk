%global momorel 7
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

# There are no official releases, so we just use git dates.
# This software is much more stable than the "0.1" version
# number might indicate.
%global gitdate 20090113

Name:           ocaml-preludeml
Version:        0.1
Release:        0.0.%{gitdate}.%{momorel}m%{?dist}
Summary:        OCaml utility functions

Group:          Development/Libraries
License:        MIT
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# To recreate the source tarball, do:
#   git clone git://github.com/kig/preludeml.git
#   tar --exclude .git -cf /tmp/preludeml-%{gitdate}.tar preludeml
#   bzip2 /tmp/preludeml-%{gitdate}.tar
URL:            http://github.com/kig/preludeml/tree/master
Source0:        preludeml-%{gitdate}.tar.bz2

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-pcre-devel >= 6.2.3-1m
BuildRequires:  ocaml-ocamlnet-devel >= 3.4.1-1m
BuildRequires:  ocaml-omake
BuildRequires:  ocaml-ounit-devel >= 1.1.0-1m
BuildRequires:  ruby


%description
Prelude.ml is a collection of utility functions for OCaml programs.
Of particular interest are high level parallel combinators, which make
multicore parallelization of OCaml programs easy.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n preludeml


%build
omake

# Create a META file.
cat >META <<_EOF_
version = "%{version}"
requires = "pcre,unix,netstring,bigarray"
description = "OCaml utility functions"
archive(byte) = "prelude.cmo"
archive(native) = "prelude.cmxa"
_EOF_


%check
# The tests fail in various delightful ways:
# - i386 & ppc fail on 1 test (out of several hundred).  Probably a
#   64 bit assumption in the code or in the test somewhere.
# - ppc64 runs out of memory during the test.
omake test ||:


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR

ocamlfind install preludeml \
  META \
  src/prelude.ml \
  src/prelude.cmi \
  src/prelude.cmo \
  src/prelude.cmx \
  src/prelude.a \
  src/prelude.cmxa


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/preludeml
%if %opt
%exclude %{_libdir}/ocaml/preludeml/*.a
%exclude %{_libdir}/ocaml/preludeml/*.cmx
%exclude %{_libdir}/ocaml/preludeml/*.cmxa
%endif
%exclude %{_libdir}/ocaml/preludeml/*.ml


%files devel
%defattr(-,root,root,-)
%doc LICENSE README TESTING
%if %opt
%{_libdir}/ocaml/preludeml/*.a
%{_libdir}/ocaml/preludeml/*.cmx
%{_libdir}/ocaml/preludeml/*.cmxa
%endif
%{_libdir}/ocaml/preludeml/*.ml


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-0.0.20090113.7m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.0.20090113.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.0.20090113.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-0.0.20090113.4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.0.20090113.3m)
- rebuild against the following packages
-- ocaml-3.11.2
-- ocaml-pcre-6.1.0-1m
-- ocaml-ocamlnet-2.2.9-9m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.0.20090113.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.0.20090113.1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-0.11.20090113
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 13 2009 Richard W.M. Jones <rjones@redhat.com> - 0.1-0.10.20090113
- Tests require ounit & ruby.

* Tue Jan 13 2009 Richard W.M. Jones <rjones@redhat.com> - 0.1-0.4.20090113
- New upstream version 20090113, resolving licensing issues.
- Use omake to build.
- Added a check section to run automated tests.

* Sat Dec 20 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1-0.2.20080922
- Fix the META file.

* Sat Dec 20 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1-0.1.20080922
- Initial RPM release.
