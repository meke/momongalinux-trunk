%global momorel 10
%global geckover 1.9.2

Summary:   An open-source implementation of Google Gadgets platform for Linux
Name:      google-gadgets
Version:   0.11.2
Release:   %{momorel}m%{?dist}
License:   Apache
URL:       http://code.google.com/p/google-gadgets-for-linux/
Group:     User Interface/Desktops
Source0:   http://google-gadgets-for-linux.googlecode.com/files/google-gadgets-for-linux-%{version}.tar.bz2
NoSource:  0
Source1:   google-gadgets.desktop
# http://google-gadgets-for-linux.googlecode.com/issues/attachment?aid=3779240887242606070&name=nm09.patch&token=36171e20b752b23e49f9d34119ca5ba4
Patch0:    nm09.patch

BuildRequires: autoconf >= 2.50, automake >= 1.9, libtool >= 2.2.0
BuildRequires: curl-devel >= 7.15.0
BuildRequires: openssl-devel, libxml2-devel >= 2.6.0, zlib-devel >= 1.2.0
BuildRequires: xulrunner-devel >= %{geckover}, dbus-devel >= 1.0.2, gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel, cairo-devel >= 1.2.0
BuildRequires: gtk2-devel >= 2.10.0 qt-devel >= 4.7.0
BuildRequires: webkitgtk-devel >= 1.3.4
Requires: curl >= 7.15.0, openssl
Requires: libxml2 >= 2.6.0, zlib >= 1.2.0, xulrunner >= %{geckover}, dbus >= 1.0.2
Requires: gstreamer, gstreamer-plugins-base, cairo >= 1.2.0, gtk2 >= 2.10.0
Requires: qt >= 4.7.0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Google Gadgets for Linux provides a platform for running desktop gadgets
under Linux, catering to the unique needs of Linux users.

%package devel
Summary:  development libraries for google-gadgets
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: dbus-devel gtk2-devel qt-devel >= 4.7.0

%description devel
development libraries for google-gadgets

%prep
%setup -q -n google-gadgets-for-linux-%{version}
%patch0 -p1 -b .nm09

%build
%configure --disable-update-mime-database \
	   --disable-update-desktop-database \
	   --disable-werror \
           --disable-gtkmoz-browser-element \
           --disable-smjs-script-runtime \
	CPPFLAGS=-DGLIB_COMPILATION
%make CXXFLAGS="-Wno-invalid-offsetof"

%install
rm -rf --preserve-root %{buildroot}

make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}%{_datadir}/pixmaps/
mkdir -p %{buildroot}%{_datadir}/applications/

install -m 644 resources/google-gadgets.png %{buildroot}%{_datadir}/pixmaps/google-gadgets.png
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/google-gadgets.desktop

find %{buildroot} -name "*.la" -delete

# clean up (conflicyting with libtool-ltdl-2.2.6-2m)
rm -rf %{buildroot}%{_includedir}/libltdl
rm -f %{buildroot}%{_includedir}/ltdl.h
rm -f %{buildroot}%{_libdir}/libltdl.*

# delete app/gg from mime
desktop-file-install --dir=%{buildroot}%{_datadir}/applications \
  --remove-mime-type=app/gg \
  %{buildroot}%{_datadir}/applications/ggl-qt.desktop
desktop-file-install --dir=%{buildroot}%{_datadir}/applications \
  --remove-mime-type=app/gg \
  %{buildroot}%{_datadir}/applications/ggl-gtk.desktop


%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
update-desktop-database %{_datadir}/applications &> /dev/null
update-mime-database %{_datadir}/mime &> /dev/null

%postun
/sbin/ldconfig
update-desktop-database %{_datadir}/applications &> /dev/null
update-mime-database %{_datadir}/mime &> /dev/null

%files
%defattr(-,root,root)
%doc AUTHORS COPYING README
%{_bindir}/ggl-gtk
%{_bindir}/ggl-qt
%{_libdir}/lib*.so.*

%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/modules
%{_libdir}/%{name}/modules/*.so

%{_datadir}/%{name}
%{_datadir}/applications/ggl-designer.desktop
%{_datadir}/applications/ggl-gtk.desktop
%{_datadir}/applications/ggl-qt.desktop
%{_datadir}/applications/google-gadgets.desktop
%{_datadir}/mime/packages/00-google-gadgets.xml
%{_datadir}/pixmaps/google-gadgets.png
%{_datadir}/icons/hicolor/*/*/*.png

%files devel
%defattr(-, root, root)
%{_includedir}/google-gadgets
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/%{name}/modules/*.a
%dir %{_libdir}/%{name}/include
%dir %{_libdir}/%{name}/include/ggadget
%{_libdir}/%{name}/include/ggadget/sysdeps.h
%{_libdir}/pkgconfig/libggadget-1.0.pc
%{_libdir}/pkgconfig/libggadget-dbus-1.0.pc
%{_libdir}/pkgconfig/libggadget-gtk-1.0.pc
%{_libdir}/pkgconfig/libggadget-js-1.0.pc
%{_libdir}/pkgconfig/libggadget-npapi-1.0.pc
%{_libdir}/pkgconfig/libggadget-qt-1.0.pc
%{_libdir}/pkgconfig/libggadget-xdg-1.0.pc

%changelog
* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.2-10m)
- build fix

* Fri Aug 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2-9m)
- add --disable-gtkmoz-browser-element and --disable-smjs-script-runtime options

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.2-7m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.2-6m)
- rebuild against webkitgtk-1.3.4

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.2-4m)
- full rebuild for mo7 release

* Fri Jul 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.2-3m)
- fix ggl-qt.desktop and ggl-gtk.desktop

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2-2m)
- rebuild against qt-4.6.3-1m

* Mon Feb  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.2-1m)
- update to 0.11.2
-- we need CXXFLAGS='-Wno-invalid-offsetof' and --disable-werror for xulrunner-1.9.2
-- http://code.google.com/p/google-gadgets-for-linux/issues/detail?id=352
- remove some workarounds

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.1-1m)
- update to 0.11.1

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.0-4m)
- fix ggl-gtk crash (Patch0)
-- http://code.google.com/p/google-gadgets-for-linux/issues/detail?id=311

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.0-3m)
- remove dependencies on SpiderMonkey

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.0-2m)
- rebuild against xulrunner-1.9.1

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.0-1m)
- update to 0.11.0

* Sun May 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.5-6m)
- remove conflicting files and a directory with libtool-ltdl-2.2.6
  (fix build on x86_64)

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.5-5m)
- rebuild against libtool-2.2.6

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-4m)
- rebuild against openssl-0.9.8k

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-3m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-2m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-1m)
- update to 0.10.5 official release

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-0.20090104.1m)
- update to 0.10.5 20090104 svn snapshot

* Mon Dec 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.4-1m)
- update

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.3-0.20081107.1m)
- update 20081107 svn version

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.3-0.20081024.1m)
- update 20081024 svn version

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.3-0.20081011.1m)
- update 20081011 svn version

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.3-0.20081003.1m)
- update 20081003 svn version

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.3-0.20080919.1m)
- update 20080919 svn version

* Tue Aug 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-2m)
- fix %%files, configure options, %%post %%postun scripts, and devel Req

* Tue Aug 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.1-1m)
- update

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.3-2m)
- change BuildRequires: gst-plugins-base-devel to gstreamer-plugins-base-devel
- change Requires: gst-plugins-base to gstreamer-plugins-base

* Mon Jul  7 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.9.3-1m)
- welcome to momonga
