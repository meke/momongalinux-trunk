%global momorel 12

Summary: irc - Internet Relay Chat
Name: irc

### include local configuration
%{?include_specopt}

### default configurations
# If you'd like to change these configurations, please copy them to
# ${HOME}/rpm/specopt/irc.specopt and edit it.

## Configuration
# long nick name (32) (1=yes 0=no)
%{?!enable_longnick:    %global enable_longnick            0}
# If your IRC server is relay server, then enable splitmode
%{?!enable_splitmode:   %global enable_splitmode           0}

%global irc_uid 406

Version: 2.11.1p1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
Source0: ftp://ftp.irc.org/irc/server/%{name}%{version}.tgz
Source1: ircd.init
Source2: ircd.conf.example-v6
Patch0: irc2.11.1p1-nicklen32.patch
Patch1: irc2.11.1p1-nosplit.patch
Patch100: http://www.ircnet.jp/dist/server/jp-patch/irc2.11.1p1-irc2.11.1p1+jp8.1beta2.patch
Patch101: irc2.10.3p7-lib64.patch
Patch102: irc2.10.3p7-ppc-libm.patch
# Url: http://irc.kyoto-u.ac.jp/
URL: http://www.irc.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: momonga-rpmmacros >= 20040310-6m
Requires(pre): shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig

%description
Internet Relay Chat

%prep
%setup -q -n %{name}%{version}
%patch100 -p1

%if %{enable_longnick}
%patch0 -p1 -b .nicklen32
%endif

%if !%{enable_splitmode}
%patch1 -p1 -b .nosplit
%endif

%if %{_lib} == "lib64"
%patch101 -p1 -b .lib64~
%endif
%ifarch ppc ia64 alpha sparc sparc64
#%%patch102 -p1 -b .ppc-libm~
%endif
# %%patch103 -p1 -b .gcc41

%build

%if %{_ipv6}
%define v6opt --enable-ip6=yes
%endif

%define TARGET `support/config.guess`
CFLAGS="%{optflags}" ./configure \
	--prefix=/usr \
	--with-zlib \
	--sysconfdir=/etc/ircd \
	--localstatedir=/var/run \
	--with-logdir=/var/log/ircd %{?v6opt:%{v6opt}}

perl -p -i -e '
     s|/\* #define\tHUB \*/|#define HUB|;
     s|#undef\tUSE_SYSLOG|#define USE_SYSLOG|;
     s|#undef\tSYSLOG_KILL|#define SYSLOG_KILL|;
     s|#undef\tSYSLOG_SQUIT|#define SYSLOG_SQUIT|;
     s|#undef\tSYSLOG_CONNECT|#define SYSLOG_CONNECT|;
     s|#undef\tSYSLOG_USERS|#define SYSLOG_USERS|;
     s|#undef\tSYSLOG_OPER|#define SYSLOG_OPER|;
     s|#undef\tSYSLOG_CONN|#define SYSLOG_CONN|' %TARGET/config.h

make -j%{_numjobs} all -C %TARGET

%install
rm -rf %{buildroot}
make install prefix="%{buildroot}/usr" \
	ircd_conf_dir="%{buildroot}/etc/ircd" \
	ircd_var_dir="%{buildroot}/var/run" \
	ircd_log_dir="%{buildroot}/var/log/ircd" \
	-C %TARGET
mkdir -p %{buildroot}%{_initscriptdir}
install -m 755 %SOURCE1 %{buildroot}%{_initscriptdir}/ircd
touch %{buildroot}/etc/ircd/ircd.motd

%if %{_ipv6}
%{__install} %{SOURCE2} %{buildroot}%{_sysconfdir}/ircd/ircd.conf.example-v6
%endif

if [ "%{_mandir}" != "/usr/man" ] ; then
  mkdir -p %{buildroot}%{_mandir}
  mv %{buildroot}/usr/man/* %{buildroot}%{_mandir}
fi

%clean
rm -rf %{buildroot}

%pre
/usr/sbin/useradd -u %{irc_uid} -M -d /var/log/ircd -r irc > /dev/null 2>&1
/usr/sbin/usermod -d /var/log/ircd irc >/dev/null 2>&1
# ignore errors, as we can't disambiguate between irc already existed
# and couldn't create account with the current adduser.
exit 0

%post
/sbin/chkconfig --add ircd

%preun
if [ $1 = 0 ]; then
   %{_initscriptdir}/ircd stop > /dev/null 2>&1
   /sbin/chkconfig --del ircd
fi

%files
%defattr(-,root,root)
%doc doc
%dir /etc/ircd
%attr(600,irc,tty) /etc/ircd/iauth.conf.example
%attr(600,irc,tty) /etc/ircd/ircd.conf.example
%attr(600,irc,tty) /etc/ircd/ircd.conf.example-v6
%attr(600,irc,tty) %config(noreplace) /etc/ircd/iauth.conf
%attr(600,irc,tty) %config(noreplace) /etc/ircd/ircd.motd
/etc/ircd/ircd.m4
%{_initscriptdir}/ircd
%{_mandir}/*/*
%{_sbindir}/chkconf
%{_sbindir}/iauth
%attr(2711,irc,tty) %{_sbindir}/ircd
%{_sbindir}/ircd-mkpasswd
%{_sbindir}/ircdwatch
%attr(-,irc,tty) /var/log/ircd

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.1p1-12m)
- add source and patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.1p1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.1p1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11.1p1-9m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11.1p1-8m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.1p1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.1p1-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11.1p1-5m)
- rebuild against gcc43

* Wed Jul  5 2006 Masahiro Takahata <takahata@momonga-linuxorg>
- (2.11.1p1-4m)
- enable irc2.11.1p1-nosplit.patch

* Sat Feb 25 2006 mutecat <mutecat@momonga-linuxorg>
- (2.11.1p1-3m)
- without irc2.10.3p7-ppc-libm.patch

* Sun Jan 29 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.11.1p1-2m)
- revised Patch101: irc2.10.3p7-lib64.patch

* Fri Jan 27 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.11.1p1-1m)
- version up 2.11.1p1

* Sat Jan  7 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.10.3p7-7m)
- add gcc-4.1 patch.
- Patch103: irc2.10.3p7-gcc41.patch

* Mon Oct 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.10.3p7-6m)
- enable ia64, alpha, sparc

* Wed Feb 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.10.3p7-5m)
- enable ppc.

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.10.3p7-4m)
- enable x86_64.

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.3p7-3m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.10.3p7-2m)
- stop daemon

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.10.3p7-1m)
- verup

* Fri Oct 24 2003 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.10.3p5-1m)
- security fixes

* Thu Oct 23 2003 zunda <zunda at freeshell.org>
- (2.10.3p3-20m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Jul 28 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.10.3p3-19m)
- _ipv6 aware
- add config sample for ipv6 enabled ircd (delimiter is changed to '%')

* Wed May 15 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.10.3p3-18k)
- can't use /sbin/nologin at useradd/usermod (in %pre)

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.10.3p3-16k)
- no /home/irc please...(^^;

* Tue Mar 12 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.10.3p3-14k)
- add patch for change nickname length 32
- (Default OFF)

* Mon Dec 03 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.10.3p3-12k)
- adduser -r  (no /home dir)

* Fri Nov 30 2001 Toru Hoshina <t@kondara.org>
- (2.10.3p3-10k)
- comment out %chkconfig

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.10.3p3-8k)
- nigittenu

* Wed Oct 31 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.10.3p3-6k)
- refresh ircd.init

* Mon Oct 29 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.10.3p3-4k)
  support HUB and syslog
  noreplace ircd.motd 

* Sat Oct 20 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.3p3-2k)
- update to 2.10.3p3

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.3-13k)
- rebuild against rpm-3.0.5-39k

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Mon Oct 09 2000 Motonobu Ichimura <famao@kondara.org>
- now disabled ipv6 :-<

* Mon Oct 09 2000 Motonobu Ichimura <famao@kondara.org>
- enabled ipv6

* Wed Jan 12 2000 Norihito Ohmori <nono@kondara.org>
- ircd uninstall bug. (%preun section)

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P
