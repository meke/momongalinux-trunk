%global momorel 1

Summary: The GNU shar utilities for packaging and unpackaging shell archives
Name: sharutils
Version: 4.13.5
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Archiving
URL: http://www.gnu.org/software/sharutils/
Source0: ftp://ftp.gnu.org/gnu/sharutils/sharutils-%{version}.tar.xz
NoSource: 0
Requires(post): info
Requires(preun): info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool, gettext
# for /usr/bin/compress
BuildRequires: ncompress

%description
The sharutils package contains the GNU shar utilities, a set of tools
for encoding and decoding packages of files (in binary or text format)
in a special plain text format called shell archives (shar).  This
format can be sent through e-mail (which can be problematic for regular
binary files).  The shar utility supports a wide range of capabilities
(compressing, uuencoding, splitting long files for multi-part
mailings, providing checksums), which make it very flexible at
creating shar files.  After the files have been sent, the unshar tool
scans mail messages looking for shar files.  Unshar automatically
strips off mail headers and introductory text and then unpacks the
shar files.

Install sharutils if you send binary files through email.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
rm -f %{buildroot}%{_infodir}/dir

%find_lang %{name}

%post
/sbin/install-info %{_infodir}/sharutils.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/sharutils.info %{_infodir}/dir
fi

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%{_bindir}/shar
%{_bindir}/unshar
%{_bindir}/uudecode
%{_bindir}/uuencode
%{_infodir}/sharutils.info*
%{_mandir}/man1/shar.1*
%{_mandir}/man1/unshar.1*
%{_mandir}/man1/uudecode.1*
%{_mandir}/man1/uuencode.1*
%{_mandir}/man5/uuencode.5*

%changelog
* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.5-1m)
- update to 4.13.5

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.3-1m)
- update to 4.13.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.9-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.9-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.9-3m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.9-2m)
- add BuildRequires

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.9-1m)
- update to 4.9

* Mon Mar  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8-1m)
- update to 4.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7-1m)
- update to 4.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.3-2m)
- rebuild against rpm-4.6

* Sat Jul 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.3-1m)
- update to 4.6.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-2m)
- %%NoSource -> NoSource

* Sun May 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.2-1m)
- update to 4.6.2
- delete all patch

* Wed Apr 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.2.1-26m)
- sync with Fedora
- [SECURITY] CAN-2005-0990

* Fri Oct  1 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.2.1-25m)
- removed shar-bof.patch (it's meaningless...)
- added sharutils-4.2.1-fix-fscanf-buffer-overrun.patch

* Wed Sep 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.2.1-24m)
- added BuildPreReq: libtool, gettext
- useed %%NoSource macro

* Wed Apr  8 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.2.1-23m)
- applied shar-bof.patch to fix buffer overflow vulnerability

* Wed May 15 2002 Toru Hoshina <t@Kondara.org>
- (4.2.1-22k)
- added fix for Unsecure outputfile handling in uudecode (#63303)

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (4.2.1-20k)

* Mon Oct 29 2001 Motonobu Ichimura <famao@kondara.org>
- (4.2.1-18k)
- added patch6

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (4.2.1-14k)
- rebuild against gettext 0.10.40.

* Sat Oct 13 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org>
- Kodaraized.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Than Ngo <than@redhat.de>
- fix typo (Bug# 12447)

* Sun Jun 18 2000 Than Ngo <than@redhat.de>
- rebuilt in the new build environment

* Thu Jun 08 2000 Than Ngo <than@redhat.de>
- add %defattr(-,root,root) (Bug# 11990)
- use rpm macros

* Sun May 21 2000 Ngo Than <than@redhat.de>
- rebuild to put man pages and info files in right place

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- rebuild to gzip man pages

* Tue Dec 21 1999 Preston Brown <pbrown@redhat.com>
- sharutils 4.2.1 for Y2K (2 digit date) fix.
- ja message catalog move (#7878)

* Tue Sep  7 1999 Jeff Johnson <jbj@redhat.com>
- handle spaces in uuencoded file names (David Fox <dsfox@cogsci.ucsd.edu>).

* Wed Jul 28 1999 Cristian Gafton <gafton@redhat.com>
- use the /usr/share/locale for the localedir instead of /usr/lib/locale
  (#2998)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 12)

* Wed Dec 30 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat Apr 11 1998 Cristian Gafton <gafton@redhat.com>
- manhattan rebuild

* Fri Oct 17 1997 Donnie Barnes <djb@redhat.com>
- ALRIGHT!  Woo-hoo!  Erik already did the install-info stuff!
- added BuildRoot
- spec file cleanups

* Sun Sep 14 1997 Erik Troan <ewt@redhat.com>
- uses install-info

* Fri Jul 18 1997 Erik Troan <ewt@redhat.com>
- built against glibc

