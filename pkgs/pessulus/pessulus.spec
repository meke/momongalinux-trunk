%global momorel 4
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: lockdown editor for GNOME
Name: pessulus
Version: 2.30.4
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/gnome/sources/%{name}/2.30/%{name}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: pygtk2-devel >= 2.24
BuildRequires: gnome-python2-devel >= 2.26.0
BuildRequires: gnome-python2-desktop
BuildRequires: pkgconfig
Requires(pre): hicolor-icon-theme gtk2

%description
pessulus is a lockdown editor for GNOME, written in python.

pessulus enables administrators to set mandatory settings in
GConf. The users can not change these settings.

Use of pessulus can be useful on computers that are open to use by
everyone, e.g. in an internet cafe.

%prep
%setup -q

%build
%configure --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog HACKING MAINTAINERS NEWS README TODO
%{_bindir}/pessulus
%{python_sitelib}/Pessulus/
%{_datadir}/applications/pessulus.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/%{name}

%changelog
* Mon May  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.30.4-4m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.4-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.4-1m)
- update to 2.30.4

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-3m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-3m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Mon May 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.0-2m)
- add BuildRequires

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.6-1m)
- update to 2.29.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.0-2m)
- rebuild against python-2.6.1-1m

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.16.4-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.4-1m)
- update to 2.16.4

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.3-2m)
- %%NoSource -> NoSource

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Tue Mar 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.16.2-3m)
- change BuildPrereq to gtk2-devel

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.2-2m)
- rebuild against python-2.5

* Tue Dec 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Thu May  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-1m)
- update to 1.10.1

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-1m)
- Initial Build
