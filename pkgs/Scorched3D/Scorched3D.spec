%global momorel 9

Summary: A 3D version of the classic DOS game Scorched Earth
Name: Scorched3D
Version: 42.1
Release: %{momorel}m%{?dist}
License: GPL+
Group: Amusements/Games
URL: http://www.scorched3d.co.uk/
Source0: http://dl.sourceforge.net/sourceforge/scorched3d/%{name}-%{version}-src.tar.gz
NoSource: 0
Source1: %{name}.desktop
Source2: scorched3d.png
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: SDL-devel >= 1.2.10
BuildRequires: SDL_net-devel
BuildRequires: freealut-devel >= 1.1.0
BuildRequires: freetype-devel >= 2.1.10
BuildRequires: libjpeg-devel >= 8a
BuildRequires: mesa-libGL-devel >= 6.5.1
BuildRequires: openal-soft-devel >= 1.10.622
BuildRequires: pkgconfig
BuildRequires: wxGTK-devel >= 2.6.3

%description
Scorched 3D is a game based loosely (or actually quite heavily now) on
the classic DOS game Scorched Earth "The Mother Of All
Games". Scorched 3D adds amongst other new features a 3D island
environment and LAN and internet play. Scorched 3D is totally free and
is available for both Microsoft Windows and Unix (Linux, FreeBSD, Mac
OS X, Solaris etc.) operating systems.

If you use alsa driver, please write below to $HOME/.openalrc

; Which backend?  Tried in order of appearance.
(define devices '(native alsa sdl esd arts null))
; Four speaker surround with ALSA.
;(define speaker-num 4)
;(define alsa-out-device "surround40:0,0")
(define alsa-out-device "hw:0,0")
; For alsa-in support. Mainly for using voice chat.
(define alsa-in-device "hw:0,0")
; Some drivers do not support select.
;(define native-use-select ;t)

(define sample-rate "22050")
( ( sample-rate 22050) )
#'

%prep
%setup -q -n scorched

%build
export OPENAL_CONFIG="%{_bindir}/pkg-config openal"
%configure --datadir=%{_datadir}/%{name} 
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x'

# isntall dekstop file
install -d %{buildroot}/%{_datadir}/applications
install %{SOURCE1} %{buildroot}/%{_datadir}/applications/

# install icon
install -d %{buildroot}/%{_datadir}/pixmaps
install %{SOURCE2} %{buildroot}/%{_datadir}/pixmaps/

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGELOG README documentation/README-*.txt
%{_bindir}/scorched3d
%{_bindir}/scorched3dc
%{_bindir}/scorched3ds
%{_datadir}/%{name}
%{_datadir}/applications/Scorched3D.desktop
%{_datadir}/pixmaps/scorched3d.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (42.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (42.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (42.1-7m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (42.1-6m)
- add StrategyGame; to Categories of Scorched3D.desktop

* Tue Jun  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (42.1-5m)
- add BuildRequires

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (42.1-4m)
- rebuild against libjpeg-8a

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (42.1-3m)
- rebuild against openal-soft-1.10.622

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (42.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (42.1-1m)
- update to 42.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-7m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-6m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPL+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-4m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-3m)
- BuildPrereq: freetype2-devel -> freetype-devel

* Sun Sep 24 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.0.1-2m)
- add "rm -rf autom4te.cache" before executing autoconf,
  because autoconf occurs an error in some environment

* Fri Sep 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0-1m)
- initial build
