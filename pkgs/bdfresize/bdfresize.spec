%global momorel 13

Summary: bdfresize - Resize BDF Format Font
Name: bdfresize
Version: 1.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Publishing
Source: http://openlab.ring.gr.jp/efont/dist/tools/bdfresize/%{name}-%{version}.tar.gz
Patch0: bdfresize-1.5-gcc34.patch
URL: http://openlab.ring.gr.jp/efont/dist/tools/bdfresize/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Bdfresize is a command to magnify or reduce fonts which are described with the
standard BDF format. 

%prep

%setup -q
%patch0 -p1 -b .gcc34~

%build
%configure
%make

%install
%makeinstall

%clean
rm -rf %{buildroot}

%files 
%defattr(-, root, root)
%{_bindir}/bdfresize
%{_mandir}/man1/*

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-13m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-10m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-7m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-6m)
- rebuild against gcc43

* Wed Oct 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.5-5m)
- add gcc34 patch

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Jan 18 2001 Kenzi Cano <kc@ring.gr.jp>
- (1.5-3k)
- update

* Sat Oct  7 2000 Kazuhiko <kazuhiko@kondara.org>
- (1.4-1k)
- initial release
