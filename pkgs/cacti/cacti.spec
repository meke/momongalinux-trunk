%global momorel 1
%global real_version 0.8.8a
%global spine_version 0.8.8a

Summary: Network monitoring/graphing tool
Name: cacti
Version: 0.8.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL: http://www.raxnet.net/products/cacti/

Source0: http://www.cacti.net/downloads/cacti-%{real_version}.tar.gz 
Source1: http://www.cacti.net/downloads/spine/cacti-spine-%{spine_version}.tar.gz 
NoSource: 0
NoSource: 1
Patch0: cacti-spine-0.8.8a-mysqlconfig.patch
Patch1: cacti-spine-snmp-version.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mysql-devel >= 5.1.32
BuildRequires: net-snmp-devel >= 5.7.1
Requires: webserver, mysql >= 5.5.10, net-snmp >= 5.7.1, net-snmp-utils, rrdtool
Requires: php, php-mysql, php-snmp, rrdtool-php
Requires: mysql-server

%description
Cacti is a complete frontend to RRDTool. It stores all of the necessary
information to create graphs and populate them with data in a MySQL database.

The frontend is completely PHP driven. Along with being able to maintain
graphs,data sources, and round robin archives in a database, Cacti also
handles the data gathering. There is SNMP support for those used to creating
traffic graphs with MRTG.

%package spine
Summary: Fast c-based poller for package %{name}
Group: Applications/System
Requires: cacti = %{version}
BuildRequires: net-snmp-devel >= 5.7.1
BuildRequires: openssl-devel >= 1.0.0
Provides: %{name}-cactid
Obsoletes: %{name}-cactid

%description spine
Spine (formaly cactid) is a supplemental poller for Cacti that makes use of 
pthreads to achieve excellent performance.

%prep
%setup -q -n %{name}-%{real_version} -a 1
pushd "cacti-spine-%{spine_version}"
%patch0 -p0
%patch1 -p1
popd

echo -e "#*/5 * * * *\tnobody\tphp %{_localstatedir}/www/cacti/poller.php > /dev/null 2>&1" >cacti.crontab

### Add a default cacti.conf for Apache.
%{__cat} <<EOF >cacti.httpd
Alias /cacti/ %{_localstatedir}/www/cacti/
<Directory %{_localstatedir}/www/cacti/>
	php_admin_value open_basedir %{_localstatedir}/www/cacti/
        DirectoryIndex index.php
	Options -Indexes
	AllowOverride all
        order deny,allow
        deny from all
        allow from 127.0.0.1
</Directory>
EOF

%build
cd cacti-spine-%{spine_version}
touch ChangeLog
autoreconf -fi
chmod 755 configure
%configure
%{__make} %{?_smp_mflags}


%install
%{__rm} -rf %{buildroot}
%{__install} -d -m0755 %{buildroot}%{_localstatedir}/www/cacti/
%{__install} -m0644 *.php cacti.sql %{buildroot}%{_localstatedir}/www/cacti/
%{__cp} -avx docs/ images/ include/ install/ lib/ log/ resource/ rra/ scripts/ %{buildroot}%{_localstatedir}/www/cacti/

%{__install} -D -m0755 cacti-spine-%{spine_version}/spine %{buildroot}%{_bindir}/spine
%{__install} -D -m0644 cacti-spine-%{spine_version}/spine.conf.dist %{buildroot}%{_sysconfdir}/spine.conf
%{__install} -D -m0644 cacti.crontab %{buildroot}%{_sysconfdir}/cron.d/cacti
%{__install} -D -m0644 cacti.httpd %{buildroot}%{_sysconfdir}/httpd/conf.d/cacti.conf.dist

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc LICENSE README
%config(noreplace) %{_localstatedir}/www/cacti/include/config.php
%config(noreplace) %{_sysconfdir}/httpd/conf.d/cacti.conf.dist
%config %{_sysconfdir}/cron.d/*
%dir %{_localstatedir}/www/cacti/
%{_localstatedir}/www/cacti/*.php
%{_localstatedir}/www/cacti/cacti.sql
%{_localstatedir}/www/cacti/docs/
%{_localstatedir}/www/cacti/images/
%{_localstatedir}/www/cacti/include/
%{_localstatedir}/www/cacti/install/
%{_localstatedir}/www/cacti/lib/
%{_localstatedir}/www/cacti/resource/
%{_localstatedir}/www/cacti/scripts/
%{_localstatedir}/www/cacti/log/
%{_localstatedir}/www/cacti/rra/
%attr(0664, root, apache) %{_localstatedir}/www/cacti/log/cacti.log
%attr(1777, root, root) %{_localstatedir}/www/cacti/rra/
 
%files spine
%defattr(-,root,root,0755)
%doc cacti-spine-%{spine_version}/AUTHORS cacti-spine-%{spine_version}/ChangeLog
%doc cacti-spine-%{spine_version}/COPYING cacti-spine-%{spine_version}/INSTALL
%doc cacti-spine-%{spine_version}/NEWS
%config(noreplace) %{_sysconfdir}/spine.conf
%{_bindir}/*

%changelog
* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8a

* Wed Jan 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-28m)
- [SECURITY] CVE-2011-4824
- update to 0.8.7i (cacti and spine)

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-27m)
- rebuild against net-snmp-5.7.1

* Sun Jul 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7-26m)
- rebuild against net-snmp-5.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-25m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-24m)
- rebuild against net-snmp-5.6.1

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-23m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-22m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-21m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-20m)
- [SECURITY] CVE-2010-2543 CVE-2010-2544 CVE-2010-2545
- update to 0.8.7g

* Tue May 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-19m)
- [SECURITY] CVE-2010-1431 (BONSAI-2010-0104)
- [SECURITY] CVE-2010-1645 http://www.bonsai-sec.com/en/research/vulnerabilities/cacti-os-command-injection-0105.php (BONSAI-2010-0105)
- [SECURITY] CVE-2010-1644 http://www.vupen.com/english/advisories/2010/1203
- [SECURITY] CVE-2010-2092 http://php-security.org/2010/05/13/mops-2010-023-cacti-graph-viewer-sql-injection-vulnerability/index.html (MOPS-2010-023)
- drop merged security patches for 0.8.7e
- update to 0.8.7f

* Wed May  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-18m)
- [SECURITY] CVE-2010-1431
- [SECURITY] http://secunia.com/advisories/39568/
- [SECURITY] apply upstream patch to resolve above issue

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-17m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-16m)
- rebuild against net-snmp-5.5

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-15m)
- [SECURITY] CVE-2009-4032 CVE-2009-4112
- [SECURITY] apply patch10 to fix multiple security issues
- see http://www.st.ryukoku.ac.jp/~kjm/security/ml-archive/full-disclosure/2009.11/msg00291.html
- update to 0.8.7e

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.7-13m)
- fix bad mode /etc/cron.d/cacti

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-12m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-11m)
- rebuild against mysql-5.1.32

* Fri Feb 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.7-10m)
- change %%patch to %%patch0

* Fri Feb 27 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.8.7-10m)
- fix cron. poller.php should choose according to config.

* Fri Feb 27 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.8.7-9m)
- update source
- fix rrdtool version error 
- fix net-snmp version error
- fix to use spine instead of obsolete cactid

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-7m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-6m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-5m)
- rebuild against gcc43

* Fri Feb 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7-4m)
- fix Obsoletes and add Provides for yum

* Wed Feb 13 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.8.7-3m)
- [SECURITY] fix SQL/HTML/Script injections
- http://secunia.com/advisories/28872/ 
- update cacti to 0.8.7b
- update cactid to spine 0.8.7a

* Wed Dec 5 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.8.7-2m)
- fix file and dir permissions

* Thu Nov 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-1m)
- [SECURITY] CVE-2007-6035
- update cacti to 0.8.7a
- update cactid to 0.8.6k

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.6-15m)
- rebuild against net-snmp

* Tue Jun 12 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.8.6-14m)
- [SECURITY] CVE-2007-3113
- apply official patches

* Wed Feb 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6-13m)
- add Requires: mysql-server

* Fri Jan 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-12m)
- update cacti to 0.8.6j
- update cactid to 0.8.6i
- Patch0 was modified
- [SECURITY] CVE-2006-6799
-- SQL injection vulnerability in Cacti 0.8.6i and earlier

* Wed Aug  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.6-11m)
- rebuild against net-snmp 5.3.1.0

* Mon Jul 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.6-10m)
- depend rrdtool-php

* Sat May 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.8.6-9m)
- update 0.8.6h (cacti)
- update 0.8.6g (cactid)

* Fri May 19 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.6-8m)
- update Patch0 for x86_64

* Tue May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.8.6-7m)
- rebuild against mysql 5.0.22-1m

* Tue Nov 1 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.8.6-6m)
- use open_basedir option for better security

* Mon Oct 17 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (0.8.6-5m)
- updated cacti to 0.8.6g (bug fix. see http://www.cacti.net/changelog.php)
- updated cactid to 0.8.6f-1 (bug fix. see http://www.cacti.net/cactid_changelog.php)

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.8.6-4m)
- rebuild against for MySQL-4.1.14

* Tue Jul 19 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.8.6-3m)
- up to 0.8.6f
- [SECURITY] 
 * Cacti multiple SQL Injection Vulnerabilities <http://www.hardened-php.net/advisory-032005.php>
 * Cacti Remote Command Execution Vulnerability <http://www.hardened-php.net/advisory-042005.php>
 * Cacti Authentification/Addslashes Bypass Vulnerability <http://www.hardened-php.net/advisory-052005.php>

* Tue Mar 31 2005 Toru Hoshina <t@momonga-linux.org>
- (0.8.6-2m)
- untar when %%setup so that we apply some patchies.
- use mysql_config to determin CFLAGS and LDFLAGS.

* Wed Mar 30 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.6-1m)
- upgrade

* Sat Jul 24 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.5-4m)
- stop service

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.5-3m)
- merge docs

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.5-2m)
- stop cron

* Sun Jul 18 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.5-1m)
- import to momonga
- s/mysql/MySQL/g at buildrequires requires

* Thu Jun 10 2004 Dag Wieers <dag@wieers.com> - 0.8.5-2.a
- Fixed correct location in cron script. (Alex Vitola)

* Fri Apr 02 2004 Dag Wieers <dag@wieers.com> - 0.8.5-1.a
- Updated to release 0.8.5a.

* Thu Apr 01 2004 Dag Wieers <dag@wieers.com> - 0.8.5-1
- Fixed cacti.httpd. (Dean Takemori)

* Tue Feb 17 2004 Dag Wieers <dag@wieers.com> - 0.8.5-0
- Cosmetic rebuild for Group-tag.
- Initial package. (using DAR)
