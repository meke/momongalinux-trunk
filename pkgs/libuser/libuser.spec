%global momorel 1
%global python 2.7

Summary: A user account administration library.
Name: libuser
Version: 0.60
Release: %{momorel}m%{?dist}
Group: System Environment/Base
License: LGPL
Source: https://fedorahosted.org/releases/l/i/libuser/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pam-devel, popt, python-devel >= %{python}
BuildRequires: glib-devel >= 2, gtk-doc, linuxdoc-tools
BuildRequires: cyrus-sasl-devel, openldap-devel >= 2.4.0
BuildRequires: openjade
BuildRequires: glib-devel >= 2.0.6-1m
BuildRequires: libselinux-devel

%description
The libuser library implements a standardized interface for manipulating
and administering user accounts. The library uses pluggable back-ends to
interface to its data sources.

Sample applications modeled after those included with the shadow password
suite are included.

%package devel
Summary: Files needed for developing applications which use libuser.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib-devel >= 2

%description devel
The libuser-devel package contains header files, static libraries, and .la
files useful for developing applications with libuser.

%package python
Summary: Python bindings for the libuser library
Group: Development/Libraries
Requires: libuser = %{version}-%{release}

%description python
The libuser-python package contains the Python bindings for
the libuser library, which provides a Python API for manipulating and
administering user and group accounts.

%prep
%setup -q

%build
gtkdocize --copy --docdir docs/reference
autoreconf -vfi
%configure --enable-gtk-doc \
        --with-selinux \
        --with-ldap \
        --with-html-dir=%{_datadir}/gtk-doc/html
%make

%clean
rm -fr --preserve-root %{buildroot}

%install
rm -fr --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

LD_LIBRARY_PATH=%{buildroot}/%{_libdir}:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# Verify that all python modules load, just in case.
pushd %{buildroot}/%{_libdir}/python%{python}/site-packages/
python%{python} -c "import libuser"
popd

%find_lang %{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README TODO docs/*.txt python/modules.txt
%config(noreplace) %{_sysconfdir}/libuser.conf

%attr(0755,root,root) %{_bindir}/*
%{_libdir}/*.so.*
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/*.so
%attr(0755,root,root) %{_sbindir}/*
%{_mandir}/man1/*
%{_mandir}/man5/*

%exclude %{_libdir}/*.la
%exclude %{_libdir}/%{name}/*.la
%exclude %{_libdir}/python%{python}/site-packages/*.la

%files devel
%defattr(-,root,root)
%{_includedir}/libuser
%{_libdir}/*.so
#%attr(0644,root,root) %{_mandir}/man3/*
%{_libdir}/pkgconfig/*
%{_datadir}/gtk-doc/html/*

%files python
%defattr(-,root,root)
%doc python/modules.txt
%{_libdir}/python%{python}/site-packages/*.so
%exclude %{_libdir}/python%{python}/site-packages/*.la

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.57.2-1m)
- update to 0.57.2

* Sat Apr 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.57.1-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.57.1-2m)
- rebuild for new GCC 4.6

* Fri Jan 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57.1-1m)
- update to 0.57.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.56.16-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.56.16-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56.16-1m)
- update to 0.56.16
- sync with Fedora devel (0.56.16-2)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.56.9-9m)
- use BuildRequires

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.56.9-8m)
- --enable-gtk-doc

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.56.9-7m)
- delete __libtoolize hack

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.56.9-6m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.56.9-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 18 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.56.9-4m)
- define libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.56.9-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.56.9-2m)
- rebuild against python-2.6.1-2m

* Wed Apr 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.56.9-1m)
- update 0.56.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.56.8-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.56.8-1m)
- update 0.56.8

* Sun Aug 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56.4-1m)
- update to 0.56.4

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56.2-1m)
- update to 0.56.2 (sync with FC-devel)

* Fri Mar  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.56-1m)
- update 0.56

* Fri Feb 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.55-1m)
- update 0.55

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.54.5-2m)
- rebuild against python-2.5

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.54.5-1m)
- update 0.54.5 (sync with FC5)

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.54.3-2m)
- rebuild against openldap-2.3.19

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.54.3-1m)
- version up 0.54.3
- rebuild against openldap-2.3.11

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.52.5-2m)
- rebuild against python-2.4.2

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (0.52.5-1m)
- ver up. sync with FC3(0.52.5-1).

* Tue Feb 22 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.51.7-3m)
- use linuxdoc-tools

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.51.7-2m)
- rebuild against python2.3

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.51.7-1m)
- version up.
- this package depends on sgml-tools, however I suppose this should depends on
  linuxdoc-tools instead...

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.51-11m)
- rebuild against openldap

* Sat Nov 08 2003 Kenta MURATA <muraken2@nifty.com>
- (0.51-10m)
- separate python package.

* Sun Nov  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.51-9m)
- accept python not only 2.2 but 2.2.*
- add PreReq: python 2.2 or 2.2.*

* Mon Jun 23 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.51-8m)
- using /usr/include/sasl/sasl.h of cyrus-sasl(2) 
  instead of /usr/include/sasl.h of cyrus-sasl1

* Mon Jun 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.51-7m)
- libtool

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.51-6m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.51-5m)
- rebuild against for gdbm

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.51-4m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Tue Jul 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.51-3m)
- 'BuildPreReq sgml-tools' instead of linuxdoc-tools

* Thu Jul 11 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.51-1m)
- version up to 0.51

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.50.2-6k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.50.2-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (0.50.2-2k)
- ver up.

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.49.96-10k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.49.96-8k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.49.96-6k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.49.96-4k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.49.96-2k)
- version 0.49.96

* Mon Nov  5 2001 Toru Hoshina <t@kondara.org>
- (0.32-6k)
- skipped a patch.

* Thu Oct 25 2001 Toru Hoshina <t@kondara.org>
- (0.32-4k)
- Require openjade explicitely.

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (0.32-2k)
- merge from rawhide.

* Wed Aug 29 2001 Nalin Dahyabhai <nalin@redhat.com> 0.32-1
- link the python module against libpam
- attempt to import the python modules at build-time to verify dependencies

* Tue Aug 28 2001 Nalin Dahyabhai <nalin@redhat.com> 0.31-1
- fix a file-parsing bug that popped up in 0.29's mmap modifications

* Mon Aug 27 2001 Nalin Dahyabhai <nalin@redhat.com> 0.30-1
- quotaq: fix argument order when reading quota information
- user_quota: set quota grace periods correctly
- luseradd: never create home directories for system accounts

* Tue Aug 21 2001 Nalin Dahyabhai <nalin@redhat.com>
- add da translation files
- update translations

* Tue Aug 21 2001 Nalin Dahyabhai <nalin@redhat.com> 0.29-1
- add an explicit build dependency on jade (for the docs)

* Mon Aug 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- HUP nscd on modifications
- userutil.c: mmap files we're reading for probable speed gain
- userutil.c: be conservative with the amount of random data we read
- docs fixes

* Wed Aug 15 2001 Nalin Dahyabhai <nalin@redhat.com> 0.28-1
- apps: print usage on errors
- lnewusers.c: initialize groups as groups, not users
- lnewusers.c: set passwords for new accounts
- luseradd.c: accept group names in addition to IDs for the -g flag
- luseradd.c: allow the primary GID to be a preexisting group

* Tue Aug 14 2001 Nalin Dahyabhai <nalin@redhat.com> 0.27-1
- add ko translation files
- files.c: fix a heap corruption bug in lock/unlock (#51750)
- files.c: close a memory leak in reading of files

* Mon Aug 13 2001 Nalin Dahyabhai <nalin@redhat.com>
- files.c: remove implementation limits on lengths of lines

* Thu Aug  9 2001 Nalin Dahyabhai <nalin@redhat.com> 0.26-1
- lusermod: change user name in groups the user is a member of during renames
- lgroupmod: change primary GID for users who are in the group during renumbers
- ldap.c: handle new attributes more gracefully if possible
- add ru translation files

* Tue Aug  7 2001 Nalin Dahyabhai <nalin@redhat.com> 0.25.1-1
- rename the quota source files to match the library, which clears up a
  file conflict with older quota packages
- add ja translation files

* Thu Aug  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- add lu_ent_clear_all() function

* Thu Aug  2 2001 Nalin Dahyabhai <nalin@redhat.com> 0.25-1
- close up some memory leaks
- add the ability to include resident versions of modules in the library

* Wed Aug  1 2001 Nalin Dahyabhai <nalin@redhat.com> 0.24-4
- fix incorrect Py_BuildValue invocation in python module

* Tue Jul 31 2001 Nalin Dahyabhai <nalin@redhat.com> 0.24-3
- stop leaking descriptors in the files module
- speed up user creation by reordering some checks for IDs being in use
- update the shadowLastChanged attribute when we set a password
- adjust usage of getXXXXX_r where needed
- fix assorted bugs in python binding which break prompting

* Mon Jul 30 2001 Nalin Dahyabhai <nalin@redhat.com> 0.23-1
- install sv translation
- make lpasswd prompt for passwords when none are given on the command line
- make sure all user-visible strings are marked for translation
- clean up some user-visible strings
- require PAM authentication in lchsh, lchfn, and lpasswd for non-networked modules

* Fri Jul 27 2001 Nalin Dahyabhai <nalin@redhat.com>
- print uids and gids of users and names in lid app
- fix tree traversal in users_enumerate_by_group and groups_enumerate_by_users
- implement enumerate_by_group and enumerate_by_user in ldap module
- fix id-based lookups in the ldap module
- implement islocked() method in ldap module
- implement setpass() method in ldap module
- add lchfn and lchsh apps
- add %%d substitution to libuser.conf

* Thu Jul 26 2001 Nalin Dahyabhai <nalin@redhat.com> 0.21-1
- finish adding a sasldb module which manipulates a sasldb file
- add users_enumerate_by_group and groups_enumerate_by_users

* Wed Jul 25 2001 Nalin Dahyabhai <nalin@redhat.com> 
- luserdel: remove the user's primary group if it has the same name as
  the user and has no members configured (-G disables)
- fixup some configure stuff to make libuser.conf get generated correctly
  even when execprefix isn't specified

* Tue Jul 24 2001 Nalin Dahyabhai <nalin@redhat.com> 0.20-1
- only call the auth module when setting passwords (oops)
- use GTrees instead of GHashTables for most internal tables
- files: complain properly about unset attributes
- files: group passwords are single-valued, not multiple-valued
- add lpasswd app, make sure all apps start up popt with the right names

* Mon Jul 23 2001 Nalin Dahyabhai <nalin@redhat.com> 0.18-1
- actually make the new optional arguments optional
- fix lu_error_new() to actually report errors right
- fix part of the python bindings
- include tools in the binary package again
- fixup modules so that password-changing works right again
- add a "key" field to prompt structures for use by apps which like to
  cache these things
- add an optional "mvhomedir" argument to userModify (python)

* Fri Jul 20 2001 Nalin Dahyabhai <nalin@redhat.com> 0.16.1-1
- finish home directory population
- implement home directory moving
- change entity get semantics in the python bindings to allow default values for .get()
- add lu_ent_has(), and a python has_key() method to Entity types
- don't include tools in the binary package
- add translated strings

* Thu Jul 19 2001 Nalin Dahyabhai <nalin@redhat.com>
- lib/user.c: catch and ignore errors when running stacks
- lusermod: fix slightly bogus help messages
- luseradd: when adding a user and group, use the gid of the group
  instead of the user's uid as the primary group
- properly set the password field in user accounts created using
  auth-only auth modules (shadow needs "x" instead of "!!")
- implement home directory removal, start on population

* Wed Jul 18 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix group password setting in the files module
- setpass affects both auth and info, so run both stacks

* Tue Jul 17 2001 Nalin Dahyabhai <nalin@redhat.com>
- make the testbed apps noinst

* Mon Jul 16 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix errors due to uninitialized fields in the python bindings
- add kwargs support to all python wrappers
- add a mechanism for passing arguments to python callbacks

* Wed Jul 11 2001 Nalin Dahyabhai <nalin@redhat.com>
- stub out the krb5 and ldap modules so that they'll at least compile again
 
* Tue Jul 10 2001 Nalin Dahyabhai <nalin@redhat.com>
- don't bail when writing empty fields to colon-delimited files
- use permissions of the original file when making backup files instead of 0600

* Fri Jul  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- finish implementing is_locked methods in files/shadow module
- finish cleanup of the python bindings
- allow conditional builds of modules so that we can build without
  all of the prereqs for all of the modules

* Thu Jun 21 2001 Nalin Dahyabhai <nalin@redhat.com>
- add error reporting facilities
- split public header into pieces by function
- backend cleanups

* Mon Jun 18 2001 Nalin Dahyabhai <nalin@redhat.com>
- make %%{name}-devel require %%{name} and not %%{name}-devel

* Fri Jun 15 2001 Nalin Dahyabhai <nalin@redhat.com>
- clean up quota bindings some more
- finish most of the ldap bindings
- fix a subtle bug in the files module that would show up when renaming accounts
- fix mapping methods for entity structures in python

* Thu Jun 14 2001 Nalin Dahyabhai <nalin@redhat.com>
- get bindings for prompts to work correctly
- clean up some of the add/remove semantics (set source on add)
- ldap: implement enumeration
- samples/enum: fix the argument order

* Wed Jun 13 2001 Nalin Dahyabhai <nalin@redhat.com>
- clean up python bindings for quota

* Tue Jun 12 2001 Nalin Dahyabhai <nalin@redhat.com> 0.11
- finish up python bindings for quota support

* Sun Jun 10 2001 Nalin Dahyabhai <nalin@redhat.com>
- finish up quota support libs

* Fri Jun  8 2001 Nalin Dahyabhai <nalin@redhat.com>
- start quota support library to get some type safety

* Thu Jun  7 2001 Nalin Dahyabhai <nalin@redhat.com>
- start looking at quota manipulation

* Wed Jun  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- add functions for enumerating users and groups, optionally per-module
- lusermod.c: -s should specify the shell, not the home directory

* Fri Jun  1 2001 Nalin Dahyabhai <nalin@redhat.com> 0.10
- finish the python bindings and verify that the file backend works again

* Wed May 30 2001 Nalin Dahyabhai <nalin@redhat.com>
- remove a redundant check which was breaking modifications

* Tue May 29 2001 Nalin Dahyabhai <nalin@redhat.com>
- finish adding setpass methods

* Wed May  2 2001 Nalin Dahyabhai <nalin@redhat.com> 0.9
- get a start on some Python bindings

* Tue May  1 2001 Nalin Dahyabhai <nalin@redhat.com> 0.8.2
- make binary-incompatible change in headers

* Mon Apr 30 2001 Nalin Dahyabhai <nalin@redhat.com> 0.8.1
- add doxygen docs and a "doc" target for them

* Sat Jan 20 2001 Nalin Dahyabhai <nalin@redhat.com> 0.8
- add a "quiet" prompter
- add --interactive flag to sample apps and default to using quiet prompter
- ldap: attempt a "self" bind if other attempts fail
- krb5: connect to the password-changing service if the user principal has
  the NULL instance

* Wed Jan 10 2001 Nalin Dahyabhai <nalin@redhat.com>
- the great adding-of-the-copyright-statements
- take more care when creating backup files in the files module

* Wed Jan  3 2001 Nalin Dahyabhai <nalin@redhat.com> 0.7
- add openldap-devel as a buildprereq
- krb5: use a continuous connection
- krb5: add "realm" config directive
- ldap: use a continuous connection
- ldap: add "server", "basedn", "binddn", "user", "authuser" config directives
- ldap: actually finish the account deletion function
- ldap: don't send cleartext passwords to the directory
- fix naming attribute for users (should be uid, not gid)
- refine the search-by-id,convert-to-name,search-by-name logic
- fix handling of defaults when the config file is read in but contains no value
- implement an LDAP information store
- try to clean up module naming with libtool
- luseradd: pass plaintext passwords along
- luseradd: use symbolic attribute names instead of hard-coded
- lusermod: pass plaintext passwords along
- lgroupadd: pass plaintext passwords along
- lgroupmod: pass plaintext passwords along
- add libuser as a dependency of libuser-devel

* Tue Jan  2 2001 Nalin Dahyabhai <nalin@redhat.com> 0.6
- initial packaging
