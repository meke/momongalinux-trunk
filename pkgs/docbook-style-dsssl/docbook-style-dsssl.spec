%global momorel 9

Name: docbook-style-dsssl
Version: 1.79
Release: %{momorel}m%{?dist}
Group: Applications/Text
Summary: Norman Walsh's modular stylesheets for DocBook.
License: see "README"
URL: http://docbook.sourceforge.net/

Source0: http://dl.sourceforge.net/sourceforge/docbook/docbook-dsssl-%{version}.tar.gz
Nosource: 0
Source1: %{name}.Makefile

%define openjadever 1.3.2
Requires(post): sgml-common >= 0.5
Requires(preun): sgml-common >= 0.5
Requires: docbook-dtds
Requires: openjade = %{openjadever}

BuildArch: noarch
Conflicts: docbook-utils < 0.6.9
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
These DSSSL stylesheets allow to convert any DocBook document to another
printed (for example, RTF or PostScript) or online (for example, HTML) format.
They are highly customizable.

%prep
%setup -q -n docbook-dsssl-%{version}
cp %{SOURCE1} Makefile
%build
%install
DESTDIR=%{buildroot}
rm -rf $DESTDIR
make install BINDIR=$DESTDIR/usr/bin DESTDIR=$DESTDIR/usr/share/sgml/docbook/dsssl-stylesheets-%{version}
cd ..
ln -s dsssl-stylesheets-%{version} $DESTDIR/usr/share/sgml/docbook/dsssl-stylesheets

%clean
DESTDIR=%{buildroot}
rm -rf $DESTDIR

%post
rel=$(echo /etc/sgml/sgml-docbook-3.0-*.cat)
rel=${rel##*-}
rel=${rel%.cat}
for centralized in /etc/sgml/*-docbook-*.cat
do
	/usr/bin/install-catalog --remove $centralized \
		/usr/share/sgml/docbook/dsssl-stylesheets-*/catalog \
		>/dev/null 2>/dev/null
done

for centralized in /etc/sgml/*-docbook-*$rel.cat
do
	/usr/bin/install-catalog --add $centralized \
		/usr/share/sgml/openjade-%{openjadever}/catalog \
		> /dev/null 2>/dev/null
	/usr/bin/install-catalog --add $centralized \
		/usr/share/sgml/docbook/dsssl-stylesheets-%{version}/catalog \
		> /dev/null 2>/dev/null
done

%preun
if [ "$1" = "0" ]; then
  for centralized in /etc/sgml/*-docbook-*.cat
  do   /usr/bin/install-catalog --remove $centralized /usr/share/sgml/openjade-%{openjadever}/catalog > /dev/null 2>/dev/null
    /usr/bin/install-catalog --remove $centralized /usr/share/sgml/docbook/dsssl-stylesheets-%{version}/catalog > /dev/null 2>/dev/null
  done
fi
exit 0

%files
%defattr (-,root,root)
%doc BUGS README ChangeLog WhatsNew
/usr/bin/collateindex.pl
/usr/share/sgml/docbook/dsssl-stylesheets-%{version}
/usr/share/sgml/docbook/dsssl-stylesheets


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.79-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.79-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.79-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.79-6m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.79-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.79-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.79-3m)
- rebuild against gcc43

* Mon May 21 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.79-2m)
- modify Makefile

* Wed Nov 17 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.79-1m)
- sync with Fedora (1.79-1)

* Tue Apr 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.78-3m)
- rebuild against for openjade-1.3.2

* Fri Oct 24 2003 zunda <zunda at freeshell.org>
- (1.78-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Mar 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.78-1m)

* Wed Jul 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.77-2m)
- fix URL

* Wed Jul 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.77-1m)
- add frames directory again (why omitted?)

* Mon May 13 2002 Toru Hoshina <t@Kondara.org>
- (1.76-2k)
- ver up.

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.74b-4k)
- rebuild against for openjade 1.3.1

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (1.74b-2k)
- version 1.74b

* Mon Oct 29 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.73-2k)
- version 1.73

* Wed Jul 25 2001 Bill Nottingham <notting@redhat.com> 1.64-3
- bump release 

* Thu May  9 2001 Tim Waugh <twaugh@redhat.com> 1.64-2
- Make an unversioned dsssl-stylesheets symbolic link.

* Wed May  2 2001 Tim Waugh <twaugh@redhat.com> 1.64-1
- 1.64 (fixes #38095).
- Fix up post/preun scripts so that we don't get duplicate entries with
  different versions on upgrade.

* Sun Mar 25 2001 Tim Waugh <twaugh@redhat.com> 1.59-10
- Fix up Makefile (patch from SATO Satoru).
- Change postun to preun.
- Make preun conditional on remove rather than upgrade.

* Tue Mar  6 2001 Tim Waugh <twaugh@redhat.com>
- PreReq docbook-dtd-sgml (it was a requirement before), so that the
  scripts work right.

* Tue Feb 20 2001 Tim Waugh <twaugh@redhat.com>
- Change Requires(...) to PreReq at Preston's request.
- PreReq at least openjade-1.3-12, so that its catalogs get installed.

* Wed Jan 24 2001 Tim Waugh <twaugh@redhat.com>
- Make scripts quieter.

* Tue Jan 23 2001 Tim Waugh <twaugh@redhat.com>
- Last fix was wrong; corrected (require openjade 1.3).

* Fri Jan 19 2001 Tim Waugh <twaugh@redhat.com>
- Require jade not openjade (bug #24306).

* Mon Jan 15 2001 Tim Waugh <twaugh@redhat.com>
- Don't play so many macro games.
- Change requirement on /usr/bin/install-catalog to sgml-common.
- Be sure to own dsssl-stylesheets-1.59 directory.

* Mon Jan 08 2001 Tim Waugh <twaugh@redhat.com>
- Change group.
- openjade not jade.
- %%{_tmppath}.
- rm before install.
- Change Copyright: to License:.
- Remove Packager: line.

* Mon Jan 08 2001 Tim Waugh <twaugh@redhat.com>
- Based on Eric Bischoff's new-trials packages.
