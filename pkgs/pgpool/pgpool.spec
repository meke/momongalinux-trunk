%global momorel 2

%global src0_dir 1446

Summary: Generic connection pool server for PostgreSQL
Name: pgpool
Version: 3.4.1
Release: %{momorel}m%{?dist}
Source0: http://pgfoundry.org/frs/download.php/%{src0_dir}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: rc.pgpool
Patch1: pgpool.conf.sample.patch
License: BSD
Group: Applications/Databases
URL: http://www2b.biglobe.ne.jp/~caco/pgpool/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): chkconfig
Requires(preun): coreutils chkconfig

%description
pgpool is a generic connection pool server for PostgreSQL. It also enables
database replication and load balancing.

%prep
%setup -q
%patch1 -p1

%build
%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

# startup script
install -d %{buildroot}%{_initscriptdir}
install -m 755 %{SOURCE1} %{buildroot}%{_initscriptdir}/pgpool

# configuration file
if [ ! -f %{buildroot}%{_sysconfdir}/pgpool.conf ]
then
    cp %{buildroot}%{_sysconfdir}/pgpool.conf.sample %{buildroot}%{_sysconfdir}/pgpool.conf
fi

%post
/sbin/chkconfig --add pgpool

%preun
if [ $1 = 0 ] ; then
    if test -r /var/run/pgpool.pid
    then
        %{_initscriptdir}/pgpool stop
    fi
    /sbin/chkconfig --del pgpool
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS README README.euc_jp COPYING ChangeLog NEWS TODO
%{_bindir}/pgpool
%{_sysconfdir}/pgpool.conf.sample
%{_sysconfdir}/pool_hba.conf.sample
%config %{_initscriptdir}/pgpool
%config(noreplace) %{_sysconfdir}/pgpool.conf
%{_datadir}/%{name}
%{_mandir}/man8/pgpool.8*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.1-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-6m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-5m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-2m)
- rebuild against gcc43

* Mon Sep 17 2007 NARITA Koichi <pulsar@momonga-linux.prg>
- (3.2-1m)
- update to 3.2
- do not use %%NoSource macro
- update conf sample patch

* Fri Feb  3 2006 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.7.2-1m)
- update to 2.7.2 - bugfix release

* Sun Nov 27 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5 - bugfix release

* Mon Oct 23 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4 - bugfix release

* Wed Sep 14 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Sat Aug 6 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Sat Jul 2 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Fri Feb 4 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.5-1m)
- update to 2.5

* Tue Dec 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.3-1m)
- initial import to Momonga
