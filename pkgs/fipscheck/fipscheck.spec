%global momorel 1

Summary:	A library for integrity verification of FIPS validated modules
Name:		fipscheck
Version:	1.4.1
Release:	%{momorel}m%{?dist}
License:	Modified BSD
Group:		System Environment/Libraries
# This is a Red Hat maintained package which is specific to
# our distribution.
URL:		http://fedorahosted.org/fipscheck/
Source0:	http://fedorahosted.org/releases/f/i/%{name}/%{name}-%{version}.tar.bz2
NoSource:	0
# Prelink blacklist
Source1:	fipscheck.conf
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: 	openssl-devel >= 1.0.0

%description
FIPSCheck is a library for integrity verification of FIPS validated
modules. The package also provides helper binaries for creation and
verification of the HMAC-SHA256 checksum files.

%package lib
Summary:	Library files for %{name}
Group:		System Environment/Libraries

Requires:	%{name} = %{version}-%{release}
Obsoletes:	%{name} < 1.2.0-1
Conflicts:	%{name} < 1.2.0-1

%description lib
This package contains the FIPSCheck library.

%package devel
Summary:	Development files for %{name}
Group:		System Environment/Libraries

Requires:	%{name}-lib = %{version}-%{release}

%description devel
This package contains development files for %{name}.

%prep
%setup -q

%build
%configure --disable-static

make %{?_smp_mflags}

# Add generation of HMAC checksums of the final stripped binaries
%define __spec_install_post \
    %{?__debug_package:%{__debug_install_post}} \
    %{__arch_install_post} \
    %{__os_install_post} \
    $RPM_BUILD_ROOT%{_bindir}/fipshmac $RPM_BUILD_ROOT%{_bindir}/fipscheck \
    $RPM_BUILD_ROOT%{_bindir}/fipshmac $RPM_BUILD_ROOT%{_libdir}/libfipscheck.so.1.2.1 \
    ln -s .libfipscheck.so.1.1.0.hmac $RPM_BUILD_ROOT%{_libdir}/.libfipscheck.so.1.hmac \
%{nil}

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name "*.la" -delete

# Prelink blacklist
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/prelink.conf.d
install -m644 %{SOURCE1} \
	$RPM_BUILD_ROOT/%{_sysconfdir}/prelink.conf.d/fipscheck.conf

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING README AUTHORS
%{_bindir}/fipscheck
%{_bindir}/.fipscheck.hmac
%{_bindir}/fipshmac
%{_mandir}/man8/*

%files lib
%defattr(-,root,root,-)
%{_libdir}/libfipscheck.so.*
%{_libdir}/.libfipscheck.so.*
%dir %{_sysconfdir}/prelink.conf.d
%{_sysconfdir}/prelink.conf.d/fipscheck.conf

%files devel
%defattr(-,root,root,-)
%{_includedir}/fipscheck.h
%{_libdir}/libfipscheck.so
%{_mandir}/man3/*

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-1m)
- update 1.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-1m)
- import from Fedora to Momonga for openssh-5.3

* Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 1.2.0-3
- rebuilt with new openssl

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue May 26 2009 Tomas Mraz - 1.2.0-1
- add lib subpackage to avoid multilib on the base package
- add ability to compute hmacs on multiple files at once
- improved debugging with FIPSCHECK_DEBUG

* Thu Mar 19 2009 Tomas Mraz - 1.1.1-1
- move binaries and libraries to /usr

* Wed Mar 18 2009 Tomas Mraz - 1.1.0-1
- hmac check itself as required by FIPS

* Mon Feb  9 2009 Tomas Mraz - 1.0.4-1
- add some docs to the README, require current openssl in Fedora

* Fri Oct 24 2008 Tomas Mraz - 1.0.3-1
- use OpenSSL in FIPS mode to do the HMAC checksum instead of NSS

* Tue Sep  9 2008 Tomas Mraz - 1.0.2-1
- fix test for prelink

* Mon Sep  8 2008 Tomas Mraz - 1.0.1-1
- put binaries in /bin and libraries in /lib as fipscheck
  will be used by modules in /lib

* Mon Sep  8 2008 Tomas Mraz - 1.0.0-2
- minor fixes for package review

* Wed Sep  3 2008 Tomas Mraz - 1.0.0-1
- Initial spec file
