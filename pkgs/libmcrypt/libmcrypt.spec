%global momorel 9

Summary: A library of encription functions.
Name: libmcrypt
Version: 2.5.8
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/mcrypt/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://mcrypt.sourceforge.net/
NoSource: 0

%description 
The libmcrypt package contains a library of functions of encrption
algorithms.  The library supports the algorithms: BLOWFISH, TWOFISH,
DES, TripleDES, 3-WAY, SAFER-sk64, SAFER-sk128, SAFER+, LOKI97, GOST,
RC2, RC6, MARS, IDEA, RIJNDAEL-128 (AES), RIJNDAEL-192, RIJNDAEL-256,
SERPENT, CAST-128 (known as CAST5), CAST-256, ARCFOUR and WAKE.  Block
algorithms can be used in: CBC, ECB, CFB and OFB (8 bit and n bit,
where n is the size of the algorithm block length).

%package devel
Summary: Development tools for programs needs libmcrypt functions.
Group: Development/Libraries
Requires: %name = %{version}-%{release}

%description devel
The libmcrypt-devel package contains the header files and static libraries
necessary for developing programs using the libmcrypt.

%prep
%setup -q

%build
./configure
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
# % doc AUTHORS COPYING ChangeLog KNOWN-BUGS INSTALL NEWS README THANKS TODO
%doc AUTHORS ChangeLog KNOWN-BUGS INSTALL NEWS README THANKS TODO
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_bindir}/libmcrypt-config
%{_libdir}/lib*.so
%{_includedir}/mcrypt.h
%{_includedir}/mutils/mcrypt.h
%{_datadir}/aclocal/libmcrypt.m4
%{_mandir}/man3/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.8-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.8-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.8-7m)
- full rebuild for mo7 release

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.8-6m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.8-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.8-3m)
- rebuild against gcc43

* Thu Aug 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.8-2m)
- fix duplicate files

* Thu Apr  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.8-1m)
- update to 2.5.8
- delete m4 patch

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.7-3m)
- delete libtool library

* Sun May 29 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.7-2m)
- suppress AC_DEFUN warning.

* Sat Oct 18 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.5.7-1m)
- update to 2.5.7
- moved to sourceforge

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.5-1m)
- update to 2.5.5
- move lib*.so to libmcrypt-devel

* Thu Aug 1 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.5.2-1m)
- add libmcrypt


