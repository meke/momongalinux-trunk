%global momorel 1

%define source_name	usb-modeswitch

Name:		usb_modeswitch
Version:	1.2.4
Release:	%{momorel}m%{?dist}
Summary:	USB Modeswitch gets 4G cards in operational mode
Group:		Applications/System
License:	GPLv2+
URL:		http://www.draisberghof.de/usb_modeswitch/
Source0: 	http://www.draisberghof.de/%{name}/%{source_name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	usb_modeswitch-data
BuildRequires:	libusb-devel

%description
USB Modeswitch brings up your datacard into operational mode. When plugged
in they identify themselves as cdrom and present some non-Linux compatible
installation files. This tool deactivates this cdrom-devices and enables
the real communication device. It supports most devices built and
sold by Huawei, T-Mobile, Vodafone, Option, ZTE, Novatel.

%prep
%setup -q -n %{source_name}-%{version}

%build
CFLAGS="$RPM_OPT_FLAGS" make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/lib/udev/rules.d/
mkdir -p $RPM_BUILD_ROOT%{_sbindir}
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}

install -p -m 755 usb_modeswitch $RPM_BUILD_ROOT%{_sbindir}/
install -p -m 644 usb_modeswitch.conf $RPM_BUILD_ROOT%{_sysconfdir}/
install -m 644 usb_modeswitch.1 $RPM_BUILD_ROOT%{_datadir}/man/man1
install -p -m 755 usb_modeswitch.tcl $RPM_BUILD_ROOT/lib/udev/usb_modeswitch

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog 
%{_sbindir}/usb_modeswitch
%{_mandir}/man1/usb_modeswitch.1*
/lib/udev/usb_modeswitch
%config(noreplace) %{_sysconfdir}/usb_modeswitch.conf

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-1m)
- import from Fedora 13

* Tue Jun 22 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> 1.1.3-1
- New upstream

* Fri Apr 23 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> - 1.1.2-3
- Fix typo in binary location

* Fri Apr 23 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> - 1.1.2-2
- Move usb_modeswitch binary back to /usr/sbin
- Package /etc/usb_modeswitch.setup for manual mode

* Tue Apr 20 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> - 1.1.2-1
- New upstream
- Split data and main package

* Mon Mar 8 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> - 1.1.0-6
- Version bump for F-12 build

* Sat Mar 6 2010 Huzaifa Sidhpurwala <huzaifas@redaht.com> - 1.1.0-5
- Fix regression in rhbz #571001
- Version bump

* Thu Mar 4 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> 1.1.0-3
- Patch usb_modeswtich to use the binary from /usr/bin/
- usb_modeswitch-data needs tcl

* Wed Mar 2 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> 1.1.0-2
- Reload udev when new rules are installed

* Wed Mar 2 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> 1.1.0-1
- New upstream 1.1.0 release
- Split package into binary and data part

* Thu Sep 17 2009 Peter Robinson <pbrobinson@gmail.com> 1.0.5-1
- new upstream 1.0.5 release

* Sun Aug 02 2009 Robert M. Albrecht <fedora@romal.de> 1.0.2-1
- new upstream release

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jan 11 2009 Robert M. Albrecht <fedora@romal.de> 0.9.6-1
* new upstream release

* Sat Dec 13 2008 Robert M. Albrecht <fedora@romal.de> 0.9.5-1
* new upstream release

* Sat Jun 22 2008 Robert M. Albrecht <romal@gmx.de> 0.9.4-2
- Fixed some rpmlint errors
- Added german translation

* Sat Jun 22 2008 Robert M. Albrecht <romal@gmx.de> 0.9.4-1
- Update to 0.9.4
- Honor RPM_OPT_FLAGS
  
* Sat May 26 2008 Robert M. Albrecht <romal@gmx.de> 0.9.4-0.1.beta2
- First package Release

