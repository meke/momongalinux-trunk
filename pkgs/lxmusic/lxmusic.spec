%global momorel 5

Name:           lxmusic
Version:        0.4.4
Release:        %{momorel}m%{?dist}
Summary:        Lightweight XMMS2 client with simple user interface

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://lxde.org/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         lxmusic-0.3.0-no-tools-menu.patch
Patch1:		lxmusic-0.4.4-libnotify-0.7.2.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.12.0 xmms2-devel >= 0.7
BuildRequires:  desktop-file-utils gettext intltool
BuildRequires:  libnotify-devel >= 0.7.2
Requires:       xmms2 >= 0.7

%description
LXMusic is a very simple gtk+ XMMS2 client written in pure C. It has very few 
functionality, and can do nothing more than play the music. The UI is very 
clean and simple. This is currently aimed to be used as the default music 
player of LXDE (Lightweight X11 Desktop Environment) project.

%prep
%setup -q
%patch0 -p1 -b .no-tools
%patch1 -p1 -b .libnotify

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
desktop-file-install --vendor=	                           \
  --delete-original                                        \
  --add-only-show-in="LXDE"                                \
  --dir=${RPM_BUILD_ROOT}%{_datadir}/applications          \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/%{name}
%{_datadir}/applications/lxmusic.desktop
%{_datadir}/lxmusic
%{_datadir}/pixmaps/lxmusic.png


%changelog
* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-5m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-2m)
- --add-only-show-in="LXDE"

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- rebuild against xmms2-0.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-1m)
- import from Fedora 11

* Mon Apr 13 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.3-3
- Disable empty tools menu

* Sun Mar 01 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.3-2
- Build Require gtk2-devel

* Sat Dec 20 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.3-1
- Initial Fedora Package
