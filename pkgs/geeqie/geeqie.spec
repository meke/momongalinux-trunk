%global momorel 1

Summary: Image browser and viewer
Name: geeqie
Version: 1.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/X
URL: http://geeqie.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{name}-%{version}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-1.0beta2-notshowin.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
BuildRequires: gettext
BuildRequires: gtk2-devel
BuildRequires: exiv2-devel >= 0.23
BuildRequires: intltool
BuildRequires: lcms-devel
BuildRequires: lirc-devel

%description
*** Caution! This is a beta release.
*** Be careful when evaluating this software.

Geeqie has been forked from the GQview project with the goal of picking up
development and integrating patches. It is an image viewer for browsing
through graphics files. Its many features include single click file
viewing, support for external editors, previewing images using thumbnails,
and zoom.

%prep
%setup -q

%patch0 -p1 -b .notshowin

%build
%configure --enable-lirc
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} INSTALL="install -p" install

# clean up
rm -f %{buildroot}%{_datadir}/doc/%{name}-%{upstreamversion}/ChangeLog

%find_lang %name

%clean
rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database &> /dev/null || :

%postun
%{_bindir}/update-desktop-database &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README* TODO
%{_bindir}/%{name}
%{_prefix}/lib/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}.png

%changelog
* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- version 1.1
- rebuild against exiv2-0.23

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-6m)
- rebuild against exiv2-0.22

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-4m)
- rebuild against exiv2-0.21.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- full rebuild for mo7 release

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- version 1.0
- and build against exiv2-0.20

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.1.3m)
- rebuild against exiv2-0.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.1m)
- import from Rawhide

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-0.19.beta2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul  6 2009 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.18.beta2
- update to beta2 tarball
- BR intltool
- print-pagesize.patch enabled in 1.0beta2 (#222639)

* Thu May 14 2009 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.16.beta1
- update to beta1 tarball

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-0.15.alpha3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Feb  7 2009 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.14.alpha3
- fetch src/utilops.c change from svn 1385 for metadata crash-fix

* Wed Jan 28 2009 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.13.alpha3
- ignore .helpdir/.htmldir values in geeqierc to fix "Help"
- add --enable-lirc again to build with LIRC

* Mon Jan 26 2009 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.12.alpha3
- update to alpha3 tarball

* Thu Jan 22 2009 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.11.alpha2.1341svn
- update to svn 1341 for pre-alpha3 testing (image metadata features)
- drop obsolete patches remote-blank and float-layout

* Wed Dec 24 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.11.alpha2.1307svn
- update to svn 1307 for "Safe delete"

* Thu Dec 18 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.11.alpha2.1299svn
- drop desktop file Exec= invocation patch (no longer necessary)

* Thu Dec 18 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.10.alpha2.1299svn
- update to svn 1299 for new exiv2
- disable LIRC support which is broken

* Thu Dec 18 2008 Rex Dieter <rdieter@fedoraproject.org> - 1.0-0.9.alpha2
- respin (exiv2)

* Tue Aug 12 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.8.alpha2
- fix float layout for --blank mode

* Mon Aug 11 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.7.alpha2
- fix options --blank and -r file:

* Thu Jul 31 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.6.alpha2
- update to 1.0alpha2 (now GPLv3)
- build with new LIRC support

* Wed Jun 25 2008 Rex Dieter <rdieter@fedoraproject.org> - 1.0-0.5.alpha1 
- respin for exiv2

* Thu May  8 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.4.alpha1
- scriptlets: run update-desktop-database without path
- drop dependency on desktop-file-utils
- drop ChangeLog file as it's too low-level

* Fri Apr 25 2008 Michael Schwendt <mschwendt@fedoraproject.org> - 1.0-0.3.alpha1
- package GQview fork "geeqie 1.0alpha1" based on Fedora gqview.spec
- BR lcms-devel exiv2-devel
- update -desktop and -editors patch
- update spec file with more dir macros

