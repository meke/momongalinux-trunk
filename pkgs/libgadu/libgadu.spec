%global         momorel 1

Name:		libgadu
Version:	1.11.4
Release:	%{momorel}m%{?dist}
Summary:	A Gadu-gadu protocol compatible communications library
License:	LGPLv2
Group:		System Environment/Libraries
Source0:        http://github.com/wojtekka/%{name}/releases/download/%{version}/%{name}-%{version}.tar.gz
NoSource:	0
URL:		http://toxygen.net/libgadu/
BuildRequires:	openssl-devel >= 1.0.0
BuildRequires:	libbind-devel >= 6.0
BuildRequires:  gnutls-devel >= 3.2.0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
libgadu is intended to make it easy to add Gadu-Gadu communication
support to your software.

%package devel
Summary:	Libgadu development library
Group:		Development/Libraries
Requires:	libgadu = %{version}-%{release}
Requires:	openssl-devel
Requires:	pkgconfig

%description devel
The libgadu-devel package contains the header files and some
documentation needed to develop application with libgadu.

%prep
%setup -q

%build
%configure \
	--disable-static \
	--with-pthread

%{__make} %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

%{__make} install INSTALL="install -p" DESTDIR=$RPM_BUILD_ROOT

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(644,root,root,755)
%doc COPYING
%attr(755,root,root) %{_libdir}/libgadu.so.*

%files devel
%defattr(644,root,root,755)
%attr(755,root,root) %{_libdir}/libgadu.so
%{_includedir}/libgadu.h
%{_libdir}/pkgconfig/*

%changelog
* Sat May 31 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.4-1m)
- [SECURITY] CVE-2014-3775
- update to 1.11.4

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.2-2m)
- rebuild against gnutls-3.2.0

* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.2-1m)
- update to 1.11.2

* Thu Jul 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.0-1m)
- update to 1.11.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.1-2m)
- rebuild for new GCC 4.6

* Tue Mar 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.1-1m)
- update to 1.10.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.1-2m)
- rebuild for new GCC 4.5

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.1-1m)
- update to 1.9.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-2m)
- full rebuild for mo7 release

* Sun May 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0

* Sat Apr 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-0.3.1m)
- update to 1.9.0-rc3

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-0.2.2m)
- rebuild against openssl-1.0.0

* Mon Dec  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-0.2.1m)
- update to 1.9.0-rc2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-0.1.1m)
- update to 1.9.0-rc1

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-4m)
- rebuild against libbind-6.0

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.2-1m)
- import from Fedora devel

* Sun Oct 26 2008 Dominik Mierzejewski <rpm@greysector.net> 1.8.2-1
- updated to 1.8.2 (security update)
- preserve timestamps during make install
- put defattr at the top of files section (fixes rpmlint error)

* Wed Jun 18 2008 Dominik Mierzejewski <rpm@greysector.net> 1.8.1-1
- updated to 1.8.1

* Sun Feb 24 2008 Dominik Mierzejewski <rpm@greysector.net> 1.8.0-1
- updated to 1.8.0

* Sat Feb 16 2008 Dominik Mierzejewski <rpm@greysector.net> 1.7.2-1
- updated to 1.7.2

* Wed Dec 05 2007 Release Engineering <rel-eng at fedoraproject dot org> - 1.7.1-3
 - Rebuild for deps

* Sun Aug 26 2007 Dominik Mierzejewski <rpm@greysector.net> 1.7.1-2
- rebuild for BuildID
- update license tag

* Wed Apr 25 2007 Dominik Mierzejewski <rpm@greysector.net> 1.7.1-1
- updated to 1.7.1 (security fixes)

* Sun Sep 17 2006 Dominik Mierzejewski <rpm@greysector.net> 1.7.0-1
- initial build
