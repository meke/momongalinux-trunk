%global momorel 15

Summary: Fast Assembly Mpeg Encoding library
Name: libfame
Version: 0.9.1
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://fame.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/fame/libfame-%{version}.tar.gz
NoSource: 0
Patch0: libfame-0.9.1-gcc34-1.patch
Patch10: libfame-0.9.0-lib64.patch
Patch11: libfame.m4.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: libtool

%description
FAME is a library for fast MPEG encoding.

%package devel
Summary: Libraries and include to develop using FAME
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
FAME is a library for fast MPEG encoding.

This is the libraries, include files and other resources you can use
to develop FAME applications.

%prep

%setup -q
%patch0 -p1 -b .gcc34~
%if %{_lib} == "lib64"
%patch10 -p1 -b .lib64~
%endif
%patch11 -p0 -b .m4~

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

%build
%configure --enable-sse --enable-mmx
export SED="%__sed" EGREP="%__grep -E" LTCC='gcc' max_cmd_len=32768
%make

%install
rm -rf %{buildroot}
export SED="%__sed" EGREP="%__grep -E" LTCC='gcc' max_cmd_len=32768
%makeinstall transform='s,x,x,'

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc CHANGES COPYING README
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%doc CHANGES COPYING README
%{_bindir}/*-config
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_includedir}/*
%{_datadir}/aclocal/*
%{_mandir}/man3/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1-13m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-12m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-10m)
- fix build on x86_64

* Thu May 21 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-9m)
- define __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-7m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-6m)
- delete libtool library

* Fri Jan  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-5m)
- import libfame-0.9.1-gcc34-1.patch from BLFS/svn
  http://blfs-bugs.linuxfromscratch.org/show_bug.cgi?id=921
- --enable-mmx

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-4m)
- suppress AC_DEFUN warning.

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.1-3m)
- enable x86_64.

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.1-2m)
- add transform='s,x,x,' to %%makeinstall

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.1-1m)
- revise Source URI

* Fri Oct 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.9.0-3m)
- add gcc34 patch

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9.0-2m)
- revised spec for enabling rpm 4.2.

* Thu Sep  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-1m)
- initial import to momonga
