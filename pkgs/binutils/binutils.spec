%global momorel 5

# !! please sync memprof.spec BuildRequires version when binutils' version up

# rpmbuild parameters:
# --define "binutils_target arm-linux-gnu" to create arm-linux-gnu-binutils.
# --with debug: Build without optimizations and without splitting the debuginfo.
# --without testsuite: Do not run the testsuite.  Default is to run it.
# --with testsuite: Run the testsuite.  Default --with debug is not to run it.

%if 0%{!?binutils_target:1}
%define binutils_target %{_target_platform}
%define isnative 1
%define enable_shared 1
%else
%define cross %{binutils_target}-
%define isnative 0
%define enable_shared 0
%endif

Summary: A GNU collection of binary utilities
Name: binutils
Version: 2.24
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Development/Tools
URL: http://sources.redhat.com/binutils/
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source2: binutils-2.19.50.0.1-output-format.sed
Patch01: binutils-2.20.51.0.2-libtool-lib64.patch
Patch02: binutils-2.20.51.0.10-ppc64-pie.patch
Patch03: binutils-2.20.51.0.2-ia64-lib64.patch
Patch04: binutils-2.20.51.0.2-version.patch
Patch05: binutils-2.20.51.0.2-set-long-long.patch
Patch06: binutils-2.20.51.0.10-copy-osabi.patch
Patch07: binutils-2.20.51.0.10-sec-merge-emit.patch
# Enable -zrelro by default: BZ #621983
Patch08: binutils-2.22.52.0.1-relro-on-by-default.patch
# Local patch - export demangle.h with the binutils-devel rpm.
Patch09: binutils-2.22.52.0.1-export-demangle.h.patch
# Disable checks that config.h has been included before system headers.  BZ #845084
Patch10: binutils-2.22.52.0.4-no-config-h-check.patch
# Fix addr2line to use the dynamic symbol table if it could not find any ordinary symbols.
Patch11: binutils-2.23.52.0.1-addr2line-dynsymtab.patch
Patch12: binutils-2.23.2-kernel-ld-r.patch
# Correct bug introduced by patch 12
Patch13: binutils-2.23.2-aarch64-em.patch
# Fix building opcodes library with -Werror=format-security
Patch14: binutils-2.24-s390-mkopc.patch
# Import fixes for IFUNC and PLT handling for AArch64.
Patch15: binutils-2.24-elfnn-aarch64.patch
# Fix decoding of abstract instance names using DW_FORM_ref_addr.
Patch16: binutils-2.24-DW_FORM_ref_addr.patch
# Backport from upstream fixes for BFD set macros issues.
# https://sourceware.org/ml/binutils/2014-01/msg00334.html
Patch17: binutils-2.24-replace-bfd-macro.patch
# Backport from upstream
# https://sourceware.org/bugzilla/show_bug.cgi?id=16428
Patch18: binutils-2.24-pr-ld-16428.patch

### include local configuration
%{?include_specopt}
### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpmbuild/specopt/binutils.specopt and edit it.
%{?!do_test: %global do_test 1}

%define gold_arches %ix86 x86_64

%ifarch %gold_arches
%define build_gold	both
%else
%define build_gold	no
%endif

%if 0%{?_with_debug:1}
# Define this if you want to skip the strip step and preserve debug info.
# Useful for testing.
%define __debug_install_post : > %{_builddir}/%{?buildsubdir}/debugfiles.list
%define debug_package %{nil}
%define run_testsuite %{do_test}
%else
%define run_testsuite %{do_test}
%endif

# Required for: ld-bootstrap/bootstrap.exp bootstrap with --static
# It should not be required for: ld-elf/elf.exp static {preinit,init,fini} array
%if %{run_testsuite}
BuildRequires: dejagnu, zlib-static, glibc-static, sharutils
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: texinfo >= 4.2, dejagnu, gettext, flex, bison
BuildRequires: zlib-devel
Conflicts: gcc-c++ < 4.0.0
Requires(post): info
Requires(preun): info
%ifarch ia64
Obsoletes: gnupro
%endif

# The higher of these two numbers determines the default ld.
%{!?ld_bfd_priority: %define ld_bfd_priority	50}
%{!?ld_gold_priority:%define ld_gold_priority	30}

%if "%{build_gold}" == "both"
Requires(post): coreutils
Requires(post): chkconfig
Requires(preun): chkconfig
%endif

# On ARM EABI systems, we do want -gnueabi to be part of the
# target triple.
%ifnarch %{arm}
%define _gnu %{nil}
%endif

%description
Binutils is a collection of binary utilities, including ar (for
creating, modifying and extracting from archives), as (a family of GNU
assemblers), gprof (for displaying call graph profile data), ld (the
GNU linker), nm (for listing symbols from object files), objcopy (for
copying and translating object files), objdump (for displaying
information from object files), ranlib (for generating an index for
the contents of an archive), readelf (for displaying detailed
information about binary files), size (for listing the section sizes
of an object or archive file), strings (for listing printable strings
from files), strip (for discarding symbols), and addr2line (for
converting addresses to file and line).

%package devel
Summary: BFD and opcodes static and dynamic libraries and header files
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Provides: binutils-static = %{version}-%{release}
Obsoletes: binutils-static < 2.21.53.0.1-2
Conflicts: binutils < 2.17.50.0.3-4
Requires(post): info
Requires(preun): info
Requires: zlib-devel

%description devel
This package contains BFD and opcodes static and dynamic libraries.

The dynamic libraries are in this package, rather than a seperate
base package because they are actually linker scripts that force
the use of the static libraries.  This is because the API of the
BFD library is too unstable to be used dynamically.

The static libraries are here because they are now needed by the
dynamic libraries.

Developers starting new projects are strongly encouraged to consider
using libelf instead of BFD.

%prep
%setup -q -n binutils-%{version}
%patch01 -p0 -b .libtool-lib64~
%patch02 -p0 -b .ppc64-pie~
%ifarch ia64
%if "%{_lib}" == "lib64"
%patch03 -p0 -b .ia64-lib64~
%endif
%endif
%patch04 -p0 -b .version~
%patch05 -p0 -b .set-long-long~
%patch06 -p0 -b .copy-osabi~
%patch07 -p0 -b .sec-merge-emit~
%if 0%{?fedora} >= 18 || 0%{?rhel} >= 7
%patch08 -p0 -b .relro~
%endif
%patch09 -p0 -b .export-demangle-h~
%patch10 -p0 -b .no-config-h-check~
%patch11 -p0 -b .addr2line~
%patch12 -p0 -b .kernel-ld-r~
%patch13 -p0 -b .aarch64~
%patch14 -p0 -b .mkopc~
%patch15 -p0 -b .elf-aarch64~
%patch16 -p0 -b .ref-addr~
%patch17 -p1 -b .replace-bfd-macro~
%patch18 -p1 -b .pr-ld-16428~

# We cannot run autotools as there is an exact requirement of autoconf-2.59.

# On ppc64 we might use 64KiB pages
sed -i -e '/#define.*ELF_COMMONPAGESIZE/s/0x1000$/0x10000/' bfd/elf*ppc.c
# LTP sucks
perl -pi -e 's/i\[3-7\]86/i[34567]86/g' */conf*
sed -i -e 's/%''{release}/%{release}/g' bfd/Makefile{.am,.in}
sed -i -e '/^libopcodes_la_\(DEPENDENCIES\|LIBADD\)/s,$, ../bfd/libbfd.la,' opcodes/Makefile.{am,in}
# Build libbfd.so and libopcodes.so with -Bsymbolic-functions if possible.
if gcc %{optflags} -v --help 2>&1 | grep -q -- -Bsymbolic-functions; then
sed -i -e 's/^libbfd_la_LDFLAGS = /&-Wl,-Bsymbolic-functions /' bfd/Makefile.{am,in}
sed -i -e 's/^libopcodes_la_LDFLAGS = /&-Wl,-Bsymbolic-functions /' opcodes/Makefile.{am,in}
fi
# $PACKAGE is used for the gettext catalog name.
sed -i -e 's/^ PACKAGE=/ PACKAGE=%{?cross}/' */configure
# Undo the name change to run the testsuite.
for tool in binutils gas ld
do
  sed -i -e "2aDEJATOOL = $tool" $tool/Makefile.am
  sed -i -e "s/^DEJATOOL = .*/DEJATOOL = $tool/" $tool/Makefile.in
done
touch */configure

%build
echo target is %{binutils_target}
export CFLAGS="$RPM_OPT_FLAGS"
CARGS=

case %{binutils_target} in i?86*|sparc*|ppc*|s390*|sh*|arm*)
  CARGS="$CARGS --enable-64-bit-bfd"
  ;;
esac

case %{binutils_target} in ia64*)
  CARGS="$CARGS --enable-targets=i386-linux"
  ;;
esac

case %{binutils_target} in ppc*|ppc64*)
  CARGS="$CARGS --enable-targets=spu"
  ;;
esac

%if 0%{?_with_debug:1}
CFLAGS="$CFLAGS -O0 -ggdb2"
%define enable_shared 0
%endif

# We could optimize the cross builds size by --enable-shared but the produced
# binaries may be less convenient in the embedded environment.
%configure \
  --build=%{_target_platform} --host=%{_target_platform} \
  --target=%{binutils_target} \
%ifarch %gold_arches
%if "%{build_gold}" == "both"
  --enable-gold=default --enable-ld \
%else
  --enable-gold \
%endif
%endif
%if !%{isnative}
  --enable-targets=%{_host} \
  --with-sysroot=%{_prefix}/%{binutils_target}/sys-root \
  --program-prefix=%{cross} \
%endif
%if %{enable_shared}
  --enable-shared \
%else
  --disable-shared \
%endif
  $CARGS \
  --enable-plugins \
  --with-bugurl=http://developer.momonga-linux.org/kagemai/
make %{_smp_mflags} tooldir=%{_prefix} all
make %{_smp_mflags} tooldir=%{_prefix} info


# Do not use %%check as it is run after %%install where libbfd.so is rebuild
# with -fvisibility=hidden no longer being usable in its shared form.
%if !%{run_testsuite}
echo ====================TESTSUITE DISABLED=========================
%else
make -k check < /dev/null || :
echo ====================TESTING=========================
cat {gas/testsuite/gas,ld/ld,binutils/binutils}.sum
echo ====================TESTING END=====================
for file in {gas/testsuite/gas,ld/ld,binutils/binutils}.{sum,log}
do
  ln $file binutils-%{_target_platform}-$(basename $file) || :
done
tar cjf binutils-%{_target_platform}.tar.bz2 binutils-%{_target_platform}-*.{sum,log}
uuencode binutils-%{_target_platform}.tar.bz2 binutils-%{_target_platform}.tar.bz2
rm -f binutils-%{_target_platform}.tar.bz2 binutils-%{_target_platform}-*.{sum,log}
%endif

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
mkdir -p %{buildroot}%{_prefix}
%makeinstall

%if %{isnative}
make prefix=%{buildroot}%{_prefix} infodir=%{buildroot}%{_infodir} install-info

# Rebuild libiberty.a with -fPIC.
# Future: Remove it together with its header file, projects should bundle it.
make -C libiberty clean
make CFLAGS="-g -fPIC $RPM_OPT_FLAGS" -C libiberty

# Rebuild libbfd.a with -fPIC.
# Without the hidden visibility the 3rd party shared libraries would export
# the bfd non-stable ABI.
make -C bfd clean
make CFLAGS="-g -fPIC $RPM_OPT_FLAGS -fvisibility=hidden" -C bfd

# Rebuild libopcodes.a with -fPIC.
make -C opcodes clean
make CFLAGS="-g -fPIC $RPM_OPT_FLAGS" -C opcodes

install -m 644 bfd/libbfd.a %{buildroot}%{_libdir}
install -m 644 libiberty/libiberty.a %{buildroot}%{_libdir}
install -m 644 include/libiberty.h %{buildroot}%{_prefix}/include
install -m 644 opcodes/libopcodes.a %{buildroot}%{_libdir}
# Remove Windows/Novell only man pages
rm -f %{buildroot}%{_mandir}/man1/{dlltool,nlmconv,windres}*

%if %{enable_shared}
chmod +x %{buildroot}%{_libdir}/lib*.so*
%endif

# Prevent programs from linking against libbfd and libopcodes
# dynamically, as they are change far too often.
rm -f %{buildroot}%{_libdir}/lib{bfd,opcodes}.so

# Remove libtool files, which reference the .so libs
rm -f %{buildroot}%{_libdir}/lib{bfd,opcodes}.la

# Sanity check --enable-64-bit-bfd really works.
grep '^#define BFD_ARCH_SIZE 64$' %{buildroot}%{_prefix}/include/bfd.h
# Fix multilib conflicts of generated values by __WORDSIZE-based expressions.
%ifarch %{ix86} x86_64 ppc ppc64 s390 s390x sh3 sh4 sparc sparc64 arm
sed -i -e '/^#include "ansidecl.h"/{p;s~^.*$~#include <bits/wordsize.h>~;}' \
    -e 's/^#define BFD_DEFAULT_TARGET_SIZE \(32\|64\) *$/#define BFD_DEFAULT_TARGET_SIZE __WORDSIZE/' \
    -e 's/^#define BFD_HOST_64BIT_LONG [01] *$/#define BFD_HOST_64BIT_LONG (__WORDSIZE == 64)/' \
    -e 's/^#define BFD_HOST_64_BIT \(long \)\?long *$/#if __WORDSIZE == 32\
#define BFD_HOST_64_BIT long long\
#else\
#define BFD_HOST_64_BIT long\
#endif/' \
    -e 's/^#define BFD_HOST_U_64_BIT unsigned \(long \)\?long *$/#define BFD_HOST_U_64_BIT unsigned BFD_HOST_64_BIT/' \
    %{buildroot}%{_prefix}/include/bfd.h
%endif
touch -r bfd/bfd-in2.h %{buildroot}%{_prefix}/include/bfd.h

# Generate .so linker scripts for dependencies; imported from glibc/Makerules:

# This fragment of linker script gives the OUTPUT_FORMAT statement
# for the configuration we are building.
OUTPUT_FORMAT="\
/* Ensure this .so library will not be used by a link for a different format
   on a multi-architecture system.  */
$(gcc $CFLAGS $LDFLAGS -shared -x c /dev/null -o /dev/null -Wl,--verbose -v 2>&1 | sed -n -f "%{SOURCE2}")"

tee %{buildroot}%{_libdir}/libbfd.so <<EOH
/* GNU ld script */

$OUTPUT_FORMAT

/* The libz dependency is unexpected by legacy build scripts.  */
/* The libdl dependency is for plugin support.  (BZ 889134)  */
INPUT ( %{_libdir}/libbfd.a -liberty -lz -ldl )
EOH

tee %{buildroot}%{_libdir}/libopcodes.so <<EOH
/* GNU ld script */

$OUTPUT_FORMAT

INPUT ( %{_libdir}/libopcodes.a -lbfd )
EOH

%else # !%{isnative}
# For cross-binutils we drop the documentation.
rm -rf %{buildroot}%{_infodir}
# We keep these as one can have native + cross binutils of different versions.
#rm -rf %{buildroot}%{_prefix}/share/locale
#rm -rf %{buildroot}%{_mandir}
rm -rf %{buildroot}%{_libdir}/libiberty.a
%endif # !%{isnative}

# This one comes from gcc
rm -f %{buildroot}%{_infodir}/dir
rm -rf %{buildroot}%{_prefix}/%{binutils_target}

%find_lang %{?cross}binutils
%find_lang %{?cross}opcodes
%find_lang %{?cross}bfd
%find_lang %{?cross}gas
%find_lang %{?cross}gprof
cat %{?cross}opcodes.lang >> %{?cross}binutils.lang
cat %{?cross}bfd.lang >> %{?cross}binutils.lang
cat %{?cross}gas.lang >> %{?cross}binutils.lang
cat %{?cross}gprof.lang >> %{?cross}binutils.lang

if [ -x ld/ld-new ]; then
  %find_lang %{?cross}ld
  cat %{?cross}ld.lang >> %{?cross}binutils.lang
fi
if [ -x gold/ld-new ]; then
  %find_lang %{?cross}gold
  cat %{?cross}gold.lang >> %{?cross}binutils.lang
fi

%if "%{build_gold}" == "both"
  rm -f %{buildroot}/%{_bindir}/%{?cross}ld
%endif

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
%if "%{build_gold}" == "both"
%{_sbindir}/alternatives --install %{_bindir}/%{?cross}ld %{?cross}ld \
  %{_bindir}/%{?cross}ld.bfd %{ld_bfd_priority}
%{_sbindir}/alternatives --install %{_bindir}/%{?cross}ld %{?cross}ld \
  %{_bindir}/%{?cross}ld.gold %{ld_gold_priority}
# workaround for bug of binutils <= 2.21.51.0.1-2m
# see http://developer.momonga-linux.org/kagemai/guest.cgi?action=view_report&id=314&project=momongaja
[ -L %{_bindir}/%{?cross}ld ] || %{_sbindir}/alternatives --auto %{?cross}ld
%endif
%if %{isnative}
/sbin/ldconfig
# For --excludedocs:
if [ -e %{_infodir}/binutils.info.gz -o -e %{_infodir}/binutils.info.bz2 ]
then
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/as.info
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/binutils.info
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/gprof.info
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/ld.info
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/standards.info
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/configure.info
fi
%endif # %{isnative}
exit 0

%preun
%if "%{build_gold}" == "both"
if [ $1 = 0 ]; then
  %{_sbindir}/alternatives --remove %{?cross}ld %{_bindir}/%{?cross}ld.bfd
  %{_sbindir}/alternatives --remove %{?cross}ld %{_bindir}/%{?cross}ld.gold
fi
%endif
%if %{isnative}
if [ $1 = 0 ] ;then
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/as.info
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/binutils.info
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/gprof.info
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/ld.info
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/standards.info
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/configure.info
fi
%endif
exit 0

%if %{isnative}
%postun -p /sbin/ldconfig

%post devel
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/bfd.info

%preun devel
if [ $1 = 0 ] ;then
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/bfd.info
fi
exit 0
%endif # %{isnative}

%files -f %{?cross}binutils.lang
%defattr(-,root,root,-)
%doc README
%{_bindir}/%{?cross}[!l]*
%if "%{build_gold}" == "both"
%{_bindir}/%{?cross}ld.*
%else
%{_bindir}/%{?cross}ld
%endif
%{_mandir}/man1/*
%if %{enable_shared}
%{_libdir}/lib*.so
%exclude %{_libdir}/libbfd.so
%exclude %{_libdir}/libopcodes.so
%endif
%if %{isnative}
%{_infodir}/[^b]*info*
%{_infodir}/binutils*info*

%files devel
%defattr(-,root,root,-)
%{_prefix}/include/*
%{_libdir}/lib*.a
%{_libdir}/libbfd.so
%{_libdir}/libopcodes.so
%{_infodir}/bfd*info*

%endif # %{isnative}

%changelog
* Sat Apr 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24-5m)
- import bug fix patch from upstream

* Thu Mar 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24-4m)
- import a fix from upstream
- enable testsuite by default

* Sat Feb  1 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24-3m)
- import bug fix patch from fedora

* Sun Jan 19 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24-2m)
- import two bugfix patches from fedora

* Thu Dec 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24-1m)
- update 2.24

* Tue May  7 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.23.2-2m)
- import fedora patches

* Tue Apr  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.23.2-1m)
- update 2.23.2-release

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.23.1-1m)
- update 2.23.1-release

* Sat Oct 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.23-1m)
- update 2.23-release

* Tue Nov 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.22-1m)
- update 2.22-release

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.90-4m)
- add binutils-2.21.90-HEAD.patch

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.90-3m)
- fix link-error

* Thu Oct 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.90-2m)
- merge from fedora
-- Rebuild libopcodes.a with -fPIC

* Thu Oct 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.90-1m)
- update 2.21.90

* Tue Aug  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.53.0.2-1m)
- update 2.21.53.0.2
-- remove unneed patch

* Sun Jul 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.53.0.1-2m)
- merge with fedora's binutils-2.21.53.0.1-3
-- import two patches
-- delete binutils-static package. see %%description for details
- revise %%post; "alternatives --auto" should be called only if there is no symlink

* Tue Jul 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.53.0.1-1m)
- update 2.21.53.0.1

* Thu Jul 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.52.0.2-3m)
- run "alternatives --auto" to restore ld symbolic link 
- support lto

* Wed Jul 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.52.0.2-2m)
- import bug fix patch for PR ld/12921

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.52.0.2-1m)
- update 2.21.52.0.2

* Thu Jun  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.52.0.1-1m)
- update 2.21.52.0.1

* Sat Jun  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21.51.0.8-2m)
- version down to 2.21.51.0.8, 2.21.51.0.9 does not work with i686

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21.51.0.9-1m)
- update to 2.21.51.0.9

* Mon Apr 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.51.0.8-1m)
- update to 2.21.51.0.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.51.0.7-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.51.0.7-1m)
- update to 2.21.51.0.7

* Fri Mar 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.51.0.6-2m)
- fix ranlib bug
-- see http://sourceware.org/bugzilla/show_bug.cgi?id=12590

* Mon Feb 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.51.0.6-1m)
- update to 2.21.51.0.6
- fix build-id bug

* Sat Dec 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21.51.0.2-2m)
- rollback to 2.21.51.0.2
- a part of KDE could not be built with 2.21.51.0.4

* Fri Dec 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.51.0.4-1m)
- update 2.21.51.0.4

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.51.0.2-1m)
- update 2.21.51.0.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.51.0.1-3m)
- fix bug in alternatives handling
-- see http://developer.momonga-linux.org/kagemai/guest.cgi?action=view_report&id=314&project=momongaja

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.21.51.0.1-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.51.0.1-1m)
- update 2.21.51.0.1

* Wed Sep 15 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.51.0.11-1m)
- update to 2.20.51.0.11
- delete merged patches
- import patches from fedora 2.20.51.0.11-1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.51.0.2-9m)
- full rebuild for mo7 release

* Sun May 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.51.0.2-8m)
- fix %%post failure 

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.51.0.2-7m)
- import Patch17,18 from Fedora 13 (2.20.51.0.2-18)
- split out static package

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.20.51.0.2-6m)
- fix Release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20.51.0.2-5m)
- use BuildRequires

* Tue Mar  9 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.51.0.2-4m)
- import two patches from rawhide

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.51.0.2-3m)
- fix %%preun

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.51.0.2-2m)
- Requires(post): chkconfig
- Requires(preun): chkconfig

* Fri Feb 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.51.0.2-1m)
- new version, including "gold" patch
-- merge changes from rawhide (2.20.51.0.2-14)

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19.51.0.14-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19.51.0.14-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19.51.0.14-4m)
- apply .ifunc-ld-s patch from Fedora 12 (2.19.51.0.14-35)

* Tue Oct 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19.51.0.14-3m)
- apply .cfi_sections support patch from Fedora 12 (2.19.51.0.14-33)

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19.51.0.14-2m)
- apply STB_GNU_UNIQUE patch from Rawhide (2.19.51.0.14-31)
-- https://bugzilla.redhat.com/show_bug.cgi?id=515700
- remove %%global _default_patch_fuzz 2

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.14-1m)
- update 2.19.51.0.14

* Thu Jun 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.9-2m)
- add Patch09: binutils-2.19.51.0.2-pr10255.patch
--    Patch10: binutils-2.19.51.0.2-sisreg.patch

* Mon Jun  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.9-1m)
- update 2.19.51.0.9

* Wed Jun  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.8-1m)
- update 2.19.51.0.8

* Mon Jun  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.6-1m)
- update 2.19.51.0.6

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.4-1m)
- update 2.19.51.0.4

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.3-1m)
- update 2.19.51.0.3

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19.51.0.2-2m)
- do not specify info file compression format

* Mon Feb  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.2-1m)
- update 2.19.51.0.2

* Wed Jan 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.51.0.1-1m)
- update 2.19.51.0.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19.50.0.1-2m)
- rebuild against rpm-4.6

* Thu Dec 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.50.0.1-1m)
- update 2.19.50.0.1

* Wed Oct 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.50.0.9-2m)
- enable compression feature explicitly

* Fri Sep  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.18.50.0.9-1m)
- update 2.18.50.0.9

* Tue Jul 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.50.0.8-1m)
- update 2.18.50.0.8

* Sat May 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.50.0.7-1m)
- update 2.18.50.0.7

* Mon Apr 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.50.0.6-1m)
- update 2.18.50.0.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.18.50.0.4-4m)
- rebuild against gcc43

* Fri Mar 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.50.0.4-3m)
- rollback binutils. unsupport any packages...

* Sat Mar 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.50.0.5-1m)
- update 2.18.50.0.5

* Wed Mar  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.50.0.4-2m)
- import fedora patches

* Mon Feb 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.50.0.4-1m)
- update 2.18.50.0.4

* Tue Dec 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.50.0.3-1m)
- update 2.18.50.0.3

* Fri Sep  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18-1m)
- update 2.18

* Mon Aug 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.18-1m)
- update 2.17.50.0.18
- licence change. GPLv3

* Wed Aug 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.17-4m)
- import fedora patch(2.17.50.0.17-7)

* Fri Jul  6 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.17.50.0.17-3m)
- fix %%{release} file name

* Tue Jul  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.17-2m)
- import Fedora patch(2.17.50.0.17-1)

* Fri Jun 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.17-1m)
- update 2.17.50.0.17

* Sat Jun 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.16-1m)
- update 2.17.50.0.16

* Wed Mar 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.12-2m)
- add Patch8: binutils-2.17.50.0.12-osabi.patch

* Mon Mar 12 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.17.50.0.12-1m)
- update 2.17.50.0.12

* Sat Jan 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.9-1m)
- update 2.17.50.0.9

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.17.50.0.8-3m)
- --enable-64-bit-bfd for ppc, sparc

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.8-2m)
- add patch
-- * Sat Dec 23 2006 Jakub Jelinek <jakub@redhat.com> 2.17.50.0.8-2
-- - fix --as-needed on ppc64 (#219629)

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.8-1m)
- update 2.17.50.0.8

* Sat Oct  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.3-3m)
- merge from TUPPA4RI branch

* Sat Oct  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.3-2m)
- merge from trunk
-- * Fri Sep 29 2006 Masayuki SANO <nosanosa@momonga-linux.org>
-- - (2.17-2m)
-- - added demangle.h to the package (it is needed to use some functions in libiberty.a)

* Fri Sep  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.50.0.3-1m)
- update 2.17.50.0.3

* Tue Aug 15 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17-2m)
- add binutils-hash-style.patch(Patch100)
-- support DT_GNU_HASH

* Wed Jun 27 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17-1m)
- update 2.17

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.94-1m)
- update 2.16.94

* Thu May 25 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.16.91.0.6-3m)
- [SECURITY] CVE-2006-2362
- add Patch11: binutils-2.16.91.0.6-pr2584.patch

* Thu Mar  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.91.0.6-2m)
- sync FC-devel 

* Wed Feb 15 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.16.91.0.6-1m)
- update 2.16.91.0.6

* Sun Feb 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.16.91.0.5-2m)
- fix for ppc64 biarch.

* Tue Jan 17 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.91.0.5-1m)
- update 2.16.91.0.5

* Sun Dec 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.91.0.3-1m)
- update 2.16.91.0.3
- (sync with Fedora 2.16.91.0.3-2)

* Tue Oct 18 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.16.91.0.2-1m)
- version up (sync with Fedora 2.16.91.0.2-4)

* Sun Jul 17 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.15.92.0.2-4m)
- maki modosi 2.15.92.0.2
- because build error any packages.

* Mon Jun 20 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.90.0.3-3m)
- add binutils-relocate-section.patch . Because build error gcc4.

* Sat Jun 18 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.90.0.3-2m)
- import Fedora-devel patchs(2.16.90.0.3-1)

* Mon May 23 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.90.0.3-1m)
- update 2.16.90.0.3

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.15.92.0.2-3m)
- remove cygwin-cross patch

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.15.92.0.2-2m)
- fix for x86_64 -m32 library.
  - another cpu's cross-compile is not work.... fixme.

* Sat Feb  5 2005 Toru Hoshina <tmomonga-linux.org>
- (2.15.92.0.2-1m)
- ver up.

* Thu Sep  9 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.15.90.0.3-1m)
- version down since version 2.15.91 is very unstable

* Fri Sep  3 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.15.91.0.2-3m)
- revive --enable-64-bit-bfd

* Thu Sep 02 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.91.0.2-2.1m)
- revive --enable-64-bit-bfd at configure 
  for avoid ld said "Value too large for defined data type" and exit

* Sun Aug 22 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.15.91.0.2-2m)
- add patches from Fedora (2.15.91.0.2-8)
- * Wed Aug 16 2004 Jakub Jelinek <jakub@redhat.com> 2.15.91.0.2-8
- - fix linker segfaults on input objects with SHF_LINK_ORDER with
-   incorrect sh_link (H.J.Lu, Nick Clifton, #130198, BZ #290)
- 
- * Wed Aug 16 2004 Jakub Jelinek <jakub@redhat.com> 2.15.91.0.2-7
- - resolve all undefined ppc64 .* syms to the function bodies through
-   .opd, not just those used in brach instructions (Alan Modra)
- 
- * Tue Aug 16 2004 Jakub Jelinek <jakub@redhat.com> 2.15.91.0.2-6
- - fix ppc64 ld --dotsyms (Alan Modra)
- 
- * Tue Aug 16 2004 Jakub Jelinek <jakub@redhat.com> 2.15.91.0.2-5
- - various ppc64 make check fixes when using non-dot-syms gcc (Alan Modra)
- - fix --gc-sections
- - on ia64 create empty .gnu.linkonce.ia64unw*.* sections for
-   .gnu.linkonce.t.* function doesn't need unwind info
- 
- * Mon Aug 16 2004 Jakub Jelinek <jakub@redhat.com> 2.15.91.0.2-4
- - kill ppc64 dot symbols (Alan Modra)
- - objdump -d support for objects without dot symbols
- - support for overlapping ppc64 .opd entries
- 
- * Mon Aug 9 2004 Jakub Jelinek <jakub@redhat.com> 2.15.91.0.2-3
- - fix a newly introduced linker crash on x86-64
- 
- * Sun Aug 8 2004 Alan Cox <alan@redhat.com> 2.15.91.0.2-2
- - BuildRequire bison and macroise buildroot - from Steve Grubb
- 
- * Fri Jul 30 2004 Jakub Jelinek <jakub@redhat.com> 2.15.91.0.2-1
- - update to 2.15.91.0.2
- - BuildRequire flex (#117763)

* Tue Aug 10 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.15.91.0.2-1m)
- version up
- disable 64-bit-bfd because it causes "Not enough room for program headers" error on ld
- cancel "rm -f c++filt" (gcc-3.4.1 does not contain it)

* Mon Mar 22 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.14.90.0.7-4m)
- install libiberty.info and as.info
- install-info as.info again.

* Mon Mar 22 2004 TAKAHASHI Tamotsu <tamo>
- (2.14.90.0.7-4m)
- comment out "install-info as.info"
 because as.info.bz2 is not installed

* Sat Mar 20 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.14.90.0.7-3m)
- change docdir %%defattr

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.14.90.0.7-2m)
- revised spec for enabling rpm 4.2.

* Thu Nov 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.14.90.0.7-1m)
- version up to 2.14.90.0.7
- update patch6 sparc-cfi.patch
- comment out patch7, included and changed
- tmp comment out patch8, many changes...
- tmp comment out patch10, mendoi

* Fri Oct 24 2003 Kenta MURATA <muraken2@nifty.com>
- (2.14.90.0.6-1m)
- version up to 2.14.90.0.6.
- split devel package.
-
-* Tue Sep 30 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.6-3
-- don't abort on some linker warnings/errors on IA-64
-
-* Sat Sep 20 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.6-2
-- fix up merge2.s to use .p2align instead of .align
-
-* Sat Sep 20 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.6-1
-- update to 2.14.90.0.6
-- speed up string merging (Lars Knoll, Michael Matz, Alan Modra)
-- speed up IA-64 local symbol handling during linking
-
-* Fri Sep  5 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.5-7
-- avoid ld -s segfaults introduced in 2.14.90.0.5-5 (Dmitry V. Levin,
-  #103180)
-
-* Fri Aug 29 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.5-6
-- build old demangler into libiberty.a (#102268)
-- SPARC .cfi* support
-
-* Tue Aug  5 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.5-5
-- fix orphan section placement
-
-* Tue Jul 29 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.5-4
-- fix ppc64 elfvsb linker tests
-- some more 64-bit cleanliness fixes, give ppc64 fdesc symbols
-  type and size (Alan Modra)
-
-* Tue Jul 29 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.5-3
-- fix 64-bit unclean code in ppc-opc.c
-
-* Mon Jul 28 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.5-2
-- fix 64-bit unclean code in tc-ppc.c
-
-* Mon Jul 28 2003 Jakub Jelinek <jakub@redhat.com> 2.14.90.0.5-1
-- update to 2.14.90.0.5
-- fix ld -r on ppc64 (Alan Modra)

* Thu Oct 23 2003 Kenta MURATA <muraken2@nifty.com>
- (2.14.90.0.4-4m)
- default enable 64-bit-bfd.

* Sun Oct 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.14.90.0.4-3m)
- add --enable-targets=all at configure

* Wed Oct  1 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.14.90.0.4-2m)
- remove binutils-2.13.90.0.4-glibc21.patch
- update sparc-nonpic patch
- remove binutils-2.13.90.0.18-compatsym.patch
- sync with RawHide(binutils-2.14.90.0.4-7)
- fix libbfd.la and libopcodes.la

* Tue Jun  3 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.14.90.0.4-1m)
  update to 2.14.90.0.4

* Wed May 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.13.90.0.20-2m)
- rewind
- revise %%post and %%preun (.info.* -> .info)

* Sun May 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.14.90.0.1-1m)
- update to 2.14.90.0.1
- update Patch103: binutils-2.14.90.0.1-eh-frame-ro.patch

* Thu Apr 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13.90.0.20-1m)
- update to 2.13.90.0.20
- delete patch2
- delete patch100
- delete patch106
- delete patch107
- delete patch108
- delete patch109
- delete patch110

* Tue Feb 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13.90.0.18-2m)
- apply patches from rawhide 2.13.90.0.18-2
- delete rawhide's Patch102: binutils-2.13.90.0.18-ia64-brl.patch
  because this patch was same Patch2: binutils-2.13.90.0.10-ia64-brl.patch
- update momonga Patch1: binutils-2.11.93.0.2-sparc-nonpic.patch
  by rawhide's Patch101: binutils-2.13.90.0.18-sparc-nonpic.patch
- rawhide's changelog is below
- * Sun Feb 09 2003 Jakub Jelinek <jakub@redhat.com> 2.13.90.0.18-2
- - fix SEARCH_DIR on x86_64/s390x
- - fix Alpha --relax
- - create DT_RELA{,SZ,ENT} on s390 even if there is just .rela.plt
-   and no .rela.dyn section
- - support IA-32 on IA-64 (#83752)
- - .eh_frame_hdr fix (Andreas Schwab)
- 
- * Thu Feb 06 2003 Jakub Jelinek <jakub@redhat.com> 2.13.90.0.18-1
- - update to 2.13.90.0.18 + 20030121->20030206 CVS diff

* Sun Feb 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13.90.0.18-1m)
- update to 2.13.90.0.18
- delete included patches
-  Patch3: binutils-2.13.90.0.16-rodata-cst.patch
-  Patch5: binutils-2.13.90.0.16-ppc-apuinfo.patch
-  Patch6: binutils-2.13.90.0.16-stt_tls.patch
-  Patch8: binutils-2.13.90.0.16-tls-strip.patch
- ? Patch4: binutils-2.13.90.0.16-eh-frame-ro.patch
- ? Patch7: binutils-2.13.90.0.16-ia64-bootstrap.patch

* Wed Jan 22 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.13.90.0.16-2m)
- more sync with binutils-2.13.90.0.16-3(rawhide).
  add --enable-64-bit-bfd to configure if target arch is sparc ppc s390

* Sun Dec 15 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13.90.0.16-1m)
- update to 2.13.90.0.16
- add Patch from 2.13.90.0.16-3vl2 from rawhide
- comment out Patch1 (binutils-2.13.90.0.16-oformat.patch)
- comment out Patch5 (binutils-2.13.90.0.10-biarch.patch)
- comment out included tarball Patch6 (binutils-2.13.90.0.10-alpha.patch)

* Fri Nov 22 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.13.90.0.10-2m)
- import patches from rawhide (2.13.90.0.10-3).
  imported patches are 
    o binutils-2.13.90.0.10-ia64-brl.patch
    o binutils-2.13.90.0.10-biarch.patch
    o binutils-2.13.90.0.10-alpha.patch
  and, here is the corresponding changelog from rawhide
  (changes I did not import are deleted):

  * Wed Oct 23 2002 Jakub Jelinek <jakub@redhat.com> 2.13.90.0.10-3
  - fix names and content of alpha non-alloced .rela.* sections (#76583)

  * Tue Oct 15 2002 Jakub Jelinek <jakub@redhat.com> 2.13.90.0.10-2
  - enable s390x resp. s390 emulation in linker too

  * Mon Oct 14 2002 Jakub Jelinek <jakub@redhat.com> 2.13.90.0.10-1
  - update to 2.13.90.0.10
  - add a bi-arch patch for sparc/s390/x86_64

* Sat Oct 12 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.13.90.0.10-1m)
- update to 2.13.90.0.10

* Fri Aug 16 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.13.90.0.4-1m)
- update to 2.13.90.0.4
- remake Patch1 for 2.13.90.0.4

* Thu Aug 15 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.13.90.0.3-1m)
- update to 2.13.90.0.3
- add BuildPrereq: byacc, flex

* Wed Aug  7 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.13.90.0.2-1m)
  update to 2.13.90.0.2

* Wed Jul 24 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.12.90.0.9-1m)
  update to 2.12.90.0.9

* Thu May 30 2002 Toru Hoshina <t@kondara.org>
- (2.12.90.0.7-4k)
- no gasp.info.

* Tue May 21 2002 TABUCHI Takaaki <tab@kondara.org>
- (2.12.90.0.7-3k)
- update to 2.12.90.0.7

* Wed Mar 13 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.12-2k)
- update to 2.12

* Mon Feb 24 2002 Shingo Akagaki <dora@kondara.org>
- (2.11.92.0.10-4k)
- just rebuild for .la fix

* Sat Nov  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11.92.0.10-2k)
- update to 2.11.92.0.10

* Wed Oct 24 2001 Toru Hoshina <t@kondara.org>
- (2.11.90.0.31-4k)

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (2.11.90.0.31-2k)
- merge from Jirai!!
- DO NOT REMOVE libiberty.a

* Mon Sep 18 2001 Motonobu Ichimura <famao@kondara.org>
- (2.11.90.0.31-3k)
- up to 2.11.90.0.31

* Mon Jul 09 2001 Kusunoki Masanori <nori@kondara.org>
- (2.11.2-5k)
- up to 2.11.2
- remove libiberty.a because conflict gcc
- remove libiberty.h

* Tue Nov 21 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- spec's bug fix

* Mon Nov 20 2000 Takaaki Tabuchi <tab@kondara.org>
- (2.10.0.33-1k)
- update to 2.10.0.33
- change source URI from .bz2 to .gz

* Wed Oct 25 2000 Daiki Matsuda <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Jul 24 2000 Jakub Jelinek <jakub@redhat.com>
- 2.10.0.18

* Mon Jul 10 2000 Jakub Jelinek <jakub@redhat.com>
- 2.10.0.12

* Mon Jun 26 2000 Jakub Jelinek <jakub@redhat.com>
- 2.10.0.9

* Thu Jun 15 2000 Jakub Jelinek <jakub@redhat.com>
- fix ld -r

* Mon Jun  5 2000 Jakub Jelinek <jakub@redhat.com>
- 2.9.5.0.46
- use _mandir/_infodir/_lib

* Mon May  8 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.9.5.0.41

* Wed Apr 12 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.9.5.0.34

* Wed Mar 22 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.9.5.0.31

* Fri Feb 04 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed
- apply kingdon's patch from #5031

* Wed Jan 19 2000 Jeff Johnson <jbj@redhat.com>
- Permit package to be built with a prefix other than /usr.

* Thu Jan 13 2000 Cristian Gafton <gafton@redhat.com>
- add pacth from hjl to fix the versioning problems in ld

* Tue Jan 11 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Add sparc patches from Jakub Jelinek <jakub@redhat.com>
- Add URL:

* Tue Dec 14 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.9.5.0.22

* Wed Nov 24 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.9.5.0.19

* Sun Oct 24 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.9.5.0.16

* Mon Sep 06 1999 Jakub Jelinek <jj@ultra.linux.cz>
- make shared non-pic libraries work on sparc with glibc 2.1.

* Fri Aug 27 1999 Jim Kingdon
- No source/spec changes, just rebuilding with egcs-1.1.2-18 because
  the older egcs was miscompling gprof.

* Mon Apr 26 1999 Cristian Gafton <gafton@redhat.com>
- back out very *stupid* sparc patch done by HJLu. People, keep out of
  things you don't understand.
- add alpha relax patch from rth

* Mon Apr 05 1999 Cristian Gafton <gafton@redhat.com>
- version  2.9.1.0.23
- patch to make texinfo documentation compile
- auto rebuild in the new build environment (release 2)

* Tue Feb 23 1999 Cristian Gafton <gafton@redhat.com>
- updated to 2.9.1.0.21
- merged with UltraPenguin

* Mon Jan 04 1999 Cristian Gafton <gafton@redhat.com>
- added ARM patch from philb
- version 2.9.1.0.19a
- added a patch to allow arm* arch to be identified as an ARM

* Thu Oct 01 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.9.1.0.14.

* Sat Sep 19 1998 Jeff Johnson <jbj@redhat.com>
- updated to 2.9.1.0.13.

* Wed Sep 09 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.9.1.0.12

* Thu Jul  2 1998 Jeff Johnson <jbj@redhat.com>
- updated to 2.9.1.0.7.

* Wed Jun 03 1998 Jeff Johnson <jbj@redhat.com>
- updated to 2.9.1.0.6.

* Tue Jun 02 1998 Erik Troan <ewt@redhat.com>
- added patch from rth to get right offsets for sections in relocateable
  objects on sparc32

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue May 05 1998 Cristian Gafton <gafton@redhat.com>
- version 2.9.1.0.4 is out; even more, it is public !

* Tue May 05 1998 Jeff Johnson <jbj@redhat.com>
- updated to 2.9.1.0.3.

* Mon Apr 20 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.9.0.3

* Tue Apr 14 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 2.9.0.2

* Sun Apr 05 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.8.1.0.29 (HJ warned me that this thing is a moving target...
  :-)
- "fixed" the damn make install command so that all tools get installed

* Thu Apr 02 1998 Cristian Gafton <gafton@redhat.com>
- upgraded again to 2.8.1.0.28 (at least on alpha now egcs will compile)
- added info packages handling

* Tue Mar 10 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 2.8.1.0.23

* Mon Mar 02 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.8.1.0.15 (required to compile the newer glibc)
- all patches are obsoleted now

* Wed Oct 22 1997 Erik Troan <ewt@redhat.com>
- added 2.8.1.0.1 patch from hj
- added patch for alpha palcode form rth
