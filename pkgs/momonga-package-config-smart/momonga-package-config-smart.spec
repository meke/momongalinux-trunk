%global momorel 7

Summary: Momonga configuration files for the Smart package manager
Name: momonga-package-config-smart
Version: 4
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Base
URL: http://dist.momonga-linux.org/
Source0: %{name}-%{version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: smart
Provides: smart-config

%description
Momonga configuration files for the Smart package manager.

%prep
%setup -q
# place the proper arch in the folders
for filein in *.channel.in; do
  file=`echo $filein | sed -e's,\.in$,,'`
  sed -e's,@ARCH@,%{_target_cpu},g' < $filein > $file
done

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/smart/channels
install -p -m 0644 *.channel %{buildroot}%{_sysconfdir}/smart/channels

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_sysconfdir}/smart
%dir %{_sysconfdir}/smart/channels
%config(noreplace) %{_sysconfdir}/smart/channels/*.channel

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4-2m)
- rebuild against gcc43

* Sun Dec  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4-1m)
- initial Momonga package for smart based on fedora-package-config-smart.spec

* Sun Sep 23 2007 Ville Skytta <ville.skytta at iki.fi> - 8-10
- Update for Fedora 8.
- Add empty %%build section.
- Improve summary and description.
- License: GPL+
- Update URL.

* Fri Jun  1 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 7.89-9
- Update to post-F7 rawhide.

* Fri Jun  1 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 7-8
- Update to Fedora 7.

* Thu Oct 26 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 6.89-7
- Update to post-FC6 rawhide.

* Sat May 13 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.89-7
- Sync with fedora-package-config-apt, see also #191580.

* Thu Apr 20 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.89-6
- Update to rawhide config.

* Thu Apr 20 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5-5
- Add virtual smart-config provides (#175630 comment 13).

* Sat Apr  1 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5-4
- Update to Fedora Core 5.

* Tue Dec 13 2005 Axel Thimm <Axel.Thimm@ATrpms.net> - 4-3
- changed name to fedora-package-config-smart
- Modify to only include what fedora-release includes for yum.

* Thu Jul 14 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Merge srpms of atrpms-package-config and medley-package-config.

* Sat Jun 18 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Add el3/el4/fc4 repos.

* Fri Dec 17 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Add smart support.

* Tue Nov 16 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Split yum config files for yum 2.0.x and yum >= 2.1.x.

* Tue Nov  9 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Add fc3 support.

* Tue Sep 28 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Add fedoralegacy repo for FC1.
- Prapare commented entries for fedoralegacy repo for FC2.

* Fri Sep  3 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Rename apt.physik.fu-berlin.de to apt.atrpms.net

* Tue Aug 17 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Change main dries repo to lower load on studentenweb.org.

* Thu Jul 29 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Fork ATrpms-only package, former atrpms-package-config
  package has been renamed to medley-package.config.

* Sat Jul 24 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Fix nr entries for production.
- Add freshrpms' x86_64 entries.

* Fri May 28 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Add RPM::Order "true" to apt.conf.

* Mon May 24 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Added back Gstreamer, now we know where it went to.
- New fc2 list officially published.

* Thu May  6 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Add fedoralegacy repos where appropriate.

* Fri Apr  2 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Remove nonfree sections of BIOrpms, as these were accidentially activated.

* Mon Mar 15 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Change in Matt's repo structure.

* Thu Mar 11 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Interrepo problems with Dag's obsoletes.

* Fri Mar  5 2004 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Split off package from atrpms package (check there for prior logs).

