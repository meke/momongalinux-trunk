%global momorel 3
%global snapver 20120611

%global stable_release_version 7
# %%global stable_release_flavor beta
%global stable_release_name Natsuki

%global trunk_release_version trunk
%global trunk_release_name Sinji

Name:           momonga-release

%{?include_specopt}
%{?!build_stable: %global build_stable 0}

%if %{build_stable}
%global release_version %{stable_release_version}
%global release_flavor %{?stable_release_flavor:%{stable_release_flavor}}
%global release_name %{stable_release_name}
%else
%define release_version %{trunk_release_version}
%define release_name %{trunk_release_name}
%endif

Summary:        Momonga Linux release file
Version:        8 
Release:        %{?snapver:0.%{snapver}.}%{momorel}m%{?dist}
License:        GPLv2
Group:          System Environment/Base
URL:            http://www.momonga-linux.org
Source:         %{name}-%{release_version}.tar.bz2
Obsoletes:      redhat-release
Provides:       redhat-release
Provides:       system-release = %{version}-%{release}
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

%description
Momonga release files such as yum configs and various /etc/ files that
define the release.

%package development 
Summary:        development repo definitions
Requires:       momonga-release = %{version}-%{release}

%description development
This package provides the development repo definitions.

%prep
%setup -q -n %{name}-%{release_version}

%build

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
install -d %{buildroot}/etc
echo "Momonga Linux release %{release_version} (%{release_name})%{?release_flavor: %{release_flavor}}" > %{buildroot}%{_sysconfdir}/momonga-release
echo "cpe:/o:momongaproject:momonga:%{version}" > %{buildroot}/etc/system-release-cpe
cp -p %{buildroot}%{_sysconfdir}/momonga-release %{buildroot}%{_sysconfdir}/issue
echo "Kernel \r on an \m (\l)" >> %{buildroot}%{_sysconfdir}/issue
cp -p %{buildroot}%{_sysconfdir}/issue %{buildroot}%{_sysconfdir}/issue.net
echo >> %{buildroot}%{_sysconfdir}/issue
ln -s momonga-release %{buildroot}%{_sysconfdir}/redhat-release
ln -s momonga-release %{buildroot}%{_sysconfdir}/system-release

cat << EOF >>$RPM_BUILD_ROOT/etc/os-release
NAME=Momonga
VERSION="%{release_version} (%{release_name})"
ID=momonga
VERSION_ID=%{release_version}
PRETTY_NAME="Momonga Linux %{release_version} (%{release_name})"
ANSI_COLOR="0;30"
CPE_NAME="cpe:/o:momongaproject:momonga:%{release_version}"
HOME_URL="https://www.momonga-linux.org/"
BUG_REPORT_URL="https://kagemai.momonga-linux.org/"
EOF

install -d -m 755 %{buildroot}/etc/pki/rpm-gpg

install -m 644 RPM-GPG-KEY* %{buildroot}/etc/pki/rpm-gpg/

# Install all the keys, link the primary keys to primary arch files
# and to compat generic location
pushd %{buildroot}/etc/pki/rpm-gpg/
for arch in i686 x86_64
do
ln -s RPM-GPG-KEY-momonga-%{release_version}-primary RPM-GPG-KEY-momonga-$arch
done
ln -s RPM-GPG-KEY-momonga-%{release_version}-primary RPM-GPG-KEY-momonga
#for arch in sparc sparc64
#do
#ln -s RPM-GPG-KEY-momonga-%{release_version}-SPARC RPM-GPG-KEY-momonga-$arch
#done

popd

install -d -m 755 %{buildroot}/etc/yum.repos.d
for file in momonga*repo ; do
install -m 644 $file %{buildroot}/etc/yum.repos.d
done

## Set up the dist tag macros
#install -d -m 755 %{buildroot}/etc/rpm
#cat >> %{buildroot}/etc/rpm/macros.dist << EOF
## dist macros.
#
#%%momonga               %{dist_version}
#%%dist         .mo%{dist_version}
#%%mo%{dist_version}            1
#EOF
 
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc GPL
%config %attr(0644,root,root) /etc/momonga-release
/etc/redhat-release
/etc/system-release
%config %attr(0644,root,root) /etc/os-release
%config %attr(0644,root,root) /etc/system-release-cpe
%dir /etc/yum.repos.d
%config(noreplace) /etc/yum.repos.d/momonga.repo
%config(noreplace) /etc/yum.repos.d/momonga-updates*.repo
%config(noreplace) /etc/yum.repos.d/momonga-local-devel.repo
%config(noreplace) %attr(0644,root,root) /etc/issue
%config(noreplace) %attr(0644,root,root) /etc/issue.net
# %config %attr(0644,root,root) /etc/rpm/macros.dist
%dir /etc/pki/rpm-gpg
/etc/pki/rpm-gpg/*

%files development
%defattr(-,root,root,-)
%config(noreplace) /etc/yum.repos.d/momonga-devel.repo

%changelog
* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20120611.3m)
- update os-release syntax

* Mon Nov 18 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (8-0.20120611.2m)
- add os-release syntax

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20120611.1m)
- fixed os-release syntax

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-0.20120401.1m)
- add os-release file

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7-2m)
- rebuild for new GCC 4.5

* Sat Sep 11 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (7-1m)
- Momonga Linux 7

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7-0.20100830.2m)
- full rebuild for mo7 release

* Mon Aug 30 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (7-0.20100830.1m)
- Momonga Linux 7 beta 3

* Wed Aug 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (7-0.20100825.1m)
- Momonga Linux 7 beta 2

* Thu Aug 19 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (7-0.20100819.1m)
- Momonga Linux 7 beta 1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (6-1m)
- Momonga Linux 6

* Sun Jul 20 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (6-0.20090720.1m)
- Momonga Linux 6 beta 2

* Sun Jul 12 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (6-0.20090712.1m)
- Momonga Linux 6 beta 1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5-3m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (5-2m)
- add /etc/system-release

* Fri Oct 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5-1m)
- Momonga Linux 5 

* Tue Sep 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5-0.20080923.1m)
- Momonga Linux 5 beta 2

* Tue Sep 02 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5-0.20080902.1m)
- Momonga Linux 5 beta 1

* Mon Aug 18 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5-0.20080818.1m)
- Momonga Linux 5 alpha 2

* Tue Aug  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5-0.20080805.1m)
- Momonga Linux 5 alpha 1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5-0.20070812.2m)
- rebuild against gcc43

* Sun Aug 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5-0.20070812.1m)
- for trunk

* Sun Aug 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4-1m)
- Momonga Linux 4
- update GPL

* Sat Jun 30 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4-0.20070630.1m)
- Momonga Linux 4 beta 1

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4-0.20070617.1m)
- Momonga Linux 4 pre alpha

* Sat Aug 12 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3-1m)
- Momonga Linux 3 Release

* Thu Jun 22 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3-0.20060622.1m)
- Momonga Linux 3 alpha

* Sun Jun  4 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3-0.20060604.1m)
- Momonga Linux 3 pre alpha

* Fri Apr 15 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2-20050415m)
- add RPM-GPG-KEY-momonga

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1-20050226m)
- support new yum (yum.repos.d)

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1-20050219m)
- Momonga LInux development (Sinji)
  (Still IN Jirai)

* Fri Aug 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1-2m)

* Wed Jul 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1-1m)
- Momonga Linux release 1 (Kaede)

* Sat Jul 17 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1-0.3m)
- Momonga Linux release 1 (Kaede) beta2

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1-0.2m)
- remove Epoch

* Tue Jul  6 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1-0.1m)
- Momonga Linux 1 beta1
