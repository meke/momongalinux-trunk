%global momorel 5

Name:		tokyotyrant
Summary:	network interface of Tokyo Cabinet
Version:	1.1.41
Release:	%{momorel}m%{?dist}
License:	LGPLv2+
Group:		Development/Libraries
URL:		http://fallabs.com/tokyotyrant/
Source0:	http://fallabs.com/tokyotyrant/%{name}-%{version}.tar.gz 
NoSource: 	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:	tokyocabinet
BuildRequires:	tokyocabinet >= 1.4.46

%description
Tokyo Tyrant is a package of network interface to the DBM called Tokyo Cabinet.
Though the DBM has high performance, you might bother in case that multiple
processes share the same database, or remote processes access the database.

%prep
%setup -q

%build
%configure --libexecdir=%{_libdir}
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%{_bindir}/*
%{_sbindir}/*
%{_includedir}/*
%{_libdir}/lib*
%{_libdir}/tt*.so
%{_libdir}/pkgconfig/*.pc
%{_mandir}/*/*
%{_datadir}/%{name}

%changelog
* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.41-5m)
- change URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.41-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.41-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.41-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.41-1m)
- update 1.2.41

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.40-1m)
- update 1.2.40

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.36-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.36-1m)
- update 1.2.36

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.35-1m)
- update 1.2.35

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.29-1m)
- Initial commit Momonga Linux

