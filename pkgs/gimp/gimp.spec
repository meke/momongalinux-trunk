%global momorel 1
%global libver 2.0
%global ver 2.8
%global helpver 2.6.0

Summary: The GNU Image Manipulation Program
Name: gimp
Version: 2.8.10
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Multimedia
URL: http://www.gimp.org/
Source0: ftp://ftp.gimp.org/pub/%{name}/v%{ver}/%{name}-%{version}.tar.bz2
NoSource: 0
Source10: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-de.tar.bz2
Source11: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-en.tar.bz2
Source12: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-es.tar.bz2
Source13: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-fr.tar.bz2
Source14: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-it.tar.bz2
Source15: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-ja.tar.bz2
Source16: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-ko.tar.bz2
Source17: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-nl.tar.bz2
Source18: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-nn.tar.bz2
Source19: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-pl.tar.bz2
Source20: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-ru.tar.bz2
Source21: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-sv.tar.bz2
Source22: ftp://ftp.gimp.org/pub/gimp/help/gimp-help-%{helpver}-html-zh_CN.tar.bz2
NoSource: 10
NoSource: 11
NoSource: 12
NoSource: 13
NoSource: 14
NoSource: 15
NoSource: 16
NoSource: 17
NoSource: 18
NoSource: 19
NoSource: 20
NoSource: 21
NoSource: 22
Patch0: gimp-2.6.4-asflags.patch
Patch1: gimp-2.7.4-gimp.pc.patch
Patch2: gimp-2.8.10-freetype-include-madness.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: autoconf >= 2.56-3m
BuildRequires: webkitgtk-devel >= 1.6.1
BuildRequires: alsa-lib-devel
BuildRequires: atk-devel
BuildRequires: babl-devel >= 0.1.10
BuildRequires: cairo-devel >= 1.10.2
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: expat-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gegl-devel >= 0.2.0
BuildRequires: glib2-devel
BuildRequires: glibc-devel
BuildRequires: glitz-devel
BuildRequires: gtk-doc >= 1.11-3m
BuildRequires: gtk2-devel
# BuildRequires: gutenprint-devel >= 5.0.1
BuildRequires: lcms-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdamage-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXmu-devel
BuildRequires: libXpm-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libXt-devel
BuildRequires: libexif-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libmng-devel
BuildRequires: libpng-devel
BuildRequires: librsvg2-devel >= 2.34.2
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libwmf-devel >= 0.2.8
BuildRequires: libxcb-devel
BuildRequires: pango-devel
BuildRequires: pixman-devel
BuildRequires: poppler-glib-devel >= 0.18.2
BuildRequires: xcb-util-devel
BuildRequires: zlib-devel >= 1.1.4-5m
Obsoletes: gimp-help < 2.6.7-5m

## include local configuration
%{?include_specopt}
# If you want to change default configuration, please copy this to
# ${HOME}/rpm/specopt/gimp.specopt and edit it.
%{?!python_scripting:         %global python_scripting         1}
%if %{python_scripting}
BuildRequires: pygtk2-devel
%endif

%description
GIMP is a multi-platform photo manipulation tool. GIMP is an acronym
for GNU Image Manipulation Program. The GIMP is suitable for a variety
of image manipulation tasks, including photo retouching, image
composition, and image construction.

It has many capabilities. It can be used as a simple paint program, an
expert quality photo retouching program, an online batch processing
system, a mass production image renderer, an image format converter,
etc.

GIMP is expandable and extensible. It is designed to be augmented with
plug-ins and extensions to do just about anything. The advanced
scripting interface allows everything from the simplest task to the
most complex image manipulation procedures to be easily scripted.

%package devel
Summary: GIMP plugin and extension development kit
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: freetype-devel
Requires: gimp-print-devel
Requires: gtk2-devel
Requires: libart_lgpl-devel
Requires: libgtkhtml-devel

%package libs
Summary: GIMP libraries
Group: System Environment/Libraries

%description libs
This package contains shared libraries needed for the GIMP.

%description devel
Static libraries and header files for writing GIMP plugins and extensions.

%package help-de
Summary: German help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-de
German help files for GIMP.

%package help-en
Summary: English help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-en
English help files for GIMP.

%package help-es
Summary: Spanish help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-es
Spanish help files for GIMP.

%package help-fr
Summary: French help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-fr
French help files for GIMP

%package help-it
Summary: Italian help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-it
Italian help files for GIMP

%package help-ja
Summary: Japanese help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-ja
Japanese help files for GIMP

%package help-ko
Summary: Korean help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-ko
Korean help files for GIMP

%package help-nl
Summary: Dutch help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-nl
Dutch help files for GIMP

%package help-nn
Summary: Norwegian-Nynorsk help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-nn
Norwegian-Nynorsk help files for GIMP

%package help-pl
Summary: Polish help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-pl
Polish help files for GIMP

%package help-ru
Summary: Russian help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-ru
Russian help files for GIMP

%package help-sv
Summary: Swedish help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-sv
Swedish help files for GIMP

%package help-zh_CN
Summary: Chinese Simplified help files for GIMP
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description help-zh_CN
Chinese Simplified help files for GIMP

%if %{python_scripting}
%package pygimp
Summary: python scripting for GIMP
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description pygimp
python scripting for GIMP
%endif

%prep
%setup -q -a 10 -a 11 -a 12 -a 13 -a 14 -a 15 -a 16 -a 17 -a 18 -a 19 -a 20 -a 21 -a 22

%patch0 -p1 -b .asflags
%patch1 -p1 -b .gtk
%patch2 -p1 -b .freetype~

%build
autoreconf -ivf
%configure \
    --enable-gimp-remote \
    --enable-print \
    --enable-gimp-console \
    --disable-gtk-doc \
%if %{python_scripting}
    --enable-python \
%endif
    --with-poppler \
    --without-aa \
    --with-libtiff --with-libjpeg --with-libpng --with-libmng --with-libjasper \
    --with-libexif --with-librsvg --with-libxpm --with-gvfs --with-alsa \
    --with-webkit --with-dbus --with-script-fu --with-cairo-pdf

%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# make symlinks for convenience
ln -sn gimptool-%{libver} %{buildroot}%{_bindir}/gimptool
ln -sn gimptool-%{libver}.1 %{buildroot}%{_mandir}/man1/gimptool.1

# create filelist of plug-ins which should be in main package
(find %{buildroot}%{_libdir}/gimp/%{libver}/plug-ins/  \
  -type f -o -type l | sort ) > %{name}.files.plug-ins
mv %{name}.files.plug-ins %{name}.files.plug-ins.in
sed -e "/\.py$/d" < %{name}.files.plug-ins.in | sed -e "s|.*/usr|/usr|" | while read file; do
  if [ -d %{buildroot}/$file ]; then
    echo -n 'dir '
  fi
  echo $file
done > %{name}.files.plug-ins

# for gimp-help
pushd gimp-help-2
for l in de en es fr it ja ko nl nn pl ru sv zh_CN;
do
    make install \
      LANGUAGE=${l} \
      GIMPDATADIR=%{_datadir}/gimp/%{libver} \
      DESTDIR=%{buildroot} 
done
popd

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files -f %{name}.files.plug-ins
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog* HACKING INSTALL LICENSE NEWS* README*
%doc docs/Wilber*
%config %{_sysconfdir}/gimp
%{_bindir}/gimp*
%dir %{_libdir}/gimp
%dir %{_libdir}/gimp/%{libver}
%dir %{_libdir}/gimp/%{libver}/environ
%{_libdir}/gimp/%{libver}/interpreters
%{_libdir}/gimp/%{libver}/environ/default.env
%dir %{_libdir}/gimp/%{libver}/modules
%{_libdir}/gimp/%{libver}/modules/*.so
%{_mandir}/man?/*.?.*
%dir %{_datadir}/gimp
%dir %{_datadir}/gimp/%{libver}
%dir %{_libdir}/gimp/%{libver}/plug-ins
%{_datadir}/appdata/gimp.appdata.xml
%{_datadir}/gimp/%{libver}/dynamics
%{_datadir}/gimp/%{libver}/brushes
%{_datadir}/gimp/%{libver}/fonts
%{_datadir}/gimp/%{libver}/fractalexplorer
%{_datadir}/gimp/%{libver}/gfig
%{_datadir}/gimp/%{libver}/gflare
%{_datadir}/gimp/%{libver}/gimpressionist
%{_datadir}/gimp/%{libver}/gradients
%{_datadir}/gimp/%{libver}/images
%{_datadir}/gimp/%{libver}/menus
%{_datadir}/gimp/%{libver}/palettes
%{_datadir}/gimp/%{libver}/patterns
%{_datadir}/gimp/%{libver}/scripts
%{_datadir}/gimp/%{libver}/themes
%{_datadir}/gimp/%{libver}/tips
%{_datadir}/gimp/%{libver}/tags
%{_datadir}/gimp/%{libver}/ui
%{_datadir}/gimp/%{libver}/tool-presets
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/locale/*/LC_MESSAGES/*.mo
#%%{_datadir}/mime-info/gimp.keys
#%%{_datadir}/application-registry/gimp.applications
%{_datadir}/applications/gimp.desktop

%files libs
%defattr(-, root, root)
%{_libdir}/libgimp*.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/gimp-%{libver}
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgimp*.so
%{_datadir}/aclocal/gimp-2.0.m4
%doc %{_datadir}/gtk-doc/html/*

%files help-de
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/de

%files help-en
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/en

%files help-es
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/es

%files help-fr
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/fr

%files help-it
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/it

%files help-ja
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/ja

%files help-ko
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/ko

%files help-nl
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/nl

%files help-nn
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/nn

%files help-pl
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/pl

%files help-ru
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/ru

%files help-sv
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/sv

%files help-zh_CN
%defattr(-, root, root)
%doc gimp-help-2/{AUTHORS,COPYING,HACKING,MAINTAINERS,README}
%{_datadir}/gimp/%{libver}/help/zh_CN

%files pygimp
%defattr(-, root, root)
%{_libdir}/gimp/%{libver}/environ/pygimp.env
%{_libdir}/gimp/%{libver}/plug-ins/*.py*
%dir %{_libdir}/gimp/%{libver}/python
%{_libdir}/gimp/%{libver}/python/*

%changelog
* Sun Mar 23 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.10-1m)
- fix build failure
-- merge from T4R/GNOME312/gimp
-- update to 2.8.10
-- add patch for freetype 2.5.3

* Sun Sep 29 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.6-1m)
- update 2.8.6

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.2-2m)
- [SECURITY] CVE-2012-5576

* Tue Aug 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.2-1m)
- update 2.8.2
- [SECURITY] CVE-2012-3481 CVE-2012-3402 CVE-2012-3403 

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-5m)
- [SECURITY] CVE-2012-3236

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-4m)
- rebuild for librsvg2 2.36.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-3m)
- rebuild for glib 2.33.2

* Tue Jun 12 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (2.8.0-2m)
- add %%post, %%postun, %%posttrans

* Mon Jun 11 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (2.8.0-1m)
- update 2.8.0

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-3m)
- rebuild against libtiff-4.0.1

* Wed Jan 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-2m)
- BuildRequires: gegl-devel >= 0.1.8

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.4-1m)
- update 2.7.4

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.10-13m)
- rebuild against poppler-0.18.2

* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.10-12m)
- remove BR hal-devel

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.10-11m)
- rebuild against poppler-0.18.0

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.10-10m)
- import poppler-0.17.patch from Fedora

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.10-9m)
- rebuild against poppler-0.17.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.10-8m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.10-7m)
- rebuild against poppler-0.16.4

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.10-6m)
- rebuild against poppler-0.16.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.10-5m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.10-4m)
- rebuild against webkitgtk-1.3.4

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.10-3m)
- rebuild against poppler-0.14.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.10-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.10-1m)
- update 2.6.10

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.9-2m)
- split package libs

* Sun Jul  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.9-1m)
- update 2.6.9

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.8-7m)
- rebuild against poppler-0.14.0

* Tue Jun  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.8-6m)
- change BuildRequires: from poppler-devel to poppler-glib-devel

* Mon Apr 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.8-5m)
- rebuild against babl, gegl

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.8-4m)
- rebuild against libjpeg-8a

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.8-3m)
- fix build failure; add *.pyo and *.pyc

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.8-2m)
- delete __libtoolize hack

* Tue Dec 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.8-1m)
- update to 2.6.8
-- delete patch1,2 merged
-- --disable-gtk-doc

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-6m)
- [SECURITY] CVE-2009-1570 CVE-2009-3909
- apply upstream patches

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-5m)
- update help files

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.7-2m)
- rebuild against libjpeg-7

* Mon Aug 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-1m)
- update to 2.6.7

* Thu Jul  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.6-3m)
- import webkit patch from Fedora 11 (2:2.6.6-6)

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.6-2m)
- define libtoolize

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.6-1m)
- update to 2.6.6

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-2m)
- rebuild against WebKit-1.1.1

* Sat Feb 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-3m)
- rebuild against rpm-4.6

* Sun Jan  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-2m)
- update Patch0 for fuzz=0
- update BuildRequires
- update %%description
- add --enable-gimp-remote configure option
- build without -fomit-frame-pointer gcc option
- revise %%install
- License: GPLv2+

* Fri Jan  2 2009 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4
- rebuild against python-2.6.1

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.7-4m)
- update Patch0 for fuzz=0

* Thu Nov 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.7-3m)
- update helpver to 2.4.2

* Sat Oct 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.7-2m)
- rebuild against poppler-0.10.0

* Sun Sep 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.7-1m)
- update to 2.4.7

* Sun May 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.6-1m)
- update to 2.4.6

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.5-3m)
- rebuild against poppler-0.8.0 and cairo-1.6.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.5-2m)
- rebuild against gcc43

* Wed Feb  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5

* Mon Dec 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Fri Dec  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2
- update gimp-help-2.4.0

* Thu Nov  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Sat Jul 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.17-1m)
- version 2.2.17
- [SECURITY] CVE-2006-4519 fix plugin integer overflow
- http://secunia.com/secunia_research/2007-63/advisory/
- http://labs.idefense.com/intelligence/vulnerabilities/display.php?id=551

* Sun Jul  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.16-1m)
- update to 2.2.16
- security fix ?

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.15-1m)
- update to 2.2.15

* Sun May  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.14-1m)
- version 2.2.14

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.13-1m)
- version 2.2.13
- update gimp-help version to 2-0.12
- clean up spec file

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.12-7m)
- gimp-devel Requires: freetype2-devel -> freetype-devel

* Sat Feb 10 2007 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.12-6m)
- update gimp-help version to 2-0.11
- run 'make' of gimp-help in %%build

* Tue Dec 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.12-5m)
- add three directories to %%files, thanks >> 837
  %%dir %%{_libdir}/gimp
  %%dir %%{_datadir}/gimp
  %%dir %%{_datadir}/gimp/%%{libver}
- http://pc8.2ch.net/test/read.cgi/linux/1092539027/837

* Tue Sep 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.12-4m)
- rebuild against gail-1.9.2

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.12-3m)
- rebuild against expat-2.0.0-1m

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.12-2m)
- delete libtool library

* Mon Jul 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.12-1m)
- update to 2.2.12
-- security fix (CVE-2006-3404)
-- http://bugzilla.gnome.org/show_bug.cgi?id=346742
- comment out patch3 (for gcc4)

* Fri Apr 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11

* Fri Apr 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.10-3m)
- update gimp-help version to 2-0.10

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.10-2m)
- rebuild against openssl-0.9.8a

* Mon Mar 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10

* Mon Feb 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.8-4m)
- build with -fomit-frame-pointer to let assembly build with gcc-4.1.0-0.12m

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.8-3m)
- revised compile problem x86_64 (libgtkhtml-2.0.pc is missing "requires gail")
- add BuildPrereq: libgtkhtml-devel >= 2.6.3-2m

* Sat Dec 3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.8-2m)
- rebuild agenst libexif-0.6.12-1m

* Thu Jun 30 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.8-1m)
- up to 2.2.8
- add gimp-2.2.7-gcc4.patch

* Wed May 11 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.7-1m)
- up to 2.2.7

* Thu Apr 14 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.6-1m)
- up to 2.2.6

* Sat Feb 26 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.4-1m)
- disable aa

* Tue Dec 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-2m)
- revise %%files section

* Mon Dec 20 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.0-1m)
- update to version 2.2.0
- - Major feature enhancements
- - It is said that "Version 2.2 is an update of GIMP 2.0 and provides full backward compatibility. Plug-ins written for GIMP 2.0 can be used without recompiliation."
- update gimp-help to version 2-0.6

* Sun Dec 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.5-3m)
- add BuildPrereq: gail-devel

* Sun Nov 07 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.5-2m)
- change absolute path to relative path when make symlink

* Mon Sep 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5

* Sat Aug 21 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.4-1m)
  update to 2.0.4

* Tue Aug 10 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.2-2m)
- rebuild against autoconf-2.59

* Tue Jun 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.2-1m)
- update to bug-fix release 2.0.2

* Thu May 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.1-1m)
- update to bug-fix release 2.0.1

* Fri Apr 23 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.0-6m)
- rebuild against libcroco-0.5.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.0.0-5m)
- rebuild against for libxml2-2.6.8

* Tue Apr  6 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.0.0-4m)
- BuildRequires: pygtk -> pygtk-devel for python_scripting

* Sat Apr  3 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.0-3m)
- fix typo (imp-help-2-%{version} -> gimp-help-2-%{helpver})

* Sat Apr  3 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.0-2m)
- docs of img-help-2-2.0.0 are no longer provided.

* Sat Apr  3 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.0-1m)
- GIMP 2.0 Release!
- make gimp-help subpackage (gimp-help-2-0.2.tar.gz)

* Mon Feb  9 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0-0.3.2m)
- revise symlnks
- - ln -s gimptool-2.0 gimptool (not 1.3)
- - ln -s gimp-remote-1.3 gimp-remote

* Mon Feb  9 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0-0.3.1m)
- version 2.0pre3

* Tue Jan 20 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.0-0.2.1m)
- update to version 2.0pre2

* Thu Jan 15 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.0-0.1.1m)
- update to version 2.0pre1 (aka 1.3.24)
- BuildPrereq: lcms (for Color proof display filter using ICC profiles)
- add pygimp (python scripting module) as gimp-pygimp subpackage (to build or not is configurable by /etc/rpm/gimp.specopt)
- add menu for KDE

* Wed Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (1.3.21-1m)
- pretty spec file.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.21-1m)
- version 1.3.21

* Sun Oct 12 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.20-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Sep 13 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.3.20-1m)
- version 1.3.20

* Tue Sep 13 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.3.17-1m)
- version 1.3.17

* Wed Aug 6 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (1.3.16-2m)
- rebuild against libexif-0.5.12

* Fri Jun 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.16-1m)
- version 1.3.16

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.15-1m)
- version 1.3.15

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.14-2m)
- rebuild against slang(utf8 version)
- use %%{?_smp_mflags}

* Tue Apr 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.14-1m)
- version 1.3.14

* Sun Apr 06 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.13-3m)
- rebuild against zlib 1.1.4-5m

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.13-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.13-1m)
- version 1.3.13

* Wed Mar  5 2003 TABUCHI Takaaki <tab@momonga-linux.org.>
- (1.3.12-2m)
- add BuildPrereq: gpm-devel for link -lgpm

* Tue Feb 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org.>
- (1.3.12-1m)
  update to 1.3.12

* Fri Jan 10 2003 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.3.11-2m)
- add gimp-1.3.11-tempname.patch. see
    https://lists.XCF.Berkeley.EDU/lists/gimp-developer/2002-December/007983.html
  for details.

* Sun Jan  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.11-1m)
- version 1.3.11

* Sun Jan 05 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.3.10-3m)
- add BuildPrereq: slang-devel

* Tue Dec 10 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.10-2m)
- use autoconf-2.53

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.10-1m)
- version 1.3.10

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.9-1m)
- version 1.3.9

* Thu Aug 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.8-1m)
- version 1.3.8

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.7-11m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.7-10m)
- add BuildPrereq: libgtkhtml-devel

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.7-9m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.7-8k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.7-6k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.7-4k)
- change build and install section

* Thu Jun  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.7-2k)
- version 1.3.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-10k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.3.5-8k)
- /sbin/install-info -> info in PreReq.

* Sat Apr 20 2002 Toru Hoshina <t@kondara.org>
- (1.3.5-6k)
- gimp-config...

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-2k)
- version 1.3.5

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.4-4k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.4-2k)
- version 1.3.4

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-14k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-12k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-10k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-8k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-4k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Thu Feb 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.3-2k)
- version 1.3.3

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.2-18k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.2-16k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.2-10k)
- rebuild against for pango-0.24

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.2-8k)
- rebuild against for gtk+-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.2-6k)
- rebuild against for glib-1.3.13

* Tue Jan  8 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.2-2k)
- version 1.3.2
- gtk2 env

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.2-12k)
- move zh_TW.Big5 => zh_TW

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.2-8k)
- nigittenu

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.2.2-6k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (1.2.2-4k)
- rebuild against libpng 1.2.0.

* Wed Jan 24 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.2.1

* Wed Dec 27 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.2.0

* Thu Dec 21 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.1.32
- added configure parameter(mandir, infodir, sysconfdir)

* Fri Dec 15 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1.30-3k)
- release number from 2k or 3k
- fixed Jitter #806

* Tue Dec 12 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.1.30

* Sat Nov 25 2000 Kenichi Matsubara <m@kondara.org>
- add Requires: gimp = %{version}  gimp-devel package.

* Thu Nov  9 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- fix script-fu INIT_I18N bug on UTF-8

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Tue Oct 17 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.1.28

* Thu Oct  5 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.1.27

* Mon Oct 02 2000 Kenichi Matsubara <m@kondara.org>
- add gimp-1.1.26-print-canon.patch :-P

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Thu Jun 29 2000 T.Makuta <makuta@fra.allnet.ne.jp>
- update to 1.1.24

* Sat May  6 2000 tom <tom@kondara.org>
- update to 1.1.21

* Thu Mar  9 2000 Akira Higuchi <a@kondara.org>
- don't grab /usr/share/icons nor /usr/share/icons/mini

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Nov  1 1999 Hidetomo Machi <machi@ailab.is.tsukuba.ac.jp>
- Fixed %doc
- libgimp was divided into gimp(lib*.so.*) and gimp-devel(lib*.so)

* Sat Oct 23 1999 Akira Higuchi <a-higuti@math.sci.hokudai.ac.jp>
- Added setlocale patch

* Wed Oct 20 1999 Akira Higuchi <a-higuti@math.sci.hokudai.ac.jp>
- Added script-fu-bugfix patch
- Removed 'Grayscale' and 'Success' patches

* Wed Jul 21 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- Change gdk_font_load to gdk_fontset_load in gdyntext plug-in

* Sat Jun 10 1999 Toru Hoshina <hoshina@best.com>
- Added spline patch, because I love SOTA chrome logo :-)

* Wed Mar 10 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- First release
