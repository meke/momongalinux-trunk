%global        momorel 10
%global        qtver 4.8.4

Summary:       A personal video recorder (PVR) application
Name:          mythtv
Version:       0.25.2
Release:       %{momorel}m%{?dist}
URL:           http://www.mythtv.org/
License:       GPL
Group:         Applications/Multimedia
Source0:       ftp://ftp.osuosl.org/pub/%{name}/old_releases/%{name}-%{version}.tar.bz2
NoSource:      0
Source1:       ftp://ftp.osuosl.org/pub/mythtv/old_releases/mythplugins-%{version}.tar.bz2
NoSource:      1
Source10:      mythtv-session.desktop
Source101:     mythbackend.sysconfig.in
Source102:     mythbackend.init.in
Source104:     mythbackend.service.in
Source106:     mythfrontend.png
Source107:     mythfrontend.desktop
Source108:     mythtv-setup.png
Source109:     mythtv-setup.desktop
Source201:     mythfrontend-1024x576.desktop
Source202:     mythfrontend-800x450.desktop
Patch2:        mythtv-ldconfig.patch
Patch3:        mythtv-python-install.patch
Patch4:        mythtv-perl-install.patch
Patch5:        mythtv-ffmpeg-fix-return.patch
## based on http://code.mythtv.org/trac/attachment/ticket/9726/0003-libmythtv-Unicable-SCR-DIN-EN-50494.patch
Patch6:        mythtv-downgrade-from-unicable-support.diff
## for libva-1.2.1
Patch10:       %{name}-%{version}-libva121.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mesa-libGL-devel
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXcursor-devel
BuildRequires: libXext-devel, libXft-devel, libXinerama-devel, libXmu-devel
BuildRequires: libXrandr-devel, libXrender-devel, libXv-devel libXxf86vm-devel
BuildRequires: freetype-devel, fontconfig-devel
BuildRequires: VLGothic-fonts, VLGothic-fonts-proportional
BuildRequires: lame-devel, mysql-devel, alsa-lib-devel, lirc-devel
BuildRequires: qt-devel >= %{qtver}
BuildRequires: sgml-tools, autoconf, automake
BuildRequires: desktop-file-utils
BuildRequires: libcdio-devel >= 0.90
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libva-devel >= 1.2.1
BuildRequires: perl-Net-UPnP
BuildRequires: python-devel
BuildRequires: x264-devel >= 0.0.2377
# for Symbolic links
Requires:      qt >= %{qtver}
Requires:      libmyth = %{version}
Requires:      VLGothic-fonts, VLGothic-fonts-proportional
Requires(pre): shadow-utils

%description
MythTV implements the following PVR features, and more, with a
unified graphical interface:

 - Basic 'live-tv' functionality. Pause/Fast Forward/Rewind "live" TV.
 - Video compression using RTjpeg or MPEG-4
 - Program listing retrieval using XMLTV
 - Themable, semi-transparent on-screen display
 - Electronic program guide
 - Scheduled recording of TV programs
 - Resolution of conflicts between scheduled recordings
 - Basic video editing

%package docs
Summary:   MythTV documentation
Group:     Documentation

%description docs
The MythTV documentation, contrib files, database initialization file
and miscellaneous other bits and pieces.

%package -n libmyth
Summary:   Library providing mythtv support
Group:     System Environment/Libraries
Provides:  mythtv-frontend-api = %{mythfeapiver}
Requires:  qt-mysql

%description -n libmyth
Common library code for MythTV and add-on modules (development)
MythTV provides a unified graphical interface for recording and viewing
television programs.  Refer to the mythtv package for more information.

%package -n libmyth-devel
Summary:    Development files for libmyth
Group:      Development/Libraries
Requires:   libmyth = %{version}-%{release}

%description -n libmyth-devel
This package contains the header files and libraries for developing
add-ons for mythtv.

%package base-themes
Summary:    Core user interface themes for mythtv
Group:      Applications/Multimedia
BuildArch:  noarch

%description base-themes
MythTV provides a unified graphical interface for recording and viewing
television programs.  Refer to the mythtv-docs package for more information.

This package contains the base themes for the mythtv user interface.

%package frontend
Summary:    Client component of mythtv (a DVR)
Group:      Applications/Multimedia
Requires:   mythtv-common       = %{version}-%{release}
Requires:   mythtv-base-themes  = %{version}
Requires:   mythtv-frontend-api = %{mythfeapiver}

%description frontend
MythTV provides a unified graphical interface for recording and viewing
television programs.  Refer to the mythtv package for more information.

This package contains only the client software, which provides a
front-end for playback and configuration.  It requires access to a
mythtv-backend installation, either on the same system or one
reachable via the network.

%package backend
Summary:    Server component of mythtv (a DVR)
Group:      Applications/Multimedia
Requires(pre): shadow-utils
Requires:   mythtv-common = %{version}-%{release}

%description backend
MythTV provides a unified graphical interface for recording and viewing
television programs.  Refer to the mythtv package for more information.

This package contains only the server software, which provides video
and audio capture and encoding services.  In order to be useful, it
requires a mythtv-frontend installation, either on the same system or
one reachable via the network.

%package setup
Summary:   Setup the mythtv backend
Group:     Applications/Multimedia
Requires:  mythtv-backend = %{version}-%{release}
Requires:  mythtv-base-themes = %{version}

%description setup
MythTV provides a unified graphical interface for recording and viewing
television programs.  Refer to the mythtv package for more information.

This package contains only the setup software for configuring the
mythtv backend.

%package common
Summary: Common components needed by multiple other MythTV components
Group: Applications/Multimedia

%description common
MythTV provides a unified graphical interface for recording and viewing
television programs.  Refer to the mythtv package for more information.

This package contains components needed by multiple other MythTV components.

%package -n perl-MythTV
Summary:        Perl bindings for MythTV
Group:          Development/Libraries
# Wish we could do this:
#BuildArch:      noarch
Requires:       perl
Requires:       perl-DBD-mysql
Requires:       perl-Net-UPnP

%description -n perl-MythTV
Provides a perl-based interface to interacting with MythTV.

%package -n python-MythTV
Summary:        Python bindings for MythTV
Group:          Development/Libraries
Requires:       python-lxml
BuildArch:      noarch

%description -n python-MythTV
Provides a python-based interface to interacting with MythTV.

%package -n php-MythTV
Summary:        PHP bindings for MythTV
Group:          Development/Libraries
Requires:       perl-MythTV
BuildArch:      noarch

%description -n php-MythTV
Provides a php-based interface to interacting with MythTV.

%package -n mytharchive
Summary:   A module for MythTV for creating and burning DVDs
Group:     Applications/Multimedia
Requires:  cdrecord
Requires:  dvd+rw-tools
Requires:  dvdauthor
Requires:  ffmpeg
Requires:  mjpegtools
Requires:  mkisofs
Requires:  python
Requires:  python-imaging
Requires:  transcode

%description -n mytharchive
MythArchive is a new plugin for MythTV that lets you create DVDs from
your recorded shows, MythVideo files and any video files available on
your system.

%package -n mythbrowser
Summary:   A small web browser module for MythTV
Group:     Applications/Multimedia

%description -n mythbrowser
MythBrowser is a full fledged web-browser (multiple tabs) to display
webpages in full-screen mode. Simple page navigation is possible.
Starting with version 0.13 it also has full support for mouse driven
navigation (right mouse opens and clos es the popup menu).

MythBrowser also contains a BookmarkManager to manage the website
links in a simple mythplugin.

%package -n mythgallery
Summary:   A gallery/slideshow module for MythTV
Group:     Applications/Multimedia
Requires:  mythtv-frontend-api = %{mythfeapiver}
Requires:  dcraw

%description -n mythgallery
A gallery/slideshow module for MythTV.

%package -n mythgame
Summary:   A game frontend (xmame, nes, snes, pc) for MythTV
Group:     Applications/Multimedia
Requires:  mythtv-frontend-api = %{mythfeapiver}

%description -n mythgame
A game frontend (xmame, nes, snes, pc) for MythTV.

%package -n mythmusic
Summary:   The music player add-on module for MythTV
Group:     Applications/Multimedia
Requires:  mythtv-frontend-api = %{mythfeapiver}

%description -n mythmusic
Music add-on for mythtv.


%package -n mythnetvision
Summary:   MythTV Plugin for watching internet content
Group:     Applications/Multimedia
Requires:  python-MythTV = %{version}-%{release}
Requires:  python-oauth
Requires:  perl

%description -n mythnetvision
MythNetvision is a plugin designed to make adding Internet video sources to
MythTV fun and easy. MythNetvision consists of two components-- a search screen
and a site/RSS browser screen. When installed, MythNetvision appears on the
Media Library screen as the options "Search Internet Video" and "Browse Internet
Video."
 
%package -n mythnews
Summary:   An RSS news feed plugin for MythTV
Group:     Applications/Multimedia
Requires:  mythtv-frontend-api = %{mythfeapiver}

%description -n mythnews
An RSS news feed reader plugin for MythTV.

%package -n mythweather
Summary:   A MythTV module that displays a weather forcast
Group:     Applications/Multimedia
Requires:  mythtv-frontend-api = %{mythfeapiver}
Requires:  perl-XML-SAX-Base
Requires:  perl-XML-XPath
Requires:  perl-Image-Size
Requires:  perl-JSON
Requires:  perl-DateTime-Format-ISO8601
Requires:  perl-SOAP-Lite

%description -n mythweather
A MythTV module that displays a weather forcast.

%package -n mythzoneminder
Summary:   A module for MythTV for camera security and surveillance
Group:     Applications/Multimedia
Requires:  mythtv-frontend-api = %{mythfeapiver}

%description -n mythzoneminder
MythZoneMinder is a plugin to interface to some of the features of
ZoneMinder. You can use it to view a status window similar to the
console window in ZM. Also there are screens to view live camera shots
and replay recorded events.

%prep
%setup -q -c -a 1
pushd mythtv-%{version}
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p0
%patch10 -p1
popd

if [ "%{_lib}" != "lib" ]; then
    grep -rlZ '/lib/' . | xargs -r0 sed -i -e 's,/lib/,/%{_lib}/,g'
    grep -rlZ '/lib$' . | xargs -r0 sed -i -e 's,/lib$,/%{_lib},'
    grep -rlZ '/lib ' . | xargs -r0 sed -i -e 's,/lib ,/%{_lib} ,g'
fi

pushd mythtv-%{version}

find contrib/ -type f -exec chmod -x "{}" \;
chmod -x themes/default/htmls/*.html

rm -rf contrib/Win32 contrib/OSX

sed -i -e 's,VENDOR_XVMC_LIBS="-lXvMCNVIDIA",VENDOR_XVMC_LIBS="-lXvMCNVIDIA -lXv",' configure

cp -a %{SOURCE101} %{SOURCE102} %{SOURCE104} .
cp -a %{SOURCE106} %{SOURCE107} %{SOURCE108} %{SOURCE109} %{SOURCE201} %{SOURCE202} .

for file in mythbackend.init mythbackend.sysconfig mythbackend.service; do
  sed -e's|@logdir@|%{_localstatedir}/log|g' \
      -e's|@rundir@|%{_localstatedir}/run|g' \
      -e's|@sysconfdir@|%{_sysconfdir}|g' \
      -e's|@sysconfigdir@|%{_sysconfdir}/sysconfig|g' \
      -e's|@initdir@|%{_sysconfdir}/init.d|g' \
      -e's|@bindir@|%{_bindir}|g' \
      -e's|@sbindir@|%{_sbindir}|g' \
      -e's|@subsysdir@|%{_localstatedir}/lock/subsys|g' \
      -e's|@varlibdir@|%{_localstatedir}/lib|g' \
      -e's|@varcachedir@|%{_localstatedir}/cache|g' \
      < $file.in > $file
done

popd

temp=`pwd`/temp
pushd mythplugins-%{version}
perl -pi -e"s,\\\$\\\${DEPLIBS}/libmyth,$temp%{_libdir}/libmyth," targetdep.pro
find . -name \*.pro | xargs perl -pi -e's,\$\${PREFIX}/include/mythtv,'$temp/usr/include/mythtv,
cat >> settings.pro << EOF
INCLUDEPATH += $temp/usr/include
LIBS *= -L$temp%{_libdir}
EOF

popd

%build
pushd %{name}-%{version}
./configure \
    --prefix=%{_prefix}                         \
    --libdir=%{_libdir}                         \
    --libdir-name=%{_lib}                       \
    --mandir=%{_mandir}                         \
    --enable-iptv                               \
    --enable-pthreads                           \
    --enable-ffmpeg-pthreads                    \
    --enable-joystick-menu                      \
    --enable-audio-alsa                         \
    --enable-audio-oss                          \
    --enable-audio-jack                         \
    --enable-libfftw3                           \
    --enable-x11 --x11-path=%{_includedir}      \
    --enable-xv                                 \
    --enable-opengl-video                       \
    --enable-xrandr                             \
    --enable-lirc                               \
    --enable-ivtv                               \
    --enable-firewire                           \
    --enable-dvb                                \
    --enable-libmp3lame                         \
    --enable-libtheora --enable-libvorbis       \
    --enable-libxvid                            \
    --enable-vdpau                              \
    --enable-vaapi                              \
    --enable-libfaac                            \
    --enable-libx264                            \
    --enable-libvpx                             \
    --enable-libxvid                            \
    --enable-libdns-sd                          \
    --enable-nonfree                            \
    --extra-cflags="%{optflags} -fomit-frame-pointer -fno-strict-aliasing" \
    --extra-cxxflags="%{optflags} -fomit-frame-pointer -fno-strict-aliasing" \
    --extra-libs="-ldl"                         \
    --compile-type=release                      \
    --disable-stripping

make %{?_smp_mflags} V=1

popd

mkdir temp
temp=`pwd`/temp
mkdir -p $temp%{perl_vendorlib}
make -C mythtv-%{version} install INSTALL_ROOT=$temp
export LD_LIBRARY_PATH=$temp%{_libdir}:$LD_LIBRARY_PATH

pushd mythplugins-%{version}

echo "QMAKE_PROJECT_DEPTH = 0" >> settings.pro
find . -name \*.pro \
    -exec sed -i -e "s,INCLUDEPATH += .\+/include/mythtv,INCLUDEPATH += $temp%{_includedir}/mythtv," {} \; \
    -exec sed -i -e "s,DEPLIBS = \$\${LIBDIR},DEPLIBS = $temp%{_libdir}," {} \; \
    -exec sed -i -e "s,\$\${PREFIX}/include/mythtv,$temp%{_includedir}/mythtv," {} \;
echo "INCLUDEPATH -= \$\${PREFIX}/include" >> settings.pro
echo "INCLUDEPATH -= %{_includedir}"       >> settings.pro
echo "INCLUDEPATH += $temp%{_includedir}"  >> settings.pro
echo "INCLUDEPATH += %{_includedir}"       >> settings.pro
echo "LIBS *= -L$temp%{_libdir}"           >> settings.pro
echo "QMAKE_LIBDIR += $temp%{_libdir}"     >> targetdep.pro

./configure \
    --prefix=${temp}%{_prefix} \
    --libdir=%{_libdir} \
    --libdir-name=%{_lib} \
    --enable-mytharchive \
    --enable-mythbrowser \
    --enable-mythgallery \
    --enable-exif \
    --enable-new-exif \
    --enable-dcraw \
    --enable-mythgame \
    --enable-mythmusic \
    --enable-mythnews \
    --enable-mythweather \
    --enable-mythzoneminder \
    --enable-opengl \
    --enable-fftw \
    --compile-type=release

%__make %{?_smp_mflags} V=1

popd

%install
rm -rf %{buildroot}
pushd mythtv-%{version}

make install INSTALL_ROOT=%{buildroot}

ln -sf mythtv-setup %{buildroot}%{_bindir}/mythtvsetup
install -d %{buildroot}%{_localstatedir}/lib/mythtv
install -d %{buildroot}%{_localstatedir}/log/mythtv
install -d %{buildroot}%{_localstatedir}/cache/mythtv
install -d %{buildroot}%{_sysconfdir}/mythtv

echo "# to be filled in by mythtv-setup" > %{buildroot}%{_sysconfdir}/mythtv/config.xml
install -D -p -m 755 mythbackend.init %{buildroot}%{_sysconfdir}/init.d/mythbackend
install -D -p -m 644 mythbackend.sysconfig %{buildroot}%{_localstatedir}/adm/fillup-templates/sysconfig.mythbackend
install -D -p -m 644 mythbackend.service %{buildroot}%{_unitdir}/mythbackend.service

for file in mythfrontend  mythtv-setup; do
  install -D -p $file.png %{buildroot}%{_datadir}/pixmaps/$file.png
  install -D -m 644 $file.desktop %{buildroot}%{_datadir}/applications/$file.desktop
done
install -D -m 644 mythfrontend-1024x576.desktop %{buildroot}%{_datadir}/applications/mythfrontend-1024x576.desktop
install -D -m 644 mythfrontend-800x450.desktop %{buildroot}%{_datadir}/applications/mythfrontend-800x450.desktop

install -m 644 -D %{SOURCE10} %{buildroot}%{_datadir}/xsessions/mythtv.desktop

install -d %{buildroot}%{_libdir}/mythtv/plugins

install -D -m 644 settings.pro %{buildroot}%{_datadir}/mythtv/build/settings.pro

ln -sf ../..%{_sysconfdir}/init.d/mythbackend %{buildroot}%{_bindir}/rcmythbackend

popd

pushd mythplugins-%{version}

make install INSTALL_ROOT=%{buildroot}

install -d %{buildroot}%{_localstatedir}/lib/mythmusic
install -d %{buildroot}%{_localstatedir}/lib/pictures
install -d %{buildroot}%{_datadir}/mythtv/games/nes/{roms,screens}
install -d %{buildroot}%{_datadir}/mythtv/games/snes/{roms,screens}
install -d %{buildroot}%{_datadir}/mythtv/games/PC/screens
install -d %{buildroot}%{_datadir}/mame
ln -s ../../mame %{buildroot}%{_datadir}/mythtv/games/xmame
install -d %{buildroot}%{_datadir}/mame/flyers
ln -s snap %{buildroot}%{_datadir}/mythtv/games/xmame/screens
install -d %{buildroot}%{_sysconfdir}/mythgame
cp -a mythgame/gamelist.xml %{buildroot}%{_sysconfdir}/mythgame/
ln -s ../../../../../%{_sysconfdir}/mythgame/ \
        %{buildroot}%{_datadir}/mythtv/games/PC/gamelist.xml

popd

%clean
rm -rf %{buildroot}

%post -n libmyth -p /sbin/ldconfig

%postun -n libmyth -p /sbin/ldconfig

%pre backend
# Add the "mythbackend" user and group
/usr/sbin/useradd -c "mythtvbackend User"  \
        -s /sbin/nologin -r -d /var/lib/mythtv mythtv 2> /dev/null || :
/bin/systemctl enable mythbackend.service

%post backend
/bin/systemctl enable mythbackend.service

%preun backend
/bin/systemctl disable mythbackend.service

%postun backend
/bin/systemctl disable mythbackend.service

%files

%files docs
%defattr(-,root,root,-)
%doc mythtv-%{version}/README*
%doc mythtv-%{version}/UPGRADING
%doc mythtv-%{version}/AUTHORS
%doc mythtv-%{version}/COPYING
%doc mythtv-%{version}/FAQ
%doc mythtv-%{version}/database
%doc mythtv-%{version}/keys.txt
%doc mythtv-%{version}/docs
%doc mythtv-%{version}/contrib

%files common
%defattr(-,root,root,-)
%dir %{_sysconfdir}/mythtv
%config(noreplace) %{_sysconfdir}/mythtv/config.xml
%{_bindir}/mythcommflag
%{_bindir}/mythtranscode
%{_bindir}/mythffmpeg
%{_bindir}/mythffplay
%{_bindir}/mythreplex
%{_bindir}/mythccextractor
%{_bindir}/mythmetadatalookup
%{_bindir}/mythutil
%{_bindir}/mythpreviewgen
%dir %{_datadir}/mythtv
%{_datadir}/mythtv/mythconverg*.pl
%dir %{_datadir}/mythtv/locales
%{_datadir}/mythtv/locales/*
%dir %{_datadir}/mythtv/metadata
%{_datadir}/mythtv/metadata/*
%dir %{_datadir}/mythtv/hardwareprofile
%{_datadir}/mythtv/hardwareprofile/*
%exclude %{_datadir}/mythtv/hardwareprofile/.gitignore
%{_bindir}/mythwikiscripts
%{_datadir}/mythtv/CDS_scpd.xml
%{_datadir}/mythtv/CMGR_scpd.xml
%{_datadir}/mythtv/MFEXML_scpd.xml
%{_datadir}/mythtv/MSRR_scpd.xml
%{_datadir}/mythtv/devicemaster.xml
%{_datadir}/mythtv/deviceslave.xml

%files backend
%defattr(-,root,root,-)
%{_bindir}/mythbackend
%{_bindir}/mythfilldatabase
%{_bindir}/mythjobqueue
%{_bindir}/mythmediaserver
%{_bindir}/rcmythbackend
%{_datadir}/mythtv/html/
%{_datadir}/mythtv/backend-config
%{_datadir}/mythtv/MXML_scpd.xml
%attr(-,mythbackend,video) %dir %{_localstatedir}/lib/mythtv
%attr(-,mythbackend,video) %dir %{_localstatedir}/cache/mythtv
%config %{_sysconfdir}/init.d/mythbackend
%{_localstatedir}/adm/fillup-templates/sysconfig.mythbackend
%attr(-,mythbackend,video) %dir %{_localstatedir}/log/mythtv
%{_unitdir}/*

%files setup
%defattr(-,root,root,-)
%{_bindir}/mythtv-setup
%{_bindir}/mythtvsetup
%{_datadir}/mythtv/setup.xml
%{_datadir}/applications/*mythtv-setup.desktop

%files frontend
%defattr(-,root,root,-)
%{_bindir}/mythavtest
%{_bindir}/mythfrontend
%{_bindir}/mythlcdserver
%{_bindir}/mythshutdown
%{_bindir}/mythwelcome
%{_libdir}/mythtv/filters
%dir %{_libdir}/mythtv
%dir %{_libdir}/mythtv/plugins
#%{_datadir}/mythtv/*.ttf
%dir %{_datadir}/mythtv/i18n
%{_datadir}/mythtv/i18n/mythfrontend_*.qm
%{_datadir}/applications/mythfrontend*.desktop
%{_datadir}/pixmaps/myth*.png
%{_datadir}/xsessions/mythtv.desktop
%dir %{_datadir}/mythtv/fonts
%{_datadir}/mythtv/fonts/*.ttf
%{_datadir}/mythtv/fonts/tiresias_gpl3.txt

%files base-themes
%defattr(-,root,root,-)
%dir %{_datadir}/mythtv/themes
%{_datadir}/mythtv/themes/*

%files -n libmyth
%defattr(-,root,root,-)
%{_libdir}/*.so.*

%files -n libmyth-devel
%defattr(-,root,root,-)
%{_includedir}/mythtv
%{_libdir}/*.so
%exclude %{_libdir}/*.a
%dir %{_datadir}/mythtv/build
%{_datadir}/mythtv/build/settings.pro

%files -n perl-MythTV
%defattr(-,root,root,-)
%{perl_vendorlib}/MythTV.pm
%dir %{perl_vendorlib}/MythTV
%{perl_vendorlib}/MythTV/*.pm
%dir %{perl_vendorlib}/IO
%dir %{perl_vendorlib}/IO/Socket
%dir %{perl_vendorlib}/IO/Socket/INET
%{perl_vendorlib}/IO/Socket/INET/MythTV.pm
%exclude %{perl_vendorarch}/auto/MythTV/.packlist

%files -n python-MythTV
%defattr(-,root,root,-)
%dir %{python_sitelib}/MythTV/
%{_bindir}/mythpython
%{python_sitelib}/MythTV/*
%{python_sitelib}/MythTV-*.egg-info

%files -n php-MythTV
%defattr(-,root,root,-)
%dir %{_datadir}/mythtv/bindings
%{_datadir}/mythtv/bindings/php

%files -n mytharchive
%defattr(-,root,root,-)
%{_bindir}/mytharchivehelper
%{_libdir}/mythtv/plugins/libmytharchive.so
%{_datadir}/mythtv/archivemenu.xml
%{_datadir}/mythtv/archiveutils.xml
%{_datadir}/mythtv/mytharchive
%{_datadir}/mythtv/i18n/mytharchive_*.qm

%files -n mythbrowser
%defattr(-,root,root,-)
%{_libdir}/mythtv/plugins/libmythbrowser.so
%{_datadir}/mythtv/i18n/mythbrowser_*.qm

%files -n mythgallery
%defattr(-,root,root,-)
%{_libdir}/mythtv/plugins/libmythgallery.so
%{_datadir}/mythtv/i18n/mythgallery_*.qm
%{_localstatedir}/lib/pictures

%files -n mythgame
%defattr(-,root,root,-)
%dir %{_sysconfdir}/mythgame
%config(noreplace) %{_sysconfdir}/mythgame/gamelist.xml
%{_libdir}/mythtv/plugins/libmythgame.so
%dir %{_datadir}/mythtv/games
%dir %{_datadir}/mythtv/games/PC
%{_datadir}/mythtv/games/PC/gamelist.xml
%dir %{_datadir}/mythtv/games/xmame
%dir %{_datadir}/mame
%dir %{_datadir}/mame/screens
%dir %{_datadir}/mame/flyers
%{_datadir}/mythtv/game_settings.xml
%{_datadir}/mythtv/i18n/mythgame_*.qm

%files -n mythmusic
%defattr(-,root,root,-)
%{_libdir}/mythtv/plugins/libmythmusic.so
%{_localstatedir}/lib/mythmusic
%{_datadir}/mythtv/musicmenu.xml
%{_datadir}/mythtv/music_settings.xml
%{_datadir}/mythtv/i18n/mythmusic_*.qm

%files -n mythnetvision
%defattr(-,root,root,-)
%{_bindir}/mythfillnetvision
%{_libdir}/mythtv/plugins/libmythnetvision.so
%{_datadir}/mythtv/mythnetvision
%{_datadir}/mythtv/netvisionmenu.xml
%{_datadir}/mythtv/i18n/mythnetvision_*.qm
%{_datadir}/mythtv/internetcontent

%files -n mythnews
%defattr(-,root,root,-)
%{_libdir}/mythtv/plugins/libmythnews.so
%{_datadir}/mythtv/mythnews
%{_datadir}/mythtv/i18n/mythnews_*.qm

%files -n mythweather
%defattr(-,root,root,-)
%{_libdir}/mythtv/plugins/libmythweather.so
%{_datadir}/mythtv/i18n/mythweather_*.qm
%{_datadir}/mythtv/weather_settings.xml
%{_datadir}/mythtv/mythweather

%files -n mythzoneminder
%defattr(-,root,root,-)
%{_libdir}/mythtv/plugins/libmythzoneminder.so
%{_datadir}/mythtv/zonemindermenu.xml
%{_bindir}/mythzmserver
%{_datadir}/mythtv/i18n/mythzoneminder_*.qm

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25.2-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25.2-9m)
- rebuild against perl-5.18.2

* Wed Jan 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25.2-7m)
- rebuild against x264

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25.2-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25.2-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25.2-4m)
- rebuild against perl-5.16.3

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25.2-3m)
- rebuild against libcdio-0.90

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25.2-2m)
- rebuild against perl-5.16.2

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25.2-1m)
- update to 0.25.2, based on SuSE

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-27m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-26m)
- rebuild for new GCC 4.5

* Mon Oct  4 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-25m)
- add patch for gcc45

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-24m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-23m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16-21m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-20m)
- rebuild against rpm-4.6

* Tue Sep  2 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.16-19m)
- use VLGothic

* Mon May 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-18m)
- rebuild against qt3 and change Requires to qt3-MySQL

* Fri Apr 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-17m)
- fix BPR

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-16m)
- rebuild against gcc43

* Tue Feb 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-15m)
- add patch for gcc43, generated by gen43patch(v1)

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-14m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-13m)
- BuildRequires: freetype2-devel -> freetype-devel

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-12m)
- remove category X-Red-Hat-Extra Application

* Tue May 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-11m)
- rebuild against mysql 5.0.22-1m

* Sun Mar 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.16-10m)
- ppc build fix

* Thu Jan 19 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.16-9m)
- i686 build fix

* Wed Jan 11 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.16-8m)
- gcc-4.1 build fix

* Thu Oct 20 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.16-7m)
- enable ia64, ppc64

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.16-6m)
- rebuild against for MySQL-4.1.14

* Wed Mar  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-5m)
- remove vendor from desktop file

* Mon Feb 28 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.16-4m)
- enable ppc.

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.16-3m)
- enable x86_64.

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.16-2m)
- rebuild with lame.

* Thu Nov 25 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.16-1m)
- up to 0.16

* Wed May 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.14-1m)
- import from paken.

* Fri Jan 31 2004 Takeru Komoriya <komoriya@paken.org>
- japanese version based on mythtv-0.13-48.rhfc1.at

* Sat Dec 20 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Patch for missing tvformat default (bttv always did PAL).

* Thu Dec 11 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.13.
- Remove ringbuffer patch (applied in 0.13).

* Tue Oct 21 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Add cvs patch fixing ringbuffer races.
- Remove settings patch for alsa/lirc and make it configurable
  from the rpmbuild invocation instead with --with/--without switches
- New qmake invocation.

* Sun Oct 19 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.12.
- Move path macros out of specfile.
- Remove imdb patch.
- Silence install/upgrade output.

* Mon Oct 13 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Activate native alsa and lirc modes.

* Sat Sep 20 2003 "Jarod C. Wilson" <jcw@wilsonet.com>
- Added patch for imdb changes.

* Sat Aug 16 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.11.

* Wed Jul  2 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.10.

* Tue Jun 24 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Make logrotate script accept non-existing logfiles.

* Sat Jun 21 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Split off mythtv-suite.

* Thu Jun 12 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.9.1 release.
- Split /var/spool/mythtv to /var/lib/mythtv and /var/cache/mythtv.
  (as Debian does).

* Tue Jun 10 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.9 release.

* Wed May 14 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Fixed packaging bug in libmyth-devel.

* Tue Apr  8 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Rebuilt for Red Hat 9.

* Sun Mar 23 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Fixed desktop entries.

* Sat Mar 22 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Add g flag to sed.
- Don't use mythtv user yet.

* Fri Mar 21 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Add mythtv user.
- Add desktop entries.

* Wed Mar 19 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Added start/stop scripts for mythbackend.

* Tue Mar 18 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Removed unneeded 0.7 patches.

* Mon Mar 17 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.8.
- Synced with Matt Zimmerman's debian package (package splitting).
- Split off lib, devel, frontend, backend packages.

* Thu Feb 13 2003 Paul Jara <pjara@rogers.com>
- Added a patch that prevents a segmentation fault in mythfilldatabase.

* Thu Jan 16 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Added dependency to qt-MySQL (Roy Stogner <roystgnr@ticam.utexas.edu>).

* Thu Dec  5 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- fixed installation paths.

* Wed Nov 13 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Initial build.


