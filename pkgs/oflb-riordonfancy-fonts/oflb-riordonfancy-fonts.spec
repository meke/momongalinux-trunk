%global momorel 6

%define fontname        riordonfancy
%define fontconf        69-%{fontname}.conf

Name:           oflb-%{fontname}-fonts
Version:        4
Release:        %{momorel}m%{?dist}
Summary:        A stylized font

Group:          User Interface/X
License:        OFL
URL:            http://openfontlibrary.org/media/files/tthurman/354
Source0:        http://openfontlibrary.org/people/tthurman/tthurman_-_Riordon_Fancy.zip
Source1:        %{fontname}-fontconfig.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  fontpackages-devel fontforge
Requires:       fontpackages-filesystem

%description
RiordonFancy is a highly-stylized font designed by ten-year-old Riordon
Thurman. It includes all ASCII glyphs, most Latin-1 glyphs, a number of Latin
Extended glyphs, the interrobang, and the snowman.

%prep
%setup -qc

%build
rm RiordonFancy.ttf
FONTFORGE_LANGUAGE=ff fontforge -script "-" *.sfd <<EOF
i = 1
while (i < \$argc)
  Open(\$argv[i], 1)
  fontname = StrJoin(StrSplit(\$fontname, "O"), "")
  familyname = StrJoin(StrSplit(\$familyname, " O"), "")
  fullname = StrJoin(StrSplit(\$fullname, " O"), "")
  SetFontNames(fontname, familyname, fullname)
  ScaleToEm(2048)
  RoundToInt()
  SetFontOrder(2)
  SelectAll()
  AutoInstr()
  Generate(\$fontname + ".ttf")
  Close()
  i++
endloop
EOF

%install
rm -rf $RPM_BUILD_ROOT

install -dm 755 $RPM_BUILD_ROOT%{_fontdir}
install -pm 644 RiordonFancy.ttf $RPM_BUILD_ROOT%{_fontdir}/

install -dm 755 $RPM_BUILD_ROOT%{_fontconfig_templatedir}
install -dm 755 $RPM_BUILD_ROOT%{_fontconfig_confdir}
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
  $RPM_BUILD_ROOT%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg -f %{fontconf} *.ttf
%doc readme.txt fontlog.txt
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4-1m)
- import from Fedora

* Mon Apr 13 2009 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 4-2
- Rebuild for stronger hashes

* Mon Mar 16 2009 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 4-1
- Upstream update
- Renamed to oflb-riordon-fancy-fonts

* Sun Jan 04 2009 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 3-2
- Removed disttag
- Moved from old font template to new font template
- Changed name to lowercase

* Thu Jan  1 2009 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 3-1
- Initial RPM release
