%global momorel 2
Summary: A remote desktop system for GNOME
Name: vino

Version: 3.6.2
Release: %{momorel}m%{?dist}
URL: http://www.gnome.org/
License: GPL
Group: User Interface/Desktops

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: gtk3-devel
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: dbus-devel >= 1.2.4
BuildRequires: dbus-glib-devel >= 0.80
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: gcr-devel >= 3.0
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: pango-devel >= 1.24.0
BuildRequires: avahi-devel >= 0.6.24
BuildRequires: avahi-glib-devel >= 0.6.24
BuildRequires: libgcrypt-devel
BuildRequires: openssl-devel >= 0.9.8g
BuildRequires: libjpeg-devel >= 8a

Requires: gtk2
Requires: libglade2
Requires: GConf2
Requires: libgnomeui
Requires: libgcrypt
Requires(post): GConf2
Requires(preun): GConf2
Requires(pre): GConf2

%description
Vino is a VNC server for GNOME. It allows remote users to
connect to a running GNOME session using VNC.

%prep
%setup -q

%build
%configure \
    --disable-static \
    --enable-session-support=yes \
    --enable-http-server=yes \
    --disable-schemas-install \
    --enable-libnotify \
    --enable-gnome-keyring \
    --enable-avahi \
    --enable-ipv6 \
    --disable-schemas-install \
    --enable-telepathy=yes \
    --enable-network-manager \
    --enable-libunique=yes
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README docs/TODO docs/remote-desktop.txt
%{_sysconfdir}/xdg/autostart/vino-server.desktop
%{_bindir}/*
%{_libexecdir}/*
%{_datadir}/applications/vino-preferences.desktop
%{_datadir}/%{name}
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Vino.service
%{_datadir}/telepathy/clients/Vino.client
%{_datadir}/glib-2.0/schemas/org.gnome.Vino.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.Vino.gschema.xml
%{_datadir}/GConf/gsettings/org.gnome.Vino.convert

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.2-2m)
- rebuild against gnutls-3.2.0

* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for glib 2.33.2

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-3m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.6

* Fri Dec 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.28.2-2m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-4m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26.2-2m)
- rebuild against libjpeg-7

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-3m)
- fix autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against rpm-4.6

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.2-2m)
- rebuild against gnutls-2.4.1

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0
- delete patch1 (fixed)

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.1-2m)
- %%NoSource -> NoSource

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-1m)
- update to 2.17.5 (unstable)

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.13.5-5m)
- rebuild against gnutls-1.4.4-1m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.13.5-4m)
- rebuild against expat-2.0.0-1m

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.5-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.5-1m)
- update to 2.13.5

* Sun Jan  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.0-2m)
- BuildRequires: libgcrypt-devel >= 1.2.2-2m

* Tue Nov 22 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-1m)
- import from Fedora

* Tue Oct 12 2004 Mark McLoughlin <markmc@redhat.com> 2.8.1-1
- Update to 2.8.1
- Remove backported fixes

* Thu Oct  7 2004 Mark McLoughlin <markmc@redhat.com> 2.8.0.1-1.1
- Don't hang with metacity's "reduced resources" mode (#134240) 
- Improve the key repeat rate situation a good deal (#134451)

* Wed Sep 29 2004 Mark McLoughlin <markmc@redhat.com> 2.8.0.1-1
- Update to 2.8.0.1

* Tue Sep 21 2004 Mark McLoughlin <markmc@redhat.com> 2.8.0-1
- Update to 2.8.0
- Remove upstreamed work-without-gnutls patch

* Tue Sep  7 2004 Matthias Clasen <mclasen@redhat.com> 2.7.92-3
- Disable help button until there is help (#131632)
 
* Wed Sep  1 2004 Mark McLoughlin <markmc@redhat.com> 2.7.92-2
- Add patch to fix hang without GNU TLS (bug #131354)

* Mon Aug 30 2004 Mark McLoughlin <markmc@redhat.com> 2.7.92-1
- Update to 2.7.92

* Tue Aug 17 2004 Mark McLoughlin <markmc@redhat.com> 2.7.91-1
- Update to 2.7.91

* Mon Aug 16 2004 Mark McLoughlin <markmc@redhat.com> 2.7.90-2
- Define libgcrypt_version

* Thu Aug 12 2004 Mark McLoughlin <markmc@redhat.com> 2.7.90-1
- Update to 2.7.90

* Wed Aug  4 2004 Mark McLoughlin <markmc@redhat.com> 2.7.4-1
- Update to 2.7.4

* Tue Jul 13 2004 Mark McLoughlin <markmc@redhat.com> 2.7.3.1-1
- Initial build.
