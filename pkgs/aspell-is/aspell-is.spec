%global momorel 7

%define lang is
%define langrelease 0
Summary: Icelandic dictionaries for Aspell
Name: aspell-%{lang}
Version: 0.51.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Buildrequires: aspell >= 0.60
Requires: aspell >= 0.60
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for the following: Icelandic

%prep
%setup -q -n aspell-%{lang}-%{version}-%{langrelease}

%build
./configure
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

mv %{buildroot}%{_libdir}/aspell-0.60/?slenska.alias \
    %{buildroot}%{_libdir}/aspell-0.60/islenska.alias

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING Copyright
%{_libdir}/aspell-0.60/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.51.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.51.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.51.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.51.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.51.1-3m)
- fix filename (utf-8) (maybe broken)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.51.1-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.51.1-1m)
- import from Fedora

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 50:0.51.1-4
- Autorebuild for GCC 4.3

* Thu Mar 29 2007 Ivana Varekova <varekova@redhat.com> - 50:0.51.1-3  
- add documentation
- use configure script to create Makefile
- update default buildroot
- some minor spec changes

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 50:0.51.1-2.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 50:0.51.1-2.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 50:0.51.1-2.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jul 19 2005 Ivana Varekova <varekova@redhat.com> 50:0.51.1-2
- build with aspell-0.60.3


* Wed Mar 16 2005 Ivana Varekova <varekova@redhat.com> 50:0.51.1-1
- initial release
