%global momorel 6
%define upname MDP

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name: 		python-mdp
Version: 	2.5
Release: 	%{momorel}m%{?dist}
Summary: 	Modular toolkit for Data Processing
Group: 		Development/Languages
License: 	LGPLv3+ 
URL: 		http://mdp-toolkit.sourceforge.net
Source0: 	http://downloads.sourceforge.net/mdp-toolkit/%{upname}-%{version}.tar.gz
NoSource:	0
BuildRoot:  	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 
BuildRequires: 	python-devel >= 2.7
Requires: 	numpy >= 1.3.0

%description
Modular toolkit for Data Processing (MDP) is a data processing framework 
written in Python.  From the user's perspective, MDP consists of 
a collection of trainable supervised and unsupervised algorithms or 
other data processing units (nodes) 
that can be combined into data processing flows and more complex 
feed-forward network architectures.

%prep
%setup -q -n %{upname}-%{version}

%build
%{__python} setup.py build

%install
%{__rm} -fr %{buildroot}
%{__python} setup.py install --root  %{buildroot} --install-lib %{python_sitearch}


%clean
%{__rm} -fr %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES COPYRIGHT COPYING COPYING.LESSER README TODO
%{python_sitearch}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-3m)
- full rebuild for mo7 release

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5-2m)
- enable to build on x86_64

* Fri Mar 12 2010 Daniel Mclellan <daniel.mclellan@gmail.com> 
- (2.5-1m) 
- initial momonga release

* Tue Oct 21 2008 Sergio Pascual <sergio.pasra@gmail.com> - 2.4-1
- New upstream source

* Mon Oct 13 2008 Sergio Pascual <sergio.pasra@gmail.com> - 2.3-1
- Initial specfile
