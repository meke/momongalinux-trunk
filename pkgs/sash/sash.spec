%global momorel 14

Summary: A statically linked shell, including some built-in basic commands
Name: sash
Version: 3.7
Release: %{momorel}m%{?dist}
License: "distributable"
Group: System Environment/Shells
Source0: http://www.canb.auug.org.au/~dbell/programs/sash-%{version}.tar.gz
NoSource: 0
Patch0: sash-3.6-misc.patch
Patch1: sash-3.6-scriptarg.patch
Patch2: sash-3.7-losetup.patch
Patch4: sash-3.7-kernel2629.patch
BuildRequires: zlib-devel >= 1.2.3-1m
URL: http://www.canb.auug.org.au/~dbell/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Sash is a simple, standalone, statically linked shell which includes
simplified versions of built-in commands like ls, dd and gzip.  Sash
is statically linked so that it can work without shared libraries, so
it is particularly useful for recovering from certain types of system
failures.  Sash can also be used to safely upgrade to new versions of
shared libraries.

%prep
%setup -q
%patch0 -p1 -b ".misc"
%patch1 -p1 -b ".scriptarg"
%patch2 -p1 -b ".losetup"
%patch4 -p1 -b .kernel2629~

%build
make RPM_OPT_FLAGS="%{optflags}"

%install
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}%{_mandir}/man8

install -s -m755 sash %{buildroot}/sbin
install -m644 sash.1 %{buildroot}%{_mandir}/man8/sash.8

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/sbin/sash
%{_mandir}/man8/sash.8*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7-12m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7-9m)
- apply kernel2629 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7-8m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7-7m)
- update Patch2 for fuzz=0
- License: "distributable"

* Tue Apr 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.7-6m)
- modify patch3 (kernel-2.6.25) build fix

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7-5m)
- rebuild against gcc43

* Wed Feb 28 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.7-4m)
- Patch3: sash-3.7-kh.patch

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.7-3m)
- rebuild against zlib-1.2.3 (CAN-2005-1849)

* Sat Jul  9 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (3.7-2m)
- rebuild against zlib-devel 1.2.2-2m, which fixes CAN-2005-2096.

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.7-1m)
- version up

* Wed Sep  8 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (3.6-6m)
- remove patch3. Please use glibc-kernheaders package on kernel 2.6.

* Thu Aug 26 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (3.6-5m)
- add patch3 for kernel 2.6

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-4m)
- rebuild against zlib 1.1.4-5m

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-3m)
- add URL tag

* Tue Feb 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.6-2m)
- rebuild against zlib-1.1.4-4m

* Tue Dec 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.6-1m)
- update to 3.6
- revise some patches for sash-3.6

* Fri Mar 15 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.4-12k)
- rebuild against zlib-1.1.4

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org>
- Kodaraized.

* Wed Jul 19 2000 Jakub Jelinek <jakub@redhat.com>
- rebuild to cope with glibc locale binary incompatibility

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 13 2000 Preston Brown <pbrown@redhat.com>
- FHS paths

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- rebuild to gzip man page

* Mon Oct 04 1999 Cristian Gafton <gafton@redhat.com>
- rebuild against new glibc in the sparc tree

* Mon Aug  2 1999 Jeff Johnson <jbj@redhat.com>
- upgrade to 3.3 (#4301).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Fri Dec 18 1998 Preston Brown <pbrown@redhat.com>
- bumped spec number for initial rh 6.0 build
