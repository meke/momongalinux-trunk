%global momorel 10

Summary: xyaku is an English-Japanese translation program and more under X11.
Name: xyaku 
Version: 1.4.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Text
Source: http://home.catv.ne.jp/pp/ginoue/software/%{name}/%{name}-%{version}.tar.gz
Patch0: xyaku-1.3.0-keymap.patch
Url: http://home.catv.ne.jp/pp/ginoue/software/%{name}/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: grep edict >= 1.0-0.20020426.3m ruby

%description
xyaku is an English-Japanese translation program and more under X11.
Furthermore, xyaku is yet another Web search engine front-end.

%prep
%setup -q 
%patch0 -p1

%build
./configure --prefix=/usr --with-egrep=/bin/egrep --with-edict=/usr/share/dict/edict
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
mkdir -p %{buildroot}%{_datadir}/X11/app-defaults
install -m 644 Xyaku.ad %{buildroot}%{_datadir}/X11/app-defaults/Xyaku

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog INSTALL NEWS TODO
%{_bindir}/xyaku
%{_datadir}/X11/app-defaults/Xyaku
%{_libexecdir}/xyaku

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-10m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.0-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.0-4m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.0-3m)
- rebuild against gcc43

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.0-3m)
- revised installdir

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.0-2m)
- change Requires:

* Tue Jan 21 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0
- change Source: and url: URI
- thanks to Tomoaki Yoshida [Momonga-devel.ja:01268]

* Sun Apr 14 2002 Hidetomo Machi <mcht@kondara.org>
- (1.3.0-8k)
- grab /usr/libexec/xyaku (%files)

* Mon Mar 18 2002 Toru Hoshina <t@kondara.org>
- (1.3.0-6k)
- should implicitely require ruby.

* Fri Jan 18 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.3.0-4k)
- follow an update of edict

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (1.3.0-2k)
- version up.

* Sat Jun 24 2000 Uechi Yasumasa <uh@kondara.org>
- 1.2.1-1k
- version up to 1.2.1

* Tue Apr 04 2000 Chiaki Nakata <hanibi@kondara.org>
- change description section.

* Tue Apr 04 2000 Chiaki Nakata <hanibi@kondara.org>
- First release in Kondara MNU/Linux
