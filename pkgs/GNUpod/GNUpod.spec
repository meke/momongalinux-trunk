%global momorel 16

%define perlver 5.12.1

Summary: GNUpod, Manage your iPod
Name: GNUpod
Version: 0.99.8
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: 	Applications/Multimedia
URL: http://www.gnu.org/software/gnupod/
Source0: http://savannah.gnu.org/download/gnupod/gnupod-%{version}.tgz 
NoSource: 0
Patch1: gnupod-0.99.7-install-info.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= %{perlver}
BuildRequires: perl-MP3-Info
BuildRequires: perl-XML-Parser
BuildRequires: perl-Getopt-Mixed
BuildRequires: perl-Unicode-String
BuildRequires: perl-XML-Simple
Requires: rescan-scsi-bus
BuildArch: noarch
Requires(post): info
Requires(preun): info

%description
GNUpod, Manage your iPod

%prep
%setup -q -n gnupod-%{version}
%patch1 -p1 -b .install-info~

%build
perl -pi -e 's/test \$PERL/test "\$PERL"/' configure
export PERL="%{__perl} -I%{buildroot}%{perl_vendorlib}"
%configure

%clean
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
%makeinstall

# removing unwanting files.
rm -f %{buildroot}/usr/share/info/dir
 
%post
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/gnupod.info

%preun
if [ $1 = 0 ] ;then
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/gnupod.info
fi

%files
%defattr(-,root,root)
%{_bindir}/*
%{perl_vendorlib}/GNUpod/*
%{_infodir}/*.info*
%doc AUTHORS BUGS CHANGES COPYING FAQ README* TODO
%doc doc/*
%{_datadir}/man/man1/*


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-16m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-15m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-14m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-13m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.8-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.8-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.8-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.8-1m)
- update to 0.99.8

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.7-7m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.7-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.7-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.99.7-4m)
- rebuild against perl-5.10.1

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.7-3m)
- use vendor

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.7-2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.7-1m)
- update to 0.99.7
-- drop Patch0, not needed
-- update Patch1 for fuzz=0
-- do not use %%makeinstall
- License: GPLv3+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.98.3-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.3-4m)
- %%NoSource -> NoSource

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.98.3-3m)
- rebuild against perl-5.10.0

* Sun Jun 11 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.98.3-2m)
- modify %%files

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.98.3-1m)
- rebuild against perl-5.8.8

* Mon Jun 13 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.98-2m)
- rebuilt against perl-5.8.7

* Wed Apr 13 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98-1m)
- update 0.98

* Sun Aug 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.29-0.1.7m)
- rebuild against perl-5.8.5

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.29-0.1.6m)
- revised spec for enabling rpm 4.2.

* Sun Nov 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.29-0.1.5m)
- move iPod.pm to %%{perl_sitelib} because this file is architecture independent

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.29-0.1.4m)
- rebuild against perl-5.8.2

* Sat Nov  8 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.29-0.1.3m)
- rebuild against perl-5.8.1

* Sun Oct 19 2003 KIMITAKE Shibata <siva@momonga-linux.org>
- (0.29-0.1.2m)
- Change install-info dir

* Sat Oct 18 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.29-0.1.1m)
- update to 0.29-rc1
- install info file

* Wed Feb  5 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.26-3m)
- remove  Patch1: momonga-GNUpod-0.26-vfat.patch
  please mount ipod with shortname=winnt

* Fri Jan 24 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.26-2m)
- add Requires: rescan-scsi-bus

* Fri Jan 24 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.26-1m)
- import to momonga
