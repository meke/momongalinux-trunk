%global momorel 3
%global srcrel 12608
%global kdever 4.5.3
%global kdebaserel 1m

Summary: audio file converter for Konqueror
Name: audiokonverter
Version: 5.9.1
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://www.kde-apps.org/content/show.php?content=12608
Group: Applications/Multimedia
Source0: http://www.kde-apps.org/CONTENT/content-files/%{srcrel}-%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-5.8.2-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdebase >= %{kdever}-%{kdebaserel}
Requires: faac
Requires: ffmpeg
Requires: flac
Requires: lame
Requires: soundkonverter
Requires: vorbis-tools
BuildArch: noarch

%description
This is audiokonverter, a small utility to easily
convert from OGG, MP3, AAC, M4A, FLAC, WMA, RealAudio, Musepack,
Wavpack, WAV and movies to MP3, OGG, M4A, WAV and FLAC.

YOU NEED to make everything work:
mplayer, ffmpeg, lame, oggenc, oggdec, flac, faac, faad, mppdec, wvunpack

OPTIONAL are:
id3lib, vorbis-tools, metaflac, apetag

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{_kde4_bindir}
mkdir -p %{buildroot}%{_kde4_datadir}/kde4/services/ServiceMenus
mkdir -p %{buildroot}%{_kde4_datadir}/mimelnk/audio

# install scripts
install -m 755 anytowav4 %{buildroot}%{_kde4_bindir}
install -m 755 audioconvert4 %{buildroot}%{_kde4_bindir}
install -m 755 movie2sound4 %{buildroot}%{_kde4_bindir}
install -m 755 oggdrop-lx %{buildroot}%{_kde4_bindir}

# install desktop files
install -m 644 audioconvert4.desktop %{buildroot}%{_kde4_datadir}/kde4/services/ServiceMenus/
install -m 644 audiofrommovie4.desktop %{buildroot}%{_kde4_datadir}/kde4/services/ServiceMenus

# install mime files
install -m 644 x-flv.desktop %{buildroot}%{_kde4_datadir}/mimelnk/audio/
install -m 644 x-wavpack.desktop %{buildroot}%{_kde4_datadir}/mimelnk/audio/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING Changelog README
%{_kde4_bindir}/anytowav4
%{_kde4_bindir}/audioconvert4
%{_kde4_bindir}/movie2sound4
%{_kde4_bindir}/oggdrop-lx
%{_kde4_datadir}/kde4/services/ServiceMenus/audioconvert4.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/audiofrommovie4.desktop
%{_kde4_datadir}/mimelnk/audio/x-flv.desktop
%{_kde4_datadir}/mimelnk/audio/x-wavpack.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.1-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.1-1m)
- update to 5.9.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9-2m)
- full rebuild for mo7 release

* Sun Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9-1m)
- version 5.9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.8.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.8.2-1m)
- version 5.8.2
- update desktop.patch

* Sun Jun 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.8.1-1m)
- version 5.8.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.8-0.1.4m)
- rebuild against rpm-4.6

* Tue Sep  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.8-0.1.3m)
- enable converting from flv to audio

* Wed Aug 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.8-0.1.2m)
- change support from KDE3 to KDE4

* Sat Aug  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8-0.1.1m)
- update to 5.8-beta

* Sun May  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-1m)
- update to 5.7.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7-2m)
- rebuild against gcc43

* Wed Apr  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7-1m)
- update to 5.7

* Fri Jan 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.6-1m)
- version 5.6

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.1-1m)
- update to 5.5.1

* Thu Apr 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.5-2m)
- remove x-wavpack.desktop, it's conflicting with soundkonverter-0.3.2
- Requires: soundkonverter

* Wed Apr 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.5-1m)
- initial package for Momonga Linux
