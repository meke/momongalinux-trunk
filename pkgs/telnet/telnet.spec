%global momorel 27

Summary: The client program for the telnet remote login protocol.
Name: telnet
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
URL: http://www.hcs.harvard.edu/~dholland/computers/old-netkit.html
Source0: ftp://sunsite.unc.edu/pub/Linux/system/network/netkit/netkit-telnet-%{version}.tar.gz
Source2: telnet-client.tar.gz
Source3: telnet-xinetd
Patch1: telnet-client-cvs.patch
Patch5: telnetd-0.17.diff
Patch6: telnet-0.17-env.patch
Patch7: telnet-0.17-issue.patch
Patch8: telnet-0.17-sa-01-49.patch
Patch9: telnet-0.17-env-5x.patch
Patch10: telnet-0.17-pek.patch
Patch11: telnet-0.17-8bit.patch
Patch12: telnet-0.17-argv.patch
Patch13: telnet-0.17-conf.patch
Patch14: telnet-0.17-cleanup_race.patch
Patch15: telnetd-0.17-pty_read.patch
Patch16: telnet-0.17-CAN-2005-468_469.patch
Patch100: telnet-0.17-makefix.patch

BuildRequires: ncurses-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0

%description
Telnet is a popular protocol for logging into remote systems over the
Internet.  The telnet package provides a command line telnet client.

Install the telnet package if you want to telnet to remote machines.

%package server
Requires: xinetd
Group: System Environment/Daemons
Summary: The server program for the telnet remote login protocol.

%description server
Telnet is a popular protocol for logging into remote systems over the
Internet.  The telnet-server package  a telnet daemon, which will
support remote logins into the host machine.  The telnet daemon is
enabled by default.  You may disable the telnet daemon by editing
/etc/xinet.d/telnet

Install the telnet-server package if you want to support remote logins
to your own machine.

%prep
%setup -q -n netkit-telnet-%{version}

mv telnet telnet-NETKIT
%setup -T -D -q -a 2 -n netkit-telnet-%{version}
%patch1 -p0 -b .cvs
%patch5 -p0 -b .fix
%patch6 -p1 -b .env
%patch10 -p0 -b .pek
%patch7 -p1 -b .issue
%patch8 -p1 -b .sa-01-49
%patch11 -p1 -b .8bit
%patch12 -p1 -b .argv
%patch13 -p1 -b .confverb
%patch14 -p1 -b .cleanup_race
%patch15 -p0 -b .pty_read
%patch16 -p1 -b .CAN-2005-468_469
%patch100 -p1 -b .make

%build
sh configure --with-c-compiler=gcc
perl -pi -e '
    s,^CC=.*$,CC=cc,;
    s,-O2,\$(RPM_OPT_FLAGS),;
    s,^BINDIR=.*$,BINDIR=%{_bindir},;
    s,^MANDIR=.*$,MANDIR=%{_mandir},;
    s,^SBINDIR=.*$,SBINDIR=%{_sbindir},;
    ' MCONFIG

# XXX hack around gcc-2.96 problem
%ifarch i386
export RPM_OPT_FLAGS="`echo $RPM_OPT_FLAGS | sed -e s/-O2/-O0/`"
%endif

LANG=C make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/usr/sbin
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_mandir}/man5
mkdir -p %{buildroot}%{_mandir}/man8

make INSTALLROOT=%{buildroot} MANDIR=%{_mandir} install

mkdir -p %{buildroot}/etc/xinetd.d
install -m644 %{SOURCE3} %{buildroot}/etc/xinetd.d/telnet

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/telnet
%{_mandir}/man1/telnet.1*

%files server
%defattr(-,root,root)
%config(missingok,noreplace) /etc/xinetd.d/telnet
%{_sbindir}/in.telnetd
%{_mandir}/man5/issue.net.5*
%{_mandir}/man8/in.telnetd.8*
%{_mandir}/man8/telnetd.8*

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-27m)
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-26m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-25m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-24m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-23m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-21m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-20m)
- rebuild against gcc43

* Tue Mar 29 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.17-19m)
- [SECURITY] CAN-2005-468, CAN-2005-469, fix race condition in telnetd on wtmp lock
- sync with Fedora

* Sat Aug 14 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.17-18m)
- remove telnet.desktop

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.17-17m)
- rebuild against new environment.

* Tue Apr 01 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (0.17-16k)
- merge patches from rawhide telnet-0.17-20

* Thu Nov 01 2001 Motonobu Ichimura <famao@kondara.org>
- (0.17-14k)
- bug fix release

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (0.17-12k)
- merge from Jirai.

* Tue Mar  6 2001 Daiki Matsuda <dyky@df-usa.com>
- (0.16-18k)
- errased IPv6 function with %{_ipv6} macro

* Thu Jan  4 2001 Daiki Matsuda <dyky@df-usa.com>
- (0.16-17k)
- set disable on default

* Sat Dec  2 2000 Daiki Matsuda <dyky@df-usa.com>
- (0.16-14k)
- modified spec file and errased telnet.fhs.patch for compatibility

* Fri Aug 11 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91 with xinetd.

* Mon Jun 26 2000 Masaaki Noro <noro@flab.fujitsu.co.jp>
- change telnet client to original , and enable IPv6 option.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.16-6).

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Wed Feb 16 2000 Shingo Akagaki <dora@kondara.org>
- fix .desktop entry

* Fri Feb 11 2000 Bill Nottingham <notting@redhat.com>
- fix description

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- wmconfig gone

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed
- fix description

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-netkit-telnet-20000115

* Tue Jan  4 2000 Bill Nottingham <notting@redhat.com>
- split client and server

* Tue Dec 21 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Wed Dec 1 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-netkit-telnet-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Oct 10 1999 Matt Wilson <msw@redhat.com>
- corrected the Terminal setting of the .desktop (needs to be 'true' not '1')

* Sat Sep 24 1999 Preston Brown <pbrown@redhat.com>
- red hat .desktop entry

* Sat Aug 21 1999 Jeff Johnson <jbj@redhat.com>
- rebuild for 6.1.

* Wed Aug 18 1999 Bill Nottingham <notting@redhat.com>
- don't trust random TERM variables in telnetd (#4560)

* Wed Jun  2 1999 Jeff Johnson <jbj@redhat.com>
- fix (#3098).

* Thu May 27 1999 Antti Andreimann <Antti.Andreimann@mail.ee>
- fixed the problem with escape character (it could not be disabled)
- changed the spec file to use %setup macro for unpacking telnet-client

* Thu Apr 15 1999 Jeff Johnson <jbj@redhat.com>
- use glibc utmp routines.

* Thu Apr  8 1999 Jeff Johnson <jbj@redhat.com>
- fix the fix (wrong way memcpy).

* Wed Apr  7 1999 Jeff Johnson <jbj@redhat.com>
- fix "telnet localhost" bus error on sparc64 (alpha?).

* Tue Apr  6 1999 Jeff Johnson <jbj@redhat.com>
- use OpenBSD telnet client (and fix minor core dump with .telnetrc #247)

* Thu Mar 25 1999 Erik Troan <ewt@redhat.com>
- use openpty in telnetd

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Apr 24 1998 Cristian Gafton <gafton@redhat.com>
- compile C++ code using egcs

* Tue Apr 14 1998 Erik Troan <ewt@redhat.com>
- built against new ncurses

* Wed Oct 29 1997 Donnie Barnes <djb@redhat.com>
- added wmconfig entry

* Tue Jul 15 1997 Erik Troan <ewt@redhat.com>
- initial build
