%global momorel 1
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:           nml
Version:        0.2.4
Release: %{momorel}m%{?dist}
Summary:        NewGRF Meta Language compiler
Group:		Development/Languages
License:        GPLv2+
URL:            http://dev.openttdcoop.org/projects/nml
Source0:        http://bundles.openttdcoop.org/nml/releases/%{version}/%{name}-%{version}.src.tar.gz
NoSource: 0

BuildArch:      noarch
BuildRequires:  python2-devel python-setuptools-devel
Requires:       python-imaging python-ply python-setuptools

%description
A tool to compile nml files to grf or nfo files, making newgrf coding easier.


%prep
%setup -q


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

bzip2 docs/nmlc.1
install -Dpm 644 docs/nmlc.1.bz2 %{buildroot}%{_mandir}/man1/nmlc.1.bz2
rm docs/nmlc.1.bz2

 
%files
%doc docs/*
%doc %{_mandir}/man1/nmlc.1.bz2
%{_bindir}/nmlc
%{python_sitelib}/*


%changelog
* Sat Apr 20 2013 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.2.4-1m)
- update to 0.2.4

* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-1m)
- import from fedora

