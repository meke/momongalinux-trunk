%global momorel 8

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?ruby_siteib: %global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")}
%{!?ruby_sitearch: %global ruby_sitearch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")}

%define codename DrO_o

Summary: A modular audio framework and plugin architecture
Name: xmms2
Version: 0.8
Release: %{momorel}m%{?dist}
License: LGPLv2+ and GPLv2+ and BSD
Group: Applications/Multimedia
URL: http://wiki.xmms2.xmms.se/
# We can't use the upstream source tarball as-is, because it includes an mp4 decoder.
# http://downloads.sourceforge.net/xmms2/%{name}-%{version}%{codename}.tar.bz2
# Cleaning it is simple, just rm -rf src/plugins/mp4
Source0: %{name}-%{version}%{codename}-clean.tar.bz2
Source1: xmms2-client-launcher.sh
# Use libdir properly for Fedora multilib
Patch1:			xmms2-0.8DrO_o-use-libdir.patch
# Set default output to pulse
Patch2:			xmms2-0.8DrO_o-pulse-output-default.patch
# Don't add extra CFLAGS, we're smart enough, thanks.
Patch4:			xmms2-0.8DrO_o-no-O0.patch
# More sane versioning
Patch5:			xmms2-0.8DrO_o-moresaneversioning.patch
# Fix xsubpp location
Patch6:			xmms2-0.8DrO_o-xsubpp-fix.patch
# ###
Patch7:			xmms2-0.8DrO_o-test.patch
### support ffmpeg-2.x
Patch100:		xmms2-0.8-ffmpeg-0.11.patch
Patch101:		xmms2-0.8-libav-9.patch
Patch102:		xmms2-0.8-libav-9-p2.patch
Patch103:		xmms2-0.9-ffmpeg2.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: CUnit >= 2.1.0
BuildRequires: Pyrex
BuildRequires: alsa-lib-devel
BuildRequires: avahi-devel
BuildRequires: avahi-glib-devel
BuildRequires: boost-devel >= 1.43.0
BuildRequires: doxygen
BuildRequires: ecore-devel >= 0.9.9.49898
BuildRequires: eina-devel  >= 0.9.9.49898
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: fftw-devel
BuildRequires: flac-devel
BuildRequires: gamin-devel
BuildRequires: gnome-vfs2-devel
BuildRequires: jack-devel
BuildRequires: libao-devel >= 1.0.0
BuildRequires: libcdio-devel >= 0.90
BuildRequires: libdiscid-devel
BuildRequires: libmodplug-devel >= 0.8.8.4
BuildRequires: libmpcdec-devel
BuildRequires: libofa-devel
BuildRequires: libsamplerate-devel
BuildRequires: libshout-devel
BuildRequires: libsmbclient-devel
BuildRequires: libxml2-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: perl-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: sqlite-devel

%description
XMMS2 is an audio framework, but it is not a general multimedia player - it 
will not play videos. It has a modular framework and plugin architecture for 
audio processing, visualisation and output, but this framework has not been 
designed to support video. Also the client-server design of XMMS2 (and the 
daemon being independent of any graphics output) practically prevents direct 
video output being implemented. It has support for a wide range of audio 
formats, which is expandable via plugins. It includes a basic CLI interface 
to the XMMS2 framework, but most users will want to install a graphical XMMS2 
client (such as gxmms2 or esperanza).

%package devel
Summary:	Development libraries and headers for XMMS2
Group:		Development/Libraries
Requires:	glib2-devel, qt-devel >= 4.7.0, boost-devel
Requires:	pkgconfig
Requires:	%{name} = %{version}-%{release}

%description devel
Development libraries and headers for XMMS2. You probably need this to develop
or build new plugins for XMMS2.

%package docs
Summary:	Development documentation for XMMS2
Group:		Documentation
Requires:	%{name} = %{version}-%{release}

%description docs
API documentation for the XMMS2 modular audio framework architecture.

%package python
Summary:	Python support for XMMS2
Group:		Applications/Multimedia
Requires:	%{name} = %{version}-%{release}

%description python
Python bindings for XMMS2.

%package perl
Summary:	Perl support for XMMS2
License:	GPL+ or Artistic
Group:		Applications/Multimedia
Requires:	%{name} = %{version}-%{release}
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description perl
Perl bindings for XMMS2.

%package ruby
Summary:	Ruby support for XMMS2
Group:		Applications/Multimedia
Requires:	%{name} = %{version}-%{release}
Requires:	ruby(abi) = 1.9.1

%description ruby
Ruby bindings for XMMS2.

%package -n nyxmms2
Summary:	Commandline client for XMMS2
Group:		Applications/Multimedia
Requires:	%{name} = %{version}-%{release}

%description -n nyxmms2
nyxmms2 is the new official commandline client for XMMS2. It can be run in
either shell-mode (if started without arguments), or in inline-mode where
it executes the command passed as argument directly.

%prep
%setup -q -n %{name}-%{version}%{codename}
%patch1 -p1 -b .plugins-use-libdir
%patch2 -p1 -b .default-output-pulse
%patch4 -p1 -b .noO0
%patch5 -p1 -b .versionsanity
%patch7 -p1 -b .test


%patch100 -p1
%patch101 -p1
%patch102 -p1
%patch103 -p1

# This header doesn't need to be executable
chmod -x src/include/xmmsclient/xmmsclient++/dict.h

# Clean up paths in wafadmin
#WAFADMIN_FILES=`find wafadmin/ -type f`
#for i in $WAFADMIN_FILES; do
#	sed -i 's|/usr/lib|%%{_libdir}|g' $i
#done
#sed -i 's|"lib"|"%%{_lib}"|g' wscript

%build
export CFLAGS="%{optflags}"
export CPPFLAGS="%{optflags}"
export LIBDIR="%{_libdir}"
export PYTHONDIR="%{python_sitearch}"
export XSUBPP="%{_bindir}/xsubpp"
./waf configure --prefix=%{_prefix} --libdir=%{_libdir} --with-ruby-libdir=%{ruby_sitelib} --with-ruby-archdir=%{ruby_sitearch} \
  --with-perl-archdir=%{perl_archlib} --with-pkgconfigdir=%{_libdir}/pkgconfig -j1
# Hacky, hacky, hacky.
patch -p0 < %{_sourcedir}/xmms2-0.8DrO_o-xsubpp-fix.patch
./waf configure --prefix=%{_prefix} --libdir=%{_libdir} --with-ruby-libdir=%{ruby_sitelib} --with-ruby-archdir=%{ruby_sitearch} \
  --with-perl-archdir=%{perl_archlib} --with-pkgconfigdir=%{_libdir}/pkgconfig -j1
./waf build -v %{?_smp_mflags}
# make the docs
doxygen

%install
rm -rf %{buildroot}
./waf -j1 install --destdir=%{buildroot} --prefix=%{_prefix} --libdir=%{_libdir} --with-ruby-libdir=%{ruby_sitearch} --with-perl-archdir=%{perl_archlib} --with-pkgconfigdir=%{_libdir}/pkgconfig

# exec flags for debuginfo
chmod +x %{buildroot}%{_libdir}/%{name}/* %{buildroot}%{_libdir}/libxmmsclient*.so* %{buildroot}%{python_sitearch}/xmmsclient/xmmsapi.so \
	%{buildroot}%{perl_archlib}/auto/Audio/XMMSClient/XMMSClient.so %{buildroot}%{ruby_sitearch}/xmmsclient_*.so

# Convert to utf-8
for i in %{buildroot}%{_mandir}/man1/*.gz; do
	gunzip $i;
done
for i in %{buildroot}%{_mandir}/man1/*.1 xmms2-0.8DrO_o.ChangeLog; do
	iconv -o $i.iso88591 -f iso88591 -t utf8 $i
	mv $i.iso88591 $i
done

install -m0755 %{SOURCE1} %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS xmms2-0.8DrO_o.ChangeLog COPYING COPYING.GPL COPYING.LGPL INSTALL README TODO
%{_bindir}/%{name}*
%{_libdir}/libxmmsclient*.so.*
%{_libdir}/%{name}
%{_mandir}/man1/%{name}*
%{_datadir}/pixmaps/%{name}*
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}/
%{_libdir}/libxmmsclient*.so
%{_libdir}/pkgconfig/%{name}-*.pc

%files docs
%defattr(-,root,root,-)
%doc doc/xmms2/html

%files perl
%defattr(-,root,root,-)
%{perl_archlib}/Audio/
%{perl_archlib}/auto/Audio/

%files python
%defattr(-,root,root,-)
%{python_sitearch}/xmmsclient/

%files ruby
%defattr(-,root,root,-)
%{ruby_sitelib}/xmmsclient*
%{ruby_sitearch}/xmmsclient_*.so

%files -n nyxmms2
%defattr(-,root,root,-)
%{_bindir}/nyxmms2

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-7m)
- rebuild against perl-5.18.2

* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-6m)
- rebuild against ffmpeg

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-3m)
- rebuild against perl-5.16.3

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-2m)
- rebuild against libcdio-0.90

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-28m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-27m)
- rebuild against perl-5.16.1

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-26m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.7-25m)
- rebuild for boost 1.50.0

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-24m)
- rebuild against perl-5.16.0

* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-23m)
- rebuild against libmodplug-0.8.8.4

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-22m)
- rebuild for boost-1.48.0

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7-21m)
- use RbConfig

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-20m)
- rebuild against perl-5.14.2

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-19m)
- rebuild for boost

* Tue Jun 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-18m)
- add -j1 to avoid a possible dead lock with python-2.7.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-17m)
- rebuild against perl-5.14.1

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7-16m)
- rebuild against ffmpeg-0.6.1-0.20110514

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-15m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7-14m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-13m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-12m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-11m)
- rebuild against boost-1.46.0

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7-10m)
- rebuild agaisnt ecore eina

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-9m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-8m)
- rebuild against boost-1.44.0

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-7m)
- rebuild against perl-5.12.2

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-5m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7-4m)
- Require ruby(abi)-1.9.1

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7-3m)
- rebuild against ruby-1.9.2

* Sun Jul 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-2m)
- fix build on i686, add -j1 to ./waf configure

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linnux.org>
- (0.7-1m)
- update to 0.7
- rebuild against libcdio-0.82

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5-15m)
- rebuild agaisnt ecore-0.9.9.49898 eina-0.9.9.49898

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-14m)
- rebuild against boost-1.43.0

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5-13m)
- rebuild agaisnt ecore-0.9.9.49539 eina-0.9.9.49539

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-12m)
- rebuild against perl-5.12.1

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-11m)
- rebuild against libao-1.0.0

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-10m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-9m)
- rebuild against openssl-1.0.0

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5-8m)
- rebuild agaisnt eina-devel 0.9.9.063
- clean spec

* Sun Nov 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-7m)
- rebuild agaisnt boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-5m)
- rebuild agaisnt boost-1.40.0

* Thu Aug 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-4m)
- BuildRequires: eina-devel >= 0.0.2.062, ecore-devel >= 0.9.9.062

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-3m)
- rebuild against new EFL

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-2m)
- rebuild against perl-5.10.1

* Fri Jul  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-1m)
- rebase
- apply ffmpeg patch by meke
- rename jack-audio-connection-kit-devel -> jack-devel
- rename avahi-compat-libdns_sd-devel -> avahi-devel
--
-- * Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-6
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
-- 
-- * Sun Jan 18 2009 Tomas Mraz <tmraz@redhat.com> - 0.5-5
-- - rebuild with new openssl
-- 
-- * Tue Jan 13 2009 Adrian Reber <adrian@lisas.de> - 0.5-4
-- - Rebuild for libcdio-0.81
-- 
-- * Wed Dec 17 2008 Benjamin Kosnik  <bkoz@redhat.com> - 0.5-3
-- - Rebuild for boost-1.37.0.
-- 
-- * Wed Dec 10 2008 Tom "spot" Callaway <tcallawa@redhat.com> 0.5-2
-- - new docs subpackage
-- - many cleanups from package review
-- 
-- * Thu Dec 4 2008 Tom "spot" Callaway <tcallawa@redhat.com> 0.5-1
-- - Initial package for Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.1.1.9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-0.1.1.8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-0.1.1.7m)
- %%NoSource -> NoSource

* Sat Jun 24 2006 Masahiro Takahata <muradaikan@momonga-linux.org>
- (0.1-0.1.1.6m)
- depend sqlite3 -> sqlite

* Thu Jul 28 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1-0.1.1.5m)
- added SDL_ttf-devel in BuildPreReq

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1-0.1.1.4m)
- rebuild against flac-1.1.2

* Sat Jul  2 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1-0.1.1.3m)
- enable x86_64.
- add patch1(xmms2-0.1DR1.1-x86_64.patch)

* Mon Jun 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1-0.1.1.2m)
- add BuildPreReq Pyrex

* Mon Jun 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1-0.1.1.1m)
- initial import to Momonga
