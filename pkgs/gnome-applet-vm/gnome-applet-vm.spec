%global momorel 9

Summary: Simple virtual domains monitor which embeds itself in the GNOME panel
Name: gnome-applet-vm
Version: 0.2.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
%define upstreamname %{name}-%{version}-rc1
Source: http://people.redhat.com/kzak/gnome-applet-vm/v0.2/%{upstreamname}.tar.bz2
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: https://fedorahosted.org/gnome-applet-vm
ExclusiveArch: %{ix86} x86_64

%define glib2_version 2.2.0
%define pango_version 1.2.0
%define gtk2_version 2.6.0
%define libgnomeui_version 2.3.0
%define gnome_panel_version 2.5.91
%define libbonoboui_version 2.3.0
%define libglade_version 2.4.0

BuildRequires:  libvirt-devel
BuildRequires:  glib2-devel >= %{glib2_version}
BuildRequires:  gtk2-devel >= %{gtk2_version}
BuildRequires:  libgnomeui-devel >= %{libgnomeui_version}
BuildRequires:  libglade2-devel >= %{libglade_version}
BuildRequires:  gnome-panel-devel >= %{gnome_panel_version}
BuildRequires:  pango-devel >= %{pango_version}
BuildRequires:  /usr/bin/automake
BuildRequires:  libbonoboui-devel >= %{libbonoboui_version}
BuildRequires:	dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  autoconf gettext
BuildRequires:  gnome-doc-utils-devel
BuildRequires:  perl-XML-Parser
BuildRequires:  pkgconfig
BuildRequires:  libxml2-devel

Requires:	gtk2 >= %{gtk2_version}
Requires:	libvirt
Requires:	gnome-panel >= %{gnome_panel_version}
Requires:	usermode
Requires:	virt-manager

Requires(post): /sbin/ldconfig, GConf2, rarian
Requires(postun): /sbin/ldconfig

%description
The gnome-applet-vm is GNOME panel applet for monitoring and controlling
virtual machines.

%prep
%setup -q -n %{upstreamname}

%build
%configure --enable-consolehelper
make CFLAGS+="`pkg-config --cflags libgnomeui-2.0`" LIBS+="`pkg-config --libs libgnomeui-2.0`"

%install
rm -rf $RPM_BUILD_ROOT

export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=$RPM_BUILD_ROOT
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

# Clean up unpackaged files
rm -rf $RPM_BUILD_ROOT%{_localstatedir}/scrollkeeper
# userhelper
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
ln -s consolehelper $RPM_BUILD_ROOT/%{_bindir}/vm_applet_wrapper
rm -rf $RPM_BUILD_ROOT/ChangeLog

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig
/usr/bin/scrollkeeper-update

export GCONF_CONFIG_SOURCE=`/usr/bin/gconftool-2 --get-default-source`
/usr/bin/gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/vm-applet.schemas > /dev/null

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING NEWS README
%{_datadir}/pixmaps/vm-applet
%{_datadir}/gnome-2.0/ui/*
%{_datadir}/gnome/help/*
%{_datadir}/omf/*
%{_libdir}/bonobo/servers/*
%{_libexecdir}/*
%{_sbindir}/*
%{_bindir}/*
%{_sysconfdir}/gconf/schemas/*
%config %{_sysconfdir}/pam.d/vm_applet_wrapper
%config %{_sysconfdir}/security/console.apps/vm_applet_wrapper

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-8m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-7m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-4m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-3m)
- rebuild against rpm-4.6

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-2m)
- modify Requires and %%files

* Sun May 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-1m)
- import from Fedora to Momonga

* Mon Mar  3 2008 Karel Zak <kzak@redhat.com> 0.2.0-2
- update tarball (forgot -rc1 suffix)
- fix URL and Source:

* Mon Mar  3 2008 Karel Zak <kzak@redhat.com> 0.2.0-1
- upgrade to 0.2.0

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.1.2-3
- Autorebuild for GCC 4.3

* Mon Apr 16 2007 Karel Zak <kzak@redhat.com> 0.1.2-2
- sync with upstream
- remove dependence on dbus libs
- fix collaboration with virt-manager
- fix rh#213790 - not able to create a new virtual machine via the applet
- fix rh#211560 - s/button-press-event/button-release-event/ (GTK events)

* Thu Apr 12 2007 Karel Zak <kzak@redhat.com> 0.1.0-2
- fix rpmlint issues

* Wed Sep 27 2006 Karel Zak <kzak@redhat.com> 0.1.0-1
- upgrade to stable upstream release
- fix build requires
- fix #205930 - missing dependency for virt-manager

* Thu Jul 27 2006 Jeremy Katz <katzj@redhat.com> - 0.1.0-0.rc1.2
- BR xen-devel

* Thu Jul 27 2006 Jeremy Katz <katzj@redhat.com> - 0.1.0-0.rc1.1
- rebuild

* Thu Jul 20 2006 Karel Zak <kzak@redhat.com> 0.1.0-0.rc1
- upgrade to version 0.1.0rc1
- fix spec file

* Mon Apr  3 2006 Karel Zak <kzak@redhat.com> 0.0.8-1
- update to version 0.0.8
- fix postun section

* Thu Mar 16 2006 Karel Zak <kzak@redhat.com> 0.0.7-1
- update to version 0.0.7

* Fri Feb 10 2006 Karel Zak <kzak@redhat.com> 0.0.6-1
- compile against libvirt instead libvir

* Fri Jan 27 2006 Karel Zak <kzak@redhat.com> 0.0.5-1
- public release

* Thu Jan  5 2006 Karel Zak <kzak@redhat.com> 0.0.3-1
- new upstream version

* Tue Dec 13 2005 Karel Zak <kzak@redhat.com> 0.0.1-1
- created
