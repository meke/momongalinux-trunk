%global momorel 1
#global snapdate 2010-12-03

Summary: The Enlightenment window manager
Name: enlightenment
Version: 0.17.3
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: User Interface/Desktops
URL: http://www.enlightenment.org/
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
Source2: session.enlightenment
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl, pkgconfig, doxygen
BuildRequires: e_dbus-devel >= 1.7.7
BuildRequires: ecore-devel  >= 1.7.7
BuildRequires: edje-devel   >= 1:1.7.7
BuildRequires: eet-devel    >= 1.7.7
BuildRequires: efreet-devel >= 1.7.7
BuildRequires: eio-devel >= 1.7.7
BuildRequires: embryo-devel >= 1.7.7
BuildRequires: evas-devel   >= 1.7.7
BuildRequires: eeze-devel   >= 1.7.7
BuildRequires: imlib2_loaders >= 1.4.1.000-5m
BuildRequires: openldap-devel
BuildRequires: libssh2-devel
Requires: xorg-x11-xinit
Obsoletes: e_modules etox etox-devel e_utils eclair evfs
Obsoletes: edb edb-ed edb-devel
Obsoletes: engrave-devel engrave
Obsoletes: epeg epeg-devel
Obsoletes: rage esmart esmart-devel entrance entrance-devel
Obsoletes: emotion emotion-devel ewl ewl-devel
Obsoletes: etk etk-devel epsilon epsilon-devel epsilon-xine


%description
Enlightenment is a window manager for the X Window System that
is designed to be powerful, extensible, configurable and
pretty darned good looking! It is one of the more graphically
intense window managers.

Enlightenment goes beyond managing windows by providing a useful
and appealing graphical shell from which to work. It is open
in design and instead of dictating a policy, allows the user to
define their own policy, down to every last detail.

This package will install the Enlightenment window manager.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static LIBS="-ldbus-1 -lecore_x -leina"
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall transform='s,x,x,'
find %{buildroot} -name "*.la" -delete

### make /etc/X11/xinit/session.d/enlightenment scripts
%__mkdir_p %{buildroot}%{_sysconfdir}/X11/xinit/session.d
%__install -m 644 %{S:2} %{buildroot}%{_sysconfdir}/X11/xinit/session.d/enlightenment

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README
%{_sysconfdir}/xdg/menus/%{name}.menu
%{_bindir}/*
%{_datadir}/applications/enlightenment_filemanager.desktop
%dir %{_datadir}/enlightenment
%{_datadir}/enlightenment/*
%{_datadir}/locale/*/*/*
%{_datadir}/xsessions/enlightenment.desktop
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/*
#%%exclude %{_libdir}/pkgconfig
%config %{_sysconfdir}/X11/xinit/session.d/enlightenment
%config %{_sysconfdir}/enlightenment/sysactions.conf

%files devel
%defattr(-, root, root)
%{_includedir}/*
%{_libdir}/pkgconfig/*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.3-1m)
- update to 0.17.3

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.55225-4m)
- rebuild against eeze

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.55225-3m)
- rebuild for new EFL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.999.55225-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.55225-1m)
- update to new svn snap

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.999.49898-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16.999.49898-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.49539-2m)
- edje version fix

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.49539-1m)
- update to new svn snap

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16.999.063-2m)
- explicitly link libdbus-1, libecore_x and libeina

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.063-1m)
- update to new svn snap

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16.999.062-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16.999.062-4m)
- temporarily Obsoletes: emotion emotion-devel ewl ewl-devel

* Fri Aug 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.999.062-3m)
- comment out BuildRequires: ewl-devel >= 0.5.3.050-0.20090607.2m

* Fri Aug 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.999.062-2m)
- add Obsoletes: rage esmart esmart-devel entrance

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.999.062-1m)
- update to new svn snap

* Sat Aug  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.999.061-2m)
- add devel package (pc file only)

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.061-1m)
- update to new svn snap

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.060-1m)
- update

- add autoreconf (build fix)
* Thu May 21 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.16.999.050-4m)
- add autoreconf (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16.999.050-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16.999.050-2m)
- rebuild against rpm-4.6

* Sat Nov  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.999.050-1m)
- update

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.999.043-1m)
- update to 0.16.999.043

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.999.042-1m)
- merge from T4R
-
- changelog is below
- * Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- - (0.16.999.042-1m)
- - update to 0.16.999.042
- - add PreReq: openldap-devel
- - add PreReq: libssh2-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.999.037-0.20061231.3m)
- rebuild against gcc43

* Tue Jan  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.999.037-0.20061231.2m)
- add Obsoletes: etox-devel

* Mon Jan  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.037-0.20061231.1m)
- version 0.16.999.037-20061231

* Sun Apr 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.999.022-0.20051209.2m)
- Requires: xinitrc -> xorg-x11-xinit

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.022-0.20051209.1m)
- version 0.16.999.022-20051209
- change install dir /usr/X11R6/bin to /usr/bin

* Tue Jun 28 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.999.010-0.20050627.1m)
- version 0.16.999.010-20050627

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.17.0-0.20050215.2m)
- %%{_prefix}/lib -> %%{_prefix}/%%{_lib}
- remove lib64 patch.

* Thu Feb 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.17.0-0.20050215.1m)
- version 0.17.0-0.20050215

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.17.0-0.20041219.4m)
- rebuild against evas-1.0.0-0.20041218.4m

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.17.0-0.20041219.3m)
- enable x86_64.

* Tue Dec 28 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.17.0-0.20041219.2m)
- revised configure args

* Mon Dec 27 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.17.0-0.20041219.1m)
- DR17(CVS Version)

* Wed Aug 25 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.7.1-2m)
- revised theme_winter_diff.tar.bz2

* Tue Aug 24 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.7.1-1m)
- update 0.16.7.1
- pretty spec file

* Tue Aug 10 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.16.6-3m)
- use autoconf instead of autoconf-2.58

* Sun Dec 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.16.6-2m)
- use autoconf-2.58 instead of autoconf-2.57

* Fri Nov 14 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.6-1m)
- update 0.16.6
- change license GPL -> Modified BSD
- remove menudefault.patch menubuild.patch
- add Makefie patch
- pretty spec file
- change e_gen.tar.gz for 0.16.6

* Wed Mar  5 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.5-45m)
- use %%{_prefix} instead of %%{prefix}
- use %%{_bindir} instead of %%{prefix}/bin
- use %%{_datadir} instead of %{_prefix}/share
- use %%{_sysconfdir} instead of /etc
- delete define %%{prefix}
- fix URL

* Tue Jun  4 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.16.5-44k)
- remove kondara theme and menu

* Wed May 22 2002 Shigeyuki Yamashita <shige@kondara.org>
- (0.16.5-42k)
- add new etheme (from debian-E and aqua)

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.16.5-40k)
- rebuild against imlib 1.9.14-4k (implies libpng >= 1.2.2).

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.16.5-38k)
- rebuild against libpng-1.2.2

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.16.5-36k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.16.5-34k)
- fix URL

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.16.5-32k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (0.16.5-30k)
- rebuild against libpng 1.2.0.

* Mon Jul 30 2001 Motonobu Ichimura <famao@kondara.org>
- (0.16.5-28k)
- added galeon to Internet menu.

* Thu Jun  7 2001 Motonobu Ichimura <famao@kondara.org>
- (0.16.5-26k)
- menu update

* Thu Jun  7 2001 Motonobu Ichimura <famao@kondara.org>
- (0.16.5-24k)
- change font dir

* Wed Jun  6 2001 Motonobu Ichimura <famao@kondara.org>
- (0.16.5-22k)
- bug-fixed
- using truetype-ja for default

* Sun Jun  3 2001 Motonobu Ichimura <famao@kondara.org>
- (0.16.5-20k)
- bug-fixed and other stuff

* Sat Jun  2 2001 Motonobu Ichimura <famao@kondara.org>
- (0.16.5-18k)
  added enlightenment-ft.patch (it enables us to AA :-))

* Tue May 29 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (0.16.5-14k)
  Change default theme

* Thu May 10 2001 Motonobu Ichimura <famao@kondara.org>
- (0.16.5-12k)
- modify menu
- added bugfix.
- modify ja.po

* Wed May 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.16.5-11k)
- re-write utf8 patch for new xinitrc packege

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (0.16.5-9k)
- rebuild against audiofile-0.2.1.

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (0.16.5-7k)
- rebuild against audiofile-0.2.0.

* Thu Nov 16 2000 Motonobu Ichimura <famao@kondara.org>
- remove alpha-gcc296 patch.

* Tue Nov 07 2000 Motonobu Ichimura <famao@kondara.org>
- up to 0.16.5
- blush-up spec file
- now patically support UTF-8 locale

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.16.4-9k)
- fixed for FHS

* Sat Aug 26 2000 Motonobu Ichimura <famao@kondara.org>
- Change PATH (FHS)

* Mon Jul 31 2000 Toru Hoshina <t@kondara.org>
- add e-alpha-gcc296.patch.

* Wed Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Jan 09 2000 Motonobu Ichimura <famao@kondara.org>
- 0.16.4

* Fri Nov 26 1999 Shingo Akagaki <dora@kondara.org>
- add /etc/X11/xinit/session.d/enlightenment script

* Sun Oct 30 1999 Motonobu Ichimura <famao@kondara.org>
- up to 0.16.1
- add some patches

* Tue Oct 12 1999 Motonobu Ichimura <famao@kondara.org>
- first release
