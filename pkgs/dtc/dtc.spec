%global momorel 5

Name:           dtc
Version:        1.2.0
Release:        %{momorel}m%{?dist}
Summary:        Device Tree Compiler
Group:          Development/Tools
License:        GPLv2+
URL:            http://dtc.ozlabs.org/
Source:         http://www.jdl.com/software/dtc-v%{version}.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  flex, bison

%description
The Device Tree Compiler generates flattened Open Firmware style device trees
for use with PowerPC machines that lack an Open Firmware implementation

%prep
%setup -q -n %{name}-v%{version}

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PREFIX=%{_prefix}

#remove the devel stuff.
rm -rf $RPM_BUILD_ROOT/usr/include/*
rm -rf $RPM_BUILD_ROOT/usr/lib/*.a

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%defattr(-,root,root,-)
%doc GPL
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0 (sync fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-3m)
- rebuild against gcc43

* Sun Dec 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-2m)
- no install ftdump, conflicts with freetype2-demos-2.1.10-6m

* Sun Dec 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-1m)
- 1st made for PS3 Linux kernel
