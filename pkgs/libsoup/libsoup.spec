%global momorel 1

Summary: Soup, an HTTP library implementation
Name: libsoup

Version: 2.46.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: Development/Libraries
URL:  http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.46/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk-doc
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: gnutls-devel >= 2.4.1
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: libproxy-devel >= 0.4.4
BuildRequires: glib-networking
BuildRequires: libgnome-keyring-devel
Requires: glib
Requires: gnutls
Requires: libxml2

%description
Libsoup is an HTTP library implementation in C. It was originally part
of a SOAP (Simple Object Access Protocol) implementation called Soup, but
the SOAP and non-SOAP parts have now been split into separate packages.
 
libsoup uses the Glib main loop and is designed to work well with GTK
applications. This enables GNOME applications to access HTTP servers
on the network in a completely asynchronous fashion, very similar to
the Gtk+ programming model (a synchronous operation mode is also
supported for those who want it).

%package devel
Summary: Header files for the Soup library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: libxml2-devel
Requires: gnutls-devel

%description devel
Libsoup is an HTTP library implementation in C. This package allows 
you to develop applications that use the libsoup library.

%prep
%setup -q

%build
%configure --enable-gtk-doc \
    --enable-silent-rules \
    --enable-static=no
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang libsoup

%post -p /sbin/ldconfig 

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files -f libsoup.lang
%defattr(-, root, root)
%doc README COPYING NEWS AUTHORS
%{_libdir}/libsoup-2.4.so.*
%{_libdir}/libsoup-gnome-2.4.so.*
%exclude %{_libdir}/lib*.la
%{_libdir}/girepository-1.0/Soup-2.4.typelib
%{_libdir}/girepository-1.0/SoupGNOME-2.4.typelib

%files devel
%defattr(-, root, root)
%{_includedir}/libsoup-2.4
%{_includedir}/libsoup-gnome-2.4
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gtk-doc/html/libsoup-2.4
%{_datadir}/gir-1.0/Soup-2.4.gir
%{_datadir}/gir-1.0/SoupGNOME-2.4.gir

%changelog
* Thu May 22 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.46.0-1m)
- update to 2.46.0

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.40.1-1m)
- update to 2.40.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.40.0-1m)
- update to 2.40.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.39.92-1m)
- update to 2.39.92

* Tue Sep 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.39.91-1m)
- update to 2.39.91

* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.39.4.1-2m)
- add BuildRequires

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.39.4.1-1m)
- update to 2.39.4.1

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.39.4-1m)
- update to 2.39.4

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.39.3-1m)
- update to 2.39.3

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.36.1-1m)
- update to 2.36.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.36.0-1m)
- update to 2.36.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.35.92-1m)
- update to 2.35.92

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.35.90-1m)
- update to 2.35.90

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.34.2-1m)
- update to 2.34.2

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.34.1-1m)
- update to 2.34.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.34.0-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.34.0-1m)
- update to 2.34.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-3m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.30.2-2m)
- rebuild against libproxy-0.4.4

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Apr 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0.9

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.5-1m)
- update to 2.25.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2.1-1m)
- update to 2.24.2.1

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-2m)
- rebuild against gnutls-2.4.1

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.104-2m)
- %%NoSource -> NoSource

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.104-1m)
- update to 2.2.104

* Mon Oct 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.103-1m)
- update to 2.2.103

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.102-1m)
- update to 2.2.102

* Sat Oct  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.101-1m)
- update to 2.2.101

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.100-1m)
- update to 2.2.100

* Sun Jan 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.99-1m)
- update to 2.2.99
- [SECURITY] CVE-2006-5876
-- libsoup "soup_headers_parse()" Denial of Service

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.98-1m)
- update to 2.2.98

* Fri Nov 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.97-1m)
- update to 2.2.97

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.96-2m)
- rebuild against gnutls-1.4.4-1m

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.96-1m)
- update to 2.2.96

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.93-1m)
- update to 2.2.93

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (2.2.92-1m)
- update to 2.2.92

* Fri Apr  7 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (2.2.91-1m)
- update to 2.2.91

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.7-1m)
- update to 2.2.7
- GNOME 2.12.2 Desktop

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.6.1-1m)
- up to 2.2.6.1

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.2.1-1m)
- import from Fedore

* Tue Oct 12 2004 David Malcolm <dmalcolm@redhat.com> - 2.2.1-1
- update from 2.2.0 to 2.2.1

* Wed Oct  6 2004 David Malcolm <dmalcolm@redhat.com> - 2.2.0-3
- added requirement on libxml2 (#134700)

* Wed Sep 22 2004 David Malcolm <dmalcolm@redhat.com> - 2.2.0-2
- added requirement on gnutls, so that we build with SSL support
- fixed source download path

* Tue Aug 31 2004 David Malcolm <dmalcolm@redhat.com> - 2.2.0-1
- update from 2.1.13 to 2.2.0

* Mon Aug 16 2004 David Malcolm <dmalcolm@redhat.com> - 2.1.13-1
- 2.1.13

* Tue Jul 20 2004 David Malcolm <dmalcolm@redhat.com> - 2.1.12-1
- 2.1.12

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jun  7 2004 David Malcolm <dmalcolm@redhat.com> - 2.1.11-1
- 2.1.11

* Thu May 20 2004 David Malcolm <dmalcolm@redhat.com> - 2.1.10-2
- added missing md5 file

* Thu May 20 2004 David Malcolm <dmalcolmredhat.com> - 2.1.10-1
- 2.1.10

* Tue Apr 20 2004 David Malcolm <dmalcolm@redhat.com> - 2.1.9-1
- Update to 2.1.9; added gtk-doc to BuildRequires and the generated files to the devel package

* Wed Mar 10 2004 Jeremy Katz <katzj@redhat.com> - 2.1.8-1
- 2.1.8

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Feb 17 2004 Jeremy Katz <katzj@redhat.com> - 2.1.7-1
- 2.1.7

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jan 26 2004 Jeremy Katz <katzj@redhat.com> 2.1.5-1
- 2.1.5

* Wed Jan 14 2004 Jeremy Katz <katzj@redhat.com> 2.1.4-0
- update to 2.1.4

* Sat Jan  3 2004 Jeremy Katz <katzj@redhat.com> 2.1.3-0
- update to 2.1.3

* Tue Sep 23 2003 Jeremy Katz <katzj@redhat.com> 1.99.26-2
- rebuild

* Fri Sep 19 2003 Jeremy Katz <katzj@redhat.com> 1.99.26-1
- 1.99.26

* Tue Jul 15 2003 Jeremy Katz <katzj@redhat.com> 1.99.23-3
- rebuild to pickup ppc64

* Mon Jun  9 2003 Jeremy Katz <katzj@redhat.com> 1.99.23-2
- rebuild 
- no openssl on ppc64 yet, excludearch

* Mon Jun  9 2003 Jeremy Katz <katzj@redhat.com> 1.99.23-1
- 1.99.23

* Wed Jun 5 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jun  5 2003 Jeremy Katz <katzj@redhat.com> 1.99.22-2
- rebuild

* Sun May 25 2003 Jeremy Katz <katzj@redhat.com> 1.99.22-1
- 1.99.22

* Tue May  6 2003 Jeremy Katz <katzj@redhat.com> 1.99.20-1
- 1.99.20

* Sun May  4 2003 Jeremy Katz <katzj@redhat.com> 1.99.17-3
- include ssl proxy so that ssl urls work properly (#90165, #90166)

* Wed Apr 16 2003 Jeremy Katz <katzj@redhat.com> 1.99.17-2
- forward port patch to use a union initializer to fix build on x86_64

* Wed Apr 16 2003 Jeremy Katz <katzj@redhat.com> 1.99.17-1
- rename package to libsoup
- update to 1.99.17
- don't obsolete soup for now, it's parallel installable

* Sun Apr  6 2003 Jeremy Katz <katzj@redhat.com> 0.7.11-1
- update to 0.7.11

* Wed Apr  2 2003 Matt Wilson <msw@redhat.com> 0.7.10-5
- added soup-0.7.10-64bit.patch to fix 64 bit platforms (#86347)

* Sat Feb 01 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- only runtime libs in normal rpm

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Tue Jan 21 2003 Jeremy Katz <katzj@redhat.com> 
- update url (#82347)

* Tue Jan  7 2003 Nalin Dahyabhai <nalin@redhat.com> 0.7.10-2
- use pkgconfig's openssl configuration information, if it exists

* Fri Dec 13 2002 Jeremy Katz <katzj@redhat.com> 0.7.10-1
- update to 0.7.10

* Thu Dec 12 2002 Jeremy Katz <katzj@redhat.com> 0.7.9-4
- fix fpic patch
- soup-devel should require soup

* Thu Dec 12 2002 Jeremy Katz <katzj@redhat.com> 0.7.9-3
- better lib64 patch
- fix building of libwsdl-build to use libtool so that it gets built 
  with -fPIC as needed

* Tue Dec 10 2002 Jeremy Katz <katzj@redhat.com> 0.7.9-2
- change popt handling in configure slightly so that it will work on 
  multilib arches

* Tue Dec 10 2002 Jeremy Katz <katzj@redhat.com> 0.7.9-1
- update to 0.7.9, pulling the tarball out of Ximian packages

* Wed Oct 23 2002 Jeremy Katz <katzj@redhat.com> 0.7.4-3
- fix to not try to include non-existent doc files and remove all 
  unwanted files from the build
- include api docs 
- don't build the apache module

* Wed Sep 25 2002 Jeremy Katz <katzj@redhat.com> 0.7.4-2
- various specfile tweaks to include in Red Hat Linux
- include all the files

* Tue Jan 23 2001 Alex Graveley <alex@ximian.com>
- Inital RPM config.
