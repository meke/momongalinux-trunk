%global momorel 1

%global gid 334
%global groupname jackuser
%global pagroup pulse-rt
%global oldname %{name}-audio-connection-kit
## maybe crashed by firewire until libffado gets fixed
%global enable_firewire 0

Summary: The Jack Audio Connection Kit
Name: jack
Version: 1.9.9.5
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
URL: http://jackaudio.org/
Group: System Environment/Daemons
Source0: http://jackaudio.org/downloads/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: %{oldname}-script.pa
Patch0: %{name}-doxygen-output-dir-fix.patch
Patch1: wscript_dox.patch
Patch3: %{name}-1.9.7-fix-implicit-dso.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): shadow-utils
Requires(post): coreutils
Requires(post): grep
Requires: %{name}-libs = %{version}-%{release}
Requires: dbus
Requires: pam
BuildRequires: alsa-lib-devel >= 1.0.13-2m
BuildRequires: celt-devel
BuildRequires: coreutils
BuildRequires: dbus-devel
BuildRequires: doxygen
BuildRequires: expat-devel
BuildRequires: findutils
BuildRequires: fltk-devel
BuildRequires: glib2-devel
BuildRequires: libavc1394-devel
BuildRequires: libcap-devel
BuildRequires: opus-devel
#BuildRequires: libffado-devel
BuildRequires: libfreebob-devel
BuildRequires: libiec61883-devel >= 1.1.0
BuildRequires: libsamplerate-devel
BuildRequires: libsndfile-devel >= 1.0.15-2m
BuildRequires: libraw1394-devel >= 1.2.1
BuildRequires: ncurses-devel
BuildRequires: pkgconfig
BuildRequires: python
BuildRequires: readline-devel >= 5.0
Provides: %{oldname} = %{version}-%{release}

%description
JACK is a low-latency audio server, written primarily for the Linux
operating system. It can connect a number of different applications to
an audio device, as well as allowing them to share audio between
themselves. Its clients can run in their own processes (ie. as a
normal application), or can they can run within a JACK server (ie. a
"plugin").

%package libs
Summary: Shared runtime libraries of jack
Group: System Environment/Libraries
Provides: %{oldname}-libs = %{version}-%{release}

%description libs
This package contains shared runtime libraries of jack.

%package devel
Summary: Development package for jack
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Provides: %{oldname}-devel = %{version}-%{release}

%description devel
This package contains the files needed to compile programs that
communicates jack clients/servers.

%prep
%setup -q

%patch0 -p1 -b .build-fix0
%patch1 -p5 -b .build-fix1
#%%patch2 -p1 -b .build-fix2
%patch3 -p1 -b .fix-implicit-dso~

%build
export CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" CPPFLAGS="%{optflags}"

./waf -j1 configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--mandir=%{_mandir}/man1 \
	--alsa \
	--classic \
	--dbus \
	--doxygen \
%if %{enable_firewire}
	--firewire \
%endif
	--freebob

./waf -j1 build %{?_smp_mflags} -v

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
./waf -j1 install --destdir=%{buildroot}

# add default limits for jackuser group and pulse-rt group
mkdir -p %{buildroot}%{_sysconfdir}/security/limits.d

cat > %{buildroot}%{_sysconfdir}/security/limits.d/99-%{name}.conf << EOF
# Default limits for users of %{oldname}
@%{groupname} - rtprio 20
@%{groupname} - memlock 4194304

@%{pagroup} - rtprio 20
@%{pagroup} - nice -20
EOF

chmod 644 %{buildroot}%{_sysconfdir}/security/limits.d/99-%{name}.conf

# fix up permission
chmod 755 %{buildroot}%{_libdir}/%{name}/*.so
chmod 755 %{buildroot}%{_libdir}/lib%{name}.so
chmod 755 %{buildroot}%{_libdir}/lib%{name}server.so

# for compatibility with jack1
mv %{buildroot}%{_bindir}/%{name}_rec %{buildroot}%{_bindir}/%{name}rec

# install pulseaudio script for jack (as config-sample)
mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/config-sample/%{name}/

# clean up docs
rm -rf %{buildroot}%{_datadir}/%{oldname}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%pre
%{_sbindir}/groupadd -g %{gid} -r %{groupname} &>/dev/null || :

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc ChangeLog README* TODO
%config(noreplace) %{_sysconfdir}/security/limits.d/99-%{name}.conf
%{_bindir}/alsa_*
%{_bindir}/%{name}*
%{_libdir}/%{name}
%{_datadir}/config-sample/%{name}
%{_datadir}/dbus-1/services/org.%{name}audio.service
%{_mandir}/man1/alsa_*.1*
%{_mandir}/man1/%{name}*.1*

%files libs
%defattr(-,root,root)
%{_libdir}/lib%{name}.so.*
%{_libdir}/lib%{name}net.so.*
%{_libdir}/lib%{name}server.so.*

%files devel
%defattr(-,root,root)
%doc build/default/html
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}.so
%{_libdir}/lib%{name}net.so
%{_libdir}/lib%{name}server.so

%changelog
* Sat Apr 13 2013 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.9.9.5-1m)
- update to 1.9.9.5
- Add buildrequires opus-devel
- Add patch from https://gist.github.com/eliotb/4262013
- disable firewire support 
- (You must remove libffado-devel to completely disable)

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.8-2m)
- rebuild for glib 2.33.2

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.8-1m)
- version 1.9.8

* Thu Aug 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.7-3m)
- fix implicit DSO linking issue; pkgconfig/jack.pc needs "-lpthread -lrt"

* Tue Aug 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.7-2m)
- add "-j1" to avoid a possible freeze of ./waf

* Mon Aug  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.7-1m)
- version 1.9.7
- add a package libs
- import 3 compilation fix patches from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.118.0-8m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.118.0-7m)
- add BR libsamplerate-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.118.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.118.0-5m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.118.0-4m)
- Provides: jack-audio-connection-kit and jack-audio-connection-kit-devel

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.118.0-3m)
- rebuild against readline6

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.118.0-2m)
- use Requires

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.118.0-1m)
- version 0.118.0

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.116.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Feb  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.116.2-1m)
- version 0.116.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.116.1-2m)
- rebuild against rpm-4.6

* Sun Dec  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.116.1-1m)
- version 0.116.1

* Sat Nov 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.115.6-1m)
- version 0.115.6

* Sun Aug 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.109.2-4m)
- disable portaudio explicitly, portaudio-19 is supporting jack now
- import bz451531.patch from Fedora
 +* Sun Jul 20 2008 Andy Shevchenko <andy@smile.org.ua> 0.109.2-2
 +- apply patch to be work on ppc64 (#451531)
- import jack-audio-connection-kit.pa from Fedora and modify %%post
 +* Mon Jul 28 2008 Andy Shevchenko <andy@smile.org.ua> 0.109.2-3
 +- provide a pulseaudio start script from README.Fedora
 +- append values for pulse-rt group to the limits.conf
- clean up %%files

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.109.2-3m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.109.2-2m)
- remove BPR libtermcap-devel

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.109.2-1m)
- version 0.109.2

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.109.0-1m)
- version 0.109.0
- License: GPLv2 and LGPL

* Sun Jun 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.103.0-2m)
- groupadd jackuser

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.103.0-1m)
- version 0.103.0

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.102.20-2m)
- delete libtool library

* Fri Oct  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.102.20-1m)
- version 0.102.20

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.101.1-2m)
- rebuild against readline-5.0

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.101.1-1m)
- version 0.101.1

* Sun Jan  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.100.7-1m)
- version 0.100.7

* Thu Aug 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.100.0-1m)
- version 0.100.0
- BuildRequires: fltk-devel, glib-devel, ncurses-devel

* Tue Apr 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.0-3m)
- modify %%files section

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.99.0-2m)
- rebuild against libtermcap-2.0.8-38m.

* Mon Feb 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.0-1m)
- initial package
