%global momorel 2

Summary: IPv6 address format change and calculation utility
Name: ipv6calc
Version: 0.81.0
Release: %{momorel}m%{?dist}
Group: System Environment/Base
URL: http://www.deepspace6.net/projects/ipv6calc.html
License: GPL
Source0: ftp://ftp.bieringer.de/pub/linux/IPv6/ipv6calc/ipv6calc-%{version}.tar.gz
Requires: openssl
BuildRequires: openssl-devel >= 0.9.8a
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0

%description
ipv6calc is a small utility which formats and calculates IPv6 addresses in
different ways.

Install this package, if you want to extend the existing address detection
on IPv6 initscript setup or make life easier in adding reverse IPv6 zones to DNS
or using in DNS queries like
 nslookup -q=ANY `ipv6calc -r 3ffe:400:100:f101::1/48`
Many more format conversions are supported, see given URL for more.

%prep
%setup -q

%build
%configure --bindir=%{buildroot}%{_bindir} --mandir=%{buildroot}%{_mandir}
%make
%make test
	
%install
rm -rf %{buildroot}

# %{__mkdir_p} %{buildroot}/bin
# %{__mkdir_p} %{buildroot}%{_bindir}
# %{__mkdir_p} %{buildroot}%{_mandir}/man8
# make install root=%{buildroot}

%makeinstall

# Copy examples and helper files together
mkdir -p redhat/{ipv6logconv,ipv6logstats/examples-{data,gri},ipv6calcweb}
cp -f ipv6calcweb/{USAGE,ipv6calcweb.cgi} redhat/ipv6calcweb
cp -f examples/analog/{analog-,ipv6calc.tab,run_analog.sh}* redhat/ipv6logconv
cp -f ipv6logstats/{README,collect_,example_}* redhat/ipv6logstats
cp -f ipv6logstats/examples-data/*.txt redhat/ipv6logstats/examples-data
cp -f ipv6logstats/examples-gri/*.gri redhat/ipv6logstats/examples-gri

%check
#make test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog README CREDITS TODO COPYING LICENSE USAGE doc/ipv6calc.* redhat/*
%{_bindir}/ipv6calc
%{_bindir}/ipv6loganon
%{_bindir}/ipv6logconv
%{_bindir}/ipv6logstats
%{_mandir}/man8/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.81.0-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81.0-1m)
- update to 0.81.0

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80.0-1m)
- update to 0.80.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.73.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.73.0-2m)
- full rebuild for mo7 release

* Mon Jan  4 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.73.0-1m)
- update to 0.73.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.71.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.71.0-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71.0-1m)
- update to 0.71.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.50-2m)
- rebuild against gcc43

* Sun May  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.50-1m)
- version up

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.47-2m)
- rebuild against openssl-0.9.8a

* Sun Mar 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.47-1m)
- update to 0.47
- move ipv6calc to /usr/bin/
- revise URL:

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.42-2m)
  rebuild against openssl 0.9.7a

* Thu Jul 18 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.42-1m)
- import to momonga

* Sat Apr 20 2002 Peter Bieringer <pb@bieringer.de>
- Change URL

* Mon Apr 07 2002 Peter Bieringer <pb@bieringer.de>
- add more analog example files

* Mon Apr 05 2002 Peter Bieringer <pb@bieringer.de>
- remove BuildRequires extension, not needed for normal build

* Mon Mar 24 2002 Peter Bieringer <pb@bieringer.de>
- extend BuildRequires for perl /usr/bin/aggregate wget

* Mon Mar 18 2002 Peter Bieringer <pb@bieringer.de>
- add ipv6calcweb.cgi

* Mon Mar 16 2002 Peter Bieringer <pb@bieringer.de>
- add ipv6logconv, analog examples

* Mon Mar 11 2002 Peter Bieringer <pb@bieringer.de>
- Add perl to buildrequire and openssl to require

* Mon Jan 21 2002 Peter Bieringer <pb@bieringer.de>
- Add LICENSE + COPYING file

* Thu Dec 27 2001 Peter Bieringer <pb@bieringer.de>
- Add comment header
- Add call to configure on build

* Tue Dec 18 2001 Peter Bieringer <pb@bieringer.de>
- Replace hardwired version number with autoconf/configure variable

* Wed Apr 25 2001 Peter Bieringer <pb@bieringer.de>
- Fix permissions of doc files

* Thu Mar 15 2001 Peter Bieringer <pb@bieringer.de>
- Add doc directory also to %files to make sure the directory will be removed
   on update or deinstall
- change install permissions for entries in doc directory
- change "make install" to "make installonly" (make test should be only executed once" 

* Wed Mar 14 2001 Peter Bieringer <pb@bieringer.de>
- Add "make clean" and "make test" on %build

* Tue Mar 13 2001 Peter Bieringer <pb@bieringer.de>
- add CREDITS and TODO for install

* Sat Mar 10 2001 Peter Bieringer <pb@bieringer.de>
- enable "URL"

* Sun Mar 04 2001 Peter Bieringer <pb@bieringer.de>
- change install location to /bin

* Tue Feb 27 2001 Peter Bieringer <pb@bieringer.de>
- review for new release, now named "ipv6calc"
- review install section for RedHat 7.0.91

* Sun Feb 25 2001 Peter Bieringer <pb@bieringer.de>
- initial build
