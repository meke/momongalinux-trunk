%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0

Name: 		gtk-xfce-engine
Version: 	3.0.1
Release:	%{momorel}m%{?dist}
Summary: 	Port of xfce engine to GTK+-2.0

Group: 		User Interface/Desktops
License:	GPL
URL: 		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/xfce/%{name}/3.0/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: 	gtk2-devel
BuildRequires: 	glib2-devel
BuildRequires: 	libxml2-devel >= 2.7.2
BuildRequires: 	pkgconfig

%description
A port of Xfce engine to GTK+-%{version}.

%prep
%setup -q 

%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_libdir}/*/*/engines/*
%{_libdir}/*/*/theming-engines/*
%{_datadir}/themes/*


%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.0-1m)
- update to version 3.0.0

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-3m)
- change Source0 URI (xfce4 latest version is 4.10)

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.0-2m)
- rebuild for glib 2.33.2

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.0-4m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.0-1m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.93-1m)
- Xfce 4.5.93
- add %%define __libtoolize :

* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.92-1m)
- Xfce 4.5.92

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.91-1m)
- Xfce 4.5.91

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.1-1m)
- Xfce 4.4.1

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-2m)
- delete libtool library

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-1m)
- Xfce 4.4.0

* Sun Sep 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.8-3m)
- rebuild against gtk+-2.10.3

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.8-2m)
- fix files

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.8-1m)
- Xfce 4.2.3.2

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.7-1m)
- Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.6-1m)
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.5-1m)
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.4-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.3-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.2-1m)
- Xfce 4.2-RC1

* Sun Oct 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.1-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.10-1m)
- version up

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.9-5m)
- rebuild against xfce4 4.0.5

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.1.9-4m)
- rebuild against for gtk+-2.4.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.1.9-3m)
- rebuild against for libxml2-2.6.8
- rebuild against for glib-2.4.0

* Fri Mar 12 2004 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.1.9-2m)
- change Source0

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.9-1m)

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.8-1m)
- version up

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.7-1m)
- version up

* Fri Sep 26 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (2.1.6-1m)
- version up

* Sat Sep 13 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.1.5-1m)
- XFce4 RC4 release

