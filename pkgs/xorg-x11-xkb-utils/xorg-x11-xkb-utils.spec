%global momorel 13

Summary: X.Org X11 xkb utilities
Name: xorg-x11-xkb-utils
Version: 1.0.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xorg-x11-xkbutils
Requires: xorg-x11-xkbcomp
Requires: xorg-x11-xkbevd
Requires: xorg-x11-xkbprint
Requires: xorg-x11-setxkbmap


%description
X.Org X11 xkb utilities

%prep

%build

%install
rm -rf --preserve-root %{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-9m)
- rebuild against rpm-4.6

* Sat May 31 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-8m)
- fix requires

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-7m)
- rebuild against gcc43

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-6m)
- meta package for
-- xorg-x11-xkbutils xorg-x11-xkbcomp xorg-x11-xkbevd xorg-x11-xkbprint
-- xorg-x11-setxkbmap

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-5m)
- update setxkbmap-1.0.4

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-4m)
- add Provides

* Thu Nov  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-3m)
- update 
-- xkbcomp-1.0.3
-- setxkbmap-1.0.3

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- update setxkbmap-1.0.2.tar.bz2

* Wed Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update Xorg-7.1RC1
-- xkbevd-1.0.2.tar.bz2
-- setxkbmap-1.0.2.tar.bz2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- Change Source URL
- To trunk

* Thu Mar  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1.3m)
- comment out Obsolete

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated all apps to version 1.0.1 from X11R7.0

* Sat Dec 17 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated all apps to version 1.0.0 from X11R7 RC4.
- Changed manpage dir from man1x to man1 to match upstream default.

* Sun Nov 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2
- Change from "Conflicts" to "Obsoletes: XFree86, xorg-x11" for upgrades.
- Rebuild against new libXaw 0.99.2-2, which has fixed DT_SONAME. (#173027)

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated to xkbutils-0.99.1, setxkbmap-0.99.2, xkbcomp-0.99.1, xkbevd-0.99.2,
  xkbprint-0.99.1 from X11R7 RC2.

* Thu Nov 10 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Updated xkbutils to version 0.99.0 from X11R7 RC1.  The upstream tarball
  changed, but the version stayed the same.  <sigh>
- Updated setxkbmap, xkbcomp, xkbevd, xkbprint.
- Change manpage location to 'man1x' in file manifest.
- Iterate over packages with for loop instead of serialized code duplication.

* Wed Oct  5 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Use Fedora-Extras style BuildRoot tag.
- Update BuildRequires to use new library package names.
- Tidy up spec file a bit.

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
