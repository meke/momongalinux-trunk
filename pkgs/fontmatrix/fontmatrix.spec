%global momorel 13

%define svnrev 1073

Name: fontmatrix
Summary: A fonts manager
Version: 0.6.99
Release: 0.%{svnrev}.%{momorel}m%{?dist}
License: GPLv2+
Group: User Interface/X
##### svn checkout HOWTO #####
#####  $ svn export -r %{svnrev} http://svn.gna.org/svn/undertype/trunk/tools/typotek fontmatrix #####
#####  $ mkdir ../fontmatrix-build                                              #####  
#####  $ cd ../fontmatrix-build                                                 ##### 
#####  $ cmake ../fontmatrix                                                    #####  
#####  $ make package_source                                                    ##### 
URL: http://www.fontmatrix.net/
## Official stable releases can be downloaded from http://www.fontmatrix.net/archives/
Source0: %{name}-%{version}-Source.tar.gz
Patch0: bug_564904_fix-missing-DSO-icuuc.patch 
Patch1: bug_561044_532882_remove-broken-fontmatrix-shaper.patch
Patch2: bug_604793_ms-symbol.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake >= 2.8.12.2
BuildRequires: qt-devel >= 4.8.1
BuildRequires: freetype-devel >= 2.5.3-2m
BuildRequires: desktop-file-utils python-devel >= 2.7
BuildRequires: openssl-devel podofo-devel >= 0.9.1 libicu-devel >= 4.6
##Following is needed to ensure that enduser can edit font with fontforge application
Requires:      fontforge

%description
A powerful and well designed fonts manager

%prep
%setup -q -n  %{name}-%{version}-Source
%patch0 -p0
%patch1 -p0
%patch2 -p0

%build
export CFLAGS="$RPM_OPT_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS"
PATH="%{_qt4_bindir}:$PATH" %{cmake} . \
         -DWANT_HARFBUZZ:bool=true -DWANT_ICU:bool=true \
         -DWANT_PYTHONQT:bool=true -DWANT_PODOFO:bool=true \
         -DPYTHON_INCLUDE_DIR:PATH=%{_includedir}/python2.7 \
         -DPYTHON_LIBRARY:FILEPATH=%{_libdir}/libpython2.7.so

make VERBOSE=1 %{?_smp_mflags}

%check
ctest --build-and-test

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

#rpmlint complains Zero-length file
rm -f $RPM_BUILD_ROOT%{_datadir}/fontmatrix/help/en/what_fonts_are.html

desktop-file-install --vendor= --delete-original  \
    --dir=$RPM_BUILD_ROOT%{_datadir}/applications/ \
    $RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING TODO INSTALL
%{_bindir}/fontmatrix
%{_datadir}/fontmatrix
%{_mandir}/man1/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/48x48/apps/fontmatrix.png


%changelog
* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.99-0.1073-13m)
- add BuildRequires: cmake >= 2.8.12.2
- specify freetype version and release

* Thu Mar 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.99-0.1073-12m)
- rebuild against icu-52

* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.99-0.1073-11m)
- specify python2.7 headers and libraries

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.99-0.1073.10m)
- rebuild against podofo-0.9.1

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.99-0.1073.9m)
- rebuild against icu-4.6

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.99-0.1073.8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.99-0.1073.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.99-0.1073.6m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.99-0.1073.5m)
- specify PATH for Qt4

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.99-0.1073.4m)
- fix build failure

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.99-0.1073.3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.99-0.1073.2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.99-0.1073.1m)
- sync with Fedora 13 (0.6.99-6.r1073)

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-r900.5m)
- rebuild against qt-4.6.3-1m

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-r900.4m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-r900.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-r900.2m)
- remove fedora from --vendor

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.0-r900.1m)
- import from Fedora

* Fri Mar 20 2009 Parag <pnemade@redhat.com> - 0.5.0-1.r900
- update to svn revision 900

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jan 16 2009 Tomas Mraz <tmraz@redhat.com> - 0.4.2-3
- rebuild with new openssl

* Tue Jun 10 2008 Parag <pnemade@redhat.com> - 0.4.2-2
- Resolves: rh#449406:FTBFS fontmatrix-0.4.2-1.fc9

* Tue Apr 08 2008 Parag <pnemade@redhat.com> - 0.4.2-1.fc9
- Update to 0.4.2

* Mon Apr 07 2008 Parag <pnemade@redhat.com> - 0.4.0-2.fc9
- Add support for Shaper functionality.

* Fri Apr 04 2008 Parag <pnemade@redhat.com> - 0.4.0-1.fc9
- Update to 0.4.0

* Mon Feb 11 2008 Parag <pnemade@redhat.com> - 0.3.1-2
- Rebuild for gcc 4.3

* Wed Jan 23 2008 Parag <pnemade@redhat.com> - 0.3.1-1
- Update to 0.3.1

* Mon Jan 14 2008 Parag <pnemade@redhat.com> - 0.3.0-4.r289
- update to svn revision 289(Stable release of 0.3.0 version)

* Thu Dec 27 2007 Parag <pnemade@redhat.com> - 0.3.0-3.r270
- update to svn revision 270
- Added Requires:fontforge

* Thu Dec 27 2007 Parag <pnemade@redhat.com> - 0.3.0-3.r270
- update to svn revision 270

* Fri Dec 21 2007 Parag <pnemade@redhat.com> - 0.3.0-3.r263
- Fixed license tag
- update to new svn checkout
- drop unnecessary BR: qt4-x11

* Tue Dec 18 2007 Parag <pnemade@redhat.com> - 0.3.0-2.r253
- Added BR:cmake

* Mon Dec 17 2007 Parag <pnemade@redhat.com> - 0.3.0-1.r253
- New upstream svn checkout

* Fri Dec 14 2007 Parag <pnemade@redhat.com> - 0.2-14.3.fc8
- Some spec cleanup

* Fri Dec 14 2007 Parag <pnemade@redhat.com> - 0.2-14.2.fc8
- Initial spec for Fedora.

* Mon Nov 26 2007 <mrdocs@scribus.info> - 0.2-14.1
- Initial upstream spec.

