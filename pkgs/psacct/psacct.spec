%global momorel 1

# Our /usr/bin/last is in the SysVInit packae
%define with_last     0

%define FHS_compliant 1

%if %{FHS_compliant}
%define accounting_logdir       /var/account
%else
%define accounting_logdir       /var/log
%endif

Summary: Utilities for monitoring process activities
Name: psacct
Version: 6.6.1
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/System
URL: ftp://ftp.gnu.org/gnu/acct/
Source0: ftp://ftp.gnu.org/gnu/acct/acct-%{version}.tar.gz
NoSource: 0
Source1: psacct.service
Source2: psacct-logrotate.in
Source3: accton-create

Patch1: psacct-6.3.2-lastcomm_man.patch
Patch2: acct-6.3.2-sa_manpage.patch
# The upstream man page is more correct than the usage
# .. the C sources need to be fixed, not the man page
#Patch3: psacct-6.3.2-man-pages.patch
Patch4: psacct-6.6.1-unnumberedsubsubsec.patch
Patch5: psacct-6.6.1-RH-man-page-scan.patch
# Partial replacement for Patch3: psacct-6.3.2-man-pages.patch
Patch6: psacct-6.6.1-man-dump-acct.patch
# Preventing SEGVs when an incomplete record appears
Patch7: psacct-6.6.1-SEGV-when-record-incomplete.patch

Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
Requires: coreutils
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

BuildRequires: autoconf
BuildRequires: systemd-units

# This conflict is to avoid psacct being forced on by old initscripts now that
# we have a proper initscript in place. initscripts 6.55 and later are fixed.
Conflicts: initscripts < 6.55

%description
The psacct package contains several utilities for monitoring process
activities, including ac, lastcomm, accton and sa. The ac command
displays statistics about how long users have been logged on. The
lastcomm command displays information about previous executed
commands. The accton command turns process accounting on or off. The
sa command summarizes information about previously executed
commands.

%prep
%setup -q -n acct-%{version}
%patch1 -p1 -b .man
%patch2 -p1 -b .pct
#%%patch3 -p1 -b .new
%patch4 -p1 -b .subsubsec
%patch5 -p1 -b .rh-man-scan
%patch6 -p1 -b .man-dump-acct
%patch7 -p1

# fixing 'gets' undeclared
sed -i 's|.*(gets,.*||g' lib/stdio.in.h

# workaround for broken autotools stuff
sed -i 's|@ACCT_FILE_LOC@|/var/account/pacct|g'      files.h.in
sed -i 's|@SAVACCT_FILE_LOC@|/var/account/savacct|g' files.h.in
sed -i 's|@USRACCT_FILE_LOC@|/var/account/usracct|g' files.h.in


%build
%configure --enable-linux-multiformat

make

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

mkdir -p %{buildroot}{/sbin,%{_bindir},%{_mandir},%{_sbindir}}
make install prefix=%{buildroot}%{_prefix} \
        bindir=%{buildroot}%{_bindir} sbindir=%{buildroot}%{_sbindir} \
        infodir=%{buildroot}%{_datadir}/info mandir=%{buildroot}%{_mandir}
cp dump-acct.8 %{buildroot}%{_mandir}/man8/

# remove unwanted file
rm -f %{buildroot}%{_infodir}/dir

mkdir -p %{buildroot}/var/account
touch %{buildroot}/var/account/pacct

# create logrotate config file
mkdir -p %{buildroot}/etc/logrotate.d
sed -e 's|%%{_bindir}|%{_bindir}|g' -e 's|%%{_sbindir}|%{_sbindir}|g' %{SOURCE2} > %{buildroot}/etc/logrotate.d/psacct

# install systemd unit file
mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE1} %{buildroot}%{_unitdir}

# install accton-create script
install -d -m 0755 %{buildroot}%{_libexecdir}/psacct
install -m 755 %{SOURCE3} %{buildroot}%{_libexecdir}/psacct/

%if ! %{with_last}
rm -f %{buildroot}%{_bindir}/last %{buildroot}%{_mandir}/man1/last.1*
%endif

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%post
%systemd_post psacct.service

/sbin/install-info %{_infodir}/accounting.info %{_infodir}/dir || :
touch /var/account/pacct

%preun
%systemd_preun psacct.service

if [ $1 -eq 0 ]; then
    # Package removal, not upgrade
    /sbin/install-info --delete %{_infodir}/accounting.info %{_infodir}/dir || :
fi


%postun
%systemd_postun_with_restart psacct.service


%triggerun -- psacct < 6.5.5-1m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply psacct
# to migrate them to systemd targets 
%{_bindir}/systemd-sysv-convert --save psacct >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del psacct >/dev/null 2>&1 || :
/bin/systemctl try-restart psacct.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc README COPYING
%if %{FHS_compliant}
%dir /var/account
%endif
%{_unitdir}/psacct.service
%attr(0600,root,root)   %ghost %config %{accounting_logdir}/pacct
%attr(0644,root,root)   %config(noreplace) /etc/logrotate.d/*
%{_sbindir}/accton
%{_sbindir}/sa
%{_sbindir}/dump-utmp
%{_sbindir}/dump-acct
%{_bindir}/ac
%if %{with_last}
%{_bindir}/last
%endif
%{_bindir}/lastcomm
%{_mandir}/man1/ac.1*
%if %{with_last}
%{_mandir}/man1/last.1*
%endif
%{_mandir}/man1/lastcomm.1*
%{_mandir}/man8/sa.8*
%{_mandir}/man8/accton.8*
%{_mandir}/man8/dump-acct.8*
%{_mandir}/man8/dump-utmp.8*
%{_infodir}/accounting.info*
%{_libexecdir}/psacct/accton-create

%changelog
* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.6.1-1m)
- update 6.6.1

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.5.5-1m)
- update 6.5.5
-- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.5.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.4-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5.4-1m)
- sync with Rawhide (6.5.4-5)

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.2-38m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.2-37m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Mar 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.2-36m)
- remove %%{_infodir}/dir

* Fri Mar 27 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.2-35m)
- exclude dir.* file

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.2-34m)
- rebuild against rpm-4.6

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.2-33m)
- fix install-info
-- remove too old rotting hacks for install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.2-32m)
- rebuild against gcc43

* Thu Feb  10 2005 Toru Hoshina <t@momonga-linux.org>
- (6.3.2-31m)
- version down. sync with FC3.

* Thu Dec 30 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3-5-1m)
- update 6.3.5
- add SuSE sources and patches
- delete gziped part

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (6.3.2-19m)
- revised spec for rpm 4.2.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (6.3.2-18k)

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (6.3.2-16k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (6.3.2-14k)
- updated Source0 URL.

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (6.3.2-12k)
- autoconf 1.5

* Sat Oct  6 2001 Toru Hoshina <t@kondara.org>
- (6.3.2-10k)

* Thu Sep 06 2001 Mike A. Harris <mharris@redhat.com> 6.3.2-9
- Fixed bug (#53307) psacct is enabled by default, and the log files
  are huge, and will fill the disk up very quickly.  logrotate will
  now compress them daily.

* Sat Sep 01 2001 Florian La Roche <Florian.LaRoche@redhat.de> 6.3.2-8
- do not fail for ENOSYS to silently support kernels without
  process accounting

* Sun Aug 26 2001 Mike A. Harris <mharris@redhat.com> 6.3.2-7
- Change spec tag Copyright -> License
- change logrotate to rotate daily, and keep 1 month (31 days) of data

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Mon Feb 02 2001 Helge Deller <hdeller@redhat.de>
- added logrotate file for /var/log/pacct (#24900)

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org>
- Kodaraized.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- FHS fixes

* Sat May  6 2000 Bill Nottingham <notting@redhat.com>
- fix for new patch

* Thu Feb 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 6.3.2

* Mon Apr 05 1999 Preston Brown <pbrown@redhat.com>
- wrap post script with reference count.

* Tue Mar 23 1999 Preston Brown <pbrown@redhat.com>
- install-info sucks.  Still.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Thu Mar 18 1999 Bill Nottingham <notting@redhat.com>
- #define HAVE_LINUX_ACCT_H too, so it works. :)

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- accton needs to be accessible to /etc/rc.d/init.d/halt

* Fri May 08 1998 Erik Troan <ewt@redhat.com>
- install-info sucks

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- updated from 6.2 to 6.3

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc
