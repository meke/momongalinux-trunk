# Generated from bones-3.8.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname bones

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Mr Bones is a handy tool that creates new Ruby projects from a code skeleton
Name: rubygem-%{gemname}
Version: 3.8.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubygems.org/gems/bones
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(rake) >= 0.8.7
Requires: rubygem(little-plugger) => 1.1.3
Requires: rubygem(little-plugger) < 1.2
Requires: rubygem(loquacious) => 1.9.1
Requires: rubygem(loquacious) < 1.10
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Mr Bones is a handy tool that creates new Ruby projects from a code
skeleton. The skeleton contains some starter code and a collection of rake
tasks to ease the management and deployment of your source code. Several Mr
Bones plugins are available for creating git repositories, creating GitHub
projects, running various test suites and source code analysis tools.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/bones
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/bin/bones
%doc %{geminstdir}/default/version.txt
%doc %{geminstdir}/spec/data/markdown.txt
%doc %{geminstdir}/spec/data/rdoc.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.0-1m)
- update 3.8.0

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.3-1m)
- update 3.7.3

* Tue Sep 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.1-1m)
- update 3.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.1-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.5.1-1m)
- update 3.5.1

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.5.0-1m)
- update 3.5.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.7-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.7-1m)
- Initial package for Momonga Linux
