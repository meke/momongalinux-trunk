%global momorel 8

Summary: Rebootless Linux kernel security updates
Name: ksplice
Version: 0.9.9
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Kernel
URL: http://www.ksplice.com/
Source0: http://www.ksplice.com/dist/ksplice-%{version}-src.tar.gz
Patch0: %{name}-%{version}-pod2man.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl
BuildRequires: binutils-devel
Requires: perl
Requires: rsync
Requires: patch
Requires: binutils

%description
Ksplice allows system administrators to apply security patches to the Linux kernel without having
to reboot. Ksplice takes as input a source code change in unified diff format and the kernel source
code to be patched, and it applies the patch to the corresponding running kernel. The running
kernel does not need to have been prepared in advance in any way.

%prep
%setup -q
%patch0 -p1

%build
# binutils requires "-lz -ldl"
./configure --libexecdir=%{_libdir} LDFLAGS="-lz -ldl"
make %{?_smp_mflags} CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} prefix=%{_prefix}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING README
%{_bindir}/*
%{_sbindir}/*
%{_libdir}/%{name}*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/*
%{_mandir}/man8/*

%changelog
* Wed Apr 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9-8m)
- fix pod2man error

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9-7m)
- add source

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-6m)
- delete "BuildRequires: binutils-static"

* Mon Jul 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-5m)
- fix build failure with new binutils

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9-1m)
- update to 0.9.9

* Mon May  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-3m)
- add "BuildRequires: binutils-static"

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7
- remove pdf manual

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-2m)
- rebuild against rpm-4.6

* Sat Oct 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Fri Aug 15 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-1m)
- initial made for Momonga
