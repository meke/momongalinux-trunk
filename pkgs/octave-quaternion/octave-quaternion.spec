%global momorel	2
%global octpkg quaternion

Name:           octave-%{octpkg}
Version:        1.0.0
Release:        %{momorel}m%{?dist}
Summary:        Quaternion package for Octave
Group:          Applications/Engineering
License:        GPLv3+
URL:            http://octave.sourceforge.net/quaternion/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
BuildArch:      noarch

BuildRequires:  octave  

Requires:       octave
Requires(post): octave
Requires(postun): octave


Obsoletes:      octave-forge <= 20090607


%description
Package for the manipulation of Quaternions used for frame transformation

%prep
%setup -q -n %{octpkg}-%{version}

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install

%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo
%doc %{octpkgdir}/packinfo/COPYING
%doc %{octpkgdir}/doc/quaternion.ps


%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0-2m)
- rebuild against octave-3.6.1-1m

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-1m)
- initial import from fedora
