%global momorel 8

%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname arrayfields
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Name: 		rubygem-%{gemname}
Summary: 	Arrayfields RubyGem
Version: 	4.7.4
Release: 	%{momorel}m%{?dist}
Group:		Development/Languages
License: 	GPLv2+ or Ruby
URL: 		http://codeforpeople.com/lib/ruby/%{gemname}/
Source0: 	http://codeforpeople.com/lib/ruby/%{gemname}/%{gemname}-%{version}/%{gemname}-%{version}.gem
#NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: 	rubygems
BuildRequires: 	rubygems
BuildArch: 	noarch
Provides: 	rubygem(%{gemname}) = %{version}

%description
Arrayfields RubyGem

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}/%{gemdir} \
            --force %{SOURCE0}

# Remove backup files
find %{buildroot}/%{geminstdir} -type f -name "*~" -delete

# Delete zero-length files
find %{buildroot}/%{geminstdir} -type f -size 0c -exec rm -rvf {} \;

# Fix anything executable that does not have a shebang
for file in `find %{buildroot}/%{geminstdir} -type f -perm /a+x`; do
    [ -z "`head -n 1 $file | grep \"^#!/\"`" ] && chmod -v 644 $file
done

# Find files with a shebang that do not have executable permissions
for file in `find %{buildroot}/%{geminstdir} -type f ! -perm /a+x -name "*.rb"`; do
    [ ! -z "`head -n 1 $file | grep \"^#!/\"`" ] && chmod -v 755 $file
done

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc %{gemdir}/doc/%{gemname}-%{version}/
%doc %{geminstdir}/README
%doc %{geminstdir}/sample
%doc %{geminstdir}/test
%dir %{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/gems/%{gemname}-%{version}/*.rb
%{gemdir}/gems/%{gemname}-%{version}/lib
%{gemdir}/gems/%{gemname}-%{version}/%{gemname}.gemspec
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (4.7.4-8m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.4-5m)
- full rebuild for mo7 release

* Thu Aug 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.4-4m)
- remove .yardoc

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.7.4-3m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.4-1m)
- import from Fedora to Momonga

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.7.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Jun 06 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 4.7.4-1
- New upstream version

* Mon Mar 16 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 4.7.2-1
- New upstream version

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.5.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jul 29 2008 Jeroen van Meeuwen <kanarip@kanarip.com> - 4.5.0-3
- Rebuild with README marked as docfor review (#457026)

* Tue Jul 29 2008 Jeroen van Meeuwen <kanarip@kanarip.com> - 4.5.0-2
- Rebuild for review

* Sun Jul 13 2008 root <root@oss1-repo.usersys.redhat.com> - 4.5.0-1
- Initial package
