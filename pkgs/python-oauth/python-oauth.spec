%global momorel 5

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-oauth
Version:        1.0.1
Release:        %{momorel}m%{?dist}
Summary:        Library for OAuth version 1.0a

Group:          Development/Languages
License:        MIT
URL:            http://code.google.com/p/oauth/
Source0:        http://pypi.python.org/packages/source/o/oauth/oauth-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7 python-setuptools-devel

%description
Library for OAuth version 1.0a.


%prep
%setup -q -n oauth-%{version}


%build
python setup.py build


%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --skip-build --root $RPM_BUILD_ROOT

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE.txt
%{python_sitelib}/*


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- full rebuild for mo7 release

* Tue Mar 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora 13

* Tue Oct 06 2009 Sebastian Dziallas <sebastian@when.com> - 1.0.1-1
- initial packaging
