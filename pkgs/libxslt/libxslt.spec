%global momorel 1

Summary: Library providing the Gnome XSLT engine
Name: libxslt
Version: 1.1.28
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Development/Libraries
Source0: ftp://xmlsoft.org/libxslt/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://xmlsoft.org/XSLT/
Requires: libxml2 >= 2.9.0
BuildRequires: libxml2-devel >= 2.9.0
BuildRequires: python python-devel >= 2.7
BuildRequires: libxml2-python
BuildRequires: libgcrypt-devel >= 1.2.2-2m
Prefix: %{_prefix}
Docdir: %{_docdir}
Patch0: multilib.patch

%description
This C library allows to transform XML files into other XML files
(or HTML, text, ...) using the standard XSLT stylesheet transformation
mechanism. To use it you need to have a version of libxml2 >= 2.6.27
installed. The xsltproc command is a command line interface to the XSLT engine

%package devel
Summary: Libraries, includes, etc. to embed the Gnome XSLT engine
Group: Development/Libraries
Requires: libxslt = %{version}-%{release}
Requires: libxml2-devel >= 2.6.27
Requires: libgcrypt-devel
Requires: pkgconfig

%description devel
This C library allows to transform XML files into other XML files
(or HTML, text, ...) using the standard XSLT stylesheet transformation
mechanism. To use it you need to have a version of libxml2 >= 2.6.27
installed.

%package python
Summary: Python bindings for the libxslt library
Group: Development/Libraries
Requires: libxslt = %{version}-%{release}
Requires: libxml2 >= 2.6.27
Requires: libxml2-python >= 2.6.27

%description python
The libxslt-python package contains a module that permits applications
written in the Python programming language to use the interface
supplied by the libxslt library to apply XSLT transformations.

This library allows to parse sytlesheets, uses the libxml2-python
to load and save XML and HTML files. Direct access to XPath and
the XSLT transformation context are possible to extend the XSLT language
with XPath functions written in Python.

%prep
%setup -q
%patch0 -p1

%build
%configure
%make

%check
make check

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

rm -fr %{buildroot}%{_libdir}/*.la \
       %{buildroot}%{_libdir}/python*/site-packages/libxsltmod*a

# multiarch crazyness on timestamp differences
touch -m --reference=%{buildroot}/%{prefix}/include/libxslt/xslt.h %{buildroot}/%{prefix}/bin/xslt-config
 
%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)

%doc AUTHORS ChangeLog NEWS README Copyright TODO FEATURES
%doc doc/*.html doc/html doc/tutorial doc/tutorial2 doc/*.gif
%doc doc/EXSLT
%{_mandir}/man1/xsltproc.1.*
%{_libdir}/lib*.so.*
%{_libdir}/libxslt-plugins
%{_bindir}/xsltproc

%files devel
%defattr(-, root, root)

%doc AUTHORS ChangeLog NEWS README Copyright TODO FEATURES
%doc doc/libxslt-api.xml
%doc doc/libxslt-refs.xml
%doc doc/EXSLT/libexslt-api.xml
%doc doc/EXSLT/libexslt-refs.xml
%doc doc/*.html doc/html doc/*.gif doc/*.png
%doc doc/images
%doc doc/tutorial
%doc doc/tutorial2
%doc doc/EXSLT
%{_mandir}/man3/libxslt.3*
%{_mandir}/man3/libexslt.3*
%{_libdir}/lib*.so
%{_libdir}/*a
%{_libdir}/*.sh
%{_datadir}/aclocal/libxslt.m4
%{_includedir}/*
%{_bindir}/xslt-config
%{_libdir}/pkgconfig/libxslt.pc
%{_libdir}/pkgconfig/libexslt.pc

%files python
%defattr(-, root, root)

%doc AUTHORS ChangeLog NEWS README Copyright FEATURES
%{_libdir}/python*/site-packages/libxslt.py*
%{_libdir}/python*/site-packages/libxsltmod*
%doc python/TODO
%doc python/libxsltclass.txt
%doc python/tests/*.py
%doc python/tests/*.xml
%doc python/tests/*.xsl

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.28-1m)
- [SECURITY] CVE-2012-6139
- update to 1.1.28

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.27-1m)
- update to 1.1.27
- add %%check

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.26-9m)
- [SECURITY] CVE-2011-3790

* Sat Apr 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.26-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.26-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.26-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.26-5m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.26-4m)
- separate package python

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.26-3m)
- fix build failure; add *.pyo and *.pyc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.26-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.26-1m)
- update 1.1.26
-- fix meinproc4 hangs

* Fri Sep 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.25-1m)
- update 1.1.25

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.24-5m)
- support libtool-2.2.x

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.24-4m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.24-3m)
- rebuild agaisst python-2.6.1-1m

* Sat Aug  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.24-2m)
- [SECURITY] CVE-2008-2935

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.24-1m)
- [SECURITY] CVE-2008-1767
- update to 1.1.24

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.23-1m)
- update to 1.1.23

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.22-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.22-2m)
- %%NoSource -> NoSource

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.22-1m)
- update to 1.1.22

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.20-2m)
- delete pyc pyo

* Mon Feb  5 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildPrereq: libgpg-error-devel

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.20-1m)
- update to 1.1.20

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.19-1m)
- update to 1.1.19

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.17-1m)
- update to 1.1.17

* Sun Aug 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16-2m)
- libxslt(not libxslt-devel) Provides: libxslt-python

* Tue May  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.16-1m)
- update to 1.1.16

* Sun Jan  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.15-3m)
- BuildPrereq: libgcrypt-devel >= 1.2.2-2m

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.15-2m)
- comment out unnessesaly autoreconf and make check

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.15-1m)
- version up.
- GNOME 2.12.1 Desktop

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.12-3m)
- build against python-2.4.2

* Wed Jan 12 2005 TAKAHASHI Tamotsu <tamo>
- (1.1.12-2m)
- License: MIT/X

* Wed Jan 12 2005 TAKAHASHI Tamotsu <tamo>
- (1.1.12-1m)
- update: minor bugfixes
- modify doc files location
- Provides: libxslt-python

* Wed Dec 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.11-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Sat Oct 16 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.11-1m)
- version 1.1.11

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.6-2m)
- rebuild against python2.3

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (1.1.6-1m)
- version 1.1.6

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.5-2m)
- adjustment BuildPreReq

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.5-1m)
- version 1.1.5
- GNOME 2.6 Desktop

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.2-2m)
- revised spec for enabling rpm 4.2.

* Sun Dec 28 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.2-1m)
- version 1.1.2.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (1.0.33-2m)
- pretty spec file.

* Tue Oct 14 2003 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.0.33-2m)
- remove libxslt-1.0.2-nazopublic.patch patch entry.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.33-1m)
- version 1.0.33

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.32-1m)
- version 1.0.32

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.31-1m)
- version 1.0.31

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.30-1m)
- version 1.0.30

* Wed Apr 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.29-1m)
- version 1.0.29

* Tue Mar 25 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.28-1m)
- version 1.0.28

* Fri Feb 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.27-1m)
- version 1.0.27

* Tue Feb 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.26-1m)
  update to 1.0.26

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.25-1m)
- version 1.0.25

* Wed Jan 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.24-1m)
- version 1.0.24

* Fri Dec 13 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.23-2m)
- update BuildPrereq: libxml2-devel >= 2.4.29-1m

* Wed Nov 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.23-1m)
- version 1.0.23

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.22-1m)
- version 1.0.22

* Fri Oct 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.21-1m)
- version 1.0.21

* Sat Aug 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.20-1m)
- version 1.0.20

* Sat Jul 27 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.0.19-2m)
- add %{_includedir}/libexslt at %{files} devel

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.19-1m)
- version 1.0.19

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.18-2k)
- version 1.0.18

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.17-2k)
- version 1.0.17

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.16-2k)
- version 1.0.16

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.15-2k)
- version 1.0.15

* Wed Mar 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.0.14-4k)
- fix %files secsion(COPYING.LIB to COPYING)

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.14-2k)
- version 1.0.14

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.13-2k)
- version 1.0.13

* Mon Mar  4 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0.12-6k)
- fix a broken symlink of COPYING

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.12-4k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.12-2k)
- version 1.0.12

* Tue Jan 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.10-2k)
- up to 1.0.10

* Fri Dec 14 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.9-2k)
- up to 1.0.9

* Fri Nov  2 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.6-2k)
- up to 1.0.6

* Sat Oct 20 2001 Motonobu Ichimura <famao@kondara.org>
- (1.0.5-2k)
- up to 1.0.5

* Thu Sep 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.0.4-2k)
- version 1.0.4

* Sat Aug 25 2001 Shingo Akagaki <dora@kondara.org>
- version 1.0.3

* Mon Aug 20 2001 Toru Hoshina <toru@df-usa.com>
- (1.0.2-4k)
- LIBXSLT_PUBLIC...

* Fri Aug 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.0.2

* Mon Jul 16 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.0.0

* Mon Jul  9 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.14.0

* Wed Jul  4 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.13.0

* Mon Jun 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.12.0

* Wed May 02 2001 Motonobu Ichimura <famao@digitalfactory.co.jp>
- (0.8.0-4k)
- now libxslt requires libxml2-2.3.7

* Tue Apr 24 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.8.0

* Fri Apr 13 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.7.0-4k)
- up libxml2 require version to 2.3.6

* Wed Apr 11 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.7.0

* Mon Mar 26 2001 Shingo Akagaki <dora@kondara.org>
- version 0.6.0
- K2K

* Mon Jan 22 2001 Daniel.Veillard <Daniel.Veillard@imag.fr>

- created based on libxml2 spec file
