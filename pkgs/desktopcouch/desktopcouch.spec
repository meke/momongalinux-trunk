%global momorel 8

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:           desktopcouch
Version:        0.6.4
Release:        %{momorel}m%{?dist}

Summary:        A CouchDB instance on every desktop
Group:          Development/Languages
License:        LGPLv3
URL:            https://launchpad.net/desktopcouch
Source0:        http://launchpad.net/desktopcouch/trunk/%{version}/+download/desktopcouch-%{version}.tar.gz
NoSource:       0
Source1:	product-logo.png

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools
BuildRequires:  python-distutils-extra
BuildRequires:  intltool
BuildRequires:  desktop-file-utils >= 0.16

Requires:       python-twisted-core
Requires:       couchdb
Requires:       avahi

Requires:       python-desktopcouch = %{version}-%{release}
Requires:       python-desktopcouch-records = %{version}-%{release}

BuildArch:      noarch

%description
A CouchDB on every desktop, and the code to help it happen.

%package tools
Summary:        Tools for desktopcouch
Requires:       pygtk2
Requires:       python-desktopcouch = %{version}-%{release}
Requires:       desktop-file-utils

%description tools
This package contains tools for desktopcouch.

%package -n python-desktopcouch
Summary:        Python bindings for desktopcouch

Requires:       desktopcouch = %{version}-%{release}
Requires:       dbus-python
Requires:       python-couchdb
Requires:       pyxdg
Requires:       gnome-python2-gnomekeyring
Requires:       python-twisted-core

%description -n python-desktopcouch
These are the python bindings for desktopcouch.

%package -n python-desktopcouch-records
Summary:        Python bindings for desktopcouch records
Requires:       python-desktopcouch = %{version}-%{release}
Requires:       python-oauth

%description -n python-desktopcouch-records
These are the python bindings for desktopcouch records.

%prep
%setup -q

%build
%{__sed} -i -e 's@/usr/lib/desktopcouch/@%{_bindir}/@' org.desktopcouch.CouchDB.service
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

# remove installed docs, we will doc them later
rm -rf $RPM_BUILD_ROOT%{_datadir}/doc

# Really? You put them there? In /usr/lib (note, not %{_libdir})? Fail.
mv %{buildroot}/usr/lib/desktopcouch/desktopcouch-* %{buildroot}%{_bindir}\

# add icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
cp %{SOURCE1} %{buildroot}%{_datadir}/pixmaps
grep -v Icon %{buildroot}/%{_datadir}/applications/desktopcouch-pair.desktop > \
	desktopcouch-pair.desktop.tmp
echo "Icon=product-logo.png" | \
	cat desktopcouch-pair.desktop.tmp - > \
	%{buildroot}/%{_datadir}/applications/desktopcouch-pair.desktop

desktop-file-validate %{buildroot}/%{_datadir}/applications/desktopcouch-pair.desktop

%clean
rm -rf $RPM_BUILD_ROOT

%post tools
update-desktop-database -q

%postun tools
update-desktop-database -q

%files
%defattr(-,root,root)
%doc README COPYING COPYING.LESSER
%dir %{_sysconfdir}/xdg/desktop-couch
%config(noreplace) %{_sysconfdir}/xdg/desktop-couch/compulsory-auth.ini
%{_bindir}/desktopcouch-service
%{_bindir}/desktopcouch-stop
%{_bindir}/desktopcouch-get-port
%{_datadir}/dbus-1/services/org.desktopcouch.CouchDB.service
%{_datadir}/desktopcouch

%files tools
%defattr(-,root,root)
%{_bindir}/desktopcouch-pair
%{_datadir}/applications/desktopcouch-pair.desktop
%{_datadir}/pixmaps/product-logo.png
%{_mandir}/man1/desktopcouch-pair.1*

%files -n python-desktopcouch
%defattr(-,root,root)
%{python_sitelib}/desktopcouch-*egg-info
%dir %{python_sitelib}/desktopcouch
%{python_sitelib}/desktopcouch/*.py*
%{python_sitelib}/desktopcouch/pair
%{python_sitelib}/desktopcouch/tests

%files -n python-desktopcouch-records
%defattr(-,root,root)
%doc desktopcouch/records/doc/*.txt desktopcouch/contacts/*.txt
%{python_sitelib}/desktopcouch/bookmarks
%{python_sitelib}/desktopcouch/contacts
%{python_sitelib}/desktopcouch/notes
%{python_sitelib}/desktopcouch/records
%{python_sitelib}/desktopcouch/replication_services

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.4-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-5m)
- full rebuild for mo7 release

* Mon Aug 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-3m)
- add menu icon

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-3m)
- build fix with desktop-file-utils-0.16

* Sun Jun 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-2m)
- rebuild against desktop-file-utils-0.16

* Sat Apr 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Tue Mar 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-1m)
- import from Fedora 13 and momonganize

* Fri Mar 12 2010 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.6.3-2
- Fix issues from package review.

* Fri Mar 12 2010 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.6.3-1
- Update to 0.6.3

* Tue Mar 09 2010 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.6.2-1
- Update to 0.6.2

* Thu Feb 11 2010 Paul W. Frields <stickster@gmail.com> - 0.6.1-1
- 0.6.1-1
- Fix requirement of avahi-tools

* Mon Nov 23 2009 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.5-2
- Fix requirement of desktop-file-utils

* Sun Nov 22 2009 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.5-1
- initial package
