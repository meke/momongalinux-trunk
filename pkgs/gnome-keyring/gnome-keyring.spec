%global momorel 1
%define glib2_version 2.25.0
%define gtk3_version 2.90.0
%define gcr_version 3.3.3
%define dbus_version 1.0
%define gcrypt_version 1.2.2
%define libtasn1_version 0.3.4
%define libgnome_keyring_version 3.5.6

Summary: Framework for managing passwords and other secrets
Name: gnome-keyring
Version: 3.6.2
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: System Environment/Libraries
#VCS: git:git://git.gnome.org/gnome-keyring
Source: http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
URL: http://www.gnome.org

BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gtk3-devel >= %{gtk3_version}
BuildRequires: gcr-devel >= %{gcr_version}
BuildRequires: dbus-devel >= %{dbus_version}
BuildRequires: libgcrypt-devel >= %{gcrypt_version}
BuildRequires: libtasn1-devel >= %{libtasn1_version}
BuildRequires: pam-devel
BuildRequires: autoconf, automake, libtool
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: libtasn1-tools
BuildRequires: libgnome-keyring-devel >= %{libgnome_keyring_version}
BuildRequires: gtk-doc
BuildRequires: libcap-ng-devel
BuildRequires: libselinux-devel
BuildRequires: p11-kit-devel
BuildRequires: gcr-devel
# for smooth transition since the core was split
Requires: libgnome-keyring >= %{libgnome_keyring_version}

# we no longer have a devel subpackage
Obsoletes: %{name}-devel < 3.3.0
Provides:  %{name}-devel = 3.3.0

%description
The gnome-keyring session daemon manages passwords and other types of
secrets for the user, storing them encrypted with a main password.
Applications can use the gnome-keyring library to integrate with the keyring.

%package pam
Summary: Pam module for unlocking keyrings
License: LGPLv2+
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
# for /lib/security
Requires: pam

%description pam
The gnome-keyring-pam package contains a pam module that can
automatically unlock the "login" keyring when the user logs in.


%prep
%setup -q -n gnome-keyring-%{version}

%build
%configure \
           --with-pam-dir=/%{_lib}/security \
           --enable-pam

# avoid unneeded direct dependencies
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' libtool

make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

rm $RPM_BUILD_ROOT/%{_lib}/security/*.la
rm $RPM_BUILD_ROOT%{_libdir}/pkcs11/*.la
rm $RPM_BUILD_ROOT%{_libdir}/gnome-keyring/devel/*.la

%find_lang gnome-keyring

%post
/sbin/ldconfig
update-mime-database %{_datadir}/mime &> /dev/null || :
touch %{_datadir}/icons/hicolor >&/dev/null || :

%postun
/sbin/ldconfig
update-mime-database %{_datadir}/mime &> /dev/null || :
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :


%files -f gnome-keyring.lang
%doc AUTHORS NEWS README COPYING COPYING.LIB
# LGPL
%dir %{_libdir}/gnome-keyring
%dir %{_libdir}/gnome-keyring/devel
%{_libdir}/gnome-keyring/devel/*.so
%dir %{_libdir}/pkcs11
%{_libdir}/pkcs11/*.so
# GPL
#%attr(0755,root,root) %caps(cap_ipc_lock=ep) %{_bindir}/gnome-keyring-daemon
%attr(0755,root,root) %{_bindir}/gnome-keyring-daemon
%{_bindir}/gnome-keyring
%{_bindir}/gnome-keyring-3
%{_datadir}/dbus-1/services/*.service
%{_sysconfdir}/xdg/autostart/*
%{_datadir}/GConf/gsettings/*.convert
%{_datadir}/glib-2.0/schemas/*.gschema.xml
%{_sysconfdir}/pkcs11/modules/gnome-keyring.module

%files pam
/%{_lib}/security/*.so

%changelog
* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1
- reimport from fedora

* Wed Feb 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-3m)
- fix WARNING: socket error

* Sun Jan  1 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-2m)
- enable-p11-tests

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Sat Oct  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0-2m)
- remove BR hal-devel

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- delete conflict dir

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- add glib-compile-schemas script

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.3-4m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.3-3m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.3-2m)
- add Requires: GConf2

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Tue Jun 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.30.1-2m)
- revert to version 2.30.1
- can not login via gdm with gnome-keyring-2.30.2

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Sat Jan 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.29.5-2m)
- add Requires: libgnome-keyring-devel to package devel (build fix of gir-repository)
- add Buildrequires: libgnome-keyring-devel

* Sat Jan 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.5-1m)
- update to 2.29.5

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sun May 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.26.1-2m)
- add REMOVE.PLEASE (thanks Dai-san...)

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-2m)
- fix autostart

* Sun Mar 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Thu Feb 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-2m)
- delete autostart file

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against rpm-4.6

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Aug 27 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.3-2m)
- sync Fedora
- change pam module directory

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- rebuild against gcc43

* Wed Mar 12 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.22.0-2m)
- add BuildRequires: libtasn1-devel

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Sun Nov 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Thu Oct 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20.1-2m)
- build fix on x86_64

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Sat Feb 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.92-1m)
- update to 0.7.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.91-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.91-1m)
- update to 0.7.91 (unstable)

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.4.9-3m)
- rebuild against exdpat-2.0.0-1m

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.9-2m)
- delete libtool library

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.9-1m)
- update to 0.4.9

* Sat Feb 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.7-1m)
- update to 0.4.7

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.6-1m)
- update to 0.4.6
- GNOME 2.12.2 Desktop

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.5-2m)
- delete autoreconf and make check

* Wed Nov 16 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.5-1m)
- version up
- GNOME 2.12.1 Desktop

* Sat Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.4.0-1m)
- version 0.4.0
- GNOME 2.8 Desktop

* Mon Dec 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-3m)
- add auto commands before %%build for fix missing lib*.so problem

* Sun Apr 18 2004 Toru Hoshina <t@momonga-linux.org>
- (0.2.0-2m)
- revised spec for rpm 4.2.

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.0-1m)
- version 0.2.0
- GNOME 2.6 Desktop
- initial import into Momonga HEAD

* Wed Mar 10 2004 Alexander Larsson <alexl@redhat.com> 0.1.90-1
- update to 0.1.90

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Feb 24 2004 Alexander Larsson <alexl@redhat.com> 0.1.4-1
- update to 0.1.4

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Jan 30 2004 Alexander Larsson <alexl@redhat.com> 0.1.3-1
- update to 0.1.3

* Mon Jan 26 2004 Bill Nottingham <notting@redhat.com>
- tweak summary

* Mon Jan 26 2004 Alexander Larsson <alexl@redhat.com> 0.1.2-2
- devel package only needs glib2-devel, not gtk2-devel

* Fri Jan 23 2004 Alexander Larsson <alexl@redhat.com> 0.1.2-1
- First version

