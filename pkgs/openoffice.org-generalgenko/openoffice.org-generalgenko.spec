%global momorel 8

%global oooinstdir %{_libdir}/openoffice.org3.0
%global jaoootemplatedir %{oooinstdir}/share/template/ja

Summary: General "genkou youshi" template for OpenOffice.org Writer
Name:    openoffice.org-generalgenko
Version: 1.0.0
Release: %{momorel}m%{?dist}
Group:   Applications/Productivity
License: Modified BSD
URL:     http://hermione.s41.xrea.com/pukiwiki/pukiwiki.php?OOoBasic%2FMacros%2Fmanuscriptpaper%2Fdev
Source0: general-genko_100.ott
Requires: openoffice.org-writer >= 2.0.4-6m
Requires: openoffice.org-langpack-ja_JP >= 2.0.4-6m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
General "genkou youshi" template for OpenOffice.org writer by Mr. Yutaka Kachi.


%prep
%setup -q -T -c

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{jaoootemplatedir}/writer
install -m 644 %{SOURCE0} %{buildroot}%{jaoootemplatedir}/writer/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{jaoootemplatedir}/writer/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-4m)
- rebuild against OOo-3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2m)
- rebuild against gcc43

* Wed Nov 08 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.0.0-1m)
- build for Momonga
