libxl: suppress device assignment to HVM guest when there is no IOMMU

This in effect copies similar logic from xend: While there's no way to
check whether a device is assigned to a particular guest,
XEN_DOMCTL_test_assign_device at least allows checking whether an
IOMMU is there and whether a device has been assign to _some_
guest.

For the time being, this should be enough to cover for the missing
error checking/recovery in other parts of libxl's device assignment
paths.

There remains a (functionality-, but not security-related) race in
that the iommu should be set up earlier, but this is too risky a
change for this stage of the 4.3 release.

This is a security issue, XSA-61.

Signed-off-by: Jan Beulich <jbeulich@suse.com>
Tested-by: George Dunlap <george.dunlap@eu.citrix.com>
Acked-by: George Dunlap <george.dunlap@eu.citrix.com>
Acked-by: Ian Jackson <ian.jackson@eu.citrix.com>

--- a/tools/libxl/libxl_pci.c
+++ b/tools/libxl/libxl_pci.c
@@ -784,6 +784,18 @@ int libxl__device_pci_add(libxl_ctx *ctx
     int num_assigned, i, rc;
     int stubdomid = 0;
 
+    if (libxl__domain_is_hvm(ctx, domid)) {
+        rc = xc_test_assign_device(ctx->xch, domid, pcidev_value(pcidev));
+        if (rc) {
+            LIBXL__LOG(ctx, LIBXL__LOG_ERROR,
+                       "PCI device %04x:%02x:%02x.%u %s?",
+                       pcidev->domain, pcidev->bus, pcidev->dev, pcidev->func,
+                       errno == ENOSYS ? "cannot be assigned - no IOMMU"
+                                       : "already assigned to a different guest");
+            goto out;
+        }
+    }
+
     if (!libxl_pcidev_assignable(ctx, pcidev)) {
         LIBXL__LOG(ctx, LIBXL__LOG_ERROR, "PCI device %x:%x:%x.%x is not assignable",
                    pcidev->domain, pcidev->bus, pcidev->dev, pcidev->func);
