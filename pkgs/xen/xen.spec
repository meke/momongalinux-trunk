%global momorel 6
%global _default_patch_fuzz 2

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

# Hypervisor ABI
%define hv_abi  4.1

Summary: Xen is a virtual machine monitor
Name:    xen
Version: 4.1.5
Release: %{momorel}m%{?dist}
Group:   Development/Libraries
License: GPLv2+ and LGPLv2+ and BSD
URL:     http://xen.org/
Source0: http://bits.xensource.com/oss-xen/release/%{version}/xen-%{version}.tar.gz
Source1: %{name}.modules
Source2: %{name}.logrotate
Source3: dom0-kernel.repo
# used by stubdoms
Source10: lwip-1.3.0.tar.gz
Source11: newlib-1.16.0.tar.gz
Source12: zlib-1.2.3.tar.gz
Source13: pciutils-2.2.9.tar.bz2
Source14: grub-0.97.tar.gz
Source15: ipxe-git-v1.0.0.tar.gz
# sysconfig bits
Source30: sysconfig.xenstored
Source31: sysconfig.xenconsoled
Source32: sysconfig.blktapctrl
# systemd bits
Source40: proc-xen.mount
Source41: var-lib-xenstored.mount
Source42: xenstored.service
Source43: blktapctrl.service
Source44: xend.service
Source45: xenconsoled.service
Source46: xen-watchdog.service
Source47: xendomains.service
Source48: libexec.xendomains

Patch1: xen-initscript.patch
Patch4: xen-dumpdir.patch
Patch5: xen-net-disable-iptables-on-bridge.patch

Patch6: xen-4.1.5-glibc217.patch

Patch23: grub-ext4-support.patch
Patch28: pygrubfix.patch
Patch34: xend.catchbt.patch
Patch35: xend-pci-loop.patch
Patch38: xen-backend.rules.patch
Patch39: xend.selinux.fixes.patch
Patch45: xen-no-pyxml.patch
Patch47: xen-4.1.5-support-encoding-pod.patch
Patch48: xen-4.1.5-user.tex-caption.patch

Patch50: upstream-23936:cdb34816a40a-rework
Patch51: upstream-23937:5173834e8476
Patch52: upstream-23938:fa04fbd56521-rework
Patch53: upstream-23939:51288f69523f-rework
Patch54: upstream-23940:187d59e32a58

Patch100: xen-configure-xend.patch

# upstream security patches
Patch319: xsa45-4.1-01-vcpu-destroy-pagetables-preemptible.patch
Patch320: xsa45-4.1-02-new-guest-cr3-preemptible.patch
Patch321: xsa45-4.1-03-new-user-base-preemptible.patch
Patch322: xsa45-4.1-04-vcpu-reset-preemptible.patch
Patch323: xsa45-4.1-05-set-info-guest-preemptible.patch
Patch324: xsa45-4.1-06-unpin-preemptible.patch
Patch325: xsa45-4.1-07-mm-error-paths-preemptible.patch
Patch335: xsa53-4.1.patch
Patch336: xsa54.patch
## patches for XSA-55
patch337: 0001-libelf-abolish-libelf-relocate.c.patch
patch338: 0002-libxc-introduce-xc_dom_seg_to_ptr_pages.patch
Patch339: 0003-libxc-Fix-range-checking-in-xc_dom_pfn_to_ptr-etc.patch
Patch340: 0004-libelf-abolish-elf_sval-and-elf_access_signed.patch
Patch341: 0005-libelf-xc_dom_load_elf_symtab-Do-not-use-syms-uninit.patch
Patch342: 0006-libelf-introduce-macros-for-memory-access-and-pointe.patch
Patch343: 0007-tools-xcutils-readnotes-adjust-print_l1_mfn_valid_no.patch
Patch344: 0008-libelf-check-nul-terminated-strings-properly.patch
Patch345: 0009-libelf-check-all-pointer-accesses.patch
Patch346: 0010-libelf-Check-pointer-references-in-elf_is_elfbinary.patch
Patch347: 0011-libelf-Make-all-callers-call-elf_check_broken.patch
Patch348: 0012-libelf-use-C99-bool-for-booleans.patch
Patch349: 0013-libelf-use-only-unsigned-integers.patch
Patch350: 0014-libxc-Introduce-xc_bitops.h.patch
Patch351: 0015-libelf-check-loops-for-running-away.patch
Patch352: 0016-libelf-abolish-obsolete-macros.patch
Patch353: 0017-libxc-Add-range-checking-to-xc_dom_binloader.patch
Patch354: 0018-libxc-check-failure-of-xc_dom_-_to_ptr-xc_map_foreig.patch
Patch355: 0019-libxc-check-return-values-from-malloc.patch
Patch356: 0020-libxc-range-checks-in-xc_dom_p2m_host-and-_guest.patch
Patch357: 0021-libxc-check-blob-size-before-proceeding-in-xc_dom_ch.patch
Patch358: xsa56.patch
Patch359: xsa57-4.1.patch
Patch360: xsa58-4.1.patch
Patch361: xsa61-4.1.patch
Patch362: xsa62-4.1.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: transfig libidn-devel zlib-devel texi2html SDL-devel curl-devel
BuildRequires: libX11-devel python-devel ghostscript tetex-latex
BuildRequires: ncurses-devel gtk2-devel libaio-devel
# for the docs
BuildRequires: perl texinfo
# so that the makefile knows to install udev rules
BuildRequires: udev
%ifnarch ia64
# so that x86_64 builds pick up glibc32 correctly
#BuildRequires: /usr/include/gnu/stubs-32.h
# for the VMX "bios"
BuildRequires: dev86
%endif
BuildRequires: gettext
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: openssl-devel
# For ioemu PCI passthrough
BuildRequires: pciutils-devel
# Several tools now use uuid
BuildRequires: libuuid-devel
# iasl needed to build hvmloader
BuildRequires: iasl
# modern compressed kernels
BuildRequires: bzip2-devel xz-devel
# libfsimage
BuildRequires: e2fsprogs-devel
BuildRequires: ocaml, ocaml-findlib
# libbrlapi
BuildRequires: brlapi-devel >= 0.6.1
Requires: bridge-utils
Requires: PyXML
Requires: udev >= 059
Requires: xen-runtime = %{version}-%{release}
# Not strictly a dependency, but kpartx is by far the most useful tool right
# now for accessing domU data from within a dom0 so bring it in when the user
# installs xen.
Requires: kpartx
Requires: chkconfig
ExclusiveArch: %{ix86} x86_64 ia64
#ExclusiveArch: %{ix86} x86_64 ia64 noarch
Obsoletes: xenner

%description
This package contains the XenD daemon and xm command line
tools, needed to manage virtual machines running under the
Xen hypervisor

%package libs
Summary: Libraries for Xen tools
Group: Development/Libraries
Requires(pre): /sbin/ldconfig
Requires(post): /sbin/ldconfig
Requires: xen-licenses

%description libs
This package contains the libraries needed to run applications
which manage Xen virtual machines.


%package runtime
Summary: Core Xen runtime environment
Group: Development/Libraries
Requires: xen-libs = %{version}-%{release}
Requires: qemu-img qemu-common
# Ensure we at least have a suitable kernel installed, though we can't
# force user to actually boot it.
Requires: xen-hypervisor-abi = %{hv_abi}

%description runtime
This package contains the runtime programs and daemons which
form the core Xen userspace environment.


%package hypervisor
Summary: Libraries for Xen tools
Group: Development/Libraries
Provides: xen-hypervisor-abi = %{hv_abi}
Requires: xen-licenses

%description hypervisor
This package contains the Xen hypervisor


%package doc
Summary: Xen documentation
Group: Documentation
#BuildArch: noarch
Requires: xen-licenses

%description doc
This package contains the Xen documentation.


%package devel
Summary: Development libraries for Xen tools
Group: Development/Libraries
Requires: xen-libs = %{version}-%{release}

%description devel
This package contains what's needed to develop applications
which manage Xen virtual machines.


%package licenses
Summary: License files from Xen source
Group: Documentation

%description licenses
This package contains the license files from the source used
to build the xen packages.


%package ocaml
Summary: Ocaml libraries for Xen tools
Group: Development/Libraries
Requires: ocaml-runtime, xen-libs = %{version}-%{release}

%description ocaml
This package contains libraries for ocaml tools to manage Xen
virtual machines.


%package ocaml-devel
Summary: Ocaml development libraries for Xen tools
Group: Development/Libraries
Requires: xen-ocaml = %{version}-%{release}

%description ocaml-devel
This package contains libraries for developing ocaml tools to
manage Xen virtual machines.


%prep
%setup -q
%patch1 -p1
%patch4 -p1
%patch5 -p1

%patch28 -p1
%patch34 -p1
%patch35 -p1
%patch38 -p1
%patch39 -p1
%patch45 -p1
%patch47 -p1
%patch48 -p1

%patch50 -p1
%patch51 -p1
%patch52 -p1
%patch53 -p1
%patch54 -p1

%patch100 -p1

# Security patches
%patch319 -p1
%patch320 -p1
%patch321 -p1
%patch322 -p1
%patch323 -p1
%patch324 -p1
%patch325 -p1
%patch335 -p1
%patch336 -p1
%patch337 -p1
%patch338 -p1
%patch339 -p1
%patch340 -p1
%patch341 -p1
%patch342 -p1
%patch343 -p1
%patch344 -p1
%patch345 -p1
%patch346 -p1
%patch347 -p1
%patch348 -p1
%patch349 -p1
%patch350 -p1
%patch351 -p1
%patch352 -p1
%patch353 -p1
%patch354 -p1
%patch355 -p1
%patch356 -p1
%patch357 -p1
%patch358 -p1
%patch359 -p1
%patch360 -p1
%patch361 -p1
%patch362 -p1

%patch6 -p1

# override INITD_DIR
cat <<__EOF__ > .config
INITD_DIR=/etc/init.d
__EOF__

# stubdom sources
cp -v %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14} stubdom
cp -v %{PATCH23} stubdom/grub.patches/99grub-ext4-support.patch
cp -v %{SOURCE15} tools/firmware/etherboot/ipxe.tar.gz


%build
export XEN_VENDORVERSION="-%{release}"
export CFLAGS="$RPM_OPT_FLAGS"
make %{?_smp_mflags} prefix=/usr dist-xen
make %{?_smp_mflags} prefix=/usr dist-tools
make                 prefix=/usr dist-docs
unset CFLAGS
make dist-stubdom


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/ocaml/stublibs
make DESTDIR=%{buildroot} prefix=/usr install-xen
make DESTDIR=%{buildroot} prefix=/usr install-tools
make DESTDIR=%{buildroot} prefix=/usr install-docs
make DESTDIR=%{buildroot} prefix=/usr install-stubdom

############ debug packaging: list files ############

find %{buildroot} -print | xargs ls -ld | sed -e 's|.*%{buildroot}||' > f1.list

############ kill unwanted stuff ############

# stubdom: newlib
rm -rf %{buildroot}/usr/*-xen-elf

# hypervisor symlinks
rm -rf %{buildroot}/boot/xen-4.0.gz
rm -rf %{buildroot}/boot/xen-4.gz

# silly doc dir fun
rm -fr %{buildroot}%{_datadir}/doc/xen
rm -rf %{buildroot}%{_datadir}/doc/qemu

# Pointless helper
rm -f %{buildroot}%{_sbindir}/xen-python-path

# qemu stuff (unused or available from upstream)
rm -rf %{buildroot}/usr/share/xen/man
rm -rf %{buildroot}/usr/bin/qemu-*-xen
ln -s qemu-img %{buildroot}/%{_bindir}/qemu-img-xen
ln -s qemu-img %{buildroot}/%{_bindir}/qemu-nbd-xen
for file in bios.bin openbios-sparc32 openbios-sparc64 ppc_rom.bin \
         pxe-e1000.bin pxe-ne2k_pci.bin pxe-pcnet.bin pxe-rtl8139.bin \
         vgabios.bin vgabios-cirrus.bin video.x openbios-ppc bamboo.dtb
do
	rm -f %{buildroot}/%{_datadir}/xen/qemu/$file
done

# README's not intended for end users
rm -f %{buildroot}/%{_sysconfdir}/xen/README*

# standard gnu info files
rm -rf %{buildroot}/usr/info

# adhere to Static Library Packaging Guidelines
rm -rf %{buildroot}/%{_libdir}/*.a

############ fixup files in /etc ############

# udev
#rm -rf %{buildroot}/etc/udev/rules.d/xen*.rules
#mv %{buildroot}/etc/udev/xen*.rules %{buildroot}/etc/udev/rules.d

# modules
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig/modules
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/sysconfig/modules/%{name}.modules

# logrotate
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d/
install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}

# remove sysv related files
rm %{buildroot}%{_initscriptdir}/xen-watchdog
rm %{buildroot}%{_initscriptdir}/xencommons
rm %{buildroot}%{_initscriptdir}/xend
rm %{buildroot}%{_initscriptdir}/xendomains

# sysconfig
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -m 644 %{SOURCE30} %{buildroot}%{_sysconfdir}/sysconfig/xenstored
install -m 644 %{SOURCE31} %{buildroot}%{_sysconfdir}/sysconfig/xenconsoled
install -m 644 %{SOURCE32} %{buildroot}%{_sysconfdir}/sysconfig/blktapctrl

# sysconfig
mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE40} %{buildroot}%{_unitdir}/proc-xen.mount
install -m 644 %{SOURCE41} %{buildroot}%{_unitdir}/var-lib-xenstored.mount
install -m 644 %{SOURCE42} %{buildroot}%{_unitdir}/xenstored.service
install -m 644 %{SOURCE43} %{buildroot}%{_unitdir}/blktapctrl.service
install -m 644 %{SOURCE44} %{buildroot}%{_unitdir}/xend.service
install -m 644 %{SOURCE45} %{buildroot}%{_unitdir}/xenconsoled.service
install -m 644 %{SOURCE46} %{buildroot}%{_unitdir}/xen-watchdog.service
install -m 644 %{SOURCE47} %{buildroot}%{_unitdir}/xendomains.service
mkdir -p %{buildroot}%{_libexecdir}
install -m 644 %{SOURCE48} %{buildroot}%{_libexecdir}/xendomains

# config file only used for hotplug, Fedora uses udev instead
rm -f %{buildroot}/%{_sysconfdir}/sysconfig/xend

############ create dirs in /var ############

mkdir -p %{buildroot}%{_localstatedir}/lib/xen/xend-db/domain
mkdir -p %{buildroot}%{_localstatedir}/lib/xen/xend-db/vnet
mkdir -p %{buildroot}%{_localstatedir}/lib/xen/xend-db/migrate
mkdir -p %{buildroot}%{_localstatedir}/lib/xen/images
mkdir -p %{buildroot}%{_localstatedir}/log/xen/console

############ add pointer to kernels ############

mkdir -p %{buildroot}%{_sysconfdir}/yum.repos.d
cp %{SOURCE3} %{buildroot}%{_sysconfdir}/yum.repos.d

############ create symlink for x86_64 for compatibility with 3.4 ############

%if "%{_libdir}" != "/usr/lib"
ln -s /usr/lib/%{name}/bin/qemu-dm %{buildroot}/%{_libdir}/%{name}/bin/qemu-dm
%endif

############ debug packaging: list files ############

find %{buildroot} -print | xargs ls -ld | sed -e 's|.*%{buildroot}||' > f2.list
diff -u f1.list f2.list || true

############ assemble license files ############

mkdir licensedir
# avoid licensedir to avoid recursion, also stubdom/ioemu and dist
# which are copies of files elsewhere
find . -path licensedir -prune -o -path stubdom/ioemu -prune -o \
  -path dist -prune -o -name COPYING -o -name LICENSE | while read file; do
  mkdir -p licensedir/`dirname $file`
  install -m 644 $file licensedir/$file
done

############ all done now ############

%post
/bin/systemctl enable xendomains.service

%preun
if [ $1 = 0 ]; then
  /bin/systemctl enable xendomains.service
fi

%post runtime
/bin/systemctl enable xenstored.service
/bin/systemctl enable xenconsoled.service

%preun runtime
if [ $1 = 0 ]; then
  /bin/systemctl disable xenstored.service
  /bin/systemctl disable xenconsoled.service
fi

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%post hypervisor
if [ -f /sbin/grub2-mkconfig -a -f /boot/grub2/grub.cfg ]; then
  /sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
fi

%postun hypervisor
if [ $1 = 0 -a -f /sbin/grub2-mkconfig -a -f /boot/grub2/grub.cfg ]; then
  /sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
fi

%clean
rm -rf %{buildroot}

# Base package only contains XenD/xm python stuff
#files -f xen-xm.lang
%files
%defattr(-,root,root)
%doc COPYING README
%{_bindir}/xencons
%{_sbindir}/xend
%{_sbindir}/xm
%{python_sitearch}/%{name}
%{python_sitearch}/xen-*.egg-info
%{_mandir}/man1/xm.1*
%{_mandir}/man5/xend-config.sxp.5*
%{_mandir}/man5/xmdomain.cfg.5*
%{_datadir}/%{name}/create.dtd

# Startup script
# Guest config files
%config(noreplace) %{_sysconfdir}/%{name}/xmexample*
# Daemon config
%config(noreplace) %{_sysconfdir}/%{name}/xend-*
# xm config
%config(noreplace) %{_sysconfdir}/%{name}/xm-*
# Guest autostart links
%dir %attr(0700,root,root) %{_sysconfdir}/%{name}/auto
# Autostart of guests
%config(noreplace) %{_sysconfdir}/sysconfig/xendomains

%{_unitdir}/xend.service
%{_unitdir}/xendomains.service
%{_libexecdir}/xendomains

# Persistent state for XenD
%dir %{_localstatedir}/lib/%{name}/xend-db/
%dir %{_localstatedir}/lib/%{name}/xend-db/domain
%dir %{_localstatedir}/lib/%{name}/xend-db/migrate
%dir %{_localstatedir}/lib/%{name}/xend-db/vnet

# pointer to dom0 kernel
%config(noreplace) %{_sysconfdir}/yum.repos.d/dom0-kernel.repo

%files libs
%defattr(-,root,root)
%{_libdir}/*.so.*
%{_libdir}/fs

# All runtime stuff except for XenD/xm python stuff
%files runtime
%defattr(-,root,root)
# Hotplug rules
%config(noreplace) %{_sysconfdir}/udev/rules.d/*

%dir %attr(0700,root,root) %{_sysconfdir}/%{name}
%dir %attr(0700,root,root) %{_sysconfdir}/%{name}/scripts/
%config %attr(0700,root,root) %{_sysconfdir}/%{name}/scripts/*

%{_sysconfdir}/bash_completion.d/xl.sh

%{_unitdir}/proc-xen.mount
%{_unitdir}/var-lib-xenstored.mount
%{_unitdir}/xenstored.service
%{_unitdir}/blktapctrl.service
%{_unitdir}/xenconsoled.service
%{_unitdir}/xen-watchdog.service

%config(noreplace) %{_sysconfdir}/sysconfig/xenstored
%config(noreplace) %{_sysconfdir}/sysconfig/xenconsoled
%config(noreplace) %{_sysconfdir}/sysconfig/blktapctrl
%config(noreplace) %{_sysconfdir}/sysconfig/xencommons
%config(noreplace) %{_sysconfdir}/xen/xl.conf
%config(noreplace) %{_sysconfdir}/xen/cpupool

# Auto-load xen backend drivers
%attr(0755,root,root) %{_sysconfdir}/sysconfig/modules/%{name}.modules

# Rotate console log files
%config(noreplace) %{_sysconfdir}/logrotate.d/xen

# Programs run by other programs
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/bin
%attr(0700,root,root) %{_libdir}/%{name}/bin/*
# QEMU runtime files
%dir %{_datadir}/%{name}/qemu
%dir %{_datadir}/%{name}/qemu/keymaps
%{_datadir}/%{name}/qemu/keymaps/*

# man pages
%{_mandir}/man1/xentop.1*
%{_mandir}/man1/xentrace_format.1*
%{_mandir}/man8/xentrace.8*

%{python_sitearch}/fsimage.so
%{python_sitearch}/grub
%{python_sitearch}/pygrub-*.egg-info

# The firmware
%ifnarch ia64
# Avoid owning /usr/lib twice on i386
%if "%{_libdir}" != "/usr/lib"
%dir /usr/lib/%{name}
%dir /usr/lib/%{name}/bin
/usr/lib/%{name}/bin/stubdom-dm
/usr/lib/%{name}/bin/qemu-dm
/usr/lib/%{name}/bin/stubdompath.sh
%endif
%dir /usr/lib/%{name}/boot
# HVM loader is always in /usr/lib regardless of multilib
/usr/lib/xen/boot/hvmloader
/usr/lib/xen/boot/ioemu-stubdom.gz
/usr/lib/xen/boot/pv-grub*.gz
%endif
# General Xen state
%dir %{_localstatedir}/lib/%{name}
%dir %{_localstatedir}/lib/%{name}/dump
%dir %{_localstatedir}/lib/%{name}/images
# Xenstore persistent state
%dir %{_localstatedir}/lib/xenstored
# Xenstore runtime state
%ghost %{_localstatedir}/run/xenstored
# XenD runtime state
%ghost %attr(0700,root,root) %{_localstatedir}/run/xend
%ghost %attr(0700,root,root) %{_localstatedir}/run/xend/boot

# All xenstore CLI tools
%{_bindir}/qemu-*-xen
%{_bindir}/xenstore
%{_bindir}/xenstore-*
%{_bindir}/pygrub
%{_bindir}/xentrace*
%{_bindir}/remus
# blktap daemon
%{_sbindir}/blktapctrl
%{_sbindir}/tapdisk*
# XSM
%{_sbindir}/flask-*
# Disk utils
%{_sbindir}/qcow-create
%{_sbindir}/qcow2raw
%{_sbindir}/img2qcow
# Misc stuff
%{_bindir}/xen-detect
%{_sbindir}/gdbsx
%{_sbindir}/gtrace*
%{_sbindir}/kdd
%{_sbindir}/lock-util
%{_sbindir}/tap-ctl
%{_sbindir}/td-util
%{_sbindir}/vhd-*
%{_sbindir}/xen-bugtool
%{_sbindir}/xen-hptool
%{_sbindir}/xen-hvmcrash
%{_sbindir}/xen-hvmctx
%{_sbindir}/xen-tmem-list-parse
%{_sbindir}/xenconsoled
%{_sbindir}/xenlockprof
%{_sbindir}/xenmon.py*
%{_sbindir}/xenpaging
%{_sbindir}/xentop
%{_sbindir}/xentrace_setmask
%{_sbindir}/xenbaked
%{_sbindir}/xenstored
%{_sbindir}/xenpm
%{_sbindir}/xenpmd
%{_sbindir}/xenperf
%{_sbindir}/xenwatchdogd
%{_sbindir}/xl
%{_sbindir}/xsview

# Xen logfiles
%dir %attr(0700,root,root) %{_localstatedir}/log/xen
# Guest/HV console logs
%dir %attr(0700,root,root) %{_localstatedir}/log/xen/console

%files hypervisor
%defattr(-,root,root)
/boot/xen-syms-*
/boot/xen-*.gz
/boot/xen.gz

%files doc
%defattr(-,root,root)
%doc docs/misc/
%doc dist/install/usr/share/doc/xen/html
%doc dist/install/usr/share/doc/xen/pdf/*.pdf

%files devel
%defattr(-,root,root)
%{_includedir}/*.h
## this directory is provided by kernel-headers
# %%dir %%{_includedir}/xen
%{_includedir}/xen/*
%{_libdir}/*.so

%files licenses
%defattr(-,root,root)
%doc licensedir/*

%files ocaml
%defattr(-,root,root)
%{_libdir}/ocaml/xen*
%exclude %{_libdir}/ocaml/xen*/*.a
%exclude %{_libdir}/ocaml/xen*/*.cmxa
%exclude %{_libdir}/ocaml/xen*/*.cmx
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner
%{_sbindir}/oxenstored

%files ocaml-devel
%defattr(-,root,root)
%{_libdir}/ocaml/xen*/*.a
%{_libdir}/ocaml/xen*/*.cmxa
%{_libdir}/ocaml/xen*/*.cmx

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.5-6m)
- rebuild against graphviz-2.36.0-1m

* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.5-5m)
- rebuild against brltty-5.0

* Fri Nov 22 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.5-4m)
- update service files

* Tue Oct 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.5-3m)
- [SECURITY] CVE-2013-2072 CVE-2013-2076 CVE-2013-2077 CVE-2013-2078
- [SECURITY] CVE-2013-2194 CVE-2013-2195 CVE-2013-2196 CVE-2013-2211
- [SECURITY] CVE-2013-2212 CVE-2013-3495 CVE-2013-4329

* Tue Sep 24 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.5-2m)
- add build fix Patch6: xen-4.1.5-glibc217.patch

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.5-1m)
- [SECURITY] CVE-2012-3494 CVE-2012-3495 CVE-2012-3496 CVE-2012-3498
- [SECURITY] CVE-2012-3515 CVE-2012-4411 CVE-2012-5634 CVE-2012-6075
- [SECURITY] CVE-2013-1917 CVE-2013-1918 CVE-2013-1919 CVE-2013-1920
- [SECURITY] CVE-2013-1952 CVE-2013-1964
- update to 4.1.5

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-4m)
- rebuild against gnutls-3.2.0

* Thu Feb  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-3m)
- [SECURITY] CVE-2012-5510 CVE-2012-5511 CVE-2012-5512 CVE-2012-5513
- [SECURITY] CVE-2012-5514 CVE-2012-5515 CVE-2012-5634 CVE-2013-0153
- [SECURITY] CVE-2013-0154 CVE-2013-0215

* Tue Nov 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-2m)
- [SECURITY] CVE-2012-4535 CVE-2012-4536 CVE-2012-4537 CVE-2012-4538
- [SECURITY] CVE-2012-4539 CVE-2012-4544 CVE-2012-2625

* Mon Aug 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-1m)
- [SECURITY] CVE-2012-3432 CVE-2012-3433
- update to 4.1.3

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.2-3m)
- [SECURITY] CVE-2012-0217

* Mon Feb 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1.2-2m)
- add systemd service file
- [SECURITY] CVE-2012-0029

* Sat Oct 29 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1.2-1m)
- version up 4.1.2

* Mon Jul  4 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-3m)
- Obsoletes: xenner

* Tue Jun 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- set OCAML_TOOLS ?= n to enable build
- release %%{_includedir}/xen, it's provided by kernel-headers

* Sun Jun 12 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1.0-1m)
- sync Fedora 15

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-10m)
- rebuild against python-2.7

* Tue Apr 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-9m)
- fix build by gcc46
- [CAUTION] touch .config to enable build only, fix me

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.3-8m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.3-7m)
- [SECURITY] http://lists.xensource.com/archives/html/xen-devel/2010-11/msg01650.html
- fix build (import gcc45 patch from Fedora devel)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.3-6m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.3-5m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.3-4m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-3m)
- fix up

* Sun Aug 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.3-2m)
- do not start all daemons at start up time

* Thu Jul 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.3-1m)
- sync with Fedora 13 (3.4.3-2)

* Mon May  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.1-7m)
- fix build with gcc-4.4.4

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.1-6m)
- use Requires

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.1-5m)
- apply glibc212 patch

* Sat Jan  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-4m)
- release %%{_includedir}/xen, it's provided by kernel-headers

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.1-2m)
- BuildRequires: graphviz

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.1-1m)
- sync with Rawhide (3.4.1-4)

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.3.1-3m)
- stop daemon. modify Patch1

* Wed May 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.1-2m)
- modify Requires of xen-runtime
- move startup scripts from %%{_initrddir} to %%{_initscriptdir}

* Tue May 12 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.1-1m)
- [SECURITY] CVE-2008-4405 CVE-2008-5716
- sync Fedora

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-16m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-15m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-14m)
- [SECURITY] CVE-2008-0928
- import a security patch (Patch16) from Fedora 9 (3.2.0-15.fc9)

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-13m)
- [SECURITY] CVE-2008-1943 CVE-2008-1952 CVE-2008-1945 CVE-2008-2004
- import security patches from Fedora 9 (3.2.0-15.fc9)
-- Patch37 (CVE-2008-{1943,1952})
-- Patch38 (CVE-2008-{1945,2004})
- update Patch8 from Fedora 9 (3.2.0-15.fc9)
- update Patch3,13,102 for fuzz=0
- License: GPLv2+ and LGPLv2+ and Modified BSD

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.2.0-12m)
- rebuild agaisst python-2.6.1-1m

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-11m)
- rebuild against gnutls-2.4.1

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-10m)
- rebuild against openssl-0.9.8h-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-9m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.0-8m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-7m)
- rebuild against gcc43

* Fri Feb 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-6m)
- adjust %%files for the newest python
- BR: python-devel >= 2.5.1-7m

* Mon Feb 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-5m)
- adjust %%files again for x86_64

* Fri Feb  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-4m)
- adjust %%files for new python
- BR: python-devel >= 2.5.1-5m

* Thu Feb  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-3m)
- fix files conflicts

* Sat Feb  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-2m)
- split xen-runtime

* Thu Jan 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-1m)
- [SECURITY] CVE-2007-5906 CVE-2007-5907
- update 3.2.0

* Mon Dec  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-1m)
- update 3.1.2

* Tue Oct 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-3m)
- cleanup spec
- fix CVE-2007-1321, CVE-2007-3919, CVE-2007-4993
-- import Fedora Patches(3.1.0-13)
 
* Mon Jul 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.0-2m)
- add Requires: initscripts
- fix %%post PATH to service
 
* Tue Jun 19 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.1.0-1m)
- up to 3.1.0
- be synchronized with fedora
- modify initscripts

* Fri Jun  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.0-0.rc7.1m)
- sync Fedora

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.3-4m)
- makimodosi 3.0.3-2

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.3-3m)
- add export LDFLAGS="-L/%{_lib} $LDFLAGS"
-- Because /lib/libncurses.so cannot be linked

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.3-2m)
- rebuild against python-2.5

* Mon Oct 06 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.3-1m)
- merge FC 3.0.3-1

* Fri Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.2-1m)
- import from fc-devel

* Sun Jan  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0-0.20051206.1m)
- sync with fc-devel

* Sat Oct 30 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2-20051030m)
- rebuild against python-2.4.2

* Wed Mar 23 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2-20050308m)
- import from FC

* Wed Mar  9 2005 Rik van Riel <riel@redhat.com> 2-20050308
- upgrade to last night's snapshot
- new compile fix patch

* Sun Mar  6 2005 Rik van Riel <riel@redhat.com> 2-20050305
- the gcc4 compile patches are now upstream
- upgrade to last night's snapshot, drop patches locally

* Fri Mar  4 2005 Rik van Riel <riel@redhat.com> 2-20050303
- finally got everything to compile with gcc4 -Wall -Werror

* Thu Mar  3 2005 Rik van Riel <riel@redhat.com> 2-20050303
- upgrade to last night's Xen-unstable snapshot
- drop printf warnings patch, which is upstream now

* Wed Feb 23 2005 Rik van Riel <riel@redhat.com> 2-20050222
- upgraded to last night's Xen snapshot
- compile warning fixes are now upstream, drop patch

* Sat Feb 19 2005 Rik van Riel <riel@redhat.com> 2-20050219
- fix more compile warnings
- fix the fwrite return check

* Fri Feb 18 2005 Rik van Riel <riel@redhat.com> 2-20050218
- upgrade to last night's Xen snapshot
- a kernel upgrade is needed to run this Xen, the hypervisor
  interface changed slightly
- comment out unused debugging function in plan9 domain builder
  that was giving compile errors with -Werror

* Tue Feb  8 2005 Rik van Riel <riel@redhat.com> 2-20050207
- upgrade to last night's Xen snapshot

* Tue Feb  1 2005 Rik van Riel <riel@redhat.com> 2-20050201.1
- move everything to /var/lib/xen

* Tue Feb  1 2005 Rik van Riel <riel@redhat.com> 2-20050201
- upgrade to new upstream Xen snapshot

* Tue Jan 25 2005 Jeremy Katz <katzj@redhat.com>
- add buildreqs on python-devel and xorg-x11-devel (strange AT nsk.no-ip.org)

* Mon Jan 24 2005 Rik van Riel <riel@redhat.com> - 2-20050124
- fix /etc/xen/scripts/network to not break with ipv6 (also sent upstream)

* Fri Jan 14 2005 Jeremy Katz <katzj@redhat.com> - 2-20050114
- update to new snap
- python-twisted is its own package now
- files are in /usr/lib/python now as well, ugh.

* Tue Jan 11 2005 Rik van Riel <riel@redhat.com>
- add segment fixup patch from xen tree
- fix %files list for python-twisted

* Mon Jan 10 2005 Rik van Riel <riel@redhat.com>
- grab newer snapshot, that does start up
- add /var/xen/xend-db/{domain,vnet} to %files section

* Thu Jan  6 2005 Rik van Riel <riel@redhat.com>
- upgrade to new snapshot of xen-unstable

* Mon Dec 13 2004 Rik van Riel <riel@redhat.com>
- build python-twisted as a subpackage
- update to latest upstream Xen snapshot

* Sun Dec  5 2004 Rik van Riel <riel@redhat.com>
- grab new Xen tarball (with wednesday's patch already included)
- transfig is a buildrequire, add it to the spec file

* Wed Dec  1 2004 Rik van Riel <riel@redhat.com>
- fix up Che's spec file a little bit
- create patch to build just Xen, not the kernels

* Wed Dec 01 2004 Che
- initial rpm release
