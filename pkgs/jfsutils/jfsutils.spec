%global momorel 7

Summary: IBM JFS utility programs
Name: jfsutils
Version: 1.1.13
URL: http://oss.software.ibm.com/jfs/
Release: %{momorel}m%{?dist}
Source0: http://jfs.sourceforge.net/project/pub/%{name}-%{version}.tar.gz 
NoSource: 0
License: GPL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group: System Environment/Base
BuildRequires: e2fsprogs-devel

%description
The jfsutils package contains a number of utilities for creating,
checking, modifying, and correcting any inconsistencies in JFS
filesystems.  The following utilities are available: fsck.jfs - initiate
replay of the JFS transaction log, and check and repair a JFS formatted
device; logdump - dump a JFS formatted device's journal log; logredo -
"replay" a JFS formatted device's journal log;  mkfs.jfs - create a JFS
formatted partition; xchkdmp - dump the contents of a JFS fsck log file
created with xchklog; xchklog - extract a log from the JFS fsck workspace
into a file;  xpeek - shell-type JFS file system editor.


%prep
%setup -q

%build
CFLAGS="${RPM_OPT_FLAGS}" ./configure --mandir=%{_mandir}
make

%install
[ "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# let brp-compress handle this policy
rm -f $RPM_BUILD_ROOT/%{_mandir}/*/*.gz

%clean
[ "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/sbin/*
%{_mandir}/man8/*
%{_mandir}/*/*
%doc AUTHORS COPYING INSTALL NEWS README ChangeLog

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.13-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.13-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.13-5m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.13-4m)
- use BuildRequires

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.13-2m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.13-1m)
- update to 1.1.13

* Thu Jul 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.12-1m)
- update to 1.1.12
- changelog is below
-
- * Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.1.12-2
- - Autorebuild for GCC 4.3
- 
- * Sat Aug 25 2007 Josh Boyer <jwboyer@jdub.homelinux.org> - 1.1.12-1
- - Update to latest release
- - Drop obsoleted patch (fixed upstream)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.11-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.11-2m)
- %%NoSource -> NoSource

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.11-1m)
- update to 1.1.11

* Tue May 16 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.1.10-1m)
- update to 1.1.10

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.7-1m)
- version update to 1.1.7

* Sun May  2 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.6-1m)
- version update to 1.1.6

* Sun Mar 30 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.2-1m)
  update to 1.1.2
  
* Sun Feb 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.1-2m)
- BuildPreReq: e2fsprogs-devel

* Thu Feb 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.1-1m)
  update to 1.1.1

* Wed Aug 21 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.21-1m)
- update to 1.0.21

* Thu Jun 27 2002 Tsutomu Yasuda <hab03481@syd.odn.ne.jp>
- (1.0.20-2k)
  update to 1.0.20
  
* Thu Jun 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0.19-2k)
  update to 1.0.19

* Fri Apr 17 2002 MATSUDA, Daiki <dyky@dyky.org>
- (1.0.17-2k)
- update to 1.0.17

* Sat Mar 16 2002 MATSUDA, Daiki <dyky@dyky.org>
- (1.0.16-2k)
- update to 1.0.16

* Tue Feb 19 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.15-2k)
- update to 1.0.15

* Mon Jan 28 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.14-2k)
- update to 1.0.14

* Sun Jan 27 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.13-2k)
- update to 1.0.13

* Thu Jan 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.12-4k)
- automake autoconf

* Wed Jan  9 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.12-2k)
- update to 1.0.12

* Wed Dec 19 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.11-2k)
- update to 1.0.11

* Mon Nov 12 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.9-2k)
- update to 1.0.9

* Wed Oct 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.8-2k)
- update to 1.0.8

* Sun Oct 10 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.7-2k)
- update to 1.0.7

* Sun Sep 30 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.6-2k)
- update to 1.0.6

* Sat Sep 22 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.5-2k)
- First Kondarization

* Fri Sep 14 2001 Barry Arndt <barndt@us.ibm.com>
- Updates for removal of JFS utils from JFS file system build tree.

* Fri Sep 7 2001 Anthony Liu <anthony@nexus-online.com>
- RPM 3.x.x compatible: please do not add macros that breaks RPM 3.x.x
  for no apparent reason

* Sun Sep 02 2001 Andy Dustman <andy@dustman.net>
- Fixed spec file to reflect 2.2 and 2.4 kernel patch naming scheme.

* Sat Nov 18 2000 Jim Nance <jlnance@intrex.net>
- Built initial spec file

