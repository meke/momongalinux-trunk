%global momorel 8
%global prever 2
%global qtver 4.8.2

Summary: Qwit is a simple Qt4-based client for Twitter
Name: qwit
Version: 1.1
Release: 0.%{prever}.%{momorel}m%{?dist}
# keep GPLv2+ for Source1
License: GPLv3 and GPLv2+
Group: Applications/Internet
URL: http://code.google.com/p/qwit/
Source0: http://qwit.googlecode.com/files/%{name}-%{version}-pre%{prever}-src.tar.bz2
NoSource: 0
Source1: jp.png
Source2: %{name}_ja_JP.ts
Patch0: %{name}-0.9-desktop.patch
Patch1: %{name}-%{version}-make-ja.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: ImageMagick
BuildRequires: coreutils
BuildRequires: qoauth-devel

%description
Qwit is a simple Qt4-based client for Twitter.

Latest version features:
- depends on Qt4 only;
- KDE native look;
- http-proxy support;
- userpics caching;
- clickable links in statuses;
- public, replies, custom user timelines, search;
- caching messages between sessions;
- replies, retweets and direct messages support;
- customizable interface.

%prep
%setup -q -n %{name}-%{version}-pre%{prever}-src

%patch0 -p1 -b .desktop~

# for Japanese
%patch1 -p1 -b .make-ja

convert -scale 16x11 %{SOURCE1} images/countries/jp.png
install -m 644 %{SOURCE2} translations/%{name}_ja_JP.ts

pushd translations
lrelease-qt4 %{name}_ja_JP.ts
popd

%build
qmake-qt4 PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

# install and link icon
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32}/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps
convert -scale 16x16 images/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
install -m 644 images/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# clean up
rm -f %{buildroot}%{_datadir}/icons/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-0.2.8m)
- change Source1 and no NoSource

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-0.2.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-0.2.6m)
- rebuild for new GCC 4.5

* Sat Sep 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.2.5m)
- [BUG FIX] fix up Japanese Translation

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-0.2.4m)
- rebuild against qt-4.7.0-0.2.1m

* Wed Sep  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.2.3m)
- [BUG FIX] more fix up Japanese Translation

* Wed Sep  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.2.2m)
- [BUG FIX] fix up Japanese Translation

* Tue Sep  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.2.1m)
- [FIX FOR NEW TWITTER] update to 1.1 pre2
- update Japanese Translation
- update make-ja.patch
- remove version.patch

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-0.1.4m)
- full rebuild for mo7 release

* Sat Jul  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.1.3m)
- use correct Japanese Translation
- cp qwit_ja_JP.r323 qwit_ja_JP.ts
- qwit_ja_JP.ts was older than qwit_ja_JP.r323
- remove switches usesvn and stable
- clean up spec file

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-0.1.2m)
- correct version

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-0.1.1m)
- update to 1.1-beta

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.20100407.3m)
- rebuild against qt-4.6.3-1m

* Thu Apr  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.20100407.2m)
- rewrite old Japanese Translation for 1.0 beta

* Wed Apr  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.20100407.1m)
- update to 20100407 svn snapshot (rev. 323)
- update Japanese Translation for svn rev. 323
- fix up old Japanese Translation for 1.0 beta
- update Source1: kdebase-runtime to 4.4.2

* Fri Mar  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.1.4m)
- update Japanese Translation

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.1.3m)
- update Japanese Translation

* Tue Mar  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.1.2m)
- fix up Japanese Translation

* Tue Mar  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.1.1m)
- update to version 1.0 beta
- add Japanese Translation
- add make-ja.patch
- install jp.png from kdebase-runtime-4.4.0/l10n/jp/flag.png

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-1m)
- version 0.10

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-1m)
- version 0.9

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-1m)
- initial package for Twitter freaks using Momonga Linux
