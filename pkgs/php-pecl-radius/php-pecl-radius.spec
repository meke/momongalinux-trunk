%global momorel 1

%global php_apiver  %((echo 0; php -i 2>/dev/null | sed -n 's/^PHP API => //p') | tail -1)
%{!?__pecl:     %{expand: %%global __pecl     %{_bindir}/pecl}}
%{!?php_extdir: %{expand: %%global php_extdir %(php-config --extension-dir)}}
%global pecl_name radius

Name:           php-pecl-radius
Version:        1.2.7
Release:        %{momorel}m%{?dist}
Summary:        Radius client library
License:        BSD
Group:          Development/Languages
URL:            http://pecl.php.net/package/radius
Source0:        http://pecl.php.net/get/radius-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  php-devel >= 5.5.2 php-pear >= 1:1.4.9-1.2
Requires(post): %{__pecl}
Requires(postun): %{__pecl}
Provides:       php-pecl(%{pecl_name}) = %{version}
%if 0%{?php_zend_api:1}
Requires:       php(zend-abi) = %{php_zend_api}
Requires:       php(api) = %{php_core_api}
%endif

# RPM 4.8
%{?filter_provides_in: %filter_provides_in %{php_extdir}/.*\.so$}
%{?filter_setup}

%description
This package is based on the libradius of FreeBSD, with some modifications
and extensions.  This PECL provides full support for RADIUS authentication
(RFC 2865) and RADIUS accounting (RFC 2866), works on Unix and on Windows.
Its an easy way to authenticate your users against the user-database of your
OS (for example against Windows Active-Directory via IAS).

%prep
%setup -qc


%build
cd %{pecl_name}-%{version}
phpize
%configure
make %{?_smp_mflags}


%install
cd %{pecl_name}-%{version}
rm -rf $RPM_BUILD_ROOT
make install INSTALL_ROOT=$RPM_BUILD_ROOT

# install config file
install -d $RPM_BUILD_ROOT%{_sysconfdir}/php.d
cat > $RPM_BUILD_ROOT%{_sysconfdir}/php.d/%{pecl_name}.ini << 'EOF'
; Enable %{pecl_name} extension module
extension=%{pecl_name}.so
EOF

# Install XML package description
install -d $RPM_BUILD_ROOT%{pecl_xmldir}

%check
# simple module load test
cd %{pecl_name}-%{version}
php --no-php-ini \
    --define extension_dir=modules \
    --define extension=%{pecl_name}.so \
    --modules | grep %{pecl_name}


%if 0%{?pecl_install:1}
%post
%{pecl_install} %{pecl_xmldir}/%{name}.xml >/dev/null || :
%endif

%if 0%{?pecl_uninstall:1}
%postun
if [ $1 -eq 0 ] ; then
    %{pecl_uninstall} %{pecl_name} >/dev/null || :
fi
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc %{pecl_name}-%{version}/CREDITS %{pecl_name}-%{version}/examples
%config(noreplace) %{_sysconfdir}/php.d/%{pecl_name}.ini
%{php_extdir}/%{pecl_name}.so

%changelog
* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7
- rebuild against php-5.5.2

* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- import from Fedora for php-pear-Auth-RADIUS

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Jan 19 2012 Remi Collet <remi@fedoraproject.org> - 1.2.5-13
- build against php 5.4 with patch from upstream
- fix filters

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Jul  6 2011  Remi Collet <Fedora@FamilleCollet.com> - 1.2.5-11
- fix php_zend_api usage, fix FTBFS #715846

* Sat Oct 23 2010  Remi Collet <Fedora@FamilleCollet.com> - 1.2.5-10
- add filter_provides to avoid private-shared-object-provides ncurses.so

* Sat Aug 28 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.2.5-9
- clean define
- use more macros
- add simple load test in %%check

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Jul 12 2009 Remi Collet <Fedora@FamilleCollet.com> - 1.2.5-7
- rebuild for new PHP 5.3.0 ABI (20090626)

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Apr 19 2008 Christopher Stone <chris.stone@gmail.com> 1.2.5-5
- Fix Requires for post/postun sections (bz #442699)

* Fri Feb 22 2008 Christopher Stone <chris.stone@gmail.com> 1.2.5-4
- Properly register package

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.2.5-3
- Autorebuild for GCC 4.3

* Sun Sep 30 2007 Christopher Stone <chris.stone@gmail.com> 1.2.5-2
- Update to new standards

* Sat Sep 08 2007 Christopher Stone <chris.stone@gmail.com> 1.2.5-1
- Upstream sync

* Sun Mar 11 2007 Christopher Stone <chris.stone@gmail.com> 1.2.4-2
- Use new ABI check for FC-6
- Create directory to untar sources
- Remove %%{release} from Provides

* Sat Jul 01 2006 Christopher Stone <chris.stone@gmail.com> 1.2.4-1
- Initial release
