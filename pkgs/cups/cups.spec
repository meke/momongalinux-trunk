%global momorel 1

%global php_extdir %(php-config --extension-dir 2>/dev/null || echo %{_libdir}/php4)
%global php_apiver %((echo 0; php -i 2>/dev/null | sed -n 's/^PHP API => //p') | tail -1)

%global use_alternatives 1
%global lspp 1
%global cups_serverbin %{_exec_prefix}/lib/cups

Summary: Common Unix Printing System
Name: cups
Version: 1.7.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Daemons
Source0: http://www.cups.org/software/%{version}/cups-%{version}-source.tar.bz2
NoSource: 0
# Pixmap for desktop file
Source2: cupsprinter.png
# socket unit for cups-lpd service
Source3: cups-lpd.socket
# cups-lpd service unit configuration
# svn cant use '@' name
Source4: cups-lpd.service
# Logrotate configuration
Source6: cups.logrotate
# Backend for NCP protocol
Source7: ncp.backend
Source8: macros.cups

Patch1: cups-no-gzip-man.patch
Patch2: cups-system-auth.patch
Patch3: cups-multilib.patch
Patch5: cups-banners.patch
Patch6: cups-serverbin-compat.patch
Patch7: cups-no-export-ssllibs.patch
Patch8: cups-direct-usb.patch
Patch9: cups-lpr-help.patch
Patch10: cups-peercred.patch
Patch11: cups-pid.patch
Patch12: cups-eggcups.patch
Patch13: cups-driverd-timeout.patch
Patch14: cups-strict-ppd-line-length.patch
Patch15: cups-logrotate.patch
Patch16: cups-usb-paperout.patch
Patch17: cups-res_init.patch
Patch18: cups-filter-debug.patch
Patch19: cups-uri-compat.patch
Patch20: cups-str3382.patch
Patch21: cups-0755.patch
Patch22: cups-hp-deviceid-oid.patch
Patch23: cups-dnssd-deviceid.patch
Patch24: cups-ricoh-deviceid-oid.patch
Patch25: cups-systemd-socket.patch
Patch26: cups-lpd-manpage.patch
Patch27: cups-avahi-address.patch
Patch28: cups-enum-all.patch
Patch29: cups-dymo-deviceid.patch
Patch30: cups-freebind.patch
Patch31: cups-no-gcry.patch
Patch32: cups-libusb-quirks.patch
Patch33: cups-use-ipp1.1.patch
Patch34: cups-avahi-no-threaded.patch
Patch35: cups-ipp-multifile.patch
Patch36: cups-web-devices-timeout.patch
Patch37: cups-final-content-type.patch
Patch38: cups-journal.patch
Patch39: cups-synconclose.patch

Patch100: cups-lspp.patch

## SECURITY PATCHES:

Url: http://www.cups.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: chkconfig initscripts
Requires: %{name}-libs = %{version}-%{release}
%if %use_alternatives
Provides: /usr/bin/lpq /usr/bin/lpr /usr/bin/lp /usr/bin/cancel /usr/bin/lprm /usr/bin/lpstat
Requires: chkconfig
%endif

# Unconditionally obsolete LPRng so that upgrades work properly.
Obsoletes: lpd <= 3.8.15-3, lpr <= 3.8.15-3, LPRng <= 3.8.15-3
Provides: lpd = 3.8.15-4, lpr = 3.8.15-4

Obsoletes: cupsddk < 1.2.3-9m
Provides: cupsddk = 1.2.3-9m
Obsoletes: cupsddk-drivers < 1.2.3-9m
Provides: cupsddk-drivers = 1.2.3-9m
Obsoletes: cups-php < %{version}
Obsoletes: perl-Net-CUPS

BuildRequires: pam-devel
BuildRequires: pkgconfig
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: libacl-devel
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: make >= 3.80
BuildRequires: php-devel >= 5.4.1
BuildRequires: pcre-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: krb5-devel
BuildRequires: avahi-devel
BuildRequires: poppler-utils >= 0.20.5
BuildRequires: systemd-units

BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: openslp-devel

# Make sure we get postscriptdriver tags.
BuildRequires: pycups

%if %lspp
BuildRequires: libselinux-devel >= 1.23
BuildRequires: audit-libs-devel >= 2.0.4
%endif

# -fstack-protector-all requires GCC 4.0.1
BuildRequires: gcc >= 4.0.1

BuildRequires: dbus-devel >= 0.90
Requires: dbus >= 0.90

# Requires tmpwatch for the cron.daily script (bug #218901).
Requires: tmpwatch

Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(post): systemd-sysv

# We use portreserve to prevent our TCP port being stolen.
# Require the package here so that we know /etc/portreserve/ exists.
Requires: portreserve

Requires: poppler

# We ship udev rules which use setfacl.
Requires: udev
Requires: acl

# Make sure we have some filters for converting to raster format.
Requires: ghostscript-cups

%package devel
Summary: Common Unix Printing System - development environment
Group: Development/Libraries
License: LGPLv2
Requires: %{name}-libs = %{version}-%{release}
Requires: gnutls-devel
Requires: krb5-devel
Requires: zlib-devel
Obsoletes: cupsddk-devel < 1.2.3-9m
Provides: cupsddk-devel = 1.2.3-9m

%package libs
Summary: Common Unix Printing System - libraries
Group: System Environment/Libraries
License: LGPLv2

%package filesystem
Summary: Common Unix Printing System - directory layout
Group: System Environment/Base
BuildArch: noarch

%package lpd
Summary: Common Unix Printing System - lpd emulation
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}
Requires: xinetd

%package ipptool
Summary: Common Unix Printing System - tool for performing IPP requests
Group: System Environment/Daemons
Requires: %{name}-libs = %{version}-%{release}

%description
The Common UNIX Printing System provides a portable printing layer for 
UNIX(R) operating systems. It has been developed by Easy Software Products 
to promote a standard printing solution for all UNIX vendors and users. 
CUPS provides the System V and Berkeley command-line interfaces. 

%description devel
The Common UNIX Printing System provides a portable printing layer for 
UNIX(R) operating systems. This is the development package for creating
additional printer drivers, and other CUPS services.

%description libs
The Common UNIX Printing System provides a portable printing layer for 
UNIX(R) operating systems. It has been developed by Easy Software Products 
to promote a standard printing solution for all UNIX vendors and users. 
CUPS provides the System V and Berkeley command-line interfaces. 
The cups-libs package provides libraries used by applications to use CUPS
natively, without needing the lp/lpr commands.

%description filesystem
The Common UNIX Printing System provides a portable printing layer for
UNIX® operating systems. This package provides some directories which are
required by other packages that add CUPS drivers (i.e. filters, backends etc.).

%description lpd
The Common UNIX Printing System provides a portable printing layer for 
UNIX(R) operating systems. This is the package that provides standard 
lpd emulation.

%description ipptool
Sends IPP requests to the specified URI and tests and/or displays the results.

%prep
%setup -q
# Don't gzip man pages in the Makefile, let rpmbuild do it.
%patch1 -p1 -b .no-gzip-man
# Use the system pam configuration.
%patch2 -p1 -b .system-auth
# Prevent multilib conflict in cups-config script.
%patch3 -p1 -b .multilib
# Ignore rpm save/new files in the banners directory.
%patch5 -p1 -b .banners
# Use compatibility fallback path for ServerBin.
%patch6 -p1 -b .serverbin-compat
# Don't export SSLLIBS to cups-config.
%patch7 -p1 -b .no-export-ssllibs
# Allow file-based usb device URIs.
%patch8 -p1 -b .direct-usb
# Add --help option to lpr.
%patch9 -p1 -b .lpr-help
# Fix compilation of peer credentials support.
%patch10 -p1 -b .peercred
# Maintain a cupsd.pid file.
%patch11 -p1 -b .pid
# Fix implementation of com.redhat.PrinterSpooler D-Bus object.
%patch12 -p1 -b .eggcups
# Increase driverd timeout to 70s to accommodate foomatic (bug #744715).
%patch13 -p1 -b .driverd-timeout
# Only enforce maximum PPD line length when in strict mode.
%patch14 -p1 -b .strict-ppd-line-length
# Re-open the log if it has been logrotated under us.
%patch15 -p1 -b .logrotate
# Support for errno==ENOSPACE-based USB paper-out reporting.
%patch16 -p1 -b .usb-paperout
# Re-initialise the resolver on failure in httpAddrGetList() (bug #567353).
%patch17 -p1 -b .res_init
# Log extra debugging information if no filters are available.
%patch18 -p1 -b .filter-debug
# Allow the usb backend to understand old-style URI formats.
%patch19 -p1 -b .uri-compat
# Fix temporary filename creation.
%patch20 -p1 -b .str3382
# Use mode 0755 for binaries and libraries where appropriate.
%patch21 -p1 -b .0755
# Add an SNMP query for HP's device ID OID (STR #3552).
%patch22 -p1 -b .hp-deviceid-oid
# Mark DNS-SD Device IDs that have been guessed at with "FZY:1;".
%patch23 -p1 -b .dnssd-deviceid
# Add an SNMP query for Ricoh's device ID OID (STR #3552).
%patch24 -p1 -b .ricoh-deviceid-oid
# Add support for systemd socket activation (patch from Lennart
# Poettering).
%patch25 -p1 -b .systemd-socket
# Talk about systemd in cups-lpd manpage (part of bug #884641).
%patch26 -p1 -b .lpd-manpage
# Use IP address when resolving DNSSD URIs (bug #948288).
%patch27 -p1 -b .avahi-address
# Return from cupsEnumDests() once all records have been returned.
%patch28 -p1 -b .enum-all
# Added IEEE 1284 Device ID for a Dymo device (bug #747866).
%patch29 -p1 -b .dymo-deviceid
# Use IP_FREEBIND socket option when binding listening sockets (bug #970809).
%patch30 -p1 -b .freebind
# Don't link against libgcrypt needlessly.
%patch31 -p1 -b .no-gcry
# Added libusb quirk for Canon PIXMA MP540 (bug #967873).
%patch32 -p1 -b .libusb-quirks
# Default to IPP/1.1 for now (bug #977813).
%patch33 -p1 -b .use-ipp1.1
# Don't use D-Bus from two threads (bug #979748).
%patch34 -p1 -b .avahi-no-threaded
# Fixes for jobs with multiple files and multiple formats.
%patch35 -p1 -b .ipp-multifile
# Increase web interface get-devices timeout to 10s (bug #996664).
%patch36 -p1 -b .web-devices-timeout
# Reverted upstream change to FINAL_CONTENT_TYPE in order to fix
# printing to remote CUPS servers (bug #1010580).
%patch37 -p1 -b .final-content-type
# Allow "journal" log type for log output to system journal.
%patch38 -p1 -b .journal
# Set the default for SyncOnClose to Yes.
%patch39 -p1 -b .synconclose

# SECURITY PATCHES:

%if %lspp
# LSPP support.
%patch100 -p1 -b .lspp
%endif

sed -i -e '1iMaxLogSize 0' conf/cupsd.conf.in

# Let's look at the compilation command lines.
perl -pi -e "s,^.SILENT:,," Makedefs.in

f=CREDITS.txt
mv "$f" "$f"~
iconv -f MACINTOSH -t UTF-8 "$f"~ > "$f"
rm "$f"~

aclocal -I config-scripts
autoconf -I config-scripts

%build
export CFLAGS="$RPM_OPT_FLAGS -fstack-protector-all -DLDAP_DEPRECATED=1 -Wno-return-type"
# --enable-debug to avoid stripping binaries
%configure --with-docdir=%{_datadir}/%{name}/www --enable-debug \
%if %lspp
        --enable-lspp \
%endif
        --with-cupsd-file-perm=0755 \
        --with-log-file-perm=0600 \
        --enable-relro \
        --with-dbusdir=%{_sysconfdir}/dbus-1 \
        --with-php=/usr/bin/php-cgi \
        --enable-avahi \
        --enable-threads --enable-openssl \
        --enable-webif \
        --with-xinetd=no \
        localedir=%{_datadir}/locale

# If we got this far, all prerequisite libraries must be here.
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make BUILDROOT=$RPM_BUILD_ROOT install

rm -rf  $RPM_BUILD_ROOT%{_initddir} \
        $RPM_BUILD_ROOT%{_sysconfdir}/init.d \
        $RPM_BUILD_ROOT%{_sysconfdir}/rc?.d
mkdir -p $RPM_BUILD_ROOT%{_unitdir}

find $RPM_BUILD_ROOT%{_datadir}/cups/model -name "*.ppd" |xargs gzip -n9f

%if %use_alternatives
pushd $RPM_BUILD_ROOT%{_bindir}
for i in cancel lp lpq lpr lprm lpstat; do
        mv $i $i.cups
done
cd $RPM_BUILD_ROOT%{_sbindir}
mv lpc lpc.cups
cd $RPM_BUILD_ROOT%{_mandir}/man1
for i in cancel lp lpq lpr lprm lpstat; do
        mv $i.1 $i-cups.1 
done
cd $RPM_BUILD_ROOT%{_mandir}/man8
mv lpc.8 lpc-cups.8
popd
%endif

mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps $RPM_BUILD_ROOT%{_sysconfdir}/X11/sysconfig $RPM_BUILD_ROOT%{_sysconfdir}/X11/applnk/System $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -p -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_datadir}/pixmaps
install -p -m 644 %{SOURCE3} %{buildroot}%{_unitdir}
install -p -m 644 %{SOURCE4} %{buildroot}%{_unitdir}/cups-lpd@.service
install -p -m 644 %{SOURCE6} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/cups
install -p -m 755 %{SOURCE7} $RPM_BUILD_ROOT%{cups_serverbin}/backend/ncp

# Ship an rpm macro for where to put driver executables.
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/rpm/
install -m 0644 %{SOURCE8} $RPM_BUILD_ROOT%{_sysconfdir}/rpm/

# Ship a printers.conf file, and a client.conf file.  That way, they get
# their SELinux file contexts set correctly.
touch $RPM_BUILD_ROOT%{_sysconfdir}/cups/printers.conf
touch $RPM_BUILD_ROOT%{_sysconfdir}/cups/classes.conf
touch $RPM_BUILD_ROOT%{_sysconfdir}/cups/client.conf
touch $RPM_BUILD_ROOT%{_sysconfdir}/cups/subscriptions.conf
touch $RPM_BUILD_ROOT%{_sysconfdir}/cups/lpoptions
        
# LSB 3.2 printer driver directory
mkdir -p $RPM_BUILD_ROOT%{_datadir}/ppd

# Remove unshipped files.
rm -rf $RPM_BUILD_ROOT%{_mandir}/cat? $RPM_BUILD_ROOT%{_mandir}/*/cat?
rm -f $RPM_BUILD_ROOT%{_datadir}/applications/cups.desktop
rm -rf $RPM_BUILD_ROOT%{_datadir}/icons
# there are pdf-banners shipped with cups-filters (#919489)
rm -rf $RPM_BUILD_ROOT%{_datadir}/cups/banners
rm -f $RPM_BUILD_ROOT%{_datadir}/cups/data/testprint

# install /usr/lib/tmpfiles.d/cups.conf (bug #656566, bug #893834)
mkdir -p ${RPM_BUILD_ROOT}%{_tmpfilesdir}
cat > ${RPM_BUILD_ROOT}%{_tmpfilesdir}/cups.conf <<EOF
# See tmpfiles.d(5) for details

d /run/cups 0755 root lp -
d /run/cups/certs 0511 lp sys -

d /var/spool/cups/tmp - - - 30d
EOF

# /usr/lib/tmpfiles.d/cups-lp.conf (bug #812641)
cat > ${RPM_BUILD_ROOT}%{_tmpfilesdir}/cups-lp.conf <<EOF
# Legacy parallel port character device nodes, to trigger the
# auto-loading of the kernel module on access.
#
# See tmpfiles.d(5) for details

c /dev/lp0 0660 root lp - 6:0
c /dev/lp1 0660 root lp - 6:1
c /dev/lp2 0660 root lp - 6:2
c /dev/lp3 0660 root lp - 6:3
EOF

find $RPM_BUILD_ROOT -type f -o -type l | sed '
s:.*\('%{_datadir}'/\)\([^/_]\+\)\(.*\.po$\):%lang(\2) \1\2\3:
/^%lang(C)/d
/^\([^%].*\)/d
' > %{name}.lang


%post
%systemd_post %{name}.path %{name}.socket %{name}.service

# Remove old-style certs directory; new-style is /var/run
# (see bug #194581 for why this is necessary).
/bin/rm -rf %{_sysconfdir}/cups/certs
%if %use_alternatives
/usr/sbin/alternatives --install %{_bindir}/lpr print %{_bindir}/lpr.cups 40 \
         --slave %{_bindir}/lp print-lp %{_bindir}/lp.cups \
         --slave %{_bindir}/lpq print-lpq %{_bindir}/lpq.cups \
         --slave %{_bindir}/lprm print-lprm %{_bindir}/lprm.cups \
         --slave %{_bindir}/lpstat print-lpstat %{_bindir}/lpstat.cups \
         --slave %{_bindir}/cancel print-cancel %{_bindir}/cancel.cups \
         --slave %{_sbindir}/lpc print-lpc %{_sbindir}/lpc.cups \
         --slave %{_mandir}/man1/cancel.1.bz2 print-cancelman %{_mandir}/man1/cancel-cups.1.bz2 \
         --slave %{_mandir}/man1/lp.1.bz2 print-lpman %{_mandir}/man1/lp-cups.1.bz2 \
         --slave %{_mandir}/man8/lpc.8.bz2 print-lpcman %{_mandir}/man8/lpc-cups.8.bz2 \
         --slave %{_mandir}/man1/lpq.1.bz2 print-lpqman %{_mandir}/man1/lpq-cups.1.bz2 \
         --slave %{_mandir}/man1/lpr.1.bz2 print-lprman %{_mandir}/man1/lpr-cups.1.bz2 \
         --slave %{_mandir}/man1/lprm.1.bz2 print-lprmman %{_mandir}/man1/lprm-cups.1.bz2 \
         --slave %{_mandir}/man1/lpstat.1.bz2 print-lpstatman %{_mandir}/man1/lpstat-cups.1.bz2
%endif
rm -f %{_localstatedir}/cache/cups/*.ipp %{_localstatedir}/cache/cups/*.cache

# Deal with config migration due to CVE-2012-5519 (STR #4223)
IN=%{_sysconfdir}/cups/cupsd.conf
OUT=%{_sysconfdir}/cups/cups-files.conf
copiedany=no
for keyword in AccessLog CacheDir ConfigFilePerm        \
    DataDir DocumentRoot ErrorLog FatalErrors           \
    FileDevice FontPath Group LogFilePerm               \
    LPDConfigFile PageLog Printcap PrintcapFormat       \
    RemoteRoot RequestRoot ServerBin ServerCertificate  \
    ServerKey ServerRoot SMBConfigFile StateDir         \
    SystemGroup SystemGroupAuthKey TempDir User; do
    if ! [ -f "$IN" ] || ! /bin/grep -iq ^$keyword "$IN"; then continue; fi
    copy=yes
    if /bin/grep -iq ^$keyword "$OUT"; then
        if [ "`/bin/grep -i ^$keyword "$IN"`" ==        \
             "`/bin/grep -i ^$keyword "$OUT"`" ]; then
            copy=no
        else
            /bin/sed -i -e "s,^$keyword,#$keyword,i" "$OUT" || :
        fi
    fi
    if [ "$copy" == "yes" ]; then
        if [ "$copiedany" == "no" ]; then
            (cat >> "$OUT" <<EOF

# Settings automatically moved from cupsd.conf by RPM package:
EOF
            ) || :
        fi

        (/bin/grep -i ^$keyword "$IN" >> "$OUT") || :
        copiedany=yes
    fi

    /bin/sed -i -e "s,^$keyword,#$keyword,i" "$IN" || :
done

exit 0


%post lpd
%systemd_post cups-lpd.socket
exit 0

%post libs -p /sbin/ldconfig
    
%postun libs -p /sbin/ldconfig
    
%preun
%systemd_preun %{name}.path %{name}.socket %{name}.service

%if %use_alternatives 
if [ $1 -eq 0 ] ; then
        /usr/sbin/alternatives --remove print %{_bindir}/lpr.cups
fi
%endif

exit 0

%preun lpd
%systemd_preun cups-lpd.socket
exit 0 

%postun
%systemd_postun_with_restart %{name}.service
exit 0

%postun lpd
%systemd_postun_with_restart cups-lpd.socket
exit 0

%triggerun -- %{name} < 1.4.6
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply cups
# to migrate them to systemd targets
%{_bindir}/systemd-sysv-convert --save %{name} >/dev/null 2>&1 || :

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del cups >/dev/null 2>&1 || :
/bin/systemctl try-restart %{name}.service >/dev/null 2>&1 || :

%triggerin -- samba-client
ln -sf ../../../bin/smbspool %{cups_serverbin}/backend/smb || :
exit 0

%triggerun -- samba-client
[ $2 = 0 ] || exit 0
rm -f %{cups_serverbin}/backend/smb

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%files -f %{name}.lang
%doc README.txt CREDITS.txt CHANGES.txt
%dir %attr(0755,root,lp) %{_sysconfdir}/cups
%dir %attr(0755,root,lp) %{_localstatedir}/run/cups
%dir %attr(0511,lp,sys) %{_localstatedir}/run/cups/certs
%{_tmpfilesdir}/cups.conf
%{_tmpfilesdir}/cups-lp.conf
%verify(not md5 size mtime) %config(noreplace) %attr(0640,root,lp) %{_sysconfdir}/cups/cupsd.conf
%verify(not md5 size mtime) %config(noreplace) %attr(0640,root,lp) %{_sysconfdir}/cups/cups-files.conf
%attr(0640,root,lp) %{_sysconfdir}/cups/cupsd.conf.default
%verify(not md5 size mtime) %config(noreplace) %attr(0644,root,lp) %{_sysconfdir}/cups/client.conf
%verify(not md5 size mtime) %config(noreplace) %attr(0600,root,lp) %{_sysconfdir}/cups/classes.conf
%verify(not md5 size mtime) %config(noreplace) %attr(0600,root,lp) %{_sysconfdir}/cups/printers.conf
%verify(not md5 size mtime) %config(noreplace) %attr(0644,root,lp) %{_sysconfdir}/cups/snmp.conf
%verify(not md5 size mtime) %config(noreplace) %attr(0644,root,lp) %{_sysconfdir}/cups/subscriptions.conf
%{_sysconfdir}/cups/interfaces
%verify(not md5 size mtime) %config(noreplace) %attr(0644,root,lp) %{_sysconfdir}/cups/lpoptions
%dir %attr(0755,root,lp) %{_sysconfdir}/cups/ppd
%dir %attr(0700,root,lp) %{_sysconfdir}/cups/ssl
%config(noreplace) %{_sysconfdir}/pam.d/cups
%config(noreplace) %{_sysconfdir}/logrotate.d/cups
%dir %{_datadir}/%{name}/www
%dir %{_datadir}/%{name}/www/ca
%dir %{_datadir}/%{name}/www/cs
%dir %{_datadir}/%{name}/www/es
%dir %{_datadir}/%{name}/www/fr
%dir %{_datadir}/%{name}/www/it
%dir %{_datadir}/%{name}/www/ja
%dir %{_datadir}/%{name}/www/ru
%{_datadir}/%{name}/www/images
%{_datadir}/%{name}/www/*.css
%doc %{_datadir}/%{name}/www/index.html
%doc %{_datadir}/%{name}/www/help
%doc %{_datadir}/%{name}/www/robots.txt
%doc %{_datadir}/%{name}/www/ca/index.html
%doc %{_datadir}/%{name}/www/cs/index.html
%doc %{_datadir}/%{name}/www/es/index.html
%doc %{_datadir}/%{name}/www/de/index.html
%doc %{_datadir}/%{name}/www/fr/index.html
%doc %{_datadir}/%{name}/www/it/index.html
%doc %{_datadir}/%{name}/www/ja/index.html
%doc %{_datadir}/%{name}/www/pt_BR/index.html
%doc %{_datadir}/%{name}/www/ru/index.html
%dir %{_datadir}/%{name}/usb
%{_datadir}/%{name}/usb/org.cups.usb-quirks
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}.socket
%{_unitdir}/%{name}.path
%{_bindir}/cupstestppd
%{_bindir}/cupstestdsc
%{_bindir}/cancel*
%{_bindir}/lp*
%{_bindir}/ppd*
%{cups_serverbin}/backend/*
%{cups_serverbin}/cgi-bin
%dir %{cups_serverbin}/daemon
%{cups_serverbin}/daemon/cups-deviced
%{cups_serverbin}/daemon/cups-driverd
%{cups_serverbin}/daemon/cups-exec
%{cups_serverbin}/notifier
%{cups_serverbin}/filter/*
%{cups_serverbin}/monitor
%{_mandir}/man[1578]/*
# devel subpackage
%exclude %{_mandir}/man1/cups-config.1.*
# ipptool subpackage
%exclude %{_mandir}/man1/ipptool.1.*
%exclude %{_mandir}/man5/ipptoolfile.5.*
# lpd subpackage
%exclude %{_mandir}/man8/cups-lpd.8.*
%{_sbindir}/*
%dir %{_datadir}/cups/templates
%dir %{_datadir}/cups/templates/ca
%dir %{_datadir}/cups/templates/cs
%dir %{_datadir}/cups/templates/de
%dir %{_datadir}/cups/templates/es
%dir %{_datadir}/cups/templates/fr
%dir %{_datadir}/cups/templates/it
%dir %{_datadir}/cups/templates/ja
%dir %{_datadir}/cups/templates/pt_BR
%dir %{_datadir}/cups/templates/ru
%{_datadir}/cups/templates/*.tmpl
%{_datadir}/cups/templates/ca/*.tmpl
%{_datadir}/cups/templates/cs/*.tmpl
%{_datadir}/cups/templates/de/*.tmpl
%{_datadir}/cups/templates/es/*.tmpl
%{_datadir}/cups/templates/fr/*.tmpl
%{_datadir}/cups/templates/it/*.tmpl
%{_datadir}/cups/templates/ja/*.tmpl
%{_datadir}/cups/templates/pt_BR/*.tmpl
%{_datadir}/cups/templates/ru/*.tmpl
%dir %attr(1770,root,lp) %{_localstatedir}/spool/cups/tmp
%dir %attr(0710,root,lp) %{_localstatedir}/spool/cups
%dir %attr(0755,lp,sys) %{_localstatedir}/log/cups
%{_datadir}/pixmaps/cupsprinter.png
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/cups.conf
%{_datadir}/cups/drv/sample.drv
%{_datadir}/cups/examples
%{_datadir}/cups/mime/mime.types
%{_datadir}/cups/mime/mime.convs
%{_datadir}/cups/ppdc/*.defs
%{_datadir}/cups/ppdc/*.h

%files libs
%doc LICENSE.txt
%{_libdir}/*.so.*

%files filesystem
%dir %{cups_serverbin}
%dir %{cups_serverbin}/backend
%dir %{cups_serverbin}/driver
%dir %{cups_serverbin}/filter
%dir %{_datadir}/cups
#%%dir %%{_datadir}/cups/banners
#%%dir %%{_datadir}/cups/charsets
%dir %{_datadir}/cups/data
%dir %{_datadir}/cups/drv
%dir %{_datadir}/cups/mime
%dir %{_datadir}/cups/model
%dir %{_datadir}/cups/ppdc
%dir %{_datadir}/ppd

%files devel
%{_bindir}/cups-config
%{_libdir}/*.so
%{_includedir}/cups
%{_mandir}/man1/cups-config.1.*
%{_sysconfdir}/rpm/macros.cups

%files lpd
%{_unitdir}/cups-lpd.socket
%{_unitdir}/cups-lpd@.service
%{cups_serverbin}/daemon/cups-lpd
%{_mandir}/man8/cups-lpd.8.*

%files ipptool
%{_bindir}/ipptool 
%{_bindir}/ippfind
%dir %{_datadir}/cups/ipptool
%{_datadir}/cups/ipptool/*
%{_mandir}/man1/ipptool.1.*
%{_mandir}/man5/ipptoolfile.5.*

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.3-1m)
- update 1.7.3

* Tue Jan 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-1m)
- [SECURITY] CVE-2013-6891
- update to 1.7.1

* Sun Nov 17 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.0-1m)
- update 1.7.0

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-2m)
- rebuild against gnutls-3.2.0

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-3m)
- rebuild against poppler-0.20.5

* Sun Sep 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-2m)
- Obsoletes: perl-Net-CUPS

* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1
- sync with Fedora (1.6.1-5)

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-4m)
- rebuild against php-5.4.1

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-3m)
- rebuild against libtiff-4.0.1

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-2m)
- import and replace patches

* Tue Feb  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- many bug fixes included (http://www.cups.org/articles.php?L654)
- update to 1.5.2

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-5m)
- update patch

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-4m)
- [SECURITY] CVE-2011-2896
- update systemd patch

* Mon Aug  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-3m)
- update systemd patch

* Wed Jul 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-2m)
- fix %%files to avoid conflicting

* Tue Jul 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update 1.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-2m)
- rebuild for new GCC 4.5

* Sat Nov 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- [SECURITY] CVE-2010-2941
- some patches are remade based on 1.4.5
- import patch31 and patch38 from Fedora devel

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-4m)
- full rebuild for mo7 release

* Sun Aug  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-3m)
- fix BuildRequires
- rebuild against poppler 0.14.0-4m

* Sun Jun 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-2m)
- revert to gnutls because segfault occurs when printing on firefox

* Sun Jun 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-1m)
- [SECURITY] CVE-2010-0540 CVE-2010-0542 CVE-2010-1748 CVE-2010-2431
- [SECURITY] CVE-2010-2432
- enable support openssl instead of gnutls
- sync with Fedora devel
- Patch12,14 and 100 are remade based on 1.4.4
- remove upstreamed ptches (Patch26,27,32,38 and 200)

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3-1m)
- sync with Fedora 13 (1:1.4.3-11)

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.11-9m)
- rebuild against libjpeg-8a

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.11-8m)
- [SECURITY] CVE-2010-0302 CVE-2010-0393
- import security patches from Ubuntu jaunty (1.3.9-17ubuntu3.6)

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.11-7m)
- rebuild against audit-2.0.4

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.11-6m)
- [SECURITY] CVE-2009-3553
- apply upstream patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.11-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.11-4m)
- [SECURITY] CVE-2009-2820
- import a security patch from Fedora 10 (1:1.3.11-2)

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.11-3m)
- rebuild against libjpeg-7

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.11-2m)
- stop daemon

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.11-1m)
- update to 1.3.11

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.10-3m)
- use portreserve
- cleanup specfile

* Wed May  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.10-2m)
- fix cups_serverbin on x86_64

* Wed May  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.10-1m)
- [SECURITY] CVE-2009-0146 CVE-2009-0147 CVE-2009-0163 CVE-2009-0164
- [SECURITY] CVE-2009-0165 CVE-2009-0166 CVE-2009-0195 CVE-2009-0799
- [SECURITY] CVE-2009-0800 CVE-2009-0949 CVE-2009-1179 CVE-2009-1180
- [SECURITY] CVE-2009-1181 CVE-2009-1182 CVE-2009-1183
- [SECURITY] CVE-2009-3608 CVE-2009-3609
- partially sync with Fedora 10 (1:1.3.10-4)

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-5m)
- do not specify man file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-4m)
- rebuild against rpm-4.6

* Sun Dec 28 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-3m)
- [SECURITY] CVE-2008-5183
- import a security patch (Patch31) from Fedora 10 (1:1.3.9-4)
- drop Patch29, merged upstream
- update Patch1,9,13,100,1000 for fuzz=0

* Sat Dec  6 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-2m)
- [SECURITY] CVE-2008-5286
- import a security patch (Patch30) from http://www.cups.org/str.php?L2974

* Wed Oct 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.9-1m)
- [SECURITY] CVE-2008-3639 CVE-2008-3640 CVE-2008-3641
- update to 1.3.9

* Thu Aug  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-1m)
- [SECURITY] CVE-2008-1722 CVE-2008-5184
- update to 1.3.8

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-2m)
- rebuild against gnutls-2.4.1

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-1m)
- [SECURITY] CVE-2008-1373 CVE-2008-1374
- update to 1.3.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6-5m)
- rebuild against gcc43

* Wed Mar 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.6-4m)
- ship pstoraster, thanks, pm890c and JW

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.6-3m)
- rebuild against openldap-2.4.8

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.6-2m)
- fix gcc-4.3

* Thu Feb 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.6-1m)
- [SECURITY] CUPS "process_browse_data()" Double Free Vulnerability
- update to 1.3.6

* Wed Dec 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.5-1m)
- [SECURITY] CUPS Backend SNMP Remote Stack Overflow Vulnerability
- update to 1.3.5

* Fri Nov  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.4-1m)
- [SECURITY] CVE-2007-4351
- update to 1.3.4

* Sun Sep 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Thu Sep 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Sun Sep  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-2m)
- revise path to cups-lpd for x86_64
- thanks to http://pc11.2ch.net/test/read.cgi/linux/1188293074/33
- do not use %%NoSource macro

* Sat Aug 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-0.2.1m)
- update to 1.3rc2
- remove some patches (17,18,22,24)
- add patch15 (import from FC)
- update some patches (8,11,19,25,100) 
- sync with FC-devel

* Sat Jul 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.12-1m)
- update to 1.2.12
- update patch18 (cups-directed-broadcast.patch)

* Thu Jun 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.11-4m)
- disable service at initial start up

* Sat Jun 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.11-3m)
- change cups_serverbin %%{_exec_prefix}/lib/cups to %%{_libdir}/cups
- revive cups-1.2.6-lib64.patch

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.11-2m)
- remove %%{cups_serverbin}/filter/pstoraster, it's already provided by
  ghostscript-cups and pstoraster of ghostscript-cups(v 1.6) is newer than
  pstoraster of cups (v 1.3)

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.11-1m)
- [SECURITY] CVE-2007-0720 (fixed on 1.2.9)
- sync FC-devel

* Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.7-4m)
- rebuild against php-5.2.1

* Mon Mar 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-3m)
- use gnutls instead of openssl; openssl is no longer required.
- rebuild against gnutls-1.4.2-2m

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.7-2m)
- add BuildPrereq: php-devel, dbus-devel

* Mon Nov 20 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7

* Mon Nov 20 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.2.6-2m)
- add lib64 patch

* Tue Nov 07 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6
- unappliable patches are commented out

* Sat Jul 22 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.23-10m)
- disable cups-RunAsUser.patch

* Sat Jul 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.23-9m)
- import cups-1.1.23-image-png.patch from gentoo-x86-portage
 +- 03 Jul 2006; Stefan Schweizer <genstef@gentoo.org>
 +  +files/cups-1.1.23-image-png.patch, cups-1.1.23-r7.ebuild:
 +  libpng >=1.2.10 compat thanks to kojiro in bug 136346

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.23-8m)
- rebuild against openssl-0.9.8a

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.23-7m)
- add gcc4 patch
- Patch200: cups-1.1.23-gcc4.patch

* Tue May 17 2005 mutecat <mutecat@momonga-linux.org>
- (1.1.23-6m)
- add cups-1.1.19-charset.patch from turbo with arrange
- update cups-extradoc

* Wed Mar  9 2005 zunda <zunda at freeshell.org>
- (1.1.23-5m)
- cupsd.cond: using basic authentication better to reduce FAQs,
  /etc/cups is still owned by lp for users who want to use digest
  authentication.

* Tue Mar  8 2005 zunda <zunda at freeshell.org>
- (1.1.23-4m)
- cupsd.conf: use digest authentication instead of basic authentication
- /etc/cups is now owned by lp and /etc/cups/passwd.md5 touched for lppasswd

* Fri Feb 25 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.1.23-3m)
- revise pam file to support multilib
- cupsd now uses pam authentication
  (root or lp group users can be permitted)

* Wed Jan 19 2005 TAKAHASHI Tamotsu <tamo>
- (1.1.23-2m)
- fix 64bit security issues
 Patch2: xpdf-goo-sizet.patch (SUSE?)
 Patch3: cups-1.1.22-xpdf2-underflow.patch (gentoo cvs)
- fix CAN-2005-0064
 Patch4: cups-CAN-2005-0064.patch (xpdf-3.00pl3.patch)

* Mon Jan 10 2005 TAKAHASHI Tamotsu <tamo>
- (1.1.23-1m)
- fix CAN-2004-1125, CAN-2004-1267, CAN-2004-1268, CAN-2004-1269, and CAN-2004-1270

* Sun Nov 28 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.1.22-2m)
- Change URL

* Thu Nov 11 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.1.22-1m)
- update to 1.1.22

* Fri Aug 20 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.1.21rc1-3m)
- change cups-RunAsUser.patch (like SUSE)

* Sat Jul  3 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.21rc1-2m)
- stop daemon

* Fri Jun 18 2004 kourin <kourin@fh.freeserve.ne.jp>
- (1.1.21rc1-1m)
- update to 1.1.21rc1

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.19-11m)
- revised spec for enabling rpm 4.2.

* Wed Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.19-10m)
- pretty spec file.

* Fri Aug  1 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.1.19-10m)
- fix RunAsUser...

* Thu Jul 31 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.1.19-9m)
- add pap backend (obtained from Mandrake)
- set default group to "lp" instead of "sys"
- add cups-RunAsUser.patch to run cupsd as ordinary user (not root)
- fix typo (s/linsten/listen/)

* Wed Jul 23 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.19-8m)
- use .png images instead of .gif images

* Wed Jul 23 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.1.19-7m)
- [IMPORTANT] Make cupsd listen(2) only localhost by default.
  To make cupsd accept connections from outside,
  "Listen" directive in /etc/cupsd.conf must be changed
  (and "Allow","Deny" directives too, of course).
- according to http://www.cups.org/str.php?L196,
  pdftops reads /etc/cups/pdftops.conf instead of /etc/xpdfrc,
  so make a symlink from /etc/xpdfrc to /etc/cups/pdftops.conf and
  remove cups-1.1.19-xpdfrc.patch.
- make %{_sysconfdir}/cups/mime.* be replacable
  (normally these files are not edited by hand)
- add %{_var}/log/cups to %%files
- use redhat's initscript (cups.init)
- add xinetd file for cups-lpd
- import config file for logrotate (cups.logrotate) from redhat
- do not own %{_sysconfdir}/pam.d
- add more documents

* Sun Jul 20 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.1.19-6m)
- update cups-extradoc to 1.1.19
  (obtained from CVS at bazaar.turbolinux.co.jp)
- add %%{_bindir}/cupstestppd
- run /sbin/ldconfig at %%post and %%postun section of -libs
- use %%make and %%makeinstall

* Sun Jul 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.19-5m)
- rebuild against glibc-2.3.2

* Wed Jun 25 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.1.19-4m)
- add cups-1.1.19-xpdfrc.patch to make pdftops read /etc/xpdfrc

* Tue Jun 24 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.1.19-3m)
- modify %%post and %%preun sections
  (do not run chkconfig --del on upgrade)

* Tue Jun 17 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.19-2m)
  don't remade gif images.

* Wed May 28 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.19-1m)
  update to 1.1.19

* Fri May 23 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.18-5m)
- enable slp
- %%{?_smp_mflags}
- add Japanese resource

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.18-4m)
- rebuild against zlib 1.1.4-5m

* Sat Mar 22 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.1.18-3m)
- make symbolic link for printing via samba
  (requested by nosanosa [Momonga-devel.ja:01495])
- cups-devel: add %%{release} to Requires:

* Thu Mar 20 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.1.18-2m)
- make subpackage "libs"
- move from Zoo to Main
- do not include cat pages
- remove pstoraster subpackage

* Sat Mar 15 2003 Kikutani Makoto <poo@momonga-linux.org>
- (1.1.18-1m)
- new upstream version
- require ghostscript-cups

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.1.14-4k)
- rebuild against libpng 1.2.2.

* Thu Mar 14 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.1.14-2k)
  merge from STABLE_2_1(by zaki <zaki@kondara.org>)
  update to 1.1.14 (buffer overflow fixed version)
  http://online.securityfocus.com/bid/4100

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.1.10-12k)
- Removed Packager and Vendotr tag.

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.1.10-10k)
- devel reqs main

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- (1.1.10-8k)
- nigirisugi

* Tue Oct 30 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1.10-6k)
- modify deffattr problem

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (1.1.10-4k)
- rebuild against libpng 1.2.0.
