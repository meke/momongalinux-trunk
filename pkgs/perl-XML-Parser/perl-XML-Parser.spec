%global         momorel 11

Name:           perl-XML-Parser
Version:        2.41
Release:        %{momorel}m%{?dist}
Summary:        Perl module for parsing XML documents
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-Parser/
Source0:        http://www.cpan.org/authors/id/T/TO/TODDR/XML-Parser-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  expat-devel
BuildRequires:  perl >= 5.00405
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-libwww-perl
Requires:       expat
Requires:       perl-libwww-perl
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 0}

%description
This module provides ways to parse XML documents. It is built on top of
XML::Parser::Expat, which is a lower level interface to James Clark's expat
library. Each call to one of the parsing methods creates a new instance of
XML::Parser::Expat which is then used to parse the document. Expat options
may be provided when the XML::Parser object is created. These options are
then passed on to the Expat object on each parse call. They can also be
given as extra arguments to the parse methods, in which case they override
options given at XML::Parser creation time.

%prep
%setup -q -n XML-Parser-%{version}

find . -type f -exec sed -i 's|/usr/local/bin/perl|/usr/bin/perl|' {} 2>/dev/null ';'

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README Changes samples
%{_mandir}/man3/*
%dir %{perl_vendorarch}/XML/Parser
%{perl_vendorarch}/XML/Parser.pm
%{perl_vendorarch}/XML/Parser/Encodings
%{perl_vendorarch}/XML/Parser/Expat.pm
%{perl_vendorarch}/XML/Parser/Style
%{perl_vendorarch}/auto/XML/Parser
%{perl_vendorarch}/XML/Parser/LWPExternEnt.pl

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-11m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-10m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-9m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-8m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-7m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-6m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-5m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-4m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-2m)
- rebuild against perl-5.14.1

* Fri Jun  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-1m)
- update to 2.41

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.40-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.40-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.40-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.40-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.40-1m)
- update to 2.40

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.36-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.36-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.36-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.36-2m)
- rebuild against gcc43

* Fri Nov 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36-1m)
- update to 2.36

* Sat Nov 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.35-1m)
- update to 2.35

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.34-10m)
- use vendor

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.34-9m)
- rebuild against expat-2.0.0-1m

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.34-8m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.34-7m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.34-6m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.34-5m)
- remove Epoch from BuildRequires

* Mon Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (2.34-4m)
- add files XML/Parser/LWPExternEnt.pl too.

* Mon Apr  5 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.34-3m)
- add files XML/Parser/Style

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (2.34-2m)
- revised spec for rpm 4.2.

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.34-1m)

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.31-5m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.31-4m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
-(2.31-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
-(2.31-2m)
- kill %%define name

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
-(2.31-1m)
- rebuild against perl-5.8.0

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.30-9m)
- remove BuildRequires: gcc2.95.3

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.30-4k)
- rebuild against for perl-5.6.1

* Mon Sep 17 2001 Toru Hoshina <toru@df-usa.com>
- (2.30-4k)
- no more fixpack...

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (2.30-2k)
- merge from rawhide. based on 2.30-7.

* Sat Jul 20 2001 Crutcher Dunnavant <crutcher@redhat.com> 2.30-6
- fix .so file list

* Thu Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 2.30-4
- imported from mandrake. tweaked man path.

* Thu Jun 21 2001 Christian Belisle <cbelisle@mandrakesoft.com> 2.30-3mdk
- Fixed distribution tag.
- Put the right directories.
- Description and Summary more explicit

* Sun Jun 17 2001 Geoffrey Lee <snailtalk@mandrakesoft.com> 2.30-2mdk
- Rebuild for the latest perl.
- Remove the Distribution and Vendor Tag.

* Mon Feb 19 2001  Daouda Lo <daouda@mandrakesoft.com> 2.30-1mdk
- release 
- use of extern expat module (thanx mad hansen )

* Sat Sep 16 2000 Stefan van der Eijk <s.vandereijk@chello.nl> 2.29-2mdk
- Call spec-helper before creating filelist

* Wed Aug 09 2000 Jean-Michel Dault <jmdault@mandrakesoft.com> 2.29-1mdk
- Macroize package
