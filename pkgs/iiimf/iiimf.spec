%global momorel 27

%global ximdir /etc/X11/xinit/xim.d

Summary: Internet/Intranet Input Method Package
Name: iiimf
Version: 11.1.1280
Release: %{momorel}m%{?dist}
Group: User Interface/X
License: see "README" and see "license.html" and MIT/X
URL: http://www.openi18n.org/subgroups/im/IIIMF/
Source0: http://www.openi18n.org/download/docs/im-sdk/im-sdk.r11_1.1280.tar.bz2
Source10: newpy-xinit
Source12: iiimxcf-xinit
Source13: htt.conf
Source14: IIim
Source20: http://www.sun.com/share/text/termsofuse.html
Patch4: im-sdk-daemonize.patch
Patch5: iiimf-encoding.patch
Patch7: im-sdk.r11_1.1280.doc_iiimcf.xml-validation.patch
Patch8: im-sdk.r11_1.1280.mkstemp.patch
Patch9: iiimf-lock.patch
Patch10: im-sdk-segv.patch
Patch11: im-sdk.r11_1.1280.gcc34.patch
Patch12: im-sdk-11.1-x86_64-build.patch
Patch13: im-sdk-11.1-x86_64-vararg.patch
Patch14: im-sdk-11.1-lib64.patch
Patch15: im-sdk-r11_1-1280-gcc4.patch
Patch16: im-sdk-r11_1-1280-gcc44.patch
Patch17: im-sdk-r11_1-1280-pid_t.patch

Requires(post): chkconfig
Requires(preun): chkconfig

Requires: filesystem >= 2.1.6
Requires: iiimf_conv
BuildRequires: openmotif-devel >= 2.2.2 perl
#BuildRequires: docbook-style-dsssl openjade sgml-common
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Input Method Server based on IIIMP.

%package -n iiimf_conv
Summary: Conversion modules for Internet/Intranet Input Method Package
Group: User Interface/X
Obsoletes: iiimf-conv

%description -n iiimf_conv
Conversion modules for IIIMP server

%package newpy
Summary: new pinyin leif for iiimf
Group: User Interface/X
Requires: iiimf

%description newpy
simplified chinese language engine for iiimf

%package xiiimp
Summary: iiimf direct imput for X
Group: User Interface/X
Requires: iiimf

%description xiiimp
iiimf direct imput for X

%package doc
Summary: iiimf documentations
Group: User Interface/X
License: see "termsofuse.html"

%description doc
iiimf documentations

%prep
%setup -q -n im-sdk-r11_1-1280

%patch8 -p0
%patch4 -p0
%patch5 -p0
%patch7 -p0
%patch9 -p0
%patch10 -p1
%patch11 -p1
%ifarch x86_64 ia64 ppc64 alpha sparc64
%patch12 -p1
%patch13 -p1
%endif
%if %{_lib} == "lib64"
%patch14 -p1
%endif
%patch15 -p1 -b .gcc4
%patch16 -p1 -b .gcc44~
%patch17 -p1 -b .pid_t

# shape up the configure scripts and Makefiles
for f in `find . -name 'configure' -o -name '*.m4'`; do
	perl -p -i.orig -e 's|fpic|fPIC|g;s|IMDIR=[\x27\"]/usr(.*)[\x27\"]|IMDIR=\x27\${prefix}$1\x27|g' $f
done
for f in `find . -name 'Makefile.*'`; do
	perl -p -i.orig -e 's|moduledir = /usr|moduledir = \${prefix}|g' $f
done

# terms of use for html documents
cp %SOURCE20 doc/

%build
%define optflags -O0

# /usr/lib or /usr/lib64 substitution
# TODO: This could be done in a cleaner way...
find . -type f | xargs grep -l "/lib/im" | \
  xargs perl -pi -e "s|/lib/im|/%{_lib}/im|g;"

# Build the converter
# We use make -j1 option because j >=2 sometimes causes error
(cd lib/CSConv
CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
	sh ./configure --prefix=%{_prefix} --host=%{_target_platform} --with-trace-message
make -j1)

# Make the clients: xiiimp.so
(cd iiimxcf/xiiimp.so
CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
	sh ./configure --prefix=%{_prefix} --host=%{_target_platform}
make)

# Make the clients: htt_xbe
(cd iiimxcf/htt_xbe
CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
	sh ./configure --prefix=%{_prefix} --host=%{_target_platform}
make)

# Make the server modules: lib/iiimp
(cd lib/iiimp
CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
	./configure --prefix=%{_prefix} --host=%{_target_platform}
make -j1)

# Make the server modules: iiimsf
(cd iiimsf
CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
	./configure --prefix=%{_prefix} --host=%{_target_platform}
make -j1)

# Make the Language engine: newpy
(cd leif
CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
	sh ./configure --prefix=%{_prefix} --host=%{_target_platform}
make LE_TARGETS='newpy newpy/xaux')

# Make the HTML and TeX documents from the xml: doc/iiimcf
# This fails when I do not have an access to http://www.oasis-open.org/ T T.
#(cd doc/iiimcf
#       make XML_DECL=/usr/share/sgml/xml.dcl DOCBOOK_SSDIR=/usr/share/sgml/docbook/dsssl-stylesheets
#       gzip -9 iiimcf-spec.ps
#       ln -s t1.htm index.html
#       rm -f iiimcf-spec.log
#       rm -f iiimcf-spec.aux
#       rm -f iiimcf-spec.rtf)

%install
rm -rf --preserve-root %{buildroot}

# converter
make -C lib/CSConv prefix=%{buildroot}%{_libdir}/im install
(cd %{buildroot}%{_libdir}/im/share/icu/1.4.0/
rm -f icudata_cnvalias.dat
ln -s cnvalias.dat icudata_cnvalias.dat)

# xiiimp.so
make -C iiimxcf/xiiimp.so prefix=%{buildroot}%{_prefix} install
make -C iiimxcf/xiiimp.so prefix=%{buildroot}%{_prefix} moduledir=%{buildroot}%{_libdir}/X11/locale/common install
mkdir -p %{buildroot}%{_datadir}/config-sample/xiiimp
cat <<EOF > %{buildroot}%{_datadir}/config-sample/xiiimp/dot-iiimp
iiimp.server=iiimp://localhost:9010
EOF

# httx/htt_xbe
make -C iiimxcf/htt_xbe prefix=%{buildroot}%{_prefix} install
mkdir -p %{buildroot}%{ximdir}
%if %{_lib} == "lib64"
  perl -pi -e 's|lib|lib64|g' %SOURCE12
%endif
cp %SOURCE12 %{buildroot}%{ximdir}/iiimxcf

# iiimsf
make -C iiimsf prefix=%{buildroot}%{_prefix} install

# leif/newpy
make -C leif/newpy prefix=%{buildroot}%{_prefix} install
rm -f %{buildroot}%{_libdir}/im/locale/zh_CN/newpy/newpy_obj.jar
mkdir -p %{buildroot}%{ximdir}/
cp %SOURCE10 %{buildroot}%{ximdir}/newpy

# /etc/init.d/IIim
mkdir -p %{buildroot}%{_initscriptdir}
install -m0755 %SOURCE14 %{buildroot}%{_initscriptdir}/

# install manpages
mkdir -p %{buildroot}%{_mandir}/man3
cp doc/conv/*.3 %{buildroot}%{_mandir}/man3
mkdir -p %{buildroot}%{_mandir}/man4
cp doc/conv/*.4 %{buildroot}%{_mandir}/man4
mkdir -p %{buildroot}%{_mandir}/man5
cp doc/conv/*.5 %{buildroot}%{_mandir}/man5

# install htt.conf
cp %SOURCE13 %{buildroot}/%{_libdir}/im/

find %{buildroot} -name "*.la" -delete

# add locale dir
mkdir -p %{buildroot}%{_libdir}/im/locale/ja
mkdir -p %{buildroot}%{_libdir}/im/locale/zh_CN

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/chkconfig --add IIim
if test -r /var/lock/subsys/IIim; then
	%{_initscriptdir}/IIim stop
	%{_initscriptdir}/IIim start
fi

%preun
if [ $1 = 0 ]; then
	if test -r /var/lock/subsys/IIim; then
		%{_initscriptdir}/IIim stop
	fi
	/sbin/chkconfig --del IIim
fi

%files
%defattr(-, root, root)
%doc lib/README lib/license.html
%config(noreplace) %{_libdir}/im/htt.conf
%{_libdir}/im/htt
%{_libdir}/im/httx
%{_libdir}/im/htt_xbe
%{_libdir}/im/htt_server
%dir %{_libdir}/im/leif
%dir %{_libdir}/im/locale
%dir %{_libdir}/im/locale/ja
%dir %{_libdir}/im/locale/zh_CN
%{_libdir}/im/xiiimp*
%{_initscriptdir}/IIim

%files -n iiimf_conv
%defattr(-, root, root)
%doc lib/README lib/license.html
%dir %{_libdir}/im
%{_libdir}/im/csconv
%{_libdir}/im/icuconv
%{_libdir}/im/share
%{_mandir}/man3/csconv.3*
%{_mandir}/man3/csconv_close.3*
%{_mandir}/man3/csconv_open.3*
%{_mandir}/man4/close.4*
%{_mandir}/man4/conv.4*
%{_mandir}/man4/csconv.conf.4*
%{_mandir}/man4/encoding.norm.4*
%{_mandir}/man4/open.4*
%{_mandir}/man5/workspace.5*

%files newpy
%defattr(-, root, root)
%doc lib/README lib/license.html
%config %{ximdir}/newpy
%{_libdir}/im/leif/newpy.so
%{_libdir}/im/locale/zh_CN/newpy

%files xiiimp
%defattr(-, root, root)
%doc lib/README lib/license.html
%config %{ximdir}/iiimxcf
%dir %{_datadir}/config-sample/xiiimp
%config %{_datadir}/config-sample/xiiimp/dot-iiimp
%{_libdir}/X11/locale/common/*

%files doc
%defattr(-,root,root)
%doc doc/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.1.1280-27m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.1.1280-26m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1280-25m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.1.1280-24m)
- fix conflicting pid_t (Patch17)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.1.1280-23m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.1.1280-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 14 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11.1.1280-21m)
- add gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (11.1.1280-20m)
- rebuild against rpm-4.6

* Fri May  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (11.1.1280-19m)
- add locale dirs

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (11.1.1280-18m)
- rebuild against gcc43

* Tue Feb 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (11.1.1280-17m)
- No NoSource20

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (11.1.1280-16m)
- delete libtool library

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (11.1.1280-15m)
- rebuild against openmotif-2.3.0-beta2

* Sat Apr  1 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (11.1.1280-14m)
- revised installdir

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (11.1.1280-13m)
- add gcc4 patch
- Patch15: im-sdk-r11_1-1280-gcc4.patch

* Wed Oct 19 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (11.1.1280-12m)
- enable ia64, ppc64, alpha

* Thu Mar 17 2005 Toru Hoshina <t@momonga-linux.org>
- (11.1.1280-11m)
- enable x86_64.

* Fri Jan  7 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (11.1.1280-10m)
- add gcc34 patch

* Sat Jul  3 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (11.1.1280-9m)
- stop daemon

* Wed Jun  1 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (11.1.1280-8m)
- add source

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (11.1.1280-7m)
- revised spec for enabling rpm 4.2.

* Tue Nov 25 2003 zunda <zunda at freeshell.org>
- (11.1.1280-6m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Jul 26 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (11.1.1280-5m)
- fix defattr problem.

* Mon Jun 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (11.1.1280-4m)
- set CXXFLAGS
- segv ra nu?

* Mon May  5 2003 zunda <zunda at freeshell.org>
- (11.1.1280-3m)
- NoSource: 0 ... sumaso

* Mon May  5 2003 zunda <zunda at freeshell.org>
- (11.1.1280-2m)
- iiimf-lock.patch by famao to avoid lock-up between htt_server and xiiimp

* Sun Apr 13 2003 zunda <zunda at freeshell.org>
- (11.1.1280-1m)
- update to r11_1.1280
- more to do: pam.d/htt, doc, www.momonga-linux.org/~famao/iiimf-lock.patch
- Added to momonga CVS: IIim im-sdk.r11_1.1280.doc_iiimcf.xml-validation.patch
  im-sdk.r11_1.1280.mkstemp.patch
- Removed from momonga CVS: im-sdk-initscript.patch iiimf-Xm.patch
  iiimf-kondara.patch iiimf-gcc296-tekitou.patch
  iiimf-glibc22-tekitou.patch im-sdk-20001221-kondara.patch
  im-sdk-20020408.tar.bz2
- Patch1: im-sdk-initscript.patch has already been applied to Source14: IIim
- Part of Patch2: im-sdk-20001221-kondara.patch is remade as
  im-sdk.r11_1.1280.kondara-alpha.patch (not tested).
- Part of Patch2: im-sdk-20001221-kondara.patch is divided into
  im-sdk.r11_1.1280.mkstemp.patch.
- Patch3: iiimf-Xm.patch seems to be no longer needed.
- Patch4: im-sdk-daemonize.patch is remade for r11_1.1280.
- /etc/init.d/IIim: -syslog option for htt removed. htt_server now seems
  not accepting the option (see iiimsf/src/IMSvrArg.cpp).

* Mon Sep 16 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (1.2-0.020020408022m)
- remove Canna-devel from BuildPrereq

* Mon Sep 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2-0.020020408021m)
- remove iiimf-canna package

* Sun Sep 15 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (1.2-0.020020408020m)
- add iiimf-encoding.patch

* Sat Sep 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2-0.020020408019m)
- add iiimf-canna.patch

* Tue Jun  4 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2-0.020020408018k)
- %dir %{_libdir}/im/leif

* Tue May 21 2002 Motonobu Ichimura <famao@kondara.org>
- (1.2-0.020020408016k)
- modified daemonize patch

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (1.2-0.020020408014k)
- rebuild against openmotif-2.2.2-2k.

* Wed May  1 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2-0.020020408012k)
- revise initscript

* Tue Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2-0.020020408010k)
- umu

* Tue Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2-0.020020408008k)
- /etc/init.d/IIim: avoid lock up

* Tue Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2-0.020020408006k)
- htt: add daemon mode

* Fri Apr 12 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.2-0.020020408004k)
- nigittenu

* Mon Apr 08 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.2-0.020020408002k)
- updated cvs r10_1 of 2002/04/08

* Sat Mar 16 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.2-0.020020207004k)
- fix build failure with lesstif

* Thu Feb 07 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.2-0.020020207002k)
- updated cvs r10_1 of 2002/02/07

* Sat Dec 01 2001 Toshiro HIKITA <toshi@sodan.org>
- (1.2-0.020011201002k)
- updated cvs r10_1 of 2001/12/01
- remove Xlcint.h-tmp.patch

* Mon Nov 12 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.0.020011030010k)
- use -O0 to build

* Mon Nov 12 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.0.020011030008k)
- add htt.conf
- use -fPIC instead -fpic
- move iiimp config file to config-sample/xiiimp

* Sun Nov 04 2001 Toshiro HIKITA <toshi@sodan.org>
- (1.2-0.020011030004k)
- remove iiimf-kondara.patch
- add im-sdk-initscript.patch

* Tue Oct 30 2001 Toshiro HIKITA <toshi@sodan.org>
- (1.2-0.020011030002k)
- updated cvs r10_1 of 2001/10/30 (Sun Resync Version)
- newsubpackage canna Canna Language Engine
- use install instead of "cp -p"
- add cannale-xinit
- add iiimxcf-xinit
- remove iiimf-gcc296-tekitou.patch
- remove xiiimp.XIC.patch
- remove iiimf.no-sampleha2.patch
- add Xlcint.h-tmp.patch

* Sat Oct 20 2001 Toshiro HIKITA <toshi@sodan.org>
- (1.2-0.020011020004k)
- updated cvs r10_1 of 2001/10/20
- fix typo of \$HOME to \\\$HOME
- change package name iiimf-conv to iiimf_conv
  to adopt with Li18nux and ATOK X packages
- add new patch xiiimp.XIC.patch
- add new patch iiimf.no-sampleha2.patch

* Mon Sep 03 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2-0.02001007070026k)
- fix Group

* Sun Jul  8 2001 Toru Hoshina <toru@df-usa.com>
- (1.2-0.02001007070024)
- add alphaev5 support.

* Sat Jul 07 2001 Toshiro HIKITA <toshi@sodan.org>
- (1.2-0.0200100707002k)
- updated to cvs HEAD of 2001/07/07 (fix ATOKX Problem)
   Thank to Hisashi Miyashita <himi@li18nux.org>

* Thu Jun 14 2001 Toshiro HIKITA <toshi@sodan.org>
- (1.2-0.020010614003k)
- updated to cvs HEAD of 2001/06/14

* Wed Jun  6 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.1-0.020001221006k)
- modify files (xiiimp, newpy)

* Sat Dec 23 2000 Toshiro HIKITA <toshi@sodan.org>
- update Server to r10_1
- new package xiiimp
- remove glibc22 patch

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Sun Nov 26 2000 KIM Hyeong Cheol <kim@kondara.org>
- new CVS tree (fixed newpy problem)
- add newpy xinit file

* Fri Nov 23 2000 Toru Hoshina <toru@df-usa.com>
- alpha support added.

* Thu Nov 22 2000 KIM Hyeong Cheol <kim@kondara.org>
- (1.1-0.20001123k1)
- new version from CVS.
- first release for Jirai trees.

* Tue Nov  7 2000 KIM Hyeong Cheol <kim@kondara.org>
- (1-3k)
- new patch (glibc22-tekitou.patch)
- Obsoletes iiimf_conv

* Sat Sep 26 2000 KIM Hyeong Cheol <kim@kondara.org>
- (1-1k)
- first release.

