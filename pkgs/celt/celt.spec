%global momorel 4

Name:           celt
Version:        0.7.0
Release:        %{momorel}m%{?dist}
Summary:        An audio codec for use in low-delay speech and audio communication

Group:          System Environment/Libraries
License:        BSD
URL:            http://www.celt-codec.org/
Source0:        http://downloads.us.xiph.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: libogg-devel

%description
CELT (Constrained Energy Lapped Transform) is an ultra-low delay audio 
codec designed for realtime transmission of high quality speech and audio. 
This is meant to close the gap between traditional speech codecs 
(such as Speex) and traditional audio codecs (such as Vorbis). 

%package devel
Summary: Development package for celt
Group: Development/Libraries
Requires: libogg-devel
Requires: celt = %{version}-%{release}
Requires: pkgconfig

%description devel
Files for development with celt.

%prep
%setup -q

%build
%configure
# Remove rpath as per https://fedoraproject.org/wiki/Packaging/Guidelines#Beware_of_Rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm %{buildroot}%{_libdir}/libcelt.a
rm %{buildroot}%{_libdir}/libcelt.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README TODO
%{_bindir}/celtenc
%{_bindir}/celtdec
%{_libdir}/libcelt.so.0
%{_libdir}/libcelt.so.0.0.0

%files devel
%defattr(-,root,root,-)
%doc COPYING README
%{_includedir}/celt
%{_libdir}/pkgconfig/celt.pc
%{_libdir}/libcelt.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.0-1m)
- import from Fedora

* Fri Oct 30 2009 Peter Robinson <pbrobinson@gmail.com> 0.7.0-1
- New 0.7.0 upstream release

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul  6 2009 Peter Robinson <pbrobinson@gmail.com> 0.6.0-1
- New 0.6.0 upstream release

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 18 2009 Peter Robinson <pbrobinson@gmail.com> 0.5.2-1
- New upstream release, remove note about license as fix upstream

* Mon Feb 2 2009 Peter Robinson <pbrobinson@gmail.com> 0.5.1-2
- Updates for package review

* Mon Jan 5 2009 Peter Robinson <pbrobinson@gmail.com> 0.5.1-1
- Initial package
