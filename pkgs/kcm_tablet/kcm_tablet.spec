%global momorel 3
%global srcrel 114856
%global srcname wacomtablet
%global qtver 4.7.0
%global kdever 4.5.2
%global kdelibsrel 1m

Summary: Wacom Linux Drivers based tablet kcontrol module for KDE
Name: kcm_tablet
Version: 1.2.3b
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://kde-apps.org/content/show.php/kcm+tablet?content=114856
Group: Applications/System
Source0: http://kde-apps.org/CONTENT/content-files/%{srcrel}-%{srcname}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: oxygen-icons
Requires: dbus
Requires: xorg-x11-drv-wacom
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake

%description
This is configuration control panel module for linuxwacom-driven tablets.

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p1 -b .desktop-ja

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING README
%{_kde4_libdir}/kde4/kcm_%{srcname}.so
%{_kde4_libdir}/kde4/kded_%{srcname}.so
%{_kde4_libdir}/kde4/plasma_applet_%{srcname}settings.so
%{_kde4_appsdir}/%{srcname}
%{_datadir}/dbus-1/interfaces/org.kde.Wacom.xml
%{_datadir}/dbus-1/interfaces/org.kde.WacomDevice.xml
%{_kde4_datadir}/kde4/services/kded/%{srcname}.desktop
%{_kde4_datadir}/kde4/services/kcm_%{srcname}.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-%{srcname}settings.desktop
%{_datadir}/locale/*/LC_MESSAGES/%{srcname}.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3b-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3b-2m)
- rebuild for new GCC 4.5

* Thu Oct 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3b-1m)
- version 1.2.3b
- update desktop.patch

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-2m)
- full rebuild for mo7 release

* Fri Aug 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-1m)
- version 1.2.1
- update desktop.patch
- remove a switch stable6

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-2m)
- rebuild against qt-4.6.3-1m

* Mon Mar  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-1m)
- version 1.2
- update desktop.patch

* Fri Feb  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-1m)
- version 1.1.3
- set NoSource: 0 again

* Thu Dec 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-1m)
- version 1.1.2

* Mon Nov 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-2m)
- add a switch stable6

* Mon Nov 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1m)
- version 1.1
- unset NoSource: 0

* Sun Nov  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-2m)
- change Req from linuxwacom to xorg-x11-drv-wacom

* Sat Nov  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- initial package for Momonga Linux
