%global momorel 4

Summary: A Tubing and Extrusion Library for OpenGL
Name: libgle
Version: 3.1.0
Release: %{momorel}m%{?dist}
License: GPLv2 or "Artistic clarified and MIT"
Group: System Environment/Libraries
URL: http://www.linas.org/gle/
Source0: http://dl.sourceforge.net/project/gle/gle/gle-%{version}/gle-%{version}.tar.gz
NoSource: 0
# Make the examples makefile multilib-compliant
Patch0: libgle-examples-makefile.patch

BuildRequires: mesa-libGL-devel 
BuildRequires: freeglut-devel
BuildRequires: libXmu-devel
BuildRequires: libXi-devel 

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The GLE Tubing and Extrusion Library consists of a number of "C"
language subroutines for drawing tubing and extrusions. It is a very
fast implementation of these shapes, outperforming all other
implementations, most by orders of magnitude. It uses the
OpenGL programming API to perform the actual drawing of the tubing
and extrusions.

%package devel
Requires: glut-devel
Requires: libGL-devel
Requires: libGLU-devel
Requires: libX11-devel
Requires: libXext-devel
Requires: libXi-devel
Requires: libXmu-devel
Requires: libXmu-devel
Requires: libXt-devel
Summary: GLE includes and development libraries
Group: Development/Libraries

%description devel
Includes, man pages, and development libraries for the GLE Tubing and
Extrusion Library.

%prep
%setup -q -n gle-%{version}
%patch0 -p5

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# Clean up a bit
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
mv $RPM_BUILD_ROOT%{_docdir}/gle docs

%clean
rm -rf $RPM_BUILD_ROOT

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/*.so.*
%doc docs/AUTHORS docs/COPYING docs/README

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_includedir}/*
%{_mandir}/man?/*
%doc docs/examples docs/html


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-1m)
- import from Fedora 13

* Mon Jan 18 2010 Mary Ellen Foster <mefoster at gmail.com> - 3.1.0-4
- Add the full set of requirements for the -devel package

* Mon Nov 30 2009 Mary Ellen Foster <mefoster at gmail.com> - 3.1.0-3
- Incorporate some more suggestions from Thomas Fitzsimmons

* Wed Sep 30 2009 Mary Ellen Foster <mefoster at gmail.com> - 3.1.0-2
- Incorporating some clean-ups from Ralf Corsepius's spec file

* Tue Sep 29 2009 Mary Ellen Foster <mefoster at gmail.com> - 3.1.0-1
- Initial version, based on upstream .spec file
