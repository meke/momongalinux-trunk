%global momorel 1

Summary: Tool to convert AsciiDoc text files to DocBook, HTML or Unix man pages
Name: asciidoc
Version: 8.6.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
URL: http://www.methods.co.nz/asciidoc/

Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch
BuildRequires: python-devel >= 2.3
Requires: python >= 2.3

%description
AsciiDoc is a text document format for writing short documents, articles,
books and UNIX man pages.

%prep
%setup -q

%build

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m0755 %{buildroot}%{_sysconfdir}/asciidoc/{docbook-xsl,filters,javascripts,stylesheets,dblatex}
%{__install} -p -m0644 *.conf %{buildroot}%{_sysconfdir}/asciidoc/
%{__install} -p -m0644 docbook-xsl/*.xsl %{buildroot}%{_sysconfdir}/asciidoc/docbook-xsl/
%{__install} -p -m0644 dblatex/*.{xsl,sty} %{buildroot}%{_sysconfdir}/asciidoc/dblatex/
#%%{__install} -p -m0644 filters/code-filter.{conf,py} %{buildroot}%{_sysconfdir}/asciidoc/filters/
%{__install} -p -m0644 javascripts/*.js %{buildroot}%{_sysconfdir}/asciidoc/javascripts/
%{__install} -p -m0644 stylesheets/*.css %{buildroot}%{_sysconfdir}/asciidoc/stylesheets/
%{__cp} -pR images/ %{buildroot}%{_sysconfdir}/asciidoc/

%{__install} -Dp -m0755 asciidoc.py %{buildroot}%{_bindir}/asciidoc
%{__install} -Dp -m0755 a2x.py %{buildroot}%{_bindir}/a2x
%{__install} -Dp -m0644 doc/asciidoc.1 %{buildroot}%{_mandir}/man1/asciidoc.1
%{__install} -Dp -m0644 doc/a2x.1 %{buildroot}%{_mandir}/man1/a2x.1

%{__install} -d -m0755 %{buildroot}%{_datadir}/asciidoc/
%{__cp} -pR images/ stylesheets/ %{buildroot}%{_datadir}/asciidoc/


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc BUGS* CHANGELOG* COPYING COPYRIGHT README*
%doc doc/*.txt examples/
%doc %{_mandir}/man1/a2x.1*
%doc %{_mandir}/man1/asciidoc.1*
%config(noreplace) %{_sysconfdir}/asciidoc/
%{_bindir}/a2x
%{_bindir}/asciidoc
%{_datadir}/asciidoc/

%changelog
* Thu Sep 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.6.8-1m)
- update to 8.6.8

* Sat May 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.6.7-1m)
- update to 8.6.7
-- fixed a2x dblatex error 

* Sat Dec  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.6.6-1m)
- update to 8.6.6

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.6.5-1m)
- update to 8.6.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.6.2-4m)
- rebuild for new GCC 4.6

* Fri Dec  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.6.2-3m)
- apply patch0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.6.2-2m)
- rebuild for new GCC 4.5

* Sat Nov 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.6.2-1m)
- update to 8.6.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.5.3-2m)
- full rebuild for mo7 release

* Wed Jan 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.5.3-1m)
- update to 8.5.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (8.5.0-1m)
- update to 8.5.0

* Fri Jul 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (8.4.5-1m)
- update 8.4.5

* Fri May  8 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.4.4-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.2.5-2m)
- rebuild against gcc43

* Wed Feb 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2.5-1m)
- update to 8.2.5

* Fri Oct  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.2.3-1m)
- update to 8.2.3

* Thu Jul 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2.2-1m)
- update to 8.2.2
- this release includes some bug fixes

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2.1-1m)
- update to 8.2.1

* Tue Sep 26 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.0.0-1m)
- update to 8.0.0

* Mon Sep 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1.2-2m)
- import to Momonga

* Wed Jun 14 2006 Dag Wieers <dag@wieers.com> - 7.1.2-2
- Installation fixes.

* Thu Mar 09 2006 Dag Wieers <dag@wieers.com> - 7.1.2-1
- Updated to release 7.1.2.

* Fri Aug 12 2005 Dag Wieers <dag@wieers.com> - 7.0.1-3
- Add missing deffatr(). (Alain Rykaert)
- Put asciidoc in %%{_bindir}, instead of a symlink.

* Wed Aug 10 2005 Dag Wieers <dag@wieers.com> - 7.0.1-1
- Initial package. (using DAR)
