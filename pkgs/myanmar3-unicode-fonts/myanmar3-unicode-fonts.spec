%global momorel 8

%global fontname myanmar3-unicode
%global fontconf 65-%{fontname}.conf

%global archivename Myanmar3_Unicode

Name:    %{fontname}-fonts
Version: 3.00
Release: %{momorel}m%{?dist}
Summary: Myanmar3 unicode font

Group:   User Interface/X
License: LGPLv2+
URL:    http://www.myanmarnlp.net.mm 
Source0: http://www.myanmarnlp.net.mm/downloads/%{archivename}.zip
Source1: %{name}-fontconfig.conf

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
Myanmar3 unicode font from Myanmar Unicode &
NLP Research Center.

%prep
%setup -q -c
unzip -j -L -q %{SOURCE0}

%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%doc


%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.00-8m)
- add source

* Wed Jan 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.00-7m)
- update sources (maybe source was replaced)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.00-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.00-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.00-4m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.00-3m)
- sync with Fedora 13 (3.00-4)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.00-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.00-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.00-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild


* Fri Aug 01 2008 Minto Joseph <mvaliyav at redhat.com> - 3.00-1 
- initial package
