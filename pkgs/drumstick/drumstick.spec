%global momorel 5
%global qtver 4.7.1

Summary:        C++/Qt4 wrapper around the ALSA library sequencer interface
Name:           drumstick
Version:        0.5.0
Release:        %{momorel}m%{?dist}
Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://drumstick.sourceforge.net/
Source0:        http://dl.sourceforge.net/project/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         %{name}-0.4.1-linking.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  alsa-lib-devel
BuildRequires:  desktop-file-utils
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description
The drumstick library is a C++ wrapper around the ALSA library sequencer
interface, using Qt4 objects, idioms and style. The ALSA sequencer interface
provides software support for MIDI technology on GNU/Linux.

%package devel
Summary: Developer files for %{name}
Group:   Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{summary}.

%package examples
Summary: Example programs for %{name}
Group:   System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description examples
This package contains the test/example programs for %{name}.

%prep
%setup -q
%patch0 -p1 -b .linking

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} .. 
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast  DESTDIR=%{buildroot} -C %{_target_platform}
for i in %{buildroot}%{_datadir}/applications/* ; do
  desktop-file-validate $i
done

# revise desktop files
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Education \
  %{buildroot}%{_datadir}/applications/drumstick-drumgrid.desktop

desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Education \
  %{buildroot}%{_datadir}/applications/drumstick-guiplayer.desktop

desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Education \
  %{buildroot}%{_datadir}/applications/drumstick-vpiano.desktop

%clean
rm -rf %{buildroot}

%post examples
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
update-desktop-database &> /dev/null || :

%postun examples
update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans examples
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING
%{_libdir}/libdrumstick-file.so.*
%{_libdir}/libdrumstick-alsa.so.*
%{_datadir}/mime/packages/drumstick.xml
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libdrumstick-file.so
%{_libdir}/libdrumstick-alsa.so
%{_libdir}/pkgconfig/drumstick-file.pc
%{_libdir}/pkgconfig/drumstick-alsa.pc
%{_includedir}/drumstick/
%{_includedir}/drumstick.h

%files examples
%defattr(-,root,root,-)
%{_bindir}/drumstick-*
%{_datadir}/applications/drumstick-*.desktop
%{_datadir}/icons/hicolor/*/apps/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-4m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-3m)
- specify PATH for Qt4

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-2m)
- fix build failure

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-4m)
- full rebuild for mo7 release

* Sat Aug 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-3m)
- remove Education from Categories of desktop files

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-2m)
- fix build error

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- import from Fedora devel and update to 0.4.1

* Fri May 28 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.3.1-2
- sysinfo: don't crash when no timer module available (#597354, upstream patch)

* Fri May 28 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.3.1-1
- update to 0.3.1
- fix FTBFS due to the strict ld in Fedora >= 13

* Mon Mar 15 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.3.0-1
- update to 0.3.0 release

* Tue Feb 08 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.2.99-0.3.svn20100208
- update from SVN for KMid2 0.2.1

* Sun Jan 31 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.2.99-0.2.svn20100107
- put the alphatag before the disttag

* Fri Jan 29 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.2.99-0.1.svn20100107
- update to 0.2.99svn tarball
- renamed from aseqmm to drumstick by upstream

* Fri Jan 22 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.2.0-2
- require the main package with exact version-release in -examples

* Fri Jan 22 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.2.0-1
- First Fedora package
