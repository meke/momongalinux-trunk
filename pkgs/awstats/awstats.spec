%global momorel 6

Summary: Web/Mail/FTP log analyzer written by perl
Name: awstats
Version: 7.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://awstats.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
# Source0: http://dl.sourceforge.net/sourceforge/awstats/awstats-%{version}.tar.gz
Source0: http://awstats.sourceforge.net/files/awstats-%{version}.tar.gz
NoSource: 0
Source1: httpd-awstats.conf
Source2: awstats.conf
Source3: awstats.cron
Requires: httpd 
Requires: perl 

%description
AWStats is a free powerful and featureful server logfile analyzer that
shows you all your Web/Mail/FTP statistics including visits, unique
visitors, pages, hits, rush hours, os, browsers, search engines,
keywords, robots visits, broken links and more...

%prep
%setup -q

%build

%install
%{__rm} -rf --preserve-root %{buildroot}
%{__install} -d %{buildroot}%{_sysconfdir}/httpd/conf.d
%{__install} -d %{buildroot}%{_sysconfdir}/cron.hourly
%{__install} -d %{buildroot}%{_sysconfdir}/awstats
%{__install} -d %{buildroot}/var/www/awstats
%{__install} -d %{buildroot}/var/lib/awstats
%{__install} -m 644 %{SOURCE1} \
	%{buildroot}%{_sysconfdir}/httpd/conf.d/awstats.conf.dist
%{__install} -m 644 %{SOURCE2} \
	%{buildroot}%{_sysconfdir}/awstats/awstats.conf
%{__install} -m 644 %{SOURCE2} \
	%{buildroot}%{_sysconfdir}/awstats/awstats.model.conf
%{__install} -m 755 %{SOURCE3} \
	%{buildroot}%{_sysconfdir}/cron.hourly/awstats
%{__cp} -a wwwroot %{buildroot}/var/www/awstats/root
%{__cp} -a tools %{buildroot}/var/www/awstats/tools

rm -f %{buildroot}/var/www/awstats/root/cgi-bin/awstats.pl.configdir

%clean
%{__rm} -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc docs README.TXT
%config(noreplace) %{_sysconfdir}/httpd/conf.d/awstats.conf.dist
%config(noreplace) %{_sysconfdir}/awstats/awstats.conf
%config(noreplace) %{_sysconfdir}/cron.hourly/awstats
%{_sysconfdir}/awstats/awstats.model.conf
%{_localstatedir}/www/awstats
%{_localstatedir}/lib/awstats


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-6m)
- rebuild for new GCC 4.6

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0-5m)
- source was changed, please remove sra.rpm and build with --rmsrc option

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-4m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (7.0-3m)
- original source was changed

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-1m)
- [SECURITY] CVE-2010-4367 CVE-2010-4368 CVE-2010-4369
- update to 7.0

* Tue Dec  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.95-1m)
- update to 6.95

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-6m)
- [SECURITY] CVE-2008-5080
- update Patch0

* Wed May 13 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.9-5m)
- stop cron.hourly

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-4m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-3m)
- update to 6.9 - 2008-12-28 14:04
- change Source0 URI

* Thu Nov 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-2m)
- [SECURITY] CVE-2008-3714
- reupdate to 6.9
- update awstats-6.9-configdir.patch based on
  1002_disable_configdir.patch in Debian unstable (6.7.dfsg-5)

* Mon Oct 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.9-1m)
- update 6.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.7-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.7-2m)
- rebuild against perl-5.10.0-1m

* Tue Oct 15 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (6.7-1m)
- update to 6.7.

* Sun Dec 31 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.6-5m)
- tarball seems to have beey changed yet again.

* Wed Nov 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (6.6-4m)
- tarball seems to have been changed. (kaigyo-code?)

* Sat Jul 15 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (6.6-3m)
- modify awstats.conf

* Sat Jun 17 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.6-2m)
- [SECURITY] CVE-2006-2644 add patch0 (from debian)
- change md5sum of source0 (but same name...)

* Sun May  7 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.6-1m)
- update 6.6
- [SECURITY] http://www.osreviews.net/reviews/comm/awstats
- http://awstats.sourceforge.net/awstats_security_news.php

* Sun Apr 03 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (6.4-1m)
- update to 6.4
- [Security Fix] Multiple vulnerabilities (CAN-2005-0363,CAN-2005-0362,CAN-2005-0116)
- see http://www.st.ryukoku.ac.jp/~kjm/security/ml-archive/bugtraq/2005.02/msg00239.html

* Sun Jul 18 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (6.0-2m)
- stop service

* Sun Apr 18 2004  <yohgaki@momonga-linux.org> - 
- Initial build.
