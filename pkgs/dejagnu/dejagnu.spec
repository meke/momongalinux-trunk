%global momorel 1
Summary: A front end for testing other programs.
Name: dejagnu
Version: 1.5.1
Release: %{momorel}m%{?dist}
License: GPL
Source0: http://ftp.gnu.org/pub/gnu/dejagnu/%{name}-%{version}.tar.gz 
NoSource: 0
Patch1: dejagnu-1.5-smp-1.patch
Patch2: dejagnu-1.5-runtest.patch
Patch3: dejagnu-1.5-usrmove.patch
Patch4: dejagnu-1.5-gfortran.patch
Patch5: dejagnu-1.5-aarch64.patch
Group: Development/Tools
Requires: tcl >= 8.0, expect >= 5.21
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
BuildRequires: jadetex docbook-utils-pdf tetex-dvips

%description
DejaGnu is an Expect/Tcl based framework for testing other programs.
DejaGnu has several purposes: to make it easy to write tests for any
program; to allow you to write tests which will be portable to any
host or target where a program must be tested; and to standardize the
output format of all tests (making it easier to integrate the testing
into software development).

%prep
%setup -q -n dejagnu-%{version}
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1

%build
%configure -v

%check
echo ============TESTING===============
# Dejagnu test suite also has to test reporting to user.  It needs a
# terminal for that.  That doesn't compute in mock.  Work around it by
# running the test under screen and communicating back to test runner
# via temporary file.  If you have better idea, we accept patches.
TMP=`mktemp`
screen -D -m sh -c '(make check RUNTESTFLAGS="RUNTEST=`pwd`/runtest"; echo $?) >> '$TMP
RESULT=`tail -n 1 $TMP`
cat $TMP
rm -f $TMP
echo ============END TESTING===========
exit $RESULT

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_infodir}/dir
chmod a-x %{buildroot}%{_datadir}/dejagnu/runtest.exp
make DESTDIR=%{buildroot} install-man
install -D -m 644 doc/dejagnu.info %{buildroot}%{_infodir}/%{name}.info

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/%{name}.info.gz --dir-file=%{_infodir}/dir &> /dev/null
:

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir &> /dev/null
fi

%files
%defattr(-,root,root)
%doc COPYING NEWS README AUTHORS ChangeLog doc/dejagnu.texi
%{_bindir}/runtest
%{_datadir}/dejagnu
%{_includedir}/dejagnu.h
%{_mandir}/*/*
%{_infodir}/dejagnu*

%changelog
* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-2m)
- %%NoSource -> NoSource

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.4-1m)
- imported from fc1.
- evaluation please.

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Mon Dec 30 2002 Karsten Hopp <karsten@redhat.de> 1:1.4.2-9
- more missing BuildRequires

* Tue Dec 17 2002 Karsten Hopp <karsten@redhat.de> 1:1.4.2-8
- Add jadetex Buildrequires

* Wed Nov 27 2002 Tim Powers <timp@redhat.com> 1:1.4.2-7
- include dejagnu.h
- move %%{_libexecdir}/config.guess into %%{_datadir}/dejagnu 
- include overview docs (bug #59095)

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Apr 29 2002 Jakub Jelinek <jakub@redhat.com> 1.4.2-4
- fix makefile style variable passing (#63984)

* Thu Feb 28 2002 Jakub Jelinek <jakub@redhat.com> 1.4.2-3
- rebuild

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Nov 28 2001 Jakub Jelinek <jakub@redhat.com> 1.4.2-1
- update to 1.4.2, mainly so that it can be built with gcc3+

* Fri Sep  7 2001 Jakub Jelinek <jakub@redhat.com> 1.4.1-3
- make it noarch again

* Wed Aug 29 2001 Jakub Jelinek <jakub@redhat.com>
- fix a typo (#52404)

* Thu Jun 28 2001 Tim Powers <timp@redhat.com>
- rebuilt for the distro

* Tue Feb 27 2001 Tim Powers <timp@redhat.com>
- minor modifications to the spec file. Built for Powertools.
- added Epoch

* Wed Feb 21 2001 Rob Savoye <rob@welcomehome.org>
- Fixed Requires line, and changed the URL to the new ftp site.

* Sun Oct 31 1999 Rob Savoye <rob@welcomehome.org>
- updated to the latest snapshot
- added doc files
- added the site.exp config file

* Mon Jul 12 1999 Tim Powers <timp@redhat.com>
- updated to 19990628
- updated patches as needed
- added %defattr in files section

* Wed Mar 10 1999 Jeff Johnson <jbj@redhat.com>
- add alpha expect patch (#989)
- use %configure

* Thu Dec 17 1998 Jeff Johnson <jbj@redhat.com>
- Update to 19981215.

* Thu Nov 12 1998 Jeff Johnson <jbj@redhat.com>
- Update to 1998-10-29.

* Wed Jul  8 1998 Jeff Johnson <jbj@redhat.com>
- Update to 1998-05-28.

* Sun Feb  1 1998 Jeff Johnson <jbj@jbj.org>
- Create.
 
