%global momorel 5

Name:           min12xxw
Version:        0.0.9
Release:        %{momorel}m%{?dist}
Summary:        Converts PBM stream to Minolta printer language

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://www.hinterbergen.de/mala/min12xxw/
Source0:        http://www.hinterbergen.de/mala/min12xxw/min12xxw-0.0.9.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#Though it includes no translations, who this doesn't matter really for now.
#The translation of both the package and SPEC (Summary and %desciption) are
#considered a TODO
#BuildRequires:  gettext

%description
This is a filter to convert pbmraw data such as produced by ghostscript to
the printer language of Minolta 1[234]xx W printers. It is meant to be used
by the PostScript Description files of the drivers from the foomatic package.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/esc-m
%{_bindir}/min12xxw
%{_mandir}/man1/min12xxw.1.*
%doc AUTHORS ChangeLog NEWS README COPYING format.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.9-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.9-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0.9-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.0.9-3
- Autorebuild for GCC 4.3

* Fri Aug 3 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.0.9-2
- Modify the License tag in accordance with the new guidelines

* Wed Jun 6 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.0.9-1
- Initial package
