%global momorel 1
# The Python templates in /usr/share/anjuta/project can not be byte-compiled.
%global _python_bytecompile_errors_terminate_build 0

Name:           anjuta
Version:        3.10.2
Release: %{momorel}m%{?dist}
Summary:        GNOME IDE for various programming languages (including C/C++, Python, Vala and JavaScript)

Group:          Development/Tools
License:        GPLv2+
URL:            http://www.anjuta.org/
Source0:        http://download.gnome.org/sources/anjuta/3.10/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  autogen
BuildRequires:  desktop-file-utils
BuildRequires:  devhelp-devel >= 3.0.0
BuildRequires:  gettext
BuildRequires:  glade-devel
BuildRequires:  gnome-doc-utils
BuildRequires:  graphviz-devel
BuildRequires:  gtk-doc
BuildRequires:  gtksourceview3-devel >= 2.91.8
BuildRequires:  intltool
BuildRequires:  libgda4-devel >= 4.2.0
BuildRequires:  libgdl-devel >= 3.6.0
BuildRequires:  libuuid-devel
BuildRequires:  neon-devel
BuildRequires:  perl(Locale::gettext)
BuildRequires:  perl(XML::Parser)
BuildRequires:  python-devel
BuildRequires:  sqlite-devel
BuildRequires:  subversion-devel
BuildRequires:  vala-devel >= 0.24
BuildRequires:  vte3-devel
BuildRequires:  libxml2-devel
BuildRequires:  gobject-introspection-devel
BuildRequires:  flex
BuildRequires:  bison

Requires:       autogen
Requires:       gdb >= 7.0
Requires:       git
Requires:       sqlite-devel
Requires:       gnome-icon-theme
Requires:       hicolor-icon-theme
Requires:       libgda4 >= 4.2.0

# Obsolete the -doc subpackage and instead install the user help in main
# anjuta package so that About->User's Manual could work out of the box.
Obsoletes:      anjuta-docs < 1:3.3.92-1

%description
Anjuta DevStudio is a versatile software development studio featuring
a number of advanced programming facilities including project
management, application wizard, interactive debugger, source editor,
version control, GUI designer, profiler and many more tools. It
focuses on providing simple and usable user interface, yet powerful
for efficient development.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains development files for %{name}.


%prep
%setup -q -n %{name}-%{version}

# Suppress rpmlint error.
iconv --from-code ISO8859-1 --to-code UTF-8 ./THANKS \
  --output THANKS.utf-8 && mv THANKS.utf-8 ./THANKS

%build
%configure \
  --disable-schemas-compile \
  --disable-scrollkeeper \
  --disable-silent-rules \
  --disable-static \
  --disable-gtk-doc \
  --enable-introspection \
  --enable-plugin-devhelp \
  --enable-glade-catalog \
  --enable-plugin-subversion

# Omit unused direct shared library dependencies.
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool

%make 


%install
make install INSTALL="%{__install} -p" DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" -delete

# Use %%doc instead.
rm -rf %{buildroot}%{_docdir}/%{name}

%find_lang %{name} --all-name --with-gnome

desktop-file-validate %{buildroot}%{_datadir}/applications/anjuta.desktop


%post
/sbin/ldconfig

update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

touch --no-create %{_datadir}/icons/gnome &>/dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
/sbin/ldconfig

update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/gnome &>/dev/null || :
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%files -f %{name}.lang
%doc AUTHORS
%doc COPYING
%doc MAINTAINERS
%doc NEWS
%doc ROADMAP
%doc THANKS
%doc %{_mandir}/man1/%{name}.1*
%doc %{_mandir}/man1/%{name}-launcher.1*
%{_bindir}/%{name}
%{_bindir}/%{name}-launcher
%{_bindir}/%{name}-tags
%{_datadir}/applications/anjuta.desktop
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-x-%{name}.png
%{_datadir}/icons/gnome/22x22/mimetypes/gnome-mime-application-x-%{name}.png
%{_datadir}/icons/gnome/24x24/mimetypes/gnome-mime-application-x-%{name}.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-x-%{name}.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-x-%{name}.png
%{_datadir}/icons/gnome/scalable/mimetypes/gnome-mime-application-x-%{name}.svg
%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
%{_datadir}/icons/hicolor/24x24/apps/%{name}.png
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{_datadir}/icons/HighContrast/*/apps/%{name}.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/mime/packages/%{name}.xml
%{_libdir}/anjuta/
%{_libdir}/girepository-1.0/Anjuta-3.0.typelib
%{_libdir}/girepository-1.0/IAnjuta-3.0.typelib
%{_libdir}/glade/modules/libgladeanjuta.so
%{_libdir}/libanjuta-3.so.*
%{_datadir}/anjuta/
%{_datadir}/help/*/anjuta-*/
%{_datadir}/glib-2.0/schemas/org.gnome.anjuta*.gschema.xml
%{_datadir}/pixmaps/anjuta/
%{_datadir}/glade/catalogs/anjuta-glade.xml

%files devel
%doc doc/ScintillaDoc.html
%{_libdir}/libanjuta-3.so
%{_libdir}/pkgconfig/libanjuta-3.0.pc
%{_datadir}/gir-1.0/Anjuta-3.0.gir
%{_datadir}/gir-1.0/IAnjuta-3.0.gir

%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%dir %{_datadir}/gtk-doc/html/libanjuta
%doc %{_datadir}/gtk-doc/html/libanjuta/*

%dir %{_includedir}/libanjuta-3.0
%{_includedir}/libanjuta-3.0/libanjuta


%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.2-2m)
- rebuild against vala-0.24

* Sun Apr 13 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.10.2-1m)
- update to 3.10.2
- rebuild against libgda4-4.2.13-1m

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.6.1-3m)
- rebuild against graphviz-2.36.0-1m

* Sun Nov 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.1-2m)
- rebuild against libgdl-3.6.0

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5
- temporarily disable gtk-doc

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Sun Jul  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- fix up Obsoletes

* Sat Jul 07 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild against vala-0.17.2

* Thu Oct 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-2m)
- rebuild against vala-0.13

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3.0-1m)
- update to 3.0.3.0

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2.0-1m)
- update to 3.0.2.0

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1.0-2m)
- build with vte

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1.0-1m)
- update to 3.0.1.0

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.1.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1.1-3m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1.1-2m)
- rebuild without vala (cannot build with vala-0.12.0) 

* Fri Dec 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1.1-1m)
- update to 2.32.1.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1.0-2m)
- rebuild for new GCC 4.5

* Sun Nov 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1.0-1m)
- update to 2.32.1.0
-- gir does not be built

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0.0-2m)
- rebuild against webkitgtk-1.3.4

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0.0-1m)
- update to 2.32.0.0

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2.1-5m)
- rebuild against vala-0.9.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2.1-4m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.30.2.1-3m)
- modify BuildRequires: libgdl-devel

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.30.2.1-2m)
- rebuild against libproxy-0.4.4

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2.1-1m)
- update to 2.30.2.1

* Sun Jun  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2.0-1m)
- update to 2.30.2.0

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.0-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor ||:

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.0-1m)
- update to 2.30.1.0

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0.0-1m)
- update to 2.30.0.0

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90.0-1m)
- update to 2.29.90.0

* Tue Feb  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2.0-1m)
- update to 2.28.2.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1.0-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1.0-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1.0-1m)
- update to 2.28.1.0

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0.0-2m)
- rebuild against devhelp-2.28.0

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0.0-1m)
- update to 2.28.0.0

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.26.2.2-3m)
- add BuildRequires: glade3-libgladeui-devel >= 3.6.7

* Mon Aug  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.2.2-2m)
- rebuild against subversion-1.6.3-2m

* Mon Jun 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2.2-1m)
- update to 2.26.2.2

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2.0-3m)
- rebuild against vte-0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2.0-2m)
- rebuild against vte-0.20.4

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2.0-1m)
- update to 2.26.2.0

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.26.1.0-2m)
- rebuild against graphviz-2.22.2

* Tue Apr 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1.0-1m)
- update to 2.26.1.0

* Tue Mar 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0.1-1m)
- update to 2.26.0.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.903.0-2m)
- rebuild against WebKit-1.1.1

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.903.0-1m)
- update to 2.25.903.0

* Fri Feb 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.902-1m)
- update to 2.25.902

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.901-3m)
- fix libgda4 version

* Sun Feb  8 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.25.901-2m)
- remove conflict files (gbf-am-parse, gbf-mkfile-parse)

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.901-1m)
- update to 2.25.901

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild against python-2.6.1-1m
- revive /etc/gconf/schemas/anjuta-valgrind.schemas 

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.2-4m)
- rebuild against db4-4.7.25-1m
- not provide /etc/gconf/schemas/anjuta-valgrind.schemas

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0.1-1m)
- update to 2.24.0.1

* Mon Jul  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-3m)
- rebuild against gnome-build

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-2m)
- rebuild against graphviz-2.18

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-3m)
- rebuild against gcc43

* Sat Mar 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-2m)
- disable gtk-doc on x86_64 for the moment
- fix "File listed twice"

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.3-2m)
- rebuild against openldap-2.4.8

* Mon Dec  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Tue Oct 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.1-3m)
- rebuild against db4-4.6.21

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-2m)
- rebuild against libwnck-2.20.0
- rebuild against glade3-3.4.0

* Sun Sep  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Fri Jul 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-4m)
- add --with-apr-config option to configure

* Thu Jul 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-3m)
- add -maxdepth 1 to del .la

* Sat Jun 30 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.0-2m)
- add BuildRequires gdl-devel >= 0.7.6, gnome-build-devel >= 0.1.7

* Sat Jun 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0 (stable rerease)

* Sat Jun 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Fri Feb 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.0-2m)
- disable glade3

* Tue Jan 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Mon Dec 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-15m)
- rebuild against graphviz-2.12-1m

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-14m)
- rebuild against binutils-2.17.50.0.8

* Sun Nov 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-13m)
- rebuild against db4-4.5.20-1m

* Wed Nov 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-12m)
- rebuild against graphiz-2.8-4m

* Sat Oct  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-11m)
- rebuild against binutils-2.17.50.0.3-3m

* Mon Sep 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-10m)
- rebuild against gdl-0.6.1-4m

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-9m)
- rebuild against vte-0.14.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.2-8m)
- rebuild against expat-2.0.0-1m

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-7m)
- delete libtool library

* Wed Jun 28 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-6m)
- rebuild against binutils-2.17

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-5m)
- rebuild against binutils-2.16.94

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-4m)
- delete duplicated dir and icon-theme.cache

* Wed Jun 01 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.2-3m)
- updated ja.po by the one at http://mikeforce.homelinux.org/

* Thu May 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-2m)
- delete duplicate dir

* Tue May 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Thu May  4 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.1-6m)
- use apr-1,apr-util-1

* Thu Apr 27 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.0.1-5m)
- rebuild against vte-0.12.1

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.1-4m)
- rebuild against httpd-2.0.55-3m

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.1-3m)
- rebuild against db4.3

* Wed Jan  3 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.1-2m)
- rebuild against neon-0.25.4

* Thu Jun 30 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Wed May 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Feb 14 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.2-5m)
- update libdir patch.

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.2-4m)
- run auto-brothers

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.2-3m)
- enable x86_64.

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.2-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sat May 8 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.2-1m)
- initial import
