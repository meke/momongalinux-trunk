%global momorel 3

Summary: transparent network access through a SOCKS 4 or 5 proxy
Name: tsocks
Version: 1.8beta5
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Internet
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/1.8%20beta%205/%{name}-%{version}.tar.gz
NoSource: 0
Url: http://tsocks.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1: tsocks-1.8-multilib.patch

%description 
tsocks provides transparent network access through a SOCKS version 4
or 5 proxy (usually on a firewall).

tsocks intercepts the calls applications make to establish TCP
connections and transparently proxies them as necessary. This allows
existing applications to use SOCKS without recompilation or
modification.

%prep
%setup -q -n tsocks-1.8

# fix multilib issue
%patch1 -p1 -b .multilib~
sed --in-place 's,@LIBDIR@,%{_libdir},g' tsocks
sed --in-place 's,@LIB@,%{_lib},g' tsocks

%build
%configure
%make

%install
%makeinstall
# prepare default config file
mkdir -p %{buildroot}%{_sysconfdir}
install -m644 tsocks.conf.complex.example %{buildroot}%{_sysconfdir}/tsocks.conf

%files
%defattr(-, root, root,-)
%doc COPYING ChangeLog FAQ tsocks.conf.*.example
%config(noreplace) %{_sysconfdir}/tsocks.conf
%{_bindir}/tsocks
%{_mandir}/man?/*
%{_libdir}/libtsocks.so*

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8beta5-3m)
- rebuild for new GCC 4.6

* Wed Apr 06 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8beta5-2m)
- add patch for x86_64

* Thu Jan  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8beta5-1m)
- initial import
