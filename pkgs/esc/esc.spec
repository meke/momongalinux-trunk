%global momorel 12

Name: esc 
Version: 1.1.0
Release: %{momorel}m%{?dist} 
Summary: Enterprise Security Client Smart Card Client
License: GPL
URL: http://directory.fedora.redhat.com/wiki/CoolKey 
Group: Applications/Internet

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch1: esc-1.1.0-fix1.patch
Patch2: esc-1.1.0-fix2.patch
Patch3: esc-1.1.0-fix3.patch
Patch4: esc-1.1.0-fix4.patch
Patch5: esc-1.1.0-fix5.patch
Patch6: esc-1.1.0-fix6.patch
Patch7: esc-1.1.0-fix7.patch
Patch8: esc-1.1.0-fix8.patch
Patch9: esc-1.1.0-fix9.patch

BuildRequires: doxygen fontconfig-devel freetype-devel >= 2.1
BuildRequires: glib2-devel libIDL-devel atk-devel gtk2-devel libjpeg-devel
BuildRequires: pango-devel libpng-devel pkgconfig zlib-devel
BuildRequires: nspr-devel nss-devel
BuildRequires: autoconf213 libX11-devel libXt-devel
BuildRequires: xulrunner xulrunner-devel = 5.0

BuildRequires: pcsc-lite-devel coolkey-devel
BuildRequires: desktop-file-utils zip binutils libnotify-devel >= 0.7.2
BuildRequires: dbus-devel
Requires: pcsc-lite coolkey nss nspr
Requires: zip dbus >= 0.90 libnotify >= 0.4.2
Requires: xulrunner

# 390 does not have coolkey or smartCards
ExcludeArch: s390 s390x 

# We can't allow the internal xulrunner to leak out
AutoReqProv: 0

#%define __prelink_undo_cmd %{nil}
%define escname %{name}-%{version}
%define escdir %{_libdir}/%{escname}
%define escbindir %{_bindir}
%define esc_chromepath   chrome/content/esc
%define appdir applications
%define icondir %{_datadir}/icons/hicolor/48x48/apps
%define esc_vendor esc 
%define autostartdir %{_sysconfdir}/xdg/autostart
%define pixmapdir  %{_datadir}/pixmaps
%define docdir    %{_defaultdocdir}/%{escname}
%define escappdir src/app/xpcom
%define escxuldir src/app/xul/esc
%define escxulchromeicons %{escxuldir}/chrome/icons/default
%define escdaemon escd


Source0: %{escname}.tar.bz2
Source1: esc
Source2: esc.desktop
Source3: esc.png


%description
Enterprise Security Client allows the user to enroll and manage their
cryptographic smartcards.

%prep

%setup -q -c -n %{escname}

#patch esc 

%patch1 -p1 -b .fix1
%patch2 -p1 -b .fix2
%patch3 -p1 -b .fix3
%patch4 -p1 -b .fix4
%patch5 -p1 -b .fix5
%patch6 -p1 -b .fix6
%patch7 -p1 -b .fix7
%patch8 -p1 -b .fix8
%patch9 -p1 -b .fix9

%build

GECKO_SDK_PATH=%{_libdir}/xulrunner-sdk-2/sdk
GECKO_BIN_PATH=%{_libdir}/xulrunner-2
GECKO_INCLUDE_PATH=%{_includedir}/xulrunner-sdk-2

%ifarch x86_64 ppc64 ia64
USE_64=1
export USE_64
%endif

export GECKO_SDK_PATH
export GECKO_BIN_PATH
export GECKO_INCLUDE_PATH
# last setup call moved  the current directory

cd esc 
#cd ../..

cp %{SOURCE3} %{escxuldir}/%{esc_chromepath}
rm -f %{escxulchromeicons}/*.ico
cp %{escxulchromeicons}/esc-window.xpm %{escxulchromeicons}/default.xpm

export LDFLAGS="`pkg-config x11 --libs`"
make BUILD_OPT=1 HAVE_LIB_NOTIFY=1 ESC_VERSION=%{version}-%{release} USE_XUL_SDK=1

%install

cd esc/src/app/xpcom 

mkdir -p $RPM_BUILD_ROOT/%{escbindir}
mkdir -p $RPM_BUILD_ROOT/%{icondir}
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/%{appdir}
mkdir -p $RPM_BUILD_ROOT/%{autostartdir}
mkdir -p $RPM_BUILD_ROOT/%{pixmapdir}
mkdir -p $RPM_BUILD_ROOT/%{docdir}


sed -e 's;\$LIBDIR;'%{_libdir}';g'  %{SOURCE1} > $RPM_BUILD_ROOT/%{escbindir}/%{name}


chmod 755 $RPM_BUILD_ROOT/%{escbindir}/esc

mkdir -p $RPM_BUILD_ROOT/%{escdir}

%ifarch x86_64 ppc64 ia64
USE_64=1
export USE_64
%endif


make BUILD_OPT=1 USE_XUL_SDK=1 install DESTDIR=$RPM_BUILD_ROOT/%{escdir}

rm -rf $RPM_BUILD_ROOT/%{escdir}/usr

cd ../../../dist/*OPT*/esc_build/esc

cp %{esc_chromepath}/esc.png $RPM_BUILD_ROOT/%{icondir}
ln -s $RPMBUILD_ROOT%{icondir}/esc.png $RPM_BUILD_ROOT/%{pixmapdir}/esc.png

cp %{SOURCE2} $RPM_BUILD_ROOT/%{_datadir}/%{appdir}
cp %{SOURCE2} $RPM_BUILD_ROOT/%{autostartdir}

cd %{_builddir}
cp %{escname}/esc/LICENSE $RPM_BUILD_ROOT/%{docdir}

chmod 755 -R $RPM_BUILD_ROOT/%{escdir}/chrome
chmod 755 -R $RPM_BUILD_ROOT/%{escdir}/defaults
chmod 755  $RPM_BUILD_ROOT/%{escdir}/application.ini


%clean

rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)

%{escdir}/esc
%{escdir}/escd
%{escbindir}/esc
%{escdir}/application.ini

%{escdir}/chrome.manifest
%{escdir}/chrome/chrome.manifest

%{escdir}/chrome/content
%{escdir}/chrome/locale
%{escdir}/chrome/icons/default
%{escdir}/components

%{escdir}/defaults/preferences/esc-prefs.js   

#%{escdir}/xulrunner
%{icondir}/esc.png
%{pixmapdir}/esc.png
%{autostartdir}/esc.desktop
%{_datadir}/%{appdir}/esc.desktop
%doc %{docdir}/LICENSE

%post
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%preun

killall --exact -q escd
exit 0

%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-12m)
- rebuild for glib 2.33.2

* Sun Jul  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-11m)
- rebuild with new xulrunner-5.0

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-10m)
- fix symlink of icon

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-9m)
- import fix9.patch from F15 to enable to build with xulrunner-2.x
- fix of new libnotify was included above patch

* Sun May  8 2011 Nishio Futosh <fotoshi@momonga-linux.org>
- (1.1.0-8m)
- rebuild against xultunner-2.0.1
-- but cannot build without nsIGenericFactory.h (No such file or directory)

* Sun May  8 2011 Nishio Futosh <fotoshi@momonga-linux.org>
- (1.1.0-7m)
- rebuild against libnotify-0.7.2
-- but cannot build without /usr/lib/xulrunner-1.9.2/xpidl (what's happen?)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-5m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-4m)
- fix build failure

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- full rebuild for mo7 release

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.oeg>
- (1.1.0-2m)
- fix esc.desktop
- Icon key 'esc.png'(should not include extension)

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-1m)
- sync with Fedora 13 (1.1.0-11)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora

* Wed Feb 13 2008 Jack Magne <jmagne@redhat.com>
- Fix xulrunner build problem.
* Fri Jan 18 2008 Jack Magne <jmagne@redhat.com> 
- Fix tray icon menu issue #253248.
* Thu Aug 30 2007 Jack Magne <jmagne@redhat.com> 
- License field change- 1.0.1-7

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 1.0.1-6
- Rebuild for selinux ppc32 issue.

* Tue Jul 17 2007 Jack Magne <jmagne@redhat.com>- 1.0.1-5
- Further fixes to the diagnostics logging.
* Wed Jun 20 2007 Jack Magne <jmagne@redhat.com>- 1.0.1-4
- Fixes to the diagnostics log files and esc  error messages.
* Thu Apr 26 2007 Jack Magne <jmagne@redhat.com>- 1.0.1-3
- Many UI usability fixes.
* Tue Apr 03 2007 Jack Magne <jmagne@redhat.com>- 1.0.1-2
* Mon Mar 05 2007 Jack Magne <jmagne@redhat.com>- 1.0.1-1
- Stability fixes
* Fri Oct 27 2006 Jack Magne <jmagne@redhat.com>- 1.0.0-19
- More mac and win fixes.
* Tue Oct 24 2006 Jack Magne <jmagne@redhat.com>- 1.0.0-18
-rebuilt on RHEL-5 branch
* Sun Oct 4  2006 Jack Magne <jmagne@redhat.com>- 1.0.0-17
- Diagnostics display fixes, Mac and Window fixes.

* Sun Oct 01 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-16
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Fri Sep 22 2006 Jack Magne <jmagne@redhat.com>- 1.0.0-15
- Fix to the build version

* Fri Sep 22 2006 Jack Magne <jmagne@redhat.com>= 1.0.0-14
- Fix to compile error in daemon

* Fri Sep 22 2006 Jack Magne <jmagne@redhat.com>- 1.0.0-13
- Fix to include the new esc daemon.  

* Sat Sep 16 2006 Jack Magne <jmagne@redhat.com>- 1.0.0-12
- Fix for Password Reset and minor UI revision.

* Fri Sep 15 2006 Jack Magne <jmagne@redhat.com>- 1.0.0-11
- Further UI enhancement bug fixes

* Thu Sep 7 2006 Jack Magne <jmagne@redhat.com>- 1.0.0-10
- Further strings revisions.

* Wed Aug 30 2006 Jack Magne <jmagne@redhat.com>-  1.0.0-9
- Revision of the strings used in ESC.

* Sat Aug 27 2006 Jack Magne <jmagne@redhat.com>-  1.0.0-8
- Fixes to get libnotify working properly on FC6 systems.

* Tue Aug 22 2006 Jack Magne <jmagne@redhat.com> - 1.0.0-7
- Fix for bug #203211, use of system NSS and NSPR for
- Xulrunner ,addressing the problem running on 64 bit.
- Overwriting 5 and 6 due to important bug #203211.

* Fri Aug  18 2006 Jack Magne <jmagne@redhat.com> - 1.0.0-6
- Correct problem with Patch #6

* Tue Aug  17 2006 Jack Magne <jmagne@redhat.com> - 1.0.0-5
- Build ESC's xulrunner component using system nss and nspr
- Build process creates run script based on {_libdir} variable,
  accounting for differences on 64 bit machines.
- UI enhancements

* Tue Aug  1 2006 Matthias Clasen <mclasen@redhat.com> - 1.0.0-4
- Don't auto-generate requires either

* Mon Jul 31 2006 Matthias Clasen <mclasen@redhat.com> - 1.0.0-3
- Don't provide mozilla libraries

* Fri Jul 28 2006 Ray Strode <rstrode@redhat.com> - 1.0.0-2
- remove bogus gtk+ requires (and some others that will
  be automatic)

* Tue Jun 13 2006 Jack Magne <jmagne@redhat.com> - 1.0.0-1
- Initial revision for fedora

