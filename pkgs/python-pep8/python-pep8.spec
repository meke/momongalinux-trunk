%global momorel 1
%if 0%{?fedora} > 12 || 0%{?rhel} > 6
%bcond_without python3
%else
%bcond_with python3
%endif

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global module_name pep8

Name:           python-%{module_name}
Version:        1.4.6
Release: %{momorel}m%{?dist}
Summary:        Python style guide checker

Group:          Development/Languages
# License is held in the comments of pep8.py
# setup.py claims license is Expat license, which is the same as MIT
License:        MIT
URL:            http://pypi.python.org/pypi/%{module_name}
Source0:        http://pypi.python.org/packages/source/p/%{module_name}/%{module_name}-%{version}.tar.gz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  python-sphinx
Requires:       python-setuptools

%description
pep8 is a tool to check your Python code against some of the style conventions
in PEP 8. It has a plugin architecture, making new checks easy, and its output
is parseable, making it easy to jump to an error location in your editor.

%if %{with python3}
%package -n python3-pep8
Summary:    Python style guide checker

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-sphinx
 
%description -n python3-pep8
pep8 is a tool to check your Python code against some of the style
conventions in PEP 8. It has a plugin architecture, making new checks
easy, and its output is parseable, making it easy to jump to an error
location in your editor.

This is a version for Python 3.

%endif


%prep
%setup -qn %{module_name}-%{version}
# Remove #! from pep8.py
sed --in-place "s:#!\s*/usr.*::" pep8.py

%if %{with python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif


%build
%{__python} setup.py build build_sphinx

%if %{with python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif

%install
rm -rf %{buildroot}
%if %{with python3}
pushd %{py3dir}
%{__python3} setup.py install -O1 --skip-build --root %{buildroot}
popd
%endif

%{__python} setup.py install -O1 --skip-build --root %{buildroot}

 

%check
python pep8.py --testsuite testsuite
python pep8.py --doctest

%if %{with python3}
pushd %{py3dir}
PYTHONPATH="%{buildroot}%{python3_sitelib}:$PYTHONPATH" %{__python3} pep8.py --testsuite testsuite
popd
%endif


%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.rst build/sphinx/html/*
%{_bindir}/pep8
%{python_sitelib}/%{module_name}.py*
%{python_sitelib}/%{module_name}-%{version}-*.egg-info

%if %{with python3}
%files -n python3-pep8
%doc README.rst CHANGES.txt build/sphinx/html/*
%{python3_sitelib}/%{module_name}.py*
%{python3_sitelib}/%{module_name}-%{version}-*.egg-info/
%{python3_sitelib}/__pycache__/%{module_name}*
%endif

%changelog
* Mon Sep 02 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-1m)
- import from fedora

