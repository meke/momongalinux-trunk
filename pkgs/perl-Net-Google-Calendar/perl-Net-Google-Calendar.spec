%global         momorel 3

Name:           perl-Net-Google-Calendar
Version:        1.05
Release:        %{momorel}m%{?dist}
Summary:        Programmatic access to Google's Calendar API
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-Google-Calendar/
Source0:        http://www.cpan.org/authors/id/P/PL/PLYTLE/Net-Google-Calendar-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-base
BuildRequires:  perl-Carp
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-Data-ICal
BuildRequires:  perl-Date-ICal
BuildRequires:  perl-DateTime
BuildRequires:  perl-DateTime-Event-Recurrence
BuildRequires:  perl-DateTime-Format-ICal
BuildRequires:  perl-Encode
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTTP-Cookies
BuildRequires:  perl-HTTP-Message
BuildRequires:  perl-lib
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-Net-Google-AuthSub
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-URI
BuildRequires:  perl-XML-Atom
Requires:       perl-base
Requires:       perl-Carp
Requires:       perl-Data-Dumper
Requires:       perl-Data-ICal
Requires:       perl-Date-ICal
Requires:       perl-DateTime
Requires:       perl-DateTime-Event-Recurrence
Requires:       perl-DateTime-Format-ICal
Requires:       perl-Encode
Requires:       perl-HTTP-Cookies
Requires:       perl-HTTP-Message
Requires:       perl-libwww-perl
Requires:       perl-Net-Google-AuthSub
Requires:       perl-URI
Requires:       perl-XML-Atom
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Interact with Google's new calendar using the GData API.

%prep
%setup -q -n Net-Google-Calendar-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE README TODO USAGE
%{_bindir}/google-calendar
%{perl_vendorlib}/Net/Google/Calendar*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-2m)
- rebuild against perl-5.18.2

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-2m)
- rebuild against perl-5.16.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.00-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.00-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.00-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.0

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-2m)
- rebuild against perl-5.12.0

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-2m)
- rebuild against perl-5.10.1

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.95-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94
- add BuildRequires: perl-Net-Google-AuthSub

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8-2m)
- rebuild against gcc43

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
