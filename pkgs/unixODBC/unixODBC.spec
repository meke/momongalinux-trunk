%global momorel 7
%global qtver 3.3.7
%global qtdir %{_libdir}/qt-%{qtver}

Summary: A complete ODBC driver manager for Linux
Name: unixODBC
Version: 2.2.14
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: System Environment/Libraries
URL: http://www.unixODBC.org/

Source: http://www.unixODBC.org/%{name}-%{version}.tar.gz
Source1: odbcinst.ini
Source2: ODBCConfig.desktop
Source3: DataManager.desktop
Source4: conffile.h
Patch1: depcomp.patch
Patch2: multilib-config.patch
Patch3: warning-cleanup.patch
Patch6: export-symbols.patch
Patch7: libtool-config.patch
Patch8: so-version-bump.patch
Patch9: keep-typedefs.patch
Patch10: odbcint64-config.patch

Conflicts: iodbc
BuildRequires: libX11-devel libXt-devel libXext-devel
BuildRequires: kdelibs3-devel qt3-devel libmng-devel readline-devel
BuildRequires: automake autoconf libtool libtool-ltdl-devel bison flex
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%ifarch x86_64 ppc64 ia64 sparc64
Provides: libodbc.so()(64bit) libodbcinst.so()(64bit)
%else
Provides: libodbc.so libodbcinst.so
%endif

%description
Install unixODBC if you want to access databases through ODBC.
You will also need the mysql-connector-odbc package if you want to access
a MySQL database, and/or the postgresql-odbc package for PostgreSQL.

%package devel
Summary: Development files for programs which will use the unixODBC library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The unixODBC package can be used to access databases through ODBC
drivers. If you want to develop programs that will access data through
ODBC, you need to install this package.

%package kde
Summary: KDE driver manager components for ODBC
Group: System Environment/Libraries
Requires: qt3 >= %{qtver}  %{name} = %{version}-%{release}

%description kde
This package contains components for the ODBCConfig and DataManager
(KDE) components of unixODBC.

%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1 -b .odbcint64-config~

# Blow away the embedded libtool and replace with build system's libtool.
# (We will use the installed libtool anyway, but this makes sure they match.)
rm -rf config.guess config.sub install-sh ltmain.sh libltdl
# this hack is so we can build with either libtool 2.2 or 1.5
libtoolize --install || libtoolize

%build
# pick up qt path
export QTDIR=
. /etc/profile.d/qt.sh
# clean up old moc files
(cd ODBCConfig && rm -f mclass*.cpp)
(cd DataManager && rm -f mclass*.cpp)

aclocal
automake --add-missing
autoconf

# unixODBC 2.2.14 is not aliasing-safe
CFLAGS="%{optflags} -fno-strict-aliasing"
CXXFLAGS="$CFLAGS"
export CFLAGS CXXFLAGS

%configure  \
    --with-gnu-ld=yes  \
    --enable-threads=yes \
    --enable-gui=yes \
    --enable-drivers \
    --enable-ltdllib \
    --with-qt-dir="%{qtdir}" \
    --with-qt-includes="%{qtdir}/include" \
    --with-qt-libraries="%{qtdir}/lib"
make all

%install
# pick up qt path
export QTDIR=
. /etc/profile.d/qt.sh

rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/applications/kde
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/32x32/apps
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/16x16/apps

%makeinstall
install -m644 %{SOURCE1} %{buildroot}%{_sysconfdir}
install -m644 %{SOURCE2} %{buildroot}%{_datadir}/applications/kde/
install -m644 %{SOURCE3} %{buildroot}%{_datadir}/applications/kde/
cp DataManagerII/LinuxODBC.xpm %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/
cp DataManagerII/ODBC.xpm %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/odbc.xpm

# multilib header hacks
# we only apply this to known Red Hat multilib arches, per bug #181335
case `uname -i` in
  i386 | x86_64 | ppc | ppc64 | s390 | s390x | sparc | sparcv9 | sparc64 )
    mv %{buildroot}/usr/include/unixodbc_conf.h %{buildroot}/usr/include/unixodbc_conf_`uname -i`.h
    rm -f unixodbc_conf.h
    sed s/CONFFILE/unixodbc_conf/ %{SOURCE4} >unixodbc_conf.h
    install -m 644 unixodbc_conf.h %{buildroot}/usr/include/
    ;;
  *)
    ;;
esac

# remove obsolete Postgres drivers from the package (but not the setup code)
rm -f %{buildroot}%{_libdir}/libodbcpsql.so*

# copy text driver documentation into main doc directory
# currently disabled because upstream no longer includes text driver
# mkdir -p doc/Drivers/txt
# cp -pr Drivers/txt/doc/* doc/Drivers/txt

# don't want to install doc Makefiles as docs
find doc -name 'Makefile*' | xargs rm

# we do not want to ship static libraries
rm -f %{buildroot}%{_libdir}/*.a

# remove unpackaged files from the buildroot
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/libltdl.*
rm -rf %{buildroot}%{_datadir}/libtool

# initialize lists of .so files; note that libodbcinstQ* go into kde subpkg
find %{buildroot}%{_libdir} -name "*.so.*" | sed "s|^%{buildroot}||" | grep -v /libodbcinstQ > base-so-list
find %{buildroot}%{_libdir} -name "*.so"   | sed "s|^%{buildroot}||" | grep -v /libodbcinstQ > devel-so-list

# move these to main package, they're often dlopened...
for lib in libodbc.so libodbcinst.so libodbcpsqlS.so libodbcmyS.so
do
    echo "%{_libdir}/$lib" >> base-so-list
    grep -v "/$lib$" devel-so-list > devel-so-list.x
    mv -f devel-so-list.x devel-so-list
done

%clean
rm -rf %{buildroot}

%files -f base-so-list
%defattr(-,root,root)
%doc README COPYING AUTHORS ChangeLog NEWS INSTALL doc
%config(noreplace) %{_sysconfdir}/odbc*
%{_bindir}/odbcinst
%{_bindir}/isql
%{_bindir}/dltest
%{_bindir}/iusql
%{_bindir}/odbc_config

%files devel -f devel-so-list
%defattr(-,root,root)
%{_includedir}/*
#%%{_libdir}/*.a

%files kde
%defattr(-,root,root)
%{_bindir}/ODBCConfig
%{_bindir}/DataManager
%{_bindir}/DataManagerII
%{_bindir}/odbctest
%{_libdir}/libodbcinstQ*so
%{_libdir}/libodbcinstQ*so.*
%{_datadir}/applications/kde/ODBCConfig.desktop
%{_datadir}/applications/kde/DataManager.desktop
%{_datadir}/icons/hicolor/32x32/apps/LinuxODBC.xpm
%{_datadir}/icons/hicolor/16x16/apps/odbc.xpm

%post kde -p /sbin/ldconfig
%postun kde -p /sbin/ldconfig

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.14-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.14-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.14-5m)
- add patch for gcc45

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.14-4m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.14-3m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.14-1m)
- update to 2.2.14 based on Fedora 11 (2.2.14-2) especially libtool22

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.12-5m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.12-4m)
- update Patch6 for fuzz=0
- License: GPLv2+ and LGPLv2+

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.12-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.12-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.12-1m)
- update to 2.2.12
- remove unixODBC-config.patch, unixODBC-2.2.11-symbols.patch
- cleanup spec

* Sat Oct 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.11-5m)
- specify automake version 1.9

* Sun Jul 16 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.11-4m)
- modify spec for 64bit arch

* Fri Jul  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.11-3m)
- update desktop files of unixODBC-kde

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.11-2m)
- import patch from fc
- rebuild against readline
- Provides: libodbc.so libodbcinst.so

* Mon May 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.2.9-1m)
- version up. a lot of fixes.

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.5-5m)
- rebuild against ncurses 5.3.

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.5-4m)
- revised spec for rpm 4.2.

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.5-3m)
- fix specopt(thanks Ichiro Nakai<i-nakai__at__gw2.gateway.ne.jp>)
- clean up

* Wed Mar 19 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.5-2m)
- add specopt build_kdetool that choose build kdetool

* Sat Mar  8 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.5-1m)
- version 2.2.5

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.7-22k)
- rebuild against libpng 1.2.2.

* Mon Mar 25 2002 Toru Hoshina <t@kondara.org>
- (2.0.7-20k)
- rebuild against qt2 2.3.1 :-P

* Tue Nov 28 2001 Toru Hoshina <t@kondara.org>
- (2.0.7-18k)
- rebuild against qt 2.3.2.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (2.0.7-16k)
- rebuild against libpng 1.2.0.

* Thu Aug 23 2001 Toru Hoshina <toru@df-usa.com>
- (2.0.7-2k)
- rebuild against KDE 2.2, Qt-2.3.1, version up.

* Sun Aug 13 2001 Toru Hoshina <toru@df-usa.com>
- (1.8.13-14k)
- rebuild against qt-2.3.1.

* Wed Apr 25 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.8.13-10k)
- add %defattr to kde %files section

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (1.8.13-8k)
- add new package unixODBC-kde, including DataManager and ODBCConfig.

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.8.13-6k)
- rebuild against qt-2.3.0.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [1.8.13-5k]
- rebuild against qt-2.2.3.

* Sat Nov 25 2000 Kenichi Matsubara <m@kondara.org>
- update to 1.8.13.
- change new create unixODBC-devel package.

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Jan 30 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)

* Tue Jan 11 2000 ichiyama ryoichi <ichiyama@jc4.so-net.ne.jp>
- Upgrade to 1.8.3 and install Documents

* Wed Dec 1 1999 ShigeYuki TAKANO <j@kondara.org>
- add install.patch

* Sun Nov 28 1999 ShigeYuki TAKANO <j@kondara.org>
- Upgrade to 1.8.1

* Sat Oct 2 1999 ichiyama ryoichi <ichiyama@jc4.so-net.ne.jp>
- Append "%defattr" description.
