%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-paste
Version:        1.7.4
Release:        %{momorel}m%{?dist}
Summary:        Tools for using a Web Server Gateway Interface stack
Group:          System Environment/Libraries
License:        MIT
URL:            http://pythonpaste.org
Source0:        http://cheeseshop.python.org/packages/source/P/Paste/Paste-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel >= 0.6c9-2m


%description
These provide several pieces of "middleware" (or filters) that can be nested
to build web applications.  Each piece of middleware uses the WSGI (PEP 333)
interface, and should be compatible with other middleware based on those
interfaces.


%prep
%setup -q -n Paste-%{version}
# Strip #! lines that make these seeme like scripts
%{__sed} -i -e '/^#!.*/,1 d' paste/util/scgiserver.py paste/debug/doctest_webapp.py

# clean docs directory
pushd docs
rm StyleGuide.txt
popd


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc docs/*
%{python_sitelib}/*


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.4-2m)
- full rebuild for mo7 release

* Sat Jun 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.4-1m)
- [SECURITY] CVE-2010-2477
- update to 1.7.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.2-2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.2-1m)
- update to 1.7.2

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6-2m)
- rebuild against python-2.6.1

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6-1m)
- version up 1.6
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.1-1m)
- import from Fedora

* Sat Mar  3 2007 Luke Macken <lmacken@redhat.com> - 1.2.1-1
- 1.2.1

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> - 1.0-2
- Add python-devel to BuildRequires
- 1.0

* Sun Sep 17 2006 Luke Macken <lmacken@redhat.com> - 0.9.8.1-1
- 0.9.8.1

* Sun Sep  3 2006 Luke Macken <lmacken@redhat.com> - 0.9.3-5
- Rebuild for FC6

* Wed Jul 19 2006 Luke Macken <lmacken@redhat.com> - 0.9.3-4
- Use a smarter shebang removal expression

* Wed Jul 19 2006 Luke Macken <lmacken@redhat.com> - 0.9.3-3
- Fix doc inclusion

* Sat Jul 15 2006 Luke Macken <lmacken@redhat.com> - 0.9.3-2
- Clean up docs directory
- Remove shebang from from non-executable scripts
- Use consistent build root variables

* Mon Jul 10 2006 Luke Macken <lmacken@redhat.com> - 0.9.3-1
- Initial package
