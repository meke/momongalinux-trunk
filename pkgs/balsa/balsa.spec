%global momorel 1
%define config_opts --prefix=%{_prefix} --sysconfdir=%{_sysconfdir} --mandir=%{_mandir} --libdir=%{_libdir} --bindir=%{_bindir} --includedir=%{_includedir} --datadir=%{_datadir} --disable-more-warnings --with-ssl --with-gss --with-gtkspell=no  --with-unique --with-gmime=2.6 --without-gnome --with-html-widget=webkit --with-gpgme

Name:           balsa
Version:        2.5.1
Release: %{momorel}m%{?dist}
Summary:        Mail Client

Group:          Applications/Internet
License:        GPLv2+
URL:            http://pawsa.fedorapeople.org/balsa/
Source0:        http://pawsa.fedorapeople.org/balsa/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: libesmtp >= 1.0.6
Requires: scrollkeeper
BuildRequires: NetworkManager-glib-devel
BuildRequires: desktop-file-utils
BuildRequires: enchant-devel
BuildRequires: gettext
BuildRequires: gmime-devel >= 2.5.1
BuildRequires: gnome-doc-utils
#BuildRequires: gnome-vfs2-devel
BuildRequires: gpgme-devel
BuildRequires: webkitgtk-devel
BuildRequires: gtkspell-devel
BuildRequires: intltool
BuildRequires: libesmtp-devel >= 1.0.6
BuildRequires: libgnome-devel
BuildRequires: libgnomeui-devel
BuildRequires: libnotify-devel
BuildRequires: libtool
BuildRequires: openssl-devel
BuildRequires: scrollkeeper
BuildRequires: unique-devel

%description
Balsa is a GNOME email client which supports mbox, maildir, and mh
local mailboxes, and IMAP4 and POP3 remote mailboxes. Email can be
sent via sendmail or SMTP. Optional multithreading support allows for
non-intrusive retrieval and sending of mail. A finished GUI similar to
that of the Eudora email client supports viewing images inline, saving
message parts, viewing headers, adding attachments, moving messages,
and printing messages.

%prep
%setup -q
perl -pi -e 's,gnome-icon-theme,,g;' configure.in

%build

autoreconf -f -i
%configure %{config_opts}

%make 


%install
rm -rf "%{buildroot}"
make install DESTDIR="%{buildroot}"

desktop-file-install %{buildroot}%{_datadir}/applications/balsa.desktop \
        --vendor= \
        --add-category=Email \
	--remove-category=Application \
        --dir=%{buildroot}%{_datadir}/applications \
        --copy-name-to-generic-name \
        --delete-original

%find_lang %{name}

%clean
rm -rf "%{buildroot}"

%post
scrollkeeper-update -q -o %{_datadir}/omf/%{name} || :

/bin/touch --no-create %{_datadir}/icons/hicolor
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
scrollkeeper-update -q || :

touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README COPYING ChangeLog NEWS TODO AUTHORS HACKING docs/mh-mail-HOWTO
%doc docs/vconvert.awk docs/pine2vcard
%{_bindir}/balsa
%{_bindir}/balsa-ab
%{_datadir}/applications/*.desktop
%{_datadir}/balsa
%{_datadir}/help/*/balsa
%{_datadir}/icons/hicolor/*
%{_datadir}/pixmaps/gnome-balsa2.png
%{_datadir}/sounds/balsa
%{_mandir}/man1/balsa.1*
%config(noreplace) %{_sysconfdir}/sound/events/balsa.soundlist


%changelog
* Mon Apr 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-2m)
- remove fedora from vendor (desktop-file-install)

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0
- disable gtkspell support (needs gtkspell3)

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.13-1m)
- update to 2.4.13

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.12-1m)
- reimport from fedora

* Wed Nov 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-1m)
- update to 2.4.11

* Mon Jun 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.10-1m)
- update to 2.4.10

* Sun May  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-5m)
- rebuild against NetworkManager-0.8.999

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.9-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.9-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.9-2m)
- rebuild for new GCC 4.5

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9-1m)
- update to 2.4.9

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-1m)
- update to 2.4.8

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.7-5m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.7-4m)
- full rebuild for mo7 release

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.7-3m)
- rebuild against NetworkManager-0.8

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.7-2m)
- rebuild against openssl-1.0.0

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.7-1m)
- update to 2.4.7

* Wed Jan 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5

* Sat Dec 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1
- change primary site and source URI

* Fri Aug 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.28-3m)
- rebuild against gmime22

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.28-2m)
- rebuild against openssl-0.9.8k

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.28-1m)
- update to 2.3.28

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.26-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.26-4m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Sat Oct 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.26-3m)
- apply gtk 2.14 patch
-- http://mail.gnome.org/archives/svn-commits-list/2008-September/msg03041.html
- please ignore 2.3.26-2m

* Mon Sep 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.26-1m)
- update to 2.3.26

* Sun Aug 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.25-1m)
- update to 2.3.25

* Sat Jun 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.24-1m)
- update to 2.3.24

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.23-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.23-2m)
- rebuild against gcc43

* Sat Mar 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.23-1m)
- update to 2.3.23

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.22-1m)
- update to 2.3.22

* Fri Dec 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.21-1m)
- update to 2.3.21

* Thu Sep 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.20-1m)
- update to 2.3.20
- [SECURITY] CVE-2007-5007, fixes a buffer overflow in the IMAP code in this version
- add Requires: gtk2 >= 2.4.0
- do not use %%NoSource macro

* Mon Aug 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.19-1m)
- update to 2.3.19

* Fri Aug 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.18-1m)
- update to 2.3.18

* Sun Jul  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.17-2m)
- missing back slash in %%build section

* Fri Jul  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.17-1m)
- add BuildPreReq: gmime-devel to use installed gmime
- clean up spec file

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.16-1m)
- update balsa to 2.3.16
- update gmime to 2.2.9

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.14-2m)
- rebuild against gtkhtml3-3.14.0

* Sat Dec 23 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.14-1m)
- update balsa to 2.3.14
- update gmime to 2.2.3

* Sat Sep  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.3.13-1m)
- update to 2.3.13

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.3.12-3m)
- rebuild against expat-2.0.0-1m

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.12-2m)
- revise %%files for rpm-4.4.2

* Sat Apr 15 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.3.12-1m)
- update to 2.3.12

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.6-2m)
- rebuild against openssl-0.9.8a

* Wed Nov 23 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.3.6-1m)
- update to 2.3.6

* Tue May 24 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.6-1m)
- update to 2.2.6

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (2.2.4-1m)
- ver up. Sync with FC3(2.2.4-1.FC3.1).

* Wed Jul 07 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.18-1m)
- update to 2.0.18

* Tue May  4 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.0.17-1m)
- update to 2.0.17
- remove patch0
- add patch1

* Sun Apr 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.16-5m)
- add patch0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.0.16-4m)
- rebuild against for libxml2-2.6.8

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.16-2m)
- revised spec for enabling rpm 4.2.

* Mon Jan 19 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.0.16-1m)
- update to 2.0.16

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.11-2m)
- s/Copyright:/License:/

* Thu Jun 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.11-1m)
- version 2.0.11

* Thu Jun 5  2003 Masahiro Takahata <takahata@momonga-linuz.org>
- (2.0.9-4m)
- add BuildPrereq: libgnomeprint >= 2.1.4, libgnomeprintui >= 2.1.4

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.9-3m)
- rebuild against for openssl-0.9.7

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.9-2m)
- rebuild against for XFree86-4.3.0

* Fri Feb 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.9-1m)
- version 2.0.9

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.7-1m)
- version 2.0.7

* Wed Nov 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-3m)
- libgnomeprint,libgnomeprintui 2.1

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-2m)
- remove omf

* Mon Sep 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Wed Jul 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- test
