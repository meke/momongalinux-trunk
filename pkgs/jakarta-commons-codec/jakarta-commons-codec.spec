%global momorel 4

%global short_name commons-codec

# enable OSGi automatic dep solving
#%%define _use_internal_dependency_generator 0
#%%define __find_provides /usr/lib/rpm/osgideps.pl -p
#%%define __find_requires /usr/lib/rpm/osgideps.pl -r

# TODO - Drop "jakarta-" from the package name
Name:          jakarta-%{short_name}
Version:       1.4
Release:       %{momorel}m%{?dist}
Summary:       Implementations of common encoders and decoders
Group:         Development/Languages
License:       "ASL 2.0"
URL:           http://commons.apache.org/codec/

Source0:       http://archive.apache.org/dist/commons/codec/source/%{short_name}-%{version}-src.tar.gz

BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:     noarch

BuildRequires: java-devel >= 0:1.6.0
BuildRequires: jpackage-utils
BuildRequires: maven2-plugin-antrun
BuildRequires: maven2-plugin-assembly
BuildRequires: maven2-plugin-compiler
BuildRequires: maven2-plugin-idea
BuildRequires: maven2-plugin-install
BuildRequires: maven2-plugin-jar
BuildRequires: maven2-plugin-javadoc
BuildRequires: maven2-plugin-resources
BuildRequires: maven-doxia-sitetools
BuildRequires: maven-plugin-bundle
BuildRequires: maven-surefire-maven-plugin
BuildRequires: maven-surefire-provider-junit
Requires:      java >= 0:1.6.0
Requires:      jpackage-utils

Provides:      %{short_name} = %{version}-%{release}
Obsoletes:     %{short_name} < %{version}-%{release}

%description
Commons Codec is an attempt to provide definitive implementations of
commonly used encoders and decoders. Examples include Base64, Hex,
Phonetic and URLs.

%package javadoc
Summary:       Javadocs for %{name}
Group:         Documentation
Requires:      %{name} = %{version}-%{release}

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n %{short_name}-%{version}-src

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp -Dmaven.repo.local=$MAVEN_REPO_LOCAL install javadoc:javadoc

%install
rm -rf %{buildroot}

# jars
install -pD -T target/%{short_name}-%{version}.jar \
  %{buildroot}%{_javadir}/%{short_name}-%{version}.jar
(cd %{buildroot}%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|%{short_name}|%{name}|g"`; done)
(cd %{buildroot}%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# javadocs
mkdir -p %{buildroot}%{_javadocdir}/%{name}-%{version}
cp -pr target/site/apidocs/* %{buildroot}%{_javadocdir}/%{name}-%{version}
(cd %{buildroot}%{_javadocdir} && ln -sf %{name}-%{version} %{name})

# pom
install -pD -T -m 644 pom.xml %{buildroot}%{_mavenpomdir}/JPP-%{short_name}.pom
%add_to_maven_depmap %{short_name} %{short_name} %{version} JPP %{short_name}

%clean
rm -rf %{buildroot}

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%doc LICENSE.txt NOTICE.txt RELEASE-NOTES*
%{_mavendepmapfragdir}/*
%{_mavenpomdir}/*
%{_javadir}/*

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-2m)
- full rebuild for mo7 release

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-1m)
- sync with Fedora (1.4-5)
- disable OSGi dependencies

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-9jpp.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-9jpp.2m)
- rebuild against rpm-4.6

* Tue May  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3-9jpp.1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-7jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-7jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3-7jpp.1m)
- import from Fedora

* Sun Oct 01 2006 Jesse Keating <jkeating@redhat.com> 0:1.3-7jpp.2
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Tue Sep 26 2006 Matt Wringe <mwringe at redhat.com> 0:1.3-7jpp.1
- Merge with upstream version.

* Tue Sep 26 2006 Matt Wringe <mwringe at redhat.com> 0:1.3-7jpp
- Add missing java-javadoc requires and buildrequires.

* Mon Sep 25 2006 Matt Wringe <mwringe at redhat.com> 0:1.3-6jpp.1
- Merge with upstream version.

* Mon Sep 25 2006 Matt Wringe <mwringe at redhat.com> 0:1.3-6jpp
- Update jakarta-commons-codec-1.3-buildscript.patch to build
  offline.

* Thu Aug 10 2006 Matt Wringe <mwringe at redhat.com> 0:1.3-5jpp.1
- Merge with upstream version
 - Add missing javadoc requires

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:1.3-4jpp_2fc
- Rebuilt

* Thu Jul 20 2006 Matt Wringe <mwringe at redhat.com> 0:1.3-4jpp_1fc
- Merged with upstream version
- Now is natively compiled

* Thu Jul 20 2006 Matt Wringe <mwringe at redhat.com> 0:1.3-4jpp
- Added conditional native compiling

* Tue Apr 04 2006 Ralph Apel <r.apel@r-apel.de> 0:1.3-3jpp
- First JPP-1.7 release

* Wed Sep 08 2004 Fernando Nasser <fnasser@redhat.com> 0:1.3-2jpp
- Do not stop on test failure

* Tue Sep 07 2004 Fernando Nasser <fnasser@redhat.com> 0:1.3-1jpp
- Upgrade to 1.3
- Rebuilt with Ant 1.6.2

* Thu Jan 22 2004 David Walluck <david@anti-microsoft.org> 0:1.2-1jpp
- 1.2
- use perl instead of patch

* Wed May 28 2003 Ville Skytt? <jpackage-discuss at zarb.org> - 0:1.1-1jpp
- First JPackage release.
