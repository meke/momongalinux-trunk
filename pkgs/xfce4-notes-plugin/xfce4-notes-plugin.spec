%global momorel 2

%global xfce4ver 4.10.0
%global major 1.7

Name: 		xfce4-notes-plugin
Version: 	1.7.7
Release:	%{momorel}m%{?dist}
Summary: 	XFce4 sticky notes plugin

Group: 		User Interface/Desktops
License:  	GPLv2+
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext, intltool, desktop-file-utils
BuildRequires:  gtk2-devel
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libxfce4util-devel >= %{xfce4ver}
BuildRequires:  libxml2-devel
BuildRequires:  pkgconfig
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}
Requires:	xfconf >= %{xfce4ver}

%description 
This plugin provides sticky notes for your desktop. You can create a note by 
clicking on the customizable icon with the middle button of your mouse, 
show/hide the notes using the left one, edit the titlebar, change the notes 
background color and much more.

%prep
%setup -q 

%build
%configure --disable-static LIBS="-lm -lX11"
V=1 %make


%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete
%find_lang %{name}

desktop-file-validate \
    $RPM_BUILD_ROOT/%{_sysconfdir}/xdg/autostart/xfce4-notes-autostart.desktop
desktop-file-validate \
    $RPM_BUILD_ROOT/%{_datadir}/applications/xfce4-notes.desktop


%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%config(noreplace) %{_sysconfdir}/xdg/autostart/xfce4-notes-autostart.desktop
%{_bindir}/xfce4-notes
%{_bindir}/xfce4-notes-settings
%{_bindir}/xfce4-popup-notes
%{_libdir}/xfce4/panel-plugins/*.so*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/locale/*/*/*
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_datadir}/%{name}/


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.7.7-2m)
- rebuild against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.7-1m)
- update
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.6-2m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.6-1m)
- update to 1.7.6

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.2-1m)
- rebuild against xfce4-4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.4-5m)
- fix build

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-2m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-1m)
- update
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-2m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-1m)
- rebuild against xfce4 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.1-2m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m
-- libxfce4util-4.2.3.2-2m

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.11.1-1m)
- updaate to 0.11.1

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-3m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-2m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.7-8m)
- rebuild against xfce4 4.1.99.2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-7m)
- rebuild against xfce4 4.1.90

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.7-6m)
- rebuild against xfce4 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.9.7-5m)
- rebuild against for libxml2-2.6.8

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9.7-4m)
- revised spec for rpm 4.2.

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-3m)
- rebuild against xfce4 4.0.3

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-2m)
- rebuild against xfce4-4.0.1

* Wed Oct 29 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.9.7-1m)
- version 0.9.7

* Wed Oct 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.9.6-1m)
- first import to Momonga
