%global momorel 2
Name: subversion
%{?include_specopt}
## Configuration
# set to zero to avoid running test suite
%{?!make_check: %global make_check 0}

%{?!with_java: %global with_java 1} 

# set JDK path to build javahl; we use java-1.6.0-openjdk
%ifarch x86_64 ppc64 sparc64
%global archdep 1
%endif
%global jdk_path /usr/lib/jvm/java-1.6.0-openjdk%{?archdep:.%{_arch}}

%global perl_vendorarch %(eval "`%{__perl} -V:installvendorarch`"; echo $installvendorarch)

%{!?ruby_sitearch: %global ruby_sitearch %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["sitearchdir"]')}

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: Modern Version Control System designed to replace CVS
Version: 1.8.9
Release: %{momorel}m%{?dist} 
License: Apache
Group: Development/Tools
URL: http://subversion.apache.org/
Source0: http://www.apache.org/dist/subversion/subversion-%{version}.tar.bz2
NoSource: 0
Source1: subversion.conf
Source3: filter-requires.sh
# Source4: http://www.xsteve.at/prg/emacs/psvn.el
Source10: svnserve.sh
Source11: pre-svnserve

Patch1: subversion-1.8.0-rpath.patch
Patch2: subversion-1.8.0-pie.patch
Patch3: subversion-1.8.0-kwallet.patch
Patch4: subversion-1.8.0-rubybind.patch
Patch5: subversion-1.8.0-aarch64.patch
Patch8: subversion-1.8.5-swigplWall.patch

BuildRequires: autoconf, libtool, python, python-devel >= 2.7, texinfo, which
BuildRequires: libdb-devel >= 5.3.15, swig, gettext
BuildRequires: apr-devel >= 1.3.8, apr-util-devel >= 1.4.1-2m
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: sqlite-devel >= 3.7.11
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: libproxy-devel >= 0.4.4
BuildRequires: libserf-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: svn = %{version}-%{release}
Provides: subversion-python
Obsoletes: subversion-python

Requires(post): coreutils libselinux-utils

%global __perl_requires %{SOURCE3}

# Put Python bindings in site-packages
%global swigdirs swig_pydir=%{python_sitearch}/libsvn swig_pydir_extra=%{python_sitearch}/svn

%description
Subversion is a concurrent version control system which enables one
or more users to collaborate in developing and maintaining a
hierarchy of files and directories while keeping a history of all
changes.  Subversion only stores the differences between versions,
instead of every complete file.  Subversion is intended to be a
compelling replacement for CVS.

%package devel
Group: Development/Tools
Summary: Development package for the Subversion libraries
Requires: subversion = %{version}-%{release}, apr-devel, apr-util-devel

%description devel
The subversion-devel package includes the static libraries and
include files for developers interacting with the subversion
package.

%package -n mod_dav_svn
Group: System Environment/Daemons
Summary: Apache server module for Subversion server
Requires: httpd-mmn = %(cat %{_includedir}/httpd/.mmn || echo missing)
Requires: subversion = %{version}-%{release}
BuildRequires: httpd-devel >= 2.4.3
Obsoletes: subversion-server

%description -n mod_dav_svn
The mod_dav_svn package allows access to a Subversion repository
using HTTP, via the Apache httpd server.

%package perl
Group: Development/Libraries
Summary: Perl bindings to the Subversion libraries
BuildRequires: perl-devel >= 2:5.8.0, perl-ExtUtils-MakeMaker
BuildRequires: perl-Test-Simple, perl-ExtUtils-Embed
Requires: %(eval `perl -V:version`; echo "perl(:MODULE_COMPAT_$version)")
Requires: subversion = %{version}-%{release}

%description perl
This package includes the Perl bindings to the Subversion libraries.

%if %{with_java}
%package javahl
Group: Development/Libraries
Summary: JNI bindings to the Subversion libraries
Requires: subversion = %{version}-%{release}
BuildRequires: java-devel
BuildRequires: java-1.6.0-openjdk-devel

%description javahl
This package includes the JNI bindings to the Subversion libraries.
%endif

%package ruby
Group: Development/Libraries
Summary: Ruby bindings to the Subversion libraries
BuildRequires: ruby-devel >= 1.9.2, ruby >= 1.9.2
Requires: subversion = %{version}-%{release}, ruby-libs >= 1.9.2
Requires: ruby(abi) = 1.9.1

%description ruby
This package includes the Ruby bindings to the Subversion libraries.

%prep
%setup -q
%patch1 -p1 -b .rpath
%patch2 -p1 -b .pie
%patch3 -p1 -b .kwallet
%patch4 -p1 -b .rubybind
%patch5 -p1 -b .aarch64
%patch8 -p1 -b .swigplWall

# can't use /usr/local/bin/python
sed -i 's|usr/local/bin/python|usr/bin/python|g' tools/server-side/svnpubsub/commit-hook.py

%build
# Regenerate the buildsystem, so that:
#  1) patches applied to configure.in take effect
#  2) the swig bindings are regenerated using the system swig
# (2) is not ideal since typically upstream test with a different
# swig version
PATH=/usr/bin:$PATH ./autogen.sh --release

# fix shebang lines, #111498
perl -pi -e 's|/usr/bin/env perl -w|/usr/bin/perl -w|' tools/hook-scripts/*.pl.in

# override weird -shrext from ruby
export svn_cv_ruby_link="%{__cc} -shared"
export svn_cv_ruby_sitedir_libsuffix=""
export svn_cv_ruby_sitedir_archsuffix=""

export CC=gcc CXX=g++ JAVA_HOME=%{jdk_path} CFLAGS="%{optflags}"
%configure \
        --with-berkeley-db=db.h:%{_includedir}/libdb:/%{_lib}:db-5.3 \
        --with-swig --with-serf=%{_prefix} \
        --with-ruby-sitedir=%{ruby_sitearch} \
        --with-apxs=%{_httpd_apxs} --disable-mod-activation \
        --with-apache-libexecdir=%{_httpd_moddir} \
        --disable-static 

#        --disable-static --with-sasl=%{_prefix} \
#        --with-libmagic=%{_prefix} \
#        --with-gnome-keyring \
%make all
make swig-py swig-py-lib %{swigdirs}
make swig-pl swig-pl-lib swig-rb swig-rb-lib
%if %{with_java}
# javahl-javah does not parallel-make with javahl
make javahl-java javahl-javah
make javahl
%endif

%install
rm -rf ${RPM_BUILD_ROOT}
make install install-swig-py install-swig-pl-lib install-swig-rb \
        DESTDIR=$RPM_BUILD_ROOT %{swigdirs}
%if %{with_java}
make install-javahl-java install-javahl-lib javahl_javadir=%{_javadir} DESTDIR=$RPM_BUILD_ROOT
%endif

make pure_vendor_install -C subversion/bindings/swig/perl/native \
        PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
install -m 755 -d ${RPM_BUILD_ROOT}%{_sysconfdir}/subversion

# Add subversion.conf configuration file into httpd/conf.d directory.
install -m 755 -d ${RPM_BUILD_ROOT}%{_sysconfdir}/httpd/conf.d
install -m 644 $RPM_SOURCE_DIR/subversion.conf ${RPM_BUILD_ROOT}%{_sysconfdir}/httpd/conf.d/subversion.conf.dist

# install wrapper script
mv %{buildroot}%{_bindir}/svnserve %{buildroot}%{_bindir}/svnserve-bin
%{__install} -m 0755 %{SOURCE10} %{buildroot}%{_bindir}/svnserve

#install -m 755 svn-config %{buildroot}%{_bindir}/svn-config

install -m 0644 %{SOURCE11} %{buildroot}%{_sysconfdir}/subversion/
#install -m 0755 contrib/client-side/svn_load_dirs/svn_load_dirs.pl %{buildroot}%{_bindir}/
#install -m 0755 contrib/client-side/svn_all_diffs.pl %{buildroot}%{_bindir}/
#install -m 0755 contrib/client-side/search-svnlog.pl %{buildroot}%{_bindir}/
#install -m 0755 contrib/client-side/svn-log.pl %{buildroot}%{_bindir}/

# Remove unpackaged files
rm -rf ${RPM_BUILD_ROOT}%{_includedir}/subversion-*/*.txt \
       ${RPM_BUILD_ROOT}%{python_sitearch}/*/*.{a,la}

# remove stuff produced with Perl modules
find $RPM_BUILD_ROOT -type f \
    -a \( -name .packlist -o \( -name '*.bs' -a -empty \) \) \
    -print0 | xargs -0 rm -f

# make Perl modules writable so they get stripped
find $RPM_BUILD_ROOT%{_libdir}/perl5 -type f -perm 555 -print0 |
        xargs -0 chmod 755

# unnecessary libraries for swig bindings
rm -f ${RPM_BUILD_ROOT}%{_libdir}/libsvn_swig_*.{so,la,a}

# Remove unnecessary ruby libraries
rm -f ${RPM_BUILD_ROOT}%{ruby_sitearch}/svn/ext/*.*a

# Trim what goes in docdir
rm -rf tools/*/*.in tools/test-scripts

## Install psvn for emacs and xemacs
#for f in emacs/site-lisp xemacs/site-packages/lisp; do
#  install -m 755 -d ${RPM_BUILD_ROOT}%{_datadir}/$f
#  install -m 644 $RPM_SOURCE_DIR/psvn.el ${RPM_BUILD_ROOT}%{_datadir}/$f
#done

# Rename authz_svn INSTALL doc for docdir
ln -f subversion/mod_authz_svn/INSTALL mod_authz_svn-INSTALL

# Trim exported dependencies to SVN and APR libraries only:
sed -i "/^dependency_libs/{
     s, -l[^ ']*, ,g;
     s,%{_libdir}/lib[^sa][^vp][^nr].*.la, ,g;
     }"  $RPM_BUILD_ROOT%{_libdir}/*.la

%find_lang %{name}

%if %{make_check}
%check
export LANG=C LC_ALL=C
make check check-swig-pl check-swig-py CLEANUP=yes
# check-swig-rb omitted: it runs svnserve
%endif

%clean
rm -rf ${RPM_BUILD_ROOT}

%post
/sbin/ldconfig

if [ -f /usr/sbin/getenforce ]; then
    result=`/usr/sbin/getenforce`
    if [ ${result} != "Disabled" ]; then
        for i in svn svnadmin svndumpfilter svnlook svnserve-bin svnsync svnversion;
        do
            /usr/bin/chcon -t textrel_shlib_t %{_bindir}/${i}
        done
    fi
fi

%postun -p /sbin/ldconfig

%post perl -p /sbin/ldconfig

%postun perl -p /sbin/ldconfig

%post ruby -p /sbin/ldconfig

%postun ruby -p /sbin/ldconfig

%if %{with_java}
%post javahl -p /sbin/ldconfig

%postun javahl -p /sbin/ldconfig
%endif

%files -f %{name}.lang
%defattr(-,root,root)
%doc BUGS COMMITTERS INSTALL README CHANGES
%doc tools mod_authz_svn-INSTALL
#%doc contrib/client-side/svn_load_dirs/svn_load_dirs{.pl,_*,.README}
%{_bindir}/*
%{_libdir}/libsvn_*.so.*
%{_mandir}/man*/*
%{python_sitearch}/svn
%{python_sitearch}/libsvn
#%%{_datadir}/emacs/site-lisp
#%%{_datadir}/xemacs/site-packages/lisp
%dir %{_sysconfdir}/subversion
%config(noreplace) %{_sysconfdir}/subversion/pre-svnserve
%exclude %{_libdir}/libsvn_swig_perl*
%exclude %{_libdir}/libsvn_swig_ruby*
%exclude %{_mandir}/man*/*::*

%files devel
%defattr(-,root,root)
%{_includedir}/subversion-1
%{_libdir}/libsvn*.*a
%{_libdir}/libsvn*.so
%exclude %{_libdir}/libsvn_swig_perl*
%if %{with_java}
%exclude %{_libdir}/libsvnjavahl-1.*
%endif

%files -n mod_dav_svn
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/httpd/conf.d/subversion.conf.dist
%{_libdir}/httpd/modules/mod_dav_svn.so
%{_libdir}/httpd/modules/mod_authz_svn.so
#%%{_libdir}/httpd/modules/mod_dontdothat.so

%files perl
%defattr(-,root,root,-)
%{perl_vendorarch}/auto/SVN
%{perl_vendorarch}/SVN
%{_libdir}/libsvn_swig_perl*
%{_mandir}/man*/*::*

%files ruby
%defattr(-,root,root,-)
%{_libdir}/libsvn_swig_ruby*
%{ruby_sitearch}/svn

%if %{with_java}
%files javahl
%defattr(-,root,root,-)
%{_libdir}/libsvnjavahl-1.*
%{_javadir}/svn-javahl.jar
%endif

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.9-2m)
- rebuild against perl-5.20.0

* Fri May 30 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.9-1m)
- update to 1.8.9
- patches from fc21

* Fri Mar 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.8-1m)
- update 1.8.8
- [SECURITY] CVE-2014-0032

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5-2m)
- rebuild against perl-5.18.2

* Mon Dec  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5-1m)
- [SECURITY] CVE-2013-4505 CVE-2013-4558
- update to 1.8.5

* Wed Oct 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4-1m)
- update 1.8.4

* Sun Sep 01 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.3-1m)
- update 1.8.3

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-2m)
- rebuild against perl-5.18.1

* Wed Jul 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-1m)
- [SECUTITY] CVE-2013-4131
- update 1.8.1

* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0-1m)
- update 1.8.0

* Thu Jun  6 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.10-1m)
- update 1.7.10

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.9-3m)
- rebuild against gnutls-3.2.0

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.9-2m)
- rebuild against perl-5.18.0

* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.9-1m)
- [SECURITY] CVE-2013-1845 CVE-2013-1846 CVE-2013-1847 CVE-2013-1849
- [SECURITY] CVE-2013-1884
- update to 1.7.9

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.8-3m)
- rebuild against perl-5.16.3

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.8-2m)
- rebuild against httpd-2.4.3

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.8-1m)
- update 1.7.8

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.6-2m)
- rebuild against perl-5.16.2

* Mon Aug 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.6-1m)
- update 1.7.6

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.5-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.5-2m)
- rebuild against perl-5.16.0

* Fri May 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5-1m)
- update 1.7.5

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.4-2m)
- rebuild against lindb-5.3.15

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4-1m)
- update 1.7.4

* Tue Feb 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.3-1m)
- update 1.7.3, fixes a number of crashes and improves error handling in several cases
- remove --with-apr and -with apr-util from configure, these are auto detected

* Tue Dec  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-1m)
- update 1.7.2

* Wed Oct 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.1-1m)
- update 1.7.1

* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-1m)
- update 1.7.0 release

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-0.5m)
- rebuild against perl-5.14.2

* Fri Sep 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-0.4m)
- update 1.7.0-rc4

* Sun Sep 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-0.3m)
- update 1.7.0-rc3

* Mon Sep  5 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.7.0-0.2m)
- rebuild against libdb

* Wed Aug 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-0.1m)
- update 1.7.0-rc2

* Tue Jul  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.17-4m)
- revise BuildRequires

* Tue Jul  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.17-3m)
- bug fix; see Momonga-devel.ja:04520
-- rebuild against sqlite-3.7.7.1

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.17-2m)
- rebuild against perl-5.14.1

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.17-1m)
- [SECURITY] CVE-2011-1752 CVE-2011-1783 CVE-2011-1921
- update to 1.6.17

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.16-4m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.16-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.16-2m)
- rebuild for new GCC 4.6

* Fri Mar  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.16-1m)
- update 1.6.16
- [SECURITY] CVE-2011-0715

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.15-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.15-1m)
- [SECURITY] CVE-2010-4539 CVE-2010-4644
- update 1.6.15

* Wed Oct  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.13-1m)
- update 1.6.13
- [SECURITY] CVE-2010-3315

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.12-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.12-7m)
- full rebuild for mo7 release

* Tue Aug 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.12-6m)
- add Requires(post): coreutils libselinux-utils

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.12-5m)
- add quick hacks for SELinux at %%post

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.12-4m)
- Require ruby(abi)-1.9.1

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.12-3m)
- rebuild against ruby-1.9.2

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.12-2m)
- rebuild against libproxy-0.4.4

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.12-1m)
- update 1.6.12

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.11-3m)
- rebuild against perl-5.12.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.11-2m)
- use BuildRequires

* Mon Apr 19 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.11-1m)
- update 1.6.11

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.9-4m)
- rebuild against perl-5.12.0

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.9-3m)
- rebuild against db-4.8.26

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.9-2m)
- rebuild against db-4.8.26

* Tue Jan 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.9-1m)
- update 1.6.9
- support ruby-1.9
-- subversion-1.6.6-ruby-binding-support-ruby19.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.6-1m)
- update 1.6.6

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.5-2m)
- rebuild against perl-5.10.1

* Sun Aug 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.5-1m)
- update 1.6.5

* Sat Aug  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-2m)
- rebuild against apr-1.3.8 and apr-util-1.3.9

* Fri Aug  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update 1.6.4
- [SECURITY] fix CVE-2009-2411

* Sun Aug  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-2m)
- build with --with-berkeley-db, so that we can build libsvn_fs_base-1 on x86_64

* Tue Jun 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3-1m)
- updated to 1.6.3

* Sun May 10 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (1.6.2-1m)
- updated to 1.6.2 test version
- requires sqlite-3.6.13 or newer

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-1m)
- update 1.6.1

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-2m)
- rebuild against openssl-0.9.8k

* Wed Apr  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update 1.6.0

* Sun Mar  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.6-1m)
- update 1.5.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-4m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-3m)
- update Patch3,6 for fuzz=0

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.5-2m)
- rebuild against python-2.6.1-22

* Thu Dec 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.5-1m)
- update 1.5.5

* Mon Oct 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.4-1m)
- update 1.5.4

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.3-2m)
- rebuild against db4-4.7.25-1m

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.3-1m)
- update 1.5.3

* Fri Sep  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-1m)
- update 1.5.2

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update 1.5.1

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-5m)
- rebuild against gnutls-2.4.1 (neon)

* Fri Jun 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-4m)
- remove old patch
- add Fedora patch
- javahl was no parallel build

* Wed Jun 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.8-3m)
- path to openjdk is depend on arch.

* Tue Jun 24 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-2m)
- use openjdk to build javahl

* Sun Jun 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-1m)
- update 1.5.0-release

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-12m)
- rebuild against openssl-0.9.8h-1m

* Tue May  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.6-11m)
- Provides and Obsoletes subversion-python

* Tue May  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.6-10m)
- enable java
- sync Fedora

* Mon Apr  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-9m)
- support neon-0.28.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-8m)
- rebuild against gcc43

* Thu Mar 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-7m)
- support neon-0.28.1

* Sun Mar  9 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-6m)
- cleanup spec

* Sat Mar  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-5m)
- use neon-0.28

* Thu Feb 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-4m)
- restrict rebuild against openldap-2.4.8 

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-4m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-3m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.6-1m)
- rebuild against perl-5.10.0-1m

* Fri Dec 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-1m)
- update 1.4.6

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.5-2m)
- rebuild against db4-4.6.21

* Tue Aug 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-1m)
- [SECURITY] CVE-2007-3846
- update 1.4.5

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.4-2m)
- rebuild against ruby-1.8.6-4m

* Wed Jun 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-1m)
- [SECURITY] CVE-2007-2448
- update 1.4.4

* Thu Apr  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.3-1m)
- update 1.4.3

* Wed Dec 27 2006 Nishio Futosi <futoshi@momonga-linux.org>
- (1.4.2-4m)
- Obsolete subversion-cvs2svn (from 1.3.1)
- Build with make_check 0 (please fix)

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-3m)
- rebuild against python-2.5

* Wed Dec 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.2-2m)
- separated cvs2svn to an independent package.

* Sat Nov 18 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.2-1m)
- version 1.4.2
- build against db-4.5.20
- set with_java 0

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-3m)
- delete libtool library

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.3.2-2m)
- rebuild against expat-2.0.0-1m

* Tue Aug 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2
- update source10: http://cvs2svn.tigris.org/files/documents/1462/32231/cvs2svn-1.3.1.tar.gz

* Sun Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-7m)
- modify configure for subversion-ruby

* Thu Jun  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-6m)
- modify subversion-ruby %files entry

* Mon Jun  5 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-5m)
- remove psvn

* Tue May 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-4m)
- add BuildPreReq: openldap-devel for link -lldap

* Wed May 10 2006 Masahiro Takahata <tab@momonga-linux.org>
- (1.3.1-3m)
- rebuild against swig

* Wed May  3 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-2m)
- add BuildPreReq: swig >= 1.3.29

* Wed May  3 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-1m)
- use apr-1
- version up

* Mon Apr 10 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.0-8m)
- rebuild against openssl-0.9.8a, openldap-2.3.19

* Sat Feb 11 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-7m)
- import subversion-1.3.0-neonver.patch from fc-devel

* Sun Jan 21 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-6m)
- add --disable-neon-version-check at configure

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-5m)
- rebuild against openldap-2.3.11

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-4m)
- rebuild against db4.3

* Sun Jan  8 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (1.3.0-3m)
- added pymodule path patch for python-2.3

* Wed Jan  4 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-2m)
- add Patch10: subversion-1.3.0-swig.patch

* Wed Jan  4 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (1.3.0-1m)
- updated to 1.3.0
- releasenote:  http://subversion.tigris.org/svn_1.3_releasenotes.html
- unofficial releasenote(japanese) http://dolphin.c.u-tokyo.ac.jp/~nori1/tmp/svn_1.3_releasenotes.html.ja

* Tue Dec  6 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.2.3-5m)
- fix wrong %files entries where libsvn_swig* libraries were doubly listed.

* Mon Dec  5 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.2.3-4m)
- rebuild against swig 1.3.27 *USING AD-HOC PATCH*.
- enable ruby binding.

* Sun Dec  4 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.2.3-3m)
- move libsvn_swig_py*.so* from subversion to subversion-python sub package
  to remove unnecessary dependency from the main package.

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-2m)
- rebuild against python-2.4.2
- update subversion-pymodulepath.patch

* Sat Aug 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3
  (1.2.2 was a placeholder release and was never officially blessed)

* Sun Aug 21 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.1-2m)
- java-sun-jdk1.5-sdk

* Wed Jul 06 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Tue May 24 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sat May 14 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.0-0.4.1m)
- updated to 1.2.0-rc4

* Tue Apr  5 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.4-1m)
- updated to 1.1.4

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (1.1.3-4m)
- bug fix. svn-config don't handle SVN_DB_*.

* Thu Mar 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-3m)
- rebuild against swig-1.3.21-1m

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.1.3-2m)
- large file support. rebuild against httpd 2.0.53.

* Sun Jan 16 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3 release version

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (1.1.2-2m)
- enable x86_64.

* Tue Dec 21 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2 release version

* Sat Oct 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1 release version

* Thu Sep 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.0-1m)
- update to 1.1.0 release version

* Sun Sep 26 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.0-0.4.2m)
- added java bindings (disabled by default)

* Sat Sep 25 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.0-0.4.1m)
- update to 1.1.0-rc4
- [SECURITY] This update includes security fix of CAN-2004-0749
- changes are:
  User-visible-changes:
  * fixed: mod_authz_svn metadata leaks (CAN-2004-0749)
  * various.po translation updates

  Developer-visible-changes:
  * fixed: buglet in fsfs history code (issue #2054)
  * stop using -std=c89 gcc flag (r11054)
  * sync with apr 1.0's find_apr.m4 and find_apu.m4 files (r10560)
  * win32 installer improvements (r10978)

* Wed Sep 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.0-0.3.1m)
- update to 1.1.0-rc3

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.0-0.2.2m)
- rebuild against python2.3

* Mon Aug 16 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-0.2.1m)
- update to 1.1.0-rc2
- add files %%{_datadir}/locale/*/*/*

* Wed Jul 21 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.6-2m)
- stop service

* Tue Jul 20 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Mon Jul 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-3m)
- set release of BuildPreReq: httpd-apr >= 2.0.50-1m
- change tar zxvf to tar zxf

* Mon Jun 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.5-2m)
- rebuild against db4.2

* Fri Jun 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Sat May 21 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Thu May 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.3-1m)
- update to 1.0.3
   this release is for security fix

* Tue Apr 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.2-1m)
- update to 1.0.2
- changes
   Fixes for the following bugs:
     a segfault when remotely deleting svn:author property
     mod_dav_svn accepting too many authors
     create runtime config files with native EOLs
     recursive propset can corrupt .svn/entries
     allow shared working copies [mostly working now]
     mod_authz_svn should ignore the URI on a MERGE request
     svnserve assertion failure on empty error messages
     commit/update memory leaks when working on many targets
     and not displaying repos-paths or URLs with '\' on Win32.

* Tue Apr 13 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.1-4m)
- add BuildPreReq: libxml2-devel

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.1-3m)
- rebuild against for libxml2-2.6.8

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.1-2m)
- revised spec for rpm 4.2.

* Sat Mar 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1
- delete tools/cvs2svn/rcsparse from file list
- make tarball use current 'svn co http://svn.collab.net/repos/cvs2svn/trunk cvs2svn'

* Mon Feb 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.0-1m)
- update to 1.0.0

* Mon Feb 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.0-0.1.2m)
- add subversion-utils subpackage which contains tiny tools for client-side svn

* Sun Feb 22 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.0-0.1.1m)
- update to 1.0.0 RC

* Mon Jan 26 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.37.0-1m)
- update to 0.37.0
- post-svnserve was gone

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.36.0-3m)
- import patch from BTS of subversion
   see http://subversion.tigris.org/issues/show_bug.cgi?id=1474
- remove IDEAS from %%files

* Wed Jan 14 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.36.0-2m)
- correct cangelog
- revise svnserve.sh

* Wed Jan 14 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.36.0-1m)
- update to 0.36.0
- add patch from http://subversion.tigris.org/issues/show_bug.cgi?id=1698
- provides pre-svnserve and post-svnserve hooks

* Thu Jan  8 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.35.1-3m)
- remove BuildPreReq: krb5-devel since neon-devel requires it if necessary.

* Wed Jan 07 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.35.1-2m)
- add BuildPreReq: krb5-devel for -lgssapi_krb5

* Sun Dec 21 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.35.1-1m)
- update to 0.35.1

* Fri Dec 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.34.0-1m)
- update to 0.34.0
- %%config(noreplace) %%{_sysconfdir}/httpd/conf.d/mod_dav_svn.conf

* Mon Nov 10 2003 Masahiro Takahata <takahata@monoga-linux.org>
- (0.32.1-2m)
- rebuild against openldap

* Sat Oct 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.32.1-1m)
- update to 0.32.1

* Tue Oct 14 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.31.0-1m)
- update to 0.31

* Wed Oct  8 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.30.0-1m)
- update to 0.30.0
- change source URL
- License

* Fri Aug 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.27.0-1m)
- update to 0.27.0

* Wed Aug 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.26.0-1m)
- add new sub package for python bindings

* Fri Jul 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.26.0-1m)
- update to 0.26.0

* Mon Jul 14 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.25-1m)
  update to 0.25

* Tue Jun 24 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.24.1-2m)
  fix niwatama

* Wed Jun 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.24.1-1m)
  update to 0.24.1
  
* Fri May 23 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.23.0-1m)
  update to 0.23.0

* Thu May  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.21.0-4m)
- BuildPreReq httpd-apr

* Sun May  4 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.21.0-3m)
- fix %%post and %%preun (svn_design.info.* -> svn-design.info)

* Sun Apr 27 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.21.0-2m)
  added cvs2svn sub package

* Sat Apr 26 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- Initial build.
