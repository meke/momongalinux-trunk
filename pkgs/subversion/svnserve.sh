#!/bin/sh -

dir=`/usr/bin/dirname "$0"`

if [ -r /etc/subversion/pre-svnserve ]; then
  . /etc/subversion/pre-svnserve
fi

exec "$dir/svnserve-bin" "$@"
