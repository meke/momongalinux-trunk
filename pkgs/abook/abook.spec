%global momorel 9
%global prever pre2

Summary: Text-based addressbook program
Name: abook
Version: 0.6.0
Release: 0.%{prever}.%{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
#Source: http://dl.sourceforge.net/sourceforge/abook/%{name}-%{version}.tar.gz
Source: http://abook.sourceforge.net/devel/%{name}-%{version}%{prever}.tar.gz
NoSource: 0
Source1: config-sample.abookrc
URL: http://abook.sourceforge.net/
BuildRequires: readline-devel >= 5.0
BuildRequires: ncurses-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Abook is a text-based addressbook program designed to
use with mutt mail client.

%prep
%setup -q -n %{name}-%{version}%{?prever}

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall
# generate localized files list
%find_lang %{name}
mkdir -p	%{buildroot}%{_datadir}/config-sample/abook
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/config-sample/abook/abookrc

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files -f %{name}.lang
%defattr(644,root,root,755)
%doc sample.abookrc ANNOUNCE AUTHORS BUGS COPYING ChangeLog FAQ INSTALL
%doc NEWS README THANKS TODO abook.spec contrib/
%defattr(755,root,root,-)
%{_bindir}/abook
%defattr(-,root,root)
%{_mandir}/man1/abook.*
%{_mandir}/man5/abookrc.*
%docdir %{_datadir}/config-sample/abook
%{_datadir}/config-sample/abook

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-0.pre2.9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-0.pre2.8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-0.pre2.7m)
- full rebuild for mo7 release

* Thu May  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-0.pre2.6m)
- add execute bit

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-0.pre2.5m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-0.pre2.4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-0.pre2.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-0.pre2.2m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.0-0.pre2.1m)
- version up 0.6.0pre2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-8m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-7m)
- remove BPR libtermcap-devel

* Fri Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.1-6m)
- rebuild against readline-5.0

* Fri May 13 2005 Kazuya Iwamoto <iwapi@momonga-linux.org>
- (0.5.1-5m)
- kossori...

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.5.1-5m)
- rebuild against libtermcap-2.0.8-38m.

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.1-4m)
- revised docdir permission

* Mon Mar 29 2004 TAKAHASHI Tamotsu <tamo>
- (0.5.1-3m)
- 0.5.1-01_mutt-filter was incomplete. apply nick_cutoff.patch
- 0.5.2 is available, but I don't want to update to that version
 because it has an "experimental wide character support" (see ChangeLog)

* Sat Jan 24 2004 TAKAHASHI Tamotsu <tamo>
- (0.5.1-2m)
- apply http://abook.sourceforge.net/patches/0.5.1-01_mutt-filter

* Mon Nov 24 2003 TAKAHASHI Tamotsu <tamo>
- (0.5.1-1m)
- update. add contrib as doc

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-2m)
- s/Copyright:/License:/

* Thu Jul  3 2003 TAKAHASHI Tamotsu <tamo>
- (0.5.0-1m)
- first import

* Sat Jun 28 2003 TAKAHASHI Tamotsu <tamo>
- (0.5.0-0.1.0m)
- 0.5.0rc1
- add BuildPreReq: readline-devel
- add config-sample

* Sat Jan 11 2003 TAKAHASHI Tamotsu
- (0.4.17-0m)
- flavor of momonga...

* Mon Mar 11 2002 Jaakko Heinonen <jheinonen@users.sourceforge.net>
- rewrote the spec file originally written by
  Gustavo Niemeyer <niemeyer@conectiva.com>
