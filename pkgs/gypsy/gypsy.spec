%global momorel 5

Name:           gypsy
Version:        0.8
Release:        %{momorel}m%{?dist}
Summary:        A GPS multiplexing daemon

Group:          System Environment/Libraries
# See LICENSE file for details
License:        LGPLv2 and GPLv2
URL:            http://gypsy.freedesktop.org/
Source0:        http://gypsy.freedesktop.org/releases/%{name}-%{version}.tar.gz
NoSource:       0
Patch1:		 gypsy-0.8-glib233.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: bluez-libs-devel
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: glib2-devel
BuildRequires: gtk-doc
BuildRequires: libxslt

Requires: dbus

%description
Gypsy is a GPS multiplexing daemon which allows multiple clients to 
access GPS data from multiple GPS sources concurrently. 

%package devel
Summary: Development package for gypsy
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: dbus-glib-devel
Requires: pkgconfig

%description devel
Header files for development with gypsy.

%package docs
Summary: Documentation files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk-doc
BuildArch: noarch

%description docs
This package contains developer documentation for %{name}.

%prep
%setup -q
%patch1 -p1 -b .glib2332~

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT%{_libdir}/libgypsy.la

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYING.lib LICENSE
%{_sysconfdir}/dbus-1/system.d/Gypsy.conf
%{_datadir}/dbus-1/system-services/org.freedesktop.Gypsy.service
%{_libexecdir}/gypsy-daemon
%{_libdir}/libgypsy.so.0
%{_libdir}/libgypsy.so.0.0.0

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/gypsy.pc
%{_includedir}/gypsy
%{_libdir}/libgypsy.so

%files docs
%defattr(-,root,root,-)
%doc %{_datadir}/gtk-doc/html/gypsy

%changelog
* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-5m)
- fix build failure with glib 2.33+

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-1m)
- import from Fedora 11 for webkitgtk

* Wed Mar  4 2009 Peter Robinson <pbrobinson@gmail.com> 0.6-8
- Move docs to noarch, some spec file updates

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Dec 18 2008 Peter Robinson <pbrobinson@gmail.com> 0.6-6
- Add gtk-doc build req

* Sat Nov 22 2008 Peter Robinson <pbrobinson@gmail.com> 0.6-5
- Rebuild

* Thu Sep 11 2008 - Bastien Nocera <bnocera@redhat.com> 0.6-4
- Rebuild

* Mon May 15 2008 Peter Robinson <pbrobinson@gmail.com> 0.6-3
- Further spec file cleanups

* Mon Apr 28 2008 Peter Robinson <pbrobinson@gmail.com> 0.6-2
- Some spec file cleanups

* Sat Apr 26 2008 Peter Robinson <pbrobinson@gmail.com> 0.6-1
- Initial package
