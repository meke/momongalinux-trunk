%global momorel 3
%global boost_version 1.55.0

Summary: Generic Programming for Computer Vision
Name: vigra
Version: 1.9.0
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Development/Libraries
Source0: http://hci.iwr.uni-heidelberg.de/%{name}/%{name}-%{version}-src.tar.gz
NoSource: 0
URL: http://kogs-www.informatik.uni-hamburg.de/~koethe/vigra/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: boost-devel >= %{boost_version}
BuildRequires: zlib-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: fftw-devel >= 3

%description
VIGRA stands for "Vision with Generic Algorithms". It's a novel computer vision
library that puts its main emphasis on customizable algorithms and data
structures. By using template techniques similar to those in the C++ Standard
Template Library, you can easily adapt any VIGRA component to the needs of your
application without thereby giving up execution speed.

%package devel
Summary: Development tools for programs which will use the vigra library
Group: Development/Libraries
Requires: vigra = %{version}-%{release}
Requires: libjpeg-devel, libtiff-devel, libpng-devel, zlib-devel, fftw-devel >= 3

%description devel
The vigra-devel package includes the header files necessary for developing
programs that use the vigra library.

%package python
Summary: Python interface for the vigra computer vision library
Requires: vigra = %{version}-%{release}
Requires: numpy numpy-f2py

%description python
The vigra-python package provides python bindings for vigra

%prep
%setup -q

%build
%cmake . -DWITH_OPENEXR=1 -DWITH_HDF5=1
make VERBOSE=1 %{?_smp_mflags}
# cleanup
rm -f doc/vigranumpy/.buildinfo
find ./doc/ -type f | xargs chmod -x

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -rf %{buildroot}/usr/doc

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root,-)
%doc LICENSE.txt README.txt
%{_libdir}/libvigraimpex.so.*

%files devel
%defattr(-, root, root,-)
%{_includedir}/vigra
%{_bindir}/vigra-config
%{_libdir}/libvigraimpex.so
%{_libdir}/vigra
%doc doc/vigra doc/vigranumpy

%files python
%defattr(-, root, root,-)
%{python_sitearch}/vigra

%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-3m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-2m)
- rebuild against boost-1.52.0

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-5m)
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-5m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-3m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-2m)
- rebuild against rpm-4.6

* Tue Jul  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.0-1m)
- import from Fedora to Momonga for OOo3

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.5.0-4
- Autorebuild for GCC 4.3

* Tue Aug 21 2007 Bruno Postle <bruno@postle.net> 1.5.0-3
  - Bumping for Jesse

* Mon Feb 19 2007 Bruno Postle <bruno@postle.net> 1.5.0-2
  - update to 1.5.0 release
  - fix bug 228926: vigra: $RPM_OPT_FLAGS not used

* Tue Sep 12 2006 Bruno Postle <bruno@postle.net> 1.4.0-4
  - remove gcc-c++ dependency

* Fri Sep 08 2006 Bruno Postle <bruno@postle.net> 1.4.0-3
  - change defattr
  - require fftw >= 3, gcc-c++

* Tue Sep 05 2006 Bruno Postle <bruno@postle.net> 1.4.0-2
  - update with review feedback

* Mon Aug 28 2006 Bruno Postle <bruno@postle.net> 1.4.0-1
  - revive, update to 1.4 with new license, remove patches, 
  - split to devel package

* Fri Jul 02 2004 Bruno Postle <bruno@postle.net>
  - add pablo's vigra_typetraits_extension.diff patch and shorten
    the impex patch so it doesn't conflict

* Wed May 05 2004 Bruno Postle <bruno@postle.net>
  - new build with patches for 16bit tiff and viff bug

* Sun Feb 08 2004 Bruno Postle <bruno@postle.net>
  - new build with shared libraries

* Wed Jan 04 2004 Bruno Postle <bruno@postle.net>
  - new build without shared libraries

* Tue Nov 25 2003 Bruno Postle <bruno@postle.net>
  - new build with shared libraries

* Sun Nov 23 2003 Bruno Postle <bruno@postle.net>
  - make a horrible mess


