%global momorel 6
%global kdever 4.9.1
%global kdelibsrel 2m
%global qtver 4.8.2
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global atticaver 0.4.0
%global stable 0
%if %{stable}
%global sourcedir stable
%else
%global sourcedir unstable
%endif
%global major 0.71
%global minor 0

Name:           gluon
Summary:        high-level game development library for the KDE
Url:            http://gluon.tuxfamily.org/
Version:        %{major}.%{minor}
Release:        %{momorel}m%{?dist}
License:        LGPLv2
Group:          System Environment/Libraries
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}/%{major}/src/%{name}-%{version}.tar.gz
NoSource:       0
Requires:       qt >= %{qtver}-%{qtrel}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  kdebase-workspace-devel >= %{kdever}
BuildRequires:  alure-devel >= 1.1
BuildRequires:  attica-devel >= %{atticaver}
BuildRequires:  eigen2-devel
BuildRequires:  glew-devel >= 1.9.0
BuildRequires:  libsndfile-devel
BuildRequires:  libvorbis-devel
BuildRequires:  openal-soft-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Gluon is a high-level game development library for the KDE desktop enviornment. 
It is the combination of the KGL, KAL and KCL libraries for a single, complete 
games devlopment solution, supporting physics, graphics, audio and controllers.

%package devel
License:        LGPLv2
Group:          Development/Libraries
Summary:        libraries and headers for %{name}

%description devel
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING
%{_kde4_bindir}/gluon_kdeextplayer
%{_kde4_bindir}/gluon_kdeplayer
%{_kde4_bindir}/gluon_qmlplayer
%{_kde4_bindir}/gluon_qtplayer
%{_kde4_bindir}/gluoncreator
%{_kde4_bindir}/gluonexamples_graphics_basic
%{_kde4_libdir}/gluon
%{_kde4_libdir}/libGluon*.so.*
%{_kde4_libdir}/kde4/*.so
%{_kde4_appsdir}/gluoncreator
%{_kde4_appsdir}/*/*.rc
%{_kde4_datadir}/config.kcfg/gluoncreatorsettings.kcfg
%{_kde4_datadir}/applications/*.desktop
%{_kde4_datadir}/applications/kde4/gluon-creator.desktop
%{_kde4_datadir}/kde4/servicetypes/*.desktop
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/mime/packages/*.xml
%{_kde4_iconsdir}/hicolor/*/apps//*.png
%{_kde4_iconsdir}/hicolor/*/apps//*.svgz
%{_kde4_iconsdir}/hicolor/scalable/apps/*.svg

%files devel
%defattr(-,root,root)
%{_kde4_includedir}/gluon
%{_kde4_datadir}/cmake/Modules/*.cmake
%{_kde4_datadir}/gluon
%{_kde4_libdir}/libGluon*.so

%changelog
* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71.0-6m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.71.0-5m)
- rebuild for glew-1.9.0

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71.0-4m)
- rebuild against attica-0.4.0

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.71.0-3m)
- rebuild against glew-1.7.0

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71.0-2m)
- rebuild against attica-0.3.0

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71.0-1m)
- update to 0.71.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.70.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.70.0-5m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70.0-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.70.0-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70.0-2m)
- rebuild against qt-4.6.3-1m

* Tue May  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70.0-1m)
- update to 0.70.0, first official release version

* Tue Jan 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.20100126-1m)
- update to 20100126 git version

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.20090116.1m)
- initial build for Momonga Linux
