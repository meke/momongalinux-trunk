%global momorel 1

%define priority	65-1
%define ppriority	65-0
%define fontname	vlgothic
%define	archivename	VLGothic-%{version}
%define	fontconf	%{priority}-%{fontname}-gothic
%define	pfontconf	%{ppriority}-%{fontname}-pgothic
%define	common_desc	\
VLGothic provides Japanese TrueType fonts from the Vine Linux project.\
Most of the glyphs are taken from the M+ and Sazanami Gothic fonts,\
but some have also been improved by the project.

Name:		%{fontname}-fonts
Version:	20140530
Release:	%{momorel}m%{?dist}
Summary:	Japanese TrueType font

License:	"mplus" and Modified BSD
Group:		User Interface/X
URL:		http://dicey.org/vlgothic
Source0:	http://dl.sourceforge.jp/vlgothic/61261/%{archivename}.tar.xz
NoSource:	0
Source1:	%{fontname}-fontconfig-pgothic.conf
Source2:	%{fontname}-fontconfig-gothic.conf
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	fontpackages-devel

Requires:	%{name}-common = %{version}-%{release}
Obsoletes:	VLGothic-fonts < 20090422-1m
Provides:	VLGothic-fonts = %{version}-%{release}
%description
%common_desc

This package provides the monospace VLGothic font.

%package	common
Summary:	Common files for VLGothic Japanese TrueType fonts
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description	common
%common_desc

This package consists of files used by other %{name} packages.

%package -n	%{fontname}-p-fonts
Summary:	Proportional Japanese TrueType font
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}
Obsoletes:	VLGothic-fonts-proportional < 20090422-1m
Provides:	VLGothic-fonts-proportional = %{version}-%{release}

%description -n	%{fontname}-p-fonts
%common_desc

This package provides the VLGothic font with proportional glyphs for some
non-Japanese characters.

%prep
%setup -q -n VLGothic


%build
%{nil}


%install
rm -rf $RPM_BUILD_ROOT

install -m 0755 -d $RPM_BUILD_ROOT%{_fontdir}
install -m 0644 -p *.ttf $RPM_BUILD_ROOT%{_fontdir}

install -m 0755 -d	$RPM_BUILD_ROOT%{_fontconfig_templatedir} \
	 		$RPM_BUILD_ROOT%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} $RPM_BUILD_ROOT%{_fontconfig_templatedir}/%{pfontconf}.conf
install -m 0644 -p %{SOURCE2} $RPM_BUILD_ROOT%{_fontconfig_templatedir}/%{fontconf}.conf

for fconf in %{pfontconf}.conf %{fontconf}.conf; do
	ln -s %{_fontconfig_templatedir}/$fconf $RPM_BUILD_ROOT%{_fontconfig_confdir}/$fconf
done


%clean
rm -rf ${RPM_BUILD_ROOT}


%files common
%defattr(0644, root, root, 0755)
%doc README* LICENSE*
%dir %{_fontdir}

%_font_pkg -f %{fontconf}.conf VL-Gothic-Regular.ttf

%_font_pkg -n p -f %{pfontconf}.conf VL-PGothic-Regular.ttf


%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (20140530-1m)
- update 20140530

* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20120629-2m)
- fix Fontconfig warning

* Sat Aug  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (20120629-1m)
- update 20120629

* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (20120610-1m)
- update 20120610

* Wed Apr 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (20120325-1m)
- update 20120325

* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (20120312-1m)
- update 20120312

* Thu Mar  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (20120102-1m)
- update 20120130

* Sat Nov 26 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (20111122-1m)
- update 20111122

* Thu Jul 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110722-1m)
- update 20110722

* Thu May 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110414-1m)
- update 20110414

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20101022-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20101022-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (20101022-1m)
- update 20101022

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100416-3m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100416-2m)
- sync with Rawhide (20100416-3)
-- the priority of gothic is 65-1
-- the priority of pgothic is 65-0

* Mon May 24 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100416-1m)
- update to 20100416

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100126-1m)
- update to 20100126

* Thu Dec 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (20091214-1m)
- update to 20091214

* Mon Nov 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (20091127-1m)
- update to 20091127

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090811-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090811-1m)
- update to 20090811

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090710-1m)
- update to 20090710

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090612-1m)
- update to 20090612

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090422-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090422-2m)
- rebuild against rpm-4.7.0-7m

* Sat Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20090422-1m)
- sync Fedora

* Fri Feb  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090204-1m)
- update to 20090204
- License: "mplus" and Modified BSD

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (20090108-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090108-2m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (20090108-1m)
- update 20090108

* Mon Dec  8 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (20081203-1m)
- update 20081203

* Mon Nov 24 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20081124-1m)
- update 20081124

* Wed Oct 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20081029-1m)
- update 20081029

* Mon Sep  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20080908-1m)
- update 20080908

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20080705-2m)
- move VL fonts configurations to fontconfig

* Sat Jul  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20080705-1m)
- update to 20080705

* Sat Jun 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20080610-1m)
- update to 20080610

* Mon May 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20080429-1m)
- update to 20080429

* Mon Apr 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20080420-1m)
- update to 20080420
- remove japanese description

* Sun Apr 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20080327-1m)
- update to 20080327

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (20071215-1m)
- import from Fedora

* Thu Jan 17 2008 Jens Petersen <petersen@redhat.com> - 20071215-2
- move monospace font to main package and obsolete monospace subpackage
- rename sans subpackage to proportional and obsolete sans subpackage
- use a separate font dir for the proportional font subpackage
- add fc-cache scriptlets
- drop the docs subpackage
- use fontname, fontdir, and fontconfdir macros
- improve summaries and descriptions
- do not require fontconfig
- drop VLGothic obsoletes and provides

* Sat Jan 12 2008 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20071215-1
- Update to 20071215

* Thu Oct 18 2007 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20071015-2
- Rename the font directory.
- Fix font selection problem in Flash 9.
- Make it remove the old configuration files on updating.

* Thu Oct 18 2007 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20071015-1
- Update to 20071015
- Make it separated into subpackages

* Sat Sep 09 2007 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20070901-1
- Update to 20070901

* Sat Jun 02 2007 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20070507-1
- Update to 20070507

* Sun Apr 22 2007 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20070328-1
- Update to 20070328

* Wed Jan 03 2007 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20070101-1
- Update to 20070101

* Sun Dec 10 2006 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20061026-5
- Decrease the priority of the VLGothic fonts lower than DejaVu fonts.
- Now config files are replaced by every updating.

* Wed Nov 29 2006 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20061026-4
- Fix the mistyped dist tag.

* Sat Nov 18 2006 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20061026-3
- Modify the specfile along with the Fedora Extras packaging policy.

* Sun Nov 12 2006 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20061026-2
- Modify the specfile.

* Sun Nov 12 2006 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20061026-1
- Preparing for Fedora Extras.

* Sat Oct 28 2006 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20061021-2
- Update to 20061021.

* Tue Sep 19 2006 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20060913-2
- Update to 20060913.

* Thu Aug 31 2006 Ryo Dairiki <ryo-dairiki@users.sourceforge.net> - 20060831-1
- Initial packaging.
