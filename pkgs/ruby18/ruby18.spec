%global momorel 1
%global real_name ruby

Name:		ruby18

# specopt
%{?include_specopt}
%{?!with_dbm_backend_type:      %global with_dbm_backend_type   qdbm}

%if %(if [ "%{with_dbm_backend_type}" = "qdbm" ]; then echo 1; else echo 0; fi)
%global with_dbm_backend_qdbm 1
%else
%global with_dbm_backend_qdbm 0
%endif

%if %(if [ "%{with_dbm_backend_type}" = "gdbm" ]; then echo 1; else echo 0; fi)
%global with_dbm_backend_gdbm 1
%else
%global with_dbm_backend_gdbm 0
%endif

%if %(if [ "%{with_dbm_backend_type}" = "db" ]; then echo 1; else echo 0; fi)
%global with_dbm_backend_db 1
%else
%global with_dbm_backend_db 0
%endif

%define manver		1.4.6
%define	rubyxver	1.8
%define	rubyver		1.8.7
%define _patchlevel	374
%define dotpatchlevel	%{?_patchlevel:.%{_patchlevel}}
%define patchlevel	%{?_patchlevel:-p%{_patchlevel}}
%define	sitedir		%{_libdir}/ruby/site_ruby
# This is required to ensure that noarch files puts under /usr/lib/... for
# multilib because ruby library is installed under /usr/{lib,lib64}/ruby anyway.
%define	sitedir2	%{_prefix}/lib/ruby/site_ruby

Version:	%{rubyver}%{?dotpatchlevel}
Release:	%{momorel}m%{?dist}
License:	Ruby or GPLv2
URL:		http://www.ruby-lang.org/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	compat-readline5-devel
BuildRequires:	ncurses ncurses-devel gdbm gdbm-devel glibc-devel tcl-devel tk-devel libX11-devel autoconf gcc unzip openssl-devel >= 1.0.0 db4-devel byacc
BuildRequires:  momonga-rpmmacros >= 20070618

Source0: http://ftp.ruby-lang.org/pub/%{real_name}/%{rubyxver}/%{real_name}-%{rubyver}%{?patchlevel}.tar.bz2 
NoSource: 0
##Source1:	ftp://ftp.ruby-lang.org/pub/%{real_name}/doc/%{real_name}-man-%{manver}.tar.gz
Source1:	%{real_name}-man-%{manver}.tar.bz2
Source2:	http://www7.tok2.com/home/misc/files/%{real_name}/%{real_name}-refm-rdp-1.8.1-ja-html.tar.gz
##Source3:	ftp://ftp.ruby-lang.org/pub/%{real_name}/doc/rubyfaq-990927.tar.gz
Source3:	rubyfaq-990927.tar.bz2
##Source4:	ftp://ftp.ruby-lang.org/pub/%{real_name}/doc/rubyfaq-jp-990927.tar.gz
Source4:	rubyfaq-jp-990927.tar.bz2
Source5:	irb.1

Patch1:		ruby-deadcode.patch
Patch20:	ruby-rubyprefix.patch
Patch21:	ruby-deprecated-sitelib-search-path.patch
Patch22:	ruby-deprecated-search-path.patch
Patch23:	ruby-multilib.patch
Patch100:	ruby-1.8.7-dl-redirect.patch

Summary:	An interpreter of object-oriented scripting language
Group:		Development/Languages
Requires:	%{name}-libs = %{version}-%{release}
Provides:	%{_bindir}/ruby18
Obsoletes: 	ruby-xemacs

%description
Ruby is the interpreted scripting language for quick and easy
object-oriented programming.  It has many features to process text
files and to do system management tasks (as in Perl).  It is simple,
straight-forward, and extensible.


%package libs
Summary:	Libraries necessary to run Ruby
Group:		Development/Libraries
Provides:	ruby(abi) = %{rubyxver}
Provides:	libruby = %{version}-%{release}
Obsoletes:	libruby <= %{version}-%{release}

%description libs
This package includes the libruby, necessary to run Ruby.


%package devel
Summary:	A Ruby development environment
Group:		Development/Languages
Requires:	%{name}-libs = %{version}-%{release}

%description devel
Header files and libraries for building a extension library for the
Ruby or an application embedded Ruby.


%package tcltk
Summary:	Tcl/Tk interface for scripting language Ruby
Group:		Development/Languages
Requires:	%{name}-libs = %{version}-%{release}

%description tcltk
Tcl/Tk interface for the object-oriented scripting language Ruby.


%package irb
Summary:	The Interactive Ruby
Group:		Development/Languages
Requires:	%{name} = %{version}-%{release}
Provides:	irb = %{version}-%{release}
Obsoletes:	irb <= %{version}-%{release}

%description irb
The irb is acronym for Interactive Ruby.  It evaluates ruby expression
from the terminal.


%package rdoc
Summary:	A tool to generate documentation from Ruby source files
Group:		Development/Languages
Requires:	%{name} = %{version}-%{release}
Requires:	%{name}-irb = %{version}-%{release}
Provides:	rdoc = %{version}-%{release}
Obsoletes:	rdoc <= %{version}-%{release}

%description rdoc
The rdoc is a tool to generate the documentation from Ruby source files.
It supports some output formats, like HTML, Ruby interactive reference (ri),
XML and Windows Help file (chm).


%package docs
Summary:	Manuals and FAQs for scripting language Ruby
Group:		Documentation

%description docs
Manuals and FAQs for the object-oriented scripting language Ruby.

%package ri
Summary:	Ruby interactive reference
Group:		Documentation
Requires:	%{name} = %{version}-%{release}
Requires:	%{name}-rdoc = %{version}-%{release}
Provides:	ri = %{version}-%{release}
Obsoletes:	ri <= %{version}-%{release}
Provides:	ruby-doc
Obsoletes:	ruby-doc

%description ri
ri is a command line tool that displays descriptions of built-in
Ruby methods, classes and modules. For methods, it shows you the calling
sequence and a description. For classes and modules, it shows a synopsis
along with a list of the methods the class or module implements.


%prep
%setup -q -c -a 1 -a 3 -a 4
mkdir -p ruby-refm-ja
pushd ruby-refm-ja
tar fxz %{SOURCE2}
popd
pushd %{real_name}-%{rubyver}%{?patchlevel}
%patch100 -p1 -b .redirect

#%patch1 -p1
%patch20 -p1
%patch21 -p1
%ifarch ppc64 s390x sparc64 x86_64
%patch22 -p1
%patch23 -p1
%endif
popd

%build
pushd %{real_name}-%{rubyver}%{?patchlevel}
for i in config.sub config.guess; do
	test -f %{_datadir}/libtool/$i && cp %{_datadir}/libtool/$i .
done
autoconf

rb_cv_func_strtod=no
export rb_cv_func_strtod
CFLAGS="$RPM_OPT_FLAGS -Wall "
export CFLAGS
%configure \
  --with-sitedir='%{sitedir}' \
  --with-default-kcode=none \
  --with-bundled-sha1 \
  --with-bundled-md5 \
  --with-bundled-rmd160 \
  --enable-shared \
  --enable-ipv6 \
%ifarch ppc
  --disable-pthread \
%else
  --enable-pthread \
%endif
  --with-dbm-type=%{with_dbm_backend_type} \
  --with-lookup-order-hack=INET \
  --disable-rpath \
  --with-readline-include=%{_includedir}/readline5 \
  --with-readline-lib=%{_libdir}/readline5 \
  --with-vendordir=%{_prefix}/lib/ruby/1.8 \
  --with-ruby-prefix=%{_prefix}/lib \
  --program-suffix=18

%ifarch ppc
cp Makefile Makefile.orig
sed -e 's/^EXTMK_ARGS[[:space:]].*=\(.*\) --$/EXTMK_ARGS=\1 --disable-tcl-thread --/' Makefile.orig > Makefile
%endif
make RUBY_INSTALL_NAME=ruby18 %{?_smp_mflags}
%ifarch ia64
# Miscompilation? Buggy code?
rm -f parse.o
make OPT=-O0 RUBY_INSTALL_NAME=ruby18 %{?_smp_mflags}
%endif

popd

%check
pushd %{real_name}-%{rubyver}%{?patchlevel}
%ifnarch ppc64
make test
%endif
popd

%install
rm -rf $RPM_BUILD_ROOT

# installing documents and exapmles...
mkdir tmp-ruby-docs
cd tmp-ruby-docs

# for ruby.rpm
mkdir ruby ruby-libs ruby-devel ruby-tcltk ruby-docs irb
cd ruby
(cd ../../%{real_name}-%{rubyver}%{?patchlevel} && tar cf - sample) | tar xvf -
cd ..

# for ruby-libs
cd ruby-libs
(cd ../../%{real_name}-%{rubyver}%{?patchlevel} && tar cf - lib/README*) | tar xvf -
(cd ../../%{real_name}-%{rubyver}%{?patchlevel}/doc && tar cf - .) | tar xvf -
(cd ../../%{real_name}-%{rubyver}%{?patchlevel} &&
 tar cf - `find ext \
  -mindepth 1 \
  \( -path '*/sample/*' -o -path '*/demo/*' \) -o \
  \( -name '*.rb' -not -path '*/lib/*' -not -name extconf.rb \) -o \
  \( -name 'README*' -o -name '*.txt*' -o -name 'MANUAL*' \)`) | tar xvf -
cd ..

# for irb
cd irb
mv ../ruby-libs/irb/* .
rmdir ../ruby-libs/irb
cd ..

# for ruby-devel
cd ruby-devel

cd ..

# for ruby-tcltk
cd ruby-tcltk
for target in tcltklib tk
do
 (cd ../ruby-libs &&
  tar cf - `find . -path "*/$target/*"`) | tar xvf -
 (cd ../ruby-libs &&
  rm -rf `find . -name "$target" -type d`)
done
cd ..

# for ruby-docs
cd ruby-docs
mkdir doc-en refm-ja faq-en faq-ja
(cd ../../ruby-man-`echo %{manver} | sed -e 's/\.[0-9]*$//'` && tar cf - .) | (cd doc-en && tar xvf -)
(cd ../../ruby-refm-ja && tar cf - .) | (cd refm-ja && tar xvf -)
(cd ../../rubyfaq && tar cf - .) | (cd faq-en && tar xvf -)
(cd ../../rubyfaq-jp && tar cf - .) | (cd faq-ja && tar xvf -)

(cd faq-ja &&
 for f in rubyfaq-jp*.html
 do
  sed -e 's/\(<a href="rubyfaq\)-jp\(\|-[0-9]*\)\(.html\)/\1\2\3/g' \
   < $f > `echo $f | sed -e's/-jp//'`
  rm -f $f; \
 done)
#"
# make sure that all doc files are the world-readable
find -type f | xargs chmod 0644

cd ..

# fixing `#!' paths
for f in `find . -type f`
do
  sed -e 's,^#![ 	]*\([^ 	]*\)/\(ruby18\|with\|perl\|env\),#!/usr/bin/\2,' < $f > $f.n
  if ! cmp $f $f.n
  then
    mv -f $f.n $f
  else
    rm -f $f.n
  fi
done

# done
cd ..

# installing binaries ...
make -C $RPM_BUILD_DIR/%{name}-%{version}/%{real_name}-%{rubyver}%{?patchlevel} DESTDIR=$RPM_BUILD_ROOT install

_cpu=`echo %{_target_cpu} | sed 's/^ppc/powerpc/'`

# generate ri doc
rubybuilddir=$RPM_BUILD_DIR/%{name}-%{version}/%{real_name}-%{rubyver}%{?patchlevel}
LD_LIBRARY_PATH=$RPM_BUILD_ROOT%{_libdir} RUBYLIB=$RPM_BUILD_ROOT%{_libdir}/ruby/%{rubyxver}:$RPM_BUILD_ROOT%{_libdir}/ruby/%{rubyxver}/$_cpu-%{_target_os} make -C $rubybuilddir DESTDIR=$RPM_BUILD_ROOT install-doc
#DESTDIR=$RPM_BUILD_ROOT LD_LIBRARY_PATH=$RPM_BUILD_ROOT%{_libdir} $RPM_BUILD_ROOT%{_bindir}/ruby18 -I $rubybuilddir -I $RPM_BUILD_ROOT%{_libdir}/ruby/%{rubyxver}/$_cpu-%{_target_os}/ -I $rubybuilddir/lib $RPM_BUILD_ROOT%{_bindir}/rdoc --all --ri-system $rubybuilddir

%{__mkdir_p} $RPM_BUILD_ROOT%{sitedir2}/%{rubyxver}
%{__mkdir_p} $RPM_BUILD_ROOT%{sitedir}/%{rubyxver}/$_cpu-%{_target_os}

%ifarch ppc64 s390x sparc64 x86_64
# correct archdir
#mv $RPM_BUILD_ROOT%{_prefix}/lib/ruby/%{rubyxver}/$_cpu-%{_target_os}/* $RPM_BUILD_ROOT%{_libdir}/ruby/%{rubyxver}/$_cpu-%{_target_os}/
#rmdir $RPM_BUILD_ROOT%{_prefix}/lib/ruby/%{rubyxver}/$_cpu-%{_target_os}
%endif

# XXX: installing irb
install -m 0644 %{SOURCE5} $RPM_BUILD_ROOT%{_mandir}/man1/irb18.1

# listing all files in ruby-all.files
(find $RPM_BUILD_ROOT -type f -o -type l) |
 sort | uniq | sed -e "s,^$RPM_BUILD_ROOT,," \
                   -e "s,\(/man/man./.*\)$,\1*," > ruby-all.files
egrep '(\.[ah]|libruby\.so)$' ruby-all.files > ruby-devel.files

_rubytmpfile=`mktemp -t %{name}-%{version}-%{release}-tmp.XXXXXXXXXX`
# for ruby-tcltk.rpm
cp /dev/null ruby-tcltk.files
for f in `find %{real_name}-%{rubyver}%{?patchlevel}/ext/tk/lib -type f; find %{real_name}-%{rubyver}%{?patchlevel}/.ext -type f -name '*.so'; find %{real_name}-%{rubyver}%{?patchlevel}/ext/tk -type f -name '*.so'`
do
  egrep "tcl|tk" ruby-all.files | grep "/`basename $f`$" >> ruby-tcltk.files || :
done
sort ruby-tcltk.files | uniq - $_rubytmpfile && mv $_rubytmpfile ruby-tcltk.files

# for irb.rpm
fgrep 'irb' ruby-all.files | fgrep -v '%{_datadir}/ri' > irb.files

# for ri
cp /dev/null ri.files
fgrep '%{_datadir}/ri' ruby-all.files >> ri.files
fgrep '%{_bindir}/ri' ruby-all.files >> ri.files

# for rdoc
cp /dev/null rdoc.files
fgrep rdoc ruby-all.files >> rdoc.files

# for ruby-libs
cp /dev/null ruby-libs.files
(fgrep    '%{_prefix}/lib' ruby-all.files; 
 fgrep -h '%{_prefix}/lib' ruby-devel.files ruby-tcltk.files irb.files ri.files rdoc.files) | egrep -v "elc?$" | \
 sort | uniq -u > ruby-libs.files

# for ruby.rpm
sort ruby-all.files \
 ruby-libs.files ruby-devel.files ruby-tcltk.files irb.files ri.files rdoc.files | 
 uniq -u > ruby.files

# for arch-dependent dir
rbconfig=`find $RPM_BUILD_ROOT -name rbconfig.rb`
export LD_LIBRARY_PATH=$RPM_BUILD_ROOT%{_libdir}
arch=`$RPM_BUILD_ROOT%{_bindir}/ruby18 -r $rbconfig -e 'puts Config::CONFIG["arch"]'`
cat <<__EOF__ >> ruby-libs.files
%%dir %%{_libdir}/ruby/%%{rubyxver}/$arch
%%dir %%{_libdir}/ruby/%%{rubyxver}/$arch/digest
__EOF__

%clean
rm -rf $RPM_BUILD_ROOT
rm -f *.files
rm -rf tmp-ruby-docs

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files -f ruby.files
%defattr(-, root, root)
%doc %{real_name}-%{rubyver}%{?patchlevel}/COPYING*
%doc %{real_name}-%{rubyver}%{?patchlevel}/ChangeLog
%doc %{real_name}-%{rubyver}%{?patchlevel}/GPL
%doc %{real_name}-%{rubyver}%{?patchlevel}/LEGAL
%doc %{real_name}-%{rubyver}%{?patchlevel}/LGPL
%doc %{real_name}-%{rubyver}%{?patchlevel}/NEWS 
%doc %{real_name}-%{rubyver}%{?patchlevel}/README
%lang(ja) %doc %{real_name}-%{rubyver}%{?patchlevel}/README.ja
%doc %{real_name}-%{rubyver}%{?patchlevel}/ToDo 
%doc %{real_name}-%{rubyver}%{?patchlevel}/doc/ChangeLog-1.8.0
%doc %{real_name}-%{rubyver}%{?patchlevel}/doc/NEWS-1.8.0
%doc tmp-ruby-docs/ruby/*

%files devel -f ruby-devel.files
%defattr(-, root, root)
%doc %{real_name}-%{rubyver}%{?patchlevel}/README.EXT
%lang(ja) %doc %{real_name}-%{rubyver}%{?patchlevel}/README.EXT.ja

%files libs -f ruby-libs.files
%defattr(-, root, root)
%doc %{real_name}-%{rubyver}%{?patchlevel}/README
%lang(ja) %doc %{real_name}-%{rubyver}%{?patchlevel}/README.ja
%doc %{real_name}-%{rubyver}%{?patchlevel}/COPYING*
%doc %{real_name}-%{rubyver}%{?patchlevel}/ChangeLog
%doc %{real_name}-%{rubyver}%{?patchlevel}/GPL
%doc %{real_name}-%{rubyver}%{?patchlevel}/LEGAL
%doc %{real_name}-%{rubyver}%{?patchlevel}/LGPL
#%dir %{_libdir}/ruby
#%dir %{_prefix}/lib/ruby
%dir %{_libdir}/ruby/%{rubyxver}
%dir %{_prefix}/lib/ruby/%{rubyxver}
%dir %{_prefix}/lib/ruby/%{rubyxver}/cgi
%dir %{_prefix}/lib/ruby/%{rubyxver}/net
%dir %{_prefix}/lib/ruby/%{rubyxver}/shell
%dir %{_prefix}/lib/ruby/%{rubyxver}/uri
%{sitedir}/%{rubyxver}
%{sitedir2}/%{rubyxver}

%files tcltk -f ruby-tcltk.files
%defattr(-, root, root)
%doc tmp-ruby-docs/ruby-tcltk/ext/*

%files rdoc -f rdoc.files
%defattr(-, root, root)

%files irb -f irb.files
%defattr(-, root, root)
%doc tmp-ruby-docs/irb/*
%dir %{_prefix}/lib/ruby/%{rubyxver}/irb
%dir %{_prefix}/lib/ruby/%{rubyxver}/irb/lc
%dir %{_prefix}/lib/ruby/%{rubyxver}/irb/lc/ja

%files ri -f ri.files
%defattr(-, root, root)
%dir %{_datadir}/ri

%files docs
%defattr(-, root, root)
%doc tmp-ruby-docs/ruby-docs/*
%doc tmp-ruby-docs/ruby-libs/*

%changelog
* Sat Jun 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.7.374-1m)
- [SECURITY] fix SSL man-in-the-middle vulnerability
- update to 1.8.7p374

* Sat Dec 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.352-1m)
- update 1.8.7-p357

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.7.352-2m)
- delete elisp-ruby-mode package. it is already included in emacs-common

* Tue Aug  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.352-1m)
- [SECURITY] CVE-2011-2686 CVE-2011-2705 CVE-2011-3009
- update 1.8.7-p352

* Mon Jun 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.334-3m)
- fix build error 
-- trunk-env broken redirect? Umm...

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.7.334-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.334-1m)
- update 1.8.7p334
-- fixed two security issue

* Sun Jan  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.330-1m)
- update 1.8.7p330

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.7.302-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.7.302-2m)
- full rebuild for mo7 release

* Tue Aug 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.302-1m)
- update 1.8.7p302
- [SECURITY] CVE-2010-0541

* Tue Aug 10 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.8.7.299-5m)
- deleted some provided directories

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.299-4m)
- rename irb.1 to irb18.1

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.299-3m)
- ruby18-libs provides ruby(abi) = 1.8

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.299-2m)
- Provide: ruby(abi)-1.8

* Fri Jun 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.299-1m)
- update to 1.8.7-p299
-- merged openssl100 patch

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.249-8m)
- rebuild against emacs-23.2

* Wed May 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.249-7m)
- use compat-readline5 instead of readline6 since Ruby's license is
  incompatible with GPLv3
-- http://redmine.ruby-lang.org/issues/show/2032

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.7.249-6m)
- rebuild against readline6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.249-5m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.249-4m)
- remove ruins of xemacs

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.249-3m)
- apply openssl100 patch

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.249-2m)
- rename ruby-emacs to elisp-ruby-mode
- kill ruby-xemacs

* Sun Jan 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.249-1m)
- [SECURITY] CVE-2009-4492
- update to 1.8.7-p249

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.248-1m)
- update 1.8.7p248

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.174-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.174-5m)
- License: Ruby or GPLv2
-- see http://redmine.ruby-lang.org/issues/show/2000

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.7.174-4m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.7.174-3m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.174-2m)
- rebuild against emacs-23.0.95

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.174-1m)
- [SECURITY] CVE-2009-1904
- update to 1.8.7-p174

* Thu Jun 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.173-1m)
- [SECURITY] CVE-2009-0642
- update 1.8.7p173

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.160-3m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.160-2m)
- rebuild against xemacs-21.5.29

* Fri Apr 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.160-1m)
- update 1.8.7p160
-- CVE-2007-1558 

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.72-6m)
- rebuild against openssl-0.9.8k

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.72-5m)
- rebuild against emacs-23.0.92

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.72-4m)
- update Patch22 for fuzz=0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.72-3m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.7.72-2m)
- rebuild against db4-4.7.25-1m

* Wed Aug 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.72-1m)
- [SECURITY] CVE-2008-1447 CVE-2008-3443 CVE-2008-3655
- [SECURITY] CVE-2008-3656 CVE-2008-3657 CVE-2008-3905
- update 1.8.7p72

* Fri Aug  8 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7.71-1m)
- [SECURITY] CVE-2008-2376
- update 1.8.7p71

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7.22-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Thu Jun 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.7.22-3m)
- x86_64 work fine

* Sat Jun 21 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.8.7.22-1m)
- up to 1.8.7-p22
- [SECURITY] CVE-2008-2662, CVE-2008-2663, CVE-2008-2725, CVE-2008-2726, CVE-2008-2664

* Fri Jun 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.7.17-2m)
- remake any patch

* Mon Jun 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.7.17-1m)
- update 1.8.7-p17

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.6-114-4m)
- rebuild against opnssl-0.9.8h-1m

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.8.6.114-3m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.6.114-2m)
- rebuild against gcc43

* Tue Mar  4 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.8.6.114-1m)
- [SECURITY] http://preview.ruby-lang.org/en/news/2008/03/03/webrick-file-access-vulnerability/
             http://www.ruby-lang.org/ja/news/2008/03/03/webrick-file-access-vulnerability/

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.6.111-6m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.6.111-5m)
- rebuild against qdbm-1.8.77

* Sat Nov 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.6.111-4m)
- [SECURITY] CVE-2007-5770
- Net::ftptls, Net::telnets, Net::imap, Net::pop, Net::smtp are also affected this issue
- patch was made from http://svn.ruby-lang.org/cgi-bin/viewvc.cgi?view=rev&revision=13656

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.6.111-3m)
- rebuild against db4-4.6.21

* Fri Oct  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.6.111-2m)
- update Patch22: ruby-deprecated-search-path.patch

* Thu Oct  4 2007 zunda <zunda at freeshell.org>
- (1.8.6.111-1m)
- [SECURITY] http://www.isecpartners.com/advisories/2007-006-rubyssl.txt
- Updated patch level to 1.8.6-p111
- Modified last hunk of Patch21 ruby-deprecated-sitelib-search-path.patch
  for ruby.c
- Simplifed detection of arch for arch-dependent dir

* Tue Jul 31 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.6-8m)
- remove %%(%%{__id_u -n}) at _rubytmpfile, for updatespecdb error

* Tue Jun 19 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.6-7m)
- elisp-ruby-mode.spec was merged into this package

* Tue Jun 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.6-6m)
- fix %%install to avoid conflicting

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.6-5m)
- modify Provides and Obsoletes
- ruby-irb is still conflicting with ruby-ri

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.6-4m)
- synchronized with Fedora

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.6-3m)
- rebuild against qdbm-1.8.75

* Wed Mar 14 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.6-2m)
- revise Patch100: ruby-1.8.6-tk-multilib.patch

* Tue Mar 13 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.8.6-1m)
- version up

* Tue Jan 16 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.8.5-1m)
- [SECURITY] CVE-2006-5467 CVE-2006-6303
- version up 1.8.5-p12
- remove patch1~5

* Wed Aug 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-13m)
- rebuild against qdbm-1.8.68

* Sat Aug 12 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.4-12m)
- {SECURITY] CVE-2006-3694
- add Patch2: ruby-1.8.4-fix-insecure-dir-operation.patch (JVN13947696)
- add Patch3: ruby-1.8.4-fix-alias-safe-level.patch (JVN83768862)
- add Patch4: ruby-1.8.4-fix-insecure-regexp-modification.patch (from FC)
- add Patch5: ruby-fix-autoconf-magic-code.patch (from FC)

* Tue Jul 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-11m)
- rebuild against qdbm-1.8.61

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.4-10m)
- rebuild against readline-5.0

* Sun Jun 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-9m)
- rebuild against qdbm-1.8.59

* Thu May 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-8m)
- rebuild against qdbm-1.8.56

* Mon Apr 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-7m)
- rebuild against qdbm-1.8.48

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.4-6m)
- rebuild against openssl-0.9.8a

* Thu Mar 09 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-5m)
- rebuild against qdbm-1.8.46

* Mon Mar  6 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.8.4-4m)
- add BuildPrereq: gperf
- enable ppc, sparc

* Tue Feb 28 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.4-3m)
- apply ruby-1.8.4-eaccess.patch to avoid redefining posix function eaccess

* Thu Feb 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-2m)
- rebuild against qdbm-1.8.45

* Sat Dec 24 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.4-1m)
- version 1.8.4

* Thu Dec 22 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.4-0.3.1m)
- 1.8.4-preview3

* Sun Dec 11 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-0.2.4m)
- rebuild against qdbm-1.8.35

* Wed Dec  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.4-0.2.3m)
- apply ruby_1_8.patch for bug fixes

* Tue Dec  6 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4-0.2.2m)
- remake tk multilib patch
- Patch100: ruby-1.8.4-pre2-tk-multilib.patch

* Sat Dec  3 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.4-0.2.1m)
- 1.8.4-preview2

* Mon Oct 31 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.4-0.1.1m)
- 1.8.4-preview1

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.8.3-4m)
- x86_64 tcltklib enabled

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (kossori)
- sorry spec miss...

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.8.3-3m)
- enable ppc64, alpha, ia64, mipsel, sparc64

* Fri Sep 30 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.3-2m)
- apply 'ruby-1.8.3-use-alloca.patch'
  http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-core/6083

* Tue Sep 20 2005 zunda <zunda at freeshell.org>
- (kossori)
- updated md5sum for source0

* Tue Sep 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.3-1m)
- [SECURITY] JVN#62914675 <http://jvn.jp/jp/JVN%2362914675/>
- remove 'ruby-1.8.2-xmlrpc.patch' since it has been merged

* Thu Sep 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.3-0.1.6m)
- rebuild against qdbm-1.8.33

* Tue Jun 28 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.8.3-0.1.5m)
- [SECURITY] CAN-2005-1992

* Wed Jun 15 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.3-0.1.4m)
- use %%configure macro again
- invoke 'autoconf'
- use '/usr/lib' and '/usr/local/lib' in x86_64

* Tue Jun 14 2005 zunda <zunda at freeshell.org>
- (kossori)
- -N option should not have been specified on OmoiKondara to prevent
  dropping of `-preview'. sumaso.

* Tue Jun  8 2005 zunda <zunda at freeshell.org>
- (1.8.3-0.1.3m)
- added a sub-package ruby-doc- including files installed by make
  install-doc
- less macro in the NoSource line: OmoiKondara and ruby-rpm-1.2.0-16m
  drops the string `-preview' from the following:
  %{?preview:-preview%{preview}}

* Tue Jun  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.3-0.1.2m)
- rebuild against qdbm-1.8.29
- not use %%configure macro

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.3-0.1.1m)
- 1.8.3-preview1

* Fri Mar 18 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.8.2-9m)
- replaced %%{_builddir} to /usr/src in rbconfig.rb

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.8.2-8m)
- rebuild against libtermcap and ncurses

* Tue Feb  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-7m)
- rebuild against qdbm-1.8.21

* Fri Jan 21 2005 mutecat <mutecat@momonga-linux.org>
- (1.8.2-6m)
- arrange ppc change ruby_arch

* Mon Jan 17 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-5m)
- delete symlink directories

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (1.8.2-4m)
- enable x86_64.

* Thu Jan  6 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-3m)
- rebuild against qdbm-1.8.20

* Sun Dec 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.2-2m)
- changed source0 md5sum (2004-12-25 version tarball)

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-1m)
- version 1.8.2

* Wed Dec 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-0.4.1m)
- 1.8.2-preview4

* Sun Nov 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-0.3.4m)
- rebuild against qdbm-1.8.18

* Thu Nov 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-0.3.3m)
- version 1.8.2 (2004-11-11)
- fix more bugs

* Thu Nov 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-0.3.2m)
- version 1.8.2 (2004-11-10)

* Mon Nov  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-0.3.1m)
- 1.8.2-preview3

* Thu Aug 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-0.2.3m)
- rebuild against qdbm-1.8.15

* Sun Aug  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-0.2.2m)
- rebuild against qdbm-1.8.14
- rebuild against tcl/tk w/ pthread support

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.2-0.2.1m)
- 1.8.2preview1
- Obsoletes: ruby-bigfloat (use BigDecimal in ruby package instead)

* Mon Jun 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.1-11m)
- rebuild against tcl, tk

* Sun Apr 18 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.8.1-10m)
- add ruby-1.8.1-relax-version-checking.patch

* Thu Apr 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.1-9m)
- add patch1 (bug report #28)

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (1.8.1-8m)
- add defattr to tcltk sub package.

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (1.8.1-7m)
- Not only *.rb contain local/bin.

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.8.1-6m)
- revised spec for rpm 4.2.

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.1-5m)
- specfile cleanup

* Thu Jan 29 2004 mutecat <mutecat@momonga-linux.org>
- (1.8.1-4m)
- add ifarch ppc.

* Wed Jan 28 2004 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-3m)
- rebuild against qdbm.

* Fri Dec 26 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-2m)
- [ruby-core:02071].

* Thu Dec 25 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-1m)
- version 1.8.1.

* Wed Dec 24 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-0.4.3m)
- [ruby-dev:22360].

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.1-0.4.2m)
- [ruby-dev:22326].

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.1-0.4.1m)
- 1.8.1preview4

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.1-0.3.3m)
- accept gdbm-1.8.0 or newer

* Fri Dec 18 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-0.3.2m)
- apply patch [ruby-list:38884].

* Sat Dec 06 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-0.3.1m)
- version up.

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.1-0.2.4m)
- rebuild against gdbm

* Sat Nov 01 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-0.2.3m)
- patch for [ruby-list:38706].

* Fri Oct 31 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-0.2.2m)
- add filelist.

* Thu Oct 30 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.1-0.2.1m)
- version up.

* Sat Aug 23 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.8.0-4m)
- fix typo in %%prep
   samples of tk are now included.

* Tue Aug 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.0-3m)
- add ruby-cvs.patch for bugfixes

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.0-2m)
- merge from ruby-1_8-branch.

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.0-1m)
- version up.
- specoptized dbm backend type as with_dbm_backend_type (default qdbm).

* Sun Aug 03 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.0-0.7.2m)
- apply patch to support gtk2 testrunner for test/unit.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.0-0.7.1m)
- version up.

* Fri Aug 01 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.0-0.6.4m)
- append some provides and obsolete.
- remove conflicts.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.0-0.6.3m)
- modify filelist.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.0-0.6.2m)
- add Conflicts.
- add documents.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.0-0.6.1m)
- version up.

* Mon Apr 14 2003 Kenta MURATA <muraken2@nifty.com>
- (1.6.8-6m)
- add BuildPreReq: db4-devel.
- use db4 for dbm.so.

* Mon Apr 14 2003 Kenta MURATA <muraken2@nifty.com>
- (1.6.8-5m)
- BuildPreReq: ncurses-devel, gdbm-devel, readline-devel.
- modify configure arguments.

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.6.8-4m)
- rebuild against for XFree86-4.3.0

* Sat Mar 22 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.6.8-3m)
- add --enable-ipv6 to configure if %%{_ipv6} is 1

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.6.8-2m)
- rebuild against for gdbm

* Sat Dec 24 2002 Kenta MURATA <muraken2@nifty.com>
- (1.6.8-1m)
- version up.

* Sat Dec 21 2002 Kenta MURATA <muraken2@nifty.com>
- (1.6.7-11m)
- hold %{_libdir}/ruby/1.6/math directory.

* Tue Aug 04 2002 Kenta MURATA <muraken2@nifty.com>
- (1.6.7-10m)
- append tcl, tk to BuildPreReq.

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.7-9m)
- revise for ppc

* Thu May 23 2002 Kenta MURATA <muraken@kondara.org>
- (1.6.7-8k)
- perl tsuka ttenn ja ne- YO!

* Wed May  1 2002 Kenta MURATA <muraken@kondara.org>
- (1.6.7-6k)
- Provides: /usr/bin/ruby

* Mon Mar  4 2002 Kenta MURATA <muraken@kondara.org>
- (1.6.7-4k)
- [ruby-dev:16182]

* Sat Mar  2 2002 Kenta MURATA <muraken@kondara.org>
- (1.6.7-2k)
- version up to 1.6.7.

* Mon Jan 07 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.6.6-4k)
- Source URL fixed.

* Thu Dec 27 2001 Kenta MURATA <muraken2@nifty.com>
- (1.6.6-2K)
- version up to 1.6.6.

* Sun Dec 15 2001 Toru Hoshina <t@kondara.org>
- (1.6.5-12K)
- site_ruby should be under /usr/local so that user could make "local"
  environment without destroy package dep/req chain.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.6.5-10K)
- nigittenu ... baatari-

* Thu Nov  1 2001 Kenta MURATA <muraken2@nifty.com>
- (1.6.5-8k)
- split ruby-mode-emacsen to elisp-ruby-mode

* Thu Nov  1 2001 Kenta MURATA <muraken2@nifty.com>
- (1.6.5-6k)
- never use %triggerin section
- append BuildPreReq tag

* Thu Oct 25 2001 Kenta MURATA <muraken2@nifty.com>
- (1.6.5-4k)
- marge 'ruby-mode-emacs' package
- make 'ruby-mode-xemacs' package

* Thu Sep 20 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (1.6.5-2k)
  update to 1.6.5

* Mon Jun 4 2001 Toru Hoshina <toru@df-usa.com>
- (1.6.4-2k)
- version 1.6.4

* Wed Mar 28 2001 Kenta MURATA <muraken2@nifty.com>
- (1.6.3-3k)
- version 1.6.3

* Mon Dec 25 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.6.2

* Wed Nov 29 2000 KIM Hyeong Cheol <kim@kondara.org>
- (1.6.1-7k)
- patch for process.c and file.c. (see [ruby-dev:11223])
 
* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- Do not use bzip2/gzip to compress man/info directly.

* Thu Oct 26 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.6.1-3k)
- fixed for bzip2ed man

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- (1.6.1-1k)
- Version 1.6.1.

* Thu Sep 28 2000 KUSUNOKI Masanori <nori@kondara.org>
- (1.6.0-1k)
- Version 1.6.0.

* Wed Sep 27 2000 Toru Hoshina <t@kondara.org>
- remove date2.rb so that ruby-date2 avoid conflict.

* Fri Sep 9 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.93.

* Fri Sep 9 2000 Toru Hoshina <t@kondara.org>
- (1.4.6-2k)
- spec file modified, compiled for Kondara 1.2.

* Thu Aug 17 2000 AYUHANA Tomnonoi <l@kondara.org>
- (1.4.6-1k)
- remove ruby-1.4.5-regexp-bug.patch (fixed)
- obey FHS
- add Requires: filesystem >= 2.0.7-1
- add Docdir: /usr/share/doc
- add --mandir=/usr/share/man at %configure
- change %file against FHS 

* Mon Aug 14 2000 AYUHANA Tomnonoi <l@kondara.org>
- (1.4.5-4k)
- add ruby-1.4.5-regexp-bug.patch (devel.ja:03242)

* Mon Jul  3 2000 AYUHANA Tomonori <l@kondara.org>
- (1.4.5-3k)
- 3rd repack tar ball
- md5sum: 0322a805a8fb7bef23127ad29fa1d537

* Sun Jun 25 2000 Toru Hoshina <t@kondara.org>
- (1.4.5-2k)
- rebuild against rpm 3.0.4 to keep backward compatibility.

* Sun Jun 25 2000 AYUHANA Tomonori <l@kondara.org>
- (1.4.5-1k)
- Version up to 1.4.5
- change /usr/local patch
- remove "ruby-fclose.diff" patch (because fixed)
- add -b at %patch

* Fri May 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Source, BuildRoot )
- Group change ( Applications -> Development )

* Thu Apr 27 2000 Toru Hoshina <t@kondara.org>
- add misc directory under DOC.

* Wed Apr 19 2000 Toru Hoshina <t@kondara.org>
- Version up to 1.4.4 (Official :-P)
- Removed date2.rb so that ruby-date package would be installed.

* Thu Mar 30 2000 Toru Hoshina <t@kondara.org>
- Version up to 1.4.4

* Sun Jan 30 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)

* Thu Jan 13 2000 Takaaki Tabuchi <tab@kondara.org>
- add ifarch sparc

* Wed Dec 8 1999 Daisuke Sato <d@kondara.org>
- version 1.4.3(reset revision)

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Oct 3 1999 Toru Hoshina <hoshina@best.com>
- applied pack patch from ruby-dev ml #7961.

* Sun Sep 26 1999 Toru Hoshina <hoshina@best.com>
- 1.4.2 repacked.

* Thu Sep 16 1999 Toru Hoshina <hoshina@best.com>
- Add barterly symlink for i[3456]86.

* Thu Sep 16 1999 Toru Hoshina <hoshina@best.com>
- Version up to 1.4.2

* Sun Aug 22 1999 Toru Hoshina <hoshina@best.com>
- fixed dbm.so bug.

* Sat Aug 14 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- Version up to 1.4.0

* Fri Aug 6 1999 Toru Hoshina<hoshina@best.com>
- Version up to 1.3.7.

* Wed Jul 28 1999 Toru Hoshina<hoshina@best.com>
- Version up to 1.3.6, this is beta release to 1.4.

* Thu Jul 22 1999 Toru Hoshina<hoshina@best.com>
- Version up to 1.3.5 development version.

* Sun Jun 14 1999 Toru Hoshina<hoshina@best.com>
- Version up to 1.3.4 990611 development version.

* Wed Jun 2 1999 Toru Hoshina<hoshina@best.com>
- Version up to 1.3.4 990531 development version.

* Fri May 14 1999 Toru Hoshina<hoshina@best.com>
- Version up to 1.3.3 990507 development version :-P
