%global momorel 13

Name:           perl-CGI-Simple
Version:        1.113
Release:        %{momorel}m%{?dist}
Summary:        Simple totally OO CGI interface that is CGI.pm compliant
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/CGI-Simple/
Source0:        http://www.cpan.org/authors/id/A/AN/ANDYA/CGI-Simple-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-IO-stringy
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple
Requires:       perl-IO-stringy
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-Cgi-Simple
Provides:       perl-Cgi-Simple = %{version}-%{release}

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
CGI::Simple provides a relatively lightweight drop in replacement for
CGI.pm. It shares an identical OO interface to CGI.pm for parameter
parsing, file upload, cookie handling and header generation. This module is
entirely object oriented, however a complete functional interface is
available by using the CGI::Simple::Standard module.

%prep
%setup -q -n CGI-Simple-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/CGI/Simple.pm
%{perl_vendorlib}/CGI/Simple
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-13m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-12m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-11m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-10m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-9m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.113-2m)
- rebuild for new GCC 4.6

* Tue Dec 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.113-1m)
- [SECURITY] CVE-2010-2761 CVE-2010-4410
- update to 1.113

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.112-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.112-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.112-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.112-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.112-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.112-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.112-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.112-2m)
- rebuild against perl-5.10.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.112-1m)
- update to 1.112

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.111-1m)
- update to 1.111

* Mon May 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.110-1m)
- update to 1.110

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.109-1m)
- update to 1.109

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.108-1m)
- update to 1.108

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.106-2m)
- rebuild against rpm-4.6

* Mon Sep 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.106-1m)
- update to 1.106

* Sun May 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.105-1m)
- update to 1.105

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.103-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.103-2m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.103-1m)
- update to 1.103

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.080-2m)
- use vendor

* Sat Mar 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.080-1m)
- update to 0.080

* Sun Feb 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.079-1m)
- update to 0.079

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.078-2m)
- Provides: perl-Cgi-Simple

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.078-1m)
- package name was changed to perl-CGI-Simple
- change source URI

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.077-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
