%global momorel 8
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global rel_num 4628

Summary: An Emacs major mode for editing Lua code
Name: emacs-lua-mode
Version: 20100617
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
URL: http://luaforge.net/projects/lua-mode/
Source0: http://luaforge.net/frs/download.php/%{rel_num}/lua-mode.el
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: lua-mode-emacs
Obsoletes: lua-mode-xemacs

Obsoletes: elisp-lua-mode
Provides: elisp-lua-mode

%description
Lua-mode supports c-mode style formatting and sending of
lines/regions/files to a Lua interpreter. An interpreter (see variable
`lua-default-application') will be started if you try to send some
code and none is running. You can use the process-buffer (named after
the application you chose) as if it were an interactive shell. See the
documentation for `comint.el' for details.

%prep
%setup -c -T
cp %{SOURCE0} .

%build
emacs -batch -q -no-site-file -f batch-byte-compile lua-mode.el

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{e_sitedir}
install -m 644 lua-mode.el %{buildroot}%{e_sitedir}
install -m 644 lua-mode.elc %{buildroot}%{e_sitedir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{e_sitedir}/lua-mode.el
%{e_sitedir}/lua-mode.elc

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100617-8m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100617-7m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20100617-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100617-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (20100617-4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100617-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100617-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100617-1m)
- update to 20100617

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-17m)
- rebuild against emacs-23.2

* Thu Mar 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20071122-16m)
- add Obsoletes: lua-mode-xemacs

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-15m)
- merge lua-mode-emacs to elisp-lua-mode

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (20071122-13m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (20071122-12m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-11m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-10m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-9m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-8m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-7m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-6m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-5m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20071122-2m)
- rebuild against gcc43

* Sun Dec 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071122-1m)
- initial packaging
