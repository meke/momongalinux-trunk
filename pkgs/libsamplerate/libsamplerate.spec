%global momorel 6

Summary: Secret Rabbit Code is Sample Rate Converter for audio
Name: libsamplerate

%{?include_specopt}
%{?!do_test: %global do_test 0}

Version: 0.1.7
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.mega-nerd.com/SRC/
Source0: http://www.mega-nerd.com/SRC/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libsndfile >= 1.0.2
BuildRequires: fftw-devel >= 2.1.5-5m
BuildRequires: libsndfile-devel >= 1.0.15-2m
BuildRequires: pkgconfig

%description
 Secret Rabbit Code (aka libsamplerate) is a Sample Rate Converter for audio. One example of where such a thing would be useful is converting audio from the CD sample rate of 44.1kHz to the 48kHz sample rate used by DAT players.

SRC is capable of arbitrary and time varying conversions ; from downsampling by a factor of 12 to upsampling by the same factor. Arbitrary in this case means that the ratio of input and output sample rates can be an irrational number. The conversion ratio can also vary with time for speeding up and slowing down effects.

SRC provides a small set of converters to allow quality to be traded off against computation cost. The current best converter provides a signal-to-noise ratio of 97dB with -3dB passband extending from DC to 96% of the theoretical best bandwidth for a given pair of input and output sample rates.

%package devel
Summary: libsamplerate header files and development documentation
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Header files and development documentation for libsamplerate.

%prep

%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall transform='s,x,x,'

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

%if %{do_test}
%check
make check
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README doc examples
%{_bindir}/sndfile-resample
%{_libdir}/%{name}.so.*

%files devel
%defattr(644,root,root,755)
%attr(755,root,root) %{_libdir}/lib*.so
%{_includedir}/samplerate.h
%{_libdir}/pkgconfig/samplerate.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-5m)
- rebuild for new GCC 4.5

* Sat Oct 30 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (0.1.7-4m)
- original source tarball was modified

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.7-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.7-1m)
- version 0.1.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.4-2m)
- rebuild against rpm-4.6

* Wed Dec  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.4-1m)
- [SECURITY] CVE-2008-5008
- update to 0.1.4
- add specopt do_test to run make check
  the default value is 0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-4m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.2-3m)
- delete libtool library

* Fri May 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.2-2m)
- revise %%files for rpm-4.4.2

* Mon Oct 04 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.1.2-1m)
- update to 0.1.2
- make install transform='s,x,x,'

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.1.0-1m)
- import to Momonga
- needed by kino-0.7.1
