%global momorel 6

# TODO: proper fontconfig hints are nice to have
%define fontname kurdit-unikurd-web
#%%define fontconf 65-%{fontname}.conf

%define archivename unikurdweb

Name:    %{fontname}-fonts
Version: 20020502
Release: %{momorel}m%{?dist}
Summary: A widely used Kurdish font for Arabic-like scripts and Latin

Group:     User Interface/X
License:   GPLv3
URL:       http://www.kurditgroup.org/node/1337
Source0:   http://www.kurditgroup.org/download/1337/%{archivename}.zip
#Source1:   %{name}-fontconfig.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem
Obsoletes:     unikurd-web-font < 20020502-2


%description
A widely used Kurdish font which supports various Arabic-like scripts
(Arabic, Kurdish, Persian) and also Latin.


%prep
%setup -q -c %{archivename}


%build


%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

#install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
#                   %{buildroot}%{_fontconfig_confdir}

#install -m 0644 -p %{SOURCE1} \
#        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
#ln -s %{_fontconfig_templatedir}/%{fontconf} \
#      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -rf %{buildroot}


#%%_font_pkg -f %{fontconf} *.ttf
%_font_pkg *.ttf

%doc gpl.txt
%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20020502-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20020502-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20020502-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20020502-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20020502-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20020502-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20020502-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jan 25 2009 Michal Nowak <mnowak@redhat.com> - 20020502-2
- repackage to save myself a grief

* Sun Jan 25 2009 Michal Nowak <mnowak@redhat.com> - 20020502-1
- renamed from unikurd-web-font -> kurdit-unikurd-web-fonts
- enhanced %%description field
- changes for F11 fonts rules
- add Obsoletes: unikurd-web-font <= 20020502-1

* Tue Oct 14 2008 Michal Nowak <mnowak@redhat.com> - 20020502
- version is now based on date of last issue of the font
- %%defattr(-,root,root,-) -> %%defattr(644,root,root,755)

* Mon Oct 13 2008 Michal Nowak <mnowak@redhat.com> - 1.00-2
- got rid of -web sub-package
- changed name from unikurd-fonts-web to unikurd-web-font
- minor structural changes in SPEC file

* Tue Jul 30 2008 Michal Nowak <mnowak@redhat.com> - 1.00-1
- initial packaging
- this package should be prepared for another unikurd fonts
  in sub-packages because on the KurdIT group/unikurd web there
  are plenthora of them, but probably not under suitable licenses

