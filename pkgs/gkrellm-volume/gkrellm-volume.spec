%global momorel 6
%global plugdir %{_libdir}/gkrellm2/plugins

Name:           gkrellm-volume
Version:        2.1.13
Release:        %{momorel}m%{?dist}
Summary:        GKrellM volume plugin

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://gkrellm.luon.net/volume.php
Source0:        http://gkrellm.luon.net/files/%{name}-%{version}.tar.gz
Patch0:         %{name}-version.patch
Patch1:         %{name}-optflags.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gkrellm-devel >= 2.0
BuildRequires:  gkrellm >= 2.0
BuildRequires:  gtk2-devel >= 2.0.1
BuildRequires:  gettext
BuildRequires:  alsa-lib-devel
Requires:       gkrellm >= 2.0

%description
GKrellM plugin for controlling mixer devices.


%prep
%setup -q -n %{name}
%patch0
%patch1
f=Changelog ; iconv -f iso-8859-1 -t utf-8 $f > $f.utf8 ; mv $f.utf8 $f


%build
CFLAGS="$RPM_OPT_FLAGS" make %{?_smp_mflags} enable_nls=1 enable_alsa=1


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT{%{plugdir},%{_datadir}/locale}
make install \
  INSTALL_PROGRAM="install -p" \
  PLUGIN_DIR=$RPM_BUILD_ROOT%{plugdir} \
  LOCALEDIR=$RPM_BUILD_ROOT%{_datadir}/locale \
  enable_nls=1
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc Changelog COPYRIGHT README THEMING
%{plugdir}/volume.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.13-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.13-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.13-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.13-2m)
- rebuild against rpm-4.6

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.13-1m)
- import from Fedora to Momonga

* Sat Feb  9 2008 Ville Skytta <ville.skytta at iki.fi> - 2.1.13-7
- Fix URL.

* Tue Sep  4 2007 Ville Skytta <ville.skytta at iki.fi> - 2.1.13-6
- Convert docs to UTF-8.

* Thu Aug 16 2007 Ville Skytta <ville.skytta at iki.fi> - 2.1.13-5
- License: GPLv2+

* Wed Aug 30 2006 Ville Skytta <ville.skytta at iki.fi> - 2.1.13-4
- Rebuild.

* Wed Feb 15 2006 Ville Skytta <ville.skytta at iki.fi> - 2.1.13-3
- Rebuild, cosmetics.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 2.1.13-2
- rebuilt

* Sat Sep 11 2004 Ville Skytta <ville.skytta at iki.fi> - 0:2.1.13-0.fdr.1
- Update to 2.1.13.
- Patch to show correct version and honor $RPM_OPT_FLAGS.

* Mon Jun  7 2004 Ville Skytta <ville.skytta at iki.fi> - 0:2.1.11-0.fdr.1
- Update to 2.1.11.
- Enable ALSA support.

* Sun Sep  7 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.1.9-0.fdr.1
- Update to 2.1.9.

* Fri Jun 20 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.1.8-0.fdr.1
- Update to 2.1.8.

* Sat Apr 12 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.1.7-0.fdr.1
- First release.
