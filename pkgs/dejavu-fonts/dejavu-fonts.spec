%global momorel 6

%global fontname    dejavu
%global archivename %{name}-%{archiveversion}

#global alphatag .20080512svn2226
#global alphatag .rc1

#global archiveversion 2.24-20080512-2226
%global archiveversion %{version}

# Let the perl maintainer worry about Unicode.org data files
%global Blocks      %(eval "$(%{__perl} -V:privlibexp)"; echo $privlibexp)/unicore/Blocks.txt
%global UnicodeData %(eval "$(%{__perl} -V:privlibexp)"; echo $privlibexp)/unicore/UnicodeData.txt

# Common description
%global common_desc \
The DejaVu font set is based on the Bitstream Vera fonts, release 1.10. Its\
purpose is to provide a wider range of characters, while maintaining the \
original style, using an open collaborative development process.

Name:    %{fontname}-fonts
Version: 2.31
Release: %{momorel}m%{?dist}
Summary: DejaVu fonts

Group:     User Interface/X
License:   "Bitstream Vera" and Public Domain
URL:       http://%{name}.org/
Source0:   %{?!alphatag:http://downloads.sourceforge.net/%{fontname}}%{?alphatag:%{fontname}.sourceforge.net/snapshots}/%{archivename}.tar.bz2
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

# Older fontforge versions will not work due to sfd format changes
BuildRequires: fontforge >= 20080429
BuildRequires: perl-Font-TTF
# Needed to compute unicode coverage
# BuildRequires: %{Blocks} %{UnicodeData}
# %{Blocks} %{UnicodeData} is provided by perl.
BuildRequires: perl >= 5.14.0-2m

BuildArch:     noarch
BuildRequires: fontpackages-devel

%description
%common_desc


%package common
Summary:  Common files for the Dejavu font set
Requires: fontpackages-filesystem

Obsoletes: dejavu-fonts-doc < 2.26-6
Obsoletes: %{name}-compat < 2.29-8
Obsoletes: %{name}-lgc-compat < 2.29-8

%description common
%common_desc

This package consists of files used by other DejaVu packages.


%package -n %{fontname}-sans-fonts
Summary:  Variable-width sans-serif font faces
Requires: %{name}-common = %{version}-%{release}

Conflicts: dejavu-fonts < 2.26-3
Conflicts: dejavu-fonts-experimental < 2.26-3

Obsoletes: %{name}-sans < 2.28-2

%description -n %{fontname}-sans-fonts
%common_desc

This package consists of the DejaVu sans-serif variable-width font faces, in
their unabridged version.

%_font_pkg -n sans -f *-%{fontname}-sans.conf DejaVuSans.ttf DejaVuSans-*.ttf DejaVuSansCondensed*.ttf


%package -n %{fontname}-serif-fonts
Summary:  Variable-width serif font faces
Requires: %{name}-common = %{version}-%{release}

Conflicts: dejavu-fonts < 2.26-3
Conflicts: dejavu-fonts-experimental < 2.26-3

Obsoletes: %{name}-serif < 2.28-2

%description -n %{fontname}-serif-fonts
%common_desc

This package consists of the DejaVu serif variable-width font faces, in their
unabridged version.

%_font_pkg -n serif -f *-%{fontname}-serif.conf DejaVuSerif.ttf DejaVuSerif-*.ttf DejaVuSerifCondensed*.ttf


%package -n %{fontname}-sans-mono-fonts
Summary:  Monospace sans-serif font faces
Requires: %{name}-common = %{version}-%{release}

Conflicts: dejavu-fonts < 2.26-3
Conflicts: dejavu-fonts-experimental < 2.26-3

Obsoletes: %{name}-sans-mono < 2.28-2

%description -n %{fontname}-sans-mono-fonts
%common_desc

This package consists of the DejaVu sans-serif monospace font faces, in their
unabridged version.

%_font_pkg -n sans-mono -f *-%{fontname}-sans-mono.conf DejaVuSansMono*.ttf


%package -n %{fontname}-lgc-sans-fonts
Summary:  Variable-width sans-serif font faces, Latin-Greek-Cyrillic subset
Requires: %{name}-common = %{version}-%{release}

Conflicts: dejavu-lgc-fonts < 2.26-3

Obsoletes: %{name}-lgc-sans < 2.28-2

%description -n %{fontname}-lgc-sans-fonts
%common_desc

This package consists of the DejaVu sans-serif variable-width font faces, with
unicode coverage restricted to Latin, Greek and Cyrillic.

%_font_pkg -n lgc-sans -f *-%{fontname}-lgc-sans.conf DejaVuLGCSans.ttf DejaVuLGCSans-*.ttf DejaVuLGCSansCondensed*.ttf


%package -n %{fontname}-lgc-serif-fonts
Summary:  Variable-width serif font faces, Latin-Greek-Cyrillic subset
Requires: %{name}-common = %{version}-%{release}

Conflicts: dejavu-lgc-fonts < 2.26-3

Obsoletes: %{name}-lgc-serif < 2.28-2

%description -n %{fontname}-lgc-serif-fonts
%common_desc

This package consists of the DejaVu serif variable-width font faces, with
unicode coverage restricted to Latin, Greek and Cyrillic.

%_font_pkg -n lgc-serif -f *-%{fontname}-lgc-serif.conf DejaVuLGCSerif.ttf DejaVuLGCSerif-*.ttf DejaVuLGCSerifCondensed*.ttf


%package -n %{fontname}-lgc-sans-mono-fonts
Summary:  Monospace sans-serif font faces, Latin-Greek-Cyrillic subset
Requires: %{name}-common = %{version}-%{release}

Conflicts: dejavu-lgc-fonts < 2.26-3

Obsoletes: %{name}-lgc-sans-mono < 2.28-2

%description -n %{fontname}-lgc-sans-mono-fonts
%common_desc

This package consists of the DejaVu sans-serif monospace font faces, with
unicode coverage restricted to Latin, Greek and Cyrillic.

%_font_pkg -n lgc-sans-mono -f *-%{fontname}-lgc-sans-mono.conf DejaVuLGCSansMono*.ttf


%prep
%setup -q -n %{archivename}

%build
make %{?_smp_mflags} VERSION=%{version} FC-LANG="" \
     BLOCKS=%{Blocks} UNICODEDATA=%{UnicodeData}

# Stop the desktop people from complaining this file is too big
bzip2 -9 build/status.txt


%check
make check


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p build/*.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

cd fontconfig
for fontconf in *conf ; do
  install -m 0644 -p $fontconf %{buildroot}%{_fontconfig_templatedir}
  ln -s %{_fontconfig_templatedir}/$fontconf \
        %{buildroot}%{_fontconfig_confdir}/$fontconf
done


%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc AUTHORS BUGS LICENSE NEWS README
%doc build/unicover.txt build/status.txt.bz2


%changelog
* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-6m)
- specify perl version and release

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.31-3m)
- full rebuild for mo7 release

* Tue Jun 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.31-2m)
- fix up Obsoletes

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.31-1m)
- sync with Fedora 13
-- * Tue Jun  3 2010 Nicolas Mailhot <nicolas.mailhot at laposte.net>
-- - 2.31-1
-- -- drop Serif Condensed Italic naming patch
-- 
-- * Mon Sep  7 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
-- - 2.30-2
-- -- patch to fix bug #505129 Serif Condensed Italic is not Serif Condensed
-- 
-- * Sun Sep  6 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
-- - 2.30-1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.29-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.29-6m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.29-5m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.24-4m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.24-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24-2m)
- rebuild against rpm-4.6

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.24-2m)
- modify %%files

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.24-1m)
- rename package dejavu-fonts
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15-2m)
- rebuild against gcc43

* Sun Apr  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.15-1m)
- import from fc

* Thu Mar 01 2007 Behdad Esfahbod <besfahbo@redhat.com> - 2.15-1
- Update to 2.15
- Define fontconffile and use it

* Tue Jan 09 2007 Behdad Esfahbod <besfahbo@redhat.com> - 2.13-1
- Update to 2.13
- Use .bz2 tarball

* Tue Sep 19 2006 Behdad Esfahbod <besfahbo@redhat.com> - 2.10-1
- Update to 2.10

* Mon Sep 11 2006 Behdad Esfahbod <besfahbo@redhat.com> - 2.9-4
- Fix number change in list of files too

* Mon Sep 11 2006 Behdad Esfahbod <besfahbo@redhat.com> - 2.9-3
- dejavu-lgc-fonts.conf: Use <prefer>.  Install as 59-dejavu-lgc-fonts.conf
  to follow the new fontconfig 2.4 numbering policy.

* Thu Aug 31 2006 Behdad Esfahbod <besfahbo@redhat.com> - 2.9-2
- Don't override Bitstream Vera in our configuration file anymore now that
  we got enough testing and gdm is fixed.

* Fri Aug 24 2006 Behdad Esfahbod <besfahbo@redhat.com> - 2.9-1
- Update to 2.9
- Add langcover.txt and unicover.txt to docs, as they are not empty anymore
- Alias DejaVu Sans Light to DejaVu LGC Sans Light

* Tue Jul 18 2006 Matthias Clasen <mclasen@redhat.com> - 2.8-1
- Update to 2.8
- Add fontconfig configuration to make Dejavu the default
  font and cover Vera

* Fri Jul 14 2006 Behdad Esfahbod <besfahbo@redhat.com> - 2.7-1
- Created based on bitstream-vera-fonts-1.10.6 spec.
