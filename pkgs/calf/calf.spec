%global momorel 3

Summary: Calf audio plugin pack
Name: calf
Release: %{momorel}m%{?dist}
License: GPL
Version: 0.0.18.6
URL: http://calf.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gtk2 >= 2.8
BuildRequires: ladspa-devel dssi-devel
BuildRequires: gtk2-devel >= 2.8
BuildRequires: libglade2-devel >= 2.4
BuildRequires: lv2core-devel >= 3.0
BuildRequires: lash-devel
Requires(pre): hicolor-icon-theme gtk2

%description
ladspa-plugins is libraries set for LADSPA.

%package -n ladspa-calf-plugins
Summary: Calf audio plugin pack for LADSPA
Group: Applications/Multimedia
Requires: ladspa

%description -n ladspa-calf-plugins
Calf plugins for LADSPA

%package -n lv2-calf-plugins
Summary: Calf audio plugin pack for LV2
Group: Applications/Multimedia
Requires: lv2core

%description -n lv2-calf-plugins
Calf plugins for LV2

%package -n dssi-calf-plugins
Summary: Calf audio plugin pack for DSSI
Group: Applications/Multimedia
Requires: dssi
Requires: %{name} = %{version}-%{release}

%description -n dssi-calf-plugins
Calf plugins for DSSI

%prep
%setup -q

%build
%configure --with-ladspa-dir=%{_libdir}/ladspa \
           --with-ladspa-rdf-dir=%{_datadir}/ladspa/rdf \
           --with-lv2-dir=%{_libdir}/lv2 \
           --with-dssi-dir=%{_libdir}/dssi
%make

%install
%makeinstall with_ladspa_dir=%{buildroot}%{_libdir}/ladspa \
             with_ladspa_rdf_dir=%{buildroot}%{_datadir}/ladspa/rdf \
             with_lv2_dir=%{buildroot}%{_libdir}/lv2 \
             with_dssi_dir=%{buildroot}%{_libdir}/dssi

%clean
%{__rm} -rf --preserve-root %{buildroot}

%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%files
%defattr(-,root,root)
%doc AUTHORS COPYING COPYING.GPL ChangeLog INSTALL NEWS
%doc README TODO
%{_bindir}/calfjackhost
%{_datadir}/applications/calf.desktop
%{_datadir}/calf
%{_datadir}/icons/*/*/*/calf.png
%exclude %{_datadir}/icons/*/icon-theme.cache
%{_mandir}/man1/calfjackhost.1*
%{_mandir}/man7/calf.7*

%files -n ladspa-calf-plugins
%defattr(-,root,root)
%{_libdir}/ladspa/*.so
%{_datadir}/ladspa/rdf/*.rdf

%files -n lv2-calf-plugins
%defattr(-,root,root)
%{_libdir}/lv2/calf.lv2

%files -n dssi-calf-plugins
%defattr(-,root,root)
%{_libdir}/dssi/*.so
%{_libdir}/dssi/calf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.18.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.18.6-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.0.18.6-1m)
- Add requires: LASH-devel
- update to 0.0.18.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.18.5-4m)
- full rebuild for mo7 release

* Wed May 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.18.5-3m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.18.5-2m)
- use BuildRequires

* Sat Dec 19 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.0.18.5-1m)
- update to 0.0.18.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.18.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.0.18.4-2m)
- add requires %%{name} = %%{version}-%%{release} in dssi-calf-plugins

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.0.18.4-1m)
- Initial Build for Momonga Linux
