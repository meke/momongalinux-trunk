######################################################################
# SuSE default font settings for CJK:

fontDir /usr/share/fonts/truetype/
fontDir /usr/share/ghostscript/Resource/CIDFont/

# If you always want to embed the TrueType fonts defined with
# displayNamedCIDFontTT and displayCIDFontTT in the PostScript output,
# uncomment *all* entries for psNamedFont16 and psFont16

# The last entries for display*CIDFont* will be used for display on screen.
#
# If you prefer TrueType font for display on screen, make sure
# that the display*CIDFontTT entries come last, if you prefer
# CID-keyed fonts, make sure the display*CIDFontT1 entries come last.

# If there are several entries for display*CIDFont* variables for
# the same PDF-font-name or registry order, the last entry where
# the font file exists will win. Lines specifying non-existing
# font files will be silently ignored.

#----------------------------------------------------------------------
# Japanese

# use this if you want to embed the Wadalab-fonts in the PostScript output:
#
#psNamedFont16 Ryumin-Light-Identity-H H WadaMin-Bold-EUC-H EUC-JP
#psNamedFont16 Ryumin-Light-Identity-V V WadaMin-Bold-EUC-V EUC-JP
#psNamedFont16 GothicBBB-Medium-Identity-H H WadaGo-Bold-EUC-H EUC-JP
#psNamedFont16 GothicBBB-Medium-Identity-V V WadaGo-Bold-EUC-V EUC-JP
#psFont16 Adobe-Japan1 H WadaMin-Bold-EUC-H EUC-JP
#psFont16 Adobe-Japan1 V WadaMin-Bold-EUC-V EUC-JP

# use this to avoid embedding fonts for Ryumin-Light and GothicBBB-Medium
# in the PostScript output but rather rely on the printing system 
# to supply these fonts. Ryumin-Light and GothicBBB-Medium are 
# the standard Japanese PostScript fonts and Japanese PostScript printers
# or Ghostscript usually have them.
# It's better not to define fallbacks "psFont16 Adobe-Japan1" here:
# If the document explicitely specifies a different font, it is probably
# better to embed this instead of replacing it with standard PostScript fonts.

psNamedFont16 Ryumin-Light-Identity-H H Ryumin-Light-EUC-H EUC-JP
psNamedFont16 Ryumin-Light-Identity-V V Ryumin-Light-EUC-V EUC-JP
psNamedFont16 GothicBBB-Medium-Identity-H H GothicBBB-Medium-EUC-H EUC-JP
psNamedFont16 GothicBBB-Medium-Identity-V V GothicBBB-Medium-EUC-V EUC-JP

displayNamedCIDFontT1 Ryumin-Light-Identity-H /usr/share/ghostscript/Resource/CIDFont/WadaMin-Bold
displayNamedCIDFontT1 GothicBBB-Medium-Identity-H /usr/share/ghostscript/Resource/CIDFont/WadaGo-Bold
displayCIDFontT1 Adobe-Japan1 /usr/share/ghostscript/Resource/CIDFont/WadaMin-Bold

displayNamedCIDFontTT Ryumin-Light-Identity-H /usr/share/fonts/truetype/kochi-mincho.ttf
displayNamedCIDFontTT GothicBBB-Medium-Identity-H /usr/share/fonts/truetype/kochi-gothic.ttf
displayNamedCIDFontTT Ryumin-Light-Identity-H /usr/share/fonts/truetype/sazanami-mincho.ttf
displayNamedCIDFontTT GothicBBB-Medium-Identity-H /usr/share/fonts/truetype/sazanami-gothic.ttf
displayNamedCIDFontTT Ryumin-Light-Identity-H /usr/share/fonts/truetype/ipamp.ttf
displayNamedCIDFontTT GothicBBB-Medium-Identity-H /usr/share/fonts/truetype/ipagp.ttf
displayNamedCIDFontTT Ryumin-Light-Identity-H /usr/share/fonts/truetype/tlmincho.ttc
displayNamedCIDFontTT GothicBBB-Medium-Identity-H /usr/share/fonts/truetype/tlgothic.ttc
displayNamedCIDFontTT Ryumin-Light-Identity-H /usr/share/fonts/truetype/hgjmlbmp.ttc
displayNamedCIDFontTT GothicBBB-Medium-Identity-H /usr/share/fonts/truetype/hgjgbbmp.ttc
displayNamedCIDFontTT Ryumin-Light-Identity-H /usr/share/fonts/truetype/msmincho.ttc
displayNamedCIDFontTT GothicBBB-Medium-Identity-H /usr/share/fonts/truetype/msgothic.ttc
displayNamedCIDFontTT Ryumin-Light-Identity-H /usr/share/fonts/truetype/hgmlsun.ttf
displayNamedCIDFontTT GothicBBB-Medium-Identity-H /usr/share/fonts/truetype/hggbsun.ttf

displayCIDFontTT Adobe-Japan1 /usr/share/fonts/truetype/kochi-mincho.ttf
displayCIDFontTT Adobe-Japan1 /usr/share/fonts/truetype/sazanami-mincho.ttf
displayCIDFontTT Adobe-Japan1 /usr/share/fonts/truetype/ipamp.ttf
displayCIDFontTT Adobe-Japan1 /usr/share/fonts/truetype/tlmincho.ttc
displayCIDFontTT Adobe-Japan1 /usr/share/fonts/truetype/hgjmlbmp.ttc
displayCIDFontTT Adobe-Japan1 /usr/share/fonts/truetype/msmincho.ttc
displayCIDFontTT Adobe-Japan1 /usr/share/fonts/truetype/hgmlsun.ttf

#----------------------------------------------------------------------
# simplified Chinese

displayCIDFontTT Adobe-GB1 /usr/share/fonts/truetype/gbsn00lp.ttf
displayCIDFontTT Adobe-GB1 /usr/share/fonts/truetype/songti_gb.ttf
displayCIDFontTT Adobe-GB1 /usr/share/fonts/truetype/FZSongTi.ttf

#----------------------------------------------------------------------
# traditional Chinese

# use these entries if you want to embed the MOE CID-keyed fonts
# in the PostScript output:
#
# psNamedFont16 ZenKai-Medium H MOEKai-Regular-B5-H Big5
# psNamedFont16 ShanHeiSun-Light H MOESung-Regular-B5-H Big5
# psFont16 Adobe-CNS1 H MOESung-Regular-B5-H Big5

displayCIDFontT1 Adobe-CNS1 /usr/share/ghostscript/Resource/CIDFont/MOESung-Regular

displayCIDFontTT Adobe-CNS1 /usr/share/fonts/truetype/bsmi00lp.ttf
displayCIDFontTT Adobe-CNS1 /usr/share/fonts/truetype/ming_big5.ttf
displayCIDFontTT Adobe-CNS1 /usr/share/fonts/truetype/FZMingTiB.ttf

#----------------------------------------------------------------------
# Korean

# use these entries if you want to embed the Munhwa CID-keyed fonts
# in the PostScript output:
#
# psNamedFont16 Gulim H MunhwaGothic-Regular-KSC-H ISO-2022-KR
# psNamedFont16 Batang H Munhwa-Regular-KSC-H ISO-2022-KR
# psFont16 Adobe-Korea1 H MunhwaGothic-Regular-KSC-H ISO-2022-KR

displayCIDFontT1 Adobe-Korea1 /usr/share/ghostscript/Resource/CIDFont/MunhwaGothic-Regular

displayCIDFontTT Adobe-Korea1 /usr/share/fonts/truetype/dotum.ttf
displayCIDFontTT Adobe-Korea1 /usr/share/fonts/truetype/gulim.ttf
displayCIDFontTT Adobe-Korea1 /usr/share/fonts/truetype/sundotump.ttf
