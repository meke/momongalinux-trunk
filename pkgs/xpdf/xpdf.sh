#!/bin/sh
XLSFONTS=/usr/bin/xlsfonts
[ -x $XLSFONTS ] || exit 1
GREP=/bin/grep
[ -x $GREP ] || exit 1
XPDF_BIN=@BINDIR@/xpdf-bin
[ -x $XPDF_BIN ] || exit 1

$XLSFONTS -fn '-efont-biwidth-medium-r-normal--14-*-*-*-*-*-*-*' \
| $GREP efont >/dev/null && XPDF_EFONT_SUPPLIED=y

if [ "x$XPDF_EFONT_SUPPLIED" = "xy" ] ; then
  # for indexes
  [ -z "$LC_CTYPE" ] && export LC_CTYPE=en_US.UTF-8 XPDF_USE_ENC_UTF8=y
else
  echo "Warning: efont-unicode-bdf is not found" >&2
fi

# dunno
[ -z "$LC_NUMERIC" ] && export LC_NUMERIC=POSIX

exec $XPDF_BIN ${XPDF_USE_ENC_UTF8+-enc UTF-8} ${1+"$@"}
