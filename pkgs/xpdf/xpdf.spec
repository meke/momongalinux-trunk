%global momorel 2

#
# spec file for package xpdf (Version 3.00)
#
# Copyright (c) 2004 SUSE LINUX AG, Nuernberg, Germany.
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.
#
# Please submit bugfixes or comments via http://www.suse.de/feedback/
#

Summary: A pdf-file viewer under X11
Name: xpdf
Version: 3.03
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Publishing
URL: http://www.foolabs.com/xpdf/
Source0: ftp://ftp.foolabs.com/pub/%{name}/%{name}-%{version}.tar.gz
Source1: ftp://ftp.foolabs.com/pub/%{name}/%{name}-arabic-2011-aug-15.tar.gz
Source2: ftp://ftp.foolabs.com/pub/%{name}/%{name}-chinese-simplified-2011-sep-02.tar.gz
Source3: ftp://ftp.foolabs.com/pub/%{name}/%{name}-chinese-traditional-2011-sep-02.tar.gz
Source4: ftp://ftp.foolabs.com/pub/%{name}/%{name}-cyrillic-2011-aug-15.tar.gz
Source5: ftp://ftp.foolabs.com/pub/%{name}/%{name}-greek-2011-aug-15.tar.gz
Source6: ftp://ftp.foolabs.com/pub/%{name}/%{name}-hebrew-2011-aug-15.tar.gz
Source7: ftp://ftp.foolabs.com/pub/%{name}/%{name}-japanese-2011-sep-02.tar.gz
Source8: ftp://ftp.foolabs.com/pub/%{name}/%{name}-korean-2011-sep-02.tar.gz
Source9: ftp://ftp.foolabs.com/pub/%{name}/%{name}-latin2-2011-aug-15.tar.gz
Source10: ftp://ftp.foolabs.com/pub/%{name}/%{name}-thai-2011-aug-15.tar.gz
Source11: ftp://ftp.foolabs.com/pub/%{name}/%{name}-turkish-2011-aug-15.tar.gz
NoSource: 0
NoSource: 1
NoSource: 2
NoSource: 3
NoSource: 4
NoSource: 5
NoSource: 6
NoSource: 7
NoSource: 8
NoSource: 9
NoSource: 10
NoSource: 11
Source20: SuSEconfig.%{name}
Source21: %{name}-cjk-config
Source22: %{name}rc-cjk
Source23: %{name}rc-cjk.sjis
Source24: %{name}.desktop
Source25: %{name}.png
Source40: %{name}.sh
Patch3: %{name}-%{version}-ext.patch
Patch6: %{name}-3.00-core.patch
Patch7: %{name}-3.00-xfont.patch
Patch9: %{name}-3.00-papersize.patch
Patch10: %{name}-3.00-gcc4.patch
Patch11: %{name}-%{version}-crash.patch
Patch12: %{name}-%{version}-64bit.patch
Patch16: %{name}-3.02-fontlist.patch
Patch19: %{name}-3.02-additionalzoom.patch
Patch20: %{name}-%{version}-compile-fix.patch
# Debian patches
Patch200: 02_permissions.dpatch
Patch201: 10_add_accelerators.dpatch
# Proper stream encoding on 64bit platforms
Patch203: fix-444648.dpatch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ghostscript-fonts
Requires: %{name}-config = %{version}
BuildRequires: openmotif-devel
Autoreqprov: on

%description
This tool is used for viewing PDF (Portable Document Format) files. It
is designed to be small and efficient.

Authors:
--------
    Derek B. Noonburg <derekn@foolabs.com>

%package -n %{name}-config
Summary:      Character maps and config files required by xpdf
Group:        Applications/Publishing
Provides:     %{name}:%{_sysconfdir}/%{name}rc

%description -n %{name}-config
Character maps and config files required by xpdf

Authors:
--------
    Derek B. Noonburg <derekn@foolabs.com>

%prep
%setup -q -a 1 -a 2 -a 3 -a 4 -a 5 -a 6 -a 7 -a 8 -a 9 -a 10

%patch3 -p1 -b .ext
%patch6 -p1 -b .core
%patch7 -p1 -b .fonts
%patch9 -p1 -b .papersize
%patch10 -p1 -b .gcc4
%patch11 -p1 -b .crash
%patch12 -p1 -b .alloc
%patch16 -p1 -b .fontlist
%patch19 -p1
# invalid conversion from 'const char*' to 'char*'
%patch20 -p1 -b .charfix

# debian patches
%patch200 -p1 -b .permissions
%patch201 -p1 -b .accelerators
%patch203 -p1 -b .64bit-stream

%build
autoreconf -f -i -v || :
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"
./configure --prefix=%{_prefix} \
            --sysconfdir=%{_sysconfdir} \
            --mandir=%{_mandir} \
            --enable-a4 \
            --with-freetype2-library \
            --without-t1-library \
            --with-freetype2-includes=%{_includedir}/freetype2
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -fr %{buildroot}
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_datadir}/%{name}
for file in %{name}-*/add-to-%{name}rc ; do
  sed -e "s|/usr/local/share/%{name}|%{_datadir}/%{name}|" -e "s|^displayCIDFontX|#displayCIDFontX|" $file >$file.new
  mv -f $file.new $file
  dir=`dirname $file`
  lang=`echo $dir|sed -e "s|%{name}-||"`
  cp -r $dir %{buildroot}%{_datadir}/%{name}/$lang
  echo >>%{buildroot}%{_sysconfdir}/%{name}rc
  cat $file >>%{buildroot}%{_sysconfdir}/%{name}rc
done
echo "# include font setup for CJK languages" >> %{buildroot}%{_sysconfdir}/%{name}rc
echo "include %{_sysconfdir}/%{name}rc-cjk-auto" >> %{buildroot}%{_sysconfdir}/%{name}rc
echo "include %{_sysconfdir}/%{name}rc-cjk" >> %{buildroot}%{_sysconfdir}/%{name}rc
echo "include %{_sysconfdir}/%{name}rc-cjk.sjis" >> %{buildroot}%{_sysconfdir}/%{name}rc
touch %{buildroot}%{_sysconfdir}/%{name}rc-cjk-auto
install -m 644 %{_sourcedir}/%{name}rc-cjk %{buildroot}%{_sysconfdir}/%{name}rc-cjk
install -m 644 %{_sourcedir}/%{name}rc-cjk.sjis %{buildroot}%{_sysconfdir}/%{name}rc-cjk.sjis
mkdir -p %{buildroot}/sbin/conf.d/
install -m 755 %{_sourcedir}/SuSEconfig.%{name} %{buildroot}/sbin/conf.d/
mkdir -p %{buildroot}%{_sbindir}
install -m 755 %{_sourcedir}/%{name}-cjk-config %{buildroot}%{_sbindir}/

mkdir -p %{buildroot}%{_datadir}/applications/
install -m 644 %{S:24} %{buildroot}%{_datadir}/applications/
mkdir -p %{buildroot}%{_datadir}/pixmaps/
install -m 644 %{S:25} %{buildroot}%{_datadir}/pixmaps/

cp -a %{buildroot}%{_bindir}/%{name} %{buildroot}%{_bindir}/%{name}-bin
cat %{SOURCE40} | sed -e "s|@BINDIR@|%{_bindir}|" \
> %{buildroot}%{_bindir}/%{name}

# poppler provides these utilities now
# http://bugzilla.redhat.com/bugzillA/SHow_bug.cgi?id=177446
rm -rf %{buildroot}%{_bindir}/pdfdetach
rm -rf %{buildroot}%{_bindir}/pdffonts
rm -rf %{buildroot}%{_bindir}/pdfimages
rm -rf %{buildroot}%{_bindir}/pdfinfo
rm -rf %{buildroot}%{_bindir}/pdftohtml
rm -rf %{buildroot}%{_bindir}/pdftops
rm -rf %{buildroot}%{_bindir}/pdftotext
rm -rf %{buildroot}%{_bindir}/pdftoppm

rm -rf %{buildroot}%{_mandir}/man1/pdfdetach.1*
rm -rf %{buildroot}%{_mandir}/man1/pdffonts.1*
rm -rf %{buildroot}%{_mandir}/man1/pdfimages.1*
rm -rf %{buildroot}%{_mandir}/man1/pdfinfo.1*
rm -rf %{buildroot}%{_mandir}/man1/pdftohtml.1*
rm -rf %{buildroot}%{_mandir}/man1/pdftops.1*
rm -rf %{buildroot}%{_mandir}/man1/pdftotext.1*
rm -rf %{buildroot}%{_mandir}/man1/pdftoppm.1*

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -fr %{buildroot}

%files
%defattr(-,root,root)
%doc doc/*.hlp ANNOUNCE COPYING README CHANGES
%{_bindir}/%{name}
%{_bindir}/%{name}-bin
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}.png

%files -n %{name}-config
%defattr(-,root,root)
%{_sbindir}/%{name}-cjk-config
/sbin/conf.d/SuSEconfig.%{name}
%config(noreplace) %{_sysconfdir}/%{name}rc
%config %verify(not md5 size mtime) %{_sysconfdir}/%{name}rc-cjk-auto
%config %{_sysconfdir}/%{name}rc-cjk
%config %{_sysconfdir}/%{name}rc-cjk.sjis
%{_mandir}/man5/%{name}rc.5*
%{_datadir}/%{name}

%changelog
* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.03-2m)
- remove pdfdetach, which is now provided by poppler-utils

* Sat Jan 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.03-1m)
- update to 3.03
- fix fonts problem (cannnot find fonts at start up xpdf)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.02-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.02-14m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02-13m)
- [SECURITY] CVE-2010-3702 CVE-2010-3704
- apply upstream security patch (pl5 patch)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.02-12m)
- full rebuild for mo7 release

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.02-11m)
- fix up desktop file

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.02-10m)
- build fix autoconf-2.65

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.02-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02-7m)
- [SECURITY] CVE-2009-1188 CVE-2009-3603 CVE-2009-3604
- [SECURITY] CVE-2009-3606 CVE-2009-3608 CVE-2009-3609
- apply upstream security patch (Patch202)

* Mon May  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02-6m)
- [SECURITY] CVE-2009-0146 CVE-2009-0147 CVE-2009-0165 CVE-2009-0166
- [SECURITY] CVE-2009-0195 CVE-2009-0799 CVE-2009-0800 CVE-2009-1179
- [SECURITY] CVE-2009-1180 CVE-2009-1181 CVE-2009-1182 CVE-2009-1183
- apply upstream security patch (Patch201)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02-5m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02-4m)
- update Patch6,9 for fuzz=0
- drop Patch50, already merged upstream

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02-3m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.02-2m)
- rebuild against gcc43

* Thu Nov  8 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.02-1m)
- [SECURITY] CVE-2007-4352 CVE-2007-5392 CVE-2007-5393
- update to 3.02pl2

* Tue Jul 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.01-7m)
- [SECURITY] CVE-2007-3387
- import upstream patch (xpdf-3.02pl1.patch)

* Mon Jan 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.01-6m)
- [SECURITY] MOAB-06-01-2007
- Multiple Vendor PDF Document Catalog Handling Vulnerability
 
* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.01-5m)
- add FreeType-2.3.0 Patch(xpdf-3.01-noftinternals.patch)

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.01-4m)
- rebuild against openmotif-2.3.0-beta2

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.01-3m)
- remove one pdf command-line utilities and require poppler ones
  instead. (pdftoppm)

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.01-2m)
- fix a path of xlsfonts in xpdf.sh for xorg-7.0

* Fri Mar 10 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.01-1m)
- version 3.01pl2
- [SECURITY] CVE-2009-4035
- [SECURITY] CVE-2005-3191 CVE-2005-3192 CVE-2005-3193 ...

* Fri Feb 24 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.00-13m)
- remove pdf command-line utilities and require poppler ones
  instead.

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.00-12m)
- add gcc41 patch.
- rename xpdf-3.00-gcc41.patch -> xpdf-3.00-gcc4.patch
- Patch51:      xpdf-3.00-gcc41.patch

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.00-11m)
- add gcc4 patch.
- Patch50:      xpdf-3.00-gcc41.patch

* Wed Jan 19 2005 TAKAHASHI Tamotsu <tamo>
- (3.00-10m)
- fix CAN-2005-0064 (3.00pl3)

* Mon Dec 27 2004 TAKAHASHI Tamotsu <tamo>
- (3.00-9m)
- fix CAN-2004-1125 (3.00pl2)

* Sat Nov 27 2004 TAKAHASHI Tamotsu <tamo>
- (3.00-8m)
- revert 7m for trunk. 8m is equivalent to 6m

* Sat Nov 27 2004 TAKAHASHI Tamotsu <tamo>
- (3.00-7m)
- BuildPrereq:  gcc-c++ (not 3.4 for STABLE_1)

* Fri Nov 26 2004 TAKAHASHI Tamotsu <tamo>
- (3.00-6m)
- update cjk fonts (jul-27)
 + no need for xpdf-config-cjk.patch (patch5) now
- BuildPreReq:  openmotif-devel
- enable i18n patch
- create xpdf.sh wrapper
- import SUSE patches (3.00-78.4)
 +* Wed Nov 03 2004 - thomas@suse.de
 +- Added fix libgoo-sizet.diff (#44963)
 +* Mon Oct 18 2004 - meissner@suse.de
 +- Added yet another sizeof() check that was missed (#44963)
 +* Tue Oct 12 2004 - nadvornik@suse.cz
 +- fixed more integer overflows - CESA-2004-002 [#43082]
 +* Tue Sep 14 2004 - nadvornik@suse.cz
 +- fixed multiple integer overflows [#43082]
.....
 +* Wed Jun 30 2004 - nadvornik@suse.cz
 +- moved pdfinfo to xpdf-config subpackage as it is needed by new
 +  version of kdegraphics
 +* Thu Jun 17 2004 - tiwai@suse.de, mfabian@suse.de
 +- Bugzilla 42142: LC_NUMERIC should be set to POSIX, otherwise
 +  xpdf may generate PostScript output using ',' as the decimal
 +  separator and Ghostscript doesn't like that.
 +- xpdf segfaulted when clicking on non-existing links
.....
 +* Tue May 11 2004 - mfabian@suse.de, tiwai@suse.de
 +- Bugzilla #39312:
 +- make cut-and-paste work for non-ASCII characters
#+- make output encoding default to "UTF-8" in UTF-8 locales
#+  instead of defaulting to "Latin1" always.
 +- workaround the crash which occurs ja_JP.UTF-8 locale when
 +  the .pdf file given on the command line doesn't exist.
 +* Mon May 10 2004 - tiwai@suse.de
 +- process the font rendering without the help of tempfile.
 +  this will improve the performance quite well with big font
 +  files and freetype.

* Sun Aug 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.00-5m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sun Jul 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.00-4m)
- change source URI

* Wed Jul  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.00-3m)
- revise config files

* Sun Jun 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.00-2m)
- change Requires to ghostscript-fonts
- use rpm macros

* Sun Jun 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.00-1m)
- momonganize
- - change release
- - use momorel
- - change Group
- - comment out Patch9: xpdf-i18n.patch
- - tmp comment out /usr/share/applications/*.desktop

* Mon May 10 2004 - mfabian@suse.de
- Bugzilla #39312:
- Comment 11: fix crash which occurs with fonts which have
  embedded bitmaps.
- add FZSongTi.ttf and FZMingTiB.ttf to xpdfrc-cjk
* Thu May 06 2004 - mfabian@suse.de, tiwai@suse.de
- Bugzilla #39312:
- apply Takashi's patch xpdf-3.00-print-ext-ttf.dif
  (has been forgotten). Takashi has improved the patch to support
  multiple faces in .ttc fonts.
- silently ignore non-existing font files to be able to
  configure more than one font in /etc/xpdfrc and let the
  last existing one win.
- add /sbin/conf.d/SuSEconfig.xpdf and /usr/sbin/xpdf-cjk-config
  to automatically create parts of the CJK font setup depending
  on the fonts actually installed.
- add /etc/xpdf-cjk and /etc/xpdf-cjk.sjis with manual
  refinements
- xpdf-i18n.patch: make it possible to display non-ASCII
  file names in the file-selector box by using fontsets
  instead of single fonts. Thanks to Gerd Knorr <kraxel@suse.de>
  for help.
- xpdf-search-non-ascii.patch: Make it possible to search for
  non-ASCII characters
- Bugzilla #39134: don't link against t1lib to avoid crashes
  in t1lib. freetype2 can render Type1 fonts as well, t1lib
  is therefore not needed.
* Wed Apr 21 2004 - tiwai@suse.de
- fixed the handling of truetype for CJK (bug #39312).
- replaced tmpnam() with mkstemp().
* Mon Mar 15 2004 - mfabian@suse.de
- Add "Provides: xpdf:/etc/xpdfrc" to xpdf-config sub-package.
* Sat Mar 13 2004 - mfabian@suse.de
- Bugzilla #33663: Split xpdf-config out of xpdf to make it
  possible for kpdf to require only the character maps and the
  config file /etc/xpdfrc and not the binary. kpdf needs the
  character maps and the config file to be able to display
  CJK .pdf files.
* Wed Mar 10 2004 - mfabian@suse.de
- Bugzilla #35524: improve the configuration to make xpdf work
  with Japanese, Korean, and traditional Chinese documents which
  don't embed the CJK fonts. It works only when the
  CID-keyed-fonts-* packages for the needed languages are
  installed.
* Mon Feb 09 2004 - nadvornik@suse.cz
- updated to 3.00
- pdf 1.5 support
* Sat Jan 10 2004 - adrian@suse.de
- build as user
* Thu Jan 08 2004 - nadvornik@suse.cz
- updated to 2.0.3
- fixed to compile wth new freetype
* Sat Aug 16 2003 - adrian@suse.de
- use translated desktop file from kappfinder
* Fri Aug 15 2003 - adrian@suse.de
- add desktop file
* Fri Jul 11 2003 - nadvornik@suse.cz
- updated to 2.02pl1
* Tue Jun 03 2003 - sndirsch@suse.de
- xpdf-2.01-fonts.patch:
  * use 17 pixel font instead of non-existing 16 pixel font
* Tue May 20 2003 - mmj@suse.de
- Package all man-files
* Tue Jan 07 2003 - nadvornik@suse.cz
- updated to 2.01
- switched to motif
- fixed possible buffer overflow
* Wed Jun 19 2002 - nadvornik@suse.cz
- update to 1.01
- improved I18N support
* Thu Nov 22 2001 - nadvornik@suse.cz
- update to 0.93:
- support for PDF 1.4
- support for TTF fonts
- use fonts from ghostscript-fonts-std
* Thu Jun 07 2001 - pblaha@suse.cz
- unused autoconf because many problems in old configure.in
* Mon Feb 19 2001 - mfabian@suse.de
- update to 0.92
- t1lib is a separate package now, remove it from this package
  and add it to #neededforbuild and Requires: instead.
- add configure options --enable-japanese, --enable-chinese-gb
  and --enable-chinese-cns
* Thu Feb 08 2001 - fober@suse.de
- Group Aplications/Text
* Mon Sep 18 2000 - bubnikv@suse.cz
- updated to 0.91
* Mon Aug 14 2000 - schwab@suse.de
- Add %%suse_update_config.
* Fri May 26 2000 - bubnikv@suse.cz
- sorted
* Mon May 15 2000 - bubnikv@suse.cz
- xpdf now uses t1 library for rendering fonts
- added buildroot
* Tue Jan 25 2000 - ro@suse.de
- fixed to compile
- specfile cleanup
- man to /usr/share using macro
* Wed Oct 27 1999 - fehr@suse.de
- changed to version 0.90
* Mon Sep 13 1999 - bs@suse.de
- ran old prepare_spec on spec file to switch to new prepare_spec.
* Thu Jan 21 1999 - ro@suse.de
- use "const char*" where needed for strict typecheck
* Fri Dec 11 1998 - fehr@suse.de
- changed to version 0.80
* Mon Oct 05 1998 - ro@suse.de
- added libgpp to neededforbuild
* Mon Jul 06 1998 - fehr@suse.de
- New version 0.7a
* Wed Jun 04 1997 - fehr@suse.de
- New version 0.7
* Wed Apr 16 1997 - fehr@suse.de
- New version 0.6
* Thu Jan 02 1997 - fehr@suse.de
- New version 0.5
