%global momorel 10

Summary: A utility to create or extract preview images in EPS files
Name: epstool
Version: 3.08
Release: %{momorel}m%{?dist}
URL: http://www.cs.wisc.edu/~ghost/gsview/epstool.htm
Group: Applications/File
License: GPL
Source0: http://mirrors.ctan.org/support/ghostscript/ghostgum/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: epstool-3.08-glibc210.patch
Requires: ghostscript
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Epstool is a utility to create or extract preview images in EPS files, fix bounding boxes and convert to bitmaps.

Features:

    * Add EPSI or DOS EPS previews.
    * Extract PostScript from DOS EPS files.
    * Uses Ghostscript to create preview bitmaps.
    * Create a TIFF, WMF or Interchange preview from part of a bitmap created by Ghostscript.
    * works under Win32 and Unix. Older versions work under OS/2 and MS-DOS.
    * works on little-endian machines (Intel) or big endian (Sun Sparc, Motorola) machines.


%prep
rm -rf %{buildroot}

%setup -q
%patch0 -p1 -b .glibc210
rm -rf epstool

%build
make epstool

%install
mkdir -p %{buildroot}/%{_bindir}
install -m 755 bin/epstool %{buildroot}%{_bindir}/epstool

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc LICENCE
%{_bindir}/*

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.08-10m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.08-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.08-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.08-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.08-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.08-5m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.08-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.08-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.08-2m)
- %%NoSource -> NoSource

* Fri Nov 10 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.08-1m)
- initial import to Momonga

* Fri Dec 23 2005 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (3.08-0.0.1m)
- version 3.08

* Fri Sep 27 2002 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (3.0-0.1m)
- version 3.0
- not work with eq4-epstool.sym in tgif
