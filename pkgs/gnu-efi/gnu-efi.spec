%global momorel 1
#%%global efidir fedora
%global efidir momonga

Summary: Development Libraries and headers for EFI
Name: gnu-efi
Version: 3.0u
Release: %{momorel}m%{?dist}
Group: Development/System
License: GPLv2+
URL: ftp://ftp.hpl.hp.com/pub/linux-ia64
Source: ftp://ftp.hpl.hp.com/pub/linux-ia64/gnu-efi_%{version}.orig.tar.gz
Patch0001: 0001-fix-compilation-on-x86_64-without-HAVE_USE_MS_ABI.patch
Patch0002: 0002-be-more-pedantic-when-linking.patch
Patch0003: 0003-Sample-boot-service-driver.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: %{ix86} x86_64 ia64
#BuildRequires: git

%define debug_package %{nil}

%description
This package contains development headers and libraries for developing
applications that run under EFI (Extensible Firmware Interface).

%package devel
Summary: Development Libraries and headers for EFI
Group: Development/System
Obsoletes: gnu-efi < %{version}-%{release}

%description devel
This package contains development headers and libraries for developing
applications that run under EFI (Extensible Firmware Interface).

%package utils
Summary: Utilities for EFI systems
Group: Applications/System

%description utils
This package contains utilties for debugging and developing EFI systems.

%prep
%setup -q -n gnu-efi-3.0

%patch0001 -p1
%patch0002 -p1
%patch0003 -p1

%build
# Package cannot build with %{?_smp_mflags}.
make

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/%{_libdir}

make PREFIX=%{_prefix} LIBDIR=%{_libdir} INSTALLROOT=%{buildroot} install

mkdir -p %{buildroot}/%{_libdir}/gnuefi
mv %{buildroot}/%{_libdir}/*.lds %{buildroot}/%{_libdir}/*.o %{buildroot}/%{_libdir}/gnuefi

make -C apps clean route80h.efi modelist.efi
mkdir -p %{buildroot}/boot/efi/EFI/%{efidir}/
mv apps/{route80h.efi,modelist.efi} %{buildroot}/boot/efi/EFI/%{efidir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/*

%files devel
%defattr(-,root,root,-)
%doc README.* ChangeLog
%{_includedir}/efi

%files utils
%dir /boot/efi/EFI/%{efidir}/
%attr(0644,root,root) /boot/efi/EFI/%{efidir}/*.efi

%changelog
* Tue Jun 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0u-1m)
- update 3.0u

* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0e-8m)
- rename /boot/efi/EFI/momonga to /boot/efi/EFI/redhat

* Mon Aug  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0e-7m)
- release %%dir /boot/efi/EFI/momonga, it's provided by grub

* Mon Aug  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0e-6m)
- import few patches from fedora and use /boot/efi/EFI/momonga

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0e-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0e-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0e-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0e-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0e-1m)
- sync with Fedora 11 (3.0e-7)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0d-2m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0d-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0a-0.3m)
- rebuild against gcc43

* Sat Mar 04 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0a-0.2m)
- renumber release
- s/x86_64/%%{ix86}/g

* Fri Mar 03 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0a-0.1m)
- import to Momonga

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 3.0a-7.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Mar  3 2005 Jeremy Katz <katzj@redhat.com> - 3.0a-7
- rebuild with gcc 4

* Tue Sep 21 2004 Jeremy Katz <katzj@redhat.com> - 3.0a-6
- add fix from Jesse Barnes for newer binutils (#129197)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Apr 21 2004 Jeremy Katz <katzj@redhat.com> - 3.0a-4
- actually add the patch

* Tue Apr 20 2004 Bill Nottingham <notting@redhat.com> 3.0a-3
- add patch to coalesce some relocations (#120080, <erikj@sgi.com>)

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Oct  4 2002 Jeremy Katz <katzj@redhat.com>
- rebuild in new environment

* Sun Jul  8 2001 Bill Nottingham <notting@redhat.com>
- update to 3.0

* Tue Jun  5 2001 Bill Nottingham <notting@redhat.com>
- add fix for invocations from the boot manager menu (#42222)

* Tue May 22 2001 Bill Nottingham <notting@redhat.com>
- add bugfix for efibootmgr (<schwab@suse.de>)

* Mon May 21 2001 Bill Nottingham <notting@redhat.com>
- update to 2.5
- add in efibootmgr from Dell (<Matt_Domsch@dell.com>)

* Thu May  3 2001 Bill Nottingham <notting@redhat.com>
- fix booting of kernels with extra arguments (#37711)

* Wed Apr 25 2001 Bill Nottingham <notting@redhat.com>
- take out Stephane's initrd patch

* Fri Apr 20 2001 Bill Nottingham <notting@redhat.com>
- fix the verbosity patch to not break passing arguments to images

* Wed Apr 18 2001 Bill Nottingham <notting@redhat.com>
- update to 2.0, build elilo, obsolete eli

* Tue Dec  5 2000 Bill Nottingham <notting@redhat.com>
- update to 1.1

* Thu Oct 26 2000 Bill Nottingham <notting@redhat.com>
- add patch for new toolchain, update to 1.0

* Thu Aug 17 2000 Bill Nottingham <notting@redhat.com>
- update to 0.9
