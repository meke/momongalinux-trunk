%global momorel 15

Summary: A library for editing typed command lines
Name: compat-readline5
Version: 5.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://cnswww.cns.cwru.edu/php/chet/readline/rltop.html
Source: ftp://ftp.gnu.org/gnu/readline/readline-%{version}.tar.gz
NoSource: 0
Patch1: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-001
Patch2: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-002
Patch3: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-003
Patch4: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-004
Patch5: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-005
Patch6: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-006
Patch7: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-007
Patch8: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-008
Patch9: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-009
Patch10: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-010
Patch11: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-011
Patch12: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-013
Patch13: ftp://ftp.gnu.org/gnu/readline/readline-5.2-patches/readline52-014
# fix file permissions, remove RPATH, use CFLAGS
Patch20: readline-5.2-shlib.patch
# fixed in readline-6.0
Patch21: readline-5.2-redisplay-sigint.patch
BuildRequires: ncurses-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Readline library provides a set of functions that allow users to
edit command lines. Both Emacs and vi editing modes are available. The
Readline library includes additional functions for maintaining a list
of previously-entered command lines for recalling or editing those
lines, and for performing csh-like history expansion on previous
commands.

%package devel
Summary: Files needed to develop programs which use the readline library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: ncurses-devel

%description devel
The Readline library provides a set of functions that allow users to
edit typed command lines. If you want to develop programs that will
use the readline library, you need to have the readline-devel package
installed. You also need to have the readline package installed.

%package static
Summary: Static libraries for the readline library
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
The readline-static package contains the static version of the readline
library.

%prep
%setup -q -n readline-%{version}
%patch1 -p0 -b .001
%patch2 -p0 -b .002
%patch3 -p0 -b .003
%patch4 -p0 -b .004
%patch5 -p0 -b .005
%patch6 -p0 -b .006
%patch7 -p0 -b .007
%patch8 -p0 -b .008
%patch9 -p0 -b .009
%patch10 -p0 -b .010
%patch11 -p0 -b .011
%patch12 -p0 -b .013
%patch13 -p0 -b .014
%patch20 -p1 -b .shlib
%patch21 -p1 -b .redisplay-sigint

%build
export CPPFLAGS="-I%{_includedir}/ncurses"
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make DESTDIR=$RPM_BUILD_ROOT install

mkdir $RPM_BUILD_ROOT%{_includedir}/readline5
mv $RPM_BUILD_ROOT%{_includedir}/readline{,5}

mkdir -p $RPM_BUILD_ROOT%{_libdir}/readline5
mv $RPM_BUILD_ROOT%{_libdir}/lib*.a $RPM_BUILD_ROOT%{_libdir}/readline5

rm -f $RPM_BUILD_ROOT%{_libdir}/lib*.so
ln -sf ../libreadline.so.5 $RPM_BUILD_ROOT%{_libdir}/readline5/libreadline.so
ln -sf ../libhistory.so.5 $RPM_BUILD_ROOT%{_libdir}/readline5/libhistory.so

rm -rf $RPM_BUILD_ROOT%{_infodir}
rm -rf $RPM_BUILD_ROOT%{_mandir}

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc CHANGES COPYING NEWS README USAGE
%{_libdir}/libreadline*.so.*
%{_libdir}/libhistory*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/readline5
%dir %{_libdir}/readline5
%{_libdir}/readline5/lib*.so

%files static
%defattr(-,root,root,-)
%{_libdir}/readline5/lib*.a

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-15m)
- support UserMove env

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-12m)
- full rebuild for mo7 release

* Thu Jan 14 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-11m)
- rename readline to compat-readline5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2-9m)
- move libreadline to /%%{_lib}
-- http://pc11.2ch.net/test/read.cgi/linux/1188293074/617
- fix install-info
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-7m)
- rebuild against gcc43

* Mon Feb 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-6m)
- update readline52-012

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-5m)
- %%NoSource -> NoSource

* Sun Jul 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2-4m)
- revise %%post and %%preun

* Sun Jun 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.2-3m)
- sync FC-devel
- split static package

* Tue Feb 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-2m)
- use ncurses

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2-1m)
- update to 5.2

* Thu Oct  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1-1m)
- update to 5.1
- delete unused patch7, because this is applied upstream source
- use %%NoSource and %%NoPatch

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.0-1m)
- update 5.0

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (4.3-4m)
- revised spec for rpm 4.2.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (4.3-3m)
- pretty spec file.

* Fri Nov 15 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (4.3-3m)
- add readline-4.3-avoid-loop.patch

* Fri Jul 26 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (4.3-2m)
- add readline-4.3-strpbrk.patch for more i18n support
- some cleanup stuff

* Thu Jul 25 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.3-1m)
  update to 4.3

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (4.1-22k)

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (4.1-20k)
- add BuildPreReq: autoconf >= 2.52-8k

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (4.1-18k)
- updated Source0 URL

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag
- fix ChangeLog to changelog

* Mon May  7 2001 Akira Higuchi <a@kondara.org>
- (4.1-12k)
- readline-20000814.patch

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun May 7 2000 Fumihiko Takayama <tekezo@catv296.ne.jp>
- fix i18n patch (fix riskness of buffer over flow. not link termcap anymore). 
- fix SPEC file ( %clean ) 

* Sun May 7 2000 Fumihiko Takayama <tekezo@catv296.ne.jp>
- fix i18n patch ( forward_word & backward_word )

* Sat May 6 2000 Fumihiko Takayama <tekezo@catv296.ne.jp>
- fix i18n patch. 

* Sat May 6 2000 Fumihiko Takayama <tekezo@catv296.ne.jp>
- update to 4.1
- i18n ( but still beta )

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Sep 28 1999 Jun Nishii <jun@flatout.org>
- EUC code

* Wed Sep 22 1999 Jun Nishii <jun@flatout.org>
- added %defattr in -devel...

* Mon Sep 20 1999 Jun Nishii <jun@flatout.org>
- added %defattr

* Fri Sep 3 1999 Jun Nishii <jun@flatout.org>
- build for Vine-1.9

* Thu Nov 25 1998 Jun Nishii <jun@flatout.org>
- fix soname of libreadline

* Thu Nov 12 1998 Jun Nishii <jun@flatout.org>
- fix sym links of libraries

* Sun Oct 11 1998 Jun Nishii <jun@flatout.org>
- add documents

* Sun Sep 27 1998 Jun Nishii <jun@flatout.org>
- updated to 2.2.1 with Japanese patch

* Sun Sep 27 1998 Jun Nishii <jun@flatout.org>
- updated to 2.2.1 with Japanese patch

* Wed May 06 1998 Prospector System <bugs@redhat.com>

- translations modified for de, fr, tr

* Wed May 06 1998 Cristian Gafton <gafton@redhat.com>
- don't package %{_infodir}/dir

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- devel package moved to Development/Libraries

* Tue Apr 21 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.2

* Tue Oct 14 1997 Donnie Barnes <djb@redhat.com>
- spec file cleanups

* Fri Oct 10 1997 Erik Troan <ewt@redhat.com>
- added proper sonames

* Tue Jul 08 1997 Erik Troan <ewt@redhat.com>
- updated to readline 2.1

* Tue Jun 03 1997 Erik Troan <ewt@redhat.com>
- built against glibc
