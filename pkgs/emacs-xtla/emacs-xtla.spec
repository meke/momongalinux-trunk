%global momorel 26
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: An Emacsen front-end for GNU Arch
Name: emacs-xtla
Version: 1.2.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Url: http://wiki.gnuarch.org/xtla
Source0: http://download.gna.org/xtla-el/xtla-%{version}.tar.gz 
NoSource: 0
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: tla
Requires: emacs >= %{emacsver}
Requires(post): info
Requires(preun): info
Obsoletes: xtla-emacs
Obsoletes: xtla-xemacs

Obsoletes: elisp-xtla
Provides: elisp-xtla

%description
Xtla is an Emacsen interface for GNU Arch (tla). It is similar to
PCL-CVS for CVS or PSVN to Subversion. The interface should give the
user full access to the functionality of tla within Emacs.

%prep
%setup -q -n xtla-%{version}

%build
%{configure} --with-emacs=emacs \
	     --with-lispdir=%{e_sitedir}/xtla
%{make}

%install
%{__rm} -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{e_sitedir}/xtla
%{__install} -m 644 lisp/*.el* %{buildroot}%{e_sitedir}/xtla
%{__mkdir_p} %{buildroot}%{_infodir}
%{__install} -m 644 texinfo/xtla.info %{buildroot}%{_infodir}

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/xtla.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/xtla.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog INSTALL docs
%dir %{e_sitedir}/xtla
%{e_sitedir}/xtla/*.el*
%{_infodir}/xtla.info*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-26m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-25m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.1-24m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-23m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-22m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-21m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-20m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-19m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-18m)
- merge xtla-emacs to elisp-xtla
- kill xtla-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-16m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-15m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-14m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-13m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-12m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-11m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-10m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-9m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-8m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-7m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-6m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-4m)
- %%NoSource -> NoSource

* Wed Jul 25 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- add PreReq: info (xtla-emacs)

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-10m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-9m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-8m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-7m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-6m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-5m)
- rebuild against emacs-22.0.92

* Mon Dec 11 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-4m)
- add BuildPreReq: tla

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-2m)
- rebuild against emacs-22.0.90

* Mon Aug 21 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-1m)
- initial momonga package based on ALTLinux
  emacs-xtla-1.1-alt2.src.rpm

* Tue Mar 14 2006 Maxim Tyurin <mrkooll@altlinux.ru> 1.1-alt2
- Fix requires

* Wed Aug 10 2005 Maxim Tyurin <mrkooll@altlinux.ru> 1.1-alt1
- New version

* Thu Jan 06 2005 Maxim Tyurin <mrkooll@altlinux.ru> 0.9-alt1
- Initial build

