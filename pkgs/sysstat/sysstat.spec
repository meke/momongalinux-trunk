%global momorel 1

Name: sysstat
Version: 10.2.0
Release: %{momorel}m%{?dist}
Summary: The sar and iostat system monitoring commands
License: GPLv2+
Group: Applications/System
URL: 	http://perso.orange.fr/sebastien.godard/
Source: http://perso.orange.fr/sebastien.godard/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 

Requires: chkconfig
Requires: sh-utils textutils grep fileutils cronie
BuildRequires: perl kernel-headers gettext
Requires: coreutils grep 
BuildRequires: systemd-units
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
This package provides the sar and iostat commands for Linux. Sar and
iostat enable system monitoring of disk, network, and other IO
activity.

%prep
%setup -q
iconv -f windows-1252 -t utf8 CREDITS > CREDITS.aux
mv CREDITS.aux CREDITS

%build
%configure sa_lib_dir=%{_libdir}/sa history=28 compressafter=31 \
    --disable-man-group --disable-stripping
%{__sed} -i 's/SADC_OPTIONS=""/SADC_OPTIONS="-S DISK"/' sysstat.sysconfig
export CFLAGS="$RPM_OPT_FLAGS -DSADC_PATH=\\\"%{_libdir}/sa/sadc\\\""
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# Install cron file
mkdir -p %{buildroot}%{_sysconfdir}/cron.d
install -m 0644 cron/sysstat.crond %{buildroot}%{_sysconfdir}/cron.d/sysstat

# Install service file
mkdir -p %{buildroot}%{_unitdir}
install -m 0644 sysstat.service %{buildroot}%{_unitdir}/

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
%systemd_post sysstat.service

%preun
%systemd_preun sysstat.service
if [[ $1 -eq 0 ]]; then
  # Remove sa logs if removing sysstat completely
  rm -f %{_localstatedir}/log/sa/*
fi

%postun
%systemd_postun sysstat.service

%triggerun -- sysstat < 10.0.2-2m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply sysstat
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save sysstat >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del sysstat >/dev/null 2>&1 || :
/bin/systemctl try-restart sysstat.service >/dev/null 2>&1 || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc CHANGES COPYING CREDITS README FAQ
%config(noreplace) %{_sysconfdir}/cron.d/sysstat
%config(noreplace) %{_sysconfdir}/sysconfig/sysstat
%config(noreplace) %{_sysconfdir}/sysconfig/sysstat.ioconf
%{_unitdir}/sysstat.service
%{_bindir}/*
%{_libdir}/sa
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man8/*
%{_localstatedir}/log/sa

%changelog
* Tue Nov 19 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (10.2.0-1m)
- update 10.2.0

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.0.2-1m)
- update 10.0.2
- support systemd

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (10.0.1-1m)
- update 10.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.0.6.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.0.6.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.0.6.1-3m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.0.6.1-2m)
- stop auto starting service at initial system startup

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.0.6.1-1m)
- update 9.0.6.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.0.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.0.4-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.0.4-4m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0.4-3m)
- modify BR and Requires

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0.4-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir} again

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (8.0.4-1m)
- versino up

* Tue Apr 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0.1-5m)
- add patch for kernel-2.6.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.0.1-4m)
- rebuild against gcc43

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0.1-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6.0.1-2m)
- enable ppc, ppc64

* Mon May  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (6.0.1-1m)
- version up
- sync with fc-devel

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0.5-3m)
- add gcc4 patch.
- Patch5: sysstat-5.0.5-gcc4fix.patch

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (5.0.5-2m)
- enable x86_64.

* Sat Jul 10 2004 TAKAHASHI Tamotsu <tamo>
- (5.0.5-1m)
- FEDORA-2004-187 (patch1: sysstat-5.0.5-overrun.patch)
  http://www.securityfocus.com/advisories/6896
  buffer overflow, not exploited so far

* Tue Jun  1 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (5.0.4-1m)
- verup

* Tue Apr 13 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (5.0.3-1m)
- update to 5.0.3
  [SECURITY] fixed temporary file vulnerability

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (5.0.0-2m)
- revised spec for rpm 4.2.

* Sat Dec  6 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (5.0.0-1m)
- update to 5.0.0

* Mon Aug  4 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (4.1.5-1m)
- update to 4.1.5

* Tue Jul  8 2003 Kimitake Shibata <cipher@da2.so-net.ne.jp>
- (4.1.4-1m)
  Update to 4.1.4

* Tue Mar  4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.2-1m)
- update to 4.1.2

* Wed Jan 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.1.1-1m)
- update to 4.1.1

* Thu Aug 15 2002 Junichiro Kita <kita@momonga-linux.org>
- (4.0.6-1m)
- up to 4.0.6

* Tue Jul  2 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (4.0.5-1m)
- up to 4.0.5

* Thu Apr 18 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (4.0.4-2k)
- up to 4.0.4

* Wed Mar 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (4.0.3-4k)
- add %clean and %changelog sections :(
