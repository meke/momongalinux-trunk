%global         momorel 1

Name:           perl-Test-MockObject
Version:        1.20140408
Release:        %{momorel}m%{?dist}
Summary:        Perl extension for emulating troublesome interfaces
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Test-MockObject/
Source0:        http://www.cpan.org/authors/id/C/CH/CHROMATIC/Test-MockObject-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-CGI
BuildRequires:  perl-Devel-Peek
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-Util
BuildRequires:  perl-Test-Exception >= 0.31
BuildRequires:  perl-Test-Simple >= 0.98
BuildRequires:  perl-Test-Warn >= 0.23
BuildRequires:  perl-UNIVERSAL-can >= 1.16
BuildRequires:  perl-UNIVERSAL-isa >= 1.03
Requires:       perl-CGI
Requires:       perl-Devel-Peek
Requires:       perl-List-Util
Requires:       perl-Test-Exception >= 0.31
Requires:       perl-Test-Simple >= 0.98
Requires:       perl-Test-Warn >= 0.23
Requires:       perl-UNIVERSAL-can >= 1.16
Requires:       perl-UNIVERSAL-isa >= 1.03
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
It's a simple program that doesn't use any other modules, and those are
easy to test. More often, testing a program completely means faking up
input to another module, trying to coax the right output from something
you're not supposed to be testing anyway.

%prep
%setup -q -n Test-MockObject-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Test/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20140408-1m)
- rebuild against perl-5.20.0
- update to 1.20140408

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20120301-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20120301-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20120301-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20120301-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20120301-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20120301-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20120301-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20120301-1m)
- update to 1.20120301

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20110612-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20110612-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20110612-1m)
- update to 1.20110612

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.09-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.09-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.09-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.09-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.09-2m)
- rebuild against rpm-4.6

* Tue May 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.08-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.08-2m)
- %%NoSource -> NoSource

* Mon Jul 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.07-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.06-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
