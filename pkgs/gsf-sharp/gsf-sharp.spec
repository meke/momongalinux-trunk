%global momorel 12

Name: gsf-sharp
Version: 0.8.1
Release: %{momorel}m%{?dist}
License: GPL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.mono-project.com/
Source0: http://primates.ximian.com/~joe/gsf-sharp-0.8.1.tar.gz
Patch0: gsf-sharp-0.8.1-lib64.patch
Summary: gsf library for .NET
Group: Development/Libraries

BuildRequires: pkgconfig
BuildRequires: mono-core
BuildRequires: monodoc-devel
BuildRequires: libgsf-devel
BuildRequires: mono-devel >= 2.8
BuildRequires: gtk-sharp2-devel >= 2.12.10-4m

%description
gsf-sharp

%package devel
Summary: gsf-sharp-devel
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
gsf-sharp-devel

%prep
%setup -q
%if %{_lib} == "lib64"                                        
%patch0 -p1 -b .lib64~
%endif

%build
autoreconf -fiv
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libgsfglue.so
%{_prefix}/lib/mono/gtk-sharp
%{_prefix}/lib/mono/gac/gsf-sharp
%{_datadir}/gapi-2.0/gsf-api.xml

%files devel
%defattr(-, root, root)
%{_libdir}/libgsfglue.a
%{_libdir}/pkgconfig/gsf-sharp.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-12m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-10m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-9m)
- rebuild against mono-2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.8.1-6m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-4m)
- rebuild against gcc43

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-3m)
- rebuild against gtk-sharp2-2.12.0

* Tue May 22 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.1-2m)
- add Patch0 gsf-sharp-0.8.1-lib64.patch

* Mon May 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- initial build
