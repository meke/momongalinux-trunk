%global momorel 2	
%global octpkg miscellaneous

Name:           octave-%{octpkg}
Version:        1.0.11
Release:        %{momorel}m%{?dist}
Summary:        Miscellaneous functions for Octave
Group:          Applications/Engineering
License:        GPLv2+
URL:            http://octave.sourceforge.net/miscellaneous/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
BuildRequires:  octave-devel
BuildRequires:  dos2unix 

Requires:       octave(api) = %{octave_api}
Requires(post): octave
Requires(postun): octave


Obsoletes:      octave-forge <= 20090607


%description
Miscellaneous tools that don't fit somewhere else. It includes
additional functions for manipulating cell arrays, computation of
Chebyshev, Hermite, Legendre and Laguerre polynomials, working with
CSV data and for Latex export.

%prep
%setup -q -n %{octpkg}-%{version}

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install
chmod a-x %{buildroot}/%{octpkgdir}/*.m
dos2unix %{buildroot}/%{octpkgdir}/*.m

%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)
%{octpkglibdir}

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo
%doc %{octpkgdir}/packinfo/COPYING
%{octpkgdir}/doc/server.txt


%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.11-2m)
- rebuild against octave-3.6.1-1m

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.11-1m)
- initial import from fedora
