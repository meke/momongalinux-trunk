%global momorel 1

Summary: Files needed to run yum updates as a cron job
Name: yum-cron
Version: 0.9.2
Release: %{momorel}m%{?dist}
URL: http://linux.duke.edu/yum/
# This is a Red Hat maintained package which is specific to
# our distribution.  Thus the source is only available from
# within this srpm.
Source0: %{name}-%{version}.tar.gz
License: GPLv2
Group: System Environment/Base
BuildArch: noarch
Requires: yum >= 3.0 vixie-cron crontabs yum-downloadonly findutils
Requires(post): chkconfig
Requires(post): initscripts
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(postun): initscripts
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
These are the files needed to run yum updates as a cron job.  They
originated in yum-2.6.1-0.fc5, but were left out of FC6's yum.
Install this package if you want auto yum updates nightly via cron
rather than the newer yum-updatesd daemon.

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
install -D -m 755 0yum.cron $RPM_BUILD_ROOT/%{_sysconfdir}/cron.daily/0yum.cron
install -D -m 755 yum-cron $RPM_BUILD_ROOT/%{_sysconfdir}/init.d/yum-cron
install -D -m 644 yum-daily.yum $RPM_BUILD_ROOT/%{_sysconfdir}/yum/yum-daily.yum
install -D -m 644 yum-weekly.yum $RPM_BUILD_ROOT/%{_sysconfdir}/yum/yum-weekly.yum
install -D -m 644 yum-cron.sysconf $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/yum-cron

%clean
rm -rf $RPM_BUILD_ROOT

%post
# Make sure chkconfig knows about the service
/sbin/chkconfig --add yum-cron
# if an upgrade:
if [ "$1" -ge "1" ]; then
# if there's a /etc/init.d/yum file left, assume that there was an
# older instance of yum-cron which used this naming convention.  Clean
# it up, do a conditional restart
 if [ -f /etc/init.d/yum ]; then
# was it on?
  /sbin/chkconfig yum
  RETVAL=$?
  if [ $RETVAL = 0 ]; then
# if it was, stop it, then turn on new yum-cron
   /sbin/service yum stop 1> /dev/null 2>&1
   /sbin/service yum-cron start 1> /dev/null 2>&1
   /sbin/chkconfig yum-cron on
  fi
# remove it from the service list
  /sbin/chkconfig --del yum
 fi
fi
exit 0

%preun
# if this will be a complete removeal of yum-cron rather than an upgrade,
# remove the service from chkconfig control
if [ $1 = 0 ]; then
 /sbin/chkconfig --del yum-cron
 /sbin/service yum-cron stop 1> /dev/null 2>&1
fi
exit 0

%postun
# If there's a yum-cron package left after uninstalling one, do a 
# conditional restart of the service
if [ "$1" -ge "1" ]; then
 /sbin/service yum-cron condrestart 1> /dev/null 2>&1
fi
exit 0

%files
%defattr(-,root,root)
%doc COPYING README
%{_sysconfdir}/cron.daily/0yum.cron
%config(noreplace) %{_sysconfdir}/yum/yum-daily.yum
%config(noreplace) %{_sysconfdir}/yum/yum-weekly.yum
%{_sysconfdir}/init.d/yum-cron
%config(noreplace) %{_sysconfdir}/sysconfig/yum-cron

%changelog
* Sun Sep 25 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.2-1m)
- import from Fedora

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Dec 27 2010 Jason L Tibbitts III <tibbs@math.uh.edu> - 0.9.2-2
- Mark /etc/yum/yum-*.yum as config files so updates don't wipe them out.

* Mon Feb 1 2010 Alec Habig <ahabig@umn.edu> - 0.9.2-1
- Changed the shebang to refer to bash rather than sh, as bashisms are
  used so bash should be required (bug 550449)
- Test for restorecon and mailx as they're used, so systems without them
  are not forced to install to use this package (removed the
  dependancies, bug 550447)
* Fri Oct 9 2009 Alec Habig <ahabig@umn.edu> - 0.9.1-1
- Fixed CLEANDAY default in script
- changed the /etc/yum/ scripts to not be marked as a config file in
  the .spec - we always want them updated, they're code not config.  
  rpmlint used to complain about them not being marked as config files,
  but seems to have grown out of this weirdness in the meantime .
* Tue Oct 6 2009 Alec Habig <ahabig@umn.edu> - 0.9.0-1
- Change cron file to 0yum.cron, so yum updates things before the other daily jobs
  such as makewhatis, prelink, updatedb, etc run.  That way updated files get picked up
  properly.  Cost - those jobs will run later.  Resolves bug 445894.  Changed default
  random delay to 60 minutes from 120 to reduce the cost.
- Eliminate the weekly package cleaning script.  Replace with logic in the daily script 
  to clean the packages and the metadata once per week (otherwise corrupted metadata messes 
  things up indefinitely).  Resolves bug 526452.  Removal of seperate weekly script resolves
  bug 524461. 
- Use find to clear stale locks at start of run, so add findutils req.
* Fri Aug 21 2009 Alec Habig <ahabig@umn.edu> - 0.8.4-2
- pushing to updates-testing to resolve bug 515814, dealing with stale lockfiles

* Wed Aug 5 2009 Alec Habig <ahabig@umn.edu> - 0.8.4-1
- deal with stale lockdirs

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Dec 14 2008 Alec Habig <ahabig@umn.edu> - 0.8.3-1
- Change writes to YUMTMP to be appends to work around selinux policy
  oddness in bug 431588
- this requires the added policycoreutils dependancy

* Wed Mar 12 2008 Alec Habig <ahabig@umn.edu> - 0.8.2-1
- Add YUM_PARAMETER for user specified parameters to the sysconf file

* Wed Mar 5 2008 Alec Habig <ahabig@umn.edu> - 0.8.1-1
- Fix bugs 429716 and 435968

* Wed Feb 20 2008 Alec Habig <ahabig@umn.edu> - 0.8-1
- Change tmp dir to /var/run (bug 431588)
- add DAYS_OF_WEEK and CHECK_FIRST config options (bug 429716)
- fix CHECK_ONLY thinking updates are present on repository problems (bug 431321)

* Tue Jan 8 2008 Alec Habig <ahabig@umn.edu> - 0.7-1
- Add DEBUG_LEVEL, ERROR_LEVEL sysconfig options (bug 426047)
- Add MAILTO, RANDOMWAIT, and SYSTEMNAME sysconfig options (bug 426755)

* Mon Oct 15 2007 Alec Habig <ahabig@umn.edu> - 0.6-1
- Fixes for bug 327401: checkonly formatting fix and make lockfile
  grabbing an atomic action using mkdir instead of touch
- Added downloadonly option (bug 333131)
- Don't update yum if set to checkonly (bug 333111)

* Sat Sep 29 2007 Alec Habig <ahabig@umn.edu> - 0.5-1
- Added a lockfile to prevent multiple instances of the cron scripts
  from running and hanging, see bug 311661

* Mon Sep 3 2007 Alec Habig <ahabig@umn.edu> - 0.4-1
- Fix bug in cron.daily which was preventing updates from running
- Integrate Frank's checkonly patches into the source

* Mon Aug 27 2007 Frank Wittig <fw@weisshuhn.de> - 0.3-3
- Actual update action is now configurable. It does check-only now if
  configured. Default behaviour is to do updates.

* Tue Aug 21 2007 Alec Habig <ahabig@umn.edu> - 0.3-2
- Changed over to dist tags, to play more nciely with the Fedora build system
- License changed "GPL" to "GPLv2" since rpmlint now cares

* Mon Aug 20 2007 Alec Habig <ahabig@umn.edu> - 0.3-1
- Bumped version for release to fedora-extras

* Mon Jul 30 2007 Alec Habig <ahabig@umn.edu> - 0.2-5
- spec file updates in response to review.

* Thu Jun 28 2007 Alec Habig <ahabig@umn.edu> - 0.2-4
- Check for existence of old "yum"-named init script.  If so, assume
  it's an upgrade, remove them, and do a condrestart.

* Tue Jun 19 2007 Alec Habig <ahabig@umn.edu> - 0.2-3
- the yum-daily.yum script marked noreplace since it's a config-y file.  
  Not so sure we want to do this, but it keeps rpmlint happy.
- service starting/stopping changed to conform to standards.

* Tue Jun 19 2007 Alec Habig <ahabig@umn.edu> - 0.2-2
- More specfile work, functionality and installed files not changed

* Tue Jun 19 2007 Alec Habig <ahabig@umn.edu> - 0.2-1
- Upgrade spec file to meet fedora-extras requirements
- rename init script yum-cron to meet rpmlint guidelines
- See bugzilla #212507 for the history of this package

* Tue Oct 31 2006 Alec Habig <ahabig@umn.edu> - 0.1-1
- Initial packaging of old scripts as a standalone rpm for use in FC6
