%global momorel 5
%global srcname libgmail-docs
%global pythonver 2.7
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Documents and examples for python-libgmail
Name: python-libgmail-docs
Version: 0.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Documentation
URL: http://libgmail.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/libgmail/%{srcname}_%{version}.tgz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: python-devel >= %{pythonver}
BuildArch: noarch 

%description
Documents and examples for python-libgmail.

%prep
%setup -q -n %{srcname}_%{version}

# fix up permission, this is %%doc
chmod 644 COPYING README *.py

# fix a dos newline
sed -i 's/\r//g' readmail.py

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc API COPYING README *.py

%changelog
* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-1m)
- initial package for python-libgmail
