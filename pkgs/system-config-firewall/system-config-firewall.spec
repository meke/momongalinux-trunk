%global momorel 2

Summary: A graphical interface for basic firewall setup
Name: system-config-firewall
Version: 1.2.29
Release: %{momorel}m%{?dist}
URL: http://fedorahosted.org/system-config-firewall
License: GPLv2+
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Source0: %{name}-%{version}.tar.bz2
Patch0: system-config-firewall-1.2.27-rhbz#717985.patch 
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: intltool
Obsoletes: system-config-securitylevel
Provides: system-config-securitylevel = 1.7.0
Requires: pygtk2
Requires: python
Requires: python-decorator
Requires: usermode-gtk >= 1.94
Requires: system-config-firewall-base = %{version}-%{release}
Requires: system-config-firewall-tui = %{version}-%{release}
Requires: hicolor-icon-theme
Requires: pygtk2-libglade
Requires: gtk2 >= 2.6

%description
system-config-firewall is a graphical user interface for basic firewall setup.

%package base
Summary: system-config-firewall base components and command line tool
Group: System Environment/Base
Obsoletes: lokkit
Provides: lokkit = 1.7.0
Requires: python
Requires: iptables >= 1.2.8
Requires: iptables-ipv6
Requires: libselinux-utils >= 1.19.1

%description base
Base components of system-config-firewall with lokkit, the command line tool
for basic firewall setup.

%package tui
Summary: A text interface for basic firewall setup
Group: System Environment/Base
Obsoletes: lokkit
Obsoletes: system-config-securitylevel-tui
Provides: lokkit = 1.7.0
Provides: system-config-securitylevel-tui = 1.7.0
Requires: system-config-firewall-base = %{version}-%{release}
Requires: iptables >= 1.2.8
Requires: iptables-ipv6
Requires: system-config-network-tui
Requires: newt-python
Requires: libselinux >= 1.19.1

%description tui
system-config-firewall-tui is a text and commandline user
interface for basic firewall setup.

%prep
%setup -q
%patch0 -p1 

%build
%configure

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

desktop-file-install --vendor system --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category System \
    --remove-category SystemSetup \
    --remove-category Application \
  %{buildroot}%{_datadir}/applications/system-config-firewall.desktop

%find_lang %{name} --all-name

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%triggerpostun -- %{name} < 1.1.0
%{_datadir}/system-config-firewall/convert-config

%triggerpostun -- system-config-securitylevel
%{_datadir}/system-config-firewall/convert-config

%files
%defattr(-,root,root)
%{_bindir}/system-config-firewall
%if %{with usermode}
%{_datadir}/system-config-firewall/system-config-firewall
%endif
%defattr(0644,root,root) 
%{_sysconfdir}/dbus-1/system.d/org.fedoraproject.Config.Firewall.conf
%{_datadir}/dbus-1/system-services/org.fedoraproject.Config.Firewall.service
%{_datadir}/polkit-1/actions/org.fedoraproject.config.firewall.policy
%{_datadir}/system-config-firewall/fw_gui.* 
%{_datadir}/system-config-firewall/fw_dbus.*
%{_datadir}/system-config-firewall/fw_nm.*
%{_datadir}/system-config-firewall/gtk_*
%{_datadir}/system-config-firewall/*.glade
%attr(0755,root,root) %{_datadir}/system-config-firewall/system-config-firewall-mechanism.*
%{_datadir}/applications/system-config-firewall.desktop
%{_datadir}/icons/hicolor/*/apps/preferences-system-firewall*.*
%if %{with usermode}
%config /etc/security/console.apps/system-config-firewall
%config /etc/pam.d/system-config-firewall
%endif

%files base -f %{name}.lang
%defattr(-,root,root)
%doc COPYING
%{_sbindir}/lokkit
%attr(0755,root,root) %{_datadir}/system-config-firewall/convert-config
%dir %{_datadir}/system-config-firewall
%defattr(0644,root,root)
%{_datadir}/system-config-firewall/etc_services.*
%{_datadir}/system-config-firewall/fw_compat.*
%{_datadir}/system-config-firewall/fw_config.*
%{_datadir}/system-config-firewall/fw_firewalld.*
%{_datadir}/system-config-firewall/fw_functions.*
%{_datadir}/system-config-firewall/fw_icmp.*
%{_datadir}/system-config-firewall/fw_iptables.*
%{_datadir}/system-config-firewall/fw_lokkit.*
%{_datadir}/system-config-firewall/fw_parser.*
%{_datadir}/system-config-firewall/fw_selinux.*
%{_datadir}/system-config-firewall/fw_services.*
%{_datadir}/system-config-firewall/fw_sysconfig.*
%{_datadir}/system-config-firewall/fw_sysctl.*
%ghost %config(missingok,noreplace) /etc/sysconfig/system-config-firewall

%files tui
%defattr(-,root,root)
%{_bindir}/system-config-firewall-tui
%{_datadir}/system-config-firewall/fw_tui.*


%changelog
* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.29-2m)
- [SECURITY] CVE-2011-2520

* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.29-1m)
- update 1.2.29

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.25-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.25-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.25-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.25-2m)
- Require: python-decorator

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.25-1m)
- update 1.2.25
- remove Requires: rhpl

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.16-1m)
- sync with Fedora 11 (1.2.16-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-3m)
- rebuild against rpm-4.6

* Thu Oct  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-2m)
- [CRITICAL] revise %%files to avoid conflicting

* Thu Oct  9 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.10-1m)
- version up 1.2.10
- lokkit: fixed path for system-config-firewall-tui (rhbz#454108)
- updated translations for: it, fr, nl, ru, sr, sr@latin

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.9-1m)
- update to 1.2.9

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.7-2m)
- revise %%files to avoid conflicting

* Mon Jun  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-1m)
- update 1.2.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.10-3m)
- %%{_sysconfdir}/sysconfig/system-config-firewall is provided by package tui

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.10-2m)
- rebuild against gcc43

* Tue Nov 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.10-1m)
- rename system-config-firewall

* Fri Feb 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.0-1m)
- update 1.7.0

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.16-6m)
- delete pyc pyo

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.16-5m)
- remove category X-Red-Hat-Base SystemSetup

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.16-4m)
- delete duplicated dir

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.16-3m)
- revise %%file (conflict)

* Sun May 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.16-2m)
- rebuild against slang-2.0.5

* Sun May 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.16-1m)
- update 1.6.16

* Thu May 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.18-4m)
- rebuild against newt-0.52.2

* Thu Apr  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.18-3m)
- remove duplicated files

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.18-2m)
- add System to Categories of desktop file for KDE

* Mon Nov 29 2004 TAKAHASHI Tamotsu <tamo>
- (1.4.18-1m)

+ * Sat Nov 13 2004 Dan Walsh <dwalsh@redhat.com> 1.4.18-1
+ - Change to match getsebool syntax
+ 
+ * Thu Nov 04 2004 Dan Walsh <dwalsh@redhat.com> 1.4.17-1
+ - Call setsebool properly, change location of selinux stuff to /usr/sbin
+ 
+ * Thu Nov 04 2004 Dan Walsh <dwalsh@redhat.com> 1.4.16-1
+ -Change nfs_home_dirs to use_nfs_home_dirs
+ 
+ * Wed Oct 20 2004 Dan Walsh <dwalsh@redhat.com> 1.4.15-1
+ - Fix descriptions
+ 
+ * Wed Oct 20 2004 Dan Walsh <dwalsh@redhat.com> 1.4.13-1
+ - Add description for httpd_unified
+ 
+ * Fri Oct 15 2004 Dan Walsh <dwalsh@redhat.com> 1.4.12-1
+ - Fix crash on badly formed /etc/selinux/config files
+ 
+ * Thu Oct 14 2004 Paul Nasrat <pnasrat@redhat.com> 1.4.11-1
+ - GTK deprecation messages
+ 
+ * Wed Oct 13 2004 Bill Nottingham <notting@redhat.com> 1.4.10-1
+ - fix cups browsing line (#131745)
+ 
+ * Tue Oct 12 2004 Dan Walsh <dwalsh@redhat.com> 1.4.9-1
+ - Don't apply if selinux not installed
+ 
+ * Fri Oct 08 2004 Paul Nasrat <pnasrat@redhat.com> 1.4.8-1
+ - Firstboot bug fix
+ 
+ * Fri Oct 01 2004 Paul Nasrat <pnasrat@redhat.com> 1.4.7-1
+ - mDNS
+ - Translations
+ 
+ * Wed Sep 29 2004 Dan Walsh <dwalsh@redhat.com> 1.4.6-1
+ - Fix handling of booleans
+ 
+ * Thu Sep 23 2004 Dan Walsh <dwalsh@redhat.com> 1.4.5-1
+ - Fix for missing /etc/selinux
+ 
+ * Tue Sep 21 2004 Dan Walsh <dwalsh@redhat.com> 1.4.4-1
+ - Fix for bad /etc/selinux/config
+ 
+ * Tue Sep 07 2004 Paul Nasrat <pnasrat@redhat.com> 1.4.3-1
+ - Translatable desktop
+ 
+ * Thu Aug 12 2004 Dan Walsh <dwalsh@redhat.com> 1.4.2-2
+ - Bug fix Boolean support 
+ 
+ * Fri Jul 30 2004 Dan Walsh <dwalsh@redhat.com> 1.4.2-1
+ - Add Boolean support 
+ 
+ * Tue Jul 27 2004 Dan Walsh <dwalsh@redhat.com> 1.4.1-4
+ - Fix so only changes made if gui activated.
+ - Save backup copies of configs
+ 
+ * Tue Jul 27 2004 Dan Walsh <dwalsh@redhat.com> 1.4.1-3
+ - Fix several problems including Tunables being reported incorrectly.
+ - Allow tool to reload policy if tunables change
+ - Allow tool to change enforcing mode 
+ 
+ * Fri Jul 16 2004 Dan Walsh <dwalsh@redhat.com> 1.3.14-2
+ - Remove checkbox from toplevel menu of tunables
+ 
+ * Thu Jul 15 2004 Dan Walsh <dwalsh@redhat.com> 1.3.14-1
+ - Turn on SELinux support
+ 
+ * Thu May 27 2004 Dan Walsh <dwalsh@redhat.com> 1.3.13-2
+ - Change lokkit to support new SELinux mode
+ 
+ * Fri May 21 2004 Bill Nottingham <notting@redhat.com> 1.3.13-1
+ - fix typo (#122907)
+ 
+ * Fri Apr 30 2004 Brent Fox <bfox@redhat.com> 1.3.12-1
+ - turn off SELinux widgets for FC2 (bug #122046)

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3.11-1m)
- import from Fedora.

* Thu Apr 15 2004 Brent Fox <bfox@redhat.com> 1.3.11-1
- comment out SELinux tunable widgets for now

* Thu Apr 15 2004 Brent Fox <bfox@redhat.com> 1.3.10-6
- test if self.doc is None in read_tunable_file, not read_selinux_file

* Tue Apr 13 2004 Brent Fox <bfox@redhat.com> 1.3.10-5
- don't write out xml file if it doesn't exist

* Tue Apr 13 2004 Brent Fox <bfox@redhat.com> 1.3.10-4
- don't try to write tunable.xml if the file doesn't exist

* Mon Apr 12 2004 Brent Fox <bfox@redhat.com> 1.3.10-3
- fix icon path (bug #120183)

* Mon Apr  5 2004 Brent Fox <bfox@redhat.com> 1.3.10-2
- more work on SELinux code

* Thu Apr  1 2004 Brent Fox <bfox@redhat.com> 1.3.10-1
- add SELinux widgets and restructure UI accordingly

* Thu Mar 25 2004 Brent Fox <bfox@redhat.com> 1.3.9-1
- replace the other ports widgets (bug #111930)

* Wed Mar 24 2004 Bill Nottingham <notting@redhat.com> 1.3.8-1
- fix writing of config file if neither of --disabled or --enabled are
  passed (#118667, redux)

* Fri Mar 19 2004 Bill Nottingham <notting@redhat.com> 1.3.7-1
- prefer commandline arguments to config file arguments (#118667)

* Tue Mar 16 2004 Jeremy Katz <katzj@redhat.com> 1.3.6-1
- fix segfault in config reading if config files don't exist
- don't flush iptables chains if run with --nostart

* Thu Mar 11 2004 Bill Nottingham <notting@redhat.com > 1.3.5-1
- read in old config in the TUI (#25510)
- have https tag along with http (#61958)
- fix segfault (#88533)

* Fri Mar  5 2004 Brent Fox <bfox@redhat.com> 1.3.4-1
- don't do strlen() on random pointer (bug #117183)

* Thu Mar  4 2004 Brent Fox <bfox@redhat.com> 1.3.3-1
- fix tab ordering bug (bug #116913)

* Tue Feb  3 2004 Brent Fox <bfox@redhat.com> 1.3.2-1
- F12 functionality fixed

* Mon Jan 12 2004 Brent Fox <bfox@redhat.com> 1.3.1-1
- break up really long strings (bug #102455)

* Tue Nov 18 2003 Brent Fox <bfox@aspen.chinfox.net> 1.3.0-1
- rename to system-config-securitylevel
- obsoletes redhat-config-securitylevel
- convert to Python2.3

* Thu Oct 16 2003 Brent Fox <bfox@redhat.com> 1.2.11-1
- require iptables >=1.2.8 (bug #104777)

* Fri Oct  3 2003 Bill Nottingham <notting@redhat.com> 1.2.10-1
- minor code cleanup

* Fri Oct  3 2003 Bill Nottingham <notting@redhat.com> 1.2.9-1
- fix interactive disabling of firewall in TUI (#106243)

* Wed Sep 17 2003 Bill Nottingham <notting@redhat.com> 1.2.8-2
- rebuild

* Wed Sep 17 2003 Bill Nottingham <notting@redhat.com> 1.2.8-1
- allow ICMP in general (#104561)

* Mon Sep 15 2003 Brent Fox <bfox@redhat.com> 1.2.7-2
- bump release number

* Mon Sep 15 2003 Brent Fox <bfox@redhat.com> 1.2.7-1
- add Requires for rhpl

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.2.6-1
- fix typo (bug #101802)

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.2.5-1
- tag on every build

* Tue Aug 12 2003 Brent Fox <bfox@redhat.com> 1.2.3-2
- bump relnum and rebuild

* Tue Aug 12 2003 Brent Fox <bfox@redhat.com> 1.2.3-1
- some string changes

* Mon Aug 11 2003 Brent Fox <bfox@redhat.com> 1.2.2-2
- bump relnum and rebuild

* Mon Aug 11 2003 Brent Fox <bfox@redhat.com> 1.2.2-1
- reorder some UI elements

* Thu Aug  7 2003 Bill Nottingham <notting@redhat.com> 1.2.1-1
- fix rules (#101841)

* Tue Aug  5 2003 Bill Nottingham <notting@redhat.com> 1.2.0-2
- woops, RPM 101 (#101708)

* Mon Aug  4 2003 Bill Nottingham <notting@redhat.com> 1.2.0-1
- add patch for stateful firewalling (#87585, <dax@gurulabs.com>)
- tweak tools appropriately (katzj@redhat.com), obsolete lokkit/gnome-lokkit

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.1.3-2
- bump relnum and rebuild

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.1.3-1
- use rhpl translation module

* Tue May 27 2003 Brent Fox <bfox@redhat.com> 1.1.2-1
- bump rev and rebuild

* Tue Feb  4 2003 Brent Fox <bfox@redhat.com> 1.1.1-3
- fix return codes for firstboot reconfig mode
- read config file in launch mode

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.1.1-1
- bump and build

* Mon Jan 27 2003 Brent Fox <bfox@redhat.com> 1.1.0-4
- remove typo

* Wed Jan 22 2003 Jeremy Katz <katzj@redhat.com> 1.1.0-3
- match dhcp handling of anaconda and lokkit

* Tue Dec 10 2002 Brent Fox <bfox@redhat.com> 1.1.0-2
- fix bug 74913

* Tue Dec 10 2002 Brent Fox <bfox@redhat.com> 1.1.0-1
- save configuration in a config file in /etc/sysconfig/ so we remember settings

* Mon Nov 18 2002 Brent Fox <bfox@redhat.com>
- add a Requires for gnome-lokkit (bug #78057)

* Tue Nov 12 2002 Brent Fox <bfox@redhat.com> 1.0.1-4
- Latest translations

* Thu Oct 10 2002 Brent Fox <bfox@redhat.com> 1.0.1-3
- Mark the word Mail for translation.  Fixes bug 75592

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.1-1
- Convert to noarch

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.0-3
- pull in latest German translations

* Tue Aug 27 2002 Brent Fox <bfox@redhat.com> 1.0.0-2
- Rebuild for translations

* Mon Aug 26 2002 Brent Fox <bfox@redhat.com> 1.0.0-1
- connect window to destroy signal

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-4
- pull translations into desktop file

* Mon Aug 12 2002 Tammy Fox <tfox@redhat.com> 0.9.9-3
- replace System with SystemSetup in desktop file categories

* Sun Aug 11 2002 Brent Fox <bfox@redhat.com> 0.9.9-2
- fix bug 71187

* Tue Aug 06 2002 Brent Fox <bfox@redhat.com>
- Mark strings for translation

* Mon Aug 05 2002 Brent Fox <bfox@redhat.com> 0.9.9-1
- convert combo widget into an OptionMenu

* Fri Aug 02 2002 Brent Fox <bfox@redhat.com> 0.9.8-1
- Make changes for new pam timestamp policy

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-3
- fix Makefiles and spec files so that translations get installed

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-2
- update spec file for public beta 2

* Wed Jul 24 2002 Tammy Fox <tfox@redhat.com> 0.9.4-3
- Fix desktop file (bug #69484)

* Tue Jul 16 2002 Brent Fox <bfox@redhat.com> 0.9.4-2
- bump rev num and rebuild

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-2
- Update changelogs and rebuild

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-1
- Update changelogs and rebuild

* Mon Jul 01 2002 Brent Fox <bfox@redhat.com> 0.9.2-1
- Bump rev number

* Thu Jun 27 2002 Brent Fox <bfox@redhat.com> 0.9.1-2
- Changed window title

* Wed Jun 26 2002 Brent Fox <bfox@redhat.com> 0.9.1-1
- Fixed description

* Tue Jun 25 2002 Brent Fox <bfox@redhat.com> 0.9.0-5
- Create pot file

* Mon Jun 24 2002 Brent Fox <bfox@redhat.com> 0.9.0-4
- Fix spec file

* Fri Jun 21 2002 Brent Fox <bfox@redhat.com> 0.9.0-3
- init doDebug
- reverse ok/cancel buttons

* Thu Jun 20 2002 Brent Fox <bfox@redhat.com> 0.9.0-2
- Pass doDebug into launch, not init
- Add snapsrc to Makefile

* Sun May 26 2002 Brent Fox <bfox@redhat.com> 0.1.0-7
- Add debug flag

* Tue Nov 27 2001 Brent Fox <bfox@redhat.com>
- initial coding and packaging

