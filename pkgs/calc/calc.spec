%global momorel 3

# Enabling this option invokes section 3 of the LGPL, applying the terms of
# the ordinary GNU General Public License version 2 instead of the LGPL.
# See the included "calc-converted-to-gpl.txt" source file for details.
%define with_readline 1

# This is disabled right now because it prevents correct linking. The issues
# here are related to the issues with linking with readline, in that the 
# split between libraries is poorly defined.
%define with_custom_interface 1

%if %{with_readline}
License:       GPLv2
%else
License:       LGPL
%endif

Name:          calc
Version:       2.12.4.2
Release:       %{momorel}m%{?dist}
Summary:       Arbitrary precision arithmetic system and calculator

Group:         Applications/Engineering
URL:           http://isthe.com/chongo/tech/comp/calc/
Source0:       http://www.isthe.com/chongo/src/calc/calc-%{version}.tar.bz2
NoSource: 0
Source1:       calc-converted-to-gpl.txt
Source2:       calc-COPYING-GPL
Patch0:        calc-2.12.2.1-entangled-readline.patch
Patch1:        calc-2.12.2.1-no-custom-means-no-custom.patch

BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: sed, util-linux, groff

%if %{with_readline}
BuildRequires: ncurses-devel >= 5.2-26, readline-devel >= 4.2
%endif

Requires:      less >= 358

Provides:      %{_bindir}/calc


%description
Calc is an arbitrary precision C-like arithmetic system that is a
calculator, an algorithm-prototyper, and a mathematical research tool. Calc
comes with a rich set of built-in mathematical and programmatic functions.
%if %{with_readline}
Note: this copy of Calc is linked against the GNU Readline library and has
been converted to the ordinary GPL as per section 3 of the LGPL. See the
included calc-converted-to-gpl.txt document for details.
%endif


%package libs
Summary:       Libraries for the calc arithmetic system
Group:         System Environment/Libraries

%description libs
Shared libraries used by the calc command line calculator and other programs
using its arbitrary precision arithmetic routines.


%package devel
Summary:        Development files for the calc arithmetic system
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}

%description devel
This package contains files necessary to build applications which use the
calc arbitrary precision arithmetic system.


%package stdrc
Summary:      Standard resource files the calc arithmetic system
Group:        Applications/Engineering
Requires:     %{name} = %{version}-%{release}

%description stdrc
This package contains the standard calc resource files and several calc
shell scripts. They serve as examples of the calc language and may also be
useful in themselves.



%prep
%setup -q

#%%patch0 -p1 -b .link
#%%patch1 -p1 -b .nomeansno

%if %{with_readline}
  for f in help.c version.c calc.man cal/* $( ls help/*|grep  '^help/credit$' ) ; do
    sed -i -e's/version 2.1 \(.*GNU\)/version 2 \1/;s/COPYING.LGPL/COPYING-GPL/;s/copying.lgpl/copying-gpl/;s/GNU LGPL/GNU GPL/;s/GNU Lesser General/GNU General/' $f
  done
  cp -pf %{SOURCE1} COPYING
  cp -pf %{SOURCE2} COPYING-GPL
%endif


%build
# note parallel make (-j3, or whatever) doesn't work correctly.
make DEBUG="%{optflags}" \
%if %{with_custom_interface}
     ALLOW_CUSTOM="-DCUSTOM" \
%else
     ALLOW_CUSTOM="" \
%endif
     LD_SHARE="" \
%if %{with_readline}
     USE_READLINE="-DUSE_READLINE" \
     READLINE_LIB="-lreadline" \
     READLINE_EXTRAS="-lhistory -lncurses" \
%else
     USE_READLINE="" \
%endif
     HAVE_FPOS="-DHAVE_NO_FPOS" \
     T=%{buildroot} \
     BINDIR=%{_bindir} \
     LIBDIR=%{_libdir} \
     CALC_SHAREDIR=%{_datadir}/%{name} \
     CALC_INCDIR=%{_includedir}/calc \
     SCRIPTDIR=%{_datadir}/%{name}/cscript \
     MANDIR=%{_mandir}/man1 \
     Q="" V="@" \
     NROFF=nroff \
     all


%install
rm -rf %{buildroot}

make T=%{buildroot} \
     BINDIR=%{_bindir} \
     LIBDIR=%{_libdir} \
     CALC_SHAREDIR=%{_datadir}/%{name} \
     CALC_INCDIR=%{_includedir}/calc \
     SCRIPTDIR=%{_datadir}/%{name}/cscript \
     MANDIR=%{_mandir}/man1 \
     install

%if %{with_readline} 
  rm -f %{buildroot}/%{_datadir}/%{name}/help/COPYING-LGPL
  # mode 444 to match the other files
  install -p -m 444 COPYING-GPL %{buildroot}/%{_datadir}/%{name}/help/
  rm -f %{buildroot}/%{_datadir}/%{name}/bindings
%endif

%if ! %{with_custom_interface}
  # if we don't enable the custom interface, don't ship it.
  rm -f %{buildroot}/%{_libdir}/libcustcalc.so*
  rm -f %{buildroot}/%{_datadir}/%{name}/custom/*
  rmdir %{buildroot}/%{_datadir}/%{name}/custom/
%endif

# Changing permissions of executables to 755 to shut up rpmlint.
chmod 755 %{buildroot}%{_datadir}/%{name}/cscript/*
chmod 755 %{buildroot}%{_bindir}/calc

# move these so the doc macro can find them
mv %{buildroot}%{_datadir}/%{name}/README README-standard-resource
mv cscript/README README-cscript


%check
make chk
     

%clean
rm -rf %{buildroot}


%post libs -p /sbin/ldconfig


%postun libs -p /sbin/ldconfig


%files
%defattr(-, root, root, -)
%doc BUGS CHANGES COPYING COPYING-%( echo %{license}|sed s/v2// )
%{_bindir}/calc
%{_mandir}/man1/calc.1*
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/help
%{_datadir}/%{name}/help/*
%dir %{_datadir}/%{name}/custhelp
%{_datadir}/%{name}/custhelp/*
%if ! %{with_readline}
%{_datadir}/%{name}/bindings
%endif

%files libs
%defattr(-, root, root, -)
%doc BUGS CHANGES COPYING COPYING-%( echo %{license}|sed s/v2// )
%{_libdir}/libcalc.so.*
%if %{with_custom_interface}
%{_libdir}/libcustcalc.so.*
%endif

%files devel
%defattr(-, root, root, -)
%doc LIBRARY
%{_libdir}/libcalc.so
%if %{with_custom_interface}    
%{_libdir}/libcustcalc.so
%endif
%dir %{_includedir}/calc
%{_includedir}/calc/*.h

%files stdrc
%defattr(-, root, root, -)
%doc README-standard-resource README-cscript
%dir %{_datadir}/%{name}/cscript
%{_datadir}/%{name}/cscript/*
%if %{with_custom_interface}
%dir %{_datadir}/%{name}/custom
%{_datadir}/%{name}/custom/*
%endif
%{_datadir}/%{name}/*.cal
%{_datadir}/%{name}/*.line


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.4.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.4.2-2m)
- rebuild for new GCC 4.5

* Fri Nov 27 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (2.12.4.2-1m)
- update

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.3.3-6m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.3.3-5m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.3.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Apr  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.3.3-3m)
- specify NROFF=nroff

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.3.3-2m)
- rebuild against rpm-4.6

* Fri Oct 31 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (2.12.3.3-1m)
- updated to 2.12.3.3

* Wed Apr 16 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.12.3.0-1m)
- update to 2.12.3.0

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.2.1-2m)
- calc Provides: %%{_bindir}/calc

* Mon Apr 14 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.12.2.1-1m)
- import to Momonga (based on the package in Fedora (2.12.2.1-12))

* Wed Feb 20 2008 Matthew Miller <mattdm@mattdm.org> - 2.12.2.1-12
- add util-linux as a build prereq

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.12.2.1-11
- Autorebuild for GCC 4.3

* Fri Sep 21 2007 Matthew Miller <mattdm@mattdm.org> - 2.12.2.1-10
- add HAVE_FPOS="-DHAVE_NO_FPOS" to make ppc build work properly. Thanks
  to Denis Leroy and David Woodhouse. RH Bug #299581.

* Thu Sep 20 2007 Matthew Miller <mattdm@mattdm.org> - 2.12.2.1-9
- Remove extra license choice information from package release field to
  properly comply with the packaging guidelines so as to not make Spot sad.
  Put a note in the description instead.
- Today is Thursday.

* Wed Sep 20 2007 Matthew Miller <mattdm@mattdm.org> - 2.12.2.1-8
- initial package for Fedora
- clear old and long pre-fedora changelog
- review request rhbug #227570
