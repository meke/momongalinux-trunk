%global momorel 4

Summary: The Ruby extension for the GSL
Name: ruby-gsl

%global srcname rb-gsl
%global rbgslver 1.14.3
%global rbgslfileid 69960

%{?!do_tests:	%global do_tests 0}

Version: %{rbgslver}
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://rb-gsl.rubyforge.org/

Source0: http://rubyforge.org/frs/download.php/%{rbgslfileid}/rb-gsl-%{rbgslver}.tar.gz 
NoSource: 0
Patch0: ruby-gsl-1.6.3.run-test-topdir.patch
Patch1: ruby-gsl-1.10.3-test-matrix.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-narray
BuildRequires: gsl-devel >= 1.10

Requires: gsl >= 1.10
Requires: ruby-narray

%description
The Ruby/GSL provides a ruby interface to the GSL (GNU Scientific
Library) for numerical computation with Ruby. This requires the GSL
library, which is found on the development website
http://sources.redhat.com/gsl/.

%prep
%setup -q -n %{srcname}-%{rbgslver}
%patch0 -p1
%patch1 -p1

%build
ruby setup.rb --quiet config
ruby setup.rb setup

# tests - oops, some of the tests fail. (1.6.2-2m) See run-test.sh.
if test "%{do_tests}" = 1; then
	pushd tests; sh run-test.sh; popd
fi

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
ruby setup.rb --quiet config
ruby setup.rb install --prefix=%{buildroot}
chmod u+w %{buildroot}%{ruby_sitearchdir}/rb_gsl.so

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog
%doc README THANKS
%doc examples tests rd html
%{ruby_sitelibdir}/gsl.rb
%{ruby_sitelibdir}/rbgsl.rb
%{ruby_sitelibdir}/gsl/
%{ruby_sitelibdir}/ool.rb
%{ruby_sitelibdir}/ool/
%{ruby_sitearchdir}/rb_gsl.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14.3-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.14.3-1m)
- update 1.14.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10.3-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10.3-2m)
- rebuild against gcc43

* Mon Feb 18 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10.3-1m)
- version up 1.10.3

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.1-3m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.1-2m)
- rebuild against ruby-1.8.6-4m

* Wed Apr 26 2006 zunda <zunda at freeshell.org>
- (1.8.1-1m)
- source update
- ruby-gsl-1.8.1.test-odeiv.patch: odeiv.rb did not follow change of
  a method name
- ruby-gsl-1.8.1.run-test.sh.patch: range.rb passes. Please note that
  some tests still fail without returning the error code. Execute
  /usr/share/doc/ruby-gsl-*/tests/run-test.sh for details.

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-2m)
- add gcc4 patch
- Patch1: ruby-gsl-1.6.3-gcc4.patch

* Wed May  8 2005 zunda <zunda at freeshell.org>
- (1.6.3-1m)
- source update: better relation with NArray
- removed ruby-gsl-1.6.2.tests.patch
- added ruby-gsl-1.6.3.run-test-topdir.patch to correct include path

* Wed Apr 13 2005 zunda <zunda at freeshell.org>
- (1.6.2-2m)
- Patch0: ruby-gsl-1.6.2.tests.patch to make following test to pass:
  multifit/test_multifit.rb multifit/test_brown.rb multifit/test_enso.rb
  rng.rb roots.rb

* Mon Apr 11 2005 zunda <zunda at freeshell.org>
- (1.6.2-1m)
- renamed from ruby-gsl-tsunesada

* Mon Aug  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.3-5m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.2.3-4m)
- build against ruby-narray-0.5.7

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.3-3m)
- build against ruby-narray-0.5.6p2

* Tue Apr 17 2002 zunda <zunda@kondara.org>
- (0.2.3-2k)
- first release
- following test programs complains about me ...
  fft.rb, histogram.rb, math.rb, matrix.rb, odeiv.rb, with_narray.rb
