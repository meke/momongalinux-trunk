%global momorel 11
%global bootstrap %{?java_bootstrap1:1}%{!?java_bootstrap1:%{?java_bootstrap2:1}%{!?java_bootstrap2:0}}
%define _with_coreonly %{?java_bootstrap2:1}%{!?java_bootstrap2:0}

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

# If you want only core modules to be built, 
# give rpmbuild option '--with coreonly'

%define with_coreonly %{?_with_coreonly:1}%{!?_with_coreonly:0}
%define without_coreonly %{!?_with_coreonly:1}%{?_with_coreonly:0}

%define full_name	jakarta-%{name}
%define tomcat3appsdir	%{_localstatedir}/tomcat3/webapps
%define tomcat4appsdir	%{_localstatedir}/lib/tomcat4/webapps
%define tomcat5appsdir  %{_localstatedir}/lib/tomcat5/webapps
%define tomcat5ctxdir   %{_sysconfdir}/tomcat5/Catalina/localhost
%define webapps		blank documentation example examples tiles-documentation
%define webapplibs commons-beanutils commons-digester commons-fileupload commons-validator oro struts
%define section		free

Name:		struts
Version:	1.2.9
Release:	4jpp.%{momorel}m%{?dist}
Epoch:		0
Summary:	Web application framework
License:	Apache
Group:          Development/Libraries
Source0:	%{name}-%{version}-src-RHCLEAN.tar.gz
Source2:	tomcat4-context-allowlinking.xml
Source3:	tomcat5-context-allowlinking.xml
#Patch0:		%{name}-%{version}.build.patch
Patch0:		struts-1.2.9-strutsel-build_xml.patch
Patch1:		struts-1.2.9-FacesRequestProcessor.patch
Patch2:		struts-1.2.9-HttpServletRequestWrapper.patch
Patch3:		struts-1.2.9-FacesTilesRequestProcessor.patch
Patch4:		struts-1.2.9-strutsfaces-example1-build_xml.patch
Patch5:		struts-1.2.9-strutsfaces-example2-build_xml.patch
Patch6:		struts-1.2.9-strutsfaces-systest1-build_xml.patch
Patch7:		struts-1.2.9.bz157205.patch
Patch8:		jakarta-struts-CVE-2008-2025.patch
Url:		http://struts.apache.org/
Requires:	servletapi5
Requires:	jdbc-stdext
Requires:	jakarta-commons-beanutils
Requires:	jakarta-commons-digester
Requires:	jakarta-commons-fileupload
Requires:	jakarta-commons-validator
Requires:	oro
BuildRequires:	jpackage-utils >= 1.6
BuildRequires:	ant >= 1.6
BuildRequires:	antlr
BuildRequires:	ant-trax
BuildRequires:	ant-nodeps
BuildRequires:	jaxp_transform_impl
BuildRequires:	sed
BuildRequires:	servletapi5
BuildRequires:	jdbc-stdext
BuildRequires:  jakarta-commons-beanutils
BuildRequires:  jakarta-commons-digester
BuildRequires:  jakarta-commons-fileupload
BuildRequires:  jakarta-commons-logging
BuildRequires:  jakarta-commons-validator
BuildRequires:  oro
%if ! %{bootstrap}
BuildRequires:	tomcat5
%endif

%if %{without_coreonly}
BuildRequires:	aspectj
BuildRequires:	httpunit
BuildRequires:	junit
BuildRequires:	jakarta-cactus >= 0:1.7.1-2jpp
BuildRequires:	jakarta-commons-chain
BuildRequires:	jakarta-commons-collections
BuildRequires:	jakarta-commons-httpclient
BuildRequires:	jakarta-commons-lang
BuildRequires:	jakarta-commons-pool
BuildRequires:	jakarta-taglibs-standard
BuildRequires:	jtidy
BuildRequires:	log4j
BuildRequires:	myfaces
BuildRequires:	xalan-j2
%endif

Group:		Development/Languages
%if ! %{gcj_support}
Buildarch:	noarch
%endif
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
Welcome to the Struts Framework! The goal of this project is to provide
an open source framework useful in building web applications with Java
Servlet and JavaServer Pages (JSP) technology. Struts encourages
application architectures based on the Model-View-Controller (MVC)
design paradigm, colloquially known as Model 2 in discussions on various
servlet and JSP related mailing lists.
Struts includes the following primary areas of functionality:
A controller servlet that dispatches requests to appropriate Action
classes provided by the application developer.
JSP custom tag libraries, and associated support in the controller
servlet, that assists developers in creating interactive form-based
applications.
Utility classes to support XML parsing, automatic population of
JavaBeans properties based on the Java reflection APIs, and
internationalization of prompts and messages.
Struts is part of the Jakarta Project, sponsored by the Apache Software
Foundation. The official Struts home page is at
http://jakarta.apache.org/struts.

%package manual
Summary:        Manual for %{name}
Group:          Documentation

%description manual
Documentation for %{name}.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Requires(post):   coreutils
Requires(postun): coreutils

%description javadoc
Javadoc for %{name}.

%if ! %{gcj_support}
%package webapps-tomcat3
Summary:        Sample %{name} webapps for tomcat3
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       jakarta-commons-beanutils
Requires:       jakarta-commons-digester
Requires:       jakarta-commons-fileupload
Requires:       jakarta-commons-validator
Requires:       oro
Requires:       tomcat3
Requires(post): %{name} = %{version}-%{release}
Requires(post): jakarta-commons-beanutils
Requires(post): jakarta-commons-digester
Requires(post): jakarta-commons-fileupload
Requires(post): jakarta-commons-validator
Requires(post): oro
Requires(post): tomcat3
Requires(pre):  tomcat3
Requires(post): coreutils
Requires(preun): coreutils

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description webapps-tomcat3
Sample %{name} webapps for tomcat3.

%package webapps-tomcat4
Summary:        Sample %{name} webapps for tomcat4
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       jakarta-commons-beanutils
Requires:       jakarta-commons-digester
Requires:       jakarta-commons-fileupload
Requires:       jakarta-commons-validator
Requires:       oro
Requires:       tomcat4
Requires(post): %{name} = %{version}-%{release}
Requires(post): jakarta-commons-beanutils
Requires(post): jakarta-commons-digester
Requires(post): jakarta-commons-fileupload
Requires(post): jakarta-commons-validator
Requires(post): oro
Requires(post): tomcat4
Requires(pre):  tomcat4
Requires(post): coreutils
Requires(preun): coreutils

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description webapps-tomcat4
Sample %{name} webapps for tomcat4.
%endif

%package webapps-tomcat5
Summary:        Sample %{name} webapps for tomcat5
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       jakarta-commons-beanutils
Requires:       jakarta-commons-digester
Requires:       jakarta-commons-fileupload
Requires:       jakarta-commons-validator
Requires:       oro
Requires:       tomcat5
Requires(post): %{name} = %{version}-%{release}
Requires(post): jakarta-commons-beanutils
Requires(post): jakarta-commons-digester
Requires(post): jakarta-commons-fileupload
Requires(post): jakarta-commons-validator
Requires(post): oro
Requires(post): tomcat5
Requires(pre):  tomcat5
Requires(post): coreutils
Requires(preun): coreutils

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description webapps-tomcat5
Sample %{name} webapps for tomcat5.

%if %{without_coreonly}
%package chain
Summary:        The %{name} Chain Of Responsibility Adapter
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       jakarta-commons-chain

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description chain
%{summary}.

%package chain-javadoc
Summary:        Javadoc for %{name}-chain
Group:          Documentation
Requires(post):   coreutils
Requires(postun): coreutils

%description chain-javadoc
%{summary}.

%package chain-webapps-tomcat5
Summary:        Sample %{name} chain adapter webapps for tomcat5
Group:          Development/Languages
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-chain = %{version}-%{release}
Requires:         tomcat5
Requires:         jakarta-commons-chain
Requires(post):   %{name}-chain = %{version}-%{release}
Requires(post):   tomcat5
Requires(pre):    tomcat5
Requires(post):   jakarta-commons-chain
Requires(preun):  coreutils

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description chain-webapps-tomcat5
%{summary}.
%endif

%if %{without_coreonly}
%package el
Summary:        The %{name} EL extension
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:         jakarta-taglibs-standard

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description el
This subproject is an extension of the Struts tag library.
Each JSP custom tag in this library is a subclass of an 
associated tag in the Struts tag library. One difference is 
that this tag library does not use "rtexprvalues", it uses
the expression evaluation engine in the Jakarta Taglibs 
implementation of the JSP Standard Tag Library (version 1.0) 
to evaluate attribute values.

%package el-javadoc
Summary:        Javadoc for %{name}-el
Group:          Documentation
Requires(post):   /bin/rm,/bin/ln
Requires(postun): /bin/rm

%description el-javadoc
%{summary}.

%package el-webapps-tomcat5
Summary:        Sample %{name} EL extension webapps for tomcat5
Group:          Development/Languages
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-el = %{version}-%{release}
Requires:         tomcat5
Requires:         jakarta-taglibs-standard
Requires(post):   %{name} = %{version}-%{release}
Requires(post):   %{name}-el = %{version}-%{release}
Requires(post):   tomcat5
Requires(pre):    tomcat5
Requires(post):   jakarta-taglibs-standard
Requires(preun):  coreutils

%description el-webapps-tomcat5
%{summary}.
%endif

%if %{without_coreonly}
%package faces
Summary:        The %{name}-faces integration library
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       myfaces

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description faces
This package contains an add-on library that supports the 
use of JavaServer Faces (JSF) user interface technology in a 
Struts based web application, in place of the Struts custom 
tag libraries.  As a proof of concept, it also includes the 
canonical "struts-example" example web application, converted 
to use JSF tags, as well as tags from the JSP Standard Tag 
Library (JSTL), version 1.0 or later.  It also includes
a very basic Tiles based application, modified in a similar 
manner.

%package faces-javadoc
Summary:        Javadoc for %{name}-faces
Group:          Documentation
Requires(post):   coreutils
Requires(postun): coreutils

%description faces-javadoc
%{summary}.

%package faces-webapps-tomcat5
Summary:        Sample %{name}-faces integration webapps for tomcat5
Group:          Development/Languages
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-faces = %{version}-%{release}
Requires:         jakarta-commons-beanutils
Requires:         jakarta-commons-collections
Requires:         jakarta-commons-digester
Requires:         jakarta-commons-fileupload
Requires:         jakarta-commons-logging
Requires:         jakarta-commons-validator
Requires:         myfaces
Requires:         oro
Requires:         tomcat5
Requires(post):   %{name} = %{version}-%{release}
Requires(post):   %{name}-faces = %{version}-%{release}
Requires(post):   jakarta-commons-beanutils
Requires(post):   jakarta-commons-collections
Requires(post):   jakarta-commons-digester
Requires(post):   jakarta-commons-fileupload
Requires(post):   jakarta-commons-logging
Requires(post):   jakarta-commons-validator
Requires(post):   myfaces
Requires(post):   oro
Requires(post):   tomcat5
Requires(pre):    tomcat5
Requires(preun):  coreutils

%description faces-webapps-tomcat5
%{summary}.
%endif


%prep
rm -rf $RPM_BUILD_ROOT
%setup -n %{name}-%{version}-src -q
%patch0 -b .sav
%patch1 -b .sav
%patch2 -b .sav
%patch3 -b .sav
%patch4 -b .sav
%patch5 -b .sav
%patch6 -b .sav
%patch7 -b .sav
%patch8 -p1 -b .CVE-2008-2025~
# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;

%build

# build struts
export CLASSPATH=
export ANT_OPTS="-Xmx256m"
STRUTS_BUILD_HOME=$(pwd)
ant -Dlibdir=/usr/share/java \
	-Dcommons-beanutils.jar=$(build-classpath commons-beanutils) \
	-Dcommons-digester.jar=$(build-classpath commons-digester) \
	-Dcommons-fileupload.jar=$(build-classpath commons-fileupload) \
	-Dcommons-logging.jar=$(build-classpath commons-logging) \
	-Dcommons-validator.jar=$(build-classpath commons-validator) \
	-Djakarta-oro.jar=$(build-classpath oro) \
        -Djdbc20ext.jar=$(find-jar jdbc-stdext) \
	-Djsp.jar=$(build-classpath jspapi) \
	-Dservlet.jar=$(build-classpath servletapi5) \
	-Dantlr.jar=$(build-classpath antlr) \
	 dist
#	 compile.library compile.webapps compile.javadoc

%if %{without_coreonly}
pushd contrib/struts-chain
export CLASSPATH=$(build-classpath catalina-ant5)
ant -Dlibdir=/usr/share/java \
	-Dcommons-chain.jar=$(build-classpath commons-chain) \
	-Djsp.jar=$(build-classpath jspapi) \
	-Dservlet.jar=$(build-classpath servletapi5) \
	-Dstruts.core.home=$STRUTS_BUILD_HOME \
	 dist
popd
%endif

%if %{without_coreonly}
pushd contrib/struts-el
ant -Dlibdir=/usr/share/java \
	-Dstruts.jar=$STRUTS_BUILD_HOME/dist/lib/struts.jar \
	-Djstl.jar=$(build-classpath taglibs-core) \
	-Djstl-standard.jar=$(build-classpath taglibs-standard) \
	-Dservlet.jar=$(build-classpath servletapi5) \
        -Djdbc20ext.jar=$(find-jar jdbc-stdext) \
	-Dcommons-beanutils.jar=$(build-classpath commons-beanutils) \
	-Dcommons-collections.jar=$(build-classpath commons-collections) \
	-Dcommons-digester.jar=$(build-classpath commons-digester) \
	-Dcommons-logging.jar=$(build-classpath commons-logging) \
	-Dcommons-pool.jar=$(build-classpath commons-pool) \
	-Dcommons-validator.jar=$(build-classpath commons-validator) \
	-Djakarta-oro.jar=$(build-classpath oro) \
	-Djsp.jar=$(build-classpath jspapi) \
	-Dlog4j.jar=$(build-classpath log4j) \
	-Daspectjrt.jar=$(build-classpath aspectjrt) \
	-Dcactus.jar=$(build-classpath cactus-14/cactus) \
	-Dcactus-ant.jar=$(build-classpath cactus-14/cactus-ant) \
	-Djunit.jar=$(build-classpath junit) \
	-Dhttpunit.jar=$(build-classpath httpunit) \
	-Djtidy.jar=$(build-classpath jtidy) \
	-Dcommons-httpclient.jar=$(build-classpath commons-httpclient) \
	-Dxalanj.jar=$(build-classpath xalan-j2) \
	compile.library compile.webapps compile.javadoc compile.website dist
popd
%endif

%if %{without_coreonly}
pushd contrib/struts-faces
export CLASSPATH=$(build-classpath catalina-ant5):../../dist/lib/struts.jar
ant -Dlibdir=/usr/share/java \
	-Dcommons-beanutils.jar=$(build-classpath commons-beanutils) \
	-Dcommons-collections.jar=$(build-classpath commons-collections) \
	-Dcommons-digester.jar=$(build-classpath commons-digester) \
	-Dcommons-fileupload.jar=$(build-classpath commons-fileupload) \
	-Dcommons-lang.jar=$(build-classpath commons-lang) \
	-Dcommons-logging.jar=$(build-classpath commons-logging) \
	-Dcommons-validator.jar=$(build-classpath commons-validator) \
	-Djakarta-oro.jar=$(build-classpath oro) \
	-Djsf-api.jar=$(build-classpath myfaces/myfaces-jsf-api) \
	-Djsf-impl.jar=$(build-classpath myfaces/myfaces-impl) \
	-Djstl.jar=$(build-classpath taglibs-core) \
	-Dstandard.jar=$(build-classpath taglibs-standard) \
	-Djsp-api.jar=$(build-classpath jspapi) \
	-Dservlet.jar=$(build-classpath servletapi5) \
	-Dstruts.jar=$STRUTS_BUILD_HOME/dist/lib/struts.jar \
	-Dstruts.home=$STRUTS_BUILD_HOME \
	 dist
popd
%endif

%install

# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 dist/lib/%{name}.jar \
	$RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
%if %{without_coreonly}
install -m 644 contrib/%{name}-chain/dist/lib/%{name}-chain.jar \
	$RPM_BUILD_ROOT%{_javadir}/%{name}-chain-%{version}.jar
install -m 644 contrib/%{name}-el/dist/lib/%{name}-el.jar \
	$RPM_BUILD_ROOT%{_javadir}/%{name}-el-%{version}.jar
install -m 644 contrib/%{name}-faces/dist/lib/%{name}-faces.jar \
	$RPM_BUILD_ROOT%{_javadir}/%{name}-faces-%{version}.jar
%endif


(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} ${jar/-%{version}/}; done)
# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr target/documentation/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}
rm -rf target/documentation/api

%if %{without_coreonly}
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-chain-%{version}
cp -pr contrib/struts-chain/dist/docs/api/* \
	$RPM_BUILD_ROOT%{_javadocdir}/%{name}-chain-%{version}
ln -s %{name}-chain-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}-chain
rm -rf contrib/struts-chain/dist/docs/api

install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-el-%{version}
cp -pr contrib/struts-el/target/documentation/api/* \
	$RPM_BUILD_ROOT%{_javadocdir}/%{name}-el-%{version}
ln -s %{name}-el-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}-el
rm -rf contrib/struts-el/target/documentation/api

install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-faces-%{version}
cp -pr contrib/struts-faces/dist/docs/api/* \
	$RPM_BUILD_ROOT%{_javadocdir}/%{name}-faces-%{version}
ln -s %{name}-faces-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}-faces
rm -rf contrib/struts-faces/dist/docs/api
%endif

# data
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 dist/lib/*.tld $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 dist/lib/*.dtd $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 644 dist/lib/vali*.xml $RPM_BUILD_ROOT%{_datadir}/%{name}

%if %{without_coreonly}
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}-chain
install -m 644 contrib/%{name}-chain/dist/webapps/%{name}-chain.war \
	$RPM_BUILD_ROOT%{_datadir}/%{name}-chain
install -m 644 contrib/%{name}-chain/src/conf/chain-config.xml \
	$RPM_BUILD_ROOT%{_datadir}/%{name}-chain

install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}-el
install -m 644 contrib/%{name}-el/dist/webapps/%{name}el-exercise-taglib.war \
	$RPM_BUILD_ROOT%{_datadir}/%{name}-el
install -m 644 contrib/%{name}-el/dist/lib/*.tld \
	$RPM_BUILD_ROOT%{_datadir}/%{name}-el

install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}-faces
install -m 644 contrib/%{name}-faces/dist/webapps/%{name}-faces-example1.war \
	$RPM_BUILD_ROOT%{_datadir}/%{name}-faces
install -m 644 contrib/%{name}-faces/dist/webapps/%{name}-faces-example2.war \
	$RPM_BUILD_ROOT%{_datadir}/%{name}-faces
install -m 644 contrib/%{name}-faces/dist/lib/%{name}-faces.tld \
	$RPM_BUILD_ROOT%{_datadir}/%{name}-faces
%endif

# core docs
install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/docs
cp -p {INSTALL,LICENSE.txt,NOTICE.txt,README,STATUS.txt} $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
cp -p target/documentation/*.html $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/docs
cp -p target/documentation/*.gif $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/docs
cp -pr target/documentation/uml $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/docs
cp -pr target/documentation/userGuide $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/docs
cp -pr target/documentation/images $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/docs

%if %{without_coreonly}
# chain docs
install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-chain-%{version}/docs
#cp -pr contrib/struts-chain/dist/docs/* $RPM_BUILD_ROOT%{_docdir}/%{name}-chain-%{version}/docs
cp -p contrib/struts-chain/{LICENSE.txt,NOTICE.txt,README.txt} $RPM_BUILD_ROOT%{_docdir}/%{name}-chain-%{version}

# el docs
install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-el-%{version}/docs
cp -pr contrib/struts-el/target/documentation/* $RPM_BUILD_ROOT%{_docdir}/%{name}-el-%{version}/docs
cp -pr contrib/struts-el/{LICENSE.txt,README.txt} $RPM_BUILD_ROOT%{_docdir}/%{name}-el-%{version}

# faces docs
install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-faces-%{version}/docs
cp -pr contrib/struts-faces/dist/docs/* $RPM_BUILD_ROOT%{_docdir}/%{name}-faces-%{version}/docs
cp -pr contrib/struts-faces/{LICENSE.txt,NOTICE.txt,README.txt} $RPM_BUILD_ROOT%{_docdir}/%{name}-faces-%{version}
%endif

%if ! %{gcj_support}
# tomcat 3 webapps
install -d -m 755 $RPM_BUILD_ROOT%{tomcat3appsdir}
for webapp in %{webapps}; do
    cp -pr target/$webapp $RPM_BUILD_ROOT%{tomcat3appsdir}/%{name}-$webapp
    # tomcat3 doesn't support allowLinking, this might not work
	# XXX: move to %%post/preun
	rm -f $RPM_BUILD_ROOT%{tomcat3appsdir}/%{name}-$webapp/WEB-INF/lib/*
    (cd $RPM_BUILD_ROOT%{tomcat3appsdir}/%{name}-$webapp/WEB-INF \
    && for tld in ../../../../..%{_datadir}/%{name}/*.tld; do ln -sf $tld `basename $tld`; done)
done

# tomcat 4 webapps
install -d -m 755 $RPM_BUILD_ROOT%{tomcat4appsdir}
for webapp in %{webapps}; do
    cp -pr target/$webapp $RPM_BUILD_ROOT%{tomcat4appsdir}/%{name}-$webapp
    cat %{SOURCE2} | sed -e "s/@@@APPNAME@@@/$webapp/g;" > $RPM_BUILD_ROOT%{tomcat4appsdir}/%{name}-$webapp.xml
	# XXX: move to %%post/preun
	rm -f $RPM_BUILD_ROOT%{tomcat4appsdir}/%{name}-$webapp/WEB-INF/lib/*
    (cd $RPM_BUILD_ROOT%{tomcat4appsdir}/%{name}-$webapp/WEB-INF \
    && for tld in ../../../../../..%{_datadir}/%{name}/*.tld; do ln -sf $tld `basename $tld`; done)
done
%endif

# tomcat 5 webapps
install -d -m 755 $RPM_BUILD_ROOT%{tomcat5appsdir}
install -d -m 755 $RPM_BUILD_ROOT%{tomcat5ctxdir}
for webapp in %{webapps}; do
    cp -pr target/$webapp $RPM_BUILD_ROOT%{tomcat5appsdir}/%{name}-$webapp
    cat %{SOURCE3} | sed -e "s/@@@APPNAME@@@/$webapp/g;" > $RPM_BUILD_ROOT%{tomcat5ctxdir}/%{name}-$webapp.xml
	# XXX: move to %%post/preun
	rm -f $RPM_BUILD_ROOT%{tomcat5appsdir}/%{name}-$webapp/WEB-INF/lib/*
    (cd $RPM_BUILD_ROOT%{tomcat5appsdir}/%{name}-$webapp/WEB-INF \
    && for tld in ../../../../../..%{_datadir}/%{name}/*.tld; do ln -sf $tld `basename $tld`; done)
done

%if %{without_coreonly}
pushd $RPM_BUILD_ROOT%{tomcat5appsdir}
mkdir %{name}-chain
pushd %{name}-chain
%{jar} xf $RPM_BUILD_ROOT%{_datadir}/%{name}-chain/%{name}-chain.war
pushd WEB-INF
#for tld in ../../../../../..%{_datadir}/%{name}-chain/*.tld; do ln -sf $tld `basename $tld`; done
rm -f lib/*
popd
popd
cat %{SOURCE3} | sed -e "s/@@@APPNAME@@@/chain/g;" > $RPM_BUILD_ROOT%{tomcat5ctxdir}/%{name}-chain.xml
mkdir %{name}-el-exercise-taglib
pushd %{name}-el-exercise-taglib
%{jar} xf $RPM_BUILD_ROOT%{_datadir}/%{name}-el/%{name}el-exercise-taglib.war
pushd WEB-INF
rm -f lib/*
for tld in ../../../../../..%{_datadir}/%{name}-el/*.tld; do ln -sf $tld `basename $tld`; done
popd
popd
cat %{SOURCE3} | sed -e "s/@@@APPNAME@@@/el-exercise-taglib/g;" > $RPM_BUILD_ROOT%{tomcat5ctxdir}/%{name}-el-exercise-taglib.xml

mkdir %{name}-faces-example1
pushd %{name}-faces-example1
%{jar} xf $RPM_BUILD_ROOT%{_datadir}/%{name}-faces/%{name}-faces-example1.war
pushd WEB-INF
rm -f lib/*
for tld in ../../../../../..%{_datadir}/%{name}-faces/*.tld; do ln -sf $tld `basename $tld`; done
popd
popd
cat %{SOURCE3} | sed -e "s/@@@APPNAME@@@/faces-example1/g;" > $RPM_BUILD_ROOT%{tomcat5ctxdir}/%{name}-faces-example1.xml

mkdir %{name}-faces-example2
pushd %{name}-faces-example2
%{jar} xf $RPM_BUILD_ROOT%{_datadir}/%{name}-faces/%{name}-faces-example2.war
pushd WEB-INF
rm -f lib/*
for tld in ../../../../../..%{_datadir}/%{name}-faces/*.tld; do ln -sf $tld `basename $tld`; done
popd
popd
cat %{SOURCE3} | sed -e "s/@@@APPNAME@@@/faces-example2/g;" > $RPM_BUILD_ROOT%{tomcat5ctxdir}/%{name}-faces-example2.xml
popd
#rm $RPM_BUILD_ROOT%{_datadir}/%{name}/%{name}-chain/%{name}-chain.war
#rm $RPM_BUILD_ROOT%{_datadir}/%{name}/%{name}-el/%{name}-el-exercise-taglib.war
#rm $RPM_BUILD_ROOT%{_datadir}/%{name}/%{name}-faces/%{name}-faces-example1.war
#rm $RPM_BUILD_ROOT%{_datadir}/%{name}/%{name}-faces/%{name}-faces-example2.war
%endif

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post webapps-tomcat5
for webapp in %{webapps}; do
build-jar-repository -s -p %{tomcat5appsdir}/%{name}-$webapp/WEB-INF/lib commons-beanutils commons-digester commons-fileupload commons-validator oro
ln -s %{_javadir}/struts.jar %{tomcat5appsdir}/%{name}-$webapp/WEB-INF/lib
done

%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%preun webapps-tomcat5
for webapp in %{webapps}; do
rm -f %{tomcat5appsdir}/%{name}-$webapp/WEB-INF/lib/*
done

%if %{gcj_support}
%postun webapps-tomcat5
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif


%if %{without_coreonly}
%post webapps-tomcat3
for webapp in %{webapps}; do
build-jar-repository -s -p %{tomcat3appsdir}/%{name}-$webapp/WEB-INF/lib commons-beanutils commons-digester commons-fileupload commons-validator oro
ln -s %{_javadir}/struts.jar %{tomcat3appsdir}/%{name}-$webapp/WEB-INF/lib
done

%preun webapps-tomcat3
for webapp in %{webapps}; do
rm -f %{tomcat3appsdir}/%{name}-$webapp/WEB-INF/lib/*
done

%post webapps-tomcat4
for webapp in %{webapps}; do
build-jar-repository -s -p %{tomcat4appsdir}/%{name}-$webapp/WEB-INF/lib commons-beanutils commons-digester commons-fileupload commons-validator oro
ln -s %{_javadir}/struts.jar %{tomcat4appsdir}/%{name}-$webapp/WEB-INF/lib
done

%preun webapps-tomcat4
for webapp in %{webapps}; do
rm -f %{tomcat4appsdir}/%{name}-$webapp/WEB-INF/lib/*
done

%post chain-webapps-tomcat5
build-jar-repository -s -p %{tomcat5appsdir}/%{name}-chain/WEB-INF/lib commons-chain struts-chain

%preun chain-webapps-tomcat5
rm -f  %{tomcat5appsdir}/%{name}-chain/WEB-INF/lib/*

%post el-webapps-tomcat5
build-jar-repository -s -p %{tomcat5appsdir}/%{name}-el-exercise-taglib/WEB-INF/lib taglibs-core taglibs-standard struts-el struts

%preun el-webapps-tomcat5
rm -f  %{tomcat5appsdir}/%{name}-el-exercise-taglib/WEB-INF/lib/*

%post faces-webapps-tomcat5
build-jar-repository -s -p %{tomcat5appsdir}/%{name}-faces-example1/WEB-INF/lib commons-beanutils commons-collections commons-digester commons-fileupload commons-logging commons-validator oro struts-faces struts myfaces/myfaces-all
build-jar-repository -s -p %{tomcat5appsdir}/%{name}-faces-example2/WEB-INF/lib commons-beanutils commons-collections commons-digester commons-fileupload commons-logging commons-validator oro struts-faces struts myfaces/myfaces-all

%preun faces-webapps-tomcat5
rm -f %{tomcat5appsdir}/%{name}-faces-example1/WEB-INF/lib/*
rm -f %{tomcat5appsdir}/%{name}-faces-example2/WEB-INF/lib/*

%endif

%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%postun javadoc
if [ "$1" = "0" ]; then
  rm -f %{_javadocdir}/%{name}
fi

%if %{without_coreonly}
%post chain-javadoc
rm -f %{_javadocdir}/%{name}-chain
ln -s %{name}-chain-%{version} %{_javadocdir}/%{name}-chain

%postun chain-javadoc
if [ "$1" = "0" ]; then
  rm -f %{_javadocdir}/%{name}-chain
fi

%post el-javadoc
rm -f %{_javadocdir}/%{name}-el
ln -s %{name}-el-%{version} %{_javadocdir}/%{name}-el

%postun el-javadoc
if [ "$1" = "0" ]; then
  rm -f %{_javadocdir}/%{name}-el
fi

%post faces-javadoc
rm -f %{_javadocdir}/%{name}-faces
ln -s %{name}-faces-%{version} %{_javadocdir}/%{name}-faces

%postun faces-javadoc
if [ "$1" = "0" ]; then
  rm -f %{_javadocdir}/%{name}-faces
fi
%endif


%if %{without_coreonly}
%if %{gcj_support}
%post chain-webapps-tomcat5
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun chain-webapps-tomcat5
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif
%endif

%if %{gcj_support}
%post
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{without_coreonly}
%if %{gcj_support}
%post el
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun el
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%post chain
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun chain
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%post faces
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun faces
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif
%endif

%files
%defattr(-,root,root)
%dir %{_docdir}/%{name}-%{version}
%doc %{_docdir}/%{name}-%{version}/INSTALL
%doc %{_docdir}/%{name}-%{version}/README
%doc %{_docdir}/%{name}-%{version}/*.txt
%{_javadir}/%{name}-%{version}.jar
%{_javadir}/%{name}.jar
%{_datadir}/%{name}

%if %{gcj_support}
%dir %attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/struts-1.2.9.jar.*
%endif

%files manual
%defattr(-,root,root)
%doc %{_docdir}/%{name}-%{version}/docs

%files javadoc
%defattr(-,root,root)
%doc %{_javadocdir}/%{name}-%{version}
%ghost %{_javadocdir}/%{name}

%if ! %{gcj_support}
%files webapps-tomcat3
%defattr(-,tomcat3,tomcat3)
%dir %{tomcat3appsdir}/%{name}-blank
%{tomcat3appsdir}/%{name}-blank/*
%dir %{tomcat3appsdir}/%{name}-documentation
%{tomcat3appsdir}/%{name}-documentation/*
%dir %{tomcat3appsdir}/%{name}-example
%{tomcat3appsdir}/%{name}-example/*
%dir %{tomcat3appsdir}/%{name}-examples
%{tomcat3appsdir}/%{name}-examples/*
%dir %{tomcat3appsdir}/%{name}-tiles-documentation
%{tomcat3appsdir}/%{name}-tiles-documentation/*

%files webapps-tomcat4
%defattr(-,tomcat4,tomcat4)
%dir %{tomcat4appsdir}/%{name}-blank
%{tomcat4appsdir}/%{name}-blank/*
%dir %{tomcat4appsdir}/%{name}-documentation
%{tomcat4appsdir}/%{name}-documentation/*
%dir %{tomcat4appsdir}/%{name}-example
%{tomcat4appsdir}/%{name}-example/*
%dir %{tomcat4appsdir}/%{name}-examples
%{tomcat4appsdir}/%{name}-examples/*
%dir %{tomcat4appsdir}/%{name}-tiles-documentation
%{tomcat4appsdir}/%{name}-tiles-documentation/*
%{tomcat4appsdir}/%{name}-*.xml
%endif

%files webapps-tomcat5
%defattr(-,tomcat,tomcat)
%dir %{tomcat5appsdir}/%{name}-blank
%{tomcat5appsdir}/%{name}-blank/*
%dir %{tomcat5appsdir}/%{name}-documentation
%{tomcat5appsdir}/%{name}-documentation/*
%dir %{tomcat5appsdir}/%{name}-example
%{tomcat5appsdir}/%{name}-example/*
%dir %{tomcat5appsdir}/%{name}-examples
%{tomcat5appsdir}/%{name}-examples/*
%dir %{tomcat5appsdir}/%{name}-tiles-documentation
%{tomcat5appsdir}/%{name}-tiles-documentation/*
%{tomcat5ctxdir}/%{name}-blank.xml
%{tomcat5ctxdir}/%{name}-documentation.xml
%{tomcat5ctxdir}/%{name}-example.xml
%{tomcat5ctxdir}/%{name}-examples.xml
%{tomcat5ctxdir}/%{name}-tiles-documentation.xml

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/%{name}-example_classes*
%attr(-,root,root) %{_libdir}/gcj/%{name}/%{name}-examples_classes*
%attr(-,root,root) %{_libdir}/gcj/%{name}/%{name}-tiles-documentation_classes*
%endif

%if %{without_coreonly}
%files chain
%defattr(-,root,root)
%doc %{_docdir}/%{name}-chain-%{version}
%{_javadir}/%{name}-chain-%{version}.jar
%{_javadir}/%{name}-chain.jar
%{_datadir}/%{name}-chain

%if %{gcj_support}
%dir %attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/struts-chain-1.2.9.jar.*
%endif

%files chain-javadoc
%defattr(-,root,root)
%doc %{_javadocdir}/%{name}-chain-%{version}
%ghost %{_javadocdir}/%{name}-chain

%files chain-webapps-tomcat5
%defattr(-,tomcat,tomcat)
%dir %{tomcat5appsdir}/%{name}-chain
%{tomcat5appsdir}/%{name}-chain/*
%{tomcat5ctxdir}/%{name}-chain.xml

%if %{gcj_support}
%dir %attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/antlr.jar.*
%attr(-,root,root) %{_libdir}/gcj/%{name}/commons-beanutils.jar.*
%attr(-,root,root) %{_libdir}/gcj/%{name}/commons-digester.jar.*
%attr(-,root,root) %{_libdir}/gcj/%{name}/commons-fileupload.jar.*
%attr(-,root,root) %{_libdir}/gcj/%{name}/commons-logging.jar.*
%attr(-,root,root) %{_libdir}/gcj/%{name}/commons-validator.jar.*
%attr(-,root,root) %{_libdir}/gcj/%{name}/jakarta-oro.jar.*
%attr(-,root,root) %{_libdir}/gcj/%{name}/struts.jar.*
%endif

%files el
%defattr(-,root,root)
%doc %{_docdir}/%{name}-el-%{version}
%{_javadir}/%{name}-el-%{version}.jar
%{_javadir}/%{name}-el.jar
%{_datadir}/%{name}-el

%if %{gcj_support}
%dir %attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/struts-el-1.2.9.jar.*
%endif

%files el-javadoc
%defattr(-,root,root)
%doc %{_javadocdir}/%{name}-el-%{version}
%ghost %{_javadocdir}/%{name}-el

%files el-webapps-tomcat5
%defattr(-,tomcat,tomcat)
%dir %{tomcat5appsdir}/%{name}-el-exercise-taglib
%{tomcat5appsdir}/%{name}-el-exercise-taglib/*
%{tomcat5ctxdir}/%{name}-el-exercise-taglib.xml

%files faces
%defattr(-,root,root)
%doc %{_docdir}/%{name}-faces-%{version}
%{_javadir}/%{name}-faces-%{version}.jar
%{_javadir}/%{name}-faces.jar
%{_datadir}/%{name}-faces

%if %{gcj_support}
%dir %attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/struts-faces-1.2.9.jar.*
%endif

%files faces-javadoc
%defattr(-,root,root)
%doc %{_javadocdir}/%{name}-faces-%{version}
%ghost %{_javadocdir}/%{name}-faces

%files faces-webapps-tomcat5
%defattr(-,tomcat,tomcat)
%dir %{tomcat5appsdir}/%{name}-faces-example1
%{tomcat5appsdir}/%{name}-faces-example1/*
%{tomcat5ctxdir}/%{name}-faces-example1.xml
%dir %{tomcat5appsdir}/%{name}-faces-example2
%{tomcat5appsdir}/%{name}-faces-example2/*
%{tomcat5ctxdir}/%{name}-faces-example2.xml
%endif


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9-4jpp.11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9-4jpp.10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.9-4jpp.9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-4jpp.8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-4jpp.7m)
- [SECURITY] CVE-2008-2025
- apply a security patch (Patch8) from openSUSE 11.1 (1.2.9-162.163.2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-4jpp.6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.9-4jpp.5m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.9-4jpp.4m)
- modify %%files

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.9-4jpp.3m)
- modify Requires

* Thu Jun  7 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9-4jpp.2m)
- Revised spec for bootstrapping.

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.9-4jpp.1m)
- import from Fedora

* Tue Mar 27 2007 Deepak Bhole <dbhole@redhat.com> 1.2.9-4jpp.6
- Rewrote pre/preun/post/postun dependencies
- Change Pre[rR]eq -> Requires(pre)
- Add unowned directory to folders section. bz# 233821.

* Wed Aug 30 2006 Deepak Bhole <dbhole@redhat.com> 1.2.9-4jpp.2
- Rebuilding.

* Fri Aug 25 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.2.9-4jpp.1
- Fix requirements

* Tue Aug 22 2006 Fernando Nasser <fnasser@redhat.com> - 0:1.2.9-3jpp.1
- Merge with upstream for upgrade to 1.2.9
- Use symlinks instead of actual files in WEB-INF/lib folders
- Add conditional native compilation

* Tue Aug 22 2006 Fernando Nasser <fnasser@redhat.com> - 0:1.2.9-3jpp
- Remove duplicate macros
- Fix removal of old struts-el documentation link
- Create coreonly option

* Thu Apr 06 2006 Ralph Apel <r.apel@r-apel.de> - 0:1.2.9-2jpp
- Switch to _javadir/cactus-14/*.jar
- Thus require cactus >= 1.7.1-2jpp

* Wed Mar 29 2006 Ralph Apel <r.apel@r-apel.de> - 0:1.2.9-1jpp
- Upgrade to 1.2.9

* Thu Oct 20 2005 Ralph Apel <r.apel@r-apel.de> - 0:1.2.7-2jpp
- Add struts-chain subpackages
- Add struts-el subpackages
- Add struts-faces subpackages

* Tue Jul 26 2005 Fernando Nasser <fnasser@redhat.com> - 0:1.2.7-1jpp
- Upgrade to 1.2.7

* Fri May 27 2005 Gary Benson <gbenson@redhat.com> - 0:1.2.4-2jpp
- Build with servletapi5.
- Add build dependency on ant-nodeps.

* Tue Feb  1 2005 Joe Orton <jorton@redhat.com>
- Change webapps subpackages' tomcat dependencies to prereqs.

* Fri Nov 26 2004 Fernando Nasser <fnasser@redhat.com> - 0:1.2.4-1jpp
- Upgrade to 1.2.4

* Sat Sep 04 2004 Fernando Nasser <fnasser@redhat.com> - 0:1.1-3jpp
- Rebuilt with Ant 1.6.2

* Sat Jan 10 2004 Kaj J. Niemi <kajtzu@fi.basen.net> - 0:1.1-2jpp
- Fixed runtime requires
- Fix examples by making symlinks work in their respective (sub-)directories,
  the template comes for tomcat4 from Source #2 and for tomcat5 from Source #3

* Fri Jan  9 2004 Kaj J. Niemi <kajtzu@fi.basen.net> - 0:1.1-1jpp
- Updated to 1.1
- Use tomcat4 real webapps dir instead of -compat provided app dir
- To compile against < JDK 1.4 jdkmajorver needs to be set to 1.3

* Tue Mar 25 2003 Nicolas Mailhot <Nicolas.Mailhot (at) JPackage.org> 1.0.2-4jpp
- for jpackage-utils 1.5

* Thu Mar 28 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.2-3jpp 
- fixed incorrect files in manual package

* Wed Mar 27 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.2-2jpp 
- corrected URL
- cleaned webapps
- tagged webapps

* Sun Mar 24 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.2-1jpp 
- 1.0.2
- generic servlet support
- distinct webapps for tomcat3 and tomcat4
- no duplicate files for webapps
- correct file ownership for webapps

* Wed Jan 23 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-4jpp
- section macro
- versioned dir for javadoc
- no dependencies for manual and javadoc packages
- stricter dependencies for webapp package
- requires tomcat3
- no more requires servlet3 as tomcat3 requires it
- requires and buildrequires xalan-j2 >= 2.2.0
- adaptation to new servlet3 package
- adaptation to new xalan-j2 package
- adapation to new tomcat3 package
- uncompressed webapps

* Wed Dec 5 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-3jpp
- javadoc into javadoc package

* Tue Nov 20 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.0-2jpp
- removed packager tag
- new jpp extension
- added xalan 2.2.D13 support

* Thu Nov 1 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-1jpp
- first JPackage release
