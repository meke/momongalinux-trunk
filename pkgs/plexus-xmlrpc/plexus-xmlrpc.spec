%global momorel 8

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# If you don't want to build with maven, and use straight ant instead,
# give rpmbuild option '--without maven'
%define with_maven %{!?_without_maven:1}%{?_without_maven:0}
%define without_maven %{?_without_maven:1}%{!?_without_maven:0}

%define parent plexus
%define subname appserver

Name:           plexus-xmlrpc
Version:        1.0
Release:        0.4.%{momorel}m%{?dist}
Epoch:          0
Summary:        Plexus XML RPC Component
License:        "ASL 1.1" and MIT
Group:          Development/Libraries
URL:            http://plexus.codehaus.org/
# svn export svn://svn.plexus.codehaus.org/plexus/tags/plexus-xmlrpc-1.0-beta-4/
# tar czf plexus-xmlrpc-1.0-beta-4-src.tar.gz plexus-xmlrpc-1.0-beta-4/
Source0:        plexus-xmlrpc-1.0-beta-4-src.tar.gz
Source1:        %{name}-1.0-build.xml

Patch0:         %{name}-add-codec-dep.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  ant >= 0:1.6
BuildRequires:  ant-nodeps
%if %{with_maven}
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven-surefire-maven-plugin
BuildRequires:  maven-surefire-provider-junit
BuildRequires:  maven-doxia
BuildRequires:  maven-doxia-sitetools
BuildRequires:  plexus-maven-plugin
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api
%endif
BuildRequires:  classworlds >= 0:1.1
BuildRequires:  jakarta-commons-codec >= 1.4-1m
BuildRequires:  plexus-container-default
BuildRequires:  plexus-utils
BuildRequires:  xmlrpc

Requires:  jakarta-commons-codec >= 1.4-1m
Requires:  classworlds >= 0:1.1
Requires:  plexus-container-default
Requires:  plexus-utils
Requires:  xmlrpc

Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2

%description
The Plexus project seeks to create end-to-end developer tools for
writing applications. At the core is the container, which can be
embedded or for a full scale application server. There are many
reusable components for hibernate, form processing, jndi, i18n,
velocity, etc. Plexus also includes an application server which
is like a J2EE application server, without all the baggage.


%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.


%prep
%setup -q -n plexus-xmlrpc-1.0-beta-4
cp %{SOURCE1} build.xml

%patch0 -b .sav

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

%if %{with_maven}
    mvn-jpp \
        -e \
                -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        install javadoc:javadoc
%else
mkdir -p target/lib
build-jar-repository -s -p target/lib \
classworlds \
commons-codec \
plexus/container-default \
plexus/utils \
xmlrpc \

ant jar javadoc
%endif

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/plexus
install -pm 644 target/plexus-xmlrpc-%{version}-beta-4.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/xmlrpc-%{version}.jar
%add_to_maven_depmap org.codehaus.plexus plexus-xmlrpc 1.0-beta-4 JPP/plexus xmlrpc
(cd $RPM_BUILD_ROOT%{_javadir}/plexus && for jar in *-%{version}*; \
  do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

#poms
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -pm 644 pom.xml $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.plexus-xmlrpc.pom

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr target/site/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_javadir}/plexus/*
%{_datadir}/maven2/poms/*
%doc LICENSE.txt
%{_mavendepmapfragdir}/plexus-xmlrpc


%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.4.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.4.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.4.6m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.4.5m)
- BuildRequires: maven-surefire-maven-plugin
- BuildRequires: maven-surefire-provider-junit
- BuildRequires: maven-doxia
- BuildRequires: maven-doxia-sitetools

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.4.4m)
- drop gcj support

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.4.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.4.2m)
- remove duplicate directories

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.4.1m)
- import from Fedora 11

* Tue Mar 24 2009 Deepak Bhole <dbhole@redhat.com> 1.0-0.3.b4.2.14
- Build with maven
- Add tomcat deps

* Mon Mar 23 2009 Deepak Bhole <dbhole@redhat.com> 1.0-0.3.b4.2.13
- Build without maven

* Mon Mar 23 2009 Deepak Bhole <dbhole@redhat.com> - 0:1.0-0.3.b4.2.12
- Add plexus-maven-plugin BR
- Build on ppc64

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.3.b4.2.11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep 24 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.2.b4.2.11
- Update xmlrpc-add-codec-dep.patch to remove fuzz... for real this time

* Wed Sep 24 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.2.b4.2.10
- Update patch0 to remove fuzz.

* Wed Jul  9 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-0.2.b4.2.9
- drop repotag

* Thu May 29 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-0.2.b4.2jpp.8
- fix license tag

* Thu Feb 28 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.2.b4.2jpp.7
- Rebuild.

* Fri Sep 21 2007 Deepak Bhole <dbhole@redhat.com> 1.0-0.1.b4.3jpp.6
- ExcludeArch ppc64

* Tue Mar 20 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-0.1.b4.3jpp.5
- Fixed BRs

* Tue Mar 20 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-0.1.b4.3jpp.4
- Build with maven

* Tue Mar 13 2007 Deepak Bhole <dbhole@redhat.com> 1.0-0.1.b4.3jpp.3
- rebuild

* Tue Mar 13 2007 Deepak Bhole <dbhole@redhat.com> 1.0-0.1.b4.3jpp.2
- Fixing typo in a Requires.

* Mon Feb 19 2007 Tania Bento <tbento@redhat.com> 0:1.0-0.1.b4.3jpp.1
- Fixed %%Release.
- Fixed %%BuildRoot.
- Marked LICENSE.txt as %%doc.
- Removed %%Vendor.
- Removed %%Distribution.
- Removed %%post and %%postun for javadoc.
- Added gcj support option.

* Tue Oct 17 2006 Deepak Bhole <dbhole@redhat.com> 1.0-0.b4.3jpp
- Update for maven2 9jpp.

* Wed Jun 21 2006 Fernando Nasser <fnasser@redhat.com> - 0:1.0-0.b4.2jpp
- Removed maven1 build support, and added maven2 support.
- First JPP 1.7 build

* Mon Nov 07 2005 Ralph Apel <r.apel at r-apel.de> - 0:1.0-0.b4.1jpp
- First JPackage build

