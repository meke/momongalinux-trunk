# Generated from json_pure-1.6.6.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname json_pure

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: JSON Implementation for Ruby
Name: rubygem-%{gemname}
Version: 1.6.6
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://flori.github.com/json
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
This is a JSON implementation in pure Ruby.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.6-1m)
- update 1.6.6

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update 1.6.4

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-1m)
- update 1.6.1

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.4-1m)
- update 1.5.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.6-1m)
- update 1.4.6

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.5-1m)
- update 1.4.5

* Mon Aug  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.3-1m)
- update 1.4.3

* Fri Jan  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0

* Wed Nov 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-2m)
- exclude conflicting files with rubygem-json

* Mon Nov 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-1m)
- Initial package for Momonga Linux
