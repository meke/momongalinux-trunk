%global momorel 1

%global xfce4ver 4.10.0
%global major 1.8

Summary:        Screenshot utility for the Xfce desktop
Name:           xfce4-screenshooter
Version:        1.8.1
Release:        %{momorel}m%{?dist}

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/applications/%{name}
Source0:        http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gettext intltool desktop-file-utils
BuildRequires:  perl-XML-Parser
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}

%description
The Xfce Screenshooter utility allows you to capture the entire screen, the 
active window or a selected region. You can set the delay that elapses before 
the screenshot is taken and the action that will be done with the screenshot: 
save it to a PNG file, copy it to the clipboard, or open it using another 
application.

%package        plugin
Summary:        Screenshot utility for the Xfce panel
Group:          User Interface/Desktops
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:       %{name} = %{version}-%{release}
Requires:       xfce4-panel >= %{xfce4ver}

%description    plugin
The Xfce Screenshooter plugin allows you to take screenshots from the Xfce 
panel.


%prep
%setup -q
# KDE and GNOME have their own screenshot utilis
echo "NotShowIn=KDE;GNOME;" >> src/xfce4-screenshooter.desktop.in.in

%build
%configure --disable-static LIBS="-lX11 -lm -lXext"
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
%find_lang %{name}
sed -e "s/NotShowIn.*//" -i ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
desktop-file-install --vendor ""                                \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
        --delete-original                                       \
        --add-only-show-in=XFCE                                 \
        ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop

# remove la file
find %{buildroot} -name '*.la' -exec rm -f {} ';'

# make sure debuginfo is generated properly
chmod -c +x %{buildroot}%{_libdir}/xfce4/panel/plugins/*.so

%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/xfce4-screenshooter
%{_datadir}/applications/%{name}.desktop
%{_datadir}/doc/%{name}/html/*
%{_datadir}/icons/hicolor/scalable/apps/applets-screenshooter.svg
%{_datadir}/xfce4/doc/*/images/%{name}-*.png
%{_datadir}/xfce4/doc/*/*.html
%{_mandir}/man1/*.*

%exclude %{_datadir}/icons/hicolor/*/apps/applets-screenshooter.png
# Mo: into list excludes by conflicts with gnome-icon-theme

%files plugin
%defattr(-,root,root,-)
%{_libdir}/xfce4/panel/plugins/libscreenshooterplugin.so*
%{_datadir}/xfce4/panel/plugins/*.desktop


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.1-1m)
- update
- build against xfce4-4.10.0

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.9-5m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.9-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.9-1m)
- rebuild against xfcd4-4.6.2

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.1-4m)
- build fix with desktop-file-utils-0.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.1-2m)
- add only show in XFCE

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1-1m)
- import from Fedora to Momonga

* Wed Feb 25 2009 Christoph Wickert <cwickert@fedoraproject.org> - 1.5.1-1
- Update to 1.5.1.
- Built for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jan 18 2009 Christoph Wickert <cwickert@fedoraproject.org> - 1.5.0-1
- Update to 1.5.0 on Xfce 4.5.93.

* Fri Jan 02 2009 Christoph Wickert <cwickert@fedoraproject.org> - 1.4.90.0-1
- Update to 1.4.90.0
- Split package into standalone app and panel plugin

* Thu Nov 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.4.0-1
- Update to 1.4.0

* Wed Aug 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.2-1
- Update to 1.3.2

* Wed Aug 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.2-1
- Update to 1.3.2

* Fri Jul 18 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.1-1
- Update to 1.3.1

* Wed Jul 16 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.0-1
- Update to 1.3.0

* Thu Jul 03 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.2.0-1
- Update to 1.2.0
- Include new xfce4-screenshooter manpage

* Sat Jun 22 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.1.0-1
- Update to 1.1.0
- BR gettext

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.0-7
- Autorebuild for GCC 4.3

* Sat Aug 25 2007 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-6
- Change license tag to GPLv2+

* Sat Apr 28 2007 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-5
- Rebuild for Xfce 4.4.1

* Sun Jan 28 2007 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-4
- Rebuild for XFCE 4.4.

* Thu Oct 05 2006 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-3
- Bump release for devel checkin.

* Wed Sep 13 2006 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-2
- Rebuild for XFCE 4.3.99.1.
- BR perl(XML::Parser).

* Mon Sep 04 2006 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-1
- Update to 1.0.0 on XFCE 4.3.90.2.

* Mon Sep 04 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.8-4
- Mass rebuild for Fedora Core 6.

* Tue Apr 11 2006 Christoph Wickert <fedora.wickert@arcor.de> - 0.0.8-3
- Require xfce4-panel.

* Thu Feb 16 2006 Christoph Wickert <fedora.wickert@arcor.de> - 0.0.8-2
- Rebuild for Fedora Extras 5.

* Sat Jan 21 2006 Christoph Wickert <fedora.wickert@arcor.de> - 0.0.8-1
- Initial Fedora Extras version.
