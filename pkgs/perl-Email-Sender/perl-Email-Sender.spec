%global         momorel 2

Name:           perl-Email-Sender
Version:        1.300011
Release:        %{momorel}m%{?dist}
Summary:        Email::Sender Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Email-Sender/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/Email-Sender-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Capture-Tiny >= 0.08
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-Email-Abstract >= 3
BuildRequires:  perl-Email-Address
BuildRequires:  perl-Email-Simple >= 1.998
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Path
BuildRequires:  perl-File-Temp
BuildRequires:  perl-IO
BuildRequires:  perl-JSON
BuildRequires:  perl-libnet
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Moose >= 0.90
BuildRequires:  perl-Net-SMTP-SSL
BuildRequires:  perl-PathTools
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Sub-Exporter
BuildRequires:  perl-Sub-Override
BuildRequires:  perl-Sys-Syslog
BuildRequires:  perl-Test-MinimumVersion
BuildRequires:  perl-Test-MockObject
BuildRequires:  perl-Test-Simple >= 0.96
BuildRequires:  perl-Throwable >= 0.100090
BuildRequires:  perl-Try-Tiny
Requires:       perl-Capture-Tiny >= 0.08
Requires:       perl-Email-Abstract >= 3
Requires:       perl-Email-Address
Requires:       perl-Email-Simple >= 1.998
Requires:       perl-File-Path
Requires:       perl-File-Temp
Requires:       perl-IO
Requires:       perl-libnet
Requires:       perl-List-MoreUtils
Requires:       perl-Moose >= 0.90
Requires:       perl-Net-SMTP-SSL
Requires:       perl-PathTools
Requires:       perl-Scalar-Util
Requires:       perl-Sub-Exporter
Requires:       perl-Sys-Syslog
Requires:       perl-Throwable >= 0.100090
Requires:       perl-Try-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Email::Sender Perl module

%prep
%setup -q -n Email-Sender-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README util
%{perl_vendorlib}/Email/Sender
%{perl_vendorlib}/Email/Sender.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300011-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300011-1m)
- update to 1.300011

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300010-1m)
- update to 1.300010
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300006-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300006-2m)
- rebuild against perl-5.18.0

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300006-1m)
- update to 1.300006

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300005-2m)
- rebuild against perl-5.16.3

* Fri Feb 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300005-1m)
- update to 1.300005

* Sun Feb 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300004-1m)
- update to 1.300004

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300003-1m)
- update to 1.300003

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.120002-2m)
- rebuild against perl-5.16.2

* Wed Sep 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.120002-1m)
- update to 0.120002

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.120001-2m)
- rebuild against perl-5.16.1

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.120001-1m)
- update to 0.120001

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110005-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110005-1m)
- update to 0.110005

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110001-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110001-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110001-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.110001-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110001-1m)
- update to 0.110001

* Fri Mar 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.110000-1m)
- update to 0.110000

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.102370-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.102370-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.102370-1m)
- update to 0.102370

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.101760-2m)
- full rebuild for mo7 release

* Sat Jun 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.101760-1m)
- update to 0.101760

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.100460-1m)
- update to 0.100460

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.100450-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.100450-2m)
- rebuild against perl-5.12.0

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.100450-1m)
- update to 0.100450

* Sun Jan 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.100110-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
