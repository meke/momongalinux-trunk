%global momorel 1

Summary:        A nl80211 based wireless configuration tool
Name:           iw
Version:        3.15
Release:        %{momorel}m%{?dist}
License:        BSD
Group:          System Environment/Base
URL:            http://www.linuxwireless.org/en/users/Documentation/iw
Source0:        https://www.kernel.org/pub/software/network/%{name}/%{name}-%{version}.tar.xz
NoSource:       0
Patch0:         iw-3.3-libnl3.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kernel-devel >= 2.6.24
BuildRequires:  libnl-devel >= 1.0
BuildRequires:  pkgconfig      

%description
iw is a new nl80211 based CLI configuration utility for wireless devices.
Currently you can only use this utility to configure devices which
use a mac80211 driver as these are the new drivers being written - 
only because most new wireless devices being sold are now SoftMAC.

%prep
%setup -q

%build
export CFLAGS="$RPM_OPT_FLAGS"
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PREFIX='' MANDIR=%{_mandir}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING
/sbin/%{name}
%{_datadir}/man/man8/%{name}.8*

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.14-1m)
- update 3.14

* Thu Apr  5 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (3.3-2m)
- compile option CFLAGS -> OPTFLAGS
- add patch0 (iw-3.3-libnl3.patch)

* Tue Feb  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3-1m)
- update 3.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.20-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.20-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.20-1m)
- update to 0.9.20

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.14-1m)
- initial package for Momonga Linux

* Wed May 13 2009 Adel Gadllah <adel.gadllah@gmail.com> 0.9.14-1
- Update to 0.9.14

* Tue May  2 2009 John W. Linville <linville@redhat.com> 0.9.13-1
- Update to 0.9.13

* Mon Apr 15 2009 John W. Linville <linville@redhat.com> 0.9.12-1
- Update to 0.9.12

* Mon Apr  6 2009 John W. Linville <linville@redhat.com> 0.9.11-1
- Update to 0.9.11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 10 2009 Adel Gadllah <adel.gadllah@gmail.com> 0.9.7-1
- Update to 0.9.7

* Sun Oct 26 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.9.6-1
- Update to 0.9.6

* Sun Sep 28 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.9.5-3
- Use offical tarball

* Sun Sep 28 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.9.5-2
- Fix BuildRequires

* Sun Sep 28 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.9.5-1
- Update to 0.9.5

* Tue Jul 22 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.0-0.3.20080703gitf6fc7dc
- Add commitid to version
- Use versioned buildrequires for kernel-devel

* Thu Jul 3 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.0-0.2.20080703git
- Add tarball instructions
- Fix install
- Fix changelog

* Thu Jul 3 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.0-0.1.20080703git
- Initial build
