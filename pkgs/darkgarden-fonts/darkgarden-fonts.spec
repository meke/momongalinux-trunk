%global momorel 6

%global fontname darkgarden
%global fontconf 69-darkgarden.conf

Name:           %{fontname}-fonts
Version:	1.1
Release:        %{momorel}m%{?dist}
Summary:	Dark Garden is a decorative outline font of unusual shape

Group:          User Interface/X
License:        GPLv2
URL:            http://darkgarden.sourceforge.net/

Source0:        http://darkgarden.sourceforge.net/darkgarden-1.1.src.zip
Source1:        %{name}-fontconfig.conf

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
BuildRequires: fontforge >= 20061025-1
Requires:      fontpackages-filesystem

%description
Dark Garden is a decorative outline font of unusual shape.
The typeface is based on author's original hand drawings.
The letterform is complex, with all characters decorated
with spikes resembling thorns or flames, character spacing
is very dense. Such a theme makes it a great font for titles,
banners, logos etc. Due to the font's complicated form,
long text passages are not very legible, but short paragraphs
such as titles or lyrics / poetry look very well.

%prep
%setup -q -n darkgarden-1.1 %{SOURCE0}

%build
fontforge -lang=ff -c 'Open($1); Generate($2)' DarkGarden.sfd DarkGarden.ttf

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}

install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -fr %{buildroot}

%_font_pkg -f %{fontconf} *.ttf

%doc COPYING.txt
%doc README.txt
%doc COPYING-GPL.txt


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- import from Fedora

* Sun Mar 15 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net> - 1.1-9
- Make sure F11 font packages have been built with F11 fontforge

* Mon Mar 2 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 1.1-8
- Fixed fontconfig problems caused by some f***er turning python bindings on.
- Deleted erroneous modifications made by rel-eng.

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 17 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 1.1-6
- Fixed errors in previous modifications.

* Sun Jan 11 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 1.1-5
- Modified spec file to comply with recent policy changes.

* Sat Sep 06 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 1.1-4
- rebuild with new release of fontforge.

* Sat Aug 02 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 1.1-2
- initial import
- bumped release number

* Mon Jul 21 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com>
- Modified spec file to more closely follow fedora's policy
- Checked spec and srpm against rpmlint and mock

* Fri Jul 11 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com>
- Created DarkGarden font package

