%global momorel 4

# Use rpmbuild --without gcj to disable gcj bits
%define with_gcj %{!?_without_gcj:1}%{?_without_gcj:0}

Name:           swing-layout
Version:        1.0.4
Release:        %{momorel}m%{?dist}
Summary:        Natural layout for Swing panels

Group:          Development/Libraries
License:        LGPLv2
URL:            https://swing-layout.dev.java.net/
# Need to register to download, from url above
Source0:        %{name}-%{version}-src.zip
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  jpackage-utils >= 1.6
BuildRequires:  java-devel >= 1.3
BuildRequires:  ant
BuildRequires:  dos2unix
Requires:       java >= 1.3

%if %{with_gcj}
BuildRequires:    java-gcj-compat-devel >= 1.0.31
Requires(post):   java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
%else
BuildArch:      noarch
%endif


%description
Extensions to Swing to create professional cross platform layout.


%package javadoc
Summary:        Javadoc documentation for Swing Layout
Group:          Documentation

%description javadoc
Documentation for Swing Layout code.


%prep
%setup -q
dos2unix releaseNotes.txt


%build
%{ant} jar javadoc dist


%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_javadir} \
        $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
install -m 644 dist/%{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar
cp -pr dist/javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

%if %{with_gcj}
%{_bindir}/aot-compile-rpm
%endif


%clean
rm -rf $RPM_BUILD_ROOT


%post
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif


%postun
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif


%files
%defattr(-,root,root)
%{_javadir}/%{name}*.jar
%if %{with_gcj}
%{_libdir}/gcj/%{name}
%endif
%doc releaseNotes.txt


%files javadoc
%defattr(-,root,root)
%{_javadocdir}/%{name}-%{version}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- import from Fedora 11 for netbeans

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Apr 03 2008 Lubomir Kundrak <lkundrak@redhat.com> - 1.0.3-2
- gcj bits
- no insane javadoc links

* Tue Feb 19 2008 Lubomir Kundrak <lkundrak@redhat.com> - 1.0.3-1
- 1.0.3
- Major specfile cleanup for Fedora

* Tue Feb 19 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0:1.0-1jpp
- Shamelessly stolen this from JPackage 1.6 without proper ChangeLog entry

* Mon Nov 12 2005 Jaroslav Tulach <jtulach@netbeans.org> - 0:0.9-1jpp
- First packaged release.
