%global momorel 1

%global xfce4ver 4.10.0
%global major 0.6

Name:		xfce4-mount-plugin
Version:	0.6.7
Release:	%{momorel}m%{?dist}
Summary:	Mount/unmount utility for the Xfce panel

Group:		User Interface/Desktops
License:	GPL
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	gettext, perl-XML-Parser
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	libxml2-devel
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
Mount and unmount filesystems from the Xfce panel.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

rm -f %{buildroot}/%{_libdir}/xfce4/panel/plugins/libmount.la

%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%{_libdir}/xfce4/panel/plugins/libmount.so
%{_datadir}/xfce4/panel/plugins/*.desktop
%{_datadir}/locale/*
%{_datadir}/icons/hicolor/scalable/apps/xfce-mount.*
%{_datadir}/icons/hicolor/scalable/apps/xfce-mount.svg


%changelog
* Thu May 15 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.7-1m)
- update to 0.6.7

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.4-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.5-8m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5-7m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.5-4m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.5-3m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.5-1m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-2m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-1m)
- update to 0.5.4
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-1m)
- import to Momonga from fc

* Sun Jan 28 2007 Christoph Wickert <fedora christoph-wickert de> - 0.4.8-2
- Rebuild for Xfce 4.4.
- Update gtk-icon-cache scriptlets.

* Thu Oct 05 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.8-1
- Update to 0.4.8.

* Wed Sep 13 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.5-2
- Rebuild for XFCE 4.3.99.1.
- BR perl(XML::Parser), drop BR intltool.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.5-1
- Update to 0.4.5 on XFCE 4.3.90.2.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 0.3.3-5
- Mass rebuild for Fedora Core 6.

* Sat Jun 03 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.3-4
- BuildRequire intltool (#193444) and gettext.

* Tue Apr 11 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.3-3
- Require xfce4-panel.

* Thu Feb 16 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.3-2
- Rebuild for Fedora Extras 5.

* Sat Jan 21 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.3-1
- Update to 0.3.3.

* Thu Dec 01 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.2-3
- Add libxfcegui4-devel BuildReqs.
- Fix %%defattr.

* Mon Nov 14 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.2-2
- Initial Fedora Extras version.
- Rebuild for XFCE 4.2.3.
- disable-static instead of removing .a files.

* Fri Sep 23 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.2-1.fc4.cw
- Updated to version 0.3.2.
- Add libxml2 BuildReqs.

* Sat Jul 09 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3-1.fc4.cw
- Updated to version 0.3.
- Rebuild for Core 4.

* Thu Apr 14 2005 Christoph Wickert <fedora wickert at arcor de> - 0.1-1.fc3.cw
- Initial RPM release.
