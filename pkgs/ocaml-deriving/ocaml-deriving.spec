%global momorel 10
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-deriving
Version:        0.1.1a
Release:        %{momorel}m%{?dist}
Summary:        Extension to OCaml for deriving functions from types

Group:          Development/Libraries
License:        MIT
URL:            http://code.google.com/p/deriving/
Source0:        http://deriving.googlecode.com/files/deriving-%{version}.tar.gz
NoSource:       0
Patch0:         ocaml-deriving-no-link-libs.patch
Patch1:         0001-fixes-for-3.12.0.patch
Patch2:         deriving-0.1.1-fix-deriving-path.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel


%description
Extension to OCaml for deriving functions from type declarations.
Includes derivers for pretty-printing, type-safe marshalling with
structure-sharing, dynamic typing, equality, and more.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n deriving-0.1.1
%patch0
%patch1 -p1 -b .fix
%patch2 -p1 -b .deriving


%build
make

cat >META <<EOF
name="deriving"
version="%{version}"
requires="%{camlp4}"
description="%{description}"
# need a syntax here XXX
EOF


%check
#cd tests
#make
#./tests


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
mkdir -p $RPM_BUILD_ROOT%{_bindir}

ocamlfind install deriving \
  META lib/*.cma lib/*.cmxa lib/*.a lib/*.mli lib/*.cmi lib/*.cmx
install -m 0755 syntax/deriving $RPM_BUILD_ROOT%{_bindir}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/ocaml/deriving
%if %opt
%exclude %{_libdir}/ocaml/deriving/*.a
%exclude %{_libdir}/ocaml/deriving/*.cmxa
%exclude %{_libdir}/ocaml/deriving/*.cmx
%endif
%exclude %{_libdir}/ocaml/deriving/*.mli
%{_bindir}/*


%files devel
%defattr(-,root,root,-)
%doc COPYING README CHANGES
%if %opt
%{_libdir}/ocaml/deriving/*.a
%{_libdir}/ocaml/deriving/*.cmxa
%{_libdir}/ocaml/deriving/*.cmx
%endif
%{_libdir}/ocaml/deriving/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1a-10m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1a-9m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1a-8m)
- no check (fix me)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1a-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1a-6m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1a-5m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1a-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1a-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1a-2m)
- rebuild against ocaml-3.11.0
-- apply Patch1,2

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1a-1m)
- import from Fedora

* Mon May 10 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1.1a-3
- Fix the License tag (MIT not BSD).

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1.1a-2
- Remove ExcludeArch ppc64.

* Fri Feb 29 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1.1a-1
- New upstream release which includes the license file.
- Patch OCamlMakefile so it doesn't statically link system libs with
  the library.

* Thu Feb 28 2008 Richard W.M. Jones <rjones@redhat.com> - 0.1.1-1
- Initial RPM release.
- Lacks a license file so we cannot release it for review yet.
