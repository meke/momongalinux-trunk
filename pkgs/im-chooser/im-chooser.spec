%global momorel 3
%if 0%{!?_with_xfce:1} && 0%{!?_without_xfce:1}
%if 0%{?rhel}
%global _with_xfce 0
%else
%global _with_xfce 1
%endif
%endif

Name:		im-chooser
Version:	1.6.0
Release: %{momorel}m%{?dist}
License:	GPLv2+
URL:		http://fedorahosted.org/im-chooser/
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%{?_with_gtk2:BuildRequires:	gtk2-devel}
%{!?_with_gtk2:BuildRequires:	gtk3-devel}
BuildRequires:	libSM-devel imsettings-devel >= 1.3.0
%if 0%{?_with_xfce}
BuildRequires:	libxfce4util-devel
%endif
BuildRequires:	desktop-file-utils intltool gettext

Source0:	http://fedorahosted.org/releases/i/m/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0

Summary:	Desktop Input Method configuration tool
Group:		Applications/System
Obsoletes:	im-chooser-gnome3 < 1.4.2-2
Provides:	im-chooser-gnome3 = %{version}-%{release}
Requires:	%{name}-common = %{version}-%{release}

%description
im-chooser is a GUI configuration tool to choose the Input Method
to be used or disable Input Method usage on the desktop.

%package	common
Summary:	Common files for im-chooser subpackages
Group:		Applications/System
Requires:	imsettings >= 1.3.0
Obsoletes:	im-chooser < 1.5.0.1

%description	common
im-chooser is a GUI configuration tool to choose the Input Method
to be used or disable Input Method usage on the desktop.

This package contains the common libraries/files to be used in
im-chooser subpackages.

# %%if 0%%{!?_with_gtk2:1}
%if 0
%package	gnome3
Summary:	control-center module for im-chooser on GNOME3
Group:		Applications/System
Requires:	%{name} = %{version}-%{release}

%description	gnome3
im-chooser is a GUI configuration tool to choose the Input Method
to be used or disable Input Method usage on the desktop.

This package contains the control-center panel module on GNOME3.
%endif

%if 0%{?_with_xfce}
%package	xfce
Summary:	XFCE settings panel for im-chooser
Group:		Applications/System
Requires:	%{name}-common = %{version}-%{release}
Obsoletes:	im-chooser < 1.5.0.1

%description	xfce
im-chooser is a GUI configuration tool to choose the Input Method
to be used or disable Input Method usage on the desktop.

This package contains the XFCE settings panel for im-chooser.
%endif


%prep
%setup -q

%build
%configure
%make 

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

desktop-file-install	--vendor=				\
	--add-category=X-GNOME-PersonalSettings			\
	--delete-original					\
	--dir=%{buildroot}%{_datadir}/applications		\
	%{buildroot}%{_datadir}/applications/im-chooser.desktop
%if 0%{?_with_xfce}
desktop-file-validate %{buildroot}%{_datadir}/applications/xfce4-im-chooser.desktop
%endif
#%%{!?_with_gtk2:desktop-file-validate %{buildroot}%%{_datadir}/applications/im-chooser-panel.desktop}

rm -rf %{buildroot}%{_libdir}/libimchooseui.{so,la,a}
#%%{!?_with_gtk2:rm -rf %{buildroot}%%{_libdir}/control-center-1/panels/libim-chooser.{a,la}}

# disable panel so far
rm -rf %{buildroot}%{_libdir}/control-center-1/panels/libim-chooser.so
rm -rf %{buildroot}%{_datadir}/applications/im-chooser-panel.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}


%post	common
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun	common
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans	common
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files
%defattr (-, root, root)
%{_bindir}/im-chooser
%{_datadir}/applications/im-chooser.desktop

%files	common -f %{name}.lang
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/libimchooseui.so.*
%{_datadir}/icons/hicolor/*/apps/im-chooser.png
%dir %{_datadir}/imchooseui
%{_datadir}/imchooseui/imchoose.ui

# %%if 0%%{!?_with_gtk2:1}
%if 0
%files	gnome3
%defattr (-, root, root, -)
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/control-center-1/panels/libim-chooser.so
%{_datadir}/applications/im-chooser-panel.desktop
%endif

%if 0%{?_with_xfce}
%files	xfce
%defattr (-, root, root, -)
%{_bindir}/xfce4-im-chooser
%{_datadir}/applications/xfce4-im-chooser.desktop
%endif

%changelog
* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-3m)
- remove fedora from vendor (desktop-file-install)

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.0-2m)
- rebuild against xfce4-4.10.0

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-1m)
- reimport from fedora

* Fri Jun 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2.2-1m)
- reimport from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-2m)
- full rebuild for mo7 release

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-1m)
- import from Fedora 13

* Tue May  4 2010 Jens Petersen <petersen@redhat.com> - 1.2.7-2
- add new gnome-icon-theme style icons by Lapo Calamandrei and Jakub Steiner
  (mizmo, #587712)
- add scriptlets for icon cache

* Mon Sep 14 2009 Akira TAGOH <tagoh@redhat.com> - 1.2.7-1
- New upstream release.
  - translation updates only.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.6-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon May 25 2009 Akira TAGOH <tagoh@redhat.com> - 1.2.6-3
- Disable the status icon check box.

* Thu Feb 26 2009 Akira TAGOH <tagoh@redhat.com> - 1.2.6-2
- Fix a typo in xfce4-im-chooser.desktop. (#487275)

* Mon Feb 23 2009 Akira TAGOH <tagoh@redhat.com> - 1.2.6-1
- New upstream release.

* Wed Oct 22 2008 Akira TAGOH <tagoh@redhat.com> - 1.2.5-1
- New upstream release.

* Tue Oct 14 2008 Akira TAGOH <tagoh@redhat.com> - 1.2.4-1
- New upstream release.

* Wed Sep 17 2008 Akira TAGOH <tagoh@redhat.com> - 1.2.3-1
- New upstream release.

* Fri Aug 29 2008 Akira TAGOH <tagoh@redhat.com> - 1.2.2-1
- New upstream release.

* Tue Jul 29 2008 Akira TAGOH <tagoh@redhat.com> - 1.2.1-1
- New upstream release.
  - Display IM icon in the list. (#454371)

* Tue Jul  8 2008 Akira TAGOH <tagoh@redhat.com> - 1.2.0-1
- New upstream release.

* Fri Jun 27 2008 Akira TAGOH <tagoh@redhat.com> - 1.1.1-1
- New upstream release.
  - Fix a segfault when no Input Method installed. (#452997)

* Thu Jun 12 2008 Akira TAGOH <tagoh@redhat.com> - 1.1.0-1
- New upstream release.

* Mon May 26 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.6-5
- Fix a typo in the package group of imsettings-xfce. (#448037)

* Wed May 14 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.6-4
- im-chooser-fix-window-border.patch: Display the progress window with
  the certain window border. (#444818)
- imsettings-ignore-error-on-check-running.patch: Fix a crash issue when
  the pidfile doesn't exist. (#445129)

* Tue Apr 29 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.6-3
- im-chooser-0.99.6-sanity-check-on-dbus-conn.patch: Do not abort even if
  getting the bus is failed. (#444494)
- im-chooser-0.99.6-validate-pid.patch: Validate the pid. (#443765)

* Wed Apr 23 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.6-2
- im-chooser-0.99.6-check-if-im-is-running.patch: Do not turn on the check box
  if IM isn't really running. (#443765)
- im-chooser-0.99.6-correct-build-order.patch: Apply to correct the build order.

* Tue Apr  8 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.6-1
- New upstream release.
  - translation updates.
- Remove unnecessary patches:
  - im-chooser-0.99.5-no-xinputrc-update.patch
  - im-chooser-0.99.5-no-crash-on-no-im.patch

* Mon Apr  7 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.5-3
- im-chooser-0.99.5-no-crash-on-no-im.patch: Fix a crash when no IM
  available. (#440519)
- Invoke ReloadConfig to apply changes on DBus services in %%post and %%postun.

* Fri Mar 28 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.5-2
- im-chooser-0.99.5-no-xinputrc-update.patch: real fix for #437732
- ensure invoking xinput.sh after the session bus is established. (#436284)

* Wed Mar 19 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.5-1
- New upstream release.
  - Fix an issue always create .xinputrc at the startup time. (#437732)
  - Add Xfce support.

* Tue Mar 11 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.4-1
- New upstream release.
  - Compress im-chooser.png icon. (#330441)

* Thu Feb 21 2008 Akira TAGOH <tagoh@redhat.com>
- Run ldconfig on scriptlet of imsettings-libs.

* Wed Feb 20 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.3-1
- New upstream release.
  - Fix taking too much CPU power. (#433575)
  - Fix not parsing the multiple command line options in xinput
    script. (#433578)

* Tue Feb 19 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.2-1
- New upstream release.
  - Fix not working the user own .xinputrc properly.

* Fri Feb  8 2008 Akira TAGOH <tagoh@redhat.com> - 0.99.1-1
- New upstream release.
  - Fix some memory leaks and clean up the code. (#431167)
  - Fix the handling of the user own .xinputrc. (#431291)

* Fri Feb  1 2008 Akira TAGOH <tagoh@redhat.com> - 0.99-1
- New upstream release.
  - IMSettings is now enabled. you don't need to restart your desktop after
    changing IM for GTK+ applications. but still need to do for others so far.

* Thu Dec 27 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.5-1
- New upstream release.
  - Rename sr@Latn to sr@latin. (#426540)

* Fri Nov 16 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.4-1
- New upstream release.
  - Improve .desktop file for GNOME HIG compliant (#330431)
  - Improve English label on GUI (#302491)
- Remove the dead link. (#330391)
- Improve a package description. (#330421)

* Mon Oct 15 2007 Akira TAGOH <tagoh@redhat.com>
- Remove the obsolete Norwegian (no) translation. (#332131)

* Thu Oct 11 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.3-1
- New upstream release.
  - Fix an issue that looks like IM can't be disabled on im-chooser. (#324231)

* Tue Oct  2 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.2-3
- Revert the previous change.

* Fri Sep 21 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.2-2
- Bring up IM by default again, except the session is on Live CD. (#250226)

* Tue Sep 18 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.2-1
- New upstream release.
  - Fix to allow users disabling IM.

* Fri Sep 14 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.1-2
- Add README into the package.

* Mon Sep 10 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.1-1
- New upstream release.

* Thu Sep  6 2007 Akira TAGOH <tagoh@redhat.com> - 0.5.0-1
- New upstream release.

* Wed Aug  8 2007 Akira TAGOH <tagoh@redhat.com>
- Update License tag.

* Mon Aug  6 2007 Akira TAGOH <tagoh@redhat.com> - 0.4.1-3
- Own /etc/X11/xinit/xinput.d (#250960)

* Mon Jul 30 2007 Akira TAGOH <tagoh@redhat.com> - 0.4.1-2
- Update Require for xorg-x11-xinit

* Wed Jul 25 2007 Akira TAGOH <tagoh@redhat.com> - 0.4.1-1
- New upstream release.
  - xinput.sh has been moved from xorg-x11-xinit.

* Tue Jan 30 2007 Akira TAGOH <tagoh@redhat.com> - 0.3.4-1
- Translations update release.

* Wed Jan 24 2007 Matthias Clasen <mclasen@redhat.com> - 0.3.3-3
- Add X-GNOME-PersonalSettings to the desktop file categories (#224159)
- Use desktop-file-install

* Mon Oct  2 2006 Akira TAGOH <tagoh@redhat.com> - 0.3.3-2
- added Assamese, Greek and Marathi translation. (#208258)

* Mon Oct  2 2006 Akira TAGOH <tagoh@redhat.com> - 0.3.3-1
- Translations update release. (#208258, #208512)

* Fri Sep  8 2006 Akira TAGOH <tagoh@redhat.com> - 0.3.2-1
- New upstream release.
  - added an icon. (#199337)
- removed the unnecessary patches:
  - im-chooser-r49.patch
  - im-chooser-r53.patch

* Tue Aug 29 2006 Akira TAGOH <tagoh@redhat.com> - 0.3.1-3
- im-chooser-r53.patch: take care of the suffix to appears current selection.
  (#204433)

* Fri Aug 25 2006 Akira TAGOH <tagoh@redhat.com> - 0.3.1-2
- im-chooser-r49.patch: removed MimeType field from .desktop file. (#203982)

* Tue Aug 15 2006 Akira TAGOH <tagoh@redhat.com> - 0.3.1-1
- New upstream release.

* Mon Jul 24 2006 Akira TAGOH <tagoh@redhat.com> - 0.3.0-2
- New upstream release.
- add libgnomeui-devel to BR.
- im-chooser-suffix-r40.patch: applied to support the recent change
  in the xinput files.

* Thu Jul 20 2006 Akira TAGOH <tagoh@redhat.com> - 0.2.2-2
- rebuilt

* Wed Jul 12 2006 Akira TAGOH <tagoh@redhat.com> - 0.2.2-1
- New upstream release.

* Mon Jul 10 2006 Akira TAGOH <tagoh@redhat.com> - 0.2.1-3
- New upstream release.
- improved the package summary and description.
- added intltool to BuildReq.
- added gettext to BuildReq.

* Fri Jul  7 2006 Akira TAGOH <tagoh@redhat.com> - 0.2.0-1
- New upstream release.
- use dist tag.
- registered xinputrc alternatives for none and xim.
- removed the empty docs.
- add Requires: xorg-x11-xinit >= 1.0.2-5.fc6 for new xinput.sh.

* Wed Jun  7 2006 Akira TAGOH <tagoh@redhat.com> - 0.1.1-1
- Initial package.

