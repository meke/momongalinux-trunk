%global momorel 2

Summary: Library for manipulating panoramic images
Name: libpano13
Version: 2.9.18
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://panotools.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/project/panotools/%{name}/%{name}-%{version}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgcj-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: zlib-devel

%description
Helmut Dersch's Panorama Tools library.  Provides very high quality
manipulation, correction and stitching of panoramic photographs.
Due to patent restrictions, this library has a maximum fisheye field-of-view
restriction of 179 degrees to prevent stitching of hemispherical photographs.

%package devel
Summary: Header files and development libraries from libpano13
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on libpano13.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of la file
rm -f %{buildroot}%{_libdir}/libpano13.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog* INSTALL NEWS README* TODO.org
%doc doc/Optimize.txt doc/stitch.txt
%{_bindir}/PT*
%{_bindir}/panoinfo
%{_libdir}/libpano13.so.*
%{_mandir}/man1/PT*.1*
%{_mandir}/man1/panoinfo.1*

%files devel
%defattr(-,root,root)
%doc COPYING
%{_includedir}/pano13
%{_libdir}/libpano13.so
%{_libdir}/pkgconfig/libpano13.pc

%changelog
* Sun Apr  8 2012 NARITA koichi <pulsar@momonga-linux.org>
- (2.9.18-2m)
- rebuild against libtiff-4.0.1

* Fri Jul 22 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.18-1m)
- version 2.9.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.17-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.17-1m)
- update to 2.9.17

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9.14-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.14-3m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.14-2m)
- rebuild against libjpeg-8a

* Sun Dec  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.9.14-1m)
- initial package for hugin-2009.2.0
