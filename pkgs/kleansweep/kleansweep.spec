%global momorel 19
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 4m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Reclaim disk space by finding unneeded files
Name: kleansweep
Version: 0.2.9
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://linux.bydg.org/~yogin/
Source0: http://linux.bydg.org/~yogin/%{name}-%{version}.tar.bz2 
Source1: %{name}-ja.po
Source2: %{name}rc
Patch0: %{name}-%{version}-desktop.patch
Patch1: http://launchpadlibrarian.net/11970309/kubuntu_03_fix_scons_chmod_error.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: perl
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: scons

%description
KleanSweep allows you to reclaim disk space by finding unneeded files. It
can search for files based on several criteria: you can seek for empty
files, backup files, broken symbolic links, dead menu entries, duplicated
files, orphaned files (files not found in the RPM database), and more.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .scons~

# for Japanese
cp %{SOURCE1} po/ja.po

# disable rpath
sed -i 's|^env.KDEuse("environ rpath")|env.KDEuse("environ")|g' SConstruct

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"

scons %{?_smp_mflags} kdeincludes=%{_includedir}/kde kdelibs=%{_libdir}/kde3

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
scons install prefix=%{buildroot}%{_prefix}

# install kleansweeprc
mkdir -p %{buildroot}%{_datadir}/config
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/config/

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --remove-category Utility \
  %{buildroot}%{_datadir}/applnk/System/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README THANKS TODO
%{_bindir}/%{name}
%{_bindir}/%{name}-helper
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/config/%{name}rc
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.9-19m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.9-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.9-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.9-16m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-15m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.9-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.9-13m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.9-12m)
- rebuild against qt3

* Sat Apr 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-11m)
- Requires: perl

* Sat Apr 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.9-10m)
- support scons-0.98.x
-- add http://launchpadlibrarian.net/11970309/kubuntu_03_fix_scons_chmod_error.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.9-9m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.9-8m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-7m)
- update Japanese translation

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.9-6m)
- %%NoSource -> NoSource

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-5m)
- update Japanese translation

* Wed Mar 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-4m)
- update Japanese translation

* Sun Mar 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-3m)
- update Japanese translation

* Sun Mar 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-2m)
- add Japanese translation

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.9-1m)
- initial package for Momonga Linux
