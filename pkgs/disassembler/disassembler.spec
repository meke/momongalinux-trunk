%global momorel 12

Summary: disassembler
Name: disassembler
Version: 0.3.4
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}.%{version}.tar.bz 
NoSource: 0
Patch0: disassembler.0.3.4-build.patch
Patch1: disassembler.0.3.4-gcc43.patch
Patch2: disassembler.0.3.4-gcc44.patch
URL: http://users.skynet.be/bk329156/disassembler.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtkmm-devel
BuildRequires: libgnomeuimm-devel
BuildRequires: libgnomecanvasmm-devel
BuildRequires: libglademm-devel
BuildRequires: gconfmm-devel

%description
For the time being, this is a one man project. Which of course means
that I have a lot of ideas for the disassembler, but a lot less time
to try and make them come true.

I would like 'disassembler' to be a versatile product that can
disassemble diffe rent kinds of executables on different kinds of
processors. But without help from others, I will only be a ble to do
something for intel environments.

%prep
%setup -q -n %{name}.%{version}
%patch0 -p1 -b .build
%patch1 -p1 -b .gcc43~
%patch2 -p1 -b .gcc44~

%build
autoreconf -vfi
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
find %{buildroot} -name "*.la" -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/%{name}
%{_bindir}/read_imports
%{_libdir}/libdisassembly.so*
%{_datadir}/disass/disassembly_winpe/imports

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-7m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-5m)
- rebuild against gcc43

* Sun Feb 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-4m)
- add gcc43 patch

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-3m)
- %%NoSource -> NoSource

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.4-2m)
- To Main

* Sat Mar 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.4-1m)
- initial build
- TO.Alter
