%global         momorel 2

Name:           perl-Class-Method-Modifiers
Version:        2.10
Release:        %{momorel}m%{?dist}
Summary:        Provides Moose-like method modifiers
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Class-Method-Modifiers/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/Class-Method-Modifiers-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Module-Install-AutoLicense
BuildRequires:  perl-Test-Exception
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Method modifiers are a convenient feature from the CLOS (Common Lisp Object
System) world.

%prep
%setup -q -n Class-Method-Modifiers-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Class/Method/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-1m)
- update to 2.09

* Sat Oct 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-1m)
- update to 2.08

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Wed Sep 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-1m)
- update to 2.06

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-2m)
- rebuild against perl-5.18.0

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-2m)
- rebuild against perl-5.16.3

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02

* Mon Jan 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-1m)
- update to 2.00

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-2m)
- rebuild against perl-5.16.2

* Sun Oct 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Wed Oct 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09
- rebuild against perl-5.16.0

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.07-2m)
- rebuild for new GCC 4.6

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Sun Nov 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.05-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.05-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.05-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-2m)
- rebuild against perl-5.10.1

* Tue Jun 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.01-2m)
- rebuild against rpm-4.6

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- Specfile autogenerated by cpanspec 1.75 for Momonga Linux.
