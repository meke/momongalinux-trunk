%global momorel 9

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?pydir: %define pydir %(%{__python} -c "from distutils.sysconfig import get_config_vars; print get_config_vars()['LIBDEST']")}

Name:           python-json
Version:        3.4
%define version_munge %(sed 's/\\./_/g' <<< %{version})
Release:        %{momorel}m%{?dist}
Summary:        A JSON reader and writer for Python

Group:          Development/Languages
License:        LGPL
URL:            https://sourceforge.net/projects/json-py/
Source0:        http://dl.sourceforge.net/sourceforge/json-py/json-py-%{version_munge}.zip
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7

%description
json.py is an implementation of a JSON (http://json.org) reader and writer in
Python.

%prep
%setup -q -c
chmod a-x *

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{python_sitelib}
install -p -m 0644 json.py minjson.py $RPM_BUILD_ROOT%{python_sitelib}
%{__python} %{pydir}/compileall.py $RPM_BUILD_ROOT%{python_sitelib}
%{__python} -O %{pydir}/compileall.py $RPM_BUILD_ROOT%{python_sitelib}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc changes.txt jsontest.py license.txt readme.txt
%{python_sitelib}/*.py
%{python_sitelib}/*.py[co]

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4-3m)
- rebuild against python-2.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.4-1m)
- import from Fedora

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> 3.4-3
- Rebuild for python 2.5

* Fri Sep  8 2006 Luke Macken <lmacken@redhat.com> 3.4-2
- Rebuild for FC6

* Fri Dec 30 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 3.4-1
- Initial RPM release
