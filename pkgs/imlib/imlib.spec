%global momorel 15

Summary: An image loading and rendering library for X11R6
Name: imlib
Version: 1.9.15
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.enlightenment.org/pages/imlib.html
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.9/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch1: imlib-1.9.15-conf.patch
Patch4: imlib.m4.patch

# http://www.securiteam.com/unixfocus/5WP030UM0W.html
Patch5: imlib-1.9.15-bpp.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libICE-devel, libSM-devel, libX11-devel
BuildRequires: libXext-devel, libXi-devel, 
BuildRequires: gtk+1-devel >= 1.2.10-41m
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libjpeg-devel >= 8a
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: libungif-devel 
Requires: gtk+1 >= 1.2
Obsoletes: Imlib
# Docdir: %{_prefix}/doc

%description
Imlib is a display depth-independent image loading and rendering library.
Imlib is designed to simplify and speed up the process of loading images
and obtaining X Window System drawables.  Imlib provides many simple
manipulation routines which can be used for common operations.  

Install imlib if you need an image loading and rendering library for X11R6.
You may also want to install the imlib-cfgeditor package, which will help
you configure Imlib.

%package devel
Summary: Development tools for Imlib applications.
Group: Development/Libraries
Requires: imlib = %{version}-%{release}
Requires: libpng-devel >= 1.2.2
Requires: libtiff-devel
Requires: libjpeg-devel
Requires: zlib-devel >= 1.1.4-5m
Requires: gtk+1-devel
Requires: libungif-devel
Obsoletes: Imlib

%description devel
The header files, static libraries and documentation needed for
developing Imlib applications.  Imlib is an image loading and rendering
library for X11R6.

Install the imlib-devel package if you want to develop Imlib applications.
You'll also need to install the imlib and imlib_cfgeditor packages.

%package cfgeditor
Summary: A configuration editor for the Imlib library.
Group: System Environment/Libraries
Requires: imlib = %{version}-%{release}

%description cfgeditor
The imlib-cfgeditor package contains the imlib_config program, which you
can use to configure the Imlib image loading and rendering library.
imlib_config can be used to control how Imlib uses color and handles
gamma corrections, etc.

If you're installing the imlib package, you should also install
imlib_cfgeditor.

%prep
%setup -q
%patch1 -p1
%patch4 -p0 -b .m4
%patch5 -p1 -b .bpp

%build
if [ ! -f configure ]; then
  CFLAGS="%{optflags}" ./autogen.sh --prefix=%{_prefix} --sysconfdir=%{_sysconfdir} --libdir=%{_libdir}
else
  CFLAGS="%{optflags}" ./configure --prefix=%{_prefix} --sysconfdir=%{_sysconfdir} --libdir=%{_libdir}
fi

if [ "$SMP" != "" ]; then
  make -j$SMP "MAKE=make -j$SMP"
else
  make
fi
###########################################################################

%install
rm -rf %{buildroot}
make prefix=%{buildroot}%{_prefix} sysconfdir=%{buildroot}%{_sysconfdir} libdir=%{buildroot}%{_libdir} install
mkdir -p %{buildroot}%{_mandir}
mv -f %{buildroot}/usr/man/man1 %{buildroot}%{_mandir}

# remove
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README
%attr(755,root,root) %{_libdir}/lib*.so.*
%config %{_sysconfdir}/*
%{_libdir}/libimlib-*.so

%files cfgeditor
%defattr(-,root,root)
%{_bindir}/imlib_config
%{_mandir}/man1/imlib-config.1*
%{_mandir}/man1/imlib_config.1*

%files devel
%defattr(-,root,root)
#%doc doc/*.gif doc/*.html
%doc doc/*.html
%{_bindir}/imlib-config
%{_libdir}/libImlib.so
%{_libdir}/libgdk_imlib.so
%{_libdir}/*.a
%{_includedir}/*
%{_datadir}/aclocal/*
%{_libdir}/pkgconfig/imlib.pc
%{_libdir}/pkgconfig/imlibgdk.pc

%changelog
* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.15-15m)
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.15-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.15-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.15-12m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.15-11m)
- rebuild against libjpeg-8a

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.15-10m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.15-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.15-8m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.15-7m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.15-6m)
- update Patch1 for fuzz=0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.15-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.15-4m)
- %%NoSource -> NoSource

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.15-3m)
- fix %%changelog section

* Mon Jul  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.15-2m)
- add patch5
- [SECURITY] CVE-2007-3568

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.15-1m)
- update to 1.9.15

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.14-20m)
- delete libtool library

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.14-19m)
- suppress AC_DEFUN warning.

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (1.9.14-18m)
- enable x86_64.

* Sun Dec 26 2004 TAKAHASHI Tamotsu <tamo>
- (1.9.14-17m)
- [SECURITY] fix a typo in the sec2 patch
- update URL tag

* Sat Dec 11 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.9.14-16m)
- [SECURITY] CAN-2004-1026, integer overflow within the image decoding routines

* Wed Sep  1 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.14-15m)
- fixed buffer overflow in bmp handling, CAN-2004-0817

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.9.14-14m)
- revised spec for enabling rpm 4.2.

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.14-13m)
- rebuild against zlib 1.1.4-5m
- delete define prefix
- delete define ver

* Mon Mar 31 2003 smbd <smbd@momonga-linux.org>
- (1.9.14-12m)
- Requires: gtk+ -> Requires: gtk+1 :p

* Mon Nov 25 2002 Kenta MURATA <muraken@momonga-linux.org>
- (1.9.14-11m)
- modify BuildPreReq section: gtk+-devel -> gtk+1-devel.
- modify Require section on devel package: gtk+-devel -> gtk+1-devel.
- modify %%install section: cancel strip imlib_config.

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.9.14-10m)
- add patch for autoconf-2.53

* Mon Oct 21 2002 Kenta MURATA <muraken@momonga-linux.org>
- (1.9.14-9m)
- modify %%install section.

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.9.14-8k)
- cancel gcc-3.1 autoconf-2.53

* Thu May 23 2002 Toru Hoshina <t@kondara.org>
- (1.9.14-6k)
- rebuild against gcc 3.1, autoconf 2.53.

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.9.14-4k)
- rebuild against libpng-1.2.2

* Thu Mar 28 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.9.14-2k)
- BuildPreReq: libungif-devel

* Mon Mar 25 2002 Toru Hoshina <t@kondara.org>
- (1.9.13-2k)
- http://www.redhat.com/support/errata/RHSA-2002-048.html
- no more netpbm.

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (1.9.10-6k)
- rebuild against libpng 1.2.0.

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.9.10-4k)
- no more ifarch alpha.

* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- version 1.9.10

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.9.9

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.9.8.1-7k)
- fixed for FHS

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.
- change dependencies from libgr to netpbm

* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 1.9.8.1

* Mon Dec 20 1999 Shingo Akagaki <dora@kondara.org>
- add imlib_config i18n patch

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Oct 26 1999 Shingo Akagaki <akagaki@ece.numazu-ct.ac.jp>
- up to 1.9.8

* Tue Aug 31 1999 Elliot Lee <sopwith@redhat.com>
- Updates from the RHL 6.0 package.

* Mon Jan 11 1999 Carsten Haitzler <raster@redhat.com>
- up to 1.9.0

* Wed Sep 23 1998 Carsten Haitzler <raster@redhat.com>
- up to 1.8.1

* Tue Sep 22 1998 Cristian Gafton <gafton@redhat.com>
- yet another build for today (%defattr and %attr in the files lists)
- devel docs are back on the spec file

* Tue Sep 22 1998 Carsten Haitzler <raster@redhat.com>
- Added minor patch for ps saving code.

* Mon Sep 21 1998 Cristian Gafton <gafton@redhat.com>
- updated to version 1.8

* Fri Sep 11 1998 Cristian Gafton <gafton@redhat.com>
- take out imlib_config from devel package

* Wed Sep 9 1998 Michael Fulbright <msf@redhat.com>
- upgraded to 1.7
- changed name so it will persist if user later install devel imlib
- added subpackage for imlib_config

* Fri Apr 3 1998 Michael K. Johnson <johnsonm@redhat.com>
- fixed typo

* Fri Mar 13 1998 Marc Ewing <marc@redhat.com>
- Added -k, Obsoletes
- Integrate into CVS source tree
