%global momorel 1

Summary: A software library for talking to the Creative Nomad Jukeboxes and Dell DJs
Name: libnjb
Version: 2.2.7
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Libraries
URL: http://sourceforge.net/projects/libnjb/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.rules
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: udev
BuildRequires: doxygen
BuildRequires: libusb-devel
BuildRequires: pkgconfig

%description
Provides a user-level API (C library) for communicating with the
Creative Nomad JukeBox MP3 player under Linux and *BSD, as well
as simple command-line utilities to demonstrate the API functions.
This library works in user space.

%package devel
Summary: Header files and static libraries from libnjb
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libnjb.

%prep
%setup -q

%build
%configure --disable-static --program-prefix=njb-
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall pkgdocdir=`pwd`/installed-docs

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

# install udev rules file
mkdir -p %{buildroot}%{_sysconfdir}/udev/rules.d
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/udev/rules.d/60-%{name}.rules

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog* FAQ HACKING INSTALL LICENSE README
%config(noreplace) %{_sysconfdir}/udev/rules.d/60-%{name}.rules
%{_bindir}/njb-*
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%doc installed-docs/*
%{_includedir}/%{name}.h
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so

%changelog
* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.7-1m)
- update 2.2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.6-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.6-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.6-9m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.6-8m)
- remove pam file

* Fri Mar 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.6-7m)
- update spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.6-5m)
- set _program_prefix to njb-

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.6-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.6-2m)
- %%NoSource -> NoSource

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.6-1m)
- version 2.2.6
- update libnjb.rules from Fedora

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.5-4m)
- libnjb-devel Requires: pkgconfig

* Sun Jan 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.5-3m)
- get rid of *.la files

* Thu Jun 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.5-2m)
- import libnjb.rules and libnjb.perms from Fedora Core extras
 +* Wed Jan 25 2006 Linus Walleij <triad@df.lth.se> 2.2.4-2
 +- Fix udev problem, let go of hotplug, fix console perms.
 +- Still working on libusb vs udev issues.
- revise Summary

* Mon Jun 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.5-1m)
- initial package for amarok-1.4.1
- Summary and %%description are imported from cooker
