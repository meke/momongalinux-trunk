%global momorel 1
%global realname Fabric

Name:           fabric
Version:        1.2.0
Release:        %{momorel}m%{?dist}
Summary:        A simple Pythonic remote deployment tool

Group:          Applications/System
License:        BSD
URL:            http://www.fabfile.org
Source0:        http://pypi.python.org/packages/source/F/Fabric/Fabric-%{version}.tar.gz
NoSource:	0

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools
Requires:       python-paramiko >= 1.7
Requires:       python-setuptools

%description
Fabric is a simple Pythonic remote deployment tool which is designed to upload
files to, and run shell commands on, a number of servers in parallel or
serially.

%prep
%setup -q -n %{realname}-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
%{__rm} -fr %{buildroot}%{python_sitelib}/paramiko

%files
%defattr(-,root,root,-)
%doc AUTHORS LICENSE README
%{_bindir}/fab
%{python_sitelib}/fabric
%{python_sitelib}/fabfile
%{python_sitelib}/%{realname}*%{version}*.egg-info

%changelog
* Mon Sep 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- Initial Commit Momonga Linux

* Sun Jul 24 2011 Silas Sewell <silas@sewell.org> - 1.2.0-1
- Update to 1.2.0

* Sun Mar 27 2011 Silas Sewell <silas@sewell.ch> - 1.0.1-1
- Update to 1.0.1

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Dec 19 2010 Silas Sewell <silas@sewell.ch> - 0.9.3-1
- Update to 0.9.3
- Remove old page and man page

* Sat Oct 09 2010 Silas Sewell <silas@sewell.ch> - 0.9.2-1
- Update to 0.9.2
- Import man page from upstream branch
- Apply upstream patch to fix incorrect requirements

* Wed Jul 21 2010 David Malcolm <dmalcolm@redhat.com> - 0.9.1-2
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Tue Jul 06 2010 Silas Sewell <silas@sewell.ch> - 0.9.1-1
- Update to 0.9.1
- Add man page

* Mon Nov 09 2009 Silas Sewell <silas@sewell.ch> - 0.9.0-1
- Update to 0.9.0

* Thu Aug 27 2009 Silas Sewell <silas@sewell.ch> - 0.9-0.1.b1
- Update to latest snapshot

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Apr 12 2009 Silas Sewell <silas@sewell.ch> - 0.1.1-2
- Add runtime setuptools requirements
- Re-import source package

* Thu Apr 09 2009 Silas Sewell <silas@sewell.ch> - 0.1.1-1
- Update to 0.1.1
- Up Paramiko version dependency to 1.7
- Remove Python version dependency
- Make sed safer

* Sat Mar 28 2009 Silas Sewell <silas@sewell.ch> - 0.1.0-3
- Fix dependencies
- Fix non-executable-script error

* Thu Mar 26 2009 Silas Sewell <silas@sewell.ch> - 0.1.0-2
- Change define to global

* Sun Feb 22 2009 Silas Sewell <silas@sewell.ch> - 0.1.0-1
- Update to 0.1.0
- Up Python requirement to 2.5 per recommendation on official site

* Thu Nov 20 2008 Silas Sewell <silas@sewell.ch> - 0.0.9-3
- Fix changelog syntax issue

* Thu Nov 20 2008 Silas Sewell <silas@sewell.ch> - 0.0.9-2
- Fix various issues with the spec file

* Wed Nov 19 2008 Silas Sewell <silas@sewell.ch> - 0.0.9-1
- Initial package
