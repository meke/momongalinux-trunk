%global momorel 8
%global rcver 1
%global prever 4

Summary: An open-source, patent-free speech codec
Name: speex
Version: 1.2
Release: 0.%{prever}.%{momorel}m%{?dist}
License: Modified BSD
URL: http://www.speex.org/
Group: System Environment/Libraries
Source0: http://downloads.us.xiph.org/releases/%{name}/%{name}-%{version}rc%{rcver}.tar.gz 
NoSource: 0
Patch0: %{name}-1.1.6-fix-pkgconfig-path.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libogg-devel >= 1.2.0
BuildRequires: libao-devel >= 1.0.0
BuildRequires: pkgconfig

%description
Speex is a patent-free audio codec designed especially for voice (unlike 
Vorbis which targets general audio) signals and providing good narrowband 
and wideband quality. This project aims to be complementary to the Vorbis
codec.

%package devel
Summary: Speex development files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Speex development files.

%prep
%setup -q -n %{name}-%{version}rc%{rcver}
%patch0 -p1 -b .pkgconfig

%build
%configure --with-ogg-libraries=%{_libdir}
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

# clean up
rm -f %{buildroot}/usr/share/doc/*/manual.pdf

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README* TODO
%doc doc/manual.pdf
%{_bindir}/speexdec
%{_bindir}/speexenc
%{_libdir}/libspeex.so.*
%{_libdir}/libspeexdsp.so.*
%{_mandir}/man1/speexdec.1*
%{_mandir}/man1/speexenc.1*

%files devel
%defattr(-,root,root)
%{_includedir}/speex
%{_libdir}/pkgconfig/speex.pc
%{_libdir}/pkgconfig/speexdsp.pc
%{_libdir}/libspeex.a
%{_libdir}/libspeexdsp.a
%{_libdir}/libspeex.so
%{_libdir}/libspeexdsp.so
%{_datadir}/aclocal/speex.m4

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-0.4.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-0.4.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-0.4.6m)
- full rebuild for mo7 release

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-0.4.5m)
- rebuild against libao and libogg

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.4.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.4.3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.4.2m)
- drop Patch1 for fuzz=0, already merged upstream
- License: Modified BSD

* Thu Jul 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.4.1m)
- update to version 1.2 rc1

* Sat May 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.3.4m)
- [SECURITY] CVE-2008-1686
- import security patch from Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-0.3.3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-0.3.2m)
- %%NoSource -> NoSource

* Sun Dec 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.3.1m)
- update to version 1.2 beta3

* Thu May 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.2.1m)
- update to version 1.2 beta2

* Fri Feb 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.1.1m)
- update to version 1.2 beta1
- import speex-1.1.6-fix-pkgconfig-path.patch from cooker
- remove merged m4.patch

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-2m)
- delete libtool library

* Thu May  4 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.5-1m)
- version up

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-3m)
- suppress AC_DEFUN warning.

* Mon Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.4-2m)
- enable x86_64. 

* Sat Nov 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sat Aug 21 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-1m)
  update to 1.0

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0rc2-3m)
- revised spec for rpm 4.2.

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0rc2-2m)
- s/Copyright:/License:/

* Wed Feb 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0rc2-1m)
- version 1.0rc2

* Thu Oct 03 2002 Jean-Marc Valin 
- Added devel package inspired from PLD spec file

* Tue Jul 30 2002 Fredrik Rambris <boost@users.sourceforge.net> 0.5.2
- Added buildroot and docdir and ldconfig. Makes it builadble by non-roots
  and also doesn't write to actual library paths when building.
