%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           papyon
Version:        0.5.6
Release:        %{momorel}m%{?dist}
Summary:        Python libraries for MSN Messenger network
Group:          Development/Languages
License:        GPLv2+
URL:            http://www.freedesktop.org/wiki/Software/papyon
Source0:        http://www.freedesktop.org/software/%{name}/releases/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  pyOpenSSL
BuildRequires:  python-devel >= 2.7
BuildRequires:  pygobject
BuildRequires:  python-crypto
Requires:       pyOpenSSL
Requires:       pygobject
Requires:       python-crypto
BuildArch:      noarch

%description
%{name} is the library behind the msn connection manager for telepathy.
%{name} uses the glib mainloop to process the network events in an
asynchronous manner

%prep
%setup -q
%{__sed} -i 's|\#!/usr/bin/env python||' papyon/msnp2p/test.py

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING NEWS AUTHORS
%{python_sitelib}/*

%changelog
* Wed Nov 23 2011 NAARITA Koichi <pulsar@momonga-linux.org>
- (0.5.6-1m)
- update to 0.5.6

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.5-2m)
- rebuild against python-2.7

* Wed Apr 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.5-1m)
- update to 0.5.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-2m)
- rebuild for new GCC 4.6

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-1m)
- update to 0.5.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-2m)
- rebuild for new GCC 4.5

* Fri Oct 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.9-1m)
- import from Fedora devel

* Fri Jul  9 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.4.9-1
- Update to 0.4.9.

* Thu May 27 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.4.8-1
- Update to 0.4.8.

* Thu May 20 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.4.7-1
- Update to 0.4.7.
- Add BR & requires on python-crypto.

* Wed May 12 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.4.6-2
- Drop patch to workaround invalid sha1d, since it was fubar'd.

* Sat May  8 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.4.6-1
- Update to 0.4.6.
- Backport patch to work around invalid SHA1D attributes in msn objects. (#568923)

* Wed Mar 10 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.4.5-1
- Update to 0.4.5.

* Tue Jan 19 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.4.4-1
- Update to 0.4.4.
- Drop base64decode patch.  Fixed upstream.

* Tue Dec 29 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.4.3-2
- Add patch to fix a crasher with TypeError in b64decode.

* Fri Oct  9 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.4.3-1
- Update to 0.4.3.

* Tue Sep  8 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.4.2-1
- Update to 0.4.2.
- Drop BR and Req on python-crypto.

* Fri Aug 14 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.4.1-3
- Clean up some rpmlint error.

* Tue Aug  4 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.4.1-2
- Update URL.

* Tue Jul 28 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.4.1-1
- Update to 0.4.1.

* Sun Jul  5 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.4.0-1
- Initial spec.
