%global momorel 1

Summary: WPA/WPA2/IEEE 802.1X Supplicant
Name: wpa_supplicant
Version: 1.1
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://w1.fi/wpa_supplicant/
Group: System Environment/Base
Source0: http://w1.fi/releases/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.config
Source2: %{name}.conf
Source3: %{name}.service
Source4: %{name}.sysconfig
Source6: %{name}.logrotate
Patch0: wpa_supplicant-assoc-timeout.patch
Patch1: wpa_supplicant-flush-debug-output.patch
Patch2: wpa_supplicant-dbus-service-file-args.patch
Patch3: wpa_supplicant-quiet-scan-results-message.patch
Patch4: wpa_supplicant-squelch-driver-disconnect-spam.patch
Patch5: wpa_supplicant-openssl-more-algs.patch
Patch6: wpa_supplicant-gui-qt4.patch
Patch7: libnl3-includes.patch
Patch8: rh837402-less-aggressive-roaming.patch
Patch100: wpa_supplicant-0.7.2-generate-libeap-peer.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: dbus-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: qt-devel
BuildRequires: readline-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: systemd-units
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units


%description
wpa_supplicant is a WPA Supplicant for Linux, BSD and Windows with support 
for WPA and WPA2 (IEEE 802.11i / RSN). Supplicant is the IEEE 802.1X/WPA 
component that is used in the client stations. It implements key negotiation 
with a WPA Authenticator and it controls the roaming and IEEE 802.11 
authentication/association of the wlan driver.

%package gui
Summary: Graphical User Interface for %{name}
Group: Applications/System

%description gui
Graphical User Interface for wpa_supplicant written using QT

%package -n libeap
Summary: EAP peer library
Group: System Environment/Libraries

%description -n libeap
This package contains the runtime EAP peer library. Don't use this
unless you know what you're doing.

%package -n libeap-devel
Summary: Header files for EAP peer library
Group: Development/Libraries
Requires: libeap = %{version}-%{release}

%description -n libeap-devel
This package contains header files for using the EAP peer library.
Don't use this unless you know what you're doing.

%prep
%setup -q
%patch0 -p1 -b .assoc-timeout
%patch1 -p1 -b .flush-debug-output
%patch2 -p1 -b .dbus-service-file
%patch3 -p1 -b .quiet-scan-results-msg
%patch4 -p1 -b .disconnect-spam
%patch5 -p1 -b .more-openssl-algs
%patch6 -p1 -b .qt4
%patch7 -p1 -b .libnl3
%patch8 -p1 -b .rh837402-less-aggressive-roaming

%build
pushd wpa_supplicant
  cp %{SOURCE1} .config
  CFLAGS="${CFLAGS:-%optflags}" ; export CFLAGS ;
  CXXFLAGS="${CXXFLAGS:-%optflags}" ; export CXXFLAGS ;
  make %{_smp_mflags}
  QTDIR=%{_libdir}/qt4 make wpa_gui-qt4 %{_smp_mflags}
  make -C doc/docbook man
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# init scripts
install -D -m 0755 %{SOURCE3} %{buildroot}/%{_unitdir}/%{name}.service
install -D -m 0644 %{SOURCE4} %{buildroot}/%{_sysconfdir}/sysconfig/%{name}
install -D -m 0644 %{SOURCE6} %{buildroot}/%{_sysconfdir}/logrotate.d/%{name}

# config
install -D -m 0600 %{SOURCE2} %{buildroot}/%{_sysconfdir}/%{name}/%{name}.conf

# binary
install -d %{buildroot}/%{_sbindir}
install -m 0755 %{name}/wpa_passphrase %{buildroot}/%{_sbindir}
install -m 0755 %{name}/wpa_cli %{buildroot}/%{_sbindir}
install -m 0755 %{name}/wpa_supplicant %{buildroot}/%{_sbindir}
install -D -m 0644 %{name}/dbus/dbus-wpa_supplicant.conf %{buildroot}/%{_sysconfdir}/dbus-1/system.d/wpa_supplicant.conf
install -D -m 0644 %{name}/dbus/fi.w1.wpa_supplicant1.service %{buildroot}/%{_datadir}/dbus-1/system-services/fi.w1.wpa_supplicant1.service
install -D -m 0644 %{name}/dbus/fi.epitest.hostap.WPASupplicant.service %{buildroot}/%{_datadir}/dbus-1/system-services/fi.epitest.hostap.WPASupplicant.service

# gui
install -d %{buildroot}%{_bindir}
install -m 0755 %{name}/wpa_gui-qt4/wpa_gui %{buildroot}%{_bindir}

# running
mkdir -p %{buildroot}%{_localstatedir}/run/%{name}

# man pages
install -d %{buildroot}%{_mandir}/man{5,8}
install -m 0644 %{name}/doc/docbook/*.8 %{buildroot}%{_mandir}/man8
install -m 0644 %{name}/doc/docbook/*.5 %{buildroot}%{_mandir}/man5

# some cleanup in docs
rm -f  %{name}/doc/.cvsignore
rm -rf %{name}/doc/docbook
chmod -R 0644 %{name}/examples/*.py

# HAAACK
patch -p1 -b --suffix .wimax < %{PATCH100}
pushd wpa_supplicant
  make clean
  make -C ../src/eap_peer
  make DESTDIR=%{buildroot} LIB=%{_lib} -C ../src/eap_peer install
  sed -i -e 's|libdir=/usr/lib|libdir=%{_libdir}|g' %{buildroot}/%{_libdir}/pkgconfig/*.pc
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
if [ $1 -eq 1 ] ; then 
  # Initial installation 
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable wpa_supplicant.service > /dev/null 2>&1 || :
  /bin/systemctl stop wpa_supplicant.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart wpa_supplicant.service >/dev/null 2>&1 || :
fi

%triggerun -- wpa_supplicant < 0.7.3-13m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply wpa_supplicant
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save wpa_supplicant >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del wpa_supplicant >/dev/null 2>&1 || :
/bin/systemctl try-restart wpa_supplicant.service >/dev/null 2>&1 || :


%files
%defattr(-, root, root)
%doc COPYING %{name}/ChangeLog README %{name}/eap_testing.txt %{name}/todo.txt %{name}/wpa_supplicant.conf %{name}/examples
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%{_unitdir}/%{name}.service
%{_sysconfdir}/dbus-1/system.d/%{name}.conf
%{_datadir}/dbus-1/system-services/fi.epitest.hostap.WPASupplicant.service
%{_datadir}/dbus-1/system-services/fi.w1.wpa_supplicant1.service
%{_sbindir}/wpa_passphrase
%{_sbindir}/wpa_supplicant
%{_sbindir}/wpa_cli
%dir %{_localstatedir}/run/%{name}
%dir %{_sysconfdir}/%{name}
%{_mandir}/man8/*
%{_mandir}/man5/*

%files gui
%defattr(-, root, root)
%{_bindir}/wpa_gui

%files -n libeap
%{_libdir}/libeap.so.0*

%files -n libeap-devel
%{_includedir}/eap_peer
%{_libdir}/libeap.so
%{_libdir}/pkgconfig/*.pc

%post -n libeap -p /sbin/ldconfig

%postun -n libeap -p /sbin/ldconfig

%changelog
* Wed May  1 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-1m)
- 1.1 release

* Tue Jun 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- 1.0 release
- fix systemd ordering

* Tue Feb  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-0.1m)
- update 1.0.0-rc2

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.3-3m)
- support systemd

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.3-2m)
- fix dbus error

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.3-1m)
- update 0.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.8-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.8-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.8-6m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-5m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-4m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.8-1m)
- rebuild against libjpeg-7

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-1m)
- sync with Fedora 11 (1:0.6.8-4)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-5m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-4m)
- update Patch4 for fuzz=0
- License: GPLv2+

* Wed Jul 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-3m)
- merge changes of F9 updates
 +* Tue Jun 10 2008 Dan Williams <dcbw@redhat.com> - 1:0.6.3-6
 +- Fix 802.11a frequency bug
 +- Always schedule specific SSID scans to help find hidden APs
 +- Properly switch between modes on mac80211 drivers
 +- Give adhoc connections more time to assocate
- fix BR: qt3-devel

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-2m)
- rebuild against openssl-0.9.8h-1m

* Sat May 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.3-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.7-3m)
- rebuild against gcc43

* Mon Jan 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.7-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.7-1m)
- version 0.5.7
- enable %%{optflags}
- BuildRequires: dbus-devel

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.8-1m)
- import from fc

* Sun Apr  2 2006 Dan Williams <dcbw@redhat.com> - 0.4.8-7
- Work around older & incorrect drivers that return null-terminated SSIDs

* Mon Mar 27 2006 Dan Williams <dcbw@redhat.com> - 0.4.8-6
- Add patch to make orinoco happy with WEP keys
- Enable Prism54-specific driver
- Disable ipw-specific driver; ipw2x00 should be using WEXT instead

* Fri Mar  3 2006 Dan Williams <dcbw@redhat.com> - 0.4.8-5
- Increase association timeout, mainly for drivers that don't
	fully support WPA ioctls yet

* Fri Mar  3 2006 Dan Williams <dcbw@redhat.com> - 0.4.8-4
- Add additional BuildRequires #rh181914#
- Add prereq on chkconfig #rh182905# #rh182906#
- Own /var/run/wpa_supplicant and /etc/wpa_supplicant #rh183696#

* Wed Mar  1 2006 Dan Williams <dcbw@redhat.com> - 0.4.8-3
- Install wpa_passphrase too #rh183480#

* Mon Feb 27 2006 Dan Williams <dcbw@redhat.com> - 0.4.8-2
- Don't expose private data on the control interface unless requested

* Fri Feb 24 2006 Dan Williams <dcbw@redhat.com> - 0.4.8-1
- Downgrade to 0.4.8 stable release rather than a dev release

* Sun Feb 12 2006 Dan Williams <dcbw@redhat.com> - 0.5.1-3
- Documentation cleanup (Terje Rosten <terje.rosten@ntnu.no>)

* Sun Feb 12 2006 Dan Williams <dcbw@redhat.com> - 0.5.1-2
- Move initscript to /etc/rc.d/init.d

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.5.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.5.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Sun Feb  5 2006 Dan Williams <dcbw@redhat.com> 0.5.1-1
- Update to 0.5.1
- Add WE auth fallback to actually work with older drivers

* Thu Jan 26 2006 Dan Williams <dcbw@redhat.com> 0.4.7-2
- Bring package into Fedora Core
- Add ap_scan control interface patch
- Enable madwifi-ng driver

* Sun Jan 15 2006 Douglas E. Warner <silfreed@silfreed.net> 0.4.7-1
- upgrade to 0.4.7
- added package w/ wpa_gui in it

* Mon Nov 14 2005 Douglas E. Warner <silfreed@silfreed.net> 0.4.6-1
- upgrade to 0.4.6
- adding ctrl interface changes recommended 
  by Hugo Paredes <hugo.paredes@e-know.org>

* Sun Oct  9 2005 Douglas E. Warner <silfreed@silfreed.net> 0.4.5-1
- upgrade to 0.4.5
- updated config file wpa_supplicant is built with
  especially, the ipw2100 driver changed to just ipw
  and enabled a bunch more EAP
- disabled dist tag

* Thu Jun 30 2005 Douglas E. Warner <silfreed@silfreed.net> 0.4.2-3
- fix typo in init script

* Thu Jun 30 2005 Douglas E. Warner <silfreed@silfreed.net> 0.4.2-2
- fixing init script using fedora-extras' template
- removing chkconfig default startup

* Tue Jun 21 2005 Douglas E. Warner <silfreed@silfreed.net> 0.4.2-1
- upgrade to 0.4.2
- new sample conf file that will use any unrestricted AP
- make sysconfig config entry
- new BuildRoot for Fedora Extras
- adding dist tag to Release

* Fri May 06 2005 Douglas E. Warner <silfreed@silfreed.net> 0.3.8-1
- upgrade to 0.3.8

* Thu Feb 10 2005 Douglas E. Warner <silfreed@silfreed.net> 0.3.6-2
- compile ipw driver in

* Wed Feb 09 2005 Douglas E. Warner <silfreed@silfreed.net> 0.3.6-1
- upgrade to 0.3.6

* Thu Dec 23 2004 Douglas E. Warner <silfreed@silfreed.net> 0.2.5-4
- fixing init script

* Mon Dec 20 2004 Douglas E. Warner <silfreed@silfreed.net> 0.2.5-3
- fixing init script
- adding post/preun items to add/remove via chkconfig

* Mon Dec 20 2004 Douglas E. Warner <silfreed@silfreed.net> 0.2.5-2
- adding sysV scripts

* Mon Dec 20 2004 Douglas E. Warner <silfreed@silfreed.net> 0.2.5-1
- Initial RPM release.

