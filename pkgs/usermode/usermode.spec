%global momorel 2

Summary: Graphical tools for certain user account management tasks.
Name: usermode
Version: 1.108
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL: https://fedorahosted.org/usermode/
Source: https://fedorahosted.org/releases/u/s/usermode/%{name}-%{version}.tar.xz
NoSource: 0
Requires: pam, passwd, util-linux
BuildRequires: desktop-file-utils, gettext, glib2-devel, gtk2-devel, intltool
BuildRequires: libblkid-devel, libSM-devel, libselinux-devel, libuser-devel
BuildRequires: pam-devel, perl-XML-Parser, startup-notification-devel
BuildRequires: util-linux

%package gtk
Summary: Graphical tools for certain user account management tasks.
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description
The usermode package contains the userhelper program, which can be
used to allow configured programs to be run with superuser privileges
by ordinary users.

%description gtk
The usermode-gtk package contains several graphical tools for users:
userinfo, usermount and userpasswd.  Userinfo allows users to change
their finger information.  Usermount lets users mount, unmount, and
format filesystems.  Userpasswd allows users to change their
passwords.

Install the usermode-gtk package if you would like to provide users with
graphical tools for certain account management tasks.

%prep
%setup -q

%build
%configure --with-selinux

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} VENDOR="" INSTALL='install -p'

# make userformat symlink to usermount
ln -sf usermount %{buildroot}%{_bindir}/userformat

# We set up the shutdown programs to be wrapped in this package.  Other
# packages are on their own....
mkdir -p %{buildroot}%{_sysconfdir}/pam.d %{buildroot}%{_sysconfdir}/security/console.apps
for wrappedapp in halt reboot poweroff ; do
        ln -s consolehelper %{buildroot}%{_bindir}/${wrappedapp}
        install -m644 $wrappedapp %{buildroot}%{_sysconfdir}/security/console.apps/${wrappedapp}
        cp shutdown.pamd %{buildroot}%{_sysconfdir}/pam.d/${wrappedapp}
done

echo 'USER=root' > %{buildroot}%{_sysconfdir}/security/console.apps/config-util

for i in userinfo.desktop userpasswd.desktop usermount.desktop ; do
	echo 'NotShowIn=GNOME;KDE;' >>%{buildroot}%{_datadir}/applications/$i
	desktop-file-install --vendor="" --delete-original \
		--dir %{buildroot}%{_datadir}/applications \
		--remove-category X-Red-Hat-Base \
		%{buildroot}%{_datadir}/applications/$i
done

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%attr(4711,root,root) %{_sbindir}/userhelper
%{_bindir}/consolehelper
%{_mandir}/man8/userhelper.8*
%{_mandir}/man8/consolehelper.8*
# PAM console wrappers
%{_bindir}/halt
%{_bindir}/reboot
%{_bindir}/poweroff
%exclude %{_bindir}/shutdown
%config(noreplace) /etc/pam.d/halt
%config(noreplace) /etc/pam.d/reboot
%config(noreplace) /etc/pam.d/poweroff
%config(noreplace) /etc/security/console.apps/config-util
%config /etc/security/console.apps/halt
%config /etc/security/console.apps/reboot
%config /etc/security/console.apps/poweroff

%files gtk
%defattr(-,root,root)
%{_bindir}/usermount
%{_bindir}/userformat
%{_mandir}/man1/usermount.1*
%{_bindir}/userinfo
%{_mandir}/man1/userinfo.1*
%{_bindir}/userpasswd
%{_mandir}/man1/userpasswd.1*
%{_bindir}/consolehelper-gtk
%{_mandir}/man8/consolehelper-gtk.8.*
%{_bindir}/pam-panel-icon
%{_mandir}/man1/pam-panel-icon.1.*
%{_datadir}/%{name}
%{_datadir}/pixmaps/*
%{_datadir}/applications/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.108-2m)
- rebuild for glib 2.33.2

* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.108-1m)
- update 1.108

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.105-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.105-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.105-3m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.105-2m)
- remove vendor=redhat from desktop files
- remove X-Red-Hat-Base from Categories of desktop files

* Sun Jul 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.105-1m)
- update 1.105

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.96.1-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.96.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.96.1-3m)
- rebuild against rpm-4.6

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.96.1-2m)
- remove vendor="redhat" from desktop files again

* Wed May  7 2008 Yohsuke Ooi <meke@momnga-linux.org>
- (1.96.1-1m)
- update 1.96.1

* Tue Apr 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.92-5m)
- remove vendor="redhat" from desktop files

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.92-4m)
- rebuild against gcc43

* Mon Feb  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.92-3m)
- BPR: libwnck -> libwnck-devel

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.92-2m)
- rebuild against libwnck-2.20.0

* Mon Jun 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.92-1m)
- rebuild against pam-0.99.7-1m
- update to 1.92

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.74-4m)
- add patch1 (desktop-file-utils)
-- remove-category AdvancedSettings Application X-Red-Hat-Base

* Mon Nov 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.74-3m)
- rebuild against libwnck-2.12.1-1m
- add usermode-1.74-intltoolize-force.patch

* Thu Apr  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.74-2m)
- remove duplicated files

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.74-1m)
- update to 1.17
- enable selinux

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (1.70-2m)
- import from Fedora.

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.69-5m)
- revised spec for rpm 4.2.

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.63-3m)
- revised spec for rpm 4.2.

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.63-2m)
- rebuild against for XFree86-4.3.0

* Sun Jan  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.63-1m)
- version 1.63

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (1.53-2k)
- ver up.
- original tar ball provides consolehelper-gtk.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.43-8k)
- change requires.

* Sun Dec 02 2001 Motonobu Ichimura <famao@kondara.org>
- (1.43-6k)
- zh_CN.GB2312 => zh_CN

* Mon Nov 26 2001 Toru Hoshina <t@kondara.org>
- (1.43-4k)
- dont't use kontrol-panel anymore.

* Fri Oct 12 2001 Toru Hoshina <t@kondara.org>
- (1.43-2k)
- version up.

* Sat Sep  1 2001 Toru Hoshina <t@kondara.org>
- (1.36-10k)
- fixed system-auth again.

* Mon Aug  6 2001 Toru Hoshina <toru@df-usa.com>
- (1.36-8k)
- fixed system-auth issue.

* Fri Jun 22 2001 Toru Hoshina <toru@df-usa.com>
- /usr/sbin/shutdown is not there...

* Thu May 31 2001 Shingo Akagaki <dora@kondara.org>
- add -x11 package

* Fri Oct  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- don't pass on arguments to halt and reboot, because they error out

* Thu Oct  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix the /usr/bin/shutdown wrapper so that root can call shutdown
- only include the /usr/bin/shutdown wrapper on 6.x
- also sanitize LC_MESSAGES
- tweak sanitizing checks (from mkj)

* Wed Oct  4 2000 Jakub Jelinek <jakub@redhat.com>
- fix a security bug with LC_ALL/LANG variables (#18046)

* Mon Aug 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- mark defined strings translateable (#17006)

* Thu Aug 24 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix incorrect user name
- add a shell wrapper version of /usr/bin/shutdown
- build for 6.x errata

* Wed Aug 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix stdin/stdout redirection shenanigans (#11706)
- fix authentication and execution as users other than root
- make sure the right descriptors are terminals before dup2()ing them
- cut out an extra-large CPU waster that breaks GUI apps

* Mon Aug 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix typo (#16664)

* Sun Aug 20 2000 Nalin Dahyabhai <nalin@redhat.com>
- previous fix, part two

* Sat Aug 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix inadvertent breakage of the shell-changing code

* Fri Aug 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix the "run unprivileged" option

* Mon Aug 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- actually use the right set of translations

* Fri Aug 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- remove the shutdown command from the list of honored commands

* Wed Aug  9 2000 Nalin Dahyabhai <nalin@redhat.com>
- merge in updated translations
- set XAUTHORITY after successful authentication (#11006)

* Wed Aug  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- install translations
- fixup a messy text string
- make "Mount"/"Unmount" translatable
- stop prompting for passwords to shut down -- we can hit ctrl-alt-del anyway,
  and gdm users can just shut down without logging in

* Mon Jul 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- attempt to add i18n support

* Wed Jul 12 2000 Nalin Dahyabhai <nalin@redhat.com>
- attempt to get a usable icon for userhelper-wrap (#13616, #13768)

* Wed Jul  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix them right this time

* Mon Jul  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix verbosity problems

* Mon Jun 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- strip all binaries by default
- add the name of the program being run to the userhelper dialog
- add a graphic to the userhelper-wrap package
- add a button to jump straight to nonprivileged operation when supported

* Sun Jun 18 2000 Matt Wilson <msw@redhat.com>
- rebuilt to see if we get stripped binaries

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- move man pages to %%{_mandir}

* Thu Jun  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- modify PAM setup to use system-auth
- bzip2 compress tarball

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.20-1).
- add -q at %setup.

* Fri Mar 17 2000 Ngo Than <than@redhat.de>
- fix problem with LANG and LC_ALL
- compress source with bzip2

* Thu Mar 09 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix problem parsing userhelper's -w flag with other args

* Wed Mar 08 2000 Nalin Dahyabhai <nalin@redhat.com>
- ignore read() == 0 because the child exits

* Tue Mar 07 2000 Nalin Dahyabhai <nalin@redhat.com>
- queue notice messages until we get prompts in userhelper to fix bug #8745

* Fri Feb 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- free trip through the build system

* Tue Jan 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- grab keyboard input focus for dialogs

* Fri Jan 07 2000 Michael K. Johnson <johnsonm@redhat.com>
- The root exploit fix created a bug that only showed up in certain
  circumstances.  Unfortunately, we didn't test in those circumstances...

* Mon Jan 03 2000 Michael K. Johnson <johnsonm@redhat.com>
- fixed local root exploit

* Thu Sep 30 1999 Michael K. Johnson <johnsonm@redhat.com>
- fixed old complex broken gecos parsing, replaced with simple working parsing
- can now blank fields (was broken by previous fix for something else...)

* Tue Sep 21 1999 Michael K. Johnson <johnsonm@redhat.com>
- FALLBACK/RETRY in consolehelper/userhelper
- session management fixed for consolehelper/userhelper SESSION=true
- fix memory leak and failure to close in error condition (#3614)
- fix various bugs where not all elements in userinfo got set

* Mon Sep 20 1999 Michael K. Johnson <johnsonm@redhat.com>
- set $HOME when acting as consolehelper
- rebuild against new pwdb

* Tue Sep 14 1999 Michael K. Johnson <johnsonm@redhat.com>
- honor "owner" flag to mount
- ask for passwords with username

* Tue Jul 06 1999 Bill Nottingham <notting@redhat.com>
- import pam_console wrappers from SysVinit, since they require usermode

* Mon Apr 12 1999 Michael K. Johnson <johnsonm@redhat.com>
- even better check for X availability

* Wed Apr 07 1999 Michael K. Johnson <johnsonm@redhat.com>
- better check for X availability
- center windows to make authentication easier (improve later with
  transients and embedded windows where possible)
- applink -> applnk
- added a little padding, especially important when running without
  a window manager, as happens when running from session manager at
  logout time

* Wed Mar 31 1999 Michael K. Johnson <johnsonm@redhat.com>
- hm, need to be root...

* Fri Mar 19 1999 Michael K. Johnson <johnsonm@redhat.com>
- updated userhelper.8 man page for consolehelper capabilities
- moved from wmconfig to desktop entries

* Thu Mar 18 1999 Michael K. Johnson <johnsonm@redhat.com>
- added consolehelper
- Changed conversation architecture to follow PAM spec

* Wed Mar 17 1999 Bill Nottingham <notting@redhat.com>
- remove gdk_input_remove (causing segfaults)

* Tue Jan 12 1999 Michael K. Johnson <johnsonm@redhat.com>
- fix missing include files

* Mon Oct 12 1998 Cristian Gafton <gafton@redhat.com>
- strip binaries
- use defattr
- fix spec file ( rm -rf $(RPM_BUILD_ROOT) is a stupid thing to do ! )

* Tue Oct 06 1998 Preston Brown <pbrown@redhat.com>
- fixed so that the close button on window managers quits the program properly

* Thu Apr 16 1998 Erik Troan <ewt@redhat.com>
- use gtk-config during build
- added make archive rule to Makefile
- uses a build root

* Fri Nov  7 1997 Otto Hammersmith <otto@redhat.com>
- new version that fixed memory leak bug.

* Mon Nov  3 1997 Otto Hammersmith <otto@redhat.com>
- updated version to fix bugs

* Fri Oct 17 1997 Otto Hammersmith <otto@redhat.com>
- Wrote man pages for userpasswd and userhelper.

* Tue Oct 14 1997 Otto Hammersmith <otto@redhat.com>
- Updated the packages... now includes userpasswd for changing passwords
  and newer versions of usermount and userinfo.  No known bugs or
  misfeatures. 
- Fixed the file list...

* Mon Oct 6 1997 Otto Hammersmith <otto@redhat.com>
- Created the spec file.
