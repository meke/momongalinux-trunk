%global momorel 6

# test builds can made faster and smaller by disabling profiled libraries
# (currently libHSrts_thr_p.a breaks no prof build)
%bcond_without prof
# build users_guide, etc
%bcond_without manual
# include extralibs
%bcond_without extralibs

# experimental
## shared libraries support available in ghc >= 6.11
%bcond_with shared
## include colored html src
%bcond_with hscolour

%global haddock_version 2.4.2

# Fixing packaging problems can be a tremendous pain because it
# generally requires a complete rebuild, which takes hours.  To offset
# the misery, do a complete build once using "rpmbuild -bc", then copy
# your built tree to a directory of the same name suffixed with
# ".built", using "cp -al".  Finally, set this variable, and it will
# copy the already-built tree into place during build instead of
# actually doing the build.
#
# Obviously, this can only work if you leave the build section
# completely untouched between builds.
%global package_debugging 0

Name: ghc
# part of haskell-platform
Version: 6.10.4
Release: %{momorel}m%{?dist}
Summary: Glasgow Haskell Compilation system
# fedora ghc has only been bootstrapped on the following archs:
ExclusiveArch: %{ix86} x86_64 ppc alpha
License: BSD
Group: Development/Languages
Source0: http://www.haskell.org/ghc/dist/%{version}/ghc-%{version}-src.tar.bz2
NoSource: 0
#%%if %{with extralibs}
Source1: http://www.haskell.org/ghc/dist/%{version}/ghc-%{version}-src-extralibs.tar.bz2
NoSource: 1
#%%endif
URL: http://haskell.org/ghc/
Requires: gcc, gmp, libedit-devel >= 2.11
Requires(post): policycoreutils
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: ghc682, ghc681, ghc661, ghc66, haddock09
# introduced for f11 and can be removed for f13:
Obsoletes: haddock < %{haddock_version}, ghc-haddock-devel < %{haddock_version}
Provides: haddock = %{haddock_version}, ghc-haddock-devel = %{haddock_version}
BuildRequires: ghc, happy, sed
BuildRequires: libedit-devel >= 2.11
BuildRequires: gmp-devel >= 5.0.0
%if %{with shared}
# not sure if this is actually needed
BuildRequires: libffi-devel
%endif
%if %{with manual}
BuildRequires: libxslt, docbook-style-xsl
%endif # %%{with manual}
%if %{with hscolour}
BuildRequires: hscolour
%endif

%description
GHC is a state-of-the-art programming suite for Haskell, a purely
functional programming language.  It includes an optimising compiler
generating good code for a variety of platforms, together with an
interactive system for convenient, quick development.  The
distribution includes space and time profiling facilities, a large
collection of libraries, and support for various language
extensions, including concurrency, exceptions, and a foreign language
interface.

%package doc
Summary: Documentation for GHC
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
# for haddock
Requires(posttrans): %{name} = %{version}-%{release}
Obsoletes: ghc-haddock-doc < %{haddock_version}
Provides: ghc-haddock-doc = %{haddock_version}

%description doc
Preformatted documentation for the Glorious Glasgow Haskell
Compilation System (GHC) and its libraries.  It should be installed if
you like to have local access to the documentation in HTML format.

%if %{with shared}
%package libs
Summary: Shared libraries for GHC
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description libs
Shared libraries for Glorious Glasgow Haskell Compilation System
(GHC).  They should be installed to build standalone programs.
%endif

%if %{with prof}
%package prof
Summary: Profiling libraries for GHC
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: ghc682-prof, ghc681-prof, ghc661-prof, ghc66-prof
Obsoletes: ghc-haddock-prof < %{haddock_version}
Provides: ghc-haddock-prof = %{haddock_version}

%description prof
Profiling libraries for Glorious Glasgow Haskell Compilation System
(GHC).  They should be installed when GHC's profiling subsystem is
needed.
%endif

# the debuginfo subpackage is currently empty anyway, so don't generate it
%global debug_package %{nil}

%prep
%setup -q -n %{name}-%{version} %{?with_extralibs:-b1}

%build
# hack for building a local test package quickly from a prebuilt tree 
%if %{package_debugging}
pushd ..
rm -rf %{name}-%{version}
cp -al %{name}-%{version}.built %{name}-%{version}
popd
exit 0
%endif #%%{package_debugging}

%if %{without prof}
echo "GhcLibWays=%{?with_shared:dyn}" >> mk/build.mk
%endif # %%{without prof}

%if %{with manual}
echo "XMLDocWays   = html" >> mk/build.mk
%endif # %%{with manual}

./configure --prefix=%{_prefix} --exec-prefix=%{_exec_prefix} \
  --bindir=%{_bindir} --sbindir=%{_sbindir} --sysconfdir=%{_sysconfdir} \
  --datadir=%{_datadir} --includedir=%{_includedir} --libdir=%{_libdir} \
  --libexecdir=%{_libexecdir} --localstatedir=%{_localstatedir} \
  --sharedstatedir=%{_sharedstatedir} --mandir=%{_mandir} \
  %{?with_shared:--enable-shared}

make %{_smp_mflags}

%if %{with manual}
make %{_smp_mflags} html
%endif # %%{with manual}

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

%if %{with manual}
make DESTDIR=${RPM_BUILD_ROOT} install-docs
%endif # %%{with manual}

%if %{with shared}
mkdir -p ${RPM_BUILD_ROOT}/%{_sysconfdir}/ld.so.conf.d
echo %{_libdir}/%{name}-%{version} > ${RPM_BUILD_ROOT}/%{_sysconfdir}/ld.so.conf.d/ghc-%{_arch}.conf
%endif

SRC_TOP=$PWD
rm -f rpm-*.files
( cd $RPM_BUILD_ROOT
  find .%{_libdir}/%{name}-%{version} \( -type d -fprintf $SRC_TOP/rpm-dir.files "%%%%dir %%p\n" \) -o \( -type f \( -name '*.p_hi' -o -name '*_p.a' \) -fprint $SRC_TOP/rpm-prof.files \) -o \( -not -name 'package.conf*' -fprint $SRC_TOP/rpm-lib.files \)
  find .%{_docdir}/%{name}/* -type d ! -name libraries ! -name src > $SRC_TOP/rpm-doc-dir.files
)

# make paths absolute (filter "./usr" to "/usr")
sed -i -e "s|\.%{_prefix}|%{_prefix}|" rpm-*.files

cat rpm-dir.files rpm-lib.files > rpm-base.files

# these are handled as alternatives
for i in hsc2hs runhaskell; do
  if [ -x ${RPM_BUILD_ROOT}%{_bindir}/$i-ghc ]; then
    rm ${RPM_BUILD_ROOT}%{_bindir}/$i
  else
    mv ${RPM_BUILD_ROOT}%{_bindir}/$i{,-ghc}
  fi
done

%check
# stolen from ghc6/debian/rules:
# Do some very simple tests that the compiler actually works
rm -rf testghc
mkdir testghc
echo 'main = putStrLn "Foo"' > testghc/foo.hs
ghc/stage2-inplace/ghc testghc/foo.hs -o testghc/foo
[ "$(testghc/foo)" = "Foo" ]
rm testghc/*
echo 'main = putStrLn "Foo"' > testghc/foo.hs
ghc/stage2-inplace/ghc testghc/foo.hs -o testghc/foo -O2
[ "$(testghc/foo)" = "Foo" ]
rm testghc/*

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
semanage fcontext -a -t unconfined_execmem_exec_t %{_libdir}/ghc-%{version}/ghc >/dev/null 2>&1 || :
restorecon %{_libdir}/ghc-%{version}/ghc

# Alas, GHC, Hugs, and nhc all come with different set of tools in
# addition to a runFOO:
#
#   * GHC:  hsc2hs
#   * Hugs: hsc2hs, cpphs
#   * nhc:  cpphs
#
# Therefore it is currently not possible to use --slave below to form
# link groups under a single name 'runhaskell'. Either these tools
# should be disentangled from the Haskell implementations, or all
# implementations should have the same set of tools. *sigh*

update-alternatives --install %{_bindir}/runhaskell runhaskell \
  %{_bindir}/runghc 500
update-alternatives --install %{_bindir}/hsc2hs hsc2hs \
  %{_bindir}/hsc2hs-ghc 500

%if %{with shared}
%post libs -p /sbin/ldconfig
%endif

%preun
if [ "$1" = 0 ]; then
  update-alternatives --remove runhaskell %{_bindir}/runghc
  update-alternatives --remove hsc2hs     %{_bindir}/hsc2hs-ghc
fi

%if %{with shared}
%postun libs -p /sbin/ldconfig
%endif

%posttrans doc
# (posttrans to make sure any old documentation has been removed first)
( cd %{_docdir}/ghc/libraries && ./gen_contents_index ) || :

%files -f rpm-base.files
%defattr(-,root,root,-)
%doc ANNOUNCE HACKING LICENSE README
%{_bindir}/*
%if %{with manual}
%{_mandir}/man1/ghc.*
%endif # %%{with manual}
%config(noreplace) %{_libdir}/ghc-%{version}/package.conf

%files doc -f rpm-doc-dir.files
%defattr(-,root,root,-)
%dir %{_docdir}/%{name}
%{_docdir}/%{name}/LICENSE
%if %{with manual}
%{_docdir}/%{name}/index.html
%endif # %%{with manual}
%{_docdir}/%{name}/libraries/gen_contents_index
%{_docdir}/%{name}/libraries/prologue.txt
%dir %{_docdir}/%{name}/libraries
%ghost %{_docdir}/%{name}/libraries/doc-index.html
%ghost %{_docdir}/%{name}/libraries/haddock.css
%ghost %{_docdir}/%{name}/libraries/haddock-util.js
%ghost %{_docdir}/%{name}/libraries/haskell_icon.gif
%ghost %{_docdir}/%{name}/libraries/index.html
%ghost %{_docdir}/%{name}/libraries/minus.gif
%ghost %{_docdir}/%{name}/libraries/plus.gif

%if %{with shared}
%files libs
%defattr(-,root,root,-)
%{_sysconfdir}/ld.so.conf.d/ghc-%{_arch}.conf
%{_libdir}/libHS*-ghc%{version}.so
%endif

%if %{with prof}
%files prof -f rpm-prof.files
%defattr(-,root,root,-)
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.10.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.10.4-5m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.10.4-4m)
- rebuild against gmp-5.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.10.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.10.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.10.4-1m)
- update to 6.10.4

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.10.3-2m)
- unprovide macros.ghc which is now provided by ghc-rpm-macros

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.10.3-1m)
- update to 6.10.3

* Mon May  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.10.1-1m)
- merge Fedora 6.10.1-13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.8.2-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8.2-4m)
- %%NoSource -> NoSource

* Fri Dec 28 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8.2-3m)
- no %%configure macro (build failure on x86_64)

* Tue Dec 25 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8.2-2m)
- remove BuildRequires: happy
- not install man page when build_doc == 0

* Mon Dec 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8.2-1m)
- update to 6.8.2
- sync with Fedora devel
-- * Tue Dec 12 2007 Bryan O'Sullivan <bos@serpentine.com> - 6.8.2-1
-- - Update to 6.8.2
-- 
-- * Fri Nov 23 2007 Bryan O'Sullivan <bos@serpentine.com> - 6.8.1-2
-- - Exclude alpha
-- 
-- * Sun Nov  4 2007 Michel Salim <michel.sylvan@gmail.com> - 6.8.1-1
-- - Update to 6.8.1
-- 
-- * Sat Sep 29 2007 Bryan O'Sullivan <bos@serpentine.com> - 6.8.0.20070928-2
-- - add happy to BuildRequires
-- 
-- * Sat Sep 29 2007 Bryan O'Sullivan <bos@serpentine.com> - 6.8.0.20070928-1
-- - prepare for GHC 6.8.1 by building a release candidate snapshot
-- 
-- * Thu May 10 2007 Bryan O'Sullivan <bos@serpentine.com> - 6.6.1-3
-- - install man page for ghc
-- 
-- * Thu May 10 2007 Bryan O'Sullivan <bos@serpentine.com> - 6.6.1-2
-- - exclude ppc64 for now, due to lack of time to bootstrap
-- 
-- * Wed May  9 2007 Bryan O'Sullivan <bos@serpentine.com> - 6.6.1-1
-- - update to 6.6.1 release
-- 
-- * Mon Jan 22 2007 Jens Petersen <petersen@redhat.com> - 6.6-2
-- - remove truncated duplicate Typeable.h header in network package
--   (Bryan O'Sullivan, #222865)

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.6-3m) 
- revise spec
-- replace __spec_install_post with __os_install_post

* Mon Jul 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.6-2m) 
- change DIR_DOCBOOK_XSL in %%build

* Fri Nov  3 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.6-1m)
- update to 6.6
- sync with Fedora Extras devel
-- * Fri Nov  3 2006 Jens Petersen <petersen@redhat.com> - 6.6-1
-- - update to 6.6 release
-- - buildrequire haddock >= 0.8
-- - fix summary of ghcver package (Michel Salim, #209574)

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.4.2-2m)
- rebuild against freeglut

* Thu Sep  7 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.4.2-1m)
- import from Fedora Extras devel

* Sat Apr 29 2006 Jens Petersen <petersen@redhat.com> - 6.4.2-2.fc6
- buildrequire libXt-devel so that the X11 package and deps get built
  (Garrett Mitchener, #190201)

* Thu Apr 20 2006 Jens Petersen <petersen@redhat.com> - 6.4.2-1.fc6
- update to 6.4.2 release

* Thu Mar  2 2006 Jens Petersen <petersen@redhat.com> - 6.4.1-3.fc5
- buildrequire libX11-devel instead of xorg-x11-devel (Kevin Fenzi, #181024)
- make ghc-doc require ghc (Michel Salim, #180449)

* Tue Oct 11 2005 Jens Petersen <petersen@redhat.com> - 6.4.1-2.fc5
- turn on build_doc since haddock is now in Extras
- no longer specify ghc version to build with (Ville Skytta, #170176)

* Tue Sep 20 2005 Jens Petersen <petersen@redhat.com> - 6.4.1-1.fc5
- 6.4.1 release
  - the following patches are now upstream: ghc-6.4-powerpc.patch,
    rts-GCCompact.h-x86_64.patch, ghc-6.4-dsforeign-x86_64-1097471.patch,
    ghc-6.4-rts-adjustor-x86_64-1097471.patch
  - builds with gcc4 so drop %%_with_gcc32
  - x86_64 build restrictions (no ghci and split objects) no longer apply

* Tue May 31 2005 Jens Petersen <petersen@redhat.com>
- add %%dist to release

* Thu May 12 2005 Jens Petersen <petersen@redhat.com> - 6.4-8
- initial import into Fedora Extras

* Thu May 12 2005 Jens Petersen <petersen@haskell.org>
- add build_prof and build_doc switches for -doc and -prof subpackages
- add _with_gcc32 switch since ghc-6.4 doesn't build with gcc-4.0

* Wed May 11 2005 Jens Petersen <petersen@haskell.org> - 6.4-7
- make package relocatable (ghc#1084122)
  - add post install scripts to replace prefix in driver scripts
- buildrequire libxslt and docbook-style-xsl instead of docbook-utils and flex

* Fri May  6 2005 Jens Petersen <petersen@haskell.org> - 6.4-6
- add ghc-6.4-dsforeign-x86_64-1097471.patch and
  ghc-6.4-rts-adjustor-x86_64-1097471.patch from trunk to hopefully fix
  ffi support on x86_64 (Simon Marlow, ghc#1097471)
- use XMLDocWays instead of SGMLDocWays to build documentation fully

* Mon May  2 2005 Jens Petersen <petersen@haskell.org> - 6.4-5
- add rts-GCCompact.h-x86_64.patch to fix GC issue on x86_64 (Simon Marlow)

* Thu Mar 17 2005 Jens Petersen <petersen@haskell.org> - 6.4-4
- add ghc-6.4-powerpc.patch (Ryan Lortie)
- disable building interpreter rather than install and delete on x86_64

* Wed Mar 16 2005 Jens Petersen <petersen@haskell.org> - 6.4-3
- make ghc require ghcver of same ver-rel
- on x86_64 remove ghci for now since it doesn't work and all .o files

* Tue Mar 15 2005 Jens Petersen <petersen@haskell.org> - 6.4-2
- ghc requires ghcver (Amanda Clare)

* Sat Mar 12 2005 Jens Petersen <petersen@haskell.org> - 6.4-1
- 6.4 release
  - x86_64 build no longer unregisterised
- use sed instead of perl to tidy filelists
- buildrequire ghc64 instead of ghc-6.4
- no epoch for ghc64-prof's ghc64 requirement
- install docs directly in docdir

* Fri Jan 21 2005 Jens Petersen <petersen@haskell.org> - 6.2.2-2
- add x86_64 port
  - build unregistered and without splitobjs
  - specify libdir to configure and install
- rename ghc-prof to ghcXYZ-prof, which obsoletes ghc-prof

* Mon Dec  6 2004 Jens Petersen <petersen@haskell.org> - 6.2.2-1
- move ghc requires to ghcXYZ

* Wed Nov 24 2004 Jens Petersen <petersen@haskell.org> - 6.2.2-0.fdr.1
- ghc622
  - provide ghc = %%version
- require gcc, gmp-devel and readline-devel

* Fri Oct 15 2004 Gerard Milmeister <gemi@bluewin.ch> - 6.2.2-0.fdr.1
- New Version 6.2.2

* Mon Mar 22 2004 Gerard Milmeister <gemi@bluewin.ch> - 6.2.1-0.fdr.1
- New Version 6.2.1

* Tue Dec 16 2003 Gerard Milmeister <gemi@bluewin.ch> - 6.2-0.fdr.1
- New Version 6.2

* Tue Dec 16 2003 Gerard Milmeister <gemi@bluewin.ch> - 6.0.1-0.fdr.3
- A few minor specfile tweaks

* Mon Dec 15 2003 Gerard Milmeister <gemi@bluewin.ch> - 6.0.1-0.fdr.2
- Different file list generation

* Mon Oct 20 2003 Gerard Milmeister <gemi@bluewin.ch> - 6.0.1-0.fdr.1
- First Fedora release
- Added generated html docs, so that haddock is not needed

* Wed Sep 26 2001 Manuel Chakravarty
- small changes for 5.04

* Wed Sep 26 2001 Manuel Chakravarty
- split documentation off into a separate package
- adapt to new docbook setup in RH7.1

* Mon Apr 16 2001 Manuel Chakravarty
- revised for 5.00
- also runs autoconf automagically if no ./configure found

* Thu Jun 22 2000 Sven Panne
- removed explicit usage of hslibs/docs, it belongs to ghc/docs/set

* Sun Apr 23 2000 Manuel Chakravarty
- revised for ghc 4.07; added suggestions from Pixel <pixel@mandrakesoft.com>
- added profiling package

* Tue Dec 7 1999 Manuel Chakravarty
- version for use from CVS

* Thu Sep 16 1999 Manuel Chakravarty
- modified for GHC 4.04, patchlevel 1 (no more 62 tuple stuff); minimises use
  of patch files - instead emits a build.mk on-the-fly

* Sat Jul 31 1999 Manuel Chakravarty
- modified for GHC 4.04

* Wed Jun 30 1999 Manuel Chakravarty
- some more improvements from vbzoli

* Fri Feb 26 1999 Manuel Chakravarty
- modified for GHC 4.02

* Thu Dec 24 1998 Zoltan Vorosbaranyi 
- added BuildRoot
- files located in /usr/local/bin, /usr/local/lib moved to /usr/bin, /usr/lib

* Tue Jul 28 1998 Manuel Chakravarty
- original version
