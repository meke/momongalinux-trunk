%global momorel 2
%global         kdever 4.5.95
%global         qtver 4.7.1
%global         qtrel 1m
%global         cmakever 2.6.4
%global         cmakerel 1m
%global         src_name lyricsplasmoid

Name:           kde-plasma-lyrics
Version:        0.5
Release:        %{momorel}m%{?dist}
Summary:        A Plamoid Widget to see the lyrics of the current playing song.
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://kde-look.org/content/show.php/Lyrics+Plasmoid?content=135292
Source0:        http://dl.sourceforge.net/project/%{src_name}/releases/%{version}/%{src_name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}

%description
A Plamoid Widget to see the lyrics of the current playing song.

%prep
%setup -q -n %{src_name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS LICENSE README
%{_kde4_libdir}/kde4/plasma_applet_lyrics.so
%{_kde4_iconsdir}/hicolor/*/apps/lyrics-plasmoid.png
%{_kde4_datadir}/kde4/services/plasma-applet-lyrics.desktop
%{_kde4_datadir}/locale/es/LC_MESSAGES/plasma_applet_lyrics-plasmoid.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-2m)
- rebuild for new GCC 4.6

* Thu Feb  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-2m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- update to 0.3

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-1m)
- initial build for Momonga Linux.
