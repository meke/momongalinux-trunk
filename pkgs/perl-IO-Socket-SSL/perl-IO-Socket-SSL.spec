%global         momorel 1

Name:           perl-IO-Socket-SSL
Version:        1.994
Epoch:          1
Release:        %{momorel}m%{?dist}
Summary:        Nearly transparent SSL encapsulation for IO::Socket::INET
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/IO-Socket-SSL/
Source0:        http://www.cpan.org/authors/id/S/SU/SULLR/IO-Socket-SSL-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO-Socket-IP
BuildRequires:  perl-Net-SSLeay >= 1.45-2m
BuildRequires:  perl-Scalar-Util
Requires:       perl-Net-SSLeay >= 1.45-2m
Requires:       perl-Scalar-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module is a true drop-in replacement for IO::Socket::INET that uses
SSL to encrypt data before it is transferred to a remote server or client.
IO::Socket::SSL supports all the extra features that one needs to write a
full-featured SSL client or server application: multiple SSL contexts,
cipher selection, certificate verification, and SSL version selection. As
an extra bonus, it works perfectly with mod_perl.

%prep
%setup -q -n IO-Socket-SSL-%{version}

%build
echo "y" | %{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BUGS Changes README README.Win32
%{perl_vendorlib}/IO/Socket/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.994-1m)
- rebuild against perl-5.20.0
- update to 1.994

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.983-1m)
- update to 1.983

* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.982-1m)
- update to 1.982

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.981-1m)
- update to 1.981

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.978-1m)
- update to 1.978

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.975-1m)
- update to 1.975

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.971-1m)
- update to 1.971

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.970-1m)
- update to 1.970

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.967-1m)
- update to 1.967
- rebuild against perl-5.18.2

* Sun Dec 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.962-1m)
- update to 1.962

* Mon Nov 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.957-1m)
- update to 1.957

* Sat Oct 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.955-1m)
- update to 1.955

* Mon Sep 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.954-1m)
- update to 1.954

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.953-2m)
- rebuild against perl-5.18.1

* Thu Jul 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.953-1m)
- update to 1.953

* Sat Jul 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.952-1m)
- update to 1.952

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.951-1m)
- update to 1.951

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.94-1m)
- update to 1.94

* Fri May 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.92-1m)
- update to 1.92

* Thu May 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.91-1m)
- update to 1.91

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.89-2m)
- rebuild against perl-5.18.0

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.89-1m)
- update to 1.89

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.88-1m)
- update to 1.88

* Thu Apr 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.87-1m)
- update to 1.87

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.86-1m)
- update to 1.86

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.85-1m)
- update to 1.85

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.84-2m)
- rebuild against perl-5.16.3

* Sat Feb 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.84-1m)
- update to 1.84

* Thu Feb 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.831-1m)
- update to 1.831

* Mon Feb  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.83-1m)
- update to 1.83

* Mon Jan 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.82-1m)
- update to 1.82

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.81-1m)
- update to 1.81

* Fri Nov 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.80-1m)
- update to 1.80

* Mon Nov 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.79-1m)
- update to 1.79

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-2m)
- rebuild against perl-5.16.2

* Fri Oct  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-1m)
- update to 1.77

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.76-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.76-1m)
- update to 1.76
- rebuild against perl-5.16.0

* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.62-1m)
- update to 1.62

* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.61-1m)
- update to 1.61

* Wed Mar 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.60-1m)
- update to 1.60

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.59-1m)
- update to 1.59

* Mon Dec 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.53-1m)
- update to 1.53

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Fri Oct 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.49-1m)
- update to 1.49

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-1m)
- update to 1.48

* Sat Oct 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.47-1m)
- update to 1.47

* Tue Oct 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-1m)
- update to 1.46

* Thu Oct 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-1m)
- update to 1.45

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-2m)
- rebuild against perl-5.14.1

* Fri May 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.43-1m)
- update to 1.43

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- update to 1.41

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-2m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.39-2m)
- rebuild for new GCC 4.6

* Thu Mar  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-1m)
- update to 1.39

* Tue Jan 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Mon Dec  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.34-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.33-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated
- Specfile re-generated by cpanspec 1.78.

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.31-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Wed Jul  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- [SECURITY] CVE-2009-3024
- update to 1.26

* Sat Apr  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Tue Feb 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Fri Nov 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Tue Oct 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Sun Sep 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Fri Aug 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13-2m)
- rebuild against gcc43

* Tue Jan 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Fri Oct 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Thu Oct 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Wed Oct 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Fri Sep 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.05-2m)
- use vendor

* Fri Apr 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Fri Mar 30 2007 NARITA Koichi <pulasr@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Thu Mar  8 2007 NARITA Koichi <pulasr@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sun Sep 17 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.01-1m)
- update to 1.01

* Fri Sep  1 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.999-1m)
- update to 0.999

* Sun Aug  6 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.997-1m)
- update to 0.997

* Sat Jul 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.994-1m)
- update to 0.994

* Sun Jun 11 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.97-3m)
- modify %%files

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.97-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.97-1m)
- update to 0.97

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.96-1m)
- version up to 0.96
- built against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.95-4m)
- rebuild against perl-5.8.5
- rebuild against perl-Net-SSLeay 1.21-8m
- rebuild against perl-HTML-Parser >= 3.33-4m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.95-3m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.95-2m)
- revised spec for rpm 4.2.

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.95-1m)

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.81-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.81-3m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.81-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.81-1m)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.80-5m)
- remove BuildRequires: gcc2.95.3

* Fri Mar  1 2002 Shingo Akagaki <dora@kondara.org>
- (0.80-4k)
- noarch

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (0.80-2k)
- create
