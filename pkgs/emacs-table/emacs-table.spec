%global momorel 34
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Table editing utility for Emacs
Name: emacs-table
Version: 1.5.54
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
URL: http://table.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/table/table-%{version}.el.gz 
NoSource: 0
Source1: table.el
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: table-emacs
Obsoletes: table-xemacs

Obsoletes: elisp-table
Provides: elisp-table

%description
This package provides text based table creation and editing
feature. With this package Emacsen is capable of editing tables that
are embedded inside a text document, the feature similar to the ones
seen in modern WYSIWYG word processors. A table is a rectangular text
area consisting from a surrounding frame and content inside the
frame. The content is usually subdivided into multiple rectangular
cells. Once a table is recognized, editing operation inside a table
cell is confined into that specific cell's rectangular area. This
means that typing and deleting characters inside a cell do not affect
any outside text but introduces appropriate formatting only to the
sell contents. If necessary for accomodating added text in the cell,
the cell automatically grows vertically and/or horizontally. The
package uses no major mode nor minor mode for its implementation
because the subject text is localized within a buffer. Therefore the
special behaviors inside a table cells are implemented by using
local-map text property instead of buffer wide mode-map. Also some
commonly used functions are advised so that they act specially inside
a table cell.
#'

%prep
%setup -qTc

gzip -dc %{SOURCE0} > table.el

%build
emacs -q -batch -f batch-byte-compile table.el

%install
rm -rf %{buildroot}%{sitepdir}
rm -rf %{buildroot}

mkdir -p %{buildroot}%{e_sitedir}/table
mkdir -p %{buildroot}%{sitepdir}/lisp/table

install -m 0644 table.el %{buildroot}%{e_sitedir}/table/table.el
install -m 0644 table.elc %{buildroot}%{e_sitedir}/table/table.elc

mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/config-sample/%{name}

%clean
rm -rf %{buildroot}%{sitepdir}
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_datadir}/config-sample/%{name}
%{_datadir}/config-sample/%{name}/*
%dir %{e_sitedir}/table
%{e_sitedir}/table/*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.54-34m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.54-33m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.54-32m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.54-31m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-30m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.54-29m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.54-28m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-27m)
- rebuild against emacs-23.2

* Fri Apr 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-26m)
- add rm -rf %%{buildroot}%{%sitepdir}

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-25m)
- merge table-emacs to elisp-table
- kill table-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-24m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-23m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-22m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-21m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-20m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-19m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-18m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-17m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-16m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-15m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-14m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-13m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-12m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.54-11m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.54-10m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.54-2m)
- rebuild against emacs-22.0.90

* Sun Jul 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.5.54-1m)
- up to 1.5.54
- telia -> jaist

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.5.48-6m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.48-5m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.5.48-4m)
- rebuild against emacs-21.3.50

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.48-3m)
- rebuild against emacs-21.3

* Thu Jun 13 2002 Kenta MURATA <muraken2@nifty.com>
- (1.5.48-2k)
- first spec file.
