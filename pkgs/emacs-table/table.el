;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; table configuration sample by muraken <muraken2@nifty.com>
;;
;; Please copy & paste to .emacsen
;;

(require 'table)

;; to load table on text-mode
(add-hook 'text-mode-hook 'table-recognize)

