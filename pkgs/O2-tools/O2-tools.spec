%global momorel 5

Summary: OCR library
Name:    O2-tools
Version: 2.00
Release: %{momorel}m%{?dist}
License: MIT/X
Group:   Applications/Text
URL:     http://www.imglab.org/p/O2/

Source0: http://www.imglab.org/p/O2/archives/%{name}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This Programs and libraries is image processing stuff designed especially
suitable for document image processing.
  
%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
# exclude is used for avoid conflict with netpbm-progs
%exclude %{_bindir}/bmptopnm
%exclude %{_mandir}/man1/bmptopnm*
%{_mandir}/man1/*.1*
%{_mandir}/man4/*.4*
%{_includedir}/*
%{_libdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.00-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.00-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.00-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.00-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 18 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.00-1m)
- initial package for Momonga
