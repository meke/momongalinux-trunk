%global         momorel 1

Name:           xterm
Summary:        The terminal programs for X
Version:        303
Release:        %{momorel}m%{?dist}
License:        MIT/X
Group:          User Interface/Desktops
Source0:        ftp://invisible-island.net/pub/%{name}/%{name}-%{version}.tgz
NoSource:       0
Source1:        ftp://invisible-island.net/%{name}/16colors.txt
URL:            http://dickey.his.com/xterm/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  fontconfig-devel
BuildRequires:  freetype-devel
BuildRequires:  libICE-devel
BuildRequires:  libSM-devel
BuildRequires:  libX11-devel
BuildRequires:  libXaw-devel
BuildRequires:  libXext-devel
BuildRequires:  libXft-devel
BuildRequires:  libXmu-devel
BuildRequires:  libXrender-devel
BuildRequires:  libXt-devel
Requires: 	xorg-x11-luit

%description
The xterm program is a terminal emulator for the X Window System. It
provides DEC VT102 and Tektronix 4014 compatible terminals for
programs that can't use the window system directly.
#'

This version implements ISO/ANSI colors using the "new" color model
(i.e., background color erase). It also implements most of the control
sequences for VT220.

%prep
%setup -q

%build
%configure --enable-luit \
           --enable-256-color \
           --enable-warnings \
           --enable-wide-chars \
           --with-app-defaults=%{_datadir}/X11/app-defaults/ \
           --with-icondir=%{_datadir}/pixmaps \
           --with-utempter \
           --with-tty-group=tty \
           --disable-setgid \
           --disable-full-tgetent \
           LIBS="-lfontconfig -lXmu"

%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

cp -fp %{SOURCE1} 16colors.txt

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc xterm.log.html ctlseqs.txt MANIFEST README README.i18n 16colors.txt
%{_bindir}/resize
%{_bindir}/koi8rxterm
%{_bindir}/uxterm
%{_bindir}/xterm
%{_datadir}/X11/app-defaults/UXTerm
%{_datadir}/X11/app-defaults/UXTerm-color
%{_datadir}/X11/app-defaults/XTerm
%{_datadir}/X11/app-defaults/XTerm-color
%{_datadir}/X11/app-defaults/KOI8RXTerm
%{_datadir}/X11/app-defaults/KOI8RXTerm-color
%{_datadir}/pixmaps/xterm*.xpm
%{_datadir}/pixmaps/filled-xterm*.xpm
%{_datadir}/pixmaps/mini.xterm*.xpm
%{_mandir}/man1/resize.1.*
%{_mandir}/man1/koi8rxterm.1.*
%{_mandir}/man1/uxterm.1.*
%{_mandir}/man1/xterm.1.*

%changelog
* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (303-1m)
- update to 303

* Tue Dec 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (300-1m)
- update to 300

* Sun Sep 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (297-1m)
- update to 297

* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (293-1m)
- update to 293

* Sat May  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (292-1m)
- update to 292

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (291-1m)
- update to 291

* Tue Feb 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (290-1m)
- update to 290

* Sun Jan 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (288-1m)
- update to 288

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (287-1m)
- update to 287

* Sat Nov 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (286-1m)
- update to 286

* Mon Oct 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (284-1m)
- update to 284

* Fri Oct 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (283-1m)
- update to 283

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (278-1m)
- update to 278

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (277-1m)
- update to 277

* Sat Dec  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (276-1m)
- update to 276

* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (275-1m)
- update to 275

* Sun Aug 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (273-1m)
- update to 273

* Fri Jul 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (271-1m)
- update to 271

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (270-1m)
- update to 270

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (269-2m)
- rebuild for new GCC 4.6

* Sun Mar  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (269-1m)
- update to 269

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (267-1m)
- update to 267

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (266-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (266-1m)
- update to 266

* Tue Oct  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (262-1m)
- update to 262

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (261-3m)
- full rebuild for mo7 release

* Sat Aug 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (261-2m)
- add Requires: xorg-x11-luit

* Sat Jul  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (261-1m)
- update to 261

* Tue Jun 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (260-1m)
- update to 260

* Sat Jun 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (259-1m)
- update to 259

* Sun May 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (258-1m)
- update to 258

* Thu Apr  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (256-1m)
- update to 256

* Tue Jan 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (255-1m)
- update to 255

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (254-1m)
- update to 254

* Wed Dec 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (253-1m)
- update to 253

* Wed Dec  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (252-1m)
- update to 252

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (251-1m)
- update to 251

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (248-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (248-1m)
- update to 248

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (247-1m)
- update to 247

* Thu May  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (243-1m)
- update to 243

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (239-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (239-1m)
- update to 239

* Thu Jan  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (238-1m)
- [SECURITY] CVE-2008-2383
- update to 238

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (237-1m)
- update to 237

* Thu May 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (235-1m)
- update to 235

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (234-2m)
- rebuild against gcc43

* Sat Mar 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (234-1m)
- update to 234

* Wed Mar 19 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (231-3m)
- delete BuildPrereq: libtermcap-devel

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (231-2m)
- %%NoSource -> NoSource

* Mon Jan  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (231-1m)
- update to 231

* Fri Aug 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (229-1m)
- update to 229
- [SECURITY] CVE-2007-2797 add --with-tty-group=tty as a configure option

* Sun Apr  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (225-1m)
- update to 225

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (223-2m)
- BuildPrereq: freetype2-devel -> freetype-devel

* Sat Dec 16 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (223-1m)
- update to 223

* Sun Sep 10 2006 Nishio Futosh <futoshi@momonga-linux.org>
- (219-1m)
- update to 219
- merge OBSOLETE (changelog)

* Sun Jul 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (215-1m)
- update to 215

* Sun May 21 2006 Nishio Futosh <futoshi@momonga-linux.org>
- (213-2m)
- Source -> NoSource

* Sun May 21 2006 Nishio Futosh <futoshi@momonga-linux.org>
- (213-1m)
- initial build (no tsumori datta)

* Tue Jan 11 2005 zunda <zunda at freeeshell.org>
- (651-11m)
- BuildRequires and Requires insted of xorg-x11-devel, xorg-x11,
  insted of XFree86-devel, XFree86

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (651-10m)
- add Patch1: xterm-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Sun Oct 26 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (651-9m)
- modified XTerm.ad

* Wed Feb 27 2002 Tsutomu Yasuda <tom@kondara.org>
- (651-8k)
- The owner and permission of the xterm command
  It changed into root.utmp 2711.

* Wed Jan 30 2002 Motonobu Ichimura <famao@kondara.org>
- (651-6k)
- add more ECMA-48 SGR sequence support

* Mon Nov 12 2001 Motonobu Ichimura <famao@kondara.org>
- (651-4k)
- change default font setting

* Fri Nov 09 2001 Motonobu Ichimura <famao@kondara.org>
- (651-2k)
- moved to CSI xterm
- bump up to 651

* Fri May 11 2001 Akira Higuchi <a@kondara.org>
-(148-8k)
- use efonts-unicode

* Wed May  9 2001 Akira Higuchi <a@kondara.org>
-(148-7k)
- a minor change in default font names

* Mon Apr 16 2001 Akira Higuchi <a@kondara.org>
-(148-5k)
- reverted to version 148
- re-enabled the kondara patch
- disabled NoSource

* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- version 153
- baatari build TT

* Sat Nov 18 2000 Akira Higuchi <a@kondara.org>
- initial package

