%global momorel 10

Name:		libbinio
Version:	1.4
Release:	%{momorel}m%{?dist}
Summary:	A software library for binary I/O classes in C++
URL:		http://libbinio.sourceforge.net/
Group:		System Environment/Libraries
Source:		http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0:		libbinio-1.4-texinfo.patch
Patch1:		libbinio-1.4-pkgconfigurl.patch
Patch2:		libbinio-1.4-gcc44.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License:	LGPL

%description
This binary I/O stream class library presents a platform-independent
way to access binary data streams in C++. The library is hardware 
independent in the form that it transparently converts between the 
different forms of machine-internal binary data representation.
It further employs no special I/O protocol and can be used on
arbitrary binary data sources.

%package devel
Summary:        Development files for libbinio
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}
BuildRequires:	texinfo

%description devel
This package contains development files for the libbinio binary
data stream class for C++.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1 -b .gcc44~

%build
autoreconf -fiv
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%makeinstall
# Remove libtool archive remnants
rm -f %{buildroot}%{_libdir}/*.la
# Remove doc "dir"
rm -rf %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/libbinio.info %{_infodir}/dir || :

%preun devel
if [ $1 = 0 ]; then
# uninstall the info reference in the dir file
/sbin/install-info --delete %{_infodir}/libbinio.info %{_infodir}/dir || :
fi

%files
%defattr(-, root, root)
%{_libdir}/*.so.*
%doc AUTHORS README COPYING INSTALL INSTALL.unix NEWS TODO

%files devel
%defattr(-, root, root)
%dir %{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/%{name}/*.h
%{_infodir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (1.4-6m)
- add autoreconf (build fix)

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-5m)
- do not specify info file compression format

* Sat Mar  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-2m)
- rebuild against gcc43

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4-1m)
- import to Momonga from Fedora

* Sat Feb 9 2008 Linus Walleij <triad@df.lth.se> 1.4-9
- Rebuild for GCC 4.3.

* Fri Jan 18 2008 Linus Walleij <triad@df.lth.se> 1.4-8
- New glibc ABI wants a rebuild.

* Fri Aug 17 2007 Linus Walleij <triad@df.lth.se> 1.4-7
- License field update from LGPL to LGPLv2+

* Mon Aug 28 2006 Linus Walleij <triad@df.lth.se> 1.4-6
- Rebuild for Fedora Extras 6.

* Tue Feb 14 2006 Linus Walleij <triad@df.lth.se> 1.4-5
- Rebuild for Fedora Extras 5.

* Thu Oct 6 2005 Linus Walleij <triad@df.lth.se> 1.4-4
- BuildRequire texinfo to get makeinfo.

* Sat Oct 1 2005 Linus Walleij <triad@df.lth.se> 1.4-3
- Conforming pkg-config for FC4 and texinfo bug patch.

* Sun Sep 18 2005 Linus Walleij <triad@df.lth.se> 1.4-2
- More minor corrections.

* Sun Sep 18 2005 Linus Walleij <triad@df.lth.se> 1.4-1
- Upstream fixed header problem.

* Fri Sep 16 2005 Linus Walleij <triad@df.lth.se> 1.3-4
- Trying to resolve dispute about header subdirs.

* Thu Sep 15 2005 Linus Walleij <triad@df.lth.se> 1.3-3
- Reverted some and added some after comments from Ville Skytta.

* Thu Sep 15 2005 Linus Walleij <triad@df.lth.se> 1.3-2
- Fixed some points raised by Ralf Corsepius.

* Wed Sep 14 2005 Linus Walleij <triad@df.lth.se> 1.3-1
- First try at a libbinio RPM.
