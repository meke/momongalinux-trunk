%global momorel 18

Summary: A GTK+ based development library to replace newt.
Name: gnewt
Version: 0.06
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://oksid.ch/
Source0: http://oksid.ch/gnewt/gnewt_%{version}-1.tar.gz
Patch0: gnewt-builderr.patch
Patch1: gnewt-0.06-gcc34.patch
Patch100: gnewt-0.06-varargs.patch
Requires: gtk2-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automake14
BuildRequires: coreutils
BuildRequires: libtool

%package devel
Summary: gNewt windowing toolkit development files.
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Group: Development/Libraries

%description
gNewt is a replacment library for the text mode library newt.

%description devel
The gnewt-devel package contains the header files and libraries
necessary for developing applications which use gnewt. 

Install gnewt-devel if you want to develop applications which will
use gnewt.

%prep
%setup -q -n gnewt-0.06
%patch0 -p1
%patch1 -p1
%ifarch ia64 alpha sparc sparc64 mips mipsel
%patch100 -p1
%endif

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

%build
./configure --prefix=%{_prefix} --libdir=%{_libdir}

make %{?_smp_mflags} AUTOMAKE=automake-1.4

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_prefix}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.06-16m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.06-15m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.06-13m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-12m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.06-11m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6-10m)
- delete libtool library

* Sun Jul  4 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.6-9m)
- No NoSource

* Tue Oct 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6-8m)
- enable ia64, alpha, sparc, mipsel

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6-7m)
- enable x86_64.

* Mon Oct 18 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6-6m)
- add patch2 for gcc 3.4.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6-5m)
- revised spec for enabling rpm 4.2.

*Thu May 23 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.6-4k)
- fix compile error (gcc-3.1)

* Fri Apr 19 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.6-2k)
  Kondarized

*  Fri Dec 31 1999 O'ksi'D <nickasil@oksid.linuxbox.com>
- version 0.02 released.

* Mon Nov  1  1999 O'ksi'D <nickasil@bluewin.ch>
- source code is clean, 0.01 release

* Mon Oct  10 1999 O'ksi'D <nickasil@bluewin.ch> 
- first beta release

%files
%defattr (-,root,root)
%doc CHANGES COPYING
%{_libdir}/libgnewt-0.6.so.*

%files devel
%defattr (-,root,root)
%{_includedir}/gnewt.h
%{_libdir}/libgnewt.a
%{_libdir}/libgnewt.so
