%global         momorel 9

Name:           perl-ParseLex
Version:        2.21
Release:        %{momorel}m%{?dist}
Summary:        ParseLex Perl module
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/ParseLex/
Source0:        http://www.cpan.org/authors/id/P/PS/PSCUST/ParseLex-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Slurp
BuildRequires:  perl-ParseTemplate >= 3.04
BuildRequires:  perl-Test-Simple >= 0.96
Requires:       perl-File-Slurp
Requires:       perl-ParseTemplate >= 3.04
Requires:       perl-Test-Simple >= 0.96
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
o-o-o-o-o-o-o-o-o-o-o-o-o o                         o o   Parse::Lex - 2.14
o o    Parse::LexEvent - 1.00   o o    Parse::Token - 2.15      o o
Parse::Template - 0.32   o o   Parse::YYLex - 0.91     o o
o o-o-o-o-o-o-o-o-o-o-o-o-o

%prep
%setup -q -n ParseLex-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes META.json README VERSION
%{perl_vendorlib}/Parse/ALex.pm
%{perl_vendorlib}/Parse/CLex.pm
%{perl_vendorlib}/Parse/Lex.pm
%{perl_vendorlib}/Parse/LexEvent.pm
%{perl_vendorlib}/Parse/Token-t.pm
%{perl_vendorlib}/Parse/Token.pm
%{perl_vendorlib}/Parse/Trace.pm
%{perl_vendorlib}/Parse/YYLex.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-2m)
- rebuild against perl-5.16.0

* Sat Dec 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-1m)
- update to 2.21

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-7m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-1m)
- update to 2.20

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.19-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19-2m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19-1m)
- rebuild against perl-5.12.0
- update to 2.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-3m)
- rebuild against perl-5.10.1

* Thu Jul 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15-2m)
- fix conflicts files

* Wed Jul 22 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15-1m)
- import from Fedora to Momonga for publican

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.15-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Apr 7 2008  Jeff Fearn <jfearn@redhat.com> 2.15-12
- Need Requires for EPEL

* Fri Jan 18 2008 Jeff Fearn <jfearn@redhat.com> 2.15-11
- iconv in the prep, beer on the way

* Thu Jan 17 2008 Jeff Fearn <jfearn@redhat.com> 2.15-10
- Fixed unwanted Provides Filter
- Consistant use of macros
- Better summary

* Wed Jan 16 2008 Jeff Fearn <jfearn@redhat.com> 2.15-9
- Add missing BuildRequires

* Wed Jan 16 2008 Jeff Fearn <jfearn@redhat.com> 2.15-8
- Changed Development/Languages to Development/Libraries
- Fixed test
- Removed useless-explicit-provides
- Converted Changes to utf-8

* Tue Jan 08 2008 Jeff Fearn <jfearn@redhat.com> 2.15-7
- Remove %%doc from man files, used glob
- Simplify Parse in filelist
- Simplify %%clean
- Remove OPTIMIZE setting from make call
- Change buildroot to fedora style
- Remove unused defines

* Mon Jan 07 2008 Jeff Fearn <jfearn@redhat.com> 2.15-6
- Tidy up spec

* Mon Dec 10 2007 Jeff Fearn <jfearn@redhat.com> 2.15-5
- noarch FTW
- add dist to release

* Tue Apr 10 2007 ttrinks@redhat.com
- Rebuilt for RHEL5
- Changed arch from noarch to i386

* Mon Jul 31 2006 mschick@redhat.com
- Tagged for e-s-o repo
- Rebuilt for RHEL4 

* Thu Sep 25 2003 pgampe@redhat.com
- Patch broken syntax in upstream Template.pm
