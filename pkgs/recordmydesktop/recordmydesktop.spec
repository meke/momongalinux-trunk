%global momorel 7

Name:           recordmydesktop
Version:        0.3.8.1
Release:        %{momorel}m%{?dist}
Summary:        Desktop session recorder with audio and video

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://recordmydesktop.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/recordmydesktop/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         recordmydesktop-0.3.8.1-xorg75.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libXdamage-devel, libSM-devel
BuildRequires:  libXext-devel
BuildRequires:  alsa-lib-devel, zlib-devel
BuildRequires:  libtheora-devel, libvorbis-devel, jack-devel


%description
recordMyDesktop is a desktop session recorder for linux that attempts to be easy to use, yet also effective at it's primary task.

As such, the program is separated in two parts; a simple command line tool that performs the basic tasks of capturing and encoding and an interface that exposes the program functionality in a usable way.

%prep
#'
%setup -q
%patch0 -p1 -b .xorg75

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="%{__install} -c -p"

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README ChangeLog
%{_bindir}/*
%{_mandir}/man?/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.8.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.8.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.8.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8.1-3m)
- apply xorg75 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8.1-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.8.1-1m)
- update to 0.3.8.1

* Wed May 14 2008 Masayuki SANO
- (0.3.7.3-1m)
- import to Momonga from Fedora
- update to 0.3.7.3

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.3.7-3
- Autorebuild for GCC 4.3

* Tue Jan 22 2008 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.7-2
- Add missing jack dependency
* Thu Jan 17 2008 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.7-1
- New upstream release
* Sun Dec 02 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.6-2
- Add jack support
* Sun Oct 21 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.6-1
- New version
- Update URL
* Sat Jun 02 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.4-1
- New version 0.3.4
* Mon Mar 05 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.3.1-3
- chmod +x on source files to make rpmlint happy
* Mon Mar 05 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.3.1-2
- Remove duplicate BR
- Add missing zlib-devel BR
- Preserve timestamps
* Sun Mar 04 2007 Sindre Pedersen Bjrdal <foolish[AT]guezz.net> - 0.3.3.1-1
- Initial build

