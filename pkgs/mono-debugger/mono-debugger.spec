%global momorel 7

Summary: mono-debugger
Name: mono-debugger
Version: 2.10
Release: %{momorel}m%{?dist}
Group: Development/Tools
License: GPL LGPL
URL: http://www.mono-project.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mono-devel >= 2.10
BuildRequires: mono-nunit-devel
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.26.1
BuildRequires: ncurses-devel >= 5.6-10
BuildRequires: xsp
Requires: mono-core mono-tools
Requires: gtk-sharp2 >= 2.10.0
Requires: xsp

%description
mono-debugger

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --program-prefix="" \
    --enable-static=no \
    --with-xsp \
	CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.mdb" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_bindir}/mdb
%{_bindir}/mdb-symbolreader
%{_libdir}/libmonodebuggerserver.so.*
%exclude %{_libdir}/*.la
%{_prefix}/lib/mono/gac/Mono.Debugger
%{_prefix}/lib/mono/gac/Mono.Debugger.Frontend
%{_prefix}/lib/mono/gac/Mono.Debugger.SymbolWriter
%{_prefix}/lib/mono/mono-debugger
%{_prefix}/lib/mono/2.0/mdb.exe
%{_prefix}/lib/mono/2.0/mdb-symbolreader.exe

%files devel
%defattr(-,root,root)
%{_libdir}/libmonodebuggerserver.so
%{_libdir}/pkgconfig/mono-debugger.pc
%{_libdir}/pkgconfig/mono-debugger-frontend.pc

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-7m)
- change Source0 URI

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-6m)
- rebuild for mono-2.10.9

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-5m)
- rebuild for glib 2.33.2

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10-4m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-3m)
- rebuild for new GCC 4.6

* Mon Feb 21 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.1-2m)
- add BuildRequires:

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.3-2m)
- full rebuild for mo7 release

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6-2m)
- use BuildRequires

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Fri Dec 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2.1-1m)
- update to 2.4.2.1

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2
-- add devel package (for pc file)

* Tue May 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4-2m)
- support libtool-2.2.x

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Tue Nov  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2m)
- change source url (NoSource)

* Thu Aug 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update 2.0

* Tue Jul  8 2008  Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60.0107284-1m)
- update to 0.60.0107284 (svn version)

* Sun Apr 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60-4m)
- import patch0 from fedora (build fix)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.60-3m)
- rebuild against gcc43

* Fri Feb  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.60-2m)
- add BuildPreReq ncurses-devel

* Sat Jan  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Sat Jul 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.50-2m)
- delete debug infomation

* Wed May 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Fri Feb  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Mon Aug 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.12-4m)
- revised for multilibarch

* Mon May 15 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.12-3m)
- modify lib64 patch

* Tue May  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-2m)
- rebuild mono -> mono-core

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Thu Feb 02 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-3m)
- add forgotten file %{_libdir}/mono/mono-debugger/*.dll

* Thu Feb 02 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.11-2m)
- add patches for x86_64
- I only checked that rpm file could be build.
- Note that hard-coding path "lib" was changed to "lib64" by patch3.

* Mon Jan 30 2006 Nishio Futoshi <futoshi@momonga-linux.org.
- (0.11-1m)
- start
