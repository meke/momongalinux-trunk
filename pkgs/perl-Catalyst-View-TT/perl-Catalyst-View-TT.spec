%global         momorel 6

Name:           perl-Catalyst-View-TT
Version:        0.41
Release:        %{momorel}m%{?dist}
Summary:        Template View Class
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-View-TT/
Source0:        http://www.cpan.org/authors/id/J/JJ/JJNAPIORK/Catalyst-View-TT-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Catalyst-Runtime >= 5.7
BuildRequires:  perl-Class-Accessor
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MRO-Compat
BuildRequires:  perl-Path-Class
BuildRequires:  perl-Template-Timer
BuildRequires:  perl-Template-Toolkit
BuildRequires:  perl-Test-Simple
Requires:       perl-Catalyst-Runtime >= 5.7
Requires:       perl-Class-Accessor
Requires:       perl-MRO-Compat
Requires:       perl-Path-Class
Requires:       perl-Template-Timer
Requires:       perl-Template-Toolkit
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is the Catalyst view class for the Template Toolkit. Your application
should defined a view class which is a subclass of this module. Throughout
this manual it will be assumed that your application is named MyApp and you
are creating a TT view named Web; these names are placeholders and should
always be replaced with whatever name you've chosen for your application
and your view. The easiest way to create a TT view class is through the
myapp_create.pl script that is created along with the application:

%prep
%setup -q -n Catalyst-View-TT-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/Helper/View/TT.pm
%{perl_vendorlib}/Catalyst/Helper/View/TTSite.pm
%{perl_vendorlib}/Catalyst/View/TT.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-2m)
- rebuild against perl-5.16.3

* Fri Mar  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Wed Jan 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-2m)
- rebuild against perl-5.14.2

* Wed Jul 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.36-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.36-2m)
- rebuild for new GCC 4.5

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.34-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-2m)
- rebuild against perl-5.12.0

* Wed Apr  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Thu Mar 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.31-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-2m)
- rebuild against perl-5.10.1

* Sat Feb 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Thu Feb 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.27-2m)
- rebuild against rpm-4.6

* Fri May  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.26-2m)
- rebuild against gcc43

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.25-2m)
- use vendor

* Tue Jan 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-2m)
- delete dupclicate directory

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.23-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
