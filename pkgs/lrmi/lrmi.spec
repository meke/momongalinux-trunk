%global momorel 5

Name:           lrmi
Version:        0.10
Release:        %{momorel}m%{?dist}
Summary:        Library for calling real mode BIOS routines

Group:          System Environment/Libraries
License:        MIT
URL:            http://sourceforge.net/projects/lrmi/
Source0:        http://dl.sourceforge.net/sourceforge/lrmi/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:         %{name}-0.9-build.patch
Patch1:         lrmi-0.10-newheaders.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kernel-headers

ExclusiveArch:  %{ix86}
Provides:       lib%{name} = %{version}-%{release}

%description
LRMI is a library for calling real mode BIOS routines.

%package        devel
Summary:        Development files for LRMI
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Provides:       lib%{name}-devel = %{version}-%{release}

%description    devel
%{summary}.

%package     -n vbetest
Summary:        Utility for listing and testing VESA graphics modes
Group:          Applications/System

%description -n vbetest
%{summary}.


%prep
%setup -q
%patch0
%patch1 -p1 -b .new-headers


%build
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS" liblrmi.so vbetest


%install
rm -rf $RPM_BUILD_ROOT
make install \
  LIBDIR=$RPM_BUILD_ROOT%{_libdir} INCDIR=$RPM_BUILD_ROOT%{_includedir}
install -Dpm 755 vbetest $RPM_BUILD_ROOT%{_sbindir}/vbetest


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc README
%{_libdir}/liblrmi.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/lrmi.h
%{_includedir}/vbe.h
%{_libdir}/liblrmi.so

%files -n vbetest
%defattr(-,root,root,-)
%{_sbindir}/vbetest


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep 17 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.10-5
- fix compile against modern kernel headers
- add BR: kernel-headers

* Sun Feb 10 2008 Kevin Fenzi <kevin@tummy.com> - 0.10-4
- Rebuild for gcc43

* Sun Aug 26 2007 Kevin Fenzi <kevin@tummy.com> - 0.10-3
- Rebuild for BuildID

* Sun Aug 27 2006 Kevin Fenzi <kevin@tummy.com> - 0.10-2
- Rebuild for fc6

* Sun Mar 12 2006 Ville Skyttä <ville.skytta at iki.fi> - 0.10-1
- 0.10, asm patch applied upstream.

* Sun Dec  4 2005 Ville Skyttä <ville.skytta at iki.fi> - 0.9-2
- Fix build with new binutils.

* Wed Nov  9 2005 Ville Skyttä <ville.skytta at iki.fi> - 0.9-1
- 0.9, patches mostly applied/obsoleted upstream.
- Don't ship static libraries.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0.8-2
- rebuilt

* Fri Jul 23 2004 Ville Skyttä <ville.skytta at iki.fi> - 0:0.8-0.fdr.1
- First build.
