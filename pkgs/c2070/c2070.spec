%global momorel 5

Name:           c2070
Version:        0.99
Release:        %{momorel}m%{?dist}
Summary:        Converts bitcmyk data to Lexmark 2070 printer language

Group:          System Environment/Libraries
License:        GPL+
# This is the original one, but has gone away...
#URL:            http://www.kornblum.i-p.com/2070/Lexmark2070.html
# ...and as the original upstream author did not respond to e-mails,
# here is at least some reference:
URL:            http://www.linuxprinting.org/show_driver.cgi?driver=%{name}
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a filter to convert bitcmyk data such as produced by ghostscript to
the printer language of Lexmark 2070 printers.  It is meant to be used
by the PostScript Description files of the drivers from the foomatic package.

%prep
%setup -q

%build
# The included Makefile is badly written
%{__cc} %{optflags} -o c2070 c2070.c

%install
rm -rf $RPM_BUILD_ROOT
%{__mkdir} -p $RPM_BUILD_ROOT/%{_bindir}
%{__install} c2070 $RPM_BUILD_ROOT/%{_bindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/c2070
%doc README

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.99-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.99-4
- Autorebuild for GCC 4.3

* Tue Sep 18 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.99-3
- The source for the package had died away

* Fri Aug 3 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.99-2
- Modify the License tag in accordance with the new guidelines

* Thu Jun 7 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.99-1
- Initial package
