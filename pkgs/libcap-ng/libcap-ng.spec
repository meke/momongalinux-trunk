%global momorel 1
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: An alternate posix capabilities library
Name: libcap-ng
Version: 0.7.3
Release: %{momorel}m%{?dist}
License: LGPLv2+ and GPLv2+
URL: http://people.redhat.com/sgrubb/libcap-ng
Group: System Environment/Libraries
Source0: http://people.redhat.com/sgrubb/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: kernel-headers
BuildRequires: libattr-devel
BuildRequires: pkgconfig
BuildRequires: python-devel >= 2.7
BuildRequires: swig

%description
Libcap-ng is a library that makes using posix capabilities easier.

%package devel
Summary: Header files and development libraries from libcap-ng
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kernel-headers
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libcap-ng.

%package python
Summary: Python bindings for libcap-ng library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description python
The libcap-ng-python package contains the bindings so that libcap-ng
and can be used by python applications.

%package utils
Summary: Utilities for analysing and setting file capabilities
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description utils
The libcap-ng-utils package contains applications to analyse the
posix capabilities of all the program running on a system. It also
lets you set the file system based capabilities.

%prep
%setup -q

%build
%configure 
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la files
rm -f %{buildroot}/%{_libdir}/%{name}.la
rm -f %{buildroot}/%{_libdir}/python?.?/site-packages/_capng.la

# get rid of static libraries
rm -f %{buildroot}/%{_libdir}/%{name}.a
rm -f %{buildroot}/%{_libdir}/python?.?/site-packages/_capng.a

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog INSTALL README
%attr(0755,root,root) %{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%attr(0644,root,root) %{_includedir}/cap-ng.h
%{_libdir}/pkgconfig/%{name}.pc
%attr(0755,root,root) %{_libdir}/%{name}.so
%attr(0644,root,root) %{_datadir}/aclocal/cap-ng.m4
%attr(0644,root,root) %{_mandir}/man3/capng_*.3*

%files python
%defattr(-,root,root)
%attr(755,root,root) /%{_libdir}/python?.?/site-packages/_capng.so
%{python_sitearch}/capng.py*

%files utils
%defattr(-,root,root)
%doc COPYING
%attr(0755,root,root) %{_bindir}/captest
%attr(0755,root,root) %{_bindir}/filecap
%attr(0755,root,root) %{_bindir}/netcap
%attr(0755,root,root) %{_bindir}/pscap
%attr(0644,root,root) %{_mandir}/man8/captest.8*
%attr(0644,root,root) %{_mandir}/man8/filecap.8*
%attr(0644,root,root) %{_mandir}/man8/netcap.8*
%attr(0644,root,root) %{_mandir}/man8/pscap.8*

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-1m)
- update 0.7.3

* Mon Jul 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.6-1m)
- update 0.6.6

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.5-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-2m)
- rebuild for new GCC 4.6

* Fri Dec 16 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.5-1m)
- version 0.6.5
- remove Patch0: libcap-ng-0.6.5-device.patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Sun Dec 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-2m)
- build fix

* Sat Dec 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.2-1m)
- initial package for bluez-4.58
- import euid.patch and setpcap.patch from Fedora
