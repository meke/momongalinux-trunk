%global momorel 16
%global api_version 1.0

Summary: OpenGL Extension to GTK
Name: gtkglext
Version: 1.2.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://gtkglext.sourceforge.net/

Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk2-devel
BuildRequires: fontconfig-devel, freetype-devel
BuildRequires: mesa-libGL-devel, mesa-libGLU-devel
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXcursor-devel
BuildRequires: libXext-devel, libXfixes-devel, libXft-devel, libXi-devel
BuildRequires: libXinerama-devel, libXmu-devel, libXrandr-devel,
BuildRequires: libXrender-devel, libXt-devel
BuildRequires: cairo-devel >= 1.2.4
BuildRequires: pangox-compat-devel

%description
GtkGLExt is an OpenGL extension to GTK. It provides the GDK objects
which support OpenGL rendering in GTK, and GtkWidget API add-ons to
make GTK+ widgets OpenGL-capable.

%package devel
Summary: Development tools for GTK-based OpenGL applications
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Requires: mesa-libGL-devel
Requires: mesa-libGLU-devel

%description devel
The gtkglext-devel package contains the header files, static libraries,
and developer docs for GtkGLExt.

%prep
%setup -q -n %{name}-%{version}

find -name "Makefile*" | while read x; do
     sed --in-place=.bak~ \
	 -e 's,-DGTK_DISABLE_DEPRECATED,,g' \
     	 -e 's,-DGDK_DISABLE_DEPRECATED,,g' \
     	 -e 's,-DG_DISABLE_DEPRECATED,,g' \
	 $x
done

%build
%configure --disable-gtk-doc --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README TODO
%{_libdir}/libgdkglext-x11-%{api_version}.so.*
%{_libdir}/libgtkglext-x11-%{api_version}.so.*
%exclude %{_libdir}/lib*.la

%files devel
%defattr(-,root,root)
%doc examples
%{_includedir}/*
%{_libdir}/gtkglext-%{api_version}
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_datadir}/aclocal/*
%{_datadir}/gtk-doc/html/*

%changelog
* Mon Apr  1 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-16m)
- add BuildRequires

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-15m)
- revise Requires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-12m)
- full rebuild for mo7 release

* Sun May 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-11m)
- fix bug
-- see http://developer.momonga-linux.org/kagemai/guest.cgi?action=view_report&id=259&project=momongaja

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-10m)
- add patch for gtk-2.20 by gengtk220patch

* Sat Feb  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-9m)
- good-bye autoreconf
--disable-static
-- add patch0 for gtk2-2.20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Apr 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-7m)
- add Requires: mesa-libGL-devel mesa-libGLU-devel

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-4m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-3m)
- BuildRequires: freetype2-devel -> freetype-devel

* Sun Oct  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-2m)
- rebuild against cairo-1.2.4
- delete libtool library

* Sun May 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-3m)
- add auto commands for fix missing lib*so* problem

* Sun Oct  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.6-2m)
- rebuild against gcc-c++-3.4.2

* Tue Mar 16 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Fri Nov 28 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5
- fix %%changelog entries

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (1.0.4-2m)
- pretty spec file.

* Tue Sep 23 2003 Kenta MURATA <muraken2@nifty.com>
- (1.0.4-2m)
- include examples directory as document into devel package.

* Sat Sep 20 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Wed Aug 20 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Fri Aug  1 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sun Jul 14 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-2m)
- delete Requires: gtk+, Requires: XFree86-libs, pushd-popd

* Sun Jul 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- initial import to Momonga

* Sun May 11 2003 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Removed LDFLAGS setting.
- Removed atk, pango, glib2 from Requires.
- Remove lib*.la.bak files.

* Mon Feb 24 2003 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Added %%{_datadir}/aclocal/* to the file list.
- Re-enabled static libraries by default.

* Tue Dec  3 2002 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Added %%{_libdir}/gtkglext-%%{api_version} to the file list.

* Fri Nov 15 2002 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Removed --disable-mesa-ext configure option.
- Disabled static libraries by default.

* Sat Aug  3 2002 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Added --disable-mesa-ext configure option.

* Sun Jun 23 2002 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Initial build.
