%global momorel 1

%global libversion 2.0x_cvs
%global __ln ln

Summary: A program which a Linux user can utilize to create a rescue/restore CD/tape.
Name:    mondo

%{?!include_specopt}
%{?!_without_xmondo:    %global _without_xmondo 0}

Version: 3.0.4
Release: %{momorel}m%{?dist}
License: GPL
Group:   Applications/Archiving
URL:     http://www.mondorescue.org
Source0: ftp://ftp.mondorescue.org/src/%{name}-%{version}.tar.gz
NoSource:  0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: newt-devel >= 0.50, slang-devel >= 1.4.1, gcc

Requires: mindi >= 1.02, bzip2 >= 0.9
Requires: newt >= 0.50, slang >= 1.4.1
Requires: afio, genisoimage, binutils, buffer, parted
# add xorg library at xmondo
%{!?_without_xmondo:BuildRequires: gcc-c++, qt3-devel, kdelibs3-devel, libpng-devel}
%ifarch ia64
Requires: elilo
%else
Requires: syslinux >= 1.52, wodim
%{!?_without_xmondo:BuildRequires: arts-devel, libart_lgpl-devel}
%endif

Autoreq: 0

%package xmondo
Summary: A QT based graphical front end for %{name}
Group:   Applications/Archiving
Requires: %{name} = %{version}-%{release}, qt3, kdelibs3

%package devel
Summary: Header files for building against Mondo
Group: Development/Libraries

%description
Objective
""""""""""
To produce a program which any Linux user can utilize to create
a rescue/restore CD (or CDs, if their installation is >2Gb approx.). Also
works for tapes and NFS.

%description xmondo
Xmondo is a QT based graphical frontend to mondoarchive.  It can help you 
set up a backup by following onscreen prompts.

%description devel
mondo-devel contains a few header files that are necessary for developing
with mondo.

%prep
%setup -q
# clear out any CVS directories if they exist
for dir in `find . -name CVS`
do
  rm -rf ${dir}
done

chmod 755 configure

%configure \
%if  %{_without_xmondo}
	--with-x11
%endif

%build
%{__make} VERSION=%{version} CFLAGS="-D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_REENTRANT"

%install
rm -Rf /usr/local/share/mondo
%{__mkdir} -p %{buildroot}%{_datadir}/mondo
%{__mkdir} -p %{buildroot}%{_includedir}/mondo
%{__mkdir} -p %{buildroot}%{_sbindir}
%{__mkdir} -p %{buildroot}%{_libdir}
%{__mkdir} -p %{buildroot}%{_mandir}/man8
for fname in src/mondoarchive/mondoarchive src/mondorestore/mondorestore ; do
    %{__install} -m 755 $fname %{buildroot}%{_sbindir}
    %{__install} -m 755 $fname %{buildroot}%{_datadir}/mondo
done
%{!?_without_xmondo:%{__install} -m 755 mondo/xmondo/.libs/xmondo %{buildroot}%{_sbindir}}

for f in libmondo libmondo.so libmondo-newt libmondo-newt.so libmondo-newt.1 libmondo-newt.so.1 libmondo-newt.1.0.0 libmondo-newt.so.1.0.0 libmondo.2 libmondo.so.2 libmondo.2.0.3 libmondo.so.2.0.3 ; do
    fname=mondo/common/.libs/$f
    if [ -e "$fname" ] ; then

# Hugo's way
#        %{__install} -m 755 $fname %{buildroot}%{_libdir}
# ----------
# Joshua's way
         %{__cp} -d $fname %{buildroot}%{_libdir}
# ----------
    
    fi
done
%{!?_without_xmondo:%{__install} -m 755 src/common/.libs/libXmondo-%{libversion}.so %{buildroot}%{_libdir}}
%{!?_without_xmondo:%{__ln} -s libXmondo-%{libversion}.so %{buildroot}%{_libdir}/libXmondo.so}
%{!?_without_xmondo:%{__install} -m 644 mondo/xmondo/mondo.png %{buildroot}%{_datadir}/mondo}
%{__install} -m 755 src/do-not-compress-these       %{buildroot}%{_datadir}/mondo
%{__install} -m 755 src/autorun                     %{buildroot}%{_datadir}/mondo
%{__install} -m 644 docs/man/mondoarchive.8 %{buildroot}%{_mandir}/man8
gzip -9 -f %{buildroot}%{_mandir}/man8/mondoarchive.8
%{__install} -m 644 docs/man/mondorestore.8 %{buildroot}%{_mandir}/man8
gzip -9 -f %{buildroot}%{_mandir}/man8/mondorestore.8
%{__cp} -Rf src/restore-scripts  %{buildroot}%{_datadir}/mondo
%{__cp} -Rf src/post-nuke.sample %{buildroot}%{_datadir}/mondo
for fname in src/include/my-stuff.h src/common/mondostructures.h src/common/libmondo-*-EXT.h src/common/newt-specific-EXT.h; do
    %{__install} -m 644 $fname %{buildroot}%{_includedir}/mondo
done

%post
ldconfig

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog docs/en/*
%dir %{_datadir}/mondo
%{_sbindir}/mondorestore
%{_sbindir}/mondoarchive
%{_datadir}/mondo/mondorestore
%{_datadir}/mondo/post-nuke.sample/*
%{_datadir}/mondo/restore-scripts/*
%{_datadir}/mondo/do-not-compress-these
%{_datadir}/mondo/mondoarchive
%{_datadir}/mondo/autorun
%{_mandir}/man8/mondoarchive.8*
%{_mandir}/man8/mondorestore.8*
##%%{_libdir}/lib*

%{!?_without_xmondo:%files xmondo}
%{!?_without_xmondo:%{_sbindir}/xmondo}
%{!?_without_xmondo:%{_libdir}/libXmondo-%{libversion}.so}
%{!?_without_xmondo:%{_libdir}/libXmondo.so}
%{!?_without_xmondo:%{_datadir}/mondo/mondo.png}

%files devel
%defattr(-,root,root)
%dir %{_includedir}/mondo
%{_includedir}/mondo/*

%changelog
* Fri Oct 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Fri Dec 14 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2
- build against syslinux-5.00-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.04-13m)
- full rebuild for mo7 release

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-12m)
- fix typo for rpm48

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-11m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.04-9m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-8m)
- rebuild against rpm-4.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-7m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.04-6m)
- rebuild against gcc43

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-5m)
- revised spec for debuginfo

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.04-4m)
- good-bye cdrtools and welcome cdrkit

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.04-3m)
- change Source URL

* Mon Jun 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.04-2m)
- fix files list (%%{_libdir})

* Sat Mar 05 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.04-1m)
- update

* Thu Feb 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.03-3m)
- add %%defattr.

* Tue Aug 10 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (2.03-2m)
- typo

* Tue Jul 10 2004 KImitake SHIBATA <siva@momonga-linux.org>
- (2.03-1m)
- New Version
- DVD+-R(W) writeing is supported.

* Tue Dec  2 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.67-2m)
- add source

* Fri Nov 14 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.67-1m)
- First Relese

