%global momorel 1
#global snapdate 2010-12-03

Summary: elementary
Name: elementary
Version: 1.7.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: User Interface/X
URL: http://www.enlightenment.org/
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: evas-devel    >= 1.7.7
BuildRequires: ecore-devel   >= 1.7.7
BuildRequires: edje-devel    >= 1:1.7.7
BuildRequires: efreet-devel  >= 1.7.7
BuildRequires: SDL-devel

%description
Elementary is a widget set. It is a new-style of widget set much more canvas
object based than anything else. Why not ETK? Why not EWL? Well they both
tend to veer away from the core of Evas, Ecore and Edje a lot to build their
own worlds. Also I wanted something focused on embedded devices -
specifically small touchscreens. Unlike GTK+ and Qt, 75% of the "widget set"
is already embodied in a common core - Ecore, Edje, Evas etc. So this
fine-grained library splitting means all of this is shared, just a new
widget "personality" is on top. And that is... Elementary, my dear watson.
Elementary.

%package devel
Summary: Elementary headers, static libraries, documentation and test programs
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Headers, static libraries, test programs and documentation for Elementary

%prep
%setup -q

%build
%configure --disable-static CFLAGS="-DUnversioneddirectory=0"
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall transform='s,x,x,'
find %{buildroot} -name "*.la" -delete

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%dir %{_libdir}/%{name}
%{_libdir}/libelementary*.so.*
%{_libdir}/%{name}/modules
%{_bindir}/*
%{_datadir}/applications/*.desktop
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/*
%{_datadir}/icons/elementary.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%files devel
%defattr(-, root, root)
%{_libdir}/*.so
%dir %{_libdir}/edje/
%{_libdir}/edje/modules/elm/*/*.so
%dir %{_libdir}/%{name}/
%{_libdir}/%{name}/modules/test_entry/*/*.so
%{_libdir}/pkgconfig/*
%dir %{_includedir}/%{name}-1
%{_includedir}/%{name}-1/*.h

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.0.55225-4m)
- rebuild for new EFL

* Sun Sep 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0.55225-3m)
- add CFLAGS="-DUnversioneddirectory=0" for build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0.55225-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.0.55225-1m)
- update to new svn snap

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0.49898-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0.49898-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.0.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.0.49539-2m)
- edje version fix

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.0.49539-1m)
- update to new svn snap

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.0.063-1m)
- update to new svn snap

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.1.0-2m)
- del BR: emotion-devel

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1.0-1m)
- first Momonga spec
