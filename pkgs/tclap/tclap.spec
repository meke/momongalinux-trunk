%global         momorel 2
%global         with_doc 1

Name:           tclap
Version:        1.2.1
Release:        %{momorel}m%{?dist}
Summary:        Templatized C++ Command Line Parser
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
URL:            http://tclap.sourceforge.net/
Group:          System Environment/Libraries
License:        MIT
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%if %{with_doc}
BuildRequires:  doxygen
BuildRequires:  fdupes
BuildRequires:  graphviz
%endif
BuildRequires:  gcc-c++
BuildRequires:  libstdc++-devel
BuildRequires:  make
BuildRequires:  glibc-devel
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  pkgconfig
Provides:       libtclap = %{version}
Provides:       libtclap-devel = %{version}

%description
TCLAP is a small, flexible library that provides a simple interface for
defining and accessing command line arguments. It was intially inspired by the
user friendly CLAP libary. The difference is that this library is templatized,
so the argument class is type independent. Type independence avoids
identical-except-for-type objects, such as IntArg, FloatArg, and StringArg.
While the library is not strictly compliant with the GNU or POSIX standards, it
is close. 

%package devel
Summary:        Headers and Libraries for %{name}
Group:          Development/Libraries

%description devel
%{summary}.

%if %{with_doc}
%package doc
Summary:        API Documentation for %{name}
Group:          Documentation
BuildArch:      noarch

%description doc
This package contains the API documentation for TCLAP, the Templatized
C++ Command Line Parser.
%endif

%prep
%setup -q

%build
%configure \
%if %{with_doc}
	 --enable-doxygen
%else
	 --disable-doxygen
%endif

%make

%install
%makeinstall

install -d "%{buildroot}%{_docdir}/%{name}"

%if %{with_doc}
fdupes -s "%{buildroot}%{_docdir}/%{name}/html"
%endif

%clean
rm -rf "%{buildroot}"

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README
%doc %dir %{_docdir}/%{name} 
# empty...

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/tclap.pc
%{_includedir}/tclap

%if %{with_doc}
%files doc
%defattr(-,root,root)
%doc %{_docdir}/%{name}/html
%doc %{_docdir}/%{name}/*.html
%doc %{_docdir}/%{name}/*.css
%endif

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.1-2m)
- rebuild against graphviz-2.36.0-1m

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- initial build for Momonga Linux (based on SuSE's spec)
