%global momorel 7

Name: mythes-en
Summary: English thesarus
Version: 3.0
Release: %{momorel}m%{?dist}
Source0: http://wordnetcode.princeton.edu/%{version}/WordNet-%{version}.tar.bz2
NoSource: 0
Source1: http://www.danielnaber.de/wn2ooo/wn2ooo20050723.tgz
NoSource: 1
Group: Applications/Text
URL: http://www.danielnaber.de/wn2ooo/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python, perl
License: BSD
BuildArch: noarch

%description
English thesarus.

%prep
%setup -q -n WordNet-%{version}
tar xzf %{SOURCE1}

%build
export WNHOME=`pwd`
python wn2ooo/wn2ooo.py > th_en_US_v2.dat
cat th_en_US_v2.dat | perl wn2ooo/th_gen_idx.pl > th_en_US_v2.idx

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/mythes
cp -p th_en_US_v2.* $RPM_BUILD_ROOT/%{_datadir}/mythes

pushd $RPM_BUILD_ROOT/%{_datadir}/mythes/
en_US_aliases="en_AU en_BS en_BZ en_CA en_GH en_GB en_IE en_IN en_JM en_NA en_NZ en_PH en_TT en_ZA en_ZW"
for lang in $en_US_aliases; do
        ln -s th_en_US_v2.idx "th_"$lang"_v2.idx"
        ln -s th_en_US_v2.dat "th_"$lang"_v2.dat"
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE README AUTHORS
%dir %{_datadir}/mythes
%{_datadir}/mythes/*

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-7m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 Masahiro Takahata <takahat@amomonga-linux.org>
- (3.0-1m)
- import from Fedora

* Wed Nov 28 2007 Caolan McNamara <caolanm@redhat.com> - 3.0-1
- initial version
