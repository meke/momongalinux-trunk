%global momorel 5
Name:		libcue
Version:	1.3.0
Release:	%{momorel}m%{?dist}
Summary:	Cue sheet parser library

Group:		System Environment/Libraries
# Files libcue/rem.{c,h} contains a BSD header
License:	GPLv2 and BSD
URL:		http://libcue.sourceforge.net/
Source0:	http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)


%description
Libcue is intended for parsing a so-called cue sheet from a char string or
a file pointer. For handling of the parsed data a convenient API is available.


%package devel
Summary:	Development files
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig


%description	devel
Development files for %{name}.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_libdir}/libcue.la


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%{_libdir}/%{name}.so.*
%doc AUTHORS COPYING NEWS


%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc


%changelog
* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-5m)
- fix momorel

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-4.1m)
- import from fedora

