%global         momorel 1

Name:           skipfish
Version:        2.02b
Release:        %{momorel}m%{?dist}
Summary:        A fully automated, active web application security reconnaissance tool.

Group:          Applications/Internet
License:        GPLv2+
URL:            http://code.google.com/p/skipfish/
Source0:        http://skipfish.googlecode.com/files/%{name}-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  zlib-devel,openssl-devel,libidn-devel
Requires:       zlib, openssl, libidn


%description
A fully automated, active web application security reconnaissance tool. Key features:
High speed: pure C code, highly optimized HTTP handling, minimal CPU footprint - easily achieving 2000 requests per second with responsive targets. 
Ease of use: heuristics to support a variety of quirky web frameworks and mixed-technology sites, with automatic learning capabilities, on-the-fly wordlist creation, and form autocompletion. 
Cutting-edge security logic: high quality, low false positive, differential security checks, capable of spotting a range of subtle flaws, including blind injection vectors. 


%prep
%setup -q 


%build
%make


%install
rm -rf %{buildroot}
install -pD skipfish %{buildroot}%{_bindir}/skipfish
install -pD sfscandiff %{buildroot}%{_bindir}/sfscandiff
mkdir -p %{buildroot}%{_datadir}/skipfish/dictionaries
install -m 0644 -p dictionaries/* %{buildroot}%{_datadir}/skipfish/dictionaries
mkdir -p %{buildroot}%{_datadir}/skipfish/assets
install -m 0644 -p assets/* %{buildroot}%{_datadir}/skipfish/assets


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog
%{_bindir}/*
%{_datadir}/skipfish


%changelog
* Wed Jul 13 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org> 
- (2.02-1m)
- Initial build
