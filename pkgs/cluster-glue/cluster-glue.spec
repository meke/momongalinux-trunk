%global momorel 4

%global gname haclient
%global uname hacluster
%global nogroup nobody

# When downloading directly from Mercurial, it will automatically add this prefix
# Invoking 'hg archive' wont but you can add one with: hg archive -t tgz -p "Reusable-Cluster-Components-" -r $upstreamversion $upstreamversion.tar.gz
%global specversion 9
%global upstreamprefix Reusable-Cluster-Components-
%global upstreamversion 8286b46c91e3

# Keep around for when/if required
%global alphatag %{upstreamversion}.hg

Summary:	Reusable cluster components
Name:		cluster-glue
Version:	1.0.6
Release:	%{momorel}m%{?dist}
License:	GPLv2+ and LGPLv2+
Group:		System Environment/Base
URL:		http://www.clusterlabs.org
Source0:	http://hg.linux-ha.org/glue/archive/%{upstreamversion}.tar.bz2
Source1:	logd.service
Patch0:         %{name}-%{version}-glib-include.patch
Patch1:		glib-everything-or-bust.patch
Requires:	perl-TimeDate
Requires:	cluster-glue-libs = %{version}-%{release}

# Directives to allow upgrade from combined heartbeat packages in Fedora11
Provides:	heartbeat-stonith = 3.0.0-1
Provides:	heartbeat-pils = 3.0.0-1
Obsoletes:	heartbeat-stonith < 3.0.0-1
Obsoletes:	heartbeat-pils < 3.0.0-1

## Setup/build bits

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Build dependencies
BuildRequires: automake autoconf libtool pkgconfig chrpath libtool-ltdl-devel
BuildRequires: bzip2-devel glib2-devel >= 2.33.6 e2fsprogs-devel python-devel libxml2-devel
BuildRequires: OpenIPMI-devel libnet-devel net-snmp-devel >= 5.7.1 libcurl-devel openhpi-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: libuuid-devel

# Needed for systemd unit
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
A collection of common tools that are useful for writing cluster managers 
such as Pacemaker.
Provides a local resource manager that understands the OCF and LSB
standards, and an interface to common STONITH devices.

%package -n cluster-glue-libs
Summary:	Reusable cluster libraries
Group:		Development/Libraries

%description -n cluster-glue-libs
A collection of libraries that are useful for writing cluster managers 
such as Pacemaker.

%package -n cluster-glue-libs-devel 
Summary:	Headers and libraries for writing cluster managers
Group:		Development/Libraries
Requires:	cluster-glue-libs = %{version}-%{release}

%description -n cluster-glue-libs-devel
Headers and shared libraries for a useful for writing cluster managers 
such as Pacemaker.

%prep
%setup -q -n %{upstreamprefix}%{upstreamversion}
%patch0 -p1

%build
./autogen.sh
%configure \
    CFLAGS="${CFLAGS} $(echo '%{optflags}')" \
    --enable-fatal-warnings=no   \
    --localstatedir=%{_var}      \
    --with-daemon-group=%{gname} \
    --with-daemon-user=%{uname} \
    --enable-static=no \
	CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

# Dont package static libs
find %{buildroot} -name '*.a' -exec rm {} \;
find %{buildroot} -name '*.la' -exec rm {} \;

# Don't package things we wont support
rm -f %{buildroot}/%{_libdir}/stonith/plugins/stonith2/rhcs.*
rm -f %{buildroot}/%{_sbindir}/hb_report

# Nuke sysvinit bits
rm -rf %{buildroot}%{_sysconfdir}/init.d/

# Install systemd bits
mkdir -p %{buildroot}%{_unitdir}
install -m0644 %{SOURCE1} %{buildroot}%{_unitdir}/

%clean
rm -rf --preserve-root %{buildroot}

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable logd.service > /dev/null 2>&1 || :
    /bin/systemctl stop logd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart logd.service >/dev/null 2>&1 || :
fi

%triggerun -- cluster-glue < 1.0.6-1m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply httpd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save logd >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del logd >/dev/null 2>&1 || :
/bin/systemctl try-restart logd.service >/dev/null 2>&1 || :

%pre -n cluster-glue-libs
getent group %{gname} >/dev/null || groupadd -r %{gname}
getent passwd %{uname} >/dev/null || \
useradd -r -g %{gname} -d %{_var}/lib/heartbeat/cores/hacluster -s /sbin/nologin \
-c "heartbeat user" %{uname}
exit 0

%post -n cluster-glue-libs -p /sbin/ldconfig

%postun -n cluster-glue-libs -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_sbindir}/ha_logger
%{_sbindir}/lrmadmin
%{_sbindir}/meatclient
%{_sbindir}/sbd
%{_sbindir}/stonith
%{_unitdir}/logd.service

%dir %{_libdir}/stonith
%dir %{_libdir}/stonith/plugins
%dir %{_libdir}/stonith/plugins/stonith2
%{_datadir}/cluster-glue/ha_log.sh
%{_libdir}/stonith/plugins/external
%{_libdir}/stonith/plugins/stonith2/*.so
%{_libdir}/stonith/plugins/stonith2/*.py*
%{_libdir}/stonith/plugins/xen0-ha-dom0-stonith-helper

%dir %{_datadir}/cluster-glue
%{_datadir}/cluster-glue/ha_cf_support.sh
%{_datadir}/cluster-glue/openais_conf_support.sh
%{_datadir}/cluster-glue/utillib.sh
%{_datadir}/cluster-glue/combine-logs.pl

%dir %{_var}/lib/heartbeat
%dir %{_var}/lib/heartbeat/cores
%dir %attr (0700, root, root)           %{_var}/lib/heartbeat/cores/root
%dir %attr (0700, nobody, %{nogroup})   %{_var}/lib/heartbeat/cores/nobody
%dir %attr (0700, %{uname}, %{gname})   %{_var}/lib/heartbeat/cores/%{uname}

%doc %{_datadir}/doc/cluster-glue/stonith
%doc %{_mandir}/man1/*
%doc %{_mandir}/man8/*
%doc AUTHORS
%doc COPYING

%files -n cluster-glue-libs
%defattr(-,root,root)
%dir %{_libdir}/heartbeat
%dir %{_libdir}/heartbeat/plugins
%dir %{_libdir}/heartbeat/plugins/RAExec
%dir %{_libdir}/heartbeat/plugins/InterfaceMgr
%{_libdir}/heartbeat/lrmd
%{_libdir}/heartbeat/ha_logd
%{_libdir}/heartbeat/plugins/RAExec/*.so
%{_libdir}/heartbeat/plugins/InterfaceMgr/*.so
%{_libdir}/lib*.so.*
#%doc AUTHORS
%doc COPYING.LIB

%files -n cluster-glue-libs-devel
%defattr(-,root,root)
%dir %{_libdir}/heartbeat/plugins/test
%dir %{_datadir}/cluster-glue
%{_libdir}/lib*.so
%{_libdir}/heartbeat/ipctest
%{_libdir}/heartbeat/ipctransientclient
%{_libdir}/heartbeat/ipctransientserver
%{_libdir}/heartbeat/transient-test.sh
%{_libdir}/heartbeat/base64_md5_test
%{_libdir}/heartbeat/logtest
%{_includedir}/clplumbing
%{_includedir}/heartbeat
%{_includedir}/stonith
%{_includedir}/pils
%{_datadir}/cluster-glue/lrmtest
%{_libdir}/heartbeat/plugins/test/test.so
#%doc AUTHORS
#%doc COPYING
#%doc COPYING.LIB

%changelog
* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-4m)
- rebuild against glib2-2.33.6

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-3m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-2m)
- build fix

* Tue Jan 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6
-- support systemd

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-6m)
- rebuild against net-snmp-5.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-3m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-2m)
- move %%{_libdir}/heartbeat from cluster-glue to cluster-glue-libs for heartbeat-libs and multilib
- correct dependency

* Wed Jul 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.5-1m)
- update 1.0.5

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.7m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.6m)
- rebuild against net-snmp-5.5 

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.4m)
- change Release

* Thu Oct  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-0.9-3m)
- fix %%files again

* Thu Oct  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-0.9-2m)
- fix %%files

* Tue Sep 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-0.9-1m)
- import from Fedora

* Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 1.0-0.9.d97b9dea436e.hg.1
- rebuilt with new openssl

* Mon Aug 17 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0-0.9.d97b9dea436e.hg
- Include relevant provides: and obsoletes: directives for heartbeat
- Update the tarball from upstream to version d97b9dea436e
  + Include license files
  + Fix error messages in autogen.sh
  + High (bnc#501723): Tools: hb_report: collect archived logs too
  + Medium: clplumbing: check input when creating IPC channels
  + Medium (bnc#510299): stonith: set G_SLICE to always-malloc to avoid bad interaction with the threaded openhpi
  + Med: hb_report: report on more packages and with more state.
  + The -E option to lrmadmin does not take an argument
  + Provide a default value for docdir and ensure it is expanded
  + Low: clplumbing: fix a potential resource leak in cl_random (bnc#525393).
  + Med: hb_report: Include dlm_tool debugging information if available.
  + hb_report: Include more possible error output.
  + Medium: logd: add init script and example configuration file.
  + High: logd: Fix init script. Remove apphbd references.
  + logd: configuration file is optional.
  + logd: print status on finished operations.
  + High: sbd: actually install the binary.
  + Medium: stonith: remove references to heartbeat artifacts.
  + High: hb_report: define HA_NOARCHBIN
  + hb_report: correct syntax error.
  + hb_report: Include details about more packages even.
  + hb_report: report corosync packages too.

* Mon Aug 10 2009 Ville Skytta <ville.skytta@iki.fi> - 1.0-0.8.75cab275433e.hg
- Use bzipped upstream tarball.

* Tue Jul  28 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0-0.7.75cab275433e.hg
- Add a leading zero to the revision when alphatag is used

* Tue Jul  28 2009 Andrew Beekhof <andrew@beekhof.net> - 1.0-0.6.75cab275433e.hg
- Incorporate results of Fedora review
  - Use global instead of define
  - Remove unused rpm variable
  - Remove redundant configure options
  - Change version to 1.0.0 pre-release and include Mercurial tag in version

* Mon Jul  27 2009 Andrew Beekhof <andrew@beekhof.net> - 0.9-5
- Use linux-ha.org for Source0
- Remove Requires: $name from -devel as its implied
- Instead of 'daemon', use the user and group from Heartbeat and create it 
  if necessary

* Fri Jul  24 2009 Andrew Beekhof <andrew@beekhof.net> - 0.9-4
- Update the tarball from upstream to version 75cab275433e
- Include an AUTHORS and license file in each package
- Change the library package name to cluster-glue-libs to be more 
  Fedora compliant

* Mon Jul  20 2009 Andrew Beekhof <andrew@beekhof.net> - 0.9-3
- Package the project AUTHORS file
- Have Source0 reference the upstream Mercurial repo

* Tue Jul  14 2009 Andrew Beekhof <andrew@beekhof.net> - 0.9-2
- More cleanups

* Fri Jul  3 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 0.9-1
- Fedora-ize the spec file

* Fri Jun  5 2009 Andrew Beekhof <andrew@beekhof.net> - 0.9-0
- Initial checkin
