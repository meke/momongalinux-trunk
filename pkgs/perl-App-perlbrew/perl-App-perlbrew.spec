%global         momorel 1

Name:           perl-App-perlbrew
Version:        0.69
Release:        %{momorel}m%{?dist}
Summary:        Manage perl installations in your $HOME
License:        MIT
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/App-perlbrew/
Source0:        http://www.cpan.org/authors/id/G/GU/GUGOD/App-perlbrew-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.0
BuildRequires:  perl-Capture-Tiny >= 0.20
BuildRequires:  perl-CPAN-Perl-Releases >= 0.76
BuildRequires:  perl-Devel-PatchPerl >= 0.76
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Temp
BuildRequires:  perl-IO-All
BuildRequires:  perl-local-lib >= 1.008004
BuildRequires:  perl-Path-Class
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-NoWarnings
BuildRequires:  perl-Test-Output
BuildRequires:  perl-Test-Simple >= 0.98
BuildRequires:  perl-Test-Spec
Requires:       curl
Requires:       perl-Capture-Tiny >= 0.20
Requires:       perl-CPAN-Perl-Releases >= 0.76
Requires:       perl-Devel-PatchPerl >= 0.76
Requires:       perl-local-lib >= 1.008004
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
perlbrew is a program to automate the building and installation of perl in
an easy way. It installs everything to ~/perl5/perlbrew, and requires you
to tweak your PATH by including a bashrc/cshrc file it provides. You then
can benefit from not having to run 'sudo' commands to install cpan modules
because those are installed inside your HOME too. It provides multiple
isolated perl environments, and a mechanism for you to switch between them.

%prep
%setup -q -n App-perlbrew-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
PERLBLEW_ROOT=%{perl_vendorlib} make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE MYMETA.yml README
%{_bindir}/perlbrew
%{perl_vendorlib}/App/perlbrew.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.69-1m)
- rebuild against perl-5.20.0
- update to 0.69

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-2m)
- rebuild against perl-5.18.1

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-1m)
- update to 0.66

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-2m)
- rebuild against perl-5.18.0

* Tue May  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- update to 0.63

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Wed Mar 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- rebuild against perl-5.16.3

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Mon Nov 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Mon Nov  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-2m)
- rebuild against perl-5.16.2

* Mon Oct 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-2m)
- rebuild against perl-5.16.1

* Sun Jul 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Sun Jan  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Wed Dec 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Sat Nov 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Thu Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Sat Oct 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-2m)
- rebuild against perl-5.14.2

* Sun Aug 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
