%global momorel 6

Summary: personal photo management application
Name: f-spot
Version: 0.8.2
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
Source0: ftp://ftp.gnome.org/pub/gnome/sources/%{name}/0.8/%{name}-%{version}.tar.bz2
NoSource: 0

# https://bugzilla.gnome.org/show_bug.cgi?id=629224
Patch0: %{name}-mono2.8.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://f-spot.org/Main_Page
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: mono-devel >= 2.8
BuildRequires: libgnome-devel >= 2.24.1
BuildRequires: libgnomeui-devel >= 2.24.0
BuildRequires: libexif-devel >= 0.6.16
BuildRequires: gtk-sharp2-devel >= 2.12.10
BuildRequires: gnome-sharp-devel >= 2.24.2
BuildRequires: ndesk-dbus-devel >= 0.6.0
BuildRequires: ndesk-dbus-glib-devel >= 0.4.1
BuildRequires: mono-core >= 2.0
BuildRequires: lcms-devel >= 1.17
BuildRequires: libgphoto2-devel >= 2.4.0
BuildRequires: gnome-icon-theme >= 2.24.0
BuildRequires: gnome-keyring-sharp-devel
BuildRequires: flickrnet-devel
BuildRequires: gtk2-devel
BuildRequires: sqlite-devel
BuildRequires: GConf2
BuildRequires: libjpeg >= 8a
BuildRequires: mono-addins-devel >= 0.6.1
# for *.dll
BuildRequires: gnome-sharp-libart_lpgl
BuildRequires: gnome-sharp-gnome-vfs2

%description
F-Spot is a full-featured personal photo management application for
the GNOME desktop (http://www.gnome.org/).

%package devel
Summary: f-spot devel
Group: Development/Libraries
Requires: %{name} >= %{version}
Requires: gtk-sharp2-devel >= 2.12.4

%description devel
F-Spot devel package

%prep
%setup -q

%patch0 -p1 -b .mono28

%build
%configure \
    --disable-static \
    --disable-schemas-install \
    --disable-scrollkeeper
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

# delete debug info
find %{buildroot} -name "*.mdb" -delete
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
if [ -x /usr/bin/gtk-update-icon-cache ]; then
    gtk-update-icon-cache -q -f -t /usr/share/icons/hicolor
fi
rarian-sk-update

export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/%{name}.schemas \
  > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/%{name}.schemas \
    > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/%{name}.schemas \
    > /dev/null || :
fi

%postun
/sbin/ldconfig
if [ -x /usr/bin/gtk-update-icon-cache ]; then
    gtk-update-icon-cache -q -f -t /usr/share/icons/hicolor
fi
rarian-sk-update

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS ChangeLog MAINTAINERS NEWS README TODO
%{_sysconfdir}/gconf/schemas/f-spot.schemas
%{_bindir}/f-spot*
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/*.dll
%{_libdir}/%{name}/*.dll.config
%{_libdir}/%{name}/*.exe
%{_libdir}/%{name}/*.exe.config
%{_libdir}/%{name}/*.so.*
%{_libdir}/%{name}/*.so
%exclude %{_libdir}/%{name}/*.la

%dir %{_libdir}/%{name}/Extensions
%{_libdir}/%{name}/Extensions/*.dll
%{_libdir}/%{name}/FSpot.Core.addins
%{_libexecdir}/gnome-screensaver/f-spot-screensaver
%{_datadir}/applications/f-spot-view.desktop
%{_datadir}/applications/f-spot.desktop
%{_datadir}/applications/screensavers/f-spot-screensaver.desktop
%{_datadir}/applications/f-spot-import.desktop
%{_datadir}/gnome/help/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/%{name}

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/f-spot.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-6m)
- rebuild for mono-2.10.9

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-5m)
- revise Requires

* Wed Jan 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-4m)
- add BuildRequires
- fix momorel

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-2m)
- rebuild against mono-addins-0.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-2m)
- rebuild for new GCC 4.6

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-2m)
- rebuild for new GCC 4.5

* Thu Oct 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-1m)
- version 0.8.0
- import 2 pstches from cooker
 +* Mon Oct 11 2010 Gtz Waschk <waschk@mandriva.org> 0.8.0-4mdv2011.0
 ++ Revision: 584931
 +- add patches for mono 2.8

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-4m)
- fix BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-3m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-2m)
- rebuild against mono-addins-0.5

* Sat Jun 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-1m)
- update 0.7.0

* Sun May 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update 0.6.2

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1.5-4m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1.5-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1.5-1m)
- update 0.6.1.5

* Tue Sep 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1.3-1m)
- update 0.6.1.3

* Thu Sep 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1.2-1m)
- update 0.6.1.2

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1.1-2m)
- fix %%files to enable build
- Compiling FSpot.Bling.dll still fails

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1.1-1m)
- update 0.6.1.1
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.3-2m)
- rebuild against rpm-4.6

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0.3-1m)
- update to 0.5.0.3

* Thu Oct  9 2008 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0.2-1m)
- update to 0.5.0.2

* Sun Oct  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0.1-1m)
- update to 0.5.0.1

* Sun Jun  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-2m)
- add Prereq: gtk2-common

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3.1-2m)
- fix desktop

* Thu May  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3.1-1m)
- update to 0.4.3.1

* Wed Apr 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Sun Apr 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2.20080413-1m)
- get from svn

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-2m)
- rebuild against gcc43

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Wed Dec  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Fri Aug  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Tue Mar  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.5-1m)
- update to 0.3.5

* Sat Feb 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.4-1m)
- update to 0.3.4

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-1m)
- update to 0.3.3

* Tue Jan 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Thu Nov 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Fri Oct 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Tue Sep 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Mon Sep 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-2m)
- delete patch0 (not used)

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-1m)
- initial build for Momonga Linux
