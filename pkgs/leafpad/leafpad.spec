%global momorel 4

Name:           leafpad
Version:        0.8.17
Release:        %{momorel}m%{?dist}

Summary:        GTK+ based simple text editor

Group:          Applications/Editors
License:        GPLv2+
URL:            http://tarot.freeshell.org/leafpad/
Source0:        http://savannah.nongnu.org/download/leafpad/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.4 desktop-file-utils libgnomeprintui22-devel
BuildRequires:	gettext
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils

%description
Leafpad is a GTK+ based simple text editor. The user interface is similar to
Notepad. It aims to be lighter than GEdit and KWrite, and to be as useful as
them.

%prep
%setup -q

%build
%configure --enable-chooser
make %{?_smp_mflags}
cat>>data/leafpad.desktop<<EOF
StartupNotify=true
GenericName=Text Editor
EOF

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
desktop-file-install --vendor= \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  --add-category X-Fedora \
  --add-category GTK \
  --delete-original \
  $RPM_BUILD_ROOT%{_datadir}/applications/leafpad.desktop
%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%{_bindir}/%{name}
%{_datadir}/applications/*%{name}.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/pixmaps/leafpad.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.17-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.17-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.17-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.17-1m)
- update to 0.8.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.16-1m)
- import from Rawhide
- update to 0.8.16

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.13-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb 10 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 0.8.13-1
- Upstream update

* Tue Aug 21 2007 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 0.8.11-2
- Fix License tag
- Rebuild for F8t2

* Fri Jul 20 2007 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 0.8.11-1
- Upstream update

* Fri May 11 2007 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 0.8.10le-1
- Upstream update

* Wed Sep 06 2006 Michael J. Knox <michael[AT]knox.net.nz> - 0.8.9-3
- Rebuild for FC6
- added BR of gettext

* Mon Apr 17 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.9-1
- Upstream update

* Mon Feb 13 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.7-2
- Rebuild for Fedora Extras 5

* Fri Feb  3 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.7-1
- Upstream update

* Sat Nov 27 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.5-1
- Upstream update

* Sat Oct  1 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.4-1
- Upstream update

* Thu Aug 18 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.3-2
- Rebuild for new Cairo

* Sat Jul 23 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.3-1
- Upstream update

* Thu May 19 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.1-1
- Upstream update

* Fri Apr 29 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.8.0-1
- Upstream update

* Fri Apr 22 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.9-7
- Used %%find_lang
- Cleaned up desktop entry generation a bit

* Thu Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sat Mar 19 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.9-5
- %%

* Sat Mar 19 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.9-4
- Added desktop-file-utils to BuildRequires

* Wed Mar 16 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.9-3
- Broke %%description at 80 columns

* Wed Mar 16 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.9-2
- Removed explicit Requires

* Tue Mar 15 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.7.9-1
- Bump release to 1

* Thu Feb  3 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0:0.7.9-0.iva.0
- Initial RPM release.
