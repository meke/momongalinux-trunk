%global	momorel 1
%define tp_glib_ver	0.17.5
%define zeitgeist_ver   0.3.14

Name:           folks
Version:        0.8.0
Release: %{momorel}m%{?dist}
Summary:        GObject contact aggregation library

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://telepathy.freedesktop.org/wiki/Folks
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.8/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires:  telepathy-glib-devel >= %{tp_glib_ver}
BuildRequires:  telepathy-glib-vala
BuildRequires:  libzeitgeist-devel >= %{zeitgeist_ver}
BuildRequires:  glib2-devel
BuildRequires:  vala-devel >= 0.15.2
BuildRequires:  vala-tools
BuildRequires:  libxml2-devel
BuildRequires:  gobject-introspection >= 0.9.12
BuildRequires:  GConf2-devel
BuildRequires:  evolution-data-server-devel >= 3.5.92
BuildRequires:  readline-devel
## BuildRequires: tracker-devel >= 0.10
BuildRequires:  pkgconfig(gee-1.0)


%description
libfolks is a library that aggregates people from multiple sources (e.g.
Telepathy connection managers and eventually evolution data server,
Facebook, etc.) to create meta-contacts.


%package        tools
Summary:        Tools for %{name}
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description    tools
%{name}-tools contains a database and import tool.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:	%{name}-tools = %{version}-%{release}
Requires:       telepathy-glib-devel >= %{tp_glib_ver}
Requires:       glib2-devel
Requires:       pkgconfig
Requires:	pkgconfig(gee-1.0)
Requires:	vala-devel >= 0.15.2
Requires:	vala-tools

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

%build
%configure --disable-static --enable-eds-backend --enable-vala --enable-inspect-tool --disable-libsocialweb-backend
%make V=1


%install
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
%find_lang %{name}


%post -p /sbin/ldconfig


%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas

%files -f %{name}.lang
%doc AUTHORS ChangeLog COPYING README
%{_libdir}/*.so.*
%{_libdir}/folks
%{_libdir}/girepository-1.0/Folks-0.6.typelib
%{_datadir}/GConf/gsettings/folks.convert
%{_datadir}/glib-2.0/schemas/org.freedesktop.folks.gschema.xml

%files tools
%{_bindir}/%{name}-import
%{_bindir}/%{name}-inspect

%files devel
%{_includedir}/folks
%{_libdir}/*.so
%{_libdir}/pkgconfig/folks*.pc
%{_datadir}/gir-1.0/Folks-0.6.gir
%{_datadir}/vala/vapi/%{name}*


%changelog
* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.4.1-1m)
- update to 0.7.4.1

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-2m)
- rebuild for evolution-data-server-devel-3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3
- import some fixes from upstream to support the latest eds and vala

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2.1-2m)
- rebuild for evolution-data-server-devel

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2.1-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-2m)
- rebuild for glib 2.33.2

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.6-1m)
- update to 0.6.6

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4.1-1m)
- update to 0.6.4.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3.2-1m)
- update 0.6.3.2

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3.1-1m)
- update 0.6.3.1

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-1m)
- update 0.6.3

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.6.2.1-2m)
- fix BuildRequires

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2.1-1m)
- update 0.6.2.1

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Mon Apr 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-2m)
- add BuildRequires

* Mon Apr 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-1m)
- initial build
