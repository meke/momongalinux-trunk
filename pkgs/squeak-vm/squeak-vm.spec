%global momorel 7

%define major   3.10
%define minor   6
%define vmver   %{major}-%{minor}
%define source  Squeak-%{vmver}

Name:           squeak-vm
Version:        %{major}.%{minor}
Release:        %{momorel}m%{?dist}
Summary:        The Squeak virtual machine

Group:          Development/Languages
License:        MIT
URL:            http://squeakvm.org/unix
Source0:        http://squeakvm.org/unix/release/%{source}.src.tar.gz
NoSource:       0
Source2:        squeak-desktop-files.tar.gz
Patch0:         squeak-vm-rpath.patch
Patch1:         squeak-vm-install-inisqueak.patch
Patch2:         squeak-vm-imgdir.patch
Patch3:         squeak-vm-tail-options.patch
Patch4:         Squeak-3.10-6-dprintf.patch

Requires(post): shared-mime-info
Requires(postun): shared-mime-info
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils

BuildRequires:  libX11-devel
BuildRequires:  libXt-devel
BuildRequires:  libvorbis-devel
BuildRequires:  libtheora-devel
BuildRequires:  speex-devel
BuildRequires:  dbus-devel
BuildRequires:  alsa-lib-devel
BuildRequires:  pango-devel
BuildRequires:  gstreamer-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:  libICE-devel
BuildRequires:  libSM-devel
BuildRequires:  libXext-devel
BuildRequires:  e2fsprogs-devel

#
# define nonXOplugins to be non-zero if you would like the plugins that 
# are unnecessary on the XO to be moved into a separate sub-package 
# to save space on the XO.  The list of plugins that are split out is
# listed below in the nonXOplugins files section
#
%define nonXOplugins 1


%description
Squeak is a full-featured implementation of the Smalltalk programming
language and environment based on (and largely compatible with) the original
Smalltalk-80 system.

This package contains just the Squeak virtual machine.

%if 0%{?nonXOplugins}
%package nonXOplugins
Summary:        Non-XO Plugins for the Squeak virtual machine
Group:          Development/Languages
Requires:       squeak-vm = %{version}-%{release}
%description nonXOplugins
Plugins for the Squeak virtual machine.
These plugins are unnecessary on the XO, and so are moved into a separate
sub-package to save space.
%endif


%prep
%setup -q -n %{source} -a 2

# The source files chmod'd here have the execute bit set in the upstream tarball
# which bothers rpmlint, need submit a request upstream to have this changed
find platforms -name '*.[ch]' -exec chmod ug=rw,o=r {} \;
##chmod ug=rw,o=r platforms/Cross/plugins/JPEGReadWriter2Plugin/*
##chmod ug=rw,o=r platforms/Cross/plugins/Mpeg3Plugin/libmpeg/*
##chmod ug=rw,o=r platforms/Cross/plugins/Mpeg3Plugin/libmpeg/audio/*
##chmod ug=rw,o=r platforms/Cross/plugins/Mpeg3Plugin/libmpeg/vidio/*
##chmod ug=rw,o=r platforms/unix/vm/osExports.c
##chmod ug=rw,o=r platforms/Cross/plugins/RePlugin/internal.h
##chmod ug=rw,o=r platforms/Cross/plugins/FilePlugin/sqFilePluginBasicPrims.c

%patch0 -p1 -b .rpath
%patch1 -p1 -b .install-inisqueak
%patch2 -p1 -b .imgdir
%patch3 -p1 -b .tail-options
%patch4 -p1 -b .dprintf

%build
mkdir -p bld
cd bld

CPPFLAGS=-DSUGAR ../platforms/unix/config/configure --prefix=%{_prefix} --mandir=%{_mandir} --datadir=%{_datadir} --libdir=%{_libdir}

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make -C bld install ROOT=%{buildroot}

# these files will be put in std RPM doc location
rm -rf %{buildroot}%{_prefix}/doc/squeak

# install the desktop stuff
install -D --mode=u=rwx,go=rx mysqueak %{buildroot}%{_bindir}/mysqueak
install -D --mode=u=rw,go=r mysqueak.1 %{buildroot}%{_mandir}/man1/mysqueak.1
install -D --mode=u=rw,go=r squeak.xml %{buildroot}%{_datadir}/mime/packages/squeak.xml
install -D --mode=u=rw,go=r squeak.desktop %{buildroot}%{_datadir}/applications/squeak.desktop
install -D --mode=u=rw,go=r squeak.png %{buildroot}%{_datadir}/pixmaps/squeak.png

%define icons_dir %{buildroot}%{_datadir}/icons/gnome
for size in 16 24 32 48 64 72 96
do
  mkdir -p %{icons_dir}/${size}x${size}/mimetypes
  install -m0644 squeak${size}.png %{icons_dir}/${size}x${size}/mimetypes/application-x-squeak-image.png
  install -m0644 squeaksource${size}.png %{icons_dir}/${size}x${size}/mimetypes/application-x-squeak-source.png
done

# If an image cant find the .sources in the current directory it will look 
# in %{_libdir}/squeak/%{vmver}
cd %{buildroot}%{_libdir}/squeak/%{vmver}
DOTDOTS=$(echo %{_libdir}/squeak/%{vmver} | sed -e 's:/[^/]\+:../:g')
ln -s ${DOTDOTS}%{_datadir}/squeak/SqueakV39.sources . 
ln -s ${DOTDOTS}%{_datadir}/squeak/SqueakV3.sources .

%ifnarch %{ix86} ppc
rm -f %{buildroot}%{_libdir}/squeak/%{vmver}/SqueakFFIPrims
%endif

%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/gnome || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
    %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/gnome || :
fi
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :


%postun
touch --no-create %{_datadir}/icons/gnome || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
    %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/gnome || :
fi
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :




%files
%defattr(-,root,root,-)
%doc platforms/unix/ChangeLog platforms/unix/doc/{README*,LICENSE,*RELEASE_NOTES}
%{_bindir}/*
%dir %{_libdir}/squeak
%dir %{_libdir}/squeak/%{vmver}
%if 0 == 0%{?nonXOplugins}
%{_libdir}/squeak/%{vmver}/FileCopyPlugin
%{_libdir}/squeak/%{vmver}/B3DAcceleratorPlugin
%{_libdir}/squeak/%{vmver}/PseudoTTYPlugin
%{_libdir}/squeak/%{vmver}/UnixOSProcessPlugin
%{_libdir}/squeak/%{vmver}/XDisplayControlPlugin
%ifarch %{ix86} ppc
%{_libdir}/squeak/%{vmver}/SqueakFFIPrims
%endif
%else
%{_libdir}/squeak/%{vmver}/AioPlugin
%{_libdir}/squeak/%{vmver}/ClipboardExtendedPlugin
%{_libdir}/squeak/%{vmver}/DBusPlugin
%{_libdir}/squeak/%{vmver}/GStreamerPlugin
%{_libdir}/squeak/%{vmver}/ImmX11Plugin
%{_libdir}/squeak/%{vmver}/KedamaPlugin
%{_libdir}/squeak/%{vmver}/KedamaPlugin2
%{_libdir}/squeak/%{vmver}/MIDIPlugin
%{_libdir}/squeak/%{vmver}/OggPlugin
%{_libdir}/squeak/%{vmver}/RomePlugin
%{_libdir}/squeak/%{vmver}/Squeak3D
%{_libdir}/squeak/%{vmver}/UUIDPlugin
## disable VideoForLinuxPlugin for the moment, should be fixed ASAP
# %%{_libdir}/squeak/%%{vmver}/VideoForLinuxPlugin
%{_libdir}/squeak/%{vmver}/HostWindowPlugin

%{_libdir}/squeak/%{vmver}/SqueakV3.sources
%{_libdir}/squeak/%{vmver}/SqueakV39.sources
%{_libdir}/squeak/%{vmver}/npsqueak.so
%{_libdir}/squeak/%{vmver}/squeak
%{_libdir}/squeak/%{vmver}/vm-display-X11
%{_libdir}/squeak/%{vmver}/vm-display-fbdev
%{_libdir}/squeak/%{vmver}/vm-display-null
%{_libdir}/squeak/%{vmver}/vm-sound-ALSA
%{_libdir}/squeak/%{vmver}/vm-sound-NAS
%{_libdir}/squeak/%{vmver}/vm-sound-OSS
%{_libdir}/squeak/%{vmver}/vm-sound-null
%endif
%{_mandir}/man*/*
%dir %{_datadir}/squeak
%{_datadir}/squeak/*
%{_datadir}/applications/*
%{_datadir}/pixmaps/*
%{_datadir}/mime/packages/*
%{_datadir}/icons/gnome/*/mimetypes/*.png


%if 0%{?nonXOplugins}
%files nonXOplugins
%defattr(-,root,root,-)
%{_libdir}/squeak/%{vmver}/FileCopyPlugin
%{_libdir}/squeak/%{vmver}/B3DAcceleratorPlugin
%{_libdir}/squeak/%{vmver}/PseudoTTYPlugin
%{_libdir}/squeak/%{vmver}/UnixOSProcessPlugin
%{_libdir}/squeak/%{vmver}/XDisplayControlPlugin
%ifarch %{ix86} ppc
%{_libdir}/squeak/%{vmver}/SqueakFFIPrims
%endif
%endif

%changelog
* Mon Jul 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.6-7m)
- disable VideoForLinuxPlugin for the moment, should be fixed ASAP

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.6-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.6-2m)
- remove SqueakFFIPrims on x86_64. it works on ix86 or ppc only.

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.6-1m)
- update to 3.10.6 with glibc210 patch

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.4-2m)
- fix build on x86_64

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.4-1m)
- import from Fedora 11
- temporarily add %%{_libdir}/squeak/%%{vmver}/vm-sound-NAS

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.10.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 20 2009 Gavin Romig-Koch <gavin@redhat.com> - 3.10.4-3
   - SqueakFFIPrims only works on ix86 and ppc
   - add a missing dir macro
   - correct the requires on -nonXOplugins
   - correct handling of no nonXOplugins
* Mon Jan 19 2009 Gavin Romig-Koch <gavin@redhat.com> - 3.10.4-3
   - add missing build dependancy for UUIDPlugin
   - add missing defattr macro
   - corrections needed by rpmlint
* Sat Jan 17 2009 Gavin Romig-Koch <gavin@redhat.com> - 3.10.3-4
   - upgrade to upstream 3.10.3-4
   - switch from svn to tarball as upstream source
* Wed Jan 07 2009 Gavin Romig-Koch <gavin@redhat.com> - 3.10.3-3
  - split out nonXOplugins
  - corrected script requirements
* Tue Nov 25 2008 Gavin Romig-Koch <gavin@redhat.com> - 3.10.3-2
  - Fix rpmlint problems
    - mode of squeak-vm-tarball-from-svn.sh
    - squeak.desktop: Icon, MimeType, Encoding
    - move the _libdir .source links into this, non-noarch, rpm
    - remove x mode of a number of .c and .h source files from upstream
    - use explicit --mode when installing files
    - replace tabs with spaces
    - shbang for mysqueak
* Tue Jul 29 2008 Gavin Romig-Koch <gavin@redhat.com> - 3.10.3-1
  - incorporated slightly modified Desktop related changes from 
    Gerard Milmeister <gemi@bluewin.ch> 
  - update .spec to Fedora Guidelines
  - add mysqueak
  - olpc macro to distinguish olpc builds 
  - distinguish between squeak image icons, and squeak source icons
* Mon Jul 07 2008 Bert Freudenberg <bert@freudenbergs.de>
  - 3.10-3olpc5; SVN r. 1899
  - re-enable new (IPv6) socket primitives (bf)
  - RomePlugin pango fixes (yo)
* Mon Jun 23 2008 Bert Freudenberg <bert@freudenbergs.de>
  - 3.10-3olpc4; SVN r. 1895
  - RomePlugin pango fixes (yo)
  - X11 drag-and-drop fixes (tak)
* Wed Jun 03 2008 Bert Freudenberg <bert@freudenbergs.de>
  - 3.10-3olpc3; SVN r.1889
  - prefer 24 and 16 bpp over 32 to fix alpha issue in compositing desktops (bf)
* Mon May 19 2008 Bert Freudenberg <bert@freudenbergs.de>
  - 3.10-3olpc2; SVN r.1879
  - re-add big cursor support (bf)
* Wed May 14 2008 Bert Freudenberg <bert@freudenbergs.de>
  - 3.10-3olpc1; SVN r. 1878
  - updated DBus plugin (incompatible w/ previous versions)
  - add plugins: GStreamer, ImmX11, Aio
  - fixed international keyboard input
  - merged with trunk
* Fri Jan 04 2008 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-12olpc5; SVN r. 1811
  - remove Mpeg3Plugin
  - fix KedamaPlugin2 (exports where not listed) 
  - fix show-source key
  - fix SEGFAULT in OggPlugin
  - add RomePlugin w/ Pango support
  - fix drag-n-drop
* Mon Dec 03 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-12olpc4: SVN r. 1795
  - fix dbus plugin not zero-terminating some strings (bf)
* Wed Oct 31 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-12olpc3: SVN r. 1787
  - fixed errors about state checking in drag and drop (tak)
  - make cursor keys work with utf32 char code (yo)
  - add SetSwitch, GetSwitch, and SetDevice mixer functions (yo)
* Tue Oct 23 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-12olpc2: SVN r. 1779
  - use CPPFLAGS not CFLAGS for -DSUGAR
  - handle view-source key (bf)
* Mon Oct 15 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-12olpc1: SVN r. 1776
  - merged with trunk (bf)
  - signed oop fixes (dave)
  - unicode key events (ian)
  - fix dbus plugin (bf)
* Thu Aug 30 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-11olpc5: SVN r. 1761
  - set _NET_WM_PID to our process id (danw)
  - generate WindowEventClose on delete window message (ian)
* Thu Jul 17 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-11olpc4: SVN r. 1747
  - added LocalePlugin (ian)
  - FileCopyPlugin is external now
  - follow symlinks in npsqueakregister (bf)
  - explicitely link against libXt (bf)
* Thu Jul 12 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-11olpc3: SVN r. 1740
  - fix spec file clean, add _smp_mflags as requested by fedora (bf)
  - XdndFinished is sent properly in drag-in with multiple types (tak)
  - Show cursor forced when drag-out (tak)
  - Fixed a bug about type index for clipboard (tak)
* Mon Jul 02 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-11olpc2: SVN r. 1735
  - clipboard/dns fixes (tak)
  - update spec to satisfy rpmlint
* Tue Jun 26 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-11olpc1: SVN r. 1728
  - IPv6 support (ian)
  - updated DBus plugin (impara, bf)
  - better drag-and-drop support, extended clipboard support (tak)
* Fri Jun 22 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-10olpc7: SVN r.1723
  - move build env to Fedora 7
  - require libdbus-1.so.3
  - configure without OpenGL (rather than hiding GL libs)
* Thu May 17 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-10olpc6: SVN r.1687
  - add ClipboardExtendedPlugin (tak)
  - add DBusPlugin (impara, bf)
  - support keypad keys (bf)
* Thu Apr 19 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-10olpc5: SVN r.1658
  - enabled Kedama plugin
* Wed Apr 04 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-10olpc4: SVN r.1652 (moved to OLPC branch)
  - added ogg plugin (tak)
* Fri Mar 23 2007 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-10olpc3: SVN r.1613 + olpc.patch
  - big cursor support (bf)
  - faster camera input (dgd)
* Wed Nov 09 2006 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-10olpc1: SVN r.1591
  - work around ALSA SIGIO issue (bf)
  - added dgd's camera support (V4L plugin)
* Wed Nov 09 2006 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-9olpc4: SVN r.1571
  - compile without GL
* Wed Oct 18 2006 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-9olpc3: SVN r.1571
  - relicensed to MIT
* Tue Oct 17 2006 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-9olpc2: SVN r.1566
  - yet another ALSA fix
* Thu Oct 12 2006 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-9olpc1: SVN r.1563
  - fix ALSA prim failed
  - add warpblt optimization
  - add Kedama2 plugin
  - exclude unneeded plugins
* Wed Oct 11 2006 Bert Freudenberg <bert@freudenbergs.de>
  - 3.9-8olpc2: SVN r.1557 adds access to ALSA mixer
* Fri Oct 06 2006 Bert Freudenberg <bert@freudenbergs.de>
  - initial RPM for OLPC (3.9-8olpc1)
