%global momorel 4

Name: hunspell-da
Summary: Danish hunspell dictionaries
Version: 1.7.31
Release: %{momorel}m%{?dist}
Source: http://da.speling.org/filer/myspell-da-%{version}.tar.bz2
Group: Applications/Text
URL: http://da.speling.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Danish hunspell dictionaries.

%prep
%setup -q -n myspell-da-%{version}

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README Copyright contributors COPYING
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.31-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.31-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.31-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.31-1m)
- update to 1.7.31

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.27-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.27-1m)
- update to 1.7.27

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.19-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.19-1m)
- import from Fedora to Momonga

* Mon Mar 17 2008 Caolan McNamara <caolanm@redhat.com> - 1.7.19-1
- latest version

* Wed Feb 13 2008 Caolan McNamara <caolanm@redhat.com> - 1.7.18-1
- latest version

* Wed Nov 28 2007 Caolan McNamara <caolanm@redhat.com> - 1.7.17-1
- latest version

* Fri Oct 05 2007 Caolan McNamara <caolanm@redhat.com> - 1.7.16-1
- latest version

* Thu Aug 30 2007 Caolan McNamara <caolanm@redhat.com> - 1.7.15-1
- latest version

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 1.7.14-2
- clarify license version

* Mon Jul 30 2007 Caolan McNamara <caolanm@redhat.com> - 1.7.14-1
- latest version

* Mon Jul 09 2007 Caolan McNamara <caolanm@redhat.com> - 1.7.13-1
- latest version

* Thu Feb 13 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070106-1
- new upstream dictionaries

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20050330-1
- initial version
