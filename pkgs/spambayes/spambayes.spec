%global momorel 8

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           spambayes
Version:        1.0.4
Release:        %{momorel}m%{?dist}
Summary:        Bayesian anti-spam filter

Group:          Development/Languages
License:        "Python"
URL:            http://spambayes.sourceforge.net
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
Patch0:         %{name}-1.0.4-py25.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7

BuildRequires: python-setuptools-devel

%description
SpamBayes will attempt to classify incoming email messages as 'spam', 'ham'
(good, non-spam email) or 'unsure'. This means you can have spam or unsure
messages automatically filed away in a different mail folder, where it won't
interrupt your email reading. First SpamBayes must be trained by each user to
identify spam and ham. Essentially, you show SpamBayes a pile of email that
you like (ham) and a pile you don't like (spam). SpamBayes will then analyze
the piles for clues as to what makes the spam and ham different. For example;
different words, differences in the mailer headers and content style.  The
system then uses these clues to examine new messages.


%prep
%setup -q
%patch0 -p0 -b .py25~

# Fix rpmlint warnings
chmod -x *.txt

# remove shell bangs
pushd %{name}
for file in $(find . -type f -name "*.py"); do
  cp $file $file.orig
  grep -v -e "^\#\!" $file.orig > $file
  rm -f $file.orig
done
popd


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} -c 'import setuptools; execfile("setup.py")' build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} -c 'import setuptools; execfile("setup.py")' install --skip-build --root $RPM_BUILD_ROOT

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc *.txt
%{python_sitelib}/*
%{_bindir}/*
#%%exclude %{_bindir}/*.pyo
#%%exclude %{_bindir}/*.pyc


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.4-2m)
- rebuild against python-2.6.1-1m

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-1m)
- import from Fedora to Momonga

* Wed Oct 17 2007 Christopher Stone <chris.stone@gmail.com> 1.0.4-5
- Providing Eggs for non-setuptools packages (bz#325041)

* Fri Apr 27 2007 Christopher Stone <chris.stone@gmail.com> 1.0.4-4
- Remove python from package name

* Thu Apr 05 2007 Christopher Stone <chris.stone@gmail.com> 1.0.4-3
- Add patch to fix python2.5 errors

* Thu Apr 05 2007 Christopher Stone <chris.stone@gmail.com> 1.0.4-2
- %%exclude pyo and pyc files from %%{_bindir}
- Add scriptlet to remove shebangs

* Sat Mar 31 2007 Christopher Stone <chris.stone@gmail.com> 1.0.4-1
- Initial Fedora release
