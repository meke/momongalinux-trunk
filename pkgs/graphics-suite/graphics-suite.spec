%global momorel 2

Summary:     Meta package for Graphics
Name:        graphics-suite
Version:     8.0
Release:     %{momorel}m%{?dist}
License:     GPL and LGPL and MPL and Modified BSD
Group:       Applications/Multimedia
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:    ImageMagick
Requires:    blender
Requires:    dcraw
Requires:    dcraw-gimp
Requires:    dia
Requires:    gqview
Requires:    gimp
Requires:    gimp-gap
Requires:    gutenprint
Requires:    gutenprint-cups
Requires:    gimp-pygimp
Requires:    inkscape
Requires:    k3d
Requires:    libsane-hpaio
Requires:    netpbm-progs
Requires:    rawstudio
Requires:    rawtherapee
Requires:    sane-frontends
Requires:    sodipodi
Requires:    ufraw
Requires:    x3f-tools
Requires:    xaralx
Requires:    xfig
Requires:    xsane
Requires:    xsane-gimp

%description
This is meta package for Graphics.
Listed packages are independent of all desktop environments.
(NOT GNOME, NOT KDE, NOT XFCE4)

%files

%changelog
* Sat Jan 25 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8.0-2m)
- remove zphoto.

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0-1m)
- version 8.0
- remove Requires: gutenprint-utils

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-3m)
- full rebuild for mo7 release

* Tue Apr 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-2m)
- add Requires: rawtherapee

* Mon Jan 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-1m)
- add Requires: rawstudio and ufraw

* Thu Dec 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-5m)
- drop gimp-help

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-4m)
- temporarily drop gimp-help

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0-2m)
- add Requires: x3f-tools

* Sun Jun 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0-1m)
- version 6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc43

* Mon Dec 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- good-bye gimp-print and hello gutenprint

* Tue Apr 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- initial package for Momonga Linux
