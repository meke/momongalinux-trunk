%global momorel 11
%global catalogue %{_sysconfdir}/X11/fontpath.d

Summary: The free fonts to display the AA (ascii art) in 2ch
Name: mona-fonts
Version: 2.90
Release: %{momorel}m%{?dist}
License: Modified BSD and Public Domain
Group: User Interface/X
URL: http://monafont.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/monafont/monafont-%{version}.tar.bz2
NoSource: 0
Patch0: mona-fonts-Makefile.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: fontpackages-devel
BuildRequires: gzip perl sed xorg-x11-font-utils

%description
Mona fonts are free fonts to display the AA (ascii art) in 2ch.

%prep
%setup -q -c -n monafont-%{version}
pushd ..
cat %{SOURCE0} | bzip2 -dc | tar xf -
popd
%patch0 -p0 -b .make

%build
make

%install
rm -rf --preserve-root %{buildroot}

make X11FONTDIR=%{buildroot}%{_fontdir} install
install -m 644 fonts.alias.mona %{buildroot}%{_fontdir}/fonts.alias

mkdir -p %{buildroot}%{catalogue}
ln -sf %{_fontdir} %{buildroot}%{catalogue}/%{name}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.euc README.ascii
%{catalogue}/%{name}
%dir %{_fontdir}
%{_fontdir}/*pcf.gz
%verify(not md5 size mtime) %{_fontdir}/fonts.alias
%verify(not md5 size mtime) %{_fontdir}/fonts.dir

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.90-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.90-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.90-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.90-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.90-7m)
- correct font catalogue
- remove PreReq: chkfontpath-momonga

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.90-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.90-5m)
- rebuild against gcc43

* Mon May  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.90-4m)
- change xorg-x11 -> xorg-x11-fonts-base

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.90-3m)
- revise for xorg-7.0
- change install dir

* Thu Jan 20 2005 TABUCHI Takaaki <tab@momnga-linux.org>
- (2.90-2m)
- revise Source0 URI
- change BuildPreReq XFree86 to xorg-x11

* Tue Jan 13 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.90-1m)
- update to version 2.90

* Thu Nov 13 2003 zunda <zunda at freeshell.org>
- (2.30pre2-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- OBSOLETed suffering from kochi-font: see README.euc

* Tue Aug 19 2003 TABUCHI Takaaki <tab@momnga-linux.org>
- (2.30pre2-1m)
- update to 2.30pre2
- change file name README to README.euc README.ascii

* Thu May 30 2002 Hidetomo Machi <mcht@kondara.org>
- (2.2-2k)
- update to 2.2

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.11-4k)

* Sun Mar 24 2002 Hidetomo Machi <mcht@kondara.org>
- (2.11-2k)
- update to 2.11
- include fonts.alias
- add PreReq

* Mon Feb 25 2002 Kenta MURATA <muraken2@nifty.com>
- (2.1-2k)
- first release.
