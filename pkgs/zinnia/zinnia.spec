%global momorel 17

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Summary:	Online handwriting recognition system with machine learning
Name:		zinnia
Version:	0.06
Release:	%{momorel}m%{?dist}

Group:		System Environment/Libraries
License:	Modified BSD
URL:		http://zinnia.sourceforge.net/
Source0:	http://downloads.sourceforge.net/zinnia/%{name}-%{version}.tar.gz
NoSource:       0
Source1:        http://zinnia.svn.sourceforge.net/viewvc/zinnia/zinnia/tomoe2s.pl
Source2:        Makefile.tomoe
Patch0:		zinnia-0.05-bindings.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  db4-devel, python-devel >= 2.7
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  tomoe

%description
Zinnia provides a simple, customizable, and portable dynamic OCR
system for hand-written input, based on Support Vector Machines.

Zinnia simply receives user pen strokes as coordinate data and outputs
the best matching characters sorted by SVM confidence. To maintain
portability, it has no rendering functionality. In addition to
recognition, Zinnia provides a training module capable of creating
highly efficient handwriting recognition models.

This package contains the shared libraries.

%package        devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package 	utils
Summary:	Utils for the zinnia library
Group:		Applications/System
Requires:	%{name} = %{version}-%{release}

%description	utils
The %{name}-utils package provides utilities for zinnia library that 
use %{name}.

%package 	doc
Summary:	Documents for the zinnia library
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
BuildArch:	noarch

%description	doc
The %{name}-doc package provide documents for zinnia library that 
use %{name}.

%package  	perl
Summary:	Perl bindings for %name
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))


%description 	perl
This package contains perl bindings for %{name}.

%package 	python
Summary:	Python bindings for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description 	python
This package contains python bindings for %{name}.

%package	tomoe
Summary:        Tomoe model file for %{name}
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}
BuildArch:      noarch

%description	tomoe
This package contains tomoe model files for %{name}.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .bindings
find . -type f -name ChangeLog -size 0c -exec rm -f {} ';'
find . -type f -name "*.pyc" -exec rm -f {} ';'
cp %{SOURCE1} .
cp %{SOURCE2} .
pushd doc
iconv -f latin1 -t utf8 zinnia.css > zinnia.css.bak 
mv -f zinnia.css.bak zinnia.css
popd

%build
%configure --disable-static
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}
make -f Makefile.tomoe build

pushd perl
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS"
make %{?_smp_mflags}
popd

pushd python
CFLAGS="$RPM_OPT_FLAGS -I../" LDFLAGS="-L../.libs" python setup.py build
popd

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
make -f Makefile.tomoe install DESTDIR=$RPM_BUILD_ROOT

#install -d -m 0755 -p $RPM_BUILD_ROOT%{_docdir}/%{name}
#cp -pfr doc $RPM_BUILD_ROOT%{_docdir}/%{name}

pushd perl
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
popd

pushd python
python setup.py install --root $RPM_BUILD_ROOT
pushd

#remove something unnecessary
find $RPM_BUILD_ROOT -type f -name "*.la" -exec rm -f {} ';'
find $RPM_BUILD_ROOT -type f -name "*.bs" -size 0c -exec rm -f {} ';'
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} ';'

#change the privilege of some files
chmod 0755 $RPM_BUILD_ROOT%{perl_vendorarch}/auto/%{name}/%{name}.so


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc README COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/lib%{name}.so

%{_libdir}/pkgconfig/%{name}.pc

%files utils
%defattr(-,root,root,-)
%{_bindir}/zinnia
%{_bindir}/zinnia_convert
%{_bindir}/zinnia_learn

%files doc
%defattr(-,root,root,-)
%doc doc/*


%files	perl
%defattr(-,root,root,-)
%{perl_vendorarch}/auto/%{name}/
%{perl_vendorarch}/%{name}.pm

%files	python
%defattr(-,root,root,-)
%{python_sitearch}/_%{name}.so
%{python_sitearch}/%{name}*

%files tomoe
%defattr(-,root,root,-)
%dir %{_datadir}/zinnia/model/tomoe/
%{_datadir}/zinnia/model/tomoe/handwriting-ja.model
%{_datadir}/zinnia/model/tomoe/handwriting-zh_CN.model

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-17m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-16m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-15m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-14m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-13m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-12m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-11m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-10m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-9m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-8m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-7m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.06-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.06-2m)
- full rebuild for mo7 release

* Fri Jun 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-1m)
- update to 0.06
- create zinnia-tomoe sub-package

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-3m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-2m)
- rebuild against perl-5.12.0

* Sat Apr 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.05-1m)
- import from Fedora to Momonga for tegaki-python

* Wed Mar 10 2010 Liang Suilong <liangsuilong@gmail.com> - 0.05-4
- Fix the bugs of SPEC file

* Fri Mar 04 2010 Liang Suilong <liangsuilong@gmail.com> - 0.05-3
- Fix something wrong of spec file

* Wed Mar 02 2010 Liang Suilong <liangsuilong@gmail.com> - 0.05-2
- Rename Subpackage for perl and python

* Tue Feb 02 2010 Liang Suilong <liangsuilong@gmail.com> - 0.05-1
- Initial Package
