%global         momorel 1
%global         srcname Image-ExifTool

Name:           perl-Image-ExifTool
Version:        9.60
Release:        %{momorel}m%{?dist}
Summary:        Read and write meta information
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Image-ExifTool/
Source0:        http://www.cpan.org/authors/id/E/EX/EXIFTOOL/%{srcname}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  coreutils
BuildRequires:  findutils
BuildRequires:  perl-Archive-Zip
BuildRequires:  perl-Digest-MD5
BuildRequires:  perl-Digest-SHA
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO-Compress
Requires:       perl-Archive-Zip
Requires:       perl-Digest-MD5
Requires:       perl-Digest-SHA
Requires:       perl-IO-Compress
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
ExifTool provides an extensible set of perl modules to read and write meta
information in a wide variety of files, including the maker note
information of many digital cameras by various manufacturers such as Canon,
Casio, FujiFilm, HP, JVC/Victor, Kodak, Leaf, Minolta/Konica-Minolta,
Nikon, Olympus/Epson, Panasonic/Leica, Pentax/Asahi, Ricoh, Samsung, Sanyo,
Sigma/Foveon and Sony.

%prep
%setup -q -n %{srcname}-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean 
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changes README
%{_bindir}/exiftool
%{perl_vendorlib}/File/RandomAccess.*
%{perl_vendorlib}/Image/ExifTool*
%{_mandir}/man1/exiftool.1*
%{_mandir}/man3/File::RandomAccess.3pm*
%{_mandir}/man3/Image::ExifTool*.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (9.60-1m)
- rebuild against perl-5.20.0
- update to 9.60

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (9.53-1m)
- update to 9.53

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (9.46-1m)
- update to 9.46
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.27-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.27-2m)
- rebuild against perl-5.18.0

* Mon Apr 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.27-1m)
- update to 9.27

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.25-1m)
- update to 9.25

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.13-2m)
- rebuild against perl-5.16.3

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.13-1m)
- update to 9.13

* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.12-1m)
- update to 9.12

* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.11-1m)
- update to 9.11

* Sun Nov  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.04-1m)
- update to 9.04

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.01-2m)
- rebuild against perl-5.16.2

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9,01-1m)
- update to 9.01

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.90-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.90-1m)
- update to 8.90
- rebuild against perl-5.16.0

* Sun Mar 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.85-1m)
- update to 8.85

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.77-1m)
- update to 8.77

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.75-1m)
- update to 8.75

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.65-2m)
- rebuild against perl-5.14.2

* Sun Sep 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.65-1m)
- update to 8.65

* Tue Jun 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.60-1m)
- update to 8.60

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.50-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.50-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sat Apr 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.50-1m)
- update to 8.50

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.40-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.40-2m)
- rebuild for new GCC 4.5

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.40-1m)
- update to 8.40

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.25-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.25-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.25-1m)
- update to 8.25

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.15-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.15-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.15-1m)
- update to 8.15

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.10-1m)
- update to 8.10

* Sun Dec  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.01-1m)
- initial package for hugin-2009.2.0
