%global momorel 7
Summary:        User configurable send-only Mail Transfer Agent
Summary(de):    Benutzerkonfigurierbarer nur versendender Mail Transfer Agent (MTA)
Name:           esmtp
Version:        1.0
Release: %{momorel}m%{?dist}
Source:         http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1:        esmtp-0.4.1-mutt
# esmtp system config file configuring procmail as mda, for the local-delivery
# sub-package
Source2:        esmtprc-mda
Url:            http://esmtp.sourceforge.net/
# no license in files. Some come from fetchmail, another from libesmtp
# esmtp-wrapper is GPLv2+
License:        GPL+ and GPLv2+
Group:          Applications/Internet
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(post):  %{_sbindir}/alternatives
Requires(preun): %{_sbindir}/alternatives
BuildRequires:   libesmtp-devel >= 1.0.6
# these files are set up with alternatives
Provides:        %{_sbindir}/sendmail
Provides:        %{_bindir}/mailq
# for esmtp-wrapper
Requires:       coreutils
# esmtp doesn't listen on port 25, so it cannot provide server(smtp). 
# This implies that any program requiring a program that sends mail
# on port 25 should rely on another package than esmtp to fulfill the 
# dependency.
#Provides:       server(smtp)

%description
ESMTP is a user configurable relay-only Mail Transfer Agent (MTA) with a
sendmail-compatible syntax. It's based on libESMTP supporting the AUTH
(including the CRAM-MD5 and NTLM SASL mechanisms) and the StartTLS SMTP
extensions.

%description -l de
ESMTP ist ein benutzerkonfigurierbarer nur versendender Mail Transfer
Agent (MTA) mit einem Sendmail-kompatiblen Syntax. Es basiert auf 
libESMTP und unterstützt AUTH (mit CRAM-MD5 und NTLM SASL) und StartTLS
SMTP.

%package local-delivery
Summary:        Configuration for esmtp allowing for local delivery
Group:          Applications/Internet
Requires:       %{name} = %{version}-%{release}
Requires:       procmail
Provides:       mail(local)

%description local-delivery
This packages contains the system ESMTP configuration file with local
delivery through an external mail delivery agent configured.

%prep
%setup -q
cp -p %{SOURCE1} mutt-esmtp
for file in esmtp.1 esmtprc.5; do
   iconv -f ISO8859-1 -t UTF8 < $file > $file.new && touch -r $file $file.new && mv -f $file.new $file
done

%build
export CFLAGS=`pkg-config libesmtp --cflags`
export LDFLAGS=`pkg-config libesmtp --libs`
%configure
%make 

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
mkdir -p %{buildroot}%{_sysconfdir}
install -p -m0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/esmtprc
install -p -m0755 esmtp-wrapper %{buildroot}%{_bindir}
# remove all the sendmail alternative installed, it is done in %post
rm %{buildroot}%{_bindir}/mailq \
   %{buildroot}%{_bindir}/newaliases \
   %{buildroot}%{_libdir}/sendmail \
   %{buildroot}%{_sbindir}/sendmail \
   %{buildroot}%{_mandir}/man1/mailq.1 \
   %{buildroot}%{_mandir}/man1/newaliases.1 \
   %{buildroot}%{_mandir}/man1/sendmail.1

%post
# newaliases is fake, so don't install the links.
%{_sbindir}/alternatives --install %{_sbindir}/sendmail mta %{_bindir}/esmtp-wrapper 30 \
  --slave %{_prefix}/lib/sendmail mta-sendmail %{_bindir}/esmtp-wrapper \
  --slave %{_mandir}/man8/sendmail.8.bz2 mta-sendmailman %{_mandir}/man1/esmtp.1.bz2 \
  --slave %{_bindir}/mailq mta-mailq %{_bindir}/esmtp-wrapper \
  --slave %{_mandir}/man1/mailq.1.bz2 mta-mailqman %{_mandir}/man1/esmtp.1.bz2

#  --slave %{_bindir}/newaliases mta-newaliases %{_bindir}/esmtp \
#  --slave %{_mandir}/man1/newaliases.1.bz2 mta-newaliasesman %{_mandir}/man1/esmtp.1.bz2 \

%preun
if [ "$1" = 0 ]; then
   %{_sbindir}/alternatives --remove mta %{_bindir}/esmtp-wrapper
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README TODO sample.esmtprc mutt-esmtp
%{_bindir}/esmtp-wrapper
%{_bindir}/esmtp
%{_mandir}/man[^3]/esmtp*

%files local-delivery
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/esmtprc

%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- reimport from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- sync with Fedora 11 (1.0-4)
5A
* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-2m)
- rebuild against rpm-4.6

* Wed Jul  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0
- use NoSource

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-1m)
- import from f7 to Momonga
- use %%{buildroot}
- care about man/alternatives suffix (.gz -> .bz2)

* Mon Sep 11 2006 Patrice Dumas <pertusus@free.fr> 0.5.1-13
- rebuild for FC6

* Thu Jun 22 2006 Patrice Dumas <pertusus@free.fr> 0.5.1-12
- provide an alternative for /usr/lib/sendmail even if %%_lib isn't lib
  fix 196277

* Thu Feb 16 2006 Patrice Dumas <pertusus@free.fr> 0.5.1-11
- rebuild for fc5

* Sun Jan  8 2006 Patrice Dumas <pertusus@free.fr> 0.5.1-10
- convert man pages to utf8 (Dmitry Butskoy report)

* Wed Nov 16 2005 Patrice Dumas <pertusus@free.fr> 0.5.1-9
- remove the workaround for libesmtp not requiring openssl (#166844 closed)

* Sun Nov 13 2005 Patrice Dumas <pertusus@free.fr> 0.5.1-8
- rebuild against new openssl

* Mon Aug 29 2005 Patrice Dumas <pertusus@free.fr> 0.5.1-7
- uncomment german translation

* Fri Aug 26 2005 Patrice Dumas <pertusus@free.fr> 0.5.1-6
- add temporarily a BuildRequires: openssl-devel to workaround missing 
  Requires: of libesmtp-devel (#166844)

* Fri Aug 26 2005 Patrice Dumas <pertusus@free.fr> 0.5.1-5
- comment out german translation
- cleanups (thanks Aurelien Bompard)

* Sat Mar 12 2004 Patrice Dumas <pertusus@free.fr> 0.5.1-2
- Use alternatives

* Sat Mar 12 2004 Patrice Dumas <pertusus@free.fr> 0.5.1-1
- Use fedora-newrpmspec to update the spec file
- Package sendmail replacements

* Sat Nov 15 2003 Robert Scheck <esmtp@robert-scheck.de> 0.5.0-1
- Update to 0.5.0
- Added german description and summary

* Mon Oct 27 2003 Robert Scheck <esmtp@robert-scheck.de> 0.4.1-1
- Update to 0.4.1
- Initial Release for Red Hat Linux
