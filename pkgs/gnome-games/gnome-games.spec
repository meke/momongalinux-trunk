%global momorel 1
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define gettext_package gnome-games

%define build_tali 1
%define have_sudoku 1

%if %{build_tali}
%define gtali gtali
%else
%define gtali %{nil}
%endif

%if !%{build_tali}
%define omitgames --enable-omitgames=gtali
%else
%define omitgames %{nil}
%endif

%if %{have_sudoku}
%define sudoku gnome-sudoku
%else
%define sudoku %{nil}
%endif

%define glib2_version 2.32.0
%define pango_version 1.29.0
%define desktop_file_utils_version 0.2.90
%define gstreamer_version 1.0.0

Summary: Games for the GNOME desktop
Name: gnome-games
Version: 3.6.0.2
Release: %{momorel}m%{?dist}
License: GPLv2+ and GPLv3 and GFDL
Group: Amusements/Games
#VCS: git:git://git.gnome.org/gnome-games
Source: http://download.gnome.org/sources/gnome-games/3.6/gnome-games-%{version}.tar.xz
NoSource: 0

Obsoletes: gnome-games-devel < %{version}-%{release}
URL: http://projects.gnome.org/gnome-games/

Requires: pygobject228
Requires: hicolor-icon-theme
Requires: gobject-introspection

BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: pango-devel >= %{pango_version}
BuildRequires: gtk3-devel
BuildRequires: pygobject2-devel
BuildRequires: desktop-file-utils >= %{desktop_file_utils_version}
BuildRequires: librsvg2-devel
BuildRequires: expat-devel
BuildRequires: gstreamer1-devel >= %{gstreamer_version}
BuildRequires: libcanberra-devel
BuildRequires: clutter-devel clutter-gtk-devel
BuildRequires: intltool
BuildRequires: vala-devel

# Newer than internal gettext needed
BuildRequires: gettext
BuildRequires: autoconf >= 2.60
BuildRequires: automake libtool
BuildRequires: gnome-doc-utils >= 0.3.2
BuildRequires: scrollkeeper
BuildRequires: gnome-common
BuildRequires: gobject-introspection-devel
BuildRequires: sqlite-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
# for autoreconf
BuildRequires: itstool
BuildRequires: yelp-tools


Provides: gnome-sudoku = %{version}-%{release}
Obsoletes: gnome-sudoku < %{version}-%{release}

Provides: glchess = %{version}-%{release}
Obsoletes: glchess < 2.0


%description
The gnome-games package is a collection of some small "five-minute" games
in a variety of styles and genres for the GNOME desktop.


%package extra
Group: Amusements/Games
Summary: More games for the GNOME desktop
Requires: %{name} = %{version}-%{release}
Requires: pygtkglext
Requires: PyOpenGL
Requires: gnuchess
Requires: gnome-python2-rsvg

%description extra
The gnome-games-extra package contains additional small "five-minute" games
in a variety of styles and genres for the GNOME desktop.


%package help
Group: Applications/Productivity
Summary: Help files for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description help
This package contains user documentation for %{name}.

%prep
%setup -q

%build
%configure --localstatedir=/var/lib \
           --enable-introspection \
           --enable-staging \
           %{omitgames}
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT

## things we just don't want in the package
rm -rf $RPM_BUILD_ROOT%{_libdir}/libgdkcardimage.*a
rm -rf $RPM_BUILD_ROOT/var/lib/scrollkeeper

## install desktop files
desktop-file-install --vendor gnome --delete-original       \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications             \
  --remove-category Application                             \
  --remove-category PuzzleGame                              \
  $RPM_BUILD_ROOT%{_datadir}/applications/*

desktop-file-install --vendor gnome --delete-original       \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications             \
  $RPM_BUILD_ROOT%{_datadir}/applications/gnome-glines.desktop \
  $RPM_BUILD_ROOT%{_datadir}/applications/gnome-gnect.desktop

rm -f $RPM_BUILD_ROOT%{_libdir}/gnome-games/libgames-support-gi.{l,}a

%find_lang %{gettext_package} --all-name --with-gnome
grep "/usr/share/locale" %{gettext_package}.lang > translations.lang
grep -v "/usr/share/locale" %{gettext_package}.lang > help-tmp.lang  || :
# not sure why sr@latin/figures is listed -- with other languages,
# only ${LANG} is listed, and ${LANG}/figures is not
grep -v "/sr@latin/figures" help-tmp.lang > help.lang || :


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%post extra
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor &> /dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%postun extra
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor &> /dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%posttrans extra
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :


%files help -f help.lang

%files -f translations.lang
%doc AUTHORS COPYING README
%{_datadir}/applications/gnome-gnomine.desktop
%{_datadir}/applications/gnome-iagno.desktop
%if %{have_sudoku}
%{_datadir}/applications/gnome-sudoku.desktop
%endif
%{_datadir}/applications/gnome-swell-foop.desktop
# find-lang should find these
%doc %{_datadir}/help/*/swell-foop
%doc %{_datadir}/help/*/gnomine
%doc %{_datadir}/help/*/iagno
%doc %{_datadir}/help/*/gnome-sudoku

%{_datadir}/iagno
%if %{have_sudoku}
%{_datadir}/gnome-sudoku
%endif

%{_datadir}/icons/hicolor/*/apps/iagno.png
%{_datadir}/icons/hicolor/*/apps/gnomine.*
%if %{have_sudoku}
%{_datadir}/icons/hicolor/*/apps/gnome-sudoku.*
%endif
%{_datadir}/icons/hicolor/*/apps/swell-foop.*

%{_datadir}/swell-foop

%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/gnomine.*

%if %{have_sudoku}
%{python_sitelib}/gnome_sudoku
%endif

# gsettings schemas
%{_datadir}/glib-2.0/schemas/org.gnome.gnomine.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.iagno.gschema.xml

# these are not setgid games
%if %{have_sudoku}
%{_bindir}/gnome-sudoku
%endif

# these are setgid games
%attr(2551, root, games) %{_bindir}/swell-foop
%attr(2551, root, games) %{_bindir}/iagno
%attr(2551, root, games) %{_bindir}/gnomine

%{_mandir}/man6/iagno.6.bz2
%{_mandir}/man6/gnomine.6.bz2
%if %{have_sudoku}
%{_mandir}/man6/gnome-sudoku.6.bz2
%endif

%{_datadir}/gnomine

%{_datadir}/glib-2.0/schemas/org.gnome.gnome-sudoku.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.swell-foop.gschema.xml

%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/swell-foop.*

%files extra
%defattr(-, root, root)

#%{python_sitelib}/glchess
%{_datadir}/glchess

%{_datadir}/gnotravex

# gsettings schemas
%{_datadir}/glib-2.0/schemas/org.gnome.glchess.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-mahjongg.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.glines.gschema.xml


# %{_datadir}/applications/gnome-freecell.desktop
%{_datadir}/applications/gnome-glchess.desktop
%{_datadir}/applications/gnome-glines.desktop
%{_datadir}/applications/gnome-gnect.desktop
%{_datadir}/applications/gnome-gnibbles.desktop
%{_datadir}/applications/gnome-gnobots2.desktop
%{_datadir}/applications/gnome-gnotravex.desktop
%{_datadir}/applications/gnome-gnotski.desktop
%{_datadir}/applications/gnome-gtali.desktop
%{_datadir}/applications/gnome-lightsoff.desktop
%{_datadir}/applications/gnome-mahjongg.desktop
%{_datadir}/applications/gnome-quadrapassel.desktop

%{_mandir}/man6/glchess.6.bz2
%{_mandir}/man6/glines.6.bz2
%{_mandir}/man6/gnect.6.bz2
%{_mandir}/man6/gnibbles.6.bz2
%{_mandir}/man6/gnobots2.6.bz2
%{_mandir}/man6/quadrapassel.6.bz2
%{_mandir}/man6/gnotravex.6.bz2
%{_mandir}/man6/gnotski.6.bz2
%{_mandir}/man6/gtali.6.bz2
%{_mandir}/man6/gnome-mahjongg.6.bz2

%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/glines.*
%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/gnibbles.*
%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/quadrapassel.*
%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/gnotravex.*
%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/gnotski.*
%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/gtali.*
%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/gnome-mahjongg.*
%verify(not md5 size mtime) %config(noreplace) %attr(664, games, games) /var/lib/games/gnobots2*


# these are not setgid games
%{_bindir}/gnect
%{_bindir}/glchess
%{_bindir}/lightsoff

# these are setgid games
%attr(2551, root, games) %{_bindir}/gnome-mahjongg
%if %{build_tali}
%attr(2551, root, games) %{_bindir}/gtali
%endif
%attr(2551, root, games) %{_bindir}/gnobots2
%attr(2551, root, games) %{_bindir}/quadrapassel
%attr(2551, root, games) %{_bindir}/gnotravex
%attr(2551, root, games) %{_bindir}/gnotski
%attr(2551, root, games) %{_bindir}/gnibbles
%attr(2551, root, games) %{_bindir}/glines

%{_datadir}/glines
%{_datadir}/gnect
%{_datadir}/gnibbles
%{_datadir}/gnobots2
%{_datadir}/gnotski
%{_datadir}/gtali
%{_datadir}/gnome-mahjongg
%{_datadir}/quadrapassel
%{_datadir}/lightsoff

%{_datadir}/icons/hicolor/*/actions/teleport*
%{_datadir}/icons/hicolor/*/apps/glchess.*
%{_datadir}/icons/hicolor/*/apps/glines.*
%{_datadir}/icons/hicolor/*/apps/gnect.*
%{_datadir}/icons/hicolor/*/apps/gnibbles.*
%{_datadir}/icons/hicolor/*/apps/gnobots2.*
%{_datadir}/icons/hicolor/*/apps/gnotravex.*
%{_datadir}/icons/hicolor/*/apps/gnotski.*
%{_datadir}/icons/hicolor/*/apps/gtali.*
%{_datadir}/icons/hicolor/*/apps/gnome-mahjongg.*
%{_datadir}/icons/hicolor/*/apps/quadrapassel.*
%{_datadir}/icons/hicolor/*/apps/lightsoff.*

# gsettings schemas
%{_datadir}/glib-2.0/schemas/org.gnome.gnotravex.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnect.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnibbles.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnobots2.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnotski.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gtali.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.quadrapassel.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.lightsoff.gschema.xml

%{_datadir}/help/*/glchess
%{_datadir}/help/*/glines
%{_datadir}/help/*/gnect
%{_datadir}/help/*/gnibbles
%{_datadir}/help/*/gnobots2
%{_datadir}/help/*/gnotravex
%{_datadir}/help/*/gnotski
%{_datadir}/help/*/gtali
%{_datadir}/help/*/gnome-mahjongg
%{_datadir}/help/*/quadrapassel
%{_datadir}/help/*/lightsoff

%changelog
* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0.2-1m)
- update to 3.6.0.2

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-3m)
- rebuild for librsvg2 2.36.1

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-2m)
- rebuild for pygobject2-devel

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-3m)
- build with pygobject-2.28.6

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.91-2m)
- add BuildRequires

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1.1-1m)
- update to 3.0.1.1

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- hide warning %%post

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Wed Jun  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-3m)
- add Req seed for some games

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.6-1m)
- update to 2.29.6

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-2m)
- delete __libtoolize hack

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Nov 13 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.28.1-2m)
- revised BR, add clutter-gtk-devel

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.28.0-2m)
- replace %%{python_sitearch} to %%{python_sitelib}

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat Jun 13 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.26.2-3m)
- add BuildRequire: libggz-devel

* Fri May 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-2m)
- add Require: gnome-python2-gnomeprint (for gnome-sudoku)

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Tue Apr 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Wed Feb 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild against python-2.6.1-1m

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Mon Nov  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1.1-1m)
- update to 2.24.1.1

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Fri May 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2.1-1m)
- update to 2.22.2.1

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1.1-2m)
- rebuild against librsvg2-2.22.2-3m

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.1-1m)
- update to 2.22.1.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- rebuild against gcc43

* Tue Mar 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-2m)
- update extra source to 2.22.0

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Wed Jan  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Sun Jul 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2.1-3m)
- add Requires: gnome-python2-gconf

* Tue Jun 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.2.1-2m)
- added BuildPrereq: gnome-python2-desktop

* Sun Jun 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2.1-1m)
- update to 2.18.2.1

* Mon May 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1.1-1m)
- update to 2.18.1.1 (extra-data 2.18.0)

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- updaet to 2.18.1

* Wed Mar 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0.1-1m)
- updaet to 2.18.0.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-4m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- good-bye %%makeinstall

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update 2.16.2

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Wed Aug 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-2m)
- use make DESIDIR=%%{buidlroot} install instead of %%makeinstall

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Sat Jul  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2.1-1m)
- update to 2.14.2.1
-- http://ftp.gnome.org/pub/GNOME/sources/gnome-games/2.14/gnome-games-2.14.2.1.news

* Sun Jun 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-4m)
- add dependency gtk+-common (for svg image)

* Sun Jun 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-3m)
- revise %%post script

* Fri Jun  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-2m)
- add patch0 (sol crash)

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update 2.14.2

* Sun Apr 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-2m)
- update SOURCE1 (gnome-games-extra-data-2.14.0)

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update 2.14.1
- remove gtali gtali.schemas was broken? It does not changed :-(

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Tue Apr  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Tue Feb 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-2m)
- fix hadling schemas

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Sun Dec 11 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-3m)
- restore games iagno gnotravex mahjongg
- some games does not use %attr(2111, root, games)
- some games use %attr(2555, root, games)

* Sun Dec 11 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-2m)
- add gnome-games-extra-data %{SOURCE1}
- change blackjack %attr(-, root, games)
- delete disable games iagno gnotravex mahjongg (?)

* Sun Dec 4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- delete devel package
- GNOME 2.12.2 Desktop

* Mon Nov 21 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-1m)
- version 2.8.2
- GNOME 2.8 Desktop

* Tue Dec 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0.1-4m)
- add auto commands before %%build for fix missing lib*.so problem

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.6.0.1-3m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Thu Jul 29 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0.1-2m)
- add patch0 (gtali schema warning fix)

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0.1-1m)
- version 2.6.0.1
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.2-2m)
- revised spec for enabling rpm 4.2.

* Thu Feb 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Tue Jan 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.1.1-1m)
- version 2.4.1.1

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.8-1m)
- version 2.3.8

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.7-1m)
- version 2.3.7

* Tue Aug 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-1m)
- version 2.3.6

* Thu Jul 24 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.5-2m)
- add BuildPrereq: guile-devel

* Tue Jul 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Thu Jul 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Thu Jun 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Mon May 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Wed Mar 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0-3m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-2m)
- rebuild against for guile

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Mon Oct 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Tue Aug 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Tue Aug 06 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1.1-3m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1.1-2m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1.1-1m)
- version 2.0.1.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-5m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-4k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Thu Jun 13 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-16k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-14k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-12k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-10k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-8k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-6k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-4k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-2k)
- version 1.94.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-6k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-2k)
- version 1.93.0

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.0-10k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.0-8k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.0-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Sat May 11 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.92.0-4k)
  patch can't applied
#'

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.0-2k)
- version 1.92.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.0-4k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.0-2k)
- version 1.91.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-20k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-18k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-16k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-14k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-12k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-10k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-8k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-6k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-4k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-2k)
- version 1.90.2

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-20k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-18k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-16k)
- change schema handring

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-14k)
- modify depends list

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-12k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-10k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-8k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-6k)
- rebuild against for gtk+-1.3.13

* Thu Jan 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-2k)
- version 1.90.1
- gnome2 env

* Mon Dec 17 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.3-10k)
- add omfenc patch 

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (1.4.0.3-8k)
- move zh_CN.GB2312 => zh_CN
- move zh_TW.Big5 => zh_TW

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.4.0.3-4k)
- rebuild against gettext 0.10.40.

* Tue Aug  4 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.3-2k)
- version 1.4.0.3

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-7k)
- rebuild against gcc 2.96.

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.0-5k)
- fixed for FHS

* Sun Jun 25 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.0

* Sat May 13 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.90

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Sat Jan 21 2000 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Fri Jan 21 2000 Shingo Akagaki <dora@kondara.org>
- change pa-tari patch to baatari patch

* Sat Nov 27 1999 Shingo Akagaki <dora@kondara.org>
- fixed setgid programs message (pa-tari)
- Update to 1.0.51
- to be nosource

* Tue Sep 21 1999 Michael Fulbright <drmike@redhat.com>
- fixed gnotravex to not loop infinitely

* Mon Sep 20 1999 Elliot Lee <sopwith@redhat.com>
- Update to 1.0.40

* Sat Apr 10 1999 Jonathan Blandford <jrb@redhat.com>
- added new sol games and a fix for the old ones.

* Mon Mar 29 1999 Michael Fulbright <drmike@redhat.com>
- removed more offending t*tris stuff

* Thu Mar 18 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.2
- made gnibbles have correct attr since its setgid
- strip binaries

* Sun Mar 14 1999 Michael Fulbright <drmike@redhat.com>
- added score files to file list

* Thu Mar 04 1999 Michael Fulbright <drmike@redhat.com>
- Version 1.0.1

* Fri Feb 19 1999 Michael Fulbright <drmike@redhat.com>
- removed *tris games

* Mon Feb 15 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.8
- added sound event lists to file list
- touched up file list some more

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- added gnibbles data to file list

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- updated to 0.99.7

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- updated to 0.99.5

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- updated to 0.99.3

* Wed Jan 06 1999 Michael Fulbright <drmike@redhat.com>
- updated to 0.99.1

* Thu Dec 16 1998 Michael Fulbright <drmike@redhat.com>
- updated to 0.99.0 in prep for GNOME 1.0

* Sat Nov 21 1998 Michael Fulbright <drmike@redhat.com>
- updated for 0.30 tree

* Fri Nov 20 1998 Pablo Saratxaga <srtxg@chanae.alphanet.ch>
- use --localstatedir=/var/lib in config state (score files for games
  for exemple will go there).

* Mon Mar 16 1998 Marc Ewing <marc@redhat.com>
- Integrate into gnome-games CVS source tree
