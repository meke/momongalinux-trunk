%global momorel 1


Summary: Generic Security Services Application Programming Interface Library
Name: libgssglue
Version: 0.3
Release: %{momorel}m%{?dist}
License: BSD and MIT
Group: System Environment/Libraries
URL: http://www.citi.umich.edu/projects/nfsv4/linux/
Source0:http://www.citi.umich.edu/projects/nfsv4/linux/%{name}/%{name}-%{version}.tar.gz
Patch0: %{name}-0.1-gssglue.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: krb5-libs >= 1.5
BuildRequires: krb5-libs >= 1.5
BuildRequires: pkgconfig
Provides: libgssapi = %{version}-%{release}
Obsoletes: libgssapi <= 0.11

%description
This library exports a gssapi interface, but doesn't implement any gssapi
mechanisms itself; instead it calls gssapi routines in other libraries,
depending on the mechanism.

%package devel
Summary: Development files for the gssclug library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Provides: libgssapi-devel = %{version}-%{release}
Obsoletes: libgssapi-devel <= 0.11

%description devel
This package includes header files and libraries necessary for
developing programs which use the gssapi library.

%prep
%setup -q

%patch0 -p1 -b .libgssapi_krb5

%build
%configure
make %{?_smp_mflags} all 

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}/%{_sysconfdir}
install -p -m 644 doc/gssapi_mech.conf %{buildroot}/%{_sysconfdir}/gssapi_mech.conf

# get rid of *.a and *.la files
rm -f %{buildroot}/%{_libdir}/%{name}.a
rm -f %{buildroot}/%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%config(noreplace) %{_sysconfdir}/gssapi_mech.conf
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/gssglue
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so

%changelog
* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-1m)
- update 0.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-9m)
- full rebuild for mo7 release

* Thu Jun 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1-8m)
- force rebuild to erase libgssapi
- this package was renamed from libgssapi
- from ChangeLog
 - * Rename library from libgssapi to libgssglue to
 - resolve conflicts with Heimdal and MIT libraries
 - named libgssapi.
- already Provides and Obsoletes: libgssapi
- change License: from GPL to BSD and MIT

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-5m)
- define  __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-4m)
- rebuild against rpm-4.6

* Thu Dec 11 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-3m)
- re-import libgssglue-0.1-gssglue.patch from fc-devel (libgssglue-0_1-6_fc10)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-1m)
- initial commit Momonga linux

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.1-5
- Autorebuild for GCC 4.3

* Wed Oct 17 2007 Steve Dickson <steved@redhat.com> 0.1-4
- updated Obsoletes: (0.1-3)
- Obsolete -devel package

* Mon Sep 17 2007 Steve Dickson <steved@redhat.com> 0.1-2
- RPM review

* Tue Sep 11 2007 Steve Dickson <steved@redhat.com>
- Initial commit
