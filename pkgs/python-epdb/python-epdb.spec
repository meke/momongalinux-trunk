%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-epdb
Version:        0.11
Release:        %{momorel}m%{?dist}
Summary:        Extended Python debugger

Group:          Development/Debuggers
License:        MIT
URL:            ftp://download.rpath.com/pub/epdb/
Source0:        ftp://download.rpath.com/pub/epdb/epdb-%{version}.tar.bz2
Patch01:	epdb-telnet-skip.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7

%description
epdb is an extended Python debugger.

%prep
%setup -q -n epdb-%{version}
sed -i 's,python2.4,python,g' Make.rules epdb/epdb.py

%patch01 -p1


%build
make %{?_smp_mflags} sitedir=%{python_sitelib}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT sitedir=%{python_sitelib}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE NEWS
%{python_sitelib}/*



%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-2m)
- full rebuild for mo7 release

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-1m)
- Initial commit Momonga Linux. import from Fedora

* Wed Oct 21 2009 Justin M. Forbes <jforbes@redhat.com> - 0.11-4
- Allow the number of frames to be passed for telnet sessions.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jan 16 2009 Justin M. Forbes <jmforbes@linuxtx.org> 0.11-1
- Initial package build.
