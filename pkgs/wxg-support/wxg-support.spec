%global momorel 11

%global ximdir /etc/X11/xinit/xim.d

Name: wxg-support
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Summary: WXG support package
Requires: wxg, psmisc
BuildArchitectures: noarch
Requires(post): chkconfig
Requires(preun): chkconfig
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
WXG Support package

%prep

%install
mkdir -p %{buildroot}/%{ximdir}
mkdir -p %{buildroot}/%{_initscriptdir}

cat <<END > %{buildroot}/%{ximdir}/WXG
NAME="WXG/kinput2"
IFEXISTS="/var/lock/subsys/wxg:/usr/X11R6/bin/kinput2"
IM_HOST=localhost
if [ -f \$HOME/.xinit.d/im_host ]; then
	IM_HOST=\`awk -F: '{print \$1}' \$HOME/.xinit.d/im_host\`
fi
IM_EXEC="kinput2 -canna -cannaserver \$IM_HOST"
XMODIFIERS=@im=kinput2
GTK_IM_MODULE=xim
QT_IM_MODULE=xim
export IM_EXEC XMODIFIERS GTK_IM_MODULE QT_IM_MODULE
END

cat <<END > %{buildroot}/%{_initscriptdir}/wxg
#!/bin/sh
#
# WXG for Linux / FreeBSD
#
# Author: A.I.SOFT < wxglinux@aisoft.co.jp >
# Modify: AYUHANA Tomonori <l@kondara.org>
# description: WXG is a Kana-Kanji conversion server.\\
#         The server is compatible with a cannaserver.
# chkconfig: 2345 92 12
# processname: wxgserver

# Source function library.
. %{_initscriptdir}/functions

SERVER=/usr/bin/wxgserver
KILLER=/usr/bin/killall
LOCKD=/var/lock/subsys
IROHA=/tmp/.iroha_unix

[ -f \$SERVER ] || exit 0

case "\$1" in
  start)
	echo -n "Starting WXG server: "
	rm -f /tmp/.iroha_unix/IROHA
	\$SERVER &
	echo 
	if [ -d \$LOCKD ]; then
		touch \$LOCKD/wxg
	fi
	;;
  stop)
	echo -n "Shutting down WXG server: "
	\$KILLER wxgserver
	echo
	if [ -d \$IROHA ]; then
		rm -rf \$IROHA
	fi
	if [ -d \$LOCKD ]; then
		rm -f \$LOCKD/wxg
	fi
	;;
  restart)
	echo -n "Restart WXG server: "
	\$0 stop
	\$0 start
	;;
  *)
	echo "Usage: wxg {start|stop|restart}"
	exit 1
esac

exit 0
END

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add wxg
if [ -x %{_initscriptdir}/canna ]; then
	/sbin/chkconfig --level 345 canna off
fi
if [ -e /tmp/.iroha_unix/IROHA -a -x %{_initscriptdir}/wxg ]; then
	%{_initscriptdir}/wxg restart
fi

%preun
if [ $1 = 0 ]; then
	if [ -e /tmp/.iroha_unix/IROHA -a -x %{_initscriptdir}/wxg ]; then
		%{_initscriptdir}/wxg stop
	fi
	/sbin/chkconfig --del wxg
	if [ -x %{_initscriptdir}/canna ]; then
		/sbin/chkconfig --level 345 canna on
	fi
fi


%files
%defattr(-,root,root)
%config %{ximdir}/WXG
%attr(755,root,root) %config %{_initscriptdir}/wxg

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-9m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-8m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-5m)
- rebuild against gcc43

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-4m)
- add GTK_IM_MODULE and QT_IM_MODULE to %%{ximdir}/WXG

* Wed Dec 13 2000 AYUHANA Tomonori <l@kondara.org>
- (1.0-0.0001006k)
- backport 1.0-0.0001007k for 2.0

* Sun Dec  3 2000 AYUHANA Tomonori <l@kondara.org>
- (1.0-0.0001007k)
- use killall
- revive canna
- (1.0-0.0001005k)
- add %attr(755)

* Sat Dec  2 2000 AYUHANA Tomonori <l@kondara.org>
- (1.0-3k)
- first release

