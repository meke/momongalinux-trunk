%global momorel 9

Summary: Quick DataBase Manager
Name: qdbm
Version: 1.8.77
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: GPL
URL: http://qdbm.sourceforge.net/

Source0: http://qdbm.sourceforge.net/%{name}-%{version}.tar.gz 
NoSource: 0

Patch0: qdbm-ruby19.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glibc-devel, gcc-c++ >= 3.4.1-1m, ruby-devel >= 1.8.6-4m

%description
QDBM is an embeded database library compatible with GDBM and NDBM.
It features hash database and B+ tree database and is developed referring
to GDBM for the purpose of the following three points: higher processing
speed, smaller size of a database file, and simpler API.

%package devel
Summary: Quick DataBase Manager
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibc-devel

%description devel
QDBM is an embeded database library compatible with GDBM and NDBM.
It features hash database and B+ tree database and is developed referring
to GDBM for the purpose of the following three points: higher processing
speed, smaller size of a database file, and simpler API.

%package c++-devel
Summary: Quick DataBase Manager
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: libstdc++-devel

%description c++-devel
QDBM is an embeded database library compatible with GDBM and NDBM.
It features hash database and B+ tree database and is developed referring
to GDBM for the purpose of the following three points: higher processing
speed, smaller size of a database file, and simpler API.

%package -n ruby-qdbm
Summary: Ruby binding for Quick DataBase Manager
Group: Development/Libraries
Requires: qdbm = %{version}

%description -n ruby-qdbm
QDBM is an embeded database library compatible with GDBM and NDBM.
It features hash database and B+ tree database and is developed referring
to GDBM for the purpose of the following three points: higher processing
speed, smaller size of a database file, and simpler API.

%prep
%setup -q
%patch0 -p1 -b .ruby19

%build
%configure --enable-devel --enable-pthread --enable-zlib --enable-iconv
%make
(
  cd plus
  %configure --enable-devel
  %make all
)

# for ruby-qdbm
cd ruby
%configure
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%__make install DESTDIR=%{buildroot}
(
  cd plus
  %__make install DESTDIR=%{buildroot}
)

# for install ruby-qdbm
cd ruby
%__make install \
  DESTDIR=%{buildroot} \
  sitelibdir=%{buildroot}%{ruby_sitelibdir} \
  sitearchdir=%{buildroot}%{ruby_sitearchdir} \
  MYBINDIR=%{_bindir} \
  MYDATADIR=%{_datadir}/qdbm/ruby

# remove needless files
rm -rf %{buildroot}%{_datadir}/qdbm

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc COPYING ChangeLog NEWS README THANKS spex*.html
%{_bindir}/*
%exclude %{_bindir}/rbcrtest
%exclude %{_bindir}/rbdptest
%exclude %{_bindir}/rbvltest
%{_mandir}/man1/*
%{_libdir}/libqdbm.so.*
%{_libdir}/libxqdbm.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/cabin.h
%{_includedir}/curia.h
%{_includedir}/depot.h
%{_includedir}/hovel.h
%{_includedir}/odeum.h
%{_includedir}/relic.h
%{_includedir}/villa.h
%{_includedir}/vista.h
%{_libdir}/libqdbm.a
%{_libdir}/libqdbm.so
%{_libdir}/pkgconfig/qdbm.pc
%{_mandir}/man3/*

%files c++-devel
%defattr(-, root, root)
%doc plus/xspex*.html plus/xapidoc
%{_includedir}/xqdbm.h
%{_includedir}/xadbm.h
%{_includedir}/xdepot.h
%{_includedir}/xcuria.h
%{_includedir}/xvilla.h
%{_libdir}/libxqdbm.a
%{_libdir}/libxqdbm.so

%files -n ruby-qdbm
%defattr(-, root, root)
%doc ruby/rbspex*.html ruby/rbapidoc
%{_bindir}/rb*
%{ruby_sitelibdir}/*.rb
%{ruby_sitearchdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.77-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.77-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.77-7m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.77-6m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.77-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.77-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.77-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.77-2m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.77-1m)
- update

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.75-2m)
- rebuild against ruby-1.8.6-4m

* Mon Apr 23 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildRequires: ruby-devel

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.75-1m)
- update

* Wed Sep 27 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.71-1m)
- update to 1.8.71

* Thu Sep 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.68-2m)
- delete duplicate files

* Wed Aug 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.68-1m)
- update to 1.8.68 for hyperestraier
- aggregate ruby-qdbm spec

* Tue Jul 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.61-1m)
- update to 1.8.61 for hyperestraier

* Sun Jun 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.59-1m)
- update to 1.8.59 for hyperestraier

* Thu May 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.56-1m)
- update to 1.8.56 for hyperestraier

* Mon Apr 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.48-1m)
- update to 1.8.48 for hyperestraier

* Thu Mar 09 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.46-1m)
- update to 1.8.46 for hyperestraier

* Thu Feb 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.45-1m)
- update to 1.8.45 for hyperestraier

* Sun Dec 11 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.35-1m)
- update to 1.8.35 for hyperestraier

* Thu Sep 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.33-1m)
- update to 1.8.33 for hyperestraier
- add %%{_libdir}/libxqdbm.so.* and %%{_libdir}/libqdbm.a to files

* Tue Jun  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.29-1m)
- minor feature enhancements

* Tue Feb  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.21-1m)
- minor feature enhancements

* Thu Jan  6 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.20-1m)
- minor feature enhancements

* Sun Nov 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.18-1m)
- minor feature enhancements

* Thu Sep 30 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.8.17-2m)
- rebuild against gcc-c++-3.4.1

* Thu Sep 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.17-1m)
- the utility API was enhanced

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.16-1m)
- minor bugfixes

* Thu Aug 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.15-1m)
- minor feature enhancements

* Mon Aug  9 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.14-2m)
- separate ruby-qdbm package

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.14-1m)
- rebuild against ruby-1.8.2

* Sun May 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.12-1m)
- minor feature enhancements

* Fri May 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.11-1m)
- minor feature enhancements

* Sun Apr 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.10-1m)
- utility for performance test was enhanced
- logger for debugging is now featured

* Thu Apr 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.8-1m)
- add '--enable-pthread --enable-zlib --enable-iconv' to configure option
- processing speed was improved to over 150%
- the basic API and the extended API were enhanced

* Wed Mar 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.7-1m)
- the configuration of C++ api escaped from a bug of pthread
- the extended api was enhanced
- bugs in the utility api about memory management were fixed

* Tue Mar 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.5-1m)
- minor feature enhancements

* Fri Mar 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.4-1m)
- minor feature enhancements

* Sun Feb 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.0-1m)
- minor feature enhancements

* Wed Feb 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.34-1m)

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.33-1m)
- minor feature enhancements

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.32-2m)
- specfile cleanup

* Wed Jan 28 2004 Kenta MURATA <muraken2@nifty.com>
- (1.7.32-1m)
- version up.
- append ruby package.

* Sat Sep 20 2003 Kenta MURATA <muraken2@nifty.com>
- (1.6.17-1m)
- version up.
- remove doxygen from BuildRequires.

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (1.6.11-1m)
- first specfile.
