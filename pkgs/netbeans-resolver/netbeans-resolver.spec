%global momorel 4

%define patched_resolver_ver 1.2
%define patched_resolver xml-commons-resolver-%{patched_resolver_ver}

Name:    netbeans-resolver
Version: 6.7.1
Release: %{momorel}m%{?dist}
Summary: Resolver subproject of xml-commons patched for NetBeans

Group:   Development/Libraries
License: "ASL 1.1"
URL:     http://xml.apache.org/commons/

Source0: http://www.apache.org/dist/xml/commons/%{patched_resolver}.tar.gz

# see http://hg.netbeans._org/main/file/721f72486327/o.apache.xml.resolver/external/readme.txt
Patch0: %{name}-%{version}-nb.patch
Patch1: %{name}-%{version}-resolver.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

BuildRequires: java-devel >= 1.6.0
BuildRequires: jpackage-utils
BuildRequires: ant
BuildRequires: dos2unix

Requires: jpackage-utils
Requires: java >= 1.6.0

%description
Resolver subproject of xml-commons, version %{patched_resolver_ver} with 
a patch for NetBeans.

%prep
%setup -q -n %{patched_resolver}
# remove all binary libs and prebuilt javadocs
find . -name "*.jar" -exec rm -f {} \;
%{__rm} -rf docs

%patch0 -p1 -b .sav
%patch1 -p1 -b .sav

dos2unix -k KEYS
dos2unix -k LICENSE.resolver.txt

%build
%{ant} -f resolver.xml jar

%install
%{__rm} -rf %{buildroot}

# JARs
%{__mkdir_p} %{buildroot}%{_javadir}
%{__cp} -p build/resolver.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_javadir}/*
%doc LICENSE.resolver.txt KEYS

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.1-2m)
- full rebuild for mo7 release

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.7.1-1m)
- sync with Rawhide (6.7.1-2)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 6.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Sep 05 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-5
- The description is formatted
- The license viersion is fixed

* Fri Aug 22 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-4
- The dos2unix package is added as the build requirements

* Fri Aug 22 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-3
- Redundant distribution tag is removed
- Redundant user-defined macros are removed
- java-devel is specified in BuildRequires insead of java-1.6.0-openjdk
- An epoch of 1 is included in the requirements for the Java versions
- The %%{buildroot} is used everywhere instead of $RPM_BUILD_ROOT
- The canonical RPM macros are used instead of the commands ant and rm
- The -k option is used for the dos2unix commands
- More correct source URL is used, i.e not a mirror

* Fri Aug 15 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-2
- Docummentaion is added
- Appropriate value of the Group Tag are chosen from the official list

* Fri Jun 06 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-1
- Initial version

