%global momorel 6

Name: hunspell-bg
Summary: Bulgarian hunspell dictionaries
Version: 4.1
Release: %{momorel}m%{?dist}
Source: http://downloads.sourceforge.net/bgoffice/OOo-spell-bg-%{version}.zip
Group: Applications/Text
URL: http://bgoffice.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Bulgarian hunspell dictionaries.

%prep
%setup -q -n OOo-spell-bg-%{version}

%build
tr -d '\r' < README > README.new
iconv -f ISO-8859-2 -t UTF-8 README.new > README
tr -d '\r' < COPYING > COPYING.new
mv COPYING.new COPYING

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING COPYING.BULGARIAN README  
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-1m)
- import from Fedora to Momonga

* Tue Dec 18 2007 Caolan McNamara <caolanm@redhat.com> 
- latest version

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 4.0-2
- clarify license, can't see a specific GPL version

* Mon Jul 09 2007 Caolan McNamara <caolanm@redhat.com> - 4.0-1
- latest version

* Mon Feb 12 2006 Caolan McNamara <caolanm@redhat.com> - 0.20040405-1
- initial version
