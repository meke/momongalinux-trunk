%global momorel 13
%global php_ver 5.4.1

%global pkg_name id3

Summary: Functions to read and write ID3 tags in MP3 files. 
Name: php-%{pkg_name}
Version: 0.2
Release: %{momorel}m%{?dist}
License: PHP
Group: Development/Languages
URL: http://pecl.php.net/package/id3 
#Source0: http://pecl.php.net/get/%{pkg_name}-%{version}.tgz
#NoSource: 0
Source0:   %{pkg_name}-%{version}.tgz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source10: id3.ini

Patch0: %{name}-%{version}-php54.patch

Requires: php >= %{php_ver}
BuildRequires: php-devel >= %{php_ver}

%description
id3 enables to to retrieve and update information from ID3 tags in
MP3 files. It supports version 1.0, 1.1 and 2.2+ (only reading text- and url-frames at the moment).

%prep
# Setup main module
%setup -q -n %{pkg_name}-%{version}

%patch0 -p1 -b .php54

# prepare php module
%{_bindir}/phpize
%configure --enable-id3

%build
# Build main module
%make

%install
rm -rf %{buildroot}

%makeinstall INSTALL_ROOT=%{buildroot}
%{__install} -m 755 -d %{buildroot}%{_sysconfdir}/php.d
%{__install} -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/php.d

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CREDITS EXPERIMENTAL TODO examples/*
%{_sysconfdir}/php.d/%{pkg_name}.ini
%{_libdir}/php/modules/%{pkg_name}.so

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-13m)
- rebuild against php-5.4.1

* Sat Jul 16 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2-12m)
- INI comments char fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-6m)
- rebuild against rpm-4.6

* Mon May 05 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.2-5m)
- rebuild against php 5.2.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-4m)
- rebuild against gcc43

* Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-3m)
- rebuild against php-5.2.1

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.2-2m)
- rebuild against PHP 5.1.4

* Thu Mar 22 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- initial version
