%global momorel 30
%define	lispdir		%{_datadir}/emacs/site-lisp
%define	pkgdir		%{_datadir}/xemacs/xemacs-packages

Summary: Basic library for handling email messages for Emacs
Name: flim
Version: 1.14.9
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
URL: http://git.chise.org/elisp/flim/
BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildRequires: emacs-apel
BuildRequires: emacs
# for apel
BuildRequires: xemacs-packages-base
BuildRequires: xemacs
BuildRequires: xemacs-packages-extra

BuildArch: noarch
Source0: http://git.chise.org/elisp/dist/%{name}/%{name}-1.14/%{name}-%{version}.tar.gz
NoSource: 0
Requires: emacs-apel
Obsoletes: elisp-flim
Provides: elisp-flim
Obsoletes: emacs-flim
Provides: emacs-flim

%description
FLIM is a library to provide basic features about message
representation and encoding for Emacs.


%package xemacs
Summary: Basic library for handling email messages for XEmacs
Group: Applications/Editors
# for apel
Requires: xemacs-packages-base

%description xemacs
FLIM is a library to provide basic features about message
representation and encoding for Emacs.


%prep
%setup -q


%build
rm -f mailcap*
make LISPDIR=$RPM_BUILD_ROOT%{lispdir}


%install
rm -rf $RPM_BUILD_ROOT

# build for emacs
%makeinstall PREFIX=$RPM_BUILD_ROOT%{_prefix} LISPDIR=$RPM_BUILD_ROOT%{lispdir}

make clean

# build for xemacs
## hack for batch-update-autoloads
touch auto-autoloads.el custom-load.el
make EMACS=xemacs PACKAGEDIR=$RPM_BUILD_ROOT%{pkgdir} install-package


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr (-, root, root)
%doc FLIM-API.en README.en README.ja
%{lispdir}

%files xemacs
%defattr (-, root, root)
%doc README.en README.ja
%{pkgdir}

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14.9-30m)
- change primary site and download URIs

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.9-29m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.9-28m)
- rename the package name
- reimport from fedora's flim.spec

* Sat May  7 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.14.9-27m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.9-26m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.9-25m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.9-24m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14.9-23m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-22m)
- update to cvs snapshot (2010-08-05)

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-21m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-20m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-19m)
- merge flim-emacs to elisp-flim
- kill flim-xemacs

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-18m)
- update to cvs snapshot (2010-02-18)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.9-16m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.9-15m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-14m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-13m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-12m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-11m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-10m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-9m)
- rebuild against rpm-4.6

* Tue Jan  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-8m)
- update to cvs snapshot (2008-11-25)
- License: GPLv2+

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-7m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.9-6m)
- rebuild against apelver 10.7-11m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-5m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-4m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.9-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14.9-2m)
- %%NoSource -> NoSource

* Wed Nov 28 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.9-1m)
- update to 1.14.9

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.8-11m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-10m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-9m)
- rebuild against emacs-22.0.96

* Sun Mar 04 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-8m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-7m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.8-6m)
- rebuild against apel-10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.8-2m)
- rebuild against emacs-22.0.90

* Sat Jul 22 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.8-1m)
- update to 1.14.8

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.14.7-5m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.7-4m)
- rebuild against emacs 22.0.50

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.14.7-3m)
- use %%{sitepdir}.

* Sat Jan 29 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.14.7-2m)
- Obsoletes: elisp-limit

* Tue Jan 25 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.14.7-1m)
- replaces elisp-limit
