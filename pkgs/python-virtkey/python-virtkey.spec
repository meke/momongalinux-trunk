%global momorel 1

# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           python-virtkey
Version:        0.61.0
Release:        %{momorel}m%{?dist}
Summary:        Python extension for emulating keypresses and getting current keyboard layout

Group:          Development/Languages
#missing copy of GPL, licensing info in source file
License:        GPLv2+
URL:            https://launchpad.net/virtkey
Source0:        https://launchpad.net/virtkey/0.61/%{version}/+download/virtkey-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel, libXtst-devel, gtk2-devel, glib2-devel

%description
Python-virtkey is a python extension for emulating keypresses and getting
current keyboard layout.

%prep
%setup -q -n virtkey-%{version}

%build
CFLAGS="$RPM_OPT_FLAGS `pkg-config --cflags gdk-2.0`" \
	LIBS="`pkg-config --libs gdk-2.0`" %{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
#no documentation included in upstream tarball
%{python_sitearch}/*


%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61.0-1m)
- update to 0.61.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.50-7m)
- rebuild for glib 2.33.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.50-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.50-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.50-4m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.50-3m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.50-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.50-1m)
- import from Fedora

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.50-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.50-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.50-4
- Rebuild for Python 2.6

* Tue May 22 2008 Sindre Pedersen Bjørdal <sindrepb@fedoraproject.org> - 0.50-3
- Add patch to fix 64bit build issue, thanks Parag and Ivazquez
* Tue May 06 2008 Sindre Pedersen Bjørdal <sindrepb@fedoraproject.org> - 0.50-2
- Add missing glib2-devel build dependency
* Tue May 06 2008 Sindre Pedersen Bjørdal <sindrepb@fedoraproject.org> - 0.50-1
- Initial build
