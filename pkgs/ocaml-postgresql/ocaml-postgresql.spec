%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-postgresql
Version:        1.16.0
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for accessing PostgreSQL databases

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"
URL:            http://www.ocaml.info/home/ocaml_sources.html#postgresql-ocaml
Source0:        http://hg.ocaml.info/release/postgresql-ocaml/archive/release-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  postgresql-devel
BuildRequires:  chrpath
BuildRequires:  rpm >= 4.4.2.3

%global __ocaml_provides_opts -i Condition -i Event -i Mutex -i Thread -i ThreadUnix


%description
This OCaml-library provides an interface to PostgreSQL, an efficient
and reliable, open source, relational database.  Almost all
functionality available through the C-API (libpq) is replicated in a
type-safe way.  This library uses objects for representing database
connections and results of queries.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n postgresql-ocaml-release-%{version}


%build
make

strip lib/dll*.so
chrpath --delete lib/dll*.so


%install
# These rules work if the library uses 'ocamlfind install' to install itself.
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/postgresql
%if %opt
%exclude %{_libdir}/ocaml/postgresql/*.a
%exclude %{_libdir}/ocaml/postgresql/*.cmxa
%endif
%exclude %{_libdir}/ocaml/postgresql/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc LICENSE AUTHORS Changelog README.txt examples
%if %opt
%{_libdir}/ocaml/postgresql/*.a
%{_libdir}/ocaml/postgresql/*.cmxa
%endif
%{_libdir}/ocaml/postgresql/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16.0-1m)
- update to 1.16.0
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12.5-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.5-1m)
- update to 1.12.5
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12.3-1m)
- [SECURITY] CVE-2009-2943
- update to 1.12.3

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11.1-1m)
- update to 1.11.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.3-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.3-1m)
- update to 1.9.3
- rebuild against ocaml-3.11.0

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.8.2-4
- Rebuild for OCaml 3.10.2

* Fri Apr 18 2008 Richard W.M. Jones <rjones@redhat.com> - 1.8.2-3
- Can't spell.  prm -> rpm.

* Fri Apr 18 2008 Richard W.M. Jones <rjones@redhat.com> - 1.8.2-2
- Rebuild against updated RPM (see bug 443114).

* Fri Apr  4 2008 Richard W.M. Jones <rjones@redhat.com> - 1.8.2-1
- New upstream version 1.8.2.

* Tue Mar  4 2008 Richard W.M. Jones <rjones@redhat.com> - 1.7.0-3
- Rebuild for ppc64.

* Mon Mar  3 2008 Richard W.M. Jones <rjones@redhat.com> - 1.7.0-2
- Only include LICENSE doc in main package.
- Include extra documentation and examples in devel package.
- Check it builds in mock.

* Sun Feb 24 2008 Richard W.M. Jones <rjones@redhat.com> - 1.7.0-1
- Initial RPM release.
