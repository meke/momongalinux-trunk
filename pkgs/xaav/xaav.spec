%global momorel 9

Summary: Xlib ASCII ART Viewer
Name: xaav
Version: 1.0
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/Text
URL: http://www.nonms.org/soft/xaav.html
Source0: http://www.nonms.org/soft/arch/xlib_xaav-%{version}.tgz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libX11-devel
Requires: libX11
Requires: libXau
Requires: libXdmcp
Requires: libxcb
Requires: mona-fonts >= 2.90-7m

%description
xaav is display AA(ASCII ART) software use Xlib.

%prep
%setup -q -n xlib_xaav

%build
%{__rm} -f xaav
make CFLAGS="-Wall -std=c99" \
     INCLUDES="-I%{_includedir} -Iinclude" \
     LIBS="-L%{_libdir} -lX11"

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_bindir}
%{__install} -m0755 xaav %{buildroot}%{_bindir}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(0755,root,root) %{_bindir}/xaav

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-9m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- Requires: mona-fonts

* Fri Nov  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- enable x86_64

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.6

* Sat May  3 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-1m)
- update to 1.0
- move to main by license change

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-3m)
- rebuild against gcc43

* Sun Mar 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-2m)
- fix xaav permission to 0755
- add BuildRequires and Requires

* Mon Mar 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-1m)
- initial version for momonga
