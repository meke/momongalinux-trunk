%global momorel 9

Summary: Utilities to create compressed CD-ROM filesystems.
Name: zisofs-tools
Version: 1.0.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL: http://www.kernel.org/pub/linux/utils/fs/zisofs/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://www.kernel.org/pub/linux/utils/fs/zisofs/zisofs-tools-%{version}.tar.bz2 
%description
Tools that, in combination with an appropriately patched version of
mkisofs, allow the creation of compressed CD-ROM filesystems.

%prep
%setup -q 

%build
%configure
%make

%install
rm -rf %{buildroot}
make install INSTALLROOT="%{buildroot}"

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README zisofs.magic
%{_bindir}/mkzftree
%{_mandir}/man1/mkzftree.1*

%changelog
* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.8-9m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.8-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-2m)
- %%NoSource -> NoSource

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.0.4-2m)
- remove Epoch

* Tue Apr 20 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org> 
- (1.0.4-1m)
- Import to momonga

* Wed Nov  6 2002 H. Peter Anvin <hpa@zytor.com> 1.0.4
- Revision update.

* Thu Nov  8 2001 H. Peter Anvin <hpa@zytor.com> 1.0.3
- Revision update.

* Mon Oct 29 2001 H. Peter Anvin <hpa@zytor.com> 1.0.2
- Initial version.
