%global momorel 1
%global date 20120823
%global _sbindir /sbin

Name: flashcache-utils
Version: 0.0.%{date}
Release: %{momorel}m%{?dist}
Summary: general purpose writeback block cache for Linux
License: GPLv2
Group: System Environment/Base
URL: https://github.com/facebook/flashcache
Source0: %{name}-%{date}.tar.xz 
BuildRequires: gcc git
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Utilities for manipulating FlashCache.

%prep
%setup -q -n %{name}-%{date}

%build
CFLAGS="$RPM_OPT_FLAGS" make -C src utils

%install
rm -rf --preserve-root %{buildroot}
make -C src utils_install DESTDIR=%{buildroot}

mkdir -p %{buildroot}/%{_bindir}
install -m755 utils/flashstat %{buildroot}/%{_bindir}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE README
%doc doc/*
%{_sbindir}/flashcache_create
%{_sbindir}/flashcache_destroy
%{_sbindir}/flashcache_load
%{_sbindir}/flashcache_setioctl
%{_sbindir}/get_agsize
%{_bindir}/flashstat

%changelog
* Mon Sep  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20120823-1m)
- update 20120823

* Sun Aug  5 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20120721-1m)
- update 20120721

* Thu May 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20120430-1m)
- update 20120430

* Mon Jan 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20120111-1m)
- update 20120111
-- need current kernel

* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.20111026-1m)
- Initial Commit Momonga Linux

