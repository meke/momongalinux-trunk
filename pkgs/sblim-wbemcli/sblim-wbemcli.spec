%global momorel 5

Name:           sblim-wbemcli
Version:        1.6.0
Release:        %{momorel}m%{?dist}
Summary:        SBLIM WBEM Command Line Interface

Group:          Applications/System
License:        EPL
URL:            http://sblim.wiki.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/sblim/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         sblim-wbemcli-1.5.1-gcc43.patch
Patch1:         sblim-wbemcli-1.6.0-consts.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  curl-devel >= 7.9.3
BuildRequires:  binutils-devel >= 2.17.50.0.3-4
Requires:       curl >= 7.9.3
Requires:       tog-pegasus

%description
WBEM Command Line Interface is a standalone, command line WBEM client. It is
specially suited for basic systems management tasks as it can be used in
scripts.

%prep
%setup -q
%patch0 -p1 -b .gcc43
%patch1 -p1 -b .consts

%build
%configure CACERT=/etc/Pegasus/client.pem
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/wbem*
%{_mandir}/man1/*
%{_datadir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-1m)
- sync with Fedora 11 (1.6.0-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-3m)
- rebuild against rpm-4.6

* Wed Sep 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-2m)
- add patch0 to fix build break

* Wed Sep 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.1-1m)
- import from Fedora

* Mon Nov 06 2006 Jindrich Novy <jnovy@redhat.com> 1.5.1-5
- rebuild against new curl

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 1.5.1-4
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Nov 21 2005 Viktor Mihajlovski <mihajlov@de.ibm.com> 1.5.1-1
  - Upgrade to version 1.5.1 (SSL V3 enforced, default CACERT).
    Created Fedora/RH specific spec file.

* Thu Oct 28 2005 Viktor Mihajlovski <mihajlov@de.ibm.com> 1.5.0-1
  - Minor enhancements for Fedora compatibility, still not daring to
    nuke the build root though

* Thu Jul 28 2005 Viktor Mihajlovski <mihajlov@de.ibm.com> 1.5.0-0
  - Updates for rpmlint complaints
