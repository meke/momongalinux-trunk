%global momorel 10

Summary: Programming Ruby - A Pragmatic Programmer's Guide
Name: ruby-programming-ruby
Version: 0.4
Release: %{momorel}m%{?dist}
Source0: http://www.rubycentral.com/downloads/files/ProgrammingRuby-%{version}.tgz 
#NoSource: 0
URL: http://www.rubycentral.com/book/
Group: Documentation
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
License: OPL

%description
A comprehensive Ruby guidebook by Dave Thomas and Andrew Hunt, the authors of
"The Pragmatic Programmer".

This package includes the book in both HTML and XML. The latter could
be used to deliver the book in various format. If you wrote any
formatter, contact rubybook@pragmaticprogramer.com.

%prep
%setup -q -n ProgrammingRuby-%{version}

%clean
rm -rf %{buildroot}

%files
%defattr (-,root,root)
%doc COPYING README html xml

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-8m)
- full rebuild for mo7 release

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-7m)
- no NoSource

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-3m)
- %%NoSource -> NoSource

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.4-2m)
- remove Epoch

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4-1m)
- rebuild against new environment.

* Tue Mar 19 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.3a-2k)
- New version.

* Fri Feb 22 2001 OZAWA -Crouton- Sakuro <crouton@weatherlight.org>
- First release for Kondara.
