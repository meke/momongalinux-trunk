%global momorel 1

Summary: System for layout and rendering of internationalized text
Name: pango
Version: 1.36.3
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://www.pango.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.36/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool >= 1.5.22
BuildRequires: gtk-doc
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.22.0
BuildRequires: freetype-devel >= 2.3.9
BuildRequires: fontconfig-devel
BuildRequires: cairo-devel >= 1.8.6
BuildRequires: libX11-devel, libXft-devel, libXrender-devel
BuildRequires: libthai-devel
BuildRequires: harfbuzz-devel >= 0.9.9
BuildRequires: gobject-introspection-devel >= 0.9.6

%description
System for layout and rendering of internationalized text.

%package devel
Summary:	System for layout and rendering of internationalized text
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel, cairo-devel
Requires: libX11-devel, libXft-devel, libXrender-devel
Requires: freetype-devel, fontconfig-devel >= 2.10.91

%description devel
The pango-devel package includes the static libraries and header files
for the pango package.

Install pango-devel if you want to develop programs which will use
pango.

%prep
%setup -q

%build
%configure \
    --without-qt \
    --enable-gtk-doc \
    --enable-man \
    --with-included-modules=basic-fc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

touch $RPM_BUILD_ROOT%{_libdir}/pango/1.8.0/modules.cache

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
/usr/bin/pango-querymodules --update-cache || :

%postun 
/sbin/ldconfig
if test $1 -gt 0; then
  /usr/bin/pango-querymodules --update-cache || :
fi

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog
%{_bindir}/pango-querymodules
%{_bindir}/pango-view
%{_libdir}/libpango*-*.so.*
%exclude %{_libdir}/*.la
%dir %{_libdir}/pango
%{_libdir}/pango/
%ghost %{_libdir}/pango/1.8.0/modules.cache
%{_mandir}/man1/pango-querymodules.1.*
%{_mandir}/man1/pango-view.1.*
%{_libdir}/girepository-1.0/Pango-1.0.typelib
%{_libdir}/girepository-1.0/PangoCairo-1.0.typelib
%{_libdir}/girepository-1.0/PangoFT2-1.0.typelib
%{_libdir}/girepository-1.0/PangoXft-1.0.typelib

%files devel
%defattr(-, root, root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/pango.pc
%{_libdir}/pkgconfig/pangoft2.pc
%{_libdir}/pkgconfig/pangoxft.pc
%{_libdir}/pkgconfig/pangocairo.pc
%{_includedir}/pango-1.0
%doc %{_datadir}/gtk-doc/html/pango
%{_datadir}/gir-1.0/Pango-1.0.gir
%{_datadir}/gir-1.0/PangoCairo-1.0.gir
%{_datadir}/gir-1.0/PangoFT2-1.0.gir
%{_datadir}/gir-1.0/PangoXft-1.0.gir

%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org> 
- (1.36.3-1m)
- update to 1.36.3

* Wed Jan  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.32.5-1m)
- update 1.32.5

* Mon Dec 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32.4-1m)
- update to 1.32.4
- enable to build with halfbuzz-0.9.9

* Tue Oct 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.32.1-1m)
- update to 1.32.1

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.30.1-1m)
- update to 1.30.1

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.4-1m)
- update to 1.29.4

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.3-1m)
- update to 1.29.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28.4-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.4-1m)
- update to 1.28.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28.3-4m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.3-1m)
- update 1.28.3

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.1-3m)
- rebuild against gobject-introspection-0.9.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.28.1-2m)
- full rebuild for mo7 release

* Fri Jun 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.1-1m)
- update 1.28.1

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.28.0-1m)
- update 1.28.0 stable-release

* Fri Feb 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.27.1-1m)
- [SECURITY] CVE-2010-0421
- update to 1.27.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26.2-3m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.26.2-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.26.2-1m)
- update to 1.26.2

* Thu Nov 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.26.1-1m)
- update to 1.26.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.26.0-1m)
- update to 1.26.0

* Fri Jul 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.5-1m)
- update to 1.24.5
- delete patch0 merged

* Mon Jul 20 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24.4-2m)
- add bug586814-fixup.patch 

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.4-1m)
- update to 1.24.4

* Sat Jun 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.3-1m)
- update to 1.24.3

* Wed May  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.2-1m)
- update to 1.24.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.1-1m)
- update to 1.24.1

* Wed Mar 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.0-1m)
- [SECURITY] CVE-2009-1194
- update to 1.24.0

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.23.0-1m)
- update to 1.23.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22.4-2m)
- rebuild against rpm-4.6

* Tue Dec 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.22.4-1m)
- update to 1.22.4

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.22.3-1m)
- update to 1.22.3

* Wed Oct 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.22.2-1m)
- update to 1.22.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.22.1-1m)
- update to 1.22.1

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.22.0-1m)
- update to 1.22.0

* Thu Jul  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.5-1m)
- update to 1.20.5

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.4-1m)
- update to 1.20.4

* Sun Jun  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.3-2m)
- fix Req for devel

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.3-1m)
- update to 1.20.3

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.2-1m)
- update to 1.20.2

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.1-1m)
- update to 1.20.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20.0-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.0-1m)
- update to 1.20.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.4-1m)
- update to 1.18.4

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.3-1m)
- update to 1.18.3

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.2-1m)
- update to 1.18.2

* Tue Aug 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.1-1m)
- update to 1.18.1

* Tue Aug 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.0-1m)
- update to 1.18.0

* Thu May  3 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.16.4-1m)
- update to 1.16.4

* Wed Apr 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.16.2-1m)
- update to 1.16.2

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.16.1-2m)
- pango-devel Requires: freetype2-devel -> freetype-devel

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.16.1-1m)
- update to 1.16.1

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.16.0-2m)
- add patch2 (xchat was broken)

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.16.0-1m)
- update to 1.16.0

* Sun Feb 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.15.6-3m)
- add --with-included-modules=basic-fc to configure

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.15.6-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.15.6-1m)
- update to 1.15.6 (unstable)
- delete patch1 (merged with FC_HINT_STYLE)

* Thu Feb  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.10-1m)
- update to 1.14.10
- delete pango-1.12.2-fc-fake-faces.patch (merged)

* Thu Nov 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.8-1m)
- update to 1.14.8

* Sat Nov 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.7-1m)
- update to 1.14.7

* Fri Oct 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.14.6-1m)
- update to 1.14.6 (bug-fix release)

* Thu Oct  5 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.14.5-1m)
- update to 1.14.5

* Sat Sep 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.14.4-1m)
- update to 1.14.4

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14.3-1m)
- update to 1.14.3

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.12.3-2m)
- rebuild against expat-2.0.0-1m

* Thu Jun  1 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.12.3-1m)
- update to 1.12.3 (bug-fix release)

* Sun May  7 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.12.2-1m)
- update to 1.12.2 (bug-fix release)
- Patch2: pango-1.12.2-fc-fake-faces.patch (faulty patch)

* Sat Apr  8 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.12.1-1m)
- update to 1.12.1 (bug-fix release)

* Wed Mar 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.12.0-1m)
- update to 1.12.0

* Thu Feb  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.3-1m)
- update 1.10.3
- add --enable-static to configure

* Fri Dec  9 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.10.2-1m)
- update 1.10.2 (bug-fix release)

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-4m)
- comment out unnessesaly autoreconf and make check

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-3m)
- enable gtk-doc man

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-2m)
- rebuils for gtk-doc-1.4
- GNOME 2.12.1 Desktop

* Wed Oct  5 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.10.1-1m)
- update 1.10.1

* Thu Sep 21 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.10.0-2m)
- revised Patch0

* Sat Sep 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.10.0-1m)
- update 1.10.0

* Mon Apr 11 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.1-1m)
- update 1.8.1

* Mon Jan 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.0-1m)
- version 1.8.0

* Tue Nov 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.6.0-2m)
- add some patches.

* Mon Oct 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.6.0-1m)
- version 1.6.0

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (1.4.1-1m)
- version 1.4.1

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.0-3m)
- adjustment BuildPreReq

* Sun Apr 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.0-2m)
- revised spec for rpm 4.2.

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.0-1m)
- version 1.4.0
- GNOME 2.6 Desktop

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (1.2.5-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.2.5-1m)
- version 1.2.5

* Tue Jun 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.3-1m)
- version 1.2.3

* Mon Jun 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.2-1m)
- version 1.2.2

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-3m)
- rebuild against for XFree86-4.3.0

* Sun Mar 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-2m)
- fix URL

* Tue Feb 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-1m)
- version 1.2.1

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.0-1m)
- version 1.2.0

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.5-1m)
- version 1.1.5

* Wed Dec 04 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.4-1m)
- version 1.1.4

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.3-1m)
- version 1.1.3

* Sat Oct 19 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.2-2m)
- pkgconfig.patch

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.2-1m)
- version 1.1.2

* Sun Oct	 6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.1-3m)
- 1.1.1-1m

* Sun Oct	 6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.1-2m)
- revise to prevent building failure

* Fri Oct	 4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.1-1m)
- version 1.1.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.4-1m)
- version 1.0.4

* Thu Jul 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.3-4m)
- BuildPreReq: XFree86-devel

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.3-2k)
- version 1.0.3

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.2-2k)
- version 1.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-2k)
- version 1.0.1

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-2k)
- version 1.0.0

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0.rc2-2k)
- version 1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0.rc1-2k)
- version 1.0.0.rc1

* Tue Mar	 5 2002 Shingo Akagaki <dora@kondara.org>
- (0.26-4k)
- modify dependency list

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.26-2k)
- version 0.26

* Sun Feb 24 2002 Kenta MURATA <muraken2@nifty.com>
- (0.25-4k)
- buildprereq: libtool >= 1.4.2

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.25-2k)
- version 0.25

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.24-6k)
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.24-2k)
- version 0.24
- rebuild against for glib-1.3.13

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (0.23-2k)
- port from Jirai
- up to 0.23

* Fri Dec 22 2001 Motonobu Ichimura <famao@kondara.org>
- (0.22-3k)
- up to 0.22

* Tue Sep 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.19-3k)
- version 0.19
- shine! qt

* Thu Sep 13 2001 Motonobu Ichimura <famao@kondara.org>
- up to 0.18

* Wed Jul 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.17

* Thu Apr 26 2001 Shingo Akagaki <dora@kondarar.org>
- first release
