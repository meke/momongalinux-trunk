%global momorel 4

%define dbver_rel 4.0
%define dbver_snap 20100204
%define hpijsver 20090701

Summary: Database of printers and printer drivers
Name: foomatic-db
Version: %{dbver_rel}
Release: 0.%{dbver_snap}.%{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
Requires: %{name}-filesystem = %{version}-%{release}
Requires: %{name}-ppds = %{version}-%{release}

Source0: http://www.openprinting.org/download/foomatic/foomatic-db-%{dbver_rel}-%{dbver_snap}.tar.gz
Source1: http://www.openprinting.org/download/foomatic/foomatic-db-hpijs-%{hpijsver}.tar.gz

Patch1: foomatic-db-device-id-kyocera-mita-fs-1020d.patch
Patch2: foomatic-db-bad-margins.patch
Patch3: foomatic-db-device-id-hp-ps2570-dj959c-ojpk550.patch
Patch4: foomatic-db-device-ids-20100416.patch

Url: http://www.openprinting.org
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# The foomatic oki8w driver works for printers that this old package
# use to drive:
Obsoletes: oki4linux < 2.1gst-5
# Note: no "Provides:" as it was not a dependency of any package.

# Make sure we get postscriptdriver tags.
BuildRequires: pycups

%description
This is the database of printers, printer drivers, and driver options
for Foomatic.

The site http://www.openprinting.org/ is based on this database.

%package filesystem
Summary: Directory layout for the foomatic package
License: Public Domain
Group: System Environment/Base

%description filesystem

Directory layout for the foomatic package.

%package ppds
Summary: PPDs from printer manufacturers
License: GPLv2+ and MIT
Group: System Environment/Libraries
# We ship a symlink in a directory owned by cups
BuildRequires: cups
Requires: cups
Requires: sed
Requires: %{name}-filesystem = %{version}-%{release}

%description ppds
PPDs from printer manufacturers.

%prep
%setup -q -c -a 1

find -type d | xargs chmod g-s

pushd foomatic-db-%{dbver_snap}

pushd db/source

# For gutenprint printers, use gutenprint-ijs-simplified.5.2.
perl -pi -e 's,>gutenprint<,>gutenprint-ijs-simplified.5.2<,' printer/*.xml

# Remove references to foo2zjs, foo2oak, foo2hp and foo2qpdl (bug #208851).
# Also foo2lava, foo2kyo, foo2xqx (bug #438319).
# Also foo2slx and foo2hiperc (bug #518267).
for x in zjs oak hp qpdl lava kyo xqx slx hiperc
do
  find printer -name '*.xml' |xargs grep -l "<driver>foo2${x}"|xargs rm -vf
  rm -f driver/foo2${x}.xml opt/foo2${x}-*
done
# Same for m2300w/m2400w
find printer -name '*.xml' |xargs grep -l '<driver>m2[34]00w<'|xargs rm -vf
rm -f driver/m2300w.xml driver/m2400w.xml opt/m2300w-*
# Same for all these.
for x in drv_x125 ml85p pbm2lwxl pbmtozjs bjc800j
do
  find printer -name '*.xml' |xargs grep -l "<driver>${x}</driver>"|xargs rm -vf
  rm -vf driver/${x}.xml opt/${x}-*
done

# Use sed instead of perl in the PPDs (bug #512739).
find PPD -type f -name '*.ppd' | xargs perl -pi -e 's,perl -p,sed,'

popd

# foomatic-db patches
# Don't use "-b" when patching PPD files as the backups will be
# packaged.

# Device ID for Kyocera Mita FS-1020D (bug #575063).
%patch1 -p1

# Fixed missing units in driver margins (bug #576370).
%patch2 -p1 -b .bad-margins

# Device IDs for HP PhotoSmart 2570, HP DeskJet 959C and HP OfficeJet
# Pro K550 (bug #577280, bug #577293, bug #577296).
%patch3 -p1 -b .device-id-hp-ps2570-dj959c-ojpk550.patch

# Device IDs for:
# - Canon BJC-4100 (bug #583060)
# - HP Color LaserJet 3800 (bug #581936).
# - HP DeskJet D2300 (bug #580341).
# - HP DeskJet F2100 (bug #579245).
# - HP OfficeJet 7300 (bug #577897).
# - Lexmark E120 (bug #577881).
# - HP DeskJet 1280 (bug #577870).
# - HP PhotoSmart 7400 (bug #577866).
# - Brother HL-2140 (bug #577863).
# - HP OfficeJet 6200 (bug #215063).
# - HP PSC 2400 (bug #188419).
%patch4 -p1 -b .device-ids-20100416.patch

popd

%build
pushd foomatic-db-hpijs-%{hpijsver}
%configure
make PREFIX=%{_prefix}
popd

pushd foomatic-db-%{dbver_snap}
%configure
make PREFIX=%{_prefix}
popd


%install
rm -rf $RPM_BUILD_ROOT

pushd foomatic-db-%{dbver_snap}
make	DESTDIR=%buildroot PREFIX=%{_prefix} \
	install
popd

pushd foomatic-db-hpijs-%{hpijsver}
make	DESTDIR=%buildroot PREFIX=%{_prefix} \
	install
popd

# Remove ghostscript UPP drivers that are gone in 7.07
rm -f %{buildroot}%{_datadir}/foomatic/db/source/driver/{bjc6000a1,PM760p,PM820p,s400a1,sharp,Stc670pl,Stc670p,Stc680p,Stc760p,Stc777p,Stp720p,Stp870p}.upp.xml

find %{buildroot}%{_datadir}/foomatic/db/source/ -type f | xargs chmod 0644

mkdir %{buildroot}%{_datadir}/foomatic/db/source/PPD/Custom

rm -f	%{buildroot}%{_datadir}/foomatic/db/source/PPD/Kyocera/*.htm \
	%{buildroot}%{_datadir}/cups/model/3-distribution

# Convert absolute symlink to relative.
rm -f %{buildroot}%{_datadir}/cups/model/foomatic-db-ppds
ln -sf ../../foomatic/db/source/PPD %{buildroot}%{_datadir}/cups/model/foomatic-db-ppds

%clean
rm -rf %{buildroot}

%files filesystem
%defattr(-,root,root,-)
%dir %{_datadir}/foomatic
%dir %{_datadir}/foomatic/db
%dir %{_datadir}/foomatic/db/source

%files
%defattr(-,root,root,-)
%doc foomatic-db-%{dbver_snap}/db/source/PPD/Kyocera/*.htm
%doc foomatic-db-%{dbver_snap}/README
%doc foomatic-db-%{dbver_snap}/COPYING
%{_datadir}/foomatic/db/oldprinterids
%{_datadir}/foomatic/db/source/printer
%{_datadir}/foomatic/db/source/driver
%{_datadir}/foomatic/db/source/opt

%files ppds
%defattr(-,root,root,-)
%{_datadir}/foomatic/db/source/PPD
%{_datadir}/cups/model/foomatic-db-ppds

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-0.20100204.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-0.20100204.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-0.20100204.2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0-0.20100204.1m)
- import from Fedora

* Fri Apr 16 2010 Tim Waugh <twaugh@redhat.com> 4.0-14.20100204
- Device IDs for:
  - Canon BJC-4100 (bug #583060)
  - HP Color LaserJet 3800 (bug #581936).
  - HP DeskJet D2300 (bug #580341).
  - HP DeskJet F2100 (bug #579245).
  - HP OfficeJet 7300 (bug #577897).
  - Lexmark E120 (bug #577881).
  - HP DeskJet 1280 (bug #577870).
  - HP PhotoSmart 7400 (bug #577866).
  - Brother HL-2140 (bug #577863).
  - HP OfficeJet 6200 (bug #215063).
  - HP PSC 2400 (bug #188419).

* Fri Mar 26 2010 Tim Waugh <twaugh@redhat.com> 4.0-13.20100204
- Device IDs for HP PhotoSmart 2570, HP DeskJet 959C and HP OfficeJet
  Pro K550 (bug #577280, bug #577293, bug #577296).

* Thu Mar 25 2010 Tim Waugh <twaugh@redhat.com> 4.0-12.20100402
- Fixed missing units in driver margins (bug #576370).

* Fri Mar 19 2010 Tim Waugh <twaugh@redhat.com> 4.0-11.20100402
- Device ID for Kyocera Mita FS-1020D (bug #575063).

* Thu Feb  4 2010 Tim Waugh <twaugh@redhat.com> 4.0-10.20100402
- Rebuild for postscriptdriver tags.

* Thu Feb  4 2010 Tim Waugh <twaugh@redhat.com> 4.0-9.20100402
- Updated to foomatic-db-4.0-20100402.

* Fri Dec  4 2009 Tim Waugh <twaugh@redhat.com> 4.0-8.20091126
- Added foomatic-db-hpijs tarball back in.

* Thu Nov 26 2009 Tim Waugh <twaugh@redhat.com> 4.0-7.20091126
- Updated to foomatic-db-4.0-20091126 (bug #538994).

* Thu Aug 20 2009 Tim Waugh <twaugh@redhat.com> 4.0-6.20090819
- Removed references to foo2slx and foo2hiperc (bug #518267).

* Wed Aug 19 2009 Tim Waugh <twaugh@redhat.com> 4.0-5.20090819
- Updated to foomatic-db-4.0-20090819.
- Removed deprecated foomatic-db-hpijs tarball.
- Use buildroot macro throughout.

* Tue Aug 18 2009 Tim Waugh <twaugh@redhat.com> 4.0-4.20090702
- Use stcolor driver for Epson Stylus Color 200 (bug #513676).

* Mon Aug 17 2009 Tim Waugh <twaugh@redhat.com> 4.0-3.20090702
- License for ppds sub-package should include GPLv2+.
- Ship COPYING file in main package.
- Added filesystem sub-package for directory ownership.

* Mon Aug  3 2009 Tim Waugh <twaugh@redhat.com> 4.0-2.20090702
- Move foomatic-db-ppds symlink to ppds sub-package.
- Use sed instead of perl in raster PPDs (bug #512739).
- Removed code to convert old-style printer IDs (there are none).
- Ship README file.

* Mon Aug  3 2009 Tim Waugh <twaugh@redhat.com> 4.0-1.20090702
- Split database out from main foomatic package.
