%global momorel 3

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           telepathy-butterfly
Version:        0.5.15
Release:        %{momorel}m%{?dist}
Summary:        MSN connection manager for Telepathy
Group:          Applications/Communications
License:        GPLv2+
URL:            http://telepathy.freedesktop.org/wiki/
Source0:        http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  dbus-devel
BuildRequires:  python-devel >= 2.7
BuildRequires:  pkgconfig
Requires:       python-telepathy >= 0.15.17
Requires:       papyon >= 0.4.9
Requires:       dbus
Requires:       telepathy-filesystem
Requires:       libproxy-python
BuildArch:      noarch

%description
An MSN connection manager that handles presence, personal messages,
and conversations

%prep
%setup -q

%build
%configure
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS NEWS
%{_libexecdir}/%{name}
%{_datadir}/dbus-1/services/*.service
%{_datadir}/telepathy/managers/*.manager
%{python_sitelib}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.15-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.15-2m)
- rebuild for new GCC 4.6

* Sat Dec  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.15-1m)
- update to 0.5.15

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.14-2m)
- rebuild for new GCC 4.5

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.14-1m)
- update to 0.5.14

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.12-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.12-1m)
- import from Fedora devel

* Fri Jul  9 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.5.12-1
- Update to 0.5.12.

* Tue Jun  8 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.5.11-1
- Update to 0.5.11.

* Thu May 20 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.5.10-1
- Update to 0.5.10.

* Sat Apr 24 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.5.9-1
- Update to 0.5.9.
- Add Requires on libproxy-python for automatic proxy config.
- Drop clean section. Not needed any longer.

* Sun Mar 21 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.5.7-1
- Update to 0.5.7.
- Drop build patch, since project has moved to autotools.

* Sat Mar 13 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.5.6-1
- Update to 0.5.6.

* Wed Mar 10 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.5.5-1
- Update to 0.5.5.
- Drop disable-av patch. Upstream has disabled audio-visual support.

* Tue Jan 19 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.5.4-1
- Update to 0.5.4.

* Sat Nov 21 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.3-2
- Add patch to disable audio-visual capabilities.

* Sun Oct 25 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.3-1
- Update to 0.5.3.

* Tue Oct 20 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.2-1
- Update to 0.5.2.

* Mon Sep 14 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.1-1
- Update to 0.5.1.

* Fri Aug 14 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.0-2
- Actually upload the tarball.

* Fri Aug 14 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.0-1
- Update to 0.5.0.
- Add requires on papyon, and drop req on pymsn.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jun 16 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.3.4-1
- Update to 0.3.4.
- Update build patch.

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan  6 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.3.3-1
- Update to 0.3.3.
- Add NEWS.

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.3.2-2
- Rebuild for Python 2.6

* Sun Jul 20 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.3.2-1
- Update to 0.3.2.

* Thu Jan 17 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.3.1-1
- Update to 0.3.1.
- Add patch to fix build.

* Mon Jan 14 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.3.0-1
- Update to 0.3.0.

* Sun Aug  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.1.4-2
- Update license tag.

* Tue Apr 10 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.1.4-1
- Update to 0.1.4.

* Fri Dec  8 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.1.3-3
- Add BR on python-devel.

* Fri Dec  8 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.1.3-2
- Rebuild against new python.

* Thu Nov 16 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.1.3-1
- Update to 0.1.3.

* Sun Oct  8 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.1.2-1
- Update to 0.1.2.

* Mon Oct  2 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.1.1-1
- Update to 0.1.1.

* Thu Sep 21 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.1.0-1
- Initial FE spec.

