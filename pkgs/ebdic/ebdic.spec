%global momorel 31

Name: ebdic
Version: 0.33.14
Release: %{momorel}m%{?dist}
License: GPLv2
Summary: Dictionary Reader
Group: User Interface/X
Source0: http://www2c.biglobe.ne.jp/~tnemoto/programs/bin/%{name}-%{version}.tar.gz
Patch0: ebdic-0.33.14-char_traits_DICCHAR.patch
Patch1:	ebdic-0.33.7-ppc.patch
Patch2:	ebdic-0.33.14-PROTOTYPES.patch
Patch3:	ebdic-0.33.14-gcc41.patch
Patch4: ebdic-0.33.14-gcc43.patch
Patch5: ebdic-0.33.14-gcc44.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www2c.biglobe.ne.jp/~tnemoto/programs/index.cgi
BuildRequires: gdk-pixbuf-devel >= 0.13.0
BuildRequires: gtk+1-devel >= 1.2.10
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: eb-devel >= 4.4.3
BuildRequires: libxml2-devel >= 2.4.9
BuildRequires: autoconf >= 2.52-8k, automake >= 1.5-8k
BuildRequires: gcc-c++ >= 3.4.1-1m

%description
'ebdic' is an dictionary reader with multimedia support.

%prep

%setup -q
%patch0 -p1 -b .char_traits_DICCHAR~
%patch2 -p1
%patch3 -p1 -b .gcc41~
%patch4 -p1 -b .gcc43~
%patch5 -p1 -b .gcc44~

%build
#export CC=gcc_2_95_3 CXX=g++_2_95_3 CFLAGS="$(echo '%{optflags}'|sed -e 's/-fstack-protector//g')" CXXFLAGS="$(echo '%{optflags}'|sed -e 's/-fstack-protector//g')"
%configure --disable-imagemagick --disable-largefiles
make pkgdatadir=%{_sysconfdir}

%install
rm -rf %{buildroot}
%makeinstall pkgdatadir=%{buildroot}%{_sysconfdir}
mv %{buildroot}/etc/ebdic.conf.skel %{buildroot}%{_sysconfdir}/ebdic.conf

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS ChangeLog README
%config %{_sysconfdir}/ebdic.conf
%config %{_sysconfdir}/ebdic.dtd
%{_bindir}/ebdic
%{_bindir}/readsnd
%{_datadir}/locale/*/LC_MESSAGES/ebdic.mo

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33.14-31m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.33.14-30m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.33.14-29m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.33.14-28m)
- full rebuild for mo7 release

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33.14-27m)
- rebuild against eb-4.4.3

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33.14-26m)
- rebuild against eb-4.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33.14-25m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Mar 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33.14-24m)
- rebuild against eb-4.4.1

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33.14-23m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33.14-22m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.33.14-21m)
- rebuild against gcc43

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33.14-20m)
- rebuild against eb-4.3.2

* Sun Jan 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.33.14-19m)
- rebuild against eb-4.3.1
- License: GPLv2

* Thu Nov 29 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.33.14-18m)
- added ebdic-0.33.14-gcc43.patch

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.33.14-17m)
- enable gcc-4.1
- - add Patch3: ebdic-0.33.14-gcc41.patch

* Wed Jun  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-16m)
- rebuild against eb-4.2

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-15m)
- rebuild against eb-4.1.3

* Sat Dec  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-14m)
- rebuild against eb-4.1.2

* Wed Sep  1 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.33.14-13m)
- rebuild against gcc-c++-3.4.1
- modify ebdic-0.33.14-char_traits_DICCHAR.patch for gcc34

* Sat Jun 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-12m)
- rebuild against eb-4.1

* Fri Feb 20 2004 Toru Hoshina <t@www.momonga-linux.org>
- (0.33.14-11m)
- rebuild against eb-4.0 2nd.

* Mon Oct 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-10m)
- rebuild against eb-4.0

* Sun Apr 20 2003 Kenta MURATA <muraken2@nifty.com>
- (0.33.14-9m)
- implements struct std::char_traits<DICCHAR>.
- cancel to use gcc2.95.3.

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.33.14-8m)
- rebuild against zlib 1.1.4-5m

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-7m)
- rebuild against eb-3.3.2

* Sun Mar  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-6m)
- rebuild against eb-3.3.1

* Thu Nov 14 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.33.14-5m)
- add BuildPrereq gcc2.95.3

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.33.14-4m)
- use gcc2.95.3 instead of gcc

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-3m)
- rebuild against eb-3.3

* Mon Jul 15 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.33.14-2m)
- Requires: eb >= 3.2.2-2k
- BuildPreReq: eb-devel >= 3.2.2-2k

* Thu Jul  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.33.14-1m)

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.33.13-6k)
- cancel gcc-3.1 autoconf-2.53

* Mon May 27 2002 Toru Hoshina <t@kondara.org>
- (0.33.13-4k)
- rebuild by g++ 2.95.3.

* Tue May  7 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.33.13-2k)

* Wed Feb 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.33.10-2k)
- remove ppc patch

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.33.9-14k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.33.9-12k)
- automake 1.5

* Sat Dec 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.33.9-10k)
- rebuild against eb-3.2

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.33.9-4k)
- rebuild against gettext 0.10.40.

* Wed Sep 19 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.33.9-2k)
- remove ebdic.sizet.patch since it has been merged

* Sat Sep  1 2001 Toru Hoshina <t@kondara.org>
- (0.33.7-4k)
- why ppc... locale.h is needed... broken autoconf?
- nawakene-sugi.

* Fri Aug 31 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.33.7-2k)
- Requires eb >= 3.1

* Fri Jun 29 2001 Motonobu Ichimura <famao@kondara.org>
- (0.33.4-8k)
- more hack :-)
- but ...

* Wed Jun 27 2001 Motonobu Ichimura <famao@kondara.org>
- (0.33.4-6k)
- added kondara.patch and remove xml2-adhoc.patch

* Wed Jun 27 2001 Motonobu Ichimura <famao@kondara.org>
- add xml2-adhoc.patch

* Sat May 19 2001 Toru Hoshina <toru@df-usa.com>
- update 0.33.4

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- update 0.32.2

* Fri Jan 19 2001 Toru Hoshina <toru@df-usa.com>
- update to 0.31.8

* Sat Oct 28 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 0.31.7

* Tue Oct 17 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- fix BuildPreReq

* Thu Jul 05 2000 Kenzi Cano<kc@furukawa.ch.kagu.sut.ac.jp>
- (0.31.1-1k)
- up to ver 0.31.1

* Thu Jun 29 2000 Kenzi Cano<kc@furukawa.ch.kagu.sut.ac.jp>
- (0.30.6-1k)
- first release

 
