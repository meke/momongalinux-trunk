%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0

Name:		xfce4-dev-tools
Version:	4.11.0
Release:	%{momorel}m%{?dist}
Summary:	Xfce developer tools

Group:		Development/Tools
License:	GPLv2+
URL:		http://xfce.org/~benny/projects/xfce4-dev-tools/
Source0:        http://archive.xfce.org/src/xfce/%{name}/%{xfcever}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:	glib2-devel
Requires:	gettext-devel, gtk-doc, libtool, intltool, make
# BuildArch:	noarch

%description
This package contains common tools required by Xfce developers and people
that want to build Xfce from SVN.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%make install DESTDIR=%{buildroot} mandir=%{_mandir}

mkdir %{buildroot}%{_datadir}/aclocal/
install -p -m 644 m4macros/xdt*.m4 %{buildroot}%{_datadir}/aclocal/ 

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog HACKING NEWS README
%{_bindir}/xdt-*
%dir %{_datadir}/xfce4
%{_datadir}/xfce4/dev-tools/
%{_datadir}/aclocal/*

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.0-1m)
- update to version 4.11

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to version 4.10

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- change Source0  URI

* Wed May  4 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.3-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.2-2m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.2-1m)
- update to 4.7.2

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-3m)
- rebuild against xfce4 4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0.1-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0.1-1m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-2m)
- rebuild against xfce4 4.4.1

* Sun Jan 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- import to Momonga from fc

* Mon Jan 22 2007 Christoph Wickert <fedora christoph-wickert de> - 4.4.0-1
- Update to 4.4.0.

* Sat Nov 11 2006 Christoph Wickert <fedora christoph-wickert de> - 4.3.99.2-1
- Update to 4.3.99.2.

* Tue Oct 03 2006 Christoph Wickert <fedora christoph-wickert de> - 4.3.99.1-3
- Require gettext-devel.
- Install m4 macros also to /usr/share/aclocal.

* Tue Oct 03 2006 Christoph Wickert <fedora christoph-wickert de> - 4.3.99.1-2
- Some more requires: glib2-devel, make and gtk-doc.
- Own %%{_datadir}/xfce4

* Sat Sep 23 2006 Christoph Wickert <fedora christoph-wickert de> - 4.3.99.1-1
- Initial Fedora Extras release.
