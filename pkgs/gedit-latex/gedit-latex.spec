%global momorel 1

Summary: a plugin that provides features to ease the edition of latex documents
Name:    gedit-latex
Version: 3.4.1
Release: %{momorel}m%{?dist}
License: GPLv3
Group: Applications/Editors
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.4/%{name}-%{version}.tar.xz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: gtk3-devel

Requires: gedit

%description
gedit-latex is a plugin that provides
features to ease the edition of latex documents.

%prep
%setup -q

%build
%configure --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc README COPYING ChangeLog NEWS AUTHORS
%{_libdir}/gedit/plugins/*
%{_datadir}/gedit/plugins/*
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.latex.gschema.xml

%changelog
* Fri Sep  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Sun Nov  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- initial build
