%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.12.3
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.12.3
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global sopranover 2.9.4
%global qimageblitzver 0.0.6

%global moz_pluginsdir %{_kde4_libdir}/mozilla/plugins

Name: kdnssd
Summary: Zeroconf Support for KDE
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: kdelibs >= %{version}
Requires: kdepimlibs >= %{version}
# kopete/yahoo
Requires(hint): jasper
## kppp
Requires: ppp
## krdc
BuildRequires: boost-devel >= 1.50.0
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: desktop-file-utils
BuildRequires: freenx-client-devel >= 1.0
BuildRequires: giflib-devel
BuildRequires: jasper-devel
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: kdepimlibs-devel >= %{version}
BuildRequires: kde-workspace-devel >= %{version}
BuildRequires: kde-baseapps-devel
BuildRequires: libgadu-devel >= 1.8.0
BuildRequires: libidn-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libktorrent-devel >= 1.3
BuildRequires: libmsn-devel >= 4.1
BuildRequires: libotr-devel
BuildRequires: libv4l-devel
BuildRequires: libvncserver-devel >= 0.9.9
BuildRequires: libxslt-devel
BuildRequires: libxml2-devel
BuildRequires: linphone-devel >= 3.4.3
BuildRequires: meanwhile-devel
BuildRequires: mozilla-filesystem
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: openslp-devel
BuildRequires: ortp-devel >= 0.15.0
BuildRequires: pcre-devel
BuildRequires: qca2-devel
BuildRequires: qimageblitz-devel >= %{qimageblitzver}
BuildRequires: soprano-devel >= %{sopranover}
BuildRequires: speex-devel
BuildRequires: sqlite-devel
BuildRequires: telepathy-qt4-devel >= 0.1.8
BuildRequires: webkitpart-devel >= 0.0.4
BuildRequires: openssl-devel >= 1.0.1c
BuildRequires: libmms-devel
BuildRequires: libsrtp-devel

Obsoletes: kdenetwork-kdnssd < %{version}-%{release}
Conflicts: kdenetwork < 4.10.90

%description
This package contains the Zeroconf support for KDE.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
export CXXFLAGS=-DGLIB_COMPILATION
%{cmake_kde4} \
	  ../
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING*
%{_kde4_libdir}/kde4/kded_dnssdwatcher.so
%{_kde4_libdir}/kde4/kio_zeroconf.so
%{_kde4_appsdir}/remoteview/zeroconf.desktop
%{_kde4_datadir}/kde4/services/zeroconf.protocol
%{_kde4_datadir}/kde4/services/kded/dnssdwatcher.desktop
%{_datadir}/dbus-1/interfaces/org.kde.kdnssd.xml

%changelog
* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- initial build for Momonga Linux

