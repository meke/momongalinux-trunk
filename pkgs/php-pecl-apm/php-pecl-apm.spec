%global momorel 1

# PECL(PHP Extension Community Library) related definitions
%{!?__pecl:     %{expand: %%global __pecl     %{_bindir}/pecl}}
%define pecl_name APM

%define extra_version beta3

Summary: Alternative PHP Monitor
Name: php-pecl-apm
Version: 1.0.0
Release: %{momorel}m%{?dist}
License: PHP
Group: Development/Languages
URL: http://pecl.php.net/package/%{pecl_name}
Source0: http://pecl.php.net/get/%{pecl_name}-%{version}%{extra_version}.tgz
NoSource: 0
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires: php-devel >= 5.2.0, php-pear
Requires: php >= 5.2.0
Requires(post): %{__pecl}
Requires(postun): %{__pecl}
%if 0%{?php_zend_api}
Requires: php(zend-abi) = %{php_zend_api}
Requires: php(api) = %{php_core_api}
%else
Requires: php-api = %{php_apiver}
%endif
Provides: php-pecl(%{pecl_name}) = %{version}-%{release}

%description
Monitoring extension for PHP, collects error events and send 
them to one of the drivers. SQLite and MySQL drivers are storing 
those in a database. More drivers to come.

%prep
%setup -c -q

%build
pushd %{pecl_name}-%{version}%{extra_version}
%{_bindir}/phpize
%configure  --enable-apm
make %{?_smp_mflags}
(echo "; Enable apm extension module"
 echo "extension=apm.so") > apm.ini.dist
popd

%install
pushd %{pecl_name}-%{version}%{extra_version}
rm -rf $RPM_BUILD_ROOT
install -D -p -m 0755 modules/apm.so %{buildroot}%{php_extdir}/apm.so
install -D -p -m 0644 apm.ini.dist %{buildroot}%{_sysconfdir}/php.d/apm.ini.dist

# Install XML package description
%{__mkdir_p} %{buildroot}%{pecl_xmldir}
%{__install} -m 644 ../package.xml %{buildroot}%{pecl_xmldir}/%{name}.xml
popd

%clean
rm -rf $RPM_BUILD_ROOT

%if 0%{?pecl_install:1}
%post
%{pecl_install} %{pecl_xmldir}/%{name}.xml >/dev/null || :
%endif

%if 0%{?pecl_uninstall:1}
%postun
if [ $1 -eq 0 ] ; then
    %{pecl_uninstall} %{pecl_name} >/dev/null || :
fi
%endif

%files
%defattr(-,root,root,-)
%doc %{pecl_name}-%{version}%{extra_version}/LICENSE %{pecl_name}-%{version}%{extra_version}/AUTHORS
%doc %{pecl_name}-%{version}%{extra_version}/web
%config(noreplace) %{_sysconfdir}/php.d/apm.ini.dist
%{php_extdir}/apm.so
%{pecl_xmldir}/%{name}.xml

%changelog
* Sun May 01 2011 Yasuo Ohgaki <yohgkai@momonga-linux.org>
- (1.0.0-1m)
- Initial release
