%global momorel 1
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Yet Another TeX mode for Emacs
Name: emacs-yatex
Version: 1.77
Release: %{momorel}m%{?dist}
License: "Distributable"
Group: Applications/Editors
Source0: http://www.yatex.org/yatex%{version}.tar.gz
NoSource: 0
URL: http://www.yatex.org/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Requires(post): info
Requires(preun): info
Obsoletes: yatex-emacs
Obsoletes: yatex-xemacs
Obsoletes: elisp-yatex
Provides:  elisp-yatex

%description
YaTeX automates typesetting and previewing of LaTeX and enables completing input of LaTeX mark-up
command such as \begin{}..\end{}.

%prep
%setup -q -n yatex%{version}
chmod +w *.el

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{e_sitedir}/yatex
mkdir -p %{buildroot}%{e_sitedir}/etc/yatex
mkdir -p %{buildroot}%{_infodir}

sed -i -e "s|    (expand-file-name help-file help-dir)|    (expand-file-name help-file \"/usr/share/emacs/site-lisp/etc/yatex\")|" yatexhlp.el
make PREFIX=%{buildroot}%{_prefix} \
     EMACS=emacs EMACSDIR=%{buildroot}%{_datadir}/emacs \
     LISPDIR=%{buildroot}%{e_sitedir}/yatex \
     HELPDIR=%{buildroot}%{e_sitedir}/etc/yatex \
     INFODIR=%{buildroot}%{_infodir} \
     INSTALL="install -m 644" \
     install-real

rm -f %{buildroot}%{e_sitedir}/yatex/yatexhlp.el.orig
rm -rf %{buildroot}%{e_sitedir}/yatex/docs %{buildroot}%{e_sitedir}/yatex/yatex.new

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/yatexe %{_infodir}/dir --section="Emacs" --entry="* YaTeX-e: (yatexe).    Yet Another tex-mode for Emacs (English)."
/sbin/install-info %{_infodir}/yatexj %{_infodir}/dir --section="Emacs" --entry="* YaTeX: (yatexj).      Yet Another tex-mode for Emacs (Japanese)."
/sbin/install-info %{_infodir}/yahtmle %{_infodir}/dir --section="Emacs" --entry="* YaHTML-e: (yahtmle).         Yet Another html-mode for Emacs (English)."
/sbin/install-info %{_infodir}/yahtmlj %{_infodir}/dir --section="Emacs" --entry="* YaHTML: (yahtmlj).      Yet Another html-mode for Emacs (Japanese)."

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/yatexe %{_infodir}/dir --section="Emacs"
    /sbin/install-info --delete %{_infodir}/yatexj %{_infodir}/dir --section="Emacs"
    /sbin/install-info --delete %{_infodir}/yahtmle %{_infodir}/dir --section="Emacs"
    /sbin/install-info --delete %{_infodir}/yahtmlj %{_infodir}/dir --section="Emacs"
fi

%files
%defattr(-,root,root,-)
%doc 00readme install readme.meadow.j yatex.new
%doc docs/{htmlqa,htmlqa.eng,qanda,qanda.eng,yatex.ref,yatexref.eng,yatexadd.doc,yatexgen.doc}
%dir %{_datadir}/emacs/site-lisp/etc
%dir %{e_sitedir}/yatex
%{e_sitedir}/yatex/*.el
%{e_sitedir}/etc/yatex
%{_infodir}/*

%changelog
* Thu Jul 25 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.77-1m)
- update to 1.77

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.74-13m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.74-12m)
- revise Provides and Obsoletes

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.74-11m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.74-10m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.74-9m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.74-8m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.74-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.74-6m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74-5m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74-4m)
- merge yatex-emacs to elisp-yatex
- kill yatex-xemacs

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74-3m)
- do not bytecompile

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74-1m)
- update to 1.74

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73-21m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73-20m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-19m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-18m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-17m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-16m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-15m)
- rebuild against emacs-23.0.92

* Sun Mar  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-14m)
- License: "Distributable"
- remove dumb fossil patches and functions

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-13m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-12m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-11m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-10m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73-9m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.73-8m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-7m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73-6m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73-5m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73-4m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73-3m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73-2m)
- rebuild against emacs-22.0.93

* Mon Dec 25 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.73-1m)
- update to 1.73

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.72.zw-3m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.72.zw-2m)
- rebuild against emacs-22.0.91

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.72.zw-1m)
- use yatex-current to make it work with recent Emacs.

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.72-6m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.72-5m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.72-4m)
- use %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.72-3m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.72-2m)
- rebuild against emacs-21.3.50

* Sat Dec 27 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.72-1m)
- update to 1.72

* Thu Aug 21 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.71-1m)
- update to 1.71
- minor bugfixes
- use %%momorel macro
- use %%NoSource macro

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.70-4m)
- rebuild against emacs-21.3
- use %%{_prefix} %%{_datadir} %%{_libdir} macro

* Mon Dec  9 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.70-3m)
- Requires: emacsen

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.70-2m)
- add BuildPreReq: xemacs-sumo

* Mon Jul 29 2002 smbd <smbd@momonga-linux.org>
- (1.70-1m)
- up to 1.70

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.69.2-12k)
- /sbin/install-info -> info in PreReq.

* Sun Apr 14 2002 Hidetomo Machi <mcht@kondara.org>
- (1.69.2-10k)
- grab /usr/share/emacs/site-lisp/etc (%files)

* Thu Nov  1 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.69.2-8k)
- Patch1 is renewal
- modify make option

* Tue Oct 30 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.69.2-6k)
- modify spec
- use "bytecompile-nw"

* Mon Oct 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.69.2-4k)
- modify BuildPreReq

* Sun Oct 28 2001 Hidetomo Machi <mcHT@kondara.org>
- (elis-yatex-1.69.2-2k)
- merge yatex-emacs and yatex-xemacs

* Tue Jan  9 2001 Toru Hoshina <toru@df-usa.com>
- (1.68-8k)
- don't use info.gz.

* Fri Oct 20 2000 Hidetomo Machi <mcHT@kondara.org>
- (1.68-5k)
- modify specfile (License)

* Tue Aug 15 2000 AYUHANA Tomonori <l@kondara.org>
- (1.68-2k)
- add yatex-nwmake.patch
- change %{_infodir}/*.gz -> %{_infodir}/**
- add BuildRequires: emacs

* Sun Aug 13 2000 Masaki IMURA <imura-m@fg7.so-net.ne.jp>
- (1.68-1k)
- %build section - Vine (1.67vl5)
- yatex-info.diff patch Vine (1.67vl5) includes yahtmle.tex patch
- yatexhlp-emacs.diff (1.67vl5)
- compiling info files - [yatex:03209]
