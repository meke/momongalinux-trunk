%global momorel 6

%define lang it
%define langrelease 0
%define aspellversion 6
Summary: Italian dictionaries for Aspell
Name: aspell-%{lang}
Version: 2.2_20050523
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell%{aspellversion}-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Buildrequires: aspell >= 0.60
Requires: aspell >= 0.60
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for the following: Italian

%prep
%setup -q -n aspell%{aspellversion}-%{lang}-%{version}-%{langrelease}

%build
./configure
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING Copyright
%{_libdir}/aspell-0.60/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2_20050523-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2_20050523-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2_20050523-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2_20050523-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2_20050523-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahat <takahata@momonga-linux.org>
- (2.2_20050523-1m)
- import from Fedora

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 50:2.2_20050523-2
- Autorebuild for GCC 4.3

* Thu Mar 29 2007 Ivana Varekova <varekova@redhat.com> - 50:2.2_20050523-1  
- update to 2.2_20050523
- add documentation
- use configure script to create Makefile
- update default buildroot
- some minor spec changes

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 50:0.53-4.2.1
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 50:0.53-4.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 50:0.53-4.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 24 2006 Ivana Varekova <varekova@redhat.com> 50:0.53-4
- fix typo (bug 178755)

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Tue Jul 19 2005 Ivana Varekova <varekova@redhat.com> 50:0.53-3
- build with aspell-0.60.3

* Mon Apr 11 2005 Ivana Varekova <varekova@redhat.com> 50:0.53-2
- rebuilt

* Tue Sep 29 2004 Adrian Havill <havill@redhat.com> 50:0.53-1
- Upgrade dict to 0.50 series
