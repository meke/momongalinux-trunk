%global momorel 1

Name: libpcap
Version: 1.4.0
Release: %{momorel}m%{?dist}
Summary: A system-independent interface for user-level packet capture
Group: Development/Libraries
License: BSD
URL: http://www.tcpdump.org/
BuildRequires: glibc-kernheaders >= 2.2.0 bison flex bluez-libs-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source: http://www.tcpdump.org/release/%{name}-%{version}.tar.gz
NoSource: 0
Patch1: libpcap-man.patch
Patch2: libpcap-multilib.patch
Patch3: libpcap-s390.patch
Patch4: libpcap-0.8.3-ppp.patch
Patch5: libpcap-1.1.1-pcap-linux.patch

%description
Libpcap provides a portable framework for low-level network
monitoring.  Libpcap can provide network statistics collection,
security monitoring and network debugging.  Since almost every system
vendor provides a different interface for packet capture, the libpcap
authors created this system-independent API to ease in porting and to
alleviate the need for several system-dependent packet capture modules
in each application.

Install libpcap if you need to do low-level network traffic monitoring
on your network.

%package devel
Summary: Libraries and header files for the libpcap library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libpcap provides a portable framework for low-level network
monitoring.  Libpcap can provide network statistics collection,
security monitoring and network debugging.  Since almost every system
vendor provides a different interface for packet capture, the libpcap
authors created this system-independent API to ease in porting and to
alleviate the need for several system-dependent packet capture modules
in each application.

This package provides the libraries, include files, and other 
resources needed for developing libpcap applications.
 
%prep
%setup -q

%patch1 -p1 -b .man 
%patch2 -p1 -b .multilib
%patch3 -p1 -b .s390
%patch4 -p0 -b .ppp
%patch5 -p1 -b .pcap-linux

%build
export CFLAGS="$RPM_OPT_FLAGS -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE"
%configure --enable-ipv6
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make DESTDIR=$RPM_BUILD_ROOT install
rm -f $RPM_BUILD_ROOT%{_libdir}/libpcap.a

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE README CHANGES CREDITS
%{_libdir}/libpcap.so.*
%{_mandir}/man7/pcap*.7*

%files devel
%defattr(-,root,root)
%{_bindir}/pcap-config
%{_includedir}/pcap*.h
%{_includedir}/pcap
%{_libdir}/libpcap.so
%{_mandir}/man1/pcap-config.1*
%{_mandir}/man3/pcap*.3*
%{_mandir}/man5/pcap*.5*

%changelog
* Tue Jun 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update 1.4.0

* Sat Oct 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0

* Sun Jan  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-3m)
- full rebuild for mo7 release

* Fri May  7 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.1.1-2m)
- add pcap-linux.patch to fix getting interface bug from git

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-4m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-3m)
- update Patch2 for fuzz=0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.8-2m)
- rebuild against gcc43

* Mon Oct  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-2m)
- NoSource

* Thu Jun  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-1m)
- import from Fedora

* Tue Nov 28 2006 Miroslav Lichvar <mlichvar@redhat.com> 14:0.9.5-1
- split from tcpdump package (#193657)
- update to 0.9.5
- don't package static library
- maintain soname
