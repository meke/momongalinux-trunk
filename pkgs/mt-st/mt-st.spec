%global momorel 5

Summary: Programs to control tape device operations
Name: mt-st
Version: 1.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: ftp://ftp.ibiblio.org/pub/linux/system/backup
Source0: ftp://metalab.unc.edu/pub/Linux/system/backup/mt-st-%{version}.tar.gz 
NoSource: 0
Source1: stinit.init
Patch0: mt-st-1.1-redhat.patch
Patch1: mt-st-1.1-SDLT.patch
Patch2: mt-st-0.7-config-files.patch
Patch3: mt-st-0.9b-manfix.patch
Patch4: mt-st-1.1-mtio.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): chkconfig
Requires(preun): chkconfig

%description
The mt-st package contains the mt and st tape drive management
programs. Mt (for magnetic tape drives) and st (for SCSI tape devices)
can control rewinding, ejecting, skipping files and blocks and more.

Install mt-st if you need a tool to  manage tape drives.

%prep
%setup -q
%patch0 -p1 -b .redhat
%patch1 -p1 -b .sdlt
%patch2 -p1 -b .configfiles
%patch3 -p1 -b .manfix
%patch4 -p1 -b .mtio

# fix encoding
f=README.stinit
iconv -f ISO8859-1 -t UTF-8 -o $f.new $f
touch -r $f $f.new
mv $f.new $f

%build
make CFLAGS="%{optflags} -Wall"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/{bin,sbin}
mkdir -p %{buildroot}%{_mandir}/man{1,8}
make install MANDIR=%{buildroot}%{_mandir}
install -D -p -m 0755 %{SOURCE1} %{buildroot}%{_initscriptdir}/stinit

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add stinit

%preun
if [ $1 -eq 0 ]; then
    /sbin/chkconfig --del stinit
fi

%files
%defattr(-,root,root)
%doc COPYING README README.stinit mt-st-1.1.lsm stinit.def.examples
/bin/mt
/sbin/stinit
%{_mandir}/man[18]/*
%{_initscriptdir}/stinit

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- update to 1.1 based on Fedora 11 (1.1-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9b-6m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9b-5m)
- update Patch1 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9b-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9b-3m)
- %%NoSource -> NoSource

* Tue Feb 27 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9b-2m)
- Patch3: mt-st-0.9b-kh.patch

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.9b-1m)
- update to 0.9b

* Tue May 18 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8-1m)
- verup

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.7-3m)
- rebuild against new environment.
- applied config-files.patch.

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (0.7-2k)
- ver up.

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (0.6-2k)
- update to 0.6, supports all ioctls up to kernel 2.4.0

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Fri Apr 07 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.5b-7).

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Fri Jan 14 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for Red Hat 6.2.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Sep  5 1999 Jeff Johnson <jbj@redhat.com>
- enable "datcompression" command (#3654).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Wed Feb 10 1999 Preston Brown <pbrown@redhat.com>
- upgrade to .5b, which fixes some cmd. line arg issues (bugzilla #18)

* Thu Jul 23 1998 Jeff Johnson <jbj@redhat.com>
- package for 5.2.

* Sun Jul 19 1998 Andrea Borgia <borgia@cs.unibo.it>
- updated to version 0.5
- removed the touch to force the build: no binaries are included!
- added to the docs: README.stinit, stinit.def.examples
- made buildroot capable

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Oct 20 1997 Donnie Barnes <djb@redhat.com>
- spec file cleanups

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc
