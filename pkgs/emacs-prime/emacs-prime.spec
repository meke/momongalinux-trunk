%global momorel 39

%global emacsver %{_emacs_version}
%global apelver 10.8-6m
%global e_sitedir %{_emacs_sitelispdir}
%global e_startdir %{e_sitedir}/site-start.d
%global confsamp %{_datadir}/config-sample
%global lispname prime
%global pkgname prime-el

Summary: PRIME for Emacs
Name: emacs-%{lispname}
Version: 1.5.1.3
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://prime.sourceforge.jp/src/%{pkgname}-%{version}.tar.gz 
NoSource: 0
Source1: prime.dot.emacs
URL: http://taiyaki.org/prime/emacs/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-apel >= %{apelver}
BuildRequires: emacs-mell >= 1.0.0-30m
BuildRequires: suikyo >= 2.1.0-13m
BuildRequires: emacs-suikyo >= 2.1.0-13m
Requires: emacs >= %{emacsver}
Requires: emacs-apel >= %{apelver}
Requires: emacs-mell >= 1.0.0-30m
Requires: emacs-suikyo >= 2.1.0-13m
Obsoletes: prime-emacs
Obsoletes: prime-xemacs

Obsoletes: elisp-prime
Provides: elisp-prime

%description
PRIME for Emacs is a client of PRIME japanese input method.

%prep
%setup -q -n %{pkgname}-%{version}

%build
%configure --with-emacs-sitelispdir=%{e_sitedir} \
  --with-prime-configdir=%{_sysconfdir}/%{lispname} \
  --with-prime-el-initdir=%{e_startdir}
make
%{__mv} etc/init-prime.el etc/init-prime.el.emacs

cd src
emacs -batch -q -no-site-file -eval "(setq load-path (nconc '(\".\") '(\"./contrib\") '(\"%{e_sitedir}/mell\") '(\"%{e_sitedir}/suikyo\") load-path) byte-compile-warnings nil)" -f batch-byte-compile *.el contrib/*.el
for f in *.elc contrib/*.elc; do
  %{__mv} $f $f.emacs
done
for f in *.el contrib/*.el; do
  %{__cp} $f $f.emacs
done
cd -

%install
%{__rm} -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_sysconfdir}/%{lispname}
%{__install} -c -m 0644 etc/Custom_prime-el.el %{buildroot}%{_sysconfdir}/%{lispname}

%{__mkdir_p} %{buildroot}%{_datadir}/config-sample/%{name}
%{__install} -c -m 0644 %{S:1} %{buildroot}%{_datadir}/config-sample/%{name}

cd src
%{__mkdir_p} %{buildroot}%{e_startdir}
%{__install} -c -m 0644 ../etc/init-prime.el.emacs %{buildroot}%{e_startdir}/init-prime.el

%{__mkdir_p} %{buildroot}%{e_sitedir}/%{lispname}/contrib
for f in *.emacs contrib/*.emacs; do
  %{__install} -c -m 0644 $f %{buildroot}%{e_sitedir}/%{lispname}/${f%%.emacs}
done
#%%{__install} -c -m 0644 contrib/insparens.el %{buildroot}%{e_sitedir}/%{lispname}/contrib
#%%{__install} -c -m 0644 contrib/insparens.elc %{buildroot}%{e_sitedir}/%{lispname}/contrib
cd -

find doc -name "Makefile*" -exec %{__rm} -f {} \;

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/prime/Custom_prime-el.el
%doc COPYING README AUTHORS ChangeLog TODO doc
%dir %{_datadir}/config-sample/%{name}
%{_datadir}/config-sample/%{name}/*
%dir %{e_sitedir}/%{lispname}
%{e_sitedir}/%{lispname}/contrib/*.el*
%{e_sitedir}/%{lispname}/*.el*
%{e_startdir}/*.el

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1.3-39m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1.3-38m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.1.3-37m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1.3-36m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-35m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1.3-34m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.1.3-33m)
- full rebuild for mo7 release

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-32m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-31m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-30m)
- merge prime-emacs to elisp-prime
- kill prime-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-29m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-28m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-27m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-26m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-25m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-24m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-23m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-22m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-21m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-20m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-19m)
- rebuild against apelver 10.7-11m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-18m)
- rebuild against xemacs-21.5.28
- use %%{xe_sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-17m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-16m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.1.3-15m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-14m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-13m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-12m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-11m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-10m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1.3-9m)
- rebuild against apel-10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-8m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-7m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-6m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-5m)
- rebuild against emacs-22.0.90

* Mon Jan  9 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.1.3-4m)
- rebuild against elisp-mell-1.0.0-5m

* Wed Dec 21 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5.1.3-3m)
- Move "Requires: apel-(x)emacs" to corresponding prime-(x)emacs subpackage

* Thu Mar 05 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1.3-2m)
- add Requires: elisp-apel, apel-emacs, apel-xemacs for use poe

* Sat Mar 05 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5.1.3-1m)
- update to 1.5.1.3

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.2-4m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2-3m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.4.2-2m)
- rebuild against emacs-21.3.50.

* Wed Nov 10 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.4.2-1m)
- initial import for Momonga.
