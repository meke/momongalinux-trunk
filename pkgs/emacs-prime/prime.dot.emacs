;;; MELL (Emacs Lisp ライブラリ)
(require 'init-mell)
;;; Suikyo (ローマ字ひらがな変換ライブラリ)
(require 'init-suikyo)
;;; PRIME for Emacs
(require 'init-prime)

;;; leim によるインプットメソッドの管理
;(setq default-input-method "japanese-prime")

;;; Ctrl-\ で prime-mode を ON/OFF する (デフォルト設定).
(global-set-key "\C-\\" 'prime-mode)

;;; prime-enum-mode を OFF の状態から始める
(setq prime-enum-mode nil)

;;; [Space] で入力される空白と [Meta] + [Space] で入力される空白のペア
(setq prime-style-space '(" " . "　"))

;;; "\M-[" で入力される, 括弧のペア
(setq prime-insparens-template-list
  '(("「" . "」") ("『" . "』") ("【" . "】") ("〈" . "〉") ("《" . "》")
    ("（" . "）") ("〔" . "〕") ("［" . "］") ("｛" . "｝") ("“" . "”")
    ("‘" . "’") ("". "")))

;;; 句読点を自動的に変更する.
(setq prime-style-kutouten-autochange-p t)

;;; 変換をしないで直接入力をするキー
;(setq prime-direct-key-alist '((?] "」") (?[ "「")))
;(setq prime-direct-key-alist '((?] "」") (?[ "「") (?! "!"))

;;; 句読点を即確定させない.
(setq prime-style-kutouten-direct-p nil)

;; 値は, リストもしくは文字列を取る. リストの第二項目は TTY 時の値.
;; PRIME モード時のカーソルの色
(setq prime-cursor-color '("Pink" 6))