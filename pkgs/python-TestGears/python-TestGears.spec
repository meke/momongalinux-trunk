%global momorel 10

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

Name:           python-TestGears
Version:        0.2
Release:        %{momorel}m%{?dist}
Summary:        Unit testing for Python

Group:          Development/Languages
License:        MIT/X
URL:            http://www.turbogears.org/testgears/
Source0:        http://www.turbogears.org/download/eggs/TestGears-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7 python-setuptools >= 0.6c9-2m

%description
TestGears provides automatic discovery of unittest.TestCases and the ability
to run tests that are written as simple functions. It generates a standard
unittest.TestSuite for use with any of the standard frontends, and provides
a distutils command to run tests with zero configuration.

%prep
%setup -q -n TestGears-%{version}

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
echo "TestGears-%{version}-py%{pyver}.egg-info" > %{buildroot}%{python_sitelib}/TestGears.pth

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc docs LICENSE.txt TODO.txt
%{python_sitelib}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2-10m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-5m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-4m)
- rebuild against python-2.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-3m)
- rebuild against gcc43

* Sun Jun 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-2m)
- modify %%files

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2-1m)
- import from Fedora

* Sun Dec 10 2006 Luke Macken <lmacken@redhat.com> 0.2-4
- Rebuld for python 2.5

* Sun Sep 10 2006 Luke Macken <lmacken@redhat.com> 0.2-3
- Fix module inclusion, and use consistent buildroots

* Sun Sep 10 2006 Luke Macken <lmacken@redhat.com> 0.2-2
- Rebuild for FC6

* Sat Dec 24 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.2-1
- Initial RPM release
