%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Name: libtevent
Version: 0.9.21
Release: %{momorel}m%{?dist}
Group: System Environment/Daemons
Summary: The tevent library
License: LGPLv3+
URL: http://tevent.samba.org/
Source0: http://samba.org/ftp/tevent/tevent-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: libtalloc-devel >= 2.0.6

%description
Tevent is an event system based on the talloc memory management library.
Tevent has support for many event types, including timers, signals, and
the classic file descriptor events.
Tevent also provide helpers to deal with asynchronous code providing the
tevent_req (Tevent Request) functions. 

%package devel
Group: Development/Libraries
Summary: Developer tools for the Tevent library
Requires: libtevent = %{version}-%{release}
Requires: libtalloc-devel >= 2.0.6
Requires: pkgconfig

%description devel
Header files needed to develop programs that link against the Tevent library.

%package -n python-tevent
Group: Development/Libraries
Summary: Python bindings for the Tevent library
Requires: libtevent = %{version}-%{release}

%description -n python-tevent
Python bindings for libtevent

%prep
%setup -q -n tevent-%{version}

%build
%configure --disable-rpath --bundled-libraries=NONE --builtin-libraries=replace
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

rm -f $RPM_BUILD_ROOT%{_libdir}/libtevent.a

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_libdir}/libtevent.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/tevent.h
%{_libdir}/libtevent.so
%{_libdir}/pkgconfig/tevent.pc

%files -n python-tevent
%defattr(-,root,root,-)
%{python_sitearch}/tevent.py*
%{python_sitearch}/_tevent.so

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.21-1m)
- update to 0.9.21

* Fri Oct 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.19-1m)
- update to 0.9.19

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.18-1m)
- update to 0.9.18

* Wed Sep 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.17-1m)
- update 0.9.17

* Tue Jul 31 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.16-2m)
- fix build on x86_64

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.16-1m)
- update to 0.9.16

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.15-1m)
- update to 0.9.15

* Sat Dec 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.14-2m)
- import patches from Fedora
- add --builtin-libraries=replace option

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.14-1m)
- update 0.9.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-1m)
- import from Fedora 13 for sssd

* Wed Sep 23 2009 Simo Sorce <ssorce@redhat.com> - 0.9.8-5
- Add patch to fix a segfault case

* Wed Sep 16 2009 Simo Sorce <ssorce@redhat.com> - 0.9.8-2
- Fix abi compatibility with 0.9.3

* Sat Sep 8 2009 Simo Sorce <ssorce@redhat.com> - 0.9.8-1
- First independent release for tevent 0.9.8 
