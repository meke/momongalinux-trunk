%global         momorel 1

Summary:	A desktop search program for KDE
Name:		strigi
Version:	0.7.8
Release:	%{momorel}m%{?dist}
License:	LGPL
Group:		Applications/Productivity
URL:		http://www.vandenoever.info/software/strigi
Source0:        http://www.vandenoever.info/software/%{name}/%{name}-%{version}.tar.bz2
Source1:	strigiclient.desktop
Source2:	strigi-daemon.desktop
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource:	0
Patch0:         %{name}-%{version}-strigiclient-install-path.patch
BuildRequires:	bison
BuildRequires:	bzip2-devel
BuildRequires:	clucene-core-devel >= 2.3.3.4
BuildRequires:	cmake >= 2.4.8-5m
BuildRequires:	cppunit
BuildRequires:	dbus-devel
BuildRequires:	desktop-file-utils
BuildRequires:	expat-devel
BuildRequires:	exiv2-devel >= 0.23
BuildRequires:	libxml2-devel
BuildRequires:	qt-devel >= 4.7.0
BuildRequires:	zlib-devel

%description
Strigi is a fast and light desktop search engine. It can handle a large range
of file formats such as emails, office documents, media files, and file
archives. It can index files that are embedded in other files. This means email
attachments and files in zip files are searchable as if they were normal files
on your harddisk.

Strigi is normally run as a background daemon that can be accessed by many
other programs at once. In addition to the daemon, Strigi comes with powerful
replacements for the popular unix commands 'find' and 'grep'. These are called
'deepfind' and 'deepgrep' and can search inside files just like the strigi
daemon can.

%package	devel
Summary:	Development files for the strigi desktop search engine
Group:		Development/Libraries
Requires:	%name-libs = %version-%release
Requires:	pkgconfig

%description	devel
Development files for the strigi desktop search engine

%package	libs
Summary:	Strigi libraries
Group:		Development/Libraries

%description	libs
Strigi search engine libraries

%prep
%setup -q
%patch0 -p1

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake \
    -DENABLE_DBUS:BOOL=ON \
    -DENABLE_FAM:BOOL=ON \
    -DENABLE_FFMPEG:BOOL=OFF \
    %{?_cmake_skip_rpath} \
    ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf --preserve-root %{buildroot}
make install/fast -C %{_target_platform}  DESTDIR=%{buildroot}

desktop-file-install					\
	--vendor=					\
	--dir=%{buildroot}%{_datadir}/applications	\
	%{SOURCE1}

# Add an autostart desktop file for the strigi daemon
mkdir -p %{buildroot}%{_sysconfdir}/xdg/autostart
cp -pr %{SOURCE2} %{buildroot}%{_sysconfdir}/xdg/autostart/

find %{buildroot}%{_libdir} -type f -name "*.so*" -exec chmod 755 {} ';'

%clean
rm -rf --preserve-root %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README.win32 SPLIT_TODO TODOFILES
%{_bindir}/*
%{_datadir}/applications/*strigiclient.desktop
%{_datadir}/dbus-1/services/*.service
%{_sysconfdir}/xdg/autostart/strigi-daemon.desktop
%{_datadir}/strigi/

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/libstream*.pc
%{_libdir}/cmake/*
%{_includedir}/strigi

%files libs
%defattr(-,root,root,-)
%{_libdir}/*.so.*
%{_libdir}/strigi

%changelog
* Sun Jun 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.8-1m)
- update to 0.7.8

* Sun Sep 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.7-3m)
- rebuild against clucene-2.3.3.4

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.7-2m)
- rebuild against exiv2-0.23

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.7-1m)
- hope this will fix 100% CPU use issue...
- update to 0.7.7

* Wed Dec 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.6-1m)
- update to 0.7.6

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-2m)
- rebuild against exiv2-0.22

* Fri Oct 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-8m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-7m)
- rebuild against exiv2-0.21.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-6m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-3m)
- rebuild against qt-4.6.3-1m

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-2m)
- rebuild against exiv2-0.20

* Sun Feb  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Fri Jan 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-3m)
- rebuild against exiv2-0.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5
- drop gcc44 patch

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-2m)
- update Source2 (OnlyShowIn = KDE)

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Fri Feb  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-3m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-2m)
- add patch0 from Fedora devel

* Tue Jan 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-0.20081012.1m)
- update to 0.6.0 svn 20081012 snapshot

* Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-0.855802.1m)
- update to rev.855802 snapshot

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.11-1m)
- update to 0.5.11

* Mon Jul 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.10-2m)
- add icon to strigiclient.desktop

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.10-1m)
- update to 0.5.10

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.9-1m)
- update to 0.5.9

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.8-2m)
- rebuild against gcc43

* Thu Feb 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-1m)
- update to 0.5.8

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.7-3m)
- rebuild against exiv2-0.16

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.7-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.7-1m)
- import from Fedora devel

* Tue Nov 13 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.5.7-2
- Rebuild for new exiv2

* Tue Oct 30 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.7-1
- Update to 0.5.7 release
- Fix multilibs conflict (Bug #343221, patch by Kevin Kofler) 

* Sun Sep 09 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.5-2
- Rebuild for BuildID changes

* Sat Aug 11 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.5-1
- Update to 0.5.5 release

* Mon Aug 06 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.4-1
- Update to 0.5.4 proper
- License tag update

* Sun Jul 29 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.4-0.1.svn20070729
- New KDE SVN snapshot version for KDE 4.0 beta 1 (bz#20015)

* Wed May 16 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.1-5
- Split out a strigi-libs subpackage as suggested in BZ#223586
_ Include a strigidaemon autostart desktop file

* Sat May 05 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.1-4
- Add dbus-devel BR.

* Sat May 05 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.1-3
- Misc. fixes from package review

* Fri May 04 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.1-2
- Updates from reviews:
-	Have the -devel subpackage require pkgconfig
-	Add a versioned dependency on cmake and remove dbus-qt buildrequire

* Fri May 04 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.1-1
- New release

* Wed May 02 2007 Deji Akingunola <dakingun@gmail.com> - 0.3.11-3
- Allow building on FC6

* Thu Feb 22 2007 Deji Akingunola <dakingun@gmail.com> - 0.3.11-2
- Assorted fixed arising from reviews

* Wed Jan 17 2007 Deji Akingunola <dakingun@gmail.com> - 0.3.11-1
- Initial packaging for Fedora Extras
