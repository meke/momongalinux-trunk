%global module	XML-Dumper
%global momorel 24

Summary:	Perl module for dumping Perl objects from/to XML
Name:		perl-XML-Dumper
Version:	0.81
Release: %{momorel}m%{?dist}
License:	GPL or Artistic
Group: 		Development/Languages
Source0: http://www.cpan.org/modules/by-module/XML/%{module}-%{version}.tar.gz 
NoSource: 0
URL:		http://www.cpan.org/modules/by-module/XML/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Prefix:		%{_prefix}
BuildRequires:	perl >= 5.8.5
BuildRequires:	perl-XML-Parser >= 2.31-6m
BuildArch: 	noarch

%description
Perl module for dumping Perl objects from/to XML

Needed by eGrail

%prep
%setup -q -n %{module}-%{version}

%build
find . -type f | xargs perl -p -i -e 's|/usr/local/bin/perl|/usr/bin/perl|'
perl Makefile.PL INSTALLDIRS=vendor
make
#make test

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm -f %{buildroot}%{perl_vendorarch}/auto/XML/Dumper/.packlist

%clean 
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README Changes
%{_mandir}/man?/*
%{perl_vendorlib}/XML/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-24m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-23m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-22m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-21m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-20m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-19m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-18m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-17m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.81-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.81-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.81-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.81-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.81-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.81-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.81-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.81-2m)
- use vendor

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.81-1m)
- update to 0.81

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.79-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.79-1m)
- update to 0.79

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.73-1m)
- version up to 0.73
- built against perl-5.8.7

* Mon Feb 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.71-1m)

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.67-5m)
- rebuild against perl-5.8.5
- rebuild against perl-XML-Parser >= 2.31-6m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.67-4m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.67-3m)
- revised spec for rpm 4.2.

* Thu Dec 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.67-2m)
- comment out 'make test'

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.67-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-9m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4-8m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.4-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-6m)
- kill %%define name

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.4-5m)
- rebuild against perl-5.8.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-4k)
- rebuild against for perl-5.6.1

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (0.4-2k)
- merge from rawhide. based on 0.4-5.

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 2.30-5
- got it to work.

* Thu Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 2.30-4
- imported from mandrake. tweaked man path.

* Thu Jun 21 2001 Christian Belisle <cbelisle@mandrakesoft.com> 0.4-3mdk
- Fixed an error in changelog.

* Thu Jun 21 2001 Christian Belisle <cbelisle@mandrakesoft.com> 0.4-2mdk
- Clean up spec.
- Fixed distribution tag.
- Needed by eGrail.

* Mon Jun 18 2001 Till Kamppeter <till@mandrakesoft.com> 0.4-1mdk
- Newly introduced for Foomatic.

