%global momorel 2

Summary: Simple Scanning Utility
Name: simple-scan
Version: 3.2.1
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: User Interface/Desktops
URL: https://launchpad.net/simple-scan
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.2/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: gtk3-devel
BuildRequires: GConf2-devel
BuildRequires: glib2-devel
BuildRequires: cairo-devel
BuildRequires: dbus-glib-devel
BuildRequires: libgudev1-devel
BuildRequires: sane-backends-devel

%description
Simple Scanning Utility

%prep
%setup -q

%build
%configure --enable-silent-rules \
  --disable-schemas-install \
  --disable-scrollkeeper
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update

%postun
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_mandir}/man1/%{name}.1.*
%{_datadir}/gnome/help/%{name}
%{_datadir}/glib-2.0/schemas/org.gnome.SimpleScan.gschema.xml

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0.1-2m)
- rebuild for new GCC 4.5

* Fri Oct 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0.1-1m)
- update to 2.32.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- initial build
- 2.31.1 was released and merge to gnome
