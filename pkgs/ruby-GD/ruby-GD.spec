%global momorel 4

Summary: Ruby/GD
Name: ruby-GD
Version: 0.8.0
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: Ruby
URL: http://www.acc.ne.jp/~tam/GD/

# gem unpack ruby-gd-xxxx.gem
Source0: http://raa.ruby-lang.org/cache/ruby-gd/%{name}-%{version}.tar.gz
#NoSource: 0

# ruby-1.9 patch
# http://d.hatena.ne.jp/takihiro/20100321/1269074490
Patch0: GD.c.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: libjpeg-devel >= 8a
BuildRequires: freetype-devel
BuildRequires: gd-devel

%description
Ruby/GD (formerly known as "GD") is an extension library to use
Thomas Boutell's gd library (http://www.boutell.com/gd/) from Ruby.
#'

%prep
%setup -q 

%patch0 -p0

%build
ruby extconf.rb --with-xpm --with-jpeg --with-freetype --with-ttf --enable-gd2_0
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changes readme* doc sample
%{ruby_sitearchdir}/GD.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-1m)
- updte 0.8.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.4-19m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.4-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.4-17m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.4-16m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.4-15m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.4-14m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.4-14m)
- Change Source URL

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.4-13m)
- rebuild against ruby-1.8.6-4m

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.4-12m)
- BuildRequires: freetype2-devel -> freetype-devel

* Thu Aug  3 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (0.7.4-11m)
- added --with-ttf to configure argument because kagemai statistics requires TTF support

* Tue Jun 28 2005 Toru Hoshina <t@momonga-linux.org>
- (0.7.4-10m)
- avoid to use sitedir= at make phase.
- ruby_libdir should be /usr/lib/ruby/$ver/$arch-linux, is it right?

* Mon Feb 14 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.4-9m)
- disable --with-ttf

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.4-8m)
- rebuild against ruby-1.8.2

* Tue May 18 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.7.4-7m)
- add --with-ttf

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.7.4-6m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.7.4-5m)
- merge from ruby-1_8-branch.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (0.7.4-4m)
- rebuild against ruby-1.8.0.

* Tue Feb 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.7.4-3m)
  Source URI changed

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.4-2m)
- rebuild against for gd

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.4-1m)
- version 0.7.4
