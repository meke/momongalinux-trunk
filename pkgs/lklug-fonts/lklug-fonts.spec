%global momorel 5

%define fontname lklug
%define cvsdate 20090803
%global fontconf 65-%{fontname}.conf

Name:           %{fontname}-fonts
# Do not trust font metadata versionning unless you've checked upstream does
# update versions on file changes. When in doubt use the timestamp of the most
# recent file as version.
Version:        0.6
Release:        %{momorel}m%{?dist}
Summary:        Fonts for Sinhala language

Group:          User Interface/X
License:        GPLv2
URL:            http://sinhala.sourceforge.net/

# cvs snapshot created with following steps
#cvs -z3 -d:pserver:anonymous@sinhala.cvs.sourceforge.net:/cvsroot/sinhala co -P sinhala/fonts
#cd sinhala/fonts/
#tar -czf lklug-%{cvsdate}.tar.gz convert.ff COPYING  CREDITS lklug.sfd Makefile README.fonts
Source0:        lklug-%{cvsdate}.tar.gz
Source1:        %{fontconf}
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  fontpackages-devel fontforge
Requires:       fontpackages-filesystem
Obsoletes:      fonts-sinhala

%description
The lklug-fonts package contains fonts for the display of
Sinhala. The original font for TeX/LaTeX is developed by Yannis 
Haralambous and are in GPL. OTF tables are added by Anuradha 
Ratnaweera and Harshani Devadithya.

%prep
%setup -q -c

%build
make

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
		%{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
		%{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} \
		%{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -fr %{buildroot}

%_font_pkg -f %{fontconf}  *.ttf
%doc CREDITS COPYING README.fonts 
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-3m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-2m)
- Obsoletes: fonts-sinhala

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-5m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.2-4m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.2-1m)
- import from Fedora

* Wed Oct 17 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.2.2-5
- Added sfd file into srpm

* Thu Oct 11 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.2.2-4
- Updated according to the review

* Thu Oct 04 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.2.2-3
- Using common template of font spec file 

* Thu Oct 04 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.2.2-2
- Spec cleanup

* Thu Oct 04 2007 Rahul Bhalerao <rbhalera@redhat.com> - 0.2.2-1
- Split package from fonts-sinhala to reflect upstream project name
