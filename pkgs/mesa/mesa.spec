%global momorel 1
%global _default_patch_fuzz 2
%global date 20120924

# S390 doesn't have video cards, so it's not much use building DRI there.
%ifarch s390 s390x
%define with_dri 0
%define driver xlib
%else
%define with_dri 1
%define driver dri
%endif

%global with_mesa_source 0
%global mesa_demos_ver 8.1.0

#-- END DRI Build Configuration ------------------------------------------

Summary: Mesa graphics libraries
Name: mesa
Version: 10.0.3
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.mesa3d.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: ftp://ftp.freedesktop.org/pub/mesa/%{version}/MesaLib-%{version}.tar.bz2 
NoSource: 0
#Source0: mesa-%{date}.tar.xz
#Source0: MesaLib-%{version}-%{date}.tar.xz
#Source1: ftp://ftp.freedesktop.org/pub/mesa/%{version}/MesaDemos-%{version}.tar.bz2 
Source1: ftp://ftp.freedesktop.org/pub/mesa/demos/%{mesa_demos_ver}/mesa-demos-%{mesa_demos_ver}.tar.bz2
NoSource: 1

Patch1: mesa-8.0-llvmpipe-shmget.patch

BuildRequires: pkgconfig
BuildRequires: libdrm-devel >= 2.4.52
BuildRequires: libXxf86vm-devel
BuildRequires: expat-devel
BuildRequires: xorg-x11-proto-devel >= 7.5
BuildRequires: libXdamage-devel
BuildRequires: freeglut-devel
BuildRequires: imake
BuildRequires: llvm-devel >= 3.4
BuildRequires: libffi-devel >= 3.0.13

BuildRequires: openmotif-devel
BuildRequires: libselinux-devel

# mesa-demo requires this
BuildRequires: glew-devel >= 1.10.0

# wayland support
BuildRequires: libwayland-client-devel
BuildRequires: libwayland-server-devel

%description
Mesa

%package libGL
Summary: Mesa libGL runtime libraries and DRI drivers
Group: System Environment/Libraries

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires: mesa-dri-drivers = %{version}-%{release}

Provides: libGL
Obsoletes: Mesa, XFree86-libs, XFree86-Mesa-libGL, xorg-x11-Mesa-libGL 
%if ! %{with_mesa_source}
Obsoletes: mesa-source
%endif

%description libGL
Mesa libGL runtime library.
  	 
%if %{with_dri}
%package dri-drivers
Summary: Mesa-based DRI drivers.
Group: User Interface/X Hardware Support
%description dri-drivers
Mesa-based DRI drivers.
%endif

%package libGL-devel
Summary: Mesa libGL development package
Group: Development/Libraries
Requires: mesa-libGL = %{version}-%{release}
Requires: libX11-devel

Provides: libGL-devel
Obsoletes: Mesa-devel, XFree86-devel, xorg-x11-devel

%description libGL-devel
Mesa libGL development package


#-- libEGL -----------------------------------------------------------
%package libEGL
Summary: Mesa libEGL runtime libraries
Group: System Environment/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires: mesa-dri-drivers = %{version}-%{release}

%description libEGL
Mesa libEGL runtime libraries

%package libEGL-devel
Summary: Mesa libEGL development package
Group: Development/Libraries
Requires: mesa-libEGL = %{version}-%{release}

%description libEGL-devel
Mesa libEGL development package

#-- libGLES ----------------------------------------------------------
%package libGLES
Summary: Mesa libGLES runtime libraries
Group: System Environment/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires: mesa-dri-drivers = %{version}-%{release}

%description libGLES
Mesa GLES runtime libraries

%package libGLES-devel
Summary: Mesa libGLES development package
Group: Development/Libraries
Requires: mesa-libGLES = %{version}-%{release}

%description libGLES-devel
Mesa libGLES development package

#-- libOSMesa -----------------------------------------------------------
%package libOSMesa
Summary: Mesa offscreen rendering libraries
Group: System Environment/Libraries

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

Provides: libOSMesa

%description libOSMesa
Mesa offscreen rendering libraries

#-- libOSMesa-devel -----------------------------------------------------
%package libOSMesa-devel
Summary: Mesa offscreen rendering development package
Group: Development/Libraries
Requires: mesa-libOSMesa = %{version}-%{release}

%description libOSMesa-devel
Mesa offscreen rendering development package

#-- libgbm -----------------------------------------------------
%package libgbm
Summary: Mesa gbm library
Group: System Environment/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Provides: libgbm

%description libgbm
Mesa gbm runtime library.

#-- libgbm-devel -----------------------------------------------------
%package libgbm-devel
Summary: Mesa libgbm development package
Group: Development/Libraries
Requires: mesa-libgbm%{?_isa} = %{version}-%{release}
Provides: libgbm-devel

%description libgbm-devel
Mesa libgbm development package

#-- source -----------------------------------------------------------
%if %{with_mesa_source}
%package source
Summary: Mesa source code required to build X server
Group: Development/Libraries

%description source
The mesa-source package provides the minimal source code needed to
build DRI enabled X servers, etc.
%endif

#-- glx-utils --------------------------------------------------------
%package -n glx-utils
Summary: GLX utilities
Group: Development/Libraries

%description -n glx-utils
The glx-utils package provides the glxinfo and glxgears utilities.

%package demos
Summary: Mesa demos
Group: Development/Libraries

%description demos
This package provides some demo applications for testing Mesa.

#-- libwayland-egl ------------------------------------------------------
%package libwayland-egl
Summary: Mesa libwayland-egl library
Group: System Environment/Libraries
Provides: libwayland-egl

%description libwayland-egl
Mesa libwayland-egl runtime library.

#-- libwayland-egl-devel ------------------------------------------------
%package libwayland-egl-devel
Summary: Mesa libwayland-egl development package
Group: Development/Libraries
Requires: mesa-libwayland-egl%{?_isa} = %{version}-%{release}
Provides: libwayland-egl-devel

%description libwayland-egl-devel
Mesa libwayland-egl development package

#-- xatracker -----------------------------------------------------------
%package -n xatracker
Summary: Mesa xatracker runtime library
Group: System Environment/Libraries

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description -n xatracker
Mesa xatracker runtime library

#-- xatracker-devel -----------------------------------------------------
%package -n xatracker-devel
Summary: Mesa xatracker development package
Group: Development/Libraries
Requires: xatracker = %{version}-%{release}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description -n xatracker-devel
Mesa xatracker development package


#-- prep -------------------------------------------------------------
%prep
%setup -q -n Mesa-%{version} -b1
#%%setup -q -n mesa-%{date} -b1

%patch1 -p1 -b .llvmcore

# WARNING: The following files are copyright "Mark J. Kilgard" under the GLUT
# license and are not open source/free software, so we remove them.
rm -f include/GL/uglglutshapes.h

# NoBuild glut
rm -rf src/glut/

# Hack the demos to use installed data files
pushd ../mesa-demos-*
sed -i 's,../images,%{_libdir}/mesa-demos-data,' src/demos/*.c
sed -i 's,geartrain.dat,%{_libdir}/mesa-demos-data/&,' src/demos/geartrain.c
sed -i 's,isosurf.dat,%{_libdir}/mesa-demos-data/&,' src/demos/isosurf.c
sed -i 's,terrain.dat,%{_libdir}/mesa-demos-data/&,' src/demos/terrain.c
popd

#-- Build ------------------------------------------------------------
%build
autoreconf --install

export CFLAGS="$RPM_OPT_FLAGS "
export CXXFLAGS="$RPM_OPT_FLAGS "

%ifarch %{ix86}
# i do not have words for how much the assembly dispatch code infuriates me
%define asm_flags --disable-asm
%endif

# now build the rest of mesa
%configure \
    %{?asm_flags} \
    --enable-selinux \
    --enable-osmesa \
    --enable-xcb \
    --with-dri-driverdir=%{_libdir}/dri \
    --enable-egl \
    --enable-gles1 \
    --enable-gles2 \
    --disable-vdpau \
    --disable-xvmc \
    --disable-gallium-egl \
    --with-egl-platforms=x11,drm,wayland \
    --enable-shared-glapi \
    --enable-gbm \
    --enable-glx-tls \
    --enable-texture-float=yes \
    --with-gallium-drivers=svga,radeonsi,swrast,r600,freedreno,r300,nouveau \
    --enable-r600-llvm-compiler \
    --enable-gallium-llvm \
    --with-llvm-shared-libs \
    --enable-xa \
    %{?dri_drivers}
make %{?_smp_mflags} MKDEP=/bin/true

#make -C progs/xdemos glxgears glxinfo
#make %{?_smp_mflags} -C progs/demos

pushd ../mesa-demos-*
%configure
make -C src/xdemos glxgears glxinfo
make -C src/util
make %{?_smp_mflags} -C src/demos
popd

#-- Install ----------------------------------------------------------
%install
rm -rf --preserve-root %{buildroot}

# core libs and headers, but not drivers.
make install DESTDIR=$RPM_BUILD_ROOT

# install libOSMesa
#cp %{_lib}/libOSMesa.so %{_lib}/libOSMesa.so.8.* $RPM_BUILD_ROOT/%{_libdir}/
#rm -f $RPM_BUILD_ROOT/%{_libdir}/libOSMesa.so.8
#cp src/mesa/drivers/osmesa/.libs/libOSMesa.so* $RPM_BUILD_ROOT/%{_libdir}/
#rm -f $RPM_BUILD_ROOT/%{_libdir}/libOSMesa.so.8
 
# FIXME: mesa-8.1-20120827 can't make install
#install src/mesa/gl.pc $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/
#install src/mesa/drivers/osmesa/osmesa.pc $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/

# just the DRI drivers that are sane
#install -d $RPM_BUILD_ROOT%{_libdir}/dri
# use gallium driver iff built
#[ -f %{_lib}/gallium/r300_dri.so ] && cp %{_lib}/gallium/r300_dri.so %{_lib}/r300_dri.so
#[ -f %{_lib}/gallium/r600_dri.so ] && cp %{_lib}/gallium/r600_dri.so %{_lib}/r600_dri.so
#[ -f %{_lib}/gallium/swrastg_dri.so ] && mv %{_lib}/gallium/swrastg_dri.so %{_lib}/swrast_dri.so
#
#for f in i810 i915 i965 mach64 mga r128 r200 r300 r600 radeon savage sis swrast tdfx unichrome nouveau_vieux gallium/vmwgfx ; do
#    so=%{_lib}/${f}_dri.so
#    test -e $so && echo $so
#done | xargs install -m 0755 -t $RPM_BUILD_ROOT%{_libdir}/dri >& /dev/null || :

# strip out undesirable headers
pushd $RPM_BUILD_ROOT%{_includedir}/GL
rm -f [a-fh-np-wyz]*.h gg*.h glf*.h glew.h glut*.h glxew.h
#rm [a-fh-np-wyz]*.h gg*.h glf*.h 
popd

# XXX demos, since they don't install automatically.  should fix that.
pushd ../mesa-demos-*
install -d $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/xdemos/glxgears $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/xdemos/glxinfo $RPM_BUILD_ROOT%{_bindir}
find src/demos/ -type f -perm /0111 |
    xargs install -m 0755 -t $RPM_BUILD_ROOT/%{_bindir}
# bah, name conflicts
#mv $RPM_BUILD_ROOT/%{_bindir}/{rain,mesa-rain}
install -d $RPM_BUILD_ROOT/%{_libdir}/mesa-demos-data
install -m 0644 src/data/*.rgb $RPM_BUILD_ROOT/%{_libdir}/mesa-demos-data
install -m 0644 src/data/*.dat $RPM_BUILD_ROOT/%{_libdir}/mesa-demos-data
popd

# this keeps breaking, check it early.  note that the exit from eu-ftr is odd.
pushd $RPM_BUILD_ROOT%{_libdir}
for i in libOSMesa*.so libGL.so ; do
    eu-findtextrel $i && exit 1
done
popd

# man pages
#pushd ../%{manpages}
#make %{?_smp_mflags} install DESTDIR=$RPM_BUILD_ROOT
#popd

%if %{with_mesa_source}
# Install the source needed to build the X server.  The egreps are just
# stripping out unnecessary dirs; only tricky bit is the [^c] to make sure
# .../dri/common is included.
%define mesasourcedir %{_datadir}/mesa/source

mkdir -p $RPM_BUILD_ROOT/%{mesasourcedir}
  	 ( find src -name \*.[ch] ; find include -name \*.h ) |
  	     egrep -v '^src/(glu|glw)' |
  	     egrep -v '^src/mesa/drivers/(directfb|dos|fbdev|glide|ggi|osmesa)' |
  	     egrep -v '^src/mesa/drivers/(windows|dri/[^c])' |
  	     xargs tar cf - --mode a=r |
  	         (cd $RPM_BUILD_ROOT/%{mesasourcedir} && tar xf -)
%endif

find $RPM_BUILD_ROOT -name \*.la | xargs rm -f

#-- Clean ------------------------------------------------------------
%clean
rm -rf --preserve-root %{buildroot}

#-- Check ------------------------------------------------------------
#%%check

%post libGL -p /sbin/ldconfig
%postun libGL -p /sbin/ldconfig
%post libOSMesa -p /sbin/ldconfig
%postun libOSMesa -p /sbin/ldconfig
%post libEGL -p /sbin/ldconfig
%postun libEGL -p /sbin/ldconfig
%post libGLES -p /sbin/ldconfig
%postun libGLES -p /sbin/ldconfig
%post libgbm -p /sbin/ldconfig
%postun libgbm -p /sbin/ldconfig
%post libwayland-egl -p /sbin/ldconfig
%postun libwayland-egl -p /sbin/ldconfig
%post -n xatracker -p /sbin/ldconfig
%postun -n xatracker -p /sbin/ldconfig

%files libGL
%defattr(-,root,root,-)
%{_libdir}/libGL.so.1
%{_libdir}/libGL.so.1.2.0

%files libEGL
%defattr(-,root,root,-)
%doc docs/COPYING
%{_libdir}/libEGL.so.1
%{_libdir}/libEGL.so.1.*

%files libGLES
%defattr(-,root,root,-)
%doc docs/COPYING
%{_libdir}/libGLESv1_CM.so.1
%{_libdir}/libGLESv1_CM.so.1.*
%{_libdir}/libGLESv2.so.2
%{_libdir}/libGLESv2.so.2.*
%{_libdir}/libglapi.so.0
%{_libdir}/libglapi.so.0.*

%files dri-drivers
%if %{with_dri}
%config(noreplace) %{_sysconfdir}/drirc
# DRI modules
%dir %{_libdir}/dri
# We only install drivers that get build and are in our white list so
# we can just glob here.
%{_libdir}/dri/*_dri.so
#%{_libdir}/dri/EGL_*.so
#%{_libdir}/libdricore*.so*
%endif

%files libGL-devel
%defattr(-,root,root,-)
%{_includedir}/GL/gl.h
%{_includedir}/GL/gl_mangle.h
%{_includedir}/GL/glext.h
%{_includedir}/GL/glx.h
%{_includedir}/GL/glx_mangle.h
%{_includedir}/GL/glxext.h
%{_includedir}/GL/internal/dri_interface.h
%{_libdir}/libGL.so
%{_libdir}/pkgconfig/dri.pc
%{_libdir}/pkgconfig/gl.pc

%files libEGL-devel
%defattr(-,root,root,-)
%dir %{_includedir}/EGL
%{_includedir}/EGL/eglext.h
%{_includedir}/EGL/egl.h
%{_includedir}/EGL/eglmesaext.h
%{_includedir}/EGL/eglplatform.h
%dir %{_includedir}/KHR
%{_includedir}/KHR/khrplatform.h
%{_libdir}/pkgconfig/egl.pc
%{_libdir}/libEGL.so

%files libGLES-devel
%defattr(-,root,root,-)
%dir %{_includedir}/GLES
%{_includedir}/GLES/egl.h
%{_includedir}/GLES/gl.h
%{_includedir}/GLES/glext.h
%{_includedir}/GLES/glplatform.h
%dir %{_includedir}/GLES2
%{_includedir}/GLES2/gl2platform.h
%{_includedir}/GLES2/gl2.h
%{_includedir}/GLES2/gl2ext.h
%dir %{_includedir}/GLES3
%{_includedir}/GLES3/gl3platform.h
%{_includedir}/GLES3/gl3.h
%{_includedir}/GLES3/gl3ext.h
%{_libdir}/pkgconfig/glesv1_cm.pc
%{_libdir}/pkgconfig/glesv2.pc
%{_libdir}/libGLESv1_CM.so
%{_libdir}/libGLESv2.so
%{_libdir}/libglapi.so

%files libOSMesa
%defattr(-,root,root,-)
%{_libdir}/libOSMesa.so.8*

%files libOSMesa-devel
%defattr(-,root,root,-)
%{_includedir}/GL/osmesa.h
%{_libdir}/libOSMesa.so
%{_libdir}/pkgconfig/osmesa.pc

%files libgbm
%defattr(-,root,root,-)
%doc docs/COPYING
%{_libdir}/libgbm.so.1
%{_libdir}/libgbm.so.1.*

%files libgbm-devel
%defattr(-,root,root,-)
%{_libdir}/libgbm.so
%{_includedir}/gbm.h
%{_libdir}/pkgconfig/gbm.pc

%if %{with_mesa_source}
%files source
%{mesasourcedir}
%defattr(-,root,root,-)
%endif

%files -n glx-utils
%defattr(-,root,root,-)
%{_bindir}/glxgears
%{_bindir}/glxinfo

%files demos
%defattr(-,root,root,-)
%{_bindir}/arbfplight
%{_bindir}/arbfslight
%{_bindir}/arbocclude
%{_bindir}/arbocclude2
%{_bindir}/bounce
%{_bindir}/fbo_firecube
%{_bindir}/clearspd
%{_bindir}/copypix
%{_bindir}/cubemap
%{_bindir}/cuberender
%{_bindir}/dinoshade
%{_bindir}/dissolve
%{_bindir}/drawpix
%{_bindir}/engine
%{_bindir}/fbotexture
%{_bindir}/fire
%{_bindir}/fogcoord
%{_bindir}/fplight
%{_bindir}/fslight
%{_bindir}/gamma
%{_bindir}/gearbox
%{_bindir}/gears
%{_bindir}/geartrain
%{_bindir}/glinfo
%{_bindir}/gloss
%{_bindir}/gltestperf
%{_bindir}/ipers
%{_bindir}/isosurf
%{_bindir}/lodbias
%{_bindir}/morph3d
%{_bindir}/multiarb
%{_bindir}/paltex
%{_bindir}/pixeltest
%{_bindir}/pointblast
%{_bindir}/projtex
#%{_bindir}/mesa-rain
%{_bindir}/ray
%{_bindir}/readpix
%{_bindir}/reflect
%{_bindir}/renormal
%{_bindir}/shadowtex
%{_bindir}/singlebuffer
%{_bindir}/spectex
%{_bindir}/spriteblast
%{_bindir}/stex3d
%{_bindir}/teapot
%{_bindir}/terrain
%{_bindir}/tessdemo
%{_bindir}/texcyl
%{_bindir}/texenv
%{_bindir}/textures
%{_bindir}/trispd
%{_bindir}/tunnel
%{_bindir}/tunnel2
%{_bindir}/vao_demo
%{_bindir}/winpos
%{_libdir}/mesa-demos-data

%files libwayland-egl
%defattr(-,root,root,-)
%doc docs/COPYING
%{_libdir}/libwayland-egl.so.1
%{_libdir}/libwayland-egl.so.1.*

%files libwayland-egl-devel
%defattr(-,root,root,-)
%{_libdir}/libwayland-egl.so
%{_libdir}/pkgconfig/wayland-egl.pc

%files -n xatracker
%defattr(-,root,root,-)
%{_libdir}/libxatracker.so.*

%files -n xatracker-devel
%defattr(-,root,root,-)
%{_libdir}/libxatracker.so
%{_includedir}/xa_*.h
%{_libdir}/pkgconfig/xatracker.pc

%changelog
* Fri Feb 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (10.0.3-1m)
- update 10.0.3

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (10.0.2-1m)
- update 10.0.2

* Sun Dec 01 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (10.0.0-1m)
- update 10.0.0

* Thu Nov 28 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.2.4-1m)
- update 9.2.4

* Thu Nov 14 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.2.3-1m)
- update 9.2.3

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.2.2-2m)
- rebuild against glew-1.10.0

* Sun Oct 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.2.2-1m)
- update 9.2.2

* Sun Oct 06 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.2.1-1m)
- update 9.2.1

* Mon Sep 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.2.0-2m)
- rebuild against llvm-3.3

* Thu Aug 29 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.2.0-1m)
- update 9.2.0

* Fri Aug  2 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.1.6-1m)
- update 9.1.6

* Mon Jul 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.5-1m)
- update 9.1.5

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.2-2m)
- support wayland

* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.1.2-1m)
- update 9.1.2

* Wed Jan 23 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.2-1m)
- update 9.0.2

* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0.1-1m)
- update 9.0.1

* Mon Oct 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0-1m)
- update 9.0

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.0-0.20120924.3m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.0-0.20120924.2m)
- rebuild for glew-1.9.0

* Mon Sep 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0-0.20120924.1m)
- update 9.0-20120824
-- Mesa-8.1 rename to Mesa-9.0 

* Sat Aug 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.1.0-0.20120811.1m)
- update 8.1-20120811

* Fri Aug  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.1.0-0.20120707.2m)
- rebuild against systemd-187

* Sun Jul  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.1.0-0.20120707.1m)
- update 8.1 branch

* Sun Jun 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.3-2m)
- fix build failure
-- merge from T4R/mesa; add patch for llvm 3.1+

* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.3-1m)
- update 8.0.3 release

* Sun Apr 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.2-2m)
- import fedora's patches
- any configure option enabled

* Fri Mar 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.2-1m)
- update 8.0.2 release

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.1-2m)
- rebuild against glew-1.7.0

* Fri Feb 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0.1-1m)
- update 8.0.1 release

* Fri Feb 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0-1m)
- update 8.0 release

* Mon Jan 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0-0.20120128.2m)
- add sub packages; xatracker and xatracker-devel
- add svga drivier

* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0-0.20120128.1m)
- update 8.0-20120128 (rc2)

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0-0.20120116.2m)
- link llvm shared library

* Mon Jan 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0-0.20120116.1m)
- update 8.0-20120116 (rc1) 

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.0-0.20120110.1m)
- update 8.0-20120110
-- mesa-7.12 rename to mesa-8.0

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.12-0.20120107.1m)
- update 7.12-20120107

* Sat Dec 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.12-0.20111231.1m)
- update 7.12-20111231

* Sun Dec 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.12-0.20111224.1m)
- update 7.12-20111224

* Sun Dec 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.12-0.20111211.1m)
- update 7.12-20111211

* Sun Dec  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.12-0.20111204.1m)
- update 7.12-20111204

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.12-0.2m)
- update 7.12-20111127
- split libEGL, libGLES packages

* Sat Nov 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.12-0.1m)
- update 7.12

* Sat Nov 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.11.1-1m)
- update 7.11.1
- split libGLw 

* Mon Sep 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.11-2m)
- merge software-texture_from_pixmap patch

* Tue Aug  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.11-1m)
- update 7.11

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.10.3-1m)
- update 7.10.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.10.2-4m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.10.2-3m)
- add mesa-i965-performance.patch

* Fri Apr  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.10.2-2m)
- add BuildRequires

* Thu Apr  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.10.2-1m)
- update 7.10.2

* Sun Mar 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.10.1-1m)
- update 7.10.1

* Mon Dec 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.8.2-5m)
- add Mesa-7.8.2-libdrm-2.4.23.patch (import from git)

* Thu Dec  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.8.2-5m)
- add mesa-7.8.3-fdo30220.patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.8.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.8.2-3m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.8.2-2m)
- import fedora patches

* Sun Jun 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.8.2-1m)
- update 7.8.2

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.8.1-3m)
- rebuild against libdrm-2.4.21-2m

* Thu Apr  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.8.1-2m)
- drop mesa-7.1-link-shared.patch to avoid AIGLX errors

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.8.1-1m)
- update 7.8.1

* Wed Mar 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.8-1m)
- update 7.8

* Wed Jan  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.7-1m)
- update 7.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.5.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5.2-1m)
- update 7.5.2

* Tue Sep 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5.1-2m)
- remove glew headers

* Fri Sep  4 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5.1-1m)
- update 7.5.1

* Sat Aug 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-2m)
- add mesa-7.5-upstream.patch

* Mon Jul 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-1m)
- update 7.5-release

* Sun Jul  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5-0.994.2m)
- add BuildRequires

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.5-0.994.1m)
- update 7.5-RC4

* Fri May 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4.2-1m)
- update 7.4.2

* Fri May 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4.1-1m)
- update 7.4.1

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4-2m)
- fix BuildRequires

* Tue Mar 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4-1m)
- update 7.4

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.3-1m)
- update 7.3

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.2-1m)
- update 7.2(stable-release)

* Sun Sep 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.1.0-3m)
- update upstream patch

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.1.0-2m)
- update upstream patch
-- support Intel G41 chipset

* Fri Sep 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.1.0-2m)
- add upstream patch

* Fri Aug 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.1.0-1m)
- update 7.1.0

* Thu Aug 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.1.0-0.994.1m)
- update mesa-7.1-rc4

* Thu Jul 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.0-0.993.1m)
- update mesa-7.1-rc3

* Tue Jul  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.1.0-0.991.4m)
- fix Obsoletes: mesa-source

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.0-0.991.3m)
- update mesa-7.1pre-osmesa-version.patch
- remove mesa-source package

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.0-0.991.2m)
- update 7.1-20080628
-- support Intel 4 Chipset
- split mesa-dri-driver package

* Thu Jun  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.0-0.991.1m)
- update 7.1rc1

* Tue May  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.1.0-0.20080503.2m)
- release %%{_includedir}/GL/internal
- it's provided by xorg-x11-proto-devel
- and mesa-*-devel(s) depend on xorg-x11-proto-devel

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.0-0.20080503.1m)
- update 7.1-20080503

* Sun Apr  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.3-1m)
- update 7.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0.3-0.992.2m)
- rebuild against gcc43

* Sat Feb 24 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.3-0.992.1m)
- update 7.0.3-RC2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.2-6m)
- %%NoSource -> NoSource

* Tue Jan 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.2-5m)
- update mesa-7.0.2-stable-branch.patch 

* Sun Jan 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.2-4m)
- add mesa-7.0.2-stable-branch.patch 

* Thu Nov 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.2-3m)
- no build i915tex. 

* Thu Nov 29 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (7.0.2-2m)
- revised spec for lib64

* Tue Nov 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.2-1m)
- update 7.0.2

* Thu Nov  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.2-0.992.1m)
- update 7.0.2-RC2

* Wed Aug 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.1-2m)
- update mesa-7.0-build-config.patch
-- add -fvisibility=hidden option

* Mon Aug  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.1-1m)
- update 7.0.1

* Sat Jul 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-4m)
- corrected that link was wrong
-- http://pc11.2ch.net/test/read.cgi/linux/1092539027/952

* Tue Jul 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-3m)
- optimization option was invalidated in power architecture.

* Tue Jul  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-2m)
- support any Intel Chips. import from Alt Linux
-- Patch301: Mesa-6.5.3-git-i915-945GME.patch
-- Patch302: Mesa-6.5.3-git-i915tex-945GME.patch
-- Patch303: Mesa-6.5.3-git-i965-965GME.patch
-- Patch304: Mesa-6.5.3-git-i915-G33_Q33_Q35.patch

* Sat Jun 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-1m)
- update 7.0-release

* Mon Jun 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-0.1m)
- update 7.0-20070618

* Sun Apr 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.3-0.2m)
- update 6.5.3-Release

* Sun Apr 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.3-0.2m)
- update 6.5.3RC2(20070422)

* Fri Apr 20 2007 zunda <zunda at freeshell.org>
- (kossori)
- Added BuildRequires: libXdamage-devel
- Modified build sequence to preserve src/mesa/depend which is
  needed by src/mesa/Makefile but erased with make -s realclean

* Thu Apr 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.3-0.1m)
- update 6.5.3pre(20070412)

* Tue Mar 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.2-8m)
- update 20070313
- fix undefined reference to `glXGetCurrentContext' 
-- (mesa-6.5.2-libgl-visibility.patch)

* Sat Mar  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.2-7m)
- update 20070303
- use parallel build
- cleanup spec

* Wed Feb  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.2-6m)
- update 20070206

* Thu Jan 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.2-5m)
- update 20070110

* Sun Dec 31 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.2-4m)
- update 20061231
- support nouveau(OSS nVidia Driver)

* Fri Dec 29 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (6.5.2-3m)
- modified mesa-6.5.1-build-config.patch for lib64

* Fri Dec 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.2-2m)
- revival mesa-libGLw*

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.2-1m)
- update 6.5.2

* Mon Sep 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.1-1m)
- update 6.5.1

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.5.1-0.2m)
- rebuild against expat-2.0.0-1m

* Mon Aug 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5.1-0.1m)
- update 6.5.1-20060820(CVS)
- Add i965 driver(http://intellinuxgraphics.org/) 

* Thu Jun 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5-3m)
- add patch
-- Patch9: mesa-6.5-tfp-fbconfig-attribs.patch

* Tue Jun 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5-2m)
- add patch
-- Patch6: mesa-6.5-noexecstack.patch
-- Patch7: mesa-6.5-force-r300.patch
-- Patch8: mesa-6.5-fix-pbuffer-dispatch.patch

* Thu Apr  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5-1m)
- sync FC-devel
- * Sat Apr  1 2006 Kristia <krh redhat com> 6.5-1
- - Update to mesa 6.5 snapshot.
- - Use -MG for generating deps and some files aren't yet symlinked at
-   make depend time.
- - Drop mesa-6.4.2-dprintf-to-debugprintf-for-bug180122.patch and
-   mesa-6.4.2-xorg-server-uses-bad-datatypes-breaking-AMD64-fdo5835.patch
-   as these are upstream now.
- - Drop mesa-6.4.1-texture-from-drawable.patch and add
-   mesa-6.5-texture-from-pixmap-fixes.patch.
- - Update mesa-modular-dri-dir.patch to apply.
- - Widen libGLU glob.
- - Reenable r300 driver install.
- - Widen libOSMesa glob.
- - Go back to patching config/linux-dri, add mesa-6.5-build-config.patch,
-   drop mesa-6.3.2-build-configuration-v4.patch.
- - Disable sis dri driver for now, only builds on x86 and x86-64.

* Sun Mar 26 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6.4.2-6m)
- enable ppc64

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.4.2-5m)
- To trunk

*  Sun Mar 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.4.2-4.2m)
- commentout Patch200

* Wed Mar  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.4.2-4.1m)
- commentout obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.4.2-4m)
- import to Momonga

* Wed Feb 22 2006 Adam Jackson <ajackson@redhat.com> 6.4.2-4
- rebuilt

* Sun Feb 19 2006 Ray Strode <rstrode@redhat.com> 6.4.2-3
- enable texture-from-drawable patch
- add glut-devel dependency

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 6.4.2-2.1
- bump again for double-long bug on ppc(64)

* Tue Feb  7 2006 Mike A. Harris <mharris@redhat.com> 6.4.2-2
- Added new "glx-utils" subpackage with glxgears and glxinfo (#173510)
- Added mesa-6.4.2-dprintf-to-debugprintf-for-bug180122.patch to workaround
  a Mesa namespace conflict with GNU_SOURCE (#180122)
- Added mesa-6.4.2-xorg-server-uses-bad-datatypes-breaking-AMD64-fdo5835.patch
  as an attempt to fix bugs (#176976,176414,fdo#5835)
- Enabled inclusion of the *EXPERIMENTAL UNSUPPORTED* r300 DRI driver on
  x86, x86_64, and ppc architectures, however the 2D Radeon driver will soon
  be modified to require the user to manually turn experimental DRI support
  on with Option "dri" in xorg.conf to test it out and report all X bugs that
  occur while using it directly to X.Org bugzilla.  (#179712)
- Use "libOSMesa.so.6.4.0604*" glob in file manifest, to avoid having to
  update it each upstream release.

* Sat Feb  4 2006 Mike A. Harris <mharris@redhat.com> 6.4.2-1
- Updated to Mesa 6.4.2
- Use "libGLU.so.1.3.0604*" glob in file manifest, to avoid having to update it
  each upstream release.

* Tue Jan 24 2006 Mike A. Harris <mharris@redhat.com> 6.4.1-5
- Added missing "BuildRequires: expat-devel" for bug (#178525)
- Temporarily disabled mesa-6.4.1-texture-from-drawable.patch, as it fails
  to compile on at least ia64, and possibly other architectures.

* Tue Jan 17 2006 Kristian Hogsberg <krh@redhat.com> 6.4.1-4
- Add mesa-6.4.1-texture-from-drawable.patch to implement protocol
  support for GLX_EXT_texture_from_drawable extension.

* Sat Dec 24 2005 Mike A. Harris <mharris@redhat.com> 6.4.1-3
- Manually copy libGLw headers that Mesa forgets to install, to fix (#173879).
- Added mesa-6.4.1-libGLw-enable-motif-support.patch to fix (#175251).
- Removed "Conflicts" lines from libGL package, as they are "Obsoletes" now.
- Do not rename swrast libGL .so version, as it is the OpenGL version.

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 6.4.1-2
- Rebuild to ensure libGLU gets rebuilt with new gcc with C++ compiler fixes.
- Changed the 3 devel packages to use Obsoletes instead of Conflicts for the
  packages the files used to be present in, as this is more friendy for
  OS upgrades.
- Added "Requires: libX11-devel" to mesa-libGL-devel package (#173712)
- Added "Requires: libGL-devel" to mesa-libGLU-devel package (#175253)

* Sat Dec 17 2005 Mike A. Harris <mharris@redhat.com> 6.4.1-1
- Updated MesaLib tarball to version 6.4.1 from Mesa project for X11R7 RC4.
- Added pkgconfig dependency.
- Updated "BuildRequires: libdrm-devel >= 2.0-1"
- Added Obsoletes lines to all the subpackages to have cleaner upgrades.
- Added mesa-6.4.1-amd64-assyntax-fix.patch to work around a build problem on
  AMD64, which is fixed in the 6.4 branch of Mesa CVS.
- Conditionalize libOSMesa inclusion, and default to not including it for now.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com> 6.4-5.1
- rebuilt

* Sun Nov 20 2005 Jeremy Katz <katzj@redhat.com> 6.4-5
- fix directory used for loading dri modules (#173679)
- install dri drivers as executable so they get stripped (#173292)

* Thu Nov 3 2005 Mike A. Harris <mharris@redhat.com> 6.4-4
- Wrote redhat-mesa-source-filelist-generator to dynamically generate the
  files to be included in the mesa-source subpackage, to minimize future
  maintenance.
- Fixed detection and renaming of software mesa .so version.

* Wed Nov 2 2005 Mike A. Harris <mharris@redhat.com> 6.4-3
- Hack: autodetect if libGL was given .so.1.5* and rename it to 1.2 for
  consistency on all architectures, and to avoid upgrade problems if we
  ever disable DRI on an arch and then re-enable it later.

* Wed Nov 2 2005 Mike A. Harris <mharris@redhat.com> 6.4-2
- Added mesa-6.4-multilib-fix.patch to instrument and attempt to fix Mesa
  bin/installmesa script to work properly with multilib lib64 architectures.
- Set and export LIB_DIR and INCLUDE_DIR in spec file 'install' section,
  and invoke our modified bin/installmesa directly instead of using
  "make install".
- Remove "include/GL/uglglutshapes.h", as it uses the GLUT license, and seems
  like an extraneous file anyway.
- Conditionalize the file manifest to include libGL.so.1.2 on DRI enabled
  builds, but use libGL.so.1.5.060400 instead on DRI disabled builds, as
  this is how upstream builds the library, although it is not clear to me
  why this difference exists yet (which was not in Xorg 6.8.2 Mesa).

* Thu Oct 27 2005 Mike A. Harris <mharris@redhat.com> 6.4-1
- Updated to new upstream MesaLib-6.4
- Updated libGLU.so.1.3.060400 entry in file manifest
- Updated "BuildRequires: libdrm-devel >= 1.0.5" to pick up fixes for the
  unichrome driver.

* Tue Sep 13 2005 Mike A. Harris <mharris@redhat.com> 6.3.2-6
- Fix redhat-mesa-driver-install and spec file to work right on multilib
  systems.
  
* Mon Sep 5 2005 Mike A. Harris <mharris@redhat.com> 6.3.2-5
- Fix mesa-libGL-devel to depend on mesa-libGL instead of mesa-libGLU.
- Added virtual "Provides: libGL..." entries for each subpackage as relevant.

* Mon Sep 5 2005 Mike A. Harris <mharris@redhat.com> 6.3.2-4
- Added the mesa-source subpackage, which contains part of the Mesa source
  code needed by other packages such as the X server to build stuff.

* Mon Sep 5 2005 Mike A. Harris <mharris@redhat.com> 6.3.2-3
- Added Conflicts/Obsoletes lines to all of the subpackages to make upgrades
  from previous OS releases, and piecemeal upgrades work as nicely as
  possible.

* Mon Sep 5 2005 Mike A. Harris <mharris@redhat.com> 6.3.2-2
- Wrote redhat-mesa-target script to simplify mesa build target selection.
- Wrote redhat-mesa-driver-install to install the DRI drivers and simplify
  per-arch conditionalization, etc.

* Sun Sep 4 2005 Mike A. Harris <mharris@redhat.com> 6.3.2-1
- Initial build.
