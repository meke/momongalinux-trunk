%global momorel 4

Name:           nss_compat_ossl
Version:        0.9.6
Release:        %{momorel}m%{?dist}
Summary:        Source-level compatibility library for OpenSSL to NSS porting

Group:          System Environment/Libraries
License:        MIT
URL:            http://rcritten.fedorapeople.org/nss_compat_ossl.html
Source0:        http://rcritten.fedorapeople.org/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Need > 3.11.7-7 so we have the NSS PKCS#11 flat-file reader available 
BuildRequires:  nss-devel > 3.11.7-7
BuildRequires:  nspr-devel

%description
This library provides a source-level compatibility layer to aid porting
programs that use OpenSSL to use the NSS instead.

%package devel
Summary:          Development libraries for nss_compat_ossl
Group:            Development/Libraries
Requires:         %{name} = %{version}-%{release}
Requires:         nss-devel

%description devel
Header and library files for doing porting work from OpenSSL to NSS.

%prep
%setup -q

%build

CFLAGS="$RPM_OPT_FLAGS -DPKCS11_PEM_MODULE"
export CFLAGS

%configure --prefix=/usr --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# We don't want to ship the .la file
rm $RPM_BUILD_ROOT/%{_libdir}/libnss_compat_ossl.la


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_libdir}/libnss_compat_ossl.so.*
%doc README COPYING

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/nss_compat_ossl
%{_includedir}/nss_compat_ossl/nss_compat_ossl.h
%{_libdir}/libnss_compat_ossl.so

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (0.9.5-3m)
- add %%dir

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-1m)
- sync with Fedora 11 (0.9.5-3)

* Wed May 27 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.2-3m)
- libtool build fix

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.2-4
- Autorebuild for GCC 4.3

* Sat Oct 13 2007 Rob Crittenden <rcritten@redhat.com> 0.9.2-3
- Bugzilla #330091, don't explicitly link with libsoftokn3.so

* Wed Sep 26 2007 Rob Crittenden <rcritten@redhat.com> 0.9.2-2
- Bugzilla #306711, need to define CERT_NewTempCertificate

* Wed Sep 20 2007 Rob Crittenden <rcritten@redhat.com> 0.9.2-1
- update to 0.9.2
- Enable loading the NSS PKCS#11 pem module
- Add a URL
- Specify the license as LGPLv2+ instead of just LGPL

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.9.1-5
- Rebuild for selinux ppc32 issue.

* Wed Jul 25 2007 Jesse Keating <jkeating@redhat.com> - 0.9.1-4
- Rebuild for RH #249435

* Fri Jul 20 2007 Rob Crittenden <rcritten@redhat.com> 0.9.1-3
- Added missing defattr in the devel package

* Fri Jul 20 2007 Rob Crittenden <rcritten@redhat.com> 0.9.1-2
- rename LICENSE to COPYING
- don't ship the .la in -devel
- fixup devel requirement to be exactly the parent package, not >=

* Tue Jul 17 2007 Rob Crittenden <rcritten@redhat.com> 0.9.1-1
- Initial build.
