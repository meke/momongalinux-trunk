%global momorel 10

%define setools_maj_ver 3.3
%define setools_min_ver 7
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name: setools
Version: %{setools_maj_ver}.%{setools_min_ver}
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://oss.tresys.com/projects/setools
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://oss.tresys.com/projects/setools/chrome/site/dists/setools-%{version}/setools-%{version}.tar.bz2
Source1: setools.pam
Source2: apol.desktop
Source3: seaudit.desktop
Source4: sediffx.desktop
Patch1: 0001-add-setools-seinfo-and-sesearch-python-bindings.patch
Patch2: 0002-setools-should-exit-with-an-error-status-if-it-gets-.patch
Patch3: 0003-Since-we-do-not-ship-neverallow-rules-all-always-fai.patch
Patch4: 0004-Fix-man-pages-and-getoptions.patch
Patch5: 0005-Fix-sepol-calls-to-work-with-latest-libsepol.patch
Patch6: 0006-Changes-to-support-named-file_trans-rules.patch
Patch7: 0007-Remove-unused-variables.patch
Patch8: 0008-Fix-output-to-match-policy-lines.patch
Patch9: 0009-Fix-swig-coding-style-for-structures.patch
Patch10: 0010-selinux_current_policy_path.patch
Patch11: 0011-setools-noship.patch
Patch12: 0012-seaudit.patch
Patch13: 0013-swig.patch

Summary: Policy analysis tools for SELinux
Group: System Environment/Base
Requires: setools-libs = %{version}-%{release} setools-libs-tcl = %{version}-%{release} setools-gui = %{version}-%{release} setools-console = %{version}-%{release}

# external requirements
%define autoconf_ver 2.59
%define bwidget_ver 1.8
%define java_ver 1.2
%define gtk_ver 2.8
%define python_ver 2.7
%define sepol_ver 2.1.8
%define selinux_ver 2.1.12
%define sqlite_ver 3.2.0
%define swig_ver 2.0.7-3
%define tcltk_ver 8.4.9

%description
SETools is a collection of graphical tools, command-line tools, and
libraries designed to facilitate SELinux policy analysis.

This meta-package depends upon the main packages necessary to run
SETools.

%package libs
License: LGPLv2
Summary: Policy analysis support libraries for SELinux
Group: System Environment/Libraries
Requires: libselinux >= %{selinux_ver} libsepol >= %{sepol_ver} sqlite >= %{sqlite_ver}
Obsoletes: setools-libs-java
BuildRequires: flex  bison  pkgconfig
BuildRequires: glibc-devel libstdc++-devel gcc gcc-c++
BuildRequires: libselinux-devel >= %{selinux_ver} libsepol-devel >= %{sepol_ver}
BuildRequires: libsepol-static >= %{sepol_ver}
BuildRequires: sqlite-devel >= %{sqlite_ver} libxml2-devel
BuildRequires: tcl-devel >= %{tcltk_ver}
BuildRequires: autoconf >= %{autoconf_ver} automake
BuildRequires: swig >= 2.0.7

%description libs
SETools is a collection of graphical tools, command-line tools, and
libraries designed to facilitate SELinux policy analysis.

This package includes the following run-time libraries:

  libapol       policy analysis library
  libpoldiff    semantic policy difference library
  libqpol       library that abstracts policy internals
  libseaudit    parse and filter SELinux audit messages in log files
  libsefs       SELinux file contexts library

%package libs-python
License: LGPLv2
Summary: Python bindings for SELinux policy analysis
Group: Development/Languages
Requires: setools-libs = %{version}-%{release} python2 >= %{python_ver} bzip2-libs
BuildRequires: python2-devel >= %{python_ver} swig >= %{swig_ver} bzip2-devel

%description libs-python
SETools is a collection of graphical tools, command-line tools, and
libraries designed to facilitate SELinux policy analysis.

This package includes Python bindings for the following libraries:

  libapol       policy analysis library
  libpoldiff    semantic policy difference library
  libqpol       library that abstracts policy internals
  libseaudit    parse and filter SELinux audit messages in log files
  libsefs       SELinux file contexts library

%package libs-tcl
License: LGPLv2
Summary: Tcl bindings for SELinux policy analysis
Group: Development/Languages
Requires: setools-libs = %{version}-%{release} tcl >= %{tcltk_ver}
BuildRequires: tcl-devel >= %{tcltk_ver} swig >= %{swig_ver}

%description libs-tcl
SETools is a collection of graphical tools, command-line tools, and
libraries designed to facilitate SELinux policy analysis.

This package includes Tcl bindings for the following libraries:

  libapol       policy analysis library
  libpoldiff    semantic policy difference library
  libqpol       library that abstracts policy internals
  libseaudit    parse and filter SELinux audit messages in log files
  libsefs       SELinux file contexts library

%package devel
License: LGPLv2
Summary: Policy analysis development files for SELinux
Group: Development/Libraries
Requires: libselinux-devel >= %{selinux_ver} libsepol-devel >= %{sepol_ver} setools-libs = %{version}-%{release}
BuildRequires: sqlite-devel >= %{sqlite_ver} libxml2-devel

%description devel
SETools is a collection of graphical tools, command-line tools, and
libraries designed to facilitate SELinux policy analysis.

This package includes header files and archives for the following
libraries:

  libapol       policy analysis library
  libpoldiff    semantic policy difference library
  libqpol       library that abstracts policy internals
  libseaudit    parse and filter SELinux audit messages in log files
  libsefs       SELinux file contexts library

%package console
Summary: Policy analysis command-line tools for SELinux
Group: System Environment/Base
License: GPLv2
Requires: setools-libs = %{version}-%{release}
Requires: libselinux >= %{selinux_ver}

%description console
SETools is a collection of graphical tools, command-line tools, and
libraries designed to facilitate SELinux policy analysis.

This package includes the following console tools:

  secmds          command line tools: seinfo, sesearch
  sediff          semantic policy difference tool

%package gui
Summary: Policy analysis graphical tools for SELinux
Group: System Environment/Base
Requires: tcl >= %{tcltk_ver} tk >= %{tcltk_ver} bwidget >= %{bwidget_ver}
Requires: setools-libs = %{version}-%{release} setools-libs-tcl = %{version}-%{release}
Requires: glib2 gtk2 >= %{gtk_ver} usermode
BuildRequires: gtk2-devel >= %{gtk_ver} libglade2-devel libxml2-devel tk-devel >= %{tcltk_ver}
BuildRequires: desktop-file-utils

%description gui
SETools is a collection of graphical tools, command-line tools, and
libraries designed to facilitate SELinux policy analysis.

This package includes the following graphical tools:

  apol          policy analysis tool
  seaudit       audit log analysis tool

%define setoolsdir %{_datadir}/setools-%{setools_maj_ver}
%define pkg_py_lib %{python_sitelib}/setools
%define pkg_py_arch %{python_sitearch}/setools
%define tcllibdir %{_libdir}/setools

%prep
%setup -q
%patch2 -p 1 -b .exitstatus
%patch3 -p 1 -b .neverallow
%patch4 -p 1 -b .manpage
%patch5 -p 1 -b .libsepol
%patch6 -p 1 -b .filenametrans
%patch7 -p 1 -b .unused 
%patch8 -p 1 -b .fixoutput
%patch9 -p 1 -b .fixswig
%patch10 -p 1 -b .current
%patch11 -p 1 -b .noship
%patch12 -p 1 -b .seaudit
%patch13 -p 1 -b .swig
%ifarch sparc sparcv9 sparc64 s390 s390x
    for file in `find . -name Makefile.am`; do
        sed -i -e 's:-fpic:-fPIC:' $file;
    done
%endif
# Fixup expected version of SWIG:
sed -i -e "s|AC_PROG_SWIG(1.3.28)|AC_PROG_SWIG(2.0.0)|g" configure.ac
# and rebuild the autotooled files:
aclocal
autoreconf -if

%build
automake
%configure --libdir=%{_libdir} --disable-bwidget-check --disable-selinux-check \
    --enable-swig-python --enable-swig-tcl
# work around issue with gcc 4.3 + gnu99 + swig-generated code:
sed -i -e 's:$(CC):gcc -std=gnu89:' libseaudit/swig/python/Makefile
make %{?_smp_mflags}

%install
rm -rf ${RPM_BUILD_ROOT}
make DESTDIR=${RPM_BUILD_ROOT} INSTALL="install -p" install
mkdir -p ${RPM_BUILD_ROOT}%{_datadir}/applications
mkdir -p ${RPM_BUILD_ROOT}%{_datadir}/pixmaps
install -d -m 755 ${RPM_BUILD_ROOT}%{_sysconfdir}/pam.d
install -p -m 644 %{SOURCE1} ${RPM_BUILD_ROOT}%{_sysconfdir}/pam.d/seaudit
install -d -m 755 ${RPM_BUILD_ROOT}%{_sysconfdir}/security/console.apps
install -p -m 644 packages/rpm/seaudit.console ${RPM_BUILD_ROOT}%{_sysconfdir}/security/console.apps/seaudit
install -d -m 755 ${RPM_BUILD_ROOT}%{_datadir}/applications
install -p -m 644 apol/apol.png ${RPM_BUILD_ROOT}%{_datadir}/pixmaps/apol.png
install -p -m 644 seaudit/seaudit.png ${RPM_BUILD_ROOT}%{_datadir}/pixmaps/seaudit.png
desktop-file-install --dir ${RPM_BUILD_ROOT}%{_datadir}/applications %{SOURCE2}
ln -sf consolehelper ${RPM_BUILD_ROOT}/%{_bindir}/seaudit
# remove static libs
rm -f ${RPM_BUILD_ROOT}/%{_libdir}/*.a
# ensure permissions are correct
chmod 0755 ${RPM_BUILD_ROOT}/%{_libdir}/*.so.*
chmod 0755 ${RPM_BUILD_ROOT}/%{_libdir}/%{name}/*/*.so.*
chmod 0755 ${RPM_BUILD_ROOT}/%{pkg_py_arch}/*.so.*
chmod 0644 ${RPM_BUILD_ROOT}/%{tcllibdir}/*/pkgIndex.tcl

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root,-)

%files libs
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING COPYING.GPL COPYING.LGPL KNOWN-BUGS NEWS README
%{_libdir}/libqpol.so.*
%{_libdir}/libapol.so.*
%{_libdir}/libpoldiff.so.*
%{_libdir}/libsefs.so.*
%{_libdir}/libseaudit.so.*
%dir %{setoolsdir}

%files libs-python
%defattr(-,root,root,-)
%{pkg_py_lib}/
%ifarch x86_64 ppc64 sparc64 s390x
%{pkg_py_arch}/
%endif

%files libs-tcl
%defattr(-,root,root,-)
%dir %{tcllibdir}
%{tcllibdir}/qpol/
%{tcllibdir}/apol/
%{tcllibdir}/poldiff/
%{tcllibdir}/seaudit/
%{tcllibdir}/sefs/

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_includedir}/qpol/
%{_includedir}/apol/
%{_includedir}/poldiff/
%{_includedir}/seaudit/
%{_includedir}/sefs/

%files console
%defattr(-,root,root,-)
%{_bindir}/seinfo
%{_bindir}/sesearch
%{_bindir}/sediff
%{_mandir}/man1/sediff.1*
%{_mandir}/man1/seinfo.1*
%{_mandir}/man1/sesearch.1*

%files gui
%defattr(-,root,root,-)
%{_bindir}/seaudit
%{_bindir}/apol
%{tcllibdir}/apol_tcl/
%{setoolsdir}/apol_help.txt
%{setoolsdir}/domaintrans_help.txt
%{setoolsdir}/file_relabel_help.txt
%{setoolsdir}/infoflow_help.txt
%{setoolsdir}/types_relation_help.txt
%{setoolsdir}/apol_perm_mapping_*
%{setoolsdir}/seaudit_help.txt
%{setoolsdir}/*.glade
%{setoolsdir}/*.png
%{setoolsdir}/apol.gif
%{setoolsdir}/dot_seaudit
%{_mandir}/man1/apol.1*
%{_mandir}/man8/seaudit.8*
%{_sbindir}/seaudit
%config(noreplace) %{_sysconfdir}/pam.d/seaudit
%config(noreplace) %{_sysconfdir}/security/console.apps/seaudit
%{_datadir}/applications/*
%attr(0644,root,root) %{_datadir}/pixmaps/*.png

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post libs-tcl -p /sbin/ldconfig

%postun libs-tcl -p /sbin/ldconfig

%changelog
* Mon Mar 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.3.7-10m)
- sync fc19 3.3.7-34
- Obsolete setools-libs-java

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.7-9m)
- update patches

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.7-8m)
- update patches

* Wed Jun 22 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-7m)
- fix build on x86_64

* Tue Jun 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.7-6m)
- add patch1-4

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.7-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.7-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.7-1m)
- update 3.3.7

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.5-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.5-1m)
- sync with Fedora 11 (3.3.5-8)

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-5m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.4-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.3.4-3m)
- rebuild against python-2.6.1-1m

* Sun Jul 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.4-2m)
- adapt libsepol-2.0.32

* Sun Apr 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.4-1m)
- version up 3.3.4
- sync Fedora

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2-5m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-4m)
- rebuild against gcc43

* Wed Mar 19 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.2-3m)
- add Patch1: setools-3.2-limits.patch for INT_MAX undeclared error

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-2m)
- %%NoSource -> NoSource

* Sat Jun  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Mon Mar  5 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1-4m)
- add setools-gui Provides: /usr/bin/awish

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-3m)
- remove duplicate file /usr/share/tcl8.4/init.tcl

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-2m)
- remove duplicate files %%dir

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-1m)
- update 3.1

* Wed Feb  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-1m)
- update 3.0. import from FC-devel

* Tue Jun  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3-1m)
- import from fc5

* Tue Apr 11 2006 Dan Walsh <dwalsh@redhat.com> 2.3-3.FC5
- Bump for FC5

* Mon Apr 10 2006 Dan Walsh <dwalsh@redhat.com> 2.3-3
- Fix help
- Add icons

* Tue Mar 21 2006 Dan Walsh <dwalsh@redhat.com> 2.3-2
- Remove console apps for sediff, sediffx and apol

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.3-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.3-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 31 2006 Dan Walsh <dwalsh@redhat.com> 2.3-1
- Update from upstream
  * apol:
	added new MLS components tab for sensitivities, 
	levels, and categories.
	Changed users tab to support ranges and default 
	levels.
	added range transition tab for searching range
	Transition rules.
	added new tab for network context components.
	added new tab for file system context components.
  * libapol:
	added binpol support for MLS, network contexts, 
	and file system contexts.
  * seinfo:
	added command line options for MLS components.
	added command line options for network contexts
	and file system contexts.
  * sesearch:
	added command line option for searching for rules
	by conditional boolean name.
  * seaudit:
	added new column in the log view for the 'comm' 
	field found in auditd log files.
	added filters for the 'comm' field and 'message'
	field.
  * manpages:
	added manpages for all tools.	



* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Wed Dec 14 2005 Dan Walsh <dwalsh@redhat.com> 2.2-4
- Fix dessktop files
- Apply fixes from bkyoung

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Nov 3 2005 Dan Walsh <dwalsh@redhat.com> 2.2-3
- Move more gui files out of base into gui 

* Thu Nov 3 2005 Dan Walsh <dwalsh@redhat.com> 2.2-2
- Move sediff from gui to main package

* Thu Nov 3 2005 Dan Walsh <dwalsh@redhat.com> 2.2-1
- Upgrade to upstream version

* Thu Oct 13 2005 Dan Walsh <dwalsh@redhat.com> 2.1.3-1
- Upgrade to upstream version

* Mon Oct 10 2005 Tomas Mraz <tmraz@redhat.com> 2.1.2-3
- use include instead of pam_stack in pam config

* Thu Sep 1 2005 Dan Walsh <dwalsh@redhat.com> 2.1.2-2
- Fix spec file
 
* Thu Sep 1 2005 Dan Walsh <dwalsh@redhat.com> 2.1.2-1
- Upgrade to upstream version
 
* Thu Aug 18 2005 Florian La Roche <laroche@redhat.com>
- do not package debug files into the -devel package

* Wed Aug 17 2005 Jeremy Katz <katzj@redhat.com> - 2.1.1-3
- rebuild against new cairo

* Wed May 25 2005 Dan Walsh <dwalsh@redhat.com> 2.1.1-0
- Upgrade to upstream version

* Mon May 23 2005 Bill Nottingham <notting@redhat.com> 2.1.0-5
- put libraries in the right place (also puts debuginfo in the right
  package)
- add %%defattr for -devel too

* Thu May 12 2005 Dan Walsh <dwalsh@redhat.com> 2.1.0-4
- Move sepcut to gui apps.

* Fri May 6 2005 Dan Walsh <dwalsh@redhat.com> 2.1.0-3
- Fix Missing return code.

* Wed Apr 20 2005 Dan Walsh <dwalsh@redhat.com> 2.1.0-2
- Fix requires line

* Tue Apr 19 2005 Dan Walsh <dwalsh@redhat.com> 2.1.0-1
- Update to latest from tresys

* Tue Apr 5 2005 Dan Walsh <dwalsh@redhat.com> 2.0.0-2
- Fix buildrequires lines in spec file

* Tue Mar 2 2005 Dan Walsh <dwalsh@redhat.com> 2.0.0-1
- Update to latest from tresys

* Mon Nov 29 2004 Dan Walsh <dwalsh@redhat.com> 1.5.1-6
- add FALLBACK=true to /etc/security/console.apps/apol

* Wed Nov 10 2004 Dan Walsh <dwalsh@redhat.com> 1.5.1-3
- Add badtcl patch from Tresys.

* Mon Nov 8 2004 Dan Walsh <dwalsh@redhat.com> 1.5.1-2
- Apply malloc problem patch provided by  Sami Farin 

* Mon Nov 1 2004 Dan Walsh <dwalsh@redhat.com> 1.5.1-1
- Update to latest from Upstream

* Wed Oct 6 2004 Dan Walsh <dwalsh@redhat.com> 1.4.1-5
- Update tresys patch

* Mon Oct 4 2004 Dan Walsh <dwalsh@redhat.com> 1.4.1-4
- Fix directory ownership

* Thu Jul 8 2004 Dan Walsh <dwalsh@redhat.com> 1.4.1-1
- Latest from Tresys

* Wed Jun 23 2004 Dan Walsh <dwalsh@redhat.com> 1.4-5
- Add build requires libselinux

* Tue Jun 22 2004 Dan Walsh <dwalsh@redhat.com> 1.4-4
- Add support for policy.18

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jun 10 2004 Dan Walsh <dwalsh@redhat.com> 1.4-2
- Fix install locations of policy_src_dir

* Wed Jun 2 2004 Dan Walsh <dwalsh@redhat.com> 1.4-1
- Update to latest from TRESYS.

* Tue Jun 1 2004 Dan Walsh <dwalsh@redhat.com> 1.3-3
- Make changes to work with targeted/strict policy
* Fri Apr 16 2004 Dan Walsh <dwalsh@redhat.com> 1.3-2
- Take out requirement for policy file

* Fri Apr 16 2004 Dan Walsh <dwalsh@redhat.com> 1.3-1
- Fix doc location

* Fri Apr 16 2004 Dan Walsh <dwalsh@redhat.com> 1.3-1
- Latest from TRESYS

* Tue Apr 13 2004 Dan Walsh <dwalsh@redhat.com> 1.2.1-8
- fix location of policy.conf file

* Tue Apr 6 2004 Dan Walsh <dwalsh@redhat.com> 1.2.1-7
- Obsolete setools-devel
* Tue Apr 6 2004 Dan Walsh <dwalsh@redhat.com> 1.2.1-6
- Fix location of 
* Tue Apr 6 2004 Dan Walsh <dwalsh@redhat.com> 1.2.1-5
- Remove devel libraries
- Fix installdir for lib64

* Sat Apr 3 2004 Dan Walsh <dwalsh@redhat.com> 1.2.1-4
- Add usr_t file read to policy

* Thu Mar 25 2004 Dan Walsh <dwalsh@redhat.com> 1.2.1-3
- Use tcl8.4

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 6 2004 Dan Walsh <dwalsh@redhat.com> 1.2.1-1
- New patch

* Fri Feb 6 2004 Dan Walsh <dwalsh@redhat.com> 1.2-1
- Latest upstream version

* Tue Dec 30 2003 Dan Walsh <dwalsh@redhat.com> 1.1.1-1
- New version from upstream
- Remove seuser.te.  Now in policy file.

* Tue Dec 30 2003 Dan Walsh <dwalsh@redhat.com> 1.1-2
- Add Defattr to devel
- move libs to base kit

* Fri Dec 19 2003 Dan Walsh <dwalsh@redhat.com> 1.1-1
- Update to latest code from tresys
- Break into three separate packages for cmdline, devel and gui
- Incorporate the tcl patch

* Mon Dec 15 2003 Jens Petersen <petersen@redhat.com> - 1.0.1-3
- apply setools-1.0.1-tcltk.patch to build against tcl/tk 8.4
- buildrequire tk-devel

* Thu Nov 20 2003 Dan Walsh <dwalsh@redhat.com> 1.0.1-2
- Add Bwidgets to this RPM

* Tue Nov 4 2003 Dan Walsh <dwalsh@redhat.com> 1.0.1-1
- Upgrade to 1.0.1

* Wed Oct 15 2003 Dan Walsh <dwalsh@redhat.com> 1.0-6
- Clean up build

* Tue Oct 14 2003 Dan Walsh <dwalsh@redhat.com> 1.0-5
- Update with correct seuser.te

* Wed Oct 1 2003 Dan Walsh <dwalsh@redhat.com> 1.0-4
- Update with final release from Tresys

* Mon Jun 2 2003 Dan Walsh <dwalsh@redhat.com> 1.0-1
- Initial version




