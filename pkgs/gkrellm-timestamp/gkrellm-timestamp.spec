%global momorel 6

Name:           gkrellm-timestamp
Version:        0.1.4 
Release:        %{momorel}m%{?dist}
Summary:        UNIX timestamp clock plugin for GKrellM
Group:          Applications/System
License:        GPLv2+
URL:            http://arnhold.org/~thomas/projects/gkrellm-timestamp/
Source0:        http://arnhold.org/~thomas/projects/gkrellm-timestamp/%{name}-%{version}.tar.bz2
Patch0:         gkrellm-timestamp-0.1.4-makefile.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gkrellm-devel
Requires:       gkrellm 

%description
GkrellM Timestamp is a GkrellM plugin that
shows the current UNIX timestamp like the default clock.

%prep
%setup -q

touch -r Makefile Makefile.stamp
%patch0
touch -r Makefile.stamp Makefile

%build
make CFLAGS="%{optflags}" %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 0755 timestamp.so  \
        $RPM_BUILD_ROOT%{_libdir}/gkrellm2/plugins/timestamp.so

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root, -)
%doc CHANGELOG LICENSE
%{_libdir}/gkrellm2/plugins/timestamp.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.4-2m)
- rebuild against rpm-4.6

* Mon Jun 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.4-1m)
- import from Fedora to Momonga

* Thu Feb 28 2008 Nikolay Vladimirov <nikolay@vladimiroff.com> 0.1.4-4
- Added SMP flags on build

* Mon Feb 25 2008 Nikolay Vladimirov <nikolay@vladimiroff.com> 0.1.4-3
- Added patch to fix Makefile(ignored optimization flags)

* Sun Aug 5 2007 Nikolay Vladimirov <nikolay@vladimiroff.com> 0.1.4-2
- Upstream added LICENSE

* Mon Jul 30 2007 Nikolay Vladimirov <nikolay@vladimiroff.com> 0.1.4-1
- initial rpm release
