%global momorel 1

Summary: Associates helper applications with particular file types
Name: mailcap
Version: 2.1.42
Release: %{momorel}m%{?dist}
License: Public Domain
Group: System Environment/Base
Source0: https://fedorahosted.org/releases/m/a/mailcap/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: mailcap-2.1.33-no-non-ascii-chars.patch
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: /etc/mime.types

%description
The mailcap file is used by the metamail program.  Metamail reads the
mailcap file to determine how it should display non-text or multimedia
material.  Basically, mailcap associates a particular type of file
with a particular program that a mail agent or other program can call
in order to handle the file.

Mailcap should be installed to allow certain programs to be able to
handle non-text files.

%prep
%setup -q
%patch0 -p1 -b .no-non-ascii-chars

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}
install -p -m 644 mailcap %{buildroot}%{_sysconfdir}
install -p -m 644 mime.types %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}%{_mandir}/man4
install -p -m 644 mailcap.4 %{buildroot}%{_mandir}/man4

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/mailcap
%config(noreplace) %{_sysconfdir}/mime.types
%{_mandir}/man4/mailcap.*

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.42-1m)
- update 2.1.42

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.33-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.33-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.33-3m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.33-2m)
- remove non-ascii chars

* Sat Jul  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.33-1m)
- update to 2.1.33

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.29-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.29-1m)
- update to 2.1.29

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.28-2m)
- rebuild against rpm-4.6

* Wed Jun 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.28-1m)
- update to 2.1.28

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.23-2m)
- rebuild against gcc43

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.23-1m)
- update to 2.1.23 (sync with FC-devel)

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.20-1m)
- update 2.1.20

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (2.1.17-1m)
- sync with FC3(2.1.17-1).

* Thu Oct  9 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.14-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Aug 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.14-1m)
- sync with redhat mailcap-2.1.14-1.1
   ooffice is used in mailcap. but I believe that takahata make it no problem pretty soon...
- add mime.type for iso

* Fri Feb 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.12-2m)
- provides /etc/mime.types

* Tue Nov  5 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.12-1m)
- version up(sync with redhat mailcap-2.1.12-1)

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.1.9-2k)
- version up.

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (2.1.7-2k)
- version up.

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org>
- Kodaraized.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jun 12 2000 Preston Brown <pbrown@redhat.com>
- add wap entries

* Fri Jun  9 2000 Bill Nottingham <notting@redhat.com>
- remove mailcap.vga

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Tue Jan 18 2000 Bill Nottingham <notting@redhat.com>
- add .bz2

* Thu Jan 13 2000 Bill Nottingham <notting@redhat.com>
- add tgz/gz to gzip

* Thu Jun 16 1999 Bill Nottingham <notting@redhat.com>
- rpm files are RPM files. :)

* Sat May 15 1999 Jeff Johnson <jbj@redhat.com>
- fix typo in pdf entry (#2618).

* Mon Mar 29 1999 Bill Nottingham <notting@redhat.com>
- comment out play

* Fri Mar 19 1999 Preston Brown <pbrown@redhat.com>
- updated mime type for images from xv to ee
- cleaned up for our new version of the package which is in CVS

* Sat Mar 13 1999 Matt Wilson <msw@redhat.com>
- updated mime.types

* Fri Feb 12 1999 Bill Nottingham <notting@redhat.com>
- comment out backticked %variables to work around security problems

* Wed Jan 06 1999 Cristian Gafton <gafton@redhat.com>
- glibc version 2.1

* Mon Sep 21 1998 Bill Nottingham <notting@redhat.com>
- we don't ship tracker, use mikmod instead

* Wed Jul 29 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Oct 21 1997 Donnie Barnes <djb@redhat.com>
- added /etc/mime.types from mutt to this package to make it universal

* Tue Sep 09 1997 Erik Troan <ewt@redhat.com>
- made a noarch package
