%global momorel 1

Summary: A text-based Web browser
Name: lynx
Version: 2.8.8
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
URL: http://lynx.isc.org/
Source0: http://lynx.isc.org/lynx%{version}/lynx%{version}.tar.bz2
NoSource: 0
Source1: lynx.desktop
Source2: gnome-html.png
Patch0: lynx2-8-8-momonga.patch
Patch3: lynx-2.8.6-backgrcolor.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: slang zlib openssl
Requires: indexhtml
BuildRequires: gettext
BuildRequires: ncurses-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: pkgconfig
BuildRequires: rsh
BuildRequires: slang-devel
BuildRequires: telnet
BuildRequires: unzip
BuildRequires: zip
BuildRequires: zlib-devel
Provides: webclient
Provides: text-www-browser

%description
Lynx is a text-based Web browser. Lynx does not display any images,
but it does support frames, tables and most other HTML tags. Lynx's
advantage over graphical browsers is its speed: Lynx starts and exits
quickly and swiftly displays Web pages.

Install lynx if you would like to try this fast, non-graphical browser
(you may come to appreciate its strengths).

%prep
%setup -q -n lynx2-8-8
%patch0 -p1 -b .momonga
%patch3 -p1 -b .bgcol

perl -pi -e "s,^HELPFILE:.*,HELPFILE:file://localhost/usr/share/doc/lynx-%{version}/lynx_help/lynx_help_main.html,g" lynx.cfg
perl -pi -e "s,^DEFAULT_INDEX_FILE:.*,DEFAULT_INDEX_FILE:http://www.google.com/,g" lynx.cfg
perl -pi -e 's,^#LOCALE_CHARSET:.*,LOCALE_CHARSET:TRUE,' lynx.cfg

%build
export CFLAGS="-ggdb %{optflags} -DNCURSES -DNCURSES_MOUSE_VERSION"
export CXXFLAGS="-ggdb %{optflags} -DNCURSES -DNCURSES_MOUSE_VERSION"

if pkg-config openssl ; then
	export CPPFLAGS=`pkg-config --cflags openssl`
	export LDFLAGS=`pkg-config --libs-only-L openssl`
fi

%configure \
	--libdir=%{_sysconfdir} \
	--with-screen=ncursesw \
	--enable-warnings \
	--enable-default-colors \
	--enable-externs \
	--enable-internal-links \
	--enable-nsl-fork \
	--enable-persistent-cookies \
	--enable-prettysrc \
	--disable-font-switch \
	--enable-source-cache \
	--enable-kbd-layout \
	--with-zlib \
	--enable-charset-choice \
	--enable-file-upload \
	--enable-cgi-links \
	--enable-read-eta \
	--enable-addrlist-page \
	--enable-cjk \
	--enable-justify-elts \
	--enable-scrollbar \
	--enable-libjs \
	--enable-cgi-links \
	--enable-nls \
	--enable-ipv6 \
	--enable-locale-charset \
	--enable-japanese-utf8 \
	--with-ssl=%{_libdir}

%{make}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

chmod -x samples/mailto-form.pl

%{makeinstall} mandir=%{buildroot}%{_mandir}/man1

# install lynx-site.cfg
cat >%{buildroot}%{_sysconfdir}/lynx-site.cfg <<EOF
# Place any local lynx configuration options (proxies etc.) here.
EOF

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/pixmaps/lynx.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS CHANGES COPYING INSTALLATION README
%doc docs samples
%doc test lynx.hlp lynx_help
%config %{_sysconfdir}/lynx.cfg
%config(noreplace) %{_sysconfdir}/lynx.lss
%config(noreplace,missingok) %{_sysconfdir}/lynx-site.cfg
%{_bindir}/lynx
%{_datadir}/applications/lynx.desktop
%{_datadir}/locale/*/LC_MESSAGES/lynx.mo
%{_mandir}/man1/lynx.1*
%{_datadir}/pixmaps/lynx.png

%changelog
* Fri May 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.8.8-1m)
- update to 2.8.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.7-6m)
- rebuild for new GCC 4.6

* Sun Dec 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.7-5m)
- source was changed, remove srpm and build with --rmsrc option
- update momonga patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.7-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.7-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.7-2m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.7-1m)
- update to 2.8.7
-- update Patch0 and build fixes
-- drop Patch1,2,4, merged upstream

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-5m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-4m)
- %%global _default_patch_fuzz 2
- License: GPLv2+

* Wed Oct 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.6-3m)
- [SECURITY] CVE-2006-7234 version 2.8.6rel.5
- import build fixes patches from Fedora

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.6-2m)
- rebuild against openssl-0.9.8h-1m

* Wed May  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.6-1m)
- version 2.8.6
- return to trunk
- import gnome-html.png as lynx.png from gnome-desktop-2.22.1
- add momonga.patch (modified redhat.patch)
- import 4 patches from Fedora

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.4rel.1c-4m)
- move lynx.desktop to %%{_datadir}/applications/
- add Source1: lynx.desktop
  OnlyShowIn=GNOME;KDE;
  lynx.desktop doesn't work on XFce4
  revise it!
- modify Patch10: lynx2-8-4-config.patch
  STARTFILE:http://lynx.browser.org/
  %%{_docdir}/HTML/index.html{.en,.ja} doesn't work...

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.8.4rel.1c-3m)
- rebuild against slang(utf8 version)
    change variable name UTF8 to UTF_8
- use %%{?_smp_mflags}

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.8.4rel.1c-2m)
  rebuild against openssl 0.9.7a

* Mon Aug 26 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.8.4rel.1c-1m)
- [security] Update to 2.8.4rel.1c.
  This patch fixes the "Lynx CRLF Injection". See
    http://online.securityfocus.com/archive/1/288054
    http://online.securityfocus.com/archive/1/288620
  for details.
- Change patch site to ftp://lynx.isc.org/lynx/lynx2.8.4/patches/.
  The patches on this site are slightly different from previous ones.

* Sun Dec 30 2001 WATABE Toyokazu <toy2@kondara.org>
- (2.8.4rel.1b-4k)
- fix a format string vulnerability.
  http://archives.neohapsis.com/archives/bugtraq/2001-12/0276.html

* Sat Dec 29 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.8.4rel.1b-2k)
- update to 2.8.4rel.1b
- enable ssl
- fix lynx.cfg 
  (move lynx2-8-3-redhat.patch to lynx2-8-4-config.patch, and
   move lynx2-8-2-vine.patch to lynx2-8-4-charset.patch)

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (2.8.4rel.1-6k)
- many stuff

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (2.8.4rel.1-4k)
- modify ja.po

* Wed Nov  7 2001 Shingo Akagaki <dora@kondara.org>
- (2.8.4rel.1-2k)
- version 2.8.4rel.1

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (2.8.3-12k)
- rebuild against gettext 0.10.40.

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (2.8.3-10k)
- no more ifarch alpha.

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.8.3-8k)
- modified spec file with macor for compatibility

* Sun Aug 27 2000 Kenichi Matsubara <m@kondara.org>
- Change PATH (FHS).

* Fri Jul 07 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Nov 11 1999 Norihito Ohmori <nono@kondara.org>
- be a NoSrc :-P
- added ja.po (imcomplete).
- use kterm in lynx.wmconfig
- added Japanese summary and description
- change for Vine
- change default CHARSET to Japanese(EUC).

* Wed Nov  3 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix compliance with RFCs describing FTP.
  We can now connect to wu-ftpd >= 2.6.0 based servers.
  
* Wed Aug 25 1999 Bill Nottingham <notting@redhat.com>
- fix path to help file.
- turn off font switching
- disable args to telnet.


* Tue Jun 15 1999 Bill Nottingham <notting@redhat.com>
- update to 2.8.2

* Mon Mar 29 1999 Bill Nottingham <notting@redhat.com>
- apply some update patches from the lynx folks
- set user's TEMP dir to their home dir to avoid /tmp races

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 10)

* Wed Feb 24 1999 Bill Nottingham <notting@redhat.com>
- return of wmconfig

* Mon Nov 30 1998 Bill Nottingham <notting@redhat.com>
- create cookie file 0600

* Fri Nov  6 1998 Bill Nottingham <notting@redhat.com>
- update to 2.8.1rel2

* Thu Oct 29 1998 Bill Nottingham <notting@redhat.com>
- build for Raw Hide (slang-1.2.2)

* Sat Oct 10 1998 Cristian Gafton <gafton@redhat.com>
- 2.8.1pre9
- strip binaries

* Mon Oct 05 1998 Cristian Gafton <gafton@redhat.com>
- updated to lynx2.8.1pre.7.tar.gz

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon May 04 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 2.8rel3
- fixed mailto: buffer overflow (used Alan's patch)

* Fri Mar 20 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.8
- added buildroot

* Tue Jan 13 1998 Erik Troan <ewt@redhat.com>
- updated to 2.7.2
- enabled lynxcgi

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- updated from 2.6 to 2.7.1
- moved /usr/lib/lynx.cfg to /etc/lynx.cfg
- build with slang instead of ncurses
- made default startup file be file:/usr/doc/HTML/index.html

