%global momorel 11

%global version_suffix b

Summary: libid3tag - ID3 tag manipulation library
Name:    libid3tag
Version: 0.15.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group:   Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/mad/libid3tag-%{version}%{version_suffix}.tar.gz 
NoSource: 0
Patch0:  libid3tag-0.15.0-id3tag.pc.patch
Patch1:  libid3tag-0.15.1b-libdir.patch
Patch10: libid3tag-0.15.1b-fix_overflow.patch
URL: http://www.underbit.com/products/mad/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  zlib-devel >= 1.1.4
BuildRequires: gperf
Obsoletes: mad
Obsoletes: mad-devel

%description
libid3tag is a library for reading and (eventually) writing ID3 tags,
both ID3v1 and the various versions of ID3v2.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
 
%description    devel
ID3 tag library development files.

%prep
%{__rm} -rf %{buildroot}

%setup -q -n %{name}-%{version}%{version_suffix}
%patch0 -p1
%patch1 -p1 -b .libdir~

%patch10 -p0 -b .CVE-2008-2109

# *.pc originally from the Debian package.
cat << \EOF > %{name}.pc
prefix=%{_prefix}
exec_prefix=%{_exec_prefix}
libdir=%{_libdir}
includedir=%{_includedir}

Name: id3tag
Description: ID3 tag manipulation library
Requires:
Version: %{version}
Libs: -L${libdir} -lid3tag
Cflags: -I${includedir}
EOF

%build
%configure --disable-dependency-tracking --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
install -Dpm 644 %{name}.pc $RPM_BUILD_ROOT%{_libdir}/pkgconfig/id3tag.pc

%clean
%{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc CHANGES COPYING COPYRIGHT CREDITS README TODO
%{_libdir}/libid3tag.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/id3tag.h
%{_libdir}/libid3tag.so
%{_libdir}/pkgconfig/id3tag.pc


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15.1-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.1-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.1-7m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.1-6m)
- update Patch1 for fuzz=0

* Tue Jul 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15.1-5m)
- separate devel package

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15.1-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15.1-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.1-2m)
- delete libtool library

* Sat Aug  6 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.15.1-1m)
- up to 0.15.1b
- change Source's URI

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.15.0-0.1.2m)
- enable x86_64.

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.15.0-0.1.1m)
- Initial specfile
