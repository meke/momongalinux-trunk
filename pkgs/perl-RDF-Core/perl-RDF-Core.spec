%global momorel 22
Summary: RDF-Core module for perl 
Name: perl-RDF-Core
Version: 0.51
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Languages
Source0: http://www.cpan.org/modules/by-module/RDF/RDF-Core-%{version}.tar.gz
NoSource: 0
URL: http://www.cpan.org/modules/by-module/RDF/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 1:5.8.8
BuildArch: noarch
Requires: perl-XML-Parser

%description
RDF-Core module for perl

%prep
%setup -q -n RDF-Core-%{version} 

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make


%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
find %{buildroot}%{perl_vendorarch}/auto -name '.packlist' | xargs rm -f

find %{buildroot}/usr -type f -print | \
	sed "s@^%{buildroot}@@g" | \
	grep -v perllocal.pod | \
	sed -e 's,\(.*/man/.*\),\1*,' | \
	grep -v "\.packlist" > RDF-Core-%{version}-filelist
if [ "$(cat RDF-Core-%{version}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

%clean 
rm -rf %{buildroot}

%files -f RDF-Core-%{version}-filelist
%defattr(-,root,root)
%doc Changes README*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.51-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.51-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.51-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.51-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.51-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.51-2m)
- rebuild against gcc43

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.50-2m)
- use vendor

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.50-1m)
- spec file was autogenerated
