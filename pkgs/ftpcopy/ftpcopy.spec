%global momorel 8

Name: ftpcopy
Version: 0.6.7
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
Source0: http://www.ohse.de/uwe/ftpcopy/ftpcopy-%{version}.tar.gz 
NoSource: 0
URL: http://www.ohse.de/uwe/ftpcopy.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary: An ftp mirroring tool.
Patch0: ftpcopy-0.6.2-unistd.patch

%description
ftpcopy is a simply FTP client written to copy files or directories
(recursively) from a FTP server. It's primary purpose is to mirror
FTP sites which support the EPLF directory listing format, but it
may be used to mirror other sites, too.

%prep
%setup -q -n web
cd %{name}-%{version}
%patch0 -p1 -b .unistd

%build
cd %{name}-%{version}
export CFLAGS="%{optflags}"
%make

%install
cd %{name}-%{version}
[ "%{buildroot}" != "/" ] && rm -fr %{buildroot}
mkdir -p %{buildroot}{%{_bindir},%{_mandir}/man1}
install -m 755 command/* %{buildroot}%{_bindir}
install -m 644 compile/*.1 %{buildroot}%{_mandir}/man1

# Copy %doc files to work around rediculous upstream packaging (web dir)
cp src/{ChangeLog,NEWS,README,THANKS} ..

%clean
[ "%{buildroot}" != "/" ] && rm -fr %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog NEWS README THANKS
%{_bindir}/*
%{_mandir}/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.7-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.7-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.7-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.7-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.7-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.7-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.7-2m)
- %%NoSource -> NoSource

* Sun Feb 13 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.6.7-1m)
- import from FC3
