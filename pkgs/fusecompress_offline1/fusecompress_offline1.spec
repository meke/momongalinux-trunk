%global         momorel 5

Name:           fusecompress_offline1
Version:        1.99.19
Release:        %{momorel}m%{?dist}
Summary:        Utility to help migrate off of fusecompress-1.x

Group:          System Environment/Libraries
License:        GPLv2
URL:            http://miio.net/fusecompress/
Source0:        http://miio.net/files/fusecompress-%{version}.tar.gz
Source1:        README.fedora
Patch0:         fusecompress-1.99.19-types.patch
Patch1:         fusecompress-1.99.19-gcc44.patch
Patch2:         fusecompress-offline-decompress.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  rlog-devel fuse-devel zlib-devel bzip2-devel file-devel
BuildRequires:  autoconf
Requires:       fuse


%description
FuseCompress provides a mountable Linux filesystem which transparently
compresses its content.  Files stored in this filesystem are compressed
on the fly and Fuse allows to create a transparent interface between
compressed files and user applications.

This package provides fusecompress_offline1, a utility which enables users
of fusecompress-1.x filesystems to decompress their files.  This is a
necessary step in preparing their files for use with fusecompress-2.x
filesystems.  Instructions for performing an upgrade are in README.fedora or
the Fedora 12 release notes.

%prep
%setup -q -n fusecompress-%{version}
%patch0 -p1 -b .types
%patch1 -p1 -b .gcc44
%patch2 -p1 -b .offline
cp %{SOURCE1} .
chmod a-x ChangeLog

find src -type f -exec chmod 0644 \{\} \;

%build
autoconf
autoheader
%configure
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mv %{buildroot}%{_bindir}/fusecompress_offline %{buildroot}%{_bindir}/fusecompress_offline1
mv %{buildroot}%{_mandir}/man1/fusecompress_offline.1 %{buildroot}%{_mandir}/man1/fusecompress_offline1.1

# This package is just for upgrading to fusecompress2.  So we don't want the
# binaries that enable mounting the old format
rm -f %{buildroot}%{_bindir}/fusecompress
rm -f %{buildroot}%{_mandir}/man1/fusecompress.1*


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_bindir}/fusecompress_offline1
%{_mandir}/man1/fusecompress_offline1.1*
%doc ChangeLog README AUTHORS COPYING NEWS README.fedora


%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.99.19-5m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.19-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.19-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.99.19-1m)
- import from Fedora

* Fri Sep 25 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 1.99.19-6
- Remove executable bit from source files
- Rename package to fusecompress_offline1 to more closely match what's actually
  in it.

* Fri Sep 25 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 1.99.19-5
- Fix build as a package just to enable migration from old on-disk format.
- Patch to fix offline decompression when the old compression format was lzo.

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.99.19-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 03 2009 Lubomir Rintel <lkundrak@v3.sk> - 1.99.19-3
- Fix build with gcc44

* Sun Sep 28 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.99.19-2
- Fix build
- Drop silly mount wrapper

* Sat Sep 27 2008 Luke Macken <lmacken@redhat.com> - 1.99.19-1
- Update to 1.99.19
- Remove fusecompress-1.99.17-attr.patch and fusecompress-1.99.17-gcc43.patch,
  as they are now upstream in this release.

* Sun Aug 10 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.99.18-1
- New upstream release, bugfix

* Wed Jul 16 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.99.17-2
- Fix build:
- Upstream added manual
- Upstream looks for xattr.h in a little non-standard place
- GCC 4.3 fix

* Wed Jul 16 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.99.17-1
- Upstream integrated our fix

* Sun Jul 13 2008 Lubomir Rintel <lkundrak@v3.sk> 1.99.16-1
- New upstream release
- Fedora patches integrated upstream
- Rebuild against newer librlog

* Mon Apr 07 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.99.14-3
- Fix build with gcc43

* Mon Apr 07 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.99.14-2
- move mount helper to doc

* Mon Mar 10 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.99.14-1
- mount helper
- new upstream

* Fri Nov 02 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.99.13-1
- Initial package
