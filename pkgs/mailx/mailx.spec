%global momorel 4

%define	use_nss	1
%define mailrc	%{_sysconfdir}/mail.rc

Summary: Enhanced implementation of the mailx command
Name: mailx
Version: 12.4
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: BSD and MPLv1.1
URL: http://heirloom.sourceforge.net/mailx.html
Source0: http://downloads.sourceforge.net/heirloom/mailx-%{version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0: nail-11.25-config.patch
Patch1: mailx-12.3-pager.patch
Patch2: mailx-12.4-openssl.patch

%if %{use_nss}
BuildRequires: nss-devel, pkgconfig, krb5-devel
%else
BuildRequires: openssl-devel
%endif

Obsoletes: nail <= %{version}
Provides: nail = %{version}


%description
Mailx is an enhanced mail command, which provides the functionality
of the POSIX mailx command, as well as SysV mail and Berkeley Mail
(from which it is derived).

Additionally to the POSIX features, mailx can work with Maildir/ e-mail
storage format (as well as mailboxes), supports IMAP, POP3 and SMTP
procotols (including over SSL) to operate with remote hosts, handles mime
types and different charsets. There are a lot of other useful features,
see mailx(1).

And as its ancient analogues, mailx can be used as a mail script language,
both for sending and receiving mail.

Besides the "mailx" command, this package provides "mail" and "Mail"
(which should be compatible with its predecessors from the mailx-8.x source),
as well as "nail" (the initial name of this project).


%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1

sed -i 's,/etc/nail.rc,%{mailrc},g' mailx.1 mailx.1.html


%build

%if %{use_nss}
INCLUDES="$INCLUDES `pkg-config --cflags-only-I nss`"
export INCLUDES
%endif

echo	PREFIX=%{_prefix} \
	BINDIR=/bin \
	MANDIR=%{_mandir} \
	SYSCONFDIR=%{_sysconfdir} \
	MAILRC=%{mailrc} \
	MAILSPOOL=%{_localstatedir}/mail \
	SENDMAIL=%{_sbindir}/sendmail \
	UCBINSTALL=install \
> makeflags

#  %{?_smp_mflags} cannot be used here
make `cat makeflags` \
	CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE" \
	IPv6=-DHAVE_IPv6_FUNCS


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT STRIP=: `cat makeflags` install

ln -s mailx $RPM_BUILD_ROOT/bin/mail

install -d $RPM_BUILD_ROOT%{_bindir}
pref=`echo %{_bindir} | sed 's,/[^/]*,../,g'`

pushd $RPM_BUILD_ROOT%{_bindir}
ln -s ${pref}bin/mailx Mail
ln -s ${pref}bin/mailx nail
popd

pushd $RPM_BUILD_ROOT%{_mandir}/man1
ln -s mailx.1 mail.1
ln -s mailx.1 Mail.1
ln -s mailx.1 nail.1
popd


%clean
rm -rf $RPM_BUILD_ROOT


%triggerpostun -- mailx < 12
[ -f %{mailrc}.rpmnew ] && {
    # old config was changed. Merge both together.
    ( echo '# The settings above was inherited from the old mailx-8.x config'
      echo
      cat %{mailrc}.rpmnew
    ) >>%{mailrc}
} || :


%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS README
%config(noreplace) %{mailrc}
/bin/mail
/bin/mailx
%{_bindir}/Mail
%{_bindir}/nail
%{_mandir}/man1/Mail.1*
%{_mandir}/man1/mail.1*
%{_mandir}/man1/mailx.1*
%{_mandir}/man1/nail.1*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (12.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (12.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (12.4-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (12.4-1m)
- sync with Fedora 13 (12.4-6)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1.1-28m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.1.1-27m)
- release %%{_mandir} and %%{_mandir}/man1 provided by filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1.1-26m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1.1-25m)
- update Patch2 for fuzz=0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1.1-24m)
- rebuild against gcc43

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (8.1.1-23m)
- add source to repository

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (8.1.1-22k)
- updated Source URL.

* Tue Jun 19 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (8.1.1-20k)
- fix paths for Kondara 2.0 (mailx-toriaezu.patch)

* Mon Aug  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- disable security-prone variables-inherited-from-environment behavior
- inherit only SHELL, DEAD, PAGER, LISTER, EDITOR, VISUAL, MBOX from environment
- document the environment variables that are used

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jun 21 2000 Preston Brown <pbrown@redhat.com>
- noreplace mail.rc

* Sun Jun 18 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.

* Sun May  7 2000 Bill Nottingham <notting@redhat.com>
- fix explosions on ia64

* Thu Feb 03 2000 Elliot Lee <sopwith@redhat.com>
- Fix bug #8451
- Fix bug #8100

* Tue Aug 31 1999 Bill Nottingham <notting@redhat.com>
- fix pathnames
- take out makefile install crud

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Thu Aug 27 1998 Alan Cox <alan@redhat.com>
- Synchronized with the Debian people (more small edge case cures)

* Tue Aug  4 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Wed Jun 24 1998 Alan Cox <alan@redhat.com>
- Switched dotlocking off. This fits the Red Hat model of not having setgid
  mail agents and fixes the "lock" problem reported.

* Mon Jun 22 1998 Alan Cox <alan@redhat.com>
- Buffer overrun patches. These dont bite us when we don't run mailx setgid
  but do want to be in as mailx needs to be setgid

* Fri Jun 12 1998 Alan Cox <alan@redhat.com>
- Moved from 5.5 to the OpenBSD 8.1 release plus Debian patches

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Oct 21 1997 Donnie Barnes <djb@redhat.com>
- spec file cleanups

* Thu Jun 12 1997 Erik Troan <ewt@redhat.com>
- built against glibc
