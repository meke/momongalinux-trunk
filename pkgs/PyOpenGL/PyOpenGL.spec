%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           PyOpenGL
Version:        3.0.1
Release:        %{momorel}m%{?dist}
Summary:        Python bindings for OpenGL
License:        BSD
Group:          System Environment/Libraries
URL:            http://pyopengl.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/pyopengl/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         PyOpenGL-3.0.1-shebang.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel python-setuptools-devel
BuildArch:      noarch
Requires:       python-numeric python-setuptools libGL libGLU freeglut
# in some other repositories this is named python-opengl
Provides:       python-opengl = %{version}-%{release}
Obsoletes:      python-opengl < %{version}-%{release}

%description
PyOpenGL is the cross platform Python binding to OpenGL and related APIs. It
includes support for OpenGL v1.1, GLU, GLUT v3.7, GLE 3 and WGL 4. It also
includes support for dozens of extensions (where supported in the underlying
implementation).

PyOpenGL is interoperable with a large number of external GUI libraries
for Python including (Tkinter, wxPython, FxPy, PyGame, and Qt). 


%package Tk
Summary:        %{name} OpenGL Tk widget
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}, tkinter

%description Tk
%{name} Togl (Tk OpenGL widget) 1.6 support.


%package doc
Summary:        Documentation files for %{name}
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}

%description doc
Documentation files for %{name}.


%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -z .shebang


%build
python setup.py build


%install
rm -rf %{buildroot}
python setup.py install -O1 --skip-build --root="%{buildroot}" \
  --prefix="%{_prefix}" 
chmod -x %{buildroot}%{python_sitelib}/%{name}-%{version}-py*.egg-info


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc license.txt ChangeLog.txt
%{python_sitelib}/*OpenGL*
%exclude %{python_sitelib}/OpenGL/Tk

%files Tk
%defattr(-,root,root,-)
%{python_sitelib}/OpenGL/Tk

%files doc
%defattr(-,root,root,-)
#%doc documentation/*


%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-0.5.b1.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-0.5.b1.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.0-0.5.b1.5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-0.5.b1.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-0.5.b1.3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.0-0.5.b1.2m)
- rebuild agaisst python-2.6.1-1m

* Sun May  4 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.0-0.5.b1.1m)
- import from Fedora

* Mon Dec 31 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 3.0.0-0.5.b1
- New upstream release 3.0.0b1

* Thu Aug 30 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 3.0.0-0.4.a6
- Change BuildRequires python-setuptools to python-setuptools-devel for
  the python-setuptools package split

* Fri Apr 13 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 3.0.0-0.3.a6
- Add missing freeglut, libGL and libGLU requires (bz 236159)

* Thu Mar 29 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 3.0.0-0.2.a6
- Remove tests from the package (bz 234121)
- Add -Tk subpackage (bz 234121)
- Remove shebang from files with shebang instead of chmod +x (bz 234121)
- Better description

* Sat Mar 24 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 3.0.0-0.1.a6
- Initial Fedora Extras package
