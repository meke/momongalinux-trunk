%global momorel 1

%define gupnp_ver 0.18.4
%define gupnp_rel 1m
%define glib2_ver 2.33.10

Name:           gupnp-av
Version:        0.10.3
Release:        %{momorel}m%{?dist}
Summary:	Library to handle UPnP A/V profiles

Group:          System Environment/Libraries
License:	LGPLv2+
URL:            http://www.gupnp.org/
Source0:        ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.10/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel >= %{glib2_ver}
BuildRequires:	gupnp-devel >= %{gupnp_ver}-%{gupnp_rel}

%description
GUPnP A/V is a small utility library that aims to ease the handling and
implementation of UPnP A/V profiles.

%package        devel
Summary:        %{name}-devel
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static \
	--enable-silent-rules \
	--enable-introspection=yes \
	--enable-gtk-doc \
	LIBS="-lgobject-2.0 -lglib-2.0"
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libgupnp-av-1.0.so.*
%exclude %{_libdir}/libgupnp-av-1.0.la
%{_libdir}/girepository-1.0/GUPnPAV-1.0.typelib

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}-1.0
%{_libdir}/libgupnp-av-1.0.so
%{_libdir}/pkgconfig/%{name}-1.*.pc
%{_datadir}/gtk-doc/html/%{name}
%{_datadir}/gir-1.0/GUPnPAV-1.0.gir

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.1-2m)
- rebuild for glib 2.33.2

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-3m)
- enable-introspection

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-2m)
- rebuild against gupnp-0.14.0-2m
- disable gobject-introspection

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- initial build

