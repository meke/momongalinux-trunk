%global momorel 1

Name:           lorax
Version:        19.6.28
Release:        %{momorel}m%{?dist}
Summary:        Tool for creating the anaconda install images

Group:          Applications/System
License:        GPLv2+
URL:            http://git.fedorahosted.org/git/?p=lorax.git
Source0:        https://fedorahosted.org/releases/l/o/%{name}/%{name}-%{version}.tar.gz

Patch0: 	lorax-19.6.27-mo8.patch
Patch1: 	lorax-18.31-no_efi.patch
Patch2: 	lorax-19.5-selinux.patch
Patch3: 	lorax-18.31-text-mode.patch

BuildRequires:  python2-devel

Requires:       GConf2
Requires:       cpio
Requires:       device-mapper
Requires:       dosfstools
Requires:       e2fsprogs
Requires:       findutils
Requires:       gawk
Requires:       genisoimage
Requires:       glib2
Requires:       glibc
Requires:       glibc-common
Requires:       gzip
Requires:       isomd5sum
Requires:       libselinux-python
Requires:       module-init-tools
Requires:       parted
Requires:       python-mako
Requires:       squashfs-tools >= 4.2
Requires:       util-linux
Requires:       xz
Requires:       yum
Requires:       pykickstart

%ifarch %{ix86} x86_64
Requires:       syslinux
%endif

%ifarch %{sparc}
Requires:       silo
%endif

%description
Lorax is a tool for creating the anaconda install images.

%prep
%setup -q
%patch0 -p1 -b .mo8
#%%patch1 -p1 -b .no_efi
%patch2 -p1 -b .selinux
%patch3 -p1 -b .text-mode

%build

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

mkdir -p %{buildroot}%{_mandir}/man1/
mv %{buildroot}/man1/* %{buildroot}%{_mandir}/man1/
rm -rf %{buildroot}/man1/

%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS README.livemedia-creator
%{python_sitelib}/pylorax
%{python_sitelib}/*.egg-info
%{_sbindir}/lorax
%{_sbindir}/mkefiboot
%{_sbindir}/livemedia-creator
%dir %{_sysconfdir}/lorax
%config(noreplace) %{_sysconfdir}/lorax/lorax.conf
%dir %{_datadir}/lorax
%{_datadir}/lorax/*
%{_mandir}/man1/livemedia-creator.1.*
%{_mandir}/man1/lorax.1.*


%changelog
* Fri Jun 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (19.6.28-1m)
- update 19.6.28

* Sat Jun 14 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (19.6.27-2m)
- no remove readline-lib

* Fri Jun 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (19.6.27-1m)
- update 19.6.27

* Wed May 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (19.5-1m)
- update 19.5

* Tue May 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.31-4m)
- add tex-mode patch

* Mon May 26 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.31-3m)
- update mo8 patch
- add grub-text patch

* Tue May 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.31-2m)
- no EFI patch

* Tue May 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18.31-1m)
- update to 18.31

* Fri Dec 14 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (18.22-1m)
- update to 18.22
- build against syslinux-5.00-1m

* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (18.17-1m)
- update 18.17

* Sun Mar 18 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (16.4.7-4m)
- update mo8 patch
-- add libsoup

* Sun Mar  4 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (16.4.7-3m)
- update mo8 patch
-- add ntfsprogs

* Thu Nov  3 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (16.4.7-2m)
- no use /boot/efi/EFI/momonga

* Wed Oct 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4.7-1m)
- update 16.4.7

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4.6-1m)
- update 16.4.6

* Thu Oct  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4.5-1m)
- update 16.4.5

* Wed Sep 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4.4-1m)
- update 16.4.4

* Tue Sep 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4.2-4m)
- update mo8 patch
-- add pygobject228

* Mon Sep 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4.2-3m)
- update mo8 patch
-- add libcanberra 

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4.2-2m)
- update mo8 patch

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.4.2-1m)
- update 16.4.2

* Mon Aug  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-5m)
- use /boot/efi/EFI/momonga

* Sat Jun 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-4m)
- update lorax-0.4.4-mo8.patch
-- support i686 arch
-- no remove yum-cli

* Fri Jun 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-3m)
- no remove gdk-pixbuf files...

* Tue Jun  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-2m)
- no remove libxcb files...

* Mon May 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
* (0.4.4-1m)
- Initial Commit Momonga Linux

* Mon May 02 2011 Martin Gracik <mgracik@redhat.com> 0.4.3-1
- Disable rsyslogd rate limiting on imuxsock.
- Add the udf module to the image.

* Tue Apr 19 2011 Martin Gracik <mgracik@redhat.com> 0.4.3-1
- bits is an integer and replace needs arguments to be strings (#697542)
- Use arch macros in the lorax.spec
- use reqs not regs for files to backup (dgilmore)
- Reflect changes made in ntfs-3g and ntfsprogs packages (#696706)
- getkeymaps resides in /usr/libexec/anaconda
- workdir is a local variable, not a class attribute
- Add sparcv9 to arch map
- Change the location of *.b files on sparc

* Wed Apr 13 2011 Martin Gracik <mgracik@redhat.com> 0.4.2-1
- Do not remove shutdown from sbin
- Change BuildRequires to python2-devel
- Remove pungi patch
- Remove pseudo code

* Wed Apr 13 2011 Martin Gracik <mgracik@redhat.com> 0.4.1-1
- Provide shutdown on s390x (#694518)
- Fix arch specific requires in spec file
- Add s390 modules and do some cleanup of the template
- Generate ssh keys on s390
- Don't remove tr, needed for s390
- Do not check if we have all commands
- Change location of addrsize and mk-s390-cdboot
- Shutdown is in another location
- Do not skip broken packages
- Don't install network-manager-netbook
- Wait for subprocess to finish
- Have to call os.makedirs
- images dir already exists, we just need to set it
- The biarch is a function not an attribute
- Create images directory in outputtree
- Create efibootdir if doing efi images
- Get rid of create_gconf().
- Replace variables in yaboot.conf
- Add sparc specific packages
- Skip keymap creation on s390
- Copy shutdown and linuxrc.s390 on s390
- Add packages for s390
- Add support for sparc
- Use factory to get the image classes
- treeinfo has to be addressed as self.treeinfo
- Add support for s390
- Add the xen section to treeinfo on x86_64
- Fix magic and mapping paths
- Fix passing of prepboot and macboot arguments
- Small ppc fixes
- Check if the file we want to remove exists
- Install x86 specific packages only on x86
- Change the location of zImage.lds
- Added ppc specific packages
- memtest and efika.forth are in /boot
- Add support for ppc
- Minor sparc pseudo code changes
- Added sparc pseudo code (dgilmore)
- Added s390 and x86 pseudo code
- Added ppc pseudo code
- Print a message when no arguments given (#684463)
- Mako template returns unicode strings (#681003)
- The check option in options causes ValueError
- Disable all ctrl-alt-arrow metacity shortcuts.
- Use xz when compressing the initrd

* Mon Mar 21 2011 Martin Gracik <mgracik@redhat.com> 0.3.2-1
- gconf/metacity: have only one workspace. (#683548)
- Do not remove libassuan. (#684742)
- Add yum-langpacks yum plugin to anaconda environment (notting) (#687866)

* Tue Mar 15 2011 Martin Gracik <mgracik@redhat.com> 0.3.1-1
- Add the images-xen section to treeinfo on x86_64
- Add /sbin to $PATH (for the tty2 terminal)
- Create /var/run/dbus directory in installtree
- Add mkdir support to template
- gpart is present only on i386 arch (#672611)
- util-linux-ng changed to util-linux

* Mon Jan 24 2011 Martin Gracik <mgracik@redhat.com> 0.3-1
- Don't remove libmount package
- Don't create mtab symlink, already exists
- Exit with error if we have no lang-table
- Fix file logging
- Overwrite the /etc/shadow file
- Use [images-xen] section for PAE and xen kernels

* Fri Jan 14 2011 Martin Gracik <mgracik@redhat.com> 0.2-2
- Fix the gnome themes
- Add biosdevname package
- Edit .bash_history file
- Add the initrd and kernel lines to .treeinfo
- Don't remove the gamin package from installtree

* Wed Dec 01 2010 Martin Gracik <mgracik@redhat.com> 0.1-1
- First packaging of the new lorax tool.
