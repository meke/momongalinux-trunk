%global momorel 12

Name: cachecc1
Version: 0.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Languages
Source0: http://dl.sourceforge.net/sourceforge/cachecc1/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: http://www.clovery.jp/dist/debian/packages/cachecc1/cachecc1_0.3-2.diff.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary: cachecc1 - a gcc cache
Url: http://cachecc1.sourceforge.net/

%description
cachecc1 is a gcc cache.
It can be compared with the well known ccache. It has some unique features:
* it uses a LD_PRELOADed shared object to catch invocations to cc1, cc1plus
  and as.
* consequently, it transparently supports all build methods.
* it can cache gcc bootstraps.
* it can be combined with distcc to transparently distribute compilations.

%prep
%setup -q
%patch0 -p1

%build
%ifarch %{ix86}
# gcc-4.6.1-0.20110422.1m.mo7 has problem with -fstack-protector
RPM_OPT_FLAGS="`echo $RPM_OPT_FLAGS | sed 's,-fstack-protector,,g'`"
%endif
case "`gcc -dumpversion`" in
4.6.*)
	# cachecc1.spec-0.3 requires this
	CFLAGS="$RPM_OPT_FLAGS  -Wno-unused-but-set-variable"
	CXXFLAGS="$RPM_OPT_FLAGS  -Wno-unused-but-set-variable"
	;;
esac
export CFLAGS CXXFLAGS
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_libexecdir}
install -m 755 bin/cachecc1.so %{buildroot}%{_libdir}/cachecc1.so
install -m 755 bin/distccwrap %{buildroot}%{_libexecdir}/distccwrap

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_libdir}/cachecc1.so
%{_libexecdir}/distccwrap
%doc COPYING README.*

%changelog
* Mon Apr 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-12m)
- fix #348; temporary disable -fstack-protector on x86

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-11m)
- rebuild for new GCC 4.6

* Tue Feb 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-10m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-5m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-3m)
- %%NoSource -> NoSource

* Tue Jan 03 2006 TABUCHI Takaaki <tab@momonga-liux.org>
- (0.3-2m)
- add debian 0.3-2.diff

* Thu Nov 11 2004 TAKAHASHI Tamotsu <tamo>
- (0.3-1m)
- cachecc1 builds gcc much faster than ccache
