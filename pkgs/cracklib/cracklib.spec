%global momorel 1

# Reflects the values hard-coded in various Makefile.am's in the source tree.
%global dictdir %{_datadir}/cracklib
%global dictpath %{dictdir}/pw_dict

Summary: A password-checking library
Name: cracklib
Version: 2.8.18
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/cracklib/cracklib-%{version}.tar.gz 
NoSource: 0

# Retrieved at 20091201191719Z.
Source1: http://iweb.dl.sourceforge.net/project/cracklib/cracklib-words/2008-05-07/cracklib-words-20080507.gz

Source10: http://ftp.cerias.purdue.edu/pub/dict/wordlists/computer/Domains.gz
Source11: http://ftp.cerias.purdue.edu/pub/dict/wordlists/computer/Dosref.gz
Source12: http://ftp.cerias.purdue.edu/pub/dict/wordlists/computer/Ftpsites.gz
Source13: http://ftp.cerias.purdue.edu/pub/dict/wordlists/computer/Jargon.gz
Source14: http://ftp.cerias.purdue.edu/pub/dict/wordlists/computer/common-passwords.txt.gz
Source15: http://ftp.cerias.purdue.edu/pub/dict/wordlists/computer/etc-hosts.gz
Source16: http://ftp.cerias.purdue.edu/pub/dict/wordlists/movieTV/Movies.gz
Source17: http://ftp.cerias.purdue.edu/pub/dict/wordlists/movieTV/Python.gz
Source18: http://ftp.cerias.purdue.edu/pub/dict/wordlists/movieTV/Trek.gz
Source19: http://ftp.cerias.purdue.edu/pub/dict/wordlists/literature/LCarrol.gz
Source20: http://ftp.cerias.purdue.edu/pub/dict/wordlists/literature/Paradise.Lost.gz
Source21: http://ftp.cerias.purdue.edu/pub/dict/wordlists/literature/cartoon.gz
Source22: http://ftp.cerias.purdue.edu/pub/dict/wordlists/literature/myths-legends.gz
Source23: http://ftp.cerias.purdue.edu/pub/dict/wordlists/literature/sf.gz
Source24: http://ftp.cerias.purdue.edu/pub/dict/wordlists/literature/shakespeare.gz
Source25: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/ASSurnames.gz
Source26: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/Congress.gz
Source27: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/Family-Names.gz
Source28: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/Given-Names.gz
Source29: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/famous.gz
Source30: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/fast-names.gz
Source31: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/female-names.gz
Source32: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/male-names.gz
Source33: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/names.french.gz
Source34: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/names.hp.gz
Source35: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/other-names.gz
Source36: http://ftp.cerias.purdue.edu/pub/dict/wordlists/names/surnames.finnish.gz

# No upstream source for this; it came in as a bugzilla attachment.
Source37: pass_file.gz
# https://bugzilla.redhat.com/show_bug.cgi?id=557592
# https://bugzilla.redhat.com/attachment.cgi?id=386022
Source38: ry-threshold10.txt
Patch1: cracklib-2.8.15-inttypes.patch
Patch2: cracklib-2.8.12-gettext.patch
Patch3: cracklib-2.8.15-init.patch
URL: http://sourceforge.net/projects/cracklib/
License: LGPLv2+
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= 2.7, words, autoconf, automake, gettext, libtool
BuildRequires: gettext-devel, git
Conflicts: cracklib-dicts < 2.8
# The cracklib-format script calls gzip, but without a specific path.
Requires: gzip

%description
CrackLib tests passwords to determine whether they match certain
security-oriented characteristics, with the purpose of stopping users
from choosing passwords that are easy to guess. CrackLib performs
several tests on passwords: it tries to generate words from a username
and gecos entry and checks those words against the password; it checks
for simplistic patterns in passwords; and it checks for the password
in a dictionary.

CrackLib is actually a library containing a particular C function
which is used to check the password, as well as other C
functions. CrackLib is not a replacement for a passwd program; it must
be used in conjunction with an existing passwd program.

Install the cracklib package if you need a program to check users'
passwords to see if they are at least minimally secure. If you install
CrackLib, you will also want to install the cracklib-dicts package.

%package devel
Summary: Development files needed for building applications which use cracklib
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The cracklib-devel package contains the header files and libraries needed
for compiling applications which use cracklib.

%package python
Summary: Python bindings for applications which use cracklib
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description python
The cracklib-python package contains a module which permits applications
written in the Python programming language to use cracklib.

%package dicts
Summary: The standard CrackLib dictionaries
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
BuildRequires: words >= 2-13

%description dicts
The cracklib-dicts package includes the CrackLib dictionaries.
CrackLib will need to use the dictionary appropriate to your system,
which is normally put in /usr/share/dict/words. Cracklib-dicts also
contains the utilities necessary for the creation of new dictionaries.

If you are installing CrackLib, you should also install cracklib-dicts.

%prep
%setup -q
cp lib/packer.h lib/packer.h.in
%patch1 -p1 -b .inttypes
%patch2 -p1 -b .gettext
%patch3 -p1 -b .init
autoreconf -f -i
mkdir cracklib-dicts
for dict in %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14} \
            %{SOURCE15} %{SOURCE16} %{SOURCE17} %{SOURCE18} %{SOURCE19} \
            %{SOURCE20} %{SOURCE21} %{SOURCE22} %{SOURCE23} %{SOURCE24} \
            %{SOURCE25} %{SOURCE26} %{SOURCE27} %{SOURCE28} %{SOURCE29} \
            %{SOURCE30} %{SOURCE31} %{SOURCE32} %{SOURCE33} %{SOURCE34} \
            %{SOURCE35} %{SOURCE36} %{SOURCE37} %{SOURCE38} %{SOURCE1}
do
        cp -fv ${dict} cracklib-dicts/
done

chmod +x util/cracklib-format

%build
%configure --with-pic --with-python --with-default-dict=%{dictpath} --disable-static
make

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -c -p" -C python
./util/cracklib-format cracklib-dicts/* | \
./util/cracklib-packer $RPM_BUILD_ROOT/%{dictpath}
./util/cracklib-format $RPM_BUILD_ROOT/%{dictdir}/cracklib-small | \
./util/cracklib-packer $RPM_BUILD_ROOT/%{dictdir}/cracklib-small
rm -f $RPM_BUILD_ROOT/%{dictdir}/cracklib-small
sed s,/usr/lib/cracklib_dict,%{dictpath},g lib/crack.h > $RPM_BUILD_ROOT/%{_includedir}/crack.h
ln -s cracklib-format $RPM_BUILD_ROOT/%{_sbindir}/mkdict
ln -s cracklib-packer $RPM_BUILD_ROOT/%{_sbindir}/packer
touch $RPM_BUILD_ROOT/top

toprelpath=..
touch $RPM_BUILD_ROOT/top
while ! test -f $RPM_BUILD_ROOT/%{_libdir}/$toprelpath/top ; do
	toprelpath=../$toprelpath
done
rm -f $RPM_BUILD_ROOT/top
if test %{dictpath} != %{_libdir}/cracklib_dict ; then
ln -s $toprelpath%{dictpath}.hwm $RPM_BUILD_ROOT/%{_libdir}/cracklib_dict.hwm
ln -s $toprelpath%{dictpath}.pwd $RPM_BUILD_ROOT/%{_libdir}/cracklib_dict.pwd
ln -s $toprelpath%{dictpath}.pwi $RPM_BUILD_ROOT/%{_libdir}/cracklib_dict.pwi
fi
rm -f $RPM_BUILD_ROOT/%{_libdir}/python*/site-packages/_cracklibmodule.*a
rm -f $RPM_BUILD_ROOT/%{_libdir}/libcrack.la

%find_lang %{name}

%check
# We want to check that the new library is able to open the new dictionaries,
# using the new python module.
LD_LIBRARY_PATH=$RPM_BUILD_ROOT/%{_libdir} %{__python} 2>&1 << EOF
import string, sys
# Prepend buildroot-specific variations of the python path to the python path.
syspath2=[]
for element in sys.path:
	syspath2.append("$RPM_BUILD_ROOT/" + element)
syspath2.reverse()
for element in syspath2:
	sys.path.insert(0,element)
# Now actually do the test.  If we get a different result, or throw an
# exception, the script will end with the error.
import cracklib
try:
	s = cracklib.FascistCheck("cracklib", "$RPM_BUILD_ROOT/%{dictpath}")
except ValueError, message:
	expected = "it is based on a dictionary word"
	if message != expected:
		print "Got unexpected result \"%s\"," % messgae,
		print "instead of expected value of \"%s\"." % expected
		sys.exit(1)
	print "Got expected result \"%s\"," % message
	sys.exit(0)
finally:
	sys.exit(0)
EOF

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%triggerpostun -p /sbin/ldconfig -- cracklib < 2.7-24

%files -f %{name}.lang
%defattr(-,root,root)
%doc README README-WORDS NEWS README-LICENSE AUTHORS COPYING.LIB
%{_libdir}/libcrack.so.*
%dir %{_datadir}/cracklib
%{_datadir}/cracklib/cracklib.magic
%{_sbindir}/*cracklib*

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/libcrack.so

%files dicts
%defattr(-,root,root)
%{_datadir}/cracklib/pw_dict.*
%{_datadir}/cracklib/cracklib-small.*
%{_libdir}/cracklib_dict.*
%{_sbindir}/mkdict
%{_sbindir}/packer

%files python
%defattr(-,root,root)
%{_libdir}/python*/site-packages/_cracklibmodule.so
%{_libdir}/../lib/python*/site-packages/*.py*

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.18-1m)
- update 2.8.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.16-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.16-4m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (2.8.16-3m)
- add git to BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.16-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.16-1m)
- sync with Rawhide (2.8.16-3)
- add REMOVE.PLEASE to pass %%check
-- if cracklib < 2.8.14 is installed, then an error
-- "_cracklibmodule.so: undefined symbol: GetDefaultCracklibDict"
-- may occur. this symbol was introduced in 2.8.14.

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.13-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Feb 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.13-1m)
- update 2.8.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.9-6m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.5-2m)
- rebuild against python-2.6.1-2m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.9-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.9-3m)
- %%NoSource -> NoSource

* Wed Mar 28 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.8.9-2m)
- roll back to 2.8.9

* Thu Mar 22 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.8.10-1m)
- up to 2.8.10

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.9-1m)
- update 2.8.9

* Sun Jun 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.6-2m)
- revise %%files

* Thu May 11 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.6-1m)
- version up

* Wed Jan 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.3-2m)
- add BuildRequires: gcc-c++ for cpp check fail

* Mon Aug  1 2005 Yohsuke Ooi  <meke@momonga-linux.org>
- (2.8.3-1m)
- sync FC-devel

- revised docdir permission
* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.7-20m)
- revised docdir permission

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (2.7-19m)
- enable x86_64.

* Fri May 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.7-18m)
- change URI

* Thu Oct  9 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.7-17m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Mar 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7-16m)
- change Source URI

* Sat Oct 26 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.7-15m)
- sync with redhat-8.0(cracklib-2.7-18)
- remove -fstack-protector(because of build failure...)

* Wed May 22 2002 Masaru Sato <masachan@kondara.org>
- (2.7-14k)
- Fix Makefile, install /usr/include/packer.h

* Fri Jan 18 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.7-12k)
- obey fhs
- add BuildPreReq

* Wed Apr  4 2001 Shingo Akagaki <dora@Kondara.org>
- make static lib
- make devel package

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Dec 10 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Apr 06 1999 Preston Brown <pbrown@redhat.com>
- strip binaries

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Wed Jan 06 1999 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Sat May 09 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Mar 10 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.7
- build shared libraries

* Mon Nov 03 1997 Donnie Barnes <djb@redhat.com>
- added -fPIC

* Mon Oct 13 1997 Donnie Barnes <djb@redhat.com>
- basic spec file cleanups

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc

