%global         momorel 3
%global         srcver 3.09

Name:           perl-CGI-FormBuilder
Version:        %{srcver}00
Release:        %{momorel}m%{?dist}
Summary:        Easily generate and process stateful forms
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/CGI-FormBuilder/
Source0:        http://www.cpan.org/authors/id/N/NW/NWIGER/CGI-FormBuilder-%{srcver}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-CGI
BuildRequires:  perl-CGI-SSI
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl-CGI-SSI
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
If this is your first time using FormBuilder, you should check out the
website for tutorials and examples at http://formbuilder.org.

%prep
%setup -q -n CGI-FormBuilder-%{srcver}

# skip failing tests due to hash randomization
# see https://rt.cpan.org/Public/Bug/Display.html?id=81650
rm -f t/1a-generate.t t/2c-template-tt2.t t/2d-template-fast.t

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/CGI/FormBuilder.pm
%{perl_vendorlib}/CGI/FormBuilder.pod
%{perl_vendorlib}/CGI/FormBuilder
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0900-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0900-2m)
- rebuild against perl-5.18.2

* Thu Jan  4 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0900-1m)
- update to 3.09

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0800-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0800-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0800-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0800-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0800-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0800-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0800-2m)
- rebuild against perl-5.14.2

* Mon Oct 03 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0800-1m)
- update to 3.08

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0501-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0501-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0501-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0501-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0501-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0501-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0501-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0501-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0501-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0501-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0501-3m)
- rebuild against rpm-4.6

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0501-2m)
- change source URI and use NoSource
- change BuildRequries from perl module name to package name

* Fri Jul 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0501-1m)
- import from Fedora to Momonga for ikiwiki

* Wed Mar  5 2008 Tom "spot" Callway <tcallawa@redhat.com> - 3.0501-5
- rebuild for new perl

* Wed Jun 20 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 3.0501-4
- Trim the description to something reasonable.
- Delete odd .orig file

* Fri Jun 15 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 3.0501-3
- BR perl(CGI::FastTemplate)

* Fri Jun 15 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 3.0501-2
- Don't BR perl or perl-devel, instead BR specific Perl modules needed to build.
- Proper license tag

* Fri Jun  1 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 3.0501-1
- First version for Fedora

