%global		momorel 1
Name:		log4c
Version:	1.2.1
Release:	%{momorel}m%{?dist}
Summary:	Library for logging application messages

Group:		System Environment/Libraries
License:	LGPLv2+
URL:		http://log4c.sourceforge.net/
Source0:	http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:		507427-Fix-M4-file.patch
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires:	expat-devel

%description
Log4c is a C language library for flexible logging to files, syslog and other 
destinations. It is modeled after the Log for Java library (log4j), 
staying as close to their API as is reasonable.

%package devel
Summary:	Header files, libraries and development documentation for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	automake

%description devel
This package contains the header files, static libraries and development
documentation for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.

%prep
%setup -q 
%patch0 -p1

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
# example config file below shouldn't live in /etc/
mkdir -p %{buildroot}/usr/share/doc/%{name}-%{version}
mv %{buildroot}/etc/log4crc.sample %{buildroot}/usr/share/doc/%{name}-%{version}/
rm %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README log4crc.sample
%{_libdir}/liblog4c.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/liblog4c.so
%{_bindir}/*
%{_includedir}/*
%{_datadir}/aclocal/log4c.m4

%changelog
* Thu Oct 18 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.2.1-1m)
- Initial import to Momonga from fc17

* Tue Jan 13 2009 Alex Hudson <fedora@alexhudson.com> - 1.2.1-1
- Initial packaging attempt
