%global momorel 1

%global ruby_sitelibdir  %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')
%global ruby_sitearchdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')

%global rbname rpm
%global reldir 66904

Summary: An interface to access RPM database for Ruby
Name: ruby18-%{rbname}
Version: 1.3.1
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPLv2+
URL: http://rubyforge.org/projects/ruby-rpm/
#Source0: http://rubyforge.org/frs/download.php/%{reldir}/ruby-rpm-%{version}.tgz 
#NoSource: 0
Source0: ruby-rpm-%{version}.tar.xz
Patch100: ruby-rpm-1.2.3-memleak.patch
Patch101: ruby-rpm-1.2.3-specfile.patch
Patch102: ruby-rpm-1.3.0-db48.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18-devel
BuildRequires: rpm-devel
BuildRequires: popt >= 1.9.1-1m
BuildRequires: db4-devel

%description
Ruby-rpm is an interface to access RPM database for Ruby.

%prep
%setup -q -n ruby-rpm-%{version}
%patch100 -p1 -b .memleak
%patch101 -p1 -b .specfile
%patch102 -p1 -b .db48

%build
pushd ext/rpm
ruby18 extconf.rb

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
pushd ext/rpm
make install DESTDIR=%{buildroot}
popd

mkdir -p %{buildroot}%{ruby_sitearchdir}
mkdir -p %{buildroot}%{ruby_sitelibdir}
mkdir -p %{buildroot}%{ruby_sitelibdir}/rpm

%{__install} -m 0644 ext/rpm/ruby-rpm.h %{buildroot}%{ruby_sitearchdir}
%{__install} -m 0644 lib/rpm.rb %{buildroot}%{ruby_sitelibdir}
%{__install} -m 0644 lib/rpm/version.rb %{buildroot}%{ruby_sitelibdir}/rpm/

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.rdoc COPYING CHANGELOG.rdoc doc
%{ruby_sitelibdir}/rpm.rb
%{ruby_sitelibdir}/rpm/version.rb
%{ruby_sitearchdir}/rpm.so
%{ruby_sitearchdir}/ruby-rpm.h

%changelog
* Mon Mar 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-4m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-3m)
- rename to ruby18-rpm

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-2m)
- rebuild against rpm-4.8.0

* Tue Feb 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4
- drop rpm64, lib64 and ia64 patches

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-20m)
- rebuild against db-4.8.26

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-19m)
- support db-4.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-17m)
- BuildRequires: db4-devel >= 4.7.25

* Fri Apr 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-16m)
- rebuild against rpm-4.7
- modify rpm46 patch (Patch200) for db4-4.7.25

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-15m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-14m)
- use compat-db45 to harmonize db4 with rpm46

* Sun Jan 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-13m)
- apply Patch201 which adds RPM::Spec#specfile method

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-12m)
- work with rpm44 and rpm46
- apply rpm46 patch
-- this patch includes Patch4 which modifies db4 check codes in extconf.rb

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-11m)
- rebuild against db4-4.7.25-1m

* Mon Oct 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-10m)
- support db-4.7

* Sat Jun 14 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-9m)
- add patch that fixes memory leak

* Wed Apr 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-8m)
- add db-4.6 patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-6m)
- %%NoSource -> NoSource

* Thu Nov  8 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.3-5m)
- modify reldir, md5sum

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.3-4m)
- rebuild against db4-4.6.21

* Mon Jul 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-3m)
- enable ruby-rpm-1.2.0-extconf-db45.patch

* Mon Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-2m)
- rebuild against ruby-1.8.6-4m

* Sun Jun 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-1m)
- update 1.2.3

* Mon Mar 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-1m)
- merge from TSUPPA4RI-baranch
- update 1.2.2

* Wed Feb  7 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-1m)
- update 1.2.1
-- remove patches 0,1,2,3,5,6,8,9 which had been merged into upstream
- remove patch4 which seem to cause problem in the latest rpm

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-20m)
- Patch10: ruby-rpm-1.2.0-extconf-db45.patch
- build against db4-4.5.20-1m

* Fri Jul 21 2006 zunda <zunda at freeshell.org>
- (kossori)
- added BuildRequires: db4-devel instead of db4
- added Requires: db4

* Tue May 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-19m)
- rebuild against rpm-4.4

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-18m)
- rebuild against db4.3

* Mon Oct 17 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-17m)
- enable ia64.

* Sun Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.0-16m)
- enable x86_64.

* Mon Nov  1 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (1.2.0-15m)
- add a patch to fix RPM::Transaction#delete (patch6).
- add a patch to fix doc/refm.rd.ja (patch7).

* Sun Oct  3 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (1.2.0-14m)
- add a patch to add RPM::Package#sprintf (patch5).

* Sat Oct  2 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (1.2.0-13m)
- add a patch to fix RPM::Transaction#commit (pacth4).

* Thu Sep 23 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (1.2.0-12m)
- add a patch (patch3).
- RPM::Transaction#delete (and callback function) fixed.
- RPM::Package#inspect RPM::Version#inspect added.

* Thu Sep 23 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (1.2.0-11m)
- add a patch to fix SEGV problem (patch2).
- fix patch1.

* Mon Sep 13 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (1.2.0-10m)
- RPM.expand RPM::Spec#expand_macros added (patch1).

* Sat Aug  7 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.0-9m)
- rebuild against rpm-4.3

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.0-8m)
- rebuild against ruby-1.8.2

* Thu Jul  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.0-7m)
- remove debug messages and merge patches

* Fri Jul  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-6m)
- add patch for db4.2

* Mon Jun 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.0-5m)
- rebuild against db4.2

* Fri Jun 25 2004 TAKAHASHI Tamotsu <tamo>
- (1.2.0-4m)
- apply getenv patch again. see the (1.1.12-2m) entry.

* Sat Jun  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.0-3m)
- just a little better

* Thu Jun  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.0-2m)
- just a little better

* Sun Mar 14 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-1m)
- for rpm 4.2.1.

* Tue Jan 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.12-2m)
- add patch to fix SEGV if TMP or TEMP or TMPDIR environment variable exists. [bug:#19]

* Sun Jan 11 2004 Kenta MURATA <muraken2@nifty.com>
- (1.1.12-1m)
- version up.

* Sat Nov 01 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.11-1m)
- version up.

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.10-3m)
- merge from ruby-1_8-branch.

* Sun Aug 03 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.10-2m)
- rebuild against ruby-1.8.0.

* Sun May  4 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.10-1m)
- version up.

* Tue Mar  4 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.9-1m)
- version up.

* Sat Jan 18 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.8-1m)
- version up.

* Wed Dec 25 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-1m)
- version up.

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.6-1m)
- version up.

* Tue Dec 10 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.5-1m)
- version up.

* Fri Dec  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.4-2m)
- bug fix.

* Fri Dec  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.4-1m)
- version up.

* Thu Dec  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.3-2m)
- bug fix.

* Thu Dec  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.3-1m)
- version up.

* Thu Dec  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.2-1m)
- version up.

* Fri May 31 2002 Kenta MURATA <muraken@kondara.org>
- (1.1.1-2k)
- version up.

* Fri May 31 2002 Kenta MURATA <muraken@kondara.org>
- (1.1.0-2k)
- version up.

* Tue May  7 2002 Kenta MURATA <muraken@kondara.org>
- (1.0.1-2k)
- version up.

* Fri May  3 2002 Kenta MURATA <muraken@kondara.org>
- (1.0.0-2k)
- version up.

* Wed May  1 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.13-2k)
- version up.

* Mon Apr 29 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.12-2k)
- version up.

* Sun Apr 28 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.11-2k)
- version up.

* Wed Apr 24 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.10-2k)
- version up.

* Sun Apr 21 2002 Toru Hoshina <t@kondara.org>
- (0.3.9-6k)
- i586tega... copytega... 

* Sun Apr 21 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.9-4k)
- applyed patch basenames and dirindexes.

* Sun Apr 21 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.9-2k)
- version up.

* Sun Apr 21 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.8-4k)
- ukekeke.

* Fri Apr 19 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.8-2k)
- version up.
- devel merge to main.

* Fri Apr 19 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.7-2k)
- version up.
- append package devel for neomph. fufufu.

* Fri Apr 19 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.6-2k)
- version up.

* Fri Apr 19 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.5-4k)
- defattr

* Thu Apr 18 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.5-2k)
- version up.

* Thu Apr 18 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.4-2k)
- version up.

* Thu Apr 18 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.3-4k)
- apply patch to DB#each_match

* Thu Apr 18 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.3-2k)
- version up.

* Wed Apr 17 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.2-2k)
- version up.

* Wed Apr 17 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.1-6k)
- apply patch version_parse.

* Wed Apr 17 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.1-4k)
- apply patch Init_hoge.

* Wed Apr 17 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.1-2k)
- first release.
