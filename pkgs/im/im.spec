%global momorel 18

Summary: Internet Message
Name: im
Version: 150
Release: %{momorel}m%{?dist}
Group: Applications/Internet
Source0: http://tats.hauN.org/im/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://tats.hauN.org/im/

# specopt
%{?include_specopt}
%{?!enable_rpop:      %global enable_rpop   0}

BuildRequires: perl >= 5.16.0
BuildArchitectures: noarch
License: Modified BSD
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package provides a series of user interface commands (im*
commands) and backend Perl5 libraries to integrate Email and NetNews
user interface. They are designed to be used both from Mew and on
command line.

The folder style for Mew is exactly the same as that of MH. So, you
can replace MH with this package without any migration
works. Moreover, you are able to operate your messages both by IM and
MH with consistent manner.

%prep
%setup -q

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sysconfdir}/im
mkdir -p %{buildroot}%{perl_vendorlib}/IM

%build
%{configure} --prefix=%{_prefix} \
	--with-perldir=%{perl_vendorlib} \
	--libdir=%{_sysconfdir} \
	--with-ssh=%{_bindir}/ssh \
%if %{enable_rpop}
	--enable-rpop \
%endif
	--with-hostname=localdomain

cd cnf.im
sed -e 's/FromDomain=.*#/FromDomain=#/' \
    -e 's/ToDomain=.*#/ToDomain=#/' \
    -e 's,/usr/local/lib/,/etc/,' < SiteConfig > SiteConfig.$$
mv SiteConfig.$$ SiteConfig
cd -

sed -e 's/-o root -m 4755//' Makefile > Makefile.$$
mv Makefile.$$ Makefile

%install
rm -rf %{buildroot}

make prefix=%{buildroot}%{_prefix} \
     bindir=%{buildroot}%{_bindir} \
     libdir=%{buildroot}%{_sysconfdir} \
     perldir=%{buildroot}%{perl_vendorlib} \
%if %{enable_rpop}
     RPOP="" \
%endif
     install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/imali
%{_bindir}/imcat
%{_bindir}/imcd
%{_bindir}/imclean
%{_bindir}/imgrep
%if %{enable_rpop}
%attr(4755,root,root) %{_bindir}/imget
%else
%{_bindir}/imget
%endif
%{_bindir}/imhist
%{_bindir}/imhsync
%{_bindir}/imjoin
%{_bindir}/imls
%{_bindir}/immv
%{_bindir}/immknmz
%{_bindir}/impack
%{_bindir}/impath
%{_bindir}/imput
%{_bindir}/impwagent
%{_bindir}/imrm
%{_bindir}/imsetup
%{_bindir}/imsort
%{_bindir}/imstore
%{_bindir}/imtar

%dir %{_sysconfdir}/im
%config %{_sysconfdir}/im/SiteConfig

%doc man/
%doc 00changes 00copyright 00copyright.jis 
%doc 00perl 00readme 00usage cnf.im dot.im

%dir %{perl_vendorlib}/IM/
%{perl_vendorlib}/IM/*.pm

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (150-18m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (150-17m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (150-16m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (150-15m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (150-14m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (150-13m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (150-12m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (150-11m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (150-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (150-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (150-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (150-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (150-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (150-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (150-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (150-3m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARRITA Koichi <pulsar@momonga-linux.org>
- (150-2m)
- rebuild against perl-5.12.0
- remove Requires: perl-suidperl

* Mon Feb 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (150-1m)
- update to 150
- build with --with-hostname=localdomain

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (149-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (149-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (149-3m)
- rebuild against gcc43

* Mon Jun  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (149-2m)
- use %{perl_vendorlib}

* Mon Jun  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (149-1m)
- update to 149
- [SECURITY] CVE-2007-1558
  IM/Pop.pm (pop_open): Don't allow non-ASCII characters for APOP
  timestamp.

* Wed Jan 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (148-1m)
- update to 148

* Mon Mar 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (146-4m)
- change Source0 URI

* Mon Dec  1 2003 Tetsuo Sakai <tetsuos@zephyr.dti.ne.jp>
- (146-3m)
- change default: disable RPOP option, not setuid imget
- use specopt
- bug report #9

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (146-2m)
- rebuild against perl-5.8.2

* Mon Nov 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (146-1m)
- update to 146
- http://www.mew.org/ml/mew-dist-3.1/msg01723.html
- reported by TetsuO.S [Momonga-devel.ja:02270]

* Sun Nov 02 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (145-3m)
- rebuild against perl-5.8.1

* Fri Oct 17 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (145-2m)
- change License: to Modified BSD

* Mon Jun 23 2003 HOSONO Hidetomo <h12o@h12o.org>
- (145-1m)

* Sat Mar 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (144-2m)
- change imget attr 4555 to 4755

* Fri Mar 21 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (144-1m)
- update to 144
- use rpm macros

* Thu Dec 19 2002 HOSONO Hidetomo <h12o@h12o.org>
- (142-1m)
- delete a patch http://tats.iris.ne.jp/im/im-141+tats20021028.diff
- set RPOP= in the "make install" phase.
- change my mail address on the previous changelog entry,
  "h@h12o.org" to "h12o@h12o.org" :D

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (141-12m)
- rebuild against perl-5.8.0

* Tue Oct 29 2002 HOSONO Hidetomo <h12o@h12o.org>
- (141-11m)
- add a patch http://tats.iris.ne.jp/im/im-141+tats20021028.diff
  (from mew-dist@mew.org and mew-int@mew.org)
- delete a patch http://tats.iris.ne.jp/im/im-141+tats20020413.diff
- change the source URL
- not include Source0
- move a buildroot cleaning procedure to the install phase
- adapt buildroot cleaning procedures for
  http://www.momonga-linux.org/docs/Specfile-Guidance/ja/tag.html#clean
- correct the changelog spell miss, "Tatsuya Kinoshi" to "Tatsuya Kinoshita"
- let this spec file use macros instead of rpm environment variables
- change the string "/usr/local/lib/" to "/etc/"
  in comments of /etc/im/SiteConfig
- add a definition of "bindir" in the install phase
- change my name on the previous changelog entry,
  "Hidetomo Hosono" to "HOSONO Hidetomo" :D

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (141-10k)
- add patch http://tats.iris.ne.jp/im/im-141+tats20020413.diff
  thanks to Tatsuya Kinoshita from [mew-dist 20773]
  and HOSONO Hidetomo http://h12o.tdiary.net/20020418.html#p04

* Fri Feb 15 2002 Tsutomu Yasuda <tom@kondara.org>
- (141-6k)
- Source0 was included.

* Sun Jul  2 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.6.0 

* Mon Apr 24 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
 - im-141-1

* Mon Feb 28 2000 Takaaki Tabuchi <tab@kondara.org>
 - im-140-1

* Mon Feb 21 2000 Hidetomo Hosono <h@kondara.org>
 - im-139-1

* Tue Feb 8 2000 Hidetomo Hosono <h@kondara.org>
 - im-138-1
 - We don't have to apply local patches any more :-) !

* Tue Feb 8 2000 Hidetomo Hosono <h@kondara.org>
 - im-137-4
 - better fix of Ssh.pm (not Ssh.pm.diff but im-ssh.diff)
   included in the following message...

   To: mew-dist@mew.org, h@kondara.org
   From: Hajimu UMEMOTO <ume@mahoroba.org>
   Subject: [Mew-dist 12289] Re: Ssh remote_port
   Date: Tue, 8 Feb 2000 00:42:30 +0900
   Message-Id: <200002071541.e17FfSX26634@peace.mahoroba.org>
 - fixed hostname as localhost

* Tue Feb 8 2000 Takaaki Tabuchi <tab@kondara.org>
 - im-137-3
 - fix this changelog part.

* Mon Feb 7 2000 Hidetomo Hosono <h@kondara.org>
 - im-137-2
 - bug fix of TcpTransaction.pm
   included the following message...

   To: mew-dist@mew.org
   From: Hajimu UMEMOTO <ume@mahoroba.org>
   Subject: [Mew-dist 12282] Re: Ssh remote_port
   Date: Mon, 7 Feb 2000 17:02:28 +0900
   Message-Id: <200002070759.e177xtR15462@mail.mahoroba.org>

* Thu Feb  3 2000 Takaaki Tabuchi <tab@kondara.org>
 - im-137-1

* Thu Jan 13 2000 Takaaki Tabuchi <tab@kondara.org>
 - im-136-1

* Fri Jan  8 2000 Hidetomo Hosono <h@kondara.org>
 - im-135-2
 - fixed the value of $ENV{PATH} in IM/Ssh.pm ,
   because imget is executed as tainted.

* Thu Jan  6 2000 Hidetomo Hosono <h@kondara.org>
 - im-135-1
 - added TcpTransaction.pm.diff (not TcpTransaction.patch!)
   included the following message...

   From: Hajimu UMEMOTO <ume@mahoroba.org>
   To: mew-dist@mew.org
   Subject: [Mew-dist 12010] Re: IM 135
   Date: Thu, 6 Jan 2000 20:26:11 +0900
   Message-Id: <200001061124.e06BOVY00718@mail.mahoroba.org>

* Wed Jan  5 2000 Hidetomo Hosono <h@kondara.org>
 - im-133-6
 - replaced Scan.pm with another one included the following message...

   To: mew-dist@mew.org
   From: SAITO Tetsuya <saito@Mew.org>
   Subject: [Mew-dist 11986] Y2K patch for im-13[34] (Re:  Re: im-133 y2k bugfix)
   Date: Wed, 5 Jan 2000 04:21:23 +0900
   Message-Id: <20000105042117T.saito@Mew.org>

 - corrected group name
   Applications/Mail -> Applications/Internet

* Sun Jan  2 2000 Hidetomo Hosono <h@kondara.org>
 - im-133-5
 - better bug fix Scan.pm from...

   From: SAITO Tetsuya <saito@Mew.org>
   To: mew-dist@mew.org
   Subject: [Mew-dist 11970] Re: im-133 y2k bugfix (Fw: [Kondara-devel:01032])
   Date: Sun, 2 Jan 2000 21:07:43 +0900
   Message-Id: <20000102210738J.saito@Mew.org>

   have been available. So replaced im-Scan.pm-y2k.patch .

* Sun Jan  2 2000 Hidetomo Hosono <h@kondara.org>
 - im-133-4
 - y2k ;-) bug fix Scan.pm (ad hoc patch :-P)

* Mon Nov 15 1999 Takaaki Tabuchi <tab@kondara.org>
 - im-133-3
 - bug fix TcpTransaction.pm (use TcpTransaction.pm in im-132)

* Sat Oct 31 1999 Takaaki Tabuchi <tab@kondara.org>
 - im-133-2
 - Add option --enable-rpop at %build section.
 - Bug fix %files section. thanks to Toshimasa Oishi <toishi@alles.or.jp>

* Sat Oct 25 1999 Takaaki Tabuchi <tab@kondara.org>
 - im-133-1

* Sat Oct 24 1999 Takaaki Tabuchi <tab@kondara.org>
 - im-132-3
 - merge im-130-1JRPM60. thanks to Hidetomo Hosono <hide@tomo.gr.jp>
 - slight modifications for Kondara MNU/Linux

* Wed Sep 29 1999 Yasushi Karino <kari-p@mc.kcom.ne.jp>
 - im-130-6
 - slight modifications for Vine-1.9
 - Change of my E-mail address.

* Sun Sep 12 1999 Yasushi Karino <kari-p@mc.kcom.ne.jp>
 - im-130-5
 - optional specification of "perldir=" is forgotten
   to "make install" it modified it.
 
* Sun Sep 12 1999 Yasushi Karino <kari-p@mc.kcom.ne.jp>
 - im-130-4
 - Description of the option of ".configre" and "make install"
   were modified.
 
* Sat Sep 11 1999 Yasushi Karino <kari-p@mc.kcom.ne.jp>
 - im-130-3
 - modified spec file.
 
* Mon Sep  6 1999 Yasushi Karino <kari-p@mc.kcom.ne.jp>
 - im-130-2
 - Spelling mistake was modified.
 - im-130-1
 - Only version change for stable release.

* Fri Sep  3 1999 Yasushi Karino <kari-p@mc.kcom.ne.jp>
 - im-127-0.1
 - updated to im-127

* Mon Aug 31 1999 Yasushi Karino <kari-p@mc.kcom.ne.jp>
 - im-126-1
 - first Release
