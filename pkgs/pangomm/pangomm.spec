%global momorel 1

Summary: This is pangomm, a C++ API for Pango. 
Name: pangomm
Version: 2.28.4
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://www.pango.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.28/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: pkgconfig
BuildRequires: glibmm-devel >= 2.22.1
BuildRequires: cairomm-devel >= 1.8.2
BuildRequires: pango-devel >= 1.26.0

%description
This is pangomm, a C++ API for Pango. 

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibmm-devel
Requires: cairomm-devel
Requires: pango-devel

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog NEWS COPYING.tools
%{_libdir}/libpangomm-1.4.so.*
%exclude %{_libdir}/libpangomm-1.4.la

%files devel
%defattr(-, root, root)
%{_libdir}/libpangomm-1.4.so
%{_libdir}/pangomm-1.4
%{_libdir}/pkgconfig/pangomm-1.4.pc
%{_includedir}/pangomm-1.4
%doc %{_datadir}/doc/pangomm-1.4
%{_datadir}/devhelp/books/pangomm-1.4/pangomm-1.4.devhelp2

%changelog
* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.4-1m)
- update to 2.28.4

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.26.2-2m)
- full rebuild for mo7 release

* Tue May  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-2m)
- define __libtoolize

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.1-2m)
- rebuild against rpm-4.6

* Tue Nov 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- initial build
