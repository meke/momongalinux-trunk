%global momorel 1
%define gettext_package gnome-panel-2.0

%define gnome_desktop_version 2.91.6
%define glib2_version 2.25.9
%define gtk2_version 2.11.3
%define libgnome_version 2.13.0
%define libgnomeui_version 2.5.4
%define libbonoboui_version 2.3.0
%define orbit_version 2.4.0
%define libwnck_version 3.4.3
%define gnome_menus_version 3.5.5
%define evolution_data_server_version 3.5.91
%define cairo_version 1.12.2
%define dbus_version 1.6.4
%define dbus_glib_version 0.60
%define gnome_doc_utils_version 0.3.2
%define libgweather_version 3.5.91

%define use_evolution_data_server 1

Summary: GNOME panel
Name: gnome-panel
Version: 3.6.2
Release: %{momorel}m%{?dist}
URL: http://www.gnome.org
#VCS: git:git://git.gnome.org/gnome-panel
Source0: http://download.gnome.org/sources/gnome-panel/3.6/%{name}-%{version}.tar.xz
NoSource: 0

# https://bugzilla.gnome.org/show_bug.cgi?id=646749
Patch0: Use-16px-menu-icons.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=685142
Patch1: gnome-panel-use-gnome-menus.patch

License: GPLv2+ and LGPLv2+ and GFDL
Group: User Interface/Desktops

Requires: gnome-desktop3 >= %{gnome_desktop_version}
Requires: libwnck3 >= %{libwnck_version}
Requires: gnome-menus >= %{gnome_menus_version}
%if %{use_evolution_data_server}
Requires: evolution-data-server >= %{evolution_data_server_version}
%endif
Requires: gnome-session-xsession
Requires: %{name}-libs%{?_isa} = %{version}-%{release}

Requires(post): hicolor-icon-theme
Requires(post): GConf2
Requires(pre): GConf2
Requires(preun): GConf2

BuildRequires: libxml2-python
BuildRequires: intltool
BuildRequires: gettext
BuildRequires: automake
BuildRequires: autoconf
BuildRequires: libtool
BuildRequires: scrollkeeper
BuildRequires: libxslt
BuildRequires: libX11-devel
BuildRequires: libXt-devel
BuildRequires: gnome-desktop3-devel >= %{gnome_desktop_version}
BuildRequires: gtk2-devel >= %{gtk2_version}
BuildRequires: libgnome-devel >= %{libgnome_version}
BuildRequires: libgnomeui-devel >= %{libgnomeui_version}
BuildRequires: libbonoboui-devel >= %{libbonoboui_version}
BuildRequires: libwnck3-devel >= %{libwnck_version}
BuildRequires: dconf-devel >= 0.13.4
BuildRequires: gnome-menus-devel >= %{gnome_menus_version}
BuildRequires: cairo-devel >= %{cairo_version}
BuildRequires: gnome-doc-utils >= %{gnome_doc_utils_version}
BuildRequires: dbus-glib-devel >= %{dbus_glib_version}
BuildRequires: gtk-doc
BuildRequires: pango-devel
BuildRequires: libbonobo-devel
BuildRequires: libXau-devel
BuildRequires: libXrandr-devel
%if %{use_evolution_data_server}
BuildRequires: evolution-data-server-devel >= %{evolution_data_server_version}
BuildRequires: ORBit2-devel >= %{orbit_version}
BuildRequires: dbus-devel >= %{dbus_version}
%endif
BuildRequires: polkit-devel >= 0.92
BuildRequires: libgweather-devel >= %{libgweather_version}
BuildRequires: librsvg2-devel
BuildRequires: NetworkManager-devel
BuildRequires: intltool
BuildRequires: gettext-devel
BuildRequires: libtool
BuildRequires: libcanberra-devel
BuildRequires: desktop-file-utils
BuildRequires: gobject-introspection-devel
BuildRequires: gnome-common

Obsoletes: gdm-user-switch-applet < 1:2.91.6
Conflicts: gnome-power-manager < 2.15.3

Obsoletes: libgail-gnome < 1.20.4

# Obsolete all old applet packages; this is the output of
# repoquery --whatrequires libpanel-applet-2.so.0 gnome-python2-applet
# as of Fedora 15 20110428, as well as ones from Fedora 14.
Obsoletes: deskbar-applet <= 0:2.32.0-5
Obsoletes: deskbar-applet-devel <= 0:2.32.0-5
Obsoletes: file-browser-applet <= 0:0.6.6-2
Obsoletes: glunarclock <= 0:0.34.1-2
Obsoletes: gnome-applet-bubblemon <= 0:2.0.15-2
Obsoletes: gnome-applet-cpufire <= 0:1.6-4
Obsoletes: gnome-applet-globalmenu <= 0:0.7.9-2
Obsoletes: gnome-applet-grandr <= 0:0.4.1-3
Obsoletes: gnome-applet-jalali-calendar <= 0:1.7.1-3
Obsoletes: gnome-applet-music <= 0:2.5.1-6
Obsoletes: gnome-applet-sensors <= 0:2.2.7-5
Obsoletes: gnome-applet-sensors-devel <= 0:2.2.7-5
Obsoletes: gnome-applet-timer <= 0:2.1.4-3
Obsoletes: gnome-applet-window-picker <= 0:0.5.8-3
Obsoletes: gnome-applets <= 1:2.32.0-5
Obsoletes: gnome-netstatus <= 0:2.28.2-2
Obsoletes: gnome-python2-applet <= 0:2.32.0-2
Obsoletes: gnome-schedule <= 0:2.0.2-7
Obsoletes: gnubiff <= 0:2.2.13-5
Obsoletes: hamster-applet <= 0:2.32.0-5
Obsoletes: lock-keys-applet <= 0:1.0-22
Obsoletes: uim-gnome <= 0:1.6.1-2
Obsoletes: x-tile <= 0:1.8.6-2
# Other ones that were already retired from Fedora 15
Obsoletes: gnome-applet-netspeed <= 0.16-6
Obsoletes: contact-lookup-applet <= 0.17-7

# Also obsolete the now-dead libpanelappletmm
Obsoletes: libpanelappletmm <= 0:2.26.0-3

%description
The GNOME panel provides the window list, workspace switcher, menus, and other
features for the GNOME desktop.

%package libs
Summary: Libraries for Panel Applets
License: LGPLv2+
Group: Development/Libraries

%description libs
This package contains the libpanel-applet library that
is needed by Panel Applets.

%package devel
Summary: Headers and libraries for Panel Applet development
License: LGPLv2+
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}

%description devel
Panel Applet development package. Contains files needed for developing
Panel Applets using the libpanel-applet library.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .16px
%patch1 -p1 -b .menus

rm -f libtool
autoreconf -i -f
%build
%configure \
   --disable-gtk-doc \
   --disable-scrollkeeper \
%if %{use_evolution_data_server}
   --enable-eds=yes
%else
   --enable-eds=no
%endif

make %{?_smp_mflags} V=1


# truncate NEWS
awk '/^========/ { seen+=1 }
{ if (seen < 3) print }
{ if (seen == 3) { print "For older news, see http://git.gnome.org/cgit/gnome-panel/plain/NEWS"; exit } }' NEWS > tmp; mv tmp NEWS


%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make DESTDIR=$RPM_BUILD_ROOT install
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

rm -rf $RPM_BUILD_ROOT/var/scrollkeeper
find $RPM_BUILD_ROOT -name '*.la' -delete;

desktop-file-install --delete-original \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  $RPM_BUILD_ROOT%{_datadir}/applications/gnome-panel.desktop

%find_lang %{gettext_package} --all-name --with-gnome

%post
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
/sbin/ldconfig

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache -q %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :


%files -f %{gettext_package}.lang
%doc AUTHORS COPYING COPYING.LIB COPYING-DOCS NEWS README
%{_datadir}/icons/hicolor/16x16/apps/*
%{_datadir}/icons/hicolor/22x22/apps/*
%{_datadir}/icons/hicolor/24x24/apps/*
%{_datadir}/icons/hicolor/32x32/apps/*
%{_datadir}/icons/hicolor/48x48/apps/*
%{_datadir}/icons/hicolor/scalable/apps/*
%{_datadir}/gnome-panel
%{_datadir}/man/man*/*
%{_datadir}/applications/gnome-panel.desktop
%{_bindir}/gnome-panel
%{_bindir}/gnome-desktop-item-edit
%{_libexecdir}/*
%doc %{_datadir}/help/*/*

%{_datadir}/dbus-1/services/org.gnome.panel.applet.ClockAppletFactory.service
%{_datadir}/dbus-1/services/org.gnome.panel.applet.FishAppletFactory.service
%{_datadir}/dbus-1/services/org.gnome.panel.applet.NotificationAreaAppletFactory.service
%{_datadir}/dbus-1/services/org.gnome.panel.applet.WnckletFactory.service

%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.applet.fish.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.applet.window-list.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.applet.workspace-switcher.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.launcher.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.menu-button.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.object.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.toplevel.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.applet.clock.gschema.xml

%files libs
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/PanelApplet-4.0.typelib

%files devel
%{_bindir}/panel-test-applets
%{_includedir}/gnome-panel-4.0
%{_libdir}/pkgconfig/*
%{_libdir}/*.so
%{_datadir}/gtk-doc
%{_datadir}/gir-1.0/PanelApplet-4.0.gir

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Sun Oct 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.0-2m)
- BR in pkgconfig style does not work properly

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Tue Sep 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-3m)
- rebuild for libgweather-3.5.90
- rebuild for evolution-data-server-3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-2m)
- rebuild for libgweather-3.5.5

* Sat Jul 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-3m)
- fix BTS #445

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-2m)
- rebuild for librsvg2 2.36.1

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-3m)
- rebuild for evolution-data-server-devel-3.5.3

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- fix build failure with glib 2.33+

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Tue Sep 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.91-2m)
- update BuildRequires

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Mon May  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0.1-2m)
- add BuildRequires

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0.1-1m)
- update to 3.0.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0.2-1m)
- update to 2.32.0.2

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-5m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-4m)
- modify momonga-panel-default-setup.entries

* Thu Jul 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-3m)
- fix req

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- split libs

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- rebuild against evolution-2.30.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.6-1m)
- update to 2.29.6

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-5m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-4m)
- build fix libtool-2.2.6b

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- build with polkit

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
-- disable polkit

* Mon Jul 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-2m)
- modify Source1

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0
- add patch0 (gtk-doc)

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Wed Feb 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91
- modify Source1

* Fri Feb 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-2m)
- add patch0 (Require: libgnomeui-2.0)

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- update Patch9 for fuzz=0
- License: GPLv2+

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Wed Oct 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-2m)
- welcome evolution-data-server

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Aug 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-3m)
- add patch0 (nautilus only! ;-p)

* Sun Jul 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-2m)
- good-bye beagle (comment out patch8)

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1.3-3m)
- rebuild against librsvg2-2.22.2-3m (firefox-3 dependency)

* Sun Apr 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.3-2m)
- add patch14 form Fedora

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.3-1m)
- update to 2.22.1.3

* Fri Apr 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-4m)
- add BuildPrereq: librsvg2 >= 2.22.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- rebuild against gcc43

* Thu Mar 13 2008 Masanobu Sato <satoshiga@momonga-linux.prg>
- (2.22.0-2m)
- add BuildRequires: libgweather

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0.1-1m)
- update to 2.20.0.1

* Wed Jul 25 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.3-2m)
- fix schemas install error

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Wed Jun 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-3m)
- add Requires: gamin

* Mon Jun 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-3m)
- no warning in install script 

* Wed Jun 20 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.2-2m)
- delete deskbar_applet entry (update momonga-panel-default-setup.entries)

* Mon May 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Sat Mar 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-2m)
- fix %%post script

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-5m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-4m)
- good-bye %%makeinstall

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- GConf2 version down

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update 2.16.2

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0
- delete patch7

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-3m)
- rebuild against dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.3-2m)
- rebuild against expat-2.0.0-1m

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update 2.14.3

* Wed Jul 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2-5m)
- adjustment momonga-panel-default-setup.entries
- add deskbar applet and netstatus applet

* Sun Jul  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2-4m)
- yappari fukkatsu gnome-panel-2.14.0-gpm-integration.patch

* Sat Jun  3 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2-3m)
- update momonga-panel-default-setup.entries
- remove gnome-panel-2.14.0-gpm-integration.patch

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-2m)
- rebuild against evolution-data-server

*Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Tue May 16 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1-3m)
- add Patch7(gnome-panel-2.14.0-gpm-integration.patch)
- add Patch8(gnome-panel-2.13.90-use-beagle.patch)
- add Patch9(gnome-panel-2.13.91-ignore-unknown-options.patch)
- add Patch10(gnome-panel-2.14.1-remove-duplicate-entry.patch)

* Sat Apr 22 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1-2m)
- change OOo desktop file path (Source1)

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Wed Apr 12 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-3m)
- update panel default setup entry file (Source1)

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update 2.14.0

* Wed Mar  8 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12.3-2m)
- add BuildPrereq: gnome-doc-utils >= 0.3.2

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Sat Nov 26 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.1-3m)
- update momonga-panel-default-setup.entries for GNOME 2.12

* Sat Nov 26 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.1-2m)
- Install the default setup into GConf database

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Sat Apr  9 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.2.12.1-1m)
- GNOME 2.12.1 Desktop

* Sat Apr  9 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-3m)
- add momonga-lize panel setting (SOURCE1)

* Wed Feb 16 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-2m)
- add Pach0 (invalid clock hour format)

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-1m)
- version 2.8.2
- GNOME 2.8 Desktop

* Sun Aug 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.2-2m)
- add BuildPrereq  GConf >= 2.6.1

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Fri Apr 30 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-2m)
- add Pach1 (from FC1)

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.2-2m)
- revised spec for enabling rpm 4.2.

* Thu Feb 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Tue Nov 12 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.4.1-3m)
- gnometool-2 argument form was correct.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-1m)
- pretty spec file.
- add %%preun.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6.2-1m)
- version 2.3.6.2

* Wed Jul 30 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4.1-1m)
- version 2.3.4.1

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Thu Jul 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3.3-1m)
- version 2.3.3.3

* Wed Jul 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3.2-1m)
- version 2.3.3.2

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3.1-1m)
- version 2.3.3.1

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Thu May 29 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2.1-2m)
- fix filelist.

* Wed May 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2.1-1m)
- version 2.3.2.1

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Thu Mar 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0.1-2m)
  rebuild against openssl 0.9.7a
  
* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-1m)
- version 2.2.0.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Wed Jan 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.90.1-1m)
- version 2.1.90.1

* Tue Jan 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.90-1m)
- version 2.1.90

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Sun Dec  8 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.3-2m)
- BuildPrereq: libbonoboui-devel >= 2.1.1
  thanks Masaru SANUKI <sanuki@hh.iij4u.or.jp> ([Momonga-devel.ja:01035])

* Mon Dec 02 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Fri Sep  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.9-1m)
- version 2.0.9

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.7-1m)
- version 2.0.7

* Fri Aug 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-2m)
- libgen_util_applet-2.so - move to main package

* Sun Aug 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-1m)
- version 2.0.6

* Thu Aug 08 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-1m)
- version 2.0.5

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-3m)
- rebuild against for gnome-desktop-2.0.4
- rebuild against for gnome-session-2.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-2m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-6k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-4k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-12k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-10k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.24-12k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.24-10k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.24-8k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.24-6k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.24-4k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.24-2k)
- version 1.5.24

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.23-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.23-2k)
- version 1.5.23

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.22-4k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Mon May 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.22-2k)
- version 1.5.22

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.21-2k)
- version 1.5.21

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.20-4k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.20-2k)
- version 1.5.20

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.19-2k)
- version 1.5.19

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.18-2k)
- version 1.5.18

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.17-4k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.17-2k)
- version 1.5.17

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.16-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.16-4k)
- rebuild against for gtk+-2.0.2

* Tue Apr 02 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.16-2k)
- version 1.5.16

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.15-2k)
- version 1.5.15

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.14-4k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.14-2k)
- version 1.5.14

* Tue Mar 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.13-2k)
- version 1.5.13

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-20k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-18k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-16k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-14k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-12k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-10k)
- change schema handring

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-8k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-6k)
- rebuild against for libwnck-0.6

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-4k)
- rebuild against for yelp-0.3
- rebuild against for libgnomeui-1.112.1
- rebuild against for nautilus-1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-2k)
- version 1.5.12

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.11-4k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.11-2k)
- version 1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.10-14k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.10-10k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.10-8k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.10-6k)
- add utf.patch

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.10-4k)
- rebuild against for libwnck-0.5

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.10-2k)
- version 1.5.10

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.9-8k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.9-6k)
- rebuild against for libwnck-0.4

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.9-4k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.9-2k)
- created spec file
