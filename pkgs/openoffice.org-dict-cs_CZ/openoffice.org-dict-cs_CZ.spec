%global momorel 6

Name: openoffice.org-dict-cs_CZ
Version: 20060303
Release: %{momorel}m%{?dist}
Summary: Czech spellchecker and hyphenation dictionaries for OpenOffice.org
License: GPL+
Group: Applications/Productivity
URL: ftp://ftp.linux.cz/pub/localization/OpenOffice.org/devel/Czech/spell_checking/
Source0: openoffice.org-dict-cs_CZ.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%define hunspelldir %{_datadir}/myspell
%define hyphendir %{_datadir}/hyphen
%define debug_package %{nil}

%description
This package contains the czech hyphenation dictionaries for the Openoffice.org
application suite.

%package -n hunspell-cs
Group: Applications/Text
Summary: Czech hunspell dictionary
Requires: hunspell
Obsoletes: openoffice.org-dict-cs_CZ < %{version}-%{release}

%description -n hunspell-cs
This package contains the czech dictionary for the hunspell spellchecker.

%package -n hyphen-cs
Group: Applications/Text
Summary: Czech hyphenation rules
Requires: hyphen
Obsoletes: openoffice.org-dict-cs_CZ < %{version}-%{release}

%description -n hyphen-cs
Czech hyphenation rules.

%prep
%setup -q -n %{name}

%build
iconv -f ISO-8859-2 -t UTF-8 README_hyph_cs_CZ.txt > README_hyph_cs_CZ.txt.new
mv -f README_hyph_cs_CZ.txt.new README_hyph_cs_CZ.txt

%install
%{__rm} -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{hunspelldir}
install -m 644 cs* $RPM_BUILD_ROOT%{hunspelldir}
mkdir -p $RPM_BUILD_ROOT%{hyphendir}
install -m 644 hyph*.dic $RPM_BUILD_ROOT%{hyphendir}

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%files -n hyphen-cs
%defattr(-,root,root)
%doc README_hyph_cs_CZ.txt
%{hyphendir}/hyph_cs*

%files -n hunspell-cs
%defattr(-,root,root)
%doc README_cs_CZ.txt
%{hunspelldir}/cs*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20060303-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20060303-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20060303-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20060303-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20060303-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (20060303-1m)
- import from Fedora

* Mon Nov 26 2007 Tomas Mraz <tmraz@redhat.com> - 20060303-7
- add obsoletes openoffice.org-dict-cs_CZ

* Mon Nov 26 2007 Caolan McNamara <caolanm@redhat.com> - 20060303-6
- Resolves: rhbz#398361 move hyphenation rules into hyphen dir where OOo will now autodetect them

* Tue Mar 27 2007 Tomas Mraz <tmraz@redhat.com> - 20060303-5
- add hunspell-cs subpackage (#232416)
- openoffice datadir moved again

* Mon Jan 29 2007 Tomas Mraz <tmraz@redhat.com> - 20060303-4
- disable useless debuginfo (#225094)

* Mon Dec 11 2006 Tomas Mraz <tmraz@redhat.com> - 20060303-3
- package must be arch-specific now because ooo is now 64bit on x86_64 as
  well (#219100)

* Thu Sep  7 2006 Tomas Mraz <tmraz@redhat.com> - 20060303-2
- rebuilt for FC6

* Tue Mar 21 2005 Tomas Mraz <tmraz@redhat.com> - 20060303-1
- Initial package
