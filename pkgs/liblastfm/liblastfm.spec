%global momorel 1
%global qtver 4.8.4

Summary: Libraries to integrate Last.fm services
Name: liblastfm
Version: 1.0.3
Release: %{momorel}m%{?dist}
License: GPLv3
URL: http://github.com/mxcl/liblastfm/tree/master
Group: System Environment/Libraries
Source0: https://github.com/lastfm/%{name}/archive/%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: fftw-devel
BuildRequires: glib2-devel
BuildRequires: keyutils-libs-devel
BuildRequires: krb5-devel
BuildRequires: libsamplerate-devel
BuildRequires: openssl-devel
BuildRequires: ruby
BuildRequires: libselinux-devel
BuildRequires: zlib-devel

%description
liblastfm is a collection of libraries to help you integrate Last.fm services
into your rich desktop software. It is officially supported software developed
by Last.fm staff.

%package fingerprint
Summary: Liblastfm fingerprint library
BuildRequires: fftw3-devel
BuildRequires: libsamplerate-devel
Requires: %{name} = %{version}-%{release}
# upgrade path
Obsoletes: liblastfm < 1.0

%description fingerprint
%{summary}.

%package devel
Summary: Header files and static libraries from liblastfm
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on liblastfm.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} \
  -DBUILD_FINGERPRINT:BOOL=ON \
  ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post fingerprint -p /sbin/ldconfig
%postun fingerprint -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING
%doc README.md
%{_libdir}/liblastfm.so.1*

%files fingerprint
%{_libdir}/liblastfm_fingerprint.so.1*

%files devel
%defattr(-,root,root,-)
%{_libdir}/liblastfm*.so
%{_includedir}/lastfm

%changelog
* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-10m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-8m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-6m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-5m)
- use ruby19

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-4m)
- rebuild against qt-4.6.3-1m

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- initial package for scrobblers using Momonga Linux
- import qmake.patch from Fedora
