%global         momorel 1

Name:           perl-Devel-Cover
Version:        1.15
Release:        %{momorel}m%{?dist}
Summary:        Code coverage metrics for Perl
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Devel-Cover/
Source0:        http://www.cpan.org/authors/id/P/PJ/PJCJ/Devel-Cover-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.006001
BuildRequires:  perl-Digest-MD5
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-JSON-PP
BuildRequires:  perl-Parallel-Iterator
BuildRequires:  perl-Perl-Tidy >= 20060719
BuildRequires:  perl-Pod-Coverage >= 0.06
BuildRequires:  perl-PPI-HTML >= 1.07
BuildRequires:  perl-Storable
BuildRequires:  perl-Template-Toolkit >= 2
BuildRequires:  perl-Test-Differences
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-Warn
BuildRequires:  perl-B-Debug
Requires:       perl-Digest-MD5
Requires:       perl-JSON-PP
Requires:       perl-Parallel-Iterator
Requires:       perl-Perl-Tidy >= 20060719
Requires:       perl-Pod-Coverage >= 0.06
Requires:       perl-PPI-HTML >= 1.07
Requires:       perl-Sereal-Decoder
Requires:       perl-Sereal-Encoder
Requires:       perl-Storable
Requires:       perl-Template-Toolkit >= 2
Requires:       perl-Test-Differences
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides code coverage metrics for Perl. Code coverage metrics
describe how thoroughly tests exercise code. By using Devel::Cover you can
discover areas of code not exercised by your tests and determine which
tests to create to increase coverage. Code coverage can be considered as an
indirect measure of quality.

%prep
%setup -q -n Devel-Cover-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Devel::Cover::Dumper)/d'

EOF
%define __perl_requires %{_builddir}/Devel-Cover-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc docs/ README
%{_bindir}/*
%{perl_vendorarch}/Devel/Cover*
%{perl_vendorarch}/auto/Devel/Cover*
%{_mandir}/man1/*.1*
%{_mandir}/man3/*.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- rebuild against perl-5.20.0
- update to 1.15

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-2m)
- rebuild against perl-5.18.2

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-2m)
- rebuild against perl-5.18.1

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-1m)
- update to 1.03
- rebuild against perl-5.18.0

* Sun Apr 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-2m)
- rebuild against perl-5.16.3

* Mon Feb 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Tue Jan  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Mon Nov 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Sun Nov 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-2m)
- rebuild against perl-5.16.2

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-2m)
- rebuild against perl-5.16.1

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.89-1m)
- update to 0.89
- rebuild against perl-5.16.0

* Sun Apr  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.84-1m)
- update to 0.84

* Sat Mar 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.83-1m)
- update to 0.83

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Mon Mar 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-1m)
- update to 0.81

* Sun Mar 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-2m)
- rebuild against perl-5.14.2

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-1m)
- update to 0.79

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.78-2m)
- rebuild against perl-5.14.1

* Wed May 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.78-1m)
- update to 0.78

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.77-1m)
- update to 0.77

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-3m)
- rebuild against perl-5.14.0-0.2.1m

* Fri Apr 29 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.76-2m)
- add BuildRequires

* Wed Apr 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-1m)
- update to 0.76

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.73-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.73-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-1m)
- update to 0.73

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.68-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-2m)
- rebuild against perl-5.12.1

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-2m)
- rebuild against perl-5.12.0

* Tue Apr 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-1m)
- update to 0.66

* Thu Jan 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.65-2m)
- remove duplicate directories

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.65-1m)
- import from Rawhide for publican-1.3

* Thu Jan 14 2010 Tom "spot" Callaway <tcallawa@redhat.com> - 0.65-1
- update to 0.65

* Mon Dec  7 2009 Stepan Kasal <skasal@redhat.com> - 0.64-4
- rebuild against perl 5.10.1

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.64-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.64-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jun 13 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.64-1
- update to 0.64

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.63-3
- Rebuild for new perl

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.63-2
- Autorebuild for GCC 4.3

* Wed Nov 28 2007 Tom "spot" Callaway <tcallawa@redhat.com> - 0.63-1
- 0.63

* Mon Oct 15 2007 Tom "spot" Callaway <tcallawa@redhat.com> - 0.61-1.1
- correct license tag
- add BR: perl(ExtUtils::MakeMaker)

* Thu Jan 11 2007 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.61-1
- Update to 0.61.

* Thu Jan  4 2007 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.60-1
- Update to 0.60.

* Wed Sep  6 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.59-1
- Update to 0.59.
- Dropped PPI::HTML from the requirements list (optional module).

* Wed Aug  9 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.58-1
- Update to 0.58.

* Fri Aug  4 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.57-1
- Update to 0.57.

* Thu Aug  3 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.56-1
- Update to 0.56.

* Fri May 12 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.55-2
- Removed dependencies pulled in by a documentation file (#191110).

* Thu May 04 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.55-1
- First build.
