%global momorel 3

Summary:	Optical Character Recognition (OCR) program
Name:		gocr
Version:	0.49
Release:	%{momorel}m%{?dist}
License:	GPL
Group:		Applications/Multimedia
URL:		http://jocr.sourceforge.net/
Source0:        http://www-e.uni-magdeburg.de/jschulen/ocr/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:		gocr-0.46-perms.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	netpbm-devel
Obsoletes:	gocr-gtk
Obsoletes:	gocr-devel

%description
GOCR is an optical character recognition program, released under the
GNU General Public License. It reads images in many formats (pnm, pbm,
pgm, ppm, some pcx and tga image files) and outputs a text file.

%prep
%setup -q
%patch0 -p1 -b .perms

%build
%configure
%make

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR="%{buildroot}"

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc AUTHORS BUGS CREDITS HISTORY README REMARK.txt REVIEW TODO
%doc doc/*.html gpl.html
%{_bindir}/gocr
%{_bindir}/gocr.tcl
%{_mandir}/man1/gocr.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.49-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.49-2m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.48-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Sun Jul 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45
- gocr-gtk was obsoleted

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.44-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.44-3m)
- %%NoSource -> NoSource

* Thu May 24 2007 Yohsuke OOi <meke@momonga-linux.org>
- (0.44-2m)
- change Source URL

* Wed Apr 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.44-1m)
- import to Momonga from freshrpms.net

* Wed Mar  7 2007 Matthias Saou <http://freshrpms.net/> 0.44-1
- Update to 0.44.
- Remove no longer used patch.

* Mon Dec 18 2006 Dag Wieers <dag@wieers.com> - 0.43-1
- Updated to release 0.43.

* Mon Oct 23 2006 Matthias Saou <http://freshrpms.net/> 0.41-1
- Update to 0.41.
- Remove (apparently) no longer needed libm hack.
- Include user and devel docs only once, in one format.

* Sun May 11 2003 Dag Wieers <dag@wieers.com> - 0.37-0
- Initial package. (using DAR)
