%global momorel 3

Summary: A free RAW converter and digital photo processing software
Name: rawtherapee
Version: 4.0.9
Release: %{momorel}m%{?dist}
License: GPLv3
Group: Applications/Multimedia
URL: http://www.rawtherapee.com/
Source0: %{name}-%{version}.tar.xz
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils
Requires(post): desktop-file-utils
Requires(post): gtk2
Requires(postun): coreutils
Requires(postun): desktop-file-utils
Requires(postun): gtk2
Requires: hicolor-icon-theme
BuildRequires: ImageMagick
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: glib2-devel
BuildRequires: glibmm-devel
BuildRequires: gtk2-devel
BuildRequires: gtkmm-devel
BuildRequires: lcms-devel
BuildRequires: libiptcdata-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: libsigc++-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: pkgconfig
BuildRequires: zlib-devel
# for /usr/bin/hg
BuildRequires: mercurial

%description
Raw Therapee is a free RAW converter and digital photo processing software.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja

### Fedora's hack
# Fix for x86_64 and ppc64
%ifarch x86_64 ppc64
sed -i "s|/lib|/lib64|g" CMakeLists.txt
%endif

# Hide info box on picture by default
sed -i "s|^.*options.showInfo.*||" rtgui/editorpanel.cc

# fix wrong line endings
sed -i "s|\r||g" AUTHORS.txt COMPILE.txt

%build
%cmake .

make %{?_smp_mflags} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Fix /usr/usr/lib to /usr/lib
mv %{buildroot}/usr/%{_libdir} %{buildroot}/%{_libdir}

# install and link icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{22x22,64x64}/apps
convert -scale 22x22 rtdata/icons/hi256-app-rawtherapee.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
convert -scale 64x64 rtdata/icons/hi256-app-rawtherapee.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor
fi 
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:
/sbin/ldconfig

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor
fi || :
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS.txt AboutThisBuild.txt COMPILE.txt LICENSE.txt
%{_bindir}/rawtherapee
%{_libdir}/librtengine.so
%{_libdir}/librtexif.so
%{_datadir}/applications/%{name}.desktop
%{_docdir}/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/%{name}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.9-3m)
- rebuild for glib 2.33.2


* Sat Jun  9 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.9-2m)
- recover desktop.patch, sorry

* Sat Jun  9 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.9-1m)
- version 4.0.9

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.6-4m)
- rebuild against libtiff-4.0.1

* Mon Feb 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.6-3m)
- [BUG FIX]
- enable execute via desktop file
- clean up spec file

* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.6-2m)
- add BuildRequires

* Tue Jan  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0.6-1m)
- update 4.0.6

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-1m)
- update 3.0.0 Release

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-0.1.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-0.1.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-0.1.5m)
- full rebuild for mo7 release

* Fri Aug 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-0.1.4m)
- modify %%post and %%postun

* Sun Jun 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-0.1.3m)
- change Source0 to Fedora's tar-ball of upstream rev ccc12f4a03
- modify %%post and %%postun

* Tue Apr 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-0.1.2m)
- change Source0 to rawtherapee-3.0-a1-fork.tar.bz2
- http://lists.fedoraproject.org/pipermail/scm-commits/2010-March/410774.html
- import Fedora's hack to %%prep section
- modify %%post and %%postun
- update rawtherapee.desktop
- remove a switch usesvn

* Mon Jan 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-0.1.1m)
- initial package for photo freaks using Momonga Linux
