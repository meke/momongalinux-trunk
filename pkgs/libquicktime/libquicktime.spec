%global momorel 4
%global with_ffmpeg 1
%global with_lame 1

Summary: A library for manipulating QuickTime files
Name: libquicktime
Version: 1.2.4
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPL
URL: http://libquicktime.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: libquicktime-1.2.4-ffmpeg2-1.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: faac-devel
BuildRequires: faad2-devel >= 2.7
BuildRequires: gtk2-devel
%if %{with_ffmpeg}
BuildRequires: ffmpeg-devel >= 2.0.0
%endif
%if %{with_lame}
BuildRequires: lame-devel
%endif
BuildRequires: libICE-devel
BuildRequires: libXaw-devel
BuildRequires: libXv-devel
BuildRequires: libdv-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libogg-devel
BuildRequires: libpng-devel
BuildRequires: libvorbis-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: pkgconfig
BuildRequires: schroedinger-devel
BuildRequires: x264-devel >= 0.0.2377

%description
Libquicktime is a library for reading and writing QuickTime files
on UNIX systems. Video CODECs supported by this library are OpenDivX, MJPA,
JPEG Photo, PNG, RGB, YUV 4:2:2, and YUV 4:2:0 compression.  Supported
audio CODECs are Ogg Vorbis, IMA4, ulaw, and any linear PCM format.

Libquicktime is based on the quicktime4linux library.  Libquicktime add
features such as a GNU build tools-based build process and dynamically
loadable CODECs.

%package devel
Summary: Header files and static libraries from libquicktime
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libquicktime.

%prep
%setup -q

%patch0 -p1

%build
%configure \
	--with-libdv \
%ifarch x86_64
	--with-pic \
%endif
%if ! %{with_ffmpeg}
	--without-ffmpeg \
%endif
%if ! %{with_lame}
	--without-lame \
%endif
	--enable-gpl

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

# clean up
rm -rf %{buildroot}%{_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO
%{_bindir}/libquicktime_config
%{_bindir}/lqt*
%{_bindir}/qt*
%dir %{_libdir}/libquicktime
%{_libdir}/libquicktime/lqt_audiocodec.*
%{_libdir}/libquicktime/lqt_dv.*
%{_libdir}/libquicktime/lqt_faac.*
%{_libdir}/libquicktime/lqt_faad2.*
%if %{with_ffmpeg}
%{_libdir}/libquicktime/lqt_ffmpeg.*
%endif
%if %{with_lame}
%{_libdir}/libquicktime/lqt_lame.*
%endif
%{_libdir}/libquicktime/lqt_mjpeg.*
%{_libdir}/libquicktime/lqt_png.*
%{_libdir}/libquicktime/lqt_rtjpeg.*
%{_libdir}/libquicktime/lqt_schroedinger.*
%{_libdir}/libquicktime/lqt_videocodec.*
%{_libdir}/libquicktime/lqt_vorbis.*
%{_libdir}/libquicktime/lqt_x264.*
%{_libdir}/libquicktime.so.*
%{_datadir}/locale/*/LC_MESSAGES/libquicktime.mo
%{_mandir}/man1/lqtplay.1*

%files devel
%defattr(-,root,root)
%doc doc/apiref
%{_includedir}/lqt
%{_libdir}/pkgconfig/libquicktime.pc
%{_libdir}/libquicktime.so
#%{_datadir}/aclocal/lqt.m4

%changelog
* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-4m)
- rebuild against ffmpeg

* Tue Jan 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-3m)
- rebuild against x264

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-2m)
- rebuild against x264-0.0.2200

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-1m)
- update 1.2.4

* Sun Mar  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-5m)
- rebuild against ffmpeg-0.10.0-20120304

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-4m)
- rebuild against x264

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-3m)
- rebuild against ffmpeg compiled with new x264

* Wed Oct 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-2m)
- rebuild against x264

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-1m)
- update 1.2.3

* Tue Jul  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-5m)
- rebuild against ffmpeg

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-4m)
- rebuild against x264 and ffmpeg

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-3m)
- rebuild for new GCC 4.6

* Sun Mar  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-2m)
- rebuild against x264-0.0.1913

* Tue Jan 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-9m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-8m)
- rebuild against x264

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-7m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-6m)
- rebuild against x264-0.0.1677

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-5m)
- rebuild against x264-0.0.1649

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-4m)
- rebuild against x264-0.0.1583

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-3m)
- rebuild against x264-0.0.1523

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-2m)
- rebuild against libjpeg-8a

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-1m)
- update 1.1.5

* Wed Feb 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-2m)
- build with schroedinger

* Tue Feb 23 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-1m)
- update 1.1.4

* Fri Dec 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-9m)
- build with libdv again, Merry Christmas!

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-8m)
- rebuild against x264

* Sat Nov 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-7m)
- rebuild against x264-0.0.1332

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-5m)
- rebuild against x264-0.0.1281

* Sun Sep 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-4m)
- rebuild against x264

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-3m)
- rebuild against libjpeg-7

* Tue Aug 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-2m)
- rebuild against x264

* Mon Aug  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-1m)
- version 1.1.3

* Tue Jun 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-1m)
- version 1.1.2

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-3m)
- rebuild against faad2-2.7

* Wed Mar 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-2m)
- rebuild against x264

* Thu Feb 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update 1.1.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-2m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-1m)
- update 1.0.3
- rebuild against x264 and ffmpeg

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-7m)
- rebuild against x264 and ffmpeg

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-6m)
- rebuild against x264

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-6m)
- rebuild against x264

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-5m)
- rebuild against gcc43

* Wed Mar 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-4m)
- build with ffmpeg again

* Sun Mar 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-3m)
- build without ffmpeg for the moment

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- %%NoSource -> NoSource

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-1m)
- initial package for mlt
- Summary and %%description are imported from cooker
