%global         momorel 1

Name:           perl-Glib
Version:        1.305
Release:        %{momorel}m%{?dist}
Summary:        Perl wrappers for the GLib utility and Object libraries
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Glib/
Source0:        http://www.cpan.org/authors/id/X/XA/XAOC/Glib-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  glib2-devel
BuildRequires:  perl-ExtUtils-Depends >= 0.306
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-ExtUtils-PkgConfig >= 1.15
Requires:       perl-ExtUtils-Depends >= 0.306
Requires:       perl-ExtUtils-PkgConfig >= 1.15
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This wrapper attempts to provide a perlish interface while remaining as
true as possible to the underlying C API, so that any reference materials
you can find on using GLib may still apply to using the libraries from
perl. This module also provides facilities for creating wrappers for other
GObject-based libraries. The "SEE ALSO" section contains pointers to all
sorts of good information.

%prep
%setup -q -n Glib-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog.pre-git LICENSE NEWS README TODO
%{perl_vendorarch}/auto/Glib/
%{perl_vendorarch}/Glib*
%{_mandir}/man3/*.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.305-1m)
- rebuild against perl-5.20.0
- update to 1.305

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.304-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.304-1m)
- update to 1.304

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.302-1m)
- update to 1.302

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.301-2m)
- rebuild against perl-5.18.1

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.301-1m)
- update to 1.301

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.300-1m)
- update to 1.300
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.291-2m)
- rebuild against perl-5.16.3

* Mon Mar  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.291-1m)
- update to 1.291

* Tue Feb  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.290-1m)
- update to 1.290

* Sun Nov  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.280-1m)
- update to 1.280

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.262-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.262-2m)
- rebuild against perl-5.16.1

* Wed Aug  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.262-1m)
- update to 1.262

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.260-1m)
- update to 1.260
- rebuild against perl-5.16.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.250-2m)
- rebuild for glib 2.33.2

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.250-1m)
- update to 1.250

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.242-1m)
- update to 1.242

* Sun Nov 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.241-1m)
- update to 1.241

* Fri Oct 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.240-1m)
- update to 1.240

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.224-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.224-1m)
- update to 1.224

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.223-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.223-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.223-2m)
- full rebuild for mo7 release

* Mon May 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-1m)
- update to 1.223

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.222-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-1m)
- update to 1.222

* Tue Mar 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-1m)
- update to 1.221

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.220-1m)
- update to 1.220

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.201-1m)
- update to 1.201

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.200-2m)
- rebuild against rpm-4.6

* Sun Sep 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.200-1m)
- update to 1.200

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.183-1m)
- update to 1.183

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.182-2m)
- rebuild against gcc43

* Tue Apr  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.182-1m)
- update to 1.182

* Sat Mar 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.181-1m)
- update to 1.181

* Tue Mar 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.180-1m)
- update to 1.180

* Thu Jan 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.164-1m)
- update to 1.164

* Wed Jan  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.163-1m)
- update to 1.163

* Sun Nov 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.162-1m)
- update to 1.162

* Mon Oct 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.161-1m)
- update to 1.161

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.160-1m)
- update to 1.160
- do not use %%NoSource macro

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.144-3m)
- use vendor

* Fri Mar 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.144-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.144-1m)
- import to Momonga from Fedora Development


* Mon Feb 26 2007 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.144-1
- Update to 1.144.

* Sun Feb 11 2007 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.143-1
- Update to 1.143.

* Thu Dec  7 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.142-1
- Update to 1.142.

* Wed Nov 22 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.141-1
- Update to 1.141.

* Wed Sep  6 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.140-1
- Update to 1.140.

* Tue Mar 14 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.120-1
- Update to 1.120.

* Mon Feb 13 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.105-2
- make tag problem.

* Mon Feb 13 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.105-1
- Update to 1.105.

* Mon Feb  6 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.104-1
- Update to 1.104 (fails one test in perl 5.8.8).

* Thu Jan 19 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.103-1
- Update to 1.103.
- Provides list: filtered out perl(MY) (#177956).

* Wed Nov 30 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.102-1
- Update to 1.102.

* Thu Oct  6 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.101-1
- Update to 1.101.

* Thu Sep  8 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.100-1
- Update to 1.100.

* Mon Jun 27 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.082-1
- Update to 1.082.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Tue Mar  8 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.080-1
- Update to 1.080.

* Tue Feb 15 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.062-1
- Update to 1.062.

* Mon Oct 18 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.061-0.fdr.2
- Removed irrelevant documentation file - Glib.exports.

* Sun Oct  3 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.061-0.fdr.1
- Update to 1.061.

* Sun Jul 18 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:1.043-0.fdr.1
- First build.
