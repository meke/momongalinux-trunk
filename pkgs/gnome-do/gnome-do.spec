%global momorel 1
%global			debug_package %{nil}
%global			mainver 0.9

Name:			gnome-do
Version:		%{mainver}
Release: %{momorel}m%{?dist}
Summary:		Quick launch and search

License:		GPLv3+
Group:			Applications/File
URL:			http://do.davebsd.com/
# http://launchpad.net/do/trunk/0.9/+download/gnome-do-0.9.tar.gz
Source0:		http://launchpad.net/do/trunk/%{mainver}/+download/gnome-do-%{version}.tar.gz
NoSource: 0

BuildRoot:		%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


BuildRequires:		mono-devel, mono-addins-devel, monodoc-devel
BuildRequires:		desktop-file-utils
BuildRequires:		dbus-sharp-devel
BuildRequires:		dbus-sharp-glib-devel
BuildRequires:		gtk-sharp2-devel, notify-sharp-devel
BuildRequires:		gnome-sharp-devel, gnome-desktop-sharp-devel >= 2.26
BuildRequires:		gnome-keyring-sharp-devel
BuildRequires:		gettext
BuildRequires:		perl-XML-Parser
BuildRequires:		intltool
BuildRequires:		gtk2-devel
BuildRequires:		desktop-file-utils

Requires(pre):		GConf2
Requires(post):		GConf2
Requires(preun):	GConf2

Requires:		mono(NDesk.DBus.GLib) = 1.0.0.0
Requires:		gnome-keyring-sharp, gnome-desktop-sharp, findutils
Requires:		gnome-desktop, pkgconfig

# Mono only available on these:
ExclusiveArch:	%ix86 x86_64 ppc ppc64 ia64 %{arm} sparcv9 alpha s390x

Obsoletes:		gnome-do-docklets < 0.8.5-1%{?dist}

%description
GNOME Do (Do) is an intelligent launcher tool that makes performing
common tasks on your computer simple and efficient. Do not only allows
you to search for items in your desktop environment
(e.g. applications, contacts, bookmarks, files, music), it also allows
you to specify actions to perform on search results (e.g. run, open,
email, chat, play).

%package devel
Summary:		Development files for GNOME Do
Group:			Development/Libraries
Requires:		%{name} = %{version}-%{release}
Requires:		pkgconfig

%description devel
Development files for GNOME Do

%prep
%setup -q

%build
# Build with the older gmcs for compatibility
DMCS="/usr/bin/gmcs"; export DMCS;
%configure
%make 


%install
rm -rf %{buildroot}
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot}

desktop-file-install	\
	--dir %{buildroot}%{_sysconfdir}/xdg/autostart	\
	--add-only-show-in=GNOME				\
	%{buildroot}%{_datadir}/applications/gnome-do.desktop
desktop-file-install --delete-original	\
	--dir %{buildroot}%{_datadir}/applications	\
	--remove-category Application	\
	%{buildroot}%{_datadir}/applications/%{name}.desktop


#own this dir:
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_datadir}/%{name}/plugins/

%find_lang %{name}

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/%{name}.schemas >/dev/null || :
fi

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/%{name}.schemas > /dev/null || :

touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ] ; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/%{name}.schemas > /dev/null || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ] ; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYRIGHT
%{_bindir}/gnome-do
%{_libdir}/gnome-do/
%{_datadir}/gnome-do/
%config(noreplace) %{_sysconfdir}/xdg/autostart/gnome-do.desktop
%config(noreplace) %{_sysconfdir}/gconf/schemas/*
%{_datadir}/icons/hicolor/*/apps/gnome-do.*
%{_datadir}/applications/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/*

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-1m)
- reimport from fedora

* Tue May 10 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-2m)
- rebuild against mono-addins-0.6.1

* Tue Apr 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-1m)
- version 0.8.4
- import upstream patches from opensuse factory

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3.1-8m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3.1-7m)
- build fix (add patch0, patch1, and patch2)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.3.1-6m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3.1-5m)
- good-bye XFCE

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.3.1-4m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.3.1-3m)
- add Requires: GConf2

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3.1-2m)
- NotShowIn LXDE

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3.1-1m)
- update to 0.8.3.1

* Thu Jul 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-4m)
- rebuild against mono-addins-0.5

* Mon Jul 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-3m)
- add BuildRequires

* Thu Jul  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-2m)
- disable auto-start at KDE startup
- License: GPLv3

* Tue Jul  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-1m)
- initial build
