%global momorel 8

Summary:	The dnswalk DNS database debugger
Name:		dnswalk
Version:	2.0.2
Release: %{momorel}m%{?dist}
Group:		Applications/Internet
URL:		http://www.visi.com/~barr/dnswalk/
License:	Artistic
Source0:	%{name}-%{version}.tar.bz2
Patch0:		%{name}-%{version}-perlpath.patch.bz2
Patch1:		%{name}-delete-filterout.patch.bz2
Requires:	perl-Net-DNS
Requires:	perl >= 5.004
Buildarch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
dnswalk is a DNS debugger. It performs zone transfers of specified
domains, and checks the database in numerous ways for internal
consistency, as well as accuracy. 

%prep
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%setup -q -c -a0
%patch0 -p1
%patch1 -p0

%build

# fix attr
chmod 644 *

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

install -d %{buildroot}%{_sbindir}
install -d %{buildroot}%{_mandir}/man1
install -m755 %{name} %{buildroot}%{_sbindir}/
install -m755 %{name}.1 %{buildroot}%{_mandir}/man1/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES README TODO makereports sendreports rfc1912.txt do-dnswalk
%attr(0755,root,root) %{_sbindir}/%{name}
%{_mandir}/man1/%{name}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.2-22m)
- rebuild against perl-5.10.0-1m

* Fri Jun 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-1m)
- adapt for momonga

* Fri Jun 03 2005 Oden Eriksson <oeriksson@mandriva.com> 2.0.2-4mdk
- rebuild
- fix autodeps

* Sun May 16 2004 Oden Eriksson <oeriksson@mandrakesoft.com> 2.0.2-3mdk
- build release

* Thu Jan 16 2003 Oden Eriksson <oden.eriksson@kvikkjokk.net> 2.0.2-2mdk
- build release

* Sat Jun 29 2002 Oden Eriksson <oden.eriksson@kvikkjokk.net> 2.0.2-1mdk
- initial cooker contrib
