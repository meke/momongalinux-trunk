%global         momorel 1
%global         srcver 1.1.1

%global         qtver 4.7.5
%global         qtrel 2m
%global         cmakever 2.6.4
%global         cmakerel 1m
%global         sourcedir stable/%{name}/%{version}/src

Name:           prison
Version:        %{srcver}
Release:        %{momorel}m%{?dist}
Summary:        a barcode library
Group:          System Environment/Libraries
License:        GPLv2+ and LGPLv2+
URL:            http://www.kde.org/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel
BuildRequires:  cmake
BuildRequires:  automoc
BuildRequires:  qrencode-devel

%description
prison is a barcode api currently offering a nice Qt api to produce 
QRCode barcodes and DataMatrix barcodes, and can easily be made support more.

%package devel
Summary: Header files and libraries for developing apps which will use prison.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The prison-devel package contains the header files and libraries needed
to develop programs that use the prison library.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} ..
popd

make %{?_smp_mflags}  -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE Mainpage.dox
%{_kde4_libdir}/libprison.so.*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/cmake/Prison
%{_kde4_libdir}/libprison.so

%changelog
* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sun May 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-2m)
- add BuildRequires

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- initial build for Momonga Linux
