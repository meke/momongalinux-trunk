%global momorel 9
Summary: A library for character and string-slyphs from Adobe Type 1 fonts
Name: t1lib
Version: 5.1.2
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: ftp://sunsite.unc.edu/pub/Linux/libs/graphics/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: t1lib-5.1.2-segf.patch
# Fixes CVE-2010-2642, CVE-2011-0433
# http://bugzilla.redhat.com/show_bug.cgi?id=679732
Patch1: t1lib-5.1.2-afm-fix.patch
# Fixes CVE-2011-0764, CVE-2011-1552, CVE-2011-1553, CVE-2011-1554
# http://bugzilla.redhat.com/show_bug.cgi?id=692909
Patch2: t1lib-5.1.2-type1-inv-rw-fix.patch
URL: http://freshmeat.net/projects/t1lib/
BuildRequires: libtool
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
t1lib is a library distributed under the GNU Lesser General Public
License for generating character- and string-glyphs from Adobe Type 1
fonts under UNIX. t1lib uses most of the code of the X11 rasterizer
donated by IBM to the X11-project. But some disadvantages of the
rasterizer being included in X11 have been eliminated. Here are some
of the features:

- t1lib is completely independent of X11 (although the program
  provided for testing the library needs X11)

- fonts are made known to library by means of a font database file at runtime

- searchpaths for all types of input files are configured by means
  of a configuration file at runtime

- characters are rastered as they are needed

- characters and complete strings may be rastered by a simple function call

- when rastering strings, pairwise kerning information from .afm-files
  may optionally be taken into account

- an interface to ligature-information of afm-files is provided

- a program to generate afm-files from Type 1 font files is included

- rotation is supported at any angles

- there's support for extending and slanting fonts

- underlining, overlining and overstriking is supported

- new encoding vectors may be loaded at runtime and fonts may be
  reencoded using these encoding vectors

- antialiasing is implemented using three gray-levels between black and white

- An interactive test program called "xglyph" is included in the
  distribution. This program allows to test all of the features of the
  library. It requires X11.
#'

%package utils
Summary: Several utilities to manipulate and examine Type1 fonts.
Group: Applications/Publishing

%description utils
This package contains some utilities which allow you to view and
manipulate Type1 fonts.  They are mainly useful for debugging and
testing purposes, and are not required for using the t1lib library.

%package devel
Summary: Header files and static library for development with t1lib.
Group: Development/Libraries
Requires: %name = %{version}-%{release}

%description devel
The t1lib-devel package contains the header files and static
library needed to develop or compile applications which use the
t1lib Type1 font rendering library. 

Install t1lib-devel if you want to develop t1lib
applications. If you simply want to run existing applications, you
won't need this package.
#'

%prep
rm -rf %{buildroot}

%setup -q
%patch0 -p1 -b .segf
%patch1 -p1 -b .afm-fix
%patch2 -p1 -b .type1-inv-rw-fix

%build
%ifarch x86_64
libtoolize -c -f
%endif

%ifnarch alpha alphaev5
CFLAGS="%{optflags}" ./configure --prefix=%{_prefix} --libdir=%{_libdir}
%else
CFLAGS="%{optflags}" ./configure --prefix=%{_prefix} --host=alpha-pc-linux
%endif
%ifarch x86_64
make LIBTOOL=%{_bindir}/libtool XXXX
%else
make XXXX
%endif

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_prefix}/{bin,include,lib}
%ifarch x86_64
make prefix=%{buildroot}%{_prefix} libdir=%{buildroot}%{_libdir} LIBTOOL=%{_bindir}/libtool install
%else
make prefix=%{buildroot}%{_prefix} libdir=%{buildroot}%{_libdir} install
%endif
chmod a+x %{buildroot}%{_libdir}/libt1x.so.*
chmod a+x %{buildroot}%{_libdir}/libt1.so.*
#strip %{buildroot}%{_bindir}/*
rm -f %{buildroot}/usr/share/t1lib/doc/t1lib_doc.pdf

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc Changes LGPL LICENSE README*
%doc doc
%{_libdir}/*.so.*
%dir %{_datadir}/t1lib
%config(noreplace) %{_datadir}/t1lib/t1lib.config

%files utils
%defattr(-,root,root)
%{_bindir}/*

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/*.a

%changelog
* Tue Jan 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.2-9m)
- [SECURITY] CVE-2010-2642 CVE-2011-0433 (patch1)
- [SECURITY] CVE-2011-0764 CVE-2011-1552 CVE-2011-1553 CVE-2011-1554 (patch2)
- import security patches from Fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1.2-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1.2-5m)
- use BuildRequires

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1.2-4m)
- revised for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.2-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.2-1m)
- update to 5.1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1.1-2m)
- %%NoSource -> NoSource

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.1-1m)
- update to 5.1.1

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.1.0-2m)
- delete libtool library

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (5.1.0-1m)
- update to 5.1.0

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (5.0.2-2m)
- enable x86_64. use %{_bindir}/libtool for x86_64.
  
* Sat May 22 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (5.0.2-1m)
- verup

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (5.0.0-2m)
- revised spec for rpm 4.2.

* Thu Sep 18 2003 Kenta MURATA <muraken2@nifty.com>
- (5.0.0-1m)
- change license for Momonga Linux Package License Notation
  Specification.

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.0-1m)
- update to 5.0.0
- use rpm macros
- delete define %%prefix /usr, use %%_prefix

* Tue Feb  5 2002 Kenta MURATA <muraken2@nifty.com>
- (1.3.1-2k)
- version up to 1.3.1.

* Fri Sep 28 2001 Kenta MURATA <muraken2@nifty.com>
- (1.1.1-2k)
- version up to 1.1.1.

* Sun Jul  8 2001 Toru Hoshina <toru@df-usa.com>
- (1.1.0-6k)
- add alphaev5 support.

* Mon Mar 26 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.1.0-3k)
- add BuildPreReq: libtool
- rebuild for Jirai

* Mon Mar 26 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.1.0-0k6KOM)
- do not strip in spec, because rpm automatically strip binaries by macro

* Sun Feb 25 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.1.0-0k5KOM)
- make t1lib.config as %%config

* Sun Feb 25 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.1.0-0k2KOM)
- update to 1.1.0

* Mon Feb 19 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.0.1-5k4KOM)
- strip binaries
- change BuildRoot

* Mon Feb 19 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.0.1-5k2KOM)
- fix License (GPL -> LGPL)
- fix description (license part)
- add %%post and %%postun
- divide into three sub pkgs (devel and utils).

* Sat Nov 25 2000 Kenichi Matsubara <m@kondara.org>
- add %clean section.

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.
- version up 0.9.4 -> 1.0.1

* Wed Apr 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, description, changelog )

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file
- Added BuildRoot.
- Make the package relocatable.

* Mon Sep 27 1999 Andy Lindeman <a_lindeman@hotmail.com>
- updated to t1lib .9.2

* Thu Aug 12 1999 Jose Romildo Malaquias <romildo@iceb.ufop.br>
- Added %changelog
