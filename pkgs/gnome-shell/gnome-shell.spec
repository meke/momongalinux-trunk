%global		momorel 1
Name:           gnome-shell
Version:        3.6.3.1
Release: 	%{momorel}m%{?dist}
Summary:        Window management and application launching for GNOME

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://live.gnome.org/GnomeShell
#VCS:           git:git://git.gnome.org/gnome-shell
Source0:        http://download.gnome.org/sources/gnome-shell/3.6/%{name}-%{version}.tar.xz
NoSource:	0
Patch0: gnome-shell-avoid-redhat-menus.patch
# Replace Epiphany with Firefox in the default favourite apps list
Patch1: gnome-shell-favourite-apps-firefox.patch

%define clutter_version 1.11.16
%define gobject_introspection_version 1.33.10
%define mutter_version 3.6.2
%define eds_version 3.6.0
%define json_glib_version 0.13.2

## Needed when we re-autogen
BuildRequires:  autoconf >= 2.53
BuildRequires:  automake >= 1.10
BuildRequires:  gnome-common >= 3.5.91
BuildRequires:  libtool >= 1.4.3
BuildRequires:  chrpath
BuildRequires:  clutter-devel >= %{clutter_version}
BuildRequires:  dbus-glib-devel
BuildRequires:  desktop-file-utils
BuildRequires:  evolution-data-server-devel >= %{eds_version}
BuildRequires:  gcr-devel
BuildRequires:  gjs-devel >= 1.33.14
BuildRequires:  glib2-devel
BuildRequires:  gsettings-desktop-schemas >= 3.6.0
BuildRequires:  gnome-menus-devel >= 3.6.0
BuildRequires:  gnome-desktop3-devel
BuildRequires:  gobject-introspection >= %{gobject_introspection_version}
BuildRequires:  json-glib-devel >= %{json_glib_version}
BuildRequires:  upower-devel
BuildRequires:  NetworkManager-glib-devel
BuildRequires:  polkit-devel
BuildRequires:  telepathy-glib-devel
BuildRequires:  telepathy-logger-devel >= 0.6.0
# for screencast recorder functionality
BuildRequires:  gstreamer1-devel
BuildRequires:  gtk3-devel
BuildRequires:  intltool
BuildRequires:  libcanberra-devel
BuildRequires:  libcroco-devel
BuildRequires:  folks-devel
BuildRequires:  at-spi2-atk-devel >= 2.5

# for barriers
BuildRequires:  libXfixes-devel >= 5.0
# used in unused BigThemeImage
BuildRequires:  librsvg2-devel
BuildRequires:  mutter-devel >= %{mutter_version}
BuildRequires:  pulseaudio-libs-devel
%ifnarch s390 s390x
BuildRequires:  gnome-bluetooth-libs-devel >= 3.6.0
BuildRequires:  gnome-bluetooth >= 3.6.0
%endif
# Bootstrap requirements
BuildRequires: gtk-doc gnome-common
Requires:       gnome-menus%{?_isa} >= 3.0.0-2
Requires:	gsettings-desktop-schemas >= 3.6.0
Requires:	gnome-control-center >= 3.6.0
# wrapper script uses to restart old GNOME session if run --replace
# from the command line
Requires:       gobject-introspection%{?_isa} >= %{gobject_introspection_version}
# needed for loading SVG's via gdk-pixbuf
Requires:       librsvg2%{?_isa}
# needed as it is now split from Clutter
Requires:       json-glib%{?_isa} >= %{json_glib_version}
# For $libdir/mozilla/plugins
Requires:       mozilla-filesystem%{?_isa}
Requires:       mutter%{?_isa} >= %{mutter_version}
Requires:       upower%{?_isa}
Requires:       polkit%{?_isa} >= 0.100
# needed for schemas
Requires:       at-spi2-atk%{?_isa}
# needed for on-screen keyboard
Requires:       caribou%{?_isa}
# needed for the user menu
Requires:       accountsservice-libs
Requires:       gdm-libs

%description
GNOME Shell provides core user interface functions for the GNOME 3 desktop,
like switching to windows and launching applications. GNOME Shell takes
advantage of the capabilities of modern graphics hardware and introduces
innovative user interface concepts to provide a visually attractive and
easy to use experience.

%prep
%setup -q
%patch0 -p1 -b .avoid-redhat-menus
%patch1 -p1 -b .firefox

rm configure

%build
(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; fi;
  %configure --disable-static)
make V=1 %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

rm -rf %{buildroot}/%{_libdir}/mozilla/plugins/*.la

desktop-file-validate %{buildroot}%{_datadir}/applications/gnome-shell.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/gnome-shell-extension-prefs.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/evolution-calendar.desktop

# fixme: do we want to include this ?
rm %{buildroot}/usr/bin/gnome-shell-perf-tool

%find_lang %{name}

%ifnarch s390 s390x
# The libdir rpath breaks nvidia binary only folks, so we remove it.
# See bug 716572
# skip on s390(x), workarounds a chrpath issue
chrpath -r %{_libdir}/gnome-shell:%{_libdir}/gnome-bluetooth $RPM_BUILD_ROOT%{_bindir}/gnome-shell
chrpath -r %{_libdir}/gnome-bluetooth $RPM_BUILD_ROOT%{_libdir}/gnome-shell/libgnome-shell.so
%endif

%preun
glib-compile-schemas --allow-any-name %{_datadir}/glib-2.0/schemas &> /dev/null ||:

%posttrans
glib-compile-schemas --allow-any-name %{_datadir}/glib-2.0/schemas &> /dev/null ||:

%files -f %{name}.lang
%doc COPYING README
%{_bindir}/gnome-shell
%{_bindir}/gnome-shell-extension-tool
%{_bindir}/gnome-shell-extension-prefs
%{_datadir}/glib-2.0/schemas/*.xml
%{_datadir}/applications/gnome-shell.desktop
%{_datadir}/applications/gnome-shell-extension-prefs.desktop
%{_datadir}/applications/evolution-calendar.desktop
%{_datadir}/gnome-shell/
%{_datadir}/dbus-1/services/org.gnome.Shell.CalendarServer.service
%{_datadir}/dbus-1/services/org.gnome.Shell.HotplugSniffer.service
%{_datadir}/dbus-1/interfaces/org.gnome.ShellSearchProvider.xml
%{_datadir}/gnome-control-center/keybindings/50-gnome-shell-screenshot.xml
%{_datadir}/gnome-control-center/keybindings/50-gnome-shell-system.xml
%{_libdir}/gnome-shell/
%{_libdir}/mozilla/plugins/*.so
%{_libexecdir}/gnome-shell-calendar-server
%{_libexecdir}/gnome-shell-perf-helper
%{_libexecdir}/gnome-shell-hotplug-sniffer
# Co own these directories instead of pulling in GConf
# after all, we are trying to get rid of GConf with these files
%dir %{_datadir}/GConf
%dir %{_datadir}/GConf/gsettings
%{_datadir}/GConf/gsettings/gnome-shell-overrides.convert
%{_mandir}/man1/%{name}.1.*
# exclude as these should be in a devel package for st etc
%exclude %{_datadir}/gtk-doc

%changelog
* Fri Aug  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.3.1-1m)
- update to 3.6.3.1

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.1-2m)
- rebuild against telepathy-logger-0.6.0

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92
- update upstream patch

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91
- update upstream patch

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-2m)
- import many bug fixes from upstream

* Wed Aug 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sat Jul 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-2m)
- add BuildRequires and Requires

* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-4m)
- rebuild against evolution-data-server-devel

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-3m)
- fix "open calender" bug; see https://bugzilla.gnome.org/show_bug.cgi?id=677907

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-2m)
- rebuild for librsvg2 2.36.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3
- reimport from fedora
- Fix build failure with glib 2.33+

* Thu May  3 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.2.1-4m)
- update patch2
-- delete gnome-shell-extension-advanced-settings-in-usermenu

* Fri Mar 23 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.2.1-3m)
- update patch2
-- add gnome-shell-extension-advanced-settings-in-usermenu

* Wed Mar 21 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.2.1-2m)
- modify patch0 (add gnome-terminal)

* Mon Jan 23 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.2.1-1m)
- update to 3.2.2.1

* Wed Jan  4 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1-5m)
- add patch2 (enabled extensions list)

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-4m)
- rebuild against telepathy-logger-0.2.12

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-3m)
- rebuild against telepathy-logger-0.2.11

* Sun Oct 23 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1-2m)
- require adjustment

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Tue Oct 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-3m)
- modify patch0 (come back shotwell)

* Mon Oct 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-2m)
- add patch1 (show minimize maximize button on title bar by default)

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91.1-2m)
- rebuild against mutter-3.1.91.1-2m

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91.1-1m)
- update to 3.1.91.1

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Fri May 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- modify menu (add patch0)

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.4-6m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.4-5m)
- build fix (add patch2)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.4-4m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.4-3m)
- fix build failure; add patch for gjs-devel-0.7.6

* Mon Oct 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.4-2m)
- fix build failure

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.4-1m)
- update to 2.31.4
-- this package is now broken (do not use make -i)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.31.2-5m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.2-4m)
- add Requires: mutter

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.31.2-3m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.31.2-2m)
- add Requires: GConf2

* Thu May 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.2-1m)
- update to 2.31.2

* Mon Apr 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.1-1m)
- update to 2.29.1
- delete patch1 merged

* Mon Mar 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.0-2m)
- add patch1 for clutter-1.2.0

* Fri Feb 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.29.0-1m)
- update for new mutter-2.29.0
-- temporally disable gnome-shell-2.27.1-ja.patch 

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-3m)
- rebuild against gnome-desktop-2.29.90

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Wed Sep 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.3-1m)
- update to 2.27.3

* Sun Sep  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.2-1m)
- update to 2.27.2

* Thu Sep  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.1-3m)
- add ja translate

* Wed Sep  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.1-2m)
- to main

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.1-1m)
- initial build
