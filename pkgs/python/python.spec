%global momorel 2
%global python python

Name: %{python}

# ======================================================
# Conditionals and other variables controlling the build
# ======================================================

%{!?__python_ver:%global __python_ver EMPTY}
#global __python_ver 27
%global unicode ucs4

%global _default_patch_fuzz 2

%if "%{__python_ver}" != "EMPTY"
%global main_python 0
%global python python%{__python_ver}
%global tkinter tkinter%{__python_ver}
%else
%global main_python 1
%global python python
%global tkinter tkinter
%endif

%global pybasever 2.7
%global pylibdir %{_libdir}/python%{pybasever}
%global tools_dir %{pylibdir}/Tools
%global demo_dir %{pylibdir}/Demo
%global doc_tools_dir %{pylibdir}/Doc/tools
%global dynload_dir %{pylibdir}/lib-dynload
%global site_packages %{pylibdir}/site-packages

# Python's configure script defines SOVERSION, and this is used in the Makefile
# to determine INSTSONAME, the name of the libpython DSO:
#   LDLIBRARY='libpython$(VERSION).so'
#   INSTSONAME="$LDLIBRARY".$SOVERSION
# We mirror this here in order to make it easier to add the -gdb.py hooks.
# (if these get out of sync, the payload of the libs subpackage will fail
# and halt the build)
%global py_SOVERSION 1.0
%global py_INSTSONAME_optimized libpython%{pybasever}.so.%{py_SOVERSION}
%global py_INSTSONAME_debug     libpython%{pybasever}_d.so.%{py_SOVERSION}


# Disabled for now:
%global with_huntrleaks 0

%global with_systemtap 1

# some arches dont have valgrind so we need to disable its support on them
%ifarch %{ix86} x86_64 ppc ppc64 s390x
%global with_valgrind 1
%else
%global with_valgrind 0
%endif

%global with_gdbm 1

### include local configuration
%{?include_specopt}
### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpmbuild/specopt/python.specopt and edit it.
%{?!do_test: %global do_test 0}
# Turn this to 0 to turn off the "check" phase:
%{?!run_selftest_suite: %global run_selftest_suite 1}
%{?!with_debug_build: %global with_debug_build 0}
%{?!with_gdb_hooks: %global with_gdb_hooks 0}

# Some of the files below /usr/lib/pythonMAJOR.MINOR/test  (e.g. bad_coding.py)
# are deliberately invalid, leading to SyntaxError exceptions if they get
# byte-compiled.
#
# These errors are ignored by the normal python build, and aren't normally a
# problem in the buildroots since /usr/bin/python isn't present.
# 
# However, for the case where we're rebuilding the python srpm on a machine
# that does have python installed we need to set this to avoid
# brp-python-bytecompile treating these as fatal errors:
#
%global _python_bytecompile_errors_terminate_build 0

# We need to get a newer configure generated out of configure.in for the following
# patches:
#   patch 4 (CFLAGS)
#   patch 52 (valgrind)
#   patch 55 (systemtap)
#   patch 145 (linux2)
# 
# For patch 55 (systemtap), we need to get a new header for configure to use
#
# configure.in requires autoconf-2.65, but the version in Fedora is currently
# autoconf-2.66
#
# For now, we'll generate a patch to the generated configure script and
# pyconfig.h.in on a machine that has a local copy of autoconf 2.65
#
# Instructions on obtaining such a copy can be seen at
#   http://bugs.python.org/issue7997
#
# To make it easy to regenerate the patch, this specfile can be run in two
# ways:
# (i) regenerate_autotooling_patch  0 : the normal approach: prep the
# source tree using a pre-generated patch to the "configure" script, and do a
# full build
# (ii) regenerate_autotooling_patch 1 : intended to be run on a developer's
# workstation: prep the source tree without patching configure, then rerun a
# local copy of autoconf-2.65, regenerate the patch, then exit, without doing
# the rest of the build
%global regenerate_autotooling_patch 0


# ==================
# Top-level metadata
# ==================
Summary: An interpreted, interactive, object-oriented programming language
# Remember to also rebase python-docs when changing this:
Version: 2.7.6
Release: %{momorel}m%{?dist}
License: "Python"
Group: Development/Languages
Requires: %{python}-libs%{?_isa} = %{version}-%{release}
Provides: python-abi = %{pybasever}
Provides: python(abi) = %{pybasever}


# =======================
# Build-time requirements
# =======================

# (keep this list alphabetized)

BuildRequires: autoconf
BuildRequires: bzip2
BuildRequires: bzip2-devel
BuildRequires: db4-devel >= 4.8
BuildRequires: expat-devel
BuildRequires: findutils
BuildRequires: gcc-c++
%if %{with_gdbm}
BuildRequires: gdbm-devel
%endif
BuildRequires: glibc-devel
BuildRequires: gmp-devel
BuildRequires: libffi-devel
BuildRequires: libGL-devel
BuildRequires: libX11-devel
BuildRequires: ncurses-devel
BuildRequires: openssl-devel
BuildRequires: pkgconfig
BuildRequires: readline-devel
BuildRequires: sqlite-devel

%if 0%{?with_systemtap}
BuildRequires: systemtap-sdt-devel
# (this introduces a circular dependency, in that systemtap-sdt-devel's
# /usr/bin/dtrace is a python script)
%global tapsetdir      /usr/share/systemtap/tapset
%endif # with_systemtap

BuildRequires: tar
BuildRequires: tcl-devel
BuildRequires: tix-devel
BuildRequires: tk-devel

%if 0%{?with_valgrind}
BuildRequires: valgrind-devel
%endif

BuildRequires: zlib-devel



# =======================
# Source code and patches
# =======================

Source0: http://www.python.org/ftp/python/%{version}/Python-%{version}.tar.xz
NoSource: 0

# Work around bug 562906 until it's fixed in rpm-build by providing a fixed
# version of pythondeps.sh:
Source2: pythondeps.sh
%global __python_requires %{SOURCE2}

# Systemtap tapset to make it easier to use the systemtap static probes
# (actually a template; LIBRARY_PATH will get fixed up during install)
# Written by dmalcolm; not yet sent upstream
Source3: libpython.stp


# Example systemtap script using the tapset
# Written by wcohen, mjw, dmalcolm; not yet sent upstream
Source4: systemtap-example.stp

# Another example systemtap script that uses the tapset
# Written by dmalcolm; not yet sent upstream
Source5: pyfuntop.stp

# Supply various useful macros for building python 2 modules:
#  __python2, python2_sitelib, python2_sitearch, python2_version
Source6: macros.python2

Patch0: python-2.7.1-config.patch
Patch1: 00001-pydocnogui.patch
Patch4: python-2.5-cflags.patch
Patch6: python-2.5.1-plural-fix.patch
Patch7: python-2.5.1-sqlite-encoding.patch
Patch10: python-2.7rc1-binutils-no-dep.patch
Patch13: python-2.7rc1-socketmodule-constants.patch
Patch14: python-2.7rc1-socketmodule-constants2.patch
Patch16: python-2.6-rpath.patch
Patch17: python-2.6.4-distutils-rpath.patch
Patch55: 00055-systemtap.patch
Patch102: python-2.7.3-lib64.patch
Patch103: python-2.7-lib64-sysconfig.patch
Patch104: 00104-lib64-fix-for-test_install.patch
Patch111: 00111-no-static-lib.patch
Patch112: python-2.7.3-debug-build.patch
Patch113: 00113-more-configuration-flags.patch
Patch114: 00114-statvfs-f_flag-constants.patch
Patch121: 00121-add-Modules-to-build-path.patch
Patch125: 00125-less-verbose-COUNT_ALLOCS.patch
Patch128: python-2.7.1-fix_test_abc_with_COUNT_ALLOCS.patch
Patch130: python-2.7.2-add-extension-suffix-to-python-config.patch
Patch131: 00131-disable-tests-in-test_io.patch
Patch132: 00132-add-rpmbuild-hooks-to-unittest.patch
Patch133: 00133-skip-test_dl.patch
Patch134: 00134-fix-COUNT_ALLOCS-failure-in-test_sys.patch
Patch135: 00135-skip-test-within-test_weakref-in-debug-build.patch
Patch136: 00136-skip-tests-of-seeking-stdin-in-rpmbuild.patch
Patch137: 00137-skip-distutils-tests-that-fail-in-rpmbuild.patch
Patch138: 00138-fix-distutils-tests-in-debug-build.patch
Patch139: 00139-skip-test_float-known-failure-on-arm.patch
Patch140: 00140-skip-test_ctypes-known-failure-on-sparc.patch
Patch141: 00141-fix-test_gc_with_COUNT_ALLOCS.patch
Patch142: 00142-skip-failing-pty-tests-in-rpmbuild.patch
Patch143: 00143-tsc-on-ppc.patch
Patch144: 00144-no-gdbm.patch
Patch146: 00146-hashlib-fips.patch
Patch147: 00147-add-debug-malloc-stats.patch
Patch153: 00153-fix-test_gdb-noise.patch
Patch155: 00155-avoid-ctypes-thunks.patch
Patch156: 00156-gdb-autoload-safepath.patch
Patch157: 00157-uid-gid-overflows.patch
Patch165: 00165-crypt-module-salt-backport.patch
Patch166: 00166-fix-fake-repr-in-gdb-hooks.patch
Patch167: 00167-disable-stack-navigation-tests-when-optimized-in-test_gdb.patch
Patch168: 00168-distutils-cflags.patch
Patch169: 00169-avoid-implicit-usage-of-md5-in-multiprocessing.patch
Patch170: 00170-gc-assertions.patch
Patch173: 00173-workaround-ENOPROTOOPT-in-bind_port.patch
Patch174: 00174-fix-for-usr-move.patch
Patch180: 00180-python-add-support-for-ppc64p7.patch
Patch181: 00181-allow-arbitrary-timeout-in-condition-wait.patch
Patch184: 00184-ctypes-should-build-with-libffi-multilib-wrapper.patch
Patch185: 00185-urllib2-honors-noproxy-for-ftp.patch
Patch187: 00187-add-RPATH-to-pyexpat.patch
Patch189: 00189-gdb-py-bt-dont-raise-exception-from-eval.patch
Patch190: 00190-get_python_version.patch
Patch191: 00191-disable-NOOP.patch
Patch192: 00192-buffer-overflow.patch
Patch193: 00193-enable-loading-sqlite-extensions.patch
Patch5000: 05000-autotool-intermediates.patch


# ======================================================
# Additional metadata, and subpackages
# ======================================================

%if %{main_python}
Obsoletes: Distutils
Provides: Distutils
Obsoletes: python2 
Provides: python2 = %{version}
Obsoletes: python-elementtree <= 1.2.6
Obsoletes: python-sqlite < 2.3.2
Provides: python-sqlite = 2.3.2
Obsoletes: python-ctypes < 1.0.1
Provides: python-ctypes = 1.0.1
Obsoletes: python-hashlib < 20081120
Provides: python-hashlib = 20081120
Obsoletes: python-uuid < 1.31
Provides: python-uuid = 1.31

Provides: /bin/python

# python-argparse is part of python as of version 2.7
# drop this Provides in F17
# (having Obsoletes here caused problems with multilib; see rhbz#667984)
Provides:   python-argparse = %{version}-%{release}
%endif

Provides: %{_bindir}/python
Provides: %{_bindir}/python2
Provides: %{_bindir}/python2.7

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

URL: http://www.python.org/

%description
Python is an interpreted, interactive, object-oriented programming
language often compared to Tcl, Perl, Scheme or Java. Python includes
modules, classes, exceptions, very high level dynamic data types and
dynamic typing. Python supports interfaces to many system calls and
libraries, as well as to various windowing systems (X11, Motif, Tk,
Mac and MFC).

Programmers can write new built-in modules for Python in C or C++.
Python can be used as an extension language for applications that need
a programmable interface.

Note that documentation for Python is provided in the python-docs
package.

This package provides the "python" executable; most of the actual
implementation is within the "python-libs" package.

%package libs
Summary: Runtime libraries for Python
Group: Applications/System

# Needed for ctypes, to load libraries, worked around for Live CDs size
# Requires: binutils

%description libs
This package contains runtime libraries for use by Python:
- the libpython dynamic library, for use by applications that embed Python as
a scripting language, and by the main "python" executable
- the Python standard library

%package devel
Summary: The libraries and header files needed for Python development
Group: Development/Libraries
Requires: %{python}%{?_isa} = %{version}-%{release}
Requires: pkgconfig
# Needed here because of the migration of Makefile from -devel to the main
# package
Conflicts: %{python} < %{version}-%{release}
%if %{main_python}
Obsoletes: python2-devel
Provides: python2-devel = %{version}-%{release}
%endif

%description devel
The Python programming language's interpreter can be extended with
dynamically loaded extensions and can be embedded in other programs.
This package contains the header files and libraries needed to do
these types of tasks.

Install python-devel if you want to develop Python extensions.  The
python package will also need to be installed.  You'll probably also
want to install the python-docs package, which contains Python
documentation.

%package tools
Summary: A collection of development tools included with Python
Group: Development/Tools
Requires: %{name} = %{version}-%{release}
Requires: %{tkinter} = %{version}-%{release}
%if %{main_python}
Obsoletes: python2-tools
Provides: python2-tools = %{version}
%endif

%description tools
This package includes several tools to help with the development of Python   
programs, including IDLE (an IDE with editing and debugging facilities), a 
color editor (pynche), and a python gettext program (pygettext.py).  

%package -n %{tkinter}
Summary: A graphical user interface for the Python scripting language
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
%if %{main_python}
Obsoletes: tkinter2
Provides: tkinter2 = %{version}
%endif

%description -n %{tkinter}

The Tkinter (Tk interface) program is an graphical user interface for
the Python scripting language.

You should install the tkinter package if you'd like to use a graphical
user interface for Python programming.

%package test
Summary: The test modules from the main python package
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description test

The test modules from the main python package: %{name}
These have been removed to save space, as they are never or almost
never used in production.

You might want to install the python-test package if you're developing python
code that uses more than just unittest and/or test_support.py.

%if %{with_debug_build}
%package debug
Summary: Debug version of the Python runtime
Group: Applications/System

# The debug build is an all-in-one package version of the regular build, and
# shares the same .py/.pyc files and directories as the regular build.  Hence
# we depend on all of the subpackages of the regular build:
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: %{name}-libs%{?_isa} = %{version}-%{release}
Requires: %{name}-devel%{?_isa} = %{version}-%{release}
Requires: %{name}-test%{?_isa} = %{version}-%{release}
Requires: tkinter%{?_isa} = %{version}-%{release}
Requires: %{name}-tools%{?_isa} = %{version}-%{release}

%description debug
python-debug provides a version of the Python runtime with numerous debugging
features enabled, aimed at advanced Python users, such as developers of Python
extension modules.

This version uses more memory and will be slower than the regular Python build,
but is useful for tracking down reference-counting issues, and other bugs.

The bytecodes are unchanged, so that .pyc files are compatible between the two
version of Python, but the debugging features mean that C/C++ extension modules
are ABI-incompatible with those built for the standard runtime.

It shares installation directories with the standard Python runtime, so that
.py and .pyc files can be shared.  All compiled extension modules gain a "_d"
suffix ("foo_d.so" rather than "foo.so") so that each Python implementation can
load its own extensions.
%endif # with_debug_build


# ======================================================
# The prep phase of the build:
# ======================================================

%prep
%setup -q -n Python-%{version}

%if 0%{?with_systemtap}
# Provide an example of usage of the tapset:
cp -a %{SOURCE4} .
cp -a %{SOURCE5} .
%endif # with_systemtap

# Ensure that we're using the system copy of various libraries, rather than
# copies shipped by upstream in the tarball:
#   Remove embedded copy of expat:
rm -r Modules/expat || exit 1

#   Remove embedded copy of libffi:
for SUBDIR in darwin libffi libffi_arm_wince libffi_msvc libffi_osx ; do
  rm -r Modules/_ctypes/$SUBDIR || exit 1 ;
done

#   Remove embedded copy of zlib:
rm -r Modules/zlib || exit 1

# Don't build upstream Python's implementation of these crypto algorithms;
# instead rely on _hashlib and OpenSSL.
#
# For example, in our builds md5.py uses always uses hashlib.md5 (rather than
# falling back to _md5 when hashlib.md5 is not available); hashlib.md5 is
# implemented within _hashlib via OpenSSL (and thus respects FIPS mode)
for f in md5module.c md5.c shamodule.c sha256module.c sha512module.c; do
    rm Modules/$f
done

#
# Apply patches:
#
%patch0 -p1 -b .rhconfig
%patch1 -p1 -b .no_gui
%patch4 -p1 -b .cflags
%patch6 -p1 -b .plural
%patch7 -p1

# Try not disabling egg-infos, bz#414711
#patch50 -p1 -b .egginfo

# patch101: upstream as of Python 2.7.4
%if "%{_lib}" == "lib64"
%patch102 -p1 -b .lib64
%patch103 -p1 -b .lib64-sysconfig
%patch104 -p1
%endif

%patch10 -p1 -b .binutils-no-dep
# patch11: upstream as of Python 2.7.3
%patch13 -p1 -b .socketmodule
%patch14 -p1 -b .socketmodule2
%patch16 -p1 -b .rpath
%patch17 -p1 -b .distutils-rpath

%if 0%{?with_systemtap}
%patch55 -p1 -b .systemtap
%endif

%patch111 -p1 -b .no-static-lib

%patch112 -p1 -b .debug-build

%patch113 -p1 -b .more-configuration-flags

%patch114 -p1 -b .statvfs-f-flag-constants

# patch115: upstream as of Python 2.7.3

%patch121 -p1
%patch125 -p1 -b .less-verbose-COUNT_ALLOCS
# 00126: upstream as of Python 2.7.5
# 00127: upstream as of Python 2.7.5
%patch128 -p1

%patch130 -p1

%ifarch ppc %{power64}
%patch131 -p1
%endif

%patch132 -p1
%patch133 -p1
%patch134 -p1
%patch135 -p1
%patch136 -p1
%patch137 -p1
%patch138 -p1
%ifarch %{arm}
%patch139 -p1
%endif
%ifarch %{sparc}
%patch140 -p1
%endif
%patch141 -p1
%patch142 -p1
%patch143 -p1 -b .tsc-on-ppc
%if !%{with_gdbm}
%patch144 -p1
%endif
# 00145: upstream as of Python 2.7.3
%patch146 -p1
%patch147 -p1
# 00148: upstream as of Python 2.7.3
# 00149: not for python 2
# 00150: not for python 2
# 00151: upstream as of Python 2.7.3
# 00152: not for python 2
%patch153 -p0
# 00154: not for python 2
%patch155 -p1
%patch156 -p1
%patch157 -p1
# 00158: upstream as of Python 2.7.4
# 00160: not for python 2
# 00161: not for python 2 yet
# 00162: not for python 2 yet
# 00163: not for python 2 yet
# 00164: not for python 2 yet
%patch165 -p1
mv Modules/cryptmodule.c Modules/_cryptmodule.c
%patch166 -p1
%patch167 -p1
%patch168 -p1
%patch169 -p1
%patch170 -p1
# 00171: upstream as of Python 2.7.4
# 00171: upstream as of Python 2.7.4
%patch173 -p1
%patch174 -p1 -b .fix-for-usr-move
# 00175: upstream as of Python 2.7.5
# 00176: not for python 2
# 00177: not for python 2
# 00178: not for python 2
# 00179: not for python 2
%patch180 -p1
%patch181 -p1
# 00182: not for python 2
# 00183: not for python 2
%patch184 -p1
%patch185 -p1
%patch187 -p1
%patch189 -p1
%patch190 -p1
%patch191 -p1
%patch192 -p1
%patch193 -p1


# This shouldn't be necesarry, but is right now (2.2a3)
find -name "*~" |xargs rm -f

%if ! 0%{regenerate_autotooling_patch}
# Normally we apply the patch to "configure"
# We don't apply the patch if we're working towards regenerating it
%patch5000 -p0 -b .autotool-intermediates
%endif

# ======================================================
# Configuring and building the code:
# ======================================================

%build
topdir=$(pwd)
export CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE -fPIC -fwrapv"
export CXXFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE -fPIC -fwrapv"
export CPPFLAGS="$(pkg-config --cflags-only-I libffi)"
export OPT="$RPM_OPT_FLAGS -D_GNU_SOURCE -fPIC -fwrapv"
export LINKCC="gcc"
if pkg-config openssl ; then
  export CFLAGS="$CFLAGS $(pkg-config --cflags openssl)"
  export LDFLAGS="$LDFLAGS $(pkg-config --libs-only-L openssl)"
fi
# Force CC
export CC=gcc

%if 0%{regenerate_autotooling_patch}
# If enabled, this code regenerates the patch to "configure", using a
# local copy of autoconf-2.65, then exits the build
#
# The following assumes that the copy is installed to ~/autoconf-2.65/bin
# as per these instructions:
#   http://bugs.python.org/issue7997

for f in pyconfig.h.in configure ; do
    cp $f $f.autotool-intermediates ;
done

# Rerun the autotools:
PATH=~/autoconf-2.65/bin:$PATH autoconf
autoheader

# Regenerate the patch:
gendiff . .autotool-intermediates > %{PATCH5000}


# Exit the build
exit 1
%endif

# Define a function, for how to perform a "build" of python for a given
# configuration:
BuildPython() {
  ConfName=$1	      
  BinaryName=$2
  SymlinkName=$3
  ExtraConfigArgs=$4
  PathFixWithThisBinary=$5

  ConfDir=build/$ConfName

  echo STARTING: BUILD OF PYTHON FOR CONFIGURATION: $ConfName - %{_bindir}/$BinaryName
  mkdir -p $ConfDir

  pushd $ConfDir

  # Use the freshly created "configure" script, but in the directory two above:
%global _configure $topdir/configure

%configure \
  --enable-ipv6 \
  --enable-unicode=%{unicode} \
  --enable-shared \
  --with-system-ffi \
%if 0%{?with_valgrind}
  --with-valgrind \
%endif
%if 0%{?with_systemtap}
  --with-dtrace \
  --with-tapset-install-dir=%{tapsetdir} \
%endif
  --with-system-expat \
  $ExtraConfigArgs \
  %{nil}

make EXTRA_CFLAGS="$CFLAGS" %{?_smp_mflags}

# We need to fix shebang lines across the full source tree.
#
# We do this using the pathfix.py script, which requires one of the
# freshly-built Python binaries.
#
# We use the optimized python binary, and make the shebangs point at that same
# optimized python binary:
if $PathFixWithThisBinary
then
  LD_LIBRARY_PATH="$topdir/$ConfDir" ./$BinaryName \
    $topdir/Tools/scripts/pathfix.py \
      -i "%{_bindir}/env $BinaryName" \
      $topdir
fi

# Rebuild with new python
# We need a link to a versioned python in the build directory
ln -s $BinaryName $SymlinkName
LD_LIBRARY_PATH="$topdir/$ConfDir" PATH=$PATH:$topdir/$ConfDir make -s EXTRA_CFLAGS="$CFLAGS" %{?_smp_mflags}

  popd
  echo FINISHED: BUILD OF PYTHON FOR CONFIGURATION: $ConfDir
}

# Use "BuildPython" to support building with different configurations:

%if %{with_debug_build}
BuildPython debug \
  python-debug \
  python%{pybasever}-debug \
%ifarch %{ix86} x86_64 ppc ppc64
  "--with-pydebug --with-tsc --with-count-allocs --with-call-profile" \
%else
  "--with-pydebug --with-count-allocs --with-call-profile" \
%endif
  false
%endif # with_debug_build

BuildPython optimized \
  python \
  python%{pybasever} \
  "" \
  true


# ======================================================
# Installing the built code:
# ======================================================

%install
topdir=$(pwd)
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_prefix} %{buildroot}%{_mandir}

# Clean up patched .py files that are saved as .lib64
for f in distutils/command/install distutils/sysconfig; do
    rm -f Lib/$f.py.lib64
done

InstallPython() {

  ConfName=$1	      
  BinaryName=$2
  PyInstSoName=$3

  ConfDir=build/$ConfName

  echo STARTING: INSTALL OF PYTHON FOR CONFIGURATION: $ConfName - %{_bindir}/$BinaryName
  mkdir -p $ConfDir

  pushd $ConfDir

make install DESTDIR=%{buildroot}

# We install a collection of hooks for gdb that make it easier to debug
# executables linked against libpython (such as /usr/lib/python itself)
#
# These hooks are implemented in Python itself
#
# gdb-archer looks for them in the same path as the ELF file, with a -gdb.py suffix.
# We put them in the debuginfo package by installing them to e.g.:
#  /usr/lib/debug/usr/lib/libpython2.6.so.1.0.debug-gdb.py
# (note that the debug path is /usr/lib/debug for both 32/64 bit)
#
# See https://fedoraproject.org/wiki/Features/EasierPythonDebugging for more
# information
# 
# Initially I tried:
#  /usr/lib/libpython2.6.so.1.0-gdb.py
# but doing so generated noise when ldconfig was rerun (rhbz:562980)
#
%if %{with_gdb_hooks}
DirHoldingGdbPy=%{_prefix}/lib/debug/%{_libdir}
PathOfGdbPy=$DirHoldingGdbPy/$PyInstSoName.debug-gdb.py

mkdir -p %{buildroot}$DirHoldingGdbPy
cp $topdir/Tools/gdb/libpython.py %{buildroot}$PathOfGdbPy

# Manually byte-compile the file, in case find-debuginfo.sh is run before
# brp-python-bytecompile, so that the .pyc/.pyo files are properly listed in
# the debuginfo manifest:
LD_LIBRARY_PATH="$topdir/$ConfDir" $topdir/$ConfDir/$BinaryName \
  -c "import compileall; import sys; compileall.compile_dir('%{buildroot}$DirHoldingGdbPy', ddir='$DirHoldingGdbPy')"

LD_LIBRARY_PATH="$topdir/$ConfDir" $topdir/$ConfDir/$BinaryName -O \
  -c "import compileall; import sys; compileall.compile_dir('%{buildroot}$DirHoldingGdbPy', ddir='$DirHoldingGdbPy')"
%endif # with_gdb_hooks

  popd

  echo FINISHED: INSTALL OF PYTHON FOR CONFIGURATION: $ConfName
}

# Use "InstallPython" to support building with different configurations:

# Install the "debug" build first, so that we can move some files aside
%if %{with_debug_build}
InstallPython debug \
  python%{pybasever}-debug \
  %{py_INSTSONAME_debug}
%endif # with_debug_build

# Now the optimized build:
InstallPython optimized \
  python%{pybasever} \
  %{py_INSTSONAME_optimized}


# Fix the interpreter path in binaries installed by distutils 
# (which changes them by itself)
# Make sure we preserve the file permissions
for fixed in %{buildroot}%{_bindir}/pydoc; do
    sed 's,#!.*/python$,#!%{_bindir}/env python%{pybasever},' $fixed > $fixed- \
        && cat $fixed- > $fixed && rm -f $fixed-
done

# Junk, no point in putting in -test sub-pkg
rm -f %{buildroot}/%{pylibdir}/idlelib/testcode.py*

# don't include tests that are run at build time in the package
# This is documented, and used: rhbz#387401
if /bin/false; then
 # Move this to -test subpackage.
mkdir save_bits_of_test
for i in test_support.py __init__.py; do
  cp -a %{buildroot}/%{pylibdir}/test/$i save_bits_of_test
done
rm -rf %{buildroot}/%{pylibdir}/test
mkdir %{buildroot}/%{pylibdir}/test
cp -a save_bits_of_test/* %{buildroot}/%{pylibdir}/test
fi

%if %{main_python}
%if %{with_debug_build}
ln -s python%{pybasever}-debug-config %{buildroot}%{_bindir}/python-debug-config
ln -s python-debug %{buildroot}%{_bindir}/python2-debug
%endif # with_debug_build
%else
mv %{buildroot}%{_bindir}/python %{buildroot}%{_bindir}/%{python}
%if %{with_debug_build}
mv %{buildroot}%{_bindir}/python-debug %{buildroot}%{_bindir}/%{python}-debug
%endif # with_debug_build
cp %{buildroot}/%{_mandir}/man1/python.1 %{buildroot}/%{_mandir}/man1/python%{pybasever}.1
%endif

# tools

mkdir -p ${RPM_BUILD_ROOT}%{site_packages}

#pynche
cat > ${RPM_BUILD_ROOT}%{_bindir}/pynche << EOF
#!/bin/bash
exec %{site_packages}/pynche/pynche
EOF
chmod 755 ${RPM_BUILD_ROOT}%{_bindir}/pynche
rm -f Tools/pynche/*.pyw
cp -r Tools/pynche \
  ${RPM_BUILD_ROOT}%{site_packages}/

mv Tools/pynche/README Tools/pynche/README.pynche

#gettext
install -m755  Tools/i18n/pygettext.py %{buildroot}%{_bindir}/
install -m755  Tools/i18n/msgfmt.py %{buildroot}%{_bindir}/

# Useful development tools
install -m755 -d %{buildroot}%{tools_dir}/scripts
install Tools/README %{buildroot}%{tools_dir}/
install Tools/scripts/*py %{buildroot}%{tools_dir}/scripts/

# Documentation tools
install -m755 -d %{buildroot}%{doc_tools_dir}
#install -m755 Doc/tools/mkhowto %{buildroot}%{doc_tools_dir}

# Useful demo scripts
install -m755 -d %{buildroot}%{demo_dir}
cp -ar Demo/* %{buildroot}%{demo_dir}

# Get rid of crap
find %{buildroot}/ -name "*~"|xargs rm -f
find %{buildroot}/ -name ".cvsignore"|xargs rm -f
find %{buildroot}/ -name "*.bat"|xargs rm -f
find . -name "*~"|xargs rm -f
find . -name ".cvsignore"|xargs rm -f
#zero length
rm -f %{buildroot}%{pylibdir}/LICENSE.txt


#make the binaries install side by side with the main python
%if !%{main_python}
pushd %{buildroot}%{_bindir}
mv idle idle%{__python_ver}
mv pynche pynche%{__python_ver}
mv pygettext.py pygettext%{__python_ver}.py
mv msgfmt.py msgfmt%{__python_ver}.py
mv smtpd.py smtpd%{__python_ver}.py
mv pydoc pydoc%{__python_ver}
popd
%endif

# Fix for bug #136654
rm -f %{buildroot}%{pylibdir}/email/test/data/audiotest.au %{buildroot}%{pylibdir}/test/audiotest.au

# Fix bug #143667: python should own /usr/lib/python2.x on 64-bit machines
%if "%{_lib}" == "lib64"
install -d %{buildroot}/usr/lib/python%{pybasever}/site-packages
%endif

# Make python-devel multilib-ready (bug #192747, #139911)
%global _pyconfig32_h pyconfig-32.h
%global _pyconfig64_h pyconfig-64.h

%ifarch ppc64 s390x x86_64 ia64 alpha sparc64
%global _pyconfig_h %{_pyconfig64_h}
%else
%global _pyconfig_h %{_pyconfig32_h}
%endif

%if %{with_debug_build}
%global PyIncludeDirs python%{pybasever} python%{pybasever}-debug
%else
%global PyIncludeDirs python%{pybasever}
%endif

for PyIncludeDir in %{PyIncludeDirs} ; do
  mv %{buildroot}%{_includedir}/$PyIncludeDir/pyconfig.h \
     %{buildroot}%{_includedir}/$PyIncludeDir/%{_pyconfig_h}
  cat > %{buildroot}%{_includedir}/$PyIncludeDir/pyconfig.h << EOF
#include <bits/wordsize.h>

#if __WORDSIZE == 32
#include "%{_pyconfig32_h}"
#elif __WORDSIZE == 64
#include "%{_pyconfig64_h}"
#else
#error "Unknown word size"
#endif
EOF
done
ln -s ../../libpython%{pybasever}.so %{buildroot}%{pylibdir}/config/libpython%{pybasever}.so

# Fix for bug 201434: make sure distutils looks at the right pyconfig.h file
# Similar for sysconfig: sysconfig.get_config_h_filename tries to locate
# pyconfig.h so it can be parsed, and needs to do this at runtime in site.py
# when python starts up.
#
# Split this out so it goes directly to the pyconfig-32.h/pyconfig-64.h
# variants:
sed -i -e "s/'pyconfig.h'/'%{_pyconfig_h}'/" \
  %{buildroot}%{pylibdir}/distutils/sysconfig.py \
  %{buildroot}%{pylibdir}/sysconfig.py

# Install macros for rpm:
mkdir -p %{buildroot}/%{_sysconfdir}/rpm
install -m 644 %{SOURCE6} %{buildroot}/%{_sysconfdir}/rpm

# Ensure that the curses module was linked against libncursesw.so, rather than
# libncurses.so (bug 539917)
ldd %{buildroot}/%{dynload_dir}/_curses*.so \
    | grep curses \
    | grep libncurses.so && (echo "_curses.so linked against libncurses.so" ; exit 1)

# Ensure that the debug modules are linked against the debug libpython, and
# likewise for the optimized modules and libpython:
for Module in %{buildroot}/%{dynload_dir}/*.so ; do
    case $Module in
    *_d.so)
        ldd $Module | grep %{py_INSTSONAME_optimized} &&
            (echo Debug module $Module linked against optimized %{py_INSTSONAME_optimized} ; exit 1)
            
        ;;
    *)
        ldd $Module | grep %{py_INSTSONAME_debug} &&
            (echo Optimized module $Module linked against debug %{py_INSTSONAME_optimized} ; exit 1)
        ;;
    esac
done

#
# Systemtap hooks:
#
%if 0%{?with_systemtap}
# Install a tapset for this libpython into tapsetdir, fixing up the path to the
# library:
mkdir -p %{buildroot}%{tapsetdir}
%ifarch ppc64 s390x x86_64 ia64 alpha sparc64
%global libpython_stp_optimized libpython%{pybasever}-64.stp
%global libpython_stp_debug     libpython%{pybasever}-debug-64.stp
%else
%global libpython_stp_optimized libpython%{pybasever}-32.stp
%global libpython_stp_debug     libpython%{pybasever}-debug-32.stp
%endif

sed \
   -e "s|LIBRARY_PATH|%{_libdir}/%{py_INSTSONAME_optimized}|" \
   %{SOURCE3} \
   > %{buildroot}%{tapsetdir}/%{libpython_stp_optimized}

%if %{with_debug_build}
sed \
   -e "s|LIBRARY_PATH|%{_libdir}/%{py_INSTSONAME_debug}|" \
   %{SOURCE3} \
   > %{buildroot}%{tapsetdir}/%{libpython_stp_debug}
%endif # with_debug_build
%endif # with_systemtap


# ======================================================
# Running the upstream test suite
# ======================================================

%if %{do_test}
%check
topdir=$(pwd)
CheckPython() {
  ConfName=$1
  BinaryName=$2
  ConfDir=$(pwd)/build/$ConfName

  echo STARTING: CHECKING OF PYTHON FOR CONFIGURATION: $ConfName

  # Note that we're running the tests using the version of the code in the
  # builddir, not in the buildroot.

  pushd $ConfDir

  EXTRATESTOPTS="--verbose"

%if 0%{?with_huntrleaks}
  # Try to detect reference leaks on debug builds.  By default this means
  # running every test 10 times (6 to stabilize, then 4 to watch):
  if [ "$ConfName" = "debug"  ] ; then
    EXTRATESTOPTS="$EXTRATESTOPTS --huntrleaks : "
  fi
%endif

  # Run the upstream test suite, setting "WITHIN_PYTHON_RPM_BUILD" so that the
  # our non-standard decorators take effect on the relevant tests:
  #   @unittest._skipInRpmBuild(reason)
  #   @unittest._expectedFailureInRpmBuild
  WITHIN_PYTHON_RPM_BUILD= EXTRATESTOPTS="$EXTRATESTOPTS" make test

  popd

  echo FINISHED: CHECKING OF PYTHON FOR CONFIGURATION: $ConfName

}

%if %{run_selftest_suite}

# Check each of the configurations:
%if %{with_debug_build}
CheckPython \
  debug \
  python%{pybasever}-debug
%endif # with_debug_build
CheckPython \
  optimized \
  python%{pybasever}

%endif # run_selftest_suite

%endif # do_test

# ======================================================
# Cleaning up
# ======================================================

%clean
rm -fr %{buildroot}


# ======================================================
# Scriptlets
# ======================================================

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig



%files
%defattr(-, root, root, -)
%doc LICENSE README
%{_bindir}/pydoc*
%{_bindir}/%{python}
%if %{main_python}
%{_bindir}/python2
%endif
%{_bindir}/python%{pybasever}
%{_mandir}/*/*

%files libs
%defattr(-,root,root,-)
%doc LICENSE README
%dir %{pylibdir}
%dir %{dynload_dir}
%{dynload_dir}/Python-%{version}-py%{pybasever}.egg-info
%{dynload_dir}/_bisectmodule.so
%{dynload_dir}/_bsddb.so
%{dynload_dir}/_codecs_cn.so
%{dynload_dir}/_codecs_hk.so
%{dynload_dir}/_codecs_iso2022.so
%{dynload_dir}/_codecs_jp.so
%{dynload_dir}/_codecs_kr.so
%{dynload_dir}/_codecs_tw.so
%{dynload_dir}/_collectionsmodule.so
%{dynload_dir}/_csv.so
%{dynload_dir}/_ctypes.so
%{dynload_dir}/_curses.so
%{dynload_dir}/_curses_panel.so
%{dynload_dir}/_elementtree.so
%{dynload_dir}/_functoolsmodule.so
%{dynload_dir}/_hashlib.so
%{dynload_dir}/_heapq.so
%{dynload_dir}/_hotshot.so
%{dynload_dir}/_io.so
%{dynload_dir}/_json.so
%{dynload_dir}/_localemodule.so
%{dynload_dir}/_lsprof.so
%{dynload_dir}/_multibytecodecmodule.so
%{dynload_dir}/_multiprocessing.so
%{dynload_dir}/_randommodule.so
%{dynload_dir}/_socketmodule.so
%{dynload_dir}/_sqlite3.so
%{dynload_dir}/_ssl.so
%{dynload_dir}/_struct.so
%{dynload_dir}/arraymodule.so
%{dynload_dir}/audioop.so
%{dynload_dir}/binascii.so
%{dynload_dir}/bz2.so
%{dynload_dir}/cPickle.so
%{dynload_dir}/cStringIO.so
%{dynload_dir}/cmathmodule.so
%{dynload_dir}/_cryptmodule.so
%{dynload_dir}/datetime.so
%{dynload_dir}/dbm.so
%{dynload_dir}/dlmodule.so
%{dynload_dir}/fcntlmodule.so
%{dynload_dir}/future_builtins.so
%if %{with_gdbm}
%{dynload_dir}/gdbmmodule.so
%endif
%{dynload_dir}/grpmodule.so
%{dynload_dir}/imageop.so
%{dynload_dir}/itertoolsmodule.so
%{dynload_dir}/linuxaudiodev.so
%{dynload_dir}/math.so
%{dynload_dir}/mmapmodule.so
%{dynload_dir}/nismodule.so
%{dynload_dir}/operator.so
%{dynload_dir}/ossaudiodev.so
%{dynload_dir}/parsermodule.so
%{dynload_dir}/pyexpat.so
%{dynload_dir}/readline.so
%{dynload_dir}/resource.so
%{dynload_dir}/selectmodule.so
%{dynload_dir}/spwdmodule.so
%{dynload_dir}/stropmodule.so
%{dynload_dir}/syslog.so
%{dynload_dir}/termios.so
%{dynload_dir}/timemodule.so
%{dynload_dir}/timingmodule.so
%{dynload_dir}/unicodedata.so
%{dynload_dir}/xxsubtype.so
%{dynload_dir}/zlibmodule.so

%dir %{site_packages}
%{site_packages}/README
%{pylibdir}/*.py*
%{pylibdir}/*.doc
%{pylibdir}/wsgiref.egg-info
%dir %{pylibdir}/bsddb
%{pylibdir}/bsddb/*.py*
%{pylibdir}/compiler
%dir %{pylibdir}/ctypes
%{pylibdir}/ctypes/*.py*
%{pylibdir}/ctypes/macholib
%{pylibdir}/curses
%dir %{pylibdir}/distutils
%{pylibdir}/distutils/*.py*
%{pylibdir}/distutils/README
%{pylibdir}/distutils/command
%exclude %{pylibdir}/distutils/command/wininst-*.exe
%dir %{pylibdir}/email
%{pylibdir}/email/*.py*
%{pylibdir}/email/mime
%{pylibdir}/encodings
%{pylibdir}/hotshot
%{pylibdir}/idlelib
%{pylibdir}/importlib
%dir %{pylibdir}/json
%{pylibdir}/json/*.py*
%{pylibdir}/lib2to3
%{pylibdir}/logging
%{pylibdir}/multiprocessing
%{pylibdir}/plat-linux2
%{pylibdir}/pydoc_data
%dir %{pylibdir}/sqlite3
%{pylibdir}/sqlite3/*.py*
%dir %{pylibdir}/test
%{pylibdir}/test/test_support.py*
%{pylibdir}/test/__init__.py*
%{pylibdir}/unittest
%{pylibdir}/wsgiref
%{pylibdir}/xml
%if "%{_lib}" == "lib64"
%attr(0755,root,root) %dir %{_prefix}/lib/python%{pybasever}
%attr(0755,root,root) %dir %{_prefix}/lib/python%{pybasever}/site-packages
%endif

# "Makefile" and the config-32/64.h file are needed by
# distutils/sysconfig.py:_init_posix(), so we include them in the libs
# package, along with their parent directories (bug 531901):
%dir %{pylibdir}/config
%{pylibdir}/config/Makefile
%dir %{_includedir}/python%{pybasever}
%{_includedir}/python%{pybasever}/%{_pyconfig_h}

%{_libdir}/%{py_INSTSONAME_optimized}
%if 0%{?with_systemtap}
%{tapsetdir}/%{libpython_stp_optimized}
%doc systemtap-example.stp pyfuntop.stp
%endif

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/python-%{pybasever}.pc
%{_libdir}/pkgconfig/python.pc
%{_libdir}/pkgconfig/python2.pc
%{pylibdir}/config/*
%exclude %{pylibdir}/config/Makefile
%{pylibdir}/distutils/command/wininst-*.exe
%{_includedir}/python%{pybasever}/*.h
%exclude %{_includedir}/python%{pybasever}/%{_pyconfig_h}
%doc Misc/README.valgrind Misc/valgrind-python.supp Misc/gdbinit
%{_bindir}/python-config
%if %{main_python}
%{_bindir}/python2-config
%endif
%{_bindir}/python%{pybasever}-config
%{_libdir}/libpython%{pybasever}.so
%config(noreplace) %{_sysconfdir}/rpm/macros.python2

%files tools
%defattr(-,root,root,755)
%doc Tools/pynche/README.pynche
%{site_packages}/pynche
%{_bindir}/smtpd*.py*
%{_bindir}/2to3*
%{_bindir}/idle*
%{_bindir}/pynche*
%{_bindir}/pygettext*.py*
%{_bindir}/msgfmt*.py*
%{tools_dir}
%{demo_dir}
%{pylibdir}/Doc

%files -n %{tkinter}
%defattr(-,root,root,755)
%{pylibdir}/lib-tk
%{dynload_dir}/_tkinter.so

%files test
%defattr(-, root, root, -)
%{pylibdir}/bsddb/test
%{pylibdir}/ctypes/test
%{pylibdir}/distutils/tests
%{pylibdir}/email/test
%{pylibdir}/json/tests
%{pylibdir}/sqlite3/test
%{pylibdir}/test/*
# These two are shipped in the main subpackage:
%exclude %{pylibdir}/test/test_support.py*
%exclude %{pylibdir}/test/__init__.py*
%{dynload_dir}/_ctypes_test.so
%{dynload_dir}/_testcapimodule.so


# We don't bother splitting the debug build out into further subpackages:
# if you need it, you're probably a developer.

# Hence the manifest is the combination of analogous files in the manifests of
# all of the other subpackages

%if %{with_debug_build}
%files debug
%defattr(-,root,root,-)

# Analog of the core subpackage's files:
%{_bindir}/%{python}-debug
%if %{main_python}
%{_bindir}/python2-debug
%endif
%{_bindir}/python%{pybasever}-debug

# Analog of the -libs subpackage's files, with debug builds of the built-in
# "extension" modules:
%{dynload_dir}/_bisectmodule_d.so
%{dynload_dir}/_bsddb_d.so
%{dynload_dir}/_codecs_cn_d.so
%{dynload_dir}/_codecs_hk_d.so
%{dynload_dir}/_codecs_iso2022_d.so
%{dynload_dir}/_codecs_jp_d.so
%{dynload_dir}/_codecs_kr_d.so
%{dynload_dir}/_codecs_tw_d.so
%{dynload_dir}/_collectionsmodule_d.so
%{dynload_dir}/_csv_d.so
%{dynload_dir}/_ctypes_d.so
%{dynload_dir}/_curses_d.so
%{dynload_dir}/_curses_panel_d.so
%{dynload_dir}/_elementtree_d.so
%{dynload_dir}/_functoolsmodule_d.so
%{dynload_dir}/_hashlib_d.so
%{dynload_dir}/_heapq_d.so
%{dynload_dir}/_hotshot_d.so
%{dynload_dir}/_io_d.so
%{dynload_dir}/_json_d.so
%{dynload_dir}/_localemodule_d.so
%{dynload_dir}/_lsprof_d.so
%{dynload_dir}/_multibytecodecmodule_d.so
%{dynload_dir}/_multiprocessing_d.so
%{dynload_dir}/_randommodule_d.so
%{dynload_dir}/_socketmodule_d.so
%{dynload_dir}/_sqlite3_d.so
%{dynload_dir}/_ssl_d.so
%{dynload_dir}/_struct_d.so
%{dynload_dir}/arraymodule_d.so
%{dynload_dir}/audioop_d.so
%{dynload_dir}/binascii_d.so
%{dynload_dir}/bz2_d.so
%{dynload_dir}/cPickle_d.so
%{dynload_dir}/cStringIO_d.so
%{dynload_dir}/cmathmodule_d.so
%{dynload_dir}/cryptmodule_d.so
%{dynload_dir}/datetime_d.so
%{dynload_dir}/dbm_d.so
%{dynload_dir}/dlmodule_d.so
%{dynload_dir}/fcntlmodule_d.so
%{dynload_dir}/future_builtins_d.so
%if %{with_gdbm}
%{dynload_dir}/gdbmmodule_d.so
%endif
%{dynload_dir}/grpmodule_d.so
%{dynload_dir}/imageop_d.so
%{dynload_dir}/itertoolsmodule_d.so
%{dynload_dir}/linuxaudiodev_d.so
%{dynload_dir}/math_d.so
%{dynload_dir}/mmapmodule_d.so
%{dynload_dir}/nismodule_d.so
%{dynload_dir}/operator_d.so
%{dynload_dir}/ossaudiodev_d.so
%{dynload_dir}/parsermodule_d.so
%{dynload_dir}/pyexpat_d.so
%{dynload_dir}/readline_d.so
%{dynload_dir}/resource_d.so
%{dynload_dir}/selectmodule_d.so
%{dynload_dir}/spwdmodule_d.so
%{dynload_dir}/stropmodule_d.so
%{dynload_dir}/syslog_d.so
%{dynload_dir}/termios_d.so
%{dynload_dir}/timemodule_d.so
%{dynload_dir}/timingmodule_d.so
%{dynload_dir}/unicodedata_d.so
%{dynload_dir}/xxsubtype_d.so
%{dynload_dir}/zlibmodule_d.so

# No need to split things out the "Makefile" and the config-32/64.h file as we
# do for the regular build above (bug 531901), since they're all in one package
# now; they're listed below, under "-devel":

%{_libdir}/%{py_INSTSONAME_debug}
%if 0%{?with_systemtap}
%{tapsetdir}/%{libpython_stp_debug}
%endif

# Analog of the -devel subpackage's files:
%dir %{pylibdir}/config-debug
%{pylibdir}/config-debug/*
%{_includedir}/python%{pybasever}-debug/*.h
%if %{main_python}
%{_bindir}/python-debug-config
%endif
%{_bindir}/python%{pybasever}-debug-config
%{_libdir}/libpython%{pybasever}_d.so

# Analog of the -tools subpackage's files:
#  None for now; we could build precanned versions that have the appropriate
# shebang if needed

# Analog  of the tkinter subpackage's files:
%{dynload_dir}/_tkinter_d.so

# Analog  of the -test subpackage's files:
%{dynload_dir}/_ctypes_test_d.so
%{dynload_dir}/_testcapimodule_d.so

%endif # with_debug_build

# We put the debug-gdb.py file inside /usr/lib/debug to avoid noise from
# ldconfig (rhbz:562980).
# 
# The /usr/lib/rpm/redhat/macros defines %__debug_package to use
# debugfiles.list, and it appears that everything below /usr/lib/debug and
# (/usr/src/debug) gets added to this file (via LISTFILES) in
# /usr/lib/rpm/find-debuginfo.sh
# 
# Hence by installing it below /usr/lib/debug we ensure it is added to the
# -debuginfo subpackage
# (if it doesn't, then the rpmbuild ought to fail since the debug-gdb.py 
# payload file would be unpackaged)


# ======================================================
# Finally, the changelog:
# ======================================================

%changelog
* Mon Jun 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.6-2m)
- Provides /bin/python

* Mon Mar 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.6-1m)
- update 2.7.6

* Mon Jan 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.5-1m)
- update 2.7.5

* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-2m)
- [SECURITY] CVE-2013-4238

* Thu May  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.4-1m)
- update 2.7.4

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-1m)
- [SECURITY] CVE-2011-3389 CVE-2012-0845 CVE-2012-0876
- [SECURITY] fix hash collision denial of service (oCERT-2011-003, no CVE id)
- update to 2.7.3

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.2-7m)
- import fedora patches

* Tue Sep 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.2-6m)
- Provides: %%{_bindir}/python again
- Provides: %%{_bindir}/python2 again
- Provides: %%{_bindir}/python2.7 again

* Sun Sep 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.2-5m)
- merge changes from fedora
- add %%check section and %%do_test specopt
-- but disabled by default because some tests fail now

* Mon Sep 19 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.7.2-4m)
- sync Fedora
- disable with_gdb_hooks
- disable with_debug_build
- remove %%check section
- add Provides python-hashlib python-uuid python-argparse

* Tue Sep  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.2-3m)
- add patch for linux 3.0

* Sun Jun 26 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.7.2-2m)
- build fix for Linux 3.0

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.2-1m)
- update 2.7.2

* Sat Jun  4 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-5m)
- move %%{pylibdir}/config/Makefile from package devel to main for yum
- move %%{_includedir}/python%%{pybasever}/%%{_pyconfig_h} from package devel to main

* Thu Jun  2 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-4m)
- modify %%files to avoid conflicting again
- %%{pylibdir}/config/Makefile is contained in package devel

* Thu Jun  2 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-3m)
- modify %%files to avoid conflicting
- %%{_pyconfig_h} is contained in package devel

* Mon May 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-2m)
- move config file. python-devel to python

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-1m)
- update 2.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.5-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.5-5m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-4m)
- fix 2to3 (Patch201)
-- http://bugs.python.org/issue8892
- REMOVEME.abrt-addon-python

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-3m)
- [SECURITY] CVE-2010-1634 CVE-2010-2089 CVE-2008-5983
- import security patches from Fedora 13 (2.6.4-27)

* Mon May 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.5-2m)
- add LDSHARED='$(CC) -shared -lpython%{pybasever}'

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.5-1m)
- update 2.6.5

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.4-5m)
- rebuild against readline6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-4m)
- rebuild against openssl-1.0.0

* Sun Feb  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-3m)
- fix build failure on i686

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-2m)
- rebuild against db-4.8.26
-- import Patch53,54 from Rawhide (2.6.4-4)

* Fri Jan 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.4-1m)
- update python-2.6.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-2m)
- [SECURITY] CVE-2009-3720
- apply Patch1001

* Wed Jun 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.2-1m)
- update python-2.6.2

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-8m)
- rebuild against tix-8.4.3-1m

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-7m)
- rebuild against openssl-0.9.8k

* Mon Mar 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-6m)
- import Patch17 from Momonga 4

* Mon Mar 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-5m)
- export CFLAGS CXXFLAGS OPT

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-3m)
- fix %%files to avoid conflicting

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-2m)
- update Patch0
-- add workaround to avoid _PyTime_DoubleToTimet missing error
   when importing datetime module (BTS#209)

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.1-1m)
- [SECURITY] CVE-2008-5031
- update to 2.6.1

* Sun Nov  9 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.2-3m)
- [SECURITY] CVE-2008-1679 CVE-2008-1721 CVE-2008-1887
- [SECURITY] CVE-2008-2315 CVE-2008-3142 CVE-2008-3144
- apply security patches
-- Patch300,301,302,306,307 from ubuntu (2.5.1-5ubuntu5.2)
-- Patch303 from Debian unstable (2.5.2-11.1)

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.2-2m)
- revise %%files to avoid conflicting

* Wed Oct  8 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.2-1m)
- [SECURITY] CVE-2008-2316 CVE-2008-3143 CVE-2008-4864
- update to 2.5.2
- build against db4-4.7.25

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-12m)
- rebuild against openssl-0.9.8h-1m

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.1-11m)
- remove python-2.5-disable-egginfo.patch

* Mon Apr 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.1-10m)
- import fedora patches. (2.5.1-24.fc9)

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.1-9m)
- rebuild against tcl-8.5.2 and tk-8.5.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.1-8m)
- rebuild against gcc43

* Mon Feb 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.1-7m)
- enable Patch50: python-2.5-disable-egginfo.patch

* Sun Feb 10 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.5.1-6m)
- rollback of python-2.5-lib64.patch

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.1-5m)
- No use ncurses

* Sun Nov 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-4m)
- [SECURITY] CVE-2007-4965
- import security patch from upstream, see http://bugs.python.org/issue1179

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.1-3m)
- reubild against db4-2.6.2.1-1m
- update Patch0: python-2.5-db46-config.patch

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.1-2m)
- fix %%changelog section

* Mon Jun  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.5.1-1m)
- [SECURITY] CVE-2007-2052
- update 2.5.1
- remove Patch2 python-2.5-distutils-bdist-rpm.patch
-        Patch6 python-db45.patch
-        Patch7 python-ctypes-execstack.patch
-        Patch25 python-syslog-fail-noatexittb.patch
-        Patch26 python-2.5-fix-invalid-assert.patch

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5-12m)
- remove Obsoletes: python-docs, it's now available

* Sun Apr  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5-11m)
- sync fc 2.5.3-11

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5-10m)
- revival pyc pyo

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.5-9m)
- revised python-2.5-lib64.patch

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5-8m)
- delete pyc pyo

* Tue Jan 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-7m)
- clean up spec
- update 2.5-r53494

* Mon Jan 01 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.5-6m)
- add python-2.5-gdbm_compat.patch

* Wed Dec 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.5-5m)
- fix wrong python path in /usr/bin/python2.5-config
- fix %%install to make "-ba --short-circuit= %%install" option work

* Wed Dec 27 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-4m)
- add python-2.5-fix-invalid-assert.patch

* Mon Dec 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-3m)
- remove JapaneseCodecs

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-2m)
- build fix lib64 arch

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-1m)
- update 2.5

* Wed Dec 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.4-1m)
- update 2.4.4

* Tue Nov 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.3-3m)
- modify %%files to avoid conflicting

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.3-2m)
- Patch16: python-2.4.3-db45.patch
- rebuild against db-4.5

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.2-11m)
- rebuild against readline-5.0

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-10m)
- delete duplicated file

* Sun Jun  4 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.2-9m)
- Provide python-abi = %{basever}

* Mon May 22 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-8m)
- roll back 2.4.2


* Thu May  4 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.3-1m)

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.2-7m)
- rebuild against openssl-0.9.8a

* Sun Mar 26 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.2-6m)
- rebuild against tix-8.4.0

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-5m)
- revise for xorg-7.0
- FIX ME!: commnt out Mesa-devel 

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.2-4m)
- rebuild against db4.3

* Tue Nov 1 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.2-3m)
- change Provides: python-abi = %{pybasever} 
              to Provides: python(abi) = %{pybasever}

* Sun Oct 30 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-2m)
- remake japanese-codecs-lib64.patch

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Wed Oct 19 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Sun Jun 26 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.5-1m)
- update to 2.3.5
- add gcc4 patch

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (2.3.4-4m)
- rebuild against libtermcap and ncurses

* Wed Dec  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.4-3m)
- Provides: /usr/bin/python2

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.3.4-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.4-1m)
- version up
- import patch from FC

* Tue Jun 29 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.2.3-11m)
- modify Patch0 for db-4.2

* Mon Jun 28 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.2.3-10m)
- rebuild against tcl, tk
- modify BuildPrereq: for tkinter

* Tue Jun  1 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.3-8m)
- modify BuildPrereq: tcl, tk

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.3-7m)
- rebuild against ncurses 5.3.

* Sun Apr  4 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.3-6m)
- applied gdbm_compat patch.

* Sat Apr  3 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.3-6m)
- modified spec (duplicate file)
- import patch from FC1
- disable html document

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.3-5m)
- revised spec for rpm 4.2.

* Fri Jan 23 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.3-4m)
- add some missing modules (%{pylibdir}/{test,email,compiler,hotshot})

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.3-3m)
- accept gdbm-1.8.0 or newer

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.3-2m)
- rebuild against gdbm

* Sun Nov  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.2.3-1m)
- update to 2.2.3 (which is bugfix release of 2.2)

* Fri Oct 31 2003 zunda <zunda at freeshell.org>
- (2.2-21m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2-20m)
- pretty spec file.

* Mon Apr 14 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2-20m)
- fix niwatama (pathfix.py).

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2-19m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2-18m)
- rebuild against for gdbm

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2-17m)
  rebuild against libgdbm

* Sun Jun  2 2002 OZAWA -Crouton- Sakuro <crouton@weatherlight.org>
- (2.2-16k)
- Removed /usr/lib/python%{version}/lib-tk from python's %files,
  because tkinter owns it.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.2-14k)
- add Provides: /usr/bin/python

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (2.2-12k)
- why exclude alpha?

* Tue Mar 19 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.2-10k)
- tcltk 8.0.5 for sketch

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (2.2-8k)
- rename python2 to python

* Thu Jan 31 2002 Elliot Lee <sopwith@redhat.com> 2.2-7
- Use version and pybasever macros to make updating easy
- Use _smp_mflags macro

* Tue Jan 29 2002 Trond Eivind Glomsrod <teg@redhat.com> 2.2-6
- Add db4-devel to BuildPrereq

* Fri Jan 25 2002 Nalin Dahyabhai <nalin@redhat.com> 2.2-5
- disable ndbm support, which is db2 in disguise (really interesting things
  can happen when you mix db2 and db4 in a single application)

* Thu Jan 24 2002 Trond Eivind Glomsrod <teg@redhat.com> 2.2-4
- Obsolete subpackages if necesarry 
- provide versioned python2
- build with db4

* Wed Jan 16 2002 Trond Eivind Glomsrod <teg@redhat.com> 2.2-3
- Alpha toolchain broken. Disable build on alpha.
- New openssl

* Wed Dec 26 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.2-1
- 2.2 final

* Fri Dec 14 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.2-0.11c1
- 2.2 RC 1
- Don't include the _tkinter module in the main package - it's 
  already in the tkiter packace
- Turn off the mpzmodule, something broke in the buildroot

* Wed Nov 28 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.2-0.10b2
- Use -fPIC for OPT as well, in lack of a proper libpython.so

* Mon Nov 26 2001 Matt Wilson <msw@redhat.com> 2.2-0.9b2
- changed DESTDIR to point to / so that distutils will install dynload
  modules properly in the installroot

* Fri Nov 16 2001 Matt Wilson <msw@redhat.com> 2.2-0.8b2
- 2.2b2

* Fri Oct 26 2001 Matt Wilson <msw@redhat.com> 2.2-0.7b1
- python2ify

* Fri Oct 19 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.2-0.5b1
- 2.2b1

* Sun Sep 30 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.2-0.4a4
- 2.2a4
- Enable UCS4 support
- Enable IPv6
- Provide distutils
- Include msgfmt.py and pygettext.py

* Fri Sep 14 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.2-0.3a3
- Obsolete Distutils, which is now part of the main package
- Obsolete python2

* Thu Sep 13 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.2-0.2a3
- Add docs, tools and tkinter subpackages, to match the 1.5 layout

* Wed Sep 12 2001 Trond Eivind Glomsrod <teg@redhat.com> 2.2-0.1a3
- 2.2a3
- don't build tix and blt extensions

* Mon Aug 13 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add tk and tix to build dependencies

* Sat Jul 21 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1.1 bugfix release - with a GPL compatible license

* Fri Jul 20 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add new build dependencies (#49753)

* Tue Jun 26 2001 Nalin Dahyabhai <nalin@redhat.com>
- build with -fPIC

* Fri Jun  1 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 2.1
- reorganization of file includes

* Wed Dec 20 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fix the "requires" clause, it lacked a space causing problems
- use %%{_tmppath}
- don't define name, version etc
- add the available patches from the Python home page

* Fri Dec 15 2000 Matt Wilson <msw@redhat.com>
- added devel subpackage

* Fri Dec 15 2000 Matt Wilson <msw@redhat.com>
- modify all files to use "python2.0" as the intrepter
- don't build the Expat bindings
- build against db1

* Mon Oct 16 2000 Jeremy Hylton <jeremy@beopen.com>
- updated for 2.0 final

* Mon Oct  9 2000 Jeremy Hylton <jeremy@beopen.com>
- updated for 2.0c1
- build audioop, imageop, and rgbimg extension modules
- include xml.parsers subpackage
- add test.xml.out to files list

* Thu Oct  5 2000 Jeremy Hylton <jeremy@beopen.com>
- added bin/python2.0 to files list (suggested by Martin v. Lowis)

* Tue Sep 26 2000 Jeremy Hylton <jeremy@beopen.com>
- updated for release 1 of 2.0b2
- use .bz2 version of Python source

* Tue Sep 12 2000 Jeremy Hylton <jeremy@beopen.com>
- Version 2 of 2.0b1
- Make the package relocatable.  Thanks to Suchandra Thapa.
- Exclude Tkinter from main RPM.  If it is in a separate RPM, it is
  easier to track Tk releases.
