%global momorel 6

Name:		epdfview
Version:	0.1.8
Release:	%{momorel}m%{?dist}
Summary:	Lightweight PDF document viewer

Group:		Applications/Publishing
License:	GPLv2+
URL:		http://trac.emma-soft.com/epdfview
Source0:	http://trac.emma-soft.com/epdfview/chrome/site/releases/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:         %{name}-%{version}-fixes-1.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	poppler-devel >= 0.18.2
BuildRequires:	gtk2-devel glib2-devel cups-devel
BuildRequires:	desktop-file-utils
BuildRequires:	autoconf automake libtool gettext
BuildRequires:	poppler-glib-devel >= 0.18.2
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils

%description
ePDFView is a lightweight PDF document viewer using Poppler and GTK+ libraries.
The aim of ePDFView is to make a simple PDF document viewer, in the lines of
Evince but without using the Gnome libraries.

%prep
%setup -q

%patch0 -p1 -b .cups16

%build
%configure CPPFLAGS=-DGLIB_COMPILATION
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

desktop-file-install \
	--dir=%{buildroot}%{_datadir}/applications \
	--vendor= \
	--delete-original \
	--remove-category Office \
	--add-category Graphics \
	%{buildroot}%{_datadir}/applications/%{name}.desktop

install -dm 755 %{buildroot}%{_datadir}/pixmaps
pushd %{buildroot}%{_datadir}/pixmaps
ln -s ../%{name}/pixmaps/icon_epdfview-48.png .
popd

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null || :

%postun
update-desktop-database &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%{_bindir}/%{name}
%doc AUTHORS COPYING README NEWS THANKS
%{_datadir}/applications/%{name}.desktop
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/pixmaps
%{_datadir}/%{name}/ui
%{_datadir}/pixmaps/icon_epdfview-48.png
%{_mandir}/man1/epdfview.1.*

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.8-6m)
- import fixes-1.patch to enable build with new cups
- http://www.linuxfromscratch.org/blfs/view/cvs/pst/epdfview.html

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-5m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.8-4m)
- build fix

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.8-3m)
- rebuild against poppler-0.18.2

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.8-2m)
- rebuild against poppler-0.18.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.8-1m)
- update to 0.1.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-11m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.7-10m)
- rebuild against poppler-0.16.4

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.7-9m)
- add patch0 (enable to build with new poppler-0.16.0)

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.7-8m)
- rebuild against poppler-0.16.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-7m)
- rebuild for new GCC 4.5

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.7-6m)
- rebuild against poppler-0.14.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.7-5m)
- full rebuild for mo7 release

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.7-4m)
- rebuild against poppler-0.14.0

* Mon May 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.7-3m)
- add BuildRequires: poppler-glib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-1m)
- import from Rawhide

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Mar 26 2009 Michal Schmidt <mschmidt@redhat.com> - 0.1.7-1
- Upstream release 0.1.7.

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.6-8.20081217svn
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Dec 17 2008 Michal Schmidt <mschmidt@redhat.com> - 0.1.6-7.20081217svn
- Add icon.

* Wed Dec 17 2008 Michal Schmidt <mschmidt@redhat.com> - 0.1.6-6.20081217svn
- Rebased to current svn snapshot.
- Fixes bz476575 "epdfview crashes on print pdf".  

* Fri Nov 28 2008 Caolan McNamara <caolanm@redhat.com> - 0.1.6-5
- rebuild for dependencies

* Fri Aug 31 2008 Michal Schmidt <mschmidt@redhat.com> - 0.1.6-4
- Fix build with the new RPM.

* Fri Mar 21 2008 Michal Schmidt <mschmidt@redhat.com> - 0.1.6-3
- Rebuild with new poppler.

* Fri Feb 15 2008 Michal Schmidt <mschmidt@redhat.com> - 0.1.6-2
- Use standard scriptlets for updating desktop-database.
- Conditional buildreqs to build on F8 too.
- Preserve timestamps of unmodified files.

* Wed Feb 13 2008 Michal Schmidt <mschmidt@redhat.com> - 0.1.6-1
- Initial Fedora package.
