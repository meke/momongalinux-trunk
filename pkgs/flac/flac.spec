%global momorel 1

Summary: flac - Free Lossless Audio Codec
Name: flac
Version: 1.3.0
Release: %{momorel}m%{?dist}
License: GPL and LGPL and GFDL
URL: http://flac.sourceforge.net/
Group: Applications/Multimedia
Source0: http://downloads.xiph.org/releases/flac/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: %{name}-xmms-euc-jp-ms.patch
# merge from cooker
Patch1: %{name}-1.1.4-xmms-plugin.patch
Patch2: flac-metaflac_strcat.patch
Patch3: flac-no_rice_asm.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
Requires: libogg
BuildRequires: doxygen
BuildRequires: gettext-devel
BuildRequires: glib2-devel
BuildRequires: id3lib-devel >= 3.8.3-3m
BuildRequires: libogg-devel
BuildRequires: nasm
BuildRequires: pkgconfig
BuildRequires: xmms-devel
BuildRequires: libtool >= 2.4

%description
 FLAC stands for Free Lossless Audio Codec. Grossly oversimplified, FLAC is
 similar to MP3, but lossless. The FLAC project consists of:

    * the stream format
    * reference encoders and decoders in library form
    * flac, a command-line program to encode and decode FLAC files
    * metaflac, a command-line metadata editor for FLAC files
    * input plugins for various music players

 "Free" means that the specification of the stream format is fully open to the
 public to be used for any purpose (the FLAC project reserves the right to set
 the FLAC specification and certify compliance), and that neither the FLAC form
 at nor any of the implemented encoding/decoding methods are covered by any 
 known patent. It also means that all the source code is available under
 open-source licenses.

%package libs
Summary: Libraries for the Free Lossless Audio Codec
Group: System Environment/Libraries

%description libs
FLAC stands for Free Lossless Audio Codec. Grossly oversimplified, FLAC
is similar to Ogg Vorbis, but lossless. The FLAC project consists of
the stream format, reference encoders and decoders in library form,
flac, a command-line program to encode and decode FLAC files, metaflac,
a command-line metadata editor for FLAC files and input plugins for
various music players.

This package contains the FLAC libraries.

%package devel
Summary: flac Development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The flac-devel package contains the header files and documentation
needed to develop applications with flac.

%package -n xmms-flac
Summary: flac input plugin for xmms
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: xmms, id3lib

%description -n xmms-flac
flac plugin.

%prep
%setup -q

%patch0 -p1 -b .euc-jp-ms
# from cooker
%patch1 -p1 -b .xmms
%patch2 -p1
%patch3 -p1

%build
autoreconf -fi

export CFLAGS="%{optflags} -funroll-loops"
%configure \
    --disable-silent-rules \
    --disable-thorough-tests

sed -e 's/^build_old_libs=yes/build_old_libs=no/' libtool > libtool-disable-static
chmod +x libtool-disable-static

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

(export docdir=`pwd`/DOCS; cd doc; make install docdir=$docdir)
make install DESTDIR=%{buildroot}

find %{buildroot} -name "*.la" -delete

# clean up doc
rm -f doc/*/*/Makefile*
rm -f doc/*/Makefile*
rm -f doc/Makefile*

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING.* README doc/html
%{_bindir}/flac
%{_bindir}/metaflac
%{_mandir}/man1/flac.1*
%{_mandir}/man1/metaflac.1*

%files libs
%doc AUTHORS COPYING* README
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/FLAC
%{_includedir}/FLAC++
%{_libdir}/pkgconfig/flac++.pc
%{_libdir}/pkgconfig/flac.pc
%{_libdir}/lib*.so
%{_datadir}/aclocal/libFLAC++.m4
%{_datadir}/aclocal/libFLAC.m4

%files -n xmms-flac
%defattr(-,root,root)
%{_libdir}/xmms/Input/libxmms-flac.so

%changelog
* Tue Jan 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-13m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-11m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-10m)
- build fix libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-7m)
- revive xmms-flac

* Fri May 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-6m)
- run ./autogen.sh. need libtool-2.2.x
- Obsoletes: xmms-flac

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-3m)
- %%NoSource -> NoSource

* Tue Nov 20 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-2m)
- added patch to fix compilation issue with gcc43

* Tue Oct 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-1m)
- [SECURITY] CVE-2007-4619
- version 1.2.1
- update gnu-stack.patch (re-import from Fedora)
 +* Tue Sep 11 2007 - Bastien Nocera <bnocera@redhat.com> - 1.2.0-2
 +- Update GNU stack patch to cover all the NASM sources used
- remove link-ogg.patch
- clean up spec file

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-1m)
- version 1.1.4
- import gnu-stack.patch and link-ogg.patch from Fedora
 +* Wed Feb 14 2007 - Bastien Nocera <bnocera@redhat.com> - 1.1.4-2
 +- Update link-ogg patch for 1.1.4
 +* Tue Feb 13 2007 - Bastien Nocera <bnocera@redhat.com> - 1.1.3-1
 +- Update with work from Matthias Clasen <mclasen@redhat.com> up
 +  to upstream 1.1.3 (#229462)
 +* Wed Feb 23 2005 Colin Walters <walters@redhat.com> 1.1.0-8
 +- New patch flac-1.1.0-gnu-stack.patch from Ulrich Drepper to mark asm
 +  as not requiring an executable stack
- update xmms-plugin.patch
- remove m4.patch and libtool.patch

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-6m)
- delete libtool library

* Mon Jan  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-5m)
- roll back to 1.1.2 sorry...

* Mon Jan  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3
- modify patch3 for version 1.1.3
- delete patch4
- revise %%files section in devel package

* Mon Mar 27 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- revise %files section

* Wed Aug 10 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.2-4m)
- without BuildRequires:nasm for ppc

* Sun Jul 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.2-3m)
- ppc build fix.

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-2m)
- import xmms-plugin.patch from cooker
 +* Wed Apr 20 2005 Gz Waschk <waschk@mandriva.org> 1.1.2-2mdk
 +- try to fix bug #15553 with patch 2

* Wed Jul  6 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.1-3m)
- enable x86_64.
  add flac-1.1.1-m4.patch (from fc3)
  add flac-1.1.1-libtool.patch (from fc3)

* Thu Jan 06 2005 mutecat <mutecat@momonga-linux.org>
- (1.1.1-2m)
- arrange ppc
- add Patch1 from cvs lpc.h diff
- and configure --disable-asm-optimizations

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1
- use %%NoSource macro
- revise %%files section
- add %%post and %%postun sections
- import flac-xmms-euc-jp-ms.patch from VineSeedPlus
 +* Mon May 03 2004 KAZUKI SHIMURA <kazuki@ma.ccnw.ne.jp> 1.1.0-0vl3
 +- xmms-flac
 +  - enable to convert charset from/to EUC-JP-MS (Patch0)

* Sun Aug 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.1.0-5m)
- rebuild against id3lib-3.8.3-3m (libstdc++-3.4.1)

* Wed May 26 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.0-4m)
- rebuild against id3lib 3.8.3.

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.0-3m)
- revised spec for enabling rpm 4.2.

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.0-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Feb 23 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.0-1m)
  Imported.
