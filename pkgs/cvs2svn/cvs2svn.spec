%global momorel 5
%global prjno 1462
%global dirno 46528

Name:           cvs2svn
Version:        2.3.0
Release:        %{momorel}m%{?dist}
Summary:        CVS to Subversion Repository Converter
Group:          Development/Tools
License:        BSD
URL:            http://cvs2svn.tigris.org/
Source0:        http://cvs2svn.tigris.org/files/documents/%{prjno}/%{dirno}/%{name}-%{version}.tar.gz 
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python-devel >= 2.7
Requires:       cvs
Requires:       rcs
Requires:       subversion >= 1.4.2-2m

# It seems that this macro should exist under %%NoSource macro 
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}


%description
cvs2svn is a Python script that converts a CVS repository to a Subversion repository. It is designed for one-time conversions, not for repeated synchronizations between CVS and Subversion.

%prep
%setup -q

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BUGS CHANGES COMMITTERS COPYING HACKING PKG-INFO README cvs2svn-example.options www/*.html
%{_bindir}/*
%{python_sitelib}/*

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-2m)
- full rebuild for mo7 release

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.1-2m)
- rebuild against python-2.6.1-1m

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.1-2m)
- %%NoSource -> NoSource

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Wed Dec 27 2006 Nishio Futosi <futoshi@momonga-linux.org>
- (1.5.0-2m)
- rebuild against python-2.5

* Wed Dec 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5.0-1m)
- separated from subversion
- specfile is based on that in Fedora Extras (1.5.0-1)

* Sat Oct 28 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 1.5.0-1
- Upstream 1.5.0

* Wed Sep 06 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 1.4.0-2
- Ghostbusting

* Sun Sep 03 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 1.4.0-1
- 1.4.0 final release

* Wed Jul 26 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 1.4.0-0.4.rc1
- Require cvs and subversion
- Add python-abi for easy builds on systems <FC4

* Wed Jul 26 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 1.4.0-0.3.rc1
- Requires rcs.

* Wed Jul 26 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 1.4.0-0.2.rc1
- Group is Development/Tools

* Wed Jul 26 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 1.4.0-0.1.rc1
- Initial packaging.
