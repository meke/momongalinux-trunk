%global momorel 7

Summary:        Utility to change processor speed on Transmeta CPUs.
Name:           longrun
Version:        0.9
Release: %{momorel}m%{?dist}
Group:          System Environment/Base
License:        GPL
Source0:	longrun-0.9.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch:	%{ix86}
Obsoletes:	kernel-utils

Patch1: longrun-0.9-14.patch

%description
longrun is a tool to adjust CPU speed on Transmeta
Crusoe processors.

%prep
%setup -q -c -a 0

%patch1 -p0

%build
rm -rf %{buildroot}

mkdir -p %{buildroot}/usr/sbin
mkdir -p %{buildroot}/usr/man
mkdir -p %{buildroot}/etc/rc.d/init.d
mkdir -p %{buildroot}/etc/sysconfig

cd longrun
make CFLAGS="%{optflags}"

%install
mkdir -p %{buildroot}/usr/share/man/man{1,8}

cd longrun
install longrun %{buildroot}/usr/sbin
install longrun.1 %{buildroot}/usr/share/man/man1
chmod -R a-s %{buildroot}

%clean
[ "%{buildroot}" != "/" ] && [ -d %{buildroot} ] && rm -rf %{buildroot};

%files
%defattr(-,root,root)
/usr/sbin/longrun
/usr/share/man/man1/longrun.1.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-2m)
- rebuild against gcc43

* Sat May 20 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9-1m)
- import from fc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Apr 13 2005 Florian La Roche <laroche@redhat.com>
- remove empty scripts

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4

* Tue Jan 11 2005 Dave Jones <davej@redhat.com>
- Add missing Obsoletes: kernel-utils

* Sat Dec 18 2004 Dave Jones <davej@redhat.com>
- Initial packaging, based upon kernel-utils.

