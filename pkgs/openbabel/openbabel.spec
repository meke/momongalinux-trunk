%define perl_vendorarch %(eval "`perl -V:installvendorarch`"; echo $installvendorarch)
%define perl_archlib %(eval "`perl -V:archlib`"; echo $archlib)
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%define ruby_sitearchdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')

%global momorel 9
%global pythonverrel 2.7

Summary: Chemistry software file format converter
Name: openbabel
Version: 2.3.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/File
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: obgui.desktop
URL: http://openbabel.org/wiki/Main_Page
Patch1: %{name}-rpm.patch
# fix plugin directory location (#680292, patch by lg)
Patch4: openbabel-2.3.0-plugindir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake
BuildRequires: inchi-devel
BuildRequires: libtool
BuildRequires: libxml2-devel
BuildRequires: perl >= 5.12.0
BuildRequires: perl(ExtUtils::MakeMaker)
BuildRequires: python >= %{pythonverrel}
BuildRequires: python-devel >= %{pythonverrel}
BuildRequires: ruby18-devel
BuildRequires: swig
BuildRequires: zlib-devel

%description
Open Babel is a free, open-source version of the Babel chemistry file
translation program. Open Babel is a project designed to pick up where
Babel left off, as a cross-platform program and library designed to
interconvert between many file formats used in molecular modeling,
computational chemistry, and many related areas.

Open Babel includes two components, a command-line utility and a C++
library. The command-line utility is intended to be used as a replacement
for the original babel program, to translate between various chemical file
formats. The C++ library includes all of the file-translation code as well
as a wide variety of utilities to foster development of other open source
scientific software. 

%package devel
Summary: Development tools for programs which will use the Open Babel library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The %{name}-devel package includes the header files and libraries
necessary for developing programs using the Open Babel library.

If you are going to develop programs which will use this library
you should install %{name}-devel.  You'll also need to have the
%{name} package installed.
#'

%package gui
Summary: Chemistry software file format converter - GUI version
Group: Applications/File

%description gui
Open Babel is a free, open-source version of the Babel chemistry file
translation program. Open Babel is a project designed to pick up where
Babel left off, as a cross-platform program and library designed to
interconvert between many file formats used in molecular modeling,
computational chemistry, and many related areas.

This package contains the graphical interface.

%package -n perl-%{name}
Group: System Environment/Libraries
Summary: Perl wrapper for the Open Babel library
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes: %{name}-perl
Provides: %{name}-perl = %{version}-%{release}

%description -n perl-%{name}
Perl wrapper for the Open Babel library.

%package -n python-%{name}
Group: System Environment/Libraries
Summary: Python wrapper for the Open Babel library
Obsoletes: %{name}-python
Provides: %{name}-python = %{version}-%{release}

%description -n python-%{name}
Python wrapper for the Open Babel library.

%package -n ruby-%{name}
Summary: Ruby wrapper for the Open Babel library
Group: Development/Libraries
Requires: ruby(abi) = 1.8
Requires: %{name} = %{version}-%{release}

%description -n ruby-%{name}
Ruby wrapper for the Open Babel library.

%prep
%setup -q
%patch1 -p1 -b .r
%patch4 -p1 -b .plugindir
chmod 644 src/formats/{fchk,genbank,mmcif}format.cpp
chmod 644 src/math/align.cpp
chmod 644 include/openbabel/{graphsym.h,math/align.h}
convert src/GUI/babel.xpm -transparent white babel.png

%build
%cmake \
 -DCMAKE_SKIP_RPATH:BOOL=ON \
 -DBUILD_GUI:BOOL=ON \
 -DPYTHON_BINDINGS:BOOL=ON \
 -DPERL_BINDINGS:BOOL=ON \
 -DRUBY_BINDINGS:BOOL=ON \
 -DRUBY_EXECUTABLE=%{_bindir}/ruby18 \
 -DOPENBABEL_USE_SYSTEM_INCHI=true \
 .
make VERBOSE=1 %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
touch scripts/CMakeLists.txt
make VERBOSE=1 DESTDIR=%{buildroot} install

rm $RPM_BUILD_ROOT%{perl_archlib}/perllocal.pod
rm -f $RPM_BUILD_ROOT%{perl_vendorarch}/auto/Chemistry/OpenBabel/{.packlist,OpenBabel.bs}
chmod 755 $RPM_BUILD_ROOT%{perl_vendorarch}/auto/Chemistry/OpenBabel/OpenBabel.so

desktop-file-install --dir=$RPM_BUILD_ROOT%{_datadir}/applications %{SOURCE1}
install -Dpm644 babel.png $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/32x32/apps/babel.png

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README THANKS
%doc doc/*.html
%doc doc/README* doc/dioxin.*
%{_bindir}/babel
%{_bindir}/obabel
%{_bindir}/obchiral
%{_bindir}/obconformer
%{_bindir}/obfit
%{_bindir}/obgen
%{_bindir}/obgrep
%{_bindir}/obprobe
%{_bindir}/obprop
%{_bindir}/obrotate
%{_bindir}/obenergy
%{_bindir}/obminimize
%{_bindir}/obrotamer
%{_bindir}/obspectrophore
%{_bindir}/roundtrip
%{_mandir}/man1/*
%{_datadir}/%{name}
%{_libdir}/libopenbabel.so.*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/%{version}
%{_libdir}/%{name}/%{version}/*.so

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}-2.0
%{_libdir}/libopenbabel.so
%{_libdir}/pkgconfig/%{name}-2.0.pc
%{_libdir}/cmake/%{name}2

%files gui
%defattr(-,root,root,-)
%{_bindir}/obgui
%{_datadir}/applications/obgui.desktop
%{_datadir}/icons/hicolor/32x32/apps/babel.png

%files -n perl-%{name}
%defattr(-,root,root,-)
%{perl_vendorarch}/Chemistry/OpenBabel.pm
%dir %{perl_vendorarch}/*/Chemistry/OpenBabel
%{perl_vendorarch}/*/Chemistry/OpenBabel/OpenBabel.so

%files -n python-%{name}
%defattr(-,root,root,-)
%{python_sitearch}/_openbabel.so
%{python_sitearch}/openbabel.py*
%{python_sitearch}/pybel.py*
%{python_sitearch}/openbabel-*.egg-info

%files -n ruby-%{name}
%defattr(-, root, root, -)
%{ruby_sitearchdir}/openbabel.so

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-2m)
- rebuild against perl-5.16.0

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-4m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.0-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Tue Feb 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-11m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.3-8m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.3-6m)
- fix ruby(abi)

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.3-5m)
- use ruby18 packages

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-4m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-3m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.2-2m)
- rebuild against perl-5.10.1

* Fri Jul 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1 based on Fedora 11 (2.2.1-0.1.b3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.0-2m)
- rebuild agaisst python-2.6.1-1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.1-8m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.1-7m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.1-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-5m)
- %%NoSource -> NoSource

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-4m)
- rebuild against perl-5.10.0
- add Patch2: openbabel-2.1.1-script-perl-xs.patch

* Tue Jan 15 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-3m)
- add patch for gcc43, generated by gen43patch(v1)

* Tue Jan  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-2m)
- sync with Fedora devel
- build perl and python modules
- use external inchi

* Sun Aug 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1
- comment out patch0

* Wed May  2 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-3m)
- revise configure patch

* Tue Mar 27 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.2-2m)
- add configure patch

* Sun Mar 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- initial build
