%global         momorel 4

Name:           perl-Template-Toolkit
Version:        2.25
Release:        %{momorel}m%{?dist}
Summary:        Template::Toolkit Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Template-Toolkit/
Source0:        http://www.cpan.org/authors/id/A/AB/ABW/Template-Toolkit-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-AppConfig >= 1.56
BuildRequires:  perl-Cwd >= 0.8
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Temp >= 0.12
BuildRequires:  perl-List-Util
Requires:       perl-AppConfig >= 1.56
Requires:       perl-Cwd >= 0.8
Requires:       perl-File-Temp >= 0.12
Requires:       perl-List-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Please see the separate INSTALL file for further information on installing
the Template Toolkit, including what to do if you don't have the CPAN
module installed, and/or installation on MS Windows.

%prep
%setup -q -n Template-Toolkit-%{version}

%build
%{__perl} Makefile.PL \
    TT_XS_ENABLE=y \
    TT_XS_DEFAULT=y \
    TT_QUIET=y \
    TT_ACCEPT=y \
    INSTALLDIRS=vendor \
    OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes HACKING META.json README TODO
%{_bindir}/tpage
%{_bindir}/ttree
%{perl_vendorarch}/auto/Template
%{perl_vendorarch}/Template
%{perl_vendorarch}/Template.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25-2m)
- rebuild against perl-5.18.1

* Thu Jul 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25-1m)
- update to 2.25

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24-1m)
- update to 2.24

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.22-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-5m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-1m)
- update to 2.22

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-1m)
- update to 2.20

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.19-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.19-2m)
- %%NoSource -> NoSource

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19-1m)
- update to 2.19

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.18-2m)
- use vendor

* Sat Feb 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-1m)
- update to 2.18

* Fri Feb  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17-1m)
- update to 2.17

* Wed Jan 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-1m)
- update to 2.16

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15-1m)
- spec file was autogenerated
