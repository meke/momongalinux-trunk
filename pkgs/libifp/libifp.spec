%global momorel 14

Summary: iRiver driver library
Name: libifp
Version: 1.0.0.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Libraries
URL: http://ifp-driver.sourceforge.net/libifp/
Source0: http://dl.sourceforge.net/sourceforge/ifp-driver/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.hotplug
Source2: 10-%{name}.rules
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: udev >= 151
BuildRequires: libusb-devel >= 0.1.12

%description
libifp is a general-purpose library-driver for iRiver's iFP (flash-based)
portable audio players.

%package devel
Summary: Headers for developing programs that will use libifp
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Headers for developing programs that will use libifp.

%prep
%setup -q

%build
export CPPFLAGS="%{optflags}"
%configure --with-libusb=%{_prefix}
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

# install libifp-hotplug
mkdir -p %{buildroot}/sbin
install -m 755 %{SOURCE1} %{buildroot}/sbin/%{name}-hotplug

# install udev rules file
mkdir -p %{buildroot}%{_sysconfdir}/udev/rules.d
install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/udev/rules.d/10-%{name}.rules

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README TODO
%config(noreplace) %{_sysconfdir}/udev/rules.d/10-%{name}.rules
/sbin/%{name}-hotplug
%{_bindir}/ifpline
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/ifp.h
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so
%{_mandir}/man3/ifp.h.3*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.2-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.2-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.2-12m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.2-11m)
- touch up spec file

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.2-10m)
- update 10-libifp.rules for udev-151

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.2-9m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (1.0.0.2-7m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0.2-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0.2-4m)
- %%NoSource -> NoSource

* Sun Jan 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.2-3m)
- get rid of *.la files

* Thu Jun 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.2-2m)
- import libifp.hotplug and 10-libifp.rules from Fedora Core extras
 +* Thu Jul 14 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 1.0.0.1-2
 +- Modified for new udev
 +* Thu Jul  7 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net>
 +- Added Per Bjornsson's hotplug files

* Sun May  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.2-1m)
- version 1.0.0.2

* Tue Feb 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.1-1m)
- initial package for amarok-1.4
