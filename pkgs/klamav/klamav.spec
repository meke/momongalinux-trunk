%global momorel 12
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Clam Anti-Virus on the KDE Desktop
Name: klamav
Version: 0.46
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://klamav.sourceforge.net
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: %{name}-ja.po
Source2: %{name}rc
Source10: admin.tar.bz2
Patch0: %{name}-0.44-desktop.patch
Patch1: %{name}-0.45-momonga-default-db-dir.patch
Patch2: %{name}-0.45-momonga-translator.patch
Patch3: %{name}-%{version}-gzip-api.patch
Patch10: %{name}-%{version}-automake111.patch
Patch11: %{name}-%{version}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: clamav >= 0.95
Requires: lha
Requires: unrar
Requires: unzip
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: clamav-devel >= 0.95
BuildRequires: coreutils
BuildRequires: curl-devel
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: gmp-devel
BuildRequires: libtool

%description
ClamAV Anti-Virus protection for the KDE desktop.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .momonga-clamav-db-path
%patch2 -p1 -b .momonga-translator
%patch3 -p1 -b .gzip-api

# for new automake and autoconf
rm -rf admin
tar xf %{SOURCE10}
%patch10 -p1 -b .automake111
%patch11 -p1 -b .autoconf265

# for Japanese
install -m 644 %{SOURCE1} po/ja.po

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install klamavrc
mkdir -p %{buildroot}%{_datadir}/config
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/config/

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop

# fix permission
chmod a+x %{buildroot}%{_bindir}/ScanWithKlamAV

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README TODO
%{_bindir}/ScanWithKlamAV
%{_bindir}/klamarkollon
%{_bindir}/%{name}
%{_bindir}/klammail
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/apps/konqueror/servicemenus/%{name}-dropdown.desktop
%{_datadir}/config/%{name}rc
%{_datadir}/config.kcfg/%{name}config.kcfg
%{_datadir}/doc/HTML/*/%{name}02
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-12m)
- import patch from Fedora to enable to build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.46-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.46-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.46-9m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-8m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-7m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-5m)
- fix build with new automake

* Sun May 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-4m)
- fix for new automake

* Sun May 10 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.46-3m)
- rebuild against clamav-0.95.1

* Tue Mar  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46-2m)
- remove gcc43 patch

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.46-1m)
- version 0.46
- update gcc43.patch

* Sat Feb 21 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.45-3m)
- fix BuildPreReq:

* Wed Feb 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.45-2m)
- add momonga-translator.patch

* Wed Feb 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.45-1m)
- version 0.45
- update and revive default-db-dir.patch
- remove clamav094.patch

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.44-7m)
- replace Patch1: %%{name}-suse-clamav-path.diff

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.44-6m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.44-5m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Wed Sep 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-4m)
- rebuild against clamav-0.94
- import patch from gentoo

* Sun Jul 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.44-3m)
- revise Japanese translation again

* Sun Jul 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.44-2m)
- revise Japanese translation

* Mon Jul  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- version 0.44

* Sun Jun 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-3m)
- rebuild against clamav-0.93.1
- import clamav093 patch from Fedora devel and modify

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-2m)
- rebuild against qt3

* Wed May  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.43-1m)
- version 0.43

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.42-5m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.42-3m)
- update Japanese translation

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.42-2m)
- %%NoSource -> NoSource

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.42-1m)
- version 0.42
- remove clam0.92.patch
- License: GPLv2

* Wed Jan  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.41.1-3m)
- added patch for gcc43

* Wed Jan  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41.1-2m)
- import klamav-0.41.1-clam0.92.patch from Fedora
 +* Sat Dec 22 2007 Andy Shevchenko <andy@smile.org.ua> 0.41.1-9
 +- rebuild against new clamav

* Thu Jul 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41.1-1m)
- version 0.41.1

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41-7m)
- set archivers
- update Japanese translation

* Sun Mar 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41-6m)
- update Japanese translation

* Tue Mar 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41-5m)
- update Japanese translation

* Mon Mar 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41-4m)
- add Japanese translation

* Mon Mar 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41-3m)
- update desktop.patch (add Japanese message to klamav-dropdown.desktop)

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41-2m)
- update desktop.patch (modify GenericName[ja])

* Sat Mar 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41-1m)
- initial package for Momonga Linux
