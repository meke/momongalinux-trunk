%global momorel 4
%global sover 1.0.6

Summary: A file compression utility.
Name: bzip2
Version: 1.0.6
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/File
URL: http://www.bzip.org/
Source0: http://www.bzip.org/%{version}/%{name}-%{version}.tar.gz 
NoSource: 0
Source2: bzme
Source3: bzme.1
Patch0: %{name}-1.0.4-saneso.patch
Patch1: %{name}-1.0.4-cflags.patch
Patch2: %{name}-1.0.4-%{name}recover.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: patch >= 2.5.9

%description
Bzip2 is a freely available, patent-free, high quality data compressor.
Bzip2 compresses files to within 10 to 15 percent of the capabilities 
of the best techniques available.  However, bzip2 has the added benefit 
of being approximately two times faster at compression and six times 
faster at decompression than those techniques.  Bzip2 is not the 
fastest compression utility, but it does strike a balance between speed 
and compression capability.

Install bzip2 if you need a compression utility.

%package libs
Summary: Libraries for applications using bzip2
Group: System Environment/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description libs
Libraries for applications using the bzip2 compression format.

%package devel
Summary: Header files and libraries for developing apps which will use bzip2.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel

Header files and a static library of bzip2 functions, for developing apps
which will use the library.

%prep
%setup -q

%patch0 -p1 -b .saneso
%patch1 -p1 -b .cflags
%patch2 -p1 -b .bz2recover

%build
make -f Makefile-libbz2_so CFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64 -fpic -fPIC" all
rm -f *.o
make CFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64" all

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}{%{_bindir},%{_mandir}/man1,/%{_libdir},%{_includedir}}
install -m 644 bzlib.h %{buildroot}%{_includedir}/
install -m 755 libbz2.so.%{sover} %{buildroot}/%{_libdir}/
install -m 755 libbz2.a %{buildroot}%{_libdir}/
install -m 755 %{name}-shared  %{buildroot}%{_bindir}/%{name}
install -m 755 %{name}recover bzgrep bzdiff bzmore  %{buildroot}%{_bindir}/
install -m 644 %{name}.1 bzdiff.1 bzgrep.1 bzmore.1  %{buildroot}%{_mandir}/man1/
install -m 755 %{SOURCE2} %{buildroot}%{_bindir}/
install -m 644 %{SOURCE3} %{buildroot}%{_mandir}/man1/
ln -s %{name} %{buildroot}%{_bindir}/bunzip2
ln -s %{name} %{buildroot}%{_bindir}/bzcat
ln -s bzdiff %{buildroot}%{_bindir}/bzcmp
ln -s bzmore %{buildroot}%{_bindir}/bzless
ln -s libbz2.so.%{sover} %{buildroot}/%{_libdir}/libbz2.so.1
ln -s ../../%{_lib}/libbz2.so.1 %{buildroot}%{_libdir}/libbz2.so
ln -s %{name}.1 %{buildroot}%{_mandir}/man1/%{name}recover.1
ln -s %{name}.1 %{buildroot}%{_mandir}/man1/bunzip2.1
ln -s %{name}.1 %{buildroot}%{_mandir}/man1/bzcat.1
ln -s bzdiff.1 %{buildroot}%{_mandir}/man1/bzcmp.1
ln -s bzmore.1 %{buildroot}%{_mandir}/man1/bzless.1

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES LICENSE README README.COMPILATION.PROBLEMS
%doc manual*html
%{_bindir}/*
%{_mandir}/*/*

%files libs
%defattr(-,root,root)
/%{_libdir}/*so.*

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*so

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-4m)
- support UserMove env

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-2m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-1m)
- [SECURITY] CVE-2010-0405
- update to 1.0.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-10m)
- full rebuild for mo7 release

* Tue Aug 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-9m)
- fix up %%post and %%postun libs, thanks to nakaya

* Mon Aug 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-8m)
- explicitly Requires: /sbin/ldconfig to avoid error when installing from iso-images

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-7m)
- fix up package devel

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-6m)
- move libraries from %%{_libdir}/ to /%%{_lib}/
- split package libs
- import cflags.patch and bzip2recover.patch from Fedora

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-2m)
- rebuild against gcc43

* Sat Mar 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- [SECURITY] see https://www.cert.fi/haavoittuvuudet/joint-advisory-archive-formats.html
- update to 1.0.5

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- %%NoSource -> NoSource

* Mon Jan 08 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0.4-1m)
- version up 1.0.4
- revice patch0 for 1.0.4
- remove patch1

* Wed Jan  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-2m)
- update bzdiff-tempfile.patch for patch-2.5.9-1m

* Thu Jul 21 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.3-1m)
- up to 1.0.3
- [SECURITY] CAN-2005-0953 CAN-2005-1260

* Tue Sep 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.2-8m)
- used %%{_libdir} and %%{_includedir} macros

* Tue Sep 28 2004 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.0.2-7m)
- revise bzdiff-tempfile.patch because
  previous version could cause race condition.

* Mon Sep 27 2004 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.0.2-6m)
- add bzdiff patch

* Mon Oct 14 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.2-5m)
- remove Source1: bzgrep (the latest one is included in source archive)
- fix rel num

* Sat Oct 12 2002 Nguyen Hung Vu <vuhung@momonga-linux.org>
- (1.0.2-4m)
- Add bzme script to bzip2 package. No changes have been made to source code

* Sat Oct 12 2002 Nguyen Hung Vu <vuhung@momonga-linux.org>
- (1.0.2-3m)
- Change the k prefix to m. Nothing else

* Sun Mar 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0.2-2k)
- add html manuals
- import from rawhide (1.0.2-2)
-   Total overhaul of build precedure
-   Add many small helper programs added to 1.0.2
-   drop old patches

* Sun Oct 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.1-6k)
- add BuildPreReq tag to build on alpha

* Thu Oct 18 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.1-4k)
- modified bzip2-1.0.1.libtoolizeautoconf-kondara.patch.bz2,
  i.e. version number correct

* Sun Nov 19 2000 TABUCHI Takaaki <tab@kondara.org>
- update version 1.0.1
- change Source: part
- update patch for 1.0.1

* Sun Jun 04 2000 TABUCHI Takaaki <tab@kondara.org>
- fix typo rawhide's libtoolizeautoconf.patch.

* Thu May 18 2000 TABUCHI Takaaki <tab@kondara.org>
- obsolete shlib patch.
- add Makefile patch. (tmp & kluge)

* Wed May 17 2000 TABUCHI Takaaki <tab@kondara.org>
- update to 1.0.0.
- adapt shlib patch for 1.0.0.
