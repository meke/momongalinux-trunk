%global momorel 4
%global abi_ver 1.9.1

# Generated from bones-extras-1.2.4.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname bones-extras
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: The extras package for Mr Bones provides some handy Rake tasks for running Rcov code coverage on your tests, running Rspec tests, running the Zentest autotest package, and uploading gems and generated rdoc to RubyForge
Name: rubygem-%{gemname}
Version: 1.3.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/TwP/bones-extras
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
Requires: rubygem(bones) >= 3.5.1
Requires: rubygem(bones-rcov)
Requires: rubygem(bones-rubyforge)
Requires: rubygem(bones-rspec) >= 1.0.1
Requires: rubygem(bones-zentest)
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
The extras package for Mr Bones provides some handy Rake tasks for running
Rcov code coverage on your tests, running Rspec tests, running the Zentest
autotest package, and uploading gems and generated rdoc to RubyForge.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/README.txt
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-4m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- update 1.3.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-1m)
- Initial package for Momonga Linux
