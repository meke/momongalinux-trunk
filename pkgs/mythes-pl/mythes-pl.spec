%global momorel 5

Name: mythes-pl
Summary: Polish thesaurus
Version: 1.5
Release: %{momorel}m%{?dist}
Source: http://dl.sourceforge.net/sourceforge/synonimy/OOo2-Thesaurus-%{version}.zip
NoSource: 0
Group: Applications/Text
URL: http://synonimy.ux.pl/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python, perl
License: LGPLv2+
BuildArch: noarch

%description
Polish thesaurus.

%prep
%setup -q -c

%build
iconv -f ISO-8859-2 -t UTF-8 README_th_pl_PL_v2.txt > README_th_pl_PL_v2.txt.new
mv -f README_th_pl_PL_v2.txt.new README_th_pl_PL_v2.txt

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/mythes
cp -p th_pl_PL_v2.* $RPM_BUILD_ROOT/%{_datadir}/mythes

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_th_pl_PL_v2.txt
%{_datadir}/mythes/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against rpm-4.6

* Sun Aug  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-2m)
- modify %%files to avoid conflicting

* Mon May 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4-1m)
- import from Fedora

* Wed Nov 28 2007 Caolan McNamara <caolanm@redhat.com> - 1.4-1
- initial version
