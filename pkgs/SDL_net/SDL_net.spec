%global momorel 1

Summary: SDL portable network library
Name: SDL_net
Version: 1.2.8
Release: %{momorel}m%{?dist}
Source0: http://www.libsdl.org/projects/%{name}/release/%{name}-%{version}.tar.gz 
NoSource: 0
URL: http://www.libsdl.org/projects/SDL_net/
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel >= 1.2.15

%description
This is a portable network library for use with SDL.

%package devel
Summary: Libraries and includes to develop SDL networked applications.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This is a portable network library for use with SDL.

This is the libraries and include files you can use to develop SDL networked applications.

%prep
%setup -q 

%build
%configure --disable-static
make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name '*.la' -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README CHANGES COPYING
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%doc README CHANGES COPYING
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/SDL/*

%changelog
* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-5m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-2m)
- delete libtool library

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.5-5m)
- build against SDL-1.2.7-9m

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.5-4m)
- build against SDL-1.2.7-4m

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.5-3m)
- revised spec for enabling rpm 4.2.

* Tue Sep  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.5-2m)
- rebuild against SDL-1.2.6
- use %%NoSource macro

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.5-1m)
  update to 1.2.5

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.4-3m)
- reubuild aginst SDL-1.2.4-3m

* Thu May 20 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.4-2k)
- version up.

* Thu May  9 2002 Toru Hoshina <t@kondara.org>
- (1.2.3-2k)
- version up.

* Thu Oct 25 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.2-2k)
- First Kondarization

* Sat Feb  3 2001 Paul S Jenner <psj@firstlinux.net>
- First spec file based on SDL spec file
