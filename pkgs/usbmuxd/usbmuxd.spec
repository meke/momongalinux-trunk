%global momorel 1

Summary: Daemon for communicating with Apple's iPod Touch and iPhone
Name: usbmuxd
Version: 1.0.8
Release: %{momorel}m%{?dist}
License: GPLv3 and GPLv2 and LGPLv2+
Group: Applications/System
URL: http://www.libimobiledevice.org/
Source0: http://www.libimobiledevice.org/downloads/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): shadow-utils
BuildRequires: cmake
BuildRequires: libusb1-devel >= 1.0.6
BuildRequires: pkgconfig

%description
usbmuxd is a daemon used for communicating with Apple's iPod Touch and iPhone
devices. It allows multiple services on the device to be accessed
simultaneously.

%package devel
Summary: Header files from usbmuxd
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on usbmuxd.

%prep
%setup -q

## Set the owner of the device node to be usbmuxd
#sed -i.owner 's/ATTR{idVendor}=="05ac"/OWNER="usbmuxd", ATTR{idVendor}=="05ac"/' udev/85-usbmuxd.rules.in
#sed -i.user 's/-U usbmux/-U usbmuxd/' udev/85-usbmuxd.rules.in

%build
mkdir -p %{_target_platform} 
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%pre
getent group usbmuxd >/dev/null || groupadd -r usbmuxd -g 113
getent passwd usbmuxd >/dev/null || \
useradd -r -g usbmuxd -d / -s /sbin/nologin \
	-c "usbmuxd user" -u 113 usbmuxd
exit 0

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* README
/lib/udev/rules.d/85-%{name}.rules
%{_bindir}/iproxy
%{_libdir}/lib%{name}.so.*
%{_sbindir}/%{name}

%files devel
%defattr(-,root,root)
%doc README.devel
%{_includedir}/%{name}-proto.h
%{_includedir}/%{name}.h
%{_libdir}/pkgconfig/lib%{name}.pc
%{_libdir}/lib%{name}.so

%changelog
* Wed Aug 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.8-1m)
- update to 1.0.8

* Sun May 29 2011 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-4m)
- rebuild for new GCC 4.6

* Mon Jan 10 2011 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.6-3m)
- fix owner name bug (-U usbmux --> -U usbmuxd)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-2m)
- rebuild for new GCC 4.5

* Fri Nov 12 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6
-- fix connection bug

* Fri Oct 29 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5
- add patch (usbmuxd-1.0.5-PID_RANGE_MAX.patch)
-   Bump udev rules to 0-9a-f, should last for a few device iterations
-   Also bump the PID range in usb.h

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-4m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.4-3m)
- remove patch
- Set the owner of the device node to be usbmuxd

* Thu Jul 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-2m)
- touch up spec file

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4
- change to new URL

* Mon Feb 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-1m)
- initial package for libimobiledevice and libgpod-0.7.90
- import udevuser.patch and %%pre script from Fedora
