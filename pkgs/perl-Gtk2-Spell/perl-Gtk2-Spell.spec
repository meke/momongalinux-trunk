%global         momorel 9

Name:           perl-Gtk2-Spell
Version:        1.04
Release:        %{momorel}m%{?dist}
Summary:        Bindings for GtkSpell with Gtk2
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Gtk2-Spell/
Source0:        http://www.cpan.org/authors/id/T/TS/TSCH/Gtk2-Spell-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk2-devel
BuildRequires:  gtkspell-devel
BuildRequires:  perl-Gtk2
BuildRequires:  perl-Glib
BuildRequires:  perl-ExtUtils-Depends
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-ExtUtils-PkgConfig
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## We do not run make test, as it requires an active X11-session.  I rather
## suspect this won't work under mock/plague :)
## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 0}

%description
Perl bindings to GtkSpell, used in concert with Gtk2::TextView. Provides
mis-spelled word highlighting in red and offers a right click pop-up menu
with suggested corrections.

%prep
%setup -q -n Gtk2-Spell-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README gtkspell_simple.pl AUTHORS LICENSE ChangeLog 
%{perl_vendorarch}/auto/Gtk2/*
%{perl_vendorarch}/Gtk2/*
%{_mandir}/man3/*.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-2m)
- rebuild against perl-5.16.0

* Wed Jan  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-18m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-17m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-16m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03-14m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-13m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03-12m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-11m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-10m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-8m)
- rebuild against perl-5.10.1

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03-7m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.03-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03-4m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.03-3m)
- use vendor

* Sat Mar 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.03-1m)
- import to Momonga from Fedora Development


* Thu Aug 31 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.03-5
- bump for mass rebuild

* Sat Feb 18 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.03-4
- fixups per #5

* Thu Feb 16 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.03-1
- fixups, initial spec

