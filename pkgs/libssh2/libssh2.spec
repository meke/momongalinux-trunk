%global momorel 1

Summary: A library implementing the SSH2 protocol
Name: libssh2
Version: 1.4.3
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Libraries
URL: http://www.libssh2.org/
Source0: http://www.libssh2.org/download/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: pkgconfig
BuildRequires: zlib-devel

%description
libssh2 is a library implementing the SSH2 protocol as defined by
Internet Drafts: SECSH-TRANS(22), SECSH-USERAUTH(25),
SECSH-CONNECTION(23), SECSH-ARCH(20), SECSH-FILEXFER(06)*,
SECSH-DHGEX(04), and SECSH-NUMBERS(10).

%package devel
Summary: Development files for libssh2
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on libssh2.

%prep
%setup -q

# make sure things are UTF-8...
for i in ChangeLog NEWS ; do
    iconv --from=ISO-8859-1 --to=UTF-8 $i > new
    mv new $i
done

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# fake file for kftpgrabber-0.8.99 svn
touch %{buildroot}%{_libdir}/LibSSH2Config.cmake

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/lib*.la

# clean up docs
( cd example && make clean )
rm -f example/simple/Makefile*
rm -rf example/simple/.deps 
rm -f example/Makefile*

# %%check
# (cd tests && make check)

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HACKING INSTALL NEWS README RELEASE-NOTES TODO
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%doc example
%{_includedir}/%{name}*.h
%{_libdir}/pkgconfig/libssh2.pc
%{_libdir}/LibSSH2Config.cmake
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so
%{_mandir}/man3/%{name}_*.3*

%changelog
* Wed Nov 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Sun Apr  1 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.0-2m)
- add Patch0: aes-the-init-function-fails-when-OpenSSL-has-AES-sup.patch

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Thu Sep 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Mon May 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-2m)
- rebuild for new GCC 4.5

* Fri Oct  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-1m)
- version 1.2.7

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.6-1m)
- version 1.2.6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-2m)
- rebuild against openssl-1.0.0

* Sun Feb 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-1m)
- version 1.2.4
- add RELEASE-NOTES to %%doc
- good-bye autoreconf

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 24 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-8m)
- disable make check

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-7m)
- version 1.1

* Fri May 15 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.18-7m)
- add autoreconf

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-5m)
- rebuild against rpm-4.6

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.18-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18-2m)
- %%NoSource -> NoSource

* Fri Jan 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18-1m)
- initial package for kftpgrabber-0.8.99
