%global momorel 4

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define with_maven 1

%define namedversion 1.0-alpha-3

%define parent plexus
%define subname registry

Name:           plexus-registry
Version:        1.0
Release:        0.1.%{momorel}m%{?dist}
Epoch:          0
Summary:        Plexus Registry Component
License:        "ASL 2.0"
Group:          Development/Tools
URL:            http://plexus.codehaus.org/
# svn export \
#  http://svn.codehaus.org/plexus/plexus-components/tags/plexus-registry-1.0-alpha-3/
# tar czf plexus-registry-1.0-alpha-3-src.tar.gz plexus-registry-1.0-alpha-3
Source0:        %{name}-%{namedversion}.tar.gz

Source3:        plexus-registry-1.0-jpp-depmap.xml

# Generated with mvn ant:ant.  Un-used by default.
Source4:        plexus-registry-api-build.xml
Source5:        plexus-registry-commons-build.xml
Source6:        plexus-registry-naming-build.xml
Source7:        plexus-registry-test-build.xml

Patch0:         plexus-registry-commons-pom.patch
Patch1:         %{name}-fixdepversion.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  java-devel >= 0:1.6.0
BuildRequires:  jpackage-utils >= 0:1.7.3
BuildRequires:  ant >= 0:1.6.5
BuildRequires:  ant-junit
BuildRequires:  junit
BuildRequires:  hsqldb
%if %{with_maven}
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-surefire = 2.3
BuildRequires:  maven-surefire-provider-junit = 2.3
BuildRequires:  plexus-maven-plugin >= 1.3.5
BuildRequires:  maven-doxia
BuildRequires:  maven-doxia-sitetools
BuildRequires:  qdox >= 1.5
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  avalon-logkit
BuildRequires:  avalon-framework
%endif
BuildRequires:  directory-naming
BuildRequires:  avalon-framework
BuildRequires:  jakarta-commons-beanutils
BuildRequires:  jakarta-commons-collections
BuildRequires:  jakarta-commons-configuration
BuildRequires:  jakarta-commons-dbcp >= 1.2.1-11m
BuildRequires:  jakarta-commons-digester
BuildRequires:  jakarta-commons-lang
BuildRequires:  jakarta-commons-logging
BuildRequires:  plexus-classworlds
BuildRequires:  plexus-containers-container-default 
BuildRequires:  plexus-naming 
BuildRequires:  plexus-utils 

Requires:  java >= 0:1.6.0
Requires:  directory-naming
Requires:  jakarta-commons-dbcp >= 1.2.1-11m
Requires:  jakarta-commons-lang
Requires:  plexus-classworlds
Requires:  plexus-containers-container-default 
Requires:  plexus-naming 
Requires:  plexus-utils 

Requires:          jpackage-utils >= 0:1.7.3
Requires(post):    jpackage-utils >= 0:1.7.3
Requires(postun):  jpackage-utils >= 0:1.7.3

%description
The Plexus project seeks to create end-to-end developer tools for 
writing applications. At the core is the container, which can be 
embedded or for a full scale application server. There are many 
reusable components for hibernate, form processing, jndi, i18n, 
velocity, etc. Plexus also includes an application server which 
is like a J2EE application server, without all the baggage.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n %{name}-%{namedversion}

cp %{SOURCE4} plexus-registry-api/build.xml
cp %{SOURCE5} plexus-registry-providers/plexus-registry-commons/build.xml
cp %{SOURCE6} plexus-registry-providers/plexus-registry-naming/build.xml
cp %{SOURCE7} plexus-registry-test/build.xml

%patch0 -b .sav0
%patch1

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

%if %{with_maven}
    mvn-jpp \
        -Dmaven.test.failure.ignore=true \
        -Dmaven2.jpp.depmap.file=%{SOURCE3} \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        install

# Manual iteration should not be needed, but there is a bug in the javadoc 
# plugin which makes this necessary. See: 
# http://jira.codehaus.org/browse/MJAVADOC-157

for pom in `find -name pom.xml`; do
    pushd `dirname $pom`
      mvn-jpp \
          -Dmaven2.jpp.depmap.file=%{SOURCE3} \
          -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
          javadoc:javadoc
    popd
done

%else
export CLASSPATH=$(build-classpath \
commons-beanutils \
commons-collections \
commons-configuration \
commons-lang \
commons-logging \
directory-naming/naming-config \
directory-naming/naming-core \
directory-naming/naming-java \
junit \
plexus/classworlds \
plexus/containers-component-api \
plexus/containers-container-default \
plexus/naming \
plexus/utils \
)
CLASSPATH=$CLASSPATH:$(pwd)/plexus-registry-api/target/plexus-registry-api-%{namedversion}.jar
CLASSPATH=$CLASSPATH:$(pwd)/plexus-registry-test/target/plexus-registry-test-%{namedversion}.jar
CLASSPATH=$CLASSPATH:target/classes:target/test-classes
pushd plexus-registry-api
ant -Dbuild.sysclasspath=only jar javadoc
popd
pushd plexus-registry-test
ant -Dbuild.sysclasspath=only jar javadoc
popd
pushd plexus-registry-providers/plexus-registry-commons
ant -Dbuild.sysclasspath=only jar javadoc
popd
pushd plexus-registry-providers/plexus-registry-naming
ant -Dbuild.sysclasspath=only jar javadoc
popd

%endif

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/plexus

install -pm 644 %{name}-api/target/%{name}-api-%{namedversion}.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/%{subname}-api-%{version}.jar
install -pm 644 \
  %{name}-providers/%{name}-commons/target/%{name}-commons-%{namedversion}.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/%{subname}-commons-%{version}.jar
install -pm 644 \
  %{name}-providers/%{name}-naming/target/%{name}-naming-%{namedversion}.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/%{subname}-naming-%{version}.jar
install -pm 644 %{name}-test/target/%{name}-test-%{namedversion}.jar \
  $RPM_BUILD_ROOT%{_javadir}/plexus/%{subname}-test-%{version}.jar

(cd $RPM_BUILD_ROOT%{_javadir}/plexus && for jar in *-%{version}*; do \
  ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# poms
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms

install -pm 644 pom.xml \
  $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}.pom
%add_to_maven_depmap org.codehaus.plexus.registry %{name} %{namedversion} JPP.%{parent} %{subname}

install -pm 644 %{name}-api/pom.xml \
  $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}-api.pom
%add_to_maven_depmap org.codehaus.plexus.registry %{name}-api %{namedversion} JPP/%{parent} %{subname}-api

install -pm 644 %{name}-test/pom.xml \
  $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}-test.pom
%add_to_maven_depmap org.codehaus.plexus.registry %{name}-test %{namedversion} JPP/%{parent} %{subname}-test

install -pm 644 %{name}-providers/pom.xml \
  $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}-providers.pom
%add_to_maven_depmap org.codehaus.plexus.registry %{name}-providers %{namedversion} JPP.%{parent} %{subname}-providers

install -pm 644 %{name}-providers/%{name}-commons/pom.xml \
  $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}-commons.pom
%add_to_maven_depmap org.codehaus.plexus.registry %{name}-commons %{namedversion} JPP/%{parent} %{subname}-commons

install -pm 644 %{name}-providers/%{name}-naming/pom.xml \
  $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}-naming.pom
%add_to_maven_depmap org.codehaus.plexus.registry %{name}-naming %{namedversion} JPP/%{parent} %{subname}-naming

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/api
cp -pr %{name}-api/target/site/apidocs/* \
    $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/api
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/commons
cp -pr %{name}-providers/%{name}-commons/target/site/apidocs/* \
    $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/commons
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/naming
cp -pr %{name}-providers/%{name}-naming/target/site/apidocs/* \
    $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/naming
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/test
cp -pr %{name}-test/target/site/apidocs/* \
    $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/test
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_javadir}/%{parent}/*
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.1.2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.1m)
- import from Fedora 13

* Mon Nov 23 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.0-0.3.a3
- BR maven-doxia-sitetools.

* Mon Aug 31 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.2.a3
- Add jakarta-commons-dbcp BR and R

* Wed Aug 26 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-0.1.a3
- Fix release to meet Fedora naming guidelines
- Remove gcj support
- Add instructions to build source tarball
- Fix license field
- Remove "excalibur-" prefix from avalon package names

* Thu Mar 05 2009 Yong Yang <yyang@redhat.com> 1.0-0.a3.3
- Build with maven2-2.0.8 built in non-bootstrap mode

* Thu Mar 05 2009 Yong Yang <yyang@redhat.com> 1.0-0.a3.2
- Import from dbhole's maven2 2.0.8 packages
- Fix Vendor, Distribution

* Thu Apr 10 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.a3.1jpp.1
- Import from JPackage

* Mon Dec 03 2007 Ralph Apel <r.apel at r-apel.de> - 0:1.0-0.a3.1jpp
- First release
