%global momorel 5

# Generated from mongrel-1.2.0.pre2.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname mongrel
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Mongrel is a small library that provides a very fast HTTP 1.1 server for Ruby web applications
Name: rubygem-%{gemname}
Version: 1.2.0
Release: 0.2.%{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://mongrel.rubyforge.org/
Source0: %{gemname}-%{version}.pre2.gem
#NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(gem_plugin) => 0.2.3
Requires: rubygem(daemons) => 1.0.10
Requires: rubygem(rubyforge) >= 2.0.3
Requires: rubygem(gemcutter) >= 0.3.0
Requires: rubygem(rake-compiler) => 0.7.0
Requires: rubygem(hoe) >= 2.5.0
BuildRequires: rubygems
Provides: rubygem(%{gemname}) = %{version}

%description
Mongrel is a small library that provides a very fast HTTP 1.1 server for Ruby
web applications.  It is not particular to any framework, and is intended to
be just enough to get a web application running behind a more complete and
robust web server.
What makes Mongrel so fast is the careful use of an Ragel extension to provide
fast, accurate HTTP 1.1 protocol parsing. This makes the server scream without
too many portability issues.
See http://mongrel.rubyforge.org for more information.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}*/bin -type f | xargs chmod a+x

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/mongrel_rails
%{gemdir}/gems/%{gemname}-%{version}.pre2/
%doc %{gemdir}/doc/%{gemname}-%{version}.pre2 
%{gemdir}/cache/%{gemname}-%{version}.pre2.gem
%{gemdir}/specifications/%{gemname}-%{version}.pre2.gemspec


%changelog
* Mon Nov 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-0.2.5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-0.2.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-0.2.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-0.2.2m)
- full rebuild for mo7 release

* Wed Aug 04 2010  <meke@localhost.localdomain>
- (1.2.0-0.2.1m)
- update 1.2.0 pre2.
-- 1.1.5 unsupported ruby-1.9.x

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.3-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.3-1m)
- Initial package for Momonga Linux

