%global         momorel 1

Summary:        x86 processor information tool
Name:           x86info
Version:        1.30
Release:        %{momorel}m%{?dist}
Group:          System Environment/Base
License:        GPLv2+
URL:            http://www.codemonkey.org.uk/projects/x86info/
Source0:        http://www.codemonkey.org.uk/projects/x86info/x86info-%{version}.tgz
NoSource:       0
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch:  %{ix86} x86_64

Obsoletes:      kernel-utils

%description
x86info displays diagnostic information about x86 processors, such
as cache configuration and supported features.

%prep
%setup -q

%build
# disabled %{?_smp_mflags}
make CFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}/usr/share/man/man{1,8}

install x86info %{buildroot}%{_sbindir}/x86info
install x86info.1 %{buildroot}/usr/share/man/man1/
install lsmsr %{buildroot}%{_sbindir}/lsmsr

chmod -R a-s %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_sbindir}/x86info
%{_sbindir}/lsmsr
%attr(0644,root,root) %{_mandir}/man1/x86info.1*

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Thu Aug 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.29-1m)
- update 1.29

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.25-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.21-2m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.21-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20-2m)
- rebuild against gcc43

* Tue Mar 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20-1m)
- import to Momonga from fc-devel

* Wed Sep 27 2006 Dave Jones <davej@redhat.com>
- New upstream (1.20)
  Fixes 'silent' output, and recognises Intel Core Extreme.

* Sat Sep 23 2006 Dave Jones <davej@redhat.com>
- New upstream (1.19)
  Improved identification of numerous new Intel CPUs.

* Wed Jul 12 2006 Dave Jones <davej@redhat.com>
- New upstream (1.18)

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1:1.17-1.22.1
- rebuild

* Thu Feb 09 2006 Dave Jones <davej@redhat.com>
- rebuild.

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Nov  4 2005 Dave Jones <davej@redhat.com>
- Update to upstream 1.17

* Sat Sep 24 2005 Dave Jones <davej@redhat.com>
- Update to upstream 1.16
  (Various 64bit fixes).

* Fri Sep  2 2005 Dave Jones <davej@redhat.com>
- Update to upstream 1.15

* Sun Aug  7 2005 Dave Jones <davej@redhat.com>
- Update to upstream 1.14 (now builds on x86-64)

* Fri Apr 15 2005 Florian La Roche <laroche@redhat.com>
- remove empty scripts

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4

* Tue Feb  8 2005 Dave Jones <davej@redhat.com>
- Update to upstream 1.13

* Sat Dec 18 2004 Dave Jones <davej@redhat.com>
- Initial packaging, based upon kernel-utils.

