%global         momorel 1

Name:           openconnect
Version:        5.99
Release:        %{momorel}m%{?dist}
Summary:        Open client for Cisco AnyConnect VPN

Group:          Applications/Internet
License:        LGPLv2+
URL:            http://git.infradead.org/users/dwmw2/openconnect.git
Source0:        ftp://ftp.infradead.org/pub/openconnect/openconnect-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  libxml2-devel
BuildRequires:  gtk2-devel
BuildRequires:  GConf2-devel
BuildRequires:  dbus-devel
BuildRequires:  libproxy-devel >= 0.4.4
BuildRequires:  gnutls-devel >= 3.2.0
Requires:       vpnc
# The "lasthost" and "autoconnect" gconf keys will cause older versions of
# NetworkManager-openconnect to barf
#Conflicts:	NetworkManager-openconnect < 0.7.0.99-2

%description
This package provides a client for Cisco's "AnyConnect" VPN, which uses
HTTPS and DTLS protocols.

%package devel
Summary:        Development package for OpenConnect VPN authentication tools
Group:          Applications/Internet
Requires:       %{name} = %{version}-%{release}

%description devel
This package provides the core HTTP and authentication support from
the OpenConnect VPN client, to be used by GUI authentication dialogs
for NetworkManager etc.

%prep
%setup -q

%build

%configure \
	--with-vpnc-script=/etc/vpnc/vpnc-script \
	--htmldir=%{_docdir}/%{name}-%{version} \
	--with-gnutls

make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}%{_libdir}/*.la
%find_lang %{name}

%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc TODO COPYING.LGPL
%{_sbindir}/openconnect
%{_libdir}/libopenconnect.so.*
%{_mandir}/man8/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libopenconnect.so
%{_includedir}/openconnect.h
%{_libdir}/pkgconfig/openconnect.pc

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.99-1m)
- update 5.99

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.00-2m)
- split devel sub package

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.00-1m)
- update to 5.00
- rebuild against gnutls-3.2.0

* Sun Jul 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.06-1m)
- update to 4.06

* Tue Apr 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.02-2m)
- fix build on x86_64

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.02-1m)
- update to 3.02

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26-2m)
- rebuild for new GCC 4.5

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.26-1m)
- update to 2.26

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.25-3m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25-2m)
- rebuild against libproxy-0.4.4

* Mon May 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.25-1m)
- [SECURITY] fixed verification about SSL server certificates
- update 2.25

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11-2m)
- rebuild against openssl-1.0.0

* Thu Nov 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11-1m)
- update 2.11

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-1m)
- import from Fedora

* Wed Apr  1 2009 David Woodhouse <David.Woodhouse@intel.com> - 1.10-1
- Update to 1.10.

* Wed Mar 18 2009 David Woodhouse <David.Woodhouse@intel.com> - 1.00-1
- Update to 1.00.

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.99-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> - 0.99-2
- rebuild with new openssl

* Tue Dec 16 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.99-1
- Update to 0.99.
- Fix BuildRequires

* Mon Nov 24 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.98-1
- Update to 0.98.

* Thu Nov 13 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.97-1
- Update to 0.97. Add man page, validate server certs.

* Tue Oct 28 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.96-1
- Update to 0.96. Handle split-includes, MacOS port, more capable SecurID.

* Thu Oct 09 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.95-1
- Update to 0.95. A few bug fixes.

* Thu Oct 09 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.94-3
- Include COPYING.LGPL file

* Mon Oct 07 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.94-2
- Fix auth-dialog crash

* Mon Oct 06 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.94-1
- Take cookie on stdin so it's not visible in ps.
- Support running 'script' and passing traffic to it via a socket
- Fix abort when fetching XML config fails

* Sun Oct 05 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.93-1
- Work around unexpected disconnection (probably OpenSSL bug)
- Handle host list and report errors in NM auth dialog

* Sun Oct 05 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.92-1
- Rename to 'openconnect'
- Include NetworkManager auth helper

* Thu Oct 02 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.91-1
- Update to 0.91

* Thu Oct 02 2008 David Woodhouse <David.Woodhouse@intel.com> - 0.90-1
- First package
