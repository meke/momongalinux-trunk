%global momorel 8
%global emacsver %{_emacs_version}
%global apelver 10.8-6m
%global e_sitedir %{_emacs_sitelispdir}

Summary: A front-end to mpg123 and OggVorbis audio player
Name: emacs-mpg123
Version: 1.55
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source: mpg123-pkg.tar.gz
Source1: http://www.gentei.org/~yuuji/software/mpg123el/mpg123.el
Source2: http://www.gentei.org/~yuuji/software/mpg123el/id3.el
Source3: http://www.gentei.org/~yuuji/software/mpg123el/id3el-0.05.tar.gz
Source4: http://www.gentei.org/~yuuji/software/mpg123el/tagput/tagput.c
URL: http://www.gentei.org/~yuuji/software/mpg123el/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-apel >= %{apelver}
Requires: emacs >= %{emacsver}
Requires: emacs-apel >= %{apelver}
Requires: mpg321
Obsoletes: mpg123-emacs
Obsoletes: mpg123-xemacs

Obsoletes: elisp-mpg123
Provides: elisp-mpg123

%description
The mpg123.el Emacs-Lisp program is a
front-end to mpg123 audio player and
OggVorbis audio player. You can select and
play an mp3 files from the list in your
Emacs's buffer with familiar interface.
#'

%prep
%setup -q -a 3 -n mpg123-pkg
cp %{SOURCE1} .
cp %{SOURCE2} .
cp %{SOURCE4} .

%build
pushd id3el-0.05
make CFLAGS="%{optflags}"
popd

make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir} \
  VERSION_SPECIFIC_LISPDIR=%{buildroot}%{_datadir}/emacs/%{emacsver}/site-lisp

make CFLAGS="%{optflags}" tagput

%install
rm -rf %{buildroot}

make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir} \
  VERSION_SPECIFIC_LISPDIR=%{buildroot}%{_datadir}/emacs/%{emacsver}/site-lisp \
  install

%{__mkdir_p} %{buildroot}%{_bindir}
%{__install} -m 755 id3el-0.05/id3put %{buildroot}%{_bindir}/
%{__install} -m 755 tagput %{buildroot}%{_bindir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc id3el-0.05/README
%{_bindir}/*
%{e_sitedir}/mpg123

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.55-8m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.55-7m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.55-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.55-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.55-4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.55-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.55-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.55-1m)
- update to revision 1.55

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.51-9m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.51-8m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.51-7m)
- merge mpg123-emacs to elisp-mpg123
- kill mpg123-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.51-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.51-5m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.51-4m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.51-3m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.51-2m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.51-1m)
- update to revision 1.51
-- drop old patches

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-28m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-27m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-26m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-25m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-24m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-23m)
- rebuild against apelver 10.7-11m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-22m)
- rebuild against xemacs-21.5.28
- add Patch3: MPG123-MK-xemacs.patch

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-21m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-20m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-19m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-18m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-17m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-16m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-15m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-14m)
- rebuild against apel-10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-13m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-12m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-11m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-10m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.33-9m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.33-8m)
- use %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-7m)
- rebuild against emacs 22.0.50

* Tue Nov 23 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.33-6m)
- rebuild against emacs-21.3.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.33-5m)
- revised patch2.

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.33-4m)
- add patch.

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-3m)
- rebuild against emacs-21.3
- use %%_prefix %%_datadir macro

* Mon Jan 13 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.33-2m)
- rebuild against elisp-apel-10.4-1m

* Sat Sep 28 2002 Junichiro Kita <kita@momonga-linux.org>
- (1.33-1m)
- update to 1.33
- add tagput for editing ogg tags
- NoSource yame!

* Mon Sep 23 2002 Junichiro Kita <kita@momonga-linux.org>
- (1.31-5m)
- kanji no id3 tag mo OK yo.

* Mon Sep 23 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.31-4m)
- noarch ha dame yo.

* Mon Sep 23 2002 OGAWA Youhei <t-nyan2@nifty.com>
- (1.31-3m)
- append Requires field, mpg321 package

* Mon Sep 23 2002 Junichiro Kita <kita@momonga-linux.org>
- (1.31-2m)
- append id3.el and id3put

* Mon Sep 23 2002 Junichiro Kita <kita@momonga-linux.org>
- (1.31-1m)
- initial revision
