%global         momorel 2
%global         __perl_requires %{_tmppath}/%{name}-%{version}.requires

Name:           perl-PDF-API2
Version:        2.021
Release:        %{momorel}m%{?dist}
Summary:        PDF::API2 Perl module
License:        LGPLv2+
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/PDF-API2/
Source0:        http://www.cpan.org/authors/id/S/SS/SSIMMS/PDF-API2-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= v5.8.1
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Font-TTF
BuildRequires:  perl-IO-Compress >= 1.0
Requires:       perl-Font-TTF
Requires:       perl-IO-Compress >= 1.0
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
PDF::API2 Perl module

%prep
%setup -q -n PDF-API2-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

# custom requires script
cat <<EOF > %{__perl_requires}
#!/bin/bash
cat | grep -v %{buildroot}%{_docdir} | /usr/lib/rpm/perl.req $* \
        | grep -v 'Win32' | grep -v ''
EOF
chmod 700 %{__perl_requires}

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}
rm -f %{__perl_requires}

%files
%defattr(-,root,root,-)
%doc Changes contrib dist.ini examples LICENSE PATENTS README
%{perl_vendorlib}/PDF/API2.pm
%{perl_vendorlib}/PDF/API2
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.021-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.021-1m)
- update to 2.021

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.020-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.020-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.020-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.020-2m)
- rebuild against perl-5.16.3

* Mon Jan 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.020-1m)
- update to 2.020

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.019-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.019-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.019-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.019-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.019-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.019-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.019-2m)
- rebuild for new GCC 4.6

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.019-1m)
- update to 2.019

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.018-1m)
- update to 2.018

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.017-1m)
- update to 2.017

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.016-1m)
- update to 2.016
- Specfile re-generated by cpanspec.

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.73-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.73-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.73-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-2m)
- rebuild against perl-5.10.1

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-1m)
- update to .0.73

* Tue Feb 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72.003-1m)
- update to 0.72.003

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.72-2m)
- rebuild against rpm-4.6

* Sat Nov  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71.001-1m)
- update to 0.71.001

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.69-2m)
- rebuild against gcc43

* Sat Jan 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.69-1m)
- update to 0.69

* Sat Nov 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Sun Nov  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-1m)
- update to 0.66

* Thu Sep 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.65-1m)
- update to 0.65
- do not use %%NoSource macro

* Tue Aug 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- update to 0.63

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Sun Apr 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.59.002-2m)
- use vendor

* Wed Mar 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59.002-1m)
- update to 0.59.002

* Sat Mar 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Fri Jan  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.55-1m)
- spec file was autogenerated
