%global momorel 10
%global qtver 4.8.0

Summary: A tool to generate Qt bindings for Qt Script
Name: qtscriptgenerator
Version: 0.1.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Libraries
URL: http://code.google.com/p/qtscriptgenerator/
Source0: http://qtscriptgenerator.googlecode.com/files/%{name}-src-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-gcc44.patch
Patch1: include_everything2.patch
Patch2: %{name}-src-%{version}-no_QFileOpenEvent.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: coreutils
BuildRequires: libxslt
BuildRequires: phonon-devel
BuildRequires: sed

%description
Qt Script Generator is a tool to generate Qt bindings for Qt Script.
With the generated bindings you get access to substantial portions of
the Qt API from within Qt Script.

%package -n qtscriptbindings
Summary: Qt bindings for Qt Script
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description -n qtscriptbindings
Bindings providing access to substantial portions of the Qt API
from within Qt Script.

%prep
%setup -q -n %{name}-src-%{version}

%patch0 -p0 -b .gcc44
%patch1 -p0 -b .phonon
%patch2 -p1 -b .no_QFileOpenEvent

# remove phonon (from gentoo ebuild)
sed -i "/typesystem_phonon.xml/d" generator/generator.qrc
sed -i "/qtscript_phonon/d" qtbindings/qtbindings.pro

# for docs
install -m 644 tools/qsexec/README.TXT README.qsexec

%build
export QTDIR=%{_qt4_prefix} INCLUDE=%{_qt4_headerdir}

# DO NOT USE %%{?_smp_mflags}, make -j8 fail (0.1.0-1m)

pushd generator 
%{_qt4_qmake}
make
./generator
popd

pushd qtbindings
%{_qt4_qmake}
make
popd

pushd tools/qsexec/src
%{_qt4_qmake}
make
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# preparing...
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_qt4_bindir}
mkdir -p %{buildroot}%{_qt4_plugindir}/script

# install doesn't do symlinks
cp -a plugins/script/libqtscript* %{buildroot}%{_qt4_plugindir}/script/

# install qsexec
install -m 755 tools/qsexec/qsexec %{buildroot}%{_bindir}/

# install generator
install -m 755 generator/generator %{buildroot}%{_qt4_bindir}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE.GPL README* doc examples
%{_qt4_bindir}/generator

%files -n qtscriptbindings
%defattr(-,root,root)
%{_bindir}/qsexec
%{_qt4_plugindir}/script/libqtscript*

%changelog
* Wed Dec 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-10m)
- import no_QFileOpenEvent.patch from fedora to enable build with new Qt

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-9m)
- rebuild for new GCC 4.6

* Wed Dec  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-8m)
- good-bye phonon for the moment, keep BR and see you later

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-7m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-5m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-4m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-1m)
- initial package for amarok-2.1
- import include_everything2.patch from gentoo's BTS
  http://bugs.gentoo.org/262256
- import gcc44.patch from Fedora
