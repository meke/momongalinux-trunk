%global         momorel 1
%global         unstable 1
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         kdever 4.13.1
%global         kdelibsrel 1m
%global         qtver 4.8.6
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         ftpdirver 1.0.71
%global         sourcedir %{release_dir}/%{name}/%{ftpdirver}/src

Name:           kscreen
Version:        %{ftpdirver}
Release:        %{momorel}m%{?dist}
Summary:        KDE Display Management software
# KDE e.V. may determine that future GPL versions are accepted
License:        GPLv2 or GPLv3
Group:          Applications/System
URL:            https://projects.kde.org/projects/playground/base/kscreen
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  libkscreen-devel >= 1.0.4
BuildRequires:  qjson-devel >= 0.8.1

%description
KCM and KDED modules for managing displays in KDE.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd
make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%files
%doc COPYING
%{_kde4_bindir}/kscreen-console
%{_kde4_libdir}/kde4/kcm_kscreen.so
%{_kde4_libdir}/kde4/kded_kscreen.so
%{_kde4_libdir}/kde4/plasma_applet_kscreen.so
%{_kde4_appsdir}/kcm_kscreen
%{_kde4_appsdir}/plasma/packages/org.kde.plasma.kscreen.qml
%{_kde4_datadir}/kde4/services/kcm_kscreen.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-kscreen-qml.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-kscreen.desktop
%{_kde4_datadir}/kde4/services/kded/kscreen.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/*.mo
%{_kde4_iconsdir}/hicolor/*/*/*.png
%{_kde4_iconsdir}/hicolor/*/*/*.svgz

%changelog
* Wed May 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.71-1m)
- update to 1.0.71

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2.1-1m)
- update to 1.0.2.1

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Wed Jun 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.92-1m)
- update to 0.0.92

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.81-1m)
- update to 0.0.81

* Tue Jan 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.71-1m)
- initial build for Momonga Linux
