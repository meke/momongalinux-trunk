%global momorel 1

%global xfce4ver 4.10.0
%global major 1.1

Summary:        A cpufreq plugin for the Xfce panel
Name:           xfce4-cpufreq-plugin
Version:        1.1.0
Release:        %{momorel}m%{?dist}

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/panel-plugins/xfce4-cpufreq-plugin
Source0:        http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  intltool >= 0.35.0
BuildRequires:  libtool
BuildRequires:  xfce4-dev-tools >= %{xfce4ver}
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:       xfce4-panel >= %{xfce4ver}
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The CpuFreq Plugin shows in the Xfce Panel the following information:
    current CPU frequency
    current used governor

In a separate dialog it provides you following information:
    all available CPU frequencies
    all available governors
    used driver for the CPU

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

find %{buildroot} -name "*.la" -delete

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
##%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_libdir}/xfce4/panel/plugins/libcpufreq.so*
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/xfce4/panel/plugins/cpufreq.desktop
%{_datadir}/locale/*

%changelog
* Sun Jan  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0-2m)
- build against xfce4-4.10.0

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-11m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1-8m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-7m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1-5m)
- add autoreconf. need libtool-2.2.x

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-4m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-1m)
- initial Momonga package based on PLD
