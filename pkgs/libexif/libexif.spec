%global momorel 1

Summary: EXIF Tag Parsing Library
Name: libexif
Version: 0.6.21
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://libexif.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.6.20-fix-libexif_pc.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: pkgconfig

%description
Most digital cameras produce EXIF files, which are JPEG files
with extra tags that contain information about the image.
The EXIF library allows you to parse an EXIF file and read
the data from those tags.

%package devel
Summary: Headers and links to compile against the libexif library
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Group: Development/Libraries

%description devel
Most digital cameras produce EXIF files, which are JPEG files
with extra tags that contain information about the image.
The EXIF library allows you to parse an EXIF file and read
the data from those tags.

%prep
%setup -q

%patch0 -p1 -b .fix-libexif_pc

%build
%configure --with-doc-dir=%{buildroot}%{_docdir}/%{name}
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# clean up docs
rm -rf %{buildroot}%{_docdir}/%{name}

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING ChangeLog* INSTALL NEWS README
%{_libdir}/*.so.*
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%files devel
%defattr(-,root,root)
%doc doc/doxygen-output/libexif-api.html
%{_includedir}/libexif
%{_libdir}/pkgconfig/libexif.pc
%{_libdir}/*.a
%{_libdir}/*.so

%changelog
* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.21-1m)
- [SECURITY] CVE-2012-2812 CVE-2012-2813 CVE-2012-2814 CVE-2012-2836
- [SECURITY] CVE-2012-2837 CVE-2012-2840 CVE-2012-2841 CVE-2012-2845
- update to 0.6.21

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.20-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.20-1m)
- update to 0.6.20

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.19-2m)
- full rebuild for mo7 release

* Thu Jan  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.19-1m)
- drop patches, CVE-2007-6351 and CVE-2007-6352 was already fixed in 0.6.17
- 0.6.18 is affected CVE-2009-3895, but resolved at 0.6.19

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.16-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.16-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.16-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.16-3m)
- rebuild against gcc43

* Thu Dec 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.16-2m)
- [SECURITY] CVE-2007-6351 CVE-2007-6352
- import cve-2007-6351.patch and cve-2007-6352.patch from RHEL5
 +* Sat Dec 15 2007 Matthias Clasen <mclasen@redhat.com> 
 +- Add patch for CVE-2007-6351. Fixes bug #425681
 +- Add patch for CVE-2007-6352. Fixes bug #425681

* Thu Jun 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.16-1m)
- [SECURITY] CVE-2006-4168
- version 0.6.16
- update fix-libexif_pc.patch

* Wed Jun  6 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.14-1m)
- [SECURITY] CVE-2007-2645
- update to 0.6.14
- remove Patch1 libexif-badentry.patch. upstream fixed.

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.13-3m)
- use md5sum_src0

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.13-2m)
- delete libtool library

* Mon Mar 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.13-1m)
- version 0.6.13 thanks, Mukai-san
  http://bugs.gentoo.org/show_bug.cgi?id=86740
- add libexif-0.6.13-fix-libexif_pc.patch
- import libexif-badentry.patch from opensuse
 +* Thu Jan 19 2006 - meissner@suse.de
 +- applied fix for crash in bad exif data #144008
- change URL
- remove gcc4.patch

* Wed Dec 7 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.6.12-2m)
- add gcc4 patch

* Sat Dec 3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.12-1m)
- update to 0.6.12
- for eog-2.12.2

* Thu Jan 20 2005 Yuya Yamaguchi <bebe@hepo.jp>
- (0.5.12-2m)
- License: LGPL

* Mon Aug 6 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (0.5.12-1m)
- major bugfixes
- use %%NoSource macro

* Mon Jun 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.5.9-2m)
- libtool

* Wed Feb 12 2003 Yuya Yamaguchi <bebe@momonga-linux.org>
- (0.5.9-1m)
- Ver up.

* Mon Jan 6 2003 Yuya Yamaguchi <bebe@momonga-linux.org>
- (0.5.8-1m)
- Ver. up
- remove "--included-gettext" from %%configure

* Fri Aug 30 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (0.5.6-1m)
- Ver. up

* Wed Jul 31 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (0.5.4-1m)
- First build for Momonga Linux
