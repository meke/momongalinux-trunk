%global momorel 35
%global dirname 37536
%global ut_patch_date 20110409

Summary: A Japanese character input system library (with dictionary)
Name: anthy
Version: 9100h
Release: %{momorel}m%{?dist}
License: GPL and LGPL
URL: http://anthy.sourceforge.jp/
Group: Applications/System
#Source0: http://dl.sourceforge.jp/%{name}/%{dirname}/%{name}-%{version}.tar.gz
#NoSource: 0
Source0: http://www.fenix.ne.jp/~G-HAL/soft/nosettle/anthy-9100h.patch13-release-2011Y23.alt-depgraph-110208-angie.zipdic-201101.tar.lzma
# http://www.geocities.jp/ep3797/anthy_dict_01.html
Source1: http://dl.sourceforge.net/sourceforge/mdk-ut/anthy-ut-patches-%{ut_patch_date}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
BuildRequires: pkgconfig
Obsoletes: anthy-xemacs

%description
Anthy is free and secure Japanese input system.

%package devel
Summary: Headers of %{name} for development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Anthy development package: static libraries, header files, and the like.

%package -n emacs-anthy
Group: Applications/Editors
Summary: Anthy mode for Emacs
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{_emacs_version}
Obsoletes: anthy-emacs
BuildArch: noarch
Obsoletes: elisp-anthy

%description -n emacs-anthy
Anthy mode for Emacs.

%prep
%setup -q

# (ut) update dictionaries and apply patches
tar -jxf %{SOURCE1}
cd anthy-ut-patches-*
sh ./apply-patches.sh

%build
autoreconf -if
%configure
%make
#%%make update_params0

%install
rm -rf %{buildroot}
%makeinstall

# remove Makefile from doc
rm -f doc/Makefile*

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog DIARY INSTALL NEWS README* doc
%config(noreplace) %{_sysconfdir}/anthy-conf
%{_bindir}/anthy-agent
%{_bindir}/anthy-dic-tool
%{_bindir}/anthy-morphological-analyzer
%{_libdir}/*.so
%{_libdir}/*.so.*
%{_datadir}/anthy 

%files devel
%defattr(-,root,root,-)
%{_includedir}/anthy
%{_libdir}/pkgconfig/anthy.pc
%{_libdir}/*.a

%files -n emacs-anthy
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/anthy
%{_emacs_sitelispdir}/anthy/*.el
%{_emacs_sitelispdir}/anthy/*.elc

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9100h-35m)
- rebuild for emacs-24.1

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-34m)
- update ut-patches-20110409

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9100h-33m)
- rename elisp-anthy to emacs-anthy

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9100h-32m)
- rebuild for new GCC 4.6

* Thu Dec 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-31m)
- update ut-patches-20101205

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9100h-30m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- update anthy-9100h.patch13-release-2010915-fix2010X25.alt-depgraph-100603d.alt-cannadic-100603-patch100628-ticket22612.zipdic-201008.tar.lzma
- update ut-patches-20101025

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9100h-28m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-27m)
- update anthy-9100h.patch13B-23-iconv-ucdict.201A0720.alt-depgraph-100603d.alt-cannadic-100603-patch100628.zipdic-201005-patch100614
- update ut-patches-20100710

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-26m)
- update ut-patches-20100615
-- G-HAL Anthy anthy-9100h.patch13B-23-iconv-ucdict.2010512.alt-depgraph-100603.alt-cannadic-100603.zipdic-201005-patch100608.tar.lzma

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-25m)
- rebuild against emacs-23.2

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9100h-24m)
- update ut-patches-20100423
-- G-HAL Anthy anthy-9100h.patch13B-23-iconv.2010327.alt-depgraph-100120-patch100126.alt-cannadic-091230.tar.lzma

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-23m)
- rename anthy-emacs to elisp-anthy
- kill anthy-xemacs

* Sat Feb 27 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9100h-22m)
- update ut-patches-20100131
-- update G-HAL Anthy patch13B

* Thu Dec 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-21m)
- update ut-patches-20091210

* Thu Dec 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-20m)
- update ut-patches-20091208
-- Change G-HAL Anthy patch13B

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-18m)
- update ut-patches-20091030

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-17m)
- update ut-patches-20091014

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-16m)
- update ut-patches-20091001

* Thu Sep 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-15m)
- update ut-patches-20090902

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-14m)
- update ut-patches-20090818

* Sun Aug 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-13m)
- update ut-patches-20090813

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (9100h-12m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (9100h-11m)
- rebuild against emacs 23.0.96

* Sat Jul 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-10m)
- update ut-patches-20090704

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-9m)
- rebuild against emacs-23.0.95

* Mon Jun  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-8m)
- update to ut-patches(20090604)

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-7m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-6m)
- update to ut-patches(20090507)

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-5m)
- rebuild against xemacs-21.5.29

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-4m)
- update to ut-patches(20090414)

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-3m)
- rebuild against emacs-23.0.92

* Mon Mar 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100h-2m)
- update to ut-patches(20090228)

* Thu Feb 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100h-1m)
- update to 9100h with ut-patches

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100e-4m)
- rebuild against rpm-4.6

* Thu Jan  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100e-3m)
- update ut-hack-20090106

* Sat Jan  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (9100e-2m)
- version down 9100e. 9100f was withdrawn. 
- update ut-hack-20081211

* Sun Nov 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9100f-1m)
- version 9100f
- delete unpatchable Patch11 and Patch12
- drop Source1 and ut-hack part

* Wed Nov 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100e-8m)
- no NoSource: 1
- no NoPatch: 10

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (9100e-7m)
- no NoPatch for patch11

* Sun Sep 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9100e-6m)
- add Patch10, Patch11 and Patch12

* Mon Sep  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9100e-5m)
- add Source1: http://dl.sourceforge.net/sourceforge/mdk-ut/anthy-ut-patches-%%{dic_date}.tar.bz2

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9100e-4m)
- rebuild against xemacs-21.5.28
- use site-packages on xemacs

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (9100e-3m)
- add anthy-9100e-fix-segfault-vu.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9100e-2m)
- rebuild against gcc43

* Tue Jan 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9100e-1m)
- version 9100e

* Thu Nov  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9100d-1m)
- version 9100d

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9100c-1m)
- version 9100c

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9100b-1m)
- version 9100b

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9100-2m)
- revise spec

* Wed Jul  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9100-1m)
- version 9100
- remove fake-version.patch 

* Tue Apr 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8700b-3m)
- add fake-version.patch for kasumi-2.2

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8700b-2m)
- modify %%install (rm *.elc -> rm -f *.elc)

* Mon Mar  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8700b-1m)
- version 8700b

* Wed Feb 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8700-1m)
- version 8700

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (8300-2m)
- delete libtool library

* Thu Nov  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8300-1m)
- version 8300

* Tue Jul 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7900-1m)
- version 7900

* Tue Apr 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7500b-1m)
- version 7500b

* Mon Mar 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7500-1m)
- version 7500

* Fri Dec 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (7100b-2m)
- separate 'anthy-emacs' package
- add 'anthy-xemacs' package

* Thu Nov  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7100b-1m)
- version 7100b

* Tue Nov  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7100-1m)
- version 7100

* Thu Sep 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6700b-2m)
- remove xim.d/Anthy-uim-xim

* Sun Jul  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6700b-1m)
- version 6700b

* Thu Jun 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6700-1m)
- version 6700

* Sun Apr 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6300d-2m)
- move %%{_libdir}/*.so from anthy-devel to anthy

* Tue Mar  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6300d-1m)
- version 6300d

* Sun Mar  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6300c-1m)
- version 6300c

* Tue Mar  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6300-1m)
- version 6300

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5900-1m)
- version 5900
- add GTK_IM_MODULE and QT_IM_MODULE to xim.d/Anthy-uim-xim

* Fri Aug 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5500-2m)
- -n %%{name}-devel -> devel
- add %%post and %%postun sections

* Sat Jul 10 2004 kourin <kourin@fh.freeserve.ne.jp>
- (5500-1m)
- version 5500

* Mon Mar 1 2004 kourin <kourin@fh.freeserve.ne.jp>
- (5100-1m)
- version 5100

* Tue Feb 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4700-2m)
- separate libanthy and libanthy-devel packages

* Thu Jan 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4700-1m)
- version 4700

* Tue Oct 14 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4300b-1m)
- import

* Tue Aug 5 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 4200b-1
- Version updated.

* Thu Jul 10 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 4200-1
- Version updated.
- added description.

* Fri Nov 25 2002 OGUMA "dellin" Hironori <dellin@team-ct.org> 3500c-1
- Version updated.
- Added some documents.

* Fri Nov 22 2002 OGUMA "dellin" Hironori <dellin@team-ct.org> 3500b-1
- Version updated.

* Thu Jan  3 2002 Yusuke Tabata <yusuke@kmc.gr.jp>
- Changed my mail address and URL of web page

* Thu Nov 11 2001 Takayoshi Nobuoka <taka@trans-nt.com>
- Added %{_sysconfdir}/* at %files

* Wed Sep  9 2001 Takayoshi Nobuoka <taka@trans-nt.com>
- Used _bindir and so on.

* Sun Aug 19 2001 Tabatee <yusuke@kmc.kyoto-u.ac.jp>
- Initial build.
