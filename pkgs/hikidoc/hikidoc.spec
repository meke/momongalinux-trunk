%global momorel 4

Summary: HikiDoc is a Text2HTML converter written by Ruby
Name: hikidoc
Version: 0.0.6
Release: %{momorel}m%{?dist}
Source0: http://rubyforge.org/frs/download.php/72253/hikidoc-%{version}.tgz
NoSource: 0
URL: http://rubyforge.org/projects/hikidoc/
License: Modified BSD
Group: Applications/Internet
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: ruby

%description
HikiDoc is a Text2HTML converter written by Ruby.

%prep
%setup -q

%build
ruby setup.rb config
ruby setup.rb setup

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
ruby setup.rb install --prefix=%{buildroot} 

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/hikidoc
%{ruby_sitelibdir}/hikidoc.rb

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.6-3m)
- rebuild for new GCC 4.5

* Sun Sep  5 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-2m)
- use ruby_sitelibdir macro

* Sun Sep  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.6-1m)
- update 0.0.6(ruby-1.9.2 support)
- need rabbit + ruby-1.9.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.3-6m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-5m)
- build with ruby18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-3m)
- rebuild against rpm-4.6

* Tue Nov 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.3-1m)
- fix libdir multilib problem

* Tue Nov 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.3-1m)
- Initial Commit Momonga Linux
