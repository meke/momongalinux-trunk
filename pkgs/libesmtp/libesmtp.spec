%global momorel 2
%define plugindir %{_libdir}/esmtp-plugins

Summary:        SMTP client library
Name:           libesmtp
Version:        1.0.6
Release: %{momorel}m%{?dist}
License:        LGPLv2+
Group:          System Environment/Libraries
Source:         http://www.stafford.uklinux.net/libesmtp/%{name}-%{version}.tar.bz2
NoSource: 0
URL:            http://www.stafford.uklinux.net/libesmtp/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  openssl-devel pkgconfig

%description
LibESMTP is a library to manage posting (or submission of) electronic
mail using SMTP to a preconfigured Mail Transport Agent (MTA) such as
Exim. It may be used as part of a Mail User Agent (MUA) or another
program that must be able to post electronic mail but where mail
functionality is not the program's primary purpose.

%package devel
Summary: Headers and development libraries for libESMTP
# example file is under the GPLv2+
License: LGPLv2+ and GPLv2+
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}, openssl-devel

%description devel
LibESMTP is a library to manage posting (or submission of) electronic
mail using SMTP to a preconfigured Mail Transport Agent (MTA) such as
Exim.

The libesmtp-devel package contains headers and development libraries
necessary for building programs against libesmtp.

%prep 
%setup -q

# Keep rpmlint happy about libesmtp-debuginfo...
chmod a-x htable.c

%build

if pkg-config openssl ; then
  export CFLAGS="$CFLAGS $RPM_OPT_FLAGS `pkg-config --cflags openssl`"
  export LDFLAGS="$LDFLAGS `pkg-config --libs-only-L openssl`"
fi
%configure --with-auth-plugin-dir=%{plugindir} --enable-pthreads \
  --enable-require-all-recipients --enable-debug \
  --enable-etrn --disable-isoc --disable-more-warnings --disable-static
%make 

cat<<EOF > libesmtp.pc
prefix=%{_prefix}
exec_prefix=%{_prefix}
libdir=%{_libdir}
includedir=%{_includedir}

Name: libESMTP
Version: %{version}
Description: SMTP client library.
Requires: openssl
Libs: -pthread -L\${libdir} -lesmtp
Cflags:
EOF

cat<<EOF > libesmtp-config
#! /bin/sh
exec pkg-config "\$@" libesmtp
EOF

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install INSTALL='install -p'
rm %{buildroot}/%{_libdir}/*.la
rm %{buildroot}/%{_libdir}/esmtp-plugins/*.la
install -p -m644 -D libesmtp.pc %{buildroot}%{_libdir}/pkgconfig/libesmtp.pc


%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING.LIB NEWS Notes README
%{_libdir}/libesmtp.so.*
%{plugindir}

%files devel
%defattr(-,root,root,-)
%doc examples COPYING
%{_bindir}/libesmtp-config
%{_prefix}/include/*
%{_libdir}/libesmtp.so
%{_libdir}/pkgconfig/libesmtp.pc

%changelog
* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-2m)
- revise pkg-config

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-1m)
- reimport from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-13m)
- full rebuild for mo7 release

* Tue May 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-12m)
- [SECURITY] CVE-2010-1192 CVE-2010-1194 CVE-2009-2408
- import a security patch from Fedora 13 (1.0.4-13)

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-11m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-10m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (1.0.4-8m)
- define __libtoolize (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-7m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-6m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-5m)
- sync with Fedora devel (install libesmtp.pc)

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-3m)
- rebuild against gcc43

* Fri Jul 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-2m)
- add -maxdepth 1 to del .la

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3r1-3m)
- delete libtool library

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.3r1-2m)
- rebuild against openssl-0.9.8a

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.3r1-1m)
- ver up. Sync with FC3(1.0.3r1-2).

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.2-2m)
- enable x86_64.

* Mon Jan 19 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.2-1m)
- update to version 1.0.2

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.1.5m)
- s/Copyright:/License:/

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-0.1.4m)
  rebuild against openssl 0.9.7a

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0-0.1.3m)
- fix build problem against autoconf-2.53

* Thu Aug  1 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.1.2m)
- devel: Requires: %{name} = %{version}-%{release}

* Wed Jul 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0-0.1.1m)
- first release for momonga
- version 1.0rc1
