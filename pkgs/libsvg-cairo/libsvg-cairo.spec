%global momorel 12

Summary:        Render SVG Content Using cairo
Name:           libsvg-cairo
Version:        0.1.6
Release:        %{momorel}m%{?dist}
License:        LGPL
Group:          System Environment/Libraries
URL:            http://www.cairographics.org/
Source0: http://cairographics.org/snapshots/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cairo-devel, libjpeg-devel >= 8a, libsvg-devel

%description
libsvg-cairo provides the ability to render SVG content from files or
buffers. All rendering is performed using the cairo rendering library.

%package devel
Summary:        Render SVG Content Using cairo
Group:          Development/Libraries
Requires:       %{name} = %{version}
Requires:       libsvg-devel, cairo-devel

%description devel
libsvg-cairo provides the ability to render SVG content from files or
buffers. All rendering is performed using the cairo rendering library.


%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

rm -f %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING INSTALL NEWS README
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/*.a
%{_libdir}/*.so
%{_includedir}/svg-cairo.h


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.6-10m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.6-9m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.6-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.6-6m)
- rebuild against libjpeg-7

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.6-5m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.6-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.6-2m)
- %%NoSource -> NoSource

* Fri Mar 31 2006 Yohsuke Ooi <meke@momonga-linux.org>
-  (0.1.6-1m)
- initial commit

