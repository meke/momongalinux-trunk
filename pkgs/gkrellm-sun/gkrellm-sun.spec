%global momorel 6
%global gkplugindir %{_libdir}/gkrellm2/plugins

Summary: Sun clock plugin for GKrellM
Name: gkrellm-sun
Version: 1.0.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://gkrellsun.sourceforge.net/
Source: http://downloads.sf.net/gkrellsun/gkrellsun-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gkrellm >= 2.2.0
BuildRequires: gkrellm-devel >= 2.2.0
BuildRequires: gkrellm >= 2.2.0

%description
A sun clock plugin for GKrellM which can display the sun's setting time, rising
time, path and current location and so on.


%prep
%setup -q -n gkrellsun-%{version}


%build
%{__make} FLAGS='%{optflags} -fPIC $(GTK_INCLUDE)'


%install
%{__rm} -rf %{buildroot}
%{__install} -D -m 0755 src20/gkrellsun.so \
    %{buildroot}%{gkplugindir}/gkrellsun.so


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{gkplugindir}/gkrellsun.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-2m)
- rebuild against rpm-4.6

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.0-6
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 Matthias Saou <http://freshrpms.net/> 1.0.0-5
- Rebuild for new BuildID feature.

* Sun Aug  5 2007 Matthias Saou <http://freshrpms.net/> 1.0.0-4
- Update License field... GPLv2 only, will be a problem with GPLv3 gkrellm.

* Fri Jun 22 2007 Matthias Saou <http://freshrpms.net/> 1.0.0-3
- Remove dist, as it might be a while before the next rebuild.
- Switch to using downloads.sf.net source URL.

* Wed Feb 14 2007 Matthias Saou <http://freshrpms.net/> 1.0.0-2
- Tweak defattr.

* Mon Feb 12 2007 Matthias Saou <http://freshrpms.net/> 1.0.0-1
- Initial RPM release as a single plugin.

