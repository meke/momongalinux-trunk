%global momorel 21

Name:           docbook2X
Version:        0.8.8
Release: %{momorel}m%{?dist}
Summary:        Convert docbook into man and Texinfo

Group:          Applications/Text
License:        BSD
URL:            http://docbook2x.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/docbook2x/docbook2X-%{version}.tar.gz 
NoSource: 0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	perl >= 5.16.0
BuildRequires:  libxslt openjade
# required by the perl -c calls during build
BuildRequires:  perl-XML-SAX
# rpmlint isn't happy with libxslt, but we need xsltproc
Requires:       libxslt openjade texinfo
Requires:  perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%description
docbook2X converts DocBook documents into man pages and Texinfo
documents.


%prep
%setup -q

%build
# to avoid clashing with docbook2* from docbook-utils
%configure --program-transform-name='s/docbook2/db2x_docbook2/'
make %{?_smp_mflags}
rm -rf __dist_html
mkdir -p __dist_html/html
cp -p doc/*.html __dist_html/html

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -c -p'
rm -rf $RPM_BUILD_ROOT/%{_datadir}/doc/
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
   /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root,-)
%doc COPYING README THANKS AUTHORS __dist_html/html/
%{_bindir}/db2x_manxml
%{_bindir}/db2x_texixml
%{_bindir}/db2x_xsltproc
%{_bindir}/db2x_docbook2man
%{_bindir}/db2x_docbook2texi
%{_bindir}/sgml2xml-isoent
%{_bindir}/utf8trans
%dir %{_datadir}/docbook2X
%{_datadir}/docbook2X/VERSION
%dir %{_datadir}/docbook2X/charmaps
%dir %{_datadir}/docbook2X/dtd
%dir %{_datadir}/docbook2X/xslt
%{_datadir}/docbook2X/charmaps/*
%{_datadir}/docbook2X/dtd/*
%{_datadir}/docbook2X/xslt/*
%{_mandir}/man1/*.1*
%{_infodir}/docbook2*


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.8-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-6m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-2m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.8-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.8-1m)
- update 0.8.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-3m)
- %%NoSource -> NoSource

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-2m)
- rebuild against perl-5.10.0

* Fri Apr 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-1m)
- import to Momonga from Fedora Extras devel

* Mon Sep 11 2006 Patrice Dumas <pertusus@free.fr> 0.8.7-2
- correct the perl-XML-SAX to be perl(XML::SAX::ParserFactory)

* Thu May 18 2006 Patrice Dumas <pertusus@free.fr> - 0.8.7-1
- update to 0.8.7

* Fri Feb 17 2006 Patrice Dumas <pertusus@free.fr> - 0.8.6-1
- update to 0.8.6
- drop patch as SGMLSpl.pm is included in the scripts, not distributed
- BR perl-XML-SAX (close 188481)

* Fri Feb 17 2006 Patrice Dumas <pertusus@free.fr> - 0.8.5-2
- rebuild for fc5

* Fri Feb  3 2006 Patrice Dumas <pertusus@free.fr> - 0.8.5-1
- FE submission
