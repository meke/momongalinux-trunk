%global momorel 5

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define gcj_support 0
# tests need lots of: cpu, mem, time
%define tests 0

Name:           asm2
Version:        2.2.3
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        A code manipulation tool to implement adaptable systems
License:        BSD
URL:            http://asm.objectweb.org/
Group:          Development/Libraries
Source0:        http://download.forge.objectweb.org/asm/asm-2.2.3.tar.gz
Source1:        http://asm.objectweb.org/current/asm-eng.pdf
Source2:        http://asm.objectweb.org/current/asm-transformations.pdf
Source3:        http://download.forge.objectweb.org/asm/asm-guide.pdf
Source4:        http://asm.objectweb.org/doc/faq.html
Source5:        asm-%{version}.pom
Source6:        asm-all-%{version}.pom
Source7:        asm-analysis-%{version}.pom
Source8:        asm-attrs-%{version}.pom
Source9:        asm-commons-%{version}.pom
Source10:       asm-parent-%{version}.pom
Source11:       asm-tree-%{version}.pom
Source12:       asm-util-%{version}.pom
Source13:       asm-xml-%{version}.pom

Patch0:         asm2-build_xml.patch
Patch1:         asm2-SerialVersionUIDAdder.patch
Patch2:         asm2-test-build_xml.patch
Patch3:         asm2-ALLPerfTest.patch
Patch4:         asm2-test-heap.patch
# Patch out the Class-path in MANIFEST.MF
Patch5:         %{name}-noclasspathinmanifest.patch

BuildRequires:  ant
%if %{tests}
BuildRequires:  ant-junit
BuildRequires:  asm2
BuildRequires:  bcel
BuildRequires:  ccl-util
BuildRequires:  cobertura
BuildRequires:  jakarta-oro
BuildRequires:  janino
BuildRequires:  javancss
BuildRequires:  javassist
BuildRequires:  log4j
%endif
BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  objectweb-anttask
%if ! %{gcj_support}
BuildArch:      noarch
%endif
BuildRoot:       %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       jpackage-utils >= 0:1.7.2
%if %{gcj_support}
BuildRequires:    java-gcj-compat-devel
Requires(post):   java-gcj-compat
Requires(postun): java-gcj-compat
%endif

%description
ASM is a Java bytecode manipulation framework. It can be 
used to dynamically generate stub classes or other proxy 
classes, directly in binary form, or to dynamically modify 
classes at load time, i.e., just before they are loaded into
the Java Virtual Machine.
ASM offers similar functionalities as BCEL or SERP, but is 
much smaller (33KB instead of 350KB for BCEL and 150KB for 
SERP) and faster than these tools (the overhead of a load 
time class transformation is of the order of 60% with ASM, 
700% or more with BCEL, and 1100% or more with SERP). Indeed 
ASM was designed to be used in a dynamic way* and was 
therefore designed and implemented to be as small and 
as fast as possible.
(* ASM can of course be used in a static way too.)


%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description    javadoc
Javadoc for %{name}.

%package        manual
Summary:        Documents for %{name}
Group:          Documentation
Requires:       %{name} = %{version}

%description    manual
%{summary}.

%package        demo
Summary:        Examples for %{name}
Group:          Documentation
Requires:       %{name} = %{version}

%description    demo
%{summary}.

%prep
%setup -q -n asm-%{version}
%patch5
find . -name "*.jar" -exec rm -f {} \;
mkdir test/lib
%if %{tests}
pushd test/lib
ln -sf $(build-classpath asm2/asm2)
ln -sf $(build-classpath cobertura)
ln -sf $(build-classpath log4j)
ln -sf $(build-classpath jakarta-oro)
ln -sf $(build-classpath ccl-util) ccl.jar
ln -sf $(build-classpath javancss)
popd
%endif

%patch0 -b .sav
%patch1 -b .sav
%patch2 -b .sav
%patch3 -b .sav
%patch4 -b .sav

rm test/perf/org/objectweb/asm/SERPPerfTest.java
rm test/conform/adviceadapter2.xml

%build
%if %{tests}
export CLASSPATH=$(build-classpath asm2/asm2 asm2/asm2-tree)
%endif
ant \
  -Dbcel.path=$(build-classpath bcel) \
  -Djanino.path=$(build-classpath janino) \
  -Djavassist.path=$(build-classpath javassist) \
  -Dobjectweb.ant.tasks.path=$(build-classpath objectweb-anttask) \
  jar \
  jdoc \
  examples \
%if %{tests}
  coverage \
  test \
  coverage.report \
  test.report \
%endif

# compile
# compile-debug
# coverage
# coverage.report
# dist
# dist.init
# dist.version
# example
# examples
# jar
# jdoc
# noshrink
# properties
# shrink
# test
# test.report

# fix encoding
sed -i 's/\r//g' README.txt LICENSE.txt

%install
rm -rf $RPM_BUILD_ROOT

# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/%{name}

for jar in output/dist/lib/*.jar; do
newjar=${jar/asm-/asm2-}
install -m 644 ${jar} \
    $RPM_BUILD_ROOT%{_javadir}/%{name}/`basename ${newjar}`
done
install -m 644 output/dist/lib/all/asm-all-%{version}.jar \
    $RPM_BUILD_ROOT%{_javadir}/%{name}-all-%{version}.jar

(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do \
ln -sf ${jar} ${jar/-%{version}/}; done)
(cd $RPM_BUILD_ROOT%{_javadir}/%{name} && for jar in *-%{version}*; do \
ln -sf ${jar} ${jar/-%{version}/}; done)

%add_to_maven_depmap asm asm-parent %{version} JPP asm2-parent
%add_to_maven_depmap asm asm-all %{version} JPP asm2-all
%add_to_maven_depmap asm asm-analysis %{version} JPP/asm2 asm2-analysis
%add_to_maven_depmap asm asm-attrs %{version} JPP/asm2 asm2-attrs
%add_to_maven_depmap asm asm-commons %{version} JPP/asm2 asm2-commons
%add_to_maven_depmap asm asm-tree %{version} JPP/asm2 asm2-tree
%add_to_maven_depmap asm asm-util %{version} JPP/asm2 asm2-util
%add_to_maven_depmap asm asm-xml %{version} JPP/asm2 asm2-xml
%add_to_maven_depmap asm asm %{version} JPP/asm2 asm2

# pom
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{name}-%{name}.pom
install -m 644 %{SOURCE6} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP-%{name}-all.pom
install -m 644 %{SOURCE7} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{name}-%{name}-analysis.pom
install -m 644 %{SOURCE8} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{name}-%{name}-attrs.pom
install -m 644 %{SOURCE9} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{name}-%{name}-commons.pom
install -m 644 %{SOURCE10} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP-%{name}-parent.pom
install -m 644 %{SOURCE11} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{name}-%{name}-tree.pom
install -m 644 %{SOURCE12} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{name}-%{name}-util.pom
install -m 644 %{SOURCE13} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{name}-%{name}-xml.pom

# javadoc
install -p -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr output/dist/doc/javadoc/user/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
(cd $RPM_BUILD_ROOT%{_javadocdir} && ln -sf %{name}-%{version} %{name})
# manual
install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 README.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 LICENSE.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}

# demo
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}-%{version}
cp -pr output/dist/examples $RPM_BUILD_ROOT%{_datadir}/%{name}-%{version}

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%update_maven_depmap
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%dir %{_docdir}/%{name}-%{version}
%doc %{_docdir}/%{name}-%{version}/README.txt
%doc %{_docdir}/%{name}-%{version}/LICENSE.txt
%doc %{_docdir}/%{name}-%{version}/asm-eng.pdf
%dir %{_datadir}/%{name}-%{version}
%dir %{_javadir}/%{name}
%{_javadir}/%{name}/*.jar
%{_javadir}/*.jar
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/%{name}
%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif


%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/*

%files manual
%defattr(0644,root,root,0755)
%doc %{_docdir}/%{name}-%{version}/faq.html
%doc %{_docdir}/%{name}-%{version}/asm-guide.pdf
%doc %{_docdir}/%{name}-%{version}/asm-transformations.pdf

%files demo
%defattr(0644,root,root,0755)
%{_datadir}/%{name}-%{version}/examples

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.3-3m)
- full rebuild for mo7 release

* Fri Feb  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.3-2m)
- release %%{_mavendepmapfragdir}
- it's already provided by jpackage-utils and asm2 Requires: jpackage-utils
- revise %%files to avoid conflicting

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.3-1m)
- import from Rawhide for netbeans-6.7.1

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:2.2.3-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Mar 31 2009 Michael Schwendt <mschwendt@fedoraproject.org> - 0:2.2.3-5
- Fix unowned directories (#473622)

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:2.2.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jul  9 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0:2.2.3-3
- drop repotag

* Tue Jan 22 2008 Permaine Cheung <pcheung@redhat.com> - 0:2.2.3-2jpp.1
- Merge with upstream

* Tue Dec 18 2007 Ralph Apel <r.apel at r-apel.de> 0:2.2.3-2jpp
- Fix parent pom filename

* Fri Oct 12 2007 Ralph Apel <r.apel at r-apel.de> 0:2.2.3-1jpp
- Upgrade to 2.2.3
- Add asm2-all jar file
- Add manual subpackage
- Optionally run tests (need lots of time, cpu, mem)
- Add maven2 poms and depmap frags
- Add gcj_support option
- Make Vendor, Distribution based on macro

* Fri Sep 21 2007 Permaine Cheung <pcheung@redhat.com> - 0:2.1-2jpp.2
- Fix Source0 URL, License tag

* Tue Feb 13 2007 Andrew Overholt <overholt@redhat.com> 0:2.1-2jpp.1
- Fedora-ize Release
- Remove faq.html and add LICENSE.TXT to %%doc
- Fix BuildRoot
- Change Group
- Un-tabify
- Remove Vendor and Distribution tags
- Remove %%define section free
- Clean up javadoc section:  no more %%ghost or %%post{,un}
- Add patch to patch-out Class-path in MANIFEST.MF

* Thu Jan 05 2006 Fernando Nasser <fnasser@redhat.com> - 0:2.1-1jpp
- First JPP 1.7 build

* Thu Oct 06 2005 Ralph Apel <r.apel at r-apel.de> 0:2.1-1jpp
- Upgrade to 2.1

* Fri Mar 11 2005 Sebastiano Vigna <vigna at acm.org> 0:2.0.RC1-1jpp
- First release of the 2.0 line.
