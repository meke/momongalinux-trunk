%global         momorel 1

Name:           perl-Text-CSV_XS
Version:        1.09
Release:        %{momorel}m%{?dist}
Summary:        Comma-separated values manipulation routines
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Text-CSV_XS/
Source0:        http://www.cpan.org/authors/id/H/HM/HMBRAND/Text-CSV_XS-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.005
BuildRequires:  perl-DynaLoader
BuildRequires:  perl-Encode >= 2.57
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO
BuildRequires:  perl-Test-Harness
BuildRequires:  perl-Test-Simple
Requires:       perl-DynaLoader
Requires:       perl-Encode >= 2.57
Requires:       perl-IO
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Text::CSV_XS provides facilities for the composition and decomposition of
comma-separated values. An instance of the Text::CSV_XS class can combine
fields into a CSV string and parse a CSV string into fields.

%prep
%setup -q -n Text-CSV_XS-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{perl_vendorarch}/auto/Text/CSV_XS/*
%{perl_vendorarch}/Text/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- rebuild against perl-5.20.0
- update to 1.09

* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04
- rebuild against perl-5.18.2

* Mon Nov 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-2m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Sat Jun 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Tue Jun 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-2m)
- rebuild against perl-5.18.0

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-2m)
- rebuild against perl-5.16.3

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Wed Dec  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Wed Nov 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Wed Nov 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-2m)
- rebuild against perl-5.16.2

* Wed Aug 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90
- rebuild against perl-5.16.0

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.87-1m)
- update to 0.87

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-2m)
- rebuild against perl-5.14.2

* Thu Sep  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-1m)
- update to 0.85

* Mon Aug  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.83-1m)
- update to 0.83

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-2m)
- rebuild against perl-5.14.1

* Sun May  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.81-2m)
- rebuild for new GCC 4.6

* Wed Mar 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-1m)
- update to 0.81

* Sat Dec 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Sun Nov 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-1m)
- update to 0.79

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.76-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-1m)
- update to 0.76

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.75-1m)
- update to 0.75

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-1m)
- update to 0.74

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-4m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.73-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-2m)
- rebuild against perl-5.12.1

* Tue May  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-1m)
- update to 0.73

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.69-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.69-1m)
- update to 0.69

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.65-1m)
- update to 0.65

* Sun Apr  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-1m)
- update to 0.64

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- update to 0.63

* Sat Mar 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Tue Mar 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.58-2m)
- rebuild against rpm-4.6

* Sun Nov  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Wed Oct 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Thu Oct 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Wed Sep  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Thu Apr 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.37-2m)
- rebuild against gcc43

* Wed Mar 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Fri Mar  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Thu Oct 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Sun Sep  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Tue Jun  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.23-3m)
- use vendor

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23-2m)
- fix duplicate files

* Wed Dec 13 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.23-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
