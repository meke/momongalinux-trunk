%global momorel 10
%global with_cli 1
%global qtver 4.8.2

Summary: A tool for submitting .scrobbler.log from portable players to Last.fm
Name: qtscrob
Version: 0.10
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://qtscrob.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-optflags.patch
Patch2: %{name}-%{version}-free.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: libcurl-devel
BuildRequires: libmtp-devel >= 1.1.0
BuildRequires: pkgconfig

%description
QTScrobbler is tool for submitting .scrobbler.log from portable players to Last.fm.
It is written in c++ using qt4 library by trolltech.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja~
%patch1 -p1 -b .optflags
%patch2 -p1 -b .free

%build
export prefix=%{_prefix}

pushd src/qt
qmake-qt4
make %{?_smp_mflags}
popd

%if %{with_cli}
pushd src/cli
make %{?_smp_mflags} OPTFLAGS="%{optflags}"
popd
%endif

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
pushd src/qt
make install INSTALL_ROOT=%{buildroot}
popd

%if %{with_cli}
pushd src/cli
make install DESTDIR=%{buildroot} prefix=%{_prefix}
popd
%endif

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# modify desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Network \
  --add-category Player \
  --add-category AudioVideo \
  --add-category Audio \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS CHANGELOG COPYING README
%{_bindir}/%{name}
%if %{with_cli}
%{_bindir}/scrobble-cli
%endif
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%if %{with_cli}
%{_mandir}/man1/scrobble-cli.1*
%endif
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-10m)
- fix build failre

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10-9m)
- rebuild for libmtp-1.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-7m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-5m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-4m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-3m)
- touch up spec file

* Tue Feb 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-2m)
- build cli with optflags

* Mon Feb 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-1m)
- initial package for music freaks using Momonga Linux
