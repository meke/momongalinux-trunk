%global momorel 1

Summary: The libevent API provides a mechanism to execute a callback function
name: libevent
Version: 2.0.21
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
Source0: https://github.com/downloads/%{name}/%{name}/%{name}-%{version}-stable.tar.gz
NoSource: 0
Patch00: libevent-2.0.11-stable-configure.patch

Url: http://www.monkey.org/~provos/libevent/

License: BSD
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The libevent API provides a mechanism to execute a callback function
when a specific event occurs on a file descriptor or after a timeout
has been reached. Furthermore, libevent also support callbacks due to
signals or regular timeouts.libevent is meant to replace the event
loop found in event driven network servers. An application just needs
to call event_dispatch() and then add or remove events dynamically
without having to change the event loop.

%package devel
Summary: Header files and libraries for developing apps which will use libevent.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The libevent-devel package contains the header files and libraries needed
to develop programs that use the libevent library.

Install the libevent-devel package if you want to develop applications that
will use the libevent library.

%prep
%setup -q -n %{name}-%{version}-stable
%patch00 -p1

%build
%configure --disable-dependency-tracking --disable-static

make %{?_smp_mflags} all

# Create the docs
make doxygen

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

mkdir -p %{buildroot}%{_docdir}/%{name}-devel-%{version}/html
(cd doxygen/html; \
	install *.* %{buildroot}%{_docdir}/%{name}-devel-%{version}/html)

mkdir -p %{buildroot}%{_docdir}/%{name}-devel-%{version}/latex
(cd doxygen/latex; \
	install *.* %{buildroot}%{_docdir}/%{name}-devel-%{version}/latex)

mkdir -p %{buildroot}%{_docdir}/%{name}-devel-%{version}/sample
(cd sample; \
	install *.c Makefile* %{buildroot}%{_docdir}/%{name}-devel-%{version}/sample)

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README
%{_libdir}/libevent*.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/event_rpcgen.py
%{_libdir}/*.so
%{_libdir}/pkgconfig/libevent*.pc
%{_includedir}/*
%{_docdir}/%{name}-devel-%{version}/html/*
%{_docdir}/%{name}-devel-%{version}/latex/*
%{_docdir}/%{name}-devel-%{version}/sample/*

%changelog
* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.21-1m)
- update to 2.0.21

* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.19-1m)
- update to 2.0.19

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-1m)
- update to 2.0.18

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.17-1m)
- update to 2.0.17

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.16-1m)
- update to 2.0.16

* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.13-1m)
- update to 2.0.13

* Sun Jun 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.12-1m)
- update to 2.0.12

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.11-1m)
- update to 2.0.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.10-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.10-1m)
- update to 2.0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.14-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.14-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.14-1m)
- update to 1.4.14b

* Wed Dec 30 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.13-2m)
- remove aclocal

* Sun Dec 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-1m)
- update to 1.4.13

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.12-1m)
- update to 1.4.12

* Mon Jun  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.11-1m)
- update to 1.4.11

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.10-1m)
- update to 1.4.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.9-2m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.9-1m)
- update to 1.4.9

* Wed Oct 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.8-1m)
- update to 1.4.8

* Wed Jul  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Sat May 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-0.1.2m)
- rebuild against gcc43

* Sat Jan  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-0.1.1m)
- update to 1.4.1-beta

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3e-1m)
- update to 1.3e

* Fri Sep 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3d-1m)
- update to 1.3d
- do not use %%NoSource macro

* Thu Aug 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3c-1m)
- update to 1.3c

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3b-1m)
- update to 1.3b

* Wed Feb 21 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.3a-1m)
- up to 1.3a

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1a-3m)
- delete libtool library

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1a-2m)
- revise %%files to avoid conflicting

* Wed Feb 2 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.1a-1m)
- Initial version
