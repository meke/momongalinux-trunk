%global momorel 4

%global spectemplatedir %{_sysconfdir}/rpmdevtools/
%global ftcgtemplatedir %{_datadir}/fontconfig/templates/
%global rpmmacrodir     %{_sysconfdir}/rpm/

Name:    fontpackages
Version: 1.44
Release: %{momorel}m%{?dist}
Summary: Common directory and macro definitions used by font packages

Group:     Development/System
# Mostly means the scriptlets inserted via this package do not change the
# license of the packages they're inserted in
License:   LGPLv3+
URL:       http://fedoraproject.org/wiki/fontpackages/
Source0:   http://fedorahosted.org/releases/f/o/%{name}/%{name}-%{version}.tar.xz
NoSource:  0
Patch0:    fontpackages-1.22-dup-dir.patch

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
This package contains the basic directory layout, spec templates, rpm macros
and other materials used to create font packages.


%package filesystem
Summary: Directories used by font packages
License: Public Domain

%description filesystem
This package contains the basic directory layout used by font packages,
including the correct permissions for the directories.


%package devel
Summary: Templates and macros used to create font packages

Requires: rpmdevtools, %{name}-filesystem = %{version}-%{release}
Requires: fontconfig

%description devel
This package contains spec templates, rpm macros and other materials used to
create font packages.

%package tools
Summary: Tools used to check fonts and font packages

Requires: fontconfig, fontforge
Requires: curl, make
Requires: rpmlint, yum-utils

%description tools
This package contains tools used to check fonts and font packages


%prep
%setup -q
%patch0 -p1


%build

for file in bin/repo-font-audit bin/compare-repo-font-audit ; do
sed -i "s|^DATADIR\([[:space:]]*\)\?=\(.*\)$|DATADIR=%{_datadir}/%{name}|g" \
  $file
done

%install
rm -fr %{buildroot}

# Pull macros out of macros.fonts and emulate them during install
for dir in fontbasedir        fontconfig_masterdir \
           fontconfig_confdir fontconfig_templatedir ; do
  export _${dir}=$(rpm --eval $(%{__grep} -E "^%_${dir}\b" \
    rpm/macros.fonts | %{__awk} '{ print $2 }'))
done

install -m 0755 -d %{buildroot}${_fontbasedir} \
                   %{buildroot}${_fontconfig_masterdir} \
                   %{buildroot}${_fontconfig_confdir} \
                   %{buildroot}${_fontconfig_templatedir} \
                   %{buildroot}%{spectemplatedir} \
                   %{buildroot}%{rpmmacrodir} \
                   %{buildroot}%{_datadir}/fontconfig/templates \
                   %{buildroot}%{_datadir}/%{name} \
                   %{buildroot}%{_bindir}
install -m 0644 -p spec-templates/*.spec       %{buildroot}%{spectemplatedir}
install -m 0644 -p fontconfig-templates/*      %{buildroot}%{ftcgtemplatedir}
install -m 0644 -p rpm/macros.fonts            %{buildroot}%{rpmmacrodir}
install -m 0644 -p private/repo-font-audit.mk  %{buildroot}/%{_datadir}/%{name}
install -m 0755 -p private/core-fonts-report \
                   private/font-links-report \
                   private/fonts-report \
                   private/process-fc-query \
                   private/test-info           %{buildroot}/%{_datadir}/%{name}
install -m 0755 -p bin/*                       %{buildroot}%{_bindir}

cat <<EOF > %{name}-%{version}.files
%defattr(0644,root,root,0755)
%dir ${_fontbasedir}
%dir ${_fontconfig_masterdir}
%dir ${_fontconfig_confdir}
%dir ${_fontconfig_templatedir}
EOF

%clean
rm -fr %{buildroot}


%files filesystem -f %{name}-%{version}.files
%defattr(0644,root,root,0755)
%dir %{_datadir}/fontconfig

%files devel
%defattr(0644,root,root,0755)
%doc license.txt readme.txt
%config(noreplace) %{spectemplatedir}/*.spec
%config(noreplace) %{rpmmacrodir}/macros.fonts
%dir %{ftcgtemplatedir}
%{ftcgtemplatedir}/*conf
%{ftcgtemplatedir}/*txt

%files tools
%defattr(0644,root,root,0755)
%doc license.txt readme.txt
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/repo-font-audit.mk
%defattr(0755,root,root,0755)
%{_datadir}/%{name}/core-fonts-report
%{_datadir}/%{name}/font-links-report
%{_datadir}/%{name}/fonts-report
%{_datadir}/%{name}/process-fc-query
%{_datadir}/%{name}/test-info
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.44-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.44-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.44-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.44-1m)
- update to 1.44

* Tue Jun  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Tue Dec  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-2m)
- refine macros.fonts

* Sun Nov 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-1m)
- update to 1.33
-- but tools does not require mutt and fedora-packager

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-2m)
- be quiet at parging spec file

* Sat Aug 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-1m)
- update to 1.22
-- * Sat Jun 20 2009 Nicolas Mailhot <nim at fedoraproject dot org>
-- - 1.22-1
-- - workaround rpm eating end-of-line after %%_font_pkg calls
-- - add script to audit font sanity of yum repositories
-- 
-- * Tue Jun 2 2009 Nicolas Mailhot <nim at fedoraproject dot org>
-- - 1.21-1
-- - try to handle more corner naming cases in lua macro - expect some fallout
--   if your spec uses weird naming

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20-2m)
- revise /etc/rpm/macros.fonts (Patch0)

* Sat Apr 25 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.20-1m)
- update 1.20

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.16-1m)
- import from Fedora-devel for OOo3

* Wed Jan 22 2009 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.16-1

* Thu Jan 15 2009 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.15-1
- lua-ize the main macro

* Wed Jan 14 2009 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.14-1
- Update for subpackage naming changes requested by FPC

* Mon Dec 22 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.13-1
- Add another directory to avoid depending on unowned stuff
- use it to put the fontconfig examples in a better place

* Sun Dec 21 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.12-2
- Change homepage

* Fri Dec 19 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.12-1
- Add another macro to allow building fontconfig without cycling

* Wed Dec 10 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.11-1
- Add actual fedorahosted references

* Sun Nov 23 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.10-1
- renamed to "fontpackages"

* Fri Nov 14 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.9-1
- fix and complete fontconfig doc
- 1.8-1
- simplify multi spec template: codify general case
- 1.7-1
- split fontconfig template documentation is separate files
- 1.6-1
- simplify spec templates
- 1.5-1
- use ".conf" extension for fontconfig templates
- 1.4-1
- small multi spec template fix

* Wed Nov 12 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.3-1
- remove trailing slashes in directory macros

* Tue Nov 11 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.2-1
- add fontconfig templates
- fix a few typos

* Mon Nov 10 2008 Nicolas Mailhot <nim at fedoraproject dot org>
- 1.0-1
- initial release
