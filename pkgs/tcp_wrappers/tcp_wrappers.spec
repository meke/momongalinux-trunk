%global momorel 35
%global LIB_MAJOR 0
%global LIB_MINOR 7
%global LIB_REL 6

Summary: A security tool which acts as a wrapper for TCP daemons
Name: tcp_wrappers
Version: 7.6
Release: %{momorel}m%{?dist}
License: see "DISCLAIMER" and see "strcasecmp.c"
Group: System Environment/Daemons
Source0: ftp://ftp.porcupine.org/pub/security/%{name}_%{version}.tar.gz
NoSource: 0
Patch0: tcpw7.2-config.patch
Patch1: tcpw7.2-setenv.patch
Patch2: tcpw7.6-netgroup.patch
Patch3: tcp_wrappers-7.6-bug11881.patch
Patch4: tcp_wrappers-7.6-bug17795.patch
Patch5: tcp_wrappers-7.6-bug17847.patch
Patch6: tcp_wrappers-7.6-fixgethostbyname.patch
Patch7: tcp_wrappers-7.6-docu.patch
Patch9: tcp_wrappers.usagi-ipv6.patch
Patch10: tcp_wrappers.ume-ipv6.patch
Patch11: tcp_wrappers-7.6-shared.patch
Patch12: tcp_wrappers-7.6-sig.patch
Patch13: tcp_wrappers-7.6-strerror.patch
Patch14: tcp_wrappers-7.6-ldflags.patch
Patch15: tcp_wrappers-7.6-fix_sig-bug141110.patch
Patch16: tcp_wrappers-7.6-162412.patch
Patch17: tcp_wrappers-7.6-220015.patch
Patch18: tcp_wrappers-7.6-restore_sigalarm.patch
Patch19: tcp_wrappers-7.6-siglongjmp.patch
Patch20: tcp_wrappers-7.6-sigchld.patch
Patch21: tcp_wrappers-7.6-196326.patch
Patch22: tcp_wrappers_7.6-249430.patch
# required by sin_scope_id in ipv6 patch
BuildRequires: glibc-devel >= 2.2		
Requires: tcp_wrappers-libs = %{version}-%{release}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The tcp_wrappers package provides small daemon programs which can
monitor and filter incoming requests for systat, finger, FTP, telnet,
rlogin, rsh, exec, tftp, talk and other network services.

Install the tcp_wrappers program if you need a security tool for
filtering incoming network services requests.

This version also supports IPv6.

%package libs
Summary: tcp_wrappers libraries
Group: System Environment/Libraries

%description libs
tcp_wrappers-libs contains the libraries of the tcp_wrappers package.

%package devel
Summary: tcp_wrappers development libraries and headers
Group: Development/Libraries
Requires: tcp_wrappers-libs = %{version}-%{release}

%description devel
tcp_wrappers-devel contains the libraries and header files needed to
develop applications with tcp_wrappers support.

%prep
%setup -q -n %{name}_%{version}
%patch0 -p1 -b .config
%patch1 -p1 -b .setenv
%patch2 -p1 -b .netgroup
%patch3 -p1 -b .bug11881
%patch4 -p1 -b .bug17795
%patch5 -p1 -b .bug17847
%patch6 -p1 -b .fixgethostbyname
%patch7 -p1 -b .docu
%patch9 -p1 -b .usagi-ipv6
%patch10 -p1 -b .ume-ipv6
%patch11 -p1 -b .shared
%patch12 -p1 -b .sig
%patch13 -p1 -b .strerror
%patch14 -p1 -b .cflags
%patch15 -p1 -b .fix_sig
%patch16 -p1 -b .162412
%patch17 -p1 -b .220015
%patch18 -p1 -b .restore_sigalarm
%patch19 -p1 -b .siglongjmp
%patch20 -p1 -b .sigchld
%patch21 -p1 -b .196326
%patch22 -p1 -b .249430

%build
make RPM_OPT_FLAGS="$RPM_OPT_FLAGS -fPIC -DPIC -D_REENTRANT -DHAVE_STRERROR" LDFLAGS="-pie" MAJOR=%{LIB_MAJOR} MINOR=%{LIB_MINOR} REL=%{LIB_REL} linux

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_includedir}
mkdir -p %{buildroot}/%{_lib}
mkdir -p %{buildroot}%{_mandir}/man{3,5,8}
mkdir -p %{buildroot}%{_sbindir}

install -p -m644 hosts_access.3 %{buildroot}%{_mandir}/man3
install -p -m644 hosts_access.5 hosts_options.5 %{buildroot}%{_mandir}/man5
install -p -m644 tcpd.8 tcpdchk.8 tcpdmatch.8 %{buildroot}%{_mandir}/man8
ln -sf hosts_access.5 %{buildroot}%{_mandir}/man5/hosts.allow.5
ln -sf hosts_access.5 %{buildroot}%{_mandir}/man5/hosts.deny.5
#cp -a libwrap.a %{buildroot}%{_libdir}
cp -a libwrap.so* %{buildroot}/%{_lib}
install -p -m644 tcpd.h %{buildroot}%{_includedir}
install -m755 safe_finger %{buildroot}%{_sbindir}
install -m755 tcpd %{buildroot}%{_sbindir}
install -m755 try-from %{buildroot}%{_sbindir}

# XXX remove utilities that expect /etc/inetd.conf (#16059).
#install -m755 tcpdchk %{buildroot}/usr/sbin
#install -m755 tcpdmatch %{buildroot}/usr/sbin
rm -f %{buildroot}%{_mandir}/man8/tcpdmatch.*
rm -f %{buildroot}%{_mandir}/man8/tcpdchk.*

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BLURB CHANGES README* DISCLAIMER Banners.Makefile strcasecmp.c
%{_sbindir}/*
%{_mandir}/man8/*

%files libs
%defattr(-,root,root,-)
/%{_lib}/*.so.*
%{_mandir}/man5/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
#%%{_libdir}/*.a
/%{_lib}/*.so
%{_mandir}/man3/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-35m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-34m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.6-33m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.6-32m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.6-31m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.6-30m)
- sycn with Rawhide (7.6-53)
-- move libwrap.so* from %%{_libdir} to /%%{_lib}
-- do not install static library libwrap.a

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.6-29m)
- rebuild against gcc43

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.6-28m)
- remove Obsoletes: tcp_wrappers

* Fri Mar  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.6-27m)
- merge fc-devel
- delete Patch8: tcp_wrappers-7.6-casesens.patch
- comment out Patch50: tcpw-barterly-yp.patch
- add Patch14: tcp_wrappers-7.6-ldflags.patch
- add Patch15: tcp_wrappers-7.6-fix_sig-bug141110.patch
- add Patch16: tcp_wrappers-7.6-162412.patch

* Mon Mar 05 2007 Tomas Janousek <tjanouse@redhat.com> - 7.6-42
- added Obsoletes field so that the upgrade goes cleanly
- added dist tag

* Mon Dec  4 2006 Thomas Woerner <twoerner@redhat.com> 7.6-41
- moved devel libraries, headers and man pages into devel sub package (#193188)
- new libs sub package for libraries
- using BuildRequires instead of BuildPreReq

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 7.6-40.2.1
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 7.6-40.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 7.6-40.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 24 2006 Thomas Woerner <twoerner@redhat.com> 7.6-40
- fixed uninitialized fp in function inet_cfg (#162412)

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri May  6 2005 Thomas Woerner <twoerner@redhat.com> 7.6-39
- fixed sig patch (#141110). Thanks to Nikita Shulga for the patch

* Wed Feb  9 2005 Thomas Woerner <twoerner@redhat.com> 7.6-38
- rebuild

* Thu Oct  7 2004 Thomas Woerner <twoerner@redhat.com> 7.6-37.2
- new URL and spec file cleanup, patch from Robert Scheck

* Mon Oct  4 2004 Thomas Woerner <twoerner@redhat.com> 7.6-37.1
- rebuilt

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Mar  5 2004 Thomas Woerner <twoerner@redhat.com> 7.6-36
- pied tcpd

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Oct 29 2003 zunda <zunda at freeshell.org>
- (7.6-26m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft).

* Tue May  6 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (7.6-25m)
- sync with redhat(tcp_wrappers-7.6-34)
- import patches
	Patch7: tcp_wrappers-7.6-docu.patch
	Patch8: tcp_wrappers-7.6-casesens.patch
	Patch9: tcp_wrappers.usagi-ipv6.patch
	Patch10: tcp_wrappers.ume-ipv6.patch
	Patch11: tcp_wrappers-7.6-shared.patch
	Patch12: tcp_wrappers-7.6-sig.patch
	Patch13: tcp_wrappers-7.6-strerror.patch
- remove utilities that expect /etc/inetd.conf (tcpdchk,tcpdmatch)
- install shared library
- always apply ipv6 patch
- revice tcpw-barterly-yp.patch

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (7.6-24k)
- updated Source0 URL.

* Mon Mar 21 2001 Motonobu Ichimura <famao@kondara.org>
- (7.6-22k)
- replace ipv6 patch with usagi's one

* Mon Mar 21 2001 Motonobu Ichimura <famao@kondara.org>
- (7.6-20k)
- applied bug-fix patches from rawhide (7.6-19)

* Tue Mar  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (7.6-18k)
- errased IPv6 functions with %{_ipv6} macro

* Mon Jun 26 2000 Masaaki Noro <noro@flab.fujitsu.co.jp>
- fix spec file.(add url of kame patch)

* Thu Jun 01 2000 KUSUNOKI Masanori <nori@kondara.org>
- Enable IPv6.

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (7.6-10).

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
-- update man-pages-ja-tcp_wrappers-20000115

* Tue Nov 30 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-tcp_wrappers-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Oct 15 1999 Toru Hoshina <t@kondara.org>
- add -DUSE_GETDOMAIN.

* Mon Aug 23 1999 Jeff Johnson <jbj@redhat.com>
- add netgroup support (#3940).

* Wed May 26 1999 Jeff Johnson <jbj@redhat.com>
- compile on sparc with -fPIC.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 7)

* Wed Dec 30 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Sat Aug 22 1998 Jeff Johnson <jbj@redhat.com>
- close setenv bug (problem #690)
- spec file cleanup

* Thu Jun 25 1998 Alan Cox <alan@redhat.com>
- Erp where did the Dec 05 patch escape to

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Dec 05 1997 Erik Troan <ewt@redhat.com>
- don't build setenv.o module -- it just breaks things

* Wed Oct 29 1997 Marc Ewing <marc@redhat.com>
- upgrade to 7.6

* Thu Jul 17 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Mon Mar 03 1997 Erik Troan <ewt@redhat.com>
- Upgraded to version 7.5
- Uses a build root
