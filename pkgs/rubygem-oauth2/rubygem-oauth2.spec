# Generated from oauth2-0.5.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname oauth2

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: A Ruby wrapper for the OAuth 2.0 protocol
Name: rubygem-%{gemname}
Version: 0.5.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/intridea/oauth2
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.3.6
Requires: ruby 
Requires: rubygem(faraday) => 0.7
Requires: rubygem(faraday) < 1
Requires: rubygem(multi_json) => 1.0
Requires: rubygem(multi_json) < 2
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.3.6
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
A Ruby wrapper for the OAuth 2.0 protocol built with a similar style to the
original OAuth gem.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr 01 2012  <meke@pikachu>
- (0.5.2-1m)
- update 0.5.2

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1m)
- udpate 0.5.1

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.0-1m)
- Initial package for Momonga Linux
