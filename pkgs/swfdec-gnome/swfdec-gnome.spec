%global momorel 4

Summary: integrate Flash functionality into the GNOME
Name: swfdec-gnome
Version: 2.30.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: User Interface/Desktops
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.30/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:    python >= 2.6
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: swfdec-devel >= 0.9.2
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: swfdec-gtk-devel >= 0.8.4
BuildRequires: gtk2-devel >= 2.16.0
Requires: GConf2
Requires(pre): hicolor-icon-theme gtk2

%description
Swfdec is a decoder/renderer for Macromedia Flash animations.  The
decoding and rendering engine is provided in a library that can be
used by other applications.

This package contains programs to integrate Flash functionality into
the GNOME desktop.  It's main application is swfdec-player, a
stand-alone viewer for Flash files.  It also contains
swfdec-thumbnailer, a program that provides screenshots for files to
display in the Nautilus file manager
#'

%prep
%setup -q

%build
sed 's,SWFDEC_MAJORMINOR=0.8,SWFDEC_MAJORMINOR=0.9,g' -i configure
%configure --disable-schemas-install
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/swfdec-thumbnailer.schemas \
    > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/swfdec-thumbnailer.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/swfdec-thumbnailer.schemas \
	> /dev/null || :
fi

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/gconf/schemas/swfdec-thumbnailer.schemas
%{_bindir}/swfdec-player
%{_bindir}/swfdec-thumbnailer
%{_datadir}/%{name}
%{_datadir}/applications/swfdec-player.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_mandir}/man1/swfdec-player.1.*
%{_mandir}/man1/swfdec-thumbnailer.1.*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-4m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-3m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-2m)
- rebuild against swfdec-0.9.2

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu Jun 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.22.2-2m)
- reuild against swfdec-0.7.2

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- initial build
