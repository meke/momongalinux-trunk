%global momorel 9

Summary: Allows several audio streams to play on a single audio device
Name: esound
Version: 0.2.41
Release: %{momorel}m%{?dist}
Group: System Environment/Daemons
License: LGPLv2+
URL: http://www.tux.org/~ricdude/EsounD.html
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.2/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: audio_linux.c
Patch0: esound-0.2.41-static.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%ifnarch sparc
BuildRequires: alsa-lib-devel
%endif
BuildRequires: audiofile-devel >= 0.2.6-5m
BuildRequires: docbook-utils >= 0.6.14-12m
BuildRequires: grep >= 2.7
BuildRequires: pkgconfig >= 0.19

%description
EsounD, the Enlightened Sound Daemon, is a server process that mixes
several audio streams for playback by a single audio device. For
example, if you're listening to music on a CD and you receive a
sound-related event from ICQ, the two applications won't have to
jockey for the use of your sound card.

Install esound if you'd like to let sound applications share your
audio device. You'll also need to install the audiofile package.

%package libs
Summary: Library to talk to the EsounD daemon
Group: Development/Libraries

%description libs
The esound-libs package includes the libraries required
for applications to talk to the EsounD daemon.

%package tools
Summary: Commandline tools to talk to the EsounD daemon
Group: System Environment/Base

%description tools
The esound-tools package includes commandline utilities
for controlling the EsounD daemon.

%package devel
Summary: Development files for EsounD applications
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: audiofile-devel
Requires: alsa-lib-devel
Requires: automake
Requires: pkgconfig

%description devel
The esound-devel Libraries, include files and other resources you can
use to develop EsounD applications.

Install esound-devel if you want to develop EsounD applications.

%prep
%setup -q

cp %{SOURCE1} .

%patch0 -p1 -b .static

%build
%configure --enable-ipv6
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall transform="s,x,x,"

# get rid of *.la files
find %{buildroot} -name "*.la" -delete

# clean up esound binary
rm -f %{buildroot}%{_bindir}/esd
rm -f %{buildroot}%{_mandir}/man1/esd.1*


%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files libs
%defattr(-, root, root)
%doc AUTHORS COPYING.LIB ChangeLog INSTALL
%doc MAINTAINERS NEWS README TIPS TODO
%config %{_sysconfdir}/esd.conf
%{_libdir}/libesd*.so.*

%files tools
%defattr(-, root, root)
%{_bindir}/esdcat
%{_bindir}/esdctl
%{_bindir}/esddsp
%{_bindir}/esdfilt
%{_bindir}/esdloop
%{_bindir}/esdmon
%{_bindir}/esdplay
%{_bindir}/esdrec
%{_bindir}/esdsample
%{_mandir}/man1/esdcat.1*
%{_mandir}/man1/esdctl.1*
%{_mandir}/man1/esddsp.1*
%{_mandir}/man1/esdfilt.1*
%{_mandir}/man1/esdloop.1*
%{_mandir}/man1/esdmon.1*
%{_mandir}/man1/esdplay.1*
%{_mandir}/man1/esdrec.1*
%{_mandir}/man1/esdsample.1*

%files devel
%defattr(-, root, root)
%doc docs/esound.sgml docs/html
%{_bindir}/esd-config
%{_includedir}/esd.h
%{_libdir}/pkgconfig/esound.pc
%{_libdir}/libesd*.so
%{_libdir}/libesd*.a
%{_datadir}/aclocal/*.m4
%{_datadir}/doc/%{name}
%{_mandir}/man1/esd-config.1*

%changelog
* Sun Sep 25 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.2.41-9m)
- add patch0 (add pkg-config --static option)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.41-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.41-7m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.41-6m)
- remove POSIXLY_CORRECT environment value
- specify docbook-utils version and release

* Sat Oct 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.41-5m)
- enable to build with new grep

* Sat Oct 30 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (0.2.41-4m)
- esound-devel requires esound-libs, no longer esound package is not exist
 
* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.41-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.41-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.41-1m)
- update to 0.2.41
- License: LGPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.38-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.38-4m)
- rebuild against gcc43

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.38-3m)
- re-split packages and good-bye esound

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.38-2m)
- %%NoSource -> NoSource

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.38-1m)
- update to 0.2.38

* Sat Mar  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.37-2m)
- add patch0 from FC

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.37-1m)
- update to 0.2.37

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.36-4m)
- delete libtool library

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.36-3m)
- comment out unnessesary autoreconf and make check

* Fri Nov 18 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.36-2m)
- emable ipv6

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.36-1m)
- version up
- GNOME 2.12.1 Desktop

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.35-3m)
- modify dependency

* Sun Feb 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.35-2m)
- use automake instead of automake-old

* Sat Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.35-1m)
- version 0.2.35
- GNOME 2.8 Desktop

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.34-2m)
- adjustment BuildPreReq

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.34-1m)
- version 0.2.34
- GNOME 2.6 Desktop
- exchange for Patch0 to Patch2

* Mon Jan 26 2004 Kenta MURATA <muraken2@nifty.com>
- (0.2.32-2m)
- fix for SEGV.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (0.2.32-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.2.32-1m)
- version 0.2.31

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.31-1m)
- version 0.2.31

* Sat Aug 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.30-1m)
- version 0.2.30

* Fri Sep 20 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.29-5m)
- change Source0

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.29-4m)
- update audio-linux.c
- update esound-use-arts.patch

* Wed Aug 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.29-3m)
- update audio-linux.c
- arts TT
  esd -arts if use arts.

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.29-2m)
- update dl patch for alsa 0.9

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.29-1m)
- version 0.2.29

* Sat Jul 06 2002 Kenta MURATA <muraken2@nifty.com>
- (0.2.28-1m)
- version 0.2.28

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.27-4k)
- dl patch

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.27-2k)
- version 0.2.27

* Fri Jun  7 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.26-2k)
- version 0.2.26

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.25-2k)
- version 0.2.25

* Wed May 01 2002 Motonobu Ichimura <famao@kondara.org>
- (0.2.24-4k)
- fix defattr

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.24-2k)
- version 0.2.24

* Sun Mar 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.24-2k)
- version 0.2.24

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.2.23-8k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.23-6k)
- automake autoconf 1.5

* Mon Jan  7 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.23-4k)
- add .pc in %files section
- clean up spec file

* Mon Sep 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.2.23-2k)
- version 0.2.23

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- (0.2.22-8k)
- add patches from RawHide

* Mon Jul 30 2001 Toru Hoshina <toru@df-usa.com>
- (0.2.22-6k)
- add alphaev5 support.

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (0.2.22-5k)
- rebuild against audiofile-0.2.1.

* Sun Mar 16 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Mon Jan 15 2001 Toru Hoshina <toru@df-usa.com>
- (0.2.22-3k)

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (0.2.20-9k)
- rebuild against audiofile-0.2.0.

* Thu Nov 23 2000 Kenichi Matsubara <m@kondara.org>
- not strip in specfile.

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.20-3k)
- fixed for FHS

* Thu Oct 12 2000 Shingo Akagaki <dora@kondara.org>
- Update to 0.2.20
- esound dl patch from Kondara MNU/Linux 
- ALSA OSS runtime selection patch from Kondara MNU/Linux

* Tue Apr 4 2000 Elliot Lee <sopwith@redhat.com> 0.2.18-1
- Update to 0.2.18

* Mon Aug 30 1999 Elliot Lee <sopwith@redhat.com> 0.2.13-1
- Update to 0.2.13
- Merge in changes from RHL 6.0 spec file.

* Sat Nov 21 1998 Pablo Saratxaga <srtxg@chanae.alphanet.ch>

- added %{_prefix}/share/aclocal/* to %files devel
- added spanish and french translations for rpm

* Thu Oct 1 1998 Ricdude <ericmit@ix.netcom.com>

- make autoconf do the version updating for us.

* Wed May 13 1998 Michael Fulbright <msf@redhat.com>

- First try at an RPM
