/* Linux DL Sounds support for EsounD
   1-05-00: Shingo Akagaki (dora@kondara.org) - support DL
   Advanced Linux Sounds Architecture support for EsounD
   7-19-98: Nick Lopez( kimo_sabe@usa.net ) - it starts!
*/
#include "esd.h"

/* debugging messages for audio device */
static int driver_trace = 0;

#if defined(DRIVER_NEWALSA) || defined(DRIVER_ALSA_09)
#  include <sys/asoundlib.h>
#else
#  include <sys/soundlib.h>
#endif

#ifdef HAVE_MACHINE_SOUNDCARD_H
#  include <machine/soundcard.h>
#else
#  ifdef HAVE_SOUNDCARD_H
#    include <soundcard.h>
#  else
#    include <sys/soundcard.h>
#  endif
#endif

#include <dlfcn.h>

void *handle;
void *lib_handle=NULL;

typedef enum _drivers{
    USE_ALSA,
    USE_OSS,
    NOT_DETECTED,
    NONE
}drivers;

drivers driver_name=NOT_DETECTED;

/* so that EsounD can use other cards besides the first */
#ifndef ALSACARD
#  define ALSACARD 0
#endif
#ifndef ALSADEVICE
#  define ALSADEVICE 0
#endif

#if defined(DRIVER_ALSA_09)
/* Error flag */
int alsaerr = 0;

/* FULL DUPLEX => two handlers */

static snd_pcm_t *alsa_playback_handle = NULL;
static snd_pcm_t *alsa_capture_handle = NULL;

static snd_output_t *output = NULL;

# define ACS  SND_PCM_ACCESS_RW_INTERLEAVED
# define BUFFERSIZE 16384

# define DEF_FP(rt, name, args) rt (* name##_func) args
DEF_FP(int, snd_card_next, (int*));
DEF_FP(int, snd_ctl_card_info, (snd_ctl_t*, snd_ctl_card_info_t*));
DEF_FP(const char*, snd_ctl_card_info_get_name, (const snd_ctl_card_info_t*));
DEF_FP(int, snd_ctl_close, (snd_ctl_t*));
DEF_FP(int, snd_ctl_open, (snd_ctl_t**, const char*, int));
DEF_FP(int, snd_output_stdio_attach, (snd_output_t**, FILE*, int));
DEF_FP(snd_pcm_sframes_t, snd_pcm_bytes_to_frames, (snd_pcm_t*, ssize_t));
DEF_FP(int, snd_pcm_close, (snd_pcm_t*));
DEF_FP(int, snd_pcm_drain, (snd_pcm_t*));
DEF_FP(ssize_t, snd_pcm_frames_to_bytes, (snd_pcm_t*, snd_pcm_sframes_t));
DEF_FP(int, snd_pcm_hw_params, (snd_pcm_t*, snd_pcm_hw_params_t*));
DEF_FP(int, snd_pcm_hw_params_any, (snd_pcm_t*, snd_pcm_hw_params_t*));
DEF_FP(int, snd_pcm_hw_params_set_access, (snd_pcm_t*, snd_pcm_hw_params_t*, snd_pcm_access_t));
DEF_FP(int, snd_pcm_hw_params_set_buffer_size_near, (snd_pcm_t*, snd_pcm_hw_params_t*, snd_pcm_uframes_t*));
DEF_FP(int, snd_pcm_hw_params_set_channels, (snd_pcm_t*, snd_pcm_hw_params_t*, unsigned int));
DEF_FP(int, snd_pcm_hw_params_set_format, (snd_pcm_t*, snd_pcm_hw_params_t*, snd_pcm_format_t));
DEF_FP(int, snd_pcm_hw_params_set_periods_integer, (snd_pcm_t*, snd_pcm_hw_params_t*));
DEF_FP(int, snd_pcm_hw_params_set_periods_max, (snd_pcm_t*, snd_pcm_hw_params_t*, unsigned int*, int*));
DEF_FP(int, snd_pcm_hw_params_set_periods_min, (snd_pcm_t*, snd_pcm_hw_params_t*, unsigned int*, int*));
DEF_FP(int, snd_pcm_hw_params_set_rate_near, (snd_pcm_t*, snd_pcm_hw_params_t*, unsigned int*, int*));
DEF_FP(int, snd_pcm_nonblock, (snd_pcm_t*, int));
DEF_FP(int, snd_pcm_open, (snd_pcm_t**, const char*, snd_pcm_stream_t, int));
DEF_FP(int, snd_pcm_prepare, (snd_pcm_t*));
DEF_FP(snd_pcm_sframes_t, snd_pcm_readi, (snd_pcm_t*, void*, snd_pcm_uframes_t));
DEF_FP(int, snd_pcm_resume, (snd_pcm_t*));
DEF_FP(snd_pcm_sframes_t, snd_pcm_writei, (snd_pcm_t*, const void*, snd_pcm_uframes_t));
DEF_FP(int, snd_card_next, (int*));
DEF_FP(int, snd_ctl_card_info, (snd_ctl_t*, snd_ctl_card_info_t*));
DEF_FP(const char*, snd_ctl_card_info_get_name, (const snd_ctl_card_info_t*));
DEF_FP(size_t, snd_ctl_card_info_sizeof, (void));
DEF_FP(int, snd_ctl_close, (snd_ctl_t*));
DEF_FP(int, snd_ctl_open, (snd_ctl_t**, const char*, int));
DEF_FP(int, snd_output_stdio_attach, (snd_output_t**, FILE*, int));
DEF_FP(snd_pcm_sframes_t, snd_pcm_bytes_to_frames, (snd_pcm_t*, ssize_t));
DEF_FP(int, snd_pcm_close, (snd_pcm_t*));
DEF_FP(int, snd_pcm_drain, (snd_pcm_t*));
DEF_FP(ssize_t, snd_pcm_frames_to_bytes, (snd_pcm_t*, snd_pcm_sframes_t));
DEF_FP(int, snd_pcm_hw_params, (snd_pcm_t*, snd_pcm_hw_params_t*));
DEF_FP(int, snd_pcm_hw_params_any, (snd_pcm_t*, snd_pcm_hw_params_t*));
DEF_FP(int, snd_pcm_hw_params_set_access, (snd_pcm_t*, snd_pcm_hw_params_t*, snd_pcm_access_t));
DEF_FP(int, snd_pcm_hw_params_set_buffer_size_near, (snd_pcm_t*, snd_pcm_hw_params_t*, snd_pcm_uframes_t*));
DEF_FP(int, snd_pcm_hw_params_set_channels, (snd_pcm_t*, snd_pcm_hw_params_t*, unsigned int));
DEF_FP(int, snd_pcm_hw_params_set_format, (snd_pcm_t*, snd_pcm_hw_params_t*, snd_pcm_format_t));
DEF_FP(int, snd_pcm_hw_params_set_periods_integer, (snd_pcm_t*, snd_pcm_hw_params_t*));
DEF_FP(int, snd_pcm_hw_params_set_periods_max, (snd_pcm_t*, snd_pcm_hw_params_t*, unsigned int*, int*));
DEF_FP(int, snd_pcm_hw_params_set_periods_min, (snd_pcm_t*, snd_pcm_hw_params_t*, unsigned int*, int*));
DEF_FP(int, snd_pcm_hw_params_set_rate_near, (snd_pcm_t*, snd_pcm_hw_params_t*, unsigned int*, int*));
DEF_FP(size_t, snd_pcm_hw_params_sizeof, (void));
DEF_FP(int, snd_pcm_nonblock, (snd_pcm_t*, int));
DEF_FP(int, snd_pcm_open, (snd_pcm_t**, const char*, snd_pcm_stream_t, int));
DEF_FP(int, snd_pcm_prepare, (snd_pcm_t*));
DEF_FP(snd_pcm_sframes_t, snd_pcm_readi, (snd_pcm_t*, void*, snd_pcm_uframes_t));
DEF_FP(int, snd_pcm_resume, (snd_pcm_t*));
DEF_FP(snd_pcm_sframes_t, snd_pcm_writei, (snd_pcm_t*, const void*, snd_pcm_uframes_t));
DEF_FP(const char*, snd_strerror, (int));

static int
setup_alsa_dl()
{
    if(!lib_handle)return 0;

# define DLSYM(name) {\
    name##_func = dlsym(lib_handle, #name);\
    if (dlerror()) return 0;\
}
    DLSYM(snd_card_next);
    DLSYM(snd_ctl_card_info);
    DLSYM(snd_ctl_card_info_get_name);
    DLSYM(snd_ctl_card_info_sizeof);
    DLSYM(snd_ctl_close);
    DLSYM(snd_ctl_open);
    DLSYM(snd_output_stdio_attach);
    DLSYM(snd_pcm_bytes_to_frames);
    DLSYM(snd_pcm_close);
    DLSYM(snd_pcm_drain);
    DLSYM(snd_pcm_frames_to_bytes);
    DLSYM(snd_pcm_hw_params);
    DLSYM(snd_pcm_hw_params_any);
    DLSYM(snd_pcm_hw_params_set_access);
    DLSYM(snd_pcm_hw_params_set_buffer_size_near);
    DLSYM(snd_pcm_hw_params_set_channels);
    DLSYM(snd_pcm_hw_params_set_format);
    DLSYM(snd_pcm_hw_params_set_periods_integer);
    DLSYM(snd_pcm_hw_params_set_periods_max);
    DLSYM(snd_pcm_hw_params_set_periods_min);
    DLSYM(snd_pcm_hw_params_set_rate_near);
    DLSYM(snd_pcm_hw_params_sizeof);
    DLSYM(snd_pcm_nonblock);
    DLSYM(snd_pcm_open);
    DLSYM(snd_pcm_prepare);
    DLSYM(snd_pcm_readi);
    DLSYM(snd_pcm_resume);
    DLSYM(snd_pcm_writei);
    DLSYM(snd_strerror);
# undef DLSYM

    return 1;
}

# define snd_card_next snd_card_next_func
# define snd_ctl_card_info snd_ctl_card_info_func
# define snd_ctl_card_info_get_name snd_ctl_card_info_get_name_func
# define snd_ctl_card_info_sizeof snd_ctl_card_info_sizeof_func
# define snd_ctl_close snd_ctl_close_func
# define snd_ctl_open snd_ctl_open_func
# define snd_output_stdio_attach snd_output_stdio_attach_func
# define snd_pcm_bytes_to_frames snd_pcm_bytes_to_frames_func
# define snd_pcm_close snd_pcm_close_func
# define snd_pcm_drain snd_pcm_drain_func
# define snd_pcm_frames_to_bytes snd_pcm_frames_to_bytes_func
# define snd_pcm_hw_params snd_pcm_hw_params_func
# define snd_pcm_hw_params_any snd_pcm_hw_params_any_func
# define snd_pcm_hw_params_set_access snd_pcm_hw_params_set_access_func
# define snd_pcm_hw_params_set_buffer_size_near snd_pcm_hw_params_set_buffer_size_near_func
# define snd_pcm_hw_params_set_channels snd_pcm_hw_params_set_channels_func
# define snd_pcm_hw_params_set_format snd_pcm_hw_params_set_format_func
# define snd_pcm_hw_params_set_periods_integer snd_pcm_hw_params_set_periods_integer_func
# define snd_pcm_hw_params_set_periods_max snd_pcm_hw_params_set_periods_max_func
# define snd_pcm_hw_params_set_periods_min snd_pcm_hw_params_set_periods_min_func
# define snd_pcm_hw_params_set_rate_near snd_pcm_hw_params_set_rate_near_func
# define snd_pcm_hw_params_sizeof snd_pcm_hw_params_sizeof_func
# define snd_pcm_nonblock snd_pcm_nonblock_func
# define snd_pcm_open snd_pcm_open_func
# define snd_pcm_prepare snd_pcm_prepare_func
# define snd_pcm_readi snd_pcm_readi_func
# define snd_pcm_resume snd_pcm_resume_func
# define snd_pcm_writei snd_pcm_writei_func
# define snd_strerror snd_strerror_func

static void
cleanup_alsa_dl()
{
    if(lib_handle)
        dlclose(lib_handle);
    if(driver_name == USE_ALSA)
        driver_name = NOT_DETECTED;
}
#endif // defined(DRIVER_ALSA_09)

#define ARCH_esd_audio_open
#if defined(DRIVER_ALSA_09)
snd_pcm_t* initAlsa(char *dev, int format, int channels, int speed, int mode)
{
    snd_pcm_t *handle;
    snd_pcm_hw_params_t *hwparams;
    int err;
    int periods;
 
    err = snd_pcm_open(&handle, dev, mode, SND_PCM_NONBLOCK);
    if (err < 0) {
        alsaerr = -2;
        return NULL;
    }
    snd_pcm_nonblock(handle, 0);
    snd_pcm_hw_params_alloca(&hwparams);
 
    err = snd_pcm_hw_params_any(handle, hwparams);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }
 
    err = snd_pcm_hw_params_set_access(handle, hwparams, ACS);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }
 
    err = snd_pcm_hw_params_set_format(handle, hwparams, format);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }

    err = snd_pcm_hw_params_set_channels(handle, hwparams,  channels);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }

    err = snd_pcm_hw_params_set_rate_near(handle, hwparams, &speed, 0);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }
    if (err != speed) {
        alsaerr = -1;
        return handle;
    }

    err = snd_pcm_hw_params_set_periods_integer(handle, hwparams);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }
    periods = 2;
    err = snd_pcm_hw_params_set_periods_min(handle, hwparams, &periods, 0);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }

    periods = 64;
    err = snd_pcm_hw_params_set_periods_max(handle, hwparams, &periods, 0);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }

    err = snd_pcm_hw_params_set_buffer_size_near(handle, hwparams, BUFFERSIZE);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }
    err = snd_pcm_hw_params(handle, hwparams);
    if (err < 0) {
        alsaerr = -1;
        return handle;
    }
    alsaerr = 0;
    return handle;
}

static int
esd_audio_open_alsa()
{
    int channels;
    int format;
    int card;
    char *dev;

    if ((esd_audio_format & ESD_MASK_BITS) == ESD_BITS16)
        format = SND_PCM_FORMAT_S16;
    else format = SND_PCM_FORMAT_U8;

    if ((esd_audio_format & ESD_MASK_CHAN) == ESD_STEREO)
        channels = 2;
    else channels = 1;


    snd_output_stdio_attach(&output, stderr, 0);

    if(esd_audio_device) {
        dev = (char*) malloc(strlen(esd_audio_device)+1);
        strcpy(dev, esd_audio_device);
    }
    else {
        card = -1;
        if( snd_card_next(&card) < 0 || card < 0 ) {
            fprintf( stderr, "audio_alsa: no cards found!\n" );
            esd_audio_fd = -1;
            return -2 ;
        }
        dev = (char*) malloc(10);
        sprintf(dev, "hw:%i", card);
    }

    alsa_playback_handle = initAlsa(dev, format, channels,
                                    esd_audio_rate, SND_PCM_STREAM_PLAYBACK);
    if(alsaerr) {
        if(alsaerr == -1) snd_pcm_close(alsa_playback_handle);
        esd_audio_fd = -1;
        free(dev);
        return alsaerr;
    }

    if ((esd_audio_format & ESD_MASK_FUNC) == ESD_RECORD) {
        alsa_capture_handle = initAlsa(dev, format, channels,
                                       esd_audio_rate, SND_PCM_STREAM_CAPTURE);
        if (alsaerr) {
            if (alsaerr==-1) snd_pcm_close(alsa_capture_handle);
            snd_pcm_close(alsa_playback_handle);
            esd_audio_fd = -1;
            free(dev);
            return alsaerr;
        }
    }
    esd_audio_fd = 0;
    free(dev);

    return 0;
}
#endif // defined(DRIVER_ALSA_09)

static int
esd_audio_open_oss()
{
    const char *device;

    int afd = -1, value = 0, test = 0;
    int mode = O_WRONLY;

    /* if recording, set for full duplex mode */
    if ( (esd_audio_format & ESD_MASK_FUNC) == ESD_RECORD )
        mode = O_RDWR;

    /* open the sound device */
    device = esd_audio_device ? esd_audio_device : "/dev/dsp";
    if ((afd = open(device, mode, 0)) == -1)
    {   /* Opening device failed */
//        //perror(device);
        return( -2 );
    }

    mode = fcntl(afd, F_GETFL);
    mode &= ~O_NONBLOCK;
    fcntl(afd, F_SETFL, mode);

    /* TODO: check that this is allowable */
    /* set for full duplex operation, if recording */
#if defined(SNDCTL_DSP_SETDUPLEX)
    if ( (esd_audio_format & ESD_MASK_FUNC) == ESD_RECORD ) {
        ioctl( afd, SNDCTL_DSP_SETDUPLEX, 0 );
    }
#endif

    /* set the sound driver fragment size and number */
#if !defined(__powerpc__) /* does not exist on powerpc */
    /* fragment = max_buffers << 16 + log2(buffer_size), (256 16) */
    value = test = ( 0x0100 << 16 ) + 0x0008;
    if (ioctl(afd, SNDCTL_DSP_SETFRAGMENT, &test) == -1)
    {   /* Fatal error */
//        perror( "SNDCTL_DSP_SETFRAGMENT" );
        close( afd );
        esd_audio_fd = -1;
        return( -1 );
    }
    if ( 0 /* value != test */ ) /* TODO: determine the real test */
    {   /* The device doesn't support the requested audio format. */
        fprintf( stderr, "unsupported fragment size: %d\n", value );
        close( afd );
        esd_audio_fd = -1;
        return( -1 );
    }
#endif /* #if !defined(__powerpc__) */

    /* set the sound driver audio format for playback */

    value = test = ( (esd_audio_format & ESD_MASK_BITS) == ESD_BITS16 )
            ? /* 16 bit */ AFMT_S16_NE : /* 8 bit */ AFMT_U8;

    if (ioctl(afd, SNDCTL_DSP_SETFMT, &test) == -1)
    {   /* Fatal error */
//        perror("SNDCTL_DSP_SETFMT");
        close( afd );
        esd_audio_fd = -1;
        return( -1 );
    }

    ioctl(afd, SNDCTL_DSP_GETFMTS, &test);
    if ( !(value & test) ) /* TODO: should this be if ( value XOR test ) ??? */
    {   /* The device doesn't support the requested audio format. */
        fprintf( stderr, "unsupported sound format: %d\n", esd_audio_format );
        close( afd );
        esd_audio_fd = -1;
        return( -1 );
    }

    /* set the sound driver number of channels for playback */
    value = test = ( ( ( esd_audio_format & ESD_MASK_CHAN) == ESD_STEREO )
                     ? /* stereo */ 1 : /* mono */ 0 );
    if (ioctl(afd, SNDCTL_DSP_STEREO, &test) == -1)
    {   /* Fatal error */
        //perror( "SNDCTL_DSP_STEREO" );
        close( afd );
        esd_audio_fd = -1;
        return( -1 );
    }

    /* set the sound driver number playback rate */
    test = esd_audio_rate;
    if ( ioctl(afd, SNDCTL_DSP_SPEED, &test) == -1)
    { /* Fatal error */
        //perror("SNDCTL_DSP_SPEED");
        close( afd );
        esd_audio_fd = -1;
        return( -1 );
    }

    /* see if actual speed is within 5% of requested speed */
    if( fabs( test - esd_audio_rate ) > esd_audio_rate * 0.05 )
    {
        fprintf( stderr, "unsupported playback rate: %d\n", esd_audio_rate );
        close( afd );
        esd_audio_fd = -1;
        return( -1 );
    }

    /* value = test = buf_size; */
    esd_audio_fd = afd;
    return afd;
}

#define ARCH_esd_audio_open
int esd_audio_open()
{
    if(driver_name == USE_ALSA) return esd_audio_open_alsa();
    if(driver_name == USE_OSS) return esd_audio_open_oss();
    if(driver_name == NOT_DETECTED){
        lib_handle = dlopen("libasound.so",RTLD_LAZY|RTLD_GLOBAL);
        if(!lib_handle){
            driver_name = USE_OSS;
            return esd_audio_open_oss();
        }
        if(setup_alsa_dl()){
            int ret = esd_audio_open_alsa();
            if(ret >= 0){
                driver_name = USE_ALSA;
                return ret;
            }
        }
        cleanup_alsa_dl();
        driver_name = USE_OSS;
        return esd_audio_open_oss();
    }
    driver_name = NONE;
    return -1;
}

#define ARCH_esd_audio_close
void esd_audio_close()
{
    switch(driver_name){
    case USE_ALSA:
#if defined(DRIVER_ALSA_09)
        if (alsa_playback_handle != NULL)
            snd_pcm_close( alsa_playback_handle );
        if (alsa_capture_handle != NULL)
            snd_pcm_close(alsa_capture_handle);
        alsa_playback_handle = NULL;
        alsa_capture_handle = NULL;
#endif
        cleanup_alsa_dl();
        driver_name = NOT_DETECTED;
        return;

    case USE_OSS:
        close( esd_audio_fd );
        return;

    default:
        return;
    }
}

#define ARCH_esd_audio_pause
void esd_audio_pause()
{
    switch(driver_name){
    case USE_ALSA:
        return;

    case USE_OSS:
        /* per oss specs */
        ioctl( esd_audio_fd, SNDCTL_DSP_POST, 0 );
        return;

    default:
        return;
    }
}

#define ARCH_esd_audio_read
int esd_audio_read( void *buffer, int buf_size )
{
#if defined(DRIVER_ALSA_09)
    int err;
    int len;
#endif
    switch(driver_name){
    case USE_ALSA:
#if defined(DRIVER_ALSA_09)
        len = snd_pcm_bytes_to_frames(alsa_capture_handle, buf_size);
        while ((err = snd_pcm_readi(alsa_capture_handle, buffer, len)) < 0) {
            if (err == -EPIPE) {
                if ((err = snd_pcm_prepare(alsa_capture_handle)) < 0) {
                    return -1;
                }
                continue;
            }
            else if (err == -ESTRPIPE) {
                while ((err = snd_pcm_resume(alsa_capture_handle)) == -EAGAIN)
                    sleep(1);
                if (err < 0) {
                    if (snd_pcm_prepare(alsa_capture_handle) < 0)
                        return -1;
                }
                continue;
            }

            err = snd_pcm_prepare(alsa_capture_handle);
            if (err < 0) {
                return -1;
            }
        }
        return snd_pcm_frames_to_bytes(alsa_capture_handle, err);
#endif

    case USE_OSS:
        return read(esd_audio_fd, buffer, buf_size);

    default:
        return -1;
    }
}

int writes;
#define ARCH_esd_audio_write
int esd_audio_write( void *buffer, int buf_size )
{
    switch(driver_name){
    case USE_ALSA:
#if defined(DRIVER_ALSA_09)
        {
            int err;
            int len = snd_pcm_bytes_to_frames(alsa_playback_handle, buf_size);

            while ((err = snd_pcm_writei(alsa_playback_handle, buffer, len)) < 0) {
                if (err == -EPIPE) {
                    if ((err = snd_pcm_prepare(alsa_playback_handle)) < 0) {
                        return -1;
                    }
                    continue;
                }
                else if (err == -ESTRPIPE) {
                    while ((err = snd_pcm_resume(alsa_playback_handle)) == -EAGAIN)
                        sleep(1);
                    if (err < 0) {
                        if (snd_pcm_prepare(alsa_playback_handle) < 0)
                            return -1;
                    }
                    continue;
                }
                err = snd_pcm_prepare(alsa_playback_handle);
                if (err < 0) {
                    return -1;
                }
            }

            return snd_pcm_frames_to_bytes(alsa_playback_handle, err);
        }
#endif

    case USE_OSS:
        return write( esd_audio_fd, buffer, buf_size );

    default:
        return -1;
    }
}

#define ARCH_esd_audio_flush
void esd_audio_flush()
{
    switch(driver_name){
    case USE_ALSA:
#if defined(DRIVER_ALSA_09)
        if (alsa_playback_handle != NULL)
            snd_pcm_drain( alsa_playback_handle );
#endif
        return;

    case USE_OSS:
        fsync( esd_audio_fd );
        return;
    default:
        return;
    }
}


#define ARCH_esd_audio_devices
const char *esd_audio_devices()
{
#if defined(DRIVER_ALSA_09)
    int card, err;
    static char *all_alsa_cards=0;
    char *alsa_card_tmp;
    snd_ctl_t *handle;
    snd_ctl_card_info_t *info;
#endif
    if(driver_name==NOT_DETECTED)
        esd_audio_open();
    switch(driver_name){
    case USE_ALSA:
#if defined(DRIVER_ALSA_09)
        snd_ctl_card_info_alloca(&info);

        if(all_alsa_cards) {
            free(all_alsa_cards);
            all_alsa_cards = 0;
        }

        card = -1;
        if(snd_card_next(&card) < 0 || card < 0) {
            /* No cards found */
        }
        else {
            while(card >= 0) {
                char name[32];
                sprintf( name, "hw:%d", card );
                err = snd_ctl_open(&handle, name, 0);
                if(err < 0) {
                    fprintf(stderr,
                            "audio_alsa: Error: control open (%i): %s\n",
                            card, snd_strerror(err));
                    continue;
                }
                err = snd_ctl_card_info(handle, info);
                if(err < 0) {
                    fprintf(stderr,
                            "audio_alsa: Error: control hardware info (%i): %s\n",
                            card, snd_strerror(err));
                    snd_ctl_close(handle);
                    continue;
                }
                alsa_card_tmp = malloc(strlen(snd_ctl_card_info_get_name(info))+20);
                sprintf(alsa_card_tmp,
                        "hw:%d  (%s)\n",
                        card, snd_ctl_card_info_get_name(info));
                if(all_alsa_cards) {
                    all_alsa_cards = realloc(all_alsa_cards,
                                             strlen(all_alsa_cards)+strlen(alsa_card_tmp)+30);
                    strcat(all_alsa_cards, "                       ");
                    strcat(all_alsa_cards, alsa_card_tmp);
                    free(alsa_card_tmp);
                }
                else {
                    all_alsa_cards = alsa_card_tmp;
                }
                snd_ctl_close(handle);
                if(snd_card_next(&card) < 0) {
                    break;
                }
            }
        }
        if(all_alsa_cards) {
            return(all_alsa_cards);
        }
        else {
            return("No available cards found");
        }
#endif

    case USE_OSS:
        return "/dev/dsp, /dev/dsp2, etc.";

    default:
        return "No available cards found";;
    }
}
