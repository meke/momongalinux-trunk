%global momorel 8

Name:		obconf
Version:	2.0.3
Release:	%{momorel}m%{?dist}
Summary:	A graphical configuration editor for the Openbox window manager

Group:		User Interface/X
License:	GPLv2+
URL:		http://openbox.org/wiki/ObConf:About
Source0:        http://openbox.org/dist/obconf/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	openbox-devel >= 3.4
BuildRequires:	libglade2-devel
BuildRequires:	startup-notification-devel
BuildRequires:	pkgconfig
BuildRequires:	desktop-file-utils
BuildRequires:	libSM-devel

%description
ObConf is a graphical configuration editor for the Openbox window manager. 


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}
desktop-file-install --vendor=	\
	--dir %{buildroot}%{_datadir}/applications	\
	--delete-original	\
	%{buildroot}%{_datadir}/applications/%{name}.desktop


%clean
rm -rf %{buildroot}


%post
update-mime-database %{_datadir}/mime &> /dev/null
update-desktop-database %{_datadir}/applications &> /dev/null
:


%postun
update-mime-database %{_datadir}/mime &> /dev/null
update-desktop-database %{_datadir}/applications &> /dev/null
:


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/%{name}
%{_datadir}/%{name}/  
%{_datadir}/applications/%{name}.desktop
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/mimelnk/application/x-openbox-theme.desktop
%{_datadir}/pixmaps/%{name}.png


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-6m)
- full rebuild for mo7 release

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-5m)
- change URI and source URI
- source was replaced again, please remove SOURCES/obconf-2.0.3.tar.gz
 
* Thu Jan 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-4m)
- source was replaced, please remove SOURCES/obconf-2.0.3.tar.gz

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Apr  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-2m)
- remove duplicate directories

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-1m)
- import from Rawhide

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Apr 17 2008 Miroslav Lichvar <mlichvar@redhat.com> - 2.0.3-2
- Rebuild for new openbox

* Sun Feb 03 2008 Miroslav Lichvar <mlichvar@redhat.com> - 2.0.3-1
- Update to 2.0.3

* Wed Aug 22 2007 Miroslav Lichvar <mlichvar@redhat.com> - 2.0.2-2
- Update license tag

* Mon Jul 23 2007 Miroslav Lichvar <mlichvar@redhat.com> - 2.0.2-1
- Update to 2.0.2

* Thu Jun 14 2007 Miroslav Lichvar <mlichvar@redhat.com> - 2.0.1-1
- Update to 2.0.1

* Sun Aug 27 2006 Peter Gordon <peter@thecodergeek.com> - 1.6-3
- Mass FC6 rebuild

* Thu Jul 13 2006 Peter Gordon <peter@thecodergeek.com> - 1.6-2
- Add BR: libSM-devel to fix build issue.

* Fri Jun 09 2006 Peter Gordon <peter@thecodergeek.com> - 1.6-1
- Initial packaging. 
