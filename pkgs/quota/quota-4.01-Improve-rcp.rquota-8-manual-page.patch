From e4e473c7bb8f30604d763074a6d04e2854249552 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Petr=20P=C3=ADsa=C5=99?= <ppisar@redhat.com>
Date: Mon, 21 Oct 2013 14:24:52 +0200
Subject: [PATCH 4/4] Improve rcp.rquota(8) manual page
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

This patch renames rquotad(8) to rpc.rquotad(8) to reflect the
executable name. It also shows completes synopsis and documents --help option.

Signed-off-by: Petr Písař <ppisar@redhat.com>
---
 rpc.rquotad.8 | 112 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 rquotad.8     |  95 -------------------------------------------------
 2 files changed, 112 insertions(+), 95 deletions(-)
 create mode 100644 rpc.rquotad.8
 delete mode 100644 rquotad.8

diff --git a/rpc.rquotad.8 b/rpc.rquotad.8
new file mode 100644
index 0000000..a045dba
--- /dev/null
+++ b/rpc.rquotad.8
@@ -0,0 +1,112 @@
+.TH RQUOTAD 8
+.SH NAME
+rpc.rquotad \- remote quota server
+.SH SYNOPSIS
+.B /usr/sbin/rpc.rquotad
+[
+.BR \-FI
+] [
+.B \-p
+.I port
+] [
+.B \-s
+|
+.B \-S
+] [
+.B \-x
+.I path
+]
+.LP
+.B /usr/sbin/rpc.rquotad
+[
+.B \-h
+|
+.B \-V
+]
+.SH DESCRIPTION
+.LP
+.IX  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
+.IX  daemons  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
+.IX  "user quotas"  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
+.IX  "disk quotas"  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
+.IX  "quotas"  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
+.IX  "filesystem"  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
+.IX  "remote procedure call services"  "rquotad"  ""  "\fLrquotad\fP \(em remote quota server"
+.B rpc.rquotad
+is an
+.BR rpc (3)
+server which returns quotas for a user of a local filesystem
+which is mounted by a remote machine over the
+.SM NFS\s0.
+It also allows setting of quotas on
+.SM NFS
+mounted filesystem (if configured during compilation and allowed by a command line option
+.BR \-S ).
+The results are used by
+.BR quota (1)
+to display user quotas for remote filesystems and by
+.BR edquota (8)
+to set quotas on remote filesystems.
+.B rquotad
+daemon uses tcp-wrappers library (under service name
+.IR rquotad )
+which allows you to specify hosts allowed/disallowed to use
+the daemon (see
+.BR hosts.allow (5)
+manpage for more information). The
+.B rquotad
+daemon is normally started at boot time from the
+system startup scripts.
+.SH OPTIONS
+.TP
+.B \-h, \-\-help
+Show program usage and exit.
+.B \-V, \-\-version
+Show version of quota tools.
+.TP
+.B \-s, \-\-no-setquota
+Don't allow setting of quotas (default). This option is available only
+if utilities were compiled with the
+.I rpcsetquota
+option.
+.TP
+.B \-S, \-\-setquota
+Allow setting of quotas. This option is available only
+if utilities were compiled with the
+.I rpcsetquota
+option.
+.TP
+.B \-F, \-\-foreground
+Run daemon in foreground (may be useful for debugging purposes).
+.TP
+.B \-I, \-\-autofs
+Do not ignore autofs mountpoints.
+.TP
+.B \-p \f2port\f3, \-\-port \f2port\f1
+Listen on alternate port
+.IR port.
+.TP
+.B \-x \f2path\f3, \-\-xtab \f2path\f1
+Set an alternative file with NFSD export table. This file is used to
+determine pseudoroot of NFSv4 exports. The pseudoroot is then prepended
+to each relative path (i.e. a path not beginning by '/') received in a
+quota RPC request.
+
+.SH FILES
+.PD 0
+.TP 20
+.B aquota.user or aquota.group
+quota file at the filesystem root (version 2 quota, non-XFS filesystems)
+.TP
+.B quota.user or quota.group
+quota file at the filesystem root (version 1 quota, non-XFS filesystems)
+.TP
+.B /etc/mtab
+default filesystems
+.PD
+.SH "SEE ALSO"
+.BR quota (1),
+.BR rpc (3),
+.BR nfs (5),
+.BR services (5),
+.BR inetd (8)
diff --git a/rquotad.8 b/rquotad.8
deleted file mode 100644
index f18bdcc..0000000
--- a/rquotad.8
+++ /dev/null
@@ -1,95 +0,0 @@
-.TH RQUOTAD 8
-.SH NAME
-rquotad, rpc.rquotad \- remote quota server
-.SH SYNOPSIS
-.B rpc.rquotad
-[
-.B \-sSFI
-] [
-.B \-p \f2port\f1
-]
-.SH DESCRIPTION
-.LP
-.IX  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
-.IX  daemons  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
-.IX  "user quotas"  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
-.IX  "disk quotas"  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
-.IX  "quotas"  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
-.IX  "filesystem"  "rquotad daemon"  ""  "\fLrquotad\fP \(em remote quota server"
-.IX  "remote procedure call services"  "rquotad"  ""  "\fLrquotad\fP \(em remote quota server"
-.B rquotad
-is an
-.BR rpc (3)
-server which returns quotas for a user of a local filesystem
-which is mounted by a remote machine over the
-.SM NFS\s0.
-It also allows setting of quotas on
-.SM NFS
-mounted filesystem (if configured during compilation and allowed by a command line option
-.BR \-S ).
-The results are used by
-.BR quota (1)
-to display user quotas for remote filesystems and by
-.BR edquota (8)
-to set quotas on remote filesystems.
-.B rquotad
-daemon uses tcp-wrappers library (under service name
-.IR rquotad )
-which allows you to specify hosts allowed/disallowed to use
-the daemon (see
-.BR hosts.allow (5)
-manpage for more information). The
-.B rquotad
-daemon is normally started at boot time from the
-system startup scripts.
-.SH OPTIONS
-.TP
-.B \-V, \-\-version
-Shows version of quota tools.
-.TP
-.B \-s, \-\-no-setquota
-Don't allow setting of quotas (default). This option is available only
-if utilities were compiled with the
-.I rpcsetquota
-option.
-.TP
-.B \-S, \-\-setquota
-Allow setting of quotas. This option is available only
-if utilities were compiled with the
-.I rpcsetquota
-option.
-.TP
-.B \-F, \-\-foreground
-Run daemon in foreground (may be useful for debugging purposes).
-.TP
-.B \-I, \-\-autofs
-Do not ignore autofs mountpoints.
-.TP
-.B \-p \f2port\f3, \-\-port \f2port\f1
-Listen on alternate port
-.IR port.
-.TP
-.B \-x \f2path\f3, \-\-xtab \f2path\f1
-Set an alternative file with NFSD export table. This file is used to
-determine pseudoroot of NFSv4 exports. The pseudoroot is then prepended
-to each relative path (i.e. a path not beginning by '/') received in a
-quota RPC request.
-
-.SH FILES
-.PD 0
-.TP 20
-.B aquota.user or aquota.group
-quota file at the filesystem root (version 2 quota, non-XFS filesystems)
-.TP
-.B quota.user or quota.group
-quota file at the filesystem root (version 1 quota, non-XFS filesystems)
-.TP
-.B /etc/mtab
-default filesystems
-.PD
-.SH "SEE ALSO"
-.BR quota (1),
-.BR rpc (3),
-.BR nfs (5),
-.BR services (5),
-.BR inetd (8)
-- 
1.8.3.1

