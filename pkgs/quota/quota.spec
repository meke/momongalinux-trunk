%global momorel 1

#allow remote set quota by defined rpcsetquota to 1(set to 0 to disabled it)
%{!?rpcsetquota:%define rpcsetquota 1}

Name: quota
Summary: System administration tools for monitoring users' disk usage
Version: 4.01
Release: %{momorel}m%{?dist}
License: BSD and GPLv2+
URL: http://sourceforge.net/projects/linuxquota/
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: initscripts >= 6.38 tcp_wrappers e2fsprogs
Requires: quota-nls = %{version}-%{release}
Conflicts: kernel < 2.4
BuildRequires: e2fsprogs-devel gettext tcp_wrappers-devel nss-devel
BuildRequires: openldap-devel openssl-devel dbus-devel libnl-devel
BuildRequires: systemd
Provides: /sbin/quotacheck
Provides: /sbin/quotaon

Source0: http://dl.sourceforge.net/sourceforge/linuxquota/%{name}-%{version}.tar.gz
NoSource: 0
Source1: quota_nld.service
Source2: quota_nld.sysconfig
# Some of the lines have been superseded by other commits probably.
Patch0: quota-4.01-warnquota.patch
Patch2: quota-3.06-pie.patch
Patch3: quota-3.13-wrong-ports.patch
# Submitted to upstream, SF#3571486
Patch4: quota-4.01-Make-group-warning-message-more-official.patch
# In upstream after 4.01, SF#3571589
Patch5: quota-4.01-define_charset_in_mail.patch
# In upstream after 4.01, SF#3602786, bug #846296
Patch6: quota-4.01-Do-not-fiddle-with-quota-files-on-XFS-and-GFS.patch
# In upstream after 4.01, SF#3602777
Patch7: quota-4.01-quotacheck-Make-sure-d-provides-at-least-as-much-inf.patch
# In upstream after 4.01, SF#3607034
Patch8: quota-4.01-Add-quotasync-1-manual-page.patch
# In upstream after 4.01, SF#3607034
Patch9: quota-4.01-Complete-quotasync-usage.patch
# In upstream after 4.01, SF#3607034
Patch10: quota-4.01-Correct-quotasync-exit-code.patch
# In upstream after 4.01, SF#3607038
Patch11: quota-4.01-Fix-various-usage-mistakes.patch
# In upstream after 4.01, SF#3600120
Patch12: quota-4.01-Recognize-units-at-block-limits-by-setquota.patch
# In upstream after 4.01, SF#3600120
Patch13: quota-4.01-Recognize-block-limit-units-on-setquota-standard-inp.patch
# In upstream after 4.01, SF#3600120
Patch14: quota-4.01-Recognize-units-at-block-limits-by-edquota.patch
# In upstream after 4.01, SF#3600120
Patch15: quota-4.01-Recognize-units-at-inode-limits-by-setquota.patch
# In upstream after 4.01, SF#3600120
Patch16: quota-4.01-Recognize-units-at-inode-limits-by-edquota.patch
# In upstream after 4.01
Patch17: quota-4.01-Close-FILE-handles-on-error.patch
# In upstream after 4.01
Patch18: quota-4.01-Remove-installation-of-manpages-into-section-2.patch
# In upstream after 4.01, <https://sourceforge.net/p/linuxquota/patches/39/>
Patch19: quota-4.01-Add-quotagrpadmins-5-manual-page.patch
# In upstream after 4.01, <https://sourceforge.net/p/linuxquota/patches/39/>
Patch20: quota-4.01-Add-quotatab-5-manual-page.patch
# In upstream after 4.01, <https://sourceforge.net/p/linuxquota/patches/39/>
Patch21: quota-4.01-Add-warnquota.conf-5-manual-page.patch
# In upstream after 4.01, <https://sourceforge.net/p/linuxquota/patches/39/>
Patch22: quota-4.01-Improve-rcp.rquota-8-manual-page.patch
# Proposed to upstream, <https://sourceforge.net/p/linuxquota/bugs/115/>,
# bug #1072769
Patch23: quota-4.01-Prevent-from-grace-period-overflow-in-RPC-transport.patch

%description
The quota package contains system administration tools for monitoring
and limiting user and or group disk usage per file system.

%description
The quota package contains system administration tools for monitoring
and limiting user and or group disk usage per file system.


%package nld
Summary: quota_nld daemon
Group: System Environment/Base
Requires: initscripts
Requires: quota-nls = %{version}-%{release}
Requires(post): chkconfig
Requires(preun): chkconfig initscripts

%description nld
Daemon that listens on netlink socket and processes received quota warnings.
Note, that you have to enable the kernel support for sending quota messages
over netlink (in Filesystems->Quota menu). The daemon supports forwarding
warning messages to the system D-Bus (so that desktop manager can display
a dialog) and writing them to the terminal user has last accessed.


%package warnquota
Summary: Send e-mail to users over quota
Group: System Environment/Base
Requires: quota-nls = %{version}-%{release}

%description warnquota
Utility that checks disk quota for each local file system and mails a warning
message to those users who have reached their soft limit.  It is typically run
via cron(8).


%package nls
Summary: Gettext catalogs for disk quota tools
Group: System Environment/Base
BuildArch: noarch

%description nls
Disk quota tools messages translated into different natural languages.


%package devel
Summary: Development files for quota
Group: System Environment/Base
Requires: quota =  %{version}-%{release}

%description devel
The quota package contains system administration tools for monitoring
and limiting user and or group disk usage per file system.

This package contains development header files for implementing quotas
on remote machines.


%package doc
Summary: Additional documentation for disk quotas
Group: Documentation
Requires: quota =  %{version}-%{release}
BuildArch: noarch

%description doc
This package contains additional documentation for disk quotas concept in
Linux/UNIX environment.


%prep
%setup -q -n quota-tools
%patch0 -p1
%ifnarch ppc ppc64
%patch2 -p1
%endif
%patch3 -p1
%patch4 -p1 -b .group_warning
%patch5 -p1 -b .charset_in_mail
%patch6 -p1 -b .gfs_files
%patch7 -p1 -b .quotackeck_debug
%patch8 -p1 -b .quotasync_manual
%patch9 -p1 -b .quotasync_usage
%patch10 -p1 -b .quotasync_exit
%patch11 -p1 -b .usage
%patch12 -p1 -b .setquota_block_units
%patch13 -p1 -b .setquota_block_units_stdin
%patch14 -p1 -b .edquota_block_units
%patch15 -p1 -b .setquota_inode_units
%patch16 -p1 -b .edquota_inode_units
%patch17 -p1 -b .close_file_handles
%patch18 -p1 -b .remove_man2
%patch19 -p1 -b .doc_quotagrpadmins
%patch20 -p1 -b .doc_quotatab
%patch21 -p1 -b .doc_warnquota
%patch22 -p1 -b .doc_rquota
%patch23 -p1 -b .rpc_time

#fix typos/mistakes in localized documentation
for pofile in $(find ./po/*.p*)
do
   sed -i 's/editting/editing/' "$pofile"
done

# Fix charset
for F in Changelog; do
    iconv -f latin1 -t utf-8 <"$F" >"${F}.utf8"
    touch -r "$F"{,.utf8}
    mv "$F"{.utf8,}
done

# Accepted by upstream in quota-4.00_pre2
# Comment example /etc/quotatab to silent warnquota
sed -i -e '/^ *[^#]\{1,\}$/ s/^/#/' quotatab


%build
%global _hardened_build 1
%configure \
    --enable-ext2direct=yes \
    --enable-ldapmail=yes \
    --enable-netlink=yes \
    --enable-rootsbin=no \
%if %{rpcsetquota}
    --enable-rpcsetquota=yes \
%endif
    --enable-strip-binaries=no
make

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/{man1,man3,man5,man8}
make install INSTALL='install -p' ROOTDIR=%{buildroot}
install -m 644 warnquota.conf %{buildroot}%{_sysconfdir}
ln -s  quotaon.8.gz \
  %{buildroot}%{_mandir}/man8/quotaoff.8

install -p -m644 -D %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/quota_nld.service
install -p -m644 -D %{SOURCE2} \
    $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/quota_nld

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post nld
%systemd_post quota_nld.service

%preun nld
%systemd_preun quota_nld.service

%postun nld
%systemd_postun_with_restart quota_nld.service


%files
%attr(0755,root,root) %{_bindir}/*
%attr(0755,root,root) %{_sbindir}/*
%exclude %{_sbindir}/quota_nld
%exclude %{_sbindir}/warnquota
%attr(0644,root,root) %{_mandir}/man1/*
%attr(0644,root,root) %{_mandir}/man8/*
%exclude %{_mandir}/man8/quota_nld.8*
%exclude %{_mandir}/man8/warnquota.8*
%doc Changelog

%files nld
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/sysconfig/quota_nld
%{_unitdir}/quota_nld.service
%attr(0755,root,root) %{_sbindir}/quota_nld
%attr(0644,root,root) %{_mandir}/man8/quota_nld.8*
%doc Changelog

%files warnquota
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/quotagrpadmins
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/quotatab
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/warnquota.conf
%attr(0755,root,root) %{_sbindir}/warnquota
%attr(0644,root,root) %{_mandir}/man5/*
%attr(0644,root,root) %{_mandir}/man8/warnquota.8*
%doc Changelog README.ldap-support README.mailserver

%files nls -f %{name}.lang
%doc Changelog

%files devel
%dir %{_includedir}/rpcsvc
%{_includedir}/rpcsvc/*
%attr(0644,root,root) %{_mandir}/man3/*

%files doc
%doc doc/* ldap-scripts

%changelog
* Mon Mar 31 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.01-1m)
- update 4.01

* Sun Sep 11 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (4.00-0.0.1m)
- sync Fedora 1:4.00-0.17.pre1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.17-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.17-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.17-5m)
- full rebuild for mo7 release

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.17-4m)
- fix build with gcc-4.4.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.17-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.17-2m)
- remove duplicate directory

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.17-1m)
- sync with Fedora 11 (1:3.17-4), but we enable xfs

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.16-3m)
- rebuild against rpm-4.6

* Thu Oct  9 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.16-2m)
- fix quotaon's path

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16-1m)
- update to 3.16
- import new patch from Fedora and update some patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.14-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.14-2m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.14-1m)
- sync FC-devel 3.14-1
- update to 3.14

* Sun Jun  4 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.13-1m)
- update to 3.13

* Sat Mar 12 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (3.12-1m)
- sync with FC3

* Thu Feb  3 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.11-4m)
- enable x86_64.

* Wed Mar 31 2004 Toru Hoshina <t@momonga-linux.org>
- (3.11-3m)
- revised spec. rpc.rquotad belongs quota package.

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (3.11-2m)
- revised spec for rpm 4.2.

* Thu Feb 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.11-1m)
- update to 3.11
- add quotagrpadmins.sample
- add quotatab.sample

* Tue Mar 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.08-1m)
- version up to 3.08
- add URL
- move binary in /sbin to %%{_sbindir} (catch up changes of tarball)
- generalize listing quota.mo at files section, but now pl only

* Sun Aug  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.06-2m)
- remove ppc specific parts

* Sat Jul 20 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.06-1m)
- update to 3.06
- add some file to %files

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (3.05-2k)
- ver up.

* Fri Nov 16 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.01-8k)
- add documents

* Sun Nov  4 2001 Toru Hoshina <t@kondara.org>
- (3.01-6k)
- rebuild against new environment. because gcc 2.95.3 doesn't recognize
  -fno-merge-constants, use gcc_2_95_3 instead of gcc -V 2.95.3.

* Wed Oct 24 2001 Toru Hsohina <t@kondara.org>
- (3.01-4k)
- PPC needs -D__BYTEORDER_HAS_U64__ ?

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (3.01-2k)
- merge from Jirai.
- avoid conflict with nfs-utils.

* Thu Oct 04 2001 Motonobu Ichimura <famao@kondara.org>
- (3.01-3k)
- up to 3.01-final

* Thu Oct 04 2001 Motonobu Ichimura <famao@kondara.org>
- (3.01-0.000907k)
- xfs enabled

* Wed Jun 20 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0-2k)
- update to quota-2.00 release.
- changed to use "configure"
- RedHat patches is disabled (for merged to original tree)
- rework quota-misc.patch for rpm building
- fixed /usr/bin/vi to /bin/vi (quota-toriaezu.patch)

* Wed Apr 18 2001 Daiki Matsuda <dyky@df-usa.com>
- (2.0-0.0003004k)
- fixed for ppc to use gcc2.95.3

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jun 15 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.

* Wed May 10 2000 Jeff Johnson <jbj@redhat.com>
- apply patch5 (H.J. Lu)

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description
- man pages are compressed

* Tue Jan 18 2000 Preston Brown <pbrown@redhat.com>
- quota 2.00 series
- removed unnecessary patches

* Thu Aug  5 1999 Jeff Johnson <jbj@redhat.com>
- fix man page FUD (#4369).

* Thu May 13 1999 Peter Hanecak <hanecak@megaloman.sk>
- changes to allow non-root users to build too (Makefile patch, %attr)

* Tue Apr 13 1999 Jeff Johnson <jbj@redhat.com>
- fix for sparc64 quotas (#2147)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Mon Dec 28 1998 Cristian Gafton <gafton@redhat.com>
- don't install rpc.rquotad - we will use the one from the knfsd package
  instead

* Thu Dec 17 1998 Jeff Johnson <jbj@redhat.com>
- merge ultrapenguin 1.1.9 changes.

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- removed patch for mntent

* Fri Mar 27 1998 Jakub Jelinek <jj@ultra.linux.cz>
- updated to quota 1.66

* Tue Jan 13 1998 Erik Troan <ewt@redhat.com>
- builds rquotad
- installs rpc.rquotad.8 symlink

* Mon Oct 20 1997 Erik Troan <ewt@redhat.com>
- removed /usr/include/rpcsvc/* from filelist
- uses a buildroot and %attr

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Mar 25 1997 Erik Troan <ewt@redhat.com>
- Moved /usr/sbin/quota to /usr/bin/quota
