%global momorel 4

%global thunarver 1.5.0
%global major 0.8
%global xfce4ver 4.10.0

Name:           thunar-volman
Version:        0.8.0
Release:        %{momorel}m%{?dist}
Summary:        Automatic management of removable drives and media for the Thunar file manager

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://foo-projects.org/~benny/projects/thunar-volman/
Source0:        http://archive.xfce.org/src/xfce/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:         %{name}-%{version}-desktop-fix.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  desktop-file-utils
BuildRequires:  dbus-glib-devel >= 0.34
BuildRequires:  exo-devel >= 0.9.0
BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  libxfce4ui-devel >= %{xfce4ver}
BuildRequires:  libgudev1-devel >= 145
BuildRequires:  libnotify-devel >= 0.4.0
BuildRequires:  Thunar-devel >= %{thunarver}
BuildRequires:  xfconf >= %{xfce4ver}

Requires:       Thunar >= %{thunarver}

%description
The Thunar Volume Manager is an extension for the Thunar file manager, which 
enables automatic management of removable drives and media. For example, if 
thunar-volman is installed and configured properly, and you plug in your 
digital camera, it will automatically launch your preferred photo application 
and import the new pictures from the camera into your photo collection.

%prep
%setup -q
%patch0 -p1 -b .desktop-fix

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT transform='s,x,x,'
%find_lang %{name}

# revise dektop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-only-show-in XFCE \
  %{buildroot}%{_datadir}/applications/thunar-volman-settings.desktop

%post
touch --no-create %{_datadir}/icons/hicolor || :
#%%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
if [ "$1" -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
#%%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README THANKS
%{_bindir}/thunar-volman
%{_bindir}/thunar-volman-settings
%{_datadir}/icons/hicolor/48x48/apps/tvm-*.png
%{_datadir}/icons/hicolor/scalable/apps/tvm-*.svg
%{_datadir}/applications/thunar-volman-settings.desktop

%changelog
* Mon Oct 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-4m)
- BR: Thunar-devel >= 1.5.0
- BR: exo-devel >= 0.9.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.0-3m)
- rebuild against Thunar 1.5.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.0-2m)
- rebuild against Thunar 1.4.0

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0
- change Source0 URI

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-2m)
- remove BR hal-devel

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-1m)
- update
- rebuild against xfce4-4.8

* Mon May 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.80-8m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.80-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.80-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.80-5m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.80-4m)
- rebuild against xfce4-4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.80-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.80-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.80-1m)
- update
- rebuild against xfce4-4.6.0

* Tue Feb 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-4m)
- revise desktop file, add OnlyShowIn=XFCE; to thunar-volman-settings.desktop

* Wed Jan 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-3m)
- do not use libtiilize on configure

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0
- use NoSource

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-3m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-2m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-1m)
- import from fc-devel to Momonga
- add transform='s,x,x,' at install
 
* Sun Jan 21 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.2-1
- Update to 0.1.2.

* Wed Jan 17 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.0-1
- Initial packaging.
