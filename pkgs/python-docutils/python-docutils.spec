%global momorel 1
%global pythonver 2.7
%global python3ver 3.4

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global with_python3 1

%define srcname docutils

Name:           python-%{srcname}
Version:        0.11
Release:        %{momorel}m%{?dist}
Summary:        A system for processing plaintext documentation

Group:          Development/Languages
License:        "see COPYING.txt"
URL:            http://docutils.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/docutils/0.11/%{srcname}-%{version}.tar.gz
NoSource:       0
Patch100: 	disable-failing-tests.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= %{pythonver}
BuildRequires:  python-setuptools-devel
%if 0%{?with_python3}
BuildRequires:  python3-devel
BuildRequires:  python-tools
%endif

Requires: python-imaging
Provides: docutils = %{version}-%{release}
Obsoletes: docutils < %{version}-%{release}

%description
The Docutils project specifies a plaintext markup language, reStructuredText,
which is easy to read and quick to write.  The project includes a python
library to parse rST files and transform them into other useful formats such
as HTML, XML, and TeX as well as commandline tools that give the enduser
access to this functionality.

Currently, the library supports parsing rST that is in standalone files and
PEPs (Python Enhancement Proposals).  Work is underway to parse rST from
Python inline documentation modules and packages.

%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:        System for processing plaintext documentation for python3
Group:          Development/Languages
# This module is optional and not yet available for python3
# Requires: python3-imaging

%description -n python3-%{srcname}
The Docutils project specifies a plaintext markup language, reStructuredText,
which is easy to read and quick to write.  The project includes a python
library to parse rST files and transform them into other useful formats such
as HTML, XML, and TeX as well as commandline tools that give the enduser
access to this functionality.

Currently, the library supports parsing rST that is in standalone files and
PEPs (Python Enhancement Proposals).  Work is underway to parse rST from
Python inline documentation modules and packages.

This package contains the module, ported to run under python3.
%endif # with_python3

%prep
%setup -q -n %{srcname}-%{version}
%patch100 -p1 -b .disable-failing-tests

# Remove shebang from library files
for file in docutils/utils/{code_analyzer.py,punctuation_chars.py,error_reporting.py,smartquotes.py} docutils/utils/math/{latex2mathml.py,math2html.py} docutils/writers/xetex/__init__.py; do
sed -i -e '/#! *\/usr\/bin\/.*/{1D}' $file
done

iconv -f ISO88592 -t UTF8 tools/editors/emacs/IDEAS.rst > tmp
mv tmp tools/editors/emacs/IDEAS.rst

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}

CFLAGS="$RPM_OPT_FLAGS" %{__python3} setup.py build
popd
%endif # with_python3


%install
rm -rf %{buildroot}

# Must do the python3 install first because the scripts in /usr/bin are
# overwritten by setup.py install (and we want the python2 version to be the
# default for now).
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}

# docutils setup.py runs 2to3 on a copy of the tests and puts it in sitelib.
rm -rf %{buildroot}%{python3_sitelib}/test

# Flash file is used for testing docutils but shouldn't be in the installed package.
mv docs/user/rst/images/biohazard.swf ./biohazard.swf
popd

rm -rf %{buildroot}%{_bindir}/*
%endif # with_python3

%{__python} setup.py install --skip-build --root %{buildroot}

for file in %{buildroot}/%{_bindir}/*.py; do
    mv $file `dirname $file`/`basename $file .py`
done

# We want the licenses but don't need this build file
rm -f licenses/docutils.conf

# Flash file is used for testing docutils but shouldn't be in the installed package.
mv docs/user/rst/images/biohazard.swf ./biohazard.swf

%check
%if 0
mv  biohazard.swf docs/user/rst/images/biohazard.swf
python test/alltests.py
rm docs/user/rst/images/biohazard.swf

%if 0%{?with_python3}
pushd %{py3dir}
mv  biohazard.swf docs/user/rst/images/biohazard.swf
python3 test3/alltests.py
rm docs/user/rst/images/biohazard.swf
popd
%endif
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BUGS.txt COPYING.txt FAQ.txt HISTORY.txt README.txt RELEASE-NOTES.txt
%doc THANKS.txt licenses docs tools/editors
%{_bindir}/*
%{python_sitelib}/*

%if 0%{?with_python3}
%files -n python3-%{srcname}
%defattr(-,root,root,-)
%doc BUGS.txt COPYING.txt FAQ.txt HISTORY.txt README.txt RELEASE-NOTES.txt
%doc THANKS.txt licenses docs tools/editors
%{python3_sitelib}/*
%endif

%changelog
* Sat Jan 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-1m)
- update 0.11

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linunx.org>
- (0.7-1m)
- update to 0.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-1m)
- sync with Fedora 11 (0.5-3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-8m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4-7m)
- rebuild agaisst python-2.6.1-1

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-6m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4-5m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-4m)
- rebuild against gcc43

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4-3m)
- rebuild against python-2.5-9m

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-2m)
- rebuild against python-2.5

* Sun Jul 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4-1m)
- import from fedora extra
  
* Thu Feb 16 2006 Toshio Kuratomi <toshio-tiki-lounge.com> 0.4-2
- Bump and rebuild for FC5.
  
* Sun Jan 15 2006 Toshio Kuratomi <toshio-tiki-lounge.com> 0.4-1
- Update to 0.4.
- Scripted the listing of files in the python module.
- Add a missingok requirement on python-imaging as docutils can make use of
  it when converting to formats that have images.
  
* Tue Jun 7 2005 Toshio Kuratomi <toshio-tiki-lounge.com> 0.3.9-1
- Update to version 0.3.9.
- Use a dist tag as there aren't any differences between supported fc
  releases (FC3, FC4, devel.)

* Thu May 12 2005 Toshio Kuratomi <toshio-tiki-lounge.com> 0.3.7-7
- Bump version and rebuild to sync across architectures.

* Sun Mar 20 2005 Toshio Kuratomi <toshio-tiki-lounge.com> 0.3.7-6
- Rebuild for FC4t1

* Sat Mar 12 2005 Toshio Kuratomi <toshio.tiki-lounge.com> 0.3.7-5
- Add GPL as a license (mschwendt)
- Use versioned Obsoletes and Provides (mschwendt)

* Fri Mar 04 2005 Toshio Kuratomi <toshio.tiki-lounge.com> 0:0.3.7-4
- Rename to python-docutils per the new packaging guidelines.

* Wed Jan 12 2005 Toshio Kuratomi <toshio.tiki-lounge.com> 0:0.3.7-0.fdr.3
- Really install roman.py and build roman.py[co].  Needed to make sure I have
  docutils installed to test that it builds roman.py fine in that case.

* Tue Jan 11 2005 Toshio Kuratomi <toshio.tiki-lounge.com> 0:0.3.7-0.fdr.2
- Special case roman.py to always install.  This is the behaviour we want
  unless something else provides it.  Will need to watch out for this in
  future Core and Extras packages, but the auto detection code makes it
  possible that builds will not be reproducible if roman.py were installed
  from another package.... Lesser of two evils here.
- Provide python-docutils in case that package were preinstalled from
  another repository.
  
* Fri Dec 31 2004 Toshio Kuratomi <toshio.tiki-lounge.com> 0:0.3.7-0.fdr.1
- Update to 0.3.7
- Rename from python-docutils to docutils.
- Make roman.py optionally a part of the files list.  In FC2, this will be
  included.  In FC3, this won't.
- BuildConflict with self since the docutils build detects the presence
  of roman.py and doesn't reinstall itself.
  
* Mon Aug 9 2004 Toshio Kuratomi <toshio.tiki-lounge.com> 0:0.3.5-0.fdr.1
- Update to 0.3.5.
- Update spec style to latest fedora-rpmdevtools.
- Merge everything into a single package.  There isn't very much space
  advantage to having separate packages in a package this small and in
  this case, the documentation on using docutils as a library is also a
  good example of how to write in ReSructuredText.

* Sat Jan 10 2004 Michel Alexandre Salim <salimma[AT]users.sf.net> 0:0.3-0.fdr.1
- Initial RPM release.
