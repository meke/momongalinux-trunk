%global momorel 12

Summary: GNOME library for CUPS integration
Name: libgnomecups
Version: 0.2.3
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.2/%{name}-%{version}.tar.bz2
NoSource: 0
Patch3: libgnomecups-dbus.patch
Patch5: libgnomecups-0.1.14-go-direct.patch
# http://bugzilla.gnome.org/show_bug.cgi?id=520449
Patch6: libgnomecups-lpoptions.patch
Patch7: libgnomecups-glib-header.patch
Patch10: libgnomecups-0.2.3-cups-1.6.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel >= 0.9.8a
BuildRequires: cups-devel >= 1.6.1

%description
GNOME library for CUPS integration

%package devel
Summary: Libraries and header files for libgnomecups
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and header files for libgnomecups

%prep
%setup -q
%patch3 -p1 -b .dbus
%patch5 -p1 -b .go-direct
%patch6 -p1 -b .lpoptions
%patch7 -p1 -b .glib-header
%patch10 -p1 -b .cups16

%build
%configure CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog NEWS README
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%{_libdir}/*a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/libgnomecups-1
%{_datadir}/locale/*/*/*

%changelog
* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.3-12m)
- import patches from Fedora
- import cups-1.6 patch from Gentoo

* Sun Mar 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.3-11m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.3-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.3-7m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 18 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-4m)
- define libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.3-2m)
- rebuild against gcc43

* Sat Mar  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-4m)
- update to 0.2.3

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-4m)
- rebuild against gtk+-2.10.3
- delete libtool library

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-3m)
- rebuild against openssl-0.9.8a

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-2m)
- delete autoreconf and make check

* Thu Nov 17 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-1m)
- version up
- for GNOME 2.12.1 Desktop (libgnomeprint)

* Sun Dec  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.13-1m)
- initial import to Momonga
