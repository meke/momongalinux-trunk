%global momorel 6

%define lang he
%define langrelease 0
Summary: Hebrew dictionary for Aspell
Name: aspell-%{lang}
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell6-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Buildrequires: aspell >= 0.60
Requires: aspell >= 0.60
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for Hebrew.

# Note that this package, like other aspell's language packs, does not come up
# cleanly through rpmlint, but with the following errors:
# E: aspell-he no-binary
# E: aspell-he only-non-binary-in-usr-lib
# This is because the package contains only data files which sit under /usr/lib.
# They have to stay there, as they are architecture-dependent (due to
# byte-ordering issues).

%prep
%setup -q -n aspell6-%{lang}-%{version}-%{langrelease}

%build
./configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING Copyright
%{_libdir}/aspell-0.60/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0-3
- Autorebuild for GCC 4.3

* Sun Apr  1 2007 Dan Kenigsberg <danken@cs.technion.ac.il> 1.0-3
- Clean spec file according to lessons from Bug #212974.
* Sat Sep 16 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 1.0-2
- Rebuild for Fedora Extras 6
* Mon May 15 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 1.0-1
- new Hspell version.
* Sun Apr 23 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.9-2
- Cleaned the spec file according to Bug 189157.
- Added a comment explaining why the rpmlint gives Errors for this package.
* Sun Apr 16 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.9-1
- Stolen from the French spec file 50:0.50-9.2.1
