%global momorel 1

%define libgnomeui_version 2.9.1
%define krb5_version 1.7.1
%define libnm_version 0.8.999
%define dbus_version 0.90

Summary: Kerberos 5 authentication dialog
Name: krb5-auth-dialog
Version: 3.2.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: User Interface/X
URL: https://honk.sigxcpu.org/piki/projects/krb5-auth-dialog/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/3.2/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: krb5-auth-dialog-autostart.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgnomeui-devel >= %{libgnomeui_version}
BuildRequires: krb5-devel >= %{krb5_version}
BuildRequires: dbus-devel >= %{dbus_version}
BuildRequires: perl-XML-Parser, gettext
BuildRequires: intltool
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: gnome-control-center-devel
%ifnarch s390 s390x
BuildRequires: NetworkManager-glib-devel >= %{libnm_version}
%endif
Requires: libgnomeui >= %{libgnomeui_version}
Requires: krb5-libs >= %{krb5_version}
Requires(pre): GConf2
Requires(post): GConf2
Requires(preun): GConf2

%description
This package contains a dialog that warns the user when their Kerberos
tickets are about to expire and lets them renew them.

%prep
%setup -q
%patch0 -p1 -b .autostart

%build
export LDFLAGS=-lcap
%configure --disable-static
make %{?_smp_mflags}

%install
%{__rm} -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
%find_lang %name

desktop-file-install --add-only-show-in=GNOME \
	--dir=%{buildroot}%{_sysconfdir}/xdg/autostart \
	%{buildroot}%{_sysconfdir}/xdg/autostart/krb5-auth-dialog.desktop
desktop-file-validate \
	%{buildroot}%{_sysconfdir}/xdg/autostart/krb5-auth-dialog.desktop

# Momonga hack
cp	%{buildroot}%{_sysconfdir}/xdg/autostart/krb5-auth-dialog.desktop \
	%{buildroot}%{_datadir}/applications/krb5-auth-dialog.desktop

rm -f %{buildroot}%{_datadir}/icons/hicolor/icon-theme.cache
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
        %{_sysconfdir}/gconf/schemas/krb5-auth-dialog.schemas >/dev/null || :

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/krb5-auth-dialog.schemas &> /dev/null || :
fi

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/krb5-auth-dialog.schemas &> /dev/null || :
fi

%files -f %name.lang
%defattr(-,root,root,-)
%doc
%{_sysconfdir}/gconf/schemas/*.schemas
%{_bindir}/krb5-auth-dialog*
%{_libdir}/%{name}
%{_datadir}/krb5-auth-dialog/
%{_datadir}/icons/*/*/*/*
%{_mandir}/man1/*
%{_sysconfdir}/xdg/autostart/krb5-auth-dialog.desktop
%{_datadir}/applications/krb5-auth-dialog.desktop
%{_datadir}/dbus-1/services/org.gnome.KrbAuthDialog.service
%{_datadir}/gnome/help/krb5-auth-dialog/*
%{_datadir}/omf/krb5-auth-dialog/*

%changelog
* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Tue Oct 18 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-2m)
- desktop hack for KDE

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-5m)
- import fedora patch

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-4m)
- add OnlyShowIn=GNOME; to autostart file

* Mon May  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-3m)
- add BuildRequires

* Sun May  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-2m)
- rebuild against NetworkManager-0.8.999

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17-3m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15-3m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-2m)
- remove duplicate directories

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14-1m)
- update 0.14

* Fri Feb 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-3m)
- fix build failure

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-1m)
- sync with Fedora 11 (0.9.1-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-2m)
- rebuild against rpm-4.6

* Wed May  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7-1m)
- import from Fedora

* Mon Feb 18 2008 Christopher Aillon <caillon@redhat.com> - 0.7-7
- Rebuild to celebrate my birthday (and GCC 4.3)

* Thu Nov  1 2007 Matthias Clasen <mclasen@redhat.com> - 0.7-6
- Fix the Comment field in the desktop file (#344351)

* Mon Oct 22 2007 Christopher Aillon <caillon@redhat.com> - 0.7-5
- Don't start multiple times in KDE (#344991)

* Fri Aug 24 2007 Adam Jackson <ajax@redhat.com> - 0.7-4
- Rebuild for build ID

* Mon Aug 13 2007 Christopher Aillon <caillon@redhat.com> 0.7-3
- Update the license tag

* Thu Mar 15 2007 Karsten Hopp <karsten@redhat.com> 0.7-2
- rebuild with current gtk2 to add png support (#232013)

* Mon Jul 24 2006 Christopher Aillon <caillon@redhat.com> - 0.7-1
- Update to 0.7
- Don't peg the network and CPU when the KDC is unavailable

* Wed Jul 19 2006 John (J5) Palmieri <johnp@redhat.com> - 0.6.cvs20060212-4
- rebuild for dbus 

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.6.cvs20060212-3.1
- rebuild

* Sat Jun 24 2006 Jesse Keating <jkeating@redhat.com> - 0.6.cvs20060212-3
- Add missing BRs perl-XML-Parser, gettext
- Work around no network manager stuff on z900s

* Sun Feb 12 2006 Christopher Aillon <caillon@redhat.com> - 0.6.cvs20060212-1
- Update to latest CVS to get some of Nalin's fixes

* Tue Feb  7 2006 Jesse Keating <jkeating@redhat.com> - 0.6-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 31 2006 Christopher Aillon <caillon@redhat.com> 0.6-1
- Update to 0.6, adding an autostart file

* Fri Dec  9 2005 Jesse Keating <jkeating@redhat.com> - 0.5-2.1
- rebuilt

* Thu Dec  1 2005 John (J5) Palmieri <johnp@redhat.com> - 0.5-2
- rebuild for new dbus

* Tue Nov  8 2005 Christopher Aillon <caillon@redhat.com> 0.5-1
- Update to 0.5

* Tue Nov  1 2005 Christopher Aillon <caillon@redhat.com> 0.4-1
- Update to 0.4

* Mon Oct 31 2005 Christopher Aillon <caillon@redhat.com> 0.3-1
- Update to 0.3, working with newer versions of krb5 and NetworkManager

* Tue Aug 16 2005 David Zeuthen <davidz@redhat.com>
- Rebuilt

* Tue Mar 22 2005 Nalin Dahyabhai <nalin@redhat.com> 0.2-5
- Change Requires: krb5 to krb5-libs, repeat $ -> % fix for build requirements.

* Tue Mar 22 2005 Dan Williams <dcbw@redhat.com> 0.2-4
- Fix $ -> % for Requires: krb5 >= ...

* Mon Mar 21 2005 David Zeuthen <davidz@redhat.com> 0.2-3
- Fix up BuildRequires and Requires (#134704)

* Fri Mar  4 2005 David Zeuthen <davidz@redhat.com> 0.2-2
- Rebuild

* Mon Aug 16 2004 GNOME <jrb@redhat.com> - auth-dialog
- Initial build.

