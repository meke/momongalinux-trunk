%global momorel 15
%global qtver 4.8.4

Summary: A graphical tool for creating and tone-mapping HDR images
Name: qtpfsgui
Version: 1.9.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://qtpfsgui.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-hdr-save.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dcraw
Requires: hugin-base
BuildRequires: qt-devel >= %{qtver}
BuildRequires: ImageMagick
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: exiv2-devel >= 0.23
BuildRequires: fftw-devel
BuildRequires: ilmbase >= 1.0.3
BuildRequires: libjpeg-devel
BuildRequires: libtiff-devel >= 4.0.1

%description
Qtpfsgui is a graphical program for assembling bracketed photos into High
Dynamic Range (HDR) images.  It also provides a number of tone-mapping
operators for creating low dynamic range versions of HDR images.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja
%patch1 -p1 -b .enable-save

%build
qmake-qt4 PREFIX=%{_prefix}

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

# install and link icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,48x48,64x64,128x128}/apps
convert -scale 16x16 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 22x22 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
convert -scale 48x48 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
convert -scale 64x64 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
convert -scale 128x128 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS Changelog INSTALL LICENSE README TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-15m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-14m)
- rebuild against exiv2-0.23

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-13m)
- rebuild against libtiff-4.0.1

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-12m)
- rebuild against exiv2-0.22

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-11m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-10m)
- rebuild against exiv2-0.21.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-9m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-8m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.3-7m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-6m)
- rebuild against qt-4.6.3-1m

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.3-5m)
- rebuild against exiv2-0.20

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.3-4m)
- touch up spec file

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-3m)
- rebuild against exiv2-0.19

* Sun Dec  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.3-2m)
- Requires: hugin-base for "Auto Align Images"

* Thu Dec  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.3-1m)
- initial package for photo freaks using Momonga Linux
- import hdr-save.patch from gentoo
 +- 23 May 2009; Markus Meier <maekke@gentoo.org> qtpfsgui-1.9.3.ebuild,
 +- +files/qtpfsgui-1.9.3-hdr-save.patch:
 +- fix saving of hdr files
