%global momorel 9
%global date 20070304

# The data in MeCab dic are compiled by arch-dependent binaries
# and the created data are arch-dependent.
# However, this package does not contain any executable binaries
# so debuginfo rpm is not created.
%define debug_package %{nil}

Name: mecab-jumandic
Version: 5.1
Release: 0.%{date}.%{momorel}m%{?dist}
Summary: JUMAN dictorionary for MeCab
Group: Applications/Text
License: BSD
URL: http://mecab.sourceforge.net/
Source0: http://downloads.sourceforge.net/mecab/%{name}-%{version}-%{date}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mecab-devel
Requires: mecab

%description
MeCab JUMAN is a dictionary for MeCab using CRF estimation
based on Kyoto corpus.

%prep
%setup -q -n %{name}-%{version}-%{date}

%build
%configure --with-charset=utf8 --with-mecab-config=%{_bindir}/mecab-config
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%make dicdir=%{buildroot}%{_libdir}/mecab/dic/jumandic install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
# Note: post should be okay. mecab-dic expects that
# mecab is installed in advance.
if test -f %{_sysconfdir}/mecabrc ; then
	sed -i -e 's|^dicdir.*|dicdir = %{_libdir}/mecab/dic/jumandic|' \
		%{_sysconfdir}/mecabrc || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/mecab/dic/jumandic/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1-0.20070304.9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1-0.20070304.8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1-0.20070304.7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1-0.20070304.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1-0.20070304.5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1-0.20070304.4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1-0.20070304.3m)
- %%NoSource -> NoSource

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1-0.20070304.2m)
- set dictionary position to mecabrc

* Thu Oct  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1-0.20070304.1m)
- initial packaging
