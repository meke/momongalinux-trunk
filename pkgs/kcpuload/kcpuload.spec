%global momorel 13
%global date 20061018
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Small applet which displays cpu time for KDE kicker
Name: kcpuload
Version: 1.99
Release: 0.%{date}.%{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
URL: http://extragear.kde.org/apps/kcpuload/
Source0: %{name}-%{version}-%{date}.tar.bz2
Source1: admin.tar.bz2
Patch0: %{name}-%{version}-%{date}-automake111.patch
Patch1: %{name}-%{version}-%{date}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool

%description
This is a small program for the KDE Kicker (the startmenu).
It will show the CPU usage in form of one or two configurable diagrams.
There are settings for colors and some different styles.
Note that it isn't the real system load that's shown, but the percent
used of the total CPU power, which is calculated from the number of
CPU ticks.

To bring up the settings menu, simply rightclick on the diagram.
Use a slower update interval to get a more stable and accurate
diagram without flicker.

If there are more than one CPU on the system, the total CPU usage from
both can be shown, or you can choose to have one separate diagram for
each of the CPU's.
Leftclick on the diagram to bring up a small information box which will
show the exact CPU usage in text form.

%prep
%setup -q -n %{name}

# fix for new automake
rm -rf admin
tar xf %{SOURCE1}

%patch0 -p1 -b .automake111
%patch1 -p1 -b .autoconf265

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--with-xinerama \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99-0.20061018.13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99-0.20061018.12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99-0.20061018.11m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20061018.10m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20061018.9m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99-0.20061018.8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20061018.7m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99-0.20061018.6m)
- rebuild against rpm-4.6

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20061018.5m)
- revise %%{_docdir}/HTML/*/kcpuload/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.99-0.20061018.4m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99-0.20061018.3m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.99-0.20061018.2m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Wed Oct 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20061018.1m)
- update to 20061018 SVN snapshot for new autoconf

* Sun Jun 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20060625.1m)
- update to 20060625 SVN snapshot
- include all translations
- remove all patches

* Tue Jun 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20060613.1m)
- update to 20060613 SVN snapshot
- update kcpuload-ja.po
- remove merged manycpus.diff

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20051208.2m)
- add --enable-new-ldflags to configure

* Thu Dec  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-0.20051208.1m)
- update to 20051208 SVN snapshot
 - 05-03-25  * Fixed SMP support under Linux 2.6 kernels.
- import index.docbook and make-doc.patch from SVN (kde.org)
- apply manycpus.diff for more than 2 CPUs
  http://lists.kde.org/?l=kde-extra-gear&m=113356046525626&w=2
- update make-ja.patch
- remove desktop-file-utils from BuildPreReq

* Wed Dec  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-6m)
- import kcpuload-ja.po from SVN (kde.org)

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-5m)
- rebuild against KDE 3.5 RC1

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-4m)
- add --disable-rpath to configure

* Wed Feb 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.99-3m)
- enable x86_64.

* Thu Feb 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-2m)
- change URL

* Sat Oct  2 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-1m)
- update to 1.99
- no NoSource: 0
- BuildPreReq: gcc-c++, desktop-file-utils

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.1-6m)
- s/Copyright:/License:/

* Sat Mar 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.1-5m)
- rebuild against for XFree86-4.3.0

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.9.1-4m)
- rebuild against kde-3.0.5a.

* Fri Oct 11 2002 Shinji Takei <stakei@nga.jp>
- (1.9.1-3m)
- modified define kdever 3.0.4

* Sat Oct  5 2002 YAMAZAKI Makoto <uomaster@nifty.com> 
- (1.9.1-2m) 
- kdever to 3.0.3 
- add BuildPreReq: qt-devel = %{qtver} 
- add BuildPreReq: kdelibs-devel = %{kdever} 

* Wed Aug 07 2002 Shinji Takei <stakei@nga.jp>
- (1.9.1-1m)
- - first release.
