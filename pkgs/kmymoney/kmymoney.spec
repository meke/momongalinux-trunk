%global        momorel 7
%global        qtver 4.8.5
%global        kdever 4.12.1
%global        kdelibsrel 1m
%global        srcname %{name}2
%global        srcdirver 4.6.3

Summary:       Personal finance manager for KDE
Name:          kmymoney
Version:       4.6.3
Release:       %{momorel}m%{?dist}
License:       GPLv2
Group:         Applications/Productivity
URL:           http://kmymoney2.sourceforge.net/
Source0:       http://dl.sourceforge.net/project/%{srcname}/KMyMoney-KDE4/%{srcdirver}/%{name}-%{version}.tar.bz2
NoSource:      0
Patch0:        %{name}-%{version}-desktop.patch
Patch10:       0020-Fix-build-with-GMP-5.1.0.patch
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils
Requires(postun): coreutils
Requires(postun): desktop-file-utils
Requires(postun): gtk2
Requires(postun): shared-mime-info
Requires(posttrans): desktop-file-utils
Requires(posttrans): gtk2
Requires(posttrans): shared-mime-info
Requires:      %{name}-libs = %{version}-%{release}
Requires:      kdelibs >= %{kdever}-%{kdelibsrel}
Requires:      sqlite
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: aqbanking-devel >= 5.0.25
BuildRequires: cppunit-devel
BuildRequires: coreutils
BuildRequires: curl-devel
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: ghostscript
BuildRequires: gwenhywfar-devel >= 3.11.3
BuildRequires: gzip
BuildRequires: html2ps
BuildRequires: calligra-kdchart-devel >= 2.7.91
BuildRequires: libalkimia-devel
BuildRequires: libofx-devel >= 0.9.5
BuildRequires: libxml2-devel
BuildRequires: openjade
BuildRequires: sqlite-devel
Provides:      %{name}2 = %{version}-%{release}

%description
KMyMoney is striving to be a full-featured replacement for your
Windows-based finance software.  We are a full double-entry accounting
software package, for personal or small-business use.

%package libs
Summary:       Shared runtime libraries of kmymoney
Group:         System Environment/Libraries
Provides:      %{name}2-libs = %{version}-%{release}

%description libs
This package contains shared runtime libraries of kmymoney.

%package devel
Summary:       Development files for kmymoney
Group:         Development/Libraries
Requires:      %{name} = %{version}-%{release}
Provides:      %{name}2-devel = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on kmymoney.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja
%patch10 -p1 -b .build-fix

## kdchart munging begin
sed -i.kdchart -e 's|ADD_SUBDIRECTORY( libkdchart )|#ADD_SUBDIRECTORY( libkdchart )|' CMakeLists.txt
sed -i.kdchart -e 's|kmm_kdchart|calligrakdchart|' kmymoney/CMakeLists.txt
sed -i.kdchart -e 's|kmm_kdchart|calligrakdchart|' kmymoney/reports/CMakeLists.txt
mv libkdchart libkdchart.kdchart
mkdir libkdchart
ln -s %{_kde4_includedir}/KDChart libkdchart/include
ln -s %{_kde4_includedir}/kdchart libkdchart/kdchart
## kdchart munging end

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/oxygen/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
touch --no-create %{_kde4_iconsdir}/locolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/locolor &> /dev/null
    touch --no-create %{_datadir}/icons/hicolor &> /dev/null
    gtk-update-icon-cache %{_datadir}/icons/locolor &> /dev/null || :
    gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
    update-desktop-database -q &> /dev/null ||:
    update-mime-database %{_kde4_datadir}/mime &> /dev/null ||:
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/locolor &> /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
update-desktop-database -q &> /dev/null ||:
update-mime-database %{_kde4_datadir}/mime &> /dev/null ||:

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS BUGS COPYING ChangeLog* INSTALL README* TODO
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/*kmm*.so
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/kmm_csvimport
%{_kde4_appsdir}/kmm_icalendarexport
%{_kde4_appsdir}/kmm_kbanking
%{_kde4_appsdir}/kmm_ofximport
%{_kde4_appsdir}/kmm_printcheck
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/config/csvimporterrc
%{_kde4_datadir}/config.kcfg/%{name}.kcfg
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/*/kmy*.png
%{_kde4_iconsdir}/locolor/*/*/kmymoney.png
%{_kde4_datadir}/kde4/services/*kmm*.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}*.desktop
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}.1*
%{_kde4_datadir}/mime/packages/x-%{name}.xml
%{_datadir}/pixmaps/%{name}.png

%files libs
%defattr(-, root, root)
%{_kde4_libdir}/libkmm_*.so.*

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/libkmm_*.so

%changelog
* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.3-7m)
- rebuild against calligra-kdchart-2.7.91

* Sun Jun 23 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.3-6m)
- rebuild against calligra-kdchart-2.6.92

* Sat May  4 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.3-5m)
- import GMP.patch from fedora to enable build

* Sat May  4 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.3-4m)
- rebuild against calligra-kdchart-2.6.90

* Thu Feb 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-3m)
- rebuild with libofx-0.9.5

* Mon Dec 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.3-2m)
- rebuild against calligra-kdchart-2.5.92

* Thu Sep 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to 4.6.3

* Mon Sep 17 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.2-4m)
- rebuild against aqbanking-5.0.25

* Sat Aug 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.2-3m)
- rebuild against calligra-2.5.0

* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-2m)
- rebuild against calligra-2.4.3

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to 4.6.2

* Tue Jan  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-3m)
- rebuild against aqbanking-5.0.21
- remove BR: q4banking-devel

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-2m)
- correct %%post script

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to 4.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.3-2m)
- rebuild for new GCC 4.6

* Tue Feb 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to 4.5.3

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to 4.5.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.1-2m)
- rebuild for new GCC 4.5

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to 4.5.1

* Mon Sep 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5-2m)
- add BuildRequires

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5-1m)
- update to 4.5
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.98.1-7m)
- full rebuild for mo7 release

* Sun Aug 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.98.1-6m)
- fix up desktop file
- install extra icon for DE/WMs

* Mon Aug  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.98.1-5m)
- build with aqbanking-4.2.4

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.98.1-4m)
- split package libs

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.98.1-3m)
- Provides kmymoney2 and kmymoney2-devel

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.98.1-2m)
- rebuild against qt-4.6.3-1m

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.98.1-1m)
- version 3.98.1

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.98.0-1m)
- version 3.98.0

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.97.2-1m)
- version 3.97.2

* Sun Feb 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.96.1-1m)
- version 3.96.1 for KDE4

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- version 1.0.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- version 1.0.2

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-2m)
- rebuild against libjpeg-7

* Sat Sep 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- version 1.0.1
- revive configure option --enable-pdf-docs

* Fri Aug 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-2m)
- disable parallel make, make -j16 fails

* Fri Aug 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-1m)
- version 1.0.0
- remove --enable-pdf-docs from configure for the moment
- update desktop.patch

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.3-3m)
- rebuild against libofx-0.9.1
- remove BuildPreReq: kbanking-devel

* Sat Mar  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.3-2m)
- explicit --enable-sqlite3

* Thu Feb 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-1m)
- version 0.9.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-2m)
- rebuild against rpm-4.6

* Sat Sep 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-1m)
- version 0.9.2
- update desktop.patch
- import missing kmm_ofximport.rc from cvs

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-2m)
- rebuild against openssl-0.9.8h-1m

* Wed May 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-7m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.8-6m)
- rebuild against gcc43

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-5m)
- move headers to %%{_includedir}/kde

* Mon Feb 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-4m)
- add patch for gcc43, generated by gen43patch(v1)

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.8-2m)
- %%NoSource -> NoSource

* Sun Dec 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-1m)
- version 0.8.8
- License: GPLv2

* Thu Jul 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7-1m)
- version 0.8.7

* Mon Apr 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6-2m)
- enable OFX support

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6-1m)
- initial package for Momonga Linux
