%global momorel 16
%define debug_package %{nil}

%global DATE 20060404
%global _unpackaged_files_terminate_build 0
%global multilib_64_archs sparc64 ppc64 s390x x86_64
%ifarch s390x
%global multilib_32_arch s390
%endif
%ifarch sparc64
%global multilib_32_arch sparc
%endif
%ifarch ppc64
%global multilib_32_arch ppc
%endif
%ifarch x86_64
%global multilib_32_arch i386
%endif

Summary: Compatibility GNU Compiler Collection
Name: gcc3.4

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# ${HOME}/rpm/specopt/gcc3.4.specopt and edit it.
#
# and you should set in ~/.rpmmacros
# %_specoptdir %(echo $HOME)/rpm/specopt
###

%{?!do_test: %global do_test 1}
%{?!build_fortran: %global build_fortran 0}

Version: 3.4.6
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Languages
Source0: ftp://ftp.gnu.org/pub/gnu/gcc/gcc-3.4.6/gcc-3.4.6.tar.bz2 
NoSource: 0
Source1: dummylib.sh
URL: http://gcc.gnu.org
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Need .eh_frame ld optimizations
# Need proper visibility support
# Need -pie support
# Need --as-needed/--no-as-needed support
# Need .weakref support
BuildRequires: binutils >= 2.16.91.0.5-1
BuildRequires: zlib-devel, gettext, dejagnu, bison, flex, texinfo
# Make sure pthread.h doesn't contain __thread tokens
BuildRequires: glibc-devel >= 2.2.90-12
# Need .eh_frame ld optimizations
# Need proper visibility support
# Need -pie support
# Need .weakref support
Requires: binutils >= 2.16.91.0.5-1
# Make sure gdb will understand DW_FORM_strp
Conflicts: gdb < 5.1-2
Requires: glibc-devel >= 2.2.90-12
Requires: libgcc >= 4.1.0
BuildRequires: elfutils-devel >= 0.72
BuildRequires: momonga-rpmmacros >= 20090121-1m
%ifarch %{multilib_64_archs} sparc ppc
# Ensure glibc{,-devel} is installed for both multilib arches
BuildRequires: /lib/libc.so.6 /usr/lib/libc.so /lib64/libc.so.6 /usr/lib64/libc.so
%endif
%ifarch x86_64
BuildConflicts: libgcc(x86-32)
%endif
Obsoletes: compat-egcs
Obsoletes: compat-gcc
Obsoletes: compat-gcc-objc
Obsoletes: compat-egcs-objc
Obsoletes: compat-gcc-g77
Obsoletes: compat-egcs-g77
Obsoletes: compat-gcc-java
Obsoletes: compat-libgcj
Obsoletes: compat-libgcj-devel

Patch1000: gcc-3.4.6-redhat.patch.bz2

Patch1: gcc34-multi32-hack.patch
Patch2: gcc34-ice-hack.patch
Patch3: gcc-3.4.6-ppc64-m32-m64-multilib-only.patch
Patch4: gcc34-ia64-lib64.patch
Patch5: gcc34-java-nomulti.patch
Patch6: gcc34-gnuc-rh-release.patch
Patch7: gcc34-pr16104.patch
Patch8: gcc34-var-tracking-fix.patch
Patch9: gcc34-i386-movsi-insv.patch
Patch10: gcc34-pr18925.patch
Patch11: gcc34-pr14084.patch
Patch12: gcc34-hashtab-recursion.patch
Patch13: gcc34-java-jnilink.patch
Patch14: gcc34-pr21955.patch
Patch15: gcc34-vsb-stack.patch
Patch16: gcc34-pr18300.patch
Patch17: gcc34-rh156291.patch
Patch18: gcc34-weakref.patch
Patch19: gcc34-dwarf2-usefbreg.patch
Patch20: gcc34-dwarf2-prefer-1elt-vartracking.patch
Patch21: gcc34-dwarf2-pr20268.patch
Patch22: gcc34-dwarf2-inline-details.patch
Patch23: gcc-3.4.6-dwarf2-frame_base.patch
Patch24: gcc34-dwarf2-i386-multreg1.patch
Patch25: gcc-3.4.6-dwarf2-i386-multreg2.patch
Patch26: gcc-3.4.6-rh176182.patch
Patch27: gcc34-pr11953.patch
Patch28: gcc34-pr23591.patch
Patch29: gcc34-pr26208.patch
Patch30: gcc-3.4.6-pr8788.patch
Patch31: gcc34-rh137200.patch
Patch32: gcc34-rh172117.patch
Patch33: gcc34-rh172876.patch
Patch34: gcc34-rh178062.patch
Patch35: gcc34-pr21412.patch
Patch36: gcc34-sw2438.patch
Patch37: gcc34-pr26208-workaround.patch
Patch38: gcc34-libgcc_eh-hidden.patch
Patch39: gcc34-frame-base-loclist.patch
Patch40: gcc34-CVE-2006-3619.patch
Patch41: gcc34-dwarf2-inline-details-fix.patch
Patch42: gcc-3.4.6-CXXABI131.patch
Patch43: gcc34-rh205919.patch
Patch44: gcc34-rh207277.patch
Patch45: gcc34-var-tracking-coalesce.patch
Patch46: gcc34-java-zoneinfo.patch
Patch47: gcc34-libgcc-additions.patch

Patch100: gcc34-ldbl-hack.patch

%global _gnu %{nil}
%ifarch sparc
%global gcc_target_platform sparc64-%{_vendor}-linux
%endif
%ifarch ppc
%global gcc_target_platform ppc64-%{_vendor}-linux
%endif
%ifnarch sparc ppc
%global gcc_target_platform %{_target_cpu}-%{_vendor}-linux
%endif

%description
This package includes a GCC 3.4.6-RH compatibility compiler.

%package c++
Summary: C++ support for compatibility compiler
Group: Development/Languages
Requires: gcc3.4 = %{version}-%{release}
# Requires: libstdc++ >= 4.1.0, libstdc++ < 4.2.0
Requires: libstdc++ >= 4.1.0
Obsoletes: compat-egcs-c++
Obsoletes: compat-gcc-c++
Obsoletes: compat-libstdc++
Obsoletes: compat-libstdc++-devel
AutoProv: false

%description c++
This package includes a GCC 3.4.6-RH compatibility C++ compiler.

%if %{build_fortran}
%package g77
Summary: Fortran 77 support for compatibility compiler
Group: Development/Languages
Requires: gcc3.4 = %{version}-%{release}
Requires: compat-libf2c-34 = %{version}-%{release}
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info
Obsoletes: gcc3-g77
Obsoletes: gcc-g77
Autoreq: true

%description g77
The compat-gcc-34-g77 package provides support for compiling Fortran 77
programs with the GNU Compiler Collection.

%package -n libf2c
Summary: Fortran 77 compatibility runtime
Group: System Environment/Libraries
Autoreq: true
Obsoletes: libf2c, compat-libf2c-32

%description libf2c
This package contains Fortran 77 shared library which is needed to run
Fortran 77 dynamically linked programs.

%endif

%prep
print_specopt() {
## echo the specopt values
cat <<EOF
Configurations:
   do_test  .......... %{do_test}

EOF
}
print_specopt

%setup -q -n gcc-%{version}

%patch1000 -p0 -E

%ifarch sparc ppc
%patch1 -p0 -b .multi32-hack~
%endif
%patch2 -p0 -b .ice-hack~
%patch3 -p1 -b .ppc64-m32-m64-multilib-only~
%ifarch ia64
%if "%{_lib}" == "lib64"
%patch4 -p0 -b .ia64-lib64~
%endif
%endif
%patch5 -p0 -b .java-nomulti~
%patch6 -p0 -b .gnuc-rh-release~
%patch7 -p0 -b .pr16104~
%patch8 -p0 -b .var-tracking-fix~
%patch9 -p0 -b .i386-movsi-insv~
%patch10 -p0 -b .pr18925~
%patch11 -p0 -b .pr14084~
%patch12 -p0 -b .hashtab-recursion~
%patch13 -p0 -b .java-jnilink~
%patch14 -p0 -b .pr21955~
%patch15 -p0 -b .vsb-stack~
%patch16 -p0 -b .pr18300~
%patch17 -p0 -b .rh156291~
%patch18 -p0 -b .weakref~
%patch19 -p0 -b .dwarf2-usefbreg~
%patch20 -p0 -b .dwarf2-prefer-1elt-vartracking~
%patch21 -p0 -b .dwarf2-pr20268~
%patch22 -p0 -b .dwarf2-inline-details~
%patch23 -p1 -b .dwarf2-frame_base~
%patch24 -p0 -b .dwarf2-i386-multreg1~
%patch25 -p1 -b .dwarf2-i386-multreg2~
%patch26 -p1 -b .rh176182~
%patch27 -p0 -b .pr11953~
%patch28 -p0 -b .pr23591~
%patch29 -p0 -b .pr26208~
%patch30 -p1 -b .pr8788~
%patch31 -p0 -b .rh137200~
%patch32 -p0 -b .rh172117~
%patch33 -p0 -b .rh172876~
%patch34 -p0 -b .rh178062~
%patch35 -p0 -b .pr21412~
%patch36 -p0 -b .sw2438~
%patch37 -p0 -b .pr26208-workaround~
%patch38 -p0 -b .libgcc_eh-hidden~
%patch39 -p0 -b .frame-base-loclist~
%patch40 -p0 -b .CVE-2006-3619~
%patch41 -p0 -b .dwarf2-inline-details-fix~
%patch42 -p1 -b .CXXABI131~
%patch43 -p0 -b .rh205919~
%patch44 -p0 -b .rh207277~
%patch45 -p0 -b .var-tracking-coalesce~
%patch46 -p0 -b .java-zoneinfo~
%patch47 -p0 -b .libgcc-additions~

%patch100 -p0 -b .ldbl-hack~

perl -pi -e 's/3\.4\.7/3.4.6/' gcc/version.c
perl -pi -e 's/"%{version}"/"%{version} \(release\)"/' gcc/version.c
perl -pi -e 's/\((prerelease|experimental|release|Red Hat[^)]*)\)/\(Momonga Linux %{momonga} %{version}-%{release}\)/' gcc/version.c

# Misdesign in libstdc++
cp -a libstdc++-v3/config/cpu/i{4,3}86/atomicity.h

./contrib/gcc_update --touch

%build

rm -fr obj-%{gcc_target_platform}
mkdir obj-%{gcc_target_platform}
cd obj-%{gcc_target_platform}

if [ ! -f /usr/lib/locale/de_DE/LC_CTYPE ]; then
  mkdir locale
  localedef -f ISO-8859-1 -i de_DE locale/de_DE
  export LOCPATH=`pwd`/locale:/usr/lib/locale
fi

CC=gcc
OPT_FLAGS=`echo $RPM_OPT_FLAGS|sed -e 's/-fno-rtti//g' -e 's/-fno-exceptions//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-m64//g;s/-m32//g;s/-m31//g'`
%ifarch %{ix86}
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mtune=pentium4/-mtune=i686/g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mtune=generic/-mtune=i686/g'`
%endif
%ifarch x86_64
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mtune=nocona//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mtune=generic//g'`
%endif
%ifarch sparc sparc64
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mcpu=ultrasparc/-mtune=ultrasparc/g'`
%endif
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-Wall//g' -e 's/-Wp,-D_FORTIFY_SOURCE=2//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-fexceptions//g' -e 's/-fasynchronous-unwind-tables//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-fstack-protector//g' -e 's/--param=ssp-buffer-size=[0-9]*//g'`
%ifarch sparc64
cat > gcc64 <<"EOF"
#!/bin/sh
exec /usr/bin/gcc -m64 "$@"
EOF
chmod +x gcc64
CC=`pwd`/gcc64
%endif
%ifarch ppc64
if gcc -m64 -xc -S /dev/null -o - > /dev/null 2>&1; then
  cat > gcc64 <<"EOF"
#!/bin/sh
exec /usr/bin/gcc -m64 "$@"
EOF
  chmod +x gcc64
  CC=`pwd`/gcc64
fi
%endif
CC="$CC" CFLAGS="$OPT_FLAGS" CXXFLAGS="$OPT_FLAGS" XCFLAGS="$OPT_FLAGS" TCFLAGS="$OPT_FLAGS" \
	GCJFLAGS="$OPT_FLAGS" \
	../configure --prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
	--enable-shared --enable-threads=posix --disable-checking \
	--with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions \
%if %{build_fortran}
	--enable-languages=c,c++,f77 \
%else
	--enable-languages=c,c++ \
%endif
	--disable-libgcj \
%ifarch sparc
	--host=%{gcc_target_platform} --build=%{gcc_target_platform} --target=%{gcc_target_platform} --with-cpu=v7
%endif
%ifarch ppc
	--host=%{gcc_target_platform} --build=%{gcc_target_platform} --target=%{gcc_target_platform} --with-cpu=default32
%endif
%ifnarch sparc ppc
	--host=%{gcc_target_platform}
%endif

%ifarch %{ix86} x86_64
make %{?_smp_mflags} BOOT_CFLAGS="$OPT_FLAGS" profiledbootstrap
%else
make %{?_smp_mflags} BOOT_CFLAGS="$OPT_FLAGS" bootstrap-lean
%endif

# Fix up libstdc++.so's
d_first=yes
for d in `pwd`/%{gcc_target_platform}/libstdc++-v3 `pwd`/%{gcc_target_platform}/*/libstdc++-v3; do
  test -d $d || continue
  pushd $d/src
    sh %{SOURCE1} .libs/libstdc++.so .libs/ll.so libstdc++-symbol.ver
    rm .libs/libstdc++.so; cp .libs/ll.so .libs/libstdc++.so
    if [ x"$d_first" = xyes ]; then
      rm .libs/libstdc++.so.6
      libstdcxx_so=`readlink -f %{_prefix}/%{_lib}/libstdc++.so.6`
      cp -a $libstdcxx_so .libs/
      cd .libs; ln -sf `basename $libstdcxx_so` libstdc++.so.6; cd -
      d_first=no
    fi
  popd
done

# run the tests.
%if %{do_test}
make %{?_smp_mflags} -k check || :
echo ====================TESTING=========================
( ../contrib/test_summary || : ) 2>&1 | sed -n '/^cat.*EOF/,/^EOF/{/^cat.*EOF/d;/^EOF/d;/^LAST_UPDATED:/d;p;}'
echo ====================TESTING END=====================
%endif

%install
rm -fr $RPM_BUILD_ROOT

perl -pi -e \
  's~href="l(ibstdc|atest)~href="http://gcc.gnu.org/onlinedocs/libstdc++/l\1~' \
  libstdc++-v3/docs/html/documentation.html
ln -sf documentation.html libstdc++-v3/docs/html/index.html
find libstdc++-v3/docs/html -name CVS | xargs rm -rf

cd obj-%{gcc_target_platform}

if [ ! -f /usr/lib/locale/de_DE/LC_CTYPE ]; then
  export LOCPATH=`pwd`/locale:/usr/lib/locale
fi

TARGET_PLATFORM=%{gcc_target_platform}

make prefix=$RPM_BUILD_ROOT%{_prefix} mandir=$RPM_BUILD_ROOT%{_mandir} \
  infodir=$RPM_BUILD_ROOT%{_infodir} install

FULLPATH=$RPM_BUILD_ROOT%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}
FULLEPATH=$RPM_BUILD_ROOT%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{version}

cxxconfig="`find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h`"
for i in `find %{gcc_target_platform}/[36]*/libstdc++-v3/include -name c++config.h 2>/dev/null`; do
  if ! diff -up $cxxconfig $i; then
    cat > $RPM_BUILD_ROOT%{_prefix}/include/c++/%{version}/%{gcc_target_platform}/bits/c++config.h <<EOF
#ifndef _CPP_CPPCONFIG_WRAPPER
#define _CPP_CPPCONFIG_WRAPPER 1
#include <bits/wordsize.h>
#if __WORDSIZE == 32
%ifarch %{multilib_64_archs}
`cat $(find %{gcc_target_platform}/32/libstdc++-v3/include -name c++config.h)`
%else
`cat $(find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h)`
%endif
#else
%ifarch %{multilib_64_archs}
`cat $(find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h)`
%else
`cat $(find %{gcc_target_platform}/64/libstdc++-v3/include -name c++config.h)`
%endif
#endif
#endif
EOF
    break
  fi
done

mkdir -p $RPM_BUILD_ROOT/%{_lib}
mv -f $RPM_BUILD_ROOT%{_prefix}/%{_lib}/libgcc_s.so.1 $RPM_BUILD_ROOT/%{_lib}/libgcc_s-%{version}-%{DATE}.so.1
chmod 755 $RPM_BUILD_ROOT/%{_lib}/libgcc_s-%{version}-%{DATE}.so.1
ln -sf libgcc_s-%{version}-%{DATE}.so.1 $RPM_BUILD_ROOT/%{_lib}/libgcc_s.so.1
ln -sf /%{_lib}/libgcc_s.so.1 $FULLPATH/libgcc_s.so
%ifarch sparc ppc
ln -sf /lib64/libgcc_s.so.1 $FULLPATH/libgcc_s_64.so
%endif
%ifarch %{multilib_64_archs}
ln -sf /lib/libgcc_s.so.1 $FULLPATH/libgcc_s_32.so
%endif

for h in `find $FULLPATH/include -name \*.h`; do
  if grep -q 'It has been auto-edited by fixincludes from' $h; then
    rh=`grep -A2 'It has been auto-edited by fixincludes from' $h | tail -1 | sed 's|^.*"\(.*\)".*$|\1|'`
    diff -up $rh $h || :
    rm -f $h
  fi
done

cd ..

%ifarch ppc ppc64 s390 s390x
# GCC 3.4.x always uses IEEE double long double type on ppc*/s390*
# Although properly written programs should use headers which will DTRT,
# the addition of -lnldbl_nonshared should fix even buggy programs
# and shared libraries.
sed -i -e 's/%G %L/%G -lnldbl_nonshared %L/' \
  $RPM_BUILD_ROOT%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/specs
%endif

%ifarch sparc ppc
ln -f $RPM_BUILD_ROOT%{_prefix}/bin/%{gcc_target_platform}-gcc \
  $RPM_BUILD_ROOT%{_prefix}/bin/%{_target_platform}-gcc
%endif
%ifarch sparc64
ln -f $RPM_BUILD_ROOT%{_prefix}/bin/%{gcc_target_platform}-gcc \
  $RPM_BUILD_ROOT%{_prefix}/bin/sparc-%{_vendor}-%{_target_os}-gcc
%endif
%ifarch ppc64
ln -f $RPM_BUILD_ROOT%{_prefix}/bin/%{gcc_target_platform}-gcc \
  $RPM_BUILD_ROOT%{_prefix}/bin/ppc-%{_vendor}-%{_target_os}-gcc
%endif

for i in $RPM_BUILD_ROOT%{_prefix}/bin/{*gcc,*++,gcov}; do
  mv -f $i ${i}_3_4
done

%if %{build_fortran}
ln -sf g77 $RPM_BUILD_ROOT%{_prefix}/bin/f77
%endif
target_libdir=`pwd`/obj-%{gcc_target_platform}/%{gcc_target_platform}/
pushd $FULLPATH
%if %{build_fortran}
mv -f $RPM_BUILD_ROOT%{_prefix}/%{_lib}/libg2c.*a .
mv -f $RPM_BUILD_ROOT%{_prefix}/%{_lib}/libfrtbegin.*a .
%endif
mv -f $RPM_BUILD_ROOT%{_prefix}/%{_lib}/libstdc++.*a .
mv -f $RPM_BUILD_ROOT%{_prefix}/%{_lib}/libsupc++.*a .
mv -f $RPM_BUILD_ROOT%{_prefix}/%{_lib}/libstdc++_nonshared.*a .
cp -a $target_libdir/libstdc++-v3/src/.libs/ll.so libstdc++_shared.so
echo 'GROUP ( -lstdc++_nonshared -lstdc++_shared )' > libstdc++.so
%if %{build_fortran}
if [ "%{_lib}" = "lib" ]; then
ln -sf ../../../libg2c.so.0.* libg2c.so
else
ln -sf ../../../../%{_lib}/libg2c.so.0.* libg2c.so
fi
%ifarch sparc ppc
mkdir -p 64
ln -sf ../`echo ../../../../lib/libg2c.so.0.* | sed s~/lib/~/lib64/~` 64/libg2c.so
mv -f $RPM_BUILD_ROOT%{_prefix}/lib64/libg2c.*a 64/
mv -f $RPM_BUILD_ROOT%{_prefix}/lib64/libfrtbegin.*a 64/
%endif

mv -f $RPM_BUILD_ROOT%{_prefix}/lib64/libstdc++.*a 64/
mv -f $RPM_BUILD_ROOT%{_prefix}/lib64/libsupc++.*a 64/
mv -f $RPM_BUILD_ROOT%{_prefix}/lib64/libstdc++_nonshared.*a 64/
cp -a $target_libdir/64/libstdc++-v3/src/.libs/ll.so 64/libstdc++_shared.so
echo 'GROUP ( -lstdc++_nonshared -lstdc++_shared )' > 64/libstdc++.so
%endif
%ifarch %{multilib_64_archs}
mkdir -p 32
%if %{build_fortran}
ln -sf ../`echo ../../../../lib64/libg2c.so.0.* | sed s~/../lib64/~/~` 32/libg2c.so
mv -f $RPM_BUILD_ROOT%{_prefix}/lib/libg2c.*a 32/
mv -f $RPM_BUILD_ROOT%{_prefix}/lib/libfrtbegin.*a 32/
%endif
mv -f $RPM_BUILD_ROOT%{_prefix}/lib/libstdc++.*a 32/
mv -f $RPM_BUILD_ROOT%{_prefix}/lib/libsupc++.*a 32/
mv -f $RPM_BUILD_ROOT%{_prefix}/lib/libstdc++_nonshared.*a 32/
cp -a $target_libdir/32/libstdc++-v3/src/.libs/ll.so 32/libstdc++_shared.so
echo 'GROUP ( -lstdc++_nonshared -lstdc++_shared )' > 32/libstdc++.so
%endif
popd

%if %{build_fortran}
# Strip debug info from Fortran static libraries
strip -g `find . \( -name libg2c.a -o -name libfrtbegin.a \
		    -o -name libstdc++\*.a -o -name libsupc++.a \) -a -type f`
chmod 755 $RPM_BUILD_ROOT%{_prefix}/%{_lib}/libg2c.so.0.*
%endif

rm -f $RPM_BUILD_ROOT%{_prefix}/lib*/libiberty.a
rm -f $RPM_BUILD_ROOT/lib*/libgcc_s*
rm -f $RPM_BUILD_ROOT%{_prefix}/bin/cpp

%ifarch %{multilib_64_archs}
# Remove libraries for the other arch on multilib arches
rm -f $RPM_BUILD_ROOT%{_prefix}/lib/lib*.so*
rm -f $RPM_BUILD_ROOT%{_prefix}/lib/lib*.a
%else
%ifarch sparc ppc
rm -f $RPM_BUILD_ROOT%{_prefix}/lib64/lib*.so*
rm -f $RPM_BUILD_ROOT%{_prefix}/lib64/lib*.a
%endif
%endif

# libstdc++.so.6.0.x is included in libstdc++ package, not here.
rm -rf $RPM_BUILD_ROOT%{_prefix}/%{_lib}/libstdc++.so*

%clean
rm -rf $RPM_BUILD_ROOT

%if %{build_fortran}
%post g77
/sbin/install-info \
  --info-dir=%{_infodir} %{_infodir}/g77.info.gz || :

%preun g77
if [ $1 = 0 ]; then
  /sbin/install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/g77.info.gz || :
fi

%post libf2c -p /sbin/ldconfig

%postun -n libf2c -p /sbin/ldconfig
%endif

%files
%defattr(-,root,root)
%{_prefix}/bin/gcc_3_4
%{_prefix}/bin/gcov_3_4
%ifarch sparc ppc
%{_prefix}/bin/%{_target_platform}-gcc_3_4
%endif
%ifarch sparc64
%{_prefix}/bin/sparc-%{_vendor}-%{_target_os}-gcc_3_4
%endif
%ifarch ppc64
%{_prefix}/bin/ppc-%{_vendor}-%{_target_os}-gcc_3_4
%endif
%{_prefix}/bin/%{gcc_target_platform}-gcc_3_4
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{version}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/stddef.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/stdarg.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/varargs.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/float.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/limits.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/stdbool.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/iso646.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/syslimits.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/unwind.h
%ifarch %{ix86} x86_64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/mmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/xmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/emmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/pmmintrin.h
%endif
%ifarch ia64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/ia64intrin.h
%endif
%ifarch ppc ppc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/ppc-asm.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/altivec.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/spe.h
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/README
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{version}/collect2
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{version}/cc1
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libgcc_s.so
%ifarch sparc ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libgcc_s_64.so
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libgcc_s_32.so
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/specs
%doc gcc/README* gcc/*ChangeLog* gcc/COPYING*

%files c++
%defattr(-,root,root)
%{_prefix}/bin/%{gcc_target_platform}-*++_3_4
%{_prefix}/bin/g++_3_4
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{version}/cc1plus
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{version}/cc1plus
%ifarch sparc ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libstdc++.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libstdc++_shared.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libstdc++_nonshared.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libsupc++.a
%endif
%ifarch %{multilib_64_archs}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libstdc++.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libstdc++_shared.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libstdc++_nonshared.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libsupc++.a
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libstdc++.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libstdc++_shared.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libstdc++_nonshared.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libsupc++.a
%{_prefix}/include/c++/%{version}
%doc gcc/cp/ChangeLog*
%doc libstdc++-v3/ChangeLog* libstdc++-v3/README* libstdc++-v3/docs/html/

%if %{build_fortran}
%files g77
%defattr(-,root,root)
%{_prefix}/bin/g77
%{_prefix}/bin/f77
%{_mandir}/man1/g77.1*
%{_infodir}/g77*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{version}
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{version}/f771
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libfrtbegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libg2c.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/libg2c.so
%ifarch sparc ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libfrtbegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libg2c.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/64/libg2c.so
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libfrtbegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libg2c.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/32/libg2c.so
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{version}/include/g2c.h
%doc gcc/f/ChangeLog*

%files libf2c
%defattr(-,root,root)
%{_prefix}/%{_lib}/libg2c.so.0*

%endif

%changelog
* Tue Aug  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.6-16m)
- rebuild without libgcc.i686 on x86_64

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.6-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.6-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.6-13m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.6-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jan 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.6-11m)
- insert momonga version to gcc version string

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.6-10m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.6-9m)
- update Patch3,23,25,26,30,42 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.6-8m)
- rebuild against gcc43

* Mon Mar 31 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.6-7m)
- enable specopt

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.6-6m)
- %%NoSource -> NoSource

* Sat Nov 24 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.6-5m)
- revised libstdc++.so's fix-up

* Tue Oct 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.6-4m)
- remove %%{_prefix}/lib/gcc/%%{gcc_target_platform}/%%{version}/32 from gcc3.4-c++,
  it's already provided by gcc3.4 main package

* Sat Sep 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.6-3m)
- disable debug_package

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.6-2m)
- modify %%files

* Mon Jun 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.6-1m)
- initial commit
-- based Fedora 3.4.6-3
