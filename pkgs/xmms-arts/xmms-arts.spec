%global momorel 14
%global src_name arts_output

Name: xmms-arts
Summary: KDE aRtsd output plugin for XMMS
Version: 0.7.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://havardk.xmms.org/plugins/arts_output/
Source0: http://havardk.xmms.org/plugins/arts_output/%{src_name}-%{version}.tar.gz 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xmms >= 1.2.10-10m
Requires: arts
BuildRequires: gtk+1-devel arts-devel
BuildRequires: xmms-devel >= 1.2.10-10m

%description
xmms - aRts is an output plugin to allow XMMS playback through
the KDE soundserver, aRts.

%prep
%setup -q -n %{src_name}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING README NEWS INSTALL AUTHORS ChangeLog
%{_bindir}/xmms-arts-helper
%{_libdir}/xmms/Output/libarts.*

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-14m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-11m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-10m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.1-8m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-5m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.1-4m)
- delete libtool library

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.1-3m)
- change installdir (/usr/X11R6 -> /usr)

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.7.1-2m)
- enable x86_64.

* Mon May 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-1m)
- version 0.7.1

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.6.0-1m)
- change source.
- rebuild against xmms-1.2.8

* Sat Mar 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4-5m)
- rebuild against for XFree86-4.3.0

* Wed Mar 27 2002 Toru Hoshina <t@kondara.org>
- (0.4-4k)
- BuildPreReq arts-devel.

* Wed Oct 24 2001 Toru Hoshina <t@kondara.org>
- (0.4-2k)
- 1st release.
