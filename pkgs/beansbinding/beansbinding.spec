%global momorel 5

Name:           beansbinding
Version:        1.2.1
Release:        %{momorel}m%{?dist}
Summary:        Beans Binding (JSR 295) reference implementation

Group:          Development/Libraries
License:        LGPLv2+
URL:            https://beansbinding.dev.java.net/
Source0:        https://beansbinding.dev.java.net/files/documents/6779/73673/beansbinding-1.2.1-src.zip
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ant
BuildRequires:  ant-nodeps
BuildRequires:  ant-junit
BuildRequires:  java-devel >= 1.6.0
BuildRequires:  jpackage-utils

Requires:       java >= 1.6.0
Requires:       jpackage-utils

BuildArch:      noarch

%description
In essence, Beans Binding (JSR 295) is about keeping two properties 
(typically of two objects) in sync. An additional emphasis is placed 
on the ability to bind to Swing components, and easy integration with 
IDEs such as NetBeans. This project provides the reference implementation.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -c -n %{name}-%{version}
# remove all binary libs
find . -type f \( -iname "*.jar" -o -iname "*.zip" \) -print0 | xargs -t -0 %{__rm} -f

%build
%{ant} dist

%install
%{__rm} -rf %{buildroot}
# jar
%{__install} -d -m 755 %{buildroot}%{_javadir}
%{__install} -m 644 dist/%{name}.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
%{__ln_s} %{name}-%{version}.jar %{buildroot}%{_javadir}/%{name}.jar
# javadoc
%{__install} -d -m 755 %{buildroot}%{_javadocdir}/%{name}-%{version}
%{__cp} -pr dist/javadoc/* %{buildroot}%{_javadocdir}/%{name}-%{version}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_javadir}/*
%doc license.txt releaseNotes.txt

%files javadoc
%defattr(-,root,root,-)
%dir %{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}-%{version}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-1m)
- import from Fedora 11

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Aug 20 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 1.2.1-3
- Canonical using of %%{__rm}
- The %%{ant} macro is used instead of the ant command
- Redundant export of the $JAVA_HOME environment variable is removed
  in the %%build script

* Wed Aug 14 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 1.2.1-2
- java-devel & jpackage-utils are added as the build requirements
- jpackage-utils is added as the run-time requirement
- Appropriate values of Group Tags are chosen from the official list
- Redundant run-time requirements for /bin/* utilities are removed
- A ghost symlink for javadoc package is removed
- Documentation added

* Tue Jul 08 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 1.2.1-1
- Changing for Fedora

* Thu Dec 13 2007 Jaroslav Tulach <jtulach@mandriva.org> 0:1.2.1-1mdv2008.1
+ Revision: 119152
- First package of beansbinding library
- create beansbinding
