%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Name: okular
Summary: A document viewer
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: https://projects.kde.org/projects/kde/kdegraphics/okular
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
## upstreamable patches
# okular.desktop : Categories=-Office,+VectorGraphics (http://bugzilla.redhat.com/591089)
Patch50: kdegraphics-4.5.1-okular_dt.patch
# don't hardcode paths in OkularConfig.cmake
Patch51: kdegraphics-4.5.80-OkularConfig-dont-hardcode-paths.patch
# get rif of gcc overflow warning
Patch52: okular-gcc-overflow.patch
# don't build component if ACTIVEAPP_FOUND FALSE 
Patch53: okular-4.10-cmake.patch
BuildRequires: chmlib-devel
BuildRequires: desktop-file-utils
BuildRequires: djvulibre-devel
BuildRequires: ebook-tools-devel
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: libkipi-devel >= %{kdever}
BuildRequires: libspectre-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: plasma-mobile-devel
BuildRequires: poppler-qt4-devel >= 0.20.1
BuildRequires: pkgconfig(qca2)
BuildRequires: qimageblitz-devel
Requires: %{name}-libs = %{version}-%{release}
Requires: %{name}-part = %{version}-%{release}
Requires: kio_msits = %{version}-%{release}
Requires: kde-runtime >= %{kdever}
Requires: kdegraphics-mobipocket >= %{kdever}
Requires: plasma-mobile
# when split occurred
Conflicts: kdegraphics < 4.7.0

%description
%{summary}.

%package active
Summary: Document viewer for plasma active
Group: Applications/Multimedia
# todo: test/confirm this dep is sufficient (or too much) -- rex
Requires: %{name}-part = %{version}-%{release}

%description active
%{summary}.

%package devel
Summary:  Development files for %{name}
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs-devel

%description devel
%{summary}.

%package  libs
Summary: Runtime files for %{name}
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description libs
%{summary}.

%package part
Summary: Okular kpart plugin
Group: System Environment/Libraries
Requires: %{name}-libs = %{version}-%{release}

%description part
%{summary}.

%package -n kio_msits
Summary: A kioslave for displaying WinHelp files
Group: System Environment/Libraries

%description -n kio_msits
%{summary}.

%prep
%setup -q
%patch50 -p2 -b .okular_dt
%patch51 -p2 -b .OkularConfig
%patch52 -p1 -b .overflow
%patch53 -p1 -b .cmake

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%check
desktop-file-validate %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
update-desktop-database -q &> /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
update-desktop-database -q &> /dev/null ||:
fi

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%files
%doc COPYING
%{_kde4_bindir}/%{name}*
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/applications/kde4/%{name}Application_*.desktop
%{_kde4_iconsdir}/hicolor/*/*/*
%{_kde4_docdir}/HTML/en/%{name}
%{_mandir}/man1/*

%files active
%{_kde4_bindir}/active-documentviewer
%{_kde4_appsdir}/plasma/packages/org.kde.active.documentviewer
%{_kde4_datadir}/applications/kde4/active-documentviewer.desktop
%{_kde4_datadir}/applications/kde4/active-documentviewer_*.desktop
# not sure if this is best put here or not -- rex
%{_kde4_libdir}/kde4/imports/org/kde/okular

%files devel
%{_kde4_includedir}/okular
%{_kde4_libdir}/libokularcore.so
%{_kde4_libdir}/cmake/Okular

%files libs
%{_kde4_appsdir}/kconf_update/okular.upd
%{_kde4_libdir}/libokularcore.so.*

%files part
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/config.kcfg/*.kcfg
%{_kde4_datadir}/kde4/services/lib%{name}Generator*.desktop
%{_kde4_datadir}/kde4/services/okular[A-Z]*.desktop
%{_kde4_datadir}/kde4/services/okular_part.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}Generator.desktop
%{_kde4_libdir}/kde4/%{name}Generator*.so
%{_kde4_libdir}/kde4/%{name}part.so

%files -n kio_msits
%{_kde4_libdir}/kde4/kio_msits.so
%{_kde4_datadir}/kde4/services/msits.protocol

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.8.4-2m)
- rebuild against poppler-0.20.1

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-2m)
- rebuild against libtiff-4.0.1

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)
- add part subpackage

* Tue Nov  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-2m)
- add mobipocket support

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Thu Sep 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-2m)
- rebuild against poppler-0.17.4

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- import from Fedora for new kdegraphics-4.7.0

* Tue Jul 26 2011 Jaroslav Reznik <jreznik@redhat.com> 4.7.0-1
- 4.7.0

* Mon Jul 18 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-4
- %%postun: +update-desktop-database

* Mon Jul 18 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-3
- BR: pkgconfig(qca2)

* Fri Jul 15 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-2
- bump release

* Mon Jul 11 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-1
- 4.6.95
- fix URL

* Wed Jul 06 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.90-3
- fix Source URL
- Conflicts: kdegraphics < 7:4.6.90-10

* Tue Jul 05 2011 Rex Dieter <rdieter@fedoraproject.org>  4.6.90-2
- first try

