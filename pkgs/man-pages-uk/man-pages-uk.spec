%global momorel 8
%global date 20060830

Summary: Ukrainian man pages from the Linux Documentation Project
Name: man-pages-uk
Version: 0.1
Release: 0.%{date}.%{momorel}m%{?dist}
License: GFDL
Group: Documentation
URL: http://docs.linux.org.ua/index.php/Man:Contents
Source0: http://dl.sourceforge.net/sourceforge/wiki2man/man-pages-uk-utf8-%{date}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
Manual pages from the Linux Documentation Project, translated into Ukrainian.

%prep
%setup -q -n man-pages-uk-utf8-%{date}
sed "s,  ,\t," -i Makefile

%build
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_mandir}/uk
make INSTALLPATH=%{buildroot}%{_mandir} install 

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
%doc COPYING
%lang(uk) %doc CREDITS.UK FAQ.UK NEWS.UK README.UK
%lang(uk) %{_mandir}/uk

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.20060830.8m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1-0.20060830.7m)
- build fix (make-3.82)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.20060830.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-0.20060830.5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.20060830.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.20060830.3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-0.20060830.2m)
- rebuild against gcc43

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1-0.20060830.1m)
- Import from Fedora

* Fri Sep 01 2006 Andy Shevchenko <andriy@asplinux.com.ua> 0.1-0.4.20060830
- fix tarball name according to mainstream

* Fri Sep 01 2006 Andy Shevchenko <andriy@asplinux.com.ua> 0.1-0.3.20060830
- update to 20060830 snapshot

* Wed Jun 07 2006 Andy Shevchenko <andriy@asplinux.com.ua> 0.1-0.2.20060328
- change the summary and description according to similar package from FC
- use lang macro for the *.UK files in the doc section
- fix the License tag

* Thu Jun 01 2006 Andy Shevchenko <andriy@asplinux.com.ua> 0.1-0.1.20060328
- renaming based to man-pages-LL
  (http://www.redhat.com/archives/fedora-extras-list/2006-May/msg00927.html)

* Tue Apr 04 2006 Andy Shevchenko <andriy@asplinux.com.ua>
- update to 20060328 snapshot
- change version according to Makefile

* Tue Feb 28 2006 Andy Shevchenko <andriy@asplinux.com.ua>
- change name to original tarball
- update to 20060228 snapshot
- catch version from Makefile

* Fri Feb 24 2006 Andy Shevchenko <andriy@asplinux.com.ua>
- update to last official build
- change url
- use new tarball

* Wed Feb 22 2006 Andy Shevchenko <andriy@asplinux.com.ua>
- fix spec for extras injection

* Tue Oct 25 2005 Andy Shevchenko <andriy@asplinux.ru> 0.2-1
- include all translated pages

* Thu Aug 12 2004 Andy Shevchenko <andriy@asplinux.ru>
- rebuild

* Tue Oct 21 2003 Andy Shevchenko <andriy@asplinux.ru>
- correct URL tag
- update tarball from original site
- fix spec

* Sun May 04 2003 Andy Shevchenko <andriy@asplinux.ru>
- rebuild for ASPLinux

* Mon Sep 30 2002 Michael Shigorin <mike@altlinux.ru> 0.1.1-alt1
- missing apt-cache(8) was added from fixed tarball

* Tue Sep 17 2002 Michael Shigorin <mike@altlinux.ru> 0.1-alt1
- initial release
- manpages from Andriy Dobrovol'skiy <dobr@altlinux.ru>
- based on man-pages-ru-0.7-alt20 spec by Sass <sass@altlinux.ru>
- ENC1<->ENC2 (source was in KOI8-R, uk is in CP1251)
