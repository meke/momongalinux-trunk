%global momorel 1

Summary: Revision Control System (RCS) file version management tools.
Name: rcs
Version: 5.9.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
Source0: ftp://ftp.gnu.org/gnu/rcs/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: rcs-5.7-stupidrcs.patch
BuildRequires: autoconf
URL: http://www.cs.purdue.edu/homes/trinkle/RCS/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Revision Control System (RCS) is a system for managing multiple
versions of files.  RCS automates the storage, retrieval, logging,
identification and merging of file revisions.  RCS is useful for text
files that are revised frequently (for example, programs,
documentation, graphics, papers and form letters).

The rcs package should be installed if you need a system for managing
different versions of files.

%prep
%setup -q
%patch0 -p1

autoconf

%build

%configure --with-diffutils

touch src/conf.h
make

%install
rm -rf %{buildroot}

#make prefix=%{buildroot}/%{_prefix} install
%makeinstall man1dir=%{buildroot}%{_mandir}/man1 man5dir=%{buildroot}%{_mandir}/man5

rm -f %{buildroot}%{_infodir}/dir

%post
/sbin/install-info %{_infodir}/rcs.info %{_infodir}/dir || :

%preun
if [ "$1" = 0 ] ; then
  /sbin/install-info --delete %{_infodir}/rcs.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HACKING INSTALL NEWS README THANKS
%{_bindir}/*
%{_infodir}/*
%{_mandir}/man[15]/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Jan 14 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9.2-1m)
- update to 5.9.2

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.0-1m)
- update to 5.9.0

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8.2-1m)
- update to 5.8.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7-29m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.7-27m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7-26m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-25m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-24m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7-23m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7-22m)
- rebuild against gcc43

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7-21m)
- add URL tag
- delete Prefix

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (5.7-20k)                      
- add BuildPreReq: autoconf >= 2.52-8k

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (5.7-18k)
- updated Source0 URL.

* Fri Aug 11 2000 Toru Hoshina <t@kondara.org>
- Kondarized.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jun 15 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 10)

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- fixed the spec file; added BuildRoot

* Fri Jul 18 1997 Erik Troan <ewt@redhat.com>
-built against glibc
