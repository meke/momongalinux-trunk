%global momorel 10
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-gsl
Version:        0.6.0
Release:        %{momorel}m%{?dist}
Summary:        Interface to GSL (GNU scientific library) for OCaml

Group:          Development/Libraries
License:        GPLv2
URL:            http://oandrieu.nerim.net/ocaml/gsl/
Source0:        http://oandrieu.nerim.net/ocaml/gsl/ocamlgsl-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  gsl-devel >= 1.9
BuildRequires:  gawk


%description
This is an interface to GSL (GNU scientific library), for the
Objective Caml language.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires(post): info
Requires(preun): info


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n ocamlgsl-%{version}


%build
make
strip dllmlgsl.so


%install
rm -rf $RPM_BUILD_ROOT

export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install-findlib

# Old farts like me want mli files!
install -m 0644 *.mli $RPM_BUILD_ROOT%{_libdir}/ocaml/gsl/

# Info files.
mkdir -p $RPM_BUILD_ROOT%{_infodir}
install -m 644 ocamlgsl.info* $RPM_BUILD_ROOT%{_infodir}


%clean
rm -rf $RPM_BUILD_ROOT


%post devel
/sbin/install-info %{_infodir}/ocamlgsl.info %{_infodir}/dir || :


%preun devel
/sbin/install-info --delete %{_infodir}/ocamlgsl.info %{_infodir}/dir || :


%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/ocaml/gsl
%if %opt
%exclude %{_libdir}/ocaml/gsl/*.a
%exclude %{_libdir}/ocaml/gsl/*.cmxa
%exclude %{_libdir}/ocaml/gsl/*.cmx
%endif
%exclude %{_libdir}/ocaml/gsl/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc COPYING README NEWS NOTES doc
%if %opt
%{_libdir}/ocaml/gsl/*.a
%{_libdir}/ocaml/gsl/*.cmxa
%{_libdir}/ocaml/gsl/*.cmx
%endif
%{_libdir}/ocaml/gsl/*.mli
%{_infodir}/*.info*


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-10m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-8m)
- rebuild for new GCC 4.5

* Thu Sep  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-7m)
- [BUG FIX] fix %%preun devel

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-6m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-5m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-1m)
- import from Fedora

* Fri Apr 25 2008 Richard W.M. Jones <rjones@redhat.com> - 0.6.0-3
- Fixed typo in description.
- Mixed use of buildroot macro / RPM_BUILD_ROOT variable fixed.
- Remove BR gsl (brought in by gsl-devel, so unnecessary).

* Tue Mar  4 2008 Richard W.M. Jones <rjones@redhat.com> - 0.6.0-2
- Rebuild for ppc64.

* Wed Feb 20 2008 Richard W.M. Jones <rjones@redhat.com> - 0.6.0-1
- Initial RPM release.
