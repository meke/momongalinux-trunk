%global momorel 1
%global real_version 1.99.43.10

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary:  A python library for manipulating kickstart files
Name: pykickstart
Url: http://fedoraproject.org/wiki/pykickstart
Version: 1.99.48.3
Release: %{momorel}m%{?dist}
# This is a Red Hat maintained package which is specific to
# our distribution.  Thus the source is only available from
# within this srpm.
Source0: %{name}-%{real_version}.tar.gz

License: GPLv2
Group: System Environment/Libraries
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel, gettext, python-setuptools-devel
BuildRequires: transifex-client
Requires: python >= %{pythonver}, python-urlgrabber

%description
The pykickstart package is a python library for manipulating kickstart
files.

%prep
%setup -q -n %{name}-%{real_version}

%build
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

sed -i "s/#!\/bin\/python/#!\/usr\/bin\/python/g" %{buildroot}/usr/bin/*

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README ChangeLog COPYING docs/programmers-guide
%doc docs/kickstart-docs.txt
%{python_sitelib}/pykickstart
%{python_sitelib}/pykickstart-*.egg-info
%{_bindir}/ksvalidator
%{_bindir}/ksflatten
%{_bindir}/ksshell
%{_bindir}/ksverdiff
%{_mandir}/man1/*.1*


%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.48.3-1m)
- version 1.99.43.10
-- toriaezu

* Wed May 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.48.2-1m)
- update 1.99.48.2

* Wed Jan 08 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.48-1m)
- update 1.99.48

* Thu Dec 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.99.22-1m)
- update 1.99.22

* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.99.15-1m)
- update 1.99.15

* Thu Sep 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.99.4-1m)
- update 1.99.4

* Sat Sep 10 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.99.3-1m)
- sync Fedora

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.82-2m)
- rebuild against python-2.7

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.82-1m)
- update 1.82

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.69-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.69-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.69-2m)
- full rebuild for mo7 release

* Sun Mar 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.69-1m)
- update 1.69

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.54-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.54-1m)
- update 1.54

* Mon May 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.53-1m)
- update 1.53

* Sun Feb 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.52-1m)
- update 1.52

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.39-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.39-2m)
- rebuild agaisst python-2.6.1-1m

* Thu Jun 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.39-1m)
- update 1.39

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.28-4m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.28-3m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.28-2m)
- rebuild against gcc43

* Tue Jan 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.28-1m)
- update 1.28

* Fri Jun 15 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1-1m)
- update 1.1

* Thu Mar 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.100-1m)
- update 0.100

* Sun Mar  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-1m)
- update 0.97

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.92-3m)
- rebuild against python-2.5-9m

* Wed Jan 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.92-2m)
- update 0.92

* Sun Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31-2m)
- rebuild against python-2.5

* Sun Jun 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31-1m)
- update 0.31

* Tue May  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.26-1m)
- first commit Momonga Linux 

* Mon Apr 17 2006 Chris Lumens <clumens@redhat.com> 0.26-1
- Ignore spaces before group names (#188095).
- Added some translations.
- Add options for repo command.
- Reorder %packages section output.
- Output %packages header options.
- Initialize RAID and volume group members to empty lists.

* Mon Mar 27 2006 Chris Lumens <clumens@redhat.com> 0.25-1
- Add support for the logging command.

* Mon Mar 27 2006 Chris Lumens <clumens@redhat.com> 0.24-1 
- Don't write out a blank xconfig line.
- Reorder output handlers to group like commands together.
- Mark strings for translation.

* Tue Mar 07 2006 Chris Lumens <clumens@redhat.com> 0.23-1
- Backwards compatibility support for options to zerombr.

* Fri Feb 24 2006 Chris Lumens <clumens@redhat.com> 0.22-1
- Get ignoredisk working again (#182934).

* Fri Feb 17 2006 Chris Lumens <clumens@redhat.com> 0.21-1
- Provide an option to not traceback on missing include files (#181760).
- Update programming documentation.

* Mon Feb 13 2006 Chris Lumens <clumens@redhat.com> 0.20-1
- Correctly set --noformat and --useexisting on lvm and raid.

* Mon Feb 13 2006 Chris Lumens <clumens@redhat.com> 0.19-1
- --onboot requires a value (#180987).
- Be more strict about commands that don't take arguments.

* Thu Feb 09 2006 Chris Lumens <clumens@redhat.com> 0.18-1
- Fix some errors pychecker caught.
- Allow exceptions to not be fatal so ksvalidator can spot more errors in
  a single pass (#179894).

* Wed Feb 01 2006 Chris Lumens <clumens@redhat.com> 0.17-1
- Don't set a default port for vnc.

* Tue Jan 31 2006 Chris Lumens <clumens@redhat.com> 0.16-1
- Give dmraid string an initial value.
- Handle None on partition size.

* Tue Jan 31 2006 Peter Jones <pjones@redhat.com> 0.15-1
- Add dmraid support

* Mon Jan 30 2006 Chris Lumens <clumens@redhat.com> 0.14-1
- Fix VNC parameter parsing (#179209).
- Deprecate --connect.  Add --host and --port instead.

* Thu Jan 19 2006 Chris Lumens <clumens@redhat.com> 0.13-1
- Recognize the --eject parameter to shutdown/halt.
- Store the exact post-installation action in ksdata.

* Mon Jan 09 2006 Chris Lumens <clumens@redhat.com> 0.12-1
- Clean up output quoting.
- Finish removing monitor-related stuff from xconfig.

* Mon Dec 12 2005 Chris Lumens <clumens@redhat.com> 0.11-1
- Deprecate monitor-related options to xconfig.

* Thu Dec 08 2005 Chris Lumens <clumens@redhat.com> 0.10-1
- Support --bytes-per-inode on raid
  (Curtis Doty <Curtis at GreenKey.net> #175288).

* Wed Nov 16 2005 Jeremy Katz <katzj@redhat.com> - 0.9-1
- fixup network --onboot

* Thu Nov 03 2005 Chris Lumens <clumens@redhat.com> 0.8-1
- Default to SELINUX_ENFORCING.
- Default partition sizes to None for anaconda (#172378).
- Don't call shlex.split on anything inside a script (#172313).

* Tue Nov 01 2005 Chris Lumens <clumens@redhat.com> 0.7-1
- Fix clearpart --all.
- vnc command does not require --connect option (#172192).
- network --onboot does not take any option.
- Remove extra spaces from firewall --ports and --trust.
- Write out network --<service> options.

* Fri Oct 28 2005 Chris Lumens <clumens@redhat.com> 0.6-1
- Add --resolvedeps and --ignoredeps as deprecated options.
- Pass line number to header functions.

* Mon Oct 24 2005 Chris Lumens <clumens@redhat.com> 0.5-1
- Add line numbers to exception reporting.
- Added ksvalidator.

* Wed Oct 19 2005 Chris Lumens <clumens@redhat.com> 0.4-1
- Correct deprecated attribute on options.
- Added programming documentation.

* Thu Oct 13 2005 Chris Lumens <clumens@redhat.com> 0.3-2
- Correct python lib directory on 64-bit archs (#170621).

* Fri Oct 07 2005 Chris Lumens <clumens@redhat.com> 0.3-1
- Add a deprecated attribute to options.
- Add --card option back to xconfig and mark as deprecated.
- Throw a deprecation warning on mouse and langsupport commands.
- Rename Writer to KickstartWriter for consistency.
- Collapse scripts into a single list and add an attribute on Script to
  differentiate.

* Wed Oct 05 2005 Chris Lumens <clumens@redhat.com> 0.2-1
- Rename module to pykickstart to avoid conflicts in anaconda.
- Rename data classes for consistency.
- Add default bytesPerInode settings.

* Wed Oct 05 2005 Chris Lumens <clumens@redhat.com> 0.1-1
- Created package from anaconda.
