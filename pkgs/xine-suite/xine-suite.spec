%global momorel 5
%global xine_lib_version 1.2.4
%global xime_skins_version 1.0
%global xine_ui_version 0.99.7

Summary:     Meta package for XINE
Name:        xine-suite
Version:     %{xine_lib_version}
Release:     %{momorel}m%{?dist}
License:     GPL and LGPL
# same as xine-lib
Group:       Applications/Multimedia
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# xine-lib
Requires:    xine-lib >= %{version}
Requires:    xine-lib-alsa >= %{version}
Requires:    xine-lib-arts >= %{version}
Requires:    xine-lib-docs >= %{version}
Requires:    xine-lib-esd >= %{version}
Requires:    xine-lib-flac >= %{version}
Requires:    xine-lib-gdk-pixbuf >= %{version}
Requires:    xine-lib-gnomevfs >= %{version}
Requires:    xine-lib-jack >= %{version}
Requires:    xine-lib-musepack >= %{version}
Requires:    xine-lib-oggvorbis >= %{version}
Requires:    xine-lib-oss >= %{version}
Requires:    xine-lib-pulseaudio >= %{version}
Requires:    xine-lib-sdl >= %{version}
%ifarch %{ix86}
Requires:    xine-lib-w32dll >= %{version}
%endif
Requires:    xine-lib-xcb >= %{version}
Requires:    xine-lib-xv >= %{version}

# xine-skins
Requires:    xine-skins >= %{xime_skins_version}

# xine-ui
Requires:    xine-ui >= %{xine_ui_version}

%description
This is meta package for XINE.

%files

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-5m)
- this package does not depend upon xine version and release, this is meta package !!

* Tue Feb 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-4m)
- sync Release with xine-lib

* Sat Feb  1 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-3m)
- version 1.2.4

* Tue Mar 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-5m)
- synchronze to xine-lib-1.1.21-5m

* Tue Feb 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-4m)
- synchronze to xine-lib-1.1.21-4m

* Tue Jan 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-3m)
- synchronze to xine-lib-1.1.21-3m

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-2m)
- version 1.1.21
- remove Requires: xine-lib-smb
- release 1m is skipped to synchronze to xine-lib

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.19-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.19-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.19-1m)
- version 1.1.19

* Mon Jan 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.17-1m)
- version 1.1.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16.3-1m)
- version 1.1.16.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15-2m)
- rebuild against rpm-4.6

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.15-1m)
- version 1.1.15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.7-2m)
- rebuild against gcc43

* Fri Sep  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.8-1m)
- version 1.1.8

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.6-1m)
- version 1.1.6
- add Requires: xine-lib-xcb again
- add Requires: xine-lib-musepack

* Sat Apr 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-1m)
- remove Requires: xine-lib-xcb for the moment

* Wed Apr 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-1m)
- initial package for XINE
