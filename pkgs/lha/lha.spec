%global momorel 18
%global dirname 22231
%global lhadate 20050924
%global pver 1
%global lhasrcname %{name}-%{version}-ac%{lhadate}%{?pver:p%{pver}}

Summary: LHa file archiving utility with compress
Name: lha
Version: 1.14i
Release: %{momorel}m%{?dist}
Group: Applications/Archiving
Source0: http://dl.sourceforge.jp/%{name}/%{dirname}/%{lhasrcname}.tar.gz
NoSource: 0
Source1: http://lists.sourceforge.jp/mailman/archives/lha-users/attachments/20030221/df26c455/licence.obj
License: see "licence.obj"
URL: http://sourceforge.jp/projects/lha/

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
LHa is the archive software with high compression algorithm.

%prep
%setup -q -n %{lhasrcname}
cp -f %{SOURCE0} .
cp -f %{SOURCE1} .

%build
CFLAGS="%{optflags} -DMKSTEMP" \
./configure --prefix=%{_prefix} --mandir=%{_mandir}/ja

make %{?_smp_mflags}

%install
[ -d %{buildroot} ] && rm -rf %{buildroot}
mkdir -p %{buildroot}/{%{_bindir},%{_mandir}/ja/man1}
make install BINDIR=%{buildroot}%{_bindir} \
             MANDIR=%{buildroot}%{_mandir}/ja MANSECT="1" \
             DESTDIR=%{buildroot}

# convert Japanese manual page from EUC-JP to UTF-8
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
        iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

%clean
[ -d %{buildroot} ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc olddoc/{MACHINES.euc,MACHINES2.euc,PROBLEMS.euc,README.euc,CHANGES.euc,change-114e.txt,change-114g.txt,config.eng,config.jpn.euc}
%doc licence.obj
%doc %{lhasrcname}.tar.gz
%{_bindir}/lha
%{_mandir}/ja/mann/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14i-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14i-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14i-16m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14i-15m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14i-14m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14i-13m)
- rebuild against gcc43

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14i-11m)
- revise spec

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.14i-11m)
- convert ja.man from EUC-JP to UTF-8
- enable %%{optflags}
- enable parallel build
- clean up spec file

* Sat Oct 21 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.14i-10m)
- [SECURITY] CVE-2006-4335 CVE-2006-4337 CVE-2006-4338
- update to 20050924p1

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14i-9m)
- update to 20050924

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.14i-8m)
- include source tarball as a document.

* Mon Jan 24 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14i-7m)
- update src to 20040929

* Wed Aug 04 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14i-6m)
- change Source0
- comment out Patch0: lha-version.patch
- comment out Patch1: lha-1.14i-CAN-2004-0234+0235.patch
- add new Patch0, new Patch1
- add DESTDIR at make install
- fix %%files section man1 to mann
- fix %%doc section

* Sat Jul 10 2004 TAKAHASHI Tamotsu <tamo>
- (1.14i-5m)
- [SECURITY] CAN-2004-0234 + CAN-2004-0235
  http://lists.netsys.com/pipermail/full-disclosure/2004-May/020776.html

* Thu Nov 20 2003 zunda <zunda at freeshell.org>
- (1.14i-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- this package is non-free because we have to select to whom we sell:
  see licence.obj
- see http://lists.sourceforge.jp/mailman/archives/lha-users/2003-February/000033.html

* Wed Aug 21 2002 WATABE Toyokazu <toy2@nidone.org>
- (1.14i-3m)
- add lha-version.patch to fix VERSION

* Tue Sep 18 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.14i-2k)
- update to 1.14i for jitterbug #929

* Sun Nov 12 2000 Toru Hoshina <toru@df-usa.com>
- don't use vername macro. to be simple (-"-)

* Thu Nov 09 2000 Kenichi Matsubara <m@kondara.org>
- (1.14g-4k)
- bugfix specfile.

* Thu Sep  7 2000 AYUHANA Tomonori <l@kondara.org>
- (1.14g-3k)
- remove Docdir: /usr/share/doc
- use %{_mandir}
- add MANDIR to make install
- (1.14g-2k)
- for stable

* Sun Aug 20 2000 AYUHANA Tomonori <l@kondara.org>
- (1.14g-1k)
- version up to 1.14g
- add -q at %setup
- add -b at %patch
- change patch0 lha-114f.patch -> lha-1.14g.patch (for Makefile)
- change %doc (add new files)
- obey FHS

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Thu Sep 16 1999 Norihito Ohmori <ohmori@flatout.org>
- %{optflags} use in compile time

* Tue Aug 31 1999 Norihito Ohmori <ohmori@flatout.org>
- new versoin 1.14f

* Thu Aug 10 1999 Norihito Ohmori <ohmori@flatout.org>
- rebuild for new environment (glibc-2.1.x)
