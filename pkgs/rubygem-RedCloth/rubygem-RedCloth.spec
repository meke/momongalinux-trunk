# Generated from RedCloth-4.2.9.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname RedCloth

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: RedCloth-4.2.9
Name: rubygem-%{gemname}
Version: 4.2.9
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://redcloth.org
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Textile parser for Ruby.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            -V \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x
# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/redcloth
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/COPYING
%doc %{geminstdir}/CHANGELOG
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.9-1m)
- update 4.2.9

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.8-1m)
- update 4.2.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.3-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.3-2m)
- rebuild against ruby-1.9.2

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.3-1m)
- update 4.2.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.9-1m)
- import from Fedora to Momonga

* Thu Jul 30 2009 Darryl Pierce <dpierce@redhat.com> - 4.1.9-7
- Resolves: rhbz#505589 - rubygem-RedCloth-debuginfo created from stripped binaries

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.1.9-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jul 14 2009 Darryl Pierce <dpierce@redhat.com> - 4.1.9-5
- Resolves: rhbz#505589 - rubygem-RedCloth-debuginfo created from stripped binaries

* Fri May  1 2009 Darryl Pierce <dpierce@redhat.com> - 4.1.9-4
- First official build for Fedora.

* Thu Apr 30 2009 Darryl Pierce <dpierce@redhat.com> - 4.1.9-3
- Changed mv to cp for binaries.
- Removed redundant %doc entries.

* Thu Apr 30 2009 Darryl Pierce <dpierce@redhat.com> - 4.1.9-2
- Added BuildRequires: ruby-devel to fix koji issues.

* Thu Apr 23 2009 Darryl Pierce <dpierce@redhat.com> - 4.1.9-1
- Initial package
