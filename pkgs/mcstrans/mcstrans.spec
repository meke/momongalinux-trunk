%global momorel 1

Summary: SELinux Translation Daemon
Name: mcstrans
Version: 0.3.3
Release: %{momorel}m%{?dist}
License: GPL+
Group: System Environment/Daemons
Source: http://fedora.redhat.com/projects/%{name}-%{version}.tgz
Source1: mcstransd.service
Patch:  mcstrans-0.3.2-writepid.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libselinux-devel >= 1.30.3-1
BuildRequires: libcap-devel pcre-devel >= 8.31 libsepol-devel >= 2.1.7 libsepol-static
BuildRequires: systemd-units
Requires:      pcre
Requires(pre): systemd-units
Provides: setransd
Provides: libsetrans
Obsoletes: libsetrans

%description
Security-enhanced Linux is a feature of the Linux(R) kernel and a number
of utilities with enhanced security functionality designed to add
mandatory access controls to Linux.  The Security-enhanced Linux
kernel contains new architectural components originally developed to
improve the security of the Flask operating system. These
architectural components provide general support for the enforcement
of many kinds of mandatory access control policies, including those
based on the concepts of Type Enforcement(R), Role-based Access
Control, and Multi-level Security.

mcstrans provides an translation daemon to translate SELinux categories 
from internal representations to user defined representation.

%prep
%setup -q
%patch -p1 -b .writepid

%build
make clean
make LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_lib}
mkdir -p %{buildroot}/%{_libdir}
mkdir -p %{buildroot}%{_usr}/share/mcstrans
mkdir -p %{buildroot}%{_sysconfdir}/selinux/mls/setrans.d

make DESTDIR="%{buildroot}" LIBDIR="%{buildroot}%{_libdir}" SHLIBDIR="%{buildroot}/%{_lib}" INITDIR="%{buildroot}%{_sysconfdir}/init.d" install
rm -f %{buildroot}%{_sbindir}/*
rm -f %{buildroot}%{_libdir}/*.a
cp -r share/* %{buildroot}%{_usr}/share/mcstrans/

# Systemd 
mkdir -p %{buildroot}%{_unitdir}
install -m644 %{SOURCE1} %{buildroot}%{_unitdir}
rm -rf %{buildroot}/%{_sysconfdir}/init.d

%clean
rm -rf %{buildroot}

%post 
if [ $1 -eq 1 ] ; then
   /usr/bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ]; then
   /usr/bin/systemctl --no-reload mcstransd.service >/dev/null 2>&1 || :
   /usr/bin/systemctl stop mcstransd.service > /dev/null 2>&1 || :
fi

%postun 
/usr/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ]; then
    /usr/bin/systemctl try-restart mcstransd.service >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,0755)
%{_mandir}/man8/mcs.8*
%{_mandir}/man8/mcstransd.8*
%{_mandir}/man8/setrans.conf.8*
/sbin/mcstransd
%{_unitdir}/mcstransd.service
%dir %{_sysconfdir}/selinux/mls/setrans.d

%dir %{_usr}/share/mcstrans

%defattr(0644,root,root,0755)
%dir %{_usr}/share/mcstrans/util
%dir %{_usr}/share/mcstrans/examples
%{_usr}/share/mcstrans/examples/*

%defattr(0755,root,root,0755)
%{_usr}/share/mcstrans/util/*

%changelog
* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-1m)
- update to 0.3.3
- rebuild against pcre-8.31

* Tue Feb 21 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.2-2m)
- support systemd

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.2-1m)
- update 0.3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-1m)
- sync with Fedora 11 (0.3.1-1)

* Fri Mar 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.11-3m)
- do not specify man file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.11-2m)
- rebuild against rpm-4.6

* Sun Jun  1 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.11-1m)
- update 0.2.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.5-2m)
- rebuild against gcc43

* Fri Mar  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-1m)
- update 0.2.5

* Fri Feb 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.3-1m)
- update 0.2.3

* Mon Jan 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.10-2m)
- /etc/rc.d/init.d/mcstrans -> /etc/init.d/mcstrans
- modify Requires

* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.10-1m)
- initial commit
- import from FedoraCore

