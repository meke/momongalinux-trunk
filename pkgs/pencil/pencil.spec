%global momorel 6

Summary: An animation/drawing software
Name:    pencil
Version: 0.4.4b
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: GPLv2
URL:     http://www.les-stooges.org/pascal/pencil/
Source0: http://dl.sourceforge.net/project/%{name}-planner/Pencil/0.4.4beta/%{name}-%{version}-src.zip
NoSource: 0
Patch0: %{name}-%{version}-makefile.patch
Patch1: %{name}-%{version}-ming044.patch
Requires: qt
Requires: ming
BuildRequires: qt-devel >= 4.7.0
BuildRequires: ming-devel >= 0.4.4
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Pencil is an animation/drawing software for Mac OS X, Windows, and Linux. It lets you create traditional hand-drawn animation (cartoon) using both bitmap and vector graphics. Pencil is free and open source.

%prep
# pencil-0.4.4b contains resource forks and 
# it breaks %%setup in some cases
rm -rf __MACOSX 

%setup -q -n %{name}-%{version}-source
%patch0 -p0 -b .unix
%patch1 -p1 -b .ming044

%build
case "`gcc -dumpversion`" in
4.6.*)
	# pencil.spec-0.4.4b requires this
	RPM_OPT_FLAGS="$RPM_OPT_FLAGS  -fpermissive"
	;;
esac
qmake-qt4 -unix -o Makefile pencil.pro \
	QMAKE_CFLAGS="$RPM_OPT_FLAGS" \
	QMAKE_CXXFLAGS="$RPM_OPT_FLAGS" \
	QMAKE_LFLAGS="-Wl,--as-needed"
%make

%install
rm -rf %{buildroot}
#make DESTDIR=%{buildroot} install
mkdir -p %{buildroot}%{_bindir}
install -m 755 Pencil %{buildroot}%{_bindir}/pencil
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 icons/pencil.png %{buildroot}%{_datadir}/pixmaps

## desktop
mkdir -p %{buildroot}%{_datadir}/applications
cat > %{name}.desktop <<EOF
[Desktop Entry]
Name=Pencil
GenericName=Pencil - An animation drawing
Comment=An animation/drawing software
Exec=pencil
Terminal=false
Type=Application
Icon=pencil
EOF
# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
    --add-category Graphics \
    ./%{name}.desktop


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE.TXT README TODO *.txt
%{_bindir}/*
%{_datadir}/pixmaps/*
%{_datadir}/applications/*.desktop

%changelog
* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4b-6m)
- rebuild against ming-0.4.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4b-5m)
- rebuild for new GCC 4.6

* Mon Feb 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4b-4m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4b-3m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4b-2m)
- fix %%prep to avoid a possible build failure

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4b-1m)
- update to 0.4.4b

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3b-10m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.3b-9m)
- full rebuild for mo7 release

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3b-8m)
- build fix with desktop-file-utils-0.16

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3b-7m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3b-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3b-5m)
- rebuild against rpm-4.6

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-4m)
- add Icon=pencil to pencil.desktop

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3b-3m)
- rebuild against qt-4.4.0-1m

* Sat Apr 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-2m)
- import pencil-0.4.4b-makefile.patch from pencil-0.4.4b-5.1.src.rpm
  (to fix build on x86_64)
- http://rpm.pbone.net/

* Sat Apr 26 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.3b-1m)
- import to Momonga
