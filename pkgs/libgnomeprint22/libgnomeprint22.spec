Name: libgnomeprint22
Summary: Library for printing in GNOME

%global momorel 5
%global realname libgnomeprint

Version: 2.18.8
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/2.18/%{realname}-%{version}.tar.bz2
NoSource: 0
Patch0: libgnomeprint-2.18.8-cups.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: libxml2-devel
BuildRequires: pango-devel
BuildRequires: libart_lgpl-devel
BuildRequires: libbonobo-devel >= 2.24.1
BuildRequires: perl
BuildRequires: perl-XML-Parser
BuildRequires: libgnomecups-devel
BuildRequires: openssl-devel >= 0.9.8b

Provides: %{realname}
Obsoletes: %{realname}

%description
Bonobo is a library that provides the necessary framework for GNOME
applications to deal with compound documents, i.e. those with a
spreadsheet and graphic embedded in a word-processing document.

%package devel
Summary: Libraries and include files for the Bonobo document model
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libxml2-devel
Requires: pango-devel
Requires: libart_lgpl-devel
Requires: libbonobo-devel
Requires: glib2-devel

Provides: %{realname}-devel
Obsoletes: %{realname}-devel

%description devel
This package provides the necessary development libraries and include
files to allow you to develop programs using the Bonobo document model.

%prep
%setup -q -n %{realname}-%{version}
%patch0 -p1 -b .cups

%build
%configure --enable-gtk-doc CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la
%dir %{_libdir}/libgnomeprint
%dir %{_libdir}/libgnomeprint/%{version}
%dir %{_libdir}/libgnomeprint/%{version}/modules
%dir %{_libdir}/libgnomeprint/%{version}/modules/transports
%{_libdir}/libgnomeprint/%{version}/modules/libgnomeprintcups.so
%{_libdir}/libgnomeprint/%{version}/modules/libgnomeprintlpd.so
%{_libdir}/libgnomeprint/%{version}/modules/transports/lib*.so
%{_libdir}/libgnomeprint/%{version}/modules/filters/lib*.so
%{_libdir}/libgnomeprint/%{version}/modules/libgnomeprintcups.la
%{_libdir}/libgnomeprint/%{version}/modules/libgnomeprintlpd.la
%{_libdir}/libgnomeprint/%{version}/modules/transports/lib*.la
%dir %{_libdir}/libgnomeprint/%{version}/modules/filters
%{_libdir}/libgnomeprint/%{version}/modules/filters/lib*.la
%{_datadir}/libgnomeprint
%{_datadir}/locale/*/*/*

%files devel
%defattr(-, root, root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgnomeprint/%{version}/modules/libgnomeprintcups*.a
%{_libdir}/libgnomeprint/%{version}/modules/libgnomeprintlpd*.a
%{_libdir}/libgnomeprint/%{version}/modules/transports/lib*.a
%{_libdir}/libgnomeprint/%{version}/modules/filters/lib*.a
%{_includedir}/libgnomeprint-2.2
%doc %{_datadir}/gtk-doc/html/libgnomeprint

%changelog
* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.8-5m)
- build fix

* Tue Sep  6 2011 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.18.8-4m)
- build fix (cups)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.8-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.8-2m)
- rebuild for new GCC 4.5

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.18.8-1m)
- update to 2.18.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.7-2m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.7-1m)
- update to 2.18.7

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.6-5m)
- --enable-gtk-doc

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.6-4m)
- delete __libtoolize hack

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.6-3m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.6-1m)
- update to 2.18.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.5-2m)
- rebuild against rpm-4.6

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.5-1m)
- update to 2.18.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.18.4-2m)
- rebuild against gcc43

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.4-1m)
- update to 2.18.4

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Thu Aug  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Wed Jul 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-3m)
- add -maxdepth 1 to del .la

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.0-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-6m)
- rename libgnomeprint -> libgnomeprint22

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-5m)
- rebuild against libgnomecups-0.2.2-4m

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-4m)
- delete libtool library

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-3m)
- rebuild against openssl-0.9.8a

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-2m)
- delete autoreconf and make check

* Thu Nov 17 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Fri May 27 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.8.2-1m)
- update to 2.8.2 (gnumeric 1.4.3 recommends 2.8.2)

* Sun Dec  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.8.1-1m)
- version up

* Sat Jul 10 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.1-2m)
- add BuildPreReq: cups-devel
- add files libgnomeprintcups

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.1-1m)
- version 2.6.1

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq
- remove patch1, patch2
- add patch3, patch4, patch5

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.2-2m)
- revised spec for enabling rpm 4.2.

* Mon Dec 22 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.2-1m)
- version 2.4.2.

* Thu Oct 30 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.0-1m)
- pretty spec file.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Mon Jun 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1.3-1m)
- version 2.2.1.3

* Sun Jun  1 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1.2-4m)
- patch update.

* Fri May 30 2003 Shingo Akagaki <droa@kitty.dnsalias.org>
- (2.2.1.2-3m)
- apply "gnome_font_face_download patch" from 
  http://mail.gnome.org/archives/gnome-print-list/2003-March/msg00032.html
- font type detection code change. now, we can use "t1cid" fonts.

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1.2-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1.2-1m)
- version 2.2.1.2

* Tue Feb 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1.1-1m)
- version 2.2.1.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Wed Jan 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.8-1m)
- version 2.1.8

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.7-1m)
- version 2.1.7

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.6-1m)
- version 2.1.6

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Dec 09 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Wed Nov 27 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.1.1-2m)
- The problem from which '/${sysconfdir}' will be made was solved.

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.116.1-1m)
- version 1.116.1

* Sat Aug 10 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.116.0-3m)
- nazeni..

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.116.0-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.116.0-1m)
- version 1.116.0

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.115.0-10m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.115.0-9m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-8k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-6k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-2k)
- version 1.115.0

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-2k)
- version 1.114.0

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-6k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-4k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-2k)
- version 1.113.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-10k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-8k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-6k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-4k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-2k)
- version 1.112.0

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-28k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-26k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-24k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-22k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-20k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar  5 2002 Shinog AKagaki <dora@kondara.org>
- (1.111.0-18k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-16k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-14k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-12k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-10k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-8k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-6k)
- rebuild against for bonobo-activation-0.9.4

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-4k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-2k)
- version 1.111.0
* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-2k)
- version 1.110.0
* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-12k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-10k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-8k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-6k)
- rebuild against for libIDL-0.7.4

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-4k)
- rebuild against for pango-0.24
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-2k)
- version 1.110.0
- rebuild against for libbonobo-1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.109.1-8k)
- rebuild against for linc-0.1.15

* Thu Dec 27 2001 Shingo Akagaki <dora@kondara.org>
- (1.108.0-2k)
- port from Jirai
- version 1.108.0

* Tue Sep 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.104.0-3k)
- created spec file

