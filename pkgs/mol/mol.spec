%global version 0.9.71
%global momorel 9

Summary: Virtual machine capable of running Mac OS and Mac OS X
Name: mol
Version: %{version}
Release: %{momorel}m%{?dist}
License: GPL
#Vendor: Ibrium HB
Group: Applications/Emulators
Source: ftp://ftp.nada.kth.se/pub/home/f95-sry/Public/mac-on-linux/mol-%{version}.tgz
#Source: http://dev.gentoo.org/~josejx/mol-0.9.71_pre8.tar.bz2
Source1: mol-0.9.71-config
#Patch0: mol-misc.patch
Patch0: mol-0.9.68-dhcp.patch
Patch1: mol-0.9.71-largefile.patch
Patch2: mol-0.9.71-gcc4.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: mol-kernel-modules
ExclusiveArch: ppc
%define _mandir /usr/share/man 
%define _docdir /usr/share/doc
%define _vendor_postfix ""

%description

With MOL you can run Mac OS [X] under Linux - in full speed!
All PowerPC versions of Mac OS are supported (including MacOS MacoS 9 and MacOS X).

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
cp %{SOURCE1} .config-ppc

%build
rm -rf Doc/config/dhcpd-mol.conf.orig
CFLAGS="-D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64" %configure
cp %{SOURCE1} .config-ppc
make defconfig
make clean
make BUILD_MODS=n prefix=/usr VENDOR=%_vendor_postfix

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install prefix=/usr
rm -f $RPM_BUILD_ROOT/etc/mol/yaboot.conf

%clean
rm -rf $RPM_BUILD_ROOT

#%preun

%define _mol_libdir 		%{_libdir}/mol/%{version}
%define _mol_datadir 		%{_datadir}/mol/%{version}
%define _mol_localstatedir	/var/lib/mol

%files
%defattr(-,root,root)
#%doc BUILDING COPYING COPYRIGHT CREDITS Doc/*
%doc Doc/config/* Doc/Dev/* Doc/{Boot-ROM,Networking,NewWorld-ROM,Sound,Video}
%config /etc/mol/session.map
%config /etc/mol/tunconfig
%config /etc/mol/dhcpd-mol.conf
%config /etc/mol/molrc.input
%config /etc/mol/molrc.linux
%config /etc/mol/molrc.macos
%config /etc/mol/molrc.video
%config /etc/mol/molrc.sound
%config /etc/mol/molrc.net
%config /etc/mol/molrc.ow
%config /etc/mol/molrc.osx

%_mol_localstatedir/nvram.nw

%_mandir/man?/*

%_bindir/startmol
%_bindir/mol*

%_mol_libdir/bin
%dir %_mol_libdir/modules
%_mol_libdir/mol.symbols

%_mol_datadir/images
%_mol_datadir/oftrees
%_mol_datadir/drivers
%_mol_datadir/syms
%_mol_datadir/vmodes
%_mol_datadir/nvram
%_mol_datadir/graphics
%_mol_datadir/startboing

%dir %_mol_datadir/config
%_mol_datadir/config/molrc.sys
%_mol_datadir/config/molrc.post

%dir %_mol_localstatedir

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.71-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.71-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.71-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.71-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.71-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.71-4m)
- rebuild against gcc43

* Sun Mar 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.71-3m)
- add gcc4 patch

* Sat Mar 12 2005 mutecat <mutecat@momonga-linux.org>
- (0.9.71-1m)
- set defattr
- add mol-0.9.71-largefile.patch

* Sun Mar 06 2005 mutecat <mutecat@momonga-linux.org>
- (0.9.71-1m)
- import from ydl.

* Wed Aug 25 2004 Dan Burcaw <dan@ydl.net>
- commented out Vendor: tag from rpm spec
- added our dhcp patch back from YDL 3's mol
- cleanup file left by dhcp diff so it doesn't get installed
- put docs in /usr/share/doc/mol-* instead of /moldoc !!
- remove /etc/mol/yaboot.conf from installation
* Wed Jan 14 2004 Samuel Rydh <samuel@ibrium.se>
- Modules are not built, vendor postfix added
* Mon Jan 12 2004 Samuel Rydh <samuel@ibrium.se>
- New build system
* Wed Jul 3 2002 Samuel Rydh <samuel@ibrium.se>
- New config files
* Wed Apr 10 2002 Samuel Rydh <samuel@ibrium.se>
- Updated to work with FHS
* Sun Mar 31 2002 Samuel Rydh <samuel@ibrium.se>
- Updated to work with automake.
* Wed Aug 25 2001 Samuel Rydh <samuel@ibrium.se>
- Use macros for /usr/lib etc.
* Wed Aug 25 2001 Samuel Rydh <samuel@ibrium.se>
- Use macros for /usr/lib etc.
* Wed Aug 14 2001 Samuel Rydh <samuel@ibrium.se>
- Added man pages
* Wed Aug 7 2001 Samuel Rydh <samuel@ibrium.se>
- Added 'mol-kernel-modules' dependency
* Wed May 2 2001 Samuel Rydh <samuel@ibrium.se>
- The mol rpm was split into mol.rpm and mol-kmods.rpm
* Sun Oct 22 2000 Samuel Rydh <samuel@ibrium.se>
- fixes for the video mode configurator
* Mon May 30 2000 Samuel Rydh <samuel@ibrium.se>
- added startboing file
* Sun Feb 13 2000 Takashi Oe <toe@unlserve.unl.edu>
- config/directory changes
* Sun Oct 24 1999 Samuel Rydh <samuel@ibrium.se>
- modified to reflect new directory structure
* Tue Jun 8 1999 Brad Midgley <brad@turbolinux.com>
- created
