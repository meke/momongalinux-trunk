%global momorel 1
Summary: High Availability monitor built upon LVS, VRRP and service pollers
Name: keepalived
Version: 1.2.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://www.keepalived.org/
Source0: http://www.keepalived.org/software/keepalived-%{version}.tar.gz
NoSource: 0
Source1: keepalived.service
Patch0: keepalived-1.1.14-installmodes.patch
Patch1: keepalived-1.1.19-fix-ipvs-loading.patch
Patch2: keepalived-1.2.2-ip_vs.h-pathfix.patch
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
BuildRequires: systemd-units
BuildRequires: openssl-devel
%if 0%{?fedora:1} || 0%{?rhel} >= 6
BuildRequires: libnl-devel
%else
# The RHEL <= 5 libnl is too old for the compilation to work
BuildConflicts: libnl-devel < 1.1
%endif
# We need both of these for proper LVS support
BuildRequires: kernel, kernel-devel, kernel-headers
# We need popt, popt-devel is split out of rpm in Fedora 8+ and RHEL 6+
%if 0%{?fedora} >= 8 || 0%{?rhel} >= 6
BuildRequires: popt-devel
%endif
# can't be built on platforms where we don't provide 32-bit kernel
ExcludeArch: s390 sparc sparcv9

%description
The main goal of the keepalived project is to add a strong & robust keepalive
facility to the Linux Virtual Server project. This project is written in C with
multilayer TCP/IP stack checks. Keepalived implements a framework based on
three family checks : Layer3, Layer4 & Layer5/7. This framework gives the
daemon the ability to check the state of an LVS server pool. When one of the
servers of the LVS server pool is down, keepalived informs the linux kernel via
a setsockopt call to remove this server entry from the LVS topology. In
addition keepalived implements an independent VRRPv2 stack to handle director
failover. So in short keepalived is a userspace daemon for LVS cluster nodes
healthchecks and LVS directors failover.


%prep
%setup -q
%patch0 -p1 -b .installmodes
%patch1 -p1 -b .fix-ipvs-loading
%patch2 -p1 -b .pathfix


%build
# Get the most recent available kernel build dir, allows to expand arch too
KERNELDIR=$(ls -1d --sort t /lib/modules/*/build | head -1)
%configure --with-kernel-dir="${KERNELDIR}"
%{__make} %{?_smp_mflags} STRIP=/bin/true


%install
%{__make} install DESTDIR=%{buildroot}
# Remove "samples", as we include them in %%doc
%{__rm} -rf %{buildroot}%{_sysconfdir}/keepalived/samples/
rm -rf %{buildroot}%{_sysconfdir}/rc.d/init.d/
mkdir -p %{buildroot}%{_unitdir}
%{__install} -p -m 0755 %{SOURCE1} \
    %{buildroot}%{_unitdir}/keepalived.service


%check
# A build could silently have LVS support disabled if the kernel includes can't
# be properly found, we need to avoid that.
if ! grep -q "IPVS_SUPPORT='_WITH_LVS_'" config.log; then
    echo "ERROR: We do not want keeepalived lacking LVS support."
    exit 1
fi

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable keepalived.service > /dev/null 2>&1 || :
    /bin/systemctl stop keepalived.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart keepalived.service >/dev/null 2>&1 || :
fi

%triggerun -- keepalived < 1.2.2-3
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply keepalived
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save keepalived >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del keepalived >/dev/null 2>&1 || :
/bin/systemctl try-restart keepalived.service >/dev/null 2>&1 || :

%files
%doc AUTHOR ChangeLog CONTRIBUTORS COPYING README TODO
%doc doc/keepalived.conf.SYNOPSIS doc/samples/
%dir %{_sysconfdir}/keepalived/
%config(noreplace) %{_sysconfdir}/keepalived/keepalived.conf
%config(noreplace) %{_sysconfdir}/sysconfig/keepalived
%{_unitdir}/keepalived.service
%{_bindir}/genhash
%{_sbindir}/keepalived
%{_mandir}/man1/genhash.1*
%{_mandir}/man5/keepalived.conf.5*
%{_mandir}/man8/keepalived.8*


%changelog
* Wed Oct 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-1m)
- fix build failure
- merge from fedora's keepalived-1.2.2-3 
-- update to 1.2.2
-- convert to systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.20-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.20-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 NARITA Koichi <pulsar@momonga-lpinux.org>
- (1.1.20-1m)
- update to 1.1.20

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.17-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.17-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.17-1m)
- update to 1.1.17
- import Source1 from Fedora 11 (1.1.17-1)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15-7m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15-6m)
- rebuild against rpm-4.6

* Sat Sep 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.15-5m)
- build fix

* Sat Jun  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.15-4m)
- build fix (PAE kernel)

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.15-3m)
- rebuild against openssl-0.9.8h

* Thu Jun  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.15-2m)
- use %%{_initscriptdir} instead of %%{_initrddir}

* Tue Jun  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.15-1m)
- import from Fedora

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.1.15-4
- Autorebuild for GCC 4.3

* Wed Dec 05 2007 Release Engineering <rel-eng at fedoraproject dot org> - 1.1.15-3
 - Rebuild for deps

* Mon Oct 22 2007 Matthias Saou <http://freshrpms.net/> 1.1.15-2
- Update to latest upstream sources, identical except for the included spec.

* Mon Sep 17 2007 Matthias Saou <http://freshrpms.net/> 1.1.15-1
- Update to 1.1.15.
- Remove merged genhashman and include patches.

* Fri Sep 14 2007 Matthias Saou <http://freshrpms.net/> 1.1.14-2
- Include patch from Shinji Tanaka to fix conf include from inside some
  directives like vrrp_instance.

* Thu Sep 13 2007 Matthias Saou <http://freshrpms.net/> 1.1.14-1
- Update to 1.1.14.
- Remove all upstreamed patches.
- Remove our init script and sysconfig files, use the same now provided by the
  upstream package (will need to patch for LSB stuff soonish).
- Include new goodies/arpreset.pl in %%doc.
- Add missing scriplet requirements.

* Wed Aug 22 2007 Matthias Saou <http://freshrpms.net/> 1.1.13-8
- Rebuild for new BuildID feature.

* Sun Aug  5 2007 Matthias Saou <http://freshrpms.net/> 1.1.13-7
- Update License field.

* Mon Mar 26 2007 Matthias Saou <http://freshrpms.net/> 1.1.13-6
- Fix doc/samples/sample.misccheck.smbcheck.sh mode (600 -> 644).

* Thu Mar 22 2007 Matthias Saou <http://freshrpms.net/> 1.1.13-5
- Include types patch to fix compile on F7 (David Woodhouse).
- Fix up file modes (main binary 700 -> 755 and config 600 -> 640).

* Tue Feb 13 2007 Matthias Saou <http://freshrpms.net/> 1.1.13-4
- Add missing \n to the kernel define, for when multiple kernels are installed.
- Pass STRIP=/bin/true to "make" in order to get a useful debuginfo package.

* Tue Feb 13 2007 Matthias Saou <http://freshrpms.net/> 1.1.13-3
- Add %%check section to make sure any build without LVS support will fail.

* Mon Feb  5 2007 Matthias Saou <http://freshrpms.net/> 1.1.13-2
- Use our own init script, include a sysconfig entry used by it for options.

* Thu Jan 25 2007 Matthias Saou <http://freshrpms.net/> 1.1.13-1
- Update to 1.1.13.
- Change mode of configuration file to 0600.
- Don't include all of "doc" since it meant re-including all man pages.
- Don't include samples in the main configuration path, they're in %%doc.
- Include patch to add an optional label to interfaces.

* Sat Apr 08 2006 Dries Verachtert <dries@ulyssis.org> - 1.1.12-1.2
- Rebuild for Fedora Core 5.

* Sun Mar 12 2006 Dag Wieers <dag@wieers.com> - 1.1.12-1
- Updated to release 1.1.12.

* Fri Mar 04 2005 Dag Wieers <dag@wieers.com> - 1.1.11-1
- Updated to release 1.1.11.

* Wed Feb 23 2005 Dag Wieers <dag@wieers.com> - 1.1.10-2
- Fixed IPVS/LVS support. (Joe Sauer)

* Tue Feb 15 2005 Dag Wieers <dag@wieers.com> - 1.1.10-1
- Updated to release 1.1.10.

* Mon Feb 07 2005 Dag Wieers <dag@wieers.com> - 1.1.9-1
- Updated to release 1.1.9.

* Sun Oct 17 2004 Dag Wieers <dag@wieers.com> - 1.1.7-2
- Fixes to build with kernel IPVS support. (Tim Verhoeven)

* Fri Sep 24 2004 Dag Wieers <dag@wieers.com> - 1.1.7-1
- Updated to release 1.1.7. (Mathieu Lubrano)

* Mon Feb 23 2004 Dag Wieers <dag@wieers.com> - 1.1.6-0
- Updated to release 1.1.6.

* Mon Jan 26 2004 Dag Wieers <dag@wieers.com> - 1.1.5-0
- Updated to release 1.1.5.

* Mon Dec 29 2003 Dag Wieers <dag@wieers.com> - 1.1.4-0
- Updated to release 1.1.4.

* Fri Jun 06 2003 Dag Wieers <dag@wieers.com> - 1.0.3-0
- Initial package. (using DAR)

