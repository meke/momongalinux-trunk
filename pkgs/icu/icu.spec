%global momorel 1

Name:      icu
Version:   52.1
Release:   %{momorel}m%{?dist}
Summary:   International Components for Unicode
Group:     Development/Tools
License:   MIT and "UCD" and "Public Domain"
URL:       http://www.icu-project.org/
Source0:   http://download.icu-project.org/files/icu4c/52.1/icu4c-52_1-src.tgz
NoSource:  0
Source1:   icu-config.sh
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen, autoconf
Requires: lib%{name} = %{version}-%{release}

Patch1: icu.8198.revert.icu5431.patch
Patch2: icu.8800.freeserif.crash.patch
Patch3: icu.7601.Indic-ccmp.patch
Patch4: icu.9948.mlym-crash.patch
Patch5: gennorm2-man.patch
Patch6: icuinfo-man.patch

%description
Tools and utilities for developing with icu.

%package -n lib%{name}
Summary: International Components for Unicode - libraries
Group:   System Environment/Libraries

%description -n lib%{name}
The International Components for Unicode (ICU) libraries provide
robust and full-featured Unicode services on a wide variety of
platforms. ICU supports the most current version of the Unicode
standard, and they provide support for supplementary Unicode
characters (needed for GB 18030 repertoire support).
As computing environments become more heterogeneous, software
portability becomes more important. ICU lets you produce the same
results across all the various platforms you support, without
sacrificing performance. It offers great flexibility to extend and
customize the supplied services.

%package  -n lib%{name}-devel
Summary:  Development files for International Components for Unicode
Group:    Development/Libraries
Requires: lib%{name} = %{version}-%{release}
Requires: pkgconfig

%description -n lib%{name}-devel
Includes and definitions for developing with icu.

%package -n lib%{name}-doc
Summary: Documentation for International Components for Unicode
Group:   Documentation
BuildArch: noarch

%description -n lib%{name}-doc
%{summary}.

%prep
%setup -q -n %{name}
%patch1 -p2 -R -b .icu8198.revert.icu5431.patch
%patch2 -p1 -b .icu8800.freeserif.crash.patch
%patch3 -p1 -b .icu7601.Indic-ccmp.patch
%patch4 -p1 -b .icu9948.mlym-crash.patch
%patch5 -p1 -b .gennorm2-man.patch
%patch6 -p1 -b .icuinfo-man.patch

%build
cd source
autoconf
CFLAGS='%optflags -fno-strict-aliasing'
CXXFLAGS='%optflags -fno-strict-aliasing'
#rhbz856594 do not use --disable-renaming or cope with the mess
%configure --with-data-packaging=library --disable-samples
#rhbz#225896
sed -i 's|-nodefaultlibs -nostdlib||' config/mh-linux
#rhbz#681941
sed -i 's|^LIBS =.*|LIBS = -L../lib -licuuc -lpthread -lm|' i18n/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -licui18n -lc -lgcc|' io/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -lc|' layout/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../lib -licuuc -licule -lc|' layoutex/Makefile
sed -i 's|^LIBS =.*|LIBS = -nostdlib -L../../lib -licutu -licuuc -lc|' tools/ctestfw/Makefile
# As of ICU 52.1 the -nostdlib in tools/toolutil/Makefile results in undefined reference to `__dso_handle'
sed -i 's|^LIBS =.*|LIBS = -L../../lib -licui18n -licuuc -lpthread -lc|' tools/toolutil/Makefile
#rhbz#813484
sed -i 's| \$(docfilesdir)/installdox||' Makefile
# There is no source/doc/html/search/ directory
sed -i '/^\s\+\$(INSTALL_DATA) \$(docsrchfiles) \$(DESTDIR)\$(docdir)\/\$(docsubsrchdir)\s*$/d' Makefile
# rhbz#856594 The configure --disable-renaming and possibly other options
# result in icu/source/uconfig.h.prepend being created, include that content in
# icu/source/common/unicode/uconfig.h to propagate to consumer packages.
test -f uconfig.h.prepend && sed -e '/^#define __UCONFIG_H__/ r uconfig.h.prepend' -i common/unicode/uconfig.h

make %{?_smp_mflags}
make %{?_smp_mflags} doc

%install
rm -rf $RPM_BUILD_ROOT source/__docs
make %{?_smp_mflags} -C source install DESTDIR=$RPM_BUILD_ROOT
make %{?_smp_mflags} -C source install-doc docdir=__docs
chmod +x $RPM_BUILD_ROOT%{_libdir}/*.so.*
(
 cd $RPM_BUILD_ROOT%{_bindir}
 mv icu-config icu-config-%{__isa_bits}
)
install -p -m755 -D %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/icu-config

%check
# test to ensure that -j(X>1) didn't "break" man pages. b.f.u #2357
if grep -q @VERSION@ source/tools/*/*.8 source/tools/*/*.1 source/config/*.1; then
    exit 1
fi
make %{?_smp_mflags} -C source check

%post -n lib%{name} -p /sbin/ldconfig

%postun -n lib%{name} -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/derb
%{_bindir}/genbrk
%{_bindir}/gencfu
%{_bindir}/gencnval
%{_bindir}/gendict
%{_bindir}/genrb
%{_bindir}/makeconv
%{_bindir}/pkgdata
%{_bindir}/uconv
%{_sbindir}/*
%{_mandir}/man1/derb.1*
%{_mandir}/man1/gencfu.1*
%{_mandir}/man1/gencnval.1*
%{_mandir}/man1/gendict.1*
%{_mandir}/man1/genrb.1*
%{_mandir}/man1/genbrk.1*
%{_mandir}/man1/makeconv.1*
%{_mandir}/man1/pkgdata.1*
%{_mandir}/man1/uconv.1*
%{_mandir}/man8/*.8*

%files -n lib%{name}
%defattr(-,root,root,-)
%doc license.html readme.html
%{_libdir}/*.so.*

%files -n lib%{name}-devel
%defattr(-,root,root,-)
%{_bindir}/%{name}-config*
%{_bindir}/icuinfo
%{_mandir}/man1/%{name}-config.1*
%{_mandir}/man1/icuinfo.1*
%{_includedir}/layout
%{_includedir}/unicode
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/%{name}
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/%{version}
%{_datadir}/%{name}/%{version}/install-sh
%{_datadir}/%{name}/%{version}/mkinstalldirs
%{_datadir}/%{name}/%{version}/config
%doc %{_datadir}/%{name}/%{version}/license.html

%files -n lib%{name}-doc
%defattr(-,root,root,-)
%doc license.html readme.html
%doc source/__docs/%{name}/html/*

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (52.1-1m)
- update 52.1

* Mon Jan 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6-2m)
- [SECURITY] CVE-2011-4599
- import patches (include security patch) from Fedora

* Sun Aug 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (4.6-1m)
- version up 4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.1-2m)
- full rebuild for mo7 release

* Tue Apr 27 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1 and sync Fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.1-2m)
- fix build failure with doxygen 1.6.1

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-1m)
- [SECURITY] CVE-2009-0153
- sync with Fedora 11 (4.0.1-3)

* Mon Jan 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-2m)
- %%global _default_patch_fuzz 2

* Sun Jul  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-1m)
- [SECURITY] CVE-2008-1036
- update to 4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.1-2m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.1-1m)
- update to 3.8.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6-4m)
- %%NoSource -> NoSource

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6-3m)
- %%NoSource -> NoSource

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6-2m)
- Provides and Obsoletes icu-locales for upgrading

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6-1m)
- update to 3.6

* Sun May 21 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.4.1-1m)
- update to 3.4.1
- add gnustack patch (from FC)
- add gcc41 patch (from FC)

* Wed Aug 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.2-4m)
- enable big-endian arch.

* Mon Aug  8 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (3.2-3m)
- %defattr(-,root,root)

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.2-2m)
- revise %%post and %%preun

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.2-1m)
- initial import to Momonga
