# Generated from json-1.6.6.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname json

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: JSON Implementation for Ruby
Name: rubygem-%{gemname}
Version: 1.6.6
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://flori.github.com/json
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
This is a JSON implementation as a Ruby extension in C.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            -V \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.6-1m)
- update 1.6.6

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update 1.6.4

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-1m)
- update 1.6.1

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.4-1m)
- update 1.5.4

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.3-1m)
- update 1.5.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.6-1m)
- update 1.4.6

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.5-1m)
- update 1.4.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-1m)
- Initial package for Momonga Linux
