%global         momorel 1

Name:           perl-Convert-ASN1
Version:        0.27
Release:        %{momorel}m%{?dist}
Summary:        ASN.1 Encode/Decode library
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Convert-ASN1/
Source0:        http://www.cpan.org/authors/id/G/GB/GBARR/Convert-ASN1-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Math-BigInt >= 1.997
BuildRequires:  perl-Test-Simple >= 0.90
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Convert::ASN1 encodes and decodes ASN.1 data structures using
BER/DER rules.

%prep
%setup -q -n Convert-ASN1-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog dist.ini examples LICENSE META.json mkparse OldChanges parser.y README.md
%{perl_vendorlib}/Convert/ASN1
%{perl_vendorlib}/Convert/ASN1.*
%{_mandir}/man?/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- rebuild against perl-5.20.0
- update to 0.27

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-2m)
- rebuild against rpm-4.6

* Wed Sep 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.21-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.21-2m)
- use vendor

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Sun Jun 11 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.20-2m)
- modify %%files

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.20-1m)
- update to 0.20

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.19-2m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.19-1m)
- version up to 0.19
- rebuilt against perl-5.8.7

* Wed Dec 29 2004 Toru Hoshina <t@momonga-linux.org>
- (1.30-4m)
- revised spec. We should not include .packlist.

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.18-4m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.18-2m)
- remove Epoch from BuildRequires

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.18-1m)

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.16-3m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.16-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.16-1m)

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.14-4k)
- rebuild against for perl-5.6.1

* Wed Nov 14 2001 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (0.14-2k)
- wrote spec from scratch.
