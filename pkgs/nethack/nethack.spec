%global momorel 12

%global majorver 3
%global minorver 4
%global patchver 3

Summary: NetHack, the classic game
Name: nethack
Version: %{majorver}.%{minorver}.%{patchver}
%global srcver %{majorver}%{minorver}%{patchver}
Release: %{momorel}m%{?dist}

%global jname j%{name}
%global jversion 0.5

Group: Amusements/Games
License: see "license"
%global src0url http://dl.sourceforge.net
Source0: %{src0url}/sourceforge/nethack/nethack-%{srcver}-src.tgz 
NoSource: 0
Patch0: %{name}-%{version}-local.patch
%global patch1url http://osdn.dl.sourceforge.jp
Patch1: %{patch1url}/jnethack/12867/%{jname}-%{version}-%{jversion}.diff.gz
NoPatch: 1
Patch2: %{jname}-%{version}-%{jversion}-local.patch

URL: http://www.nethack.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: bison, flex
#, awk, groff 

%global builddir %{name}-%{version}
%global jbuilddir %{jname}-%{version}-%{jversion}

%global	prefix		/usr
%global	bin_dir		%{prefix}/games
%global	gamedir		%{prefix}/games/lib/%{name}dir
%global	jgamedir	%{prefix}/games/lib/%{jname}dir
%global	vardir		%{_var}/games/%{name}
%global	jvardir		%{_var}/games/%{jname}
%global	man_dir		%{_mandir}/man6

%description
NetHack is a single player display-oriented Dungeons & Dragons(TM)
type game.  NetHack's display and command structure resemble the game
Rogue; it also resembles and is a direct descendant of the game Hack.
To win the game, you need to locate the Amulet of Yendor (somewhere below
the 20th level of the dungeon) and take it out of the dungeon.

%package common
Summary: NetHack, the classic game - common files
Group: Amusements/Games

%description common
NetHack is a single player display-oriented Dungeons & Dragons(TM)
type game.  NetHack's display and command structure resemble the game
Rogue; it also resembles and is a direct descendant of the game Hack.
To win the game, you need to locate the Amulet of Yendor (somewhere below
the 20th level of the dungeon) and take it out of the dungeon.

This package contains common files for both nethack and jnethack.
%package -n jnethack
Summary: NetHack, the classic game - Japanised version
Group: Amusements/Games

%description -n jnethack
NetHack is a single player display-oriented Dungeons & Dragons(TM)
type game.  NetHack's display and command structure resemble the game
Rogue; it also resembles and is a direct descendant of the game Hack.
To win the game, you need to locate the Amulet of Yendor (somewhere below
the 20th level of the dungeon) and take it out of the dungeon.

This package contains Japanized nethack.

%prep
%setup -q -c -n %{name}
mv %{name}-%{version} %{jbuilddir}
%setup -q -c -n %{name} -D
pushd %{builddir}
%patch0 -p1 -b .local
popd
pushd %{jbuilddir}
%patch1 -p1 -b .jnethack
%patch2 -p1 -b .local
popd

%build
pushd %{builddir}
sh ./sys/unix/setup.sh links
make "RPM_OPT_FLAGS=%{optflags}" all
strip src/%{name}
pushd util
make "RPM_OPT_FLAGS=%{optflags}" recover
strip recover
popd # util
popd # %{builddir}

pushd %{jbuilddir}
sh ./sys/unix/setup.sh links
make "RPM_OPT_FLAGS=%{optflags}" all
strip src/%{jname}
pushd util
make "RPM_OPT_FLAGS=%{optflags}" recover
strip recover
popd # util
popd # %{jbuilddir}

%install
if [ -d %{buildroot} ]; then rm -rf %{buildroot}; fi
mkdir -p %{buildroot}%{gamedir}
mkdir -p %{buildroot}%{jgamedir}
mkdir -p %{buildroot}%{man_dir}
mkdir -p %{buildroot}%{vardir}
mkdir -p %{buildroot}%{jvardir}

pushd %{builddir}
make PREFIX=%{buildroot}%{prefix} VARDIR=%{buildroot}%{vardir} CHOWN=true CHGRP=true install
cd doc
make PREFIX=%{buildroot}%{prefix} VARDIR=%{buildroot}%{vardir} MANDIR=%{buildroot}%{man_dir} CHOWN=true CHGRP=true manpages
cd ..
sed -e "s=%{buildroot}==" < %{buildroot}%{bin_dir}/%{name} > %{buildroot}%{bin_dir}/%{name}.fixed
mv %{buildroot}%{bin_dir}/%{name}.fixed %{buildroot}%{bin_dir}/%{name}
install util/recover %{buildroot}%{gamedir}
popd # %{builddir}

pushd %{jbuilddir}
make PREFIX=%{buildroot}%{prefix} VARDIR=%{buildroot}%{jvardir} CHOWN=true CHGRP=true install
sed -e "s=%{buildroot}==" < %{buildroot}%{bin_dir}/%{jname} > %{buildroot}%{bin_dir}/%{jname}.fixed
mv %{buildroot}%{bin_dir}/%{jname}.fixed %{buildroot}%{bin_dir}/%{jname}
install util/recover %{buildroot}%{jgamedir}
popd # %{jbuilddir}

%clean
if [ -d %{buildroot} ]; then rm -rf %{buildroot}; fi


%files
%defattr(-,root,root)
%doc nethack-%{version}/dat/license
%doc nethack-%{version}/dat/*help
%doc nethack-%{version}/dat/history
%doc %{builddir}/doc/Guidebook.tex %{builddir}/doc/Guidebook.txt
%doc %{builddir}/doc/Guidebook.mn %{builddir}/doc/tmac.n 
%attr(0755,root,games)		%{bin_dir}/%{name}
%attr(0775,root,games) %dir	%{vardir}
%attr(0775,root,games) %dir	%{vardir}/save
%attr(0664,root,games) %config	%{vardir}/record
%attr(0664,root,games) %config	%{vardir}/logfile
%attr(0664,root,games) %config	%{vardir}/perm

%attr(0775,root,games) %dir	%{gamedir}
%attr(2755,root,games)		%{gamedir}/%{name}
%attr(2755,root,games)		%{gamedir}/recover
%attr(0664,root,games)		%{gamedir}/nhdat
%attr(0664,root,games)		%{gamedir}/license

%files -n jnethack
%defattr(-,root,root)
%doc jnethack-%{version}-%{jversion}/dat/license
%doc jnethack-%{version}-%{jversion}/dat/*help
%doc jnethack-%{version}-%{jversion}/dat/history
%doc %{jbuilddir}/README.j %{jbuilddir}/ChangeLog.j
%doc %{jbuilddir}/doc/jGuidebook.txt
%doc %{jbuilddir}/doc/jGuidebook.mn %{builddir}/doc/tmac.n 
%attr(0755,root,games)		%{bin_dir}/%{jname}
%attr(0775,root,games) %dir	%{jvardir}
%attr(0775,root,games) %dir	%{jvardir}/save
%attr(0664,root,games) %config	%{jvardir}/record
%attr(0664,root,games) %config	%{jvardir}/logfile
%attr(0664,root,games) %config	%{jvardir}/perm

%attr(0775,root,games) %dir	%{jgamedir}
%attr(2755,root,games)		%{jgamedir}/%{jname}
%attr(2755,root,games)		%{jgamedir}/recover
%attr(0664,root,games)		%{jgamedir}/nhdat
%attr(0664,root,games)		%{jgamedir}/license

%files common
%defattr(-,root,root)
%doc nethack-%{version}/dat/license
%doc %{builddir}/README
%doc %{builddir}/doc/window.doc
%attr(0444,root,root)		%{man_dir}/*.6.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.3-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.3-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.3-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.3-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.3-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.3-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.3-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.3-5m)
- %%NoSource -> NoSource

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.3-4m)
- change Source URL

* Sun Feb 13 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.3-3m)
- update jnethack to 0.5
- NoPatch: 1

* Mon Sep  6 2004 OZAWA Sakuro <crouton@momonga-linux.org>
- (3.4.3-2m)
- jnethack update.
- enable mail delivery daemon(&).

* Sun May 23 2004 Toru Hoshina <t@momonga-linux.org>
- (3.4.3-1m)
- ver up.

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-5m)
- revised spec for rpm 4.2.

* Wed Nov  5 2003 zunda <zunda at freeshell.org>
- (3.4.0-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- some documents are added in binary packages
- %%define changed to %%global

* Wed Jan 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.0-3m)
- add defattr(-,root,root) files and files -n jnethack section

* Wed Aug 21 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (3.4.0-2m)
- JNetHack is merged.

* Tue Aug 20 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (3.4.0-1m)
- Initial import.
- No graphical interface is compiled in (yet).
