%global momorel 5

Name:         freeimage
License:      GPL
Group:        System Environment/Libraries
Summary:      FreeImage - A library project to support popular graphics image formats
Version:      3.13.1
Release:      %{momorel}m%{?dist}
URL:          http://freeimage.sourceforge.net/
Source0:      http://dl.sourceforge.net/sourceforge/freeimage/FreeImage3100.zip
NoSource:     0
Patch0:	      freeimage-3.10.0-x86_64.patch

BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gcc-c++ 

%description
FreeImage is an Open Source library project for developers who would like to support
popular graphics image formats like PNG, BMP, JPEG, TIFF and others as needed by today's
multimedia applications. FreeImage is easy to use and fast, multithreading safe.

%package devel
Group:        System Environment/Libraries
Summary:      FreeImage development package
Requires:     %{name} = %{version}

%description devel
FreeImage is an Open Source library project for developers who would like to support
popular graphics image formats like PNG, BMP, JPEG, TIFF and others as needed by today's
multimedia applications. FreeImage is easy to use and fast, multithreading safe.

%prep
%setup -n FreeImage

%patch0 -p1 -b .x86_64

# undesirable setting
sed -i "s/-o root -g root//g" Makefile.gnu
sed -i "s/ldconfig//g" Makefile.gnu

%build
# freeimage default(Makefile.gnu) was "-O3 -fPIC -fexceptions -fvisibility=hidden"
make  %{?_smp_mflags} COMPILERFLAGS="$RPM_OPT_FLAGS -fPIC -fexceptions -fvisibility=hidden"

%install
mkdir -p -m 755 %{buildroot}/%{_includedir}
mkdir -p -m 755 %{buildroot}/%{_libdir}
make INCDIR=%{buildroot}/%{_includedir} INSTALLDIR=%{buildroot}%{_libdir} install

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root)
%{_libdir}/*.so.*
%{_libdir}/libfreeimage-*.so

%files devel
%defattr(-,root,root)
%doc Whatsnew.txt README.linux license-gpl.txt license-fi.txt
%{_includedir}/FreeImage.h
%{_libdir}/*.a
%{_libdir}/libfreeimage.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.13.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.13.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13.1-3m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13.1-2m)
- add 64bit patch

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13.1-1m)
- update 3.13.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.10.0-4m)
- rebuild against gcc43

* Mon Feb 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.0-3m)
- update x86_64 patch

* Sun Feb 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.0-2m)
- support x86_64

* Tue Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.0-1m)
- Initial commit Momonga Linux. import from Mandriva

