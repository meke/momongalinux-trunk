%global momorel 2

%global emacsver %{_emacs_version}
#%%global cvs_date 20100808
%global apelver 10.8-6m

%global e_sitedir %{_emacs_sitelispdir}
#%%global srcname ddskk-%{cvs_date}
%global srcname ddskk-%{version}

Summary: Simple Kana to Kanji conversion program for Emacs
Name: emacs-skk
Version: 14.4
#Release: 0.%{cvs_date}.%{momorel}m%{?dist}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source0: http://openlab.ring.gr.jp/skk/maintrunk/%{srcname}.tar.gz
URL: http://openlab.ring.gr.jp/skk/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-apel >= %{apelver}
# to avoid infinite loop if gcc >= 3
BuildRequires: dbskkd-cdb >= 1.01-10m
Requires: emacs >= %{emacsver}
Requires: emacs-apel >= %{apelver}
Requires: skk-jisyo
Requires(post): info
Requires(preun): info
Obsoletes: skk-emacs
Obsoletes: skk-xemacs

Obsoletes: elisp-skk
Provides: elisp-skk

%description
SKK is a software system that provides a very fast and efficient Japanese
input environment for Mule.  As it is implemented based on a very simple
principle, you can master the usage of SKK quickly. This tutorial explains
SKK assuming that you know enough about Mule.

%prep
%setup -q -n %{srcname}

%build
rm -rf %{buildroot}

### emacs
rm -f SKK-CFG
cat <<EOF > SKK-CFG
(setq PREFIX "%{_prefix}")
(setq SKK_LISPDIR "%{e_sitedir}/skk")
(setq SKK_DATADIR "%{_datadir}/skk")
(setq SKK_INFODIR "%{_infodir}")
EOF

make EMACS=emacs

sed -i -e 's|#!/usr/bin/env ruby|#!/usr/bin/env ruby18|' bayesian/bskk

%install
rm -f SKK-CFG
cat <<EOF > SKK-CFG
(setq PREFIX "%{buildroot}%{_prefix}")
(setq SKK_LISPDIR "%{buildroot}%{e_sitedir}/skk")
(setq SKK_DATADIR "%{buildroot}%{_datadir}/skk")
(setq SKK_INFODIR "%{buildroot}%{_infodir}")
EOF

make EMACS=emacs install

emacs -batch -q -no-site-file -eval "(progn (add-to-list 'load-path \".\") (require 'emu) (require 'skk-vars) (require 'skk-macs))" \
    -f batch-byte-compile bayesian/skk-bayesian.el
install -m 644 bayesian/skk-bayesian.el %{buildroot}%{e_sitedir}/skk
install -m 644 bayesian/skk-bayesian.elc %{buildroot}%{e_sitedir}/skk
mkdir -p %{buildroot}%{_bindir}
install -m 755 bayesian/bskk %{buildroot}%{_bindir}

cp etc/ChangeLog ChangeLog.etc
mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 etc/dot.emacs %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 etc/dot.skk %{buildroot}%{_datadir}/config-sample/%{name}

# remove
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/skk.info %{_infodir}/dir || :

%preun
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/skk.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root,-)
%doc ChangeLog* READMEs etc/Emacs.ad
%{_datadir}/skk/*
%{_datadir}/config-sample/%{name}
%{_bindir}/bskk
%{e_sitedir}/skk
%{_infodir}/*.info*

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (14.4-2m)
- add source

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (14.4-1m)
- update to 14.4
- remove epoch, which is no longer needed

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:14.1-7m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:14.1-6m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1:14.1-5m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:14.1-4m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (14.1-3m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:14.1-2m)
- rebuild for new GCC 4.5

* Sun Sep  5 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.1-1m)
- update to 14.1
-- bskk uses ruby18

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:14.0.92-2m)
- full rebuild for mo7 release

* Mon Aug 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.0.92-1m)
- update to 14.0.92

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.0.91-6m)
- add epoch to %%changelog

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.0.91-5m)
- update to ddskk-20100808

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.0.91-4m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.0.91-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.0.91-2m)
- merge skk-emacs to elisp-skk

* Sun Feb 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.0.91-1m)
- update to 14.0.91

* Wed Jan 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:14.0.90-1m)
- update to 14.0.90

* Thu Nov 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1.50-0.20091115.1m)
- update to ddskk-20091115

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1.50-0.20091004.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1.50-0.20091004.1m)
- update to ddskk-20091004 (last update 2009-08-20)

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:13.1.50-0.20090712.3m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:13.1.50-0.20090712.2m)
- rebuild against emacs 23.0.96

* Sat Jul 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1.50-0.20090712.1m)
- update to ddskk-20090712
- good-bye skk-xemacs

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1.50-0.20090628.1m)
- update to ddskk-20090628

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1.50-0.20090517.3m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1.50-0.20090517.2m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1.50-0.20090517.1m)
- update to ddskk-20090517

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-11m)
- temporarily disable skk-xemacs because of compile errors

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-10m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-9m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-8m)
- rebuild against rpm-4.6

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-7m)
- remove --section option of install-info

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-6m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:13.1-5m)
- rebuild against apelver 10.7-11m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-4m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:13.1-2m)
- rebuild against gcc43

* Sat Aug 18 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.1-1m)
- update to 13.1

* Wed Aug 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.0.91-0.20070812.1m)
- update to ddskk-20070812

* Sun Jul 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.0.91-0.20070729.1m) 
- update to ddskk-20070729

* Tue Jul 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.0.91-0.20070715.2m)
- fix installaion of skk-bayesian

* Sun Jul 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.0.91-0.20070715.1m)
- update to ddskk-20070715
- no %%NoSource
- revise %%doc
- install config-sample
- install skk-bayesian.el and bskk

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.0.91-0.20070617.2m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.0.91-0.20070617.1m) 
- update to ddskk-20070617

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.0.91-0.20070610.1m)
- update to ddskk-20070610
-- use SKK-CFG instead of environment variables in %%build section

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:13.0.90-0.20060917.11m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:13.0.90-0.20060917.10m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:13.0.90-0.20060917.9m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:13.0.90-0.20060917.8m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:13.0.90-0.20060917.7m)
- rebuild against apel-10.7

* Sun Feb 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:13.0.90-0.20060917.6m)
- add Epoch: 1

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (13.0.90-0.20060917.5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (13.0.90-0.20060917.4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (13.0.90-0.20060917.3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (13.0.90-0.20060917.2m)
- rebuild against emacs-22.0.90

* Tue Sep 19 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (13.0.90-0.20060917.1m)
- update

* Fri Aug 18 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (13.0.90-0.20060813.1m)
- update to cvs snapshot
- delete config-sample (too old)
  useful config-sample files locate in %%doc etc/{dot.emacs,dot.skk}
- enable NoSource

* Wed Dec 28 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (20050206-6m)
- no NoSource

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (20050206-5m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (20050206-2m)
- use %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (20050206-1m)
- rebuild against emacs 22.0.50
- update to 20050206

* Thu Nov 25 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (20040509-3m)
- update apelver to 10.6-3m

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (20040509-2m)
- rebuild against emacs-21.3.50

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (20040509-1m)
- verup

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (20031214-2m)
- revised spec for enabling rpm 4.2.

* Thu Dec 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (20031214-1m)
- update to 20031214
- comment out 'mv experimental/skk-study.el .' for rebuild

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (20030112-3m)
- rebuild against emacs-21.3
- define apelver
- update apel version
- use %%{_prefix} macro

* Mon Jan 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (20030112-2m)
- add BuildPreReq: dbskkd-cdb >= 1.01-10m to avoid infinite loop if gcc >= 3

* Mon Jan 13 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (20030112-1m)
- update to 20030112

* Sun Oct  6 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (20021006-1m)
- update to 20021006

* Sat Aug 17 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (20020811-1m)
- update to 20020811
- modify %doc because ChangeLog and ChangeLog* was repeated.

* Tue Jul 18 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (20020714-1m)
- update to 20020714

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (20020127-4k)
- /sbin/install-info -> info in PreReq.

* Sun Feb  3 2002 Kenta MURATA <muraken2@nifty.com>
- (20020127-2k)
- version up.

* Sat Nov 17 2001 Kenta MURATA <muraken2@nifty.com>
- (10.62a-6k)
- add Requires: skk-jisyo
- add skk.el to config-sample

* Sun Oct 28 2001 Hidetomo Machi <mcHT@kondara.org>
- (elisp-skk-10.62a-4k)
- merge skk-emacs and skk-xemacs

* Thu Nov  9 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- Change BuildPreReq, Requires, Provides and Obsoletes TAG

* Mon Oct 23 2000 Hidetomo Machi <mcHT@kondara.org>
- (10.61-1k)
- version 10.61 (not stable ?)
- modify specfile (License)

* Thu Aug 10 2000 Toru Hoshina <t@kondara.org>
- rebuild against xemacs 21.1.12.

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Jun 10 2000 Hidetomo Machi <mcHT@kondara.org>
- version 10.59
- add "-q" at %setup
- remove info entry

* Fri Mar 10 2000 Hidetomo Machi <mcHT@kondara.org>
- modified reference of XEmacs info
- rebuild for apel-10.2

* Mon Mar  6 2000 Akira Higuchi <a@kondara.org>
- added skk-10.57-skknumlist.patch

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Wed Feb 23 2000 Hidetomo Machi <mcHT@kondara.org>
- version up to 10.57
- describe info entry in %{_infodir}/dir
- add BuildPreReq tag

* Fri Nov 26 1999 Hidetomo Hosono <h@kondara.org>
- to let require apel-xemacs.

* Tue Nov 23 1999 Hidetomo Hosono <h@kondara.org>
- release for xemacs, based on Hidetomo Hosono's <h@kondara.org> 's
  skk-mule.spec
