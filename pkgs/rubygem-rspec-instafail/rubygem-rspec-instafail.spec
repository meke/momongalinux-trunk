# Generated from rspec-instafail-0.2.4.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname rspec-instafail

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Show failing specs instantly
Name: rubygem-%{gemname}
Version: 0.2.4
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: MIT
URL: http://github.com/grosser/rspec-instafail
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description



%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.4-1m)
- update 0.2.4

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.2-1m)
- update 0.2.2

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.9-1m)
- update 0.1.9

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.8-1m)
- Initial package for Momonga Linux
