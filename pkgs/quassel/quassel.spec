%global momorel 1
%global qtver 4.7.4
%global qtdir %{_libdir}/qt4
%global kdever 4.7.1

Summary:        Quassel is a program to connect to an IRC network
Name:           quassel
Version:        0.7.3
Release:        %{momorel}m%{?dist}
License:        GPLv2+
URL:            http://quassel-irc.org/
Group:          Applications/Internet
Source0:        http://quassel-irc.org/pub/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         quassel-0.5.0-momonga.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       qt
Requires:       openssl
Requires:       sqlite
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  kdebase-workspace-devel >= %{kdever}
BuildRequires:  openssl-devel
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  libindicate-qt-devel >= 0.2.5

%description
Quassel is a program to connect to an IRC network. 
It has the unique ability to split the graphical component (quasselclient) from 
the part that handles the IRC connection (quasselcore). 
This means that you can have a remote core permanently connected to one 
or more IRC networks and attach a client from wherever you are without moving 
around any information or settings. 
However, Quassel can easily behave like any other client by combining them into 
one binary that behaves like a traditional IRC client.

%package core
Summary: Quassel core component
Group: Applications/Internet
%description core
The Quassel IRC Core maintains a connection with the 
server, and allows for multiple clients to connect

%package client
Summary: Quassel client
Group: Applications/Internet
Requires: %{name} = %{version}
%description client
Quassel client 

%prep
%setup -q
%patch0 -p1 -b .momonga

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} .. -DWITH_KDE=ON -DWANT_MONO=ON
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &> /dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :

%post client
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun client
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &> /dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
fi

%posttrans client
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_kde4_bindir}/quassel
%{_kde4_datadir}/applications/kde4/quassel.desktop
%{_kde4_datadir}/pixmaps/quassel.png
%{_kde4_appsdir}/%{name}
%{_kde4_iconsdir}/hicolor/*/*/quassel*.png

%files core
%defattr(-,root,root,-)
%attr(755,root,root) 
%{_kde4_bindir}/quasselcore

%files client 
%defattr(-,root,root,-)
%attr(755,root,root) 
%{_kde4_bindir}/quasselclient
%{_kde4_datadir}/applications/kde4/quasselclient.desktop

%changelog
* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-2m)
- rebuild for new GCC 4.5

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-3m)
- rebuild against qt-4.6.3-1m

* Sat May 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-2m)
- BuildRequires: libindicate-qt-devel >= 0.2.5

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Wed Jan  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-2m)
- add momonga patch
- almost sync Fedora

* Tue Oct 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- initial build for Momonga Linux
