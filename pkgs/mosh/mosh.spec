%global	momorel 2

Name:		mosh
Version:	1.2.4
Release:	%{momorel}m%{?dist}
Summary:	Mobile shell that supports roaming and intelligent local echo

License:	GPLv3+
Group:		Applications/Internet
URL:		http://mosh.mit.edu/
Source0:	http://mosh.mit.edu/mosh-%{version}.tar.gz
NoSource:	0

BuildRequires:	protobuf-devel >= 2.5.0
BuildRequires:	libutempter-devel
BuildRequires:	zlib-devel
BuildRequires:	ncurses-devel
Requires:	openssh-clients
Requires:	perl-IO-Tty

%description
Mosh is a remote terminal application that supports:
  - intermittent network connectivity,
  - roaming to different IP address without dropping the connection, and
  - intelligent local echo and line editing to reduce the effects
    of "network lag" on high-latency connections.


%prep
%setup -q


%build
# -fstack-protector was broken (mosh-1.2)
RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS| sed 's/-fstack-protector//g'`
export CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS"
%configure --enable-compile-warnings=error
make %{?_smp_mflags} V=1


%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
%doc README.md COPYING ChangeLog
%{_bindir}/mosh
%{_bindir}/mosh-client
%{_bindir}/mosh-server
%{_mandir}/man1/mosh.1.*
%{_mandir}/man1/mosh-client.1.*
%{_mandir}/man1/mosh-server.1.*

%changelog
* Wed Jul 17 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-2m)
- rebuild against protobuf-2.5.0

* Mon Jun  3 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-1m)
- update 1.2.4

* Thu Jun 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2

* Sun May 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- Initial COmmit Momonga Linux

* Fri Apr 27 2012 Alexander Chernyakhovsky <achernya@mit.edu> - 1.2
- Update to mosh 1.2.

* Mon Mar 26 2012 Alexander Chernyakhovsky <achernya@mit.edu> - 1.1.1-1
- Update to mosh 1.1.1.

* Wed Mar 21 2012 Alexander Chernyakhovsky <achernya@mit.edu> - 1.1-1
- Initial packaging for mosh.
