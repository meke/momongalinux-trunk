%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define maven_settings_file %{_builddir}/%{name}/settings.xml
%define namedversion 1.2

Name:           maven-scm
Version:        1.2
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Common API for doing SCM operations
License:        "ASL 2.0"
Group:          Development/Libraries
URL:            http://maven.apache.org/scm

# svn export 
#   http://svn.apache.org/repos/asf//maven/scm/tags/maven-scm-1.2/ maven-scm-1.2/
# tar czf maven-scm-1.2.tar.gz maven-scm-1.2/
Source0:        %{name}-%{namedversion}.tar.gz
Source1:        %{name}-jpp-depmap.xml
Source2:        %{name}-mapdeps.xsl
Source3:        %{name}-add-plexusutils-dep.xml

# Keeping this here means we don't have to modify the netbeans package
Source4:        http://repo2.maven.org/maven2/org/netbeans/lib/cvsclient/20060125/cvsclient-20060125.pom

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-plugin
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-assembly
BuildRequires:  maven-surefire-maven-plugin
BuildRequires:  maven-surefire-provider-junit
BuildRequires:  maven2-common-poms >= 1.0-3
BuildRequires:  modello >= 1.0-0.2.2m
BuildRequires:  modello-maven-plugin >= 1.0-0.2.2m
BuildRequires:  netbeans-ide >= 6.7.1
BuildRequires:  plexus-utils >= 1.2
BuildRequires:  saxon-scripts
BuildRequires:  tomcat5-jsp-2.0-api
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  tomcat5
BuildRequires:  maven-shared-plugin-testing-harness
BuildRequires:  maven-doxia-sitetools
BuildRequires:  bzr
BuildRequires:  plexus-maven-plugin

Requires:       junit >= 3.8.2
Requires:       jakarta-commons-collections >= 3.1
Requires:       modello >= 1.0-0.2.2m
Requires:       modello-maven-plugin >= 1.0-0.2.2m
Requires:       netbeans-ide >= 6.7.1
Requires:       jakarta-oro >= 2.0.8
Requires:       plexus-utils >= 1.2
Requires:       velocity >= 1.4

Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2

%description
Maven SCM supports Maven 2.x plugins (e.g. maven-release-plugin) and other
tools (e.g. Continum) in providing them a common API for doing SCM operations.

%package test
Summary:        Tests for %{name}
Group:          Development/Tools
Requires:       maven-scm = %{epoch}:%{version}-%{release}

%description test
Tests for %{name}.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q 

# We don't have svm synergy classes, so disable those tests for now
rm -f maven-scm-providers/maven-scm-provider-synergy/src/test/java/org/apache/maven/scm/provider/synergy/util/SynergyCCMTest.java

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

# Doing this here means we don't have to modify the netbeans package
mkdir -p $MAVEN_REPO_LOCAL/org/netbeans/lib/cvsclient/20060125/
cp -p %{SOURCE4} $MAVEN_REPO_LOCAL/org/netbeans/lib/cvsclient/20060125/
ln -s %{_datadir}/netbeans/ide12/modules/org-netbeans-lib-cvsclient.jar \
  $MAVEN_REPO_LOCAL/org/netbeans/lib/cvsclient/20060125/cvsclient-20060125.jar

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
export MAVEN_OPTS="-Xmx512M -XX:MaxPermSize=512M"
mvn-jpp \
        -e \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        -Dmaven.test.failure.ignore=true \
        -Dmaven2.jpp.depmap.file=%{SOURCE1} \
        install javadoc:javadoc

%install
rm -rf $RPM_BUILD_ROOT
# jars/poms
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/%{name}
install -d -m 755 $RPM_BUILD_ROOT/%{_datadir}/maven2/poms

# remove test files, they are used for build time testing
#find  -type f -name "*cvstest*" -exec rm -f '{}' \; \
#-o -type f -name "*svntest*" -exec rm -f '{}' \;

for jar in `find . -type f -name "*.jar" | grep -E "target/.*.jar$"`; do
        newname=`basename $jar | sed -e s:^maven-scm-::g`
        install -pm 644 $jar \
          $RPM_BUILD_ROOT%{_javadir}/%{name}/$newname
done

(cd $RPM_BUILD_ROOT%{_javadir}/%{name} && for jar in *-%{namedversion}*; \
  do ln -sf ${jar} `echo $jar| sed  "s|-%{namedversion}||g"`; done)

#poms (exclude the svn/cvstest poms. They are unnecessary)
# ignore 
#  1) poms in target/ (they are either copies, or temps)
#  2) poms in src/test/ (they are poms needed for tests only)
for i in `find . -name pom.xml | grep -v \\\./pom.xml | \
   grep -v target | grep -v src/test`; do
        artifactname=`basename \`dirname $i\``
        jarname=`echo $artifactname | sed -e s:^maven-scm-::g`
        cp -p $i $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.$artifactname.pom
        %add_to_maven_depmap org.apache.maven.scm $artifactname %{namedversion} JPP/%{name} $jarname
done
cp -p pom.xml $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.maven-scm-scm.pom
%add_to_maven_depmap org.apache.maven.scm maven-scm %{namedversion} JPP/maven-scm scm

%add_to_maven_depmap org.apache.maven.plugins maven-scm-plugin %{namedversion} JPP/maven-scm plugin

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

for docsdir in `find -name apidocs`; do
        subdir=`echo $docsdir | \
          awk -F / '{print $(NF-3)}' | sed -e s:^maven-scm-::g`
        install -dm 755 \
          $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/$subdir
        cp -pr $docsdir/* \
          $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/$subdir
done

ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%dir %{_javadir}/%{name}
%{_datadir}/maven2/poms/*
%{_javadir}/%{name}/api*
%{_javadir}/%{name}/client*
%{_javadir}/%{name}/manager-plexus*
%{_javadir}/%{name}/plugin*
%{_javadir}/%{name}/provider-*
%exclude %{_javadir}/%{name}/provider-cvstest*
%exclude %{_javadir}/%{name}/provider-svntest*
%{_mavendepmapfragdir}/*

%files test
%defattr(-,root,root,-)
%{_javadir}/%{name}/provider-cvstest*
%{_javadir}/%{name}/provider-svntest*
%{_javadir}/%{name}/test*

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-3m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-2m)
- fix "java.lang.OutOfMemoryError" exceptions in build

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-1m)
- sync with Fedora 13 (0:1.2-5)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.2.b3.1jpp.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.2.b3.1jpp.2m)
- rebuild against rpm-4.6

* Wed May 21 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.2.b3.1jpp.1m)
- import from Fedora to Momonga

* Thu Feb 28 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.2.b3.1jpp.3
- Rebuild

* Fri Sep 21 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-0.1.b3.2jpp.2
- Rebuild with excludearch for ppc64

* Tue Feb 27 2007 Tania Bento <tbento@redhat.com> 0:1.0-0.1.b3.2jpp.1
- Fixed %%Release.
- Fixed %%BuildRoot.
- Removed %%Vendor.
- Removed %%Distribution.
- Removed %%post and %%postun sections for javadoc.
- Fixed instructions on how to generate source drop.
- Marked documentation files as %%doc in %%files section.
- Fixed %%Summary.
- Fixed %%description.

* Tue Oct 17 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.0-0.b3.2jpp
- Update for maven 9jpp.

* Tue Sep 18 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.0-0.b3.1jpp
- Initial build
