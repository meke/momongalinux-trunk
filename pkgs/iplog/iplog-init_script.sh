#!/bin/sh
#
#	This shell script takes care of starting and stopping
#	iplog.
#
# chkconfig: - 90 08
# description:  This program lets you log tcp, udp, and icmp 
#               connections in syslog, along with the hostname.
#

#
# Configuration is in /etc/iplog.conf
#

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

# Check that networking is up.
[ ${NETWORKING} = "no" ] && exit 0

# See how we were called.
case "$1" in
  start)
        # Start daemons.
        echo -n "Starting iplog: "
	daemon iplog
        echo
        ;;
  stop)
        # Stop daemons.
        echo -n "Shutting down iplog: "
	killproc iplog
        echo
        ;;
  restart|reload)
	$0 stop
	$0 start
	;;
  status)
	status iplog
	;;

  *)
	echo "Usage: iplog {start|stop|restart|status}"
	exit 1
esac

exit $RETVAL
