%global momorel 13
%global iplog_user	iplog
%global iplog_piddir	/var/run/iplog
%global iplog_pidfile	%{iplog_piddir}/iplog.pid

Summary: Logs TCP, UDP, and ICMP connections to syslog.
Name: iplog
Version: 2.2.3
Release: %{momorel}m%{?dist}
Source0: http://dl.sourceforge.net/sourceforge/ojnk/%{name}-%{version}.tar.gz
NoSource: 0
Source1: iplog-init_script.sh
Source2: iplog.conf.in
Patch0: iplog-2.2.3-gcc34.patch
License: GPL
Group: System Environment/Base
BuildRequires: libpcap-devel >= 1.1.1
Requires(post): chkconfig shadow-utils
Requires(preun): chkconfig shadow-utils
URL: http://ojnk.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
iplog is a TCP/IP traffic logger.  Currently, it is capable of logging 
TCP, UDP and ICMP traffic.  Adding support for other protocols
should be relatively easy.  iplog contains a built-in packet filter,
allowing for logging or excluding packets that fit a given set of
criteria.

%prep
rm -rf %{buildroot}
%setup -q
%patch0 -p1 -b .gcc34

%build
%configure
make -j%{_numjobs} || :
make

%install
%makeinstall

mkdir -p %{buildroot}/%{_initscriptdir}
cp %SOURCE1 %{buildroot}/%{_initscriptdir}/iplog
sed -e 's|@IPLOG_USER@|%{iplog_user}|g' \
    -e 's|@IPLOG_GROUP@|%{iplog_user}|g' \
    -e 's|@PID_FILE@|%{iplog_pidfile}|g' \
       %SOURCE2 > %{buildroot}/%{_sysconfdir}/iplog.conf

%post
/sbin/chkconfig --add iplog

if ! grep -q '^%{iplog_user}:' /etc/passwd
then
    /usr/sbin/useradd -M -r -d "/" -s "/bin/false" -c "iplog sandbox" %{iplog_user}
fi

if [ ! -d %{iplog_piddir} ]
then
    mkdir -p %{iplog_piddir}
    chown %{iplog_user}:%{iplog_user} %{iplog_piddir}
    chmod 755 %{iplog_piddir}
fi

if [ -r %{iplog_pidfile} ]
then
    %{_initscriptdir}/iplog restart
fi

%preun
if [ "$1" = 0 ]
then
    if [ -r %{iplog_pidfile} ]
    then
	%{_initscriptdir}/iplog stop
    fi

    /sbin/chkconfig --del iplog

    rmdir %{iplog_piddir}

    if grep -q '^%{iplog_user}:' /etc/passwd
    then
	/usr/sbin/userdel %{iplog_user}
    fi
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* README TODO NEWS ChangeLog
%config(noreplace) %{_initscriptdir}/*
%config(noreplace) %{_sysconfdir}/iplog.conf
%{_mandir}/man5/*
%{_mandir}/man8/*
%{_sbindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.3-11m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.3-10m)
- rebuild against libpcap-1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.3-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.3-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.3-7m)
- rebuild against gcc43

* Thu Jun 07 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.3-6m)
- rebuild against libpcap-0.9.5 (restrict by version)

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-5m)
- reuild against libpcap-0.9.5

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.3-4m)
- add Patch0: iplog-2.2.3-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Tue Apr 29 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.3-3m)
- not active by default 

* Sun Mar 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.3-2m)
- fix URL

* Mon Jan 27 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (2.2.3-1m)
- port from Mandrake (2.2.3-2mdk)

* Fri May 18 2001  <warly@mandrakesoft.com> 2.2.3-2mdk
- add init.d file

* Mon Feb 19 2001 Lenny Cartier <lenny@mandrakesoft.com> 2.2.3-1mdk
- used srpm from Vlatko Kosturjak <kost@iname.com> :
	- new release

* Mon Aug 07 2000 Frederic Lepied <flepied@mandrakesoft.com> 2.2.1-2mdk
- automatically added BuildRequires

* Fri Jul 21 2000 Warly <warly@mandrakesoft.com> 2.2.1-1mdk
- new release

* Tue Jul 04 2000 Thierry Vignaud <tvignaud@mandrakesoft.com> 2.2.0-1mdk
- new release
- use new macros

* Sat Mar 25 2000 Warly <warly@mandrakesoft.com> 2.1.1-1mdk
- v2.1.1
- spechelper
- new group:

* Tue Feb 15 2000 Lenny Cartier <lenny@mandrakesoft.com>
- v2.1.0

* Fri Jan 14 2000 Chmouel Boudjnah <chmouel@mandrakesoft.com> 1.0-1mdk
- First spec file for Mandrake distribution.
