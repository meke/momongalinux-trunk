%global         momorel 2

Name:           perl-Gimp
Version:        2.3
Release:        %{momorel}m%{?dist}
Summary:        Gimp Perl module
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Gimp/
Source0:        http://www.cpan.org/authors/id/E/ET/ETJ/Gimp-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Data-Dumper >= 2
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Gtk2 >= 1
BuildRequires:  perl-PDL
Requires:       perl-Data-Dumper >= 2
Requires:       perl-Gtk2 >= 1
Requires:       perl-PDL
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Gimp-Perl is a module for writing plug-ins, extensions, standalone scripts,
and file-handlers for The GNU Image Manipulation Program (The GIMP). It can
be used to automate repetitive tasks, acheive a precision hard to get
through manual use of The GIMP, interface to a web server, or other tasks
that involve Gimp.

%prep
%setup -q -n Gimp-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog Changes COPYING COPYING.Artistic COPYING.GNU insenums logo.xpm MAINTAINERS META.json NEWS Perl-Server pxgettext README README.win32 TODO typemap.pdl
%{_bindir}/embedxpm
%{_bindir}/gimpdoc
%{_bindir}/xcftopnm
%{perl_vendorarch}/auto/Gimp
%{perl_vendorarch}/Gimp.pm
%{perl_vendorarch}/Gimp
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-1m)
- update to 2.3
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-28m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-27m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-26m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-25m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-24m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-23m)
- rebuild against perl-5.16.0

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2pre1-22m)
- rebuild against gimp-2.7.4

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-21m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-20m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-19m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2pre1-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2pre1-17m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-16m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2pre1-15m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-14m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-13m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2pre1-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-11m)
- rebuild against perl-5.10.1

* Sun Mar  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2pre1-10m)
- add BuildRequires

* Sat Feb 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2pre1-9m)
- this package is arch dependent

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2pre1-8m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2pre1-7m)
- rebuild against rpm-4.6

* Fri Sep 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2pre1-6m)
- remove conflicting file with gimp

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2pre1-5m)
- rebuild against gcc43

* Fri Feb 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2pre1-4m)
- add Requires: perl-File-Slurp

* Fri Feb 29 2008 Masanobu Sato <satoshiga@momonga-linux-org>
- (2.2pre1-3m)
- revise spec file for removing perllocal.pod file

* Fri Feb 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2pre1-2m)
- fix build on perl-5.10.0

* Thu Feb 28 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2pre1-1m)
- import to Momonga (for magicrescue)

