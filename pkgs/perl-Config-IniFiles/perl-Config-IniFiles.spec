%global         momorel 2

Name:           perl-Config-IniFiles
Version:        2.83
Release:        %{momorel}m%{?dist}
Summary:        Module for reading .ini-style configuration files
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Config-IniFiles/
Source0:        http://www.cpan.org/authors/id/S/SH/SHLOMIF/Config-IniFiles-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-base
BuildRequires:  perl-Module-Build
BuildRequires:  perl-PathTools
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-Config-INI-Simple

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Config::IniFiles provides a way to have readable configuration files
outside your Perl script. Configurations can be imported (inherited,
stacked,...), sections can be grouped, and settings can be accessed from a
tied hash.

%prep
%setup -q -n Config-IniFiles-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Config/IniFiles.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.83-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.83-1m)
- update to 2.83
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.82-2m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.82-1m)
- update to 2.82
- rebuild against perl-5.18.0

* Thu May 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.81-1m)
- update to 2.81

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.80-1m)
- update to 2.80

* Tue May  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.79-1m)
- update to 2.79

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.78-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.78-2m)
- rebuild against perl-5.16.2

* Mon Oct 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.78-1m)
- update to 2.78

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.77-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.77-1m)
- update to 2.77
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.68-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.68-2m)
- rebuild against perl-5.14.1

* Wed Jun 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.68-1m)
- update to 2.68

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.66-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.66-2m)
- rebuild for new GCC 4.6

* Sat Feb  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.66-1m)
- update to 2.66

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.65-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.65-1m)
- update to 2.65

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.64-1m)
- update to 2.64

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.63-1m)
- update to 2.63

* Fri Nov 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.62-1m)
- update to 2.62

* Mon Nov 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.61-1m)
- update to 2.61

* Sat Nov 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.60-1m)
- update to 2.60

* Fri Nov 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.59-1m)
- update to 2.59
- Specfile re-generated by cpanspec 1.78.

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.58-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.58-2m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.58-1m)
- rebuild against perl-5.12.1
- update to 2.58

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.55-2m)
- rebuild against perl-5.12.0

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.55-1m)
- update to 2.55

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.54-1m)
- update to 2.54

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.52-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.52-2m)
- rebuild against perl-5.10.1

* Mon Jun 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.52-1m)
- update to 2.52

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.49-1m)
- update to 2.49

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.48-1m)
- update to 2.48

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.47-1m)
- update to 2.47

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.46-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.46-1m)
- update to 2.46

* Sun Dec 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.45-1m)
- update to 2.45

* Fri Dec 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.44-1m)
- update to 2.44

* Sun Dec  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.43-1m)
- update to 2.43

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.39-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.39-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.39-2m)
- use vendor

* Tue May 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.39-1m)
- update to 2.39

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.38-5m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.38-4m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.38-3m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.38-2m)
- remove Epoch from BuildRequires

* Sun May 30 2004 Toru Hoshina <t@momonga-linux.org>
- (2.38-1m)
- spec file was autogenerated
