%global         momorel 3

Name:           perl-MIME-Types
Version:        2.04
Release:        %{momorel}m%{?dist}
Summary:        Definition of MIME types
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MIME-Types/
Source0:        http://www.cpan.org/authors/id/M/MA/MARKOV/MIME-Types-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl-Test-Simple >= 0.47
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
MIME::Types and mod_perl

%prep
%setup -q -n MIME-Types-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%doc ChangeLog README
%{perl_vendorlib}/MIME/Type*
%{perl_vendorlib}/MIME/types.db
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-2m)
- rebuild against perl-5.18.2

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-1m)
- update to 2.00

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-2m)
- rebuild against perl-5.16.3

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Sat Dec 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-2m)
- rebuild against perl-5.16.2

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.31-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.31-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.30-2m)
- full rebuild for mo7 release

* Fri Jun  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29
- Specfile re-generated by cpanspec 1.78.

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.27-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-2m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26-2m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.23-2m)
- rebuild against gcc43

* Fri Dec 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Thu Nov 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Tue Sep 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21
- do not use %%NoSource macro

* Sat Jun  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.18-2m)
- use vendor

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17-1m)
- spec file was autogenerated
