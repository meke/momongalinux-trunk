%global momorel 28
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global srcname color-mate

Summary: Colorize Emacs's window
Name: emacs-color-mate
Version: 10.6.4
Release: %{momorel}m%{?dist}
License: see "README.color-mate"
URL: http://www.netlab.is.tsukuba.ac.jp/~yokota/izumi/color_mate/
Group: Applications/Editors
Source0: http://www.netlab.is.tsukuba.ac.jp/~yokota/archive/%{srcname}-%{version}.tar.gz
NoSource: 0
Source1: cm_select.sh
Source2: emacs-color.sh
Patch0: %{srcname}-fontset.patch
Patch1: %{srcname}-10.6.3-%{srcname}-fontset-emacs.patch
Patch2: %{srcname}-10.6.3-%{srcname}-fontset-xemacs.patch
Patch3: %{srcname}-face-nound.patch
Patch4: %{srcname}-SunnyDay-LCD.patch
Patch5: %{srcname}-10.6.3-tail.patch
BuildArch: noarch
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: color-mate
Obsoletes: color-mate-emacs
Obsoletes: color-mate-xemacs

Obsoletes: elisp-color-mate
Provides: elisp-color-mate

%description
Color-Mate is a colorize tool for Emacs.

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p0
%patch1 -p1
%patch2 -p1 
%patch3 -p1
%patch4 -p1
%patch5 -p1

%build
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# build color-mate-emacs
./configure \
	--with-emacs=emacs \
	--with-install-path=%{e_sitedir}/%{srcname}

make all

# prepare docs
mv contrib/README README.cm_select
mv kanakan-cursor/README.kanakan-cursor* ./
mv kanakan-cursor/org-doc/README* ./
rm -rf kanakan-cursor/org-doc
mv theme/README README.theme

# install el and elc files for Emacs
mkdir -p %{buildroot}%{e_sitedir}/%{srcname}

cp -ar *.el *.elc kanakan-cursor contrib theme/\
	%{buildroot}%{e_sitedir}/%{srcname}

%install
# remove Makefile and more
find %{buildroot} -name 'Makefile*' -exec rm -f {} \;
# find %%{buildroot} -name '*-compile.el' -exec rm -f {} \;

# for Emacs
rm -f %{buildroot}%{e_sitedir}/%{srcname}/contrib/cm_select.sh.in
rm -f %{buildroot}%{e_sitedir}/%{srcname}/theme/SunnyDay-LCD.el~
rm -f %{buildroot}%{e_sitedir}/%{srcname}/theme/Xdefaults.cpp

# install contrib
mkdir -p %{buildroot}%{_bindir}
install -m 755 %{SOURCE1} %{buildroot}%{_bindir}/cm_select
install -m 755 %{SOURCE2} %{buildroot}%{_bindir}/emacs-color

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Q_and_A.color-mate QuickStart.color-mate README*
%doc Customize* ChangeLog Dot.Xdefaults.default Dot.emacs.default
%{_bindir}/cm_select
%{_bindir}/emacs-color
%{e_sitedir}/color-mate

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.6.4-28m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.6.4-27m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (10.6.4-26m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.6.4-25m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.6.4-24m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.6.4-23m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (10.6.4-22m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-21m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-20m)
- merge color-mate-emacs to elisp-color-mate
- kill color-mate-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.6.4-18m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (10.6.4-17m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-16m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-15m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-14m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-13m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-12m)
- rebuild against emacs-23.0.92

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.4-11m)
- update face-nound.patch to enable build with rpm-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-10m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-9m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.6.4-8m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.4-7m)
- rebuild against xemacs-21.5.28

* Fri Aug 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.4-6m)
- import color-mate-10.6.3-tail.patch from VineSeed

* Thu Nov 25 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.4-5m)
- rebuild against emacs-21.3.50

* Tue Apr 20 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.4-4m)
- revise spec file for emacs kossorily

* Mon Apr 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.4-3m)
- modify modify modify... and more!

* Mon Apr 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.4-2m)
- modify modify modify...

* Mon Apr 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.4-1m)
- version 10.6.4

* Mon Apr 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.6.3-1m)
- import from Vine Seed

* Wed Apr  9 2003 IWAI Masaharu <iwai@alib.jp> 10.6.3-0vl1
- version up to 10.6.3
- update color-mate-fontset-emacs.patch (Patch1)
- update color-mate-fontset-xemacs.patch (Patch2)

* Fri Mar 29 2002 Jun Nishii <jun@vinelinux.org> 10.1.1-0vl8
- fix color-mate-{install,remove}.sh

* Fri Mar 29 2002 Jun Nishii <jun@vinelinux.org> 10.1.1-0vl7
- install missing .X files
- use symlink for color-xemacs
- update cm_select

* Sun Dec 16 2001 MATSUBAYASHI Kohji <shaolin@vinelinux.org> 10.1.1-0vl6
- install script (SOURCE10) modified a little

* Sun Dec 16 2001 MATSUBAYASHI Kohji <shaolin@vinelinux.org> 10.1.1-0vl5
- emacsen-common aware

* Sun Dec 03 2000 Lisa Sagami <czs14350@mb.infoweb.ne.jp>
- 10.1.1-0vl4
- fixed description
- added BuildRequires and %%{emacs} / %%{xemacs} for each package to be
  built on anti-BuildRequired host (both set to be 1 by default). 
- cp -a for egg-cursor.el to work around with emacs's murmur :-)

* Wed Oct 11 2000 Jun Nishii <jun@vinelinux.org>
- 10.1.1-0vl3
- with egg-cursor

* Wed Sep 20 2000 Jun Nishii <jun@vinelinux.org>
- 10.1.1-0vl2
- modify emacs-color 

* Fri Sep 15 2000 Jun Nishii <jun@vinelinux.org>
- 9.1.1-0vl1
- enable to set default fontset in .emacs
- obsletes SunnyDay-LCD 12
- added cm_select in bins 
- added emacs-color, xemacs-color

* Mon Sep 11 2000 T.R. Kobayashi <tkoba@ike-dyn.ritsumei.ac.jp>
- 10.0.1-8
- added SunnyDay-LCD 12 dots fontset theme 

* Fri Feb 11 2000 MATSUBAYASHI 'Shaolin' Kohji <shaolin@rhythmaning.org>
- 10.0.1-7
- added %defattr(-,root,root) 
- added %clean section

* Wed Jan 26 2000 MATSUBAYASHI 'Shaolin' Kohji <shaolin@rhythmaning.org>
- 10.0.1-6
- fixed SunnyDay-LCD.el for "wrong type argument" problem

* Fri Dec  3 1999 MATSUBAYASHI 'Shaolin' Kohji <shaolin@rins.st.ryukoku.ac.jp>
- 10.0.1-5
- %files xemacs section modified for correct load-path
  to /usr/lib/xemacs/site-packages/lisp/color-mate

* Wed Dec  1 1999 MATSUBAYASHI 'Shaolin' Kohji <shaolin@rins.st.ryukoku.ac.jp>
- change Group to Applications/Editors/EmacsLisp

* Wed Nov 17 1999 Jun NISHII <jun@vinelinux.org>
- build for Vine-2.0 
