#!/bin/sh
# emacs-color by Jun Nishii <jun@vinelinux.org>
#        Time-stamp: <2000-09-20 23:57:49 vine>

Usage(){
cat <<EOF
Usage)
  Print Usage:      ${0##*/} -h
  Execute:          ${0##*/} [ThemeName] [emacs options...]
    ex)
	${0##*/} Wine
	${0##*/} GreenTea
	${0##*/} MyTheme -g 80x25

  Available ThemeName is
	SunnyDay(Default), GreenTea, Maron, MidNight, Null, Stone, 
	SunnyDay-LCD, WaterWorld, Wine

[Attention!] To use cm_select edit your .emacs-color.
    Change this line
          (setq color-mate:theme-file "SunnyDay")
    to
          (load "cm_select")
EOF
}

CM_EMACS=${0%%-*}

case $1 in
    -h) Usage; exit 0 ;;
    -*) echo "No such option."; Usage; exit 0 ;;
esac

CM_THEME=${1-SunnyDay}

if [ $# -ge 2 ]
then
    shift
    CM_OPTION=$*
fi

exec cm_select ${CM_THEME} ${CM_EMACS} ${CM_OPTION}

# color-emacs.sh ends here


