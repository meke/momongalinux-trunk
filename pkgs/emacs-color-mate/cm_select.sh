#!/bin/sh
#
# Theme Selecter for Color-Mate shell script part
#
#  "cm_select.sh" is X resource loader. The resource file exists
# ~/elisp/color-mate/theme. If you change this path name when install,
# edit this file.
#
# Usage:
#  1. First, edit your .emacs
#      change this line
#           (setq color-mate:theme-file "SunnyDay")
#      to
#           (load "cm_select")
#
#  2. Use this script
#
#    cm_select.sh [ThemeName] [emacs name [emacs options...]]
#
#    ex)
#      cm_select.sh Wine
#      cm_select.sh GreenTea xemacs
#      cm_select.sh MyTheme emacs -g 80x25

# added many improvement by Jun Nishii <jun@vinelinux.org>
# (based on $Id: cm_select.sh.in,v 10.1 1999/08/05 23:30:09 elca Exp $)

UserThemePath=${HOME}/elisp/color-mate/theme
ThemePath=/usr/share/emacs/site-lisp/color-mate/theme

Usage(){
cat <<EOF
Usage)
  Print Usage:      cm_select.sh -h
  Execute:          cm_select.sh [ThemeName] [emacs name [emacs options...]]
    ex)
	cm_select.sh Wine
	cm_select.sh GreenTea xemacs
	cm_select.sh MyTheme emacs -g 80x25

  Available ThemeName is
	GreenTea, Maron, MidNight, Null, Stone, 
	SunnyDay, SunnyDay-LCD, WaterWorld, Wine

[Attention!] To use cm_select edit your .emacs-color.
    Change this line
          (setq color-mate:theme-file "SunnyDay")
    to
         (load "cm_select")

EOF
}

setBin(){
    BIN=`which $1 2>/dev/null`
    if [ -z ${BIN} ];then
	echo "No $1 in this system."
	echo "Aborted."
	exit 1
    fi
}

setThemeX(){ # $1:  theme name
    if [ -f "$UserThemePath/${1}.X" ]; then
	xrdb ${UserThemePath}/${1}.X
    elif [ -f "${ThemePath}/${1}.X" ]; then
	xrdb ${ThemePath}/${1}.X
    else
	echo "no such theme: ${1}"
    fi
}

if [ $# -eq 0 ]; then
    Usage; exit 0
fi

case $1 in
    -h) Usage; exit 0;;
esac

CM_THEME=${1-SunnyDay}
setBin ${2-emacs}
if [ $# -ge 3 ]; then
    shift ; shift
    CM_OPTION=$*
fi

setThemeX ${CM_THEME}
export CM_THEME

exec ${BIN} ${CM_OPTION}

# cm_select.sh ends here
