%global momorel 4

#
## To download development trunk
#
# cvs -d:pserver:anonymous@ngspice.cvs.sourceforge.net:/cvsroot/ngspice login
# cvs -z3 -d:pserver:anonymous@ngspice.cvs.sourceforge.net:/cvsroot/ngspice co -P ngspice
# tar cjf ~/rpmbuild/SOURCES/ngspice-rework-20.cvs`date '+%Y%m%d'`.tar.bz2 ngspice

Name:              ngspice
Version:           21
Release:           %{momorel}m%{?dist}
Summary:           A mixed level/signal circuit simulator

License:           BSD
Group:             Applications/Engineering
URL:               http://ngspice.sourceforge.net

#Source0:           http://downloads.sourceforge.net/sourceforge/%{name}/ngspice%{version}_100620.zip
Source0:           ngspice-rework-20.cvs20100719.tar.bz2
Source1:           ngspice-21.pdf

BuildRoot:         %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Ensured interoperability with xcircuit via Tcl

BuildRequires:     readline-devel, libXext-devel, libpng-devel, libICE-devel
BuildRequires:     libXaw-devel, mesa-libGL-devel, libXt-devel, automake, libtool
BuildRequires:     lyx
BuildRequires:     bison
BuildRequires:     byacc
BuildRequires:     flex ImageMagick

Obsoletes:         ngspice-doc < 20
Provides:          ngspice-doc = %{version}-%{release}

%description
Ngspice is a general-purpose circuit simulator program.
It implements three classes of analysis:
- Nonlinear DC analyses
- Nonlinear Transient analyses
- Linear AC analyses

Ngspice implements the usual circuits elements, like resistors, capacitors,
inductors (single or mutual), transmission lines and a growing number of
semiconductor devices like diodes, bipolar transistors, mosfets (both bulk
and SOI), mesfets, jfet and HFET. Ngspice implements the EKV model but it
cannot be distributed with the package since its license does not allow to
redistribute EKV source code.

Ngspice integrates Xspice, a mixed-mode simulator built upon spice3c1 (and
then some tweak is necessary merge it with spice3f5). Xspice provides a
codemodel interface and an event-driven simulation algorithm. Users can
develop their own models for devices using the codemodel interface.

It could be used for VLSI simulations as well.


%package -n        tclspice
Summary:           Tcl/Tk interface for ngspice
Group:             Applications/Engineering
BuildRequires:     tk-devel
BuildRequires:     blt-devel

%description -n    tclspice
TclSpice is an improved version of Berkeley Spice designed to be used with
the  Tcl/Tk scripting language. The project is based upon the NG-Spice source
code base with many improvements.

%prep
%setup -q -n ngspice
cd ng-spice-rework

# make sure the examples are UTF-8...
for nonUTF8 in examples/tclspice/tcl-testbench4/selectfromlist.tcl \
               examples/tclspice/tcl-testbench1/testCapa.cir \
               examples/tclspice/tcl-testbench1/capa.cir ; do
  %{_bindir}/iconv -f ISO-8859-1 -t utf-8 $nonUTF8 > $nonUTF8.conv
  %{__mv} -f $nonUTF8.conv $nonUTF8
done

# rpmlint warnings
find examples/ -type f -name ".cvsignore" -exec rm -rf {} ';'
find src/ -type f -name "*.c" -exec chmod -x  {} ';'
find src/ -type f -name "*.h" -exec chmod -x  {} ';'

sed -i '15,/ /i\#include <ftedev.h>;' src/include/tclspice.h

%ifarch x86_64 sparc64 ppc64 amd64
sed -i "s|@XSPICEINIT@ codemodel @prefix@/lib|@XSPICEINIT@ codemodel %{_libdir}|" \
src/spinit.in
%endif

# Fix Tclspice's examples
sed -i \
"s|load \"../../../src/.libs/libspice.so\"|lappend auto_path \"%{_libdir}/tclspice\"\npackage require spice|" \
examples/tclspice/*/*.tcl
sed -i \
"s|load ../../../src/.libs/libspice.so|lappend auto_path \"%{_libdir}/tclspice\"\npackage require spice|" \
examples/tclspice/*/*.tcl
sed -i \
"s|spice::codemodel ../../src/xspice/icm/spice2poly|spice::codemodel %{_libdir}/tclspice/spice|" \
examples/tclspice/tcl-testbench4/tcl-testbench4.tcl

#{__libtoolize} --force --copy
#{__aclocal}
#{__automake} --add-missing
#{__autoconf}
./autogen.sh

%build
# ---- Manual ----------------------------------------------------------------
#cd ng-spice-manuals
#autoreconf  -Wno-portability --install
#./configure ; make
#cd ..

cd ng-spice-rework
cp -p %{SOURCE1} .

# ---- Tclspice ----------------------------------------------------------------
# Adding BLT support
export CFLAGS="%{optflags} -I%{_includedir}/blt"

# Make builddir for tclspice
%{__mkdir} -p tclspice
%{__cp} -Rl `ls . | grep -v tclspice` tclspice

# Configure tclspice
cd tclspice
sed -i "s|\#define NGSPICEDATADIR \"\`echo \$dprefix/share/ngspice\`\"|\#define NGSPICEDATADIR \"\`echo %{_libdir}/tclspice\`\"|" configure*
# fix ng-spice-manuals directory location
#sed -i  's|../ng-spice-manuals|../../ng-spice-manuals|g' manual/Makefile.am

%configure \
    --disable-xgraph \
    --enable-xspice \
    --enable-maintainer-mode \
    --enable-dependency-tracking \
    --enable-capzerobypass \
    --enable-cider \
    --enable-newpred \
    --enable-expdevices \
    --enable-intnoise \
    --enable-predictor \
    --enable-numparam \
    --enable-dot-global \
    --enable-shared \
    --enable-ndev \
    --with-tcl=%{_libdir} \
    --libdir=%{_libdir}/tclspice

%{__make}
cd ..
# ------------------------------------------------------------------------------

%configure \
    --disable-xgraph \
    --enable-xspice \
    --enable-maintainer-mode \
    --enable-dependency-tracking \
    --enable-capzerobypass \
    --enable-cider \
    --enable-newpred \
    --enable-expdevices \
    --enable-intnoise \
    --enable-predictor \
    --enable-numparam \
    --enable-dot-global \
    --enable-shared \
    --enable-ndev \
    --libdir=%{_libdir}

%{__make}
# %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
cd ng-spice-rework

%{__make} INSTALL="install -p" install DESTDIR=%{buildroot}


# ---- Tclspice ----------------------------------------------------------------
# Tclspice : Make install
cd tclspice
%{__make} INSTALL="install -p" install DESTDIR=%{buildroot}
cd ..

%{__rm} -rf %{buildroot}%{_libdir}/tclspice/libspice.la
# ------------------------------------------------------------------------------


# Ensuring that all docs are under %%{_docdir}/%%{name}-%%{version}/
rm -rf   %{buildroot}%{_docdir}/%{name}-%{version}/
mkdir -p %{buildroot}%{_docdir}/%{name}-%{version}/
cp -pr examples/ %{buildroot}%{_docdir}/%{name}-%{version}/
cp -p ngspice-21.pdf %{buildroot}%{_docdir}/%{name}-%{version}/%{name}-%{version}.pdf
rm -rf %{buildroot}%{_docdir}/%{name}
cp -pr Stuarts_Poly_Notes FAQ DEVICES ANALYSES %{buildroot}%{_docdir}/%{name}-%{version}/
cp -pr AUTHORS COPYING README BUGS ChangeLog NEWS %{buildroot}%{_docdir}/%{name}-%{version}/


# pull as debuginfo
chmod +x %{buildroot}%{_libdir}/spice/*.cm
chmod +x %{buildroot}%{_libdir}/tclspice/spice/*.cm

%{__rm} -rf %{buildroot}%{_datadir}/info/dir

%check
cd ng-spice-rework/tests
#make check


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_datadir}/%{name}/
%{_libdir}/spice/
%{_mandir}/man1/*
%exclude %doc %{_docdir}/%{name}-%{version}/examples/tclspice
%doc %{_docdir}/%{name}-%{version}/

%files -n tclspice
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}/examples/tclspice
%{_libdir}/tclspice/


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (21-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (21-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (21-1m)
- sync with Fedora 13 (21-2.cvs20100620)

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (19-5m)
- use Requires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (19-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (19-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (19-2m)
- remove unnecessary lines

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (19-1m)
- update to 19

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (17-7m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (17-6m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (17-5m)
- rm -f %%{buildroot}%%{_infodir}/dir

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (17-4m)
- run install-info

* Sun Aug  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (17-3m)
- modify %%files to avoid conflicting

* Sun May 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (17-2m)
- --disable-xgraph since xgraph is provided by xgraph-12.1

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (17-1m)
- import to Momonga for xcircuit-3.4.28

* Fri Aug 24 2007 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-13
- mass rebuild for fedora 8 - BuildID

* Sat Jul 08 2007 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-12
- fixed ScriptletSnippets for Texinfo #246780
- moved documentations to -doc package

* Sat Mar 17 2007 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-11
- droped patch: ngspice-bjt.patch, upstream will provide a better patch soon

* Sat Mar 17 2007 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-10
- fixed bug #227519 in spec file - Ville Skytta
- patch: ngspice-bjt.patch fixes the problem with bjt devices that have less than five nodes

* Tue Jan 09 2007 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-9
- dropped --enable-cider since it requires non-opensource software
- dropped --enable-predictor from %%configure

* Tue Dec 19 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-8
- patch0 for xcircuit pipemode
- XCircuit can work as an ng-spice front-end
- fixed infodir to mean FE guidelines

* Sun Oct 15 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-7
- Fixed src/spinit.in for 64 bit

* Thu Oct 12 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-6
- Testing on 64 bit arch

* Mon Sep 04 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-5
- Added libXt-devel to include X headers

* Wed Aug 30 2006 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> 17-4
- Fix to pass compiler flags in xgraph.

* Tue Aug 29 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-3
- Fixed BR and script-without-shellbang for debug file

* Mon Aug 28 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-2
- Fixed BRs and excluded libbsim4.a
- Removed duplicates and useless ldconfig from %%post

* Sun Aug 27 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 17-1
- Initial Package for Fedora Extras
