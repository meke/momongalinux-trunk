%global         momorel 10
%global         qtver 4.8.2
%global         qtrel 2m
%global         cmakever 2.8.5
%global         cmakerel 2m

Name:           libindicate-qt
Summary:        Qt bindings for libindicate
Url:            https://launchpad.net/libindicate-qt
Version:        0.2.5
Release:        %{momorel}m%{?dist}
License:        LGPLv3
Group:          System Environment/Libraries
Source:         http://launchpad.net/%{name}/trunk/0.2.5/+download/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         %{name}-%{version}-indicate.patch
Requires:       qt >= %{qtver}-%{qtrel}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRequires:  libindicate-devel >= 0.6.1
BuildRequires:  dbusmenu-glib-devel >= 0.5.1
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This project provides a set of Qt bindings for libindicate, 
the indicator system developed by Canonical Desktop Experience team.

%package devel
License:        LGPLv3
Group:          System Environment/Libraries
Summary:        header files and libraries for %{name}
Provides:       %{name} = %{version}-%{release}

%description devel
%{summary}

%prep
%setup -q
%patch0 -p1 -b .libindicate

%build
export CFLAGS="%{optflags} -fpermissive"
export CXXFLAGS="%{optflags} -fpermissive"
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING* NEWS RELEASE_CHECK_LIST
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/libindicate-qt
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/indicate-qt.pc

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-10m)
- rebuild with libindicate-12.10.0

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-9m)
- rebuild against libdbusmenu-0.5.1
- rebuild against libindicate-0.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-7m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-6m)
- specify PATH for Qt4

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-5m)
- fix build failure

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-2m)
- rebuild against qt-4.6.3-1m

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-1m)
- update to 0.2.5

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-1m)
- initial buld for Momonga Linux
