%global         momorel 1

Name:           perl-Moo
Version:        1.005000
Release:        %{momorel}m%{?dist}
Summary:        Minimalist Object Orientation (with Moose compatiblity)
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Moo/
Source0:        http://www.cpan.org/authors/id/H/HA/HAARG/Moo-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Class-Method-Modifiers >= 1.05
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Role-Tiny >= 1.003003
BuildRequires:  perl-strictures >= 1.001001
BuildRequires:  perl-Test-Fatal >= 0.003
BuildRequires:  perl-Test-Simple >= 0.96
Requires:       perl-Class-Method-Modifiers >= 1.05
Requires:       perl-Role-Tiny >= 1.003003
Requires:       perl-strictures >= 1.001001
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module is an extremely light-weight, high-performance Moose
replacement. It also avoids depending on any XS modules to allow simple
deployments. The name Moo is based on the idea that it provides almost -but
not quite- two thirds of Moose.

%prep
%setup -q -n Moo-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/^perl(Moo::Conflicts/d'

EOF
%define __perl_requires %{_builddir}/Moo-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Method
%{perl_vendorlib}/Moo
%{perl_vendorlib}/Moo.pm
%{perl_vendorlib}/Sub/*
%{perl_vendorlib}/oo.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005000-1m)
- rebuild against perl-5.20.0
- update to 1.005000

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004002-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004002-1m)
- update to 1.004002

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003001-1m)
- update to 1.003001

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003000-2m)
- rebuild against perl-5.18.1

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003000-1m)
- update to 1.003000

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.002000-2m)
- rebuild against perl-5.18.0

* Sun May  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.002000-1m)
- update to 1.002000

* Sun Mar 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.001000-1m)
- update to 1.001000

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000008-2m)
- rebuild against perl-5.16.3

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000008-1m)
- update to 1.000008

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000007-1m)
- update to 1.000007

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000006-1m)
- update to 1.000006

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000005-2m)
- rebuild against perl-5.16.2

* Wed Oct 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000005-1m)
- update to 1.000005

* Thu Oct  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000004-1m)
- update to 1.000004

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000003-1m)
- update to 1.000003
- rebuild against perl-5.16.1

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000002-1m)
- update to 1.000002

* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000001-1m)
- update to 1.000001

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000000-1m)
- update to 1.000000

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.091014-1m)
- update to 0.091014

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.091012-1m)
- update to 0.091012

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.091011-1m)
- update to 0.091011
- rebuild against perl-5.16.0

* Fri Mar 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009014-1m)
- update to 0.009014

* Sat Dec 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009013-1m)
- update to 0.009013

* Tue Nov 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009012-1m)
- update to 0.009012

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009011-2m)
- rebuild against perl-5.14.2

* Mon Oct  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009011-1m)
- update to 0.009011

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009008-2m)
- rebuild against perl-5.14.1

* Sat Jun  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009008-1m)
- update to 0.009008

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009007-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.009007-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
