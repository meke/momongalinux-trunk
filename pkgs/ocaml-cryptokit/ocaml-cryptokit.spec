%global momorel 9
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-cryptokit
Version:        1.3
Release:        %{momorel}m%{?dist}
Summary:        OCaml library of cryptographic and hash functions

Group:          Development/Libraries
License:        "LGPLv2 with exceptions"
URL:            http://pauillac.inria.fr/~xleroy/software.html
Source0:        http://forge.ocamlcore.org/frs/download.php/326/cryptokit-%{version}.tar.gz
NoSource:       0
Source1:        cryptokit-META
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}, zlib-devel, chrpath


%description
The Cryptokit library for Objective Caml provides a variety of
cryptographic primitives that can be used to implement cryptographic
protocols in security-sensitive applications. The primitives provided
include:

* Symmetric-key cryptography: AES, DES, Triple-DES, ARCfour, in ECB,
  CBC, CFB and OFB modes.
* Public-key cryptography: RSA encryption and signature; Diffie-Hellman
  key agreement.
* Hash functions and MACs: SHA-1, SHA-256, RIPEMD-160, MD5, and MACs
  based on AES and DES.
* Random number generation.
* Encodings and compression: base 64, hexadecimal, Zlib compression. 

Additional ciphers and hashes can easily be used in conjunction with
the library. In particular, basic mechanisms such as chaining modes,
output buffering, and padding are provided by generic classes that can
easily be composed with user-provided ciphers. More generally, the
library promotes a "Lego"-like style of constructing and composing
transformations over character streams.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n cryptokit-%{version}
rm doc/.cvsignore

%build
make all
%if %opt
make allopt
%endif

chrpath --delete dllcryptokit.so
strip dllcryptokit.so


%check
# This opens /dev/random but never reads from it.
make test


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_libdir}/ocaml/cryptokit
mkdir -p $RPM_BUILD_ROOT%{_libdir}/ocaml/cryptokit/stublibs
make INSTALLDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml/cryptokit install
mv $RPM_BUILD_ROOT%{_libdir}/ocaml/cryptokit/stublibs \
  $RPM_BUILD_ROOT%{_libdir}/ocaml/stublibs

sed 's/@VERSION@/%{version}/' < %{SOURCE1} \
  > $RPM_BUILD_ROOT%{_libdir}/ocaml/cryptokit/META


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README LICENSE
%{_libdir}/ocaml/cryptokit
%if %opt
%exclude %{_libdir}/ocaml/cryptokit/*.a
%exclude %{_libdir}/ocaml/cryptokit/*.cmxa
%exclude %{_libdir}/ocaml/cryptokit/*.cmx
%endif
%exclude %{_libdir}/ocaml/cryptokit/*.mli
%{_libdir}/ocaml/stublibs/*.so


%files devel
%defattr(-,root,root,-)
%doc LICENSE Changes doc
%if %opt
%{_libdir}/ocaml/cryptokit/*.a
%{_libdir}/ocaml/cryptokit/*.cmxa
%{_libdir}/ocaml/cryptokit/*.cmx
%endif
%{_libdir}/ocaml/cryptokit/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-9m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-6m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-5m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-2m)
- import Fedora devel changes (1.3-5)
- rebuild against ocaml-3.11.0 

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.3-4
- Rebuild for OCaml 3.10.2

* Fri Feb 29 2008 David Woodhouse <dwmw2@infradead.org> 1.3-3
- Build on PPC64

* Fri Feb 15 2008 Richard W.M. Jones <rjones@redhat.com> - 1.3-2
- Don't duplicate the README file in both packages.
- Change the license to LGPLv2 with exceptions.

* Tue Feb 12 2008 Richard W.M. Jones <rjones@redhat.com> - 1.3-1
- Initial RPM release.
