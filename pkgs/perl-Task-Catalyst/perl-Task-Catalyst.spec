%global         momorel 11

Name:           perl-Task-Catalyst
Version:        4.02
Release:        %{momorel}m%{?dist}
Summary:        All you need to start with Catalyst
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Task-Catalyst/
Source0:        http://www.cpan.org/authors/id/B/BO/BOBTFISH/Task-Catalyst-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Catalyst-Runtime >= 5.90042
BuildRequires:  perl-Catalyst-Action-REST
BuildRequires:  perl-Catalyst-ActionRole-ACL
BuildRequires:  perl-Catalyst-Authentication-Credential-HTTP
BuildRequires:  perl-Catalyst-Authentication-Store-DBIx-Class
BuildRequires:  perl-Catalyst-Component-InstancePerContext
BuildRequires:  perl-Catalyst-Controller-ActionRole
BuildRequires:  perl-Catalyst-Devel >= 1.26
BuildRequires:  perl-Catalyst-Engine-HTTP-Prefork
BuildRequires:  perl-Catalyst-Engine-PSGI
BuildRequires:  perl-Catalyst-Manual >= 5.80
BuildRequires:  perl-Catalyst-Model-Adaptor
BuildRequires:  perl-Catalyst-Model-DBIC-Schema
BuildRequires:  perl-Catalyst-Plugin-Authentication
BuildRequires:  perl-Catalyst-Plugin-ConfigLoader
BuildRequires:  perl-Catalyst-Plugin-I18N
BuildRequires:  perl-Catalyst-Plugin-Session
BuildRequires:  perl-Catalyst-Plugin-Session-State-Cookie
BuildRequires:  perl-Catalyst-Plugin-Session-Store-DBIC
BuildRequires:  perl-Catalyst-Plugin-Session-Store-File
BuildRequires:  perl-Catalyst-Plugin-StackTrace
BuildRequires:  perl-Catalyst-Plugin-Static-Simple
BuildRequires:  perl-Catalyst-View-Email
BuildRequires:  perl-Catalyst-View-TT
BuildRequires:  perl-CatalystX-Component-Traits
BuildRequires:  perl-CatalystX-LeakChecker
BuildRequires:  perl-CatalystX-Profile
BuildRequires:  perl-CatalystX-REPL
BuildRequires:  perl-CatalystX-SimpleLogin
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-FCGI
BuildRequires:  perl-FCGI-ProcManager
BuildRequires:  perl-local-lib
BuildRequires:  perl-Test-WWW-Mechanize-Catalyst
Requires:       perl-Catalyst-Runtime >= 5.90042
Requires:       perl-Catalyst-Action-REST
Requires:       perl-Catalyst-ActionRole-ACL
Requires:       perl-Catalyst-Authentication-Credential-HTTP
Requires:       perl-Catalyst-Authentication-Store-DBIx-Class
Requires:       perl-Catalyst-Component-InstancePerContext
Requires:       perl-Catalyst-Controller-ActionRole
Requires:       perl-Catalyst-Devel >= 1.26
Requires:       perl-Catalyst-Engine-HTTP-Prefork
Requires:       perl-Catalyst-Engine-PSGI
Requires:       perl-Catalyst-Manual >= 5.80
Requires:       perl-Catalyst-Model-Adaptor
Requires:       perl-Catalyst-Model-DBIC-Schema
Requires:       perl-Catalyst-Plugin-Authentication
Requires:       perl-Catalyst-Plugin-ConfigLoader
Requires:       perl-Catalyst-Plugin-I18N
Requires:       perl-Catalyst-Plugin-Session
Requires:       perl-Catalyst-Plugin-Session-State-Cookie
Requires:       perl-Catalyst-Plugin-Session-Store-DBIC
Requires:       perl-Catalyst-Plugin-Session-Store-File
Requires:       perl-Catalyst-Plugin-StackTrace
Requires:       perl-Catalyst-Plugin-Static-Simple
Requires:       perl-Catalyst-View-Email
Requires:       perl-Catalyst-View-TT
Requires:       perl-CatalystX-Component-Traits
Requires:       perl-CatalystX-LeakChecker
Requires:       perl-CatalystX-Profile
Requires:       perl-CatalystX-REPL
Requires:       perl-CatalystX-SimpleLogin
Requires:       perl-FCGI
Requires:       perl-FCGI-ProcManager
Requires:       perl-local-lib
Requires:       perl-Test-WWW-Mechanize-Catalyst
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Installs everything you need to write serious Catalyst applications.

%prep
%setup -q -n Task-Catalyst-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Task/Catalyst.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-11m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-10m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-9m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-8m)
- remove perl-Catalyst-Plugin-Unicode-Encoding from BuildRequires and Requires

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-2m)
- rebuild against perl-5.14.2

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-1m)
- update to 4.02

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.01-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.01-2m)
- rebuild for new GCC 4.5

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01-1m)
- update to 4.01

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.00-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00-1m)
- update to 4.00

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0000-6m)
- rebuild against perl-5.12.1

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0000-5m)
- add BuildRequires:  perl-FCGI
- add BuildRequires:  perl-Catalyst-Plugin-Authorization-ACL
- add BuildRequires:  perl-Catalyst-Plugin-Authorization-Roles

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0000-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0000-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0000-2m)
- rebuild against perl-5.10.1

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0000-1m)
- update to 3.0000
- modify BuildRequires: and Requires:
- perl-Catalyst-Plugin-Authentication-Store-DBIC was DEPRECATED

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.90-7m)
- rebuild against rpm-4.6

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.90-6m)
- change Source0 URI

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.90-5m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.90-4m)
- use vendor

* Thu Jan  4 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.90-3m)
- add BuildRequires:  perl-Catalyst-Plugin-Authentication-Store-Htpasswd

* Mon Oct 02 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.90-2m)
- add BuildRequires:  perl-Catalyst-Plugin-Authorization-Roles
- add BuildRequires:  perl-Catalyst-Model-DBIC-Schema
- add BuildRequires:  perl-Catalyst-Plugin-I18N
- add BuildRequires:  perl-Catalyst-Plugin-Authentication-Store-DBIC
- add BuildRequires:  perl-Catalyst-Runtime
- add Requires:  perl-Catalyst-Runtime

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.90-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
