# Generated from bluecloth-2.2.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname bluecloth

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: BlueCloth is a Ruby implementation of John Gruber's Markdown[http://daringfireball.net/projects/markdown/], a text-to-HTML conversion tool for web writers
Name: rubygem-%{gemname}
Version: 2.2.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: BSD
URL: http://deveiate.org/projects/BlueCloth
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby >= 1.8.7
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby >= 1.8.7
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
BlueCloth is a Ruby implementation of John Gruber's
Markdown[http://daringfireball.net/projects/markdown/], a text-to-HTML
conversion tool for web writers. To quote from the project page: Markdown
allows you to write using an easy-to-read, easy-to-write plain text format,
then convert it to structurally valid XHTML (or HTML).
It borrows a naming convention and several helpings of interface from
{Redcloth}[http://redcloth.org/], Why the Lucky Stiff's processor for a
similar text-to-HTML conversion syntax called
Textile[http://www.textism.com/tools/textile/].
BlueCloth 2 is a complete rewrite using David Parsons'
Discount[http://www.pell.portland.or.us/~orc/Code/discount/] library, a C
implementation of Markdown. I rewrote it using the extension for speed and
accuracy; the original BlueCloth was a straight port from the Perl version
that I wrote in a few days for my own use just to avoid having to shell out to
Markdown.pl, and it was quite buggy and slow. I apologize to all the good
people that sent me patches for it that were never released.
Note that the new gem is called 'bluecloth' and the old one 'BlueCloth'. If
you have both installed, you can ensure you're loading the new one with the
'gem' directive:
# Load the 2.0 version
gem 'bluecloth', '>= 2.0.0'
# Load the 1.0 version
gem 'BlueCloth'
require 'bluecloth'


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            -V \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x
# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%files
%dir %{geminstdir}
%{_bindir}/bluecloth
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/History.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.0-1m)
- update 2.2.0

* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-1m)
- udpate 2.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.9-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.9-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.9-1m)
- update to 2.0.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-1m)
- renamed to rubygem-bluecloth from ruby-bluecloth
- update to 2.0.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-1m)
- update 2.0.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2m)
- rebuild against gcc43

* Sun Nov  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-1m)
- initial import to Momonga
