%global         momorel 3

Name:           perl-POE-Filter-IRCD
Version:        2.44
Release:        %{momorel}m%{?dist}
Summary:        POE-based parser for the IRC protocol
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/POE-Filter-IRCD/
Source0:        http://www.cpan.org/authors/id/B/BI/BINGOS/POE-Filter-IRCD-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-POE >= 0.3202
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl-POE >= 0.3202
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
POE::Filter::IRCD provides a convenient way of parsing and creating IRC
protocol lines. It provides the parsing engine for
POE::Component::Server::IRC and POE::Component::IRC. A standalone version
exists as Parse::IRC.

%prep
%setup -q -n POE-Filter-IRCD-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/POE/Filter/IRCD.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.44-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.44-2m)
- rebuild against perl-5.18.2

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.44-1m)
- update to 2.44

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-16m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-15m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-14m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-13m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-12m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-11m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.42-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.42-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.42-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-2m)
- rebuild against perl-5.12.0

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-1m)
- update to 2.42

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.40-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.40-2m)
- rebuild against perl-5.10.1

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.40-1m)
- update to 2.40

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.38-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.38-1m)
- update to 2.38

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36-1m)
- update to 2.36

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.35-2m)
- rebuild against gcc43

* Fri Jan  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.35-1m)
- update to 2.35

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.34-1m)
- update to 2.34

* Fri Nov  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32-1m)
- update to 2.32

* Sun Sep  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-1m)
- update to 2.31
- do not use %%NoSource macro

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.30-1m)
- update to 2.30

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-1m)
- spec file was autogenerated
