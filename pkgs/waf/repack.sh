#! /bin/bash

version=1.6.6

src=waf-${version}.tar.bz2
dst=${src%.tar.bz2}.stripped.tar.bz2

rm -f ${dst} ${dst%.bz2}
cp -av ${src} ${dst}

bzip2 -d ${dst}

tar --delete --file=${dst%.bz2} \
     waf-${version}/docs/book \
     waf-${version}/waflib/extras/subprocess.py

bzip2 ${dst%.bz2}
