%global momorel 1

%global with_python3 1
# Turn off the brp-python-bytecompile script
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

# Enable building without html docs (e.g. in case no recent sphinx is
# available)
%global with_docs 1

Name:           waf
Version:        1.6.10
Release:        %{momorel}m%{?dist}
Summary:        A Python-based build system
Group:          Development/Tools
# The entire source code is BSD apart from pproc.py (taken from Python 2.5)
License:        BSD and "Python"
URL:            http://code.google.com/p/waf/
Source0:        http://waf.googlecode.com/files/waf-%{version}.tar.bz2
NoSource:	0
# use _datadir instead of /usr/lib
Patch0:         waf-1.6.2-libdir.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python3-devel
BuildRequires:  python-sphinx
BuildRequires:  graphviz
BuildRequires:  ImageMagick

# Seems like automatic ABI dependency is not detected since the files are
# going to a non-standard location
Requires:       python-abi %(%{__python} -c "import sys ; print \"=\", sys.version[:3]")

# the demo suite contains a perl module, which draws in unwanted
# provides and requires
%global __perl_provides %{nil}
%global __perl_requires %{nil}


%description
Waf is a Python-based framework for configuring, compiling and
installing applications. It is a replacement for other tools such as
Autotools, Scons, CMake or Ant.


%prep
%setup -q
%patch0 -p0 -b .libdir


%build
extras=
for f in waflib/extras/*.py ; do
  f=$(basename "$f" .py);
  if [ "$f" != "__init__" ]; then
    extras="${extras:+$extras,}$f" ;
  fi
done
./waf-light --make-waf --strip --tools="$extras"

%if 0%{?with_docs}
# build html docs
pushd docs/sphinx
../../waf configure build
popd
%endif # with_docs


%install
rm -rf %{buildroot}

# use waf so it unpacks itself
mkdir _temp ; pushd _temp
cp -av ../waf .
%{__python} ./waf >/dev/null 2>&1
pushd .waf-%{version}-*
find . -name '*.py' -printf '%%P\0' |
  xargs -0 -I{} install -m 0644 -p -D {} %{buildroot}%{_datadir}/waf/{}
popd
%if 0%{?with_python3}
%{__python3} ./waf >/dev/null 2>&1
pushd .waf3-%{version}-*
find . -name '*.py' -printf '%%P\0' |
  xargs -0 -I{} install -m 0644 -p -D {} %{buildroot}%{_datadir}/waf3/{}
popd
%endif # with_python3
popd
install -m 0755 -p -D waf-light %{buildroot}%{_bindir}/waf

# remove shebangs from and fix EOL for all scripts in wafadmin
find %{buildroot}%{_datadir}/ -name '*.py' \
     -exec sed -i -e '1{/^#!/d}' -e 's|\r$||g' {} \;

# fix waf script shebang line
sed -i "1c#! /usr/bin/python" %{buildroot}%{_bindir}/waf

# remove x-bits from everything going to doc
find demos utils -type f -exec %{__chmod} 0644 {} \;

# remove hidden file
rm -f docs/sphinx/build/html/.buildinfo

%if 0%{?with_python3}
# do byte compilation
%py_byte_compile %{__python} %{buildroot}%{_datadir}/waf
%py_byte_compile %{__python3} %{buildroot}%{_datadir}/waf3
%endif # with_python3


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README TODO ChangeLog demos
%if 0%{?with_docs}
%doc docs/sphinx/build/html
%endif # with_docs
%{_bindir}/waf
%{_datadir}/waf
%if 0%{?with_python3}
%{_datadir}/waf3
%endif # with_python3


%changelog
* Tue Dec 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.10-1m)
- update 1.6.10

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.9-1m)
- update 1.6.9

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.8-1m)
- update 1.6.8

* Sun Sep 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.7-1m)
- update 1.6.7

* Mon Jul 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6-1m)
- update 1.6.6

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.18-7m)
- rebuild against xfce4-4.8

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.18-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.18-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.18-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.18-3m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.18-2m)
- rebuild against xfce4 4.6.2

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.18-1m)
- update to 1.5.18

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.6-3m)
- rebuild against xfce4 4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.6-1m)
- update to 1.5.6

* Sat Mar  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.3-2m)
- release %%{_sysconfdir}/bash_completion.d

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.3-1m)
- import from Fedora to Momonga

* Mon Feb  2 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.5.3-1
- Update to 1.5.3, which contains various enhancements and bugfixes,
  see http://waf.googlecode.com/svn/trunk/ChangeLog for a list of
  changes.

* Fri Jan 16 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.5.2-2
- Remove the documentation again, as it is under CC-BY-NC-ND. Also
  remove it from the tarfile.

* Fri Jan 16 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.5.2-1
- Update to 1.5.2.
- Generate html documentation (though without highlighting).

* Fri Dec 19 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.5.1-1
- Update to 1.5.1.

* Mon Dec 01 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.4.4-2
- Rebuild for Python 2.6

* Sun Aug 31 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.4.4-1
- Update to 1.4.4:
  - python 2.3 compatibility was restored
  - task randomization was removed
  - the vala tool was updated

* Sat Jun 28 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.4.3-1
- Update to 1.4.3.
- Remove fcntl patch (fixed upstream).
- Prefix has to be set in a configure step now.
- Pack the bash completion file.

* Mon May 26 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.4.2-2
- Patch: stdout might not be a terminal.

* Sat May 17 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.4.2-1
- Update to 1.4.2.
- Remove shebang lines from files in wafadmin after installation, not
  before, otherwise install will re-add them.

* Sun May  4 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.4.1-1
- Update to upstream version 1.4.1.

* Sat Apr 19 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.4.0-1
- Update to upstream version 1.4.0.

* Wed Apr  9 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.3.2-6
- Upstream patch to fix latex dependency scanning: trunk rev 2340.

* Sun Feb 10 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.3.2-5
- Update to 1.3.2.
- Remove version and revision information from path to waf cache.

* Fri Feb  1 2008 Michel Salim <michel.sylvan@gmail.com> - 1.3.1-4
- Upstream patch to fix check_tool('gnome'): trunk rev 2219

* Mon Jan 28 2008 Michel Salim <michel.sylvan@gmail.com> - 1.3.1-3
- Fix python-abi requirement so it can be parsed before python is installed
- rpmlint tidying-up

* Fri Jan 25 2008 Michel Salim <michel.sylvan@gmail.com> - 1.3.1-2
- Merge in changes from Thomas Mochny <thomas.moschny@gmx.de>:
  * WAF cache moved from /usr/lib to /usr/share
  * Remove shebangs from scripts not meant from users, rather than
    making them executable
  * Include tools and demos

* Sun Jan 20 2008 Michel Salim <michel.sylvan@gmail.com> - 1.3.1-1
- Initial Fedora package

