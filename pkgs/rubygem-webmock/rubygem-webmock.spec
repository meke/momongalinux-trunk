# Generated from webmock-1.8.5.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname webmock

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Library for stubbing HTTP requests in Ruby
Name: rubygem-%{gemname}
Version: 1.8.5
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/bblimke/webmock
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(addressable) >= 2.2.7
Requires: rubygem(crack) >= 0.1.7
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
WebMock allows stubbing HTTP requests and setting expectations on HTTP
requests.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.5-1m)
- update 1.8.5

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.10-1m)
- update 1.7.10

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.7-1m)
- udpate 1.7.7

* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.6-1m)
- update 1.7.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update 1.5.0

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update 1.4.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-1m)
- Initial package for Momonga Linux
