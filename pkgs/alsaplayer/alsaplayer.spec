%global momorel 2

Summary: A advanced, graphical PCM audio player
Name: alsaplayer
Version: 0.99.81
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://www.alsaplayer.org/
Source0: http://www.alsaplayer.org/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: %{name}-large.png
Source2: %{name}-small.png
Source3: %{name}.png
Patch0: %{name}-%{version}-jp.patch
Patch1: %{name}-0.99.80-desktop.patch
Patch2: %{name}-0.99.80-glibc28.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: audiofile-devel
BuildRequires: doxygen
BuildRequires: esound-devel
BuildRequires: flac-devel >= 1.1.4
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: jack-devel
BuildRequires: libid3tag-devel
BuildRequires: libmad
BuildRequires: libsndfile-devel
BuildRequires: libvorbis-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mikmod
BuildRequires: libmikmod >= 3.2.0
BuildRequires: pkgconfig
BuildRequires: xosd

%description
AlsaPlayer is a new PCM player developed on the Linux Operating
System. AlsaPlayer was written in the first place to excercise the new
ALSA (Advanced Linux Sound Architecture) driver and library system.

It has now developed into a versitile audio player with rich plugin system.
The  Input Plugins plugins include: OGG, MPEG, MAD, CDDA, MikMod, and
Audiofile. The Output Plugins include: ALSA, OSS and OSS/Lite, Esound,
Sparc (tested on UltraSparc), SGI, and JACK. There are also a few scope
plugins included.

%package devel
Summary: Files needed for building applications with libalsaplayer
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The functionality of the alsaplayer is also exposed via a c programming
library. This package is neede to compile programs that uses the library.

%prep
%setup -q

%patch0 -p1 -b .jp
%patch1 -p1 -b .desktop~
%patch2 -p1 -b .glibc28

export CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE"
%build
%configure --enable-jconv=yes
%make

%install
%{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot}

# inastall icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/pixmaps/

# for doc
mkdir -p install-doc
mv -f %{buildroot}%{_docdir}/%{name} install-doc/

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README TODO docs/*txt
%{_bindir}/alsaplayer
%{_libdir}/alsaplayer
%{_libdir}/*.so.*
%{_datadir}/applications/alsaplayer.desktop
%{_datadir}/man/man1/alsaplayer.1*
%{_datadir}/pixmaps/alsaplayer.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%files devel
%defattr(-,root,root)
%doc install-doc/alsaplayer/reference
%{_includedir}/alsaplayer
%{_libdir}/pkgconfig/alsaplayer.pc
%{_libdir}/*.so

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.81-2m)
- rebuild for glib 2.33.2

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.81-1m)
- update to 0.99.81

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.80-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.80-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.80-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.80-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.80-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.80-7m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.80-6m)
- rebuild against rpm-4.6

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.80-5m)
- change BR from libid3tag to libid3tag-devel

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.80-4m)
- rebuild against libmikmod.so.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.80-3m)
- rebuild against gcc43

* Mon Mar  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.80-2m)
- fix glibc-2.8 error

* Sun Nov 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.80-1m)
- [SECURITY] CVE-2007-5301
- update to 0.99.80

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.79-4m)
- rebuild against libvorbis-1.2.0-1m

* Tue Jul 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.79-3m)
- import 03_flacstream.dpatch from debian unstable

* Mon Jul 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.79-2m)
- enable flac support again
- add %%doc to devel package
- fix alsaplayer.desktop
- add icons
- http://lists.tartarus.org/pipermail/alsaplayer-devel/2007-February/001939.html

* Mon May 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.99.79-1m)
- [SECURITY] CVE-2006-4089
- update to 0.99.79

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.77-1m)
- version 0.99.77
- build without flac-1.1.4

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.76-7m)
- delete libtool library

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.76-6m)
- rebuild against flac-1.1.2

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.76-5m)
- rebuild against flac 1.1.1
- BuildPreReq: flac-devel >= 1.1.1

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.99.76-4m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Wed May 5 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.99.76-3m)
- add BuildPreReq xosd

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.99.76-2m)
- revised spec for enabling rpm 4.2.
- DONATIONS is nolonger deliverred.

* Thu Dec 25 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99.76-1m)
- upgrade 0.99.76

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.99.75-2m)
- use libmad, libid3tag

* Fri May  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.99.75-1m)

* Thu Mar  6 2003 Shingo Akagaqki <dora@kitty.dnsalias.org>
- (0.99.74-2m)
- [Momonga-devel.ja:01456]

* Tue Feb 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.99.74-1m)
- major feature enhancements

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.99.73-1m)
- minor feature enhencements

* Thu Sep 19 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.99.72-1m)
- minor bugfixes

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.99.71-2m)
- rebuild against alsa-lib-0.9.0
- never use 'AutoReqProv: no'

* Mon Aug 26 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.99.71-1m)
- version up

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.99.70-8k)
- cancel gcc-3.1 autoconf-2.53

* Thu May 23 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.99.70-6k)
  applied gcc 3.1 patch

* Mon May 20 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.99.70-4k)
  can't apply patch0, 2.
  boroboro B-<

* Mon May 20 2002 Kenta MURATA <muraken@kondara.org>
- (0.99.70-2k)
- version up.

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.99.50-6k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Thu Jan 10 2002 Shingo Akagaki <dora@kondara.org>
- (0.99.50-4k)
- automake autoconf 1.5

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (0.99.50-2k)
- up to 0.99.50 because
  tar xzf alsaplayer09932 -> alsaplayer09950 TT

* Wed Apr 11 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.99.32-10k)
- add alsaplayer.ppc.patch

* Tue Dec  5 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.99.32-8k)
- fixed JITTERUBG #799

* Fri Nov 17 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- Fix %doc

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.99.32-5k)
- fixed for FHS

* Sat Jun 17 2000 Uechi Yasumasa <uh@kondara.org>
- fix spec file for Kondara
- jp patch
* Tue Oct 19 1999 Miles Lott <milos@insync.net>
- added Provides: libasound.so.0 line to fix
- that require.
- Removed /usr/lib/libasound.so.0 from files and
- ldconfig from spec file post and postun sections
- Altered version/release to adhere to convention
* Mon Oct 4 1999 0.99.28
- Fixes in the Mpg123 and WAV plugins
- Input plugins are now pure C, easier development
- Playlist works better
- Multiple file select in dialogs
- Rewrote Levelmeter and spacescope, less code
- Accepts files on the command line now
- NULL output plugin, for testing purposes
- Many other minor bug fixes
