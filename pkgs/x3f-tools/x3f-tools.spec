%global momorel 1
%global date 20120807
%global srcname x3f
%global zipname rolkar-%{srcname}-2bf3328
%global kdever 4.9.0
%global kdelibsrel 1m

Summary: Tools for SIGMA Feveon X3 X3F RAW files
Name: x3f-tools
Version: 041
Release: 0.%{date}.%{momorel}m%{?dist}
License: BSD
Group: Applications/Multimedia
URL: http://www.proxel.se/x3f.html
Source0: %{zipname}.zip
Source1: %{srcname}_extract_to_jpg.desktop
Source2: %{srcname}_extract_to_raw.desktop
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# for macros.kde4
BuildRequires: cmake
BuildRequires: coreutils

%description
The info tool

This is really a test program - doing nothing of any great value. It shows how to use the X3F IO library for reading and writing X3F files. It also dumps some information about the file. It is used thus:

x3f_io_test foo.x3f

The extract tool

This tool can extract the RAW Foveon sensor data and dump it as TIFF (or RAW). By default the data is scaled and gamma coded. It can also dump the JPEG (thumbnail) file. It is used thus:

x3f_extract -raw [-noscale] foo.x3f

x3f_extract -jpg foo.x3f

The first will create a file called foo.x3f.tif and the second foo.x3f.jpg. The x3f_extract tool can take several input files, i.e. work in batch mode.

DP1 RAW extraction unsupported...

%package kde
Summary: X3F extracting support for Dolphin File Manager
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: oxygen-icons

%description kde
X3F extracting service menus for Dolphin File Manager.

%prep
%setup -q -n %{zipname}

%build
make CFLAGS="%{optflags}"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# install x3f_extract and x3f_io_test
mkdir -p %{buildroot}%{_bindir}
install -m 755 bin/linux/%{srcname}_extract %{buildroot}%{_bindir}/
install -m 755 bin/linux/%{srcname}_io_test %{buildroot}%{_bindir}/
install -m 755 bin/linux/%{srcname}_merge %{buildroot}%{_bindir}/

# install Dolphin support
mkdir -p %{buildroot}%{_kde4_datadir}/kde4/services/ServiceMenus
install -m 644 %{SOURCE1} %{buildroot}%{_kde4_datadir}/kde4/services/ServiceMenus/
install -m 644 %{SOURCE2} %{buildroot}%{_kde4_datadir}/kde4/services/ServiceMenus/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc doc/changes.txt doc/copyright.txt doc/readme.txt doc/todo.txt
%{_bindir}/%{srcname}_extract
%{_bindir}/%{srcname}_io_test
%{_bindir}/%{srcname}_merge

%files kde
%defattr(-,root,root)
%{_kde4_datadir}/kde4/services/ServiceMenus/%{srcname}_extract_to_jpg.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/%{srcname}_extract_to_raw.desktop

%changelog
* Tue Aug  7 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (041-0.20120807.1m)
- version 041, add SD1 RAW extract support

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (016-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (016-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (016-5m)
- full rebuild for mo7 release

* Tue Jun 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (016-4m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (016-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (016-2m)
- build with optflags
- add Dolphin support as a package x3f-tools-kde

* Thu Oct 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (016-1m)
- initial package for Feveon X3 freaks using Momonga Linux
