%global momorel 1

Summary: libdrm Direct Rendering Manager runtime library
Name: libdrm
Version: 2.4.54
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://dri.freedesktop.org/
# No .bz2 avail upstream 	 
Source0: http://dri.freedesktop.org/libdrm/%{name}-%{version}.tar.bz2
NoSource: 0

Source2: 91-drm-modeset.rules

# hardcode the 666 instead of 660 for device nodes
Patch3: libdrm-make-dri-perms-okay.patch
# remove backwards compat not needed on Fedora
Patch4: libdrm-2.4.0-no-bc.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires: xorg-x11-proto-devel
#BuildRequires: xorg-x11-xtrans-devel

BuildRequires: pkgconfig
BuildRequires: libX11-devel
Requires: kernel >= 2.6.32
Requires: udev

#Obsoletes: XFree86-libs, xorg-x11-libs

%description
libdrm Direct Rendering Manager runtime library

%package devel
Summary: libdrm-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kernel-headers >= 2.6.27

#Obsoletes: XFree86-devel, xorg-x11-devel

%description devel
libdrm Direct Rendering Manager development package

%prep
# %setup -q -n libdrm
%setup -q
%patch3 -p1 -b .forceperms
%patch4 -p1 -b .no-bc

%build
autoreconf -if
%configure --enable-udev --enable-kms \
           --enable-radeon-experimental-api \
           --enable-vmwgfx-experimental-api \
           --enable-freedreno-experimental-api

# --enable-nouveau-experimental-api \
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=$RPM_BUILD_ROOT
# FIXME: makeinstall doesn't work due to upstream bug
#%%makeinstall DESTDIR=$RPM_BUILD_ROOT

# SUBDIRS=libdrm
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d/
install -m 0644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d/

# NOTE: We intentionally don't ship *.la files
find $RPM_BUILD_ROOT -type f -name '*.la' | xargs rm -f -- || :
for i in r300_reg.h via_3d_reg.h
do
 rm -f $RPM_BUILD_ROOT%{_includedir}/drm/$i
done

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README
%{_libdir}/libdrm.so.2
%{_libdir}/libdrm.so.2.4.0
%{_libdir}/libdrm_freedreno.so.1
%{_libdir}/libdrm_freedreno.so.1.0.0
%{_libdir}/libdrm_intel.so.1
%{_libdir}/libdrm_intel.so.1.0.0
%{_libdir}/libdrm_radeon.so.1
%{_libdir}/libdrm_radeon.so.1.0.1
%{_libdir}/libdrm_nouveau.so.2
%{_libdir}/libdrm_nouveau.so.2.0.0
%{_libdir}/libkms.so.1
%{_libdir}/libkms.so.1.0.0
%config %{_sysconfdir}/udev/rules.d/91-drm-modeset.rules

%files devel
%defattr(-,root,root,-)
%{_mandir}/man3/*.3.*
%{_mandir}/man7/*.7.*
# FIXME should be in drm/ too
%{_includedir}/xf86drm.h
%{_includedir}/xf86drmMode.h
%dir %{_includedir}/libdrm
%{_includedir}/libdrm/drm.h
%{_includedir}/libdrm/drm_fourcc.h
%{_includedir}/libdrm/drm_mode.h
%{_includedir}/libdrm/drm_sarea.h
%{_includedir}/libdrm/intel_aub.h
%{_includedir}/libdrm/intel_bufmgr.h
%{_includedir}/libdrm/intel_debug.h
%{_includedir}/libdrm/nouveau.h
%{_includedir}/libdrm/r600_pci_ids.h
%{_includedir}/libdrm/radeon*.h
%{_includedir}/libdrm/*_drm.h
%{_includedir}/freedreno//*.h
%dir %{_includedir}/libkms
%{_includedir}/libkms/libkms.h
%{_libdir}/libdrm.so
%{_libdir}/libdrm_freedreno.so
%{_libdir}/libdrm_intel.so
%{_libdir}/libdrm_radeon.so
%{_libdir}/libdrm_nouveau.so
%{_libdir}/libkms.so
%{_libdir}/pkgconfig/libdrm.pc
%{_libdir}/pkgconfig/libdrm_freedreno.pc
%{_libdir}/pkgconfig/libdrm_intel.pc
%{_libdir}/pkgconfig/libdrm_radeon.pc
%{_libdir}/pkgconfig/libdrm_nouveau.pc
%{_libdir}/pkgconfig/libkms.pc

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.54-1m)
- update 2.4.54

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.52-2m)
- add freedreno support

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.52-1m)
- update 2.4.52

* Tue Jan 14 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.51-1m)
- update 2.4.51

* Fri Nov 29 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.49-1m)
- update 2.4.49

* Tue Nov 19 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.48-1m)
- update 2.4.48

* Mon Oct 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.47-1m)
- update 2.4.47

* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.46-1m)
- update 2.4.46

* Thu Jun  6 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.45-1m)
- update 2.4.45

* Sun Feb 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.42-1m)
- update 2.4.42

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.40-1m)
- update 2.4.40

* Mon Aug 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.39-1m)
- update 2.4.39

* Sat Jun 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.37-1m)
- update 2.4.37

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.34-1m)
- update 2.4.34

* Fri Mar 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.33-1m)
- update 2.4.33

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.31-1m)
- update 2.4.31

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.30-1m)
- update 2.4.30

* Mon Dec 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.29-1m)
- update 2.4.29

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.27-1m)
- update 2.4.27

* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.26-2m)
- change URL

* Sun Jun 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.26-1m)
- update 2.4.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.24-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.24-1m)
- update 2.4.24

* Tue Dec 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.23-1m)
- update 2.4.23

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.22-2m)
- rebuild for new GCC 4.5

* Tue Oct  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.22-1m)
- update 2.4.22

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.21-4m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.21-3m)
- enable-kms

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.21-2m)
- install *_drm.h

* Sat Jun 12 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.21-1m)
- update 2.4.21

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.20-1m)
- fix %%files

* Thu Apr  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.20-1m)
- update 2.4.20

* Wed Mar 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.19-1m)
- update 2.4.19

* Sun Feb 28 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.18-1m)
- update 2.4.18

* Wed Jan  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.17-1m)
- update 2.4.17

* Tue Dec  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.16-1m)
- update 2.4.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.15-1m)
- update 2.4.15

* Mon Oct  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.14-1m)
- update 2.4.14

* Wed Sep  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.13-1m)
- update 2.4.13

* Fri Jul 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.12-1m)
- update 2.4.12

* Fri May 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.11-1m)
- update 2.4.11

* Thu Apr  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.6-1m)
- update 2.4.6

* Fri Mar 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.5-2m)
- add radeon modesetting patch
- support nVidia Nouveau driver

* Tue Mar  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.5-1m)
- update 2.4.5

* Sat Jan 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.4-1m)
- update 2.4.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-2m)
- rebuild against rpm-4.6

* Sat Dec 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.3-1m)
- update 2.4.3

* Wed Nov 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-2m)
- support kernel-2.6.27

* Sat Nov  1 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-1m)
- update 2.4.1

* Thu Oct 16 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.0-1m)
- update 2.4.0

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-6m)
- update 2.3.1-20081012

* Fri Sep 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-5m)
- update 2.3.1-20080912

* Fri Aug 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-4m)
- update 2.3.1-20080829

* Thu Aug 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-3m)
- update 2.3.1-20080821

* Sun Jul 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-2m)
- update 2.3.1-20080720

* Thu Jul 10 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-1m)
- update 2.3.1-stable

* Thu Jul  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.0-22m)
- rewind 2.3.0

* Thu Jul  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-1m)
- update 2.3.1

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-21m)
- update 20080628

* Wed Jun 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-20m)
- update 20080618
-- support Intel G4x Chipset

* Mon May 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-19m)
- update 20080526

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-18m)
- update 20080503

* Sun Apr  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-17m)
- update 20080406

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.0-16m)
- rebuild against gcc43

* Wed Mar  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-15m)
- update 20080304

* Sun Feb  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-14m)
- update 20080203

* Sun Jan  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-13m)
- update 20080106

* Thu Nov 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-12m)
- update 20071129

* Fri Nov 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-11m)
- update 20071116

* Sun Jul 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-10m)
- update 20070729

* Sat Jun 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-9m)
- update 20070616

* Sun Apr 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-8m)
- update 20070429

* Thu Apr 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-7m)
- update 20070412

* Tue Mar 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-6m)
- update 20070313

* Sat Mar  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-5m)
- update 20070303

* Wed Feb  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-4m)
- update 20070206

* Thu Jan 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-3m)
- update 2.3.0-20080110

* Sun Dec 31 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-2m)
- update 2.3.0-20061229
- support nouveau(OSS nVidia Driver)

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-1m)
- update 2.3.0

* Tue Sep 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-0.2m)
- update 20060923-git

* Mon Aug 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-0.1m)
- update 20060809-git
-- needs Intel i965 Driver

* Thu Jun 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-1m)
- update 2.0.2

* Wed Jun  7 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-3m)
- delete duplicated dir

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-2m)
- delete duplicated dir

* Thu Apr  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-3m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-2.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.0-2.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.0-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 11 2006 Mike A. Harris <mharris@redhat.com> 2.0-2
- Replaced the temporary tongue-in-cheek humourous package summary and
  description with the proper package descriptions, as many people didn't get
  the joke, while others felt it was getting old.  Ah well, I had my fun for
  a while anyway.  ;o)

* Wed Nov 30 2005 Mike A. Harris <mharris@redhat.com> 2.0-1
- Updated libdrm to version 2.0 from dri.sf.net.  This is an ABI incompatible
  release, meaning everything linked to it needs to be recompiled.

* Tue Nov  1 2005 Mike A. Harris <mharris@redhat.com> 1.0.5-1
- Updated libdrm to version 1.0.5 from dri.sf.net upstream to work around
  mesa unichrome dri driver compile failure.

* Mon Oct 24 2005 Mike A. Harris <mharris@redhat.com> 1.0.4-1
- Updated libdrm to version 1.0.4 from X11R7 RC1
- Remove i915_drv.h, imagine_drv.h, mach64_drv.h, mga_drv.h, mga_ucode.h,
  r128_drv.h, radeon_drv.h, savage_drv.h, sis_drv.h, sis_ds.h, tdfx_drv.h,
  via_drv.h, via_ds.h, via_mm.h, via_verifier.h from file manifest.

* Tue Oct 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.3-3
- Update BuildRoot to use Fedora Packaging Guidelines.
- Add missing "BuildRequires: libX11-devel, pkgconfig"

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 1.0.3-2
- Add missing documentation to doc macro
- Fix spec file project URL

* Sat Sep 3 2005 Mike A. Harris <mharris@redhat.com> 1.0.3-1
- Initial build.
