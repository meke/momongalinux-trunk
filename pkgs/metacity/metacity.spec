%global momorel 1
%define _default_patch_fuzz 999

Summary: Unobtrusive window manager
Name: metacity
Version: 2.34.13
Release: %{momorel}m%{?dist}
URL: http://download.gnome.org/sources/metacity/
Source0: http://download.gnome.org/sources/metacity/2.34/metacity-%{version}.tar.xz
NoSource: 0
# http://bugzilla.gnome.org/show_bug.cgi?id=558723
Patch4: stop-spamming-xsession-errors.patch
# http://bugzilla.gnome.org/show_bug.cgi?id=135056
Patch5: dnd-keynav.patch

# fedora specific patches
Patch12: fresh-tooltips.patch

# https://bugzilla.gnome.org/show_bug.cgi?id=598995
Patch16: Dont-focus-ancestor-window-on-a-different-workspac.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=599097
Patch18: For-mouse-and-sloppy-focus-return-to-mouse-mode-on.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=599248
Patch19: Add-nofocuswindows-preference-to-list-windows-that.patch
Patch119: Exclude-the-current-application-from-no_focus_window.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=599261
Patch20: Add-a-newwindowsalwaysontop-preference.patch
Patch120: Apply-new_windows_always_on_top-to-newly-raised-acti.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=559816
Patch24: metacity-2.28-empty-keybindings.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=604319
Patch25: metacity-2.28-xioerror-unknown-display.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=599181
Patch28: Stop-confusing-GDK-s-grab-tracking.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=622517
Patch29: Allow-breaking-out-from-maximization-during-mouse.patch

Source1: window.png
Source2: mini-window.png

License: GPLv2+
Group: User Interface/Desktops
BuildRequires: gtk2-devel 
BuildRequires: pango-devel 
BuildRequires: fontconfig-devel
BuildRequires: gsettings-desktop-schemas-devel
BuildRequires: desktop-file-utils
BuildRequires: libglade2-devel 
BuildRequires: autoconf, automake, libtool, gnome-common
BuildRequires: intltool
BuildRequires: startup-notification-devel 
BuildRequires: libtool automake autoconf gettext
BuildRequires: xorg-x11-proto-devel 
BuildRequires: libSM-devel, libICE-devel, libX11-devel
BuildRequires: libXext-devel, libXinerama-devel, libXrandr-devel, libXrender-devel
BuildRequires: libXcursor-devel
BuildRequires: libXcomposite-devel, libXdamage-devel
# for gnome-keybindings.pc
BuildRequires: control-center 
BuildRequires: gnome-doc-utils
BuildRequires: zenity
BuildRequires: dbus-devel
BuildRequires: libcanberra-devel

Requires: startup-notification 
Requires: gsettings-desktop-schemas
# for /usr/share/control-center/keybindings, /usr/share/gnome/wm-properties
Requires: control-center-filesystem
Requires: zenity

# http://bugzilla.redhat.com/605675
Provides: firstboot(windowmanager) = metacity

%description
Metacity is a window manager that integrates nicely with the GNOME desktop.
It strives to be quiet, small, stable, get on with its job, and stay out of
your attention.

%package devel
Group: Development/Libraries
Summary: Development files for metacity
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the files needed for compiling programs using
the metacity-private library. Note that you are not supposed to write
programs using the metacity-private library, since it is a private
API. This package exists purely for technical reasons.

%prep
%setup -q
%patch4 -p1 -b .stop-spamming-xsession-errors
%patch5 -p1 -b .dnd-keynav
%patch12 -p1 -b .fresh-tooltips

%patch16 -p1 -b .focus-different-workspace
%patch18 -p1 -b .focus-on-motion
%patch19 -p1 -b .no-focus-windows
%patch119 -p1 -b .no-focus-windows-current-app
%patch20 -p1 -b .always-on-top
%patch120 -p1 -b .always-on-top-activate
%patch24 -p1 -b .empty-keybindings
%patch25 -p1 -b .xioerror-unknown-display
%patch28 -p1 -b .grab-tracking
%patch29 -p1 -b .mouse-unmaximize

cp -p %{SOURCE1} %{SOURCE2} src/

# force regeneration
rm -f src/org.gnome.metacity.gschema.valid

%build
CPPFLAGS="$CPPFLAGS -I$RPM_BUILD_ROOT%{_includedir}"
export CPPFLAGS

# Always rerun configure for now
rm -f configure
(if ! test -x configure; then autoreconf -i -f; fi;
 %configure --disable-static --disable-schemas-compile)

SHOULD_HAVE_DEFINED="HAVE_SM HAVE_XINERAMA HAVE_XFREE_XINERAMA HAVE_SHAPE HAVE_RANDR HAVE_STARTUP_NOTIFICATION"

for I in $SHOULD_HAVE_DEFINED; do
  if ! grep -q "define $I" config.h; then
    echo "$I was not defined in config.h"
    grep "$I" config.h
    exit 1
  else
    echo "$I was defined as it should have been"
    grep "$I" config.h
  fi
done

make CPPFLAGS="$CPPFLAGS" LIBS="$LIBS" %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

# the desktop file is not valid, I've complained on metacity-devel-list
#desktop-file-install --vendor "" --delete-original \
#	--dir $RPM_BUILD_ROOT%{_datadir}/applications \
#	$RPM_BUILD_ROOT%{_datadir}/applications/metacity.desktop

%find_lang %{name}

%post
/sbin/ldconfig

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README AUTHORS COPYING NEWS HACKING doc/theme-format.txt doc/metacity-theme.dtd rationales.txt
%{_bindir}/metacity
%{_bindir}/metacity-message
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/GConf/gsettings/metacity-schemas.convert
%{_datadir}/metacity
%{_datadir}/themes/*
%{_datadir}/gnome-control-center/keybindings/*
%{_libdir}/lib*.so.*
%{_mandir}/man1/metacity.1.bz2
%{_mandir}/man1/metacity-message.1.bz2
%{_datadir}/applications/metacity.desktop
%{_datadir}/gnome/wm-properties/metacity-wm.desktop
%{_datadir}/help/*/creating-metacity-themes/*

%files devel
%defattr(-,root,root,-)
%{_bindir}/metacity-theme-viewer
%{_bindir}/metacity-window-demo
%{_includedir}/*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_mandir}/man1/metacity-theme-viewer.1.bz2
%{_mandir}/man1/metacity-window-demo.1.bz2

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.34.13-1m)
- update to 2.34.13

* Sat Oct 13 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.34.8-1m)
- update to 2.34.8
- remove Patch30: disable-mouse-button-modifiers.patch

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.34.3-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.34.1-3m)
- rebuild for glib 2.33.2

* Fri Sep  9 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (2.34.1-2m)
- add patch0 - set a key bindings to run terminal (<Control>+Return)


* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.34.1-1m)
- update to 2.34.1

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.34.0-1m)
- update to 2.34.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.3-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-3m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.30.1-2m)
- fix build

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.144-1m)
- update to 2.25.144

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Jul 23 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.0-4m)
- change %%preun script (add if block)

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-3m)
- add %pre script

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Mon Mar 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Sun Nov 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Sun Sep 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Thu Jul  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.5-1m)
- update to 2.18.5

* Wed Jun 27 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.3-3m)
- remove some PreReq (control-center&gnome-session)

* Sun Jun 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-2m)
- add some PreReq:

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Sat Apr  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.8-1m)
- update to 2.17.8

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-4m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-3m)
- good-bye %%makeinstall

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.5-1m)
- update to 2.17.5 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-1m)
- update to 2.16.5

* Sun Oct  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-2m)
- enable compositor

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Tue Sep 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Tue Sep 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.5-3m)
- rebuild against expat-2.0.0-1m

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.5-2m)
- delete libtool library

* Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.5-1m)
- update to 2.14.5

* Wed Apr 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3
 
* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Mon Feb 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-2m)
- fix conflict directories

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Mon Dec  5 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-2m)
- delete autoreconf and make check

* Wed Nov 16 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 12.2.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.8-1m)
- version 2.8.8
- GNOME 2.8 Desktop

* Wed Dec 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.8.1-1m)
- version 2.8.1

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.5-1m)
- version 2.6.5
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.3-2m)
- revised spec for enabling rpm 4.2.

* Mon Jan 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.3-1m)
- version 2.6.3

* Wed Oct 08 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.3-1m)
- version 2.5.3

* Tue Jun 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.2-1m)
- version 2.5.2

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.34-2m)
- rebuild against for XFree86-4.3.0

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.34-1m)
- version 2.4.34

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.21-1m)
- version 2.4.21

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.13-1m)
- version 2.4.13

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.8-2m)
- add BuildPrereq: startup-notification >= 0.4
  for avoid "undefined reference to `sn_startup_sequence_complete'"

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.8-1m)
- version 2.4.8

* Thu Nov 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.5-1m)
- version 2.4.5

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.3-2m)
- update to cvs head.
- create -devel package

* Fri Nov  1 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.3-1m)
- version 2.4.3

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.2-1m)
- version up

* Sat Sep 21 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.4.1-1m)
- version up

* Mon Aug 12 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.4.0-2m)
- sumaso, nigiri naoshi

* Mon Aug 12 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.4.0-1m)
- Version up

* Wed Jul 31 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.3.987-2m)
- Fix %{buildroot} stuff

* Sat Jul 27 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.3.987-1m)
- Initial import into momonga head
