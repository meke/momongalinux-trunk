%global momorel 1

#define gitrev 894cea01

Name:           openni-primesense
Version:        5.0.3.3
Release:        %{momorel}m%{?dist}
Summary:        PrimeSensor/Kinect Modules for OpenNI
Group:          System Environment/Libraries
License:        LGPLv3+
URL:            https://github.com/PrimeSense/Sensor
# No official releases, yet. To reproduce tarball (adapt version and gitrev):
# git clone https://github.com/PrimeSense/Sensor.git
# cd Sensor
# git archive --format tar --prefix=openni-primesensor-5.0.3.3/ Stable-5.0.3.3 | tar -xC..
# cd ..
# rm -rf openni-primesensor-5.0.3.3/Platform/Win32
# tar cvfz openni-primesensor-5.0.3.3.tar.gz openni-primesensor-5.0.3.3
Source0:        openni-primesense-%{version}.tar.gz
Source1:        openni-primesense-55-primesense-usb.rules
Patch0:         openni-primesense-5.0.3.3-fedora.patch
Patch1:         openni-primesense-5.0.3.3-willowgarage.patch
Patch2:         openni-primesense-5.0.3.3-disable-sse.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  openni-devel >= 1.0.0, python
Requires:       openni >= 1.0.0

%description
This modules enables OpenNI to make use of the PrimeSense, also known as
Kinect depth camera.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .fedora
%patch1 -p1 -b .willow
%patch2 -p1 -b .disable-sse

dos2unix LGPL.txt
rm -rf Source/External/LibJPEG

%build
cd Platform/Linux-x86/CreateRedist
sed -i "s|make -C ../Build|make -C ../Build CFLAGS_EXT=\\\"%{optflags}\\\" SSE_GENERATION=2 DEBUG=1|" RedistMaker
./RedistMaker

%install
rm -rf %{buildroot}
pushd Platform/Linux-x86/Redist
INSTALL_LIB=%{buildroot}%{_libdir} \
INSTALL_BIN=%{buildroot}%{_bindir} \
INSTALL_ETC=%{buildroot}%{_sysconfdir}/openni/primesense \
SERVER_LOGS_DIR=%{buildroot}%{_var}/log/primesense \
INSTALL_RULES=%{buildroot}%{_sysconfdir}/udev/rules.d \
./install.sh -n
popd

rm -rf %{buildroot}%{_var}/log/primesense

rm %{buildroot}%{_sysconfdir}/udev/rules.d/55-primesense-usb.rules
install -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/udev/rules.d/55-primesense-usb.rules


%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
if [ $1 == 1 ]; then
  niReg -r %{_libdir}/libXnDeviceSensorV2.so
  niReg -r %{_libdir}/libXnDeviceFile.so
fi

%preun
if [ $1 == 0 ]; then
  niReg -u %{_libdir}/libXnDeviceSensorV2.so
  niReg -u %{_libdir}/libXnDeviceFile.so
fi

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LGPL.txt README
%dir %{_sysconfdir}/openni/primesense
%config(noreplace) %{_sysconfdir}/openni/primesense/*
%config(noreplace) %{_sysconfdir}/udev/rules.d/55-primesense-usb.rules
%{_libdir}/*.so
%attr(4755,root,root) %{_bindir}/XnSensorServer

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.3.3-1m)
- update to 5.0.3.3 (sync with Fedora)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.0.25-0.git894cea01.3m)
- rebuild for new GCC 4.6

* Mon Feb 14 2011 ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.0.24-0.gitc0e70ea1.2m)
- adapt 55-primesense-usb.rules to rules of udev
- (to avoid warning at booting up system)

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.0.24-0.gitc0e70ea1.1m)
- Initial Commit Momonga Linux

* Thu Jan 20 2011 Tim Niemueller <tim@niemueller.de> - 5.0.0.24-0.1gitc0e70ea1
- Initial revision

