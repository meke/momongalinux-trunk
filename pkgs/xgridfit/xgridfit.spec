%global momorel 2
%global prever 3
%global archivever 2.2a
%global date 20100725

Name:    xgridfit
Version: 2.2
Release: %{prever}.%{date}.%{momorel}m%{?dist}
Summary: Font hinting tool

# This is where we drop fontforge
Group:   Applications/Publishing
License: LGPLv2
URL:     http://%{name}.sf.net/
Source0: %{name}-%{name}.tar.gz
Patch1:  %{name}-%{archivever}-maindir-in-python.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires(post): libxml2
Requires(preun): libxml2
Requires: libxslt, fontforge, libxml2-python
Requires: libxslt
BuildRequires: python-devel

%description
Xgridfit is a high-level, XML-based language for gridfitting, or 'hinting',
fonts. The Xgridfit program compiles an XML source file into tables and
instructions that relate to the gridfitting of glyphs. Xgridfit does not
insert these elements into a font itself, but rather relies on FontForge, the
Open-Source font editor, to do so.


%package doc
Group:    Documentation
Summary:  Font hinting tool use documentation
# Does not really make sense without the tool itself
Requires: %{name} = %{version}-%{release}

%description doc
Xgridfit font hinting tool user documentation.

%prep
%setup -q -n %{name}
%patch1 -p1 -b .mip

%build

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} \
             BINDIR=%{_bindir} \
             MANDIR=%{_mandir} \
             MAINDIR=%{_datadir}/xml/%{name}-%{version}

# Simplify preun/post catalog logic
ln -s catalog.xml \
      %{buildroot}%{_datadir}/xml/%{name}-%{version}/schemas/catalog-%{version}.%{release}.xml

%clean
rm -rf %{buildroot}

%post
cd %{_sysconfdir}/xml
[ -e catalog ] || xmlcatalog --noout --create catalog
xmlcatalog --noout --add \
  nextCatalog %{_datadir}/xml/%{name}-%{version}/schemas/catalog-%{version}.%{release}.xml "" catalog >/dev/null
:

%preun
xmlcatalog --noout --del \
  %{_datadir}/xml/%{name}-%{version}/schemas/catalog-%{version}.%{release}.xml \
  %{_sysconfdir}/xml/catalog >/dev/null >/dev/null
:

%files
%defattr(0644,root,root,0755)
%doc COPYING ChangeLog

%{_datadir}/xml/%{name}-%{version}
%{_mandir}/man1/*

%defattr(0755,root,root,0755)
%{_bindir}/*

%{python_sitelib}/*

%files doc
%defattr(0644,root,root,0755)
%doc docs/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2-3.20100725.2m)
- rebuild against python-2.7

* Sat Apr 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-3.20100725.1m)
- update to 20100725 snapshot for apanov-edrip-fonts 20100430

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-1m)
- rebuild aagainst netcdf-4.1.2
- update to 2.2a

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19-2m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.19-1m)
- sync with Fedora 13 (1.19-2.b)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.17-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.17-4.a
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 23 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.17-3.a
- global-ization

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.17-2.a

* Fri Jan 30 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.17-1

* Fri Nov 14 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.11-1.a
- update for F11 cycle

* Mon Sep 1 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.9-1
- Major update, many changes
- Only package xsltproc support for now

* Sun May 18 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.6-3
- Lots of new makefile goodness

* Thu Mar 13 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.5-2
- Fix path munging

* Tue Mar 11 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.5-1
- Initial Fedora packaging
