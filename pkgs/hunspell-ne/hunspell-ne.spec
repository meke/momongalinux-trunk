%global momorel 4

Name: hunspell-ne
Summary: Nepali hunspell dictionaries
Version: 20080425
Release: %{momorel}m%{?dist}
Source: http://nepalinux.org/downloads/ne_NP_dict.zip
Group: Applications/Text
URL: http://nepalinux.org/downloads
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2
BuildArch: noarch

Requires: hunspell

%description
Nepali hunspell dictionaries.

%prep
%setup -q -c -n ne_NP_dict

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
ne_NP_aliases="ne_IN"
for lang in $ne_NP_aliases; do
        ln -s ne_NP.aff $lang.aff
        ln -s ne_NP.dic $lang.dic
done
popd


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_ne_NP.txt 
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080425-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080425-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20080425-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080425-1m)
- sync with Fedora 13 (20080425-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20061217-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20061217-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20061217-1m)
- import from Fedora to Momonga

* Mon Jan 21 2008 Parag <pnemade@redhat.com> - 20061217-2
- Corrected License tag.

* Thu Jan 03 2008 Parag <pnemade@redhat.com> - 20061217-1
- Initial Fedora release
