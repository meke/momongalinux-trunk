%global momorel 9
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

# Prevent unwanted bytecode stripping by RPM.
%global __strip /bin/true

Name:           ocaml-cil
Version:        1.4.0
Release:        %{momorel}m%{?dist}
Summary:        Infrastructure for C Program Analysis and Transformation

Group:          Development/Libraries
License:        Modified BSD
URL:            http://cil.sourceforge.net/
Source0:        http://dl.sourceforge.net/project/cil/cil/cil-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ocaml >= %{ocamlver}, ocaml-findlib-devel, ocaml-ocamldoc


%description
CIL (C Intermediate Language) is a high-level representation along
with a set of tools that permit easy analysis and source-to-source
transformation of C programs.

CIL is both lower-level than abstract-syntax trees, by clarifying
ambiguous constructs and removing redundant ones, and also
higher-level than typical intermediate languages designed for
compilation, by maintaining types and a close relationship with the
source program. The main advantage of CIL is that it compiles all
valid C programs into a few core constructs with a very clean
semantics. Also CIL has a syntax-directed type system that makes it
easy to analyze and manipulate C programs. Furthermore, the CIL
front-end is able to process not only ANSI-C programs but also those
using Microsoft C or GNU C extensions. If you do not use CIL and want
instead to use just a C parser and analyze programs expressed as
abstract-syntax trees then your analysis will have to handle a lot of
ugly corners of the language (let alone the fact that parsing C itself
is not a trivial task).

In essence, CIL is a highly-structured, "clean" subset of C. CIL
features a reduced number of syntactic and conceptual forms. For
example, all looping constructs are reduced to a single form, all
function bodies are given explicit return statements, syntactic sugar
like "->" is eliminated and function arguments with array types become
pointers.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%package        doc
Summary:        Documentation for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    doc
The %{name}-doc package contains documentation for users of %{name}.


%package        cilly
Summary:        Support programs for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Provides:       perl(CilConfig) = %{version}
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))


%description    cilly
The %{name}-cilly package contains the 'cilly' wrapper/replacement
for gcc.


%prep
%setup -q -n cil-%{version}


%build
./configure --libdir=%{_libdir}

make RELEASE=1
make quicktest

archos=`ls obj`

rm -f bin/CilConfig.pm
cat > bin/CilConfig.pm <<EOF
\$::archos    = "$archos";
\$::cc        = "gcc";
\$::cilhome   = "%{_libexecdir}/cil";
\$::default_mode = "GNUCC";
EOF

#strip obj/$archos/libcil.o
strip obj/$archos/cilly.byte.exe
%if %opt
strip obj/$archos/cilly.asm.exe
%endif


%install
rm -rf $RPM_BUILD_ROOT

# This sets $archos to something like 'x86_LINUX':
archos=`ls obj`

export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml

# CIL's make install rule is totally borked.
mkdir -p $DESTDIR%{perl_vendorlib}
install -m 0644 lib/*.pm bin/CilConfig.pm $DESTDIR%{perl_vendorlib}
mkdir -p $OCAMLFIND_DESTDIR
ocamlfind install cil META obj/$archos/*.{ml,mli,cmi,cmo,cmx,cma,cmxa,o,a} \
  src/*.mli

mkdir -p $DESTDIR%{_bindir}
install -m 0755 bin/cilly $DESTDIR%{_bindir}

mkdir -p $DESTDIR%{_libexecdir}/cil/obj/$archos
install -m 0755 obj/$archos/cilly.*.exe $DESTDIR%{_libexecdir}/cil/obj/$archos

mkdir -p $DESTDIR/etc/prelink.conf.d
echo '-b /usr/libexec/cil' > $DESTDIR/etc/prelink.conf.d/ocaml-cil-cilly.conf

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README.md LICENSE
%{_libdir}/ocaml/cil
%if %opt
%exclude %{_libdir}/ocaml/cil/*.a
%exclude %{_libdir}/ocaml/cil/*.cmxa
%exclude %{_libdir}/ocaml/cil/*.cmx
%endif
%exclude %{_libdir}/ocaml/cil/*.mli
%exclude %{_libdir}/ocaml/cil/*.ml


%files devel
%defattr(-,root,root,-)
%doc README.md LICENSE
%if %opt
%{_libdir}/ocaml/cil/*.a
%{_libdir}/ocaml/cil/*.cmxa
%{_libdir}/ocaml/cil/*.cmx
%endif
%{_libdir}/ocaml/cil/*.mli
%{_libdir}/ocaml/cil/*.ml


%files doc
%defattr(-,root,root,-)
%doc README.md LICENSE doc/*

%files cilly
%defattr(-,root,root,-)
%doc README.md LICENSE
%{perl_vendorlib}/CilConfig.pm
%{perl_vendorlib}/Cilly.pm
%{perl_vendorlib}/KeptFile.pm
%{perl_vendorlib}/OutputFile.pm
%{perl_vendorlib}/TempFile.pm
%{_bindir}/cilly
%{_libexecdir}/cil
%config(noreplace) /etc/prelink.conf.d/ocaml-cil-cilly.conf


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-2m)
- rebuild against perl-5.16.0

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0
- rebuild against ocaml-3.12.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.7-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.7-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.7-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-3m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-2m)
- rebuild against perl-5.12.0

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.6-6m)
- rebuild against perl-5.10.1

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-5m)
- import the following Fedora 11 changes
-- * Wed Jan 21 2009 Richard W.M. Jones <rjones@redhat.com> - 1.3.6-10
-- - Fix prelink configuration file.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-4m)
- rebuild against rpm-4.6

* Sat Dec  6 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-3m)
- import Patch0 from Fedora devel (1.3.6-9)

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-2m)
- sync with Fedora devel (1.3.6-7)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.3.6-5
- Rebuild for OCaml 3.10.2

* Wed Nov  7 2007 Richard W.M. Jones <rjones@redhat.com> - 1.3.6-4
- ExcludeArch ppc - CIL doesn't build on PPC as it turns out.

* Wed Nov  7 2007 Richard W.M. Jones <rjones@redhat.com> - 1.3.6-3
- Change upstream URL.
- perl(CilConfig) set to package version
- Split out documentation into a separate -doc package.

* Mon Aug 20 2007 Richard W.M. Jones <rjones@redhat.com> - 1.3.6-2
- Initial RPM release.
