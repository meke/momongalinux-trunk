%global momorel 1

%global xfce4ver 4.10.0
%global major 0.7

Name:           xfce4-dict
Version:        0.7.0
Release:        %{momorel}m%{?dist}
Summary:        A Dictionary Client for the Xfce desktop environment
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/applications/%{name}
Source0:        http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         xfce4-dict-0.7.0-dsofix.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  gettext, perl-XML-Parser, desktop-file-utils, intltool
Requires:       aspell, enchant, xdg-utils


%description
Xfce4 Dictionary is a client program to query different dictionaries. It can 
query a Dict server (RFC 2229), open online dictionaries in a web browser or 
verify the spelling of a word using aspell or ispell. This package contains 
the stand-alone application, that can be used in different desktop 
environments too.

%package        plugin
Summary:        Xfce panel plugin to query a Dict server
Group:          User Interface/Desktops
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:       %{name} = %{version}-%{release}
Requires:       xfce4-panel >= %{xfce4ver}

%description    plugin
Xfce4 Dictionary is a client program to query different dictionaries. It can 
query a Dict server (RFC 2229), open online dictionaries in a web browser or 
verify the spelling of a word using aspell or ispell. This package contains 
the plugin for the Xfce panel.


%prep
%setup -q
%patch0 -p1 -b .dsofix


%build
%configure --disable-static LIBS="-lm -lX11"
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'
%find_lang %{name}
desktop-file-install --vendor ""                                \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
        --add-only-show-in=XFCE                                 \
        --delete-original                                       \
        ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop


%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ] ; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ] ; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_mandir}/man1/%{name}.1.*

%files plugin
%defattr(-,root,root,-)
%{_libdir}/xfce4/panel/plugins/*
%{_datadir}/xfce4/panel/plugins/*.desktop


%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.0-6m)
- rebuild against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-5m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-1m)
- update
- rebuild against xfce4-4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-4m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.2-2m)
- add autoreconf. need libtool-2.2.x

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-1m)
- import from Fedora to Momonga

* Sun Jan 18 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.2-2
- Rebuild for Xfce 4.6 (Beta 3)

* Thu Nov 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.2-1
- Update to 0.5.2

* Thu Nov 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.1-1
- Update to 0.5.1
- Update gtk-update-icon-cache scriptlets

* Tue Nov 11 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.0-1
- Update to 0.5.0
- Only show in Xfce menu

* Tue Sep 30 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.1-2
- BuildRequire intltool

* Tue Sep 30 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.1-1
- Update to 0.4.1
- Require xdg-utils as xdg-open is the preferred command to open URLs now

* Sat May 24 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.0-1
- Update to 0.4.0
- Rename to xfce4-dict because we now have a standalone application

* Sun Mar 09 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0-1
- Update to 0.3.0

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.2.1-4
- Autorebuild for GCC 4.3

* Sat Aug 25 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.1-3
- Rebuild for BuildID feature
- Update license tag

* Sat Apr 28 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.1-2
- Rebuild for Xfce 4.4.1

* Mon Jan 22 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.1-1
- Update to 0.2.1 on Xfce 4.4.
- Update gtk-icon-cache scriptlets.

* Sun Nov 12 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-2
- Add %%defattr (#215169).

* Sat Nov 11 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-1
- Update to 0.2.0.

* Sat Sep 23 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-1
- Initial Fedora Extras version.
