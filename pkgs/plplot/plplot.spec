%global momorel 3

%{!?python_sitearch:%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?octave_api:%global octave_api %(octave-config -p API_VERSION || echo 0)}
# Set to bcond_with or use --without doc to disable doc build
%bcond_with doc
# Set to bcond_with or use --without octave to disable octave support
%bcond_with octave

# Set to bcond_with or use --without pdl to disable pdl tests - needed to 
# bootstrap or for soname bumps in a plplot dependency
%bcond_without pdl
%bcond_with ada

# conditionalize Ocaml support
%ifarch sparc64 s390 s390x
%bcond_with ocaml
%else
%bcond_without ocaml
%endif

Name:           plplot
Version:        5.9.9
Release:        %{momorel}m%{?dist}
Summary:        Library of functions for making scientific plots

Group:          Applications/Engineering
License:        LGPLv2+
URL:            http://plplot.sourceforge.net/
Source0:        http://downloads.sourceforge.net/plplot/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:         plplot-5.9.7-gnat46.patch
Patch1:         plplot-5.9.7-multiarch.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  cmake, libtool-ltdl-devel, swig
BuildRequires:  gcc-gfortran
%if %{with ada}
BuildRequires:  gcc-gnat
BuildRequires:  lapack-devel
%endif
%if %{with octave}
BuildRequires:  octave-devel
%define build_octave %{nil}
%else
Obsoletes:      %{name}-octave < %{version}-%{release}
%define build_octave -DENABLE_octave:BOOL=OFF
%endif
BuildRequires:  java-devel, libgcj-devel
BuildRequires:  freetype-devel, qhull-devel , ncurses-devel
BuildRequires:  gd-devel, tcl-devel, tk-devel
BuildRequires:  itcl-devel, itk-devel
BuildRequires:  python-devel, pygtk2-devel, numpy
BuildRequires:  libgnomeui-devel, libgnomeprintui22-devel, gnome-python2-devel
BuildRequires:  gnome-python2-canvas, gnome-python2-gnome
BuildRequires:  perl(XML::DOM), lasi-devel, wxGTK-devel, agg-devel
BuildRequires:  gnu-free-mono-fonts
BuildRequires:  gnu-free-sans-fonts
BuildRequires:  gnu-free-serif-fonts
%if %{with pdl}
BuildRequires:  perl(PDL::Graphics::PLplot)
%endif
%if %{with doc}
BuildRequires:  texlive, texinfo, openjade, jadetex, docbook2X, docbook-style-dsssl
%endif
%if %{with ocaml}
BuildRequires:  ocaml
BuildRequires:  ocaml-cairo-devel
BuildRequires:  ocaml-camlidl-devel
BuildRequires:  ocaml-findlib
BuildRequires:  ocaml-lablgtk-devel
BuildRequires:  ocaml-ocamldoc
%endif
BuildRequires:  lua-devel
BuildRequires:  qt-devel
#For pyqt4
BuildRequires:  PyQt4-devel
#For Qt tests
BuildRequires:  xorg-x11-xauth, xorg-x11-server-Xvfb, mesa-dri-drivers
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info
Requires:       gnu-free-mono-fonts
Requires:       gnu-free-sans-fonts
Requires:       gnu-free-serif-fonts
Requires:       numpy
Obsoletes:      %{name}-gnome <= %{version}-%{release}
Obsoletes:      %{name}-gnome-devel <= %{version}-%{release}


%description
PLplot is a library of functions that are useful for making scientific
plots.

PLplot can be used from within compiled languages such as C, C++,
FORTRAN and Java, and interactively from interpreted languages such as
Octave, Python, Perl and Tcl.

The PLplot library can be used to create standard x-y plots, semilog
plots, log-log plots, contour plots, 3D surface plots, mesh plots, bar
charts and pie charts. Multiple graphs (of the same or different sizes)
may be placed on a single page with multiple lines in each graph.

A variety of output file devices such as Postscript, png, jpeg, LaTeX
and others, as well as interactive devices such as xwin, tk, xterm and
Tektronics devices are supported. New devices can be easily added by
writing a small number of device dependent routines.

There are almost 2000 characters in the extended character set. This
includes four different fonts, the Greek alphabet and a host of
mathematical, musical, and other symbols. Some devices supports its own
way of dealing with text, such as the Postscript and LaTeX drivers, or
the png and jpeg drivers that uses the Freetype library.


%package        libs
Summary:        Libraries for PLplot
Group:          Development/Libraries
Requires(post): /sbin/ldconfig

%description    libs
%{summary}.


%package        devel
Summary:        Development headers and libraries for PLplot
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-libs = %{version}-%{release}
Requires:       pkgconfig
Requires:       qhull-devel, freetype-devel, libtool-ltdl-devel

%description    devel
%{summary}.


%if %{with ada}
%package        ada
Summary:        Functions for scientific plotting with Ada
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    ada
%{summary}.

%package        ada-devel
Summary:        Development files for using PLplot Ada bindings
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       %{name}-ada = %{version}-%{release}
Requires:       pkgconfig
Requires:       gcc-gnat

%description    ada-devel
%{summary}.
%endif


%package        fortran-devel
Summary:        Development files for using PLplot Fortran bindings
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       gcc-gfortran
Requires:       pkgconfig

%description    fortran-devel
%{summary}.


%package        java
Summary:        Functions for scientific plotting with Java
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       java

%description    java
%{summary}.

%package        java-devel
Summary:        Development files for using PLplot GNOME
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       %{name}-java = %{version}-%{release}

%description    java-devel
%{summary}.


%package        lua
Summary:        Functions for scientific plotting with Lua
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       lua

%description    lua
%{summary}.


%if %{with ocaml}
%package        ocaml
Summary:        Functions for scientific plotting with OCaml
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Provides:       ocaml-plplot = %{version}-%{release}

%description    ocaml
%{summary}.


%package        ocaml-devel
Summary:        Development files for PLplot OCaml
Group:          Development/Libraries
Requires:       %{name}-ocaml = %{version}-%{release}
Provides:       ocaml-plplot-devel = %{version}-%{release}

%description    ocaml-devel
%{summary}.
%endif


%if %{with octave}
%package        octave
Summary:        Functions for scientific plotting with Octave
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}, octave
Requires:       octave(api) = %{octave_api}

%description    octave
%{summary}.
%endif


%package        perl
Summary:        Examples for using plplot with PDL
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    perl
%if %{with pdl}
%{summary}.
%else
This is a dummy package, since pdl support was not enabled in the plplot build.
%endif


%package        pyqt
Summary:        Functions for scientific plotting with PyQt
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    pyqt
%{summary}.


%package        qt
Summary:        Functions for scientific plotting with Qt
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    qt
%{summary}.

%package        qt-devel
Summary:        Development files for using PLplot with Qt
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       %{name}-qt = %{version}-%{release}
Requires:       pkgconfig

%description    qt-devel
%{summary}.


%package        tk
Summary:        Functions for scientific plotting with Tk
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    tk
%{summary}.

%package        tk-devel
Summary:        Development files for using PLplot with Tk
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       %{name}-tk = %{version}-%{release}
Requires:       pkgconfig

%description    tk-devel
%{summary}.


%package        wxGTK
Summary:        Functions for scientific plotting with wxGTK
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    wxGTK
%{summary}.

%package        wxGTK-devel
Summary:        Development files for using PLplot with wxGTK
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       %{name}-wxGTK = %{version}-%{release}
Requires:       pkgconfig

%description    wxGTK-devel
%{summary}.


%prep
%setup -q
%patch0 -p1 -b .gnat46
%patch1 -p1 -b .multiarch

%build

mkdir momonga
cd momonga
export CFLAGS="$RPM_OPT_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS"
export FFLAGS="$RPM_OPT_FLAGS"
#Needed for octave output to not have control characters
unset TERM
printenv
%cmake .. \
        -DF95_MOD_DIR:PATH=%{_fmoddir} \
        -DUSE_RPATH:BOOL=OFF \
%if %{with ada}
        -DENABLE_ada:BOOL=ON \
        -DHAVE_ADA_2007:BOOL=OFF \
%endif
        -DENABLE_d:BOOL=ON \
        -DENABLE_itcl:BOOL=ON \
        -DENABLE_itk:BOOL=ON \
        -DENABLE_lua:BOOL=ON \
        -DHAVE_lua51:BOOL=ON \
        -DENABLE_ocaml:BOOL=ON \
        -DOCAML_INSTALL_DIR:PATH=`ocamlc -where` \
        %{build_octave} \
        -DENABLE_pdl:BOOL=ON \
        -DENABLE_tk:BOOL=ON \
        -DHAVE_PDL_GRAPHICS_PLPLOT_40:BOOL=ON \
        -DHAVE_PTHREAD:BOOL=ON \
        -DJAVAWRAPPER_DIR:PATH="%{_libdir}/plplot%{version}" \
        -DPL_FREETYPE_FONT_PATH:PATH="/usr/share/fonts/gnu-free" \
        -DPLD_aqt:BOOL=ON \
        -DPLD_conex:BOOL=ON \
        -DPLD_imp:BOOL=ON \
        -DPLD_mskermit:BOOL=ON \
        -DPLD_ntk:BOOL=ON \
        -DPLD_pstex:BOOL=ON \
        -DPLD_svg:BOOL=ON \
        -DPLD_tek4010:BOOL=ON \
        -DPLD_tek4010f:BOOL=ON \
        -DPLD_tek4107:BOOL=ON \
        -DPLD_tek4107f:BOOL=ON \
        -DPLD_versaterm:BOOL=ON \
        -DPLD_vlt:BOOL=ON \
        -DPLD_xterm:BOOL=ON \
        -DPLD_wxwidgets:BOOL=ON \
        -DPYTHON_INCLUDE_DIR:PATH=%{_includedir}/python2.7 \
        -DPYTHON_LIBRARY:FILEPATH=%{_libdir}/libpython2.7.so \
%if %{with doc}
        -DXML_DECL:FILEPATH=/usr/share/sgml/xml.dcl \
        -DBUILD_DOC:BOOL=ON \
%else
        -DPREBUILT_DOC:BOOL=ON \
%endif
        -DBUILD_TEST:BOOL=ON
make %{?_smp_flags} VERBOSE=1


%install
rm -rf $RPM_BUILD_ROOT
cd momonga
make install DESTDIR=$RPM_BUILD_ROOT
mv $RPM_BUILD_ROOT%{_docdir}/plplot $RPM_BUILD_ROOT%{_docdir}/plplot-%{version}

# Fix up tclIndex files so they are the same on all builds
for file in $RPM_BUILD_ROOT%{_datadir}/plplot%{version}/examples/*/tclIndex
do
   grep '^[# ]' ${file} > tclIndex.hd
   grep -v '^[# ]' ${file} | sort > tclIndex
   cat tclIndex.hd tclIndex > ${file}
done

rm -rf $RPM_BUILD_ROOT%{_datadir}/plplot%{version}/examples/cmake/modules/Platform

#Don't pull in script interpreters for example binaries
chmod -x $RPM_BUILD_ROOT%{_datadir}/plplot%{version}/examples/tk/tk*

# Place Fortran include file in a more appropriate location
mkdir -p %{buildroot}%{_includedir}
mv %{buildroot}%{_libdir}/fortran/include/plplot/plplot_parameters.h %{buildroot}%{_includedir}/

%check
cd momonga
#Hack
export ITCL_LIBRARY=%{_libdir}/tcl8.5/itcl3.4
# Exclude compare tests for now, and ocaml from ppc/ppc64
# The *qt tests requires an X server
LOGFILE=`mktemp`
%ifarch ppc ppc64
ctest -V -E 'compare|ocaml|qt' &> $LOGFILE
%else
ctest -V -E 'compare|qt' &> $LOGFILE || (cat $LOGFILE && exit 1)
%endif


%clean
rm -rf $RPM_BUILD_ROOT


%post
/sbin/install-info %{_infodir}/plplotdoc.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/plplotdoc.info %{_infodir}/dir || :
fi

%post   libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%if %{with ada}
%post   ada -p /sbin/ldconfig
%postun ada -p /sbin/ldconfig
%endif

%if %{with octave}
%post   octave -p /sbin/ldconfig
%postun octave -p /sbin/ldconfig
%endif

%post   qt -p /sbin/ldconfig
%postun qt -p /sbin/ldconfig

%post   tk -p /sbin/ldconfig
%postun tk -p /sbin/ldconfig

%post   wxGTK -p /sbin/ldconfig
%postun wxGTK -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%{_docdir}/plplot-%{version}/
%{_bindir}/pltek
%{_bindir}/pstex2eps
%{python_sitearch}/_plplotcmodule.so
%{python_sitearch}/plplot.py*
%{python_sitearch}/plplotc.py*
%{python_sitearch}/Plframe.py*
%{python_sitearch}/TclSup.py*
%{_infodir}/plplotdoc.info*
%{_mandir}/man1/pltek.1.*
%{_mandir}/man1/pstex2eps.1.*
%dir %{_datadir}/plplot%{version}
%{_datadir}/plplot%{version}/*.fnt
%{_datadir}/plplot%{version}/*.map
%{_datadir}/plplot%{version}/*.pal
%dir %{_datadir}/plplot%{version}/examples
%{_datadir}/plplot%{version}/examples/lena.pgm
%{_datadir}/plplot%{version}/examples/plplot-test.sh
%{_datadir}/plplot%{version}/examples/plplot-test-interactive.sh
%{_datadir}/plplot%{version}/examples/python/
%{_datadir}/plplot%{version}/examples/test_python.sh

%files libs
%defattr(-,root,root,-)
%doc COPYING.LIB Copyright
%{_libdir}/libcsirocsa.so.*
%{_libdir}/libcsironn.so.*
%{_libdir}/libplplotcxxd.so.*
%{_libdir}/libplplotd.so.*
%{_libdir}/libplplotf77cd.so.*
%{_libdir}/libplplotf77d.so.*
%{_libdir}/libplplotf95cd.so.*
%{_libdir}/libplplotf95d.so.*
%{_libdir}/libqsastime.so.*
%dir %{_libdir}/plplot%{version}
%dir %{_libdir}/plplot%{version}/driversd
%{_libdir}/plplot%{version}/driversd/cairo.so
%{_libdir}/plplot%{version}/driversd/cairo.driver_info
%{_libdir}/plplot%{version}/driversd/mem.so
%{_libdir}/plplot%{version}/driversd/mem.driver_info
%{_libdir}/plplot%{version}/driversd/ntk.so
%{_libdir}/plplot%{version}/driversd/ntk.driver_info
%{_libdir}/plplot%{version}/driversd/null.so
%{_libdir}/plplot%{version}/driversd/null.driver_info
%{_libdir}/plplot%{version}/driversd/ps.so
%{_libdir}/plplot%{version}/driversd/ps.driver_info
%{_libdir}/plplot%{version}/driversd/pstex.so
%{_libdir}/plplot%{version}/driversd/pstex.driver_info
%{_libdir}/plplot%{version}/driversd/psttf.so
%{_libdir}/plplot%{version}/driversd/psttf.driver_info
%{_libdir}/plplot%{version}/driversd/svg.so
%{_libdir}/plplot%{version}/driversd/svg.driver_info
%{_libdir}/plplot%{version}/driversd/xfig.so
%{_libdir}/plplot%{version}/driversd/xfig.driver_info
%{_libdir}/plplot%{version}/driversd/xwin.so
%{_libdir}/plplot%{version}/driversd/xwin.driver_info

%files devel
%defattr(-,root,root,-)
%{_includedir}/plplot/
%{_libdir}/libcsirocsa.so
%{_libdir}/libcsironn.so
%{_libdir}/libplplotcxxd.so
%{_libdir}/libplplotd.so
%{_libdir}/libqsastime.so
%{_libdir}/pkgconfig/plplotd.pc
%{_libdir}/pkgconfig/plplotd-c++.pc
%{_datadir}/plplot%{version}/examples/CMakeLists.txt
%dir %{_datadir}/plplot%{version}/examples/cmake
%dir %{_datadir}/plplot%{version}/examples/cmake/modules
%{_datadir}/plplot%{version}/examples/cmake/modules/FindPkgConfig.cmake
%{_datadir}/plplot%{version}/examples/cmake/modules/export_plplot-noconfig.cmake
%{_datadir}/plplot%{version}/examples/cmake/modules/export_plplot.cmake
%{_datadir}/plplot%{version}/examples/cmake/modules/language_support.cmake
%if %{with ada}
%{_datadir}/plplot%{version}/examples/cmake/modules/language_support/
%endif
%{_datadir}/plplot%{version}/examples/cmake/modules/pkg-config.cmake
%{_datadir}/plplot%{version}/examples/cmake/modules/plplot_configure.cmake
%{_datadir}/plplot%{version}/examples/c/
%{_datadir}/plplot%{version}/examples/c++/
%{_datadir}/plplot%{version}/examples/Makefile
%{_datadir}/plplot%{version}/examples/test_c.sh
%{_datadir}/plplot%{version}/examples/test_c_interactive.sh
%{_datadir}/plplot%{version}/examples/test_cxx.sh
%{_datadir}/plplot%{version}/examples/test_diff.sh
%{_mandir}/man3/pl*.3*


%if %{with ada}
%files ada
%defattr(-,root,root,-)
%{_libdir}/libplplotadad.so.*

%files ada-devel
%defattr(-,root,root,-)
#Until we find an owner for %{_libdir}/ada/adalib/
%{_libdir}/ada/
#%{_libdir}/ada/adalib/plplotadad/
%{_libdir}/libplplotadad.so
%{_libdir}/pkgconfig/plplotd-ada.pc
#Until we find an owner for %{_datadir}/ada/adainclude/
%{_datadir}/ada/
#%{_datadir}/ada/adainclude/plplotadad/
%{_datadir}/plplot%{version}/examples/ada/
%{_datadir}/plplot%{version}/examples/test_ada.sh
%endif

%files fortran-devel
%defattr(-,root,root,-)
%{_fmoddir}/plplot.mod
%{_fmoddir}/plplot_flt.mod
%{_fmoddir}/plplotp.mod
%{_libdir}/libplplotf77cd.so
%{_libdir}/libplplotf77d.so
%{_libdir}/libplplotf95cd.so
%{_libdir}/libplplotf95d.so
%{_includedir}/plplot_parameters.h
%{_libdir}/pkgconfig/plplotd-f77.pc
%{_libdir}/pkgconfig/plplotd-f95.pc
%{_datadir}/plplot%{version}/examples/f77/
%{_datadir}/plplot%{version}/examples/f95/
%{_datadir}/plplot%{version}/examples/test_f77.sh
%{_datadir}/plplot%{version}/examples/test_f95.sh

%files java
%defattr(-,root,root,-)
%{_libdir}/plplot%{version}/plplotjavac_wrap.so
%{_datadir}/java/plplot.jar

%files java-devel
%defattr(-,root,root,-)
%{_datadir}/plplot%{version}/examples/java/
%{_datadir}/plplot%{version}/examples/test_java.sh

%files lua
%defattr(-,root,root,-)
%{_libdir}/lua/5.1/plplot/
%{_datadir}/plplot%{version}/examples/lua/
%{_datadir}/plplot%{version}/examples/test_lua.sh

%if %{with ocaml}
%files ocaml
%defattr(-,root,root,-)
%dir %{_libdir}/ocaml/plcairo/
%{_libdir}/ocaml/plcairo/META
%{_libdir}/ocaml/plcairo/*.cma
%{_libdir}/ocaml/plcairo/*.cmi
%dir %{_libdir}/ocaml/plplot/
%{_libdir}/ocaml/plplot/META
%{_libdir}/ocaml/plplot/*.cma
%{_libdir}/ocaml/plplot/*.cmi
%{_libdir}/ocaml/stublibs/*

%files ocaml-devel
%defattr(-,root,root,-)

%{_libdir}/pkgconfig/plplotd-ocaml.pc
%{_libdir}/ocaml/plcairo/*.a
%{_libdir}/ocaml/plcairo/*.cmxa
%{_libdir}/ocaml/plcairo/plcairo.mli
%{_libdir}/ocaml/plplot/*.a
%{_libdir}/ocaml/plplot/*.cmxa
%{_libdir}/ocaml/plplot/plplot.mli
%{_datadir}/plplot%{version}/examples/ocaml/
%{_datadir}/plplot%{version}/examples/test_ocaml.sh
%endif

%if %{with octave}
%files octave
%defattr(-,root,root,-)
%{_datadir}/plplot_octave/
%{_datadir}/octave/site/m/PLplot/
%{_libdir}/octave/site/oct/*/plplot_octave.oct
%{_datadir}/plplot%{version}/examples/lena.img
%{_datadir}/plplot%{version}/examples/octave/
%{_datadir}/plplot%{version}/examples/test_octave.sh
%{_datadir}/plplot%{version}/examples/test_octave_interactive.sh
%endif

#%if %{with pdl}
%files perl
%defattr(-,root,root,-)
%if %{with pdl}
%{_datadir}/plplot%{version}/examples/perl/
%{_datadir}/plplot%{version}/examples/test_pdl.sh
%endif

%files pyqt
%defattr(-,root,root,-)
%{python_sitearch}/plplot_pyqt4.so

%files qt
%defattr(-,root,root,-)
%{_libdir}/libplplotqtd.so.*
%{_libdir}/plplot%{version}/driversd/qt.so
%{_libdir}/plplot%{version}/driversd/qt.driver_info

%files qt-devel
%defattr(-,root,root,-)
%{_libdir}/libplplotqtd.so
%{_libdir}/pkgconfig/plplotd-qt.pc

%files tk
%defattr(-,root,root,-)
%{_bindir}/plserver
%{_bindir}/pltcl
%{_libdir}/libplplottcltkd.so.*
%{_libdir}/libtclmatrixd.so.*
%{_libdir}/plplot%{version}/driversd/tk.so
%{_libdir}/plplot%{version}/driversd/tk.driver_info
%{_libdir}/plplot%{version}/driversd/tkwin.so
%{_libdir}/plplot%{version}/driversd/tkwin.driver_info
%{python_sitearch}/plplot_widgetmodule.so
%{_datadir}/plplot%{version}/pkgIndex.tcl
%{_datadir}/plplot%{version}/examples/test_tcl.sh
%{_datadir}/plplot%{version}/examples/tcl/
%{_datadir}/plplot%{version}/examples/tk/
%{_datadir}/plplot%{version}/tcl/
%{_mandir}/man1/plserver.1.*
%{_mandir}/man1/pltcl.1.*

%files tk-devel
%defattr(-,root,root,-)
%{_libdir}/libplplottcltkd.so
%{_libdir}/libtclmatrixd.so
%{_libdir}/pkgconfig/plplotd-tcl.pc

%files wxGTK
%defattr(-,root,root,-)
%{_libdir}/libplplotwxwidgetsd.so.*
%{_libdir}/plplot%{version}/driversd/wxwidgets.so
%{_libdir}/plplot%{version}/driversd/wxwidgets.driver_info

%files wxGTK-devel
%defattr(-,root,root,-)
%{_libdir}/libplplotwxwidgetsd.so
%{_libdir}/pkgconfig/plplotd-wxwidgets.pc

%changelog
* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.9-3m)
- use python2.7

* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.9.9-2m)
- disable octave support since package test fails for octave  
  due to split function was deprecated from octave-3.6.1 

* Wed Nov  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.9-1m)
- update to 5.9.9

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.8-2m)
- rebuild against perl-5.14.2

* Mon Aug 29 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.8-1m)
- update to 5.9.8
- merge from fedora

* Mon Jun 27 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.7-12m)
- add BuildRequires

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.7-11m)
- rebuild against perl-5.14.1

* Thu Jun 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.7-10m)
- minor update for octave-3.4.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.7-9m)
- rebuild against perl-5.14.0-0.2.1m

* Wed May  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.7-8m)
- this package needs perl-PDL-Graphics-Plplot <= 0.52

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9.7-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.7-6m)
- rebuild for new GCC 4.6

* Sun Mar 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.7-5m)
- add BR  PyQt4-devel

* Fri Mar 11 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.9.7-4m)
- add BR  lua-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.7-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.7-2m)
- specify PATH for Qt4

* Wed Oct 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.7-1m)
- update to 5.9.7

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.6-3m)
- fix BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9.6-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.6-1m)
- update to 5.9.6

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.5-6m)
- rebuild against qt-4.6.3-1m

* Mon May 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.5-5m)
- add buildrequires

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9.5-4m)
- rebuild against ocaml-3.11.2

* Thu Apr  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9.5-3m)
- buildfix (Patch10)
-- http://www.mail-archive.com/cmake@cmake.org/msg02490.html

* Sun Jan  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.5-2m)
- add BuildRequires

* Sun Jan  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9.5-1m)
- update to 5.9.5 and go to Main (sync with Fedora devel)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9.0-2m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.9.0-1m)
- import from Fedora for gdl
- Change BR on gnome-python2-devel to gnome-python2
- tmp comment out BuildRequires:  gcc-gnat

* Wed Feb 27 2008 - Orion Poplawski <orion@cora.nwra.com> - 5.9.0-1
- Update to 5.9.0
- Re-enable smp builds
- Add ada subpackage

* Fri Jan 18 2008 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-10
- Add Requries: libtool-ltdl-devel to devel package

* Tue Jan 15 2008 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-9
- Require numpy (bug #428876)
- Update SVN patch to add support for detecting Tcl version
- Look for itcl 3.4

* Sat Jan 12 2008 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-8
- Re-enable itcl

* Mon Jan  7 2008 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-7
- Update to latest svn for Tcl 8.5 support
- Don't build against itcl - does not support tcl 8.5

* Thu Jan  3 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 5.8.0-6
- Rebuild for new Tcl 8.5

* Fri Dec 23 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-5
- Rebuild for octave 3.0

* Fri Dec 14 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-4
- Rebuild for new octave api

* Wed Dec  4 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-3
- Updated multiarch patch for all language example makefiles (bug #342901)

* Thu Nov 29 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-2
- Rebuild for new octave api

* Mon Nov 19 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.8.0-1
- Update to 5.8.0

* Fri Nov  9 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.4-5
- Update to latest svn: adds cairo, drops plmeta, plrender
- Rebuild for new octave api

* Tue Oct 16 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.4-4
- Add patch from svn to fix octave bindings for octave 2.9.15, drop
  old version

* Thu Aug 23 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.4-3
- Add perl sub-package for PDL/plpot examples

* Thu Aug 23 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.4-2
- Rebuild for BuildID

* Tue Aug 14 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.4-1
- Update to 5.7.4
- Re-enable ada on x86_64
- Enable PDL tests
- Set HAVE_PTHREAD for the xwin driver

* Wed Aug  8 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.3-4
- BR numpy rather than python-numeric - doesn't do anything yet?

* Tue Aug  7 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.3-3
- Disable Ada interface on ppc64 until available (bug #241233)
- Add svn patch to fix ada bindings on x86_64 and other issues
- Add with doc conditional to test doc builds
- Disable octave until plplot supports octave 2.9.11+
- Update license to LGPLv2+
- Exclude psttfc test until fixed

* Mon Apr 16 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.3-2
- Use cmake macros

* Mon Mar 26 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.3-1
- Update to 5.7.3
- Hack to run itcl examples
- Install Java JNI into %%{_libdir}/plplot%%{_version}
- java package requires java (bug #233905)
- devel requires main package (bug #233905)
- Enable octave api requirement
- Enable new ada interface - not installed yet though
- Renaable pstex driver

* Tue Mar 20 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.2-4
- Set PREBUILT_DOC so docs are installed

* Wed Mar 14 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.2-3
- Fix up tclIndex files so they are the same on all builds (bug #228172)
- Fix up examples/tk/Makefile for multilib (bug #228172)
- Install python in arch specific dirs (bug #228173)
- Enable itcl
- Update to CVS

* Fri Feb 09 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.2-2
- Rebuild for Tcl 8.5

* Tue Jan 23 2007 - Orion Poplawski <orion@cora.nwra.com> - 5.7.2-1
- Update to 5.7.2 which uses cmake
- Add patch to use -nopgcpp with swig java generation
- Enable more drivers

* Thu Dec 14 2006 - Jef Spaleta <jspaleta@gmail.com> - 5.6.1-9
- Bump and build for python 2.5

* Thu Nov  2 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.1-8
- Point to the freefont package properly (bug #210517)
- Move libriaries to -libs, -devel requires -libs for multilib support
- Change BR on gnome-python2 to gnome-python2-devel

* Fri Oct  6 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.1-7
- Rebuild for new octave API version

* Tue Oct  3 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.1-6
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Sep 25 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.1-5
- Add needed Requires for -devel packages (bug #202168)
- Patch .info rather than .texi, and actually apply

* Mon Aug 28 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.1-4
- Rebuild for new octave API version
- Add patch for texinfo file to fix category

* Mon Aug  7 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.1-3
- Add BR ncurses-devel

* Mon Aug  7 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.1-2
- Add patch to support octave 2.9.7 (bug #201443)

* Mon May 22 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.1-1
- Update to 5.6.1
- Enable f95 bindings
- Remove patches applied upstream
- Now include pkgconfig files
- pstex driver no longer shipped

* Mon Apr 24 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.6.0-1
- Update to 5.6.0 with new psttf driver
- Add wxGTK support

* Fri Feb 24 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-12
- Rebuild for FC5 gcc/glibc changes

* Wed Feb  1 2006 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-11
- Package .pyc and .pyo files

* Thu Dec 22 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-10
- Add patch to strip X check from configure for modular X
- Rework patches to patch configure and avoid autoconf
- Teporarily add BR on libXau-devel and libXdmcp (bz #176313)

* Thu Dec 15 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-9
- Rebuild for gcc 4.1

* Tue Aug 30 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-8
- Re-enable java on ppc - should be fixed in upstream rawhide

* Thu Aug 11 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-7
- Breakout java into its own sub-package, don't build on non x86
- Add patch to fix c test on ppc

* Wed Aug 10 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-6
- Add patch to find tcl on x86_64
- Add patch to use new octave in devel

* Tue Aug 09 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-5
- Explicitly turn off unused features in configure
- rpmlint cleanup
- add make check

* Mon Aug 08 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-4
- Remove smp build due to problems
- Add gnome-python2 BuildRequires

* Mon Aug 08 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-3
- Split into multipe packages

* Mon Aug 08 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-2
- Shorten summary
- Fix Source URL

* Thu Aug 04 2005 - Orion Poplawski <orion@cora.nwra.com> - 5.5.3-1
- Initial Fedora Extras release
