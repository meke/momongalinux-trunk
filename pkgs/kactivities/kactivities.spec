%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Name:          kactivities
Summary:       API for using and interacting with Activities
Version:       %{kdever}
Release:       %{momorel}m%{?dist}
License:       GPLv2+ and LGPLv2+
Group:         System Environment/Libraries
URL:           https://projects.kde.org/projects/kde/kdelibs/kactivities
Source0:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:      0
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: nepomuk-core-devel >= %{kdever}
Requires:      kdelibs >= %{kdever}
Obsoletes:     libkactivities
Provides:      libkactivities

%description
API for using and interacting with Activities as a consumer,
application adding information to them or as an activity manager.

%package  devel
Summary:       Developer files for %{name}
Group:         Development/Libraries
Requires:      %{name} = %{version}-%{release}
Requires:      kdelibs-devel
Obsoletes:     libkactivities-devel
Provides:      libkactivities-devel

%description devel
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_kde4_bindir}/kactivitymanagerd
%{_kde4_libdir}/libkactivities.so.*
%{_kde4_libdir}/libkactivities-models.so.*
%{_kde4_libdir}/kde4/activitymanager_plugin_activityranking.so
%{_kde4_libdir}/kde4/activitymanager_plugin_globalshortcuts.so
%{_kde4_libdir}/kde4/activitymanager_plugin_nepomuk.so
%{_kde4_libdir}/kde4/activitymanager_plugin_slc.so
%{_kde4_libdir}/kde4/activitymanager_plugin_sqlite.so
%{_kde4_libdir}/kde4/activitymanager_plugin_virtualdesktopswitch.so
%{_kde4_libdir}/kde4/kactivitymanagerd_fileitem_linking_plugin.so
%{_kde4_libdir}/kde4/imports/org/kde/activities/models/libkactivities-models-component-plugin.so
%{_kde4_libdir}/kde4/imports/org/kde/activities/models/qmldir
%{_kde4_libdir}/kde4/kcm_activities.so
%{_kde4_libdir}/kde4/kio_activities.so
%{_kde4_appsdir}/activitymanager/workspace/settings/BlacklistApplicationView.qml
%{_kde4_datadir}/kde4/services/activities.protocol
%{_kde4_datadir}/kde4/services/activitymanager-plugin-activityranking.desktop
%{_kde4_datadir}/kde4/services/activitymanager-plugin-globalshortcuts.desktop
%{_kde4_datadir}/kde4/services/activitymanager-plugin-nepomuk.desktop
%{_kde4_datadir}/kde4/services/activitymanager-plugin-slc.desktop
%{_kde4_datadir}/kde4/services/activitymanager-plugin-sqlite.desktop
%{_kde4_datadir}/kde4/services/activitymanager-plugin-virtualdesktopswitch.desktop
%{_kde4_datadir}/kde4/services/kactivitymanagerd.desktop
%{_kde4_datadir}/kde4/services/kactivitymanagerd_fileitem_linking_plugin.desktop
%{_kde4_datadir}/kde4/services/kcm_activities.desktop
%{_kde4_datadir}/kde4/servicetypes/activitymanager-plugin.desktop
%{_kde4_datadir}/ontology/kde/kao.ontology
%{_kde4_datadir}/ontology/kde/kao.trig

%files devel
%defattr(-,root,root,-)
%{_kde4_libdir}/libkactivities.so
%{_kde4_libdir}/libkactivities-models.so
%{_kde4_libdir}/cmake/KActivities
%{_kde4_libdir}/cmake/KActivities-Models
%{_kde4_libdir}/pkgconfig/libkactivities.pc
%{_kde4_libdir}/pkgconfig/libkactivities-models.pc
%{_kde4_includedir}/KDE/KActivities
%{_kde4_includedir}/kactivities
%{_kde4_includedir}/kactivities-models

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Thu Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- initial build for Momonga Linux
- old name is libkactivity
