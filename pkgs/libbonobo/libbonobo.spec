%global momorel 2

%define po_package libbonobo-2.0

Summary: Bonobo component system
Name: libbonobo
Version: 2.32.1
Release: %{momorel}m%{?dist}
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.32/%{name}-%{version}.tar.bz2
NoSource: 0
License: GPLv2+ and LGPLv2+
# bonobo-activation-server, bonobo-activation-sysconf and bonobo-slay are GPL
# libbonobo and libbonobo-activation are LGPLv2+
Group: System Environment/Libraries
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libxml2-devel >= 2.6.30
BuildRequires: ORBit2-devel >= 2.14.18
BuildRequires: intltool >= 0.14.1
BuildRequires: automake autoconf libtool
BuildRequires: gtk-doc >= 1.5
BuildRequires: flex, bison, zlib-devel, popt-devel
BuildRequires: dbus-glib-devel
BuildRequires: gettext gettext-devel
BuildRequires: glib2-devel >= 2.25.7



# From 2.3, libbonobo swallowed bonobo-activation
Obsoletes: bonobo-activation
Provides: bonobo-activation

Patch0: libbonobo-multishlib.patch

%description
Bonobo is a component system based on CORBA, used by the GNOME desktop.

%package devel
Summary: Libraries and headers for libbonobo
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: ORBit2-devel
Requires: libxml2-devel
Requires: pkgconfig
Requires: popt-devel

# From 2.3, libbonobo swallowed bonobo-activation
Obsoletes: bonobo-activation-devel
Provides: bonobo-activation-devel = %{version}-%{release}

Conflicts: bonobo-devel < 1.0.8

%description devel
Bonobo is a component system based on CORBA, used by the GNOME desktop.

This package contains header files used to compile programs that
use Bonobo.

%prep
%setup -q

%ifarch ppc64 s390x x86_64
%patch0 -p1 -b .multishlib
%endif

%build
autoreconf -vfi
%configure --enable-silent-rules \
	--disable-static \
	--enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

## kill other stuff (why? comment out by futoshi)
#rm $RPM_BUILD_ROOT%{_bindir}/echo-client-2
#rm $RPM_BUILD_ROOT%{_bindir}/bonobo-slay

for serverfile in $RPM_BUILD_ROOT%{_libdir}/bonobo/servers/*.server; do
    sed -i -e 's|location *= *"/usr/lib\(64\)*/|location="/usr/$LIB/|' $serverfile
done

# noarch packages install to /usr/lib/bonobo/servers
mkdir -p $RPM_BUILD_ROOT%{_prefix}/lib/bonobo/servers

%find_lang %{po_package}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
 
%postun -p /sbin/ldconfig

%files -f %{po_package}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README doc/NAMESPACE
%dir %{_sysconfdir}/bonobo-activation
%config %{_sysconfdir}/bonobo-activation/bonobo-activation-config.xml
%{_bindir}/activation-client
%{_bindir}/bonobo-activation-run-query
%{_bindir}/bonobo-slay
%{_bindir}/echo-client-2
%{_sbindir}/bonobo-activation-sysconf

%{_libdir}/libbonobo-*.so.*
%exclude %{_libdir}/*.la
%{_libdir}/bonobo
%{_libdir}/bonobo-2.0
%{_libdir}/orbit-2.0/Bonobo_module.*
%{_libexecdir}/bonobo-activation-server
%{_mandir}/man1/bonobo-activation-server.1.*

%files devel
%defattr(-,root,root)
%{_libdir}/libbonobo-*.so
%{_libdir}/pkgconfig/bonobo-activation-2.0.pc
%{_libdir}/pkgconfig/libbonobo-2.0.pc
%{_includedir}/libbonobo-2.0
%{_includedir}/bonobo-activation-2.0
%{_datadir}/idl/bonobo-2.0
%{_datadir}/idl/bonobo-activation-2.0
%{_datadir}/gtk-doc/html/libbonobo
%{_datadir}/gtk-doc/html/bonobo-activation

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for glib 2.33.2

* Mon Aug  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-4m)
- rebuild for new GCC 4.6

* Fri Apr 08 2011 McLellan Daniel <daniel.mclellan@gmail.com>
- (2.32.0-3m)
- added needed BuildRequires: glib2 >= 2.25.7 and gettext-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.3-3m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.24.3-2m)
- modify spec
- add libbonobo-multishlib.patch

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.0-2m)
- rebuild agaisst python-2.6.1-1m

* Mon Jul  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sun Dec 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Dec 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- delete libtool library

* Thu Apr  6 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-1m)
- version up.
- GNOME 2.14.0 Desktop

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-3m)
- comment out unnessesaly autoreconf and make check

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-2m)
- enable gtk-doc

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- version up.
- GNOME 2.12.1 Desktop

* Fri Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.0-1m)
- version 2.8.0
- GNOME 2.8 Desktop

* Sun Dec 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.2-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Sun Aug 14 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-3m)
- adjustment BuildPreReq

* Sat Apr 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.0-2m)
- add BuildPrereq: perl-XML-Parser >= 2.34

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.3-2m)
- revised spec for enabling rpm 4.2.

* Wed Jan 21 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.4.3-1m)
- version 2.4.3

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-1m)
- pretty spec file.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-1m)
- version 2.3.6

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Thu Jun 12 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.3.3-2m)
  ORBit2-devel >= 2.7.2

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Tue Jun 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Sun May 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-2m)
- merge bonobo-activation

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-10m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-9m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.1-2k)
- version 1.117.1

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.0-2k)
- version 1.117.0

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-4k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.0-2k)
- version 1.116.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-6k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-4k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-2k)
- version 1.115.0

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-6k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-4k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-2k)
- version 1.113.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-16k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-14k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-12k)
- modify dependency

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-10k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Wed Feb 27 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-8k)
- unuse intltoolize

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-6k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-4k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-2k)
- version 1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-8k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-6k)
- rebuild against for bonobo-activation-0.9.4

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-4k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-2k)
- version 1.111.0
* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-14k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-12k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-10k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-8k)
- rebuild against for libIDL-0.7.4

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-4k)
- rebuild against for glib-1.3.13

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-2k)
- version 1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.109.0-8k)
- rebuild against for linc-0.1.15

* Wed Dec 26 2001 Shingo Akagaki <dora@kondara.org>
- (1.108.0-2k)
- port from Jiraii
- version 1.108.0

* Tue Sep 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.102.0-3k)
- created spec file
