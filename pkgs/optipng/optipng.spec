%global momorel 1
%global makefile gcc.mak

Name:           optipng
Version:        0.6.5
Release:        %{momorel}m%{?dist}
Summary:        PNG optimizer and converter
Group:          Applications/Multimedia
License:        "zlib"
URL:            http://optipng.sourceforge.net/
Source0:        http://downloads.sourceforge.net/optipng/%{name}-%{version}.tar.gz
NoSource:       0
# http://sf.net/tracker/?func=detail&aid=3294326&group_id=151404&atid=780915
Patch0:         %{name}-0.6.5-setjmp-include.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  zlib-devel
BuildRequires:  libpng-devel

%description
OptiPNG is a PNG optimizer that recompresses image files to a smaller size,
without losing any information. This program also converts external formats
(BMP, GIF, PNM and TIFF) to optimized PNG, and performs PNG integrity checks
and corrections.

%prep
%setup -q
%patch0 -p1

# Ensure system libs and headers are used; as of 0.6.3 pngxtern will use
# the bundled headers if present even with -with-system-*, causing failures.
rm -rf lib/libpng lib/zlib

%build
./configure -with-system-zlib -with-system-libpng
cd src/
make -f scripts/%{makefile} %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS"\
                                            LDFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf $RPM_BUILD_ROOT
cd src/
make -f scripts/%{makefile} install DESTDIR="$RPM_BUILD_ROOT"\
                                    prefix="%{_prefix}" \
                                    man1dir="%{_mandir}/man1"

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README.txt LICENSE.txt doc/*
%{_bindir}/optipng
%{_mandir}/man1/optipng.1*

%changelog
* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.5-1m)
- import from Fedora for kdesdk-4.7.0 (kdesdk-scripts)

* Thu Apr 28 2011 Ville Skyttä <ville.skytta@iki.fi> - 0.6.5-1
- Update to 0.6.5.
- Patch to fix setjmp.h duplicate inclusion with system libpng.

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat May 15 2010 Till Maas <opensource@till.name> - 0.6.4-1
- update to new release

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Jul 19 2009 Ville Skyttä <ville.skytta@iki.fi> - 0.6.3-1
- Update to 0.6.3.
- Use %%global instead of %%define.

* Wed Feb 25 2009 Till Maas <opensource@till.name> - 0.6.2.1-1
- Update to new release to fix array overflow
- Red Hat Bugzilla #487364

* Wed Nov 12 2008 Till Maas <opensource@till.name> - 0.6.2-1
- Update to new release to fix buffer overflow
- Red Hat Bugzilla #471206

* Thu Aug 28 2008 Ville Skyttä <ville.skytta@iki.fi> - 0.6.1-1
- 0.6.1.

* Thu Feb 14 2008 Ville Skyttä <ville.skytta@iki.fi> - 0.5.5-4
- Apply sf.net patch #1790969 to fix crash with -log.
- Cosmetic specfile changes.

* Thu Aug 02 2007 Till Maas <opensource till name> - 0.5.5-3
- update License: Tag according to new Guidelines

* Wed Feb 14 2007 Till Maas <opensource till name> - 0.5.5-2
- rebuild because of new libpng

* Tue Feb 06 2007 Till Maas <opensource till name> - 0.5.5-1
- Version bump

* Wed Nov 29 2006 Till Maas <opensource till name> - 0.5.4-4
- splitting makefile patches
- make LDFLAGS=$RPM_OPT_FLAGS
- Use own makefile define
- Fixing 216784 with upstream patch

* Wed Oct 11 2006 Till Maas <opensource till name> - 0.5.4-3
- bumping release because of errors while importing to extras

* Tue Oct 10 2006 Till Maas <opensource till name> - 0.5.4-2
- shortening Summary

* Thu Sep 14 2006 Till Maas <opensource till name> - 0.5.4-1
- version bump
- use system zlib and libpng
- link without "-s" flag for non-empty debuginfo
- use DESTDIR

* Fri Jul 28 2006 Till Maas <opensource till name> - 0.5.3-1
- version bump
- Changed license tag back to zlib/libpng (#198616 rpmlint) 
- use $RPM_OPT_FLAGS instead of %%{optflags}

* Thu Jul 06 2006 Till Maas <opensource till name> - 0.5.2-2
- Changed license tag from zlib/libpng to zlib

* Tue Jul 04 2006 Till Maas <opensource till name> - 0.5.2-1
- Created from scratch for fedora extras
