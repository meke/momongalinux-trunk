%global momorel 4
Name:		gnome-phone-manager
Summary:	Gnome Phone Manager
Version: 	0.68.20120802
Release: %{momorel}m%{?dist}
License: 	GPLv2+
Group: 		Applications/System
#Source:		http://ftp.gnome.org/pub/GNOME/sources/gnome-phone-manager/%{version}/%{name}-%{version}.tar.xz
# git archive --format=tar --prefix=gnome-phone-manager-0.68.x/ HEAD | xz > ../gnome-phone-manager-0.68.x.tar.xz 
Source:		gnome-phone-manager-%{version}.tar.xz
#
Patch0:		new_eds_build.patch
URL: 		https://live.gnome.org/PhoneManager/
BuildRequires:	gtk3-devel
BuildRequires:	libcanberra-devel
BuildRequires:	gnome-bluetooth-libs-devel
BuildRequires:	bluez-libs-devel
BuildRequires:	gnokii-devel >= 0.6.31
BuildRequires:	gstreamer-devel >= 0.10
BuildRequires:	gnome-icon-theme >= 2.19.1
BuildRequires:	evolution-data-server-devel >= 3.5.90
BuildRequires:	gtkspell-devel
BuildRequires:	telepathy-glib-devel
BuildRequires:	intltool perl(XML::Parser)
BuildRequires:	gettext
BuildRequires:	desktop-file-utils
BuildRequires:	GConf2-devel

%description
This program will connect to your mobile phone over a serial port,
either via a cable, infrared (IrDA) or Bluetooth connection.

For example it listens for text messages, and when they arrive,
displays them on the desktop. A visual indicator is displayed in
the notification area, if one is presently added to the panel.

%package telepathy
Summary: Telepathy connection manager to send and receive SMSes
Group: Applications/System

%description telepathy
This program will connect to your mobile phone over a serial port,
either via a cable, infrared (IrDA) or Bluetooth connection.

This plugin to Telepathy allows you to send and receive messages using any
Instant Messaging application that uses Telepathy, such as Empathy.

%prep
%setup -q

%patch0 -p1 -b .evolution-data-server-35

./autogen.sh

%build
%configure
%make 

%install
make DESTDIR=%{buildroot} install

# This should be in empathy instead
install -m0644 -D telepathy/sms.profile %{buildroot}%{_datadir}/mission-control/profiles/sms.profile

rm %{buildroot}%{_libdir}/gnome-bluetooth/plugins/libphonemgr.a
rm %{buildroot}%{_libdir}/gnome-bluetooth/plugins/libphonemgr.la

%find_lang %{name}
desktop-file-install \
  --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category X-Fedora \
  %{buildroot}%{_datadir}/applications/gnome-phone-manager.desktop

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
        %{_sysconfdir}/gconf/schemas/gnome-phone-manager.schemas 	\
	>& /dev/null || :

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule 				\
        %{_sysconfdir}/gconf/schemas/gnome-phone-manager.schemas 	\
	>& /dev/null || :
fi

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule 				\
        %{_sysconfdir}/gconf/schemas/gnome-phone-manager.schemas 	\
	>& /dev/null || :
fi

%files -f %{name}.lang
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog.pre-gitlog NEWS README TODO
%{_sysconfdir}/gconf/schemas/gnome-phone-manager.schemas
%{_bindir}/gnome-phone-manager
%{_datadir}/applications/*.desktop
%{_datadir}/gnome-phone-manager/
%{_mandir}/man1/gnome-phone-manager.1.bz2
%{_libdir}/gnome-bluetooth/plugins/libphonemgr.so

%files telepathy
%{_libexecdir}/telepathy-phoney
%{_datadir}/telepathy/managers/*
%{_datadir}/dbus-1/services/*
%{_datadir}/mission-control/profiles/*

%changelog
* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.68.20120802-4m)
- rebuild for evolution-data-server-3.5.90

* Wed Aug 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.68.20120802-3m)
- set BR: evolution-data-server-devel >= 3.5.5

* Wed Aug 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.68.20120802-2m)
- import new_eds_build.patch from ubuntu to enable build

* Mon Aug 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.68.20120802-1m)
- fix build failure by updating to the latest snapshot

* Mon Aug 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.68-2m)
- reimport from fedora

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Thu Sep 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.66-1m)
- update to 0.66

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.65-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.65-11m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.65-10m)
- rebuild against evolution-2.32.0
- rebuild against gnome-bluetooth-2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.65-9m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.65-8m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.65-7m)
- add Requires: GConf2

* Thu Jul 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.65-6m)
- rebuild against gnokii-0.6.29

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.65-5m)
- rebuild against evolution-2.30.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.65-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.65-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.65-2m)
- To main

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.65-1m)
- update to 0.65

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.60-4m)
- rebuild against rpm-4.6

* Fri Dec 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.60-2m)
- rebuild against bluez-4.22 gnokii-0.6.27

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60-2m)
- rebuild against evolution-data-server-2.24.0

* Thu Jun 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60-1m)
- update to 0.60
- comment out patch0

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.51-3m)
- remove desktop-file-install section (to remove vendor="fedora")

* Tue May 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.51-2m)
- modify spec

* Sun May 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.51-1m)
- import from Fedora to Momonga

* Wed May 14 2008 - Bastien Nocera <bnocera@redhat.com> - 0.51-2
- Rebuild

* Fri Mar 21 2008 - Bastien Nocera <bnocera@redhat.com> - 0.51-1
- Update to 0.51

* Fri Mar 14 2008 - Bastien Nocera <bnocera@redhat.com> - 0.50-3
- Add a patch from upstream to fix connection to serial devices (#356861)

* Mon Mar 10 2008 - Bastien Nocera <bnocera@redhat.com> - 0.50-2
- Work-around for telepathy brokeness

* Mon Mar 10 2008 - Bastien Nocera <bnocera@redhat.com> - 0.50-1
- Update to 0.50

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.40-3
- Autorebuild for GCC 4.3

* Wed Dec 12 2007 - Bastien Nocera <bnocera@redhat.com> - 0.40-2
- Rebuild against new release of gnokii

* Wed Nov 14 2007 - Bastien Nocera <bnocera@redhat.com> - 0.40-1
- Update to 0.40
- Add experimental telepathy backend in a subpackage

* Mon Oct 29 2007 - Bastien Nocera <bnocera@redhat.com> - 0.30-1
- Update to 0.30
- Fix sending non-ASCII texts (for real)
- Add debugging improvements

* Wed Oct 17 2007 - Bastien Nocera <bnocera@redhat.com> - 0.20-1
- Update to 0.20

* Sun Aug 19 2007 - Bastien Nocera <bnocera@redhat.com> - 0.10-1
- Update to 0.10 (#253400)
- Remove obsolete work-arounds and dependencies
- Update BRs
- Add GConf scriptlets

* Fri Aug 17 2007 Linus Walleij <triad@df.lth.se> 0.8-6
- Update license field from GPL to GPLv2+

* Sun Nov 15 2006 Linus Walleij <triad@df.lth.se> 0.8-5
- Rebuild to pick up libbtctl i/f bump.

* Sun Oct 29 2006 Linus Walleij <triad@df.lth.se> 0.8-4
- Rebuild to pick up libedataserver i/f bump.

* Sun Oct 8 2006 Linus Walleij <triad@df.lth.se> 0.8-3
- Pick up intltool.

* Sun Oct 8 2006 Linus Walleij <triad@df.lth.se> 0.8-2
- Pick up libtool.

* Sun Oct 8 2006 Linus Walleij <triad@df.lth.se> 0.8-1
- New upstream version including patch.
- The icon is an an even weirder place now!
- Have to run som autotools on this one to get it working.

* Tue Sep 5 2006 Linus Walleij <triad@df.lth.se> 0.7-5
- Patch to compile with new version of E-D-S.

* Thu Aug 10 2006 Linus Walleij <triad@df.lth.se> 0.7-4
- Missing BR

* Thu Aug 3 2006 Linus Walleij <triad@df.lth.se> 0.7-3
- Updated after feedback from Chris Weyl
- Bogus problem with cellphone.png icon image

* Sat Jul 29 2006 Linus Walleij <triad@df.lth.se> 0.7-2
- Updated after feedback from Parag and Paul

* Tue Jun 27 2006 Linus Walleij <triad@df.lth.se> 0.7-1
- 0.7 Release
- Took Matthews nrpm package and Fedora Extrasificated it

* Fri Sep 09 2005 Matthew Hall <matt@nrpms.net> 0.6-1
- 0.6 Release

* Mon Jun 20 2005 Matthew Hall <matt@nrpms.net> 0.4-1
- 0.4 Release
