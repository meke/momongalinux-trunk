%global         momorel 4

Name:           perl-WWW-YouTube-Download
Version:        0.56
Release:        %{momorel}m%{?dist}
Summary:        Very simple YouTube video download interface
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/WWW-YouTube-Download/
Source0:        http://www.cpan.org/authors/id/X/XA/XAICRON/WWW-YouTube-Download-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008001
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-JSON
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Term-ANSIColor
BuildRequires:  perl-Test-Simple >= 0.98
BuildRequires:  perl-URI
BuildRequires:  perl-XML-TreePP
Requires:       perl-HTML-Parser
Requires:       perl-JSON
Requires:       perl-libwww-perl
Requires:       perl-Term-ANSIColor
Requires:       perl-URI
Requires:       perl-XML-TreePP
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
WWW::YouTube::Download is a download video from YouTube.

%prep
%setup -q -n WWW-YouTube-Download-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes eg LICENSE META.json README.md xt
%{_bindir}/youtube-download
%{_bindir}/youtube-playlists
%{perl_vendorlib}/WWW/YouTube/Download.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-2m)
- rebuild against perl-5.18.1

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-2m)
- rebuild against perl-5.18.0

* Tue May  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Sat Apr 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Wed Apr 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49

* Tue Apr  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Sun Mar 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Wed Mar 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-2m)
- rebuild against perl-5.16.3

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-2m)
- rebuild against perl-5.16.2

* Thu Oct  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Mon Oct 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Tue Oct 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Wed Oct 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-2m)
- rebuild against perl-5.14.2

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Mon Aug  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-2m)
- rebuild against perl-5.14.1

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-2m)
- rebuild for new GCC 4.6

* Sat Mar 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Fri Mar  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Tue Feb 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Wed Jan  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Thu Dec 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Wed Dec 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-2m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
