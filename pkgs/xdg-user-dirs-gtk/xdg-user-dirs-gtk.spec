%global momorel 7

Name:		xdg-user-dirs-gtk
Version:	0.8
Release:	%{momorel}m%{?dist}
Summary:	Gnome integration of special directories

Group:		User Interface/Desktops
License:	GPL
URL:		http://freedesktop.org/wiki/Software/xdg-user-dirs
Source0:	http://download.gnome.org/sources/xdg-user-dirs-gtk/%{version}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext, gtk2-devel, pkgconfig, perl-XML-Parser, xdg-user-dirs
Requires:	xdg-user-dirs

%description
Contains some integration of xdg-user-dirs with the gnome
desktop, including creating default bookmarks and detecting
locale changes.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}


%install
rm -rf --preserve-root $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
%find_lang %name

# wrapper script
mv %{buildroot}%{_bindir}/xdg-user-dirs-gtk-update \
  %{buildroot}%{_bindir}/xdg-user-dirs-gtk-update.bin

cat << __EOF > %{buildroot}%{_bindir}/xdg-user-dirs-gtk-update
#! /bin/sh
LANG=C xdg-user-dirs-gtk-update.bin
__EOF

chmod +x %{buildroot}%{_bindir}/xdg-user-dirs-gtk-update

%clean
rm -rf --preserve-root $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc NEWS AUTHORS README ChangeLog COPYING
%{_sysconfdir}/xdg/autostart/user-dirs-update-gtk.desktop
%{_bindir}/xdg-user-dirs-gtk-update.bin
%{_bindir}/xdg-user-dirs-gtk-update

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-5m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-4m)
- fix spec

* Sun Aug 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-3m)
- delete patch0 (autostart)

* Fri Aug  6 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8-2m)
- add patch0 (Excluded from autostart)

* Mon Jul 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-1m)
- update to 0.8
- delete patch0 merged

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7-4m)
- fix autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7-3m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-2m)
- rebuild against rpm-4.6

* Sat May 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7-1m)
- import from Fedora

* Tue Feb 12 2008 Alexander Larsson <alexl@redhat.com> - 0.7-1
- Update to 0.7
- Uncomment missing patches

* Sun Nov  4 2007 Matthias Clasen <mclasen@redhat.com> - 0.6-4
- Correct the URL

* Mon Oct  1 2007 Matthias Clasen <mclasen@redhat.com> - 0.6-2
- Fix the special case for en_US  (#307881)

* Tue Aug 21 2007 Alexander Larsson <alexl@redhat.com> - 0.6-1
- Update to 0.6 (new translations)

* Fri Jul  6 2007  Matthias Clasen  <mclasen@redhat.com> - 0.5-2
- Make the autostart file work in KDE (#247304)

* Wed Apr 25 2007  <alexl@redhat.com> - 0.5-1
- Update to 0.5
- Fixes silly dialog when no translations (#237384)

* Wed Apr 11 2007 Alexander Larsson <alexl@redhat.com> - 0.4-1
- update to 0.4 (#234512)

* Tue Mar  6 2007 Alexander Larsson <alexl@redhat.com> - 0.3-1
- update to 0.3
- Add xdg-user-dirs buildreq

* Fri Mar  2 2007 Alexander Larsson <alexl@redhat.com> - 0.2-1
- Update to 0.2

* Fri Mar  2 2007 Alexander Larsson <alexl@redhat.com> - 0.1-2
- Add buildrequires
- Mark autostart file as config

* Wed Feb 28 2007 Alexander Larsson <alexl@redhat.com> - 0.1-1
- Initial version

