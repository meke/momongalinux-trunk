%global         momorel 1

Name:           perl-Net-DNS
Version:        0.77
Release:        %{momorel}m%{?dist}
Summary:        Perl interface to the DNS resolver
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-DNS/
Source0:        http://www.cpan.org/authors/id/N/NL/NLNETLABS/Net-DNS-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Digest-HMAC >= 1
BuildRequires:  perl-Digest-MD5 >= 2.12
BuildRequires:  perl-Digest-SHA >= 5.23
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO
BuildRequires:  perl-MIME-Base64 >= 2.11
BuildRequires:  perl-Test-Simple >= 0.18
Requires:       perl-Digest-HMAC >= 1
Requires:       perl-Digest-MD5 >= 2.12
Requires:       perl-Digest-SHA >= 5.23
Requires:       perl-IO
Requires:       perl-MIME-Base64 >= 2.11
Requires:       perl-Test-Simple >= 0.18
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Net::DNS is a collection of Perl modules that act as a Domain Name System
(DNS) resolver. It allows the programmer to perform DNS queries that are
beyond the capabilities of gethostbyname and gethostbyaddr.

%prep
%setup -q -n Net-DNS-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Win32::IPHelper)/d' | \
  sed -e '/^perl(Win32::TieRegistry)/d'

EOF
%define __perl_requires %{_builddir}/Net-DNS-%{version}/%{name}-req
chmod +x %{name}-req

%build
for file in lib/Net/DNS/Update.pm contrib/* demo/*; do \
        sed 's|/usr/local/bin/perl|/usr/bin/perl|' $file > $file.new; \
        mv $file.new $file; \
done

%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changes README TODO contrib demo
%{perl_vendorarch}/Net/DNS
%{perl_vendorarch}/Net/DNS.pm
%{perl_vendorarch}/auto/Net/DNS
%{_mandir}/man?/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.77-1m)
- rebuild against perl-5.20.0
- update to 0.77

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-1m)
- update to 0.74
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-1m)
- update to 0.73

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-2m)
- rebuild against perl-5.16.3

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Thu Dec 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Wed Dec  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.69-1m)
- update to 0.69

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Mon Nov  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.66-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.66-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.66-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-2m)
- rebuild against perl-5.12.0

* Thu Dec 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-1m)
- update to 0.66

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.65-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.65-2m)
- rebuild against perl-5.10.1

* Tue Feb 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.65-1m)
- update to 0.65

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.63-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.63-2m)
- rebuild against gcc43

* Sun Feb 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- update to 0.63

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Fri Jun 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.60-1m)
- update to 0.60
- [SECURITY] CVE-2007-3377 CVE-2007-3377

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.59-2m)
- use vendor

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.58-1m)
- update to 0.58

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.57-1m)
- update to 0.57

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.55-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.55-1m)
- update to 0.55
- add BuildRequires: perl-Net-IP

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.48-2m)
- built against perl-5.8.7

* Mon Feb 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.48-1m)
- version up

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.47-3m)
- rebuild against perl-5.8.5
- rebuild against perl-Digest-HMAC 1.01-10m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.47-2m)
- remove Epoch from BuildRequires

* Wed May 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.47-1m)
- verup

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.42-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.39_02-4m)
- rebuild against perl-5.8.2
- rebuild against perl-Digest-HMAC 1.01-9m

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.39_02-3m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.39_02-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Mon Sep  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.39_02-1m)

* Sat Jan 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.33-1m)

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.31-2m)
- rebuild against perl-5.8.0

* Wed Nov 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.31-1m)

* Sun Sep 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.28-1m)

* Sat Aug 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.27-1m)

* Mon Aug 12 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.26-1m)

* Mon Aug  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.25-1m)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.24-1m)

* Sun Sep 16 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.12-2k)
