%global momorel 1

%define tarball xf86-video-intel
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 i810 video driver
Name:      xorg-x11-drv-intel
Version:   2.99.912
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0

License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.11.99
BuildRequires: libXvMC-devel
BuildRequires: mesa-libGL-devel >= 7.5
BuildRequires: libdrm-devel >= 2.4.23
BuildRequires: libxcb-devel >= 1.5
BuildRequires: xcb-util-devel >= 0.3.9
BuildRequires: xorg-x11-glamor-devel

Requires:  hwdata
Requires:  xorg-x11-server-Xorg >= 1.11.99

Obsoletes: xorg-x11-drv-i810
Provides: xorg-x11-drv-i810 = %{version}

%description 
X.Org X11 intel video driver.

%package devel
Summary:   Xorg X11 intel video driver XvMC development package
Group:     Development/System
Requires:  %{name} = %{version}-%{release}

Obsoletes: xorg-x11-drv-i810-devel
Provides: xorg-x11-drv-i810-devel = %{version}

%description devel
X.Org X11 intel video driver XvMC development package.

%prep
%setup -q -n %{tarball}-%{version}

autoreconf -if
%build
OPTS="--disable-static --libdir=%{_libdir} --mandir=%{_mandir} --enable-kms --enable-sna --enable-glamor "
./configure ${OPTS}
%make 

%install
rm -rf --preserve-root %{buildroot}

%make install DESTDIR=%{buildroot}

#FIXME
mv %{buildroot}/usr/{local/,}bin
rm -rf %{buildroot}//usr/{local/

find %{buildroot} -regex ".*\.la$" -delete

# Nuke until actually hooked up in the code
rm -f %{buildroot}/%{driverdir}/ch7017.so

# FIXME
mv %{buildroot}/usr/local/libexec %{buildroot}/usr/
mv %{buildroot}/usr/local/share/polkit-1 %{buildroot}/usr/share

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/intel-virtual-output
%{driverdir}/intel_drv.so
%{_libdir}/libIntelXvMC.so.1
%{_libdir}/libIntelXvMC.so.1.0.0
%{_libdir}/libI810XvMC.so.1
%{_libdir}/libI810XvMC.so.1.0.0
%{_mandir}/man4/i*
%{_libexecdir}/xf86-video-intel-backlight-helper
%{_datadir}/polkit-1/actions/org.x.xf86-video-intel.backlight-helper.policy


%files devel
%defattr(-,root,root,-)
%{_libdir}/libIntelXvMC.so
%{_libdir}/libI810XvMC.so

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.99.912-1m)
- update 2.99.912

* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.99.911-1m)
- update 2.99.911

* Mon Feb 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.99.909-2m)
- enable Galmor
 
* Mon Feb 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.99.909-1m)
- update 2.99.909
 
* Mon Feb 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.99.908-1m)
- update 2.99.908

* Sun Jan 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.99.907-1m)
- update 2.99.907

* Mon Sep 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.99.903-1m)
- update 2.99.903

* Mon Sep 09 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.99.902-1m)
- update 2.99.902

* Thu Aug 22 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.21.15-1m)
- update 2.21.15

* Fri Aug  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.14-1m)
- update 2.21.14

* Mon Jul 15 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.12-1m)
- update 2.21.12

* Sat Jul  6 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.11-1m)
- update 2.21.11

* Sun Jun 23 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.10-1m)
- update 2.21.10

* Fri Jun  7 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.9-1m)
- update 2.21.9

* Fri May 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.7-1m)
- update 2.21.7

* Sun Apr 14 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.6-1m)
- update 2.21.6

* Sun Mar 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.5-1m)
- update 2.21.5

* Sun Feb 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.21.3-1m)
- update 2.21.3

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.18-1m)
- update 2.20.18

* Thu Dec 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.17-1m)
- update 2.20.17

* Sat Dec 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.16-1m)
- update 2.20.16

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.15-1m)
- update 2.20.15

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.12-1m)
- update 2.20.12

* Mon Oct 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.10-1m)
- update 2.20.10

* Mon Oct  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.9-1m)
- update 2.20.9

* Mon Sep 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.8-1m)
- update 2.20.8

* Wed Sep 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.7-1m)
- update 2.20.7

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.6-1m)
- update 2.20.6

* Mon Aug 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.2-2m)
- rebuild for xcb-util

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.2-1m)
- update 2.20.2

* Mon Apr 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.0-1m)
- update 2.19

* Sat Feb 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.18.0-1m)
- update 2.18

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.17.0-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Nov 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.17.0-1m)
- update to 2.17.0

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.16.0-1m)
- update to 2.16.0

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.0-3m)
- rebuild for xcb-util-0.3.8

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15.0-2m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15.0-1m)
- update to 2.15.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.902-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.902-1m)
- update to 2.14.902

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.0-2m)
- BuildRequires libdrm-2.4.23

* Sun Jan  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.0-4m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.0-3m)
- rebuild for new GCC 4.5

* Thu Oct 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.13.0-2m)
- BR libdrm-2.4.22

* Tue Oct  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.13.0-1m)
- update 2.13.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.0-2m)
- full rebuild for mo7 release

* Fri Jun 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update 2.12.0

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11.0-2m)
- rebuild against xorg-server-1.8

* Wed Mar 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11.0-1m)
- update 2.11.0

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.0-1m)
- update 2.10.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.1-1m)
- update 2.9.1

* Wed Sep 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.0-1m)
- update 2.9.0

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.1-1m)
- update 2.8.1

* Fri Jul 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.0-1m)
- update 2.8.0

* Fri Jun  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-1m)
- update 2.7.1

* Sun May  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.0-3m)
- update BuildRequires: libdrm-devel >= 2.4.6

* Thu Apr 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.0-2m)
- adjustment spec

* Thu Apr 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.0-1m)
- update 2.7.0
- rename to xorg-x11-drv-intel

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.99.903-1m)
- update 2.6.99.903

* Sun Mar 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.99.902-1m)
- update 2.6.99.902

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.3-1m)
- update 2.6.3

* Tue Jan 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.1-1m)
- update 2.6.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-2m)
- rebuild against rpm-4.6

* Tue Nov 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.1-1m)
- update 2.5.1

* Tue Oct 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.0-1m)
- update 2.5.0

* Sun Oct 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.98.0-1m)
- update 2.4.98.0(2.5.0-RC)

* Sat Oct  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.2-3m)
- remove upstream patch. Its Buggy on EeePC901

* Sun Sep 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.2-2m)
- add upstream patch
-- Disable render standby
-- Don't allocate a pipe for hotplug detection

* Fri Aug 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.2-1m)
- update 2.4.2

* Fri Aug 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-2m)
- add no-mm patch

* Fri Aug 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-1m)
- update 2.4.1

* Fri Jul 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.0-1m)
- update 2.4.0

* Wed Jun 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-1m)
- update 2.3.2

* Thu May 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-1m)
- update 2.3.1

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-1m)
- update 2.3.0
- remove legacy i810 driver

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-2m)
- rebuild against gcc43

* Mon Feb 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-1m)
- update 2.2.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-2m)
- %%NoSource -> NoSource

* Fri Nov 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-1m)
- update 2.2.0

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-2m)
- rebuild against xorg-x11-server-1.4

* Wed Aug 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-1m)
- update 2.1.1

* Tue Aug  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-3m)
- i810_drv came to be installed. 

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-2m)
- support current libdrm
-- xf86-video-i810-1.7.4-libdrm-2.3.1.patch
-- xf86-video-intel-2.1.0-libdrm-2.3.1.patch

* Wed Jul  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-1m)
- update 2.1.0

* Wed Jun 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-2m)
- NoSource 1

* Wed Jun 27 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.4-1m)
- update to 1.7.4

* Mon Aug 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.5-1m)
- update 1.6.5

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-1m)
- update 1.6.0(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1.3-4m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1.3-3.1m)
- import to Momonga

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.4.1.3-3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Sat Feb  4 2006 Mike A. Harris <mharris@redhat.com> 1.4.1.3-3
- Added 8086:2772 mapping to i810.xinf for bug (#178451)

* Fri Feb  3 2006 Mike A. Harris <mharris@redhat.com> 1.4.1.3-2
- Added 8086:2592 mapping to i810.xinf for bug (#172884)

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.4.1.3-1
- Updated xorg-x11-drv-i810 to version 1.4.1.3 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.4.1.2-1
- Updated xorg-x11-drv-i810 to version 1.4.1.2 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.4.1-1
- Updated xorg-x11-drv-i810 to version 1.4.1 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.4.0.1-1
- Updated xorg-x11-drv-i810 to version 1.4.0.1 from X11R7 RC1
- Fix *.la file removal.
- Added 'devel' subpackage for XvMC .so
- Added 'BuildRequires: libXvMC-devel' for XvMC drivers.

* Mon Oct 3 2005 Mike A. Harris <mharris@redhat.com> 1.4.0-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Limit "ExclusiveArch" to x86, x86_64, ia64

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.4.0-0
- Initial spec file for i810 video driver generated automatically
  by my xorg-driverspecgen script.
