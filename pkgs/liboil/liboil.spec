%global momorel 5
Summary: Library of Optimized Inner Loops, CPU optimized functions
Name: liboil
Version: 0.3.17
Release: %{momorel}m%{?dist}

License: LGPL
Group: System Environment/Libraries
URL: http://liboil.freedesktop.org/
Source0: http://liboil.freedesktop.org/download/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: liboil-0.3.7-ppc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel, gcc-c++

%description
Liboil is a library of simple functions that are optimized for various CPUs.
These functions are generally loops implementing simple algorithms, such as
converting an array of N integers to floating-poing numbers or multiplying
and summing an array of N numbers. Clearly such functions are candidates for
significant optimization using various techniques, especially by using
extended instructions provided by modern CPUs (Altivec, MMX, SSE, etc.).


%package devel
Summary: Development files and static library for liboil
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}, pkgconfig

%description devel
Liboil is a library of simple functions that are optimized for various CPUs.
These functions are generally loops implementing simple algorithms, such as
converting an array of N integers to floating-poing numbers or multiplying
and summing an array of N numbers. Clearly such functions are candidates for
significant optimization using various techniques, especially by using
extended instructions provided by modern CPUs (Altivec, MMX, SSE, etc.).


%prep
%setup -q

%build
rm m4/gtk-doc.m4
gtkdocize --copy --docdir doc
cp doc/gtk-doc.make .
autoreconf -vfi
%configure \
    --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc AUTHORS BUG-REPORTING COPYING HACKING
%{_bindir}/oil-bugreport
%{_libdir}/liboil-*.so.*
%exclude %{_libdir}/liboil-*.la

%files devel
%defattr(-, root, root, 0755)
%{_includedir}/liboil-0.3
%{_libdir}/*.so
%{_libdir}/liboil*.a
%{_libdir}/pkgconfig/liboil-0.3.pc
%doc %{_datadir}/gtk-doc/html/liboil/

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.17-5m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.17-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.17-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.17-2m)
- full rebuild for mo7 release

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.17-1m)
- update to 0.3.17

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.16-3m)
- --enable-gtk-doc

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.16-3m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Apr 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.16-1m)
- update to 0.3.16

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.15-2m)
- rebuild against rpm-4.6

* Tue Jul  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.15-1m)
- update to 0.3.15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.14-2m)
- rebuild against gcc43

* Tue Mar 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.14-1m)
- update to 0.3.14

* Sat Mar  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.13-1m)
- update to 0.3.13

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.12-2m)
- %%NoSource -> NoSource

* Fri Jun  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.12-1m)
- update to 0.3.12

* Wed Mar 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.11-1m)
- update to 0.3.11

* Mon Sep 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.9-1m)
- update to 0.3.9
- delete libtool library

* Sun May  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8-1m)
- update to 0.3.8
- comment out patch0

* Tue Mar 14 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.3.7-2m)
- enable ppc build.

* Sun Feb 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.7-1m)
- import from FC
- update to 0.3.7

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.3.6-3.fc5.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.3.6-3.fc5.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Dec 22 2005 Warren Togami <wtogami@redhat.com> 0.3.6-3
- s390 build patch

* Mon Dec 19 2005 Warren Togami <wtogami@redhat.com> 0.3.6-2
- cleanup spec, remove static, and import into FC5 for gstreamer-0.10

* Wed Dec 14 2005 Matthias Saou <http://freshrpms.net/> 0.3.6-1
- Update to 0.3.6.

* Mon Nov 14 2005 Matthias Saou <http://freshrpms.net/> 0.3.5-3
- Sync spec files across branches.
- Parallel make seems to have worked for 0.3.5 on devel, but just in case...

* Sat Nov 12 2005 Thomas Vander Stichele <thomas at apestaart dot org> 0.3.5-2
- Trigger rebuild.

* Sat Nov 12 2005 Thomas Vander Stichele <thomas at apestaart dot org> 0.3.5-1
- Update to 0.3.5.

* Wed Oct 12 2005 Matthias Saou <http://freshrpms.net/> 0.3.3-3
- Add patch to disable unrecognized "-fasm-blocks" gcc option on PPC.

* Tue Oct  4 2005 Matthias Saou <http://freshrpms.net/> 0.3.3-2
- Update to 0.3.3.
- Update liboil-0.3.3-gccoptfixes.patch.

* Thu Jun 16 2005 Thomas Vander Stichele <thomas at apestaart dot org> 0.3.2-2
- Disable parallel make

* Wed May 25 2005 Matthias Saou <http://freshrpms.net/> 0.3.2-1
- Update to 0.3.2.
- Change ldconfig calls to be the program.
- Include new gtk-doc files in the devel package.
- add dist macro.

* Tue May 24 2005 Tom "spot" Callaway <tcallawa@redhat.com> - 0.3.0-4
- fix compilation error in FC-4 (bz #158641)
- use buildtime exported CFLAGS instead of making up its own

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 0.3.0-3
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Fri Jan 28 2005 Matthias Saou <http://freshrpms.net/> 0.3.0-1
- Update to 0.3.0.

* Wed Nov 24 2004 Matthias Saou <http://freshrpms.net/> 0.2.2-1
- Update to 0.2.2.

* Thu Nov  4 2004 Matthias Saou <http://freshrpms.net/> 0.2.0-1
- Initial RPM release.

