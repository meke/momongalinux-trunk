%global momorel 6

Name:           libglpng
Version:        1.45
Release:        %{momorel}m%{?dist}
Summary:        Toolkit for loading PNG images as OpenGL textures
Group:          System Environment/Libraries
License:        MIT
URL:            https://admin.fedoraproject.org/pkgdb/packages/name/libglpng
# Upstream's dead
Source0:        http://ftp.de.debian.org/debian/pool/main/libg/%{name}/%{name}_%{version}.orig.tar.gz
# From Debian - a Makefile. Yay.
Source1:        libglpng-1.45-makefile
# Debian patch, couple of small fixes.
Patch0:         libglpng-1.45-debian.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libpng-devel libGL-devel

%description
glpng is a small toolkit to make loading PNG image files as an OpenGL
texture as easy as possible.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q -n %{name}-%{version}.orig
%patch0 -p1
cp %{SOURCE1} Makefile


%build
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS -fPIC -Iinclude" libglpng.so.1.45


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT%{_prefix} LIB=%{_lib}


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc glpng.htm
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/GL/*
%{_libdir}/%{name}.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.45-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.45-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.45-2m)
- fix conflict dir

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45-1m)
- import from Rawhide for chromium-bsu

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.45-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun May 17 2009 Hans de Goede <hdegoede@redhat.com> 1.45-1
- Initial Fedora package, based on Mandriva package
