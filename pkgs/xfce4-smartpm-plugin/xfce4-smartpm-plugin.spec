%global momorel 10

%global xfce4ver 4.10.0
%global major 0.4

Name:           xfce4-smartpm-plugin
Version:        0.4.0
Release:        %{momorel}m%{?dist}
Summary:        Smart Package Manager plugin for the Xfce panel

Group:          Applications/System
License:        GPLv2
URL:            http://goodies.xfce.org/projects/panel-plugins/%{name}
Source:		http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gettext
BuildRequires:  libXt-devel
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}
Requires:	smart smart-gui

Provides:	xfce4-smart-plugin = %{version}
Obsoletes:	xfce4-smart-plugin <= %{version}

%description
Plugin for the Xfce4 panel that checks for package upgrades using the Smart
Package Manager. Install the "smart-update" package for silent channel updates.

%prep
%setup -q

%build
%configure --disable-static --program-prefix=""
%{__make}

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot} program-prefix=""
%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS ChangeLog README THANKS TODO
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_datadir}/icons/hicolor/*/apps/xfce*-*.png
%{_datadir}/icons/hicolor/*/apps/xfce*-*.svg

%changelog
* Thu Sep  6 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.0-10m)
- rebuild against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-9m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-8m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-5m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-4m)
- rebuild against xfce4-4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-1m)
- update
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-1m)
- initial Momonga package base on PLD
