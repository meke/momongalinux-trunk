%global momorel 1
%global poppler_version 0.20
%global glib2_version 2.25.9
%global dbus_version 0.70
%global theme_version 2.17.1
%global gxps_version 0.2.0

Name:           evince
Version:        3.6.1
Release: %{momorel}m%{?dist}
Summary:        Document viewer

License:        GPLv2+ and GFDL
Group:          Applications/Publishing
URL:            http://projects.gnome.org/evince/
Source0:        http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  gtk3-devel
BuildRequires:  glib2-devel >= %{glib2_version}
BuildRequires:  poppler-glib-devel >= %{poppler_version}
BuildRequires:  libXt-devel
BuildRequires:  libgnome-keyring-devel
BuildRequires:  libglade2-devel
BuildRequires:  libtiff-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libspectre-devel
BuildRequires:  gnome-doc-utils
BuildRequires:  scrollkeeper
BuildRequires:  gettext
BuildRequires:  desktop-file-utils
BuildRequires:  gnome-icon-theme >= %{theme_version}
BuildRequires:  libtool
BuildRequires:  gtk-doc
BuildRequires:  intltool
BuildRequires:  t1lib-devel
BuildRequires:  GConf2-devel
BuildRequires:  gobject-introspection-devel
# For autoconf.sh
BuildRequires:  gnome-common >= 2.26

# for the nautilus properties page
BuildRequires: nautilus-devel
# for the dvi backend
BuildRequires: kpathsea-devel
# for the djvu backend
BuildRequires: djvulibre-devel
# for the xps backend
BuildRequires:  libgxps-devel >= %{gxps_version}

Requires: %{name}-libs = %{version}-%{release}

%description
Evince is simple multi-page document viewer. It can display and print
Portable Document Format (PDF), PostScript (PS) and Encapsulated PostScript
(EPS) files. When supported by the document format, evince allows searching
for text, copying text to the clipboard, hypertext navigation,
table-of-contents bookmarks and editing of forms.

 Support for other document formats such as DVI and DJVU can be added by
installing additional backends.


%package libs
Summary: Libraries for the evince document viewer
Group: System Environment/Libraries

%description libs
This package contains shared libraries needed for evince


%package devel
Summary: Support for developing backends for the evince document viewer
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}

%description devel
This package contains libraries and header files needed for evince
backend development.


%package dvi
Summary: Evince backend for dvi files
Group: Applications/Publishing
Requires: %{name}-libs = %{version}-%{release}

%description dvi
This package contains a backend to let evince display dvi files.


%package djvu
Summary: Evince backend for djvu files
Group: Applications/Publishing
Requires: %{name}-libs = %{version}-%{release}

%description djvu
This package contains a backend to let evince display djvu files.


%package nautilus
Summary: Evince extension for nautilus
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Requires: nautilus

%description nautilus
This package contains the evince extension for the nautilus file manger.
It adds an additional tab called "Document" to the file properties dialog.

%prep
%setup -q

%build
./autogen.sh
%configure \
        --disable-static \
        --disable-scrollkeeper \
        --enable-introspection \
        --enable-comics=yes \
        --enable-dvi=yes \
        --enable-djvu=yes \
        --enable-xps=yes \
        --enable-t1lib=yes
make %{?_smp_mflags} V=1 LIBTOOL=/usr/bin/libtool

%install
make install DESTDIR=$RPM_BUILD_ROOT

desktop-file-install --delete-original --vendor="" \
  --dir=$RPM_BUILD_ROOT%{_datadir}/applications \
  --remove-category=Application \
  --remove-key=NoDisplay \
  $RPM_BUILD_ROOT%{_datadir}/applications/evince.desktop

%find_lang evince --with-gnome

mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications
/bin/rm -rf $RPM_BUILD_ROOT/var/scrollkeeper
# Get rid of static libs and .la files.
rm -f $RPM_BUILD_ROOT%{_libdir}/nautilus/extensions-3.0/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/nautilus/extensions-3.0/*.a
rm -f $RPM_BUILD_ROOT%{_libdir}/evince/4/backends/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/evince/4/backends/*.a
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a

# don't ship icon caches
rm -f $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/icon-theme.cache


%post
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%post libs -p /sbin/ldconfig

%postun
update-desktop-database &> /dev/null ||:
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null ||:

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null ||:

%postun libs -p /sbin/ldconfig

%files -f evince.lang
%{_bindir}/*
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/evince.*
%{_mandir}/man1/evince.1.bz2
%{_libexecdir}/evinced
%{_datadir}/dbus-1/services/org.gnome.evince.Daemon.service
%{_datadir}/glib-2.0/schemas/org.gnome.Evince.gschema.xml
%{_datadir}/GConf/gsettings/evince.convert
%{_datadir}/thumbnailers/evince.thumbnailer

%files libs
%doc README COPYING NEWS AUTHORS
%{_libdir}/libevview3.so.*
%{_libdir}/libevdocument3.so.*
%dir %{_libdir}/evince
%dir %{_libdir}/evince/4
%dir %{_libdir}/evince/4/backends
%{_libdir}/evince/4/backends/libpdfdocument.so
%{_libdir}/evince/4/backends/pdfdocument.evince-backend
%{_libdir}/evince/4/backends/libpsdocument.so
%{_libdir}/evince/4/backends/psdocument.evince-backend
%{_libdir}/evince/4/backends/libtiffdocument.so
%{_libdir}/evince/4/backends/tiffdocument.evince-backend
%{_libdir}/evince/4/backends/libcomicsdocument.so
%{_libdir}/evince/4/backends/comicsdocument.evince-backend
%{_libdir}/evince/4/backends/libxpsdocument.so
%{_libdir}/evince/4/backends/xpsdocument.evince-backend
%{_libdir}/girepository-1.0/EvinceDocument-3.0.typelib
%{_libdir}/girepository-1.0/EvinceView-3.0.typelib

%files devel
%{_datadir}/gtk-doc/html/evince/
%{_datadir}/gtk-doc/html/libevview-3.0
%{_datadir}/gtk-doc/html/libevdocument-3.0
%dir %{_includedir}/evince
%{_includedir}/evince/3.0
%{_libdir}/libevview3.so
%{_libdir}/libevdocument3.so
%{_libdir}/pkgconfig/evince-view-3.0.pc
%{_libdir}/pkgconfig/evince-document-3.0.pc
%{_datadir}/gir-1.0/EvinceDocument-3.0.gir
%{_datadir}/gir-1.0/EvinceView-3.0.gir
%doc %{_datadir}/help/*/evince

%files dvi
%{_libdir}/evince/4/backends/libdvidocument.so*
%{_libdir}/evince/4/backends/dvidocument.evince-backend

%files djvu
%{_libdir}/evince/4/backends/libdjvudocument.so
%{_libdir}/evince/4/backends/djvudocument.evince-backend

%files nautilus
%{_libdir}/nautilus/extensions-3.0/libevince-properties-page.so

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-4m)
- rebuild for glib 2.33.2

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-3m)
- rebuild against libtiff-4.0.1

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-2m)
- rebuild against poppler-0.18.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-2m)
- rebuild against poppler-0.18.0

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90.1-1m)
- update to 3.1.90.1

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Sun Jul 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-3m)
- fix build failure, which occurs when evince-devel is not installed

* Tue Jul 19 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (3.0.2-2m)
- Rebuild Against texlive-2010 (kpathsea 6)

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2
--enable-introspection=yes

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0
--enable-introspection=no

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-6m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32.0-5m)
- rebuild against poppler-0.16.4

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.32.0-4m)
- rebuild against poppler-0.16.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- add glib-compile-schemas script

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-4m)
- rebuild against poppler-0.14.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.3-4m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.30.3-3m)
- rebuild against kpathsea-2009 (TeX Live)

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-2m)
- split libs

* Thu Jun 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2
- delete patch0 (merged)

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-5m)
- rebuild against poppler-0.14.0

* Mon May 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-4m)
- add BuildRequires: poppler-glib-devel

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-3m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Tue May 11 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- imort patch0 from fedora

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Tue Apr 28 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.30.0-2m)
- fix strange BuildRequires

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0
- delete gtk-2.20-patch

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.29.91-2m)
- add patch for gtk-2.20 by gengtk220patch

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.5-1m)
- update to 2.29.5

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.2-2m)
- rebuild against djvulibre-3.5.22

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Thu Apr 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Sun Oct 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.24.1-2m)
- add BuildPreReq: libspectre-devel

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.2-3m)
- change %%preun script

* Sat Jun 14 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.2-2m)
- add BuildPreReq: kpathsea-devel

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.22.1.1-1m)
- update to 2.22.1.1
- rebuild against poppler-0.8.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0
- comment out patch0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.2-2m)
- %%NoSource -> NoSource

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri Sep  7 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.92-1m)
- update to 2.19.92

* Thu Jul  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-1m)
- update to 0.8.3

* Wed Jun 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-2m)
- add Requires: gamin

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-2m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-1m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2 (unstable)

* Wed Dec 06 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.1-2m)
- rebuild with djvulibre to enable djvu support

* Tue Oct 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Mon Oct  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-4m)
- add post script (gtk-update-icon-cache)

* Fri Sep 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-3m)
- remove category Application (desktop)

* Tue Sep 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-2m)
- modify patch 0 (text/plain)

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-4m)
- rebuild against dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.5.3-3m)
- rebuild against expat-2.0.0-1m

* Wed Jun 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-2m)
- enable commic
- delete devel package
- enable web browser to displauy pdf, but this is bug of gnome-vfs
-- all file can open evince (umm shome thing wrong)

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.5.7-3m)
- rebuild against dbus-0.61

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.7-2m)
- rebuild against openssl-0.9.8a

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.7-1m)
- update to 0.5.7

* Wed Jan  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-6m)
- add --disable-scrollkeeper --disable-schemas-install  to %configure

* Tue Jan  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-5m)
- rebuild against dbus-0.60-2m

* Mon Nov 28 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-5m)
- use /sbin/ldconfig instead of %%post(un) -p /sbin/ldconfig

* Mon Nov 28 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-4m)
- add %%post and %%preun for schemas
- add BuildRequires: ghostscript-devel

* Sun Nov 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.4.0-3m)
- delete "NoDisplay" entry from evince.desktop file

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-2m)
- delete autoreconf and make check

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- start
- GNOME 2.12.1 Desktop
