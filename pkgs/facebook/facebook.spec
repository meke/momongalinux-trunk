%global         momorel 2
%global         qtver 5.1.1

Summary:        Simple and stylish Facebook app
Name:           facebook
Version:        1.1
Release:        %{momorel}m%{?dist}
License:        GPL
Group:          Applications/Internet
URL:            http://anandbose.github.io/
Source0:        https://github.com/anandbose/facebook/archive/facebook-master.zip
Source1:        %{name}.desktop
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt5-qtbase-devel >= %{qtver}
BuildRequires:  qt5-qtwebkit-devel >= %{qtver}
BuildRequires:  ImageMagick
BuildRequires:  desktop-file-utils
Requires:       qt5-qtbase >= %{qtver}
Requires:       qt5-qtwebkit >= %{qtver}

%description
Simple and stylish Facebook app. This app provides native Facebook experience plus a system tray icon for providing notifications.

This app uses a simple approach for delivering notifications in the system tray. It reads the number of notifications,
messages and friend requests directly from the facebook page, using Javascript calls, without using any components
from Facebook SDK.

%prep
%setup -q -n %{name}-master

%build
qmake-qt5 PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
install -m0755 Facebook %{buildroot}%{_bindir}

# install and link icon
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32}/apps
mkdir -p %{buildroot}%{_datadir}/pixmaps
for size in 16 32; do
    convert -scale ${size}x${size} res/logo/%{name}-256x256.png %{buildroot}%{_datadir}/icons/hicolor/${size}x${size}/apps/%{name}.png
done
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category Qt \
  --add-category Network \
  %{SOURCE1}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{buildroot}%{_datadir}/icons/hicolor &> /dev/null ||:

%posttrans
gtk-update-icon-cache %{buildroot}%{_datadir}/icons/hicolor &> /dev/null ||:
update-desktop-database -q &> /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{buildroot}%{_datadir}/icons/hicolor &> /dev/null ||:
gtk-update-icon-cache %{buildroot}%{_datadir}/icons/hicolor &> /dev/null ||:
update-desktop-database -q &> /dev/null ||:
fi

%files
%defattr(-, root, root)
%doc README.md
%{_bindir}/Facebook
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Nov 24 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-2m)
- add category Network to dekstop file, good-bye Lost & Found!

* Wed Sep 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Wed Aug 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- initial build for Momonga Linux
