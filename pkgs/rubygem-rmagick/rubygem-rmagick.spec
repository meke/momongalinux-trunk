%global momorel 10
%global abi_ver 1.9.1

# Generated from rmagick-2.9.1.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname rmagick
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Ruby binding to ImageMagick
Name: rubygem-%{gemname}
Version: 2.13.1
Release: %{momorel}m%{?dist}
License: GPLv2+ or Ruby
Group: Development/Languages
URL: http://rubyforge.org/projects/rmagick
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Patch0: %{gemname}-%{version}-ImageMagick-6.8.3.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
BuildRequires: rubygems
BuildRequires: ImageMagick-devel >= 6.8.8.10
Provides: rubygem(%{gemname}) = %{version}
Obsoletes: ruby-rmagick
Provides: ruby-rmagick = %{version}-%{release}
Obsoletes: ruby-RMagick
Provides: ruby-RMagick = %{version}-%{release}

%description
RMagick is an interface between Ruby and ImageMagick.

%prep
%setup -q -c

gem unpack %{SOURCE0}
pushd %{gemname}-%{version}
%patch0 -p1
gem build %{gemname}.gemspec
mv %{gemname}-%{version}.gem ..
popd
rm -rf %{gemname}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force %{gemname}-%{version}.gem

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13.1-10m)
- rebuild against ImageMagick-6.8.8.10

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13.1-9m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13.1-8m)
- rebuild against ImageMagick-6.8.2.10

* Sun Dec 16 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.1-7m)
- apply pulsar's ImageMagick-6.8.0.patch to rebuild against ImageMagick-6.8.0.10

* Thu Dec  8 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.13.1-6m)
- use RbConfig

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13.1-5m)
- rebuild against ImageMagick-6.7.2.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.13.1-1m)
- update to 2.13.1

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.2-3m)
- rebuild against ImageMagick-6.6.2.10

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.2-2m)
- rebuild against ImageMagick-6.5.9.10

* Thu Dec 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.2-1m)
- update to 2.12.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.1-2m)
- Obsoletes and Provides ruby-{rmagick,RMagick}

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.1-1m)
- update to 2.12.1

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Mon May 04 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.1-1m)
- generate specfile by gem2rpm
- update to 2.9.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.17-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.17-2m)
- rebuild against ImageMagick-6.4.8.5-1m

* Tue Jan  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15.17-1m)
- update to 1.15.17
-- support new ParseSizeGeometry API in ImageMagick 6.4.6-9 or higher

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.15-1m)
- update to 1.15.15

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.14-1m)
- update to 1.15.14
- rebuild against ImageMagick-6.4.2.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.15.13-2m)
- rebuild against gcc43

* Sun Mar  2 2008 NARTIA Koichi <pulsar@momonga-linux.org>
- (1.15.13-1m)
- update to 1.15.13

* Mon Feb 11 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.15.11-2m)
- fix BuildPreReq

* Tue Dec 11 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.15.11-1m)
- up to 1.15.11
- require fixed version of ImageMagick (need rebuild)

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.10-1m)
- update to 1.15.10

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.7-1m)
- update to 1.15.7
- rebuild against ImageMagick-6.3.5-2m

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.14.1-2m)
- rebuild against ruby-1.8.6-4m

* Wed Oct 25 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (1.14.1-1m)
- version up 1.14.1

* Sun Jun  4 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.12.0-1m)
- version up

* Sun Feb 26 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.10.1-1m)
- version up

* Tue Feb 14 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.10.0-2m)
- rebuild against ImageMagick-6.2.6-1m

* Fri Feb  3 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.10.0-1m)
- version up

* Fri Dec 23 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.9.3-2m)
- enable all doc scripts again

* Sun Dec 18 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.9.3-1m)
- update to 1.9.3
- rebuild against ImageMagick-6.2.5

* Sun May  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.0-1m)
- version up

* Thu Feb 24 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.2-1m)
- bugfix release

* Sun Jan 23 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.1-2m)
- disable some scripts temporarily that will not run with ImageMagick-6.1.8

* Wed Jan  5 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.1-1m)
- version up

* Tue Dec 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.0-1m)
- version up

* Wed Dec  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.2-1m)
- bug fix release

* Wed Nov 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-1m)

* Fri Nov 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.0-1m)

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.0-3m)
- rebuild against ruby-1.8.2

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.0-2m)
- revised spec for rpm 4.2.

* Tue Feb 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.0-1m)

* Fri Dec 12 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.3.2-1m)
- bug fix release

* Tue Dec  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.3.1-1m)
- initial import
