%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-iniparse
Version:        0.4
Release:        %{momorel}m%{?dist}
Summary:        Python Module for Accessing and Modifying Configuration Data in INI files
Group:          Development/Libraries
License:        MIT/X
URL:            http://code.google.com/p/iniparse/
Source0:        http://iniparse.googlecode.com/files/iniparse-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: python-setuptools-devel
BuildArch: noarch

%description
iniparse is an INI parser for Python which is API compatible
with the standard library's ConfigParser, preserves structure of INI
files (order of sections & options, indentation, comments, and blank
lines are preserved when data is updated), and is more convenient to
use.

%prep
%setup -q -n iniparse-%{version}

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
# fixes
chmod 644 $RPM_BUILD_ROOT//usr/share/doc/iniparse-%{version}/index.html
mv $RPM_BUILD_ROOT/usr/share/doc/iniparse-%{version} $RPM_BUILD_ROOT/usr/share/doc/python-iniparse-%{version}

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc  %{_docdir}/python-iniparse-%{version}/*
%{python_sitelib}/*



%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-1m)
- update to 0.4
- set NoSource: 0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-1m)
- update 0.3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4-2m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.4-1m)
- support python-2.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.3-1m)
- import from Fedora to Momonga

* Tue Dec 11 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2.3-3
- handle egg-info too
* Tue Dec 11 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2.3-2
- removed patch source line
* Tue Dec 11 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2.3-1
- Updates to release 0.2.3
- removed empty ini file patch, it is included in 0.2.3
* Mon Nov 19 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2.2-2
- Added upstream patch to fix problems with empty ini files.
* Tue Sep 25 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2.2-1
- Updated to release 0.2.2
- removed patch to to fix problems with out commented lines, included in upstream source
* Wed Sep 12 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2.1-4
- Added some logic to get the right python-setuptools buildrequeres
- based on the fedora version, to make the same spec file useful in
- all fedora releases.
* Mon Sep 10 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2.1-3
- Added patch from upstream svn to fix problems with out commented lines.
* Tue Aug 28 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2.1-2
- Changed BuildRequires python-setuptools to python-setuptools-devel
* Tue Aug 7 2007 Paramjit Oberoi <param@cs.wisc.edu> - 0.2.1-1
- Release 0.2.1
* Fri Jul 27 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2-3
- relocated doc to %{_docdir}/python-iniparse-%{version}
* Thu Jul 26 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2-2
- changed name from iniparse to python-iniparse
* Tue Jul 17 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2-1
- Release 0.2
- Added html/* to %%doc
* Fri Jul 13 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.1-1
- Initial build.
