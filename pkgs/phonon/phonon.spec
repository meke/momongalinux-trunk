%global momorel 3
%global srcver 4.7.1

## qtver and qtrel need to be synchronized with qt
%global qtver 4.8.5
%global qtrel 1m
## obso_rel is fixed to 3m
%global obso_ver 4.7.2
%global obso_rel 3m

%global cmakever 2.8.5
%global cmakerel 2m
%global pulseaudiover 0.9.22
%global phonon_backend_vlc_ver 0.3.2
%global sourcedir stable/%{name}/%{version}

Summary: Multimedia framework api
Name: phonon 
Version: %{srcver}
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPLv2+
URL: http://phonon.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
## upstreamable patches
# fix rootDir (set hard at install-time and not rely on fragile relInstallDir
Patch53: phonon-4.7.0-rootDir.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automoc4 >= 0.9.86
BuildRequires: cmake >= %{cmakever}
BuildRequires: glib2-devel
BuildRequires: libxcb-devel
BuildRequires: libxml2-devel
BuildRequires: pkgconfig
BuildRequires: pulseaudio-libs-devel >= %{pulseaudiover}
BuildRequires: qt-devel >= %{qtver}-%{qtrel}
## Beware bootstrapping, have -Requires/+Requires this for step 0, then build at least one backend
Requires: phonon-backend-gstreamer >= 4.4.4
Requires: phonon-backend-vlc >= %{phonon_backend_vlc_ver}
Requires: pulseaudio-libs >= %{pulseaudiover}
Requires: qt >= %{_qtver}
Conflicts: qt-designer-plugin-phonon = %{obso_ver}-%{obso_rel}
Provides: qt-designer-plugin-phonon = %{qtver}-%{qtrel}

Obsoletes: phonon-backend-xine

%description
%{summary}.

%package devel
Summary: Developer files for %{name}
Group:   Development/Libraries
Requires(pre): coreutils
Requires: %{name} = %{version}-%{release}
Requires: qt-devel
Requires: pkgconfig

%description devel
%{summary}.

%prep
%setup -q

%patch53 -p1 -b .rootDir

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} \
    -DPHONON_QT_MKSPECS_INSTALL_DIR:PATH=%{_libdir}/qt4/mkspecs/modules \
    -DPHONON_INSTALL_QT_EXTENSIONS_INTO_SYSTEM_QT:BOOL=ON \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# symlink for qt/phonon compatibility
mkdir -p %{buildroot}%{_includedir}/phonon
ln -s ../KDE/Phonon %{buildroot}%{_includedir}/phonon/Phonon

# own these dirs
mkdir -p %{buildroot}%{_kde4_libdir}/kde4/plugins/phonon_backend/
mkdir -p %{buildroot}%{_kde4_datadir}/kde4/services/phononbackends/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%pre devel
rm -rf %{_includedir}/phonon/Phonon

%files
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_libdir}/libphonon*.so.4*
%{_datadir}/dbus-1/interfaces/org.kde.Phonon.AudioOutput.xml
# kde4-specific dirs
%{_qt4_plugindir}/designer/libphononwidgets.so
%dir %{_kde4_libdir}/kde4/plugins/phonon_backend
%dir %{_kde4_datadir}/kde4/services/phononbackends

%files devel
%defattr(-,root,root,-)
%{_includedir}/KDE/Phonon
%{_includedir}/phonon
%{_libdir}/pkgconfig/phonon.pc
%{_libdir}/libphonon*.so
%{_libdir}/cmake/phonon
%{_libdir}/qt4/mkspecs/modules/qt_phonon.pri
%{_datadir}/phonon

%changelog
* Sun Feb  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-3m)
- Obsoletes: phonon-backend-xine

* Sun Feb  2 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.1-2m)
- remove Requires: phonon-backend-xine

* Sat Dec  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to 4.7.1

* Tue Nov  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to 4.7.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-3m)
- rebuild for glib 2.33.2

* Thu Mar 29 2012 NARITA koichi <pulsar@momonga-linux.org>
- (4.6.0-2m)
- import two patches from Fedora

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to 4.6.0

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to 4.5.1

* Wed Aug 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.0-3m)
- fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.0-2m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to 4.5.0

* Sat Jan 22 2011 NARITA Koichi <pulsar@momomga-linux.org>
- (4.4.4-1m)
- update to 4.4.4
- phonon-backend-gstreamer and phonon-backend-xine are separated

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-3m)
- release %%{_includedir}/KDE

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.3-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-1m)
- update to 4.4.3

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.2-5m)
- full rebuild for mo7 release

* Fri Aug 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.2-4m)
- revise %%pre devel

* Fri Aug 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.2-3m)
- add %%pre devel to enable upgrading from STABLE_6

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-2m)
- rebuild against qt-4.6.3-1m

* Wed Jun  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to 4.4.2

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to 4.4.1

* Mon Mar 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-2m)
- NoSurce

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to 4.4.0

* Mon Jan 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-2m)
- import upstream patch from Fedora devel
-- backport GStreamer backend bugfixes, fix random disappearing sound under KDE

* Sat Dec  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to 4.3.80
- revive phonon, phonon-devel, phonon-backend-gstreamer

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.1-4m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-2m)
- this package provides phonon-backend-xine only

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to 4.3.1
- apply apstream patches
- add some BuildRequres

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to 4.3.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to 4.2.96

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.81-0.20090103.2m)
- update Patch0 for fuzz=0

* Sat Jan  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.81-0.20090103.1m)
- update to 4.2.80 svn 20090103 snapshot

* Tue Dec 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.80-1m)
- version 4.2.80
- BuildRequires: ImageMagick to convert icons

* Tue Dec 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.1-0.20081107.2m)
- apply xine-pulseaudio.patch
- BuildRequires: xine-lib

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-0.20081107.1m)
- update 4.2.1 svn 20081107 snapshot

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-0.20081012.1m)
- update 4.2.1 svn 20081012 snapshot

* Thu Sep  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-0.855256.1m)
- update to rev.855256 snapshot

* Wed Sep  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-3m)
- import pulseaudio.patch from Fedora
 +* Sun Aug 10 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.2.0-4
 +- fix PulseAudio not being the default in the Xine backend (4.2 regression)

- * Mon Sep  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.1-0.855256.1m)
- - update to rev.855256 snapshot

- * Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.1-0.849968.1m)
- - update to rev.849968 snapshot
- - rename from phonon-backend-gst to phonon-backend-gstreamer

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.0-2m)
- change BuildRequires: gst-plugins-base-devel to gstreamer-plugins-base-devel

* Fri Jul 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to 4.2.0 (stable version)

* Wed Jun 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2-0.2.1m)
- import from Fedora devel

* Fri Jun 20 2008 Rex Dieter <rdieter@fedoraproject.org> 4.2-0.2.beta2
- phonon 4.2beta2 (aka 4.1.83)

* Sat Jun 14 2008 Rex Dieter <rdieter@fedoraproject.org> 4.2-0.1.20080614svn820634
- first try

