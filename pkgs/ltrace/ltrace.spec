%global momorel 11

Summary: Tracks runtime library calls from dynamically linked executables
Name: ltrace
Version: 0.5
Release: %{momorel}m%{?dist}
URL: http://alioth.debian.org/projects/ltrace/
#Source0: http://ftp.debian.org/pool/main/l/ltrace/%{name}_%{version}.orig.tar.gz
#NoSource: 0
Source0: %{name}-%{version}.tar.gz
License: GPLv2+
Group: Development/Debuggers
ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64 s390 s390x alpha sparc
BuildRequires: elfutils-libelf-devel dejagnu 
BuildRequires: sed
BuildRequires: libstdc++-static
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpm/specopt/ltrace.specopt and edit it.
%{?include_specopt}
%{?!do_test: %global do_test 0}

Patch0: ltrace-0.4-exec.patch
Patch1: ltrace-0.4-fork.patch
Patch2: ltrace-0.5-opd.patch
Patch3: ltrace-ppc32fc5.patch
Patch4: ltrace-0.5-gnuhash.patch
Patch5: ltrace-0.5-testsuite.patch
Patch6: ltrace-0.5-ppc-symval.patch
Patch7: ltrace-0.5-a2bp.patch
Patch8: ltrace-0.5-attach.patch
Patch9: ltrace-0.5-fork.patch
Patch10: ltrace-0.5-exec.patch
Patch11: ltrace-0.5-exec-tests.patch
Patch12: ltrace-0.5-man.patch
Patch13: ltrace-0.5-ia64-sigill.patch
Patch14: ltrace-0.5-build.patch
Patch15: ltrace-0.5-o.patch
Patch1000: install-perm.patch

%description
ltrace is a debugging program which runs a specified command until it
exits.  While the command is executing, ltrace intercepts and records
the dynamic library calls which are called by
the executed process and the signals received by that process.
It can also intercept and print the system calls executed by the program.

The program to be traced need not be recompiled for this, so you can
use it on binaries for which you don't have the source handy.

You should install ltrace if you need a sysadmin tool for tracking the
execution of processes.
 
%prep
%setup -q
%patch0 -p0
%patch1 -p1
%patch2 -p1
%patch3 -p0
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1
%patch14 -p1
%patch15 -p1

%patch1000 -p0 -b .install-perm

%build
export CC="gcc`echo $RPM_OPT_FLAGS | sed -n 's/^.*\(-m[36][124]\).*$/ \1/p'` -D_LARGEFILE64_SOURCE"
%configure CC="$CC"
make %{?_smp_mflags}

%if %{do_test}
%check
echo ====================TESTING=========================
make check || :
echo ====================TESTING END=====================
%endif

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall 
%{__rm} -rf %{buildroot}/usr/share/doc/ltrace

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/ltrace
%{_mandir}/man1/ltrace.1*
%doc COPYING README TODO BUGS ChangeLog
%config(noreplace) %{_sysconfdir}/ltrace.conf

%changelog
* Fri Apr 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-11m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-6m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-5m)
- modify CC environment variable value
- import Patch14,15 from Rawhide (0.5-12.45svn)
- update Patch9,10 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-4m)
- rebuild against gcc43

* Wed Oct 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-3m)
- fix CFLAGS 

* Wed Oct 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.5-2m)
- add "-D_GNU_SORCE" to CFLAGS for glibc-2.7

* Sat Jun 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-1m)
- sync with fc-devel (ltrace-0_5-7_45svn_fc7)
- add do_test option, which enables %%check section for make test

* Fri May 19 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.4-1m)
- upgrade to 0.4
- add install-perm.patch

* Wed Oct 19 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.3.36-2m)
- enable x86_64, ppc, alpha

* Thu Feb 24 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.36-1m)
- version 0.3.36 
- change source URI
- revised specfile

* Sun Aug 22 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.3.35-1m)
- update to 0.3.35

* Tue May 18 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.32-1m)
- verup

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.31-2m)
- revised spec for enabling rpm 4.2.

* Sat Dec  27 2003 mutecat <mutecat@momonga-linux.org>
- (0.3.31-1m)
- update to 0.3.31
- config noreplace

* Tue Apr  2 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3.26-2k)

* Mon Mar  4 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3.23-2k)
- fix a broken symlink of ChangeLog

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.3.16-2k)
- update to 0.3.16

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.3.10-6k)
- added configure.mandir.enable.patch for compatibility

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Thu Feb 17 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Kondara Adaptations.

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description

* Fri Jan  7 2000 Jeff Johnson <jbj@redhat.com>
- update to 0.3.10.
- include (but don't apply) sparc patch from Jakub Jellinek.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Fri Mar 12 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.3.6.

* Mon Sep 21 1998 Preston Brown <pbrown@redhat.com>
- upgraded to 0.3.4
