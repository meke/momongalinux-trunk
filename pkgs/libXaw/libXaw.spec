%global momorel 1

Summary: X.Org X11 libXaw runtime library
Name: libXaw
Version: 1.0.12
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: xorg-x11-proto-devel
BuildRequires: libX11-devel
BuildRequires: libXt-devel
BuildRequires: libXmu-devel
BuildRequires: libXpm-devel
# configure doesn't complain about libXext missing, but the build fails
# without it.
BuildRequires: libXext-devel

%description
X.Org X11 libXaw runtime library

%package devel
Summary: X.Org X11 libXaw development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3
Requires: libXmu-devel

%description devel
X.Org X11 libXaw development package

%prep
%setup -q

# Disable static library creation by default.
%define with_static 0

%build
%configure --disable-xaw8 \
    --enable-docs \
%if ! %{with_static}
	--disable-static
%endif
%make

%install
rm -rf --preserve-root %{buildroot}

%makeinstall

# We intentionally don't ship *.la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog
%{_libdir}/libXaw.so.6
%{_libdir}/libXaw.so.7
%{_libdir}/libXaw6.so.6
%{_libdir}/libXaw6.so.6.0.1
%{_libdir}/libXaw7.so.7
%{_libdir}/libXaw7.so.7.0.0

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/X11/Xaw
%{_includedir}/X11/Xaw/*.h
# FIXME:  Is this C file really supposed to be here?
%{_includedir}/X11/Xaw/Template.c
%{_libdir}/libXaw.so
%if %{with_static}
%{_libdir}/libXaw6.a
%{_libdir}/libXaw7.a
%endif
%{_libdir}/libXaw6.so
%{_libdir}/libXaw7.so

%{_libdir}/pkgconfig/xaw6.pc
%{_libdir}/pkgconfig/xaw7.pc

%{_mandir}/man3/*.3*

%{_datadir}/doc/%{name}

%changelog
* Mon Sep 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.12-1m)
- update 1.0.12

* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.11-1m)
- update 1.0.11

* Sun Mar 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.10-2m)
- remove unused x86_64 patch

* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.10-1m)
- update 1.0.10

* Sun Jan  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-3m)
- build fix for x86_64

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-2m)
- rebuild for new GCC 4.6

* Wed Jan 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.9-1m)
- update 1.0.9

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-1m)
- update 1.0.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-1m)
- update 1.0.7

* Sat Jul 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-4m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-3m)
- use autoreconf (for autoconf-2.63)

* Wed Dec  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-2m)
- add patch0 for xsm xdm
-- from cygwin-ports

* Tue Nov 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.5-1m)
- update 1.0.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- %%NoSource -> NoSource

* Wed Aug 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated libXaw to version 1.0.1 from X11R7.0

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated libXaw to version 1.0.0 from X11R7 RC4

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Updated libXaw to version 0.99.3 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Sat Nov 12 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Added libXaw-0.99.2-bug-173027-libtool-sucks.patch to fix bug #173027,
  added 'autoconf' invocation prior to configure, and conditionalized it
  all with with_libtool_sucks_workaround macro.
- Added _smp_mflags to make invocation.
- Use *.h glob in file manifest instead of listing each header individually.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated libXaw to version 0.99.2 from X11R7 RC2

* Mon Oct 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated libXaw to version 0.99.1 from X11R7 RC1
- Update file manifest to find manpages in "man3x"
- Added {_includedir}/X11/Xaw/Template.c to file manifest

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-5
- Renamed package to remove xorg-x11 from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro
- Fix all "BuildRequires:" deps with s/xorg-x11-//g

* Wed Aug 25 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-4
- Added dependency on xorg-x11-libXmu-devel to devel subpackage, as libXaw
  headers include libXmu headers directly which caused xkbutils to fail to
  build.

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Changed all virtual BuildRequires to the "xorg-x11-" prefixed non-virtual
  package names, as we want xorg-x11 libs to explicitly build against
  X.Org supplied libs, rather than "any implementation", which is what the
  virtual provides is intended for.

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
