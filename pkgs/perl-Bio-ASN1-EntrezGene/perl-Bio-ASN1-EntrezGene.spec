%global         momorel 3

Name:           perl-Bio-ASN1-EntrezGene
Version:        1.70
Release:        %{momorel}m%{?dist}
Epoch:          1
Summary:        Regular expression-based Perl Parser for NCBI Entrez Gene
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Bio-ASN1-EntrezGene/
Source0:        http://www.cpan.org/authors/id/C/CJ/CJFIELDS/Bio-ASN1-EntrezGene-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-bioperl >= 1.6.920
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Bio::ASN1::EntrezGene is a regular expression-based Perl Parser for NCBI
Entrez Gene genome databases
(http://www.ncbi.nih.gov/entrez/query.fcgi?db=gene). It parses an ASN.1-
formatted Entrez Gene record and returns a data structure that contains all
data items from the gene record.

%prep
%setup -q -n Bio-ASN1-EntrezGene-%{version}

# remove all execute bits from the doc stuff
# so that dependency generator doesn't try to fulfill deps
find examples -type f -exec chmod -x {} 2>/dev/null ';'

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc examples
%doc Changes LICENSE README.md
%{perl_vendorlib}/Bio/ASN1/EntrezGene
%{perl_vendorlib}/Bio/ASN1/EntrezGene.pm
%{perl_vendorlib}/Bio/ASN1/Sequence
%{perl_vendorlib}/Bio/ASN1/Sequence.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-2m)
- rebuild against perl-5.18.2

* Sun Sep 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-1m)
- update to 1.70

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-16m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-15m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-14m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-13m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-12m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-11m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.10-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.10-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.10-4m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.10-3m)
- add epoch to %%changelog

* Thu Jun 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:1.10-2m)
- add Epoch: 1 to enable upgrading from STABLE_6

* Wed Jun  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.091-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.091-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.091-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.091-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.091-3m)
- rebuild against perl-5.10.1

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.091-2m)
- remove duplicate directories

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.091-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.091-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.091-6
Rebuild for new perl

* Tue Mar 04 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.091-5
- rebuild for new perl

* Tue Sep 04 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.091-4
- Clarified license terms: GPL+ or Artistic

* Mon Apr 15 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.091-3
- Bump for rebuild now that perl-bioperl is in the repo, which fulfills
  the Requires for this package.

* Mon Apr 02 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.091-2
- Include the "examples" subdirectory as documentation as per comments
  from Chris Weyl.  Strip executable bits from these scripts.

* Fri Mar 23 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.091-1
- Disable tests because of circular BuildRequires with perl-bioperl.
- Specfile autogenerated by cpanspec 1.69.1.
