%global         modname Jcode
%global         momorel 22

Summary:        Perl extension interface to convert Japanese text
Name:           perl-%{modname}
Version:        2.07
Release:        %{momorel}m%{?dist}
License:        GPL or Artistic
Group:          Development/Languages
URL:            http://search.cpan.org/dist/Jcode/
Source0:        http://www.cpan.org/authors/id/D/DA/DANKOGAI/%{modname}-%{version}.tar.gz
NoSource:       0
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.8.5

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is a Perl extension interface to convert Japanese text.

%prep
%setup -q -n %{modname}-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%doc Changes Changes.ver0X README
%defattr(-, root, root)
%{perl_vendorlib}/Jcode*
%{_mandir}/man?/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-17m)
- rebuild against perl-5.16.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-16m)
- change primary site and download URIs

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.07-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.07-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.07-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.07-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.07-2m)
- rebuild against rpm-4.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.06-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.06-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.06-2m)
- use vendor

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.06-1m)
- update to 2.06

* Wed May 24 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (2.05-1m)
- update to 2.05

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.04-1m)
- update to 2.04

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.03-2m)
- built against perl-5.8.8

* Sun Jul 31 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.03-1m)
- version 2.03
- use %%NoSource

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.00-2m)
- rebuilt against perl-5.8.7

* Mon May 23 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.00-1m)
- version 2.00
  Jcode now acts as a wrapper to Encode.
- made a noarch package
- add Changes.ver0X to %%doc

* Sat Dec	 4 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.88-1m
- version 0.88

* Wed Sep 29 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.87-1m)
- version 0.87

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.86-3m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.86-2m)
- remove Epoch from BuildRequires

* Wed Jun 23 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.86-1m)
- version 0.86

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.83-5m)
- revised spec for rpm 4.2.

* Sun Nov	 9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.83-4m)
- rebuild against perl-5.8.2

* Sat Nov	 1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.83-3m)
- rebuild against perl-5.8.1

* Fri Oct 24 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.83-2m)
- adapt the License: preamble for the Momonga Linux license
	expression unification policy (draft)
- fix %%files

* Fri Jul 11 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.83-1m)
- version 0.83

* Fri Nov 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.82-2m)
- rebuild against perl-5.8.0

* Tue Sep 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.82-1m)

* Sun Sep 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.81-1m)

* Sun Sep 15 2002 HOSONO Hidetomo <h@h12o.org>
- (0.80-1m)
- version up

* Sat Aug	 3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.79-5m)
- fix %files (HISTORY -> Changes)

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.79-4k)
- rebuild against for perl-5.6.1

* Tue Jan	 5 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (0.79-2k)
- update to 0.79

* Thu Dec 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.76-2k)

* Mon Sep	 3 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.75-2k)

* Sun Jun 3 2001 Toru Hoshina <toru@df-usa.com>
- (0.72-2k)

* Sat Apr 7 2001 SAKUMA Junichi <fk5j-skm@asahi-net.or.jp>
- (0.68-5k)
- version 0.68
- added prerequirement perl-MIME-Base64

* Wed Nov 29 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- use perl_archlib & perl_sitearch macro

* Fri Aug 18 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.600.

* Sat Mar 11 2000 Motonobu Ichiura <famao@kondara.org>
- fix .packlist problem

* Sat Jan 15 2000 Kazuyuki Funada <fun@shikoku.ne.jp>
- 1st release
