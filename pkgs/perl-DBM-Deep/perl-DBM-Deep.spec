%global         momorel 2

Name:           perl-DBM-Deep
Version:        2.0011
Release:        %{momorel}m%{?dist}
Summary:        Pure perl multi-level hash/array DBM that supports transactions
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBM-Deep/
Source0:        http://www.cpan.org/authors/id/R/RK/RKINYON/DBM-Deep-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-2.0010-no-prompt.patch
Patch1:         %{name}-%{version}-Module-Build-version.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008_004
BuildRequires:  perl-Digest-MD5 >= 1.00
BuildRequires:  perl-Fcntl >= 0.01
BuildRequires:  perl-File-Path >= 0.01
BuildRequires:  perl-File-Temp >= 0.01
BuildRequires:  perl-List-Util >= 1.14
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Pod-Parser >= 1.3
BuildRequires:  perl-Test-Deep >= 0.095
BuildRequires:  perl-Test-Exception >= 0.21
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Test-Warn >= 0.08
Requires:       perl-Digest-MD5 >= 1.00
Requires:       perl-Fcntl >= 0.01
Requires:       perl-List-Util >= 1.14
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
A unique flat-file database module, written in pure perl. True multi-level
hash/array support (unlike MLDBM, which is faked), hybrid OO / tie()
interface, cross-platform FTPable files, ACID transactions, and is quite
fast. Can handle millions of keys and unlimited levels without significant
slow-down. Written from the ground-up in pure perl -- this is NOT a
wrapper around a C-based DBM. Out-of-the-box compatibility with Unix, Mac
OS X and Windows.

%prep
%setup -q -n DBM-Deep-%{version}
%patch0 -p1
%patch1 -p1

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/DBM/Deep*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0011-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0011-1m)
- update to 2.0011
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0010-1m)
- update to 2.0010

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0009-2m)
- rebuild against perl-5.18.1

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0009-1m)
- update to 2.0009

* Mon May 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0008-6m)
- fix pod test

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0008-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0008-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0008-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0008-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0007-1m)
- update to 2.0007
- rebuild against perl-5.16.0

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0005-1m)
- update to 2.0005

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0004-7m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0004-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0004-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0004-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0004-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0004-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0004-1m)
- update to 2.0004

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0000-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0000-1m)
- update to 2.0000

* Mon Jun  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0025-1m)
- update to 1.0025

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0023-2m)
- rebuild against perl-5.12.1

* Mon May 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0023-1m)
- update to 1.0023

* Mon Apr 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0022-1m)
- update to 1.0022
- enable test

* Mon Apr 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0021-1m)
- update to 1.0021

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0020-2m)
- rebuild against perl-5.12.0

* Sun Apr 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0020-1m)
- update to 1.0020
- disable test for a while...

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0014-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0014-2m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0014-1m)
- update to 1.0014

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0013-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0013-1m)
- update to 1.0013

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0009-1m)
- update to 1.0009

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0006-2m)
- rebuild against gcc43

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0006-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
