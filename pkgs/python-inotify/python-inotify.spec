%global momorel 2

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(0)")}
%global with_python3 1

%define oname  pyinotify

Summary:       Monitor filesystem events with Python under Linux
Name:          python-inotify
Version:       0.9.4
Release:       %{momorel}m%{?dist}
License:       GPLv2+
Group:         Development/Libraries
URL:           http://trac.dbzteam.org/pyinotify
Source0:       http://seb.dbzteam.org/pub/pyinotify/releases/%{oname}-%{version}.tar.gz
NoSource:      0
Source1:       %{oname}
BuildRequires: python-devel >= 2.7
%if 0%{?with_python3}
BuildRequires: python3-devel >= 3.4
%endif
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a Python module for watching filesystems changes. pyinotify
can be used for various kind of fs monitoring. pyinotify relies on a
recent Linux Kernel feature (merged in kernel 2.6.13) called
inotify. inotify is an event-driven notifier, its notifications are
exported from kernel space to user space.

%package examples
Summary:  Examples for Python inotify module
Group:    Development/Libraries
Requires: python-inotify = %{version}-%{release}

%description examples
This package includes some examples usage of the Python inotify module,
extensive documentation is also included.

%if 0%{?with_python3}
%package -n python3-inotify
Summary:       Monitor filesystem events with Python under Linux
Group:         Development/Languages

%description -n python3-inotify
This is a Python 3 module for watching filesystems changes. pyinotify
can be used for various kind of fs monitoring. pyinotify relies on a
recent Linux Kernel feature (merged in kernel 2.6.13) called
inotify. inotify is an event-driven notifier, its notifications are
exported from kernel space to user space.

This is the Python 3 build of pyinotify
%endif # if with_python3

%prep
%setup -q -n %{oname}-%{version}

%if 0%{?with_python3}
%{__rm} -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build
%{__python} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif

%install
%{__rm} -rf %{buildroot}

# Install python 3 first, so that python 2 gets precedence:
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install -O1 --skip-build --root %{buildroot}
%{__install} -D -m 0755 -p %{SOURCE1} %{buildroot}%{_bindir}/python3-%{oname}
%{__sed} -i -e 's/^python /python3 /' %{buildroot}%{_bindir}/python3-%{oname}
%{__chmod} 0755 %{buildroot}%{python3_sitelib}/%{oname}.py
popd
%endif

%{__python} setup.py install -O1 --skip-build --root %{buildroot}
%{__install} -D -m 0755 -p %{SOURCE1} %{buildroot}%{_bindir}/%{oname}
%{__chmod} 0755 %{buildroot}%{python_sitelib}/%{oname}.py

# examples
%{__install} -d -m 0755 %{buildroot}%{_datadir}/%{oname}
%{__cp} -a python2/examples/* %{buildroot}%{_datadir}/%{oname}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc ACKS COPYING MANIFEST.in README.md
%{_bindir}/%{oname}
%{python_sitelib}/%{oname}*

%files examples
%defattr(-, root, root, -)
%{_datadir}/%{oname}

%if 0%{?with_python3}
%files -n python3-inotify
%defattr(-, root, root, -)
%doc ACKS COPYING MANIFEST.in README.md
%{_bindir}/python3-%{oname}
%{python3_sitelib}/%{oname}*
%{python3_sitelib}/__pycache__/pyinotify*
%endif

%changelog
* Mon Jan  6 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-2m)
- fix build on x86_64

* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-1m)
- update 0.9.4

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2-1m)
- update 0.9.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.6-1m)
- update to git snapshot (20090518) for glibc210

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-1m)
- import from Fedora to Momonga for git-cola

* Sat Feb  8 2009 Terje Rosten <terje.rosten@ntnu.no> - 0.8.1-1.git20090208
- 0.8.1

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.8.0-4.r
- Rebuild for Python 2.6

* Sun Jun 22 2008 Terje Rosten <terjeros@phys.ntnu.no> - 0.8.0-3.r
- rebuild 

* Tue Jun 17 2008 Terje Rosten <terjeros@phys.ntnu.no> - 0.8.0-2.r
- 0.8.0r
- add wrapper in /usr/bin

* Mon Jun 16 2008 Terje Rosten <terjeros@phys.ntnu.no> - 0.8.0-1.q
- 0.8.0q
- Update url, license and source url

* Sat Feb  9 2008 Terje Rosten <terjeros@phys.ntnu.no> - 0.7.1-2
- Rebuild

* Wed Aug 08 2007 Terje Rosten <terjeros@phys.ntnu.no> - 0.7.1-1
- New upstream release: 0.7.1
- Fix license tag

* Mon Jun 25 2007 Terje Rosten <terjeros@phys.ntnu.no> - 0.7.0-3
- Remove autopath from example package (bz #237464)

* Tue Mar 27 2007 Terje Rosten <terjeros@phys.ntnu.no> - 0.7.0-2
- Fix email address

* Tue Mar  6 2007 Terje Rosten <terjeros@phys.ntnu.no> - 0.7.0-1
- Initial build

