%global momorel 5

Summary: Run real-mode video BIOS code to alter hardware state
Name: vbetool
Version: 1.2.2
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://www.codon.org.uk/~mjg59/vbetool/
Group: System Environment/Base
Source0: http://www.codon.org.uk/~mjg59/vbetool/download/%{name}-%{version}.tar.bz2
# NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Requires: udev
BuildRequires: pciutils-devel
BuildRequires: zlib-devel
BuildRequires: libx86-devel >= 1.1-3m
BuildRequires: autoconf automake libtool pkgconfig
ExcludeArch: ppc ppc64

%description
vbetool uses lrmi in order to run code from the video BIOS. Currently, it is
able to alter DPMS states, save/restore video card state and attempt to
initialize the video card from scratch.

%prep
%setup -q

%build
autoreconf -v --install
%configure --with-x86emu
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

## can not boot NVIDIA VGA machines without timeout of udev
# install -m 0644 -D udev-video-post-example.rules \
#   %{buildroot}%{_sysconfdir}/udev/rules.d/92-video-post.rules

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING
# %%{_sysconfdir}/udev/rules.d/92-video-post.rules
%{_sbindir}/vbetool
%{_mandir}/man1/vbetool.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-3m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-2m)
- remove udev file to boot up machines with NVIDIA graphic cards
- GTX 275 GTX 285
- https://bugzilla.redhat.com/show_bug.cgi?id=583372

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-2m)
- rebuild against rpm-4.6

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-1m)
- initial package for Momonga Linux
- split from pm-utils
