%global momorel 9
%global qtdir %{_libdir}/qt3
%global srcname dbus-qt3

Summary: D-BUS message bus qt
Name: dbus-qt
Version: 0.70
Release: %{momorel}m%{?dist}
URL: http://www.freedesktop.org/Software/dbus
Source0: %{srcname}-%{version}.tar.bz2
License: "AFL/GPL"
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: qt3-devel
BuildRequires: dbus-devel >= 0.92

%description
D-BUS add-on library to integrate the standard D-BUS library with
the Qt thread abstraction and main loop.

%package devel
Summary: D-BUS message bus qt devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
D-BUS add-on library to integrate the standard D-BUS library with the
Qt thread abstraction and main loop. This contains the Qt specific
headers and libraries.

%prep
%setup -q -n %{srcname}-%{version}

%build
export QTDIR=%{qtdir}

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--enable-qt3 \
	--enable-new-ldflags \
	--disable-debug \
	--disable-warnings \
	--disable-rpath

%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=%{buildroot}

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING INSTALL
%{_libdir}/libdbus-qt-1.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/dbus-1.0/dbus/*.h
%{_libdir}/libdbus-qt-1.a
%{_libdir}/libdbus-qt-1.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.70-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.70-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.70-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.70-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.70-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.70-4m)
- rebuild against rpm-4.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.70-2m)
- rebuild against gcc43

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.70-1m)
- import dbus-qt3-0.70.tar.bz2 from cooker, dbus-1-qt3-0.2 is not detected by k3b

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.63-3m)
- add %%post and %%postun

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.63-2m)
- revise spec file to provide libdbus-1-qt3.so.* (DO NOT USE %%configure and %%makeinstall)

* Thu Aug 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.63-1m)
- initila build
