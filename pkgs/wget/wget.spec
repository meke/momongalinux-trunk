%global momorel 1

Summary: A utility for retrieving files using the HTTP or FTP protocols
Name: wget
Version: 1.15
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Internet
URL: http://www.gnu.org/software/wget/
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
Provides: webclient
Requires(post): info
Requires(preun): info
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: gettext
BuildRequires: pcre-devel >= 8.31
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
GNU Wget is a file retrieval utility which can use either the HTTP or
FTP protocols.  Wget features include the ability to work in the
background while you're logged out, recursive retrieval of
directories, file name wildcard matching, remote file timestamp
storage and comparison, use of Rest with FTP servers and Range with
HTTP servers to retrieve files over slow or unstable connections,
support for Proxy servers, and configurability.

Install wget if you need to retrieve large numbers of files with HTTP or
FTP, or if you need a utility for mirroring web sites or FTP directories.

%prep
%setup -q

%build
if pkg-config openssl ; then
    CPPFLAGS=`pkg-config --cflags openssl`; export CPPFLAGS
    LDFLAGS=`pkg-config --libs openssl`; export LDFLAGS
fi
%configure --with-ssl=openssl --enable-largefile --enable-opie --enable-digest --enable-ntlm --enable-nls --enable-ipv6 --disable-rpath
%make

%install
rm -rf %{buildroot}
%makeinstall

cp doc/ChangeLog ChangeLog.doc
cp src/ChangeLog ChangeLog.src

rm -f %{buildroot}%{_infodir}/dir

%post
/sbin/install-info %{_infodir}/wget.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/wget.info %{_infodir}/dir
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog* INSTALL MAILING-LIST NEWS README
%config /etc/wgetrc
%{_mandir}/man1/wget.*
%{_bindir}/wget
%{_infodir}/wget*
%{_datadir}/locale/*/LC_MESSAGES/*

%changelog
* Tue Jan 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-2m)
- rebuild against pcre-8.31

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.14-1m)
- update to 1.14

* Wed Feb 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.4-2m)
- use libssl only.
-- fix gnome-keyring WARNING

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.4-1m)
- update to 1.13.4

* Wed Aug 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.3-1m)
- update to 1.13.3

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13.1-1m)
- update to 1.13.1

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13-1m)
- update 1.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- [SECURITY] CVE-2009-3490
- update to 1.12

* Sat Aug 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11.4-4m)
- [SECURITY] CVE-2009-2408
- import upstream patch (Patch0) from hg wget/mainline, but a little bit modified
-- http://hg.addictivecode.org/wget/mainline/rev/2d8c76a23e7d
-- http://hg.addictivecode.org/wget/mainline/rev/f2d2ca32fd1b

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11.4-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11.4-2m)
- rebuild against rpm-4.6

* Sat Jul 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11.4-1m)
- update to 1.11.4

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.2-2m)
- rebuild against openssl-0.9.8h-1m

* Tue May  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.11.2-1m)
- update 1.11.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11.1-2m)
- rebuild against gcc43

* Wed Mar 26 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.11.1-1m)
- update to 1.11.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.11-2m)
- %%NoSource -> NoSource

* Tue Jan 29 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-1m)
- update to 1.11
- License: GPLv3
- URL: http://www.gnu.org/software/wget/
- modify %%doc

* Fri Feb  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-4m)
- [SECURITY] CVE-2006-6719, import security patch from Mandriva

* Tue Apr 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10.2-3m)
- add BuildRequires: gettext for use msgfmt

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10.2-2m)
- rebuild against openssl-0.9.8a

* Sun Nov 27 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.10.2-1m)
- version up.

* Mon Aug  8 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10-2m)
- add a file 'PATCHES'

* Wed Jun 15 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.10-1m)
- up to 1.10
- [SECURITY] CAN-2004-1487, CAN-2004-1488

* Thu Nov 11 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.9.2-0.20041110.3m)
- updated PF independent patch

* Wed Nov 10 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.9.2-0.20041110.2m)
- added wget-cvs-20041110-use_xfree_null.patch

* Wed Nov 10 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.9.2-0.20041110.1m)
- update to cvs 20041110
- added protocol family independent patch (just for test)

* Sat May 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.1-1m)
- update to 1.9.1
- delete Patch12

* Wed Oct 29 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-1m)
- update to 1.9
- comment out patch4
- comment out Patch10 since included into tarball
- comment out patch11
- comment out patch13

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.8.2-8m)
- revise %%post and %%preun

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.8.2-7m)
  rebuild against openssl 0.9.7a

* Tue Mar  4 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.8.2-6m)
- corrected .netrc parsing bug (wget-1.8.2-netrc.patch)
  http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=152990
- added largefile support patch from Eduard Bloch
  (wget-1.8.2-largefile.patch)
  http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=137989

* Wed Dec 18 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.8.2-5m)
- [security] add wget-1.8.2-filename.patch to fix
  directory traversal vulnerability.
  https://rhn.redhat.com/errata/RHSA-2002-229.html
- add wget-1.8.2-ht.patch not to make wget segfault
  when URLs are specified more than twice.
  http://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=67484

* Thu Jun 13 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.8.2-4k)
- update ipv6 patch (Patch4)

* Wed Jun 12 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (1.8.2-2k)
- update to 1.8.2.

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.8.1-6k)
- Prereq: /sbin/install-info -> info

* Mon Apr  15 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.8.1-4k)
  revise ipv6 patch
  (this patch was written by Hiroyuki YAMAMORI <h-yamamo@db3.so-net.ne.jp>)
  add URL tag
  remove strip

* Thu Apr  4 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.8.1-2k)
  update to 1.8.1

* Tue Nov 21 2001 TABUCHI Takaaki <tab@kondara.org>
- (1.7.1-2k)
- update to 1.7.1
- delete man patch
- delete %{eversion}

* Thu Jun  7 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (1.7-2k)
  update to 1.7

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.6-5k)
- changable IPv6 function with %{_ipv6} macro

* Tue Jan  2 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.6

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Fri Oct 13 2000 Motonobu Ichimura <famao@kondara.org>
- up to wget-153-20000915.diff.gz (http://www.kame.net)
- updated wget-1.5.3-symlink.patch -> wget-1.5.3-symlink2.patch

* Mon Oct 09 2000 Motonobu Ichimura <famao@kondara.org>
- fixed ja.po ( for converting from EUC-JP to UTF-8 )

* Sat Jul 29 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5.3-9k)
- restruct version against forked

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Jun 01 2000 KUSUNOKI Masanori <nori@kondara.org>
- Enable IPv6.

* Sun Apr 02 2000 TABUCHI Takaaki <tab@kondara.org>
- merge redhat-6.2 (1.5.3-6).

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
+- handle compressed man pages

* Wed Jan 26 2000 Norihito Ohmori <nono@vinelinux.org>
- fix ja.po

* Thu Jan 20 2000 HIROSE, Masaaki <hirose31@t3.rim.or.jp>
- added getmore.patch. to get files like LINK HREF="XXX".

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Sep 18 1999 Norihito Ohmori <ohmori@flatout.org>
- add ja.po
- add Japanese Summary and Description

* Thu Aug 26 1999 Jeff Johnson <jbj@redhat.com>
- don't permit chmod 777 on symlinks (#4725).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 4)

* Fri Dec 18 1998 Bill Nottingham <notting@redhat.com>
- build for 6.0 tree
- add Provides

* Sat Oct 10 1998 Cristian Gafton <gafton@redhat.com>
- strip binaries
- version 1.5.3

* Sat Jun 27 1998 Jeff Johnson <jbj@redhat.com>
- updated to 1.5.2

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- modified group to Applications/Networking

* Wed Apr 22 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.5.0
- they removed the man page from the distribution (Duh!) and I added it back
  from 1.4.5. Hey, removing the man page is DUMB!

* Fri Nov 14 1997 Cristian Gafton <gafton@redhat.com>
- first build against glibc
