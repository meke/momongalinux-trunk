%global momorel 14

Summary: A graphics library for drawing .gif files
Name: gd
Version: 2.0.35
Release: %{momorel}m%{?dist}
License: BSD
URL: http://www.libgd.org/Main_Page
Group: System Environment/Libraries
Source: http://www.libgd.org/releases/%{name}-%{version}.tar.bz2
Patch0: gd-2.0.35-freetype.patch
Patch1: gd-2.0.35-dos-dirty-fix.patch
Patch3: gd-2.0.35-multilib.patch
Patch5: gd-2.0.34-sparc64.patch
Patch6: gd-2.0.35-overflow.patch
Patch7: gd-2.0.35-AALineThick.patch
Patch8: gd-2.0.33-BoxBound.patch
Patch9: gd-2.0.35-CVE-2009-3546.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: freetype-devel, fontconfig-devel >= 2.2.3-14m
BuildRequires: libX11-devel, libXpm-devel
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: libjpeg-devel >= 8a, libpng-devel >= 1.2.2
BuildRequires: expat-devel >= 2.0.0-2m
Requires: libjpeg, freetype, libpng
Requires: zlib >= 1.1.4-5m

%description
Gd is a graphics library for drawing .gif files.	Gd allows your code to
quickly draw images (lines, arcs, text, multiple colors, cutting and
pasting from other images, flood fills) and write out the result as a
.gif file. Gd is particularly useful in web applications, where .gifs
are commonly used as inline images.	 Note, however, that gd is not a
paint program.
Install gd if you are developing applications which need to draw .gif
files.	If you install gd, you'll also need to install the gd-devel
package.

%package devel
Requires: gd = %{version}-%{release}
Summary: The development libraries and header files for gd.
Group: Development/Libraries

%description devel
These are the development libraries and header files for gd, the .gif
graphics library.
If you're installing the gd graphics library, you must install gd-devel.

%prep
mkdir -p %{buildroot}%{_prefix}/bin
mkdir -p %{buildroot}%{_prefix}/lib
mkdir -p %{buildroot}%{_prefix}/include

%setup -q
%patch0 -p1 -b .freetype
%patch1 -p1 -b .dos
%patch3 -p1 -b .mlib
%patch6 -p1 -b .overflow
#%%patch5 -p1 -b .sparc64
%patch7 -p1 -b .AALineThick
%patch8 -p1 -b .bb
%patch9 -p0 -b .CVE-2009-3546

%build
autoconf
# ugly hack!!
touch aclocal.m4
touch Makefile.in
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall transform='s,x,x,'
find %{buildroot} -name "*.la" -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README* COPYING
%doc index.html readme.jpn
%doc gddemo *.png webpng.c pngtogd.c gddemo.c gdtest.c test
%{_bindir}/pngtogd2
%{_bindir}/pngtogd2
%{_bindir}/annotate
%{_bindir}/gdtopng
%{_bindir}/gd2topng
%{_bindir}/gdparttopng
%{_bindir}/gd2copypal
%{_bindir}/webpng
%{_bindir}/bdftogd
%{_bindir}/pngtogd
%{_bindir}/gd2togif
%{_bindir}/gdcmpgif
%{_bindir}/giftogd2
%{_libdir}/libgd.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/gdlib-config
%{_libdir}/libgd.so
%{_libdir}/libgd.*a
%{_includedir}/*.h
%{_libdir}/pkgconfig/gdlib.pc

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.35-14m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.35-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.35-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.35-11m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.35-10m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.35-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.35-8m)
- [SECURITY] CVE-2009-3546
- import a security patch from Mandriva (2.0.35-8.1mdv2009.1)

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.35-7m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.35-6m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.35-5m)
- rebuild against db4-4.7.25-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.35-4m)
- rebuild against gcc43

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.35-3m)
- rebuild against db4-4.6.21

* Wed Sep 26 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.0.35-2m)
- tmpfs build fix

* Thu Jun 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.35-1m)
- [SECURITY] http://www.libgd.org/ReleaseNote020035

* Sat Jun  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.34-2m)
- [SECURITY] CVE-2007-2756
- add Patch9 gd-2.0.34-CVE-2007-2756.patch

* Sat Jun  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.34-1m)
- [SECURITY] CVE-2007-0455
- update to 2.0.34
- add some patches from FC-devel

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.33-9m)
- Requires: freetype2 -> freetype

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.33-8m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.33-7m)
- rebuild against expat-2.0.0-1m

* Wed Jul  5 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.33-6m)
- [SECURITY] allow remote attack to cause a DoS via malformed GIF data.CVE-2006-2906 
- import patch from debian

* Wed Jun 14 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.0.33-5m)
- import patches from FC.

* Wed Jun 14 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.0.33-4m)
- fix DoS security issue. CVE-2006-2906
- http://www.securiteam.com/unixfocus/5SP0120IUU.html
- enable CAN patch. it's disabled for reasons, please disable if there
- are no security problem(s) without it.

* Fri Feb 11 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.33-3m)
- modify dependency freetype -> freetype2

* Tue Nov 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.0.33-2m)
- security fix.
- [CAN-2004-0941]
		http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0941
	[Secunia Security Advisory: SA13179]
		http://secunia.com/advisories/13179/

* Mon Nov	 8 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.0.33-1m)
- version 2.0.33
	http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0990

* Fri Oct 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.28-1m)
- version 2.0.28

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.11-5m)
- revised spec for enabling rpm 4.2.

* Sat Oct 11 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (2.0.11-4m)
- adapt the License: preamble for the Momonga Linux license
	expression unification policy (draft)

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.11-3m)
- rebuild against zlib 1.1.4-5m

* Wed Feb 26 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.11-2m)
- change macro inst_prefix to _prefix and delete inst_prefix
- aggregate BuildPreReq: libpng and libpng-devel
- arrange Requires

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.11-1m)
- version 2.0.11-1m

* Sat Nov	 9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.8.4-9m)
- rebuild against gcc-3.2 with autoconf-2.53

* Fri May 31 2002 Toru Hoshina <t@kondara.org>
- (1.8.4-8k)
	gd-Makefile: /usr/local ....

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.8.4-6k)
- rebuild against libpng 1.2.2.

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (1.8.4-4k)
- rebuild against libpng 1.2.0.

* Sun Mar 25 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.4-3k)
- up to 1.8.4
- now gd supports freetype2
- remove strip section (now rpm does)
- changes Source's URL

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Mon Jul 03 2000 Motonobu Ichimura <famao@kondara.org>
- remove xpm-devel from BuildPrereq
- add libjpeg-devel to BuildPrereq
- up to 1.8.3

* Sat Feb 19 2000 Motonobu Ichimura <famao@kondara.org>
- fix freetype-ken :-P

* Sun Jan 16 2000 Motonobu Ichimura <famao@kondara.org>
- up to 1.7.3 :-)

* Sun Jan 02 2000 Motonobu Ichimura <famao@kondara.org>
- strip binaries

* Fri Nov 26 1999 Motonobu Ichimura <famao@kondara.org>
- added %defattr

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Oct 17 1999 Norihito Ohmori <nono@kondara.org>
- unuse %{ver}

* Sat Oct 10 1999 Norihito Ohmori <nono@kondara.org>
- mv libgd.so to -devel package

* Mon Jun 7 1999 Tomohiko Yoshida <kitcho@tk.xaxon.ne.jp>
- Release 4.1
- VFlib -> VFlib2

* Wed Jan 27 1999 Satoshi Ishikawa <tigre@cc.gifu-u.ac.jp>
- Release 4
- /usr/local -> /usr
- small fix in SPEC

* Sat Nov 14 1998 Masahito Yamaga <yamaga@ipc.chiba-u.ac.jp>
- Version 1.3
- Release 3
- update kanji patch	(change to read ENVIRONMENT GD_KANJIFONT)
- when making shared library, VFlib makes dynamic link,
	so -lgd automatically link	VFlib.

* Mon May 26 1998 Satoshi Ishikawa <tigre@cc.gifu-u.ac.jp>
- Version 1.3
- Release 2
- update kanji patch

* Tue Mar 24 1998 Satoshi Ishikawa <tigre@cc.gifu-u.ac.jp>
- Version 1.3
- Release 1
- GD.kanji.patch-JRPM.gz
	(based on GD.kanji.patch.gz -- Mitsuhiro Maeda <mitsu@tramp.co.jp>)
- removed script for /etc/ld.so.conf

* Wed Nov 19 1997 Satoshi Ishikawa <tigre@cc.gifu-u.ac.jp>
- 2nd Release.
- added Distribution: and Vendor: tags
- added script for /etc/ld.so.conf at %post
- changed Group: to Extensions/Japanese

* Mon Jul 24 1997 Satoshi Ishikawa <tigre@cc.gifu-u.ac.jp>
- 1st Release.


