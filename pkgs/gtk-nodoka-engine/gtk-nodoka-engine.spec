%global momorel 5

%define gtk2version %(pkg-config gtk+-2.0 --modversion 2> /dev/null)
%define nogtk2 %(pkg-config gtk+-2.0 --modversion &> /dev/null; echo $?)

Name:           gtk-nodoka-engine
Version:        0.7.2
Release:        %{momorel}m%{?dist}
Summary:        The Nodoka GTK Theme Engine

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://fedorahosted.org/nodoka
Source0:        https://fedorahosted.org/releases/n/o/gtk-nodoka-engine-%{version}.tar.gz
Patch0:         %{name}-scale-trough.patch
Patch1:         %{name}-handle-selection.patch
Patch2:         %{name}-0.7.2-missing-widget-check.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel
%if 0%{?nogtk2}
Requires:       gtk2
%else
Requires:       gtk2 >= %{?gtk2version}
%endif
Requires:       nodoka-filesystem

%description
Nodoka is a Murrine engine based gtk2 theme engine. The package is shipped with 
a default Nodoka theme featuring the engine.

%package extras
Summary:  Extra themes for Nodoka gtk2 theme engine
Group:    System Environment/Libraries
Requires: %{name} >= 0.6.90.1

%description extras
This package contains extra themes fot the Nodoka gtk2 theme engine.


%prep
%setup -q
%patch0 -p1 -b .scale-trough
%patch1 -p1 -b .handle-selection
%patch2 -p1 -b .missing-widget-check

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

#remove .la files
find $RPM_BUILD_ROOT -name *.la | xargs rm -f || true


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING CREDITS NEWS README TODO
%{_libdir}/gtk-2.0/2.10.0/engines/libnodoka.so
%{_datadir}/themes/Nodoka/*

%files extras
%defattr(-,root,root,-)
%doc COPYING
%{_datadir}/themes/Nodoka-*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-1m)
- sync with Fedora 11 (0.7.2-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against rpm-4.6

* Sun May 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.0-1m)
- import from Fedora to Momonga

* Mon Apr 14 2008 Martin Sourada <martin.sourada@gmail.com> - 0.7.0-1
- Update to stable

* Thu Apr 03 2008 Martin Sourada <martin.sourada@gmail.com> - 0.7.0-0.4.gitab3ed15
- Update to latest git

* Wed Apr 02 2008 Martin Sourada <martin.sourada@gmail.com> - 0.7.0-0.3.git98ce81e
- Update to latest git

* Sat Mar 08 2008 Martin Sourada <martin.sourada@gmail.com> - 0.6.99.1-1
- 0.7 RC1

* Fri Feb 08 2008 Martin Sourada <martin.sourada@gmail.com> - 0.6.90.2-2
- Rebuild for gcc 4.3
- Use full source path

* Sat Jan 26 2008 Martin Sourada <martin.sourada@seznam.cz> - 0.6.90.2-1
- Update to 0.7. beta 2 release
 - mostly bug fixes

* Sat Jan 05 2008 Martin Sourada <martin.sourada@seznam.cz> - 0.6.90.1-1
- Update to 0.7 beta 1 release
 - Extra themes add to -extras subpackage

* Mon Oct 29 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.6-5
- fix gimp crashing in some dialogs when using Small theme
 - rhbz #291121 and rhbz #355931

* Mon Sep 24 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.6-4
- update the treeview patch
 - rhbz #297271 and rhbz #302551

* Sun Sep 23 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.6-3
- Require at least the version of gtk it was build against
 - rhbz #301851 (patch from Ignacio Vazquez-Abrams)

* Thu Sep 20 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.6-2
- Add user specified tooltips coloring
- Fix colours in unfocused GTKTreeView (rhbz #297271)

* Sat Aug 25 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.6-1
- new version
  -- first stable
  -- fixes checkbutton firefox positioning issue

* Sun Aug 19 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.90-1
- new version

* Thu Aug 09 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.3-2
- update License: field to GPLv2

* Sun Aug 05 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.3-1
- new version

* Sat Aug 04 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5.2-1
- new version

* Fri Jul 27 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.5-2
- add Requires: gtk2 for engines directory ownership
- remove --enable-animation
- remove Obsoletes/Provides, not needed now

* Wed Jul 25 2007 Daniel Geiger <dgeiger_343@yahoo.com> - 0.5-1
- Numerous Nodoka specific themeing adjustments (so a large version number jump), including:
 - Adjustment of arrow styling
 - Frames and notebooks now use roundness
 - Adjustment of scrollbar handle styling
 - Scale sliders now use same handle styling as scrollbars
 - Adjustment of resize handle styling
 - Menubaritems are now tab-like, use roundness; menuitems are squared
 - Changing of gtkrc engine configuration options to more fit the Nodoka style (see README), plus general code tweaks

* Mon Jul 16 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.2.1-1
- new upstream version
- fix scrollbar coloring

* Mon Jul 16 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.2-1
- new upstream version
- new, reworked function for setting cairo gradients
- add shadows to buttons and editboxes
- rework radiobutton and checkbutton

* Fri Jul 13 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.1.1-1
- split metacity and metatheme into separate package in upstream

* Fri Jul 13 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.1-1
- merge with Nodoka Theme both in upstream and in rpm
- patches included in upstream

* Thu Jul 12 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.1-2
- Some patches to make it look more like we want

* Thu Jul 12 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.1-1
- Initial release
