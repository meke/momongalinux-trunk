%global momorel 1

Summary: X.Org X11 libXt runtime library
Name: libXt
Version: 1.1.3
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: xorg-x11-proto-devel
BuildRequires: libX11-devel
BuildRequires: libSM-devel
BuildRequires: libxcb-devel

%description
X.Org X11 libXt runtime library

%package devel
Summary: X.Org X11 libXt development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3
Requires: libX11-devel
Requires: libSM-devel

%description devel
X.Org X11 libXt development package

%prep
%setup -q

# Disable static library creation by default.
%define with_static 0

%build
# FIXME: Work around pointer aliasing warnings from compiler for now
# EventUtil.c:105: warning: dereferencing type-punned pointer will break strict-aliasing rules
# NextEvent.c:1122: warning: dereferencing type-punned pointer will break strict-aliasing rules
# NextEvent.c:1132: warning: dereferencing type-punned pointer will break strict-aliasing rules
# NextEvent.c:1142: warning: dereferencing type-punned pointer will break strict-aliasing rules
# NextEvent.c:1156: warning: dereferencing type-punned pointer will break strict-aliasing rules
# NextEvent.c:1299: warning: dereferencing type-punned pointer will break strict-aliasing rules
# NextEvent.c:1315: warning: dereferencing type-punned pointer will break strict-aliasing rules
# NextEvent.c:1340: warning: dereferencing type-punned pointer will break strict-aliasing rules
# NextEvent.c:1572: warning: dereferencing type-punned pointer will break strict-aliasing rules
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
%configure \
%if ! %{with_static}
	--disable-static \
%endif
	--with-xfile-search-path="%{_sysconfdir}/X11/%%L/%%T/%%N%%C%%S:%{_sysconfdir}/X11/%%l/%%T/\%%N%%C%%S:%{_sysconfdir}/X11/%%T/%%N%%C%%S:%{_sysconfdir}/X11/%%L/%%T/%%N%%S:%{_sysconfdir}/X\11/%%l/%%T/%%N%%S:%{_sysconfdir}/X11/%%T/%%N%%S:%{_datadir}/X11/%%L/%%T/%%N%%C%%S:%{_datadir}/X1\1/%%l/%%T/%%N%%C%%S:%{_datadir}/X11/%%T/%%N%%C%%S:%{_datadir}/X11/%%L/%%T/%%N%%S:%{_datadir}/X11/%%\l/%%T/%%N%%S:%{_datadir}/X11/%%T/%%N%%S"

%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# We intentionally don't ship *.la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README INSTALL ChangeLog
%{_libdir}/libXt.so.6
%{_libdir}/libXt.so.6.0.0

%files devel
%defattr(-,root,root,-)
# Listed explicitly instead of with glob, in order for rpm to autodetect
# when any additions or removals happen.  ie: New Xprint support that we
# do not want.
%{_includedir}/X11/CallbackI.h
%{_includedir}/X11/Composite.h
%{_includedir}/X11/CompositeP.h
%{_includedir}/X11/ConstrainP.h
%{_includedir}/X11/Constraint.h
%{_includedir}/X11/ConvertI.h
%{_includedir}/X11/Core.h
%{_includedir}/X11/CoreP.h
%{_includedir}/X11/CreateI.h
%{_includedir}/X11/EventI.h
%{_includedir}/X11/HookObjI.h
%{_includedir}/X11/InitialI.h
%{_includedir}/X11/Intrinsic.h
%{_includedir}/X11/IntrinsicI.h
%{_includedir}/X11/IntrinsicP.h
%{_includedir}/X11/Object.h
%{_includedir}/X11/ObjectP.h
%{_includedir}/X11/PassivGraI.h
%{_includedir}/X11/RectObj.h
%{_includedir}/X11/RectObjP.h
%{_includedir}/X11/ResConfigP.h
%{_includedir}/X11/ResourceI.h
%{_includedir}/X11/SelectionI.h
%{_includedir}/X11/Shell.h
%{_includedir}/X11/ShellI.h
%{_includedir}/X11/ShellP.h
%{_includedir}/X11/StringDefs.h
%{_includedir}/X11/ThreadsI.h
%{_includedir}/X11/TranslateI.h
%{_includedir}/X11/VarargsI.h
%{_includedir}/X11/Vendor.h
%{_includedir}/X11/VendorP.h
%{_includedir}/X11/Xtos.h
%if %{with_static}
%{_libdir}/libXt.a
%endif
%{_libdir}/libXt.so
%{_libdir}/pkgconfig/xt.pc
%{_mandir}/man3/*.3*

%doc %{_datadir}/doc/%{name}

%changelog
* Fri Mar 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-1m)
- update to 1.1.3

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update to 1.1.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-2m)
- full rebuild for mo7 release

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-1m)
- update 1.0.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-1m)
- update 1.0.7

* Sat Jul 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-2m)
- %%NoSource -> NoSource

* Mon Oct  1 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildRequires to libxcb-devel to avoid configure error:
  Package requirements (sm x11 xproto kbproto) were not met.

* Sun Jan 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-1m)
- update 1.0.5

* Fri Nov 10 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-1m)
- update 1.0.4

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Sat May 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-1m)
- update 1.0.1

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-3m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-2.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan 23 2006 Mike A. Harris <mharris@redhat.com> 1.0.0-2
- Bumped and rebuilt

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated libXt to version 1.0.0 from X11R7 RC4
- Added makestrs and it's manpage to the devel subpackage.

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Updated libXt to version 0.99.3 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Dec  2 2005 Kristian Hogsberg <krh@redhat.com> 0.99.2-3
- Use the default value from configure.ac for --with-xfile-search-path
  except with %%{_datadir} instead of $(libdir), so Xt can search for
  app-default files as usual.
- Move the --with-xfile-search-path outside the with_static condition.

* Tue Nov 29 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Invoke ./configure --with-xfile-search-path=%%{_datadir}/X11/app-defaults
  to make sure Xt is looking in the right place for app-defaults files.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated libXt to version 0.99.2 from X11R7 RC2
- Changed 'Conflicts: XFree86-devel, xorg-x11-devel' to 'Obsoletes'
- Changed 'Conflicts: XFree86-libs, xorg-x11-libs' to 'Obsoletes'

* Wed Nov 2 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-3
- Actually spell RPM_OPT_FLAGS correctly this time.

* Mon Oct 31 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Build with -fno-strict-aliasing to work around possible pointer aliasing
  issue

* Mon Oct 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated libXt to version 0.99.1 from X11R7 RC1
- Updated file manifest to find manpages in 'man3x'

* Thu Oct 6 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-5
- Added Requires: libX11-devel to libXt-devel subpackage, as Xt headers
  include Xlib headers causing xterm and other things to fail to compile.

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-4
- Renamed package to remove xorg-x11 from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro
- Fix BuildRequires to use new style X library package names

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Changed all virtual BuildRequires to the "xorg-x11-" prefixed non-virtual
  package names, as we want xorg-x11 libs to explicitly build against
  X.Org supplied libs, rather than "any implementation", which is what the
  virtual provides is intended for.

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
