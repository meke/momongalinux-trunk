%global momorel 11

Summary: GNU Triangulated Surface Library
Name: gts
Version: 0.7.6
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://gts.sourceforge.net/
Group: Applications/Engineering
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel
BuildRequires: netpbm-devel

%description
GTS provides a set of useful functions to deal with 3D surfaces meshed
with interconnected triangles including collision detection,
multiresolution models, constrained Delaunay triangulations and robust
set operations (union, intersection, differences).

%package devel
Summary: Header files and static libraries from gts
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on GTS.

%prep
%setup -q

%build
autoreconf -fiv
export CFLAGS="-I/usr/include/netpbm"
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README THANKS TODO
%{_bindir}/delaunay
%{_bindir}/gts2dxf
%{_bindir}/gts2oogl
%{_bindir}/gts2stl
%{_bindir}/gtscheck
%{_bindir}/gtscompare
%{_bindir}/gtstemplate
%{_bindir}/happrox
%{_bindir}/stl2gts
%{_bindir}/transform
%{_libdir}/libgts-0.7.so.*

%files devel
%defattr(-,root,root)
%doc doc/html
%{_bindir}/gts-config
%{_includedir}/gts*.h
%{_libdir}/pkgconfig/gts.pc
%{_libdir}/libgts.a
%{_libdir}/libgts.so
%{_datadir}/aclocal/gts.m4

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.6-11m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.6-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.6-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.6-8m)
- full rebuild for mo7 release

* Sat Aug 14 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.6-7m)
- fix build failure with netpbm >= 10.47.17-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.7.6-5m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.6-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.6-2m)
- %%NoSource -> NoSource

* Wed Mar  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.6-1m)
- initial package for k3d-0.6.6.0
- Summary and %%description are imported from Fedora
