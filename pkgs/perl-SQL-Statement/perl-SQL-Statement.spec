%global         momorel 4

Name:           perl-SQL-Statement
Version:        1.405
Release:        %{momorel}m%{?dist}
Summary:        SQL parsing and processing engine
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/SQL-Statement/
Source0:        http://www.cpan.org/authors/id/R/RE/REHSACK/SQL-Statement-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008
BuildRequires:  perl-Clone >= 0.30
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-DBD-AnyData >= 0.110
BuildRequires:  perl-DBD-CSV >= 0.30
BuildRequires:  perl-DBD-SQLite
BuildRequires:  perl-DBI >= 1.616
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MLDBM
BuildRequires:  perl-Params-Util >= 1.00
BuildRequires:  perl-Scalar-Util >= 1.0
BuildRequires:  perl-Test-Simple >= 0.90
BuildRequires:  perl-Text-Soundex
Requires:       perl-Clone >= 0.30
Requires:       perl-Data-Dumper
Requires:       perl-Params-Util >= 1.00
Requires:       perl-Scalar-Util >= 1.0
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The SQL::Statement module implements a pure Perl SQL parsing and execution
engine. While it by no means implements full ANSI standard, it does support
many features including column and table aliases, built-in and user-defined
functions, implicit and explicit joins, complex nested search conditions,
and other features.

%prep
%setup -q -n SQL-Statement-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/SQL/*
%{_mandir}/man3/*.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.405-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.405-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.405-2m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.405-1m)
- update to 1.405

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.404-1m)
- update to 1.404
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.402-2m)
- rebuild against perl-5.16.3

* Sat Dec 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.402-1m)
- update to 1.402

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.401-2m)
- rebuild against perl-5.16.2

* Wed Oct 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.401-1m)
- update to 1.401

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-8m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-7m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33-3m)
- rebuild for new GCC 4.6

* Sun Feb 27 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33-2m)
- add BuildRequires

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.31-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.27-3m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-2m)
- version down to 1.27
- build with DBI <= 1.611
- DBD::CSV could not be built with 1.28

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-2m)
- rebuild against perl-5.12.1

* Fri May  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-3m)
- rebuild against perl-5.12.0

* Sun Apr 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.26-2m)
- add "env SQL_STATEMENT_WARN_UPDATE=sure" at perl Makefile.PL for force update

* Sun Apr 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26
 
* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-liux.org>
- (1.23-1m)
- update to 1.23

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Tue Sep 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-0.4.1m)
- update to 1.21_4

* Sat Sep 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-0.3.1m)
- update to 1.21_3

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-3m)
- rebuild against perl-5.10.1

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-2m)
- specify perl-DBI version

* Sun Mar  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.15-2m)
- rebuild against gcc43

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
