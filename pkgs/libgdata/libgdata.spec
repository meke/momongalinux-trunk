%global		momorel 1
Name:           libgdata
Version:        0.13.2
Release: %{momorel}m%{?dist}
Summary:        Library for the GData protocol

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://live.gnome.org/libgdata
Source0:        http://download.gnome.org/sources/%{name}/0.13/%{name}-%{version}.tar.xz
NoSource: 	0

BuildRequires:  glib2-devel libsoup-devel libxml2-devel gtk-doc intltool
BuildRequires:  libgnome-keyring-devel
BuildRequires:  gobject-introspection-devel liboauth-devel
Requires:       gobject-introspection

%description
libgdata is a GLib-based library for accessing online service APIs using the
GData protocol --- most notably, Google's services. It provides APIs to access
the common Google services, and has full asynchronous support.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires:       gtk-doc
Requires:       gobject-introspection-devel

# https://bugzilla.gnome.org/show_bug.cgi?id=610273
BuildRequires:  libtool

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

%find_lang gdata


%check
# Only the general test can be run without network access
# Actually, the general test doesn't work either without gconf
#cd gdata/tests
#./general

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f gdata.lang
%doc COPYING NEWS README AUTHORS
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/GData-0.0.typelib

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc
%{_datadir}/gtk-doc/html/gdata/
%{_datadir}/gir-1.0/GData-0.0.gir

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.2-1m)
- update 0.13.2

* Sat Aug  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.1-1m)
- update 0.13.1

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.0-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.2-2m)
- rebuild for glib 2.33.2

* Sat Jun  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-1m)
- [SECURITY] CVE-2012-2653
- update to 0.10.2

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-2m)
- rebuild for new GCC 4.6

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-2m)
- full rebuild for mo7 release

* Mon Apr 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Mon Feb 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-3m)
- build fix libtool-2.2.6b

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- initial build
