%global momorel 1

%global xfce4ver 4.10.0
%global thunarver 1.4.0

Summary:        Simple CD burning tool for Xfce
Name:           xfburn
Version:        0.5.2
Release:        %{momorel}m%{?dist}

Group:          Applications/Archiving
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/applications/%{name}
Source0:        http://archive.xfce.org/src/apps/xfburn/0.5/xfburn-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  Thunar-devel >= %{thunarver}
BuildRequires:  libburn-devel >= 0.4.2
BuildRequires:  libisofs-devel >= 0.6.2
BuildRequires:  dbus-glib-devel >= 0.34
BuildRequires:  gstreamer-devel >= 0.10.2
BuildRequires:  gstreamer-plugins-base-devel
BuildRequires:  gtk2-devel >= 2.10.0
BuildRequires:  gettext desktop-file-utils intltool
BuildRequires:  thunar-vfs-devel >= 1.2.0-2m
Requires:       hicolor-icon-theme

%description
Xfburn is a simple CD/DVD burning tool based on libburnia libraries. It can 
blank CD-RWs, burn and create iso images, as well as burn personal 
compositions of data to either CD or DVD.


%prep
%setup -q

%build
%configure --disable-hal
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
%find_lang %{name}

desktop-file-install --vendor ""                                \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
        --delete-original                                       \
        --add-mime-type=application/x-cd-image                  \
        --add-only-show-in XFCE                                 \
        ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop


%post
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%postun
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/Thunar/sendto/*.desktop
%{_datadir}/icons/hicolor/*/stock/media/stock_%{name}*.png
%{_datadir}/icons/hicolor/scalable/stock/media/stock_%{name}*.svg
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/*.ui
%{_mandir}/man1/%{name}.*.*


%changelog
* Sun Apr 20  2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.3-6m)
- rebuild against libxfce4util-4.10.0-1m

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-5m)
- build fix

* Mon Feb 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-4m)
- this package requires thunar-vfs without hal

* Sun Feb 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.3-3m)
- rebuild against HAL removed env

* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.3-2m)
- remove BR hal

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-2m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-6m)
- rebuild against xfce4 4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.1-3m)
- define __libtoolize (build fix)

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-2m)
- revise desktop file, add OnlyShowIn=XFCE; to xfburn.desktop
  because xfburn.desktop has no icon, KDE needs icon

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-1m)
- import from Fedora to Momonga

* Wed Feb 25 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.1-1
- Update to 0.4.1
- Include new manpage

* Mon Jan 26 2009 Denis Leroy <denis@poolshark.org> - 0.4.0-1
- Update to upstream 0.4.0

* Tue Nov 04 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.91-2
- Don't enable debug
- Require hicolor-icon-theme

* Tue Nov 04 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.91-1
- Update to 0.3.91

* Tue Jul 16 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.2-1
- Update to 0.3.2

* Fri Jul 11 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.1-1
- Update to 0.3.1

* Wed Jun 11 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0-1
- Update to 0.3.0 stable

* Mon Jan 07 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0-0.1.20080107svn26552
- Update to 0.3.0svn-26552.
. Switch to libburn and drop requirements for genisoimage, cdrdao and wodim

* Sun Feb 25 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-0.1.20070225svn25032
- Initial package.
