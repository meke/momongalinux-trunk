%global momorel 17

Summary: galago library for .Net
Name: galago-gtk-sharp
Version: 0.5.0
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/ 
Source0: http://www.galago-project.org/files/releases/source/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: http://www.galago-project.org/files/releases/source/libgalago-gtk/libgalago-gtk-0.5.0.tar.gz 
NoSource: 1
Patch0: galago-gtk-sharp-0.5.0-libgalago.patch
Patch1: galago-gtk-sharp-0.5.0-nunit.patch
Patch2: galago-gtk-sharp-0.5.0-pkgconfig.patch
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mono-devel >= 2.8
BuildRequires: gtk-sharp2-devel >= 2.12.10-4m
BuildRequires: libgalago-devel >= 0.5.1
BuildRequires: galago-sharp-devel >= 0.5.0-12m
BuildRequires: libgalago-gtk-devel
BuildRequires: gnome-sharp-devel

%description
Galago-Gtk# for Mono v0.1.0

%package devel
Summary:	Files for development using %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
This package contains pkg-config file for development of programs
using %{name}.

%prep
%setup -q
%patch0 -p1 -b .libgalago
%patch1 -p1 -b .nunit
%patch2 -p1 -b .pkgconfig

cp %{SOURCE1} sources

%build
autoreconf -vfi
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%if %_lib != lib
mkdir -p  %buildroot%_prefix/lib/
mv %buildroot%_libdir/mono %buildroot%_prefix/lib/
#perl -pi -e "s^%_libdir^%_prefix/lib^" %buildroot%_libdir/pkgconfig/galago-sharp.pc
%endif

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_prefix}/lib/mono/gac/%{name}
%{_prefix}/lib/mono/%{name}
%{_datadir}/gapi-2.0/galago-gtk-api.xml

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-17m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-15m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-14m)
- rebuild against mono-2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-13m)
- full rebuild for mo7 release

* Sun Jun 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-12m)
- add BuildRequires

* Sun Jun  6 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-11m)
- add BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-8m)
- rebuild against gcc43

* Wed Mar 19 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.5.0-7m)
- add pkgconfig patch

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-6m)
- rebuild against gtk-sharp2-2.12.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-5m)
- %%NoSource -> NoSource

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-4m)
- fix %%changelog section

* Mon Sep  4 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.1.0-3m)
- revise %%install section for x86_64

* Mon Sep  4 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1.0-2m)
- add BuildRequires: galago-sharp >= 0.5.0

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.0-1m)
- initila build
