%global momorel 4
%global srcrel 87586
%global qtver 4.6.3
%global kdever 4.4.5
%global kdebaseworkspacerel 1m
%global kdebaserel 1m
%global kdedir /usr

Summary: An OpenGL screensaver with exploding comets for KDE
Name: kcometen4
Version: 1.0.7
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Amusements/Graphics
URL: http://www.mehercule.net/staticpages/index.php/kcometen4
Source0: http://www.kde-apps.org/CONTENT/content-files/%{srcrel}-%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdebase >= %{kdever}-%{kdebaserel}
Requires: xscreensaver-base
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdebase-workspace-devel >= %{kdever}-%{kdebaseworkspacerel}
BuildRequires: cmake
BuildRequires: libGLU-devel
Provides: kcometen3
Obsoletes: kcometen3

%description
KCometen4 is an OpenGL screensaver with exploding comets for KDE.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL
%{_kde4_bindir}/%{name}.kss
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/kde4/services/ScreenSavers/%{name}.desktop
%{_mandir}/man1/%{name}.kss.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-4m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.6-3m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.6-1m)
- version 1.0.6

* Wed Jul  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- version 1.0.5

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- version 1.0.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-2m)
- rebuild against rpm-4.6

* Mon Sep 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-1m)
- version 1.0.3

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-1m)
- switch to kcometen4

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-3m)
- specify KDE3 headers and libs
- modify Requires and buildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- %%NoSource -> NoSource

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1m)
- initial package for Momonga Linux
