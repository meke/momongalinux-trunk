%global momorel 1
#%%global snapdate 2010-06-27

Summary: embryo
Name: embryo
Version: 1.7.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://www.enlightenment.org/
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen pkgconfig

%description
Embryo is primarily a shared library that gives you an API to load and control
interpreted programs compiled into an abstract machine bytecode that it
understands.  This abstract (or virtual) machine is similar to a real machine
with a CPU, but it is emulated in software.  The architecture is simple and is
the same as the abstract machine (AMX) in the PAWN language (formerly called
SMALL) as it is based on exactly the same code. Embryo has modified the code
for the AMX extensively and has made it smaller and more portable.  It is VERY
small.  The total size of the virtual machine code AND header files is less
than 2500 lines of code.  It includes the floating point library support by
default as well.  This makes it one of the smallest interpreters around, and
thus makes is very efficient to use in code.

%package devel
Summary: Embryo headers, static libraries, documentation and test programs
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static
%make
sed -i -e 's/$projectname Documentation Generated: $datetime/$projectname Documentation/' doc/foot.html


%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
find %{buildroot} -name '*.la' -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/embryo_cc
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/include
%{_datadir}/embryo/include/*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0 release of core EFL 

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9.49898-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9.49898-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49539-1m)
- update to new svn snap

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.063-1m)
- update to new svn snap

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.062-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.062-1m)
- update to new svn snap

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.061-1m)
- update to new svn snap

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.060-1m)
- update

* Thu May 21 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.9.9.050-3m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.050-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.050-1m)
- update

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.043-1m)
- update to 0.9.9.043

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1.042-1m)
- merge from T4R
-
- changelog is below
* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- * Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- - (0.9.1.042-1m)
- - update to 0.9.1.042
- - delete %%{_bindir}/embryo-config

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1.037-0.20061222.3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1.037-0.20061222.2m)
- %%NoSource -> NoSource

* Mon Jan  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.1.037-0.20061222.1m)
- version 0.9.1.037-20061222

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.1.022-0.20051209.1m)
- version 0.9.1.022-20051209

* Tue Jun 28 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.1.010-0.20050627.1m)
- version 0.9.1.010-20050627

* Thu Feb 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.1-0.20050215-1m)
- version 0.9.1-0.20050215

* Mon Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.1-0.20041218-2m)
- enable x86_64.

* Tue Dec 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.1-0.20041218-1m)
- first import to Momonga
