%global         momorel 1
%global         qtver 4.8.5
%global         kdever 4.11.1
%global         kdelibsrel 1m
%global         libbluedevilver 1.9.4
%global         qtdir %{_libdir}/qt4
%global         kdedir /usr
%global         ftpdirver 1.3.2
%global         sourcedir stable/%{name}/%{ftpdirver}/src

Name:           bluedevil
Version:        %{ftpdirver}
Release:        %{momorel}m%{?dist}
Summary:        Bluetooth stack for KDE
Group:          Applications/Communications
License:        GPLv2+
URL:            http://www.afiestas.org/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libbluedevil-devel >= %{libbluedevilver}
BuildRequires:  cmake
BuildRequires:  kdelibs-devel >= %{kdever}
Obsoletes:      kbluetooth

%description
BlueDevil is the bluetooth stack for KDE.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
Development files for %{name}.

%prep
%setup -q -n %{name}-v%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_kde4_bindir}/bluedevil-audio
%{_kde4_bindir}/bluedevil-helper
%{_kde4_bindir}/bluedevil-input
%{_kde4_bindir}/bluedevil-monolithic
%{_kde4_bindir}/bluedevil-network-dun
%{_kde4_bindir}/bluedevil-network-panu
%{_kde4_bindir}/bluedevil-sendfile
%{_kde4_bindir}/bluedevil-wizard
%{_kde4_libdir}/kde4/*.so
%{_kde4_libexecdir}/bluedevil-*
%{_kde4_libdir}/libbluedevilaction.so
%{_kde4_datadir}/applications/kde4/*
%{_kde4_appsdir}/bluedevil
%{_kde4_appsdir}/bluedevilwizard
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/kde4/services/*.protocol
%{_kde4_datadir}/kde4/services/kded/*
%{_datadir}/dbus-1/services/*.service
%{_datadir}/mime/packages/bluedevil-mime.xml
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/kde4/servicetypes/actionplugin.desktop

%files devel
%defattr(-,root,root,-)
%doc HACKING
%{_includedir}/bluedevil

%changelog
* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Tue Jan  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Sun Sep 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-2m)
- modify %%files to avoid conflicting

* Sat Apr 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Wed Feb  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Mon Dec  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-2m)
- modify %%files to avoid conflicting with kdelibs

* Sun Dec  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- import from Fedora devel
- use bluedevil instead of kbluetooth

* Tue Nov 30 2010 Jaroslav Reznik <jreznik@redhat.com> 1.0-1
- update to 1.0 final

* Mon Sep 27 2010 Jaroslav Reznik <jreznik@redhat.com> 1.0-0.1.rc4.1
- update to rc4-1

* Thu Aug 19 2010 Jaroslav Reznik <jreznik@redhat.com> 1.0-0.1.rc3
- update to rc3

* Fri Aug 13 2010 Jaroslav Reznik <jreznik@redhat.com> 1.0-0.1.rc2
- initial package
