%global         momorel 3

Name:           perl-Email-Abstract
Version:        3.007
Release:        %{momorel}m%{?dist}
Summary:        Unified interface to mail representations
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Email-Abstract/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/Email-Abstract-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Email-Simple >= 1.91
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Module-Pluggable >= 1.5
BuildRequires:  perl-MRO-Compat
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl-Email-Simple >= 1.91
Requires:       perl-Module-Pluggable >= 1.5
Requires:       perl-MRO-Compat
Requires:       perl-Scalar-Util
Requires:       perl-Test-Simple >= 0.47
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Email::Abstract provides module writers with the ability to write simple,
representation-independent mail handling code. For instance, in the cases
of Mail::Thread or Mail::ListDetector, a key part of the code involves
reading the headers from a mail object. Where previously one would either
have to specify the mail class required, or to build a new object from
scratch, Email::Abstract can be used to perform certain simple operations
on an object regardless of its underlying representation.

%prep
%setup -q -n Email-Abstract-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE
%{perl_vendorlib}/Email/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.007-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.007-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.007-1m)
- update to 3.007

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.006-1m)
- update to 3.006

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.005-2m)
- rebuild against perl-5.18.1

* Sat Aug  3 2013 NARITA Koicho <pulsar@momonga-linux.org>
- (3.005-1m)
- update to 3.005

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-10m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-9m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.004-2m)
- rebuild for new GCC 4.6

* Sat Feb 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.004-1m)
- update to 3.004

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.003-2m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.003-1m)
- update to 3.003

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.002-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.002-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.002-1m)
- update to 3.002

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.001-7m)
- rebuild against perl-5.12.1

* Sun May  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.001-6m)
- add BuildRequires

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.001-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.001-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.001-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.001-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.000-1m)
- update to 3.000

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.134-2m)
- rebuild against gcc43

* Sat Nov 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.134-1m)
- update to 2.134

* Wed Aug 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.132-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
