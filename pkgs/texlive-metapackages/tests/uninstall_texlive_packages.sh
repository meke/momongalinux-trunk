#!/bin/sh

export LANG=C

error()
{
    echo $@
    exit -1
}


get_pkg_name()
{
    [ -n $1 ] || error "get_pkg_name() failed."
    
    echo $1 | sed 's,^\(.*\)-\([^-]*\)-\([^-]*\)$,\1,g'
}

get_pkg_version()
{
    [ -n $1 ] || error "get_pkg_version() failed."
    
    echo $1 | sed 's,^\(.*\)-\([^-]*\)-\([^-]*\)$,\2-\3,g'
}

remove_test()
{
    rpm -q $1 > /dev/null || return

    echo $1
    echo -n "$1" > /dev/stderr
    rpm -e --test $1  2>&1 \
	| awk '$5=="needed" && $7=="(installed)" { print $8; }
               $3=="needed" && $5=="(installed)" { print $6; }' \
		   | while read pkg; do
	echo -n "." > /dev/stderr
	get_pkg_name $pkg
    done
    echo  > /dev/stderr
}


remove_pkgs() 
{
    A=`mktemp /tmp/remove_pkg_XXXXXXXX ` || exit -1
    B=`mktemp /tmp/remove_pkg_XXXXXXXX ` || exit -1
    
    while [ -n "$1" ]; do 
	remove_test "$1"
	shift
    done | sort | uniq > $A
    
    if [ "0" -eq `wc -l < $A` ]; then
	rm -f $A $B
	return 
    fi 
    
    while [ /bin/true ]; do
	cat $A | while read pkg; do  remove_test $pkg; done | sort | uniq > $B
	
	diff $A $B > /dev/null 
	[ 0 -eq $? ] && break
	cp $B $A
    done

    echo 
    echo "*** Dump  packages to remove ....."
    cat  $A

    read -p "delete these packages? (y/N) " ans
    
    case "$ans" in
	y)
	    LIST=`cat $A | while read pkg; do echo -n "$pkg "; done`
	    sudo rpm -e $LIST
	    ;;
    esac

    rm -f $A $B
}



remove_pkgs `rpm -qa 'texlive*'` kpathea

LIST=
for dir in /usr/share/texmf*; do 
    [ -d $dir ] || continue
    for file in `find $dir`; do 
	for pkg in `rpm -qf $file | awk '$1!="file" { print $1}'`; do
		LIST="$LIST $pkg"
	done
    done
done

remove_pkgs $LIST


sudo rm -rf /usr/share/texmf*

exit
