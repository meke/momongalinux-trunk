%global momorel 8

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define module tg_expanding_form_widget

Name:           python-tgexpandingformwidget
Version:        0.1.3
Release:        %{momorel}m%{?dist}
Summary:        A repeating form widget for TurboGears

Group:          Development/Languages
License:        MIT
URL:            http://www.psychofx.com/TGExpandingFormWidget/
Source0:        http://www.psychofx.com/TGExpandingFormWidget/download/TGExpandingFormWidget-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python-devel >= 2.7 python-setuptools >= 0.6c9-2m


%description
TGExpandingFormWidget is a type of repeating form widget for TurboGears.
It contains groups of widgets which are repeated on the page. The user is
able to dynamically add or remove widget groups as required before submitting
the form.

%prep
%setup -q -n TGExpandingFormWidget-%{version}


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE.txt
%{python_sitelib}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.3-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.3-2m)
- rebuild against python-2.6.1

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.3-1m)
- import from Fedora

* Fri Jan 18 2008 Rob Crittenden <rcritten@redhat.com> - 0.1.3-5
- consistent use of %{buildroot} and $RPM_BUILD_ROOT
- remove unused pyver definition

* Thu Jan 17 2008 Rob Crittenden <rcritten@redhat.com> - 0.1.3-4
- Add python-devel and pytnon-setuptools to BuildRequires
- Updated tarball. It changed upstream without updating the version. It
  now includes its own setup.py
- Package LICENCE.txt as documentation

* Wed Jan 16 2008 Rob Crittenden <rcritten@redhat.com> - 0.1.3-3
- Build from the tar.gz source instead of the egg
- Provide setup.py since the upstream doesn't include it

* Tue Nov 13 2007 Rob Crittenden <rcritten@redhat.com> - 0.1.3-2
- Rename to python-tgexpandingformwidget
- Used tgfastdata rpm as new template for spec

* Thu Nov  8 2007 Rob Crittenden <rcritten@redhat.com> - 0.1.3-1
- Initial creation

