%global momorel 1

Summary: Utility for editing audit configuration
Name: system-config-audit
Version: 0.4.19
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: https://fedorahosted.org/system-config-audit/
Source0: https://fedorahosted.org/releases/s/y/system-config-audit/system-config-audit-%{version}.tar.xz
Requires: pygtk2-libglade, usermode, usermode-gtk
BuildRequires: audit-libs-devel, desktop-file-utils >= 0.16, gettext, intltool
BuildRequires: python-devel >= 2.7

%description
This package provides a GUI that allows the user to configure the Linux
audit subsystem.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
make install-fedora DESTDIR=%{buildroot} INSTALL='install -p'
desktop-file-validate \
	%{buildroot}/%{_datadir}/applications/system-config-audit.desktop

%find_lang system-config-audit

%clean
rm -rf %{buildroot}

%files -f system-config-audit.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/system-config-audit
%{_datadir}/applications/system-config-audit.desktop
%{_datadir}/system-config-audit
%{_libexecdir}/system-config-audit-server-real
%{_libexecdir}/system-config-audit-server
%config(noreplace) %{_sysconfdir}/pam.d/system-config-audit-server
%config(noreplace) %{_sysconfdir}/security/console.apps/system-config-audit-server

%changelog
* Sun Oct  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.19-1m)
- update 0.4.19

* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.18-1m)
- update 0.4.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.15-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.15-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.15-4m)
- full rebuild for mo7 release

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.15-2m)
- build fix with desktop-file-utils-0.16

* Sat Jun 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.15-2m)
- rebuild against desktop-file-utils-0.16

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.15-1m)
- update 0.4.15

* Thu Dec 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.13-1m)
- import from Rawhide for audit2

* Mon Oct  5 2009 Miloslav Trmac <mitr@redhat.com> - 0.4.13-1
- Update to system-config-audit-0.4.13.

* Tue Sep 15 2009 Miloslav Trmac <mitr@redhat.com> - 0.4.12-1
- Update to system-config-audit-0.4.12.

* Thu Jul 30 2009 Miloslav Trmac <mitr@redhat.com> - 0.4.11-2
- Expand %%description.

* Wed Jul 29 2009 Miloslav Trmac <mitr@redhat.com> - 0.4.11-1
- Separate from audit.
