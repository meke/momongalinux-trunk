%global momorel 1

%bcond_without selinux
%bcond_without pam
%bcond_without audit
%bcond_without inotify

Summary: Cron daemon for executing programs at set times
Name: cronie
Version: 1.4.11
Release: %{momorel}m%{?dist}
License: MIT and BSD and "ISC" and GPLv2
Group: System Environment/Base
URL: https://fedorahosted.org/cronie
Source0: https://fedorahosted.org/releases/c/r/cronie/%{name}-%{version}.tar.gz
NoSource: 0
Source1:   cronie.systemd
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: syslog, bash >= 2.0
Requires: /usr/sbin/sendmail
#Conflicts: sysklogd < 1.4.1
Conflicts: rsyslog < 1.4.1
# now sysklogd is provided by rsyslog
Provides: vixie-cron = 4:4.4
Obsoletes: vixie-cron <= 4:4.3
Requires: dailyjobs

%if %{with selinux}
Requires: libselinux >= 2.0.64
Buildrequires: libselinux-devel >= 2.0.64
%endif
%if %{with pam}
Requires: pam >= 1.1.1
Buildrequires: pam-devel >= 1.1.1
%endif
%if %{with audit}
Buildrequires: audit-libs-devel >= 2.0.4
%endif

BuildRequires:    systemd
Obsoletes:        %{name}-sysvinit

Requires(post): chkconfig coreutils sed
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd
Requires(post):   systemd

%description
Cronie contains the standard UNIX daemon crond that runs specified programs at
scheduled times and related tools. It is a fork of the original vixie-cron and
has security and configuration enhancements like the ability to use pam and
SELinux.

%package anacron
Summary: Utility for running regular jobs
Requires: crontabs
Group: System Environment/Base
Provides: dailyjobs
Provides: anacron = 2.4
Obsoletes: anacron <= 2.3
Requires(post): coreutils
Requires: %{name} = %{version}-%{release}

%description anacron
Anacron becames part of cronie. Anacron is used only for running regular jobs.
The default settings execute regular jobs by anacron, however this could be
overloaded in settings.

%package noanacron
Summary: Utility for running simple regular jobs in old cron style
Group: System Environment/Base
Provides: dailyjobs
Requires: crontabs
Requires: %{name} = %{version}-%{release}

%description noanacron
Old style of {hourly,daily,weekly,monthly}.jobs without anacron. No features.

%prep
%setup -q

%build

%configure \
%if %{with pam}
--with-pam \
%endif
%if %{with selinux}
--with-selinux \
%endif
%if %{with audit}
--with-audit \
%endif
%if %{with inotify}
--with-inotify \
%endif
--enable-anacron --enable-pie --enable-relro

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT DESTMAN=$RPM_BUILD_ROOT%{_mandir}
mkdir -pm700 $RPM_BUILD_ROOT%{_localstatedir}/spool/cron
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/
mkdir -pm755 $RPM_BUILD_ROOT%{_sysconfdir}/cron.d/
%if ! %{with pam}
    rm -f $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/crond
%endif
install -m 755 crond.sysconfig $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/crond
touch $RPM_BUILD_ROOT%{_sysconfdir}/cron.deny
install -m 644 contrib/anacrontab $RPM_BUILD_ROOT%{_sysconfdir}/anacrontab
install -c -m755 contrib/0hourly $RPM_BUILD_ROOT%{_sysconfdir}/cron.d/0hourly
mkdir -pm 755 $RPM_BUILD_ROOT%{_sysconfdir}/cron.hourly
install -c -m755 contrib/0anacron $RPM_BUILD_ROOT%{_sysconfdir}/cron.hourly/0anacron
mkdir -p $RPM_BUILD_ROOT/var/spool/anacron
touch $RPM_BUILD_ROOT/var/spool/anacron/cron.daily
touch $RPM_BUILD_ROOT/var/spool/anacron/cron.weekly
touch $RPM_BUILD_ROOT/var/spool/anacron/cron.monthly

# noanacron package
install -m 644 contrib/dailyjobs $RPM_BUILD_ROOT/%{_sysconfdir}/cron.d/dailyjobs

# install systemd initscript
mkdir -p $RPM_BUILD_ROOT/lib/systemd/system/
install -m 644 %SOURCE1 $RPM_BUILD_ROOT/lib/systemd/system/crond.service

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ $1 -eq 1 ] ; then
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
    /bin/systemctl enable crond.service >/dev/null 2>&1 || :
fi

%post anacron
[ -e /var/spool/anacron/cron.daily ] || touch /var/spool/anacron/cron.daily
[ -e /var/spool/anacron/cron.weekly ] || touch /var/spool/anacron/cron.weekly
[ -e /var/spool/anacron/cron.monthly ] || touch /var/spool/anacron/cron.monthly

%preun
if [ $1 -eq 0 ]; then 
    /bin/systemctl --no-reload disable crond.service >/dev/null 2>&1 || :
    /bin/systemctl stop crond.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ]; then
    /bin/systemctl try-restart crond.service >/dev/null 2>&1 || :
fi

# empty /etc/crontab in case there are only old regular jobs
%triggerun -- cronie < 1.4.1
cp -a /etc/crontab /etc/crontab.rpmsave
sed -e '/^01 \* \* \* \* root run-parts \/etc\/cron\.hourly/d'\
  -e '/^02 4 \* \* \* root run-parts \/etc\/cron\.daily/d'\
  -e '/^22 4 \* \* 0 root run-parts \/etc\/cron\.weekly/d'\
  -e '/^42 4 1 \* \* root run-parts \/etc\/cron\.monthly/d' /etc/crontab.rpmsave > /etc/crontab
exit 0

#copy the lock, remove old daemon from chkconfig
%triggerun -- vixie-cron
cp -a /var/lock/subsys/crond /var/lock/subsys/cronie > /dev/null 2>&1 ||:

#if the lock exist, then we restart daemon (it was running in the past).
#add new daemon into chkconfig everytime, when we upgrade to cronie from vixie-cron
%triggerpostun -- vixie-cron
/sbin/chkconfig --add crond
[ -f /var/lock/subsys/cronie ] && ( rm -f /var/lock/subsys/cronie ; service crond restart ) > /dev/null 2>&1 ||:

%triggerun -- cronie < 1.4.7
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply crond
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save crond

# The package is allowed to autostart:
/bin/systemctl enable crond.service >/dev/null 2>&1

/sbin/chkconfig --del crond >/dev/null 2>&1 || :
/bin/systemctl try-restart crond.service >/dev/null 2>&1 || :
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%triggerin -- pam, glibc, libselinux
# changes in pam, glibc or libselinux can make crond crash
# when it calls pam
/bin/systemctl try-restart crond.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL README ChangeLog
%attr(755,root,root) %{_sbindir}/crond
%attr(6755,root,root) %{_bindir}/crontab
%{_mandir}/man8/crond.*
%{_mandir}/man8/cron.*
%{_mandir}/man5/crontab.*
%{_mandir}/man1/crontab.*
%dir %{_localstatedir}/spool/cron
%dir %{_sysconfdir}/cron.d
%if %{with pam}
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/pam.d/crond
%endif
%config(noreplace) %{_sysconfdir}/sysconfig/crond
%config(noreplace) %{_sysconfdir}/cron.deny
%attr(0644,root,root) %{_sysconfdir}/cron.d/0hourly
%attr(0644,root,root) /lib/systemd/system/crond.service

%files anacron
%defattr(-,root,root,-)
%{_sbindir}/anacron
%attr(0755,root,root) %{_sysconfdir}/cron.hourly/0anacron
%config(noreplace) %{_sysconfdir}/anacrontab
%dir /var/spool/anacron
%ghost %verify(not md5 size mtime) /var/spool/anacron/cron.daily
%ghost %verify(not md5 size mtime) /var/spool/anacron/cron.weekly
%ghost %verify(not md5 size mtime) /var/spool/anacron/cron.monthly
%{_mandir}/man5/anacrontab.*
%{_mandir}/man8/anacron.*

%files noanacron
%defattr(-,root,root,-)
%attr(0644,root,root) %{_sysconfdir}/cron.d/dailyjobs

%changelog
* Mon Jul 29 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.11-1m)
- update 1.4.11

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.8-2m)
- update systemd script

* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.8-1m)
- update 1.4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-2m)
- full rebuild for mo7 release

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-1m)
- [SECURITY] CVE-2010-0424
- sync with Fedora 13 (1.4.4-1)
-- create anacron and noanacron subpackages

* Wed Jan  6 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.4.3-2m)
- add pam patch

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.3-1m)
- update 1.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-1m)
- sync with Fedora 11 (1.3-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43

* Fri Feb 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1m)
- import to Momonga from fedora
- use %%{_initscriptdir}

* Tue Feb 12 2008 Marcela Maslanova <mmaslano@redhat.com> - 1.0-5
- upgrade from less than cronie-1.0-4 didn't add chkconfig

* Wed Feb  6 2008 Marcela Maslanova <mmaslano@redhat.com> - 1.0-4
- 431366 after reboot wasn't cron in chkconfig

* Tue Feb  5 2008 Marcela Maslanova <mmaslano@redhat.com> - 1.0-3
- 431366 trigger part => after update from vixie-cron on cronie will 
	be daemon running.

* Wed Jan 30 2008 Marcela Maslanova <mmaslano@redhat.com> - 1.0-2
- change the provides on higher version than obsoletes

* Tue Jan  8 2008 Marcela Maslanova <mmaslano@redhat.com> - 1.0-1
- packaging cronie
- thank's for help with packaging to my reviewers
