%global        momorel 1
%global        base_name kde-telepathy
%global        unstable 0
%if 0%{unstable}
%global        release_dir unstable
%else
%global        release_dir stable
%endif
%global        kdever 4.13.0
%global        ftpdirver 0.8.1
%global        sourcedir %{release_dir}/%{base_name}/%{ftpdirver}/src
%global        qtver 4.8.5
%global        qtrel 1m
%global        cmakever 2.8.5
%global        cmakerel 2m
%global        telepathy_logger_qt_version 0.8.0

Name:          ktp-text-ui
Summary:       Text UI Module for Telepathy Instant Messaging
Version:       %{ftpdirver}
Release:       %{momorel}m%{?dist}
License:       LGPLv2+
Group:         System Environment/Libraries
URL:           http://www.kde.org
Source0:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:      0
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: ktp-common-internals-devel >= %{ftpdirver}
BuildRequires: telepathy-qt4-devel >= 0.9.3
BuildRequires: telepathy-logger-devel >= 0.6.0
BuildRequires: telepathy-logger-qt-devel >= %{telepathy_logger_qt_version}
Obsoletes:     telepathy-kde-text-ui
Obsoletes:     telepathy-kde-text-ui-devel
Obsoletes:     ktp-text-ui-devel

%description
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc
%{_kde4_bindir}/ktp-log-viewer
%{_kde4_libexecdir}/ktp-adiumxtra-protocol-handler
%{_kde4_libexecdir}/%{name}
%{_kde4_libdir}/libktpchat.so
%{_kde4_libdir}/kde4/kcm_ktp_chat_appearance.so
%{_kde4_libdir}/kde4/kcm_ktp_chat_behavior.so
%{_kde4_libdir}/kde4/kcm_ktp_chat_messages.so
%{_kde4_libdir}/kde4/kcm_ktp_logviewer_behavior.so
%{_kde4_libdir}/kde4/kcm_ktptextui_message_filter_latex.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_bugzilla.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_emoticons.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_formatting.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_highlight.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_images.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_latex.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_searchexpansion.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_tts.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_urlexpansion.so
%{_kde4_libdir}/kde4/ktptextui_message_filter_youtube.so
%{_kde4_appsdir}/ktelepathy/Template.html
%{_kde4_appsdir}/ktelepathy/longurl.js
%{_kde4_appsdir}/ktelepathy/longurlServices.json
%{_kde4_appsdir}/ktelepathy/showBugzillaInfo.js
%{_kde4_appsdir}/ktelepathy/styles
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/ktp-log-viewer
%{_kde4_datadir}/kde4/services/adiumxtra.protocol
%{_kde4_datadir}/kde4/services/kcm_ktp_chat_appearance.desktop
%{_kde4_datadir}/kde4/services/kcm_ktp_chat_behavior.desktop
%{_kde4_datadir}/kde4/services/kcm_ktp_chat_messages.desktop
%{_kde4_datadir}/kde4/services/kcm_ktp_logviewer_behavior.desktop
%{_kde4_datadir}/kde4/services/kcm_ktptextui_message_filter_latex.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_bugzilla.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_emoticons.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_formatting.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_highlight.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_images.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_latex.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_searchexpansion.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_tts.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_urlexpansion.desktop
%{_kde4_datadir}/kde4/services/ktptextui_message_filter_youtube.desktop
%{_kde4_datadir}/kde4/servicetypes/ktptxtui_message_filter.desktop
%{_kde4_datadir}/applications/kde4/ktp-log-viewer.desktop
%{_kde4_datadir}/telepathy/clients/KTp.TextUi.client
%{_kde4_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.KTp.TextUi.service

%changelog
* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.1-1m)
- update to 0.8.0.1

* Wed Mar 12 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Wed Feb 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.80-1m)
- update to 0.7.80

* Fri Nov 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-2m)
- revise source URI

* Mon Oct 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Thu Sep 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.80-1m)
- update to 0.6.80

* Wed Aug  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Tue Apr  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Thu Feb 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Tue Dec 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-2m)
- rebuild against telepathy-logger-0.6.0 and telepathy-logger-qt-0.5.1-2m

* Fri Oct  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-2m)
- fix %%files

* Fri Jul 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Sun May  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0
- rename from telepathy-kde-text-ui to ktp-text-ui

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-2m)
- add BuildRequires

* Mon Nov 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Sat Sep 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-1m)
- initial build for Momonga Linux
