%global momorel 1
%define gtk3_version           2.99.3
%define dbus_version           0.90
%define dbus_glib_version      0.74
%define redhat_menus_version   5.0.1
%define gnome_desktop3_version 3.1.91
%define libgnomekbd_version    3.5.2

Summary: GNOME Screensaver
Name: gnome-screensaver
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Amusements/Graphics
#VCS: git:git://git.gnome.org/gnome-screensaver
Source0: http://download.gnome.org/sources/gnome-screensaver/3.6/%{name}-%{version}.tar.xz
NoSource: 0
Source1: gnome-screensaver-hide-xscreensaver.menu

Patch8: gnome-screensaver-2.20.0-selinux-permit.patch

URL: http://www.gnome.org
BuildRequires: gtk3-devel => %{gtk3_version}
BuildRequires: dbus-devel >= %{dbus_version}
BuildRequires: dbus-glib-devel >= %{dbus_glib_version}
BuildRequires: gnome-desktop3-devel >= %{gnome_desktop3_version}
BuildRequires: pam-devel
BuildRequires: nss-devel
BuildRequires: libX11-devel, libXScrnSaver-devel, libXext-devel
BuildRequires: libXinerama-devel libXmu-devel
BuildRequires: libgnomekbd-devel >= %{libgnomekbd_version}
# this is here because the configure tests look for protocol headers
BuildRequires: xorg-x11-proto-devel
BuildRequires: gettext
BuildRequires: automake, autoconf, libtool, intltool, gnome-common
BuildRequires: libXxf86misc-devel
BuildRequires: libXxf86vm-devel
BuildRequires: libXtst-devel
BuildRequires: desktop-file-utils
BuildRequires: gsettings-desktop-schemas >= 0.1.7
BuildRequires: systemd-devel

Requires: gsettings-desktop-schemas >= 0.1.7
#Requires: redhat-menus >= %{redhat_menus_version}

# since we use it, and pam spams the log if a module is missing
Requires: gnome-keyring-pam
Conflicts: xscreensaver < 5.00-19

# I have no idea why this is required, but for now, just get things to build
BuildRequires:  libxklavier-devel

%description
gnome-screensaver is a screen saver and locker that aims to have
simple, sane, secure defaults and be well integrated with the desktop.

%prep
%setup -q
%patch8 -p1 -b .selinux-permit

autoreconf -f -i

%build
%configure --with-mit-ext=no --enable-systemd
make

%install
make install DESTDIR=%{buildroot}

%find_lang %{name}

%files -f %{name}.lang
%doc AUTHORS NEWS README COPYING
%{_bindir}/*
%{_libexecdir}/*
%{_sysconfdir}/pam.d/*
%{_sysconfdir}/xdg/autostart/gnome-screensaver.desktop
%doc %{_mandir}/man1/*.1.bz2

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sat Jul 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Sat Jul 21 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-2m)
- rebuild against libgnomekbd-3.5.2

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.3-1m)
- update to 3.4.3

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.2-2m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-3m)
- rebuild for glib 2.33.2

* Mon Feb 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- add BuildRequires

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-5m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-4m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-3m)
- add Requires: GConf2

* Mon Jul 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- move menu to XDG_CONFIG_DIR

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- [SECURITY] CVE-2010-0422
- update to 2.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-3m)
- rebuild against libxklavier-5.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-2m)
- rebuild against libxklavier-4.0

* Tue Apr 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Feb 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-1m)
- update to 2.25.2

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-2m)
- back to 2.24.1

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-1m)
- update to 2.25.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against rpm-4.6

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Thu Oct 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-2m)
- delete startup file

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-3m)
- add Source1 (start with gnome-session, not gnome-settings-daemon)

* Tue May 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-2m)
- fix dekstop files

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Fri Apr  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sun Feb 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-2m)
- rebuild against libxklavier-3.4

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.8-1m)
- update to 2.17.8

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.7-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.7-2m)
- revice %%post script (gconftool-2)

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.7-1m)
- update to 2.17.7 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-3m)
- add patch0 for rename category

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-2m)
- rename category Screensaver to ScreenSaver

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- updateo to 2.16.0

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-3m)
- rebuils against dbus 0.92
- add patch0 for dbus

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.3-2m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu May 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-4m)
- add migrate scripts

* Wed May 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-3m)
- delete BuildPrereq: xscreensaver-*

* Tue May 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-2m)
- mv gnome-screensavers.menu to %%{_sysconfdir}/gconf/schemas/

* Wed May 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- Initial Build
