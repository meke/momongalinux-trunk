%global momorel 1

%define webroot /var/www/lighttpd

Summary: Lightning fast webserver with light system requirements
Name: lighttpd
Version: 1.4.35
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Daemons
URL: http://www.lighttpd.net/
Source0: http://download.lighttpd.net/lighttpd/releases-1.4.x/lighttpd-%{version}.tar.bz2
NoSource: 0
Source1: lighttpd.logrotate
Source2: php.d-lighttpd.ini
Source3: lighttpd.init
Source4: lighttpd.service
Source10: index.html
Source11: http://www.lighttpd.net/favicon.ico
Source12: http://www.lighttpd.net/light_button.png
Source13: http://www.lighttpd.net/light_logo.png
Source14: lighttpd-empty.png
Source100: lighttpd-mod_geoip.c
Source101: lighttpd-mod_geoip.txt
Source1000: powered_by_momonga.png
Patch0: lighttpd-1.4.28-defaultconf.patch
Patch1: lighttpd-1.4.34-mod_geoip.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: system-logos
# this is provided by momonga-logos
Requires(pre): shadow-utils
Requires(post): systemd-units
Requires(post): systemd-sysv
Requires(preun): systemd-units
Requires(postun): systemd-units
Provides: webserver
BuildRequires: openssl-devel >= 1.0.0, pcre-devel >= 8.31, bzip2-devel, zlib-devel
BuildRequires: awk
BuildRequires:  mysql-devel >= 5.5.10
%{!?_without_ldap:BuildRequires: openldap-devel >= 2.4.0}
%{?_with_gamin:BuildRequires: gamin-devel}
%{!?_without_gdbm:BuildRequires: gdbm-devel}
%{!?_without_lua:BuildRequires: lua-devel}

%description
Secure, fast, compliant and very flexible web-server which has been optimized
for high-performance environments. It has a very low memory footprint compared
to other webservers and takes care of cpu-load. Its advanced feature-set
(FastCGI, CGI, Auth, Output-Compression, URL-Rewriting and many more) make
it the perfect webserver-software for every server that is suffering load
problems.

Available rpmbuild rebuild options :
--with : gamin webdavprops webdavlocks memcache
--without : ldap gdbm lua (cml)


%package fastcgi
Summary: FastCGI module and spawning helper for lighttpd and PHP configuration
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}

%description fastcgi
This package contains the spawn-fcgi helper for lighttpd's automatic spawning
of local FastCGI programs. Included is also a PHP .ini file to change a few
defaults needed for correct FastCGI behavior.


%package mod_geoip
Summary: GeoIP module for lighttpd to use for location lookups
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}
BuildRequires: GeoIP-devel

%description mod_geoip
GeoIP module for lighttpd to use for location lookups.


%package mod_mysql_vhost
Summary: Virtual host module for lighttpd that uses a MySQL database
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}
BuildRequires: mysql-devel >= 5.1.32

%description mod_mysql_vhost
Virtual host module for lighttpd that uses a MySQL database.


%prep
%setup -q
%patch0 -p1 -b .defaultconf
%patch1 -p1 -b .mod_geoip
%{__install} -p -m 0644 %{SOURCE100} src/mod_geoip.c
%{__install} -p -m 0644 %{SOURCE101} mod_geoip.txt


%build
autoreconf -vif

%configure \
    --libdir="%{_libdir}/lighttpd" \
    --with-mysql \
    %{!?_without_ldap:--with-ldap} \
    --with-openssl \
    %{?_with_gamin:--with-fam} \
    %{?_with_webdavprops:--with-webdav-props} \
    %{?_with_webdavlocks:--with-webdav-locks} \
    %{!?_without_gdbm:--with-gdbm} \
    %{?_with_memcache:--with-memcache} \
    %{?!_without_lua:--with-lua}
%{__make} %{?_smp_mflags}


%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

# Install our own logrotate entry
%{__install} -D -p -m 0644 %{SOURCE1} \
    %{buildroot}%{_sysconfdir}/logrotate.d/lighttpd

# Install our own php.d ini file
%{__install} -D -p -m 0644 %{SOURCE2} \
    %{buildroot}%{_sysconfdir}/php.d/lighttpd.ini

install -D -p -m 0644 %{SOURCE4} \
    %{buildroot}%{_unitdir}/lighttpd.service

# Install our own default web page and images
%{__mkdir_p} %{buildroot}%{webroot}
%{__install} -p -m 0644 %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} \
    %{buildroot}%{webroot}/
%{__install} -p -m 0644 %{SOURCE1000} %{buildroot}%{webroot}/poweredby.png

# Example configuration to be included as %%doc
rm -rf config
cp -a doc/config config
find config -name 'Makefile*' | xargs rm -f
# Remove +x from scripts to be included as %%doc to avoid auto requirement
chmod -x doc/scripts/*.sh

# Install (*patched above*) sample config files
mkdir -p %{buildroot}%{_sysconfdir}/lighttpd
cp -a config/*.conf config/*.d %{buildroot}%{_sysconfdir}/lighttpd/

# Install empty log directory to include
mkdir -p %{buildroot}%{_var}/log/lighttpd

# Install empty run directory to include (for the example fastcgi socket)
mkdir -p %{buildroot}%{_var}/run/lighttpd
# Setup tmpfiles.d config for the above
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d
echo 'D /var/run/lighttpd 0750 lighttpd lighttpd -' > \
    %{buildroot}%{_sysconfdir}/tmpfiles.d/lighttpd.conf

%clean
%{__rm} -rf %{buildroot}


%pre
/usr/sbin/useradd -s /sbin/nologin -M -r -d %{webroot} \
    -c 'lighttpd web server' lighttpd &>/dev/null || :

%post
if [ $1 -eq 1 ] ; then
  /bin/systemctl daemon-reload &>/dev/null || :
fi

%preun
if [ $1 -eq 0 ]; then
  /bin/systemctl --no-reload disable lighttpd.service &>/dev/null || :
  /bin/systemctl stop lighttpd.service &>/dev/null || :
fi

%postun
/bin/systemctl daemon-reload &>/dev/null || :
if [ $1 -ge 1 ]; then
    /bin/systemctl try-restart lighttpd.service &>/dev/null || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%doc config/ doc/scripts/rrdtool-graph.sh
%dir %{_sysconfdir}/lighttpd/
%dir %{_sysconfdir}/lighttpd/conf.d/
%dir %{_sysconfdir}/lighttpd/vhosts.d/
%config(noreplace) %{_sysconfdir}/lighttpd/*.conf
%config(noreplace) %{_sysconfdir}/lighttpd/conf.d/*.conf
%config %{_sysconfdir}/lighttpd/conf.d/mod.template
%config %{_sysconfdir}/lighttpd/vhosts.d/vhosts.template
%config(noreplace) %{_sysconfdir}/logrotate.d/lighttpd
%{_unitdir}/lighttpd.service
%config(noreplace) %{_sysconfdir}/tmpfiles.d/lighttpd.conf
%{_sbindir}/lighttpd
%{_sbindir}/lighttpd-angel
%{_libdir}/lighttpd/
%exclude %{_libdir}/lighttpd/*.la
%exclude %{_libdir}/lighttpd/mod_fastcgi.so
%exclude %{_libdir}/lighttpd/mod_geoip.so
%exclude %{_libdir}/lighttpd/mod_mysql_vhost.so
%{_mandir}/man8/lighttpd.8*
%attr(0750, lighttpd, lighttpd) %{_var}/log/lighttpd/
%attr(0750, lighttpd, lighttpd) %{_var}/run/lighttpd/
%dir %{webroot}/
%{webroot}/*.ico
%{webroot}/*.png
# This is not really configuration, but prevent loss of local changes
%config %{webroot}/index.html

%files fastcgi
%defattr(-,root,root,-)
%doc doc/outdated/fastcgi*.txt doc/scripts/spawn-php.sh
%config(noreplace) %{_sysconfdir}/php.d/lighttpd.ini
#%%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_fastcgi.so

%files mod_geoip
%defattr(-,root,root,-)
%doc mod_geoip.txt
#%%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_geoip.so

%files mod_mysql_vhost
%defattr(-,root,root,-)
%doc doc/outdated/mysqlvhost.txt
#%%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_mysql_vhost.so


%changelog
* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.35-1m)
- [SECURITY] CVE-2014-2323 CVE-2014-2324
- update to 1.4.35

* Mon Dec 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.33-2m)
- update service file

* Wed Nov 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.33-1m)
- [SECURITY] CVE-2013-4508
- update to 1.4.33

* Sun Nov 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.31-2m)
- [SECURITY] CVE-2012-5533

* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.31-1m)
- update to 1.4.31
- rebuild against pcre-8.31

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.30-1m)
- [SECURITY] CVE-2011-3389 CVE-2011-4362
- update to 1.4.30

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.29-1m)
- update 1.4.29

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.26-5m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.26-4m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.26-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.26-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.26-1m)
- update to 1.4.26

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.23-5m)
- rebuild against openssl-1.0.0

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.23-4m)
- [SECURITY] CVE-2010-0295
- apply the upstream patch (rediffed)

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.23-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.23-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.23-1m)
- update to 1.4.23 based on Fedora 11 (1.4.22-3)
-- removed spawn-fcgi from upstream

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.20-5m)
- define __libtoolize :

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.20-4m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.20-3m)
- rebuild against mysql-5.1.32
- License: Modified BSD

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.20-2m)
- rebuild against rpm-4.6

* Thu Oct  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.20-1m)
- [SECURITY] CVE-2008-1531 (lighttpd_sa_2008_04)
- [SECURITY] CVE-2008-4359 (lighttpd_sa_2008_05)
- [SECURITY] CVE-2008-4360 (lighttpd_sa_2008_06)
- [SECURITY] CVE-2008-4298 (lighttpd_sa_2008_07)
- update to 1.4.20
- ommit patch10, this fix was included upstream source

* Fri Jul 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.19-1m)
- update to 1.4.19
- comment out conflict Patch10: lighttpd-1.4.19-sslshutdownfix.patch

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.18-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.18-3m)
- rebuild against gcc43

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.18-2m)
- rebuild against openldap-2.4.8

* Sat Sep 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.18-1m)
- [SECURITY] CVE-2007-4727
- update to 1.4.18

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.15-5m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sun Jul  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.15-4m)
- modify Requires for Momonga linux

* Sun Jul  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.15-3m)
- modify %%files

* Sun Jul  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.15-2m)
- modify spec

* Tue Jun  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.15-1m)
- update to 1.4.15
- [SECURITY] CVE-2007-1869 CVE-2007-1870

* Mon Apr  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.13-1m)
- import from fc-devel to Momonga
- add transform='s,x,x,'

* Fri Feb  2 2007 Matthias Saou <http://freshrpms.net/> 1.4.13-5
- Update defaultconf patch to change php binary to /usr/bin/php-cgi (#219723).
- Noticed %%{?_smp_mflags} was missing, so add it as it works fine.

* Mon Jan 29 2007 Matthias Saou <http://freshrpms.net/> 1.4.13-4
- Remove readline-devel build req, added by lua but since fixed (#213895).

* Mon Nov  6 2006 Matthias Saou <http://freshrpms.net/> 1.4.13-3
- Switch to using killall for log rotation to fix it when using workers.

* Mon Oct 16 2006 Matthias Saou <http://freshrpms.net/> 1.4.13-2
- Remove gcc-c++ build req, it's part of the defaults.
- Add readline-devel build req, needed on RHEL4.

* Wed Oct 11 2006 Matthias Saou <http://freshrpms.net/> 1.4.13-1
- Update to 1.4.13, which contains the previous fix.

* Tue Oct  3 2006 Matthias Saou <http://freshrpms.net/> 1.4.12-3
- Include fix for segfaults (lighttpd bug #876, changeset 1352).

* Mon Sep 25 2006 Matthias Saou <http://freshrpms.net/> 1.4.12-1
- Update to 1.4.12 final.

* Fri Sep 22 2006 Matthias Saou <http://freshrpms.net/> 1.4.12-0.1.r1332
- Update to latest 1.4.12 pre-release, fixes SSL issues and other bugs.
- Update powered_by_fedora.png to the new logo.

* Mon Aug 28 2006 Matthias Saou <http://freshrpms.net/> 1.4.11-2
- FC6 rebuild.

* Thu Mar  9 2006 Matthias Saou <http://freshrpms.net/> 1.4.11-1
- Update to 1.4.11.

* Mon Mar  6 2006 Matthias Saou <http://freshrpms.net/> 1.4.10-2
- FC5 rebuild.

* Wed Feb  8 2006 Matthias Saou <http://freshrpms.net/> 1.4.10-1
- Update to 1.4.10.
- Remove now included fix.

* Wed Jan 25 2006 Matthias Saou <http://freshrpms.net/> 1.4.9-2
- Add mod_fastcgi-fix patch to fix crash on backend overload.

* Mon Jan 16 2006 Matthias Saou <http://freshrpms.net/> 1.4.9-1
- Update to 1.4.9.

* Wed Nov 23 2005 Matthias Saou <http://freshrpms.net/> 1.4.8-1
- Update to 1.4.8.

* Fri Nov  4 2005 Matthias Saou <http://freshrpms.net/> 1.4.7-1
- Update to 1.4.7.

* Wed Oct 12 2005 Matthias Saou <http://freshrpms.net/> 1.4.6-1
- Update to 1.4.6.

* Mon Oct  3 2005 Matthias Saou <http://freshrpms.net/> 1.4.5-1
- Update to 1.4.5.
- Disable gamin/fam support for now, it does not work.

* Tue Sep 27 2005 Matthias Saou <http://freshrpms.net/> 1.4.4-3
- Update to current SVN to check if it fixes the remaining load problems.

* Wed Sep 21 2005 Matthias Saou <http://freshrpms.net/> 1.4.4-2
- Patch to SVN 722 revision : Fixes a crash in mod_mysql_vhost and a problem
  with keepalive and certain browsers.

* Mon Sep 19 2005 Matthias Saou <http://freshrpms.net/> 1.4.4-1
- Update to 1.4.4 final.
- Enable ldap auth, gdbm and gamin/fam support by default.

* Thu Sep 15 2005 Matthias Saou <http://freshrpms.net/> 1.4.4-0
- Update to 1.4.4 pre-release (fixes another fastcgi memleak).
- Enable lua (cml module) by default.
- Add --with-webdav-props conditional option.

* Tue Sep 13 2005 Matthias Saou <http://freshrpms.net/> 1.4.3-2
- Include lighttpd-1.4.3-stat_cache.patch to fix memleak.

* Fri Sep  2 2005 Matthias Saou <http://freshrpms.net/> 1.4.3-1.1
- Rearrange the included index.html to include the new logo, button and
  favicon from lighttpd.net.

* Fri Sep  2 2005 Matthias Saou <http://freshrpms.net/> 1.4.3-1
- Update to 1.4.3.
- No longer override libdir at make install stage, use DESTDIR instead, as
  the resulting binary would now have referenced to %%{buildroot} :-(

* Tue Aug 30 2005 Matthias Saou <http://freshrpms.net/> 1.4.2-1
- Update to 1.4.2.

* Mon Aug 22 2005 Matthias Saou <http://freshrpms.net/> 1.4.1-1
- Update to 1.4.1.

* Sun Aug 21 2005 Matthias Saou <http://freshrpms.net/> 1.4.0-1
- Update to 1.4.0.
- Add conditional of gamin, gdbm, memcache and lua options.

* Mon Aug  1 2005 Matthias Saou <http://freshrpms.net/> 1.3.16-2
- Update to 1.3.16, rebuild.

* Mon Jul 18 2005 Matthias Saou <http://freshrpms.net/> 1.3.15-1
- Update to 1.3.15.

* Mon Jun 20 2005 Matthias Saou <http://freshrpms.net/> 1.3.14-1
- Update to 1.3.14.

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 1.3.13-5
- rebuild on all arches

* Mon Apr  4 2005 Matthias Saou <http://freshrpms.net/> 1.3.13-4
- Change signal sent from the logrotate script from USR1 to HUP, as that's the
  correct one.

* Fri Apr  1 2005 Michael Schwendt <mschwendt[AT]users.sf.net> 1.3.13-2
- Include /etc/lighttpd directory.

* Sun Mar  6 2005 Matthias Saou <http://freshrpms.net/> 1.3.13-1
- Update to 1.3.13.

* Wed Mar  2 2005 Matthias Saou <http://freshrpms.net/> 1.3.12-1
- Update to 1.3.12.
- Remove obsolete empty_cgi_handler patch.

* Tue Mar  1 2005 Matthias Saou <http://freshrpms.net/> 1.3.11-2
- Add missing defattr to sub-packages (#150018).

* Mon Feb 21 2005 Matthias Saou <http://freshrpms.net/> 1.3.11-0
- Update to 1.3.11.
- Remove cleanconf and init.d patches (merged upstream).
- Add empty_cgi_handler patch.

* Fri Feb 18 2005 Matthias Saou <http://freshrpms.net/> 1.3.10-0
- Split off -fastcgi sub-package.
- Include php.d entry to set sane FastCGI defaults.

* Wed Feb 16 2005 Matthias Saou <http://freshrpms.net/> 1.3.10-0
- Spec file cleanup for freshrpms.net/Extras.
- Compile OpenSSL support unconditionally.
- Put modules in a subdirectory of libdir.
- Don't include all of libdir's content to avoid debuginfo content.
- Add optional LDAP support.
- Add patch to change the default configuration.
- Add dedicated lighttpd user/group creation.
- Add logrotate entry.
- Include a nice little default page for the default setup.
- Split off mod_mysql_vhost sub-package, get dep only there.
- Use webroot in /srv by default.
- Exclude .la files, I doubt anyone will need them.

* Thu Sep 30 2004 <jan@kneschke.de> 1.3.1
- upgraded to 1.3.1

* Tue Jun 29 2004 <jan@kneschke.de> 1.2.3
- rpmlint'ed the package
- added URL
- added (noreplace) to start-script
- change group to Networking/Daemon (like apache)

* Sun Feb 23 2003 <jan@kneschke.de>
- initial version

