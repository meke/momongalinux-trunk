%global momorel 1

Summary: A library which allows userspace access to USB devices.
Name: libusb-compat
Version: 0.1.4
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://libusb.sourceforge.net/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/libusb/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: libusb-0.1.4-includedir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: docbook-utils
BuildRequires: docbook-utils-pdf
BuildRequires: docbook-dtds >= 1.0
BuildRequires: jadetex
BuildRequires: doxygen
BuildRequires: pkgconfig
BuildRequires: libusb1-devel
ExcludeArch: s390 s390x

Obsoletes: libusb
Provides: libusb

%description
This package provides a way for applications to access USB devices.

%package devel
Summary: Development files for libusb.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: usbutils-devel
Obsoletes: libusb-devel
Provides: libusb-devel

%description devel
This package contains the header files and documentation needed to
develop applications that use libusb.

%prep
%setup -q
sed -e 's|\${libdir}|$RPM_BUILD_ROOT%{_libdir}|' -i configure.ac
%patch0 -p1 -b .includedir

%build
autoconf
%configure --disable-static --enable-examples-build
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog LICENSE NEWS README
%{_libdir}/libusb-0.1.so.*
%exclude %{_libdir}/libusb.la

%files devel
%defattr(-,root,root)
%{_bindir}/libusb-config
%{_includedir}/usb.h
%{_libdir}/pkgconfig/libusb.pc
%{_libdir}/libusb.so

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-1m)
- update 0.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-3m)
- rebuild for new GCC 4.5

* Sat Oct 30 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (0.1.3-2m)
- use own header file

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-2m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.3-1m)
- rename package from libusb to libusb-compat

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.12-10m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.12-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.12-8m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.12-7m)
- rebuild against rpm-4.6

* Fri Oct 17 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (0.1.12-6m)
- added BuildRequires docbook-utils-pdf (/usr/bin/docbook2ps)

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.12-5m)
- add Patch0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.12-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.12-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.12-2m)
- delete libtool library

* Sun May  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.12-1m)
- version 0.1.12

* Fri Dec 30 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.10a-1m)
- sync with fc-devel

* Sat Jun 11 2005 mutecat <mutecat@momonga-linux.org>
- (0.1.10-2m)
- arrange ppc

* Sun Feb 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.10-1m)
- major bugfixes

* Wed Dec 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.1.8-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Mon Feb 16 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (0.1.8-1m)
- version 0.1.8
- removed libusb-doc.tar.gz from repository

* Mon Nov 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.7-1m)
- minor bugfixes

* Mon Aug  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.6a-4m)
- revise URL

* Wed Jul 24 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.6a-3m)
- source url change
- add BuildPreReq docbook-utils

* Sun Jul 21 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (0.1.6a-2m)
- devel require base with same release number

* Sun Jul 21 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (0.1.6a-1m)
- Import from RawHide (0.1.6-1)
- 0.1.6 -> 0.1.6a

* Tue Jun 25 2002 Tim Waugh <twaugh@redhat.com> 0.1.6-1
- 0.1.6.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com> 0.1.5-6
- automated rebuild

* Fri Jun 21 2002 Tim Waugh <twaugh@redhat.com> 0.1.5-5
- Rebuild to fix broken deps.

* Thu May 23 2002 Tim Powers <timp@redhat.com> 0.1.5-4
- automated rebuild

* Thu Apr 11 2002 Tim Waugh <twaugh@redhat.com> 0.1.5-3
- Rebuild (fixes bug #63196).

* Thu Feb 21 2002 Tim Waugh <twaugh@redhat.com> 0.1.5-2
- Rebuild in new environment.

* Thu Feb  7 2002 Tim Waugh <twaugh@redhat.com> 0.1.5-1
- 0.1.5.

* Fri Jan 25 2002 Tim Waugh <twaugh@redhat.com> 0.1.4-2
- Rebuild in new environment.
- Work around tarball brokenness (doc directory was not automade).

* Mon Oct 29 2001 Tim Waugh <twaugh@redhat.com> 0.1.4-1
- Adapted for Red Hat Linux.
- 0.1.4.

* Thu Mar  1 2001 Frederic Crozat <fcrozat@mandrakesoft.com> 0.1.3b-1mdk
- Initial Mandrake release
