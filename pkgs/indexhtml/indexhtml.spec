%global momorel 10

Summary: The HTML page you'll see after installing Momonga Linux
Name: indexhtml
Version: 1.0
Release: %{momorel}m%{?dist}
Source0: indexhtml.tar.bz2
Source1: index.html.ja
Source2: index.html.en
Source3: line.css
License: OPL
Group: Documentation
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Prefix: %{_docdir}/HTML

%description
The indexhtml package contains the HTML page and graphics for a
welcome page shown by your Web browser, which you'll see after you've
successfully installed Momonga Linux.

%prep
%setup -q -n indexhtml
cp -f %{SOURCE1} ./
cp -f %{SOURCE2} ./
cp -f %{SOURCE3} ./

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_docdir}/HTML

#install -m 644 index.html %{buildroot}%{_docdir}/HTML/index.html
#cp -ap $RPM_BUILD_DIR/indexhtml-%{version}/img %{buildroot}/usr/doc/HTML
cd %{buildroot}%{_docdir}/HTML
cp -p $RPM_BUILD_DIR/indexhtml/* .

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_docdir}/HTML/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-5m)
- rebuild against gcc43

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.0-4m)
- remove Epoch

* Mon Nov 03 2003 TAKAHASHI Tamotsu <tamo>
- (1.0-3m)
- <html xmlns="http://www.w3.org/1999/xhtml">

* Mon Nov 03 2003 TAKAHASHI Tamotsu <tamo>
- (1.0-2m)
- delete unwanted "\n" and
  add "<?xml-stylesheet href="line.css" type="text/css"?>" and
  put "<!DOCTYPE...>" at the top of the files
  (Source1/2: index.html.{en,ja}) [Momonga-devel.ja:01978]
  Thanks, Ichiro Nakai!
- use momorel
- License: OPL (add more if the author wants)

* Sat Sep 21 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.0-1m [Epoch:1])
- Source1/2: index.html.en/ja
- Source3: line.css
- Epoch: 1
- Prefix: _docdir/HTML
- first release for Momonga Linux

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (1.1-8k)
- add link to config-sample

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1-5k)
- fixed for FHS

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Wed Apr 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, description )
- Kondarized.

* Sun Mar 12 2000 Tenkou N. Hattori <tnh@kondara.org>
- bug fixed.
- version down to 1.1 :-P

* Wed Mar  8 2000 MATSUDA, Daiki <dyky@df-usa.com>
- include english version
- update version to 1.1.1

* Thu Jan 27 2000 Tenkou N. Hattori <tnh@kondara.org>
- update version to 1.1.

* Tue Jan 25 2000 Norihito Ohmori <nono@kondara.org>
- modified for Kondara.

* Tue Sep 21 1999 Bill Nottingham <notting@redhat.com>
- update for 6.1

* Wed Apr 14 1999 Bill Nottingham <notting@redhat.com>
- update for 6.0

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Wed Feb 10 1999 Michael Johnson <johnsonm@redhat.com>
- use relative links instead of absolute links

* Sun Oct 11 1998 Bill Nottingham <notting@redhat.c9om>
- point to 5.2 installation guide
- 90 days installation support, not 30

* Tue Oct  6 1998 Matt Wilson <msw@redhat.com>
- Remove link to Applixware as we no longer ship it.

* Sat Sep 19 1998 Jeff johnson <jbj@redhat.com>
- update version to 5.2.
- tweak description so that it's slightly different than summary.

* Tue Jul 21 1998 Jeff Johnson <jbj@redhat.com>
- fix spelling error.
- add build root.

* Wed May 06 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed May 06 1998 Edward C. Bailey <ed@redhat.com>
- Added link to 5.1 Installation Guide

* Mon May 04 1998 Erik Troan <ewt@redhat.com>
- changed summary and description for new version of RPM

* Fri Oct 24 1997 Otto Hammersmith <otto@redhat.com>
- updated for 5.0.. new links to various LDP guides

* Thu Jul 31 1997 Erik Troan <ewt@redhat.com>
- made a noarch package

* Tue Apr 15 1997 Erik Troan <ewt@redhat.com>
- Incorporated Otto's fix into tarball properly. 

* Mon Apr 14 1997 Otto Hammersmith <otto@redhat.com>
- Fixed an error in /usr/doc/HTML/index.html that said we provided 
  60 days of installation support, not 30 like it should.  

