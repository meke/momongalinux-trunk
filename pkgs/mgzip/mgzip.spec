%global momorel 6
%global srcname smp_mgzip

Summary: The multi-processor capable .gz file creator
Name: mgzip
Version: 1.2c
Release: %{momorel}m%{?dist}
License: "zlib"
Group: Applications/File
Source0: http://lemley.net/%{srcname}_%{version}.tar.gz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://lemley.net/mgzip.html

%description
mgzip is a program that makes use of SMP machines and zlib to use as many processors as
you have to quickly compress files into gzip compatible format. 

%prep
%setup -q -n %{srcname}_%{version}

%build
./configure
perl -pi -e 's/gz_header/mgz_header/g' mgzip.c
%make

%clean
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
%{__install} -m 0755 mgzip %{buildroot}%{_bindir}

%files
%defattr(-,root,root)
%{_bindir}/*

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2c-6m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2c-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2c-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2c-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2c-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2c-1m)
- initial spec file from gzip.spec
