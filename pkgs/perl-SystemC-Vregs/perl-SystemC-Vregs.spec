%global momorel 8

# If the emacs-el package has installed a pkgconfig file, use that to determine
# install locations and Emacs version at build time, otherwise set defaults.
%if %($(pkg-config emacs) ; echo $?)
%define emacs_version 22.1
%define emacs_lispdir  %{_datadir}/emacs/site-lisp
%define emacs_startdir %{_datadir}/emacs/site-lisp/site-start.d
%else
%define emacs_version  %{expand:%(pkg-config emacs --modversion)}
%define emacs_lispdir  %{expand:%(pkg-config emacs --variable sitepkglispdir)}
%define emacs_startdir %{expand:%(pkg-config emacs --variable sitestartdir)}
%endif

Name:           perl-SystemC-Vregs
Version:        1.470
Release:        %{momorel}m%{?dist}
Summary:        Utility routines used by vregs
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/SystemC-Vregs/
Source0:        http://www.cpan.org/authors/id/W/WS/WSNYDER/SystemC-Vregs-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Bit-Vector
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-HTML-TableExtract
BuildRequires:  perl-Pod-Parser >= 1.34
BuildRequires:  perl-Verilog >= 2.1
BuildRequires:  readline-devel
Requires:       perl-Bit-Vector
Requires:       perl-HTML-Parser
Requires:       perl-HTML-TableExtract
Requires:       perl-Pod-Parser >= 1.34
Requires:       perl-Verilog >= 2.1
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
A Vregs object contains a documentation "package" containing enumerations,
definitions, classes, and registers.

%package -n     emacs-vregs-mode
Summary:        Elisp source files for systemc-vregs under GNU Emacs
Group:          Development/Libraries
BuildRequires:  emacs
Requires:       emacs >= %{emacs_version}

%description -n emacs-vregs-mode
This package provides emacs support for systemc-vregs

%prep
%setup -q -n SystemC-Vregs-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

emacs -batch -f batch-byte-compile vregs-mode.el
%{__install} -d %{buildroot}%{emacs_lispdir}
%{__install} -pm 0644 vregs-mode.el vregs-mode.elc %{buildroot}%{emacs_lispdir}/

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING README vregs_spec.doc vregs_spec.htm
%{_bindir}/vreg*

%dir %{perl_vendorlib}/SystemC
%{perl_vendorlib}/SystemC/Vregs.pm
%{perl_vendorlib}/SystemC/vregs_spec__rules.pl

%dir %{perl_vendorlib}/SystemC/Vregs
%{perl_vendorlib}/SystemC/Vregs/*
%{_mandir}/man?/*

%files -n emacs-vregs-mode
%defattr(-,root,root,-)
%{emacs_lispdir}/vregs-mode.el*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.470-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.470-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.470-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.470-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.470-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.470-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.470-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.470-1m)
- update to 1.470
- rebuild against perl-5.16.0

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.464-11m)
- rebuild for emacs-24.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.464-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.464-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.464-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.464-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.464-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.464-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.464-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.464-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.464-2m)
- rebuild against perl-5.12.0

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.464-1m)
- update to 1.464

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.463-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.463-4m)
- rebuild against perl-5.10.1

* Wed Jul  8 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.463-3m)
- revised BR

* Mon Jun 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.463-2m)
- remove BuildRequires: emacs-el

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.463-1m)
- import from Fedora
- version up 1.463

* Wed Mar 04 2009 Chitlesh GOORAH <chitlesh [AT] fedoraproject DOT org> 1.462-1
- new upstream release

* Fri Jan 09 2009 Chitlesh GOORAH <chitlesh [AT] fedoraproject DOT org> 1.461-1
- new upstream release

* Sun Dec 28 2008 Chitlesh GOORAH <chitlesh [AT] fedoraproject DOT org> 1.460-2
- spec file revisited upon request : #476449c1

* Sun Dec 14 2008 Chitlesh GOORAH <chitlesh [AT] fedoraproject DOT org> 1.460-1
- Specfile autogenerated by cpanspec 1.77.
