%global         momorel 1

Name:           shotwell
Version:        0.13.1
Release:        %{momorel}m%{?dist}
Summary:        A photo organizer for the GNOME desktop

Group:          Applications/Multimedia
License:        LGPLv2+ 
URL:            http://www.yorba.org/shotwell/
Source0:	http://yorba.org/download/%{name}/0.13/%{name}-%{version}.tar.xz
NoSource: 	0
# http://redmine.yorba.org/issues/3379
Source1:        shotwell-icons.tar.bz2
# Fix installed prefix detection when invoked as /bin/shotwell
# https://bugzilla.redhat.com/show_bug.cgi?id=812652 and
# http://redmine.yorba.org/issues/5181
Patch0:         shotwell-usrmove.patch
BuildRequires:  gtk2-devel
BuildRequires:  GConf2-devel
BuildRequires:  sqlite-devel
BuildRequires:  vala-devel >= 0.17.5
BuildRequires:  libgee-devel
BuildRequires:  libgexiv2-devel >= 0.4.1
BuildRequires:  libgudev1-devel
BuildRequires:  libxml2-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  exiv2-devel >= 0.23
BuildRequires:  libgexiv2-devel >= 0.3.0
BuildRequires:  unique-devel
BuildRequires:  libexif-devel
BuildRequires:  libgphoto2-devel
BuildRequires:  webkitgtk-devel >= 1.3.4
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  LibRaw-devel >= 0.14

%description
Shotwell is a new open source photo organizer designed for the GNOME desktop
environment. It allows you to import photos from your camera, view and edit
them, and share them with others.

%prep
%setup -q
%patch0 -p1 -b .usrmove

%build
./configure --prefix=%{_prefix} \
	--lib=%{_lib} \
	--disable-schemas-install \
	--disable-icon-update
echo CFLAGS=%{optflags} >> configure.mk
make %{?_smp_mflags}


%install
rm -rf %{buildroot}

export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
export XDG_DISABLE_MAKEFILE_UPDATES=1
make install DESTDIR=%{buildroot} SCHEMA_FILE_DIR=%{buildroot}%{_sysconfdir}/gconf/schemas

# Temporary fix
echo "Encoding=UTF-8" >> %{buildroot}%{_datadir}/applications/shotwell.desktop
echo "Encoding=UTF-8" >> %{buildroot}%{_datadir}/applications/shotwell-viewer.desktop

desktop-file-validate %{buildroot}%{_datadir}/applications/shotwell.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/shotwell-viewer.desktop

# put hi-res icons in place
(
  cd $RPM_BUILD_ROOT%{_datadir}/icons/hicolor
  rm -rf 16x16 24x24 scalable
  tar xf %{SOURCE1}
)

%find_lang %{name}
%find_lang %{name}-extras
cat %{name}-extras.lang >> %{name}.lang

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
update-desktop-database &> /dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%pre
if [ "$1" -gt 1 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:
fi

%preun
if [ "$1" -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:
fi

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null
  gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor &>/dev/null || :
fi
update-desktop-database &> /dev/null || :

%posttrans
gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README COPYING MAINTAINERS NEWS THANKS AUTHORS
#%{_sysconfdir}/gconf/schemas/shotwell.schemas
%{_bindir}/shotwell
%{_bindir}/shotwell-video-thumbnailer
%{_libdir}/shotwell
%{_libexecdir}/shotwell
%{_datadir}/GConf/gsettings/shotwell.convert
%{_datadir}/shotwell
%{_datadir}/gnome/help/shotwell/
%{_datadir}/applications/shotwell.desktop
%{_datadir}/applications/shotwell-viewer.desktop
%{_datadir}/glib-2.0/schemas/gschemas.compiled
%{_datadir}/glib-2.0/schemas/org.yorba.shotwell-extras.gschema.xml
%{_datadir}/glib-2.0/schemas/org.yorba.shotwell.gschema.xml
%{_datadir}/icons/hicolor/*/apps/shotwell.png

%changelog
* Sat Oct  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.1-1m)
- REMOVE.PLEASE is no longer needed

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.0-1m)
- update to 0.13.0

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.3-3m)
- import libgphoto25.patch from Fedora, but still cannnot build...

* Thu Aug 23 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.3-2m)
- rebuild against libgphoto2-2.5.0
- comment out BR: vala-compat-devel for the moment, we have no vala-compat-devel

* Wed Aug 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.3-1m)
- update 0.12.3
- rebuild against exiv2-0.23
- rebuild against libgexiv2-0.4.1
- change BuildRequires from libgudev-devel to libgudev1-devel

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.6-1m)
- update 0.11.6

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.4-1m)
- update 0.11.4

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.2-2m)
- rebuild against LibRaw

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.2-1m)
- update to 0.11.2

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.1-1m)
- update 0.10.1

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3-1m)
- update 0.9.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-1m)
- update 0.9.1

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-2m)
- rebuild against libgexiv2-0.3.0 (exiv2-0.21.1)

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-7m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-6m)
- rebuild against webkitgtk-1.3.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-5m)
- full rebuild for mo7 release

* Sat Aug 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-4m)
- update ja.po

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-3m)
- rebuild against vala-0.9.3 or later

* Thu Jul  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-2m)
- fix build on x86_64

* Wed Jul  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-1m)
- Initial commit Momonga Linux

