%global momorel 13

Summary: MAL (Mobil Application Link) library
Name: libmal
Version: 0.44
Release: %{momorel}m%{?dist}
License: MPL
Group: System Environment/Libraries
Source0: http://jasonday.home.att.net/code/libmal/libmal-%{version}.tar.gz 
#Patch0: libmal-0.31-lib64.patch
Patch0: libmal-0.44-lib64.patch
URL: http://jasonday.home.att.net/code/libmal/libmal.html
BuildRequires: pilot-link-devel >= 0.12.1-2m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
libmal is really just a convenience library of the functions in Tom Whittaker's
malsync distribution, along with a few wrapper functions. 

%package devel
Summary: MAL (Mobil Application Link) static library and headers
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
libmal is really just a convenience library of the functions in Tom Whittaker's
malsync distribution, along with a few wrapper functions. 

%prep
%setup -q
%if %{_lib} == "lib64"
%patch0 -p1 -b .lib64
%endif

%build
autoreconf -vfi
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README ChangeLog MPL-1_0.txt TODO
%{_bindir}/malsync
%{_libdir}/libmal.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/libmal.a
%{_libdir}/libmal.so

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-13m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.44-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.44-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.44-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.44-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.44-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.44-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.44-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.44-5m)
- %%NoSource -> NoSource

* Mon Aug 13 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.44-4m)
- revised lib64 patch for adding autoreconf

* Sun Jul 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.44-3m)
- add autoreconf (libtool-1.5.24)

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.44-2m)
- delete libtool library

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44
- rebuild against pilot-link-0.12.1-1m

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.31-4m)
- enable x86_64.

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31-3m)
- s/Copyright:/License:/

* Mon Aug 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31-2m)
- add defattr

* Tue May 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.31-1m)
- import to momonga

* Tue Jan 21 2003 Jason Day <jasonday@worldnet.att.net>
- Initial version

