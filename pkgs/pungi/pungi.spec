%global momorel 1
%global pythonver 2.7
#%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           pungi
Version:        2.11
Release:        %{momorel}m%{?dist}
Summary:        Distribution compose tool

Group:          Development/Tools
License:        GPLv2
URL:            https://fedorahosted.org/pungi
Source0:        https://fedorahosted.org/pungi/attachment/wiki/%{version}/%{name}-%{version}.tar.bz2
Source5:	mo5-momonga.ks
Source6:	mo5-minimal-momonga.ks
Source7:	mo6-momonga.ks
Source8:	mo7-momonga.ks
Source9:	mo8-momonga.ks
Patch0:		pungi-2.8-momonga.patch
Patch1:		pungi-2.5-i686_support.patch
Patch2:		pungi-2.9-createrepo_bz2.patch
Patch10:	pungi-2.11-wildcard-fix.patch
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       anaconda, yum => 3.2.13, repoview, createrepo
Requires:       lorax
BuildRequires:  python-devel >= %{pythonver}

BuildArch:      noarch

%description
A tool to create anaconda based installation trees/isos of a set of rpms.


%prep
%setup -q

%patch10 -p1

%patch0 -p1 -b .momonga
%patch1 -p1 -b .i686
%patch2 -p1 -b .compress_type

%build
%{__python} setup.py build


%install


rm -rf --preserve-root %{buildroot}
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
%{__install} -d $RPM_BUILD_ROOT/var/cache/pungi
%{__install} -d $RPM_BUILD_ROOT/%{_mandir}/man8
%{__install} doc/pungi.8 $RPM_BUILD_ROOT/%{_mandir}/man8/
%{__mv} $RPM_BUILD_ROOT/%{_bindir}/pungi.py $RPM_BUILD_ROOT/%{_bindir}/pungi


# install Momonga config
install -m 644 %{SOURCE5} %{buildroot}/%{_datadir}/pungi/
install -m 644 %{SOURCE6} %{buildroot}/%{_datadir}/pungi/
install -m 644 %{SOURCE7} %{buildroot}/%{_datadir}/pungi/
install -m 644 %{SOURCE8} %{buildroot}/%{_datadir}/pungi/
install -m 644 %{SOURCE9} %{buildroot}/%{_datadir}/pungi/
 
%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc Authors Changelog COPYING GPL ToDo doc/README
%{python_sitelib}/pungi-*.egg-info
%{python_sitelib}/pypungi
%{_bindir}/pungi
%config %{_datadir}/pungi/*
%{_mandir}/man8/pungi*8*
/var/cache/pungi

%changelog
* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11-1m)
- update 2.11

* Wed Apr 11 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (2.9-4m)
- change mo8-momonga.ks
-- add legacy-software-support group

* Thu Oct 20 2011 SANUKI Masaru <sanuki@momonga-linux.org> 
- (2.9-3m)
- add patch2 (add compress-type option for createrepo)

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9-2m)
- update mo8.ks

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9-1m)
- update 2.9

* Mon May 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5-2m)
- add mo8-momonga.ks
-- default use development repository

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5-1m)
- update 2.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.21-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.21-6m)
- rebuild for new GCC 4.5

* Sat Sep 11 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.21-5m)
- update mo7-momonga.ks

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.21-4m)
- full rebuild for mo7 release

* Fri Aug 20 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.21-3m)
- update mo7-momonga.ks

* Fri Aug 20 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.21-2m)
- update mo7-momonga.ks

* Tue Jun 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.21-1m)
- update 2.0.21
- add mo7-momonga.ks

* Thu Apr  1 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.20-1m)
- update 2.0.20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.16-1m)
- update 2.0.16

* Mon Jul  6 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.0.14-2m)
- add Momonga Linux 6 kickstart file (mo6-momonga.ks)

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.14-1m)
- update 2.0.14

* Mon Mar 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.12-1m)
- update 2.0.12

* Fri Feb 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.10-1m)
- update 2.0.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.18.1-7m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.18-6m)
- update Patch0 for fuzz=0
- License: GPLv2

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.18-5m)
- rebuild agaisst python-2.6.1-1m

* Wed Oct  1 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.18.1-4m)
- add minimal kickstart file (mo5-minimal-momonga.ks)

* Mon Sep 29 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.18.1-3m)
- add buildsys-build group

* Thu Sep  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.18.1-2m)
- add nilfs-utils, btrfs-progs

* Thu Jun 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.18.1-1m)
- update 1.2.18.1

* Fri Jun 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.18-4m)
- remove explicitly java-sun-j2se1.6-sdk and seamonkey

* Sun Jun  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.18-3m)
- remove following groups from mo5-momonga.ks (to diet)
- xfce-desktop
- xfce-software-development
- window-managers

* Tue May 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.18-2m)
- add following groups to mo5-momonga.ks (to correcting standard install)
- admin-tools
- core
- education
- engineering-and-scientific
- legacy-network-server
- legacy-software-development
- legacy-software-support
- ruby
- xfce-desktop
- xfce-software-development
- window-managers

* Sat May 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.18-1m)
- update 1.2.18

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.17-1m)
- update 1.2.17

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.16-3m)
- restrict python ver-rel for egginfo

* Mon Apr 28 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.16-2m)
- enable egg-info

* Wed Apr 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.16-1m)
- update 1.2.16

* Wed Apr 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.14-1m)
- update 1.2.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.11-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-1m)
- update 1.2.11

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-2m)
- enable sqlite-base package list

* Thu Jan 24 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-1m)
- update 1.2.7

* Mon Jan  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6-1m)
- update 1.2.6

* Mon Nov  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.9-1m)
- update 1.1.9

* Thu Oct 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-1m)
- update 1.1.5

* Tue Aug 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-1m)
- update 0.4.0

* Fri Aug  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.8-2m)
- support momonga-release, momonga-release-notes

* Thu Jun 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.8-1m)
- update 0.3.8

* Sun Jun  3 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.3.5-2m)
- delete comps.xml.momonga4.core(Source50)
- add yum.conf.i686(Source12)

* Wed May 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.5-1m)
- update 0.3.5

* Wed Apr 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-2m)
- createrepo unused "--database" option.
- pungi support i686 arch

* Mon Apr  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-1m)
- initial commit

