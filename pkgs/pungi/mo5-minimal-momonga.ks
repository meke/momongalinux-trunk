# Kickstart file for composing the "Momonga" spin of Momonga 5

# Use a part of 'iso' to define how large you want your isos.
# Only used when composing to more than one iso.
# Default is 670 (megs), CD size.
#part iso --size=4998

# Add the repos you wish to use to compose here.  At least one of them needs group data.
repo --name=momonga --mirrorlist=http://www.momonga-linux.org/download/mirrors/momonga-$releasever
repo --name=momonga-updates --mirrorlist=http://www.momonga-linux.org/download/mirrors/updates-released-mo$releasever

# local development
# repo --name=momonga-development --baseurl=file:///home/momonga/pkgs/PKGS/

# Package manifest for the compose.  Uses repo group metadata to translate groups.
# (@base is added by default unless you add --nobase to %packages)
%packages
# core
@core
@base
@base-x
-kernel*debug*
-kernel*-devel*
-kernel-kdump*
-syslog-ng
-gnome-packagekit

# Desktop Packages
@xfce-desktop

# apps
##firefox

# Languages
fonts-japanese
opfc-ModuleHP
mplus_medium
anthy
scim-anthy
jfbterm
lv
man-pages-ja
nkf
-scim-bridge

# Compose Needs
anaconda-runtime
#cracklib-python
iscsi-initiator-utils
memtest86+
vnc-server

# Size removals
-gimp-help
-java-1.6.0-openjdk-src
-xorg-x11-docs
-kernel-doc
-java-1.5.0-gcj-src
-java-1.5.0-gcj-devel
-java-sun-j2se1.6-sdk
-libgcj-src
-*javadoc*
-seamonkey*
-kernel-PAE
-kernel-xen
-kernel-openvz*
-man-pages-*
-hunspell
-hunspell-*
-httpd
-bind
-bind-libs
-bind-utils
-sazanami-fonts*
-sendmail
-postfix
-momonga-backgrounds
-mod_perl
#-poppler
-xorg-x11-apps
-xorg-x11-utils
-xorg-x11-xinit-french
-xorg-x11-xinit-german
-xorg-x11-xinit-spanish
#-Terminal
-perl-Template-Toolkit
-cups
-foomatic
-a2ps
-system-config-printer
-system-config-printer-libs

-sdr
%end
