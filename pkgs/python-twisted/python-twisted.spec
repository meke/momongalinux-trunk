%global momorel 1

%{!?python:%define python python}

Name:           %{python}-twisted
Version:        11.0.0
Release:        %{momorel}m%{?dist}
Summary:        Event-based framework for internet applications
Group:          Development/Libraries
License:        MIT
URL:            http://twistedmatrix.com/
Source:         README.fedora
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
Requires:       %{python}-twisted-core   >= %{version}
Requires:       %{python}-twisted-conch  >= 11.0.0
Requires:       %{python}-twisted-lore   >= 11.0.0
Requires:       %{python}-twisted-mail   >= 11.0.0
Requires:       %{python}-twisted-names  >= 11.0.0
Requires:       %{python}-twisted-news   >= 11.0.0
Requires:       %{python}-twisted-runner >= 11.0.0
Requires:       %{python}-twisted-web    >= 11.0.0
Requires:       %{python}-twisted-words  >= 11.0.0
Obsoletes:      Twisted < 2.4.0-1
Provides:       Twisted = %{version}-%{release}
Obsoletes:      twisted < 2.4.0-1
Provides:       twisted = %{version}-%{release}

%description
Twisted is an event-based framework for internet applications.  It includes a
web server, a telnet server, a chat server, a news server, a generic client
and server for remote object access, and APIs for creating new protocols and
services. Twisted supports integration of the Tk, GTK+, Qt or wxPython event
loop with its main event loop. The Win32 event loop is also supported, as is
basic support for running servers on top of Jython.

Installing this package brings all Twisted sub-packages into your system.

%prep
%setup -c -T
install -p -m 0644 %{SOURCE0} README

%files
%defattr(-,root,root,-)
%doc README

%changelog
* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (11.0.01-m)
- update 11.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.2.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.2.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.2.0-1m)
- completely sync with Fedora 11 (8.2.0-2)
- now this is a meta package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1.0-8m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.1.0-2m)
- rebuild agaisst python-2.6.1-1m

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (8.1.0-1m)
- update to 8.1.0

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-6m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.3.0-5m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-4m)
- rebuild against gcc43

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-3m)
- rebuild against python-2.5

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.0-2m)
- rebuild against python-2.4.2

* Wed Mar 23 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-1m)
- import from FC

* Thu Mar  3 2005 Jeremy Katz <katzj@redhat.com> - 1.3.0-4
- rebuild for gcc 4

* Mon Feb 21 2005 Jeremy Katz <katzj@redhat.com> - 1.3.0-3
- disable the -docs subpackage for now

* Fri Jan 14 2005 Jeremy Katz <katzj@redhat.com> - 1.3.0-2
- remove some of the macro abuse
- require python-abi

* Tue Dec  1 2004 Rik van Riel <riel@redhat.com> 1.3.0-1
- grab freshrpms.net python-twisted package, since it is a requirement
  for the Xen tools.  Thanks go out to Matthias Saou and Thomas Vander
  Stichele, who created this package.

* Tue Jun 22 2004 Matthias Saou <http://freshrpms.net> 1.3.0-1
- Update to 1.3.0.
- Spec file changes.

* Sun May 09 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 1.2.0-0.fdr.1: Update to new upstream release
- split off docs
- packaged man pages correctly
- patch to remove hardcoding of python2.2

* Fri Feb 13 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 1.1.1-0.fdr.1: Initial RPM release

