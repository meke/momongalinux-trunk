%global         momorel 1
%global         qtver 4.8.5
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         kdever 4.12.0

Summary:        a KDE browser based on Webkit
Name:           rekonq
Version:        2.4.1
Release:        %{momorel}m%{?dist}
License:        GPLv2
Group:          Applications/Internet
URL:            http://rekonq.kde.org/
Source0:        http://dl.sourceforge.net/project/%{name}/2.0/%{name}-%{version}.tar.xz
NoSource:       0
Patch0:         %{name}-2.3.0-desktop.patch
Patch1:         %{name}-0.8.1-defaultbookmarks.patch
Patch2:         %{name}-2.4.0-ja.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       kdelibs >= %{kdever}-%{kdelibsrel}
Requires:       qt >= %{qtver}
Requires:       qt-webkit >= %{qtver}
Requires:       openssl
Requires(postun): desktop-file-utils, gtk2
Requires(posttrans): desktop-file-utils, gtk2
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRequires:  qt-webkit-devel >= %{qtver}-%{qtrel}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  openssl-devel
BuildRequires:  phonon-devel

%description
rekonq is a KDE browser based on Webkit. Its code is based on Nokia QtDemoBrowser, just like Arora. 
Anyway its implementation is going to embrace KDE technologies to have a full-featured KDE web browser.
This software is the product of my "playing to be a programmer", so the higher its quality is, 
the better I can play.. I've got lots of ideas to implement in it, so many features that I will like 
to have in my "perfect browser". Anyway, rekonq is far from perfect. It needs a lot of love..

rekonq will never have tons of features like (some) other browsers. Anyway, it can:
    * Provide a good tabbed browsing experience ;)
    * Download files through KDE download system
    * share bookmarks with Konqueror
    * navigate in a proxied net
    * browse anonymously
    * inspect web pages

%prep
%setup -q
%patch0 -p1 -b .desktop
%patch1 -p1 -b .defaultbookmarks
%patch2 -p1 -b .ja

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    update-desktop-database -q &> /dev/null
    touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null
    gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi

%posttrans
update-desktop-database -q &> /dev/null
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL TODO
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/libkdeinit4_%{name}.so
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/config.kcfg/%{name}.kcfg
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png

%changelog
* Sun Jan 12 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Sat Nov 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Sat Jul  6 2013 NARITA Koichi <pulsar[momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2

* Mon Jun 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Mon Apr 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Tue Mar 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-2m)
- add Japanese translations

* Fri Mar  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Fri Mar  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Sun Jan 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-1m)
- update to 2.1

* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Fri Nov 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.80-1m)
- update to 1.80

* Mon Oct 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-1m)
- update to 1.70

* Sat Oct  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Wed Jul 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.90-1m)
- update to 0.9.90

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.80-1m)
- update to 0.9.80

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Sun Apr  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0-1

* Sun Dec 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-2m)
- set NoSource again
- pleasse use --rmsrc option to build

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Wed Sep 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.92-1m)
- update to 0.7.92

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.90-1m)
- update to 0.7.90

* Sun Sep 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.80-1m)
- update to 0.7.80

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Mon Mar 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.95-1m)
- update to 0.6.95

* Mon Feb 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.85-1m)
- update to 0.6.85

* Thu Feb 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.80-1m)
- update to 0.6.80

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-2m)
- rebuild for new GCC 4.5

* Sat Oct 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- initial build for Momonga Linux
