%global momorel 4

Name: hunspell-mai
Summary: Maithili hunspell dictionaries
Version: 1.0.1
Release: %{momorel}m%{?dist}
Group: Applications/Text
Source: http://bhashaghar.googlecode.com/files/mai_IN.oxt
URL: http://bhashaghar.googlecode.com
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+ or LGPLv2+ or MPLv1.1
BuildArch: noarch

Requires: hunspell

%description
Maithili hunspell dictionaries.

%prep
%setup -q -c -n hunspell-mai

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p mai_IN.* $RPM_BUILD_ROOT/%{_datadir}/myspell/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_mai_IN.txt COPYING COPYING.MPL COPYING.GPL COPYING.LGPL

%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora 13

* Mon Jan 04 2010 Parag Nemade <pnemade AT redhat.com> - 1.0.1-1
- initial version
