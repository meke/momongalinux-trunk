%global         momorel 1

Name:           perl-ExtUtils-Depends
Version:        0.308
Release:        %{momorel}m%{?dist}
Summary:        Easily build XS extensions that depend on XS extensions
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/ExtUtils-Depends/
Source0:        http://www.cpan.org/authors/id/X/XA/XAOC/ExtUtils-Depends-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO
BuildRequires:  perl-PathTools
BuildRequires:  perl-Test-Simple
Requires:       perl-Data-Dumper
Requires:       perl-IO
Requires:       perl-PathTools
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module tries to make it easy to build Perl extensions that use
functions and typemaps provided by other perl extensions. This means that a
perl extension is treated like a shared library that provides also a C and
an XS interface besides the perl one.

%prep
%setup -q -n ExtUtils-Depends-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/ExtUtils/*
%{_mandir}/man3/*.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.308-1m)
- rebuild against perl-5.20.0
- update to 0.308

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.307-1m)
- update to 0.307

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.306-2m)
- rebuild against perl-5.18.2

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.306-1m)
- update to 0.306

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.305-1m)
- update to 0.305

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-11m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-10m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-9m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.304-2m)
- rebuild for new GCC 4.6

* Thu Jan 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.304-1m)
- update to 0.304

* Tue Nov 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.303-1m)
- update to 0.303

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.302-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.302-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.302-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.302-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.302-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.302-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.302-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.302-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.302-1m)
- update to 0.302

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.301-2m)
- rebuild against rpm-4.6

* Sun Sep  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.301-1m)
- update to 0.301

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.300-2m)
- rebuild against gcc43

* Tue Apr  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.300-1m)
- update to 0.300

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.205-4m)
- %%NoSource -> NoSource

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.205-3m)
- fix files dup

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.205-2m)
- use vendor

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.205-1m)
- import to Momonga from Fedora Development


* Fri Sep  8 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.205-5
- Rebuild for FC6.

* Wed Feb 15 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.205-4
- Rebuild for FC5 (perl 5.8.8).

* Thu Dec 29 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.205-3
- Dist tag.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0.205-2
- rebuilt

* Tue Feb 15 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:0.205-1
- Update to 0.205.

* Sun Oct  3 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:0.204-0.fdr.1
- Update to 0.204.

* Sun Jul 18 2004 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0:0.202-0.fdr.1
- First build.
