%global momorel 9

Summary: EXIF Tag Parsing Library for gtk
Name: libexif-gtk
Version: 0.3.5
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://libexif.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/libexif/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: libexif-gtk-0.3.5-dep.patch
Patch1: libexif-gtk-0.3.5-gtk214.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gettext
BuildRequires: gtk2-devel
BuildRequires: libexif-devel

%description
libexif for gtk

%package devel
Summary: Headers and links to compile against the libexif library
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Group: Development/Libraries

%description devel
libexif for gtk, headers and libraries

%prep
%setup -q
%patch0 -p1 -b .dep
%patch1 -p1 -b .gtk214~

%build
autoreconf -vfi
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/*.so.*
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/*.a
%{_libdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.5-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.5-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.5-5m)
- rebuild against rpm-4.6

* Sat Nov  1 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-4m)
- add patch for gtk-2.14.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.5-3m)
- rebuild against gcc43

* Mon Sep 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.5-2m)
- add patch0 for gtk+-1.12.0

* Sat Mar 31 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.5-1m)
- initial package
