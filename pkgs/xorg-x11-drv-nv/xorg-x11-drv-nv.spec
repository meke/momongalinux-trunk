%global momorel 2
%define tarball xf86-video-nv
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 nv video driver
Name:      xorg-x11-drv-nv
Version:   2.1.20
Release:   %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0:   %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource:  0

Patch4:     nv-reserve-fbarea.patch
Patch5:     nv-2.1.6-starvation.patch
Patch6:     nv-2.1.6-panel-fix.patch
Patch7:     nv-save-rom.patch
Patch9:     nv-2.1.8-g80-no-doublescan.patch

License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64 alpha sparc sparc64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 nv video driver.

%prep
%setup -q -n %{tarball}-%{version}

%patch4 -p1 -b .reserve-fbarea
%patch5 -p1 -b .starve
%patch6 -p1 -b .panel
%patch7 -p1 -b .save-rom
%patch9 -p1 -b .doublescan

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -regex ".*\.la$" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README README.G80
%{driverdir}/nv_drv.so
%{_mandir}/man4/nv.4*

%changelog
* Sat Sep  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.20-2m)
- import fedora patches

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.20-1m)
- update 2.1.20

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.18-7m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.18-6m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.18-5m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.18-4m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.18-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.18-2m)
- full rebuild for mo7 release

* Sat Jul 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.18-1m)
- update to 2.1.18

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.17-1m)
- update to 2.1.17

* Wed Jan  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.16-1m)
- update to 2.1.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.15-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.15-3m)
- import 8 patches from Fedora
- update nv.xinf

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.15-2m)
- rebuild against xorg-x11-server-1.7.1

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.15-1m)
- update to 2.1.15

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.14-1m)
- update to 2.1.14

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.12-3m)
- rebuild against xorg-x11-server 1.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.12-2m)
- rebuild against rpm-4.6

* Fri Aug 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.12-1m)
- update to 2.1.12

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.10-1m)
- update to 2.1.10

* Sat May 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.9-1m)
- update to 2.1.9

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.8-3m)
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.8-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.8-1m)
- update 2.1.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.7-2m)
- %%NoSource -> NoSource

* Mon Jan 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.7-1m)
- update 2.1.7

* Fri Jan 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.6-2m)
- support GeForce8800 GT

* Wed Oct 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.6-1m)
- update 2.1.6

* Tue Oct 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.5-1m)
- update 2.1.5

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.3-2m)
- rebuild against xorg-x11-server-1.4

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Mon Jul 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Wed Jul  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1
-- support geforce8400

* Thu Jun 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sat Apr 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Wed Mar 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Wed Mar 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2.1-1m)
- update to 1.2.2.1

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-1m)
- update 1.2.1

* Sun Jul  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0
-- PCI IDs for GeForce 6 and 7 variants.
-- new chip support

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-2m)
- delete duplicated dir

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1.5-4m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.5-3.1m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1.5-3.1
- bump again for double-long bug on ppc(64)

* Thu Feb 09 2006 Mike A. Harris <mharris@redhat.com> 1.0.1.5-3
- Added nv-1.0.1.5-updateto-cvs20050209.patch to sync driver with CVS and pick
  up support for newer Nvidia chips, and RandR rotation support.  (#180101)

* Thu Feb 09 2006 Mike A. Harris <mharris@redhat.com> 1.0.1.5-2
- Syncronized nv.xinf with nv_driver.c PCI ID list, including 10DE:0092 and
  10DE:00F2 for bug (#179997).

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> 1.0.1.5-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1.5-1
- Updated xorg-x11-drv-nv to version 1.0.1.5 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.0.1.4-1
- Updated xorg-x11-drv-nv to version 1.0.1.4 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.1.2-1
- Updated xorg-x11-drv-nv to version 1.0.1.2 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.1.1-1
- Updated xorg-x11-drv-nv to version 1.0.1.1 from X11R7 RC1
- Fix *.la file removal.
- Add riva128.so subdriver to file manifest.

* Mon Oct 3 2005 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Limit "ExclusiveArch" to x86, x86_64, ia64, ppc

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.0.1-0
- Initial spec file for nv video driver generated automatically
  by my xorg-driverspecgen script.
