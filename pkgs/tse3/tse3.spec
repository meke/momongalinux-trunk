%global momorel 14

Summary: Trax Sequencer Engine
Name: tse3
Version: 0.3.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://tse3.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: %{name}-%{version}-gcc4.patch
Patch1: %{name}_alsa1.x_and_sustain.patch
Patch2: %{name}-fix-compile-amd64.patch
Patch3: %{name}-%{version}-libtool.patch
Patch4: %{name}-%{version}-parallelmake.patch
Patch5: %{name}-%{version}_gcc43_fixes.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: audiofile-devel
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: esound-devel
BuildRequires: glib2-devel
BuildRequires: jack-devel
BuildRequires: libmad
BuildRequires: libogg-devel
BuildRequires: libtool
BuildRequires: libvorbis-devel

%description
TSE3 is a powerful open source sequencer engine written in C++. It is
a 'sequencer engine' because it provides the actual driving force
elements of a sequencer but provides no form of fancy interface.
Sequencer applications or multimedia presentation packages will
incorporate the TSE3 libraries to provide a user with MIDI sequencing
facilities.

%package devel
Summary: Header files and static libraries from tse3
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on TSE3.

%prep
%setup -q

%patch0 -p0 -b .gcc4
%patch2 -p0 -b .fix_compile_amd64
%patch3 -p0 -b .libtool
%patch4 -p0 -b .parallelmake
%patch5 -p0 -b gcc43

autoreconf -fi

%build
%configure --with-alsa --without-oss
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

# for doc
rm -rf package-doc
mv %{buildroot}%{_prefix}/doc package-doc

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING ChangeLog INSTALL NEWS README THANKS TODO
%doc package-doc/%{name}-%{version}/*
%{_bindir}/tse3play
%{_libdir}/libtse3.so.*
%{_mandir}/man1/tse3play.1*

%files devel
%defattr(-,root,root)
%{_includedir}/tse3
%{_libdir}/libtse3.so
%{_mandir}/man3/tse3.3*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-14m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-9m)
- rebuild against rpm-4.6

* Fri Apr  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-8m)
- import gcc43_fixes.patch from opensuse
 +* Tue Nov 13 2007 mrueckert@suse.de
 +- added tse3-0.3.1_gcc43_fixes.patch:
 +  mostly missing includes

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-7m)
- rebuild against gcc43

* Sat Feb 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-6m)
- build without kdemultimedia and aRts support

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-5m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-4m)
- rebuild against libvorbis-1.2.0-1m

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-3m)
- build without OSS support

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-2m)
- import new gcc4.patch and libtool.patch from opensuse
- import tse3-0.3.1-parallelmake.patch from gentoo
- add kdemultimedia-header-path.patch
- remove alsa1.x_and_sustain.patch

* Tue Mar  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-1m)
- initial package for kguitar-0.5
- import and modify tse3_alsa1.x_and_sustain.patch.bz2 from cooker
 +* Tue Jan 18 2005 Abel Cheung <deaddog@mandrake.org> 0.2.7-6mdk
 +- P0: Patch from noteedit website, supposed to fix alsa 1.0 detection and
 +  problem with sustain pedal
- import tse3-fix-compile-amd64.patch.bz2 from cooker
 +* Mon May 09 2005 Nicolas Lecureuil <neoclust@mandriva.org> 0.2.7-7mdk
 +- Patch2: Fix build for amd64 ( gentoo )
- add gcc4.patch
- Summary and %%description are imported from cooker
