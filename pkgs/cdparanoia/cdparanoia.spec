%global momorel 5

Summary: A Compact Disc Digital Audio (CDDA) extraction tool
Name: cdparanoia
Version: 10.2
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2
Group: Applications/Multimedia
URL: http://www.xiph.org/paranoia/
Source: http://downloads.xiph.org/releases/%{name}/%{name}-III-%{version}.src.tgz
NoSource: 0
Patch0: %{name}-%{version}-#463009.patch
Patch1: %{name}-%{version}-endian.patch
Patch2: %{name}-%{version}-install.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
Requires(post): libselinux-utils
Obsoletes: cdparanoia-III

%description
Cdparanoia (Paranoia III) reads digital audio directly from a CD, then
writes the data to a file or pipe in WAV, AIFC or raw 16 bit linear
PCM format.  Cdparanoia's strength lies in its ability to handle a
variety of hardware, including inexpensive drives prone to
misalignment, frame jitter and loss of streaming during atomic reads.
Cdparanoia is also good at reading and repairing data from damaged
CDs.

%package libs
Summary: Libraries for libcdda_paranoia (Paranoia III)
License: LGPLv2
Group: Applications/Multimedia

%description libs
The cdparanoia-libs package contains the dynamic libraries needed for
applications which read CD Digital Audio disks.

%package devel
Summary: A Compact Disc Digital Audio (CDDA) extraction tool. lib & header
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
A Compact Disc Digital Audio (CDDA) extraction tool. lib & header.

%prep
%setup -q -n %{name}-III-%{version}

%patch0 -p3 -b .#463009
%patch1 -p1 -b .endian
%patch2 -p1 -b .install

%build
%configure
%make  

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig
if test -f %{_sbindir}/getenforce ; then
  result=$(%{_sbindir}/getenforce)
  if test "x${result}" != "xDisabled" ; then
    chcon -t textrel_shlib_t %{_libdir}/libcdda_paranoia.so.0
  fi
fi
%postun libs
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING* README
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*

%files libs
%defattr(-,root,root)
%{_libdir}/libcdda_interface.so.*
%{_libdir}/libcdda_paranoia.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/cdda_interface.h
%{_includedir}/cdda_paranoia.h
%{_includedir}/utils.h
%{_libdir}/libcdda_interface.a
%{_libdir}/libcdda_interface.so
%{_libdir}/libcdda_paranoia.a
%{_libdir}/libcdda_paranoia.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (10.2-3m)
- add Requires(post): libselinux-utils

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (10.2-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (10.2-1m)
- version 10.2 STABLE RELEASE
- split package libs
- import 3 patches from Fedora
- clean up old patches

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (alpha9.8-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (alpha9.8-10m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (alpha9.8-9m)
- rebuild against gcc43

* Wed Jun 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (alpha9.8-8m)
- merge FC patches
- add %%post and %%postun
- remove gcc34.patch

* Sun Feb  6 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (alpha9.8-7m)
- enable x86_64.

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (alpha9.8-6m)
- add Patch0: cdparanoia-III-alpha9.8-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Wed Nov  5 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (alpha9.8-5m)
- use %%{momorel}

* Fri Jun 22 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (alpha9.8-2k)
  update to alpha9.8

* Fri Dec  1 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (alpha9.7-10k)
- modified to use %{_mandir} macro

* Tue Aug 22 2000 Kenichi Matsubara <m@kondara.org>
- Change PATH (FHS)

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-cdparanoia-20000115

* Sun Nov 28 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-cdparanoia-19991115

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Aug 20 1999 Preston Brown <pbrown@redhat.com>
- updated to 9.6, and added to base distribution
- changed name to just cdparanoia, obsoleting old -III suffix

* Sat Jul 10 1999 Tim Powers <timp@redhat.com>
- updated package to cdparanoia-III-alpha9.5

* Mon Apr 12 1999 Michael Maher <mike@redhat.com>
- updated pacakge

* Tue Oct 06 1998 Michael Maher <mike@redhat.com>
- updated package

* Mon Jun 29 1998 Michael Maher <mike@redhat.com>
- built package
