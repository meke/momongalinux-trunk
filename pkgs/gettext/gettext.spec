%global momorel 1
%bcond_with jar
%bcond_with java
%bcond_without check
%bcond_without git

Summary: GNU libraries and utilities for producing multi-lingual messages
Name: gettext
Version: 0.18.3.1
Release: %{momorel}m%{?dist}
License: GPLv3+ and LGPLv2+
Group: Development/Tools
URL: http://www.gnu.org/software/gettext/
Source: ftp://ftp.gnu.org/gnu/gettext/%{name}-%{version}.tar.gz
NoSource: 0
Source2: msghack.py
# removal of openmp.m4
BuildRequires: autoconf >= 2.62
BuildRequires: automake
BuildRequires: libtool, bison, gcc-c++
%if %{with java}
# libintl.jar requires gcj >= 4.3 to build
BuildRequires: gcc-java, libgcj
# For javadoc
BuildRequires: java-1.6.0-openjdk-devel
%if %{with jar}
BuildRequires: %{_bindir}/fastjar
# require zip and unzip for brp-java-repack-jars
BuildRequires: zip, unzip
%endif
%endif
# for po-mode.el
BuildRequires: emacs
%if %{with git}
# for autopoint:
BuildRequires: git
%endif
BuildRequires: chrpath
# following suggested by DEPENDENCIES:
BuildRequires: ncurses-devel
BuildRequires: expat-devel
BuildRequires: libxml2-devel
BuildRequires: glib2-devel
BuildRequires: libcroco-devel
BuildRequires: libunistring-devel
Requires(post): info
Requires(preun): info
# for F17 UsrMove
#Conflicts: filesystem < 3
Provides: /bin/gettext
# exception for bundled gnulib copylib
Provides: bundled(gnulib)

Obsoletes: %{name}-sharp

%description
The GNU gettext package provides a set of tools and documentation for
producing multi-lingual messages in programs. Tools include a set of
conventions about how programs should be written to support message
catalogs, a directory and file naming organization for the message
catalogs, a runtime library which supports the retrieval of translated
messages, and stand-alone programs for handling the translatable and
the already translated strings. Gettext provides an easy to use
library and tools for creating, using, and modifying natural language
catalogs and is a powerful and simple method for internationalizing
programs.


%package common-devel
Summary: Common development files for %{name}
Group: Development/Tools
# autopoint archive
License: GPLv3+
BuildArch: noarch

%description common-devel
This package contains common architecture independent gettext development files.


%package devel
Summary: Development files for %{name}
Group: Development/Tools
# autopoint is GPLv3+
# libasprintf is LGPLv2+
# libgettextpo is GPLv3+
License: LGPLv2+ and GPLv3+
Requires: %{name} = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}
Requires: %{name}-common-devel = %{version}-%{release}
Requires(post): info
Requires(preun): info
# for autopoint
Requires: git
Obsoletes: gettext-autopoint < 0.18.1.1-3
Provides: gettext-autopoint = %{version}-%{release}

%description devel
This package contains all development related files necessary for
developing or compiling applications/libraries that needs
internationalization capability. You also need this package if you
want to add gettext support for your project.


%package libs
Summary: Libraries for %{name}
Group: System Environment/Libraries
# libasprintf is LGPLv2+
# libgettextpo is GPLv3+
License: LGPLv2+ and GPLv3+

%description libs
This package contains libraries used internationalization support.


%package -n emacs-%{name}
Summary: Support for editing po files within GNU Emacs
Group: Applications/Editors
BuildArch: noarch
# help users find po-mode.el
Provides: emacs-po-mode
Requires: emacs(bin) >= %{_emacs_version}

%description -n emacs-%{name}
This package provides a major mode for editing po files within GNU Emacs.


%package -n emacs-%{name}-el
Summary: Elisp source files for editing po files within GNU Emacs
Group: Applications/Editors
BuildArch: noarch
Requires: emacs-%{name} = %{version}-%{release}

%description -n emacs-%{name}-el
This package contains the Elisp source files for editing po files within GNU
Emacs.


%prep
%setup -q


%build
%if %{with java}
export JAVAC=gcj
%if %{with jar}
export JAR=fastjar
%endif
%endif
# --disable-rpath doesn't work properly on lib64
%configure --without-included-gettext --enable-nls --disable-static \
    --enable-shared --with-pic --disable-csharp --disable-rpath \
%if %{with java}
    --enable-java \
%else
    --disable-java --disable-native-java \
%endif
%if %{without git}
    --disable-git \
%endif
%{nil}

make %{?_smp_mflags} %{?with_java:GCJFLAGS="-findirect-dispatch"}


%install
make install DESTDIR=${RPM_BUILD_ROOT} INSTALL="%{__install} -p" \
    lispdir=%{_datadir}/emacs/site-lisp/gettext \
    aclocaldir=%{_datadir}/aclocal EXAMPLESFILES=""


install -pm 755 %SOURCE2 ${RPM_BUILD_ROOT}/%{_bindir}/msghack


# make preloadable_libintl.so executable
chmod 755 ${RPM_BUILD_ROOT}%{_libdir}/preloadable_libintl.so

rm -f ${RPM_BUILD_ROOT}%{_infodir}/dir

# doc relocations
for i in gettext-runtime/man/*.html; do
  rm ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext/`basename $i`
done
rm -r ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext/javadoc*

rm -rf ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext/examples

rm -rf htmldoc
mkdir htmldoc
mv ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext/* ${RPM_BUILD_ROOT}/%{_datadir}/doc/libasprintf/* htmldoc
rm -r ${RPM_BUILD_ROOT}%{_datadir}/doc/libasprintf
rm -r ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext

## note libintl.jar does not build with gcj < 4.3
## since it would not be fully portable
%if %{with jar}
### this is no longer needed since examples not packaged
## set timestamp of examples ChangeLog timestamp for brp-java-repack-jars
#for i in `find ${RPM_BUILD_ROOT} examples -newer ChangeLog -type f -name ChangeLog`; do
#  touch -r ChangeLog  $i
#done
%else
# in case another java compiler is installed
rm -f ${RPM_BUILD_ROOT}%{_datadir}/%{name}/libintl.jar
%endif

rm -f ${RPM_BUILD_ROOT}%{_datadir}/%{name}/gettext.jar

# remove .la files
rm ${RPM_BUILD_ROOT}%{_libdir}/lib*.la

# remove internal .so lib files
rm ${RPM_BUILD_ROOT}%{_libdir}/libgettext{src,lib}.so

# move po-mode initialization elisp file to the right place, and remove byte
# compiled file
install -d ${RPM_BUILD_ROOT}%{_emacs_sitestartdir}
mv ${RPM_BUILD_ROOT}%{_emacs_sitelispdir}/%{name}/start-po.el ${RPM_BUILD_ROOT}%{_emacs_sitestartdir}
rm ${RPM_BUILD_ROOT}%{_emacs_sitelispdir}/%{name}/start-po.elc

%find_lang %{name}-runtime
%find_lang %{name}-tools
cat %{name}-*.lang > %{name}.lang

# cleanup rpaths
for i in $RPM_BUILD_ROOT%{_bindir}/* `find $RPM_BUILD_ROOT%{_libdir} -type f`; do
  if file $i | grep "ELF 64-bit" >/dev/null; then
     chrpath -l $i && chrpath --delete $i
  fi
done


%if %{with check}
%check
# this takes quite a lot of time to run
make check
%endif


%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/gettext.info.bz2 %{_infodir}/dir || :


%preun
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/gettext.info.bz2 %{_infodir}/dir || :
fi


%postun -p /sbin/ldconfig


%post devel
/sbin/ldconfig
/sbin/install-info %{_infodir}/autosprintf.info %{_infodir}/dir || :


%preun devel
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/autosprintf.info %{_infodir}/dir || :
fi


%postun devel -p /sbin/ldconfig

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS gettext-runtime/BUGS
%doc COPYING gettext-tools/misc/DISCLAIM README
%doc NEWS THANKS 
%doc gettext-runtime/man/*.1.html
%doc gettext-runtime/intl/COPYING*
%{_bindir}/*
%exclude %{_bindir}/autopoint
%{_infodir}/gettext*
%exclude %{_mandir}/man1/autopoint.1*
%{_mandir}/man1/*
%{_libdir}/%{name}
%if %{with java}
%exclude %{_libdir}/%{name}/gnu.gettext.*
%endif
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/ABOUT-NLS
%{_datadir}/%{name}/intl
%{_datadir}/%{name}/po
%{_datadir}/%{name}/styles

%files common-devel
%defattr(-,root,root,-)
%{_datadir}/%{name}/archive.*.tar.*

%files devel
%defattr(-,root,root,-)
%doc gettext-runtime/man/*.3.html ChangeLog
%{_bindir}/autopoint
%{_datadir}/%{name}/projects/
%{_datadir}/%{name}/config.rpath
%{_datadir}/%{name}/*.h
%{_datadir}/%{name}/msgunfmt.tcl
%{_datadir}/aclocal/*
%{_includedir}/*
%{_infodir}/autosprintf*
%{_libdir}/libasprintf.so
%{_libdir}/libgettextpo.so
%{_libdir}/preloadable_libintl.so
%{_mandir}/man1/autopoint.1*
%{_mandir}/man3/*
%{_datadir}/%{name}/javaversion.class
%doc gettext-runtime/intl-java/javadoc*
%if %{with java}
%{_libdir}/%{name}/gnu.gettext.*
%endif

%files libs
%defattr(-,root,root,-)
%{_libdir}/libasprintf.so.*
%{_libdir}/libgettextpo.so.*
%{_libdir}/libgettextlib-0.*.so
%{_libdir}/libgettextsrc-0.*.so
%if %{with jar}
%{_datadir}/%{name}/libintl.jar
%endif

%files -n emacs-%{name}
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/%{name}
%{_emacs_sitelispdir}/%{name}/*.elc
%{_emacs_sitestartdir}/*.el

%files -n emacs-%{name}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{name}/*.el

%changelog
* Thu Aug 22 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18.3.1-1m)
- update to 0.18.3.1

* Wed Mar  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18.2-1m)
- update to 0.18.2

* Sat Jun 30 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18.1.1-9m)
- set Obsoletes: gettext-sharp

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.1.1-8m)
- reimport from fedora

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.18.1.1-7m)
- update msghack.py

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.1.1-6m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.18.1.1-5m)
- add BR git

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18.1.1-3m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18.1.1-2m)
- split off libs subpackage
- --disable-static

* Sat Jul  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.1.1-1m)
- update to 0.18.1.1

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17-13m)
- add patch7 for build fix with autoconf-2.65

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-12m)
- use BuildRequires and Requires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-11m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-9m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-8m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-7m)
- update Patch2 for fuzz=0
- License: GPLv3+ and LGPLv2+

* Fri Nov 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-6m)
- fix install-info

* Sun Jun  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17-5m)
- delete Requires: cvs from gettext (needed by gettext-devel)

* Tue Apr 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.17-4m)
- release directories provided by new filesystem

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-2m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17-1m)
- update to 0.17
- add patch4
-- gettext-0.17 delete gl_AC_TYPE_LONG_LONG macro
-- but use in gettex-0.17/gettext-runtime/libasprintf/configure.ac

* Thu Oct 18 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.1-2m)
- removed unused patches
- added a bug fix patch for gcj.m4
- added -findirect-dispatch for gcj to use the binary compatibility ABI

* Sat Apr 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16.1-1m)
- update 0.16.1

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-9m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.15-8m)
- disable mono for ppc64, alpha, sparc64

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-7m)
- delete another conflict files (fix %%files)

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-6m)
- delete conflict files (fix %%files)

* Sat Oct 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-5m)
- use autoconf-2.60, automake-1.10

* Mon Oct  9 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-4m)
- rebuild against libgcj-4.1.1-19m.

* Sun Sep 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-3m)
- delete libintl.jar (good-by java-sun-j2se1.5-sdk)

* Sat Sep 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-2m)
- add BuildPrereq: java-sun-j2se1.5-sdk

* Wed Sep 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15-1m)
- update to 0.15
-- add patch2 (does not use dlopen for expat)
-- add patch3 (for autoconf-2.59, doc_DATA)

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.5-5m)
- add gettext-sharp package

* Sun May 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.5-5m)
- java -> devel subpackage

* Sun Feb 19 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.14.5-4m)
- rebuild against gcc-4.1

* Tue Jan 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.5-3m)
- --disable-csharp

* Mon Jan  2 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.14.5-2m)
- rebuild against gcc-java-4.1.0

* Sun Dec 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.5-1m)
- update 0.14.5

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.14.1-3m)
- build against python-2.4.2

* Wed Dec  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.14.1-2m)
- make gettext-java sub-package

* Tue Nov 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.14.1-1m)
- updated to 1.14.1

* Mon Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (0.12.1-3m)
- too much removed. sumaso.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.12.1-2m)
- revised spec for enabling rpm 4.2.

* Wed Nov 05 2003 Kenta MURATA <muraken2@nifty.com>
- (0.12.1-1m)
- version up.

* Mon Apr 28 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.11.5-1m)
- update to 0.11.5
- import patch from rawhide(gettext-0.11.4-7)
   Patch11: gettext-0.11.4-gettextize.patch
- disable patch4
- remove Patch12: gettext-0.10.35-charset.patch which is included in 1.11.5
- remove Patch13: gettext-0.10.40-autoconf253.patch which is included in 1.11.5
- include man files

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.40-15m)
- add URL tag

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.10.40-14m)
- add patch for autoconf-2.53

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.10.40-12k)
- cancel gcc-3.1 autoconf-2.53

* Fri May 24 2002 Toru Hoshina <t@kondara.org>
- (0.10.40-10k)
- autoconf 2.53...

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.10.40-8k)
- /sbin/install-info -> info in PreReq.

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.10.40-6k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.10.40-4k)
- automake autoconf 1.5

* Tue Oct 16 2001 Motonobu Ichimura <famao@kondara.org>
- (0.10.40-3k)
- up to 0.10.40

* Wed Aug 22 2001 KUSUNOKI Masanori <nori@kondara.org>
- update to 0.10.39
- rebuild against gcc-3.0.1

* Tue May 22 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- gettext-0.10.35.tar.bz2 -> gettext-0.10.35.tar.gz

* Wed Oct 25 2000 Daiki Matsuda <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Fri Aug 04 2000 Akira Higuchi <a@kondara.org>
- msgfmt: output "" entry even if it is fuzzy
- removed iconv patch which is not needed for glibc-2.2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat May 20 2000 Akira Higuchi <a@kondara.org>
- apply iconv if needed

* Thu Apr 06 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.10.35-17).

* Sun Mar 12 2000 Hidetomo Machi <mcHT@kondara.org>
- add "PreReq: /sbin/install-info"
- add %doc
- po-mode.el is under /usr/doc/gettext-version/

* Sun Feb 27 2000 Cristian Gafton <gafton@redhat.com>
- add --comments to msghack

* Thu Feb 10 2000 Cristian Gafton <gafton@redhat.com>
- fix bug #9240 - gettextize has the right aclocal patch

* Wed Jan 12 2000 Cristian Gafton <gafton@redhat.com>
- add the --diff and --dummy options

* Sat Jan  1 2000 Akira Higuchi <a@kondara.org>
- merged rawhide updates
- removed gettext-0.10.35-message2 patch. added nowrap patch

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Oct 06 1999 Cristian Gafton <gafton@redhat.com>
- add the --missing option to msghack

* Wed Sep 22 1999 Cristian Gafton <gafton@redhat.com>
- updated msghack not to merge in fuzzies in the master catalogs

* Sat Sep 18 1999 Norihito Ohmori <ohmori@flatout.org>
- update ja.po

-* Tue Sep 14 1999 Jun Nishii <jun@flatout.org>
- use message.c.diff-2

* Thu Aug 26 1999 Cristian Gafton <gafton@redhat.com>
- updated msghack to understand --append

* Wed Aug 11 1999 Cristian Gafton <gafton@redhat.com>
- updated msghack to correctly deal with sorting files

* Tue Jun 29 1999 Norihito Ohmori <ohmori@flatout.org>
- added multibyte patch for msgmerge

* Sun Jun 27 1999 Norihito Ohmori <ohmori@flatout.org>
- ja locale support from JaPO Project

* Thu May 06 1999 Cristian Gafton <gafton@redhat.com>
- msghack updates

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Mon Mar 08 1999 Cristian Gafton <gafton@redhat.com>
- added patch for misc hacks to facilitate rpm translations

* Thu Dec 03 1998 Cristian Gafton <gafton@redhat.com>
- patch to allow to build on ARM

* Wed Sep 30 1998 Jeff Johnson <jbj@redhat.com>
- add Emacs po-mode.el files.

* Sun Sep 13 1998 Cristian Gafton <gafton@redhat.com>
- include the aclocal support files

* Fri Sep  3 1998 Bill Nottingham <notting@redhat.com>
- remove devel package (functionality is in glibc)

* Tue Sep  1 1998 Jeff Johnson <jbj@redhat.com>
- update to 0.10.35.

* Mon Jun 29 1998 Jeff Johnson <jbj@redhat.com>
- add gettextize.
- create devel package for libintl.a and libgettext.h.

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sun Nov 02 1997 Cristian Gafton <gafton@redhat.com>
- added info handling
- added misc-patch (skip emacs-lisp modofications)

* Sat Nov 01 1997 Erik Troan <ewt@redhat.com>
- removed locale.aliases as we get it from glibc now
- uses a buildroot

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- Built against glibc
