%global momorel 4

Summary: CPU Frequency changing related utilities
Name: cpufrequtils
Version: 008
Release: %{momorel}m%{?dist}
Group: System Environment/Base
License: GPLv2
URL: http://www.kernel.org/pub/linux/utils/kernel/cpufreq/cpufrequtils.html
Source0: http://www.kernel.org/pub/linux/utils/kernel/cpufreq/%{name}-%{version}.tar.bz2
Patch0: disable-gsic.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext
BuildRequires: libsysfs-devel
# pulls in automake and autoconf
BuildRequires: libtool
ExclusiveArch: %{ix86} x86_64 ppc ppc64 ppc64iseries
Provides: cpufreq-utils = %{version}-%{release}
Obsoletes: cpufreq-utils < %{version}-%{release}

%description
cpufrequtils contains several utilities that can be used to control
the cpufreq interface provided by the kernel on hardware that
supports CPU frequency scaling.

%prep
%setup -q

%patch0 -p1

%build
make CFLAGS="%{optflags}"

%ifarch %{ix86}
cd debug/i386
make CFLAGS="%{optflags}"
%endif

%ifarch x86_64
cd debug/x86_64
make CFLAGS="%{optflags}"
%endif

cd ..

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,' \
mandir=%{_mandir} bindir=%{_bindir} includedir=%{_includedir} libdir=%{_libdir}

# get rid of la file
rm -rf %{buildroot}%{_libdir}/libcpufreq.la

%ifarch %{ix86}
cd debug/i386
install centrino-decode %{buildroot}%{_bindir}/centrino-decode
install dump_psb %{buildroot}%{_bindir}/dump_psb
install powernow-k8-decode %{buildroot}%{_bindir}/powernow-k8-decode
cd ../..
%endif

%ifarch x86_64
cd debug/x86_64
install powernow-k8-decode %{buildroot}%{_bindir}/powernow-k8-decode
cd ../..
%endif

chmod -R a-s %{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,0755)
%doc AUTHORS COPYING README
%{_includedir}/cpufreq.h
%{_libdir}/libcpufreq.so*
%{_bindir}/cpufreq-aperf
%{_bindir}/cpufreq-info
%{_bindir}/cpufreq-set
%ifarch %{ix86}
%{_bindir}/centrino-decode
%{_bindir}/dump_psb
%endif
%ifarch %{ix86} x86_64
%{_bindir}/powernow-k8-decode
%endif
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/cpufreq-info.1*
%{_mandir}/man1/cpufreq-set.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (008-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (008-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (008-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (008-1m)
- version 008

* Sun Apr 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (007-1m)
- version 007

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (006-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Nov  9 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (006-1m)
- version 006
- remove Patch1: cpufrequtils-fix-parallel-build-of-ccdv.patch

* Sun Oct 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (005-1m)
- version 005
- rename package from cpufreq-utils to cpufrequtils
- import fix-parallel-build-of-ccdv.patch from Fedora
- add %%doc
- update %%description
- License: GPLv2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (004-2m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (004-1m)
- update 004

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (002-2m)
- rebuild against gcc43

* Mon Nov  6 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (002-1m)
- merge fc cpufreq-utils-002-1.1.41.fc6
- Upstream 002 release.
- rebuilid against lybsysfs-devel
- buildrequire libsysfs-devel

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-4m)
- change autoreconf to each autotools

* Mon Oct  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-3m)
- add patch2 (for mkinstalldirs)

* Sun May 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-2m)
- add patch1 for build (configure.in is too old)
- add transform='s,x,x,' to make install

* Sat May 20 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4-1m)
- import from fc

* Mon Dec 19 2005 Dave Jones <davej@redhat.com>
- New upstream 0.4 release.

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Sun Jul 31 2005 Florian La Roche <laroche@redhat.com>
- package all files

* Mon May  9 2005 Dave Jones <davej@redhat.com>
- Update to upstream 0.3

* Fri Apr 22 2005 Matthias Saou <http://freshrpms.net/> 0.2-2
- Major spec file cleanup. (#155731)
- Use %%find_lang macro.
- Add missing sysfsutils-devel build requirement.

* Fri Apr 15 2005 Florian La Roche <laroche@redhat.com>
- remove empty preun script

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4.

* Sun Feb 27 2005 Dave Jones <davej@redhat.com>
- Update to upstream 0.2

* Tue Feb  8 2005 Dave Jones <davej@redhat.com>
- Rebuild with -D_FORTIFY_SOURCE=2

* Sat Dec  4 2004 Dave Jones <davej@redhat.com>
- Initial packaging

