%global momorel 12

%global xfce4ver 4.10.0
%global major 1.9

Name:		xfce4-quicklauncher-plugin
Version:	1.9.4
Release:	%{momorel}m%{?dist}
Summary:	Quicklauncher plugin for the Xfce panel
Group:		User Interface/Desktops
License:	GPL
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
# http://bugzilla.xfce.org/show_bug.cgi?id=7015
Patch1:         %{name}-1.9.4-xfce4-panel-4.7.patch
# make new apps appear on the correct X screen
# http://bugzilla.xfce.org/show_bug.cgi?id=4323
Patch2:         %{name}-1.9.4-fix-multiscreen.patch
# apply settings without restart
# http://bugzilla.xfce.org/show_bug.cgi?id=3782
Patch3:         %{name}-1.9.4-save-settings.patch
# plugin speaks french if no translations are found
# http://bugzilla.xfce.org/show_bug.cgi?id=3783
# contains all translations from 
# http://translations.xfce.org/projects/p/xfce4-quicklauncher-plugin/c/master/ 
# as of 2010-12-19
Patch4:         %{name}-1.9.4-update-translations.patch
# Xfce 4.6: xfce-setting-show instead of xfce4-settings-manager
# http://bugzilla.xfce.org/show_bug.cgi?id=5752
Patch5:         %{name}-1.9.4-xfce4-settings-manager.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext, perl-XML-Parser
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	libxml2-devel
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
This plugin allows you to have lots of launchers in the Xfce panel, displaying 
them on several lines.

%prep
%setup -q
%patch1 -p1 -b .xfce4-panel-4.7
%patch2 -p0 -b .multiscreen
%patch3 -p1 -b .save-settings
%patch4 -p1 -b .update-translations
%patch5 -p0 -b .xfce46

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'
%find_lang %{name}

%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog TODO
%{_libdir}/xfce4/panel/plugins/*.so
%{_datadir}/xfce4/panel-plugins/*.desktop


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.9.4-12m)
- rebuild against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-11m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.4-10m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.4-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.4-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.4-7m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-6m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-4m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-2m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-1m)
- update
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.2-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.2-1m)
- import to Momonga from fc

* Sun Jan 28 2007 Christoph Wickert <fedora christoph-wickert de> - 1.9.2-2
- Rebuild for Xfce 4.4.

* Thu Oct 05 2006 Christoph Wickert <fedora christoph-wickert de> - 1.9.2-1
- Update to 1.9.2.

* Wed Sep 13 2006 Christoph Wickert <fedora christoph-wickert de> - 1.9.1-2
- Rebuild for XFCE 4.3.99.1.
- BR perl(XML::Parser).

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 1.9.1-1
- Update to 1.9.1 on XFCE 4.3.90.2.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 0.81-5
- Mass rebuild for Fedora Core 6.

* Tue Apr 11 2006 Christoph Wickert <fedora wickert at arcor de> - 0.81-4
- Require xfce4-panel.

* Thu Feb 16 2006 Christoph Wickert <fedora wickert at arcor de> - 0.81-3
- Rebuild for Fedora Extras 5.

* Thu Dec 01 2005 Christoph Wickert <fedora wickert at arcor de> - 0.81-2
- Add libxfcegui4-devel BuildReqs.
- Fix %%defattr.

* Fri Nov 18 2005 Christoph Wickert <fedora wickert at arcor de> - 0.81-1
- Initial Fedora Extras release.
