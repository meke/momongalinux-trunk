%global momorel 16

%global realname fftw

# RPM spec file for FFTW.
# This file is used to build Redhat Package Manager packages for the
# FFTW library.  Such packages make it easy to install and uninstall
# the library and related files from binaries or source.
#
# This spec file is for version 2.1.2 of FFTW, and will need to be
# modified for future releases.  First, the string "2.1.2" should
# be replaced everywhere in this file with the new version number.
# Second, the shared library version numbers (in the %files list)
# will need to be updated.  Any other changes in the installed files
# list, build commands, etcetera will of course also require changes.
#
# The icon associated with this package can be downloaded from:
#     http://theory.lcs.mit.edu/~fftw/fftw-logo-thumb.gif
# and will need to be placed in the SOURCES directory to build the RPM.
#
Name: fftw2
Summary: C subroutines for computing the Discrete Fourier Transform.
Version: 2.1.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Libraries
Source0: ftp://ftp.fftw.org/pub/fftw/%{realname}-%{version}.tar.gz
NoSource: 0
Patch0: fftw-2.1.5-config_h.patch
URL: http://www.fftw.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info

%description
FFTW is a collection of fast C routines for computing the Discrete
Fourier Transform in one or more dimensions.  It includes complex,
real, and parallel transforms, and can handle arbitrary array sizes
efficiently.  This package includes both the double- and
single-precision FFTW uniprocessors and the threads libraries.

%package devel
Summary: Development files for the FFTW Discrete Fourier Transform library.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the additional header files, documentation, and
libraries you need to develop programs using the FFTW fast fourier
transform library.

%prep
%setup -q -n %{realname}-%{version}
%patch0 -p1 -b .config_h~
mkdir html
cp -p doc/*.{html,gif} html

%build
%define config_args --enable-shared --enable-threads --srcdir=..
%ifarch %{ix86} athlon
%{expand: %%define config_args %config_args --enable-i386-hacks}
%endif

### undefine optflags because configure knows best CFLAGS
CFLAGS=""

mkdir build_dir_double 
pushd build_dir_double
ln -fs ../configure .
%configure %{config_args}
%make
popd

# also compile/install single-precision version:
mkdir build_dir_single
pushd build_dir_single
ln -fs ../configure .
%configure %{config_args} --enable-type-prefix --enable-float
%make
popd

%install
rm -rf %{buildroot}

for dir in build_dir_double build_dir_single; do
    pushd $dir 
    %makeinstall
    popd
done

find %{buildroot} -name "*.la" -delete
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
ldconfig
if [ -x /sbin/install-info ]; then
    /sbin/install-info --section="Math" --entry='* FFTW: (fftw).		C subroutines for computing the Discrete Fourier Transform.' %{_infodir}/fftw.info %{_infodir}/dir
fi

%preun
if [ $1 = 0 -a -x /sbin/install-info ]; then
    /sbin/install-info --delete %{_infodir}/fftw.info %{_infodir}/dir
fi

%postun
ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING COPYRIGHT ChangeLog FAQ/fftw-faq.html NEWS README* TODO html
%{_libdir}/lib*.so.*
%{_infodir}/*

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/lib*.a
%{_libdir}/lib*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.5-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.5-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.5-14m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.5-13m)
- use Requires

* Tue Dec 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.5-12m)
- revise for new %%configure macro

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.5-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.5-10m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.5-9m)
- release %%{_infodir}/dir conflicts with info package

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.5-8m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Wed Apr 23 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1.5-7m)
- rename: fftw3 -> fftw, fftw -> fftw2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.5-6m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.5-5m)
- delete libtool library

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.5-4m)
- revised spec for enabling rpm 4.2.

* Sun Jul 20 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (2.1.5-3m)
- make use of new %%configure and %%make
- use pushd/popd instead of invoking subshell

* Sun Jul 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.5-2m)
- revise spec

* Wed Mar 26 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (2.1.5-1m)
- update to 2.1.5

* Wed Oct 16 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.1.3-2m)
- add fftw-2.1.3-srcdir.patch to build successfully.
  (reported by yAmanOji (devel.ja:00590). thanx.)

* Fri Oct  4 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.1.3-1m)
- initial revision (from redhat 6.2 powertools)
- (After cvs added this spec, I realized that there existed 
   Kondara version of fftw in the past. I'll merge it later.)
