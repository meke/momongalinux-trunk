%global momorel 1

Summary: X.Org X11 libXpm runtime library
Name: libXpm
Version: 3.5.11
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: xorg-x11-proto-devel
BuildRequires: libX11-devel
# FIXME: Although ./configure checks for libXt and libXext, and indicates
# that they are missing, it continues to build anyway, and just does not
# build sxpm if they are not present.  Therefore, libXt-devel and
# libXext-devel are required build deps in order to get sxpm built.
BuildRequires: libXt-devel
BuildRequires: libXext-devel
BuildRequires: libXau-devel

%description
X.Org X11 libXpm runtime library

%package devel
Summary: X.Org X11 libXpm development package
Group: Development/Libraries

Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3
Requires: libX11-devel

%description devel
X.Org X11 libXpm development package

%prep
%setup -q

# Disable static library creation by default.
%define with_static 0

%build
%configure \
%if ! %{with_static}
	--disable-static
%endif
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# We intentionally don't ship *.la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README INSTALL ChangeLog
%{_libdir}/libXpm.so.4
%{_libdir}/libXpm.so.4.11.0

%files devel
%defattr(-,root,root,-)
%defattr(-,root,root,-)
%{_bindir}/cxpm
%{_bindir}/sxpm
%{_includedir}/X11/xpm.h
%if %{with_static}
%{_libdir}/libXpm.a
%endif
%{_libdir}/libXpm.so
%{_libdir}/pkgconfig/xpm.pc
%{_mandir}/man1/*.1*

%changelog
* Mon Sep 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.11-1m)
- update to 3.5.11

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.10-1m)
- update to 3.5.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.9-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.9-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.9-1m)
- update to 3.5.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.8-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.8-1m)
- update to 3.5.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.7-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.7-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.7-2m)
- %%NoSource -> NoSource

* Thu Aug 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.7-1m)
- update to 3.5.7

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.6-1m)
- update 3.5.6

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.5-1m)
- update 3.5.5

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4.2-3m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4.2-2.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.4.2-2.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 3.5.4.2-2.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 3.5.4.2-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan 23 2006 Mike A. Harris <mharris@redhat.com> 3.5.4.2-2
- Bumped and rebuilt

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 3.5.4.2-1
- Updated libXpm to version 3.5.4.2 from X11R7 RC4

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 3.5.4.1-1
- Updated libXpm to version 3.5.4.1 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.
- Added "Requires: libX11-devel" to devel subpackage as xpm.h includes various
  X headers. (#174163)
- Added missing build dep libXau-devel

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 3.5.4-1
- Updated libXpm to version 3.5.4 from X11R7 RC2
- Changed 'Conflicts: XFree86-devel, xorg-x11-devel' to 'Obsoletes'
- Changed 'Conflicts: XFree86-libs, xorg-x11-libs' to 'Obsoletes'


* Mon Oct 24 2005 Mike A. Harris <mharris@redhat.com> 3.5.3-1
- Updated libXpm to version 3.5.3 from X11R7 RC1
- Added manpages for sxpm, cxpm

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 3.5.2-5
- Renamed package to remove xorg-x11 from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro
- Fix BuildRequires to use new style X library package names

* Sun Sep 4 2005 Mike A. Harris <mharris@redhat.com> 3.5.2-4
- Although ./configure checks for libXt and libXext, and indicates
  that they are missing, it continues to build anyway, and just does not
  build sxpm if they are not present.  Therefore, libXt-devel and
  libXext-devel are required build deps in order to get sxpm built.
- Added missing defattr to devel subpackage.

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 3.5.2-3
- Changed all virtual BuildRequires to the "xorg-x11-" prefixed non-virtual
  package names, as we want xorg-x11 libs to explicitly build against
  X.Org supplied libs, rather than "any implementation", which is what the
  virtual provides is intended for.

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 3.5.2-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added missing sxpm binary to devel subpackage
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 3.5.2-1
- Initial build.
