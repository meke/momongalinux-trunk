%global momorel 2
%global realname opengfx
#global prever   alpha6

Name:           openttd-opengfx
Version:        0.4.6.1
Release: %{momorel}m%{?dist}
Summary:        OpenGFX replacement graphics for OpenTTD

Group:          Amusements/Games
License:        GPLv2
URL:            http://dev.openttdcoop.org/projects/opengfx
Source0:        http://bundles.openttdcoop.org/opengfx/releases/%{version}%{?prever:-%{prever}}/%{realname}-%{version}%{?prever:-%{prever}}-source.tar.xz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  gimp grfcodec nml
Requires:       openttd


%description
OpenGFX is an open source graphics base set for OpenTTD which can completely
replace the TTD base set. The main goal of OpenGFX therefore is to provide a
set of free base graphics which make it possible to play OpenTTD without
requiring the (copyrighted) files from the TTD CD. This potentially increases
the OpenTTD fan base and makes it a true free game (with "free" as in both
"free beer" and "free speech").

As of version 0.2.0 OpenGFX has a full set of sprites. Future versions will aim
to improve the graphics. 


%prep
%setup -q -n %{realname}-%{version}%{?prever:-%{prever}}-source 

cat >> Makefile.local << EOF
INSTALL_DIR=%{buildroot}%{_datadir}/games/openttd/data
DO_NOT_INSTALL_DOCS = 1
DO_NOT_INSTALL_LICENSE = 1
DO_NOT_INSTALL_CHANGELOG = 1
EOF


%build
make clean-gfx
make 

%install
rm -rf %{buildroot}
make install

%check
make check


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc docs/
%{_datadir}/games/openttd/data/*


%changelog
* Sat Apr 20 2013 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.4.6.1-2m)
- fix up file paths

* Sat Apr 20 2013 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.4.6.1-1m)
- update to 0.4.6.1

* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Sat Dec 10 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.7-1m)
- update to 0.3.7

* Thu Jun 23 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.4-1m)
- update to 0.3.4

* Tue Jun 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-3m)
- fix up %%prep section to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-2m)
- rebuild for new GCC 4.6

* Thu Mar 10 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2
- change source URI, NoSource: 0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-2m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.4-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.4-1m)
- update to 0.2.4

* Sun Feb 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.2.1-2m)
- No NoSource: 0

* Sun Feb 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.2.1-1m)
- Import from Fedora
- update to 0.2.1

* Sun Aug 23 2009 Felix Kaechele <heffer@fedoraproject.org> - 0.1.0-0.1.alpha6

- new upstream release

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-0.5.alpha4.2

- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu May 28 2009 Felix Kaechele <heffer@fedoraproject.org> - 0-0.4.alpha4.2

- added md5 check

* Tue Apr 14 2009 Felix Kaechele <heffer@fedoraproject.org> - 0-0.3.alpha4.2

- now compiles from source

* Sun Mar 29 2009 Felix Kaechele <heffer@fedoraproject.org> - 0-0.2.alpha4.2

- improved macro usage
- touch sample.cat

* Sat Mar 21 2009 Felix Kaechele <heffer@fedoraproject.org> - 0-0.1.alpha4.2

- initial build
