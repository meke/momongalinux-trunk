%global momorel 8

Summary: Open Sound Control protocol
Name: liblo
Version: 0.24
Release: %{momorel}m%{?dist}
License: GPL
URL: http://plugin.org.uk/liblo/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen
BuildRequires: pkgconfig

%description
LibLO is an implementation of the Open Sound Control protocol for POSIX
systems, started by Steve Harris.

%package devel
Summary: Header files and static libraries from liblo
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on liblo.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/liblo.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog INSTALL README TODO
%{_libdir}/liblo.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/lo
%{_libdir}/pkgconfig/liblo.pc
%{_libdir}/liblo.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.24-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.24-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.24-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.24-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.24-2m)
- %%NoSource -> NoSource

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.24-1m)
- initial package for ardour-2.0
- Summary and %%description are imported from cooker
