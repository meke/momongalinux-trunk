%global momorel 5

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: A collection of XML-related technologies for Python
Name: python-4Suite-XML
Version: 1.0.2
Release: %{momorel}m%{?dist}

Group: Development/Libraries
License: "ASL 1.1"
URL: http://foursuite.sourceforge.net/
Source0: http://downloads.sourceforge.net/foursuite/4Suite-XML-%{version}.tar.bz2
NoSource: 0
# https://sourceforge.net/tracker/?func=detail&aid=2891269&group_id=39954&atid=428294
Patch0: python-4Suite-XML-1.0.2-expat-dos.patch
# https://sourceforge.net/tracker/?func=detail&aid=2959726&group_id=39954&atid=428294
Patch1: python-4Suite-XML-1.0.2-tests-as.patch
BuildRequires: python-devel >= 2.7
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%filter_provides_in %{python_sitearch}/Ft/.*\.so$
%filter_setup

%description
4Suite-XML is a suite of Python modules for XML and RDF processing.
Its major components include the following:

 * Ft.Xml.Domlette: A very fast, lightweight XPath-oriented DOM.
 * Ft.Xml.XPath: An XPath 1.0 implementation for Domlette documents.
 * Ft.Xml.Xslt: A robust XSLT 1.0 processor.
 * Ft.Lib: Various support libraries that can be used independently.

%prep
%setup -q -n 4Suite-XML-%{version}
%patch0 -p1 -b .expat-dos
%patch1 -p1 -b .tests-as

%build
export CFLAGS="$RPM_OPT_FLAGS"
python setup.py config --system --pythonlibdir=%{python_sitearch} \
	--bindir=%{_bindir} --datadir=%{_datadir}/4Suite \
	--sysconfdir=%{_sysconfdir}/4Suite \
	--localstatedir=%{_localstatedir}/lib/4Suite \
	--libdir=%{_libdir}/4Suite --docdir=%{_datadir}/doc/4Suite \
	--localedir=%{_datadir}/locale --mandir=%{_mandir}
python setup.py build

%install
python setup.py install --root=$RPM_BUILD_ROOT

chmod a-x $RPM_BUILD_ROOT%{_datadir}/4Suite/Data/Stylesheets/*.xslt

# Move documentation to the right place
mv $RPM_BUILD_ROOT%{_datadir}/doc/4Suite/html html
rm $RPM_BUILD_ROOT%{_datadir}/doc/4Suite/{COPYRIGHT,README}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYRIGHT README html
%{_bindir}/4*
%exclude %{_libdir}/4Suite
%{python_sitearch}/Ft
%{python_sitearch}/4Suite_XML-*.egg-info
%{_datadir}/4Suite

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-1m)
- import for opendnssec from rawhide

* Thu Mar  4 2010 Miloslav Trmač <mitr@redhat.com> - 1.0.2-10
- Filter out bogus Provides:
- Remove no longer necessary references to BuildRoot:
- s/%%define/%%global/g

* Fri Feb 26 2010 Miloslav Trmač <mitr@redhat.com> - 1.0.2-9
- Don't use "as" identifier in the test suite
  Resolves: #564872
- Point URL: to something that doesn't fail with 503
- Update source URL

* Tue Nov  3 2009 Miloslav Trmač <mitr@redhat.com> - 1.0.2-8
- Fix an expat DoS
  Related: #531697

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.0.2-5
- Rebuild for Python 2.6

* Fri Aug 29 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.0.2-4
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.2-3
- Autorebuild for GCC 4.3

* Fri Jan 11 2008 Miloslav Trmač <mitr@redhat.com> - 1.0.2-2
- Ship Python egg information

* Tue Feb 27 2007 Miloslav Trmac <mitr@redhat.com> - 1.0.2-1
- Update to 4Suite-XML-1.0.2

* Tue Jan  9 2007 Miloslav Trmac <mitr@redhat.com> - 1.0.1-2
- Drop the .egg-info file

* Wed Dec 13 2006 Miloslav Trmac <mitr@redhat.com> - 1.0.1-1
- Update to 4Suite-XML-1.0.1

* Sat Dec  9 2006 Miloslav Trmac <mitr@redhat.com> - 1.0-2
- Rebuild with Python 2.5

* Sun Nov  5 2006 Miloslav Trmac <mitr@redhat.com> - 1.0-1
- Update to 4Suite-XML-1.0

* Sun Sep 10 2006 Miloslav Trmac <mitr@redhat.com> - 1.0-0.5.b3
- Rebuild for Fedora Extras 6

* Mon Feb 20 2006 Miloslav Trmac <mitr@redhat.com> - 1.0-0.4.b3
- Rebuild for Fedora Extras 5

* Fri Jan  6 2006 Miloslav Trmac <mitr@redhat.com> - 1.0-0.3.b3
- Fix Summary:
- Use an URL in Source0:

* Thu Jan  5 2006 Miloslav Trmac <mitr@redhat.com> - 1.0-0.2.b3
- Require python-abi
- Ship .pyo files
- Use macros instead of hardcoded paths

* Tue Dec 13 2005 Miloslav Trmac <mitr@redhat.com> - 1.0-0.1.b3
- Update to 4Suite-XML-1.0b3

* Mon Nov  7 2005 Miloslav Trmac <mitr@redhat.com> - 1.0-0.1.b2
- Initial package, based on Fedora Core 4Suite-1.0-9.b1.
