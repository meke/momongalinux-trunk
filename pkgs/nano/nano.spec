%global momorel 1

Summary: A small text editor
Name: nano
Version: 2.3.4
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Editors
URL: http://www.nano-editor.org/
Source0: http://www.nano-editor.org/dist/v2.3/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel autoconf gettext groff
Requires: ncurses
Requires(post): info
Requires(preun): info

%description
GNU nano is a small and friendly text editor.

%prep
%setup -q

for f in doc/man/fr/{nano.1,nanorc.5,rnano.1}; do
    iconv -f iso-8859-1 -t utf-8 -o $f.tmp $f && mv $f.tmp $f
    touch $f.html
done

%build
%configure --enable-all
%make

%install
rm -rf %{buildroot}
make install DESTDIR="%{buildroot}"

rm -f %{buildroot}%{_infodir}/dir

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir
 
%preun
/sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS BUGS COPYING* ChangeLog* INSTALL NEWS README THANKS TODO 
%doc doc/nanorc.sample
%doc doc/faq.html
%{_bindir}/*
%{_mandir}/man*/*
%{_mandir}/fr/man*/*
%{_infodir}/nano.info*
%{_datadir}/nano
%{_datadir}/doc/nano

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.4-1m)
- update to 2.3.4

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.0-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.6-2m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.6-1m)
- update to 2.2.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.4-3m)
- full rebuild for mo7 release

* Tue May 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.4-2m)
- cleanup this spec

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.4-1m)
- [SECURITY] CVE-2010-1160 CVE-2010-1161
- update to 2.2.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.6-5m)
- apply glibc210 patch from Rawhide (2.0.6-7)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.6-2m)
- %%NoSource -> NoSource

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-1m)
- update to 2.0.6

* Tue Nov 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.0-1m)
- initial import to Momonga
- based on the package in Fedora Core (1.3.12-1.1)

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.3.12-1.1
- rebuild

* Mon Jul 10 2006 David Woodhouse <dwmw2@redhat.com> - 1.3.12-1
- Update to 1.3.12

* Tue May 16 2006 David Woodhouse <dwmw2@redhat.com> - 1.3.11-1
- Update to 1.3.11
- BuildRequires: groff (#191946)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.3.8-1.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.3.8-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Sep 5 2005 David Woodhouse <dwmw2@redhat.com> 1.3.8-1
- 1.3.8

* Wed Mar 2 2005 David Woodhouse <dwmw2@redhat.com> 1.3.5-0.20050302
- Update to post-1.3.5 CVS tree to get UTF-8 support.

* Wed Aug 04 2004 David Woodhouse <dwmw2@redhat.com> 1.2.4-1
- 1.2.4

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Apr 02 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- 1.2.3

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Aug 11 2003 Bill Nottingham <notting@redhat.com> 1.2.1-4
- build in different environment

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue May  6 2003 Bill Nottingham <notting@redhat.com> 1.2.1-2
- description tweaks

* Mon May  5 2003 Bill Nottingham <notting@redhat.com> 1.2.1-1
- initial build, tweak upstream spec file
