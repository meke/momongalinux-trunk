%global momorel 4

Name:           fuse-convmvfs
Version:        0.2.5
Release:        %{momorel}m%{?dist}
Summary:        FUSE-Filesystem to convert filesystem encodings

Group:          System Environment/Base
License:        GPLv2
URL:            http://fuse-convmvfs.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  fuse-devel >= 2.5.0

%description
This is a filesystem client use the FUSE(Filesystem in
USErspace) interface to convert file name from one charset
to another. Inspired by convmv.

%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_bindir}/convmvfs
%doc README* COPYING ChangeLog AUTHORS NEWS


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-1m)
- update to 0.2.5

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.4-1m)
- import from Fedora

* Thu Sep 17 2009 Peter Lemenkov <lemenkov@gmail.com> - 0.2.4-10
- Rebuilt with new fuse

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.4-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.4-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jul 18 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.2.4-7
- fix license tag

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.2.4-6
- Autorebuild for GCC 4.3

* Fri Jan 18 2008 ZC Miao <hellwolf.misty@gmail.com> - 0.2.4-5
- rebuild

* Tue Aug  7 2007 ZC Miao <hellwolf.misty@gmail.com> - 0.2.4-4
- increase release ver to fix EVR problems

* Fri Jun  1 2007 ZC Miao <hellwolf.misty@gmail.com> - 0.2.4-1
- update to 0.2.4

* Mon Feb 12 2007 ZC Miao <hellwolf.misty@gmail.com> - 0.2.3-2
- add doc AUTHORS NEWS, remove doc INSTALL
- Change URL

* Mon Feb 12 2007 ZC Miao <hellwolf.misty@gmail.com> - 0.2.3-1
- update to 0.2.3

* Fri Jul 28 2006 ZC Miao <hellwolf@seu.edu.cn> - 0.2.2-1
- update to 0.2.2

* Tue Jul 25 2006 ZC Miao <hellwolf@seu.edu.cn> - 0.2.1-1
- update to 0.2.1

* Mon Jul  3 2006 ZC Miao <hellwolf@seu.edu.cn> - 0.2-2
- add URL for Source0

* Fri Jun 30 2006 ZC Miao <hellwolf@seu.edu.cn> - 0.2-1
- init build
