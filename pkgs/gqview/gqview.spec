%global momorel 11

Summary: Graphics file browser utility
Name: gqview
Version: 2.0.4
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://gqview.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ImageMagick
BuildRequires: coreutils
BuildRequires: gtk2-devel
BuildRequires: perl

%description
GQview is a browser for graphics files.
Offering single click viewing of your graphics files.
Includes thumbnail view, zoom and filtering features.
And external editor support.

%prep
%setup -q

%patch0 -p1 -b .fix-menu

%build
%configure LIBS="-lm"
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48}/apps
convert -scale 16x16 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 22x22 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
convert -scale 32x32 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
install -m 644 %{name}.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-9m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-8m)
- fix build

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-7m)
- fix up desktop file

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.4-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.4-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-2m)
- %%NoSource -> NoSource

* Wed Dec  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-1m)
- version 2.0.4

* Thu Nov  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-1m)
- version 2.0.3

* Mon Oct 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- version 2.0.2

* Sun Jun 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-1m)
- version 2.0.1

* Sat Apr 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1m)
- version 2.0.0

* Sat Oct 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-1m)
- version 1.4.5

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.0-2m)
- revised spec for enabling rpm 4.2.

* Sun Feb 22 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.0-1m)
- version 1.4.0

* Mon Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.3.5-1m)
- update to 1.3.5

* Fri Oct 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.2-1m)
- version 1.3.2

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.1-2m)
- rebuild against for XFree86-4.3.0

* Thu Mar  6 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (1.3.1-1m)
- version 1.3.1

* Sat Aug 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.2-2m)
- change Source0 URI

* Thu Aug 08 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (1.0.2-1m)
- Version 1.0.2
- zh_CN.GB2312 => zh_CN

* Sun Dec 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.9.1-8k)
- zh_TW.Big5 => zh_TW

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.9.1-6k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (0.9.1-4k)
- rebuild against libpng 1.2.0.

* Sun Nov 19 2000 AYUHANA Tomonori <l@kondara.org>
- (0.9.1-1k)
- version 0.9.1
- remove gqview-0.7.0e-ja.po
- add New docs to %doc

* Tue Jun 20 2000 AYUHANA Tomonori <l@kondara.org>
- merge RawHide (0.8.1-1)
- version 0.8.2
- SPEC fixed ( URL )

* Sun Jan 16 2000 Shingo Aakagaki <dora@kondara.org>
- version 0.7.0e

* Fri Jan 14 1999 AYUHANA Tomonori <l@kondara.org>
- enable to install in Kondara MNU/Linux
