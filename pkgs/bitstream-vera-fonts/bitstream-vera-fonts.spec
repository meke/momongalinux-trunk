%global momorel 14

%global fontname    bitstream-vera
%global archivename ttf-bitstream-vera

%global common_desc \
The Vera fonts are high-quality Latin fonts donated by Bitstream. \
These fonts have been released under a liberal license, see the  \
licensing FAQ in COPYRIGHT.TXT or the online up-to-date version \
at %{url} for details.

Name:    %{fontname}-fonts
Version: 1.10
Release: %{momorel}m%{?dist}
Summary: Bitstream Vera fonts

Group:     User Interface/X
License:   "Bitstream Vera"
URL:       http://www.gnome.org/fonts/
Source0:   ftp://ftp.gnome.org/pub/GNOME/sources/%{archivename}/%{version}/%{archivename}-%{version}.tar.bz2
NoSource:  0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:     noarch
BuildRequires: fontpackages-devel

%description
%common_desc


%package common
Summary:   Common files of the Bitstream Vera font set
Requires:  fontpackages-filesystem
Obsoletes: %{name}-compat < 1.10-11m

%description common
%common_desc

This package consists of files used by other %{name} packages.


%package -n %{fontname}-sans-fonts
Summary:  Variable-width sans-serif Bitstream Vera fonts
Requires: %{name}-common = %{version}-%{release}

Obsoletes: %{name}-sans < 1.10-13

%description -n %{fontname}-sans-fonts
%common_desc

This package consists of the Bitstream Vera sans-serif variable-width font
faces.

%_font_pkg -n sans Vera.ttf VeraBd.ttf VeraIt.ttf VeraBI.ttf


%package -n %{fontname}-serif-fonts
Summary:  Variable-width serif Bitstream Vera fonts
Requires: %{name}-common = %{version}-%{release}

Obsoletes: %{name}-serif < 1.10-13

%description -n %{fontname}-serif-fonts
%common_desc

This package consists of the Bitstream Vera serif variable-width font faces.

%_font_pkg -n serif VeraSe*ttf


%package -n %{fontname}-sans-mono-fonts
Summary:  Monospace sans-serif Bitstream Vera fonts
Requires: %{name}-common = %{version}-%{release}

Obsoletes: %{name}-sans-mono < 1.10-13

%description -n %{fontname}-sans-mono-fonts
%common_desc

This package consists of the Bitstream Vera sans-serif monospace font faces.

%_font_pkg -n sans-mono VeraMo*ttf


%prep
%setup -q -n %{archivename}-%{version}

%build

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}


%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc *.TXT


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10-12m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-11m)
- sync with Fedora 13 (1.10-18)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-9m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-8m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-7m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10-6m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10-4m)
- rebuild against gcc43

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10-3m)
- change fontdir to %%{_datadir}/fonts/bitstream-vera

* Sun Apr  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-2m)
- modify install dir

* Sun Apr  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-1m)
- import from fc

* Fri Sep 08 2006 Behdad Esfahbod <besfahbo@redhat.com> - 1.10-7
- s/latin/Latin/ in package description (#205693)

* Fri Jul 14 2006 Behdad Esfahbod <besfahbo@redhat.com> - 1.10-6
- remove ghost file fonts.cache-1 as fontconfig uses out of tree
  cache files now.

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.10-5.1.1
- rebuild

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Sat Jan 08 2005 Florian La Roche <laroche@redhat.com>
- rebuilt to get rid of legacy selinux filecontexts

* Sun May 30 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- change post/postun scripts

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jun 10 2003 Owen Taylor <otaylor@redhat.com> 1.10-1
- Base package on spec file from Nicolas Mailhot <Nicolas.Mailhot at laPoste.net>
- Cleanups from Warren Togami and Nicolas Mailhot
