%global momorel 14


Summary: sary is a suffix array library.
Name: sary
Version: 1.2.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
# Source0: http://sary.sourceforge.net/sary-%{version}.tar.gz
Source0: http://sary.sourceforge.net/sary-%{version}.tar.gz 
NoSource: 0
URL: http://sary.namazu.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: glib2 >= 2.0.0
BuildRequires: glib2-devel >= 2.0.0

%description
Sary is a suffix array library.  It provides fast full-text
search facilities for huge, say, 10 MB, 100 MB text files.

%package devel
Summary: Libraries and include files of sary
Group: Development/Libraries
Requires: %name = %{version}-%{release}

%description devel
Libraries and include files of sary.

%prep 
%setup -q 

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete
%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS ChangeLog COPYING NEWS README TODO
%{_bindir}/sary
%{_bindir}/mksary
%{_datadir}/sary/docs/*
%{_libdir}/libsary.so.*
%{_mandir}/*/*

%files devel
%defattr(-, root, root)
#%{_bindir}/sary-config
%{_includedir}/sary.h
%{_includedir}/sary
%{_libdir}/libsary.so
%{_libdir}/libsary.a
%{_libdir}/pkgconfig/sary.pc
#%{_datadir}/aclocal/sary.m4

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-14m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-11m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-10m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-9m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-7m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-4m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-3m)
- delete libtool library

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-2m)
- delete own /usr/share/man/man1 directory

* Fri Apr 15 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0.1m)
- update to 1.2.0
- change Source0 URI
- change %setup

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.0-0.20040613.1m)
- update to latest version imported from cvs tree

* Sun Jun 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-2m)
- change Source0 URI

* Sun Sep 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.4-1m)

* Fri Feb 22 2002 Toshiro Hikita <toshi@sodan.org>
- (1.0.3-2k)
- Imported from sary-1.0.3-1.src.rpm
- Kondarized

* Tue Dec 01 2000 Satoru Takabayashi <satoru@namazu.org>
- Fix %files for recent updates.

* Tue Nov 21 2000 Ryuji Abe <rug@namazu.org>
- Fix %files for recent updates.
- Fix URL.

* Mon Nov 06 2000 Satoru Takabayashi <satoru@namazu.org>
- Initial version.
