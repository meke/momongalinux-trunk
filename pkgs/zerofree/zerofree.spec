%global         momorel 1

Summary:        Utility to force unused ext2 inodes and blocks to zero
Name:           zerofree
Version:        1.0.3
Release:        %{momorel}m%{?dist}
License:        GPL+
Group:          System Environment/Libraries

Source0:        http://intgat.tigress.co.uk/rmy/uml/%{name}-%{version}.tgz
NoSource:	0
Source1:        http://intgat.tigress.co.uk/rmy/uml/sparsify.c
NoSource:	1
Source2:        http://intgat.tigress.co.uk/rmy/uml/index.html
#NoSource:	2

# zerofree.sgml is the source for the man page from Debian.
# Unfortunately we cannot build this in Fedora because of an apparent
# bug in our DocBook tools.  Therefore I also include the generated
# man page (generated on a Debian system from this source).
Source3:        zerofree.sgml
Source4:        zerofree.8

URL:            http://intgat.tigress.co.uk/rmy/uml/

BuildRequires:  e2fsprogs-devel

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
zerofree is a utility to set unused filesystem inodes and blocks of an
ext2 filesystem to zero.  This can improve the compressibility and
privacy of an ext2 filesystem.

This tool was inspired by the ext2fs privacy (i.e. secure deletion)
patch described in a Linux kernel mailing list thread.

WARNING: The filesystem to be processed should be unmounted or mounted
read-only.  The tool tries to check this before running, but you
should be careful.


%prep
%setup -q
cp -p %{SOURCE1} .
cp -p %{SOURCE2} .


%build
make CC="gcc $RPM_OPT_FLAGS"
gcc $RPM_OPT_FLAGS sparsify.c -o sparsify -lext2fs


%install
rm -rf %{buildroot}

install -D -p -m 755 zerofree %{buildroot}%{_sbindir}/zerofree
install -D -p -m 755 sparsify %{buildroot}%{_sbindir}/sparsify
install -D -p -m 644 %{SOURCE4} %{buildroot}%{_mandir}/man8/zerofree.8


%files
%defattr(-,root,root,-)
%doc COPYING index.html
%{_sbindir}/zerofree
%{_sbindir}/sparsify
%{_mandir}/man8/zerofree.8*


%clean
rm -rf %{buildroot}


%changelog
* Tue Sep  4 2012 ARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Tue Jan  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-3m)
- support e2fsprogs-1.42

* Mon Jul  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-2m)
- add index.html

* Sun Jul  3 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora 15

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu May 27 2010 Richard W.M. Jones <rjones@redhat.com> - 1.0.1-7
- Include zerofree(8) man page from Debian (RHBZ#596732).

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri May 15 2009 Richard W.M. Jones <rjones@redhat.com> - 1.0.1-5
- Include the index file as a source file.
- Improve the description, remove spelling mistakes and other typos.
- Use the upstream SRPM directly, unpacking source from it.
- Fix use of dist macro.
- Pass the RPM OPTFLAGS to C compiler (should also fix debuginfo pkg).
- Use 'cp -p' to preserve timestamps when copying index.html file.
- Fix the defattr line.
- License is GPL+ (any version of the GPL including 1).
- Use a simpler install command to install the binary.
- Fix the upstream URL to point to the real original project.
- Add the sparsify command.

* Thu May 14 2009 Richard W.M. Jones <rjones@redhat.com> - 1.0.1-1
- Initial packaging for Fedora, based on R P Herrold's package.

* Wed May 13 2009 R P Herrold <info@owlriver.com> - 1.0.1-1
- initial packaging
