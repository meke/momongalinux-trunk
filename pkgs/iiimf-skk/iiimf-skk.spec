%global momorel 23

Summary:     SKK Language Engine for IIIMF
Name:	     iiimf-skk
Version:     0.1.22.96
Release:     %{momorel}m%{?dist}
License:     MIT/X
Group:	     User Interface/X
URL:         http://www.momonga-linux.org/~famao/iiimf-skk/
Source0:     http://www.momonga-linux.org/~famao/%{name}-%{version}.tar.gz 
Patch0:      %{name}-%{version}-db48.patch
Patch1:      %{name}-%{version}-xml.patch
Patch2:      iiimf-skk-0.1.22.96-linking.patch
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:    glib2
Requires:    gtk2
Requires:    skkserv
Requires:    iiimf
Requires:    iiimf_conv
Requires:    iiimf-xiiimp
Requires:    libxml2
BuildRequires: autoconf
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: db4-devel >= 4.8
BuildRequires: libxml2-devel

%description
IIIMF-SKK is a Language Engine Module for IIIMF(Internet/Intranet Input Method Framework).
This module provides SKK(Simple Kana to Kanji conversion program, an input method of Japanese) 
like input method. 

%prep
%setup -q
%patch0 -p1 -b .db48
%patch1 -p1 -b .xml
%patch2 -p1 -b .linking

# for Patch0
autoreconf -fi

%build
%configure --enable-gtk2 LIBS="-lX11"

# DO NOT USE %%make, make -j4 doesn't work (iiimf-skk-0.1.22.96-9m)
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall xauxdir=%{buildroot}%{_libdir}/im/locale/ja/skk/xaux \
				leifdir=%{buildroot}%{_libdir}/im/leif plugindir=%{buildroot}%{_libdir}/%{name}/plugins

# Momonga Linux Specific
mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/xim.d
cat <<EOF >%{buildroot}%{_sysconfdir}/X11/xinit/xim.d/SKKLE
NAME="SKKLE/IIIMP"
IM_EXEC="env LC_ALL=ja_JP.eucJP /usr/lib/im/httx -if skk -lc_basic_locale ja_JP.eucJP -xim htt_xbe"
XMODIFIERS=@im=htt
HTT_DISABLE_STATUS_WINDOW=t
HTT_GENERATES_KANAKEY=t
HTT_USES_LINUX_XKEYSYM=t
GTK_IM_MODULE=xim
QT_IM_MODULE=xim
export XMODIFIERS HTT_DISABLE_STATUS_WINDOW HTT_GENERATES_KANAKEY \
HTT_USES_LINUX_XKEYSYM GTK_IM_MODULE QT_IM_MODULE

export IM_EXEC
EOF

install -D lib/all.config.xml %{buildroot}%{_datadir}/config-sample/iiimf-skk/config.xml.sample

%find_lang %{name}

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%{_sysconfdir}/X11/xinit/xim.d/SKKLE
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/plugins
%{_libdir}/im/locale/ja/skk
%{_libdir}/im/leif/skk.*
%{_datadir}/config-sample/iiimf-skk

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.22.96-23m)
- add source

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.22.96-22m)
- rebuild for glib 2.33.2

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.22.96-21m)
- regenerate patch for db-4.8.30

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.22.96-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.22.96-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.22.96-18m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.22.96-17m)
- fix build

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.22.96-16m)
- explicitly link libdl

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.22.96-15m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.22.96-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.22.96-13m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.22.96-12m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.22.96-11m)
- %%NoSource -> NoSource

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.22.96-10m)
- rebuild against db4-4.6.21

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.22.96-9m)
- revise configure to avoid requiring compat-db

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.22.96-8m)
- delete libtool library

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.22.96-7m)
- rebuild against db4-4.5.20-1m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.1.22.96-6m)
- rebuild against expat-2.0.0-1m

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.22.96-5m)
- rebuild against db4.3

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.22.96-4m)
- change xim.d/SKKLE (add QT_IM_MODULE)

* Fri Nov  5 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.1.22.96-3m)
- change xim.d/SKKLE (add GTK_IM_MODULE)

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.22.96-2m)
- rebuild against db4.2

* Sun May 03 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22.96-1m)
- version 0.1.22.96

* Sun May 03 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22.95-1m)
- version 0.1.22.95

* Fri Apr 23 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22.94-1m)
- version 0.1.22.94

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22.93-2m)
- change License

* Fri Apr 18 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22.93-1m)
- version 0.1.22.93

* Mon Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.22.91-2m)
- rebuild against for XFree86-4.3.0

* Thu Mar 13 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22.92-1m)
- version 0.1.22.92

* Mon Feb 23 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22.91-1m)
- version 0.1.22.91

* Sat Feb 15 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22.90-1m)
- version 0.1.22.90

* Thu Jan 02 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.22-1m)
- version 0.1.22

* Sun Nov 10 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.21-1m)
- version 0.1.21

* Fri Sep 13 2002 Motonobu Ichimura <famao@momonga-linux.org>
- first import to momonga

* Sun Mar 03 2002 Motonobu Ichimura <famao@kondara.org>
- first release for public
