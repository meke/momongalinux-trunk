%global momorel 1
%global vimversion vim74
%global vimsrcversion 7.4
%global patchlevel 347
%global _default_patch_fuzz 2

%global vimruntime %{_datadir}/vim/%{vimversion}

Summary: The VIM editor
Name: vim
Version: %{vimsrcversion}.%{patchlevel}
#Version: %{vimsrcversion}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
URL: http://www.vim.org/
Source0: ftp://ftp.vim.org/pub/vim/unix/vim-%{vimsrcversion}.tar.bz2
#Source1: ftp://ftp.vim.org/pub/vim/unreleased/unix/vim-%{vimsrcversion}-rt.tar.bz2
Source2: vimx.wmconfig
Source3: vimrc
Source4: README
Source5: gvimrc
Source10: gvim16.png
Source11: gvim32.png
Source12: gvim48.png
Source13: gvim64.png
Source14: gvim.desktop
Source100: euc-jp.ps
Source101: sjis.ps
Source102: euc-cn.ps
Source103: euc-kr.ps
Source104: big5.ps
Source200: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/MD5SUMS
Source250: https://vim.googlecode.com/hg/src/po/nl.po

# security fixes
Source300: ftp://ftp.vim.org/vol/2/vim/runtime/plugin/netrwPlugin.vim
Source301: ftp://ftp.vim.org/vol/2/vim/runtime/plugin/gzip.vim
Source302: ftp://ftp.vim.org/vol/2/vim/runtime/filetype.vim
Source303: ftp://ftp.vim.org/vol/2/vim/runtime/autoload/zip.vim
Source304: ftp://ftp.vim.org/vol/2/vim/runtime/autoload/tar.vim
Source305: ftp://ftp.vim.org/vol/2/vim/runtime/autoload/netrwFileHandlers.vim
Source306: ftp://ftp.vim.org/vol/2/vim/runtime/autoload/netrw.vim
Source307: ftp://ftp.vim.org/vol/2/vim/runtime/autoload/netrwSettings.vim

Patch1: vim72-vimnotvi.patch
Patch2: vim-5.6a-paths.patch
Patch3: vim-6.0-fixkeys.patch
#Patch4: vim-6.0-specsyntax.patch
Patch5: vim-6.0r-crv.patch
Patch6: vim-6.0z-menui18n.patch
Patch7: vim-6.0t-phphighlight.patch
# Patch8: vim-6.0v-lilo.patch
Patch10: vim-6.2-hardcopy-multibyte.patch
# for security --> Patch145 6.3.045
#Patch11: vim-6.3-modeline_command_execution_vulnerablity.patch

# look at not only %%define but %%global on searching a variable definition in .spec file
Patch12: vim-6.3-accept-global-variable-definition-in-spec.vim.patch


# momonga specific patches
Patch50: vim-6.3-fix-changelog-format-for-momonga.patch
Patch51: vim-6.3-spec_vim-momorel.patch
Patch52: vim-7.0-syntax-spec-momonga.patch

# vim official patches
# (bzipped ftp://ftp.vim.org/pub/vim/patches/7.4/* )
Patch0101: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.001
Patch0102: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.002
Patch0103: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.003
Patch0104: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.004
Patch0105: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.005
Patch0106: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.006
Patch0107: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.007
Patch0108: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.008
Patch0109: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.009
Patch0110: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.010
Patch0111: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.011
Patch0112: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.012
Patch0113: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.013
Patch0114: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.014
Patch0115: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.015
Patch0116: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.016
Patch0117: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.017
Patch0118: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.018
Patch0119: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.019
Patch0120: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.020
Patch0121: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.021
Patch0122: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.022
Patch0123: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.023
Patch0124: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.024
Patch0125: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.025
Patch0126: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.026
Patch0127: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.027
Patch0128: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.028
Patch0129: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.029
Patch0130: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.030
Patch0131: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.031
Patch0132: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.032
Patch0133: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.033
Patch0134: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.034
Patch0135: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.035
Patch0136: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.036
Patch0137: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.037
Patch0138: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.038
Patch0139: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.039
Patch0140: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.040
Patch0141: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.041
Patch0142: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.042
Patch0143: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.043
Patch0144: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.044
Patch0145: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.045
Patch0146: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.046
Patch0147: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.047
Patch0148: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.048
Patch0149: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.049
Patch0150: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.050
Patch0151: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.051
Patch0152: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.052
Patch0153: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.053
Patch0154: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.054
Patch0155: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.055
Patch0156: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.056
Patch0157: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.057
Patch0158: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.058
Patch0159: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.059
Patch0160: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.060
Patch0161: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.061
Patch0162: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.062
Patch0163: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.063
Patch0164: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.064
Patch0165: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.065
Patch0166: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.066
Patch0167: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.067
Patch0168: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.068
Patch0169: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.069
Patch0170: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.070
Patch0171: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.071
Patch0172: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.072
Patch0173: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.073
Patch0174: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.074
Patch0175: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.075
Patch0176: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.076
Patch0177: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.077
Patch0178: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.078
Patch0179: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.079
Patch0180: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.080
Patch0181: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.081
Patch0182: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.082
Patch0183: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.083
Patch0184: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.084
Patch0185: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.085
Patch0186: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.086
Patch0187: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.087
Patch0188: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.088
Patch0189: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.089
Patch0190: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.090
Patch0191: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.091
Patch0192: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.092
Patch0193: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.093
Patch0194: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.094
Patch0195: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.095
Patch0196: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.096
Patch0197: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.097
Patch0198: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.098
Patch0199: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.099
Patch0200: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.100
Patch0201: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.101
Patch0202: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.102
Patch0203: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.103
Patch0204: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.104
Patch0205: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.105
Patch0206: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.106
Patch0207: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.107
Patch0208: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.108
Patch0209: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.109
Patch0210: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.110
Patch0211: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.111
Patch0212: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.112
Patch0213: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.113
Patch0214: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.114
Patch0215: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.115
Patch0216: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.116
Patch0217: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.117
Patch0218: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.118
Patch0219: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.119
Patch0220: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.120
Patch0221: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.121
Patch0222: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.122
Patch0223: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.123
Patch0224: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.124
Patch0225: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.125
Patch0226: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.126
Patch0227: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.127
Patch0228: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.128
Patch0229: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.129
Patch0230: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.130
Patch0231: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.131
Patch0232: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.132
Patch0233: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.133
Patch0234: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.134
Patch0235: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.135
Patch0236: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.136
Patch0237: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.137
Patch0238: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.138
Patch0239: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.139
Patch0240: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.140
Patch0241: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.141
Patch0242: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.142
Patch0243: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.143
Patch0244: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.144
Patch0245: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.145
Patch0246: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.146
Patch0247: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.147
Patch0248: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.148
Patch0249: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.149
Patch0250: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.150
Patch0251: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.151
Patch0252: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.152
Patch0253: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.153
Patch0254: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.154
Patch0255: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.155
Patch0256: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.156
Patch0257: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.157
Patch0258: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.158
Patch0259: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.159
Patch0260: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.160
Patch0261: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.161
Patch0262: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.162
Patch0263: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.163
Patch0264: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.164
Patch0265: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.165
Patch0266: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.166
Patch0267: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.167
Patch0268: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.168
Patch0269: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.169
Patch0270: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.170
Patch0271: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.171
Patch0272: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.172
Patch0273: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.173
Patch0274: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.174
Patch0275: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.175
Patch0276: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.176
Patch0277: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.177
Patch0278: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.178
Patch0279: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.179
Patch0280: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.180
Patch0281: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.181
Patch0282: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.182
Patch0283: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.183
Patch0284: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.184
Patch0285: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.185
Patch0286: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.186
Patch0287: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.187
Patch0288: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.188
Patch0289: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.189
Patch0290: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.190
Patch0291: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.191
Patch0292: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.192
Patch0293: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.193
Patch0294: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.194
Patch0295: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.195
Patch0296: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.196
Patch0297: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.197
Patch0298: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.198
Patch0299: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.199
Patch0300: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.200
Patch0301: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.201
Patch0302: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.202
Patch0303: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.203
Patch0304: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.204
Patch0305: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.205
Patch0306: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.206
Patch0307: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.207
Patch0308: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.208
Patch0309: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.209
Patch0310: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.210
Patch0311: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.211
Patch0312: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.212
Patch0313: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.213
Patch0314: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.214
Patch0315: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.215
Patch0316: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.216
Patch0317: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.217
Patch0318: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.218
Patch0319: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.219
Patch0320: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.220
Patch0321: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.221
Patch0322: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.222
Patch0323: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.223
Patch0324: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.224
Patch0325: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.225
Patch0326: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.226
Patch0327: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.227
Patch0328: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.228
Patch0329: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.229
Patch0330: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.230
Patch0331: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.231
Patch0332: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.232
Patch0333: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.233
Patch0334: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.234
Patch0335: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.235
Patch0336: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.236
Patch0337: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.237
Patch0338: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.238
Patch0339: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.239
Patch0340: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.240
Patch0341: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.241
Patch0342: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.242
Patch0343: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.243
Patch0344: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.244
Patch0345: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.245
Patch0346: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.246
Patch0347: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.247
Patch0348: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.248
Patch0349: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.249
Patch0350: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.250
Patch0351: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.251
Patch0352: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.252
Patch0353: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.253
Patch0354: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.254
Patch0355: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.255
Patch0356: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.256
Patch0357: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.257
Patch0358: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.258
Patch0359: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.259
Patch0360: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.260
Patch0361: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.261
Patch0362: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.262
Patch0363: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.263
Patch0364: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.264
Patch0365: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.265
Patch0366: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.266
Patch0367: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.267
Patch0368: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.268
Patch0369: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.269
Patch0370: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.270
Patch0371: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.271
Patch0372: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.272
Patch0373: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.273
Patch0374: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.274
Patch0375: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.275
Patch0376: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.276
Patch0377: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.277
Patch0378: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.278
Patch0379: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.279
Patch0380: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.280
Patch0381: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.281
Patch0382: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.282
Patch0383: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.283
Patch0384: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.284
Patch0385: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.285
Patch0386: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.286
Patch0387: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.287
Patch0388: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.288
Patch0389: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.289
Patch0390: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.290
Patch0391: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.291
Patch0392: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.292
Patch0393: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.293
Patch0394: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.294
Patch0395: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.295
Patch0396: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.296
Patch0397: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.297
Patch0398: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.298
Patch0399: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.299
Patch0400: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.300
Patch0401: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.301
Patch0402: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.302
Patch0403: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.303
Patch0404: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.304
Patch0405: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.305
Patch0406: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.306
Patch0407: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.307
Patch0408: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.308
Patch0409: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.309
Patch0410: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.310
Patch0411: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.311
Patch0412: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.312
Patch0413: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.313
Patch0414: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.314
Patch0415: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.315
Patch0416: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.316
Patch0417: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.317
Patch0418: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.318
Patch0419: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.319
Patch0420: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.320
Patch0421: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.321
Patch0422: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.322
Patch0423: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.323
Patch0424: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.324
Patch0425: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.325
Patch0426: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.326
Patch0427: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.327
Patch0428: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.328
Patch0429: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.329
Patch0430: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.330
Patch0431: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.331
Patch0432: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.332
Patch0433: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.333
Patch0434: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.334
Patch0435: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.335
Patch0436: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.336
Patch0437: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.337
Patch0438: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.338
Patch0439: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.339
Patch0440: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.340
Patch0441: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.341
Patch0442: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.342
Patch0443: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.343
Patch0444: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.344
Patch0445: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.345
Patch0446: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.346
Patch0447: ftp://ftp.vim.org/pub/vim/patches/%{vimsrcversion}/%{vimsrcversion}.347

NoPatch: 0101
NoPatch: 0102
NoPatch: 0103
NoPatch: 0104
NoPatch: 0105
NoPatch: 0106
NoPatch: 0107
NoPatch: 0108
NoPatch: 0109
NoPatch: 0110
NoPatch: 0111
NoPatch: 0112
NoPatch: 0113
NoPatch: 0114
NoPatch: 0115
NoPatch: 0116
NoPatch: 0117
NoPatch: 0118
NoPatch: 0119
NoPatch: 0120
NoPatch: 0121
NoPatch: 0122
NoPatch: 0123
NoPatch: 0124
NoPatch: 0125
NoPatch: 0126
NoPatch: 0127
NoPatch: 0128
NoPatch: 0129
NoPatch: 0130
NoPatch: 0131
NoPatch: 0132
NoPatch: 0133
NoPatch: 0134
NoPatch: 0135
NoPatch: 0136
NoPatch: 0137
NoPatch: 0138
NoPatch: 0139
NoPatch: 0140
NoPatch: 0141
NoPatch: 0142
NoPatch: 0143
NoPatch: 0144
NoPatch: 0145
NoPatch: 0146
NoPatch: 0147
NoPatch: 0148
NoPatch: 0149
NoPatch: 0150
NoPatch: 0151
NoPatch: 0152
NoPatch: 0153
NoPatch: 0154
NoPatch: 0155
NoPatch: 0156
NoPatch: 0157
NoPatch: 0158
NoPatch: 0159
NoPatch: 0160
NoPatch: 0161
NoPatch: 0162
NoPatch: 0163
NoPatch: 0164
NoPatch: 0165
NoPatch: 0166
NoPatch: 0167
NoPatch: 0168
NoPatch: 0169
NoPatch: 0170
NoPatch: 0171
NoPatch: 0172
NoPatch: 0173
NoPatch: 0174
NoPatch: 0175
NoPatch: 0176
NoPatch: 0177
NoPatch: 0178
NoPatch: 0179
NoPatch: 0180
NoPatch: 0181
NoPatch: 0182
NoPatch: 0183
NoPatch: 0184
NoPatch: 0185
NoPatch: 0186
NoPatch: 0187
NoPatch: 0188
NoPatch: 0189
NoPatch: 0190
NoPatch: 0191
NoPatch: 0192
NoPatch: 0193
NoPatch: 0194
NoPatch: 0195
NoPatch: 0196
NoPatch: 0197
NoPatch: 0198
NoPatch: 0199
NoPatch: 0200
NoPatch: 0201
NoPatch: 0202
NoPatch: 0203
NoPatch: 0204
NoPatch: 0205
NoPatch: 0206
NoPatch: 0207
NoPatch: 0208
NoPatch: 0209
NoPatch: 0210
NoPatch: 0211
NoPatch: 0212
NoPatch: 0213
NoPatch: 0214
NoPatch: 0215
NoPatch: 0216
NoPatch: 0217
NoPatch: 0218
NoPatch: 0219
NoPatch: 0220
NoPatch: 0221
NoPatch: 0222
NoPatch: 0223
NoPatch: 0224
NoPatch: 0225
NoPatch: 0226
NoPatch: 0227
NoPatch: 0228
NoPatch: 0229
NoPatch: 0230
NoPatch: 0231
NoPatch: 0232
NoPatch: 0233
NoPatch: 0234
NoPatch: 0235
NoPatch: 0236
NoPatch: 0237
NoPatch: 0238
NoPatch: 0239
NoPatch: 0240
NoPatch: 0241
NoPatch: 0242
NoPatch: 0243
NoPatch: 0244
NoPatch: 0245
NoPatch: 0246
NoPatch: 0247
NoPatch: 0248
NoPatch: 0249
NoPatch: 0250
NoPatch: 0251
NoPatch: 0252
NoPatch: 0253
NoPatch: 0254
NoPatch: 0255
NoPatch: 0256
NoPatch: 0257
NoPatch: 0258
NoPatch: 0259
NoPatch: 0260
NoPatch: 0261
NoPatch: 0262
NoPatch: 0263
NoPatch: 0264
NoPatch: 0265
NoPatch: 0266
NoPatch: 0267
NoPatch: 0268
NoPatch: 0269
NoPatch: 0270
NoPatch: 0271
NoPatch: 0272
NoPatch: 0273
NoPatch: 0274
NoPatch: 0275
NoPatch: 0276
NoPatch: 0277
NoPatch: 0278
NoPatch: 0279
NoPatch: 0280
NoPatch: 0281
NoPatch: 0282
NoPatch: 0283
NoPatch: 0284
NoPatch: 0285
NoPatch: 0286
NoPatch: 0287
NoPatch: 0288
NoPatch: 0289
NoPatch: 0290
NoPatch: 0291
NoPatch: 0292
NoPatch: 0293
NoPatch: 0294
NoPatch: 0295
NoPatch: 0296
NoPatch: 0297
NoPatch: 0298
NoPatch: 0299
NoPatch: 0300
NoPatch: 0301
NoPatch: 0302
NoPatch: 0303
NoPatch: 0304
NoPatch: 0305
NoPatch: 0306
NoPatch: 0307
NoPatch: 0308
NoPatch: 0309
NoPatch: 0310
NoPatch: 0311
NoPatch: 0312
NoPatch: 0313
NoPatch: 0314
NoPatch: 0315
NoPatch: 0316
NoPatch: 0317
NoPatch: 0318
NoPatch: 0319
NoPatch: 0320
NoPatch: 0321
NoPatch: 0322
NoPatch: 0323
NoPatch: 0324
NoPatch: 0325
NoPatch: 0326
NoPatch: 0327
NoPatch: 0328
NoPatch: 0329
NoPatch: 0330
NoPatch: 0331
NoPatch: 0332
NoPatch: 0333
NoPatch: 0334
NoPatch: 0335
NoPatch: 0336
NoPatch: 0337
NoPatch: 0338
NoPatch: 0339
NoPatch: 0340
NoPatch: 0341
NoPatch: 0342
NoPatch: 0343
NoPatch: 0344
NoPatch: 0345
NoPatch: 0346
NoPatch: 0347
NoPatch: 0348
NoPatch: 0349
NoPatch: 0350
NoPatch: 0351
NoPatch: 0352
NoPatch: 0353
NoPatch: 0354
NoPatch: 0355
NoPatch: 0356
NoPatch: 0357
NoPatch: 0358
NoPatch: 0359
NoPatch: 0360
NoPatch: 0361
NoPatch: 0362
NoPatch: 0363
NoPatch: 0364
NoPatch: 0365
NoPatch: 0366
NoPatch: 0367
NoPatch: 0368
NoPatch: 0369
NoPatch: 0370
NoPatch: 0371
NoPatch: 0372
NoPatch: 0373
NoPatch: 0374
NoPatch: 0375
NoPatch: 0376
NoPatch: 0377
NoPatch: 0378
NoPatch: 0379
NoPatch: 0380
NoPatch: 0381
NoPatch: 0382
NoPatch: 0383
NoPatch: 0384
NoPatch: 0385
NoPatch: 0386
NoPatch: 0387
NoPatch: 0388
NoPatch: 0389
NoPatch: 0390
NoPatch: 0391
NoPatch: 0392
NoPatch: 0393
NoPatch: 0394
NoPatch: 0395
NoPatch: 0396
NoPatch: 0397
NoPatch: 0398
NoPatch: 0399
NoPatch: 0400
NoPatch: 0401
NoPatch: 0402
NoPatch: 0403
NoPatch: 0404
NoPatch: 0405
NoPatch: 0406
NoPatch: 0407
NoPatch: 0408
NoPatch: 0409
NoPatch: 0410
NoPatch: 0411
NoPatch: 0412
NoPatch: 0413
NoPatch: 0414
NoPatch: 0415
NoPatch: 0416
NoPatch: 0417
NoPatch: 0418
NoPatch: 0419
NoPatch: 0420
NoPatch: 0421
NoPatch: 0422
NoPatch: 0423
NoPatch: 0424
NoPatch: 0425
NoPatch: 0426
NoPatch: 0427
NoPatch: 0428
NoPatch: 0429
NoPatch: 0430
NoPatch: 0431
NoPatch: 0432
NoPatch: 0433
NoPatch: 0434
NoPatch: 0435
NoPatch: 0436
NoPatch: 0437
NoPatch: 0438
NoPatch: 0439
NoPatch: 0440
NoPatch: 0441
NoPatch: 0442
NoPatch: 0443
NoPatch: 0444
NoPatch: 0445
NoPatch: 0446
NoPatch: 0447

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= 2.7
BuildRequires: perl >= 5.12.0
BuildRequires: perl-ExtUtils-Embed
BuildRequires: ruby
BuildRequires: gnome-libs-devel
BuildRequires: libbsd-devel
BuildRequires: ncurses-devel >= 5.6-10
BuildRequires: gpm-devel >= 1.20.5
BuildRequires: autoconf

NoSource: 0
#NoSource: 1

%description
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and more.

%package macros
Summary: The macro files needed by enhanced or X11 version of the VIM editor
Group: Applications/Editors

%description macros
vim macros

%package common
Summary: The common files needed by any version of the VIM editor
Group: Applications/Editors

%description common
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and more.  The
vim-common package contains files which every VIM binary will need in
order to run.

If you are installing any version of the VIM editor, you'll also need
to the vim-common package installed.

%package minimal
Summary: A minimal version of the VIM editor
Group: Applications/Editors
Requires: vim-common
Obsoletes:  vim

Requires: ncurses

%description minimal
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and more. The
vim-minimal package includes a minimal version of VIM, which is
installed into /bin/vi for use when only the root partition is
present.

%package enhanced
Summary: A version of the VIM editor which includes recent enhancements
Group: Applications/Editors
Requires: vim-common vim-macros
Obsoletes: vim-color

Requires: gpm
Requires: ncurses
Requires: ruby
Requires: perl-libs
Requires: python

%description enhanced
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and more.  The
vim-enhanced package contains a version of VIM with extra, recently
introduced features like Python and Perl interpreters.

Install the vim-enhanced package if you'd like to use a version of the
VIM editor which includes recently added enhancements like
interpreters for the Python and Perl scripting languages.  You'll also
need to install the vim-common package.

%package X11
Summary: The VIM version of the vi editor for the X Window System
Group: Applications/Editors
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: vim-common vim-macros

Requires: gpm
Requires: ncurses
Requires: gnome-libs
Requires: perl
Requires: ruby
Requires: python
Requires: desktop-file-utils >= 0.10

%description X11
VIM (VIsual editor iMproved) is an updated and improved version of the
vi editor.  Vi was the first real screen-based editor for UNIX, and is
still very popular.  VIM improves on vi by adding new features:
multiple windows, multi-level undo, block highlighting and
more. VIM-X11 is a version of the VIM editor which will run within the
X Window System.  If you install this package, you can run VIM as an X
application with a full GUI interface and mouse support.

Install the vim-X11 package if you'd like to try out a version of vi
with graphics and mouse capabilities.  You'll also need to install the
vim-common package.

%prep
# check md5sum of downloaded patches...
#pushd ../SOURCES
#md5sum --check %{SOURCE200}
#popd

%setup -q -n %{vimversion}
## copy nl.po
cp %{SOURCE250} src/po

%patch1 -p1 -b .vim
# fix rogue dependencies from sample code
chmod -x runtime/tools/mve.awk
%patch2 -p1 -b .paths
find . -name \*.paths | xargs rm -f
#%patch3 -p1 -b .fixkeys
#%patch4 -p1 -b .highlite
#%patch5 -p1 -b .crv
#%patch6 -p1 -b .menui18n
#%patch7 -p1 -b .php
#%patch100 -p0

# Base patches...
%patch0101 -p0
%patch0102 -p0
%patch0103 -p0
%patch0104 -p0
%patch0105 -p0
%patch0106 -p0
%patch0107 -p0
%patch0108 -p0
%patch0109 -p0
%patch0110 -p0
%patch0111 -p0
%patch0112 -p0
%patch0113 -p0
%patch0114 -p0
%patch0115 -p0
%patch0116 -p0
%patch0117 -p0
%patch0118 -p0
%patch0119 -p0
%patch0120 -p0
%patch0121 -p0
%patch0122 -p0
%patch0123 -p0
%patch0124 -p0
%patch0125 -p0
%patch0126 -p0
%patch0127 -p0
%patch0128 -p0
%patch0129 -p0
%patch0130 -p0
%patch0131 -p0
%patch0132 -p0
%patch0133 -p0
%patch0134 -p0
%patch0135 -p0
%patch0136 -p0
%patch0137 -p0
%patch0138 -p0
%patch0139 -p0
%patch0140 -p0
%patch0141 -p0
%patch0142 -p0
%patch0143 -p0
%patch0144 -p0
%patch0145 -p0
%patch0146 -p0
%patch0147 -p0
%patch0148 -p0
%patch0149 -p0
%patch0150 -p0
%patch0151 -p0
%patch0152 -p0
%patch0153 -p0
%patch0154 -p0
%patch0155 -p0
%patch0156 -p0
%patch0157 -p0
%patch0158 -p0
%patch0159 -p0
%patch0160 -p0
%patch0161 -p0
%patch0162 -p0
%patch0163 -p0
%patch0164 -p0
%patch0165 -p0
%patch0166 -p0
%patch0167 -p0
%patch0168 -p0
%patch0169 -p0
%patch0170 -p0
%patch0171 -p0
%patch0172 -p0
%patch0173 -p0
%patch0174 -p0
%patch0175 -p0
%patch0176 -p0
%patch0177 -p0
%patch0178 -p0
%patch0179 -p0
%patch0180 -p0
%patch0181 -p0
%patch0182 -p0
%patch0183 -p0
%patch0184 -p0
%patch0185 -p0
%patch0186 -p0
%patch0187 -p0
%patch0188 -p0
%patch0189 -p0
%patch0190 -p0
%patch0191 -p0
%patch0192 -p0
%patch0193 -p0
%patch0194 -p0
%patch0195 -p0
%patch0196 -p0
%patch0197 -p0
%patch0198 -p0
%patch0199 -p0
%patch0200 -p0
%patch0201 -p0
%patch0202 -p0
%patch0203 -p0
%patch0204 -p0
%patch0205 -p0
%patch0206 -p0
%patch0207 -p0
%patch0208 -p0
%patch0209 -p0
%patch0210 -p0
%patch0211 -p0
%patch0212 -p0
%patch0213 -p0
%patch0214 -p0
%patch0215 -p0
%patch0216 -p0
%patch0217 -p0
%patch0218 -p0
%patch0219 -p0
%patch0220 -p0
%patch0221 -p0
%patch0222 -p0
%patch0223 -p0
%patch0224 -p0
%patch0225 -p0
%patch0226 -p0
%patch0227 -p0
%patch0228 -p0
%patch0229 -p0
%patch0230 -p0
%patch0231 -p0
%patch0232 -p0
%patch0233 -p0
%patch0234 -p0
%patch0235 -p0
%patch0236 -p0
%patch0237 -p0
%patch0238 -p0
%patch0239 -p0
%patch0240 -p0
%patch0241 -p0
%patch0242 -p0
%patch0243 -p0
%patch0244 -p0
%patch0245 -p0
%patch0246 -p0
%patch0247 -p0
%patch0248 -p0
%patch0249 -p0
%patch0250 -p0
%patch0251 -p0
%patch0252 -p0
%patch0253 -p0
%patch0254 -p0
%patch0255 -p0
%patch0256 -p0
%patch0257 -p0
%patch0258 -p0
%patch0259 -p0
%patch0260 -p0
%patch0261 -p0
%patch0262 -p0
%patch0263 -p0
%patch0264 -p0
%patch0265 -p0
%patch0266 -p0
%patch0267 -p0
%patch0268 -p0
%patch0269 -p0
%patch0270 -p0
%patch0271 -p0
%patch0272 -p0
%patch0273 -p0
%patch0274 -p0
%patch0275 -p0
%patch0276 -p0
%patch0277 -p0
%patch0278 -p0
%patch0279 -p0
%patch0280 -p0
%patch0281 -p0
%patch0282 -p0
%patch0283 -p0
%patch0284 -p0
%patch0285 -p0
%patch0286 -p0
%patch0287 -p0
%patch0288 -p0
%patch0289 -p0
%patch0290 -p0
%patch0291 -p0
%patch0292 -p0
%patch0293 -p0
%patch0294 -p0
%patch0295 -p0
%patch0296 -p0
%patch0297 -p0
%patch0298 -p0
%patch0299 -p0
%patch0300 -p0
%patch0301 -p0
%patch0302 -p0
%patch0303 -p0
%patch0304 -p0
%patch0305 -p0
%patch0306 -p0
%patch0307 -p0
## We can ignore this patch (for mercurial source users)
#patch0308 -p0 
%patch0309 -p0
%patch0310 -p0
%patch0311 -p0
%patch0312 -p0
%patch0313 -p0
%patch0314 -p0
%patch0315 -p0
%patch0316 -p0
%patch0317 -p0
%patch0318 -p0
%patch0319 -p0
%patch0320 -p0
%patch0321 -p0
%patch0322 -p0
%patch0323 -p0
%patch0324 -p0
%patch0325 -p0
%patch0326 -p0
%patch0327 -p0
%patch0328 -p0
%patch0329 -p0
%patch0330 -p0
%patch0331 -p0
%patch0332 -p0
%patch0333 -p0
%patch0334 -p0
%patch0335 -p0
%patch0336 -p0
%patch0337 -p0
%patch0338 -p0
%patch0339 -p0
%patch0340 -p0
%patch0341 -p0
%patch0342 -p0
%patch0343 -p0
%patch0344 -p0
%patch0345 -p0
%patch0346 -p0
%patch0347 -p0
%patch0348 -p0
%patch0349 -p0
%patch0350 -p0
%patch0351 -p0
%patch0352 -p0
%patch0353 -p0
%patch0354 -p0
%patch0355 -p0
%patch0356 -p0
%patch0357 -p0
%patch0358 -p0
%patch0359 -p0
%patch0360 -p0
%patch0361 -p0
%patch0362 -p0
%patch0363 -p0
%patch0364 -p0
%patch0365 -p0
%patch0366 -p0
%patch0367 -p0
%patch0368 -p0
%patch0369 -p0
%patch0370 -p0
%patch0371 -p0
%patch0372 -p0
%patch0373 -p0
%patch0374 -p0
%patch0375 -p0
%patch0376 -p0
%patch0377 -p0
%patch0378 -p0
%patch0379 -p0
%patch0380 -p0
%patch0381 -p0
%patch0382 -p0
%patch0383 -p0
%patch0384 -p0
%patch0385 -p0
%patch0386 -p0
%patch0387 -p0
%patch0388 -p0
%patch0389 -p0
%patch0390 -p0
%patch0391 -p0
%patch0392 -p0
%patch0393 -p0
%patch0394 -p0
%patch0395 -p0
%patch0396 -p0
%patch0397 -p0
%patch0398 -p0
%patch0399 -p0
%patch0400 -p0
%patch0401 -p0
%patch0402 -p0
%patch0403 -p0
%patch0404 -p0
%patch0405 -p0
%patch0406 -p0
%patch0407 -p0
%patch0408 -p0
%patch0409 -p0
%patch0410 -p0
%patch0411 -p0
%patch0412 -p0
%patch0413 -p0
%patch0414 -p0
%patch0415 -p0
%patch0416 -p0
%patch0417 -p0
%patch0418 -p0
%patch0419 -p0
%patch0420 -p0
%patch0421 -p0
%patch0422 -p0
%patch0423 -p0
%patch0424 -p0
%patch0425 -p0
%patch0426 -p0
%patch0427 -p0
%patch0428 -p0
%patch0429 -p0
%patch0430 -p0
%patch0431 -p0
%patch0432 -p0
%patch0433 -p0
%patch0434 -p0
%patch0435 -p0
%patch0436 -p0
%patch0437 -p0
%patch0438 -p0
%patch0439 -p0
%patch0440 -p0
%patch0441 -p0
%patch0442 -p0
%patch0443 -p0
%patch0444 -p0
%patch0445 -p0
%patch0446 -p0
%patch0447 -p0

%patch12 -p1

# momonga specific patches
%patch50 -p1
%patch51 -p1
%patch52 -p1

cp %{SOURCE4} .

cp -f %{SOURCE300} runtime/plugin/netrwPlugin.vim
cp -f %{SOURCE301} runtime/plugin/gzip.vim
cp -f %{SOURCE302} runtime/plugin/filetype.vim
cp -f %{SOURCE303} runtime/autoload/zip.vim
cp -f %{SOURCE304} runtime/autoload/tar.vim
cp -f %{SOURCE305} runtime/autoload/netrwFileHandlers.vim
cp -f %{SOURCE306} runtime/autoload/netrw.vim
cp -f %{SOURCE307} runtime/autoload/netrwSettings.vim

perl -pi -e "s,bin/nawk,bin/awk,g" runtime/tools/mve.awk
perl -pi -e "s|prompt|prompt lba32|g" runtime/syntax/lilo.vim

%build
cd src
autoconf
perl -pi -e "s,\\\$VIMRUNTIME,%{vimruntime},g" os_unix.h
perl -pi -e "s,\\\$VIM,%{vimruntime},g" os_unix.h

%configure --with-features=huge \
           --enable-pythoninterp \
           --enable-perlinterp \
           --disable-tclinterp \
           --enable-multibyte \
           --enable-broken-locale \
           --with-x=yes \
           --enable-gui=gnome2 \
           --exec-prefix=%{_prefix} \
           --enable-xim \
           --enable-fontset

#           --enable-rubyinterp \
%make
cp vim gvim
make clean
rm -f auto/{config.log,config.cache,config.guess}

%configure --prefix=%{_prefix} \
           --with-features=huge \
           --enable-pythoninterp \
           --enable-perlinterp \
           --disable-tclinterp \
           --with-x=no \
           --enable-gui=no \
           --exec-prefix=/usr \
           --enable-multibyte \
           --enable-broken-locale

#           --enable-rubyinterp \
%make
cp vim enhanced-vim
make clean

rm -f auto/{config.log,config.cache,config.guess}

%configure --prefix='${DEST}'/usr \
           --with-features=small \
           --with-x=no \
           --disable-pythoninterp \
           --disable-perlinterp \
           --disable-tclinterp \
           --disable-rubyinterp \
           --enable-gui=no \
           --disable-gpm \
           --exec-prefix=/ \
           --with-tlib=ncurses \
           --enable-multibyte \
           --enable-broken-locale

%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/bin
mkdir -p %{buildroot}/usr/{bin,share/vim}
mkdir -p %{buildroot}%{_mandir}/man1

pushd src
%makeinstall BINDIR=%{buildroot}/bin DATADIR=%{buildroot}%{_datadir} MANDIR=%{buildroot}%{_mandir}
mv %{buildroot}/bin/xxd %{buildroot}%{_bindir}
install -s -m755 gvim %{buildroot}%{_bindir}
install -s -m755 enhanced-vim %{buildroot}%{_bindir}/vim
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64}/apps
install -m644 %{SOURCE10} \
   %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/gvim.png
install -m644 %{SOURCE11} \
   %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/gvim.png
install -m644 %{SOURCE12} \
   %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/gvim.png
install -m644 %{SOURCE13} \
   %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/gvim.png

( cd %{buildroot}
  mv -f ./bin/vim ./bin/vi
  rm -f ./bin/rvim
  ln -sf vi ./bin/view
  ln -sf vi ./bin/ex
  ln -sf vi ./bin/rvi
  ln -sf vi ./bin/rview
  ln -sf vim ./usr/bin/ex
  ln -sf gvim ./usr/bin/gview
  ln -sf gvim ./usr/bin/gex
  ln -sf gvim ./usr/bin/vimx
#
# for vim-enhanced (ln -sf /usr/bin/vim /usr/bin/ex)
  ln -sf vim ./usr/bin/ex
  perl -pi -e "s,%{buildroot},," .%{_mandir}/man1/vim.1 .%{_mandir}/man1/vimtutor.1
  rm -f .%{_mandir}/man1/rvim.1
  ln -sf vim.1.bz2 .%{_mandir}/man1/vi.1.bz2
  ln -sf vim.1.bz2 .%{_mandir}/man1/rvi.1.bz2
  ln -sf vim.1.bz2 .%{_mandir}/man1/gvim.1.bz2

  mkdir -p .%{_datadir}/applications
  desktop-file-install --vendor=                  \
      --dir %{buildroot}%{_datadir}/applications  \
      --add-category Application                  \
      --add-category Development                  \
      --add-category TextEditor                   \
      %{SOURCE14}

#mkdir -p ./etc/X11/wmconfig
#install -m644 $RPM_SOURCE_DIR/vimx.wmconfig ./etc/X11/wmconfig/gvim
#  install -s -m644 $RPM_SOURCE_DIR/vimrc ./usr/share/vim/%%{vimversion}/macros/
install -m644  %{SOURCE3}  ./%{vimruntime}/macros/vimrc
# install -m644  %%{SOURCE5}  ./usr/share/vim/%%{vimversion}/macros/gvimrc

install -m644 %{SOURCE100} %{SOURCE101} %{SOURCE102} %{SOURCE103} %{SOURCE104} ./usr/share/vim/%{vimversion}/print/

  pushd ./%{vimruntime}/lang

# for nl_nl
#  iconv -f ISO-8859-1 -t UTF-8 menu_nl_nl.iso_8859-1.vim >menu_nl_nl.utf-8.vim
#  perl -p -i -e "s|scriptencoding iso-8859-1|scriptencoding utf-8|g" menu_nl_nl.utf-8.vim
# for de_de
#  iconv -f ISO-8859-1 -t UTF-8 menu_de_de.iso_8859-1.vim >menu_de_de.utf-8.vim
#  perl -p -i -e "s|scriptencoding iso-8859-1|scriptencoding utf-8|g" menu_de_de.utf-8.vim
# for cs_cz
  iconv -f ISO-8859-2 -t UTF-8 menu_cs_cz.iso_8859-2.vim >menu_cs_cz.utf-8.vim
  perl -p -i -e "s|scriptencoding iso-8859-2|scriptencoding utf-8|g" menu_cs_cz.utf-8.vim
# for chinese
  iconv -f BIG5 -t UTF-8 menu_chinese_taiwan.950.vim >menu_chinese_taiwan.utf-8.vim
  perl -p -i -e "s|scriptencoding cp950|scriptencoding utf-8|g" menu_chinese_taiwan.utf-8.vim
# for korean
  iconv -f EUC-KR -t UTF-8 menu_ko_kr.euckr.vim >menu_ko_kr.utf-8.vim
  perl -p -i -e "s|scriptencoding euc-kr|scriptencoding utf-8|g" menu_ko_kr.utf-8.vim
# for af_af
#  iconv -f ISO-8859-1 -t UTF-8 menu_af_af.iso_8859-1.vim >menu_af_af.utf-8.vim
#  perl -p -i -e "s|scriptencoding iso-8859-1|scriptencoding utf-8|g" menu_af_af.utf-8.vim
# for es_es
#  iconv -f ISO-8859-1 -t UTF-8 menu_es_es.iso_8859-1.vim >menu_es_es.utf-8.vim
#  perl -p -i -e "s|scriptencoding iso-8859-1|scriptencoding utf-8|g" menu_es_es.utf-8.vim
# for pl_pl
  iconv -f ISO-8859-1 -t UTF-8 menu_pl_pl.iso_8859-2.vim >menu_pl_pl.utf-8.vim
  perl -p -i -e "s|scriptencoding iso-8859-1|scriptencoding utf-8|g" menu_pl_pl.utf-8.vim
# for hu_hu
  iconv -f ISO-8859-2 -t UTF-8 menu_hu_hu.iso_8859-2.vim >menu_hu_hu.utf-8.vim
  perl -p -i -e "s|scriptencoding iso-8859-2|scriptencoding utf-8|g" menu_hu_hu.utf-8.vim
# for it_it
#  iconv -f ISO-8859-1 -t UTF-8 menu_it_it.iso_8859-1.vim >menu_it_it.utf-8.vim
#  perl -p -i -e "s|scriptencoding iso-8859-1|scriptencoding utf-8|g" menu_it_it.utf-8.vim
#for sk_sk
  iconv -f ISO-8859-2 -t UTF-8 menu_sk_sk.iso_8859-2.vim >menu_sk_sk.utf-8.vim
  perl -p -i -e "s|scriptencoding iso-8859-2|scriptencoding utf-8|g" menu_sk_sk.utf-8.vim

#  install -s -m644 %%{SOURCE3} ./usr/share/vim/%%{vimversion}/macros/
#  ln -s vimrc ./usr/share/vim/%%{vimversion}/macros/gvimrc
)

# Dependency cleanups
chmod 644 %{buildroot}%{vimruntime}/doc/vim2html.pl \
 %{buildroot}%{vimruntime}/tools/*.pl \
 %{buildroot}%{vimruntime}/tools/vim132

popd

%if 0
# file list for vim-macros
ls %{buildroot}%{vimruntime} |\
    sed -n '/^doc$/ ! s,^,%{vimruntime}/,p' > files-macros.list
%endif

%clean
rm -rf --preserve-root %{buildroot}

%post X11
update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun X11
update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files common
%defattr(-,root,root)
%doc README README*.txt runtime/doc/uganda.txt
%{_bindir}/xxd
%dir %{_datadir}/vim
%dir %{_datadir}/vim/%{vimversion}
%docdir %{_datadir}/vim/%{vimversion}/doc
%{_datadir}/vim/%{vimversion}/doc
%{_mandir}/fr.ISO8859-1/man1/*
%{_mandir}/fr.UTF-8/man1/*
%{_mandir}/fr/man1/*
%{_mandir}/it.ISO8859-1/man1/*
%{_mandir}/it.UTF-8/man1/*
%{_mandir}/it/man1/*
%{_mandir}/ja/man1/*
%{_mandir}/pl.ISO8859-2/man1/*
%{_mandir}/pl.UTF-8/man1/*
%{_mandir}/pl/man1/*
%{_mandir}/ru.KOI8-R/man1/*
%{_mandir}/ru.UTF-8/man1/*
%{_mandir}/man1/ex.*
%{_mandir}/man1/gvim.*
%{_mandir}/man1/rvi.*
%{_mandir}/man1/rview.*
%{_mandir}/man1/vi.*
%{_mandir}/man1/view.*
%{_mandir}/man1/vim.*
%{_mandir}/man1/vimdiff.*
%{_mandir}/man1/vimtutor.*
%{_mandir}/man1/xxd.*

%files macros
%defattr(-,root,root)
/bin/vimtutor
#/bin/gvimtutor
%{vimruntime}/*.vim
%{vimruntime}/autoload
%{vimruntime}/colors
%{vimruntime}/compiler
%{vimruntime}/ftplugin
%{vimruntime}/indent
%{vimruntime}/keymap
%{vimruntime}/lang
%{vimruntime}/macros
%{vimruntime}/plugin
%{vimruntime}/print
%{vimruntime}/spell
%{vimruntime}/syntax
%{vimruntime}/tools
%{vimruntime}/tutor
%exclude %{vimruntime}/plugin/filetype.vim

%files minimal
%defattr(-,root,root)
/bin/ex
/bin/vi
/bin/view
/bin/rvi
/bin/rview

%files enhanced
%defattr(-,root,root)
%{_bindir}/vim
%{_bindir}/ex

%files X11
%defattr(-,root,root)
%{_datadir}/applications/gvim.desktop
#config(missingok) /etc/X11/wmconfig/gvim
%{_bindir}/gvim
%{_bindir}/vimx
%{_bindir}/gex
%{_bindir}/gview
%{_datadir}/icons/hicolor/*/apps/*
%{_mandir}/man1/evim.*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.347-1m)
- rebuild against perl-5.20.0
- update version 7.4 patchlevel 7.4.347

* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.265-1m)
- update version 7.4 patchlevel 7.4.265

* Sat Apr  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.245-1m)
- update version 7.4 patchlevel 7.4.245

* Mon Mar 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.193-1m)
- update version 7.4 patchlevel 7.4.193

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.182-1m)
- update version 7.4 patchlevel 7.4.182

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.160-2m)
- rebuild against perl-5.18.2

* Mon Jan 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.160-1m)
- update version 7.4 patchlevel 7.4.160

* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.103-1m)
- update version 7.4 patchlevel 7.4.103

* Sun Nov 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.083-1m)
- update version 7.4 patchlevel 7.4.083

* Mon Nov  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.068-1m)
- update version 7.4 patchlevel 7.4.068

* Sun Oct 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.052-1m)
- update version 7.4 patchlevel 7.4.052

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.050-1m)
- update version 7.4 patchlevel 7.4.050

* Sun Sep 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.035-1m)
- update version 7.4 patchlevel 7.4.035

* Sun Sep 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.027-1m)
- update version 7.4 patchlevel 7.4.027

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.005-1m)
- update version 7.4 patchlevel 7.4.005

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.1314-2m)
- rebuild against perl-5.18.1

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.1314-1m)
- update version 7.3 patchlevel 7.3.1314

* Mon Jun  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.1106-1m)
- update version 7.3 patchlevel 7.3.1106

* Fri May 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.1071-1m)
- update version 7.3 patchlevel 7.3.1071

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.967-2m)
- rebuild against perl-5.18.0

* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.967-1m)
- update version 7.3 patchlevel 7.3.967

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.931-1m)
- update version 7.3 patchlevel 7.3.931

* Mon Apr 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.918-1m)
- update version 7.3 patchlevel 7.3.918

* Wed Apr 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.905-1m)
- update version 7.3 patchlevel 7.3.905

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.888-1m)
- update version 7.3 patchlevel 7.3.888

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.884-1m)
- update version 7.3 patchlevel 7.3.884

* Sat Mar 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.875-1m)
- update version 7.3 patchlevel 7.3.875

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.861-1m)
- update version 7.3 patchlevel 7.3.861
- rebuild against perl-5.16.3

* Sat Mar  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.854-1m)
- update version 7.3 patchlevel 7.3.854

* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.843-1m)
- update version 7.3 patchlevel 7.3.843

* Sun Feb 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.831-1m)
- update version 7.3 patchlevel 7.3.831

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.806-1m)
- update version 7.3 patchlevel 7.3.806

* Thu Jan 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.797-1m)
- update version 7.3 patchlevel 7.3.797

* Sat Jan 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.785-1m)
- update version 7.3 patchlevel 7.3.785

* Tue Jan 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.773-1m)
- update version 7.3 patchlevel 7.3.773

* Wed Dec 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.762-1m)
- update version 7.3 patchlevel 7.3.762

* Tue Dec 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.754-1m)
- update version 7.3 patchlevel 7.3.754

* Sun Nov 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.729-1m)
- update version 7.3 patchlevel 7.2.729

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.715-1m)
- update version 7.3 patchlevel 7.3.715

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.712-2m)
- rebuild against perl-5.16.2

* Thu Oct 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.712-1m)
- update version 7.3 patchlevel 7.3.712

* Mon Oct 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.691-1m)
- update version 7.3 patchlevel 7.3.691

* Sat Oct  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.682-1m)
- update version 7.3 patchlevel 7.3.682

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.666-1m)
- update version 7.3 patchlevel 7.3.666

* Wed Sep 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.659-1m)
- update version 7.3 patchlevel 7.3.659

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.646-1m)
- update version 7.3 patchlevel 7.3.646
- patch 7.3.336 was changed

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.629-1m)
- update version 7.3 patchlevel 7.3.629

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.622-2m)
- rebuild against perl-5.16.1

* Mon Aug  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.622-1m)
- update version 7.3 patchlevel 7.3.622

* Tue Jul 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.618-1m)
- update version 7.3 patchlevel 7.3.618

* Sun Jul 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.600-1m)
- update version 7.3 patchlevel 7.3.600

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.591-1m)
- update version 7.3 patchlevel 7.3.591
- rebuild against perl-5.16.0

* Sat Jun 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.566-1m)
- update version 7.3 patchlevel 7.3.566

* Sun May 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.524-1m)
- update version 7.3 patchlevel 7.3.524

* Mon May 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.515-1m)
- update version 7.3 patchlevel 7.3.515

* Sat Mar 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.486-1m)
- update version 7.3 patchlevel 7.3.486

* Mon Mar 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.480-1m)
- update version 7.3 patchlevel 7.3.480

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.475-1m)
- update version 7.3 patchlevel 7.3.475

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.470-1m)
- update version 7.3 patchlevel 7.3.470

* Sun Feb 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.456-1m)
- update version 7.3 patchlevel 7.3.456

* Tue Feb 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.444-1m)
- update version 7.3 patchlevel 7.3.444

* Fri Feb 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.434-1m)
- update version 7.3 patchlevel 7.3.434

* Mon Jan 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.421-1m)
- update version 7.3 patchlevel 7.3.421

* Fri Jan 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.401-1m)
- update version 7.3 patchlevel 7.3.401

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.393-1m)
- update version 7.3 patchlevel 7.3.393

* Sun Jan  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.390-1m)
- update version 7.3 patchlevel 7.3.390

* Sat Dec 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.386-1m)
- update version 7.3 patchlevel 7.3.386

* Thu Dec 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.381-1m)
- update version 7.3 patchlevel 7.3.381

* Mon Dec 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.372-1m)
- update version 7.3 patchlevel 7.3.372

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.364-1m)
- update version 7.3 patchlevel 7.3.364

* Thu Dec  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.363-1m)
- update version 7.3 patchlevel 7.3.363

* Mon Oct 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.353-1m)
- update version 7.3 patchlevel 7.3.353

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.338-1m)
- update version 7.3 patchlevel 7.3.338

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.333-1m)
- update version 7.3 patchlevel 7.3.333

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.315-2m)
- rebuild against perl-5.14.2

* Wed Sep 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.315-1m)
- update version 7.3 patchlevel 7.3.315

* Tue Aug 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.289-1m)
- update version 7.3 patchlevel 7.3.289

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.266-1m)
- update version 7.3 patchlevel 7.3.266

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.260-1m)
- update version 7.3 patchlevel 7.3.260

* Sat Jul 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.254-1m)
- update version 7.3 patchlevel 7.3.254

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.244-1m)
- update version 7.3 patchlevel 7.3.244

* Thu Jun 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.237-2m)
- revert 7.3.202

* Thu Jun 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.237-1m)
- 7.3.202/7.3.203/7.3.223 was released again and set NoPatch to them
- update version 7.3 patchlevel 7.3.237

* Sun Jun 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.235-1m)
- update version 7.3 patchlevel 7.3.235

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.230-2m)
- rebuild against perl-5.14.1

* Tue Jun 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.230-1m)
- update version 7.3 patchlevel 7.3.230
- ftp://ftp.vim.org/pub/vim/patches/7.3/7.3.223 could not be applied...

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.219-1m)
- update version 7.3 patchlevel 7.3.219

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.206-1m)
- update version 7.3 patchlevel 7.3.206
- ftp://ftp.vim.org/pub/vim/patches/7.3/7.3.202 could not be applied...
- ftp://ftp.vim.org/pub/vim/patches/7.3/7.3.203 could not be applied...

* Fri May 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.198-1m)
- update version 7.3 patchlevel 7.3.198

* Fri May 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.189-1m)
- update version 7.3 patchlevel 7.3.189

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.184-1m)
- update version 7.3 patchlevel 7.3.184

* Sat May  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3.170-3m)
- fix broken BuildRequires

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.170-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu May  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.170-1m)
- update version 7.3 patchlevel 7.3.170

* Wed May  4 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3.162-3m)
- fix momorel

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3.162-2m)
- rebuild for python-2.7

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.162-1m)
- update version 7.3 patchlevel 7.3.162

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3.154-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.154-1m)
- update version 7.3 patchlevel 7.3.154

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.146-1m)
- update version 7.3 patchlevel 7.3.146

* Mon Mar  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.138-1m)
- update version 7.3 patchlevel 7.3.138

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.107-1m)
- update version 7.3 patchlevel 7.3.107

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.102-1m)
- update version 7.3 patchlevel 7.3.102

* Wed Jan  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.098-1m)
- update version 7.3 patchlevel 7.3.098

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.087-1m)
- update version 7.3 patchlevel 7.3.087

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.081-1m)
- update version 7.3 patchlevel 7.3.081

* Fri Dec  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.075-1m)
- update version 7.3 patchlevel 7.3.075

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3.069-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.069-1m)
- update version 7.3 patchlevel 7.3.069

* Wed Nov 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.062-1m)
- update version 7.3 patchlevel 7.3.062

* Thu Nov 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.055-1m)
- update version 7.3 patchlevel 7.3.055
- CVE-2010-3914 (perhaps, windows only...) was resolved in 7.3.034

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.050-1m)
- update version 7.3 patchlevel 7.3.050

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.047-1m)
- update version 7.3 patchlevel 7.3.047

* Mon Oct 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.035-1m)
- update version 7.3 patchlevel 7.3.035
- add Requires: perl-libs to vim-enhanced

* Fri Oct 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.032-2m)
- add BuildRequires: perl-ExtUtils-Embed
- add BuildRequires: libbsd-devel

* Thu Oct 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.032-1m)
- update version 7.3 patchlevel 7.3.032
- do not install filetype.vim to plugin directory to fix #309
-- http://developer.momonga-linux.org/kagemai/guest.cgi?action=view_report&id=309&project=momongaja

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.029-1m)
- update version 7.3 patchlevel 7.3.029

* Sun Oct 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.020-1m)
- update version 7.3 patchlevel 7.3.020

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.011-1m)
- update version 7.3 patchlevel 7.3.011

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.444-4m)
- rebuild against perl-5.12.2

* Thu Sep  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.2.444-3m)
- [BUG FIX] fix %%post and %%postun X11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2.444-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.444-1m)
- update version 7.2 patchlevel 7.2.444

* Sat May 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.440-1m)
- update version 7.2 patchlevel 7.2.440

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.433-2m)
- rebuild against perl-5.12.1

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.433-1m)
- update version 7.2 patchlevel 7.2.433

* Thu May 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.416-1m)
- update version 7.2 patchlevel 7.2.416

* Mon Apr 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.411-1m)
- rebuild against perl-5.12.0
- update version 7.2 patchlevel 7.2.411

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.403-1m)
- update version 7.2 patchlevel 7.2.403

* Sun Mar  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.385-1m)
- update version 7.2 patchlevel 7.2.385

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.351-1m)
- update version 7.2 patchlevel 7.2.351

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.323-1m)
- update version 7.2 patchlevel 7.2.323

* Sun Nov 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.309-1m)
- update version 7.2 patchlevel 7.2.309

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.2.267-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  9 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2.267-1m)
- update version 7.2 patchlevel 7.2.267

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.2.245-3m)
- [SECURITY] CVE-2008-4677 CVE-2008-3076 CVE-2008-6235
- replace vulnerable vim scripts

* Wed Aug 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2.245-2m)
- rebuild against perl-5.10.1

* Mon Aug 24 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.2.245-1m)
- update version 7.2 patchlevel 7.2.245

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2.218-1m)
- update version 7.2 patchlevel 7.2.218

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.197-1m)
- update version 7.2 patchlevel 7.2.197

* Fri May  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.2.166-1m)
- update version 7.2 patchlevel 7.2.166
- add README

* Tue Apr 21 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.2.148-1m)
- update version 7.2 patchlevel 7.2.148

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.2.069-5m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.2.069-4m)
- rebuild against gpm-1.20.5

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.2.069-3m)
- drop Patch0 for fuzz=0, merged upstream
- update Patch1 for fuzz=0

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.2.069-2m)
- rebuild against python-2.6.1-2m

* Sat Dec 20 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.2.069-1m)
- [SECURITY] CVE-2009-0316
- update version 7.2 patchlevel 7.2.069

* Sat Sep 27 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.2.022-1m)
- [SECURITY] CVE-2008-4101 CVE-2008-3074 CVE-2008-3075
- update version 7.2 patchlevel 7.2.022

* Mon Aug 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.2.2-1m)
- update 7.2.2

* Wed Aug  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1.330-1m)
- [SECURITY] CVE-2008-3294
- update to version 7.1 patchlevel 7.1.330

* Thu Jun 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1.315-1m)
- [SECURITY] CVE-2008-2712
- update to version 7.1 patchlevel 7.1.315

* Mon Jun  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.305-1m)
- update to version 7.1 patchlevel 7.1.305

* Wed Apr 16 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.1.293-1m)
- update version 7.1 patchlevel 7.1.293

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1.285-2m)
- rebuild against gcc43

* Sun Mar 23 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.1.285-1m)
- update version 7.1 patchlevel 7.1.285

* Mon Mar 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.269-1m)
- update to version 7.1 patchlevel 7.1.269

* Fri Feb  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.244-2m)
- need ncurses-5.6-10m

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.244-1m)
- update to version 7.1 patchlevel 7.1.244
- rebuild against ncurses-5.6-10(Obso libtermcap)

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.1.242-1m)
- update to version 7.1 patchlevel 7.1.242
- build against perl-5.10.0-1m

* Mon Jan 15 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.1.228-1m)
- update to version 7.1 patchlevel 7.1.228

* Tue Dec 11 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.1.175-1m)
- update to version 7.1 patchlevel 7.1.175

* Sun Nov  4 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.1.147-1m)
- update to version 7.1 patchlevel 7.1.147

* Fri Aug 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (7.1.093-1m)
- update to version 7.1 patchlevel 7.1.093
- [SECURITY] CVE-2007-2953 was resolved at patchlevel 7.1.039

* Thu Jul 12 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.1.028-1m)
- update to version 7.1 patchlevel 7.1.028

* Sun Jul  8 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.1.018-1m)
- update to version 7.1 patchlevel 7.1.018

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.0.243-2m)
- add Patch52 vim-7.0-syntax-spec-momonga.patch

* Thu May 11 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.243-1m)
- updatre with basic patches 7.0.216 to 7.0.243

* Sat Mar 10 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.215-1m)
- update with basic patches 7.0.183 to 7.0.215

* Wed Jan 10 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.182-1m)
- update with basic patches 7.0.179 to 7.0.182

* Sat Dec 16 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.178-1m)
- update with basic patches 7.0.165 to 7.0.178

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.164-1m)
- update with basic patches 7.0.147 to 7.0.164

* Fri Oct 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0.146-2m)
- comment out --enable-rubyinterp (for autoconf-2.60)

* Sun Oct 22 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.146-1m)
- update with basic patches 7.0.040 to 7.0.146

* Mon Jul 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.39-1m)
- update with basic patches 7.0.018 to 7.0.039

* Fri Jun  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0.17-4m)
- delete duplicated dir

* Wed Jun  7 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0.17-3m)
- comment out gvim.1.* from vim-X11 files list for avoid file conflicts
  between vim-comon

* Thu May 18 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.17-1m)
- update with patches 7.0.001 to 7.0.017

* Wed May 16 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (7.0-2m)
- fix build

* Wed May 10 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0-1m)

* Thu May  4 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (6.4.10-1m)
- add Patch110
- updated to 6.4 patchlevel 10

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.4.9-2m)
- revised installdir

* Thu Mar 16 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.4.9-1m)
- add Patch109
- updated to 6.4 patchlevel 9

* Fri Mar 3 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.4.8-1m)
- add Patch108
- updated to 6.4 patchlevel 8

* Sun Feb 12 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.4.1-3m)
- add Patch 107

* Sat Jan 22 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.4.1-2m)
- rename Patch100 to 101
- add Patches 102 to 106

* Sun Nov 5 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.4.1-1m)
- version 6.4 patch level 001

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.3.86-4m)
- build against python-2.4.2

* Fri Aug 22 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (6.3.86-3m)
- modified spec.vim for momonga

* Fri Aug 19 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (6.3.86-2m)
- revised spec.vim: look at not only %%define but %%global on searching a variable definition in .spec file

* Fri Aug 19 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (6.3.86-1m)
- updated to 6.3 patchlevel 86

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (6.3.62-2m)
- rebuild against libtermcap and ncurses

* Sun Feb 20 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (6.3.62-1m)
- update to 6.3 patchlevel 62
- Patch11 --> Patch145
- add gvim.desktop and gvim icon file
- add execute MIME application databese create command for %%post & %%postun

* Fri Dec 17 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (6.3.20-2m)
- [SECURITY] Vim / Gvim Modelines Command Execution Vulnerabilities
  http://secunia.com/advisories/13490/

* Fri Sep  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (6.3.20-1m)
- update to 6.3 patchlevel 20

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (6.3.4-2m)
- remove Epoch from BuildPrereq

* Tue Jun 15 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (6.3.4-1m)
- up to version 6.3 pache level 4

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (6.2.127-6m)
- rebuild against ncurses 5.3.

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (6.2.127-5m)
- rebuild against for libxml2-2.6.8

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (6.2.127-4m)
- revised spec for rpm 4.2.

* Sun Nov  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (6.2.127-3m)
- accept any version of python

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (6.2.127-2m)
- rebuild against perl-5.8.1

* Fri Oct 24 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (6.2.127-1m)
- up to version 6.2.127
- update hardcopy-multibyte patch for vim 6.2

* Mon Aug  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (6.1.474-2m)
- rebuild against ruby-1.8

* Wed Apr 23 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (6.1.474-1m)
- update to patch level 474.
- use the source archives that include all the patches
  up to and including 6.1.405.
  http://marc.theaimsgroup.com/?l=vim-announce&m=104914253629987&w=2
- fix Requires:
    vim-minimal -> vim-common
    vim-{enhanced,X11} -> vim-common, vim-macros
- remove files in -common that are contained in -macros.
- move $VIMRUNTIME/doc to -common so as to
  make help be available by vim-minimal.
- move vimtutor to -macros since it is useless for -minimal
- define macro %%vimruntime and use it.
- use more macros
- see [Momonga-devel.ja:01592] for details.

* Fri Feb 07 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (6.1-9m)
- added patch ( from 6.1.248-6.1.320)

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (6.1-8m)
- invoke autoconf-old to ignore gcc-3.X warnings

* Sat Nov 02 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (6.1-7m)
- added patch (from 6.1.145-6.1.247)

* Mon Sep 09 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (6.1-6m)
- added patch (from 6.1.144-6.1.174)

* Sat Aug 03 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (6.1-5m)
- added patch (from 6.1.095-6.1.143)

* Thu Jun 13 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (6.1-4k)
- added patch to 6.1.094.
- (change patch100 from vim-6.1-001-041.gz to vim-6.1-001-094.gz).

* Tue Apr 30 2002 Motonobu Ichimura <famao@kondara.org>
- (6.1-2k)
- up to 6.1
- added patch from 6.1.001 to 6.1.041

* Fri Mar 01 2002 Motonobu Ichimura <famao@kondara.org>
- (6.0-14k)
- added patch (from 6.0-223-6.0.270)

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (6.0-12k)
- rebuild against for db3,4 and python-2.2

* Wed Feb 13 2002 Motonobu Ichimura <famao@kondara.org>
- (6.0-10k)
- added patch (from 6.0.063-6.0.222)
- added ps patch for multibyte support (at :hardcopy)

* Sat Nov 03 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-8k)
- moved vim patches to one gzip'ed patch

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-6k)
- added patch (from 6.0.012 to 6.0.063)
- added BuildRequires,Requires Tag

* Thu Oct 25 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-4k)
- added --enable-multibyte option to vim-minimal
-    (for li18nux test suite)

* Thu Oct 04 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-3k)
- now official release
- patches (from 6.0.001 to 6.0.011) added

* Wed Aug 22 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-0.00520503k)
- up to 6.0as
- more translations to UTF-8

* Tue Aug 21 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-0.00520403k)
- import to Jirai

* Tue Aug 21 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-0.00520402k)
- up to 6.0ar

* Tue Aug 07 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-0.005202k)
- import to STABLE_2_0

* Tue Jul 31 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-0.005203k)
- up to 6.0ap
- this is first-beta relase (see http://www.vim.org)

* Tue Jul 17 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-0.005005k)
- modified vimrc
- now re-enable to be built by non-root users :-P)

* Sun Jul  8 2001 Toru Hoshina <toru@df-usa.com>
- (6.0-0.004010k)
- fixed ruby hdr issue.

* Sun May 20 2001 Toru Hoshina <toru@df-usa.com>
- (6.0-0.004008k)
- remove /usr/bin/vi symlink because the vipw and thew visudo no longer
  depends on that, appologize to confused.

* Fri May 18 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (6.0-0.004006k)
- change "vi" --with-features=tiny to --with-features=small

* Sat May 12 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (6.0-0.004004k)
- add /usr/bin/vi -> /bin/vi symlinks

* Fri May 11 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (6.0-0.004002k)
- add URL:
- update to 6.0ae

* Thu May 03 2001 Motonobu Ichimura <famao@digitalfactory.co.jp>
- (6.0-0.003002k)
- imported to Mary

* Thu May 03 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0-0.003003k)
- added more stuff.
- renamed version to 0.003003k

* Thu May 03 2001 Motonobu Ichimura <famao@kondara.org>
- (6.0ad-3k)
- version up
- many changes added.

* Sat Dec 23 2000 Motonobu Ichimura <famao@kondara.org>
- up to 6.0q-3k

* Sat Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (6.0h-7k)
- modified spec file with %{_mandir} macor for compatible

* Mon Oct 09 2000 Motonobu Ichimura <famao@kondara.org>
- up to 6.0h

* Thu Sep 27 2000 Motonobu Ichimura <famao@kondara.org>
- add support for UTF-8

* Wed Sep 13 2000 Motonobu Ichimura <famao@kondara.org>
- up to 6.0g
- added many many changes, but I need more ;-)

* Thu Apr 18 2000 Takaaki Tabuchi <tab@kondara.org>
- fix vim was not exist.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- fix file not found vim.

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (5.6-11).

* Wed Mar 08 2000 Motonobu Ichimura <famao@kondara.org>
- now DONT uses macros for initial setting

* Tue Mar 07 2000 Preston Brown <pbrown@redhat.com>
- fix home/end in vimrc (we did a term = rxvt, totally wrong)

* Thu Mar 02 2000 Shingo Akagaki <dora@kondara.org>
- use static link (vim-minimal)

* Tue Feb 29 2000 Preston Brown <pbrown@redhat.com>
- change F1-F4 keybindings for xterm builtin terminfo to match real terminfo

* Thu Feb 17 2000 Bill Nottingham <notting@redhat.com>
- kill autoindent

* Fri Feb 11 2000 Motonobu Ichimura <famao@kondara.org>
- now uses include vimrc_example for initial setting

* Tue Feb 10 2000 Motonobu Ichimura <famao@kondara.org>
- up to 5.6a
- some change added for japanese

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- wmconfig -> desktop

* Sat Feb  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Patchlevel 11
- handle compressed man pages
- fix man page symlinks

* Wed Feb  2 2000 Bill Nottingham <notting@redhat.com>
- eliminate dependencies on X in vim-enhanced, and ncurses/gpm
  in vim-minimal

* Fri Jan 28 2000 Bill Nottingham <notting@redhat.com>
- eliminate dependencies on csh and perl in vim-common

* Wed Jan 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Use awk, not nawk

* Tue Jan 18 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 5.6
- patch 5.6.001
- remove /usr/bin/vi - if you want vim, type vim

* Tue Jan 11 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 5.6a
- Remove dependency on nawk (introduced by base update)
- some tweaks to make updating easier

* Wed Nov 24 1999 Motonobu Ichimura <famao@kondara.org>
- up to 5.5
- fix mouse-enabled problem. sorry :-P)

* Thu Nov 11 1999 Motonobu Ichimura <famao@kondara.org>
- add some changes and rebuild against without libtermcap

* Tue Nov  9 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 5.5
- fix path to vimrc

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Oct 5 1999 Shin Fukui <shinta@april.co.jp>
- add configure options for multibyte support

* Tue Jul 27 1999 Michael K. Johnson <johnsonm@redhat.com>
- moved from athena to gtk widgets for X version
- removed vim.1 from X11 filelist because X11 depends on vim-common anyway
- fixed rogue dependencies from sample files

* Tue Jul 27 1999 Jeff Johnson <jbj@redhat.com>
- update to 5.4.

* Thu Jul 22 1999 Jeff Johnson <jbj@redhat.com>
- man page had buildroot pollution (#3629).

* Thu Mar 25 1999 Preston Brown <pbrown@redhat.com>
- with recent termcap/terminfo fixes, regular vim works in xterm/console
- in color, so vim-color package removed.

* Tue Mar 23 1999 Erik Troan <ewt@redhat.com>
- removed "set backupdir=/tmp/vim_backup" from default vimrc

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 5)

* Thu Dec 17 1998 Michael Maher <mike@redaht.com>
- built pacakge for 6.0

* Tue Sep 15 1998 Michael Maher <mike@redhat.com>
- removed '--with-tlib=termcap' so that color-vim works

* Wed Sep  2 1998 Jeff Johnson <jbj@redhat.com>
- update to 5.3.

* Mon Aug 10 1998 Jeff Johnson <jbj@redhat.com>
- merge in Toshio's changes
- color-vim: changed "--disable-p" to "--disable-perlinterp --with-tlib=termcap"
- added minimal rvi/rview and man pages.
- move Obsoletes to same package as executable.

* Thu Aug 06 1998 Toshio Kuratomi <badger@prtr-13.ucsc.edu>
- Break the package apart similar to the way the netscape package was
  broken down to handle navigator or communicator: The vim package is
  Obsolete, now there is vim-common with all the common files, and a
  package for each binary: vim-minimal (has /bin/vi compiled with no
  frills), vim-enhanced (has /usr/bin/vim with extra perl and python
  interpreters), and vim-X11 (has /usr/X11R6/bin/gvim compiled with
  GUI support.)
- Enable the perl and python interpreters in the gui version (gvim).

* Tue Jun 30 1998 Michael Maher <mike@redhat.com>
- Fixed tutor help.
- cvim package added.  Thanks to Stevie Wills for finding this one :-)

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri May 01 1998 Donnie Barnes <djb@redhat.com>
- added patch to turn off the "vi compatibility" by default.  You can
  still get it via the -C command line option

* Thu Apr 23 1998 Donnie Barnes <djb@redhat.com>
- removed perl and python interpreters (sorry, but those don't belong
  in a /bin/vi and having two vi's seemed like overkill...complain
  to suggest@redhat.com if you care)

* Fri Apr 17 1998 Donnie Barnes <djb@redhat.com>
- fixed buildroot bug

* Sat Apr 11 1998 Donnie Barnes <djb@redhat.com>
- updated from 4.6 to 5.1
- moved to buildroot

* Sun Nov 09 1997 Donnie Barnes <djb@redhat.com>
- fixed missing man page

* Wed Oct 22 1997 Donnie Barnes <djb@redhat.com>
- added wmconfig entry to vim-X11

* Mon Oct 20 1997 Donnie Barnes <djb@redhat.com>
- upgraded from 4.5 to 4.6

* Fri Jun 13 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Mar 25 1997 Michael K. Johnson <johnsonm@redhat.com>
- Upgraded to 4.5
- Added ex symlinks

* Tue Mar 11 1997 Michael K. Johnson <johnsonm@redhat.com>
- Added view symlink.
