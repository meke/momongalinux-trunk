%global momorel 2

###############################################################################
###############################################################################
##
##  Copyright (C) 2004-2010 Red Hat, Inc.  All rights reserved.
##
##  This copyrighted material is made available to anyone wishing to use,
##  modify, copy, or redistribute it subject to the terms and conditions
##  of the GNU General Public License v.2.
##
###############################################################################
###############################################################################

Name: gfs2-utils
Version: 3.1.6
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: System Environment/Kernel
Summary: Utilities for managing the global filesystem (GFS2)
Requires(post): chkconfig
Requires(preun): initscripts
Requires(preun): chkconfig
Requires: ncurses
Requires: file
BuildRequires: ncurses-devel
BuildRequires: glibc-kernheaders
BuildRequires: automake
BuildRequires: perl
BuildRequires: libtool
Obsoletes: gfs2-cluster < %{version}
URL: http://sources.redhat.com/cluster/wiki/

# The source for this package was pulled from the upstream git tree.
# Use the following commands to generate the tarball:
# git clone git://git.fedorahosted.org/gfs2-utils.git
# cd gfs2-utils
# ./make-tarball.sh
#
Source0: gfs2-utils-%{version}.tar.gz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%prep
%setup -q -n gfs2-utils

%build
./autogen.sh
%configure
make %{_smp_mflags}

%install
rm -rf %{buildroot}
# gfs and group installed separately to avoid pulling in the docs
make -C gfs2 install DESTDIR=%{buildroot}
# Don't ship gfs2_{trace,lockcapture} in this package
rm -f %{buildroot}/sbin/gfs2_trace
rm -f %{buildroot}/sbin/gfs2_lockcapture
rm -f %{buildroot}%{_mandir}/man8/gfs2_trace.8
rm -f %{buildroot}%{_mandir}/man8/gfs2_lockcapture.8

%clean
rm -rf %{buildroot}


%description -n gfs2-utils
The gfs2-utils package contains a number of utilities for creating,
checking, modifying, and correcting any inconsistencies in GFS2
filesystems.

This package can be used standalone for single node GFS2 filesystems,
but for multi-node filesystems (i.e. the normal case), the gfs2-cluster
package is required as well.

%post -n gfs2-utils
/sbin/chkconfig --add gfs2

%preun -n gfs2-utils
if [ "$1" = 0 ]; then
	/sbin/service gfs2 stop >/dev/null 2>&1
	/sbin/chkconfig --del gfs2
fi
exit 0

%files -n gfs2-utils
%defattr(-,root,root,-)
%doc doc/COPYING.* doc/COPYRIGHT doc/README.* doc/*.txt
/sbin/*
%{_sbindir}/gfs2_*
%{_sbindir}/tunegfs2
%{_mandir}/man8/*gfs2*
%{_mandir}/man5/*


%changelog
* Tue Aug 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.6-2m)
- fix BR

* Sun Aug 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.6-1m)
- update 3.1.6
- Obsoletes: gfs2-cluster

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.3-1m)
- udpate 3.1.3

* Mon Jul  4 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.1-1m)
- import from Fedora 15

* Fri Feb 25 2011 Bob Peterson <rpeterso@redhat.com> - 3.1.1-2
- Bumping release number to keep upgrades consistent.

* Wed Feb 23 2011 Bob Peterson <rpeterso@redhat.com> - 3.1.1-1
- gfs2_edit savemeta doesn't save all leafs for big directories
- gfs2_edit improvements
- fsck.gfs2: can't repair rgrps resulting from gfs_grow->gfs2_convert
- fsck.gfs2: reports master/root dinodes as unused and fixes bitmap

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.1.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Jan 20 2011 Steven Whitehouse <swhiteho@redhat.com> - 3.1.0-4
- Drop mount.gfs2 and its man page
- Only list gfs2_tool once in the files list

* Wed Dec  8 2010 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.1.0-3
- Drop circular dependency on cman

* Fri Dec  3 2010 Fabio M. Di Nitto <fdinitto@redhat.com> - 3.1.0-2
- gfs2-cluster should Obsoletes/Provides gfs-pcmk

* Tue Sep 30 2010 Steven Whitehouse <swhiteho@redhat.com> - 3.1.0-1
- Bringing this package back for upstream GFS2
  Addition of gfs2tune to the utils
  Merge of gfs_controld from cman

* Thu Jan 22 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 2.03.11-1
- New upstream release
  Fix several bugs and drastically improve startup errors.

* Wed Dec 10 2008 Fabio M. Di Nitto <fdinitto@redhat.com> - 2.03.10-1
- New upstream release
  Fix several bugs and port gfs1 code to match 2.6.27 kernel.

* Fri Oct 31 2008 Fabio M. Di Nitto <fdinitto@redhat.com> - 2.03.09-1
- New upstream release
  Fix rhbz#468966
  Addresses several security issues similar to CVE-2008-4192 and
  CVE-2008-4579 after deep code audit from upstream
- cleanup patches to match 2.6.26 kernel in F-9

* Tue Oct 21 2008 Fabio M. Di Nitto <fdinitto@redhat.com> - 2.03.08-1
- New upstream release
  Fix rhbz#460376 CVE-2008-4192
  Fix rhbz#467386 CVE-2008-4579
- cleanup/update patches to match 2.6.26 kernel in F-9

* Thu Aug 14 2008 Fabio M. Di Nitto <fdinitto@redhat.com> - 2.03.07-1
- New upstream release
- Fix rgmanager startup locking issues
- Apply patch to include kernel headers from 2.6.26 required to build
  userland. Userland will run in 2.6.25 compatibility mode
- Apply patch to keep kernel modules at 2.6.25 (upstream is at 2.6.26)
  (this patch is purely cosmetic since we don't build kernel modules
  but keep the source in sync is Good (tm))
- Cleanup packaging for installed docs and file permissions

* Mon Jul 14 2008 Fabio M. Di Nitto <fdinitto@redhat.com> - 2.03.05-1
- New upstream release
- Cleanup installed doc after upstream

* Wed Jun 11 2008 Fabio M. Di Nitto <fdinitto@redhat.com> 2.03.04-1
- New upstream release
- Resolves: #446995 #318271 #447378 #445662
- Update license tags after major upstream cleanup
- Include COPYRIGHT file

* Fri May 30 2008 Fabio M. Di Nitto <fdinitto@redhat.com> 2.03.03-1
- New upstream release
- Fix several build warnings
- Update spec files to use macros
- Update Requires to use packages rather than pointing at files
- Drop BR on kernel-devel since it's not required anymore
- Update build section to use proper _sysconfdir, libdir and sbindir
- Avoid abusing cd when we can ask make to do the work for us
- Remove /usr/sbin from file section. We don't have any file there
  and we can avoid shipping stuff by mistake

* Mon Apr 14 2008 Steven Whitehouse <swhiteho@redhat.com> 2.03.00-3
- Fabbione saves the day. We can get rid of the sed stuff after all

* Mon Apr 14 2008 Steven Whitehouse <swhiteho@redhat.com> 2.03.00-1
- New upstream sources
- Eric Sandeen's solution to kernel version dep

* Wed Apr 09 2008 Steven Whitehouse <swhiteho@redhat.com> 0.1.25.2.02.01-15
- Remove obsolete chkconfig patch for initscript
- Enable parallel make
- Remove obsolete copy of gfs2_ondisk.h (this should be in glibc-kernheaders)

* Wed Apr 09 2008 Steven Whitehouse <swhiteho@redhat.com> 0.1.25.2.02.01-14
- Update URL
- Fix license spec

* Fri Mar 14 2008 Chris Feist <cfeist@redhat.com> 0.1.25.2.02.00-2
- New upstream sources.

* Tue Jan 16 2007 Chris Feist <cfeist@redhat.com> 0.1.24-1
- New upstream sources.
- Resolves: rhbz#222747

* Wed Jan 03 2007 Chris Feist <cfeist@redhat.com> 0.1.24-1
- Updated sources
- Resolves: rhbz#218560

* Thu Dec 21 2006 Chris Feist <cfeist@redhat.com> 0.1.23-1
- Updated sources
- Resolves: rhbz#218560

* Tue Dec 19 2006 Chris Feist <cfeist@redhat.com> 0.1.22-1
- New upstream sources.
- Resolves: rhbz#219878

* Tue Dec 04 2006 Chris Feist <cfeist@redhat.com> 0.1.21-1
- New upstream sources.
- Resolves: rhbz#218134 rhbz#215962

* Thu Nov 30 2006 Chris Feist <cfeist@redhat.com> 0.1.19-1
- New upstream sources.
- Resolves: rhbz#217798

* Wed Nov 29 2006 Chris Feist <cfeist@redhat.com> 0.1.18-1
- New upstream sources.
- Resolves: rhbz#217460

* Thu Oct 26 2006 Chris Feist <cfeist@redhat.com> 0.1.14-1
- New upstream sources.

* Fri Oct 13 2006 Chris Feist <cfeist@redhat.com> 0.1.12-1
- New Upstream sources.

* Fri Oct 13 2006 Chris Feist <cfeist@redhat.com> 0.1.10-1
- New Upstream sources.

* Mon Oct 09 2006 Chris Feist <cfeist@redhat.com> 0.1.9-1
- New Upstream sources.

* Mon Sep 25 2006 Chris Feist <cfeist@redhat.com> 0.1.8-1
- New Upstream sources.

* Wed Sep 13 2006 Chris Feist <cfeist@redhat.com> 0.1.7-1
- New Upstream sources.

* Thu Sep 07 2006 Chris Feist <cfeist@redhat.com> 0.1.6-2
- Fix typo in uninstall script (turn off gfs2 instead of gfs)

* Mon Aug 28 2006 Chris Feist <cfeist@redhat.com> 0.1.6-1
- New Upstream sources.

* Tue Aug 22 2006 Chris Feist <cfeist@redhat.com> 0.1.5-1
- New Upstream sources.

* Mon Aug 14 2006 Chris Feist <cfeist@redhat.com> 0.1.3-0
- New Upstream sources, use dist tag.

* Fri Jul 14 2006 Chris Feist <cfeist@redhat.com>
- Rebuild with updated sources

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com>
- rebuild

* Tue Jun 27 2006 Florian La Roche <laroche@redhat.com>
- fix typo in preun script

* Fri Jun 09 2006 Chris Feist <cfeist@redhat.com> - 0.1.0-1.fc6.3
- Initial build of gfs-utils.
