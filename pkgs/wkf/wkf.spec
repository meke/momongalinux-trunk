%global momorel 5

Name:		wkf
Version:	1.3.11
Release:	%{momorel}m%{?dist}
Summary:	Japanese Kanji code converting filter

Group:		System Environment/Libraries
License:	BSD
URL:		http://www.mysticwall.com/software/wkf/
Source0:	http://www.mysticwall.com/download/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:		wkf-1.3.11-func-declaration.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	groff
Requires:	%{name}-libs = %{version}-%{release}

%description
WKF is a Japanese Kanji code converting filter.


%package	libs
Summary:	Libraries for WKF filter
Group:		System Environment/Libraries

%description	libs
WKF is a Japanese Kanji code converting filter.
This package contains shared libraries for WKF.


%package	devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name}-libs = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q
%patch0 -p1 -b .func_declare

%{__cp} -p /usr/lib/rpm/config.{guess,sub} .

for f in \
	API \
	HISTORY \
	JAPANESE \
	README \
	wkf.1
	do
	iconv -f EUC-JP -t UTF-8 $f > $f.tmp && \
		( touch -r $f $f.tmp ; %{__mv} -f $f.tmp $f ) || \
		%{__rm} -f $f.tmp
done

%build
%configure --disable-static
%{__make} %{?_smp_mflags} \
	CC="%{__cc} %{optflags} -Werror-implicit-function-declaration"


%install
%{__rm} -rf $RPM_BUILD_ROOT

# Use %%makeinstall
%makeinstall \
	INSTALL="%{__install} -p" \
	INSTALL_PROGRAM="%{__install} -p" \
	INSTALL_DATA="%{__install} -p -m 0644" \
	RM="%{__rm} -f"

%{__chmod} 0755 $RPM_BUILD_ROOT%{_libdir}/libwkf*.so.*
find $RPM_BUILD_ROOT -name '*.la' -or -name '*.a' \
	-exec %{__rm} -f {} ';'

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%check
%{__make} test

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc	README

%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*

%files	libs
%defattr(-,root,root,-)
%doc	HISTORY
%doc	TODO

%{_libdir}/libwkf*.so.*

%files	devel
%defattr(-,root,root,-)
%doc	API

%{_includedir}/wkf.h
%{_libdir}/libwkf*.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.11-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.11-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.11-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.11-1m)
- import from Fedora

* Tue Feb 24 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3.11-4
- F-11: Mass rebuild

* Sun Jun  1 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3.11-3
- Explicitly override config.{guess,sub} (due to redhat-rpm-config
  change on F-10)

* Thu Apr  3 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3.11-2
- Fix implicit function declaration

* Sat Feb  9 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp>
- Rebuild against gcc43 (F-9)

* Wed Dec  5 2007 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.3.11-1
- Initial packaging

