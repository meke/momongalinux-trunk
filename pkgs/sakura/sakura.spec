%global         momorel 1

Name:           sakura
Version:        3.0.4
Release:        %{momorel}m%{?dist}
Summary:        Terminal emulator based on GTK and VTE

Group:          User Interface/X
License:        GPLv2
URL:            https://launchpad.net/sakura
Source0:        https://launchpad.net/sakura/trunk/%{version}/+download/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.14
BuildRequires:  vte028-devel >= 0.16.15
BuildRequires:  cmake desktop-file-utils gettext

%description
Sakura is a terminal emulator based on GTK and VTE. It's a terminal emulator 
with few dependencies, so you don't need a full GNOME desktop installed to 
have a decent terminal emulator.


%prep
%setup -q
# fix desktop file (icon is svg instead of png)
sed -i 's!terminal-tango.png!terminal-tango!' sakura.desktop


%build
find . -type f -name CMakeCache.txt -exec rm -rf {} \;
%cmake CMAKE_C_FLAGS=%{optflags} .
make VERBOSE=1 %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
desktop-file-install                                       \
  --vendor=                                                \
  --delete-original                                        \
  --remove-category=Utility                                \
  --add-category=System                                    \
  --dir=${RPM_BUILD_ROOT}%{_datadir}/applications          \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
%find_lang %{name}

rm -f %{buildroot}%{_datadir}/doc/sakura/INSTALL

#%check
#ctest .


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS GPL INSTALL
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/terminal-tango.svg
%{_mandir}/man1/sakura.1*


%changelog
* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-2m)
- fix BuildRequires; use vte028

* Sat Aug 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-2m)
- rebuild for new GCC 4.6

* Sun Feb 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.8-2m)
- full rebuild for mo7 release

* Fri Jun 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.8-1m)
- update to 2.3.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.4-1m)
- import from Fedora 11
- update to 2.3.4

* Tue Jun 09 2009 Christoph Wickert <cwickert@fedoraproject.org> - 2.3.3-3
- Rebuilt for libvte SONAME bump

* Sun Apr 19 2009 Christoph Wickert <cwickert@fedoraproject.org> - 2.3.3-2
- Add patch to honor RPM_OPT_FLAGS
- Include INSTALL in %%doc since it contains some valuable information

* Sat Apr 11 2009 Christoph Wickert <cwickert@fedoraproject.org> - 2.3.3-1
- Update to 2.3.3

* Tue Nov 11 2008 Christoph Wickert <cwickert@fedoraproject.org> - 2.3.2-1
- Update to 2.3.2

* Sat Nov 01 2008 Christoph Wickert <cwickert@fedoraproject.org> - 2.3.1-1
- Update to 2.3.1

* Sun Sep 28 2008 Christoph Wickert <cwickert@fedoraproject.org> - 2.3.0-1
- Update to 2.3.0

* Sun Sep 21 2008 Christoph Wickert <cwickert@fedoraproject.org> - 2.2.1-1
- Update to 2.2.1
- Disable %%check again

* Fri Jun 20 2008 Christoph Wickert <cwickert@fedoraproject.org> - 2.2.0-1
- Update to 2.2.0
- Enable %%check again

* Fri Jun 06 2008 Christoph Wickert <cwickert@fedoraproject.org> - 2.1.2-1
- Update to 2.1.2
- No test configuration, disable %%check temporarily 

* Mon May 12 2008 Christoph Wickert <cwickert@fedoraproject.org> - 2.1.0-1
- Update to 2.1.0

* Mon May 05 2008 Christoph Wickert <cwickert@fedoraproject.org> - 2.0.2-1
- Initial Fedora RPM
