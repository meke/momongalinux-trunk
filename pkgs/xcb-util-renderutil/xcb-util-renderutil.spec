%global momorel 1

Summary: xcb-util-renderutil
Name: xcb-util-renderutil
Version: 0.3.9
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Development/Libraries
URL: http://xcb.freedesktop.org/
Source0: http://xcb.freedesktop.org/dist/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: xcb-util-devel >= 0.3.8

%description
%{name}

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static --enable-silent-rules --enable-devel-docs
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc ChangeLog NEWS README
%{_libdir}/libxcb-render-util.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-, root, root)
%{_includedir}/xcb/xcb_*.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/xcb-renderutil.pc

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.9-1m)
- update 0.3.9

* Tue Jun 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8-1m)
- initial build
