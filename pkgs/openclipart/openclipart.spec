%global momorel 18
%global clip_dir %{_datadir}/openclipart

%global openclipartdir %{clip_dir}
%global ooodir %{_libdir}/openoffice.org3.0
%global ooobasisdir %{ooodir}/basis3.3
%global gal_dir %{ooodir}/share/gallery
%global gal_minimal_number 70

Name:    openclipart
Version: 0.18
Release: %{momorel}m%{?dist}
Summary: Open Clip Art Library

# local configuration
%{?include_specopt}
# Notice: OOo gallery requires openclipart-png
%{?!make_png: %global make_png 1}

License: "Public Domain (Creative Commons)"
Group: Applications/Productivity
URL: http://www.openclipart.org/
Source0: http://www.openclipart.org/downloads/%{version}/openclipart-%{version}-svgonly.tar.bz2
NoSource: 0
Buildarch: noarch
BuildRequires: librsvg2
BuildRequires: libreoffice-core
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Open Clip Art Library is a collection of 100% license-free, royalty-free, and
restriction-free art that you can use for whatever purpose you see fit.

Most of the art in this package is in the Scalable Vector Graphic (SVG)
format, which is an XML format approved by the W3C and used in a wide
range of software applications, including Inkscape, Adobe Illustrator,
Batik, and more.

The goal of the Open Clip Art Library is to provide the public with a
huge collection of reusable art for any purpose

For more information, including how you can contribute to this growing
library, please see http://www.openclipart.org/

Notice: This package contains a directory structure only. The images are in %{name}-png or %{name}-svg package.

%package svg
Summary: Open Clip Art Library (SVG format)
Group: Applications/Productivity
Requires: %{name} = %{version}-%{release}

%description svg
The Open Clip Art Library is a collection of 100% license-free, royalty-free, and
restriction-free art that you can use for whatever purpose you see fit.

The goal of the Open Clip Art Library is to provide the public with a
huge collection of reusable art for any purpose

For more information, including how you can contribute to this growing
library, please see http://www.openclipart.org/

This package includes cliparts of SVG format.

%if %{make_png}
%package png
Summary: Open Clip Art Library (PNG format)
Group: Applications/Productivity
Requires: %{name} = %{version}-%{release}

%description png
The Open Clip Art Library is a collection of 100% license-free, royalty-free, and
restriction-free art that you can use for whatever purpose you see fit.

The goal of the Open Clip Art Library is to provide the public with a
huge collection of reusable art for any purpose

For more information, including how you can contribute to this growing
library, please see http://www.openclipart.org/

This package includes cliparts of PNG format.

%package -n openoffice.org-openclipart
Summary: OpenOffice.org Gallery files for Open Clip Art Library
Group: Applications/Productivity
Requires: %{name}-png = %{version}-%{release}
Requires: openoffice.org-core
Conflicts: openoffice.org-core <= 2.0.4-8m

%description -n openoffice.org-openclipart
The Open Clip Art is a collection of 100% license-free, royalty-free, and
restriction-free art that you can use for whatever purpose you see fit.

The goal of the Open Clip Art Library is to provide the public with a
huge collection of reusable art for any purpose

For more information, including how you can contribute to this growing
library, please see http://www.openclipart.org/

This package contains files needed to use cliparts from OpenOffice.org gallery.

%endif

%prep
%setup -q -n openclipart-%{version}-svgonly

# clean ugly/broken
(cd clipart
 rm -rf ./unsorted
 rm -f	./computer/icons/lemon-theme/devices/automatic \
	./computer/icons/lemon-theme/actions/automatic \
	./computer/icons/lemon-theme/filesystems/automatic \
	./computer/icons/lemon-theme/apps/automatic \
	./computer/icons/lemon-theme/mimetypes/automatic \
	./signs_and_symbols/flags/america/united_states/.usa_hawaii.svg.swp
)

# no brank
(cd clipart
mv recreation/music/CD\ Player recreation/music/CD_Player 
)

%build
unset DISPLAY

# taken from ooo-build's "bin/build-galleries" script

%if %{make_png}

# make png files
(cd clipart
 numtotal=`find ./ -name "*.svg" -type f| wc -l`
# subtract skip files
 numtotal=`expr $numtotal - 4`
 numdone=0
 for pict_svg in `find ./ -name "*.svg" -type f \
     	      	 | grep -v ./recreation/religion/christianity/coat_of_arms_of_anglica_01.svg \
		 | grep -v ./recreation/religion/christianity/trefoil_architectural_e_01.svg \
		 | grep -v ./geography/europe_francesco_rollan_.svg \
		 | grep -v ./geography/europe_francesco_rolland_.svg`; do
    pict_dir=${pict_svg#./}
    pict_dir=${pict_dir%/*}
    pict_png=${pict_svg##*/}
    pict_png=${pict_png%.svg}.png
    mkdir -p build/$pict_dir
    echo "[${numdone}/${numtotal}] Converting $pict_svg to $pict_dir/$pict_png..."
    rsvg-convert $pict_svg -f png -o $pict_dir/$pict_png
    numdone=$(($numdone+1))
done
)
%endif

%install
unset DISPLAY
rm -rf %{buildroot}

mkdir -p %{buildroot}%{clip_dir}
(cd ./clipart
 tar c ./ | tar x -C %{buildroot}%{clip_dir}
)

rm -f %{buildroot}%{clip_dir}/{PASSFAIL,README,TODO}

# remove empty dirs
rm -rf %{buildroot}%{clip_dir}/build

find %{buildroot}%{clip_dir} -name '*bat' -type f -exec rm '{}' \;
find %{buildroot}%{clip_dir} -type f -exec chmod 644 '{}' \;

# for OOo gallery
%if %{make_png}

OOINSTDIR=%{ooodir}
GAL_DIR=%{ooodir}/share/gallery
GAL_BIN=%{ooobasisdir}/program/gengal
# start number for the new galleries
GAL_NUMBER_FROM=%{gal_minimal_number}
mkdir -p %{buildroot}%{gal_dir}

echo "Building extra galleries from openclipart..."
for dir in `find -L %{buildroot}%{openclipartdir} -mindepth 1 -maxdepth 1 -type d | LC_CTYPE=C sort` ; do
	# get the gallery name from the directory name
	# and make the first character uppercase
    gal_name=${dir##*/}
    gal_name=`echo $gal_name | tr "_-" "  "`
    gal_name_rest=${gal_name#?}
    gal_name_first_char=${gal_name%$gal_name_rest}
    gal_name_first_char=`echo $gal_name_first_char | tr "a-z" "A-Z"`
    gal_name=$gal_name_first_char$gal_name_rest
    
    echo "Doing gallery $gal_name..."
	# xargs is necessary because I 
    find $dir -name "*.png" -print0 | LC_CTYPE=C sort -z | { xargs -0 $GAL_BIN --name "$gal_name" --path "%{buildroot}%{gal_dir}" --destdir %{buildroot} --number-from "$GAL_NUMBER_FROM" || exit 1; }
done

%endif

# filelist direcotries
rm -rf %{name}.dirs* %{name}.png.files* %{name}.svg.files*
(find %{buildroot}%{clip_dir}  \
	-type d | sort ) > %{name}.dirs

mv %{name}.dirs %{name}.dirs.in
sed "s|.*%{clip_dir}|%{clip_dir}|" < %{name}.dirs.in | while read file; do
	if [ -d %{buildroot}/${file} ]; then
		echo -n '%dir '
	fi
	echo ${file}
done > %{name}.dirs
%if %{make_png}
# filelist png
(find %{buildroot}%{clip_dir}  \
	-type f -name "*.png" | sort ) > %{name}.png.files

mv %{name}.png.files %{name}.png.files.in
sed "s|.*%{clip_dir}|%{clip_dir}|" < %{name}.png.files.in | while read file; do
	if [ -d %{buildroot}/${file} ]; then
		echo -n '%dir '
	fi
	echo ${file}
done > %{name}.png.files
%endif
# filelist svg
(find %{buildroot}%{clip_dir}  \
	-type f -name "*.svg" | sort ) > %{name}.svg.files

mv %{name}.svg.files %{name}.svg.files.in
sed "s|.*%{clip_dir}|%{clip_dir}|" < %{name}.svg.files.in | while read file; do
	if [ -d %{buildroot}/${file} ]; then
		echo -n '%dir '
	fi
	echo ${file}
done > %{name}.svg.files

%clean
rm -rf %{buildroot}

%files -f %{name}.dirs
%defattr(-,root,root)
%doc README AUTHORS ChangeLog INSTALL LICENSE NEWS
%{clip_dir}/computer/icons/etiquette-theme/AUTHORS
%{clip_dir}/computer/icons/etiquette-theme/COPYING
%{clip_dir}/computer/icons/etiquette-theme/README
%{clip_dir}/computer/icons/lemon-theme/README
%{clip_dir}/computer/icons/lemon-theme/actions/32
%{clip_dir}/special/examples/00_strawberry_border/README
%{clip_dir}/special/examples/01_gift_certificate/README

%files svg -f %{name}.svg.files
%defattr(-,root,root)
%doc LICENSE

%if %{make_png}
%files png -f %{name}.png.files
%defattr(-,root,root)
%doc LICENSE

%files -n openoffice.org-openclipart
%defattr(-,root,root)
%doc LICENSE
%{ooodir}/share/gallery/*
%endif

%changelog
* Sun Jun  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-18m)
- unset DISPLAY to fix a possible build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-17m)
- rebuild for new GCC 4.6

* Mon Feb 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18-16m)
- rebuild against libreoffice

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18-14m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-13m)
- rebuild against openoffice.org-3.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18-11m)
- rebuild against openoffice.org-3.1.1

* Tue Jul 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-10m)
- rebuild against openoffice.org-3.1.0

* Sat Jul 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-9m)
- skip png-convert broken files
-- coat_of_arms_of_anglica_01.svg
-- trefoil_architectural_e_01.svg
-- europe_francesco_rollan_.svg
-- europe_francesco_rolland_.svg

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.18-8m)
- use rsvg-convert

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-7m)
- rebuild against rpm-4.6

* Wed Oct  1 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.18-6m)
- inkscape --without-gui

* Tue May 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18-5m)
- fix Changelog. remove non ASCII char

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.18-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18-3m)
- %%NoSource -> NoSource

* Sat Nov 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.18-2m)
- make OOo gallery files and put them into openoffice.org-clipart subpackage to aboid a potential mismatch of contents in openclipart and OOo gallery index.
- use Xvfb to build on a host not running X.

* Tue Oct 31 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.18-1m)
- initial packaging for Momonga
- - based on the package at Mandriva (clipart-openclipart-0.18-3mdv2007.0.src.rpm)
- png subpackage are required by OOo to build image galleries.

* Thu Sep 14 2006 Giuseppe Ghibo <ghibo@mandriva.com> 0.18-3mdv2007.0
- Rebuild to force package signing... (bug #21436).

* Thu Jan 26 2006 Giuseppe Ghibo <ghibo@mandriva.com> 0.18-2mdk
- Removed empty dirs in %{clip_dir}/build.

* Mon Jan 02 2006 Giuseppe Ghibo <ghibo@mandriva.com> 0.18-1mdk
- Release 0.18.

* Mon Sep 12 2005 Giuseppe Ghibo <ghibo@mandriva.com> 0.17-2mdk
- Bigger .png files for galleries converted using inkscape.

* Sat Sep 10 2005 Giuseppe Ghibo <ghibo@mandriva.com> 0.17-1mdk
- Release 0.17.

* Mon Jul 11 2005 Mandriva Linux Team <http://www.mandrivaexpert.com/> 0.15-1mdk
- New release 0.15

* Tue Feb  1 2005 Frederic Lepied <flepied@mandrakesoft.com> 0.10-1mdk
- New release 0.10

* Mon Jan  3 2005 Frederic Lepied <flepied@mandrakesoft.com> 0.09-1mdk
- New release 0.09

* Fri Oct  1 2004 Frederic Lepied <flepied@mandrakesoft.com> 0.06-1mdk
- initial packaging (spec from AltLinux)

* Thu Jul 01 2004 Vitaly Lipatov <lav@altlinux.ru> 0.04-alt1
- first build for Sisyphus
