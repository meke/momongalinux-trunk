%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global strigiver 0.7.8

Name: kde-dev-utils
Summary: The KDE Software Development Kit (SDK)
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPLv2
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: kdelibs >= %{version}
Requires: kdepimlibs >= %{version}
Requires: kde-workspace >= %{version}
BuildRequires:  kdelibs-devel >= %{version}
BuildRequires:  kdepimlibs-devel >= %{version}
BuildRequires:  kde-workspace-devel >= %{version}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  antlr-tool
BuildRequires:  boost-devel >= 1.50.0
BuildRequires:  flex
BuildRequires:  gettext-devel >= 0.18.2
BuildRequires:  apr-devel
BuildRequires:  libxml2-devel
BuildRequires:  libxslt-devel
BuildRequires:  libtool-ltdl-devel
BuildRequires:  qca2-devel
BuildRequires:  strigi-devel >= %{strigiver}
BuildRequires:  subversion-devel >= 1.6.3-2m
BuildRequires:  antlr-C++
# for libiberty (used by kmtrace for cp_demangle)
# IMPORTANT: check licensing from time to time, currently libiberty is still
#            GPLv2+/LGPLv2+
BuildRequires:  binutils-devel

Requires: %{name}-kmtrace = %{version}-%{release}
Requires: %{name}-kmtrace-libs = %{version}-%{release}
Requires: %{name}-kstartperf = %{version}-%{release}
Requires: %{name}-kuiviewer = %{version}-%{release}
Requires: %{name}-kpartloader = %{version}-%{release}

Obsoletes: kdesdk-kmtrace < %{version}-%{release}
Obsoletes: kdesdk-kmtrace-libs < %{version}-%{release}
Obsoletes: kdesdk-kmtrace-devel < %{version}-%{release}
Obsoletes: kdesdk-kstartperf < %{version}-%{release}
Obsoletes: kdesdk-kuiviewer < %{version}-%{release}
Obsoletes: kdesdk-kpartloader < %{version}-%{release}
Conflicts: kdesdk < 4.10.90

%description
Small utilities for developers using KDE/Qt libs/frameworks.

%package kmtrace
Summary: Assist with malloc debugging using glibc's "mtrace" functionality
Group: Development/Debuggers
Requires: %{name}-kmtrace-libs = %{version}-%{release}

%description kmtrace
%{summary}.

%package kmtrace-libs
Summary: Runtime libraries for kmtrace
Group: System Environment/Libraries
Requires: %{name}-kmtrace = %{version}-%{release}

%description kmtrace-libs
%{summary}.

%package kmtrace-devel
Summary: Developer files for kmtrace
Group: Development/Libraries
Requires: %{name}-kmtrace-libs = %{version}-%{release}

%description kmtrace-devel
%{summary}.

%package kpartloader
Summary: KPart loader
Group: System Environment/Daemons

%description kpartloader
%{summary}.

%package kstartperf
Summary: Startup time measurement tool for KDE applications
Group: Applications/System

%description kstartperf
%{summary}.

%package kuiviewer
Summary: Displays designer UI files
Group: Development/Tools

%description kuiviewer
%{summary}.

%package devel
Group:    Development/Libraries
Summary:  Header files for %{name}

%description devel
Header files for developing applications using %{name}.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post kuiviewer
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%posttrans kuiviewer
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%postun kuiviewer
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
fi

%files
%defattr(-,root,root,-)
%doc COPYING*

%files kmtrace
%defattr(-,root,root,-)
%doc kmtrace/README kmtrace/doc
%{_kde4_bindir}/kmtrace
%{_kde4_bindir}/demangle
%{_kde4_bindir}/kmmatch
%{_kde4_bindir}/kminspector
%{_kde4_appsdir}/kmtrace
%{_mandir}/man1/demangle.1.*

%files kmtrace-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libktrace.so.4*

%files kmtrace-devel
%defattr(-,root,root,-)
%{_kde4_includedir}/ktrace.h
%{_kde4_libdir}/libktrace.so

%files kpartloader
%defattr(-,root,root,-)
%{_kde4_bindir}/kpartloader
%{_kde4_appsdir}/kpartloader/kpartloaderui.rc

%files kstartperf
%defattr(-,root,root,-)
%doc kstartperf/README
%{_kde4_bindir}/kstartperf
%{_kde4_libdir}/kde4/kstartperf.so

%files kuiviewer
%defattr(-,root,root,-)
%{_kde4_bindir}/kuiviewer
%{_kde4_libdir}/kde4/kuiviewerpart.so
%{_kde4_libdir}/kde4/quithumbnail.so
%{_kde4_appsdir}/kuiviewer
%{_kde4_appsdir}/kuiviewerpart
%{_kde4_datadir}/applications/kde4/kuiviewer.desktop
%{_kde4_datadir}/kde4/services/kuiviewer_part.desktop
%{_kde4_datadir}/kde4/services/designerthumbnail.desktop
%{_kde4_iconsdir}/hicolor/*/apps/kuiviewer.*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/kprofilemethod.h

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Mon Dec  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-2m)
- remove Require: kde-dev-utils-kmtrace-devel from kde-dev-utils

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- initial build for Momonga Linux
