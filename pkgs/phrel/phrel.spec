%global momorel 17

Summary: Per Host RatE Limiter
Name: phrel
Version: 0.9.6
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Languages
Source0: ftp://ftp.digitalgenesis.com/pub/phrel/%{name}-%{version}.tar.bz2 
NoSource: 0
URL: http://www.digital-genesis.com/software/phrel/
BuildRequires: libpcap-devel >= 1.1.1
BuildRequires: net-snmp-devel >= 5.7.1 tcp_wrappers
Requires: libpcap >= 0.9.5
Requires: net-snmp >= 5.7.1 tcp_wrappers iptables
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
PHREL is a Per Host RatE Limiter written in C to efficiently track the rate of
incoming traffic on a per host basis and insert a chain into iptables when a
configured threshold is crossed. The inserted chain may either rate limit or
completely block the offending host for a period of time and will be
automatically removed when the offending host's traffic levels return to
normal. PHREL can be used with any type of traffic, but it is particularly
well suited to protecting name servers from random hosts that flood DNS
requests and preventing SSH brute force login attempts. 
# < http://www.digital-genesis.com/software/phrel/
#'

%prep
rm -rf %{buildroot}
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall transform='s,x,x,'

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_sbindir}/*
%{_libdir}/*.a
%{_mandir}/man?/*

%changelog
* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-17m)
- rebuild against net-snmp-5.7.1

* Sun Jul 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.6-16m)
- rebuild against net-snmp-5.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-15m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-14m)
- rebuild against net-snmp-5.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-12m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-11m)
- rebuild against libpcap-1.1.1

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-10m)
- rebuild against net-snmp-5.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-8m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-7m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-6m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-4m)
- %%NoSource -> NoSource

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.6-4m)
- rebuild against net-snmp

* Thu Jun 07 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-3m)
- rebuild against libpcap-0.9.5 (restrict by version)

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-2m)
- rebuild against libpcap-0.9.5

* Sat Nov 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-1m)
- creat
