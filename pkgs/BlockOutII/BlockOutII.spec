%global momorel 6

# Copyright (c) 2007 oc2pus <toni@links2linux.de>
# Copyright (c) 2007 Hans de Goede <j.w.r.degoede@hhs.nl>
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

Name:           BlockOutII
Version:        2.3
Release:        %{momorel}m%{?dist}
Summary:        A free adaptation of the original BlockOut DOS game
Group:          Amusements/Games
License:        GPLv2+
URL:            http://www.blockout.net/blockout2/
# To regenerate:
# cvs -z3 -d:pserver:anonymous@blockout.cvs.sourceforge.net:/cvsroot/blockout co -D 2007-11-25 -P blockout
# mv blockout BlockOutII-2.3
# cd BlockOutII-2.3
# rm -r `find -name CVS` contrib/dxsdk8.rar contrib/D3DTools BlockOut/setup
# rm -r CVSROOT contrib/ImageLib/src/png/png contrib/ImageLib/src/png/zlib
# mp32ogg BlockOut/sounds/music.mp3
# cd ..
# tar cvfj BlockOutII-2.3.tar.bz2 BlockOutII-2.3
Source0:        %{name}-%{version}.tar.bz2
Source1:        %{name}.desktop
Source2:        %{name}.png
Patch0:         BlockOutII-2.3-syslibs.patch
Patch1:         BlockOutII-2.3-64bit.patch
Patch2:         BlockOutII-2.3-bl2Home.patch
Patch3:         BlockOutII-2.3-music.patch
Patch4:         BlockOutII-2.3-restore-resolution.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  SDL_mixer-devel libpng-devel desktop-file-utils
Requires:       hicolor-icon-theme opengl-games-utils

%description
BlockOut II is a free adaptation of the original BlockOut
DOS game edited by California Dreams in 1989. BlockOut II
has the same features than the original game with few graphic
improvements. The score calculation is also nearly similar to
the original game. BlockOut II has been designed by an addicted
player for addicted players. BlockOut II is an open source
project available for both Windows and Linux.

Blockout is a registered trademark of Kadon Enterprises, Inc.,
used by permission for the BlockOut II application by Jean-Luc
Pons.


%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1 -z .fs
chmod -x `find -type f`
iconv -f ISO8859-1 -t UTF8 BlockOut/README.txt > t; mv t BlockOut/README.txt


%build
pushd contrib/ImageLib/src
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS -Dlinux -c" \
    CXXFLAGS="$RPM_OPT_FLAGS -Dlinux -c"
popd

pushd BlockOut_GL
make %{?_smp_mflags} \
    CXXFLAGS="$RPM_OPT_FLAGS -Dlinux `sdl-config --cflags` -I../contrib/ImageLib/src -c" \
    SDL_ROOT=%{_prefix} LIBS="-L../contrib/ImageLib/src -lpng -lz" \
    IMGLIB_ROOT=../contrib/ImageLib/src
popd


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/images
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/sounds

install -m 755 BlockOut_GL/blockout $RPM_BUILD_ROOT%{_bindir}/%{name}
ln -s opengl-game-wrapper.sh $RPM_BUILD_ROOT%{_bindir}/%{name}-wrapper
install -p -m 644 BlockOut/images/* $RPM_BUILD_ROOT%{_datadir}/%{name}/images
install -p -m 644 BlockOut/sounds/* $RPM_BUILD_ROOT%{_datadir}/%{name}/sounds

# below is the desktop file and icon stuff.
mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications
desktop-file-install --vendor=                  \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  %{SOURCE1}
mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/32x32/apps
install -p -m 644 %{SOURCE2} \
  $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/32x32/apps


%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%files
%defattr(-,root,root,-)
%doc BlockOut/README.txt
%{_bindir}/%{name}*
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-2m)
- remove fedora from --vendor

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.3-5
- Autorebuild for GCC 4.3

* Sat Dec  1 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 2.3-4
- Use opengl-games-utils wrapper to show error dialog when DRI is missing

* Thu Nov 29 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 2.3-3
- Fix restoration of resolution when leaving fullscreen
- Don't use macros in cvs co instructions

* Mon Nov 26 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 2.3-2
- Add missing libpng-devel BR (bz 398791)
- Add include date in CVS tarbal reproduction instructions (bz 398791)

* Sat Nov 24 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 2.3-1
- Initial Fedora Package based on the packman package
