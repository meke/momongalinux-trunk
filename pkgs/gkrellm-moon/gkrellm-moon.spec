%global momorel 6
%global gkplugindir %{_libdir}/gkrellm2/plugins

Summary: Moon clock plugin for GKrellM
Name: gkrellm-moon
Version: 0.6
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://gkrellmoon.sourceforge.net/
Source: http://downloads.sf.net/gkrellmoon/gkrellmoon-%{version}.tar.gz
Patch0: gkrellmoon-0.6-strcpy.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gkrellm >= 2.2.0
BuildRequires: gkrellm-devel >= 2.2.0
BuildRequires: gkrellm >= 2.2.0

%description
A moon clock plugin for GKrellM.


%prep
%setup -q -n gkrellmoon-%{version}
%patch0 -p1 -b .strcpy


%build
%{__make} FLAGS='%{optflags} -fPIC $(GTK_INCLUDE)'


%install
%{__rm} -rf %{buildroot}
%{__install} -D -m 0755 gkrellmoon.so %{buildroot}%{gkplugindir}/gkrellmoon.so


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%{gkplugindir}/gkrellmoon.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-2m)
- rebuild against rpm-4.6

* Mon Apr 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.6-6
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 Matthias Saou <http://freshrpms.net/> 0.6-5
- Rebuild for new BuildID feature.

* Sun Aug  5 2007 Matthias Saou <http://freshrpms.net/> 0.6-4
- Update License field... GPLv2 only, will be a problem with GPLv3 gkrellm.

* Fri Jun 22 2007 Matthias Saou <http://freshrpms.net/> 0.6-3
- Remove dist, as it might be a while before the next rebuild.
- Switch to using downloads.sf.net source URL.

* Wed Feb 14 2007 Matthias Saou <http://freshrpms.net/> 0.6-2
- Tweak defattr.

* Mon Feb 12 2007 Matthias Saou <http://freshrpms.net/> 0.6-1
- Initial RPM release as a single plugin.
- Include trivial patch to remove strcpy related warnings.

