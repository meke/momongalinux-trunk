%global momorel 8
%global srcname kdewebdev
%global qtver 3.3.7
%global kdever 3.5.10
%global kde4ver 4.4.3
%global kdelibsrel 6m
%global kdesdkrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global ftpdirver %{kdever}
%global sourcedir stable/%{ftpdirver}/src
%global enable_final 1
%global enable_gcc_check_and_hidden_visibility 0

Summary: Web development tools for KDE
Name: kdewebdev3
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Tools
URL: http://kdewebdev.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{srcname}-%{version}.tar.bz2 
NoSource: 0
# document
Source1: http://dl.sourceforge.net/sourceforge/%{srcname}/css.tar.bz2
Source2: http://dl.sourceforge.net/sourceforge/%{srcname}/html.tar.bz2
Source3: http://dl.sourceforge.net/sourceforge/%{srcname}/php_manual_en_20030401.tar.bz2
Source4: http://dl.sourceforge.net/sourceforge/%{srcname}/javascript.tar.bz2
Source5: hi48-app-kxsldbg.png
Source6: quantarc
Patch0: javascript.patch
Patch1: %{srcname}-3.5.4-kxsldbg-icons.patch
Patch2: %{srcname}-3.5.9-automake111.patch
Patch3: %{srcname}-%{version}-autoconf265.patch
Patch4: %{srcname}-%{version}-no-docbParseFile.patch

# upstream patches
# no patch to be applied

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: quanta
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: tidy
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
# keep...
BuildRequires: kdesdk-devel >= %{kde4ver}-%{kdesdkrel}
BuildRequires: libxml2-devel >= 2.6
BuildRequires: libxslt-devel

%description
Web development tools for the K Desktop Environment, including

  quanta, an HTML editor 
  kfilereplace, multi file mutli line find and replace
  klinkstatus, 
  kimagemapeditor, a imagemap editor
  kommander, a dialog builder and executor
  kxsldbg, etc.
  
Especially, Quanta Plus (http://quanta.sourceforge.net/) is an HTML editor 
for the K Desktop Environment. This program is designed for quick Web 
development. Its objective is to produce a complete Web development 
environment for active development with a focus on supporting professional 
level development. This means full HTML 4.0 support including cascading style 
sheets and lots of time saving features. 

%prep
%setup -q -n %{srcname}-%{version} -a 1 -a 2 -a 3 -a 4

%patch0 -p0 -b .javascript
%patch1 -p1 -b .kxsldbg-icons
%patch2 -p1 -b .automake111
%patch3 -p1 -b .autoconf265
%patch4 -p1 -b .no-docbParseFile

# upstream patches
# no patch to be applied

install -m 644 %{SOURCE5} kxsldbg/

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

export CFLAGS="%{optflags} -DNDEBUG -UDEBUG -D_GNU_SOURCE"
export CXXFLAGS="$CFLAGS -fno-use-cxa-atexit -fpermissive"

./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--includedir=%{_includedir}/kde \
	--mandir=%{_mandir} \
	--with-qt-dir=%{qtdir} \
	--with-qt-includes=%{qtdir}/include \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--with-xinerama \
	--with-kommander \
	--disable-debug \
%if %{enable_final}
	--enable-final \
%endif
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags} KDE_NO_UNDEFINED="-Wl,--allow-shlib-undefined"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make transform='s,x,x,' DESTDIR=%{buildroot} install

# install quantarc
mkdir -p %{buildroot}%{_datadir}/config
install -m 644 %{SOURCE6} %{buildroot}%{_datadir}/config/

# install docs
for i in css html javascript ; do
   pushd $i
   ./install.sh <<EOF
%{buildroot}%{_datadir}/apps/quanta/doc
EOF
   popd
   rm -rf $i
done

cp -R php php.docrc %{buildroot}%{_datadir}/apps/quanta/doc

## link missing icons for menu

# kommander.png
ln -s ../../../crystalsvg/16x16/apps/kommander.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/kommander.png
ln -s ../../../crystalsvg/22x22/apps/kommander.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/kommander.png
ln -s ../../../crystalsvg/32x32/apps/kommander.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/kommander.png
ln -s ../../../crystalsvg/64x64/apps/kommander.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/kommander.png

# fix dangling symlinks
pushd %{buildroot}%{_docdir}/HTML/en
for i in *; do
   if [ -d $i -a -L $i/common ]; then
      rm -f $i/common
      ln -s ../common $i
   fi
done
popd

# remove broken desktop file
rm -f %{buildroot}%{_datadir}/applnk/.hidden/kmdr-executor.desktop

# remove to avoid conflicting with kdewebdev-4.4.1
rm -f %{buildroot}%{_bindir}/kfilereplace
rm -f %{buildroot}%{_bindir}/kimagemapeditor
rm -f %{buildroot}%{_bindir}/klinkstatus
rm -f %{buildroot}%{_libdir}/libkommanderwidgets.so
rm -rf %{buildroot}%{_datadir}/apps/kfilereplace
rm -f %{buildroot}%{_datadir}/apps/kfilereplacepart/icons/crystalsvg/22x22/actions/backup_option.png
rm -f %{buildroot}%{_datadir}/apps/kfilereplacepart/icons/crystalsvg/22x22/actions/casesensitive_option.png
rm -f %{buildroot}%{_datadir}/apps/kfilereplacepart/icons/crystalsvg/22x22/actions/command_option.png
rm -f %{buildroot}%{_datadir}/apps/kfilereplacepart/icons/crystalsvg/22x22/actions/filesimulate.png
rm -f %{buildroot}%{_datadir}/apps/kfilereplacepart/icons/crystalsvg/22x22/actions/invert.png
rm -f %{buildroot}%{_datadir}/apps/kfilereplacepart/icons/crystalsvg/22x22/actions/recursive_option.png
rm -f %{buildroot}%{_datadir}/apps/kfilereplacepart/icons/crystalsvg/22x22/actions/regularexpression_option.png
rm -f %{buildroot}%{_datadir}/apps/kfilereplacepart/kfilereplacepartui.rc
rm -f %{buildroot}%{_datadir}/apps/kimagemapeditor/addpointcursor.png
rm -f %{buildroot}%{_datadir}/apps/kimagemapeditor/freehandcursor.png
rm -f %{buildroot}%{_datadir}/apps/kimagemapeditor/kimagemapeditorpartui.rc
rm -f %{buildroot}%{_datadir}/apps/kimagemapeditor/kimagemapeditorui.rc
rm -f %{buildroot}%{_datadir}/apps/kimagemapeditor/polygoncursor.png
rm -f %{buildroot}%{_datadir}/apps/kimagemapeditor/removepointcursor.png
rm -rf %{buildroot}%{_datadir}/apps/klinkstatus
rm -rf %{buildroot}%{_datadir}/apps/klinkstatuspart
rm -rf %{buildroot}%{_docdir}/HTML/en/kfilereplace
rm -rf %{buildroot}%{_docdir}/HTML/en/klinkstatus
rm -f %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/klinkstatus.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/kimagemapeditor.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/klinkstatus.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/kfilereplace.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/klinkstatus.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/kfilereplace.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/kimagemapeditor.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/klinkstatus.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/kfilereplace.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/kimagemapeditor.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/klinkstatus.png
rm -f %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/klinkstatus.png

# desktop files have no executable files
rm -f %{buildroot}%{_datadir}/applications/kde/kfilereplace.desktop
rm -f %{buildroot}%{_datadir}/applications/kde/kimagemapeditor.desktop
rm -f %{buildroot}%{_datadir}/applications/kde/klinkstatus.desktop

# kcfg file has no executable files
rm -f %{buildroot}%{_datadir}/config.kcfg/klinkstatus.kcfg

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL* NEWS PACKAGING README TODO
%{_bindir}/kmdr-editor
%{_bindir}/kmdr-executor
%{_bindir}/kmdr-plugins
%{_bindir}/kxsldbg
%{_bindir}/quanta
%{_bindir}/xsldbg
%{_libdir}/kde3/*.la
%{_libdir}/kde3/*.so
%{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/*.so.??*
%{_datadir}/applications/kde/*.desktop
%{_datadir}/apps/kafkapart
%{_datadir}/apps/katepart/syntax/kommander.xml
%{_datadir}/apps/katepart/syntax/kommander-new.xml
%{_datadir}/apps/kdevappwizard/kommanderplugin.png
%{_datadir}/apps/kdevappwizard/kommanderplugin.tar.gz
%{_datadir}/apps/kdevappwizard/templates/kommanderplugin.kdevtemplate
%{_datadir}/apps/kfilereplacepart/icons/crystalsvg/22x22/actions/*.png
%{_datadir}/apps/kimagemapeditor/icons
%{_datadir}/apps/kmdr-editor
%{_datadir}/apps/kommander
%{_datadir}/apps/kxsldbg
%{_datadir}/apps/kxsldbgpart
%{_datadir}/apps/quanta
%{_datadir}/config/quantarc
%{_datadir}/doc/HTML/en/kommander
%{_docdir}/HTML/en/kxsldbg
%{_datadir}/doc/HTML/en/quanta
%{_docdir}/HTML/en/xsldbg
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/mimelnk/application/*.desktop
%{_datadir}/services/*.desktop
%{_datadir}/servicetypes/*.desktop

%{_includedir}/kde/*.h
%{_libdir}/*.so.?

%changelog
* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-7m)
- fix link error

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-6m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-5m)
- add -fpermissive for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.10-3m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-2m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Sun Mar 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-1m)
- update to KDE 3.5.10
- revive kxsldbg
- License: GPLv2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-5m)
- remove %%{_datadir}/doc/HTML/en/quanta, conflicting with kdewebdev-4.2.87

* Tue May 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-4m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-3m)
- rebuild against rpm-4.6

* Thu Sep  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-2m)
- add link of missing icon for menu entry

* Sun Aug  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-1m)
- revive for quanta
- http://d.hatena.ne.jp/lugia/20080803/1217729537

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.8-2m)
- %%NoSource -> NoSource

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Sun Jul 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-3m)
- add Requires: tidy for Momonga Linux 4 beta2

* Thu Jul  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- import hi48-app-kxsldbg.png and kxsldbg-icons.patch from Fedora
 +* Fri Aug 25 2006 Than Ngo <than@redhat.com> 6:3.5.4-2
 +- fix #203893, add missing icon for kxsldbg

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against kdelibs etc.

* Wed Jan 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete unused upstream patches

* Fri Jan 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-7m)
- import upstream patches to fix following problems
  #135524, "scrolling" tabs in CSS-editor no possible
  #137186, Even if ftp fails active seems the ftp one
  #134534, Cannot close image tab

* Wed Jan 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-6m)
- import upstream patch to following problem
  #125145, kimagemap editor crashes when I start it

* Thu Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-5m)
- import upstream patch to fix following problem
  #139548, Mismatch in HTML areas properties UI between the onclick and the ondblclick javascript fields

* Sat Dec 23 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- import upstream patch to fix following problem
  #138343, File open dialog doesn't use standard encoding set in options

* Wed Dec 20 2006 NARITA Koichi <pulasr@momonga-linux.org>
- (3.5.5-3m)
- import upstream patch to fix following problem
  #135203, Wrong Button-Text on Upload-Projekct-File-Dialog

* Sun Dec 10 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- import upstream patch to fix "crash editing <style> CSS" bug

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-2m)
- rebuild against kdelibs-3.5.4-3m kdesdk-3.5.4-4m

* Fri Aug 18 2006 Nishio Futoshi <futoshi@momonga-linix.org>
- (3.5.4-2m)
- rebuild against kdelibs-3.5.4-2m
- rebuild against kdesdk-3.5.4-3m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-3m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Fri Dec  2 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.5.0-2m)
- revise kmdr-executor.desktop for GNOME
- BuildPreReq: desktop-file-utils

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- remove gcc4.patch

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3
- remove kdewebdev-3.4.2-uic.patch

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-3m)
- import kdewebdev-3.4.2-uic.patch from Fedora Core devel
 +* Thu Sep 22 2005 Than Ngo <than@redhat.com> 6:3.4.2-2
 +- fix uic build problem

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure
- BuildPreReq: kdesdk-devel

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- import gcc4.patch from Fedora Core
- add %%doc

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1
- remove post-3.4.0-kdewebdev-kommander-fixed.diff

* Fri May  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- [SECURITY] update Patch20: post-3.4.0-kdewebdev-kommander(-fixed).diff
  http://kde.org/info/security/advisory-20050504-1.txt

* Sat Apr 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-2m)
- [SECURITY] post-3.4.0-kdewebdev-kommander.diff for "Kommander untrusted code execution"
  http://kde.org/info/security/advisory-20050420-1.txt
- modify %%files section

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1
 
* Thu Oct 14 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- add KDE_NO_UNDEFINED="-Wl,--allow-shlib-undefined" to build with gcc-3.4

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0
- import Patch1 from Fedora Core

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release
 
* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- install document of html, php, javascript and css
  (Source1-4 and Patch1)
- temporaly omit Patch0: momonga-quanta-3.1.1-preview-browser.patch (unappliable)

* Tue Jan 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Fri Sep 26 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- import to momonga
    HTML Editor
- momonga-quanta-3.1.1-preview-browser.patch(Patch0)
    use mozilla and w3m instead of netscap and lynx as preview browser
