%global momorel 5

Summary: NIS (or YP) client programs
Name: yp-tools
Version: 2.12
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Base
Source0: ftp://ftp.kernel.org/pub/linux/utils/net/NIS/yp-tools-%{version}.tar.bz2
Url: http://www.linux-nis.org/nis/yp-tools/
Obsoletes: yppasswd, yp-clients
Requires: ypbind

%description
The Network Information Service (NIS) is a system which provides
network information (login names, passwords, home directories, group
information) to all of the machines on a network.  NIS can enable
users to login on any machine on the network, as long as the machine
has the NIS client programs running and the user's password is
recorded in the NIS passwd database.  NIS was formerly known as Sun
Yellow Pages (YP).

This package's NIS implementation is based on FreeBSD's YP and is a
special port for glibc 2.x and libc versions 5.4.21 and later.  This
package only provides the NIS client programs.  In order to use the
clients, you'll need to already have an NIS server running on your
network. An NIS server is provided in the ypserv package.

Install the yp-tools package if you need NIS client programs for machines
on your network.  You will also need to install the ypbind package on
every machine running NIS client programs.  If you need an NIS server,
you'll need to install the ypserv package on one machine on the network.

%prep
%setup -q

%build
%configure --disable-domainname
%make

%install
rm -rf %{buildroot}
make DESTDIR="%{buildroot}" install

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING README ChangeLog NEWS etc/nsswitch.conf
%doc THANKS TODO
/usr/bin/*
%{_mandir}/*/*
/usr/sbin/*
/var/yp/nicknames

%changelog
* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-5m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12-1m)
- update to 2.12

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9-2m)
- rebuild against gcc43

* Wed Nov  9 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.9-1m)
- up to 2.9

* Sun Oct 26 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (2.8-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- use %%{momorel}

* Thu Jan 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.8-1m)
- minor bugfixes

* Thu May 23 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.7-2k)
  update to 2.7

* Tue Jan 22 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (2.6-2k)
- update to 2.6

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (2.5-4k)
- nigirisugi /var/yp

* Wed Oct  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.5-2k)
- update to 2.5 and merge rh 2.5-1

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.4-1).

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Thu Feb 03 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed
- version 2.4

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-yp-tools-20000115

* Tue Nov 30 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-yp-tools-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Oct 26 1999 Bill Nottingham <notting@redhat.com>
- get rid of bogus messages.

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- patched /var/yp/nicknames so that hosts resolves to hosts.byname,
- not hosts.byaddr (bug # 2389)

* Sun May 30 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.3.

* Fri Apr 16 1999 Cristian Gafton <gafton@redhat.com>
- version 2.2
- make it obsolete older yp-clients package

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2/1
- version 2.1
- require ypbind

* Fri Jun 12 1998 Aron Griffis <agriffis@coat.com>
- upgraded to 2.0

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Apr 13 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.4.1

* Thu Dec 04 1997 Cristian Gafton <gafton@redhat.com>
- put yppasswd again in the package, 'cause it is the right thing to do
  (sorry djb!)
- obsoletes old, unmaintained yppasswd package

* Sat Nov 01 1997 Donnie Barnes <djb@redhat.com>
- removed yppasswd from this package.

* Fri Oct 31 1997 Donnie Barnes <djb@redhat.com>
- pulled from contrib into distribution (got fresh sources).  Thanks
  to Thorsten Kukuk <kukuk@vt.uni-paderborn.de> for the original.
- used fresh sources
