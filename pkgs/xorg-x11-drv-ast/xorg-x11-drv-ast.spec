%global momorel 1

%global tarball xf86-video-ast
%global moduledir %(pkg-config xorg-server --variable=moduledir )
%global driverdir	%{moduledir}/drivers

Summary:   Xorg X11 ast video driver
Name:      xorg-x11-drv-ast
Version:   0.98.0
Release:   %{momorel}m%{?dist}
URL:       http://www.x.org
License:   MIT
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0:   %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2
NoSource:  0

ExcludeArch: s390 s390x

BuildRequires: xorg-x11-server-sdk >= 1.13.0

Requires:  hwdata
Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 ast video driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
%configure --disable-static --disable-dri
%make

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# FIXME: Remove all libtool archives (*.la) from modules directory.  This
# should be fixed in upstream Makefile.am or whatever.
find %{buildroot} -regex ".*\.la$" | xargs rm -f --

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/ast_drv.so

%changelog
* Mon Sep 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.00-1m)
- update to 0.98.00

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97.00-1m)
- update to 0.97.00

* Tue May  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.95.00-1m)
- update to 0.95.00

* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.93.10-1m)
- update to 0.93.10
- comment out patch0

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org>
- (0.93.9-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.93.9-1m)
- update to 0.93.9

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.91.10-2m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.91.10-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.91.10-1m)
- update to 0.91.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.89.9-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.89.9-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.89.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.89.9-2m)
- rebuild against xorg-x11-server-1.7.1

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.89.9-1m)
- update 0.89.9

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.89.8-1m)
- update 0.89.8

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.88.8-2m)
- rebuild against xorg-x11-server 1.6

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.88.8-1m)
- update 0.88.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.85.0-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.85.0-1m)
- Initial commit Momonga Linux

* Thu Mar 20 2008 Dave Airlie <airlied@redhat.com> 0.85.0-1
- Latest upstream release

* Mon Mar 10 2008 Dave Airlie <airlied@redhat.com> 0.81.0-8
- pciaccess conversion

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.81.0-7
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 Adam Jackson <ajax@redhat.com> - 0.81.0-6
- Rebuild for PPC toolchain bug

* Mon Jun 18 2007 Adam Jackson <ajax@redhat.com> 0.81.0-5
- Update Requires and BuildRequires.  Disown the module directories.  Add
  Requires: hwdata.

* Fri Feb 16 2007 Adam Jackson <ajax@redhat.com> 0.81.0-4
- ExclusiveArch -> ExcludeArch

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.81.0-3
- rebuild

* Sat Jun 17 2006 Mike A. Harris <mharris@redhat.com> 0.81.0-2
- Update ast.xinf with PCI ID list from driver.

* Sat Jun 17 2006 Mike A. Harris <mharris@redhat.com> 0.81.0-1
- Initial spec file for ast cloned from mga video driver spec version 1.4.1-3
