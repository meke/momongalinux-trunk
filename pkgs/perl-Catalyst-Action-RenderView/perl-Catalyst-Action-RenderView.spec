%global         momorel 13

Name:           perl-Catalyst-Action-RenderView
Version:        0.16
Release:        %{momorel}m%{?dist}
Summary:        Sensible default end action
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Action-RenderView/
Source0:        http://www.cpan.org/authors/id/B/BO/BOBTFISH/Catalyst-Action-RenderView-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Catalyst-Runtime >= 5.90007
BuildRequires:  perl-Data-Visitor >= 0.24
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTTP-Request-AsCGI
BuildRequires:  perl-MRO-Compat
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-Catalyst-Runtime >= 5.90007
Requires:       perl-Data-Visitor >= 0.24
Requires:       perl-MRO-Compat
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This action implements a sensible default end action, which will forward to
the first available view, unless $c->res->status is a 3xx code
(redirection, not modified, etc.), 204 (no content), or $c->res->body has
already been set. It also allows you to pass dump_info=1 to the url in
order to force a debug screen, while in debug mode.

%prep
%setup -q -n Catalyst-Action-RenderView-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/Action/RenderView.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-13m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-12m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-11m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-10m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-9m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-6m)
- rebuild against perl-5.16.0

* Tue Jan 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-5m)
- enable test

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sat Apr 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-2m)
- rebuild for new GCC 4.6

* Wed Jan  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-5m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-4m)
- update BuildRequires:  perl-Data-Visitor >= 0.24

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-3m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-2m)
- rebuild against perl-5.10.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Thu Feb 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08-2m)
- rebuild against rpm-4.6

* Sat May  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.07-2m)
- rebuild against gcc43

* Sat Sep  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Tue Jul 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05
- use Makefile.PL to build package

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.04-3m)
- use vendor

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.04-2m)
- delete dupclicate directory

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.04-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
