%global momorel 1

Summary: Momonga Linux background images
Name: momonga-backgrounds-extras
Version: 8.0
Release: %{momorel}m%{?dist}
Source0: %{name}-%{version}.tar.bz2
License: "Momonga Project"
Group: User Interface/Desktops
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# for macros.kde4
BuildRequires: cmake
BuildRequires: coreutils

%description
Momonga Linux background image package.

%package kde
Summary: Momonga Linux extra background images for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
# for directory ownership
Requires: homura-backgrounds-kde >= 8.0.0-1m

%description kde
This package contains Momonga Linux extra background images for KDE.

%prep
%setup -q

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/backgrounds/images
cp -f images/*.png %{buildroot}%{_datadir}/backgrounds/images
cp -f images/*.jpg %{buildroot}%{_datadir}/backgrounds/images

mkdir -p %{buildroot}/%{_datadir}/gnome-background-properties/
cp -f gnome-background-properties/*.xml %{buildroot}/%{_datadir}/gnome-background-properties/

mkdir -p %{buildroot}%{_datadir}/backgrounds/momonga-extras
cp -f momonga-extras/*.xml %{buildroot}/%{_datadir}/backgrounds/momonga-extras/

# links and metadatas for KDE
mkdir -p %{buildroot}%{_datadir}/wallpapers/{Momonga,Momonga_Fly_High}/contents/images

# Momonga Theme
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1024x576.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1024x600.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1024x768.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1280x1024.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1280x800.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1400x1050.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1440x900.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1600x1200.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1680x1050.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1920x1080.png
ln -s ../../../../backgrounds/images/mo_1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/1920x1200.png
ln -s ../../../../backgrounds/images/mo_800x600.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/images/800x600.png
ln -s ../../../backgrounds/images/momonga_screenshot.png %{buildroot}%{_datadir}/wallpapers/Momonga/contents/screenshot.png
install -m 644 images/momonga_metadata.desktop %{buildroot}%{_datadir}/wallpapers/Momonga/metadata.desktop

# Momonga Fly High Theme
ln -s ../../../../backgrounds/images/mo_flyhigh-1024x576.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1024x576.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1024x600.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1024x600.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1024x768.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1024x768.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1280x1024.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1280x1024.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1280x800.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1280x800.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1600x1200.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1400x1050.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1440x900.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1440x900.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1600x1200.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1600x1200.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1680x1050.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1680x1050.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1920x1080.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1920x1080.png
ln -s ../../../../backgrounds/images/mo_flyhigh-1920x1200.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/1920x1200.png
ln -s ../../../../backgrounds/images/mo_flyhigh-800x600.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/images/800x600.png
ln -s ../../../backgrounds/images/momonga_flyhigh_screenshot.png %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/contents/screenshot.png
install -m 644 images/momonga_flyhigh_metadata.desktop %{buildroot}%{_datadir}/wallpapers/Momonga_Fly_High/metadata.desktop

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING Credits
%{_datadir}/backgrounds/images/*
%{_datadir}/backgrounds/momonga-extras/*
%{_datadir}/gnome-background-properties/*

%files kde
%defattr(-, root, root)
%{_kde4_datadir}/wallpapers/Momonga
%{_kde4_datadir}/wallpapers/Momonga_Fly_High

%changelog
* Sun Feb 12 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (8.0-1m)
- Momonga Linux 8
- add Momonga Linux 7 (natsuki) images

* Fri Feb 10 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-8m)
- change backgrounds package name

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-6m)
- rebuild for new GCC 4.5

* Thu Sep  9 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (7.0-5m)
- modify image Credits file

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-4m)
- full rebuild for mo7 release

* Wed Aug 25 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (7.0-3m)
- modify GNOME Desktop properties file

* Wed Aug 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-2m)
- add a package kde, it contains image-links for KDE

* Wed Aug 18 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (7.0-1m)
- Momonga Linux 7
- add old images

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (6.0-1m)
- Momonga Linux 6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0-3m)
- rebuild against rpm-4.6

* Sun Aug 31 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.0-2m)
- fix %%files

* Thu Aug 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (5.0-1m)
- Create
- Momonga Linux 5
