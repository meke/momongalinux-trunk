%global momorel 11
%global kdever 4.5.1
%global qtver 4.7.0
%global qtrel 0.2.1m
%global cmakever 2.6.4
%global cmakerel 1m

Name:           kffmpegthumbnailer
Version:        1.1.0
Release:        %{momorel}m%{?dist}
Summary:        a video thumbnailer for kde based on ffmpegthumbnailer
Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://code.google.com/p/ffmpegthumbnailer/
Source0:        http://ffmpegthumbnailer.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  ffmpeg-devel >= 2.0.0
BuildRequires:  ffmpegthumbnailer-devel >= 2.0.6
BuildRequires:  libjpeg-devel >= 8a

%description
kffmpegthumbnailer is a video thumbnailer for kde based on ffmpegthumbnailer. 
The thumbnailer uses ffmpeg decode frames from the video files, so supported 
videoformats depend on the configuration flags of ffmpeg.

This thumbnailer was designed to be as fast and lightweight as possible.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYRIGHT Changelog INSTALL README
%{_kde4_libdir}/kde4/kffmpegthumbnailer.so
%{_kde4_datadir}/kde4/services/kffmpegthumbnailer.desktop

%changelog
* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-11m)
- rebuild against ffmpeg-0.7.0

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-10m)
- rebuild against ffmpeg-0.7.0-0.20110705

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-9m)
- rebuild for ffmpeg

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-7m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-5m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-4m)
- rebuild against qt-4.6.3-1m

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-3m)
- rebuild against libjpeg-8a

* Sat Jan  9 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.0-2m)
- add BR ffmpegthumbnailer-devel version

* Thu Jan  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- initial build for Momonga Linux
