%global         momorel 27

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           clearsilver
Version:        0.10.5
Release:        %{momorel}m%{?dist}
Summary:        Fast and powerful HTML templating system
Group:          Development/Libraries
License:        "Neotonic ClearSilver Software License"
URL:            http://www.clearsilver.net/
Source0:        http://www.clearsilver.net/downloads/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-fedora.patch
Patch1:		%{name}-%{version}-regression.patch
Patch2:		%{name}-%{version}-ruby19.patch
Patch10:        %{name}-%{version}-CVE-2011-4357.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  zlib-devel
BuildRequires:  python-setuptools-devel >= 0.6c9-2m
Provides:       %{name}-devel = %{version}-%{release}

%description
ClearSilver is a fast, powerful, and language-neutral HTML template
system.  In both static content sites and dynamic HTML applications,
it provides a separation between presentation code and application
logic which makes working with your project easier.  The design of
ClearSilver began in 1999, and evolved during its use at onelist.com,
egroups.com, and Yahoo! Groups.  Today many other projects and
websites are using it.

%package        devel
Summary:        ClearSilver development package
Group:          Development/Libraries

%description devel
This package provides needed files to develop extensions
to ClearSilver.

%package     -n python-%{name}
Summary:        Python interface to the ClearSilver HTML templating system
Group:          Development/Libraries
BuildRequires:  python-devel >= 2.7
Provides:       %{name}-python = %{version}-%{release}

%description -n python-%{name}
%{summary}.

%package     -n perl-%{name}
Summary:        Perl interface to the ClearSilver HTML templating system
Group:          Development/Libraries
BuildRequires:  perl >= 5.12.0
Requires:  perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Provides:       %{name}-perl = %{version}-%{release}

%description -n perl-%{name}
%{summary}.

%package     -n ruby-%{name}
Summary:        Ruby interface to the ClearSilver HTML templating system
Group:          Development/Libraries
BuildRequires:  ruby >= 1.8.6-4m
BuildRequires:  ruby-devel >= 1.8.6-4m
Provides:       %{name}-ruby = %{version}-%{release}

%description -n ruby-%{name}
%{summary}.

%if 0
# %%ifarch %{ix86} ppc
%package     -n java-%{name}
Summary:        Java interface to the ClearSilver HTML templating system
Group:          Development/Libraries
BuildRequires:  java-devel
Provides:       %{name}-java = %{version}-%{release}
%endif

%if 0
# %%ifarch %{ix86} ppc 
%description -n java-%{name}
%{summary}.
%endif

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1 -b .ruby19
%patch10 -p1 -b .CVE-2011-4357

touch configure
sed -i 's|/neo/opt/bin/python|%{__python}|' python/examples/*/*.py
sed -i 's|PYTHON_SITE = @PYTHON_SITE@|PYTHON_SITE = %{python_sitearch}|' rules.mk.in
find python/examples -type f | xargs chmod -x
# perl -pi -e "s|config -- |config --site-ruby=%{ruby_libdir} --so-dir=%{ruby_archdir} -- |" ruby/Makefile

%build
# %%configure \
#  --with-java=%{_libdir}/jvm/java \
#  --with-python=%{__python} \
#  --disable-csharp 
%configure \
  --with-python=%{__python} \
  --disable-java \
  --disable-csharp 
cd perl && %{__perl} Makefile.PL INSTALLDIRS=vendor && cd ..
make %{?_smp_mflags} OPTIMIZE="$RPM_OPT_FLAGS"

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PREFIX=%{_prefix}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -type f -name perllocal.pod -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
find %{buildroot} -type d -depth -exec rmdir {} 2>/dev/null ';'
chmod -R u+w %{buildroot}/*

%if 0
# %ifarch %{ix86} ppc 
install -dm 755 %{buildroot}%{_libdir}/java
mv %{buildroot}%{_libdir}{,/java}/libclearsilver-jni.so
mv %{buildroot}%{_libdir}/clearsilver.jar \
  %{buildroot}%{_libdir}/java/clearsilver-%{version}.jar
chmod 644 %{buildroot}%{_libdir}/java/clearsilver-%{version}.jar
ln -s clearsilver-%{version}.jar %{buildroot}%{_libdir}/java/clearsilver.jar
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CS_LICENSE INSTALL LICENSE README
%{_bindir}/cs
%{_bindir}/cstest
%{_bindir}/cs_static.cgi
%{_mandir}/man3/*

%files devel
%defattr(-, root, root, 0755)
%doc CS_LICENSE LICENSE
%{_includedir}/ClearSilver/
%{_libdir}/libneo_*.a

%files -n python-clearsilver
%doc CS_LICENSE LICENSE
%defattr(-,root,root,-)
%doc README.python python/examples/
%{python_sitearch}/neo_cgi.so

%files -n perl-clearsilver
%doc CS_LICENSE LICENSE
%defattr(-,root,root,-)
%{perl_vendorarch}/auto/ClearSilver/
%{perl_vendorarch}/ClearSilver.pm

%files -n ruby-clearsilver
%doc CS_LICENSE LICENSE
%defattr(-,root,root,-)
%{ruby_sitearchdir}/hdf.so
%{ruby_sitelibdir}/neo.rb

%if 0
# %%ifarch %{ix86} ppc 
%files -n java-clearsilver
%doc CS_LICENSE LICENSE
%defattr(-,root,root,-)
%{_libdir}/java/*clearsilver*
%endif

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-27m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-26m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-25m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-24m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-23m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-22m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-21m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-20m)
- rebuild against perl-5.16.0

* Fri Jan 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-19m)
- [SECURITY] CVE-2011-4357

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-18m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-17m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-16m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.5-15m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.5-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.5-13m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-12m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.5-11m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.5-10m)
- rebuild against ruby-1.9.2

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-9m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-6m)
- rebuild against perl-5.10.1

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-5m)
- use vendor

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.5-3m)
- rebuild against python-2.6.1

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-2m)
- rebuild against python-setuptools-0.6c9

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-1m)
- update to 0.10.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.4-3m)
- rebuild against gcc43

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.4-2m)
- rebuild against perl-5.10.0

* Tue Sep 11 2007 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.10.4-1m)
- up to 0.10.4

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10.3-3m)
- rebuild against ruby-1.8.6-4m

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.3-2m)
- rebuild against python-2.5

* Wed Aug  2 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10.3-1m)
- import from fc extras
- enable ruby ext for x86_64
- disable java ext

* Thu Jun  1 2006 Paul Howarth <paul@city-fan.org> - 0.10.3-4
- ruby subpackage fix: use ruby_sitearchdir and ruby_sitelibdir

* Fri Mar 17 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.10.3-3
- fix for x86_64

* Mon Mar 13 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.10.3-1
- release 0.10.3

* Mon Feb 13 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.10.2-3
- Rebuild for Fedora Extras 5

* Fri Jan  6 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.10.2-2
- Rebuild with disable-ruby, disable-java for any arch other than i386
- hardcoded version in Patch0
- extra line in prep-section
- license files in all subpackages

* Thu Dec 15 2005 Joost Soeterbroek <fedora@soeterbroek.com> - 0.10.2-1
- Rebuild for 0.10.2

* Tue Nov 29 2005 Joost Soeterbroek <fedora@soeterbroek.com> - 0.10.1-1
- Rebuild for Fedora Extras

* Sun Jul 31 2005 Ville Skytta <ville.skytta at iki.fi> - 0.10.1-0.1
- 0.10.1, PIC issues fixed upstream.

* Sun May 29 2005 Ville Skytta <ville.skytta at iki.fi> - 0.9.14-0.3
- Rebuild for FC4.
- Rename subpackages to $foo-clearsilver.

* Mon Apr 25 2005 Ville Skytta <ville.skytta at iki.fi> - 0.9.14-0.2
- Build as PIC, fixes Ruby and Java builds on FC4.
- More parallel make fixing.

* Fri Apr  8 2005 Ville Skytta <ville.skytta at iki.fi> - 0.9.14-0.1
- First build.
