%global momorel 21

Summary:	KinoSearch module for perl 
Name:		perl-KinoSearch
Version:	0.165
Release:	%{momorel}m%{?dist}
License:	GPL
Group:		Development/Languages
#Source0:	http://www.cpan.org/modules/by-module/KinoSearch/KinoSearch-%{version}.tar.gz
Source0:	http://www.cpan.org/authors/id/C/CR/CREAMYG/KinoSearch-%{version}.tar.gz
NoSource:	0
URL:		http://www.cpan.org/modules/by-module/KinoSearch/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	perl >= 1:5.8.1
BuildRequires:	perl-Clone
BuildRequires:	perl-Lingua-Stem-Snowball
BuildRequires:	perl-Lingua-StopWords
BuildRequires:	perl-Template-Toolkit

%description
KinoSearch module for perl

%prep
%setup -q -n KinoSearch-%{version} 

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make


%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
find %{buildroot}%{perl_vendorarch}/auto -name '.packlist' | xargs rm -f
find %{buildroot} -type f -name '*.bs' -a -empty -delete || :
find %{buildroot} -depth -type d -a -empty -delete || :

find %{buildroot}/usr -type f -print | \
	sed "s@^%{buildroot}@@g" | \
	grep -v perllocal.pod | \
	sed -e 's,\(.*/man/.*\),\1*,' | \
	grep -v "\.packlist" > KinoSearch-%{version}-filelist
if [ "$(cat KinoSearch-%{version}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

%clean 
rm -rf %{buildroot}

%files -f KinoSearch-%{version}-filelist
%defattr(-,root,root)
%doc Changes README*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.165-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.165-9m)
- rebuild for new GCC 4.5

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-8m)
- roll back to 0.165 due to dependency issues

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.310-1m)
- update to 0.31
- Specfile re-generated by cpanspec 1.78.

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.165-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.165-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-2m)
- rebuild against perl-5.10.1

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.165-1m)
- update to 0.165

* Sun Mar  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.164-1m)
- update to 0.164

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.163-2m)
- rebuild against rpm-4.6

* Thu Aug 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.163-1m)
- update to 0.163

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.162-2m)
- rebuild against gcc43

* Sun Oct 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.162-1m)
- update to 1.162

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.161-1m)
- update to 0.161

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.14-2m)
- use vendor

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13-1m)
- spec file was autogenerated
