%global momorel 11
Summary: Gnome Attacks - a game of bombing cities into oblivion
Name: gnomeattacks
Version: 0.3
Release: %{momorel}m%{?dist}
License: GPL
Group: Amusements/Games
URL: http://gnomeattacks.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: gnomeattacks.desktop
Patch0: gnomeattacks-0.3-schemas.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgnomeui-devel

%description
Gnome Attacks is a Gnome 2 game where you bomb cities into oblivion in order to
make a flat surface for your rapidly-losing-height spaceship to land. It is
extremely accessible since you can control it "simply using one finger."

%prep
%setup -q
%patch0 -p1 -b .schema

%build
autoreconf -vfi
%configure "LIBS=-lm"
%make 

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} GCONF_SCHEMA_CONFIG_SOURCE=`gconftool-2 --get-default-source` install
install -d %{buildroot}/%{_datadir}/applications/
install %{SOURCE1} %{buildroot}/%{_datadir}/applications/
cp %{buildroot}/%{_datadir}/pixmaps/%{name}/ship.png \
    %{buildroot}/%{_datadir}/pixmaps/%{name}.png

%clean
rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/gnomeattacks.schemas \
  > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/gnomeattacks.schemas \
    > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/gnomeattacks.schemas \
    > /dev/null || :
fi

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_sysconfdir}/gconf/schemas/%{name}.schemas
%{_datadir}/applications/%{name}.desktop
%{_bindir}/%{name}
%{_datadir}/pixmaps/%{name}
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-9m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-8m)
- build fix

* Sat Apr 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-7m)
- build fix

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-4m)
- rebuild against gcc43

* Fri Jul 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-3m)
- fix schemas install dir

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-2m)
- To Main

* Fri Sep 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-1m)
- initial build
