%global momorel 1
%global qtver 4.7.4

Summary: Qt Library that wraps the gpodder.net Web API
Name: libmygpo-qt
Version: 1.0.6
Release: %{momorel}m%{?dist}
License: LGPLv3
Group: System Environment/Libraries
URL: http://wiki.gpodder.org/wiki/Libmygpo-qt
Source0: http://stefan.derkits.at/files/%{name}/%{name}.%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: cmake
BuildRequires: pkgconfig
BuildRequires: qjson

%description
libmygpo-qt is a Qt Library that wraps the gpodder.net Web API.

v1.0 wraps nearly every Request from the gpodder.net API except:
-) Simple API Calls Downloading subscription Lists & Uploading
   subscription Lists
-) Retrieving Subscription Changes (you should use "Retrieving Updates
   for a given Device" instead)

%package devel
Summary: Header files and static libraries from libmygpo-qt
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libmygpo-qt.

%prep
%setup -q -n %{name}.%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} \
	-DMYGPO_BUILD_TESTS=OFF \
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS LICENSE README
%{_libdir}/%{name}.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/mygpo-qt
%{_libdir}/cmake/mygpo-qt
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so

%changelog
* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Sat Nov 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-2m)
- rebuild for new GCC 4.6

* Mon Mar 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- initial package for amarok-2.4.0.90
