%global momorel 4
%global date 20100817
%global qtver 4.8.6
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Summary: Standalone Flickr Uploader
Name: kflickr
Version: %{date}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
URL: http://kflickr.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-20081202-desktop.patch
Patch1: %{name}-%{version}-ssl.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: gettext

%description
kflickr is an easy to use photo uploader for flickr.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .ssl

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop files
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-category FileTransfer \
  --remove-category Network \
  --add-category Qt \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* README
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.svgz
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sat Jun 28 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20100817-4m)
- adapt new flickr API URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100817-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100817-2m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20100817-1m)
- update to 20100817
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20081222-6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20081222-5m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20081222-4m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081222-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081222-2m)
- rebuild against rpm-4.6

* Tue Dec 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20081222-1m)
- version 20081222

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20081202-1m)
- version 20081202
- update desktop.patch

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-2m)
- add DocPath to desktop file

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-1m)
- initial package for Momonga Linux
