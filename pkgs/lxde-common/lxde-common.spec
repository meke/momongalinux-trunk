%global momorel 7

Name:           lxde-common
Version:        0.5.4
Release:        %{momorel}m%{?dist}
Summary:        Default configuration files for LXDE

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.sourceforge.net/
#Source0:        http://downloads.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
Source0:        http://downloads.sourceforge.net/sourceforge/lxde/%{name}-0.5.0.tar.gz
NoSource:       0
Source1:        lxde-lock-screen.desktop
Source2:        lxde-desktop-preferences.desktop
# momonga's source
Source10:       xsession.lxde

# http://lxde.svn.sourceforge.net/viewvc/lxde/trunk/lxde-common/startlxde.in?r1=1964&r2=2187
Patch0:         lxde-common-0.5.0-fix-session-startup.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxde-common;a=commit;h=847a8e73e658bb9ced5eb7b12242b0064224f49e
#Patch1:         lxde-common-0.5.1-Launch-dbus-in-startlxde-when-needed.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxde-common;a=commit;h=34cd793a18138463133a72b713173d2d01d32d66
Patch2:         lxde-common-0.5.1-start-xscreensaver-after-lxpanel-and-pcmanfm.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxde-common;a=commit;h=28309e598478254fa9c918782cf089aea6358abb
#Patch3:         lxde-common-0.5.1-Ensure-the-existance-of-the-Desktop-folder.patch
Patch3:         lxde-common-0.5.1-Ensure-the-existance-of-the-Desktop-folder-rediffed.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxde-common;a=commit;h=0f3b683ccd0c208c1f32fae28f628db9e103ccd8
#Patch5:         lxde-common-0.5.1-switch-from-pcmanfm-to-pcmanfm2.patch
Patch5:         lxde-common-0.5.1-switch-from-pcmanfm-to-pcmanfm2-rediffed.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxde-common;a=commit;h=fcefd17a7187232d15aca13fdedf47a4b854fc21
#Patch7:         lxde-common-0.5.1-Properly-set-XDG_MENU_PREFIX.patch
Patch7:         lxde-common-0.5.1-Properly-set-XDG_MENU_PREFIX-rediffed.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxde-common;a=commit;h=bf7093b8c45c7a38a2f42ddb61135fca7566ff5e
Patch8:         lxde-common-0.5.1-Fix-config-file-of-pcmanfm2.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxde-common;a=commit;h=908f14c74766eb237afda780237db67e7798da0b
# pcmanfm 0.9.7 finally re-renamed pcmanfm2 binary to "pcmanfm"
# Adjusting contents in autostart file
# bug 603468
# This patch contains patch-to-patch part for Patch8
Patch9:         lxde-common-0.5.5-pcmanfm2-finally-re-renamed-to-pcmanfm.patch

# fedora specific patches
Patch14:        %{name}-0.5.1-pulseaudio.patch

# momonga specific patches
# fix lxde-logout.desktop
Patch106:       lxde-common-0.5.0-lxde-logout.desktop.patch
Patch111:       lxde-common-0.4.1-lxpanel-config.patch
Patch113:       lxde-common-0.5.0-default-wallpaper.patch
Patch114:       lxde-common-0.5.0-desktop-notshowin.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  desktop-file-utils
# because of some patches:
BuildRequires:  automake
Requires:       lxsession >= 0.4.0
Requires:       lxpanel pcmanfm openbox xdg-utils 
Requires:       lxmenu-data
# Use momonga's artwork
Requires:       lxde-icon-theme
Requires:       homura-backgrounds-single

Obsoletes:      %{name}-data
Provides:       %{name}-data

BuildArch:      noarch

%description
This package contains the configuration files for LXDE, the Lightweight X11 
Desktop Environment.


%prep
%setup -q -n %{name}-0.5.0
%patch0 -p2 -b .session-startup
#%%patch1 -p1 -b .dbus
%patch2 -p1 -b .xscreensaver
%patch3 -p1 -b .desktop
%patch5 -p1 -b .pcmanfm2
%patch7 -p1 -b .menu-prefix
%patch8 -p1 -b .fix-pcmanfm2
%patch9 -p1 -b .pcmanfm2_2_pcmanfm

%patch14 -p1 -b .pulseaudio

%patch106 -p1 -b .logout-desktop
%patch111 -p1 -b .lxpanel-config
%patch113 -p1 -b .default-wallpaper
%patch114 -p1 -b .notshowin

# Calling autotools must be done before executing
# configure if needed
autoreconf -fi

%build
%configure


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'
desktop-file-install                                           \
      --vendor=                                                \
      --remove-key=Encoding                                    \
      --dir=%{buildroot}%{_datadir}/applications               \
      lxde-logout.desktop
desktop-file-install                                           \
      --vendor=                                                \
      --dir=%{buildroot}%{_datadir}/applications               \
      %{SOURCE1}
# cannot use desktop-file-utils because it is out of date
install -pm 0644 %{SOURCE2} %{buildroot}%{_datadir}/applications/

# momonga specific
mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/session.d/
install -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/X11/xinit/session.d/lxde


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%dir %{_sysconfdir}/xdg/lxsession/LXDE/
%config(noreplace) %{_sysconfdir}/xdg/lxsession/LXDE/autostart
%config(noreplace) %{_sysconfdir}/xdg/lxsession/LXDE/desktop.conf
%dir %{_sysconfdir}/xdg/pcmanfm/
%config(noreplace) %{_sysconfdir}/xdg/pcmanfm/lxde.conf
# momonga specific
%config(noreplace) %{_sysconfdir}/X11/xinit/session.d/lxde
%{_bindir}/startlxde
%{_bindir}/lxde-logout
%{_bindir}/openbox-lxde
%{_datadir}/lxde/*
%{_datadir}/lxpanel/profile/LXDE/
%{_mandir}/man1/*.1*
%{_datadir}/xsessions/LXDE.desktop
%{_datadir}/applications/lxde-*.desktop


%changelog
* Fri Feb 10 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-7m)
- set new background image

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.4-4m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-3m)
- drop dbus patch (Patch1) because we can not shutdown system through
  lxde-logout and rediff Patch3,5,7

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-2m)
- set new background image (Natsuki)

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-1m)
- update to 0.5.4 based on Fedora 13 (0.5.4-2)

* Sat Apr 24 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0-2m)
- temporary fix, Req: lxsession 0.4.0 -> 0.3.8
- comment out Req desktop-backgrounds-compat (obso)

* Sat Apr 17 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Thu Dec 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-3m)
- move out nuoveXT2 to lxde-icon-theme

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2
- insert NotShowIn=GNOME;KDE;XFCE; to lxde-lock-screen.desktop and lxde-logout.desktop

* Sat Jul 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-4m)
- do not install firefox icons

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-3m)
- merge the following Fedora 11 changes
-- * Sat Jun 13 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.1-2
-- - Include logout and screenlock buttons (#503919)

* Thu Jun 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-2m)
- add patch20 (add NotShowIn=GNOME;)

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-1m)
- momonganize Fedora 11's lxde-common (0.4-2)
- update to 0.4.1
#'

* Wed Apr  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2.1-5m)
- install %%{_sysconfdir}/X11/xinit/session.d/lxde

* Wed Apr  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2.1-4m)
- update Patch0
- update Patch1,2 from Rawhide (0.3.2.1-4)
- import pulseaudio patch (Patch13) from Rawhide (0.3.2.1-4)
- use lxde's default artwork

* Sun Mar 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2.1-3m)
- Requires: momonga-backgrounds instead of gnome-backgrounds
- update default wallpaper patch

* Mon Mar 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2.1-2m)
- Requires: gnome-backgrounds instead of desktop-backgrounds-compat

* Tue Mar  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2.1-1m)
- import from Fedora to Momonga

* Fri Oct 10 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.2.1-3
- Require fedora-icon-theme and fedora-logos

* Thu Oct 09 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.2.1-2
- Rebase patches for rpm's new fuzz=0 behaviour

* Thu Jul 10 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.2.1-1
- Update to 0.3.2.1
- Switch from ROXTerm to LXterminal
- Rebased most patches
- Add mixer to the panel 

* Mon Apr 14 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0.1-2
- Make a separate package for lxde-icon-theme

* Sat Apr 12 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0.1-1
- Update to 0.3.0.1
- Use distros default artwork

* Sat Mar 29 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0.0-1
- Initial Fedora RPM
- Use roxterm instead of gnome-terminal and xterm
- Patch default panel config
