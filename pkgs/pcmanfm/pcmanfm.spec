%global momorel 1

# Upstream git:
# git://pcmanfm.git.sourceforge.net/gitroot/pcmanfm/pcmanfm

Name:		pcmanfm
Version:	1.2.0
Release:	%{momorel}m%{?dist}
Summary:	Extremly fast and lightweight file manager

Group:		User Interface/Desktops
License:	GPLv2+
URL:		http://pcmanfm.sourceforge.net/
Source0:	http://downloads.sourceforge.net/pcmanfm/%{name}/%{name}-%{version}.tar.xz
NoSource:       0

# Create module directory anyway
# https://sourceforge.net/p/pcmanfm/bugs/831/
Patch0:	pcmanfm-1.2.0-create-module-dir.patch

BuildRequires:	libfm-gtk-devel = 1.2.0
BuildRequires:	menu-cache-devel

BuildRequires:	desktop-file-utils
BuildRequires:	gettext
BuildRequires:	intltool

# Patch0
BuildRequires:	automake

# Request for now
Requires:			libfm-gtk-utils
# Write explicitly
Requires:	libfm >= %{libfm_minver}

# For compatibility
BuildRequires:	lxde-icon-theme

%description
PCMan File Manager is an extremly fast and lightweight file manager 
which features tabbed browsing and user-friendly interface.

%package			devel
Summary:			Development files for %{name}
Group:				Development/Libraries
Requires:			%{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q
%patch0 -p1 -b .emptydir

# Patch0
autoreconf -fi

# permission fix
chmod 0644 [A-Z]*

%build
export LDFLAGS="-lm"
%configure \
	--disable-silent-rules \
	--with-gtk=3

make -C po -j1 GMSGFMT="msgfmt --statistics"
make  %{?_smp_mflags} -k

%install
rm -rf $RPM_BUILD_ROOT
make install \
	DESTDIR=$RPM_BUILD_ROOT \
	INSTALL="install -p"

desktop-file-install \
	--delete-original \
	--dir $RPM_BUILD_ROOT%{_datadir}/applications \
	--remove-category 'Application' \
	--vendor '' \
	$RPM_BUILD_ROOT%{_datadir}/applications/%{name}*.desktop

# compatibility
mkdir %{buildroot}%{_datadir}/pixmaps
# No link but copy so that non-LXDE user won't require
# LXDE stuff
cp -p %{_datadir}/icons/nuoveXT2/128x128/apps/system-file-manager.png \
	%{buildroot}%{_datadir}/pixmaps/%{name}.png

%find_lang %{name}

%{_prefix}/lib/rpm/check-rpaths

%clean
rm -rf $RPM_BUILD_ROOT

%post
update-desktop-database &> /dev/null
exit 0

%postun
update-desktop-database &> /dev/null
exit 0

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%config(noreplace) %{_sysconfdir}/xdg/%{name}/
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*

%{_libdir}/%{name}/
%{_datadir}/%{name}/
%{_datadir}/applications/*%{name}*.desktop
%{_datadir}/pixmaps/%{name}.png
%config(noreplace) %{_sysconfdir}/xdg/%{name}/

%files devel
%{_includedir}/pcmanfm-modules.h

%changelog
* Mon May 19 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.10-2m)
- remove BR hal-storage-addon

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.10-1m)
- update to 0.9.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-1m)
- sync with Rawhide (0.9.7-1)

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-4m)
- fix build

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sun May 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-2m)
- apply the following patch from Fedora 11 (0.5-7)
- http://sourceforge.net/tracker/?func=detail&aid=2313286&group_id=156956&atid=801864

* Tue Mar  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-1m)
- import from Fedora to Momonga
- comment out unknown exec /usr/lib/rpm/check-rpaths

* Tue Feb 24 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.5-6
- F-11: Mass rebuild

* Fri Aug  8 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.5-5
- More fallback

* Wed Jul 30 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.5-4
- More fallback for gnome-icon-theme 2.23.X (F-10)

* Tue Jul 29 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.5-2
- F-10+: Use more generic icon name due to gnome-icon-theme 2.23.X change
  First try (need more fix)

* Thu Jul 17 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.5-1
- 0.5

* Wed Jul 16 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.6.2-1
- 0.4.6.2

* Tue Jul 15 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.6.1-1
- 0.4.6
- 0.4.6.1
- -Werror-implicit-function-declaration is added upstream

* Sat Jun 28 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.5-1
- 0.4.5 (remote server access function temporally removed)
- BR: intltool

* Sun May 25 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.4.2-1
- 0.4.4.2

* Mon May 19 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.4.0-1
- 0.4.4.0

* Sun May 11 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.1.1-1
- 0.4.1
- 0.4.1.1

* Mon May  5 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.4.0-1
- 0.4.0

* Sun Apr 13 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.9.98-2
- First trial to suppress compilation warning (containing fix for
  crash on an occasion)

* Wed Apr  9 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.9.98-1
- 0.3.9.98

* Thu Mar 20 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.9.10-1
- 0.3.9.10

* Sat Mar 15 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.9.5-1
- 0.3.9.5

* Wed Mar  5 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.9-1
- 0.3.9

* Fri Feb 29 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.6.1-1
- 0.3.6.1

* Sat Feb 23 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.6-1
- 0.3.6

* Wed Feb 20 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.5.99-1
- 0.3.6 RC
- 2 patches dropped (applied by upstream)

* Tue Feb 19 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.5.23-3
- Fix crash on mounting removable devices

* Mon Feb 18 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.5.23-2
- Apply patch to fix crash on 64bits arch as suggested by Hans
  (bug 433182)
- Disable to mount removable devices for now

* Sun Feb 17 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.3.5.23-1
- Initial draft
- Disable inotify support, too buggy (also default is no currently)


