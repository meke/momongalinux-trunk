%global momorel 13

Name:           perl-Digest-BubbleBabble
Version:        0.02
Release:        %{momorel}m%{?dist}
Summary:        Create bubble-babble fingerprints
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Digest-BubbleBabble/
Source0:        http://www.cpan.org/authors/id/B/BT/BTROTT/Digest-BubbleBabble-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Filter-Util-Call
Requires:       perl-Filter-Util-Call
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Digest::BubbleBabble takes a message digest (often generated by either of
the MD5 or SHA-1 message digest algorithms) and creates a fingerprint of
that digest in "bubble babble" format. Bubble babble is a method of
representing a message digest as a string of "real" words, to make the
fingerprint easier to remember. The "words" are not necessarily real words,
but they look more like words than a string of hex characters.

%prep
%setup -q -n Digest-BubbleBabble-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Digest/BubbleBabble.pm
%{_mandir}/man3/Digest::BubbleBabble.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-13m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-12m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-11m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-10m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-9m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.02-2m)
- rebuild for new GCC 4.6

* Thu Mar 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-1m)
- update to 0.02

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.01-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.01-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.01-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-3m)
- rebuild against perl-5.10.1

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-2m)
- fix BuildRequres

* Tue Feb  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.01-1m)
- import from Fedora to Momonga for dnssec-tools

* Wed Feb 27 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.01-8
- Rebuild for perl 5.10 (again)

* Thu Jan 31 2008 Tom "spot" Callaway <tcallawa@redhat.com> 0.01-7
- rebuild for new perl

* Mon Aug 13 2007 Paul Howarth <paul@city-fan.org> 0.01-6
- Clarify license as GPL v1 or later, or Artistic (same as perl)

* Wed Apr 18 2007 Paul Howarth <paul@city-fan.org> 0.01-5
- Buildrequire perl(ExtUtils::MakeMaker)
- Fix argument order for find with -depth

* Tue Aug 29 2006 Paul Howarth <paul@city-fan.org> 0.01-4
- FE6 mass rebuild

* Wed Feb 15 2006 Paul Howarth <paul@city-fan.org> 0.01-3
- Rebuild for perl 5.8.8 (FC5)

* Tue Jan  3 2006 Paul Howarth <paul@city-fan.org> 0.01-2
- Don't include README, which contains only install instructions (#175280)

* Fri Nov 25 2005 Paul Howarth <paul@city-fan.org> 0.01-1
- Initial build
