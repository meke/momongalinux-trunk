%global momorel 10

Name: timemachine
Version: 2.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source0: http://www.shiratori.riec.tohoku.ac.jp/~jir/linux/products/timemachine/timemachine.tar.gz 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary: Time Machine
URL: http://www.shiratori.riec.tohoku.ac.jp/~jir/linux/products/timemachine/

%description
  This program hooks time(2), gettimeofday(2) and clock_gettime(3), then run another program under the user specified time system. So if you start a program at 00:00:00 1st Jan 1900 then, after 1 hour later the program's time system will be 01:00:00 1st Jan 1900.

%prep
%setup -q

%build
libtoolize -c -f
aclocal
automake -c -f -a
autoconf
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/timemachine
%{_libdir}/libtimemachine*

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-10m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-4m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-3m)
- delete libtool library

* Sun Apr 18 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0-2m)
- use %%NoSource macro

* Thu Nov  6 2003 Shotaro Kamio <skamio@momonga-linux.org>
- (2.0-1m)
- version 2.0
- removed the patch for version 1.1

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-4m)
- kill %%define version

* Tue Jul  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1-3m)
- omit multiline string from timemachine.c

* Sun Mar 10 2002 kitaj <kita@kitaj.no-ip.com>
- initial build
