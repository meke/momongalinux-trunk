%global momorel 13
Summary: Wnn7 Client Library
Name: libwnn7
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
Source0: http://www.omronsoft.co.jp/SP/pcunix/sdk/wnn/Wnn7SDK.tgz
NoSource: 0
Patch0: 64bit.patch
Patch1: do-not-return-random-data-in-functions.patch
Patch2: wnn7-malloc.patch
Patch3: wnn7-library.patch
Patch4: libwnn7-gcc4.patch
Patch5: libwnn7-Makefile.ini.patch

URL: http://www.omronsoft.co.jp/SP/pcunix/wnn7/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Runtime Wnn7 client library necessary to run Wnn7 clients.

%package devel
Summary: Development kit for Wnn7 Client Library
Group: Development/Libraries
Requires: %name = %{version}-%{release}

%description devel
Development kit for Wnn7 client library necessary to run Wnn7 clients.

%prep
%setup -q -n src
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1 -b .gcc4
%patch5 -p1 -b .imake

%build
make World -f Makefile.ini CXXDEBUGFLAGS="%{optflags}" CDEBUGFLAGS="%{optflags}"

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%install
#make install DESTDIR=%{buildroot} MKDIRHIER='mkdir -p' WNNLIBDIR=%{_libdir} WNNWNNDIR=%{_libdir}/wnn7

mkdir -p  $RPM_BUILD_ROOT/usr/include/wnn7
mkdir -p  $RPM_BUILD_ROOT/%{_libdir}
install -m 644 Wnn/include/*.h $RPM_BUILD_ROOT/usr/include/wnn7
install -m 644 Wnn/jlib/*.a $RPM_BUILD_ROOT/%{_libdir}
cp -a Wnn/jlib/*.so* $RPM_BUILD_ROOT/%{_libdir}

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root)
%doc README
%{_libdir}/libwnn7.so.1
%{_libdir}/libwnn7.so.1.0
%{_libdir}/libwnn7.so

%files devel
%defattr(-,root,root)
%{_libdir}/libwnn7.a
%{_includedir}/wnn7

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-13m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-10m)
- full rebuild for mo7 release

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-5m)
- %%NoSource -> NoSource

* Tue Mar 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-4m)
- revise for xorg-7.0

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-3m)
- add gcc4 patch.
- Patch4: libwnn7-gcc4.patch

* Wed Jun 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0-2m)
- force libdir.

* Wed Jun  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-1m)
- initial commit

