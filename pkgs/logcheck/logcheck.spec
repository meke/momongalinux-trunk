%global momorel 1

Name:           logcheck 
Version:        1.3.16
Release:        %{momorel}m%{?dist}
Summary:        Analyzes log files and sends noticeable events as email 

License:        GPLv2
Group:          Applications/System
URL:            http://logcheck.org/
Source0:        http://ftp.de.debian.org/debian/pool/main/l/%{name}/%{name}_%{version}.tar.xz
# NoSource:       0
Source1:        logchk-systemd-ignore
Source2:        logchk-NetworkManager-ignore			
Source3:        logchk-dbus-ignore
Patch0:         logcheck-dhclient.patch
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       lockfile-progs perl-mime-construct
Requires(pre):  shadow-utils
BuildRequires:  docbook-utils
Obsoletes:      logsentry


%description
Logcheck is a simple utility which is designed to allow a system administrator 
to view the log-files which are produced upon hosts under their control.

It does this by mailing summaries of the log-files to them, after first 
filtering out "normal" entries.

Normal entries are entries which match one of the many included regular 
expression files contain in the database.

%prep
%setup -q

%patch0 -p2

# use fedora-style logfiles. (/var/log/syslog is /var/log/messages, 
#                              auth.log is named secure)
sed -i "s/syslog/messages/" etc/logcheck.logfiles
sed -i "s/auth\.log/secure/" etc/logcheck.logfiles

echo "d /var/lock/logcheck 1700 logcheck logcheck 1d" > etc/tmpfiles.d-logcheck

%build

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}/%{_sysconfdir}/%{name}/violations.d/logcheck

%{__mkdir} -pm 755 %{buildroot}%{_mandir}/man8
%{__install} -pm 644 docs/logtail.8 %{buildroot}%{_mandir}/man8/
%{__install} -pm 644 docs/logtail2.8 %{buildroot}%{_mandir}/man8/
# create man-page from sgml. 
docbook2man -o %{buildroot}%{_mandir}/man8/ docs/logcheck.sgml 
# man-page-name is named Logcheck instead of logcheck: 
mv %{buildroot}%{_mandir}/man8/Logcheck.8 %{buildroot}%{_mandir}/man8/%{name}.8

rm -f %{buildroot}%{_mandir}/man8/manpage.*

%{__mkdir} -pm 755 %{buildroot}%{_mandir}/man1
%{__install} -pm 644 docs/logcheck-test.1 %{buildroot}%{_mandir}/man1/
%{__install} -pm 644 docs/logtail2.8 %{buildroot}%{_mandir}/man8/

%{__mkdir} -pm 755 %{buildroot}%{_sysconfdir}/cron.d
%{__install} -pm 644 debian/%{name}.cron.d %{buildroot}%{_sysconfdir}/cron.d/%{name}

%{__mkdir} -pm 755 %{buildroot}%{_localstatedir}/lock/%{name}
# create homedir for logcheck-user
%{__mkdir} -pm 644 %{buildroot}%{_sharedstatedir}/%{name}

%{__mkdir} -pm 755 %{buildroot}%{_sysconfdir}/tmpfiles.d
%{__install} -pm 644 etc/tmpfiles.d-logcheck %{buildroot}%{_sysconfdir}/tmpfiles.d/%{name}.conf

# install fedora tweaked rule files
%{__install} -pm 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/%{name}/ignore.d.server/systemd
%{__install} -pm 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/%{name}/ignore.d.server/NetworkManager
%{__install} -pm 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/%{name}/ignore.d.server/dbus

%clean
rm -rf %{buildroot}

%pre
getent group logcheck >/dev/null || groupadd -r logcheck
getent passwd logcheck >/dev/null || \
    useradd -r -g logcheck -G adm -d /var/lib/logcheck -s /sbin/nologin \
            -c "Logcheck user" logcheck && \
    mkdir /var/lock/logcheck && chown logcheck:logcheck /var/lock/logcheck && \
    mkdir /var/lib/logcheck && chown logcheck:logcheck /var/lib/logcheck && \
    [ -x /sbin/restorecon ] && /sbin/restorecon /var/lock/logcheck /var/lib/logcheck
exit 0

%files
%defattr(-,root,root,-)
%attr(0755,root,root) %dir %{_sysconfdir}/%{name}/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/logcheck.conf
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/logcheck.logfiles
%attr(0755,root,root) %dir %{_sysconfdir}/%{name}/cracking.d/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/cracking.d/*
%attr(0755,root,root) %dir %{_sysconfdir}/%{name}/ignore.d.workstation/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/ignore.d.workstation/*
%attr(0755,root,root) %dir %{_sysconfdir}/%{name}/ignore.d.server/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/ignore.d.server/*
%attr(0755,root,root) %dir %{_sysconfdir}/%{name}/ignore.d.paranoid/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/ignore.d.paranoid/*
%attr(0755,root,root) %dir %{_sysconfdir}/%{name}/violations.d/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/violations.d/*
%attr(0755,root,root) %dir %{_sysconfdir}/%{name}/violations.ignore.d/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/violations.ignore.d/*
%config(noreplace) %{_sysconfdir}/cron.d/*
%{_bindir}/logcheck-test
%{_sbindir}/*
%{_datadir}/logtail/
%{_mandir}/man?/*
%ghost %{_localstatedir}/lock/%{name}
%ghost %{_sharedstatedir}/%{name}
%config(noreplace) %{_sysconfdir}/tmpfiles.d/%{name}.conf
%doc docs/* LICENSE


%changelog
* Sun Jun 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.15-1m)
- version 1.3.15

* Sun Jan 19 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.14-1m)
- version 1.3.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.13-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.13-2m)
- rebuild for new GCC 4.5

* Sun Sep 12 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.13-1m)
- rename to logcheck
- sync fedora

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-12m)
- full rebuild for mo7 release

* Thu May  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-11m)
- add execute bit

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-9m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-8m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-7m)
- rebuild against gcc43

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.1-6m)
- revised docdir permission

* Wed Feb 16 2005 zunda <zunda at freeshell.org>
- (1.1.1-5m)
- needs /bin/mail to send out the results

* Sat Nov 29 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (1.1.1-4m)
- change URL and Source0 URL
- use %%{momorel}

* Sat Nov  8 2003 Shotaro Kamio <skamio@momonga-linux.org>
- (1.1.1-3m)
- changed source URL because original source went away

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.1.1-2k)
- command renamed to LogSentry

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.1.1-6k)
- s/Copyright/License/

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.1.1-4k)
- nigittenu

* Thu Jul 12 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.1-2k)
- from rawhide logcheck-1.1.1-6.src.rpm
- NoSource
- add logcheck-1.1.1-ignorelist.patch against Kondara-2.0
- fixed that %{_localstatedir} is not /var/lib

* Fri Mar  9 2001 Tim Powers <timp@redhat.com>
- incorporated most if the ideas from the patch sent in in bug 31069,
  except moved the variables into logcheck.conf to keep things
  consistant.

* Thu Mar  8 2001 Tim Powers <timp@redhat.com>
- split out the configuration stuff from the shell script that is run
  into a config file (#31069)

* Mon Feb  5 2001 Tim Powers <timp@redhat.com>
- changed schedule to run back to hourly as per bug 25949 which makes
  more sense.

* Fri Jan 19 2001 Tim Powers <timp@redhat.com>
- maillog was erroneously changed to mail.log in logcheck.sh

* Thu Jan  4 2001 Tim Powers <timp@redhat.com>
- not building as noarch, has arch dependant binaries

* Fri Oct  6 2000 Tim Powers <timp@redhat.com> 1.1.1-5
- using Mandrakes package for Powertools. Thanks Mandrake :)
- build for noarch
- change group to be a valid Red Hat group
- added URL to the logcheck sources
- fixed URL to logcheck webpage
- removed redundant defines at top of spec file
- fix location of /tmp dir to be /var/logcheck. Don't want to use 
  something everyone has access to.

* Mon Sep 18 2000 Vincent Danen <vdanen@mandrakesoft.com> 1.1.1-4mdk
- move logcheck script from running hourly to running daily

* Thu Aug  3 2000 Vincent Danen <vdanen@mandrakesoft.com> 1.1.1-3mdk
- macros
- fix path for config files
- change group
- add patch to fix configuration variables in logcheck.sh
- add script in cron.hourly

* Wed Jun 21 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- 1.1-4k
- fixed cron script

* Sun Jun 18 2000 AYUHANA Tomonori <l@kondara.org>
- requires: "full patch" -> packages
- add %defattr
- add -q at %setup

* Sun Jun 11 2000 Kenji Yamaguchi <yamk@sophiaworks.com>
- Recompiled under Kondara MNU/Linux 1.1
- NoSource.
- appended "Distribution:"
- changed "BuildRoot:"
- changed "Group:"
- changed "Copyright:"

* Thu May  4 2000 Vincent Danen <vdanen@linux-mandrake.com> 1.1.1-2mdk
- fix group
- fix for spec-helper
- change prefix to /usr
- bzip patch

* Wed Dec 1 1999 Vincent Danen <vdanen@linux-mandrake.com>
- updated specfile for Mandrake contribs
- specfile cleanups
- bzip sources
- 1.1.1

* Tue Nov 9 1999 Vincent Danen <vdanen@softhome.net>
- updated spec file to clean up properly
- specfile adaptations

* Tue Sep 28 1999 Vincent Danen <vdanen@softhome.net>
- updated spec file

* Mon Sep 27 1999 Vincent Danen <vdanen@softhome.net>
- 1.1
- Mandrake adaptions
