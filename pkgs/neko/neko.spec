%global momorel 9
%global ocamlver 3.11.2

%global __os_install_post %{nil}

Summary: The Neko Programming Language
Name: neko
Version: 1.8.1
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Development/Languages
Source0: %{name}-%{version}.tar.gz
Patch0: neko-1.8.1-library-path.patch
Patch1: neko-1.8.1-httpd24.patch
URL: http://nekovm.org/
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: gc-devel
BuildRequires: httpd-devel >= 2.4.3
BuildRequires: mysql-devel
BuildRequires: pcre-devel >= 8.31
BuildRequires: zlib-devel
BuildRequires: sqlite-devel >= 3.4.0
BuildRequires: libX11-devel libXau-devel libXcursor-devel libXdmcp-devel
BuildRequires: libXext-devel libXfixes-devel libXft-devel libXi-devel
BuildRequires: libXinerama-devel libXrandr-devel libXrender-devel libxcb-devel
BuildRequires: fontconfig-devel freetype-devel glib2-devel gtk2-devel
BuildRequires: atk-devel cairo-devel pango-devel expat-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Neko is a high-level dynamically typed programming language which can
also be used as an embedded scripting language. 
It has been designed to provide a common runtime for several different
languages. Neko is not only very easy to learn and use, but also has
the flexibility of being able to extend the language with C libraries.
You can even write generators from your own language to Neko and then
use the Neko Runtime to compile, run, and access existing libraries.

If you need to add a scripting language to your application, Neko
provides one of the best tradeoffs available between simplicity,
extensibility and speed.

Neko allows the language designer to focus on design whilst reusing a
fast and well constructed runtime, as well as existing libraries for
accessing filesystem, network, databases, xml... 

%prep
%setup -q 
%patch0 -p1 -b .library-path
%patch1 -p1 -b .httpd24

%build
%{__make} INSTALL_PREFIX=%{_prefix}

%install
rm -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_bindir}
%{__mkdir_p} %{buildroot}%{_libdir}
%{__mkdir_p} %{buildroot}%{_includedir}
%{__install} -m 755 bin/neko* %{buildroot}%{_bindir}
%{__install} -m 644 bin/test.n %{buildroot}%{_bindir}
%{__install} -m 755 bin/libneko.so %{buildroot}%{_libdir}
%{__install} -m 755 bin/*.ndll %{buildroot}%{_libdir}
%{__install} -m 644 vm/neko.h %{buildroot}%{_includedir}
%{__install} -m 644 vm/neko_mod.h %{buildroot}%{_includedir}
%{__install} -m 644 vm/neko_vm.h %{buildroot}%{_includedir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES LICENSE
%{_bindir}/*
%{_includedir}/*
%{_libdir}/lib*
%{_libdir}/*.ndll

%changelog
* Thu Feb 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-9m)
- rebuild with httpd-2.4.x

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-8m)
- rebuild against pcre-8.31

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-7m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.1-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.1-3m)
- rebuild against ocaml-3.11.2

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.1-2m)
- disable mysql4

* Fri Feb 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1 for glibc212

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0
-- update Patch0
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-5m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-4m)
- rebuild against ocaml-3.10.2

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-3m)
- revised spec for debuginfo

* Mon Aug 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-2m)
- update Patch0: neko-1.6.0-add-library-path.patch
-- enable x86_64
- add some BuildRequires

* Sun Aug  5 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-1m)
- import to Momonga from <http://shield.jp/~dseg/rpms/neko/neko.spec>
- no strip

* Sat Feb 24 2007 Daichi Shinozaki <dseg@shield.jp>
- New upstream.

* Thu Nov 23 2006 Daichi Shinozaki <dseg@shield.jp>
- Initial release.
