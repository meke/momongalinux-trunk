%global momorel 5

%define base_name       cli
%define short_name      commons-%{base_name}
%define name            jakarta-%{short_name}
%define section         devel

Name:           %{name}
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        Command Line Interface Library for Java
License:        "ASL 2.0"
Group:          Development/Libraries
URL:            http://jakarta.apache.org/commons/cli/
Source0:        http://archive.apache.org/dist/commons/cli/source/commons-cli-%{version}-src.tar.gz
NoSource:       0
Patch0:         %{name}-MultiOptions.patch

Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ant >= 1.6, ant-junit >= 1.6, junit, jakarta-commons-lang, jakarta-commons-logging
BuildRequires:  jpackage-utils >= 1.5
# libgcj aot-compiled native libraries
BuildRequires:    java-gcj-compat-devel >= 1.0.31
Requires(post):   java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
Requires:       jakarta-commons-lang, jakarta-commons-logging

%description
The CLI library provides a simple and easy to use API for working with
the command line arguments and options.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
BuildRequires:  java-javadoc

%description    javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{short_name}-%{version}-src

%patch0 -b .sav

%build
export OPT_JAR_LIST="ant/ant-junit junit"
export CLASSPATH=%(build-classpath commons-logging commons-lang )
export CLASSPATH="$CLASSPATH:target/%{short_name}.jar:target/test-classes:target/classes"
 # for tests
mkdir lib
ant \
  -Dbuild.sysclasspath=only \
  -Dfinal.name=%{short_name} \
  -Dj2se.javadoc=%{_javadocdir}/java \
  jar test dist


%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p dist/%{short_name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|jakarta-||g"`; done)
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

aot-compile-rpm

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr dist/docs/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

%clean
rm -rf $RPM_BUILD_ROOT

%post -p %{_bindir}/rebuild-gcj-db

%postun -p %{_bindir}/rebuild-gcj-db

%files
%defattr(0644,root,root,0755)
%doc LICENSE.txt README.txt
%{_javadir}/*
%{_libdir}/gcj/%{name}

%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/%{name}-%{version}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-3m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- sync with Fedora 11 (0:1.1-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6jpp.2m)
- rebuild against rpm-4.6

* Tue Jan  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-6jpp.1m)
- import from fedora to Momonga for bootchart

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> - 0:1.0-6jpp_10
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Sep 18 2006 Anthony Green <green@redhat.com> - 0:1.0-6jpp_9
- Rebuild.

* Tue Jul 25 2006 Anthony Green <green@redhat.com> - 0:1.0-6jpp_8
- Rebuild with new compiler.

* Tue Feb 28 2006 Anthony Green <green@redhat.com> - 0:1.0-6jpp_6
- Rebuild with new compiler.

* Wed Jan 18 2006 Anthony Green <green@redhat.com> - 0:1.0-6jpp_5
- Fix Summary.
- Clean up %files.

* Tue Jan 17 2006 Anthony Green <green@redhat.com> - 0:1.0-6jpp_4
- Build for Fedora.
- Remove unversioned javadoc dir.
- Adjust Groups.

* Wed Dec 21 2005 Jesse Keating <jkeating@redhat.com> - 0:1.0-6jpp_3fc
- rebuilt again

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com> - 0:1.0-6jpp_2fc
- rebuilt

* Thu Jun 16 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-6jpp_1fc
- Build into Fedora.

* Mon Dec 20 2004 Fernando Nasser <fnasser at redhat.com> - 0:1.0-6jpp_1rh
- First Red Hat release

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:1.0-6jpp
- Rebuild with ant-1.6.2

* Fri Aug 06 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.0-5jpp
- Void change

* Tue Jun 01 2004 Randy Watler <rwatler at finali.com> - 0:1.0-4jpp
- Upgrade to Ant 1.6.X

* Sun Oct 12 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-3jpp
- Non-versioned javadoc dir symlink.
- Crosslink with local J2SE javadocs.

* Fri Apr  4 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-2jpp
- Rebuild for JPackage 1.5.

* Tue Dec 10 2002 Ville Skytta <ville.skytta at iki.fi> - 1.0-1jpp
- 1.0, first JPackage release.
