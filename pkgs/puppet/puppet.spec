%global momorel 1

%global vimver 74
%global vimmacrodir %{_datadir}/vim/vimfiles/

%{!?ruby_sitelibdir: %define ruby_sitelibdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["sitelibdir"]')}
%define confdir ext/redhat

Summary: A network tool for managing many disparate systems
Name: puppet
Version: 3.3.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
URL: http://puppetlabs.com/
Source0: http://puppetlabs.com/downloads/puppet/%{name}-%{version}.tar.gz
NoSource: 0
Source1: puppet-nm-dispatcher.systemd
Source2: start-puppet-wrapper
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: facter >= 1.5.7-2m
BuildRequires: ruby >= 1.9.2
%ifnarch ppc64
BuildRequires:  emacs >= %{emacsver}
%endif
BuildArch: noarch
Requires: ruby(abi) = 1.9.1
Requires: ruby-shadow
Requires: libselinux-ruby
Requires: facter >= 1.5
Requires: ruby >= 1.9.2
Requires: ruby-augeas
Requires(pre): shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(postun): initscripts
Obsoletes: puppet-xemacs

%description
Puppet lets you centrally manage every important aspect of your system using a 
cross-platform specification language that manages all the separate elements 
normally aggregated in different files, like users, cron jobs, and hosts, 
along with obviously discrete elements like packages, services, and files.

%package server
Group: System Environment/Base
Summary: Server for the puppet system management tool
Requires: puppet = %{version}-%{release}
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(postun): initscripts

%description server
Provides the central puppet server daemon which provides manifests to clients.
The server can also function as a certificate authority and file server.

%ifnarch ppc64
%package -n emacs-puppet
Summary: Emacs Lisp puppet-mode for puppet manifest
Group: Applications/Editors
Requires: emacs >= %{_emacs_version}
Obsoletes: puppet-emacs
Obsoletes: elisp-puppet

%description -n emacs-puppet
Emacs Lisp puppet-mode for puppet manifest
%endif

%package vim
Summary: VIM macro for puppet manifest
Group: Applications/Editors
Requires: vim-macros >= 7.3

%description vim
VIM macro for puppet manifest

%prep
%setup -q

%build
# Fix some rpmlint complaints
for f in mac_automount.pp  mcx_dock_absent.pp  mcx_dock_default.pp \
    mcx_dock_full.pp mcx_dock_invalid.pp mcx_nogroup.pp \
    mcx_notexists_absent.pp; do
    sed -i -e'1d' examples/$f
    chmod a-x examples/$f
done
for f in external/nagios.rb relationship.rb; do
    sed -i -e '1d' lib/puppet/$f
done
chmod +x ext/puppet-load.rb ext/regexp_nodes/regexp_nodes.rb

%install
rm -rf %{buildroot}
ruby install.rb --destdir=%{buildroot} --quick --no-rdoc

install -d -m0755 %{buildroot}%{_sysconfdir}/puppet/manifests
install -d -m0755 %{buildroot}%{_datadir}/%{name}/modules
install -d -m0755 %{buildroot}%{_localstatedir}/lib/puppet
install -d -m0755 %{buildroot}%{_localstatedir}/run/puppet
install -d -m0750 %{buildroot}%{_localstatedir}/log/puppet

install -d -m0755  %{buildroot}%{_unitdir}
install -Dp -m0644 ext/systemd/puppetagent.service %{buildroot}%{_unitdir}/puppetagent.service
install -Dp -m0644 ext/systemd/puppetmaster.service %{buildroot}%{_unitdir}/puppetmaster.service
install -Dp -m0644 %{confdir}/fileserver.conf %{buildroot}%{_sysconfdir}/puppet/fileserver.conf
install -Dp -m0644 %{confdir}/puppet.conf %{buildroot}%{_sysconfdir}/puppet/puppet.conf
install -Dp -m0644 %{confdir}/logrotate %{buildroot}%{_sysconfdir}/logrotate.d/puppet

# Install a NetworkManager dispatcher script to pickup changes to
# /etc/resolv.conf and such (https://bugzilla.redhat.com/532085).
install -Dpv %{SOURCE1} \
    %{buildroot}%{_sysconfdir}/NetworkManager/dispatcher.d/98-%{name}

# Install the ext/ directory to %%{_datadir}/%%{name}
install -d %{buildroot}%{_datadir}/%{name}
cp -a ext/ %{buildroot}%{_datadir}/%{name}
# emacs and vim bits are installed elsewhere
rm -rf %{buildroot}%{_datadir}/%{name}/ext/{emacs,vim}
# remove misc packaging artifacts in source not applicable to rpm
rm -rf %{buildroot}%{_datadir}/%{name}/ext/{gentoo,freebsd,solaris,suse,windows,osx,ips,debian}
rm -f %{buildroot}%{_datadir}/%{name}/ext/{build_defaults.yaml,project_data.yaml}
rm -f %{buildroot}%{_datadir}/%{name}/ext/redhat/*.init

# Install emacs mode files
emacsdir=%{buildroot}%{_datadir}/emacs/site-lisp
install -Dp -m0644 ext/emacs/puppet-mode.el $emacsdir/puppet-mode.el
install -Dp -m0644 ext/emacs/puppet-mode-init.el \
    $emacsdir/site-start.d/puppet-mode-init.el

# Install vim syntax files
vimdir=%{buildroot}%{_datadir}/vim/vimfiles
install -Dp -m0644 ext/vim/ftdetect/puppet.vim $vimdir/ftdetect/puppet.vim
install -Dp -m0644 ext/vim/syntax/puppet.vim $vimdir/syntax/puppet.vim

# Install wrappers for SELinux
install -Dp -m0755 %{SOURCE2} %{buildroot}%{_bindir}/start-puppet-agent
sed -i 's/@@COMMAND@@/agent/g' %{buildroot}%{_bindir}/start-puppet-agent
install -Dp -m0755 %{SOURCE2} %{buildroot}%{_bindir}/start-puppet-master
sed -i 's/@@COMMAND@@/master/g' %{buildroot}%{_bindir}/start-puppet-agent

# Setup tmpfiles.d config
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d
echo "D /var/run/%{name} 0755 %{name} %{name} -" > \
    %{buildroot}%{_sysconfdir}/tmpfiles.d/%{name}.conf

# Create puppet modules directory for puppet module tool
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/modules

%files
%defattr(-, root, root, 0755)
%doc LICENSE README.md examples
%{_bindir}/puppet
%{_bindir}/start-puppet-agent
%{_bindir}/start-puppet-master
%{_bindir}/extlookup2hiera
%{ruby_sitelibdir}/*
%{_unitdir}/puppetagent.service
%dir %{_sysconfdir}/puppet
%dir %{_sysconfdir}/%{name}/modules
%config(noreplace) %{_sysconfdir}/tmpfiles.d/%{name}.conf
%config(noreplace) %{_sysconfdir}/puppet/puppet.conf
%config(noreplace) %{_sysconfdir}/puppet/auth.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/puppet
%dir %{_sysconfdir}/NetworkManager
%dir %{_sysconfdir}/NetworkManager/dispatcher.d
%{_sysconfdir}/NetworkManager/dispatcher.d/98-puppet
%{_datadir}/%{name}
# These need to be owned by puppet so the server can
# write to them
%attr(-, puppet, puppet) %{_localstatedir}/run/puppet
%attr(0750, puppet, puppet) %{_localstatedir}/log/puppet
%attr(-, puppet, puppet) %{_localstatedir}/lib/puppet
%{_mandir}/man5/puppet.conf.5.*
%{_mandir}/man8/puppet.8.*
%{_mandir}/man8/puppet-agent.8.*
%{_mandir}/man8/puppet-apply.8.*
%{_mandir}/man8/puppet-catalog.8.*
%{_mandir}/man8/puppet-describe.8.*
%{_mandir}/man8/puppet-ca.8.*
%{_mandir}/man8/puppet-cert.8.*
%{_mandir}/man8/puppet-certificate.8.*
%{_mandir}/man8/puppet-certificate_request.8.*
%{_mandir}/man8/puppet-certificate_revocation_list.8.*
%{_mandir}/man8/puppet-config.8.*
%{_mandir}/man8/puppet-device.8.*
%{_mandir}/man8/puppet-doc.8.*
%{_mandir}/man8/puppet-facts.8.*
%{_mandir}/man8/puppet-file.8.*
%{_mandir}/man8/puppet-filebucket.8.*
%{_mandir}/man8/puppet-help.8.*
%{_mandir}/man8/puppet-inspect.8.*
%{_mandir}/man8/puppet-instrumentation_data.8.*
%{_mandir}/man8/puppet-instrumentation_listener.8.*
%{_mandir}/man8/puppet-instrumentation_probe.8.*
%{_mandir}/man8/puppet-key.8.*
%{_mandir}/man8/puppet-man.8.*
%{_mandir}/man8/puppet-module.8.*
%{_mandir}/man8/puppet-node.8.*
%{_mandir}/man8/puppet-parser.8.*
%{_mandir}/man8/puppet-plugin.8.*
%{_mandir}/man8/puppet-report.8.*
%{_mandir}/man8/puppet-resource.8.*
%{_mandir}/man8/puppet-resource_type.8.*
%{_mandir}/man8/puppet-secret_agent.8.*
%{_mandir}/man8/puppet-status.8.*
%{_mandir}/man8/extlookup2hiera.8.*

%files server
%defattr(-, root, root, 0755)
%{_unitdir}/puppetmaster.service
%config(noreplace) %{_sysconfdir}/puppet/fileserver.conf
%dir %{_sysconfdir}/puppet/manifests
%{_mandir}/man8/puppet-kick.8.*
%{_mandir}/man8/puppet-master.8.*
%{_mandir}/man8/puppet-queue.8.*

%ifnarch ppc64
%files -n emacs-puppet
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/*.el*
%{_emacs_sitestartdir}/*init.el
%endif

%files vim
%defattr(-, root, root)
%{vimmacrodir}/*/*.vim

# Fixed uid/gid were assigned in bz 472073 (Fedora), 471918 (RHEL-5),
# and 471919 (RHEL-4)
%pre
getent group puppet &>/dev/null || groupadd -r puppet -g 52 &>/dev/null
getent passwd puppet &>/dev/null || \
useradd -r -u 52 -g puppet -d %{_localstatedir}/lib/puppet -s /sbin/nologin \
    -c "Puppet" puppet &>/dev/null
# ensure that old setups have the right puppet home dir
if [ $1 -gt 1 ] ; then
  usermod -d %{_localstatedir}/lib/puppet puppet &>/dev/null
fi
exit 0

%post
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%post server
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%preun
if [ "$1" -eq 0 ] ; then
  /bin/systemctl --no-reload disable puppetagent.service > /dev/null 2>&1 || :
  /bin/systemctl stop puppetagent.service > /dev/null 2>&1 || :
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun server
if [ $1 -eq 0 ] ; then
  /bin/systemctl --no-reload disable puppetmaster.service > /dev/null 2>&1 || :
  /bin/systemctl stop puppetmaster.service > /dev/null 2>&1 || :
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%postun
if [ $1 -ge 1 ] ; then
  /bin/systemctl try-restart puppetagent.service >/dev/null 2>&1 || :
fi

%postun server
if [ $1 -ge 1 ] ; then
  /bin/systemctl try-restart puppetmaster.service >/dev/null 2>&1 || :
fi

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-1m)
- update to 3.3.1

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- [SECURITY] CVE-2013-2716
- update to 3.1.1
- support systemd (sync Fedora)

* Fri Mar 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.21-1m)
- [SECURITY] CVE-2013-1640 CVE-2013-1632 CVE-2013-1653 CVE-2013-1654
- [SECURITY] CVE-2013-1655 CVE-2013-2275
- update to 2.7.21

* Tue Aug  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.18-1m)
- [SECURITY] CVE-2012-3864 CVE-2012-3865 CVE-2012-3866 CVE-2012-3867 CVE-2012-3408
- update to 2.7.18

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.14-2m)
- rebuild for emacs-24.1

* Sat Jun  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.14-1m)
- update to 2.7.14
- fix vim-macro version

* Fri Apr 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.13-1m)
- [SECURITY] CVE-2012-1906 CVE-2012-1986 CVE-2012-1987 CVE-2012-1988
- update to 2.7.13

* Tue Feb 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.11-1m)
- [SECURITY] CVE-2012-1053 CVE-2012-1054
- update to 2.7.11

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.9-1m)
- update 2.7.9

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.7.1-3m)
- use RbConfig

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.1-2m)
- rename elisp-puppet to emacs-puppet

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-1m)
- update 2.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.0-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.0-2m)
- Requires: ruby(abi)-1.9.1

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.0-1m)
- update 2.6.0

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.9-2m)
- rebuild against emacs-23.2

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.9-1m)
- [SECURITY] CVE-2010-0156
- update to 0.24.9 for mo6 updates

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.8-7m)
- rename puppet-emacs to elisp-puppet
- kill puppet-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.8-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.8-5m)
- [SECURITY] CVE-2009-3564
- change /var/log/puppet permission
- import some bug fix patches from Rawhide (0.24.8-4)

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.24.8-4m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.24.8-3m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.8-2m)
- rebuild against emacs-23.0.95

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.8-1m)
- update to 0.24.8
- drop Source1, manpages included

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.4-7m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.4-6m)
- rebuild against xemacs-21.5.29

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.4-5m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.4-4m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.4-3m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Wed Jul 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.24.4-2m)
- use %%{_initscriptdir} instead of %%{_initrddir} again

* Wed Jul 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.24.4-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23.0-5m)
- rebuild against gcc43

* Wed Nov 14 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.23.0-4m)
- add emacs lisp, vim script, ldap schema

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.23.0-3m)
- PreReq: chkconfig, initscripts, shadow-utils

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.23.0-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sun Jul  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23.0-1m)
- update 0.23.0

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22.4-2m)
- remove puppetd.conf from puppet-server

* Fri Jun 15 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.22.4-1m)
- import from Fedora

* Wed May  2 2007 David Lutterkort <dlutter@redhat.com> - 0.22.4-1
- New version

* Thu Mar 29 2007 David Lutterkort <dlutter@redhat.com> - 0.22.3-1
- Claim ownership of _sysconfdir/puppet (bz 233908)

* Mon Mar 19 2007 David Lutterkort <dlutter@redhat.com> - 0.22.2-1
- Set puppet's homedir to /var/lib/puppet, not /var/puppet
- Remove no-lockdir patch, not needed anymore

* Mon Feb 12 2007 David Lutterkort <dlutter@redhat.com> - 0.22.1-2
- Fix bogus config parameter in puppetd.conf

* Sat Feb  3 2007 David Lutterkort <dlutter@redhat.com> - 0.22.1-1
- New version

* Fri Jan  5 2007 David Lutterkort <dlutter@redhat.com> - 0.22.0-1
- New version

* Mon Nov 20 2006 David Lutterkort <dlutter@redhat.com> - 0.20.1-2
- Make require ruby(abi) and buildarch: noarch conditional for fedora 5 or
  later to allow building on older fedora releases

* Mon Nov 13 2006 David Lutterkort <dlutter@redhat.com> - 0.20.1-1
- New version

* Mon Oct 23 2006 David Lutterkort <dlutter@redhat.com> - 0.20.0-1
- New version

* Tue Sep 26 2006 David Lutterkort <dlutter@redhat.com> - 0.19.3-1
- New version

* Mon Sep 18 2006 David Lutterkort <dlutter@redhat.com> - 0.19.1-1
- New version

* Thu Sep  7 2006 David Lutterkort <dlutter@redhat.com> - 0.19.0-1
- New version

* Tue Aug  1 2006 David Lutterkort <dlutter@redhat.com> - 0.18.4-2
- Use /usr/bin/ruby directly instead of /usr/bin/env ruby in
  executables. Otherwise, initscripts break since pidof can't find the
  right process

* Tue Aug  1 2006 David Lutterkort <dlutter@redhat.com> - 0.18.4-1
- New version

* Fri Jul 14 2006 David Lutterkort <dlutter@redhat.com> - 0.18.3-1
- New version

* Wed Jul  5 2006 David Lutterkort <dlutter@redhat.com> - 0.18.2-1
- New version

* Wed Jun 28 2006 David Lutterkort <dlutter@redhat.com> - 0.18.1-1
- Removed lsb-config.patch and yumrepo.patch since they are upstream now

* Mon Jun 19 2006 David Lutterkort <dlutter@redhat.com> - 0.18.0-1
- Patch config for LSB compliance (lsb-config.patch)
- Changed config moves /var/puppet to /var/lib/puppet, /etc/puppet/ssl 
  to /var/lib/puppet, /etc/puppet/clases.txt to /var/lib/puppet/classes.txt,
  /etc/puppet/localconfig.yaml to /var/lib/puppet/localconfig.yaml

* Fri May 19 2006 David Lutterkort <dlutter@redhat.com> - 0.17.2-1
- Added /usr/bin/puppetrun to server subpackage
- Backported patch for yumrepo type (yumrepo.patch)

* Wed May  3 2006 David Lutterkort <dlutter@redhat.com> - 0.16.4-1
- Rebuilt

* Fri Apr 21 2006 David Lutterkort <dlutter@redhat.com> - 0.16.0-1
- Fix default file permissions in server subpackage
- Run puppetmaster as user puppet
- rebuilt for 0.16.0

* Mon Apr 17 2006 David Lutterkort <dlutter@redhat.com> - 0.15.3-2
- Don't create empty log files in post-install scriptlet

* Fri Apr  7 2006 David Lutterkort <dlutter@redhat.com> - 0.15.3-1
- Rebuilt for new version

* Wed Mar 22 2006 David Lutterkort <dlutter@redhat.com> - 0.15.1-1
- Patch0: Run puppetmaster as root; running as puppet is not ready 
  for primetime

* Mon Mar 13 2006 David Lutterkort <dlutter@redhat.com> - 0.15.0-1
- Commented out noarch; requires fix for bz184199

* Mon Mar  6 2006 David Lutterkort <dlutter@redhat.com> - 0.14.0-1
- Added BuildRequires for ruby

* Wed Mar  1 2006 David Lutterkort <dlutter@redhat.com> - 0.13.5-1
- Removed use of fedora-usermgmt. It is not required for Fedora Extras and
  makes it unnecessarily hard to use this rpm outside of Fedora. Just
  allocate the puppet uid/gid dynamically

* Sun Feb 19 2006 David Lutterkort <dlutter@redhat.com> - 0.13.0-4
- Use fedora-usermgmt to create puppet user/group. Use uid/gid 24. Fixed 
problem with listing fileserver.conf and puppetmaster.conf twice

* Wed Feb  8 2006 David Lutterkort <dlutter@redhat.com> - 0.13.0-3
- Fix puppetd.conf

* Wed Feb  8 2006 David Lutterkort <dlutter@redhat.com> - 0.13.0-2
- Changes to run puppetmaster as user puppet

* Mon Feb  6 2006 David Lutterkort <dlutter@redhat.com> - 0.13.0-1
- Don't mark initscripts as config files

* Mon Feb  6 2006 David Lutterkort <dlutter@redhat.com> - 0.12.0-2
- Fix BuildRoot. Add dist to release

* Tue Jan 17 2006 David Lutterkort <dlutter@redhat.com> - 0.11.0-1
- Rebuild

* Thu Jan 12 2006 David Lutterkort <dlutter@redhat.com> - 0.10.2-1
- Updated for 0.10.2 Fixed minor kink in how Source is given

* Wed Jan 11 2006 David Lutterkort <dlutter@redhat.com> - 0.10.1-3
- Added basic fileserver.conf

* Wed Jan 11 2006 David Lutterkort <dlutter@redhat.com> - 0.10.1-1
- Updated. Moved installation of library files to sitelibdir. Pulled 
initscripts into separate files. Folded tools rpm into server

* Thu Nov 24 2005 Duane Griffin <d.griffin@psenterprise.com>
- Added init scripts for the client

* Wed Nov 23 2005 Duane Griffin <d.griffin@psenterprise.com>
- First packaging
