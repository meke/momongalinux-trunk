%global momorel 2
Summary:        GNU Smalltalk
Name:           gnu-smalltalk
Version:        3.2.5
Release: %{momorel}m%{?dist}

Source:         ftp://ftp.gnu.org/gnu/smalltalk/smalltalk-%{version}.tar.xz
NoSource:	0
Source1:        gnu-smalltalk.desktop
Source2:        gnu-smalltalk.svg

Patch1:         gst-3.2.5-am.patch
Patch2:         gst-3.2.3-ltdl.patch
Patch3:         gst-3.2.5-inf.patch
# for http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=435
Patch4:		smalltalk-3.2.4-emacs24.patch

License:        "GPLv2+ with exceptions"
Group:          Development/Languages
URL:            http://www.gnu.org/software/smalltalk/smalltalk.html

Buildroot:      %{_tmppath}/%{name}-%{version}-root-%(%{__id_u} -n)

ExcludeArch:    ppc64 ppc

Requires(post):  /sbin/install-info
Requires(preun): /sbin/install-info

BuildRequires:  prelink
BuildRequires:  tk-devel
BuildRequires:  gtk2-devel
BuildRequires:  gdbm-devel
BuildRequires:  gmp-devel
BuildRequires:  gnutls-devel >= 3.2.0
BuildRequires:  readline-devel
BuildRequires:  emacs
BuildRequires:  libtool
BuildRequires:  libtool-ltdl-devel
BuildRequires:  texinfo
BuildRequires:  zlib-devel
BuildRequires:  libsigsegv-devel
BuildRequires:  mysql-devel
BuildRequires:  sqlite-devel
BuildRequires:  libffi-devel
BuildRequires:  pkgconfig
BuildRequires:  zip
BuildRequires:  mesa-libGL-devel
BuildREquires:  mesa-libGLU-devel
BuildRequires:  desktop-file-utils

%description
GNU Smalltalk is an implementation that closely follows the
Smalltalk-80 language as described in the book `Smalltalk-80: the
Language and its Implementation' by Adele Goldberg and David Robson.
The Smalltalk programming language is an object oriented programming
language.

Unlike other Smalltalks (including Smalltalk-80), GNU Smalltalk
emphasizes Smalltalk's rapid prototyping features rather than the
graphical and easy-to-use nature of the programming environment.

Therefore, even though we have a nice GUI environment including a class
browser, the goal of the GNU Smalltalk project is currently to produce a
complete system to be used to write your scripts in a clear, aesthetically
pleasing, and philosophically appealing programming language.

%package -n emacs-gnu-smalltalk
Summary:    Emacs mode for GNU Smalltalk
Group:      Applications/Editors
Requires:   %{name} = %{version}, emacs
Provides:   gnu-smalltalk-emacs = %{version}-%{release}
Obsoletes:  gnu-smalltalk-emacs < 3.2-5
BuildArch:  noarch

%description -n emacs-gnu-smalltalk
This Package contains the Smalltalk mode for Emacs.

%package -n emacs-gnu-smalltalk-el
Summary:    ELisp source files for emacs-gnu-smaltalk
Group:      Applications/Editors
Requires:   emacs-gnu-smalltalk = %{version}-%{release}
Provides:   gnu-smalltalk-emacs = %{version}-%{release}
Obsoletes:  gnu-smalltalk-emacs < 3.2.4
BuildArch:  noarch

%description -n emacs-gnu-smalltalk-el
This Package contains the ELisp sources for the 
Smalltalk mode.

%package devel
Summary: Development Stuff for the GNU Smalltalk package
Group: Development/Libraries
#Requires: automake
Requires: pkgconfig
%description devel
This Package contains header files and other stuff provided by
GNU Smalltalk.

You will need this package, if you want to extent GNU Smalltalk
with functions written in C.

%prep
%setup -q -n smalltalk-%{version}
%patch1 -p1 -b .auto
%patch2 -p1 -b .ltdl
%patch3 -p1 -b .inf
%patch4 -p1 -b .emacs24~

%build
libtoolize
autoreconf
rm -rf lib-src/lt*
CFLAGS="$RPM_OPT_FLAGS -Wa,--noexecstack"
%configure --with-tcl=%{_libdir} --with-tk=%{_libdir} \
  --enable-static=no --enable-shared=yes --disable-rpath \
  --with-system-libsigsegv \
  --with-system-libffi=yes \
  --with-system-libltdl=yes \
  --with-lispdir=%{_emacs_sitelispdir}/gnu-smalltalk \
  --with-lispstartdir=%{_emacs_sitestartdir} \
  --with-imagedir=%{_libdir}/%{name}

# _smp_mflags seems not to work
make # %{?_smp_mflags}

cd doc

for i in gst*; do
  sed -e 's!%{_libdir}!/usr/lib(64)!g' \
      -e 's!/usr/lib!/usr/lib(64)!g' \
      -e 's!/usr/share/gnu-smalltalk/kernel!/usr/lib(64)/gnu-smalltalk/kernel!g' \
      $i >$i.new
  mv -f $i.new $i
done

%install
rm -rf $RPM_BUILD_ROOT

make DESTDIR=$RPM_BUILD_ROOT INSTALL="install -c -p" install

mkdir -p  %{buildroot}%{_emacs_sitestartdir}

mv %{buildroot}%{_emacs_sitelispdir}/gnu-smalltalk/site-start.d/* \
   %{buildroot}%{_emacs_sitestartdir}

rmdir %{buildroot}%{_emacs_sitelispdir}/gnu-smalltalk/site-start.d

rm -rf $RPM_BUILD_ROOT/%{_libdir}/libgst*a*
rm -rf $RPM_BUILD_ROOT/%{_libdir}/gnu-smalltalk/*.la

rm -rf $RPM_BUILD_ROOT/%{_libdir}/%{name}/*.a

rm -rf $RPM_BUILD_ROOT/%{_infodir}/dir

cd $RPM_BUILD_ROOT/%{_mandir}/man1

rm gst-reload.1
ln -sf ./gst-load.1 gst-reload.1

mkdir -p %{buildroot}/%{_datadir}/pixmaps
install -p -m 0644 %{SOURCE2} %{buildroot}/%{_datadir}/pixmaps

desktop-file-install \
   --dir %{buildroot}%{_datadir}/applications \
  %{SOURCE1}

%check
make check

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/install-info %{_infodir}/gst.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/gst-base.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/gst-libs.info %{_infodir}/dir || :
/sbin/ldconfig

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/gst.info %{_infodir}/dir || :
  /sbin/install-info --delete %{_infodir}/gst-base.info %{_infodir}/dir || :
  /sbin/install-info --delete %{_infodir}/gst-libs.info %{_infodir}/dir || :
fi

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/gst
%{_bindir}/gst-blox
%{_bindir}/gst-convert
%{_bindir}/gst-doc
%{_bindir}/gst-load
%{_bindir}/gst-package
%{_bindir}/gst-reload
%{_bindir}/gst-remote
%{_bindir}/gst-sunit
%{_bindir}/gst-browser
%{_bindir}/gst-profile

%{_libdir}/libgst.so.*

%{_infodir}/gst.info*
%{_infodir}/gst-*.info*

%{_datadir}/gnu-smalltalk/
%{_libexecdir}/gnu-smalltalk/

%{_mandir}/man1/gst.1*
%{_mandir}/man1/gst-*


%{_datadir}/applications/*.desktop
%{_datadir}/pixmaps/*.svg

%doc AUTHORS COPYING COPYING.DOC COPYING.LIB ChangeLog 
%doc NEWS README THANKS TODO

%{_libdir}/gnu-smalltalk/

%files devel
%defattr(-,root,root,-)
%{_bindir}/gst-config
%{_libdir}/libgst.so
%{_libdir}/pkgconfig/gnu-smalltalk.pc

%{_datadir}/aclocal/*.m4

%{_includedir}/gst.h   
%{_includedir}/gstpub.h 

%files -n emacs-gnu-smalltalk
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/gnu-smalltalk/*.elc
%{_emacs_sitestartdir}/*.elc

%files -n emacs-gnu-smalltalk-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/gnu-smalltalk/*.el
%{_emacs_sitestartdir}/*.el

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.5-2m)
- rebuild against gnutls-3.2.0

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.5-1m)
- update to 3.2.5

* Sun Aug  5 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.4-2m)
- fix up Obsoletes

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.4-1m)
- update to 3.2.4
-- add patch to fix BTS #435

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-7m)
- rebuild for emacs-24.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-5m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-4m)
- rebuild against gmp-5.0.1

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-3m)
- build fix with libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.2-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-4m)
- rebuild against emacs-23.2

* Tue Jun  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2-3m)
- fix gnu-smalltalk.pc (patch4)

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-2m)
- remove dups

* Tue May 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-1m)
- update to 3.2
- use system ltdl

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-5m)
- rebuild against readline6

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-4m)
- [SECURITY] CVE-2009-3736
- fixed ltdl privilege escalation

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-3m)
- rebuild against libsigsegv-2.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-1m)
- import from Fedora 11

* Sun May 24 2009 Jochen Schmitt <Jochen herr-schmitt de> 3.1-5.1
- Fix dependency issue

* Thu Mar  5 2009 Jochen Schmitt <Jochen herr-schmitt de> 3.1-4
- Supporting noarch subpackages

* Tue Mar  3 2009 Jochen Schmitt <Jochen herr-schmitt de> 3.1-3
- Fix retcode source issue

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> 3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Oct 19 2008 Jochen Schmitt <Jochen herr-schmitt de> 3.1-1
- New upstream release

* Sun Aug 10 2008 Jochen Schmitt <Jochen herr-schmitt de> 3.0.4-2
- Add zip as BR
- New upstream release

* Sun Jun  8 2008 Jochen Schmitt <Jochen herr-schmitt de> 3.0.3-2
- Disable 'make check'

* Wed May 14 2008 Jochen Schmitt <Jochen herr-schmitt de> 3.0.3-1
- New upstream release

* Thu Apr 17 2008 Jochen Schmitt <Jochen herr-schmitt de> 3.0.2-4
- Patch configure.ac to make version independency from libffi

* Sat Mar 08 2008 Xavier Lamien <lxtnow[at]gmail.com> 3.0.2-2
- Updated release.
- Disable x86_64 arch, because 'make test' fails on this arch

* Sun Feb 17 2008 Jochen Schmitt <Jochen herr-schmitt de> 3.0.1-3
- Use system libffi

* Sun Feb 10 2008 Jochen Schmitt <Jochen herr-schmitt de> 3.0.1-1
- New upstream release

* Mon Jan 21 2008 Jochen Schmitt <Jochen herr-schmitt de> 3.0-1
- New upstream release

* Thu Jan 03 2008 Alex Lancaster <alexlan fedoraproject.org> 2.3.6-8
- Rebuild for Tcl 8.5

* Sun Nov 18 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.6-7
- Fix broken Changelog

* Thu Oct 25 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.6-6
- Add upstream multilib patch

* Wed Oct 24 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.6-4
- Another try to fix the multilib issue

* Mon Oct 22 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.6-3
- Create new subpackage to solve mulitlib issue (#341341)

* Sun Sep  9 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.6-2
- Remove build path from gst.im
- Temporarly disable ppc64

* Thu Sep  6 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.6-1
- New upstream release

* Thu Aug  9 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.5-3
- Try to fix smp_mflags issue

* Wed Aug  8 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.5-2
- Changing license tag

* Sun Jun  3 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.5-1
- New upstream release

* Wed May 30 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.4-4
- Remove references to sigseg lib shiped with the package

* Mon May 28 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.4-1
- New upstream release

* Sun Mar 18 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.3-5
- Include Publish.st patch

* Tue Mar 13 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3.3-4
- Fix wrong paths in gst.im

* Wed Feb 14 2007 Jochen Schmitt <s4504kr@zeus.herr-schmitt.de> 2.3.3-3
- New upstream release

* Tue Feb 13 2007 Jochen Schmitt <s4504kr@zeus.herr-schmitt.de> 2.3.2-6
- Solve multilib issue (#228175)

* Sun Feb 11 2007 Jochen Schmitt <s4504kr@zeus.herr-schmitt.de> 2.3.2-5
- Rebuild to fix broken deps.

* Wed Jan 31 2007 Jochen Schmitt <s4504kr@zeus.herr-schmitt.de> 2.3.2-4
- New upstream release

* Wed Dec 13 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3.1-1
- New upstream release

* Thu Dec  7 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-4
- Exclude x86_64 bc/ build failure

* Thu Dec  7 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-3
- Fix wrong lib option in gst-config

* Wed Dec  6 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-2
- Fix wrong Requires
- Fix gst-package.in file

* Tue Dec  5 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-1
- New upstream release

* Wed Nov 29 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.2c-4
- Remove files which will be gone in gnu-smalltalk-2.3

* Tue Nov 28 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.2c-3
- Cleanup configure section
- Try to preserve timestamps

* Mon Nov 27 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.2c-2
- Done some cleanup on configure step
- Add Patch to fix broken gst-config

* Mon Nov 20 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.2c-1
- New upstream release

* Mon Feb 20 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.2-8
- Add libtool as BuildRequires
- Add LIBTOOL=/usr/bin/libtool at the make step

* Tue Jan 31 2006 Jochen Schmitt <jochen herr-schmitt de> 2.2-7
- Fix rpmlint errors

* Tue Jan 10 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.2-6
- Added --disable-rpath
- Added --enable-static=no
- fix broken Shebangs

* Tue Dec 13 2005 Jochen Schmitt <Jochen herr-schmitt de> 2.2-5
- Deps from -devel and -emacs more strict
- Move libgst.so.* to main package

* Wed Dec  7 2005 Jochen Schmitt <Jochen herr-schmitt de> 2.2-4
- remove dep to lightning

* Sun Dec  4 2005 Jochen Schmitt <Jochen herr-schmitt de> 2.2-3
- Add aclocal
- Add depend to lightning

* Tue Nov 29 2005 Jochen Schmitt <Jochen herr-schmitt de> 2.2-2
- Rename package
- install-info for gst-base and gst-libs
- move libgst.so to devel package

* Thu Nov 24 2005 Jochen Schmitt <Jochen herr-schmitt de> 2.2-1
- Initial RPM
