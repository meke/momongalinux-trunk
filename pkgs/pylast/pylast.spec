%global momorel 4
%global pythonver 2.7
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: A Python interface to the Last.fm API
Name: pylast
Version: 0.4.30
Release: %{momorel}m%{?dist}
License: ASL 2.0
Group: Development/Languages
URL: http://code.google.com/p/pylast/
Source0: http://pypi.python.org/packages/source/p/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python >= %{pythonver}
BuildRequires: coreutils
BuildRequires: python-devel >= %{pythonver}
BuildRequires: python-setuptools

%description
A Python interface to the Last.fm API 2.0.

Features

    * Simple public interface
    * Silent exception handling
    * Internal support for asynchronous operations
    * ...errr, what else?! 

%prep
%setup -q

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install --root=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING INSTALL README
%{python_sitelib}/%{name}-%{version}-py?.?.egg-info
%{python_sitelib}/%{name}.py*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.30-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.30-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.30-2m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.30-1m)
- version 0.4.30
- change License from GPLv3 to ASL 2.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.26-2m)
- full rebuild for mo7 release

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.26-1m)
- version 0.4.26

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.24-1m)
- version 0.4.24

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.20-1m)
- version 0.4.20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.18-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.18-1m)
- version 0.4.18

* Sun Oct 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.16-1m)
- version 0.4.16
- change License from GPLv2+ to GPLv3

* Sat May 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.4-1m)
- version 0.3.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.18-2m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.18-1m)
- version 0.2.18

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.17-2m)
- rebuild against python-2.6.1-2m

* Sun Nov  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.17-1m)
- version 0.2.17

* Fri Oct 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2b13-1m)
- split from lastagent

* Wed Oct  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.3-1m)
- version 0.2.3

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-3m)
- explicitly Requires: dbus-python, python-sexy

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-2m)
- apply pidof.patch

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-1m)
- initial package for music freaks using Momonga Linux
