%global momorel 3

Summary: DHIS client daemon
Name: dhid
Version: 5.5
Release: %{momorel}m%{?dist}
Url: http://www.dhis.org/
Group: System Environment/Daemons
License: Modified BSD

Source0: ftp://ftp.dhis.org/pub/dhis/%{name}-%{version}.tar.gz
NoSource: 0
Source1: dhid.init

BuildRequires: gmp-devel >= 5.0
Requires(post): chkconfig
#Requires(preun): chkconfig

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
dhid is the DHIS client daemon. After setting up with a DHIS provider,
each machine should run a dhid daemon (in background) in order to
update its DNS tables at the server.

%prep
%setup -q

%build
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_initscriptdir}
mkdir -p %{buildroot}%{_datadir}/config-sample/dhid
install -m 755 dhid %{buildroot}%{_sbindir}
install -m 755 genkeys %{buildroot}%{_sbindir}
install -m 755 %{SOURCE1} %{buildroot}%{_initscriptdir}/dhid
install -m 644 dhid.conf.sample %{buildroot}%{_datadir}/config-sample/dhid

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add dhid
if test -r /var/run/dhid.pid; then
    %{_initscriptdir}/dhid restart
fi

%preun
if [ "$1" = 0 ]; then
    if test -r /var/run/dhid.pid; then
        %{_initscriptdir}/dhid stop
    fi
    # /sbin/chkconfig --del dhid
fi

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.5-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.5-1m)
- update 5.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1-7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1-4m)
- rebuild against gcc43

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1-3m)
- stop daemon

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.1-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Jul  2 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (5.1-1m)
- version 5.1

* Thu Nov 22 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (4.0.2-4k)
- comment out %chkconfig

* Sat Feb 10 2001 Uechi Yasumasa <uh@u.dhis.portside.net>
- (4.0.2-3k)
- 1st release.

%files
%defattr(-,root,root)
%doc AUTHORS COPYRIGHT INSTALL README WHATSNEW 
%config %{_initscriptdir}/dhid
%{_sbindir}/dhid
%{_sbindir}/genkeys
%{_datadir}/config-sample/dhid
