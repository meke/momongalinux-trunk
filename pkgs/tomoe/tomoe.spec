%global momorel 29
%global ruby_os %{_target_os}
%global ruby_arch %{_target_cpu}-%{ruby_os}

%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

%global ruby18_sitearchdir %(ruby18 -rrbconfig -e 'puts RbConfig::CONFIG["sitearchdir"]')
%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts RbConfig::CONFIG["sitelibdir"]')

Summary: A program which does Japanese handwriting recognition
Name: tomoe
Version: 0.6.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Applications/System
URL: https://sourceforge.jp/projects/tomoe/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-no-e.patch
Patch1: %{name}-%{version}-bz502662.patch
Patch2: %{name}-%{version}-lib64.patch
Patch3: tomoe-0.6.0-glib2332.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: apr-devel
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: doxygen
BuildRequires: glib2-devel
BuildRequires: gtk-doc
BuildRequires: hyperestraier
BuildRequires: libtool
BuildRequires: neon-devel
BuildRequires: mysql-devel >= 5.5.10
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: pkgconfig
BuildRequires: python-devel >= 2.7
BuildRequires: qdbm-devel >= 1.8.77
BuildRequires: subversion-devel >= 1.8.0
BuildRequires: unzip

%description
A program which does Japanese handwriting recognition.

%package devel
Summary: Headers of tomoe for development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk-doc
Requires: pkgconfig

%description devel
tomoe development package: static libraries, header files, and the like.

%prep
%setup -q
%patch0 -p1 -b .bye-e
%patch1 -p0 -b .load
%patch2 -p1 -b .lib64
%patch3 -p1 -b .glib2332~

autoreconf -fi

%build
%configure --disable-static
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of la files
find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%dir %{_sysconfdir}/%{name}
%config %{_sysconfdir}/%{name}/config
%{python_sitearch}/%{name}.so
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/module
%dir %{_libdir}/%{name}/module/dict
%{_libdir}/%{name}/module/dict/*.so
%dir %{_libdir}/%{name}/module/recognizer
%{_libdir}/%{name}/module/recognizer/simple.so
%{_libdir}/lib%{name}.so.*
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/%{name}

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/py%{name}.pc
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}.so
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-29m)
- rebuild against subversion-1.8

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-28m)
- fix glib-2.33 patch

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-27m)
- fix build failure with glib-2.33

* Wed Apr 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-26m)
- good-bye ruby18

* Mon Mar 19 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-25m)
- rebuild against hyperestraier 1.4.13-11m

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-24m)
- build fix

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-23m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-22m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-21m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-19m)
- full rebuild for mo7 release

* Tue Aug 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-18m)
- build with ruby18

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-17m)
- rebuild against ruby-1.9.2

* Thu May  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-16m)
- use python_sitearch

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-15m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-13m)
- enable to build with subversion on x86_64 (Patch2)

* Mon Aug  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-12m)
- rebuild against subversion-1.6.3-2m

* Fri Jul  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-11m)
- import bug fix patch from Fedora
-- https://bugzilla.redhat.com/show_bug.cgi?id=502662

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-10m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-9m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-8m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.0-7m)
- rebuild against python-2.6.1-2m

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-6m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-4m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-3m)
- rebuild against qdbm-1.8.77

* Thu Aug 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-2m)
- change BPR from ruby-gnome2-devel to ruby-glib2-devel

* Thu Jul  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-1m)
- version 0.6.0
- update no-e.patch

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.1-3m)
- rebuild against ruby-1.8.6-4m

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-2m)
- rebuild against qdbm-1.8.75

* Mon Feb 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-1m)
- version 0.5.1

* Fri Dec 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-1m)
- version 0.5.0

* Thu Nov 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- version 0.4.0
- BuildPreReq: glib-devel, pkgconfig, ruby-devel
- tomoe-devel Requires: pkgconfig, gtk-doc

* Thu Nov  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- version 0.3.0

* Fri Jan 13 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.1-3m)
- apply 'tomoe-0.2.1-no-e.patch' to avoid dependency with enlightenment

* Wed Jan 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-2m)
- modify %%build section for new libtool

* Sat Aug 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-1m)
- version 0.2.1

* Fri Jul  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-1m)
- version 0.2.0
- add a package tomoe-devel
- add %%post and %%postun sections

* Wed Mar  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-1m)
- initial package for Momonga Linux
- import %%description from cooker
