# Generated from rest-client-1.6.7.gem by gem2rpm -*- rpm-spec -*-
%global momorel 3
%global gemname rest-client

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Simple HTTP and REST client for Ruby, inspired by microframework syntax for specifying actions
Name: rubygem-%{gemname}
Version: 1.6.7
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/archiloque/rest-client
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(mime-types) >= 1.16
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
A simple HTTP and REST client for Ruby, inspired by the Sinatra microframework
style of specifying actions: get, put, post, delete.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%files
%dir %{geminstdir}
%{_bindir}/restclient
%{gemdir}/gems/%{gemname}-%{version}/
%exclude %{geminstdir}/README.rdoc
%exclude %{geminstdir}/history.md
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/history.md


%changelog
* Fri Jul 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.7-3m)
- fix %%files to avoid conflicting

* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.7-2m)
- old pkg was broken

* Wed Sep 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.7-1m) 
- Initial package for Momonga Linux
