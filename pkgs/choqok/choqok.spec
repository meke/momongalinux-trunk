%global momorel 1
%global sourcever 1.4
%global git 0
%global date 20130620
%global gitmomorel 0
%global qtver 4.8.5
%global kdever 4.11.0
%global kdelibsrel 1m
%global atticaver 0.4.2

Summary: KDE Micro-Blogging client, Currently supports Identi.ca and Twitter.com
Name: choqok
Version: 1.4
%if %{git}
Release: %{gitmomorel}.%{date}.%{momorel}m%{?dist}
%else
Release: %{momorel}m%{?dist}
%endif
License: GPLv3
Group: Applications/Internet
URL: http://choqok.gnufolks.org/
Source0: http://dl.sourceforge.net/project/%{name}/Choqok/%{name}-%{sourcever}.tar.xz
NoSource: 0
Patch0: %{name}-1.2-default-font.patch
%if %{git}
Patch100: %{name}-%{sourcever}-%{date}git.patch
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: qca2
Requires: qjson
Requires: qoauth
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: attica-devel >= %{atticaver}
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: qca2-devel
BuildRequires: qjson-devel
BuildRequires: qoauth-devel

%description
Choqok:
KDE Micro-Blogging client, Currently supports Identi.ca and Twitter.com.

%package libs
Summary: Shared runtime libraries of choqok
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of choqok.

%package devel
Summary: choqok - files for developing
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on choqok.

%prep
%setup -q -n %{name}-%{sourcever}

%patch0 -p1 -b .default-font

%if %{git}
%patch100 -p1
%endif

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# move dbus service file to correct space
mkdir -p %{buildroot}%{_datadir}/dbus-1/services
mv %{buildroot}%{_kde4_appsdir}/dbus-1/services/org.kde.choqok.service %{buildroot}%{_datadir}/dbus-1/services/

# revise desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# remove conflicting files with KDE 4.10
rm -f %{buildroot}%{_kde4_appsdir}/cmake/modules/FindQtOAuth.cmake

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database &> /dev/null || :

%postun
%{_bindir}/update-desktop-database &> /dev/null || :

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING README TODO changelog
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/*%{name}*.so
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}*
%{_kde4_appsdir}/khtml/kpartplugins/konq%{name}.*
%{_kde4_datadir}/config.kcfg/*.kcfg
%{_datadir}/dbus-1/services/org.kde.choqok.service
%{_kde4_iconsdir}/hicolor/*/actions/retweet.png
%{_kde4_iconsdir}/hicolor/*/apps/*.png
%{_kde4_datadir}/kde4/services/ServiceMenus/%{name}_*.desktop
%{_kde4_datadir}/kde4/services/%{name}_*.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}*plugin.desktop
%{_datadir}/doc/HTML/en/choqok
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%files libs
%defattr(-, root, root)
%{_kde4_libdir}/lib%{name}*.so.*
%{_kde4_libdir}/libtwitterapihelper.so.*

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/lib%{name}*.so
%{_kde4_libdir}/libtwitterapihelper.so
%{_kde4_appsdir}/cmake/modules/Find*.cmake

%changelog
* Thu Sep  5 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-1m)
- version 1.4

* Thu Jun 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-0.20130620.1m)
- update to 20130620 git snapshot

* Tue Jun 18 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-0.20130618.1m)
- apply many fixes from https://launchpad.net/~pfoo/+archive/choqok/+packages to work with new twitter
- https://bugs.kde.org/show_bug.cgi?id=264091

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-3m)
- rebuild with KDE 4.10 beta2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-2m)
- rebuild against attica-0.4.0

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-3m)
- rebuild against attica-0.3.0

* Wed Nov  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-2m)
- change default font from "DejaVu Sans" to "M+1P+IPAG"

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-3m)
- fix "this method requires authentication" issue
- https://bugs.kde.org/show_bug.cgi?id=275185

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Thu Feb  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Tue Dec  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.98-1m)
- update to 1.0RC1 (0.9.98)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.92-3m)
- rebuild for new GCC 4.5

* Tue Nov  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.92-2m)
- fix up Requires and BuildRequires

* Tue Nov  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.92-1m)
- initial package for Twitter freaks using Momonga Linux
