%global momorel 2

Name:           accountsservice
Version:        0.6.29
Release: %{momorel}m%{?dist}
Summary:        D-Bus interfaces for querying and manipulating user account information

Group:          System Environment/Daemons
License:        GPLv3+
URL:            http://www.fedoraproject.org/wiki/Features/UserAccountDialog
#VCS: git:git://git.freedesktop.org/accountsservice
Source0:        http://www.freedesktop.org/software/accountsservice/accountsservice-%{version}.tar.xz
NoSource: 0
Patch0:         32bit-crash.patch

BuildRequires:  glib2-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  polkit-devel
BuildRequires:  intltool
BuildRequires:  systemd-units
BuildRequires:  systemd-devel >= 188
BuildRequires:  gobject-introspection-devel
BuildRequires:  vala-devel
BuildRequires:  vala-tools

Requires:       polkit
Requires:       shadow-utils

%package libs
Summary: Client-side library to talk to accountsservice
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description libs
The accountsservice-libs package contains a library that can
be used by applications that want to interact with the accountsservice
daemon.


%package devel
Summary: Development files for accountsservice-libs
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}

%description devel
The accountsservice-devel package contains headers and other
files needed to build applications that use accountsservice-libs.


%description
The accountsservice project provides a set of D-Bus interfaces for
querying and manipulating user account information and an implementation
of these interfaces, based on the useradd, usermod and userdel commands.


%prep
%setup -q

%patch0 -p1 -b .gdm

%build
%configure
%make 


%install
make install DESTDIR=%{buildroot}
rm %{buildroot}%{_libdir}/*.la
rm %{buildroot}%{_libdir}/*.a
%find_lang accounts-service


%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig


%files -f accounts-service.lang
%defattr(-,root,root,-)
%doc COPYING README AUTHORS
%{_sysconfdir}/dbus-1/system.d/org.freedesktop.Accounts.conf
%{_libexecdir}/accounts-daemon
%{_datadir}/dbus-1/interfaces/org.freedesktop.Accounts.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Accounts.User.xml
%{_datadir}/dbus-1/system-services/org.freedesktop.Accounts.service
%{_datadir}/polkit-1/actions/org.freedesktop.accounts.policy
%dir %{_localstatedir}/lib/AccountsService/
%dir %{_localstatedir}/lib/AccountsService/users
%dir %{_localstatedir}/lib/AccountsService/icons
%{_unitdir}/accounts-daemon.service

%files libs
%{_libdir}/libaccountsservice.so.*
%{_libdir}/girepository-1.0/AccountsService-1.0.typelib

%files devel
%{_includedir}/accountsservice-1.0
%{_libdir}/libaccountsservice.so
%{_libdir}/pkgconfig/accountsservice.pc
%{_datadir}/gir-1.0/AccountsService-1.0.gir
%{_datadir}/vala/vapi/*
%{_datadir}/gtk-doc/html/libaccountsservice

%changelog
* Sat Dec 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.29-2m)
- [CRASH FIX]
- import 32bit-crash.patch from arch linux, gdm is working again

* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.29-1m)
- update to 0.6.29

* Thu Sep 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.24-1m)
- update to 0.6.24

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.22-2m)
- rebuild with systemd-188

* Tue Jul 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.22-1m)
- reimport from fedora
- [SECURITY] CVE-2012-2737

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.15-3m)
- fix build failure with glib 2.33

* Wed Dec 21 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.6.15-2m)
- Bug 757257 - after selecting a user image (avatar), can't go back to no user image
- add 0001-Be-more-careful-when-resetting-the-users-icons.patch

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.15-1m)
- update to 0.6.15

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.14-1m)
- update to 0.6.14

* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.13-2m)
- add 0001-Fix-fast-user-switching.patch

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.13-1m)
- update to 0.6.13

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.12-2m)
- enable to build with systemd

* Wed Jun 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.12-1m)
- update to 0.6.12

* Mon May  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.10-1m)
- initial build

