%global momorel 5

Name:		gbdfed
Summary: 	Bitmap Font Editor
Version:	1.6
Release:	%{momorel}m%{?dist}
License:	MIT
Group:		Applications/System
Source0:	http://www.math.nmsu.edu/~mleisher/Software/gbdfed/%{name}-%{version}.tbz2
NoSource:       0
Source1:	http://www.math.nmsu.edu/~mleisher/Software/gbdfed/%{name}16x16.png
Source2:	gbdfed.desktop
Patch0:		gbdfed-1.6-gtk222.patch
URL:		http://www.math.nmsu.edu/~mleisher/Software/gbdfed/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	freetype-devel, pango-devel, libX11-devel, libICE-devel, gtk2-devel
BuildRequires:	desktop-file-utils

%description
gbdfed lets you interactively create new bitmap font files or 
modify existing ones. It allows editing multiple fonts and multiple 
glyphs, it allows cut and paste operations between fonts and glyphs and 
editing font properties. The editor works natively with BDF fonts.

%prep
%setup -q 
%patch0 -p1 -b .gtk222~

%build
autoconf
%configure LIBS="-lX11"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR="%{buildroot}" install
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -p -m0644 %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/gbdfed.png
desktop-file-install					\
	--vendor="" --dir %{buildroot}%{_datadir}/applications	\
	%{SOURCE2}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%{_bindir}/gbdfed
%{_datadir}/pixmaps/gbdfed.png
%{_datadir}/applications/*.desktop
%{_mandir}/man1/gbdfed*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-4m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6-3m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-5m)
- fix build

* Sun Apr 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-4m)
- add patch for gtk-2.20 by gengtk220patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- apply glibc210 patch

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Dec 10 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.4-1
- initial Fedora package
