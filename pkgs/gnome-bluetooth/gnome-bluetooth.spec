%global momorel 1
Name:		gnome-bluetooth
Version:	3.6.0
Release: %{momorel}m%{?dist}
Summary:	Bluetooth graphical utilities

Group:		Applications/Communications
License:	GPLv2+
URL:		http://live.gnome.org/GnomeBluetooth
Source0:	http://download.gnome.org/sources/gnome-bluetooth/3.6/gnome-bluetooth-%{version}.tar.xz
NoSource: 0
Source1:	61-gnome-bluetooth-rfkill.rules

ExcludeArch:	s390 s390x

BuildRequires:	gtk3-devel >= 3.0
BuildRequires:	GConf2-devel
BuildRequires:	dbus-glib-devel
BuildRequires:	libnotify-devel
BuildRequires:	gnome-doc-utils rarian-compat
BuildRequires:	nautilus-sendto-devel >= 2.90.0

BuildRequires:	intltool desktop-file-utils gettext gtk-doc

BuildRequires:	gobject-introspection-devel

Obsoletes:	bluez-pin
Provides:	dbus-bluez-pin-helper
Conflicts:	bluez-gnome <= 1.8
Obsoletes:	bluez-gnome <= 1.8

# Otherwise we might end up with mismatching version
Requires:	%{name}-libs = %{version}-%{release}
Requires:	gvfs-obexftp
Requires:	bluez >= 4.42
Requires:	obexd
Requires:	desktop-notification-daemon
Requires:	pulseaudio-module-bluetooth
Requires:	control-center

Requires(post):		desktop-file-utils
Requires(postun):	desktop-file-utils

%description
The gnome-bluetooth package contains graphical utilities to setup,
monitor and use Bluetooth devices.

%package libs
Summary:	GTK+ Bluetooth device selection widgets
Group:		System Environment/Libraries
License:	LGPLv2+
Requires:	gobject-introspection

%description libs
This package contains libraries needed for applications that
want to display a Bluetooth device selection widget.

%package libs-devel
Summary:	Development files for %{name}-libs
Group:		Development/Libraries
License:	LGPLv2+
Requires:	%{name}-libs = %{version}-%{release}
Requires:	gobject-introspection-devel
Obsoletes:	gnome-bluetooth-devel < 3.4
Provides:	gnome-bluetooth-devel = %{version}

%description libs-devel
This package contains the libraries and header files that are needed
for writing applications that require a Bluetooth device selection widget.

%prep
%setup -q
# Upstream commit 855afd329b9d833c57b9652380b15ee5ab29c372
sed -i 's|3.1.0|3.7.0|g' configure

%build
%configure --disable-desktop-update --disable-icon-update --enable-nautilus-sendto=yes --disable-schemas-compile
%make 

%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot}

rm -f 	   %{buildroot}/%{_libdir}/gnome-bluetooth/plugins/*.la \
	   %{buildroot}/%{_libdir}/nautilus-sendto/plugins/*.la
#	   %{buildroot}%{_libdir}/libgnome-bluetooth.la \

install -m0644 -D %{SOURCE1} %{buildroot}/lib/udev/rules.d/61-gnome-bluetooth-rfkill.rules

%find_lang gnome-bluetooth2

%post
update-desktop-database &>/dev/null || :

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%pre
if [ "$1" -gt 1 ]; then
	export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
	if [ -f %{_sysconfdir}/gconf/schemas/gnome-obex-server.schemas ] ; then
		gconftool-2 --makefile-uninstall-rule \
		%{_sysconfdir}/gconf/schemas/gnome-obex-server.schemas >/dev/null || :
	fi
	if [ -f %{_sysconfdir}/gconf/schemas/bluetooth-manager.schemas ] ; then
		gconftool-2 --makefile-uninstall-rule 				\
		%{_sysconfdir}/gconf/schemas/bluetooth-manager.schemas		\
		>& /dev/null || :
	fi
fi

%preun
if [ "$1" -eq 0 ]; then
	export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
	if [ -f %{_sysconfdir}/gconf/schemas/gnome-obex-server.schemas ] ; then
		gconftool-2 --makefile-uninstall-rule \
		%{_sysconfdir}/gconf/schemas/gnome-obex-server.schemas > /dev/null || :
	fi
	if [ -f %{_sysconfdir}/gconf/schemas/bluetooth-manager.schemas ] ; then
		gconftool-2 --makefile-uninstall-rule 				\
		%{_sysconfdir}/gconf/schemas/bluetooth-manager.schemas		\
		>& /dev/null || :
	fi
fi

%postun
update-desktop-database &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
if [ $1 -eq 0 ] ; then
	touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
	gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%post libs
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%posttrans libs
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%postun libs
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
	touch --no-create %{_datadir}/icons/hicolor &>/dev/null
	gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%files
%defattr(-,root,root,-)
%doc README NEWS COPYING
%{_sysconfdir}/xdg/autostart/bluetooth-applet.desktop
%{_bindir}/bluetooth-applet
%{_bindir}/bluetooth-sendto
%{_bindir}/bluetooth-wizard
%{_libdir}/gnome-bluetooth/
%{_datadir}/applications/*.desktop
%{_datadir}/gnome-bluetooth/
%{_mandir}/man1/*
%{_libdir}/nautilus-sendto/plugins/*.so
/lib/udev/rules.d/61-gnome-bluetooth-rfkill.rules
%{_datadir}/GConf/gsettings/*
%{_datadir}/glib-2.0/schemas/*
%doc %{_datadir}/help/*/*

%files -f gnome-bluetooth2.lang libs
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_libdir}/libgnome-bluetooth.so.*
%{_libdir}/girepository-1.0/GnomeBluetooth-1.0.typelib
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/icons/hicolor/*/status/*

%files libs-devel
%defattr(-,root,root,-)
%{_includedir}/gnome-bluetooth/
%{_libdir}/libgnome-bluetooth.so
%{_libdir}/libgnome-bluetooth.la
%{_libdir}/pkgconfig/gnome-bluetooth-1.0.pc
%{_datadir}/gir-1.0/GnomeBluetooth-1.0.gir
%{_datadir}/gtk-doc

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- reimport from fedora

* Sun Jul  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- fix up Obsoletes

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.2-1m)
- update to 3.4.2
- disable nautilus-sendto plugin for a while

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.1-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for glib 2.33.2

* Fri Feb 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.4-1m)
- update to 3.1.4

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-6m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-5m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.32.0-4m)
- add BR unique-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- add glib-compile-schemas script

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-7m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-6m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-5m)
- add Requires: GConf2

* Thu Jul 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-4m)
- fix req

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-3m)
- split libs

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Wed Dec 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.6-1m)
- update to 2.28.6
 
* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Tue Sep 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-2m)
- Obsoletes: bluez-gnome bluez-gnome-analyzer

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
-- disable-introspection
-- DbusGir-1.0.dir does not exist in momonga

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.1-3m)
- fix autostart

* Sun Mar 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.1-2m)
- fix conflicts files

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.1-1m)
- update to 2.27.1

* Sat Jan 24 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.0-4m)
- add BuildRequires:

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11.0-2m)
- rebuild against python-2.6.1-1m

* Sun Dec 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.0-1m)
- update 0.11.0

* Sun Dec 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-4m)
- rebuild against bluez-4.22

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-3m)
- rebuild against librsvg2-2.22.2-3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-2m)
- rebuild against gcc43

* Sun Sep 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Wed Jul 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-3m)
- add -maxdepth 1 to del .la

* Thu May 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-2m)
- fix %%files

* Thu May 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- initial build
