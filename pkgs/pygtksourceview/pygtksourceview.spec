%global momorel 5
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: python bindings of the GtkSourceView library
Name: pygtksourceview
Version: 2.10.1
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.10/%{name}-%{version}.tar.bz2 
NoSource: 0
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: pygobject-devel >= 2.16.1
BuildRequires: pygtk2-devel >= 2.24
BuildRequires: gtksourceview2-devel >= 2.8.0
BuildRequires: gtk-doc

%description
This archive contains python bindings for the version 2 of the
GtkSourceView library.

%package devel
Summary: gtksourceview-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtksourceview2-devel

%description devel
pygtksourceview-devel

%prep
%setup -q

%build
%configure --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{python_sitearch}/gtksourceview2.so
%{python_sitearch}/gtksourceview2.la
%{_datadir}/pygtk/2.0/defs/gtksourceview2.defs
 
%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/*.pc
%doc %{_datadir}/gtk-doc/html/pygtksourceview2

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.1-2m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.1-1m)
- update to 2.9.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.0-2m)
- revise BR: gtksourceview2-devel >= 2.8.0

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Sun Mar 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.0-2m)
- rebuild against python-2.6.1-1m

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-2m)
- %%NoSource -> NoSource

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Mar 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.1-1m)
- version 1.1.1
- GNOME 2.8 Desktop

* Tue Dec 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.1-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Thu Apr 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- version 1.0.1

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.0-1m)
- version 1.0.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.7.0-2m)
- revised spec for enabling rpm 4.2.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.0-1m)
- version 0.7.0

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.6.0-1m)
- version 0.6.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.0-1m)
- version 0.5.0

* Tue Jul 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4.0-1m)
- version 0.4.0

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.0-1m)
- version 0.3.0

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.1-1m)
- version 0.2.1

* Fri May  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.0-1m)
- version 0.2.0
