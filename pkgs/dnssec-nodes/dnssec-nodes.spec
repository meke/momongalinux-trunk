%global momorel 1

Summary: DNS Visualization Tool
Name: dnssec-nodes
Version: 1.12
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
URL: http://www.dnssec-tools.org/
Source0: https://www.dnssec-tools.org/download/%{name}-%{version}.tar.gz
NoSource: 0
Source1: dnssec-nodes.desktop

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: qt-devel
BuildRequires: dnssec-tools-libs-devel >= 1.11
BuildRequires: openssl-devel
BuildRequires: desktop-file-utils
Requires: dnssec-tools-libs >= 1.11

%description
A graphical DNS visualization application, specializing in DNSSEC.
The DNSSEC-Nodes application is a graphical debugging utility that
allows administrators to watch the data being logged into a libval or
bind logging file.

%prep
%setup -q 

%build
qmake-qt4 PREFIX=/usr
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install INSTALL_ROOT=%{buildroot}

%{__mkdir_p} %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/
%{__install} -p -m 644 icons/dnssec-nodes.svg %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}

%{__mkdir_p} %{buildroot}/%{_mandir}/man1
%{__install} -p -D -m 644 man/dnssec-nodes.1 %{buildroot}/%{_mandir}/man1/dnssec-nodes.1

%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor &> /dev/null
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/dnssec-nodes
%{_datadir}/icons/hicolor/*/apps/*.svg
%{_datadir}/applications/dnssec-nodes.desktop
%doc %{_mandir}/man1/*

%changelog
* Thu Mar 29 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-1m)
- import from Fedora devel

* Tue Jan 31 2012 Wes Hardaker <wjhns174@hardakers.net> - 1.12-1
- Upgraded to version 1.12

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.11.p2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Oct 27 2011 Wes Hardaker <wjhns174@hardakers.net> - 1.11.p2-1
- Update to better upstream version with a man page and COPYING file

* Mon Oct 17 2011 Wes Hardaker <wjhns174@hardakers.net> - 1.11.p1-2
- Update to reflect needed changes for review

* Thu Oct 13 2011 Wes Hardaker <wjhns174@hardakers.net> - 1.11.p1-1
- Initial version
