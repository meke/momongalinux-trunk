%global momorel 1

Summary:        Library for accessing USB devices
Name:           libusbx
Version:        1.0.18
Release:        %{momorel}m%{?dist}
Source0:        http://downloads.sourceforge.net/libusbx/libusbx-%{version}.tar.bz2
NoSource:	0
License:        LGPLv2+
Group:          System Environment/Libraries
URL:            http://sourceforge.net/apps/mediawiki/libusbx/
BuildRequires:  systemd-devel doxygen
Provides:       libusb1 = %{version}-%{release}
Obsoletes:      libusb1 <= 1.0.9

%description
This package provides a way for applications to access USB devices.

Libusbx is a fork of the original libusb, which is a fully API and ABI
compatible drop in for the libusb-1.0.9 release. The libusbx fork was
started by most of the libusb-1.0 developers, after the original libusb
project did not produce a new release for over 18 months.

Note that this library is not compatible with the original libusb-0.1 series,
if you need libusb-0.1 compatibility install the libusb package.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}
Provides:       libusb1-devel = %{version}-%{release}
Obsoletes:      libusb1-devel <= 1.0.9
Obsoletes:      libusb1-static <= 1.0.9

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package devel-doc
Summary:        Development files for %{name}
Group:          Development/Libraries
Provides:       libusb1-devel-doc = %{version}-%{release}
Obsoletes:      libusb1-devel-doc <= 1.0.9
BuildArch:      noarch

%description devel-doc
This package contains API documentation for %{name}.


%prep
%setup -q


%build
%configure --disable-static --enable-examples-build
make %{?_smp_mflags}
pushd doc
make docs
popd


%install
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT%{_libdir}/*.la


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%doc AUTHORS COPYING README ChangeLog
%{_libdir}/*.so.*

%files devel
%{_includedir}/libusb-1.0
%{_libdir}/*.so
%{_libdir}/pkgconfig/libusb-1.0.pc

%files devel-doc
%doc doc/html examples/*.c


%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.18-1m)
- update 1.0.18

* Tue Nov 19 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.17-1m)
- Initial Commit Momonga Linux

* Fri Sep  6 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.17-1
- Update to 1.0.17 final release

* Wed Aug 28 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.17-0.1.rc1
- New upstream 1.0.17-rc1 release

* Tue Jul 30 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.16-3
- Fix another libusb_exit deadlock (rhbz#985484)

* Fri Jul 19 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.16-2
- Fix libusb_exit sometimes (race) deadlocking on exit (rhbz#985484)

* Thu Jul 11 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.16-1
- New upstream 1.0.16 final release

* Sat Jul  6 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.16-0.2.rc3
- New upstream 1.0.16-rc3 release

* Mon Jul  1 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.16-0.1.rc2
- New upstream 1.0.16-rc2 release

* Fri Apr 19 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.15-2
- Replace tarbal with upstream re-spun tarbal which fixes line-ending and
  permission issues

* Wed Apr 17 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.15-1
- Upgrade to 1.0.15 (rhbz#952575)

* Tue Apr  2 2013 Hans de Goede <hdegoede@redhat.com> - 1.0.14-3
- Drop devel-doc Requires from the devel package (rhbz#947297)

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.14-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Wed Sep 26 2012 Hans de Goede <hdegoede@redhat.com> - 1.0.14-1
- Upgrade to 1.0.14

* Mon Sep 24 2012 Hans de Goede <hdegoede@redhat.com> - 1.0.13-1
- Upgrade to 1.0.13

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.11-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Wed May 23 2012 Hans de Goede <hdegoede@redhat.com> - 1.0.11-2
- Fix URL to actually point to libusbx
- Improve description to explain the relation between libusbx and libusb
- Build the examples (to test linking, they are not packaged)

* Tue May 22 2012 Hans de Goede <hdegoede@redhat.com> - 1.0.11-1
- New libusbx package, replacing libusb1
- Switching to libusbx upstream as that actually does releases (hurray)
- Drop all patches (all upstream)
- Drop -static subpackage (there are no packages using it)
