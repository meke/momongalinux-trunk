%global momorel 1
%define majorver 8.5
%define        vers %{majorver}.12
%{!?sdt:%define sdt 1}

Summary: Tool Command Language, pronounced tickle
Name: tcl
Version: %{vers}
Release: %{momorel}m%{?dist}
License: "TCL"
Group: Development/Languages
URL: http://tcl.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/tcl/%{name}%{version}-src.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Buildrequires: autoconf, sed
Provides: tcl(abi) = %{majorver}
Obsoletes: tcl-tcldict <= %{vers}
Provides: tcl-tcldict = %{vers}
Provides: %{_bindir}/tclsh
Patch0: tcl8.5.7-autopath.patch
Patch1: tcl-8.5.10-conf.patch
Patch2: tcl-8.5.12-hidden.patch

%if %sdt
BuildRequires: systemtap-sdt-devel
%endif

%description
The Tcl (Tool Command Language) provides a powerful platform for
creating integration applications that tie together diverse
applications, protocols, devices, and frameworks. When paired with the
Tk toolkit, Tcl provides a fastest and powerful way to create
cross-platform GUI applications.  Tcl can also be used for a variety
of web-related tasks and for creating powerful command languages for
applications.

%package devel
Summary: Tcl scripting language development environment
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description devel
The Tcl (Tool Command Language) provides a powerful platform for
creating integration applications that tie together diverse
applications, protocols, devices, and frameworks. When paired with the
Tk toolkit, Tcl provides a fastest and powerful way to create
cross-platform GUI applications.  Tcl can also be used for a variety
of web-related tasks and for creating powerful command languages for
applications.

The package contains the development files and man pages for tcl.

%prep
%setup -q -n %{name}%{version}
chmod -x generic/tclThreadAlloc.c

%patch0 -p1 -b .autopath
%patch1 -p1 -b .conf
%patch2 -p1 -b .hidden

%build
pushd unix
autoconf
%configure \
%if %sdt
--enable-dtrace \
%endif
--disable-threads \
--enable-symbols \
--enable-shared

make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS -DTCL_NO_STACK_CHECK=1" \
TCL_LIBRARY=%{_datadir}/%{name}%{majorver}
 
%check
# don't run "make test" by default
%{?_without_check: %define _without_check 0}
%{!?_without_check: %define _without_check 1}

%if ! %{_without_check}
#  make test
%endif

%install
rm -rf %{buildroot}
make install -C unix INSTALL_ROOT=%{buildroot} TCL_LIBRARY=%{_datadir}/%{name}%{majorver}

ln -s tclsh%{majorver} %{buildroot}%{_bindir}/tclsh

# for linking with -lib%%{name}
ln -s lib%{name}%{majorver}.so %{buildroot}%{_libdir}/lib%{name}.so

mkdir -p %{buildroot}/%{_libdir}/%{name}%{majorver}

# postgresql and maybe other packages too need tclConfig.sh
# paths don't look at /usr/lib for efficiency, so we symlink into tcl8.5 for now
ln -s %{_libdir}/%{name}Config.sh $RPM_BUILD_ROOT/%{_libdir}/%{name}%{majorver}/%{name}Config.sh

mkdir -p %{buildroot}/%{_includedir}/%{name}-private/{generic,unix}
find generic unix -name "*.h" -exec cp -p '{}' %{buildroot}/%{_includedir}/%{name}-private/'{}' ';'
( cd %{buildroot}/%{_includedir}
	for i in *.h ; do
		[ -f %{buildroot}/%{_includedir}/%{name}-private/generic/$i ] && ln -sf ../../$i %{buildroot}/%{_includedir}/%{name}-private/generic ;
	done
)

# remove buildroot traces
sed -i -e "s|$PWD/unix|%{_libdir}|; s|$PWD|%{_includedir}/%{name}-private|" %{buildroot}/%{_libdir}/%{name}Config.sh
rm -rf %{buildroot}/%{_datadir}/%{name}%{majorver}/ldAix

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/tclsh*
%{_datadir}/%{name}%{majorver}
%exclude %{_datadir}/%{name}%{majorver}/tclAppInit.c
%{_datadir}/%{name}8
%{_libdir}/lib%{name}%{majorver}.so
%{_mandir}/man1/*
%{_mandir}/man3/*
%{_mandir}/mann/*
%dir %{_libdir}/%{name}%{majorver}
%doc README changes 
%doc license.terms

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/lib%{name}stub%{majorver}.a
%{_libdir}/lib%{name}.so
%{_libdir}/%{name}Config.sh
%{_libdir}/%{name}8.5/%{name}Config.sh
%{_datadir}/%{name}%{majorver}/tclAppInit.c

%changelog
* Tue Aug 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.5.12-1m)
- update to 8.5.12

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.5.11-1m)
- update to 8.5.11 (bug fix release)

* Sat Aug 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (8.5.10-2m)
- sync Fedora

* Sun Jun 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.5.10-1m)
- update to 8.5.10

* Tue Jun 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.5.9-3m)
- add -DTCL_NO_STACK_CHECK=1.
-- because any env overflow stack

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.5.9-2m)
- rebuild for new GCC 4.6

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.5.9-1m)
- update to 8.5.9

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.5.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.5.8-2m)
- full rebuild for mo7 release

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.5.8-1m)
- update to 8.5.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.5.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.5.7-2m)
- fix buffer overflow (Patch4)

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.5.7-1m)
- update to 8.5.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.5.6-2m)
- rebuild against rpm-4.6

* Sun Dec 28 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.5.6-1m)
- update to 8.5.6
-- update Patch3
- License: Modified BSD

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.5.5-1m)
- update to 8.5.5

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.5.2-3m)
- remove man files from devel package

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.5.2-2m)
- Provides: %%{_bindir}/tclsh

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.5.2-1m)
- update to 8.5.2 (based on 8.5.1 in Fedora)
- 
- * Mon Mar 17 2008 Marcela Maslanova <mmaslano@redhat.com> - 1:8.5.1-3
- - #436567 change auto path, tk can't be found.
- - #437399 fix files permission
- * Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1:8.5.1-2
- - Autorebuild for GCC 4.3
- * Mon Jan 18 2008 Marcela Maslanova <mmaslano@redhat.com> - 1:8.5.1-1
- - new version tcl8.5.1
- - fix 433151 problem with regular expression
- - Version 2.5.3 of the http package requires Tcl 8.4 or better -> change make patch, add tm files back to 8.4
- * Tue Jan 15 2008 Marcela Maslanova <mmaslano@redhat.com> - 1:8.5.0-6
- - tclsh8.5 is back because of back compatibility #428712
- * Tue Jan  8 2008 Marcela Maslanova <mmaslano@redhat.com> - 1:8.5.0-5
- - stack checking is ok, error is in application. Removing withouth stack.
- - tcl-8.5.0-hidden.patch isn't ok, fix should be in expect. In the meantime the patch stay here.
- * Mon Jan  7 2008 Marcela Maslanova <mmaslano@redhat.com> - 1:8.5.0-4
- - add patch from atkac at redhat dot com - fix linking with hidden objects
- * Sat Jan  5 2008 Wart <wart@kobold.org> - 1:8.5.0-3
- - Obsolete the tcl-tcldict package that has been incorporated
-   into the main Tcl source code.
- - Disable the the stack checking code; it makes assumptions that are
-   not necessarily true on Fedora and causes some apps to fail (BZ #425799)
- * Thu Jan  3 2008 Marcela Maslanova <mmaslano@redhat.com> - 1:8.5.0-2
- - rebuilt because of nonsense in tag
- * Wed Jan  2 2008 Marcela Maslanova <mmaslano@redhat.com> - 1:8.5.0-1
- - upgrade on the new whole version 8.5.0
- - thank you for patches and clean spec to wart (at kobold)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.4.16-3m)
- rebuild against gcc43

* Sun Feb 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.4.16-2m)
- Provides: %%{_bindir}/tclsh

* Tue Oct 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.4.16-1m)
- [SECURITY] CVE-2007-4851 CVE-2007-5137

* Fri Dec 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.4.14-1m)
- update to 8.4.14 (based on the package of Fedora)
-
- * Thu Jul 20 2006 David Cantrell <dcantrell@redhat.com> - 8.4.13-3
- - Fix cflags patch so it applies correctly
- - Changes $(CFLAGS) to ${CFLAGS} in cflags patch
- * Thu Jul 20 2006 David Cantrell <dcantrell@redhat.com> - 8.4.13-2
- - Patch from Dennis Gilmore <dennis@ausil.us> for sparc64 (#199375)
- * Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 8.4.13-1.2
- - rebuild
- * Wed Apr 19 2006 David Cantrell <dcantrell@redhat.com> - 8.4.13-1
- - Upgraded to Tcl 8.4.13

* Mon Mar 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.4.12-1m)
- update to 8.4.12 (sync with Fedora Core  8.4.12-4)
- - * Fri Feb 17 2006 David Cantrell <dcantrell@redhat.com> - 8.4.12-4
- - - Enable threads (#181871)
- - 
- - * Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 8.4.12-3.2
- - - bump again for double-long bug on ppc(64)
- - 
- - * Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 8.4.12-3.1
- - - rebuilt for new gcc4.1 snapshot and glibc changes
- - 
- - * Thu Feb 02 2006 David Cantrell <dcantrell@redhat.com> - 8.4.12-3
- - - Patched syntax errors in configure and tcl.m4 so it works with bash
- - 
- - * Thu Feb 02 2006 David Cantrell <dcantrell@redhat.com> - 8.4.12-2
- - - Don't use ksh on ia64
- - 
- - * Thu Feb 02 2006 David Cantrell <dcantrell@redhat.com> - 8.4.12-1
- - - Upgraded to tcl-8.4.12
- - - Use ksh rather than bash for the configure script (known bug w/ bash-3.1)
- - - Generate HTML docs from source
- - - Add in the Tk source for HTML doc generation
- - 
- - * Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- - - rebuilt
- - 
- - * Fri Jul  1 2005 Jens Petersen <petersen@redhat.com> - 8.4.11-1
- - - update to latest stable release
- -   - update tcl-8.4-autoconf.patch
- - - buildrequire sed and use it instead of perl for editting tclConfig.sh
- - 
- - * Wed Mar  9 2005 Jens Petersen <petersen@redhat.com> - 8.4.9-3
- - - rebuild with gcc 4
- - 
- - * Tue Dec 14 2004 Jens Petersen <petersen@redhat.com> - 8.4.9-2
- - - move tclConfig.sh into -devel (Axel Thimm, 142724)
- - 
- - * Thu Dec  9 2004 Jens Petersen <petersen@redhat.com> - 8.4.9-1
- - - new stable release
- - 
- - * Wed Nov 24 2004 Jens Petersen <petersen@redhat.com> - 8.4.8-1
- - - update to latest release
- - 
- - * Fri Oct 15 2004 Jens Petersen <petersen@redhat.com> - 8.4.7-2
- - - improve tcl8.3.5-tclConfig-package-path-90160.patch to look in libdir in addition to datadir for packages, so that tclsh can load binary packages in lib64 (135310)
- - 
- - * Fri Jul 30 2004 Jens Petersen <petersen@redhat.com> - 8.4.7-1
- - - update to 8.4.7
- -   - replace tcl-8.4.5-no_rpath.patch by tcl-8.4-no_rpath.patch
- -   - replace tcl-8.4.5-autoconf.patch by tcl-8.4-autoconf.patch
- - - no longer obsolete itcl

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (8.4.7-1m)
- disable-threads... orz

* Sun Aug  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (8.4.5-3m)
- enable-threads

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (8.4.5-2m)
- Obsoletes: tclx

* Sun Jun 27 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (8.4.5-1m)
- upgrade 8.4.5 
- import patch from FC2
- separate package tcl,tk,tix,expect

* Tue Jun  1 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (8.3.4-4m)
- add source

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (8.3.4-3m)
- pretty spec file.

* Wed Jan 22 2003 zunda <zunda@freeshell.org>
- (8.3.4-3m)
- mv /usr/share/man/man3/Thread.3.bz2 /usr/share/man/man3/Tcl_Thread.3.bz2

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (8.3.4-2m)
- rebuild against gcc-3.2

* Sat Sep 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (8.3.4-1m)
- minor feature enhancements
- tcl8.3.4, tk8.3.4, tcllib-1.3

* Fri Sep 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (8.3.3-10m)
- revise PATH in %build again etc.

* Fri Jul 26 2002 Kazuhiko <kazuhiko@fdiary.net>
- (8.3.3-9m)
- revise PATH in %build

* Sat May 25 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (8.3.3-8k)
- Added PATH for wish so that Itcl configure can find.
 
* Tue Apr 30 2002 Toru Hoshina <t@kondara.org>
- (8.3.3-6k)
- fixed Provides tag.

* Thu Jan 10 2002 Shingo Akagaki <dora@kondara.org>
- add tcltk-8.3.3-tottemoba-tari-.patch. 

* Tue Oct  2 2001 Toru Hoshina <t@kondara.org>
- merger from rawhide. 8.3.3-65.
- no i18n patch applied.

* Mon Aug 29 2001 Adrian Havill <havill@redhat.com>
- hard-coded the compat symlink for tix libdir. (bug 52812)

* Mon Aug 28 2001 Adrian Havill <havill@redhat.com>
- fixed itkwish/itclsh lib problem (bug 52608)
- make itcl install not need tclsh/wish during config/make (bug 52606)
- expect's fixline1 busted for expectk scripts (tkpasswd/tknewsbiff/tkterm)

* Mon Aug  8 2001 Adrian Havill <havill@redhat.com>
- changed rev for tcllib to 1.0
- added execute bit mode for itclsh and itksh compat shells
- re-enable glibc string and math inlines; recent gcc is a-ok.
- optimize at -O2 instead of -O
- rename "soname" patches related to makefile/autoconf changes
- added elf "needed" for tk, tclx, tix, itk
- removed warnings from tclX

* Wed Jul 25 2001 Adrian Havill <havill@redhat.com>
- added itclsh/itkwish for backwards compatibility, fixed rpath (bug 46086)
- fixed 64 bit RPM provides for dependencies

* Thu Jul 19 2001 Adrian Havill <havill@redhat.com>
- updated tclX to the latest version
- fixed tclX 8.3's busted help install
- eliminated make TK_LIB kludge for multiple math libs for tclX
- used %%makeinstall to brute force fix any remaining unflexible makefile dirs
- fixed bad ref count release in tcl (bug 49406)
- improved randomness of expect's mkpasswd script via /dev/random (bug 9507)
- revert --enable-threads, linux is (still) not ready (yet) (bug 49251)
- fixed DirTree in Tix (bug 45570)

* Sun Jul  8 2001 Adrian Havill <havill@redhat.com>
- refresh all sources to latest stable (TODO: separate expect/expectk)
- massage out some build stuff to patches (TODO: libtoolize hacked constants)
- remove patches already rolled into the upstream
- remove unneeded autopasswd
- removed RPATH (bugs 45569, 46085, 46086), added SONAMEs to ELFs
- changed shared object filenames to something less gross
- fixed tclX shell's argv parsing (bug 47710)
- reenable threads which seem to work now
- added all necessary header files for itcl (bug 41374)
- fixed spawn/eof read problem with expect (bug 43310)
- made compile-friendly for IA64

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild for 7.2.

* Fri Mar 23 2001 Bill Nottingham <notting@redhat.com>
- bzip2 sources

* Mon Mar 19 2001 Bill Nottingham <notting@redhat.com>
- build with -D_GNU_SOURCE - fixes expect on ia64

* Mon Mar 19 2001 Preston Brown <pbrown@redhat.com>
- build fix from ahavill.

* Wed Feb 21 2001 Tim Powers <timp@redhat.com>
- fixed weather expect script using wrong server (#28505)

* Tue Feb 13 2001 Adrian Havill <havill@redhat.com>
- added "ja_JP.eucJP" to locale list for tcl

* Tue Feb 13 2001 Adrian Havill <havill@redhat.com>
- rebuild so make check passes

* Fri Oct 20 2000 Than Ngo <than@redhat.com>
- rebuild with -O0 on alpha (bug #19461)

* Thu Aug 17 2000 Jeff Johnson <jbj@redhat.com>
- summaries from specspo.

* Tue Aug  8 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- remove symlink to libtixsam.so

* Thu Aug  3 2000 Jeff Johnson <jbj@redhat.com>
- merge "best known" patches from searching, stubs were broken.
- tix needs -fwritable-strings (#14352).
- create tixwish symlink.

* Thu Jul 27 2000 Jeff Johnson <jbj@redhat.com>
- rebuild against "working" util-linux col.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Jun 16 2000 Jeff Johnson <jbj@redhat.com>
- make sure that tix shared libraries are installed.
- don't mess with %%{_libdir}, it's gonna be a FHS pita.

* Sat Jun 10 2000 Jeff Johnson <jbj@redhat.com>
- move, not symlink, unix/tk8.0 to generate Tix pkgIndex.tcl correctly (#11940).

* Tue Jun  6 2000 Jeff Johnson <jbj@redhat.com>
- tclX had wrong version.

* Fri Jun  2 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging changes.
- revert --enable-threads, linux is not ready (yet) (#11789).
- tcl/tk: update to 8.3.1 (#10779).
- expect: update to 5.31.7+ (#11595).
- add tcllib-0.4.
- abstract major tcltk version for soname expansion etc.

* Sat Mar 18 2000 Jeff Johnson <jbj@redhat.com>
- update to (tcl,tk}-8.2.3, expect-5.31, and itcl-3.1.0, URL's as well.
- use perl to drill out pre-pended RPM_BUILD_ROOT.
- configure with --enable-threads (experimental).
- improved version of autopasswd (#4788).
- autopasswd needs to handle password starting with hyphen (#9917).
- handle 553 ftp status in rftp expect script (#7869).
- remove cryptdir/decryptdir, as Linux has not crypt command (#6668).
- correct hierarchy spelling (#7082).
- fix "expect -d ...", format string had int printed as string (#7775).

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Thu Feb 03 2000 Elliot Lee <sopwith@redhat.com>
- Make changes from bug number 7602
- Apply patch from bug number 7537
- Apply fix from bug number 7157
- Add fixes from bug #7601 to the runtcl patch

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix descriptions
- man pages are compressed (whatapain)

* Tue Nov 30 1999 Jakub Jelinek <jakub@redhat.com>
- fix tclX symlinks.
- compile on systems where SIGPWR == SIGLOST.

* Sat May  1 1999 Jeff Johnson <jbj@redhat.com>
- update tcl/tk to 8.0.5.
- avoid "containing" in Tix (#2332).

* Thu Apr  8 1999 Jeff Johnson <jbj@redhat.com>
- use /usr/bin/write in kibitz (#1320).
- use cirrus.sprl.umich.edu in weather (#1926).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 28)

* Mon Mar 08 1999 Preston Brown <pbrown@redhat.com>
- whoops, exec-prefix for itcl was set to '/foo', changed to '/usr'.

* Tue Feb 16 1999 Jeff Johnson <jbj@redhat.com>
- expect does unaligned access on alpha (#989)
- upgrade tcl/tk/tclX to 8.0.4
- upgrade expect to 5.28.
- add itcl 3.0.1

* Tue Jan 12 1999 Cristian Gafton <gafton@redhat.com>
- call libtoolize to allow building on the arm
- build for glibc 2.1
- strip binaries

* Thu Sep 10 1998 Jeff Johnson <jbj@redhat.com>
- update tcl/tk/tclX to 8.0.3, expect is updated also.

* Mon Jun 29 1998 Jeff Johnson <jbj@redhat.com>
- expect: mkpasswd needs delay before sending password (problem #576)

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- fixed expect binaries exec permissions

* Thu Apr 09 1998 Erik Troan <ewt@redhat.com>
- updated to Tix 4.1.0.006
- updated version numbers of tcl/tk to relflect includsion of p2

* Wed Mar 25 1998 Cristian Gafton <gafton@redhat.com>
- updated tcl/tk to patch level 2
- updated tclX to 8.0.2

* Thu Oct 30 1997 Otto Hammersmith <otto@redhat.com>
- fixed filelist for tix... replacing path to the expect binary in scripts
  was leaving junk files around.

* Wed Oct 22 1997 Otto Hammersmith <otto@redhat.com>
- added patch to remove libieee test in configure.in for tcl and tk.
  Shoudln't be needed anymore for glibc systems, but this isn't the "proper" 
  solution for all systems
- fixed src urls

* Mon Oct 06 1997 Erik Troan <ewt@redhat.com>
- removed version numbers from descriptions

* Mon Sep 22 1997 Erik Troan <ewt@redhat.com>
- updated to tcl/tk 8.0 and related versions of packages

* Tue Jun 17 1997 Erik Troan <ewt@redhat.com>
- built against glibc
- fixed dangling tclx/tkx symlinks
