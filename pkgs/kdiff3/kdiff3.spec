%global momorel 1
%global qtver 4.8.2
%global kdever 4.9.0
%global kdelibsrel 2m
%global kdebaserel 1m

Summary: a utility for comparing and/or merging two or three text files or directories
Name: kdiff3
Version: 0.9.97
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Tools
URL: http://kdiff3.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: libidn
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdebase-devel >= %{kdever}-%{kdebaserel}
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libidn-devel

%description
 KDiff3 is a program that

    * compares two or three text input files,
    * shows the differences line by line and character by character (!),
    * provides an automatic merge-facility and
    * an integrated editor for comfortable solving of merge-conflicts
    * and has an intuitive graphical user interface,
    * and it can also compare and merge directories!

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --add-category Development \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README TODO
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/kdiff3fileitemaction.so
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/%{name}part
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/*/*/*/%{name}.png
%{_kde4_datadir}/kde4/services/%{name}fileitemaction.desktop
%{_kde4_datadir}/kde4/services/%{name}part.desktop
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_datadir}/locale/*/LC_MESSAGES/%{name}*.mo

%changelog
* Mon Aug 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.97-1m)
- update to 0.9.97

* Sun Sep  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.96-1m)
- update to 0.9.96

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.95-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.95-15m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-14m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.95-13m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-12m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.95-11m)
- touch up spec file

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-10m)
- new source for KDE 4.4.0

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-9m)
- new source for KDE 4.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.95-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-7m)
- NoSource again

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-6m)
- no NoSource for STABLE_6

* Fri Sep  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-5m)
- new source for KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-4m)
- new source for KDE 4.3.0

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-3m)
- new source for KDE 4.2.4

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-2m)
- new source for KDE 4.2.3

* Sun Mar 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.95-1m)
- version 0.9.95

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.94-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.94-1m)
- version 0.9.94

* Fri Jan  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.93-1m)
- version 0.9.93
- good-bye KDE3
- License: GPLv2

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.92-6m)
- revise %%{_docdir}/HTML/*/kdiff3/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.92-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.92-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.92-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.92-2m)
- %%NoSource -> NoSource

* Sat Apr 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.92-1m)
- version 0.9.92

* Mon Mar 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.91-2m)
- add some options to configure
- remove %%{_datadir}/doc/HTML/kdiff3
- move kdiff3.desktop to %%{_datadir}/applications/kde/
- add Categories to kdiff3.desktop

* Mon Mar 19 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.91-1m)
- import to Momonga

* Mon Nov 13 2006 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.91-0.0.1m)
- version 0.9.91

* Tue Oct 17 2006 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.90-0.0.1m)
- version 0.9.90

* Mon Apr 10 2006 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.89-0.0.1m)
- version 0.9.89

* Thu May 19 2005 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.88-0.0.1m)
- version 0.9.88

* Wed Jun 16 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.86-0.0.1m)
- version 0.9.86

* Thu Mar 11 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.83-0.0.1m)
- version 0.9.83

* Tue Feb  3 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.82-0.0.1m)
- version 0.9.82

* Thu Jan 29 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.80-0.0.2m)
- add %{_libdir} to %files

* Wed Jan  7 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.80-0.0.1m)
- version 0.9.80

* Wed Aug  6 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.61-0.0.1m)
- version 0.9.61

* Thu Jun  5 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.60-0.0.1m)
- build for Momonga Linux

