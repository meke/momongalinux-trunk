%global momorel 13
%global rbname erbscan

%global ruby18_sitearchdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')
%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: ERB compiler accelerator
Name: ruby18-%{rbname}
Version: 0.0.20030723b
Release: %{momorel}m%{?dist}
URL: http://www.ruby-lang.org/en/raa-list.rhtml?name=erbscan
Source0: http://www.moonwolf.com/ruby/archive/erbscan-%{version}.tar.gz
NoSource: 0
License: Ruby
Group: Applications/Text
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18-devel
Obsoletes: ruby-erbscan < 0.0.20030723b-10m

%description
ERB::Compiler accelerator
8 times faster than erb.rb(Ruby1.8.0)
  require "erb" # slow
  require "erb_fast" # FAST!!!!

%prep
%setup -q -n %{rbname}-%{version}
chmod 644 README*

%build
ruby18 extconf.rb
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README*
%{ruby18_sitelibdir}/*.rb
%{ruby18_sitearchdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.20030723b-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.20030723b-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.20030723b-11m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20030723b-10m)
- renamed from ruby-erbscan which became obsolete

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20030723b-9m)
- build with ruby18

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.20030723b-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20030723b-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.20030723b-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.20030723b-5m)
- rebuild against gcc43

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.20030723b-4m)
- rebuild against ruby-1.8.6-4m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.0.20030723b-3m)
- /usr/lib/ruby

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.20030723b-2m)
- rebuild against ruby-1.8.2

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.0.20030723b-1m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- version/release division changed to follow speclint

* Sun Sep 14 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20030723b.1m)
- initial import to Momonga
