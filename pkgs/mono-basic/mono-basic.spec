%global momorel 4

Summary: a VB.NET runtime environment
Name: mono-basic
Version: 2.10
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Base
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://www.mono-project.com/VisualBasic.NET_support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: /sbin/ldconfig
BuildRequires: mono-core >= 2.10
BuildRequires: mono-locale-extras

%description
VB.NET 

%prep
%setup -q

%build
./configure --prefix=/usr
%make || %make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'
find %{buildroot} -name "*.mdb" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc README
%{_bindir}/vbnc
%{_bindir}/vbnc2
%{_prefix}/lib/mono/2.0/Microsoft.VisualBasic.dll

%{_prefix}/lib/mono/4.0/Microsoft.VisualBasic.dll
%{_prefix}/lib/mono/4.0/Mono.Cecil.VB.Mdb.dll
%{_prefix}/lib/mono/4.0/Mono.Cecil.VB.Pdb.dll
%{_prefix}/lib/mono/4.0/Mono.Cecil.VB.dll
%{_prefix}/lib/mono/4.0/vbnc.exe
%{_prefix}/lib/mono/4.0/vbnc.rsp

%{_prefix}/lib/mono/gac/Mono.Cecil.VB.Mdb
%{_prefix}/lib/mono/gac/Mono.Cecil.VB.Pdb
%{_prefix}/lib/mono/gac/Mono.Cecil.VB
%{_prefix}/lib/mono/gac/Microsoft.VisualBasic

%{_mandir}/man1/vbnc.1.*

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-4m)
- change Source0 URI

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-3m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.2-3m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.2-2m)
- add BuildRequires

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6-2m)
- use BuildRequires

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-3m)
- good-bye debug symbol

* Tue Nov  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2m)
- change source url (NoSource)

* Thu Aug 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update 2.0

* Sat Jul  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.0.0103275-1m)
- update to 1.9.0.0103275 (svn version)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-2m)
- rebuild against gcc43

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Thu Dec 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Sun Sep  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Wed May 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3.1-2m)
- add LANG=ja_JP.UTF-8

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3.1-1m)
- initial build
