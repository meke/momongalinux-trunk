%global momorel 1

Summary: X.Org X11 libfontenc runtime library
Name: libfontenc
Version: 1.1.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: libfontenc-1.0.5-get-fontdir-from-pkgconfig.patch

BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros
BuildRequires: xorg-x11-proto-devel >= 7.2
BuildRequires: zlib-devel

#Obsoletes: XFree86-libs, xorg-x11-libs

%description
X.Org X11 libfontenc runtime library

%package devel
Summary: X.Org X11 libfontenc development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3

#Obsoletes: XFree86-devel, xorg-x11-devel

%description devel
X.Org X11 libfontenc development package

%prep
%setup -q

# Disable static library creation by default.
%define with_static 0

%build
# FIXME: libfontenc-0.99.2-use-datadir-for-encodings.patch requires that
# aclocal, automake, and autoconf get invoked to activate the changes.
# These can be removed once the patch is no longer necessary.
#aclocal
#automake
autoconf
%configure \
%if ! %{with_static}
	--disable-static
%endif
make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=$RPM_BUILD_ROOT

# Remove all libtool archives (*.la)
find $RPM_BUILD_ROOT -type f -name '*.la' -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog
%{_libdir}/libfontenc.so.1
%{_libdir}/libfontenc.so.1.0.0

%files devel
%defattr(-,root,root,-)
%{_includedir}/X11/fonts/fontenc.h
%if %{with_static}
%{_libdir}/libfontenc.a
%endif
%{_libdir}/libfontenc.so
%{_libdir}/pkgconfig/fontenc.pc

%changelog
* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2

* Sun Mar  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update 1.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.5-1m)
- update 1.0.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-2m)
- rebuild against gcc43

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update 1.0.4

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-2m)
- delete duplicate dir

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated libfontenc to version 1.0.1 from X11R7.0

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated libfontenc to version 1.0.0 from X11R7 RC4
- Added libfontenc-1.0.0-get-fontdir-from-pkgconfig.patch which now replaces
  libfontenc-0.99.2-use-datadir-for-encodings.patch by using pkgconfig to
  query fontutil.pc for fontdir.
- Added "BuildRequires: font-utils >= 1.0.0" to find fontutil.pc
- Removed libfontenc-0.99.2-use-datadir-for-encodings.patch

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Updated libfontenc to version 0.99.3 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Nov 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-3
- Bump xorg-x11-filesystem dep to >= 0.99.2-3 as -2 had bugs.

* Tue Nov 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Added libfontenc-0.99.2-use-datadir-for-encodings.patch and invoke aclocal
  and automake to activate the changes.  This fixes a bug reported against
  mkfontscale, in which it looks in _datadir for the font encodings files,
  which was traced back to libfontenc (#173875).
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-2" to ensure that
  /usr/include/X11 is a directory rather than a symlink before this package
  gets installed, to avoid bug (#173384).

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated libfontenc to version 0.99.2 from X11R7 RC2
- Changed 'Conflicts: XFree86-devel, xorg-x11-devel' to 'Obsoletes'
- Changed 'Conflicts: XFree86-libs, xorg-x11-libs' to 'Obsoletes'

* Mon Oct 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated libfontenc to version 0.99.1 from X11R7 RC1

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-4
- Renamed package to remove xorg-x11 from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Added zlib-devel build dependency

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
