%global momorel 1

Name:           libxshmfence
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        X11 shared memory fences

License:        MIT
URL:            http://www.x.org/
Group:		Development/Libraries
Source0:        http://ftp.x.org/pub/individual/lib/%{name}-%{version}.tar.bz2
NoSource:	0

# upstream tarball has broken libtool because libtool is never not broken
BuildRequires:  autoconf automake libtool xorg-x11-util-macros
BuildRequires:  pkgconfig(xproto)
#Requires:       

%description
Shared memory fences for X11, as used in DRI3.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
autoreconf -v -i -f
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}

%makeinstall
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc
%{_libdir}/libxshmfence.so.1*

%files devel
%doc
%{_includedir}/*
%{_libdir}/pkgconfig/xshmfence.pc
%{_libdir}/*.so

%changelog
* Thu Mar 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-1m)
- initial commit Momonga Linux

* Thu Dec 12 2013 Adam Jackson <ajax@redhat.com> 1.1-1
- xshmfence 1.1

* Wed Nov 06 2013 Adam Jackson <ajax@redhat.com> 1.0-1
- Initial packaging

