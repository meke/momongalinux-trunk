%global momorel 8
%global nosrc 0

Summary: Multiple stacked system monitors in one process
Name: gkrellm
Version: 2.3.4
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/System
URL: http://gkrellm.net/
Source0: http://members.dslextreme.com/users/billw/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: %{name}d.init
Source2: %{name}.desktop
Source3: %{name}.png

Patch1: %{name}-2.3.2-config.patch
Patch2: %{name}-2.2.4-sansfont.patch
Patch3: %{name}-2.2.7-width.patch
Patch4: %{name}-2.3.2-libdir.patch
#Patch5: %{name}-2.3.2-netdb.patch
Patch10: %{name}-font.patch
Patch12: %{name}-2.3.4-lib64.patch
Patch14: %{name}itime-1.0.1-gcc4.patch
#Patch15: %{name}-wifi-0.9.12-kh.patch
Patch16: %{name}-2.3.2-nologh.patch
Patch20: %{name}-%{version}-linking.patch

# Plugins
Source102: http://eric.bianchi.free.fr/Softwares/gkrellmitime-1.0.1.tar.gz
%if %nosrc
NoSource: 102
%endif
Source103: http://dl.sourceforge.net/sourceforge/gkrellkam/gkrellkam_2.0.0.tar.gz
%if %nosrc
NoSource: 103
%endif
Source110: http://dl.sourceforge.net/sourceforge/gkrellstock/gkrellstock-0.5.1.tar.gz
%if %nosrc
NoSource: 110
%endif
Source111: http://dl.sourceforge.net/sourceforge/gkrellshoot/gkrellshoot-0.4.4.tar.gz
%if %nosrc
NoSource: 111
%endif
Source112: http://shisha.spb.ru/debian/gkrellmwho2_0.2.8.orig.tar.gz
%if %nosrc
NoSource: 112
%endif
Source113: http://brix.gimp.org/projects/gkPCcard/gkPCcard-1.0.0-pre3.tar.gz
%if %nosrc
#NoSource: 113
%endif
Source114: http://anchois.free.fr/gkx86info0.0.2.tar.gz
%if %nosrc
NoSource: 114
%endif
Source115: http://horus.comlab.uni-rostock.de/flynn/gkrellflynn-0.8.tar.gz
%if %nosrc
#NoSource: 115
%endif
Source117: http://gkrellm.luon.net/files/gkrellmwireless-2.0.3.tar.gz
%if %nosrc
NoSource: 117
%endif
Source119: http://gkrellm.luon.net/files/gkrellmms-2.1.22.tar.gz
%if %nosrc
NoSource: 119
%endif
Source120: http://web.wt.net/~billw/gkrellm/Plugins/gkrellm-reminder-2.0.0.tar.gz
%if %nosrc
NoSource: 120
%endif
Source122: http://dl.sourceforge.net/sourceforge/gkrellmlaunch/gkrellmlaunch-0.5.tar.gz
%if %nosrc
#NoSource: 122
%endif
Source124: http://heim.ifi.uio.no/~oyvinha/gkleds/gkleds-0.8.2.tar.gz
%if %nosrc
NoSource: 124
%endif
Source125: http://www.bender-suhl.de/stefan/comp/sources/gkrellmbgchg2-0.1.8.tar.gz
%if %nosrc
NoSource: 125
%endif
Source127: http://freshmeat.net/redir/gkrellm-hddtemp/22296/url_tgz/gkrellm-hddtemp-0.2-beta.tar.gz
%if %nosrc
NoSource: 127
%endif

# Themes
Source400: http://www.muhri.net/gkrellm/GKrellM-Skins.tar.gz
%if %nosrc
NoSource: 400
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: gnutls-devel >= 2.4.1
BuildRequires: gtk2-devel
BuildRequires: gtk+1-devel
BuildRequires: libSM-devel
BuildRequires: lm_sensors-devel >= 3.0.2
BuildRequires: xmms-devel
BuildRequires: openssl-devel >= 1.0.0

%description
GKrellM charts CPU, load, Disk, and all active net interfaces
automatically.  An on/off button and online timer for the PPP
interface is provided, as well as monitors for memory and swap usage,
file system, internet connections, APM laptop battery, mbox style
mailboxes, and temperature sensors on supported systems.  Also
included is an uptime monitor, a hostname label, and a clock/calendar.
Additional features are:

  * Autoscaling grid lines with configurable grid line resolution.
  * LED indicators for the net interfaces.
  * A gui popup for configuration of chart sizes and resolutions.

%package daemon
Summary: The GNU Krell Monitors Server
Group: System Environment/Daemons
Requires(pre): shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(postun): initscripts

%description daemon
gkrellmd listens for connections from gkrellm clients. When a gkrellm
client connects to a gkrellmd server all builtin monitors collect their
data from the server.

%package devel
Summary: Development files for the GNU Krell Monitors
Group: Development/System
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Provides: %{name}-server
Obsoletes: %{name}-server

%description devel
Development files for the GNU Krell Monitors.

%package xmms
Summary: xmms plugin for GKrellM
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description xmms
xmms plugin for GKrellM

%prep
%setup -q

%patch1 -p1 -z .config
%patch2 -p1 -z .sansfont
%patch3 -p1 -z .width
%patch4 -p1 -z .libdir2
#%patch5 -p1 -z .netdb
%patch16 -p1 -z .nologh

#%%patch10 -p1 -b .font~

%patch20 -p1 -b .link

mkdir plugins
pushd plugins
tar xzf %{SOURCE102}
tar xzf %{SOURCE103}
tar xzf %{SOURCE110}
tar xzf %{SOURCE111}
tar xzf %{SOURCE112}
tar xzf %{SOURCE113}
%ifarch %{ix86} x86_64
tar xzf %{SOURCE114}
%endif
tar xzf %{SOURCE115}
tar xzf %{SOURCE117}
tar xzf %{SOURCE119}
tar xzf %{SOURCE120}
tar xzf %{SOURCE122}
tar xzf %{SOURCE124}
tar xzf %{SOURCE125}
tar xzf %{SOURCE127}

pushd gkrellmitime*
%patch14 -p1 -b .gcc4
popd

popd

#%%patch15 -p1 -b .kh

tar xzf %{SOURCE400}
%if %{_lib} == "lib64"
%patch12 -p1
perl -p -i -e 's|/lib|/lib64/|g' gkrellm.* src/gkrellm.h
perl -p -i -e 's|-Wall -O2|-Wall -fPIC -O2|g' plugins/gkx86info0.0.2/build
%endif

%build
make CFLAGS="%{optflags}" INSTALLROOT=%{_prefix} SYSTEM_PLUGINS_DIR=%{_libdir}/gkrellm2/plugins
export PKG_CONFIG_PATH="`pwd`"
pushd plugins
for j in */configure */*.*
do
  /usr/bin/perl -p -i -e 's|<gkrellm.h>|"../../src/gkrellm.h"|' $j
  /usr/bin/perl -p -i -e 's|<gkrellm2/gkrellm.h>|"../../src/gkrellm.h"|' $j
  /usr/bin/perl -p -i -e 's|<gkrellm2/gkrellmd.h>|"../../server/gkrellmd.h"|' $j
done
for j in */*/*.*
do
  /usr/bin/perl -p -i -e 's|<gkrellm.h>|"../../../src/gkrellm.h"|' $j
  /usr/bin/perl -p -i -e 's|<gkrellm2/gkrellm.h>|"../../../src/gkrellm.h"|' $j
  /usr/bin/perl -p -i -e 's|<gkrellm2/gkrellmd.h>|"../../../server/gkrellmd.h"|' $j
done

for i in *
do
  cd $i
  if [ -e build ]; then
      ./build
  elif [ -e gkrellflynn.c -o -e gkrellm-hddtemp.c ]; then
    make gkrellm2
  elif [ -e src20/gkrellsun.c ]; then
    pushd src20
    make
    cp gkrellsun.so ..
    popd
  elif [ -e src/gkleds.c ]; then
    /usr/bin/perl -p -i -e 's|GK_LDFLAGS|GKM_LDFLAGS|' src/Makefile.am
    aclocal-1.9
    automake-1.9
    libtoolize
    export  LIBS="-lgthread-2.0 -lgmodule-2.0"
    ./configure --libdir=`pwd`
    make install
  else
    make clean
    make
  fi
  cd ..
done
popd

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_includedir}
mkdir -p %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}%{_initscriptdir}
mkdir -p %{buildroot}%{_libdir}/pkgconfig
make install \
    LOCALEDIR=%{buildroot}%{_datadir}/locale \
    INSTALLDIR=%{buildroot}%{_bindir} \
    SINSTALLDIR=%{buildroot}%{_sbindir} \
    MANDIR=%{buildroot}%{_mandir}/man1 \
    PKGCONFIGDIR=%{buildroot}%{_libdir}/pkgconfig \
    INCLUDEDIR=%{buildroot}%{_includedir} \
    STRIP=""
install -m 755 %{SOURCE1} %{buildroot}%{_initscriptdir}/gkrellmd
cp server/gkrellmd.conf %{buildroot}%{_sysconfdir}/gkrellmd.conf
cp gkrellm.pc %{buildroot}%{_libdir}/pkgconfig/
perl -p -i -e 's,/usr/local,/usr,g' %{buildroot}%{_libdir}/pkgconfig/gkrellm.pc

# Plugins
mkdir -p %{buildroot}%{_libdir}/gkrellm2/plugins
mkdir -p %{buildroot}%{_libdir}/gkrellm2/plugins-gkrellmd
cp plugins/*/*.so %{buildroot}%{_libdir}/gkrellm2/plugins

# Themes
mkdir -p %{buildroot}%{_datadir}/gkrellm2/themes
pushd GKrellM-skins
for i in *
do
  tar xzf $i -C %{buildroot}%{_datadir}/gkrellm2/themes
done
popd
find %{buildroot}%{_datadir}/gkrellm2/themes -type d | xargs chmod 755

perl -p -i -e "s,\r,," \
  %{buildroot}%{_datadir}/gkrellm2/themes/Evolution/gkrellmrc
perl -p -i -e "s,StyleChar ,StyleChart," \
  %{buildroot}%{_datadir}/gkrellm2/themes/HeliX-Sweetpill.gkrellm/gkrellmrc*
perl -p -i -e "s,StylePane;,StylePanel," \
  %{buildroot}%{_datadir}/gkrellm2/themes/HeliX-Sweetpill.gkrellm/gkrellmrc*

%find_lang %{name}

# below is the desktop file and icon stuff.
mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor gnome             \
  --dir %{buildroot}%{_datadir}/applications \
  %{SOURCE2}
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/48x48/apps
install -p -m 644 %{SOURCE3} \
  %{buildroot}%{_datadir}/icons/hicolor/48x48/apps

%post
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%pre daemon
getent group gkrellmd >/dev/null || groupadd -r gkrellmd
getent passwd gkrellmd >/dev/null || \
useradd -r -g gkrellmd -M -d / -s /sbin/nologin -c "GNU Krell daemon" gkrellmd
:

%post daemon
/sbin/chkconfig --add gkrellmd || :

%preun daemon
if [ "$1" = "0" ]; then
    /sbin/service gkrellmd stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del gkrellmd || :
fi

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYRIGHT Changelog README Themes.html
%{_bindir}/%{name}
%{_libdir}/gkrellm2
%exclude %{_libdir}/gkrellm2/plugins/gkrellmms.so
%{_datadir}/gkrellm2
%{_mandir}/man1/%{name}.1*
%{_datadir}/applications/gnome-%{name}.desktop
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png

%files devel
%defattr(-,root,root,-)
%{_includedir}/gkrellm2
%{_libdir}/pkgconfig/%{name}.pc

%files daemon
%defattr(-,root,root,-)
%{_initscriptdir}/gkrellmd
%config(noreplace) %{_sysconfdir}/gkrellmd.conf
%{_sbindir}/gkrellmd
%{_mandir}/man1/gkrellmd.*

%files xmms
%defattr(-,root,root)
%{_libdir}/gkrellm2/plugins/gkrellmms.so

%changelog
* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.4-8m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.4-6m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.4-5m)
- fix build failure

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.4-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.4-3m)
- fix build

* Thu Apr 22 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.3.4-2m)
- fix lib64 patch

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.4-1m)
- update 2.3.4

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-7m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-5m)
- apply glibc210 patch

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-4m)
- rebuild against openssl-0.9.8k

* Thu Jan 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-3m)
- update lib64.patch to enable build with rpm-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-2m)
- rebuild against rpm-4.6

* Wed Dec 24 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2
- update patches:
  Patch1: gkrellm-2.3.2-config.patch
  Patch4: gkrellm-2.3.2-libdir.patch
- add patch:
  Patch16: gkrellm-2.3.2-nologh.patch
- remove patch:
  Patch5: gkrellm-2.3.1-netdb.patch

* Wed Aug 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-9m)
- correct %%{_initscriptdir}/gkrellmd

* Fri Jul 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-8m)
- package daemon Provides and Obsoletes package server

* Fri Jul 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-7m)
- update lib64.patch

* Fri Jul 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.1-6m)
- separate packages : gkrellaclock
- add Source2:        gkrellm.desktop
- add Source3:        gkrellm.png
- rename patch(n) to patch(n+10)
- add Patch1 .. Patch5
- rename subpackage server to daemon
- new subpackage devel

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-5m)
- rebuild against gnutls-2.4.1

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-4m)
- rebuild against lm_sensors-3.0.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-2m)
- %%NoSource -> NoSource

* Wed Dec 26 2007 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (2.3.1-1m)
- update to 2.3.1
- update GkrellMBgChg2    to 0.1.8
- update gkrelltop        to 2.2.10

* Fri Oct 12 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Mon Mar 12 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.10-5m)
- Source129: gkrellm-gkfreq-1.0.tar.gz

* Tue Mar  6 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.10-4m)
- Source111: gkrellshoot-0.4.4.tar.gz
- Source128: gkrelltop_2.2.9.orig.tar.gz 
- update Patch: gkrellm-2.2.10-lib64.patch

* Thu Mar  1 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.10-3m)
- Patch5: gkrellm-wifi-0.9.12-kh.patch

* Fri Nov 24 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.2.10-2m)
- revised lib64 patch and spec file

* Sat Nov 04 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.2.10-1m)
- update to 2.2.10
- update GKrellAclock     to 0.3.4
- update gkrellsun        to 1.0.0
- update gkleds           to 0.8.2
- update GkrellMBgChg2    to 0.1.7
- remove gkrellsun-0.12.2-gcc4.patch

* Sat Jul  8 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.9-2m)
- disable xmms2

* Sat May  6 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.9-1m)
- update to 2.2.9

* Sat Mar 11 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.7-10m)
- build fix for not %%{ix86}, x86_64 arch.
- rename x86_64.patch -> lib64.patch

* Fri Jan 27 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.7-9m)
- edit spec for x86_64 to locate the correct plugins directory
- plugin gkx86info is available for x86_64

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.7-8m)
- add gcc4 patch
- Patch3: gkrellsun-0.12.2-gcc4.patch
- Patch4: gkrellmitime-1.0.1-gcc4.patch

* Wed Aug 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.7-7m)
- new x86_64.patch.

* Wed Jul 27 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.2.7-6m)
- remove xmms2 plugin from gkrellm package
  already belongs with gkrellm-xmms2 package

* Sun Jul 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.7-5m)
- add xmms2 to BuildRequires 

* Thu Jul 21 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.7-4m)
- update xmms2 plugin GkrellXmms2 to version 0.4.0

* Thu Jul 14 2005 Toru Hoshina <t@momonga-linux.org>
- (2.2.7-3m)
- enable x86_64.

* Tue Jun 14 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.7-2m)
- add xmms2 plugin	 GkrellXmms2 0.2.1

* Wed Jun  1 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.2.7-1m)
- version update to 2.2.7
- revise Source0 URI
- add %%{_libdir}/gkrellm2/plugins-gkrellmd and move gkrelltopd.so in it
- update GkrellmItime     to 1.0.1
- update GKrellAclock     to 0.3.3
- update GkrellShoot      to 0.4.3
- update GKrellflynn      to 0.8
- update GKrellM volume   to 2.1.13
- update GKrellMMS        to 2.1.22
- update gkrellsun        to 0.12.2
- update GKrellMBgChg     to 0.1.4
- update gkrelltop        to 2.2.6
- remove gkrellmwho.configure.patch, gkrelltop.includepath.patch
  and rewrite on %%build

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.4-2m)
- enable x86_64.

* Thu Sep  9 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.4-1m)
- version update to 2.2.4
  some bug fix and source improvements since 2.2.2
  
* Thu Jul 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.2-1m)
- version update to 2.2.2

* Thu Jun 10 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.1-2m)
- add gkrelltop includepath patch

* Fri Jun  4 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.1-1m)
- version upgrade to 2.2.1
- update gkrellaclock 	  to 0.3.2.1

* Wed Jun  2 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.0-5m)
- update gkrellm-volume   to 2.1.11 
- update gkrellmms	  to 2.1.20 
- update gkrellsun	  to 0.11.1 
- update gkrellmbgchg2	  to 0.1.0
- add gkrelltop-2.2.4
- remove gkrellmms-fix_for_2.2.0.patch

* Wed Jun  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.0-4m)
- update to 2.2.0(release)

* Sun May  9 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.2.0-3m)
- update to 2.2.0-test2

* Tue Apr 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.2.0-2m)
- resurrect gkrellmms

* Fri Apr 16 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.0-1m)
- update to gkrellm-2.2.0-test1.tar.bz2
- temporarily removed gkrellmms plugin

* Fri Apr 16 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.28-4m)
- update to gkrellmms	to 2.1.16
 
* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.1.28-3m)
- rebuild against for gtk+-2.4.0

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.28-2m)
- add hddtemp plugin.

* Tue Mar  9 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.1.28-1m)
- update to 2.1.28
- add INSTALLROOT on make
- use %%find_lang
- add CREDITS to %%doc
- update gkrellsun        to 0.10.6
- update GKrellMMS        to 2.1.15

* Wed Mar  3 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.1.27-1m)
- update to 2.1.27
- update GKrellMBgChg     to 0.0.9

* Wed Jan 21 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.25-1m)
- update to 2.1.25

* Sun Jan  4 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.1.24-4m)
- replace /usr/local with /usr in gkrellm.pc

* Sun Jan  4 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.1.24-3m)
- export PKG_CONFIG_PATH="`pwd`" to enable gkrellm.pc
- add gkrellm-wifi plugin

* Sun Jan  4 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.1.24-2m)
- add gkrellm.pc to %%files

* Fri Dec 26 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.1.24-1m)
- update to 2.1.24

* Sat Dec 20 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.1.23-1m)
- update to 2.1.23
- revise gkrellm-font.patch for gkrellm-2.1.23
- update GKrellMBgChg     to 0.0.8
- update GKrellMMS        to 2.1.13
- update GKrellM-Wireless to 2.0.3

* Wed Oct 15 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.1.21-1m)
- update to 2.1.21

* Sat Oct 11 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.1.20-1m)
- update to 2.1.20
- update Volume           to 2.1.9
- separate gkrellmd to server package
  import gkrellmd.init from RawHide

* Fri Sep 26 2003 Kimitake Shibta <cipher@da2.so-net.ne.jp>
- (2.1.19-2m)
- gkrellmwho test Program Change include file dir

* Sat Sep 13 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.1.19-1m)
- update to 2.1.19
- use %%NoSource

* Fri Aug 29 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.1.16-1m)
- update to 2.1.16
- update gkPCcard         to 1.0.0pre3
- update GKrellStock      to 0.5.1
- update GKrellMMS        to 2.1.12
- update Volume           to 2.1.8
- add GKrellMBgChg 0.0.7
- add GkrellmWHO2 0.2.8
- use %%{momorel}

* Thu Jul 1 2003 kourin <kourin@fh.freeserve.ne.jp>
- (2.1.14-1m)
  update to 2.1.14
  
* Thu Jun 26 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.13-1m)
  update to 2.1.13
  
* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (%{ver}-6m)
- rebuild against for XFree86-4.3.0

* Thu Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.7-5m)
- move plugins dir

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.7-4m)
- new gkx86info0.0.2.tar.gz

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.7-3m)
- update to 2.1.7a
  this version contains:
    - ja.po update.
    - Hajimu UMEMOTO's small one liner gkrellmd change to make 
      the new gkrellmd drop privileges options work in *BSD.
    - small win32 fix.

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.7-2m)
- revise gkrellm-font.patch for 2.1.7

* Thu Jan 30 2003 Yonekawa Susumu <yone@webtech.co.jp> (commited by zaki)
- (2.1.7-1m)
- update to 2.1.7
- update GkrellMItime     to 1.0
- update GKrellKam        to 2.0.0
- update gkrellmoon       to 0.6
- update GKrellAclock     to 0.3.2
- update GKrellStock      to 0.5
- update GKrellShoot      to 0.4.1
- update gkPCcard         to 1.0pre1
- update Gkx86info for GKrellM2
- update GKrellflynn      to 0.6
- update GKrellM-Wireless to 2.0.2
- update Volume           to 2.1.7
- update GKrellMMS        to 2.1.6
- update gkrellm-reminder to 2.0.0
- update gkrellsun        to 0.9.1
- update GKrellMLaunch    to 0.5
- add gkleds 0.8.1
- remove American Flag, Earth Anim
- remove GKrellMouse
- remove GK-Taskman
- remove gkrellmPing
- remove gkrellmwho
- remove IBAM
- remove SNMP
- remove all themes and use tarball of all the skins on www.muhri.net
- fix gkrellm-font.patch for GKrellM 2.1.6
- add system plugin directory patch
- remove gcc-3.1 patch
- system plugin directory, system theme directory were changed
- gkPCcard has normal Makefile. remove ./build_gkPCcard
- gkrellsun must be make in src20/
- requires gtk+ instead gtk1+
- remove imlib, net-snmp requires
- remove ./enable_nls on build
- add SYSTEM_PLUGINS_DIR on make command
- GKrellflynn needs make gkrellm2
- gkrellsun needs make on src20/ directory
- add %{_bindir}/gkrellmd on %files
- use %{_bindir}, %{_includedir} macro tag

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.13-2m)
- add gcc-3.1 patch

* Sun Jul 28 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.13-1m)
- update to 1.2.13
- update gkrellmms    to 0.5.7, and remove gkrellmms-patch-0.5.5 (merged)
- update gkrellkam    to 0.3.4
- update gkrellaclock to 0.2
- update gkrellstock  to 0.4
- update gkrellshoot  to 0.3.11
- update gkPCcard     to r0.5

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2.11-10k)
- cancel gcc-3.1 autoconf-2.53

* Sat May 25 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2.11-8k)
- gcc 3.1(Patch100: gkrellm-gcc31.patch)

* Mon Apr 29 2002 Toru Hoshina <t@kondara.org>
- (1.2.11-6k)
- muki-. gkx86info.so is x86 only.

* Mon Apr 29 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.11-4k)
- separate xmms plugin and snmp plugin

* Sat Apr 27 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.11-2k)
- version up.
- apply patch for gkrellmms-0.5.5.
- omit NoSource: 0.

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.8-12k)
- rebuild against for ucd-snmp 4.2.3

* Fri Feb 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.8-10k)
- add xmms-devel build require

* Tue Jan 15 2002 Tsutomu Yasuda <tom@digitalfacotry.co.jp>
- (1.2.8-8k)
  remove fmonitor.
  dora ni o ko ra re ta------

* Wed Jan  9 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.2.8-6k)
- Should BuildPreReq: ucd-snmp-devel.

* Mon Jan  7 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.2.8-4k)
  niwatama

* Mon Jan  7 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.2.8-2k)
  update

* Sun Dec  9 2001 Toru Hoshina <t@kondara.org>
- (1.2.5-6k)
  alpha....

* Fri Dec  7 2001 Tsutomu Yasuda <tom@kondara.org>
- (1.2.5-2k)
  added plugins and themes

* Tue Dec  4 2001 Toru Hoshina <t@kondara.org>
- (1.2.4-6k)
- add plugins dir. it'd be owned by gkrellm itself.

* Fri Nov  9 2001 Tsutomu Yasuda <tom@kondara.org>
- (1.2.4-4k)
  enabled font_override & use_fontset
  changed default font

* Thu Nov 01 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.4-2k)
- up to 1.2.4

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.2.2-6k)
- rebuild against gettext 0.10.40.

* Sun Sep 16 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2.2-4k)
- add gkrellmrc to document dir

* Sun Aug 26 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.2-2k)
- from Jirai (1.2.2-3k)
 
* Sun Aug 26 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.2-3k)
- up to 1.2.2
- cleanup spec file

* Wed Feb 15 2001 Daisuke Yabuki <dxy@acm.org>
- (1.0.6-3k)
- initial build
