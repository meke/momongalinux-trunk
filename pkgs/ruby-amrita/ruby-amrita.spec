%global momorel 15

%global rbname amrita
Summary: amrita -- a html/xhtml template library for Ruby
Name: ruby-%{rbname}

Version: 1.0.2
Release: %{momorel}m%{?dist}
Group: Applications/Text
License: GPL
URL: http://amrita.sourceforge.jp/

Source0: http://osdn.dl.sourceforge.jp/amrita/10939/amrita-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: ruby >= 1.9.2

Requires: ruby

%description
a html/xhtml template library for Ruby

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q -n amrita-%{version}

%build
find sample -type f -exec ruby -p -i -e '$_.sub!( %r|/usr/local/bin/ruby|, "/usr/bin/env ruby" )' {} \;

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

make PREFIX=%{buildroot}%{_prefix} \
    SITE_DIR=%{buildroot}%{ruby_libdir} \
    install

## because of automatically version generation with ruby :-) .
(find %{buildroot}/usr -type f -o -type l | sort ) > %{name}.files

mv %{name}.files %{name}.files.in
sed "s|.*/usr|/usr|" < %{name}.files.in | while read file; do
        if [ -d %{buildroot}/${file} ]; then
                echo -n '%dir '
        fi
        echo ${file}
done > %{name}.files

%clean
test %{buildroot} != / && rm -rf %{buildroot}

%files -f %{name}.files
%defattr (-,root,root)
%doc ChangeLog MANIFEST README README_ja docs sample sample.log

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2-15m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-12m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-11m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-10m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-6m)
- %%NoSource -> NoSource

* Sun Jan  9 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.2-5m)
- revise URI

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (1.0.2-4m)
- merge from ruby-1_8-branch.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (1.0.2-3m)
- rebuild against ruby-1.8.0.

* Thu Apr  3 2003 zunda <zunda at freeshell.org>
- (1.0.2-2m)
- replace #!/usr/local/bin/ruby in sample codes into #!/usr/bin/env ruby

* Sat Jan 18 2003 HOSONO Hidetomo <h@h12o.org>
- (1.0.2-1m)
- initial release
