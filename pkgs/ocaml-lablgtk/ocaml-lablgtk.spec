%global momorel 3
%global ocamlver 3.12.1

%global debug_package %{nil}

Summary: Objective Caml interface to gtk+
Name: ocaml-lablgtk
Version: 2.14.2
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: "LGPLv2 with exceptions"
URL: http://wwwfun.kurims.kyoto-u.ac.jp/soft/olabl/lablgtk.html
Source0: http://wwwfun.kurims.kyoto-u.ac.jp/soft/olabl/dist/lablgtk-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: lablgtk = %{version}-%{release}, lablgtk2 = %{version}-%{release}
Obsoletes: lablgtk, lablgtk2
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: ocaml-camlp4-devel >= %{ocamlver}
BuildRequires: ocaml-ocamldoc >= %{ocamlver}
BuildRequires: ocaml-lablgl-devel >= 1.04-7m
BuildRequires: GConf2-devel
BuildRequires: ORBit2-devel
BuildRequires: atk-devel
BuildRequires: cairo-devel
BuildRequires: enchant-devel
BuildRequires: expat-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel
BuildRequires: glibc-devel
BuildRequires: gcr-devel
BuildRequires: gnome-panel-devel
BuildRequires: gnome-vfs2-devel
BuildRequires: gtk2-devel
BuildRequires: gtkglarea-devel
BuildRequires: gtkspell-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libart_lgpl-devel
BuildRequires: libbonobo-devel
BuildRequires: libbonoboui-devel
BuildRequires: libglade2-devel
BuildRequires: libgnome-devel
BuildRequires: libgnomecanvas-devel
BuildRequires: libgnomeui-devel
BuildRequires: libpng-devel
BuildRequires: librsvg2-devel >= 2.22.2-3m
BuildRequires: libxcb-devel
BuildRequires: libxml2-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: pango-devel
BuildRequires: zlib-devel

%global __ocaml_requires_opts -i GtkSourceView_types -i GtkSourceView2_types

%description
LablGTK is is an Objective Caml interface to gtk+.

It uses the rich type system of Objective Caml 3 to provide a strongly
typed, yet very comfortable, object-oriented interface to gtk+. This
is not that easy if you know the dynamic typing approach taken by
gtk+.

%package doc
Group: System Environment/Libraries
Summary: Documentation for LablGTK
Requires: %{name} = %{version}-%{release}
Provides: lablgtk-doc = %{version}-%{release}, lablgtk2-doc = %{version}-%{release}
Obsoletes: lablgtk-doc, lablgtk2-doc

%description doc
Documentation for LablGTK.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Provides: lablgtk = %{version}-%{release}, lablgtk2 = %{version}-%{release}
Obsoletes: lablgtk, lablgtk2

%description devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%setup -q -n lablgtk-%{version}

# version information in META file is wrong
perl -pi -e 's|version="1.3.1"|version="%{version}"|' META

%build
%configure --with-gl --enable-debug
perl -pi -e "s|-O|$RPM_OPT_FLAGS|" src/Makefile
make world
make doc CAMLP4O="camlp4o -I %{_libdir}/ocaml/camlp4/Camlp4Parsers"

%install
rm -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_bindir}
%{__mkdir_p} %{buildroot}%{_libdir} 
%{__mkdir_p} %{buildroot}%{_libdir}/ocaml/lablgtk2
%{__mkdir_p} %{buildroot}%{_libdir}/ocaml/stublibs
make install \
     BINDIR=%{buildroot}%{_bindir} \
     LIBDIR=%{buildroot}%{_libdir} \
     INSTALLDIR=%{buildroot}%{_libdir}/ocaml/lablgtk2 \
     DLLDIR=%{buildroot}%{_libdir}/ocaml/stublibs
%{__cp} META %{buildroot}%{_libdir}/ocaml/lablgtk2

# Remove unnecessary *.ml files (ones which have a *.mli).
pushd %{buildroot}%{_libdir}/ocaml/lablgtk2
for f in *.ml; do \
  b=`basename $f .ml`; \
  if [ -f "$b.mli" ]; then \
    rm $f; \
  fi; \
done
popd

# Remove .cvsignore files from examples directory.
find examples -name .cvsignore -exec rm {} \;

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README COPYING LGPL CHANGES CHANGES.API
%dir %{_libdir}/ocaml/lablgtk2
%{_libdir}/ocaml/lablgtk2/*.cmi
%{_libdir}/ocaml/lablgtk2/*.cma
%{_libdir}/ocaml/lablgtk2/*.cmxs
%{_libdir}/ocaml/stublibs/*.so
%{_bindir}/gdk_pixbuf_mlsource
%{_bindir}/lablgladecc2
%{_bindir}/lablgtk2

%files devel
%defattr(-,root,root,-)
%{_libdir}/ocaml/lablgtk2/META
%{_libdir}/ocaml/lablgtk2/*.a
%{_libdir}/ocaml/lablgtk2/*.cmxa
%{_libdir}/ocaml/lablgtk2/*.cmx
%{_libdir}/ocaml/lablgtk2/*.mli
%{_libdir}/ocaml/lablgtk2/*.ml
%{_libdir}/ocaml/lablgtk2/*.h
%{_libdir}/ocaml/lablgtk2/gtkInit.cmo
%{_libdir}/ocaml/lablgtk2/gtkInit.o
%{_libdir}/ocaml/lablgtk2/gtkThInit.cmo
%{_libdir}/ocaml/lablgtk2/gtkThread.cmo
%{_libdir}/ocaml/lablgtk2/gtkThread.o
%{_libdir}/ocaml/lablgtk2/propcc
%{_libdir}/ocaml/lablgtk2/varcc

%files doc
%defattr(-,root,root,-)
%doc examples doc/html

%changelog
* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.2-3m)
- rebuild for librsvg2 2.36.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.2-2m)
- rebuild for glib 2.33.2

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2
- rebuild against ocaml-3.12.1

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-6m)
- rebuild against gnome-3.0.1 (rebuild only)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.1-4m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-3m)
- build fix (add patch0)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14.1-2m)
- full rebuild for mo7 release

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0
-- import Patch0 from Rawhide (2.12.0-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.1-6m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.1-5m)
- rebuild against ocaml-3.11.0

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.1-4m)
- rebuild against librsvg2-2.22.2-3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.1-3m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.1-2m)
- rebuild against ocaml-3.10.2

* Fri Feb 29 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.0-4m)
- %%NoSource -> NoSource

* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.0-3m)
- rebuild against ocaml-3.10.1-1m

* Mon Dec 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.0-2m)
- rebuild against ocaml-lablgl-1.03-1m

* Tue Oct 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Mon Oct  1 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-0.20060908.4m) 
- disable debug_package

* Sat Sep 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-0.20060908.3m)
- rename to ocaml-lablgtk and split packages
- rebuild against ocaml-3.10.0-1m
- sync with Fedora devel
-- * Sat Jul  7 2007 Gerard Milmeister <gemi@bluewin.ch> - 2.6.0-8.20060908cvs
-- - update to cvs version
-- - renamed package from lablgtk to ocaml-lablgtk

* Wed Feb 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-0.20060908.2m)
- rename lablgtk to lablgtk2

* Fri Oct  6 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-0.20060908.1m)
- update to cvs snapshot
- revise %%changelog

* Wed Jul 12 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-1m)
- import from Fedora Extras 5

* Wed May 10 2006 Gerard Milmeister <gemi@bluewin.ch> - 2.6.0-4
- rebuilt for ocaml 3.09.2
- removed unnecessary ldconfig

* Sun Feb 26 2006 Gerard Milmeister <gemi@bluewin.ch> - 2.6.0-3
- Rebuild for Fedora Extras 5

* Sun Jan  1 2006 Gerard Milmeister <gemi@bluewin.ch> - 2.6.0-1
- new version 2.6.0

* Sat Sep 10 2005 Gerard Milmeister <gemi@bluewin.ch> - 2.4.0-6
- include META file

* Sun May 22 2005 Toshio Kuratomi <toshio-iki-lounge.com> - 2.4.0-5
- Removed gnome-1.x BuildRequires
- Removed BuildRequires not explicitly mentioned in the configure script
  (These are dragged in through dependencies.)
- Fix a gcc4 error about lvalue casting.

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 2.4.0-4
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sat Feb 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 0:2.4.0-2
- Remove %%{_smp_mflags} as it breaks the build

* Sat Feb 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 0:2.4.0-1
- New Version 2.4.0

* Sat Nov 13 2004 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:2.2.0-5
- BR gnome-panel-devel instead of gnome-panel (since FC2!)

* Wed Apr 28 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:2.2.0-0.fdr.4
- Compile with debug

* Tue Dec  2 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:2.2.0-0.fdr.3
- Make GL support optional using --with gl switch

* Fri Nov 28 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:2.2.0-0.fdr.2
- Added dependency on libcroco
- Honor RPM_OPT_FLAGS

* Fri Oct 31 2003 Gerard Milmeister <milmei@ifi.unizh.ch> - 0:2.2.0-0.fdr.1
- First Fedora release

* Mon Oct 13 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 2.2.0.

* Sun Aug 17 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Provide ocaml-lablgtk (reported by bishop@platypus.bc.ca).

* Wed Apr  9 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Rebuilt for Red Hat 9.

* Tue Nov 26 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Initial build
