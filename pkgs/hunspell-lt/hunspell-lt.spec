%global momorel 6

Name: hunspell-lt
Summary: Lithuanian hunspell dictionaries
Version: 1.2.1
Release: %{momorel}m%{?dist}
Source: ftp://ftp.akl.lt/ispell-lt/lt_LT-%{version}.zip
Group: Applications/Text
URL: ftp://ftp.akl.lt/ispell-lt/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: BSD
BuildArch: noarch

Requires: hunspell

%description
Lithuanian hunspell dictionaries.

%prep
%setup -q -n lt_LT-%{version}

%build
chmod -x *
tr -d '\r' < INSTRUKCIJOS.txt > INSTRUKCIJOS.txt.new
mv INSTRUKCIJOS.txt.new INSTRUKCIJOS.txt

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README.EN INSTRUKCIJOS.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-1m)
- import from Fedora to Momonga

* Wed Feb 20 2008 Caolan McNamara <caolanm@redhat.com> - 1.2.1-1
- latest version

* Sat Feb 16 2008 Caolan McNamara <caolanm@redhat.com> - 1.2-1
- next version

* Tue Jun 05 2007 Caolan McNamara <caolanm@redhat.com> - 1.1-1.20070510cvs
- next version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 1.1-1.20061127cvs
- initial version
