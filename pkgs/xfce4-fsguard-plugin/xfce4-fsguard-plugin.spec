%global momorel 1

%global xfce4ver 4.10.0
%global major 1.0

Summary: 	plugin for xfce4 which checks free disk space
Name: 		xfce4-fsguard-plugin
Version: 	1.0.1
Release:	%{momorel}m%{?dist}

Group:          User Interface/Desktops
License:        BSD
URL:		http://goodies.xfce.org/
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel libxml2-devel pkgconfig
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:   	xmms xfce4-panel

%description 
A little Xfce plugin, which checks the free space on the chosen mountpoint 
frequently. It displays 4 different icons and a message box, depending on the
free space. The amount of free disk space is visible in a tooltip. If you 
left-click on its icon, it opens the mountpoint directory in the file manager.

%prep 
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete
%find_lang %{name}


%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :



%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
%{_libdir}/xfce4/panel/plugins/libfsguard.so
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/xfce4/panel/plugins/*.desktop
%{_datadir}/locale/*

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.1-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-8m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-5m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-4m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-2m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-1m)
- update
- rebuild against rpm-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-1m)
- update 
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0
- rebuild against xfce4 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-2m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.1-1m)
- update to 0.2.1

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-10m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-9m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-8m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.0-7m)
- rebuild against xfce4 4.1.99.2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-6m)
- rebuild against xfce4 4.1.90

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.0-5m)
- rebuild against xfce4 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.0-4m)
- rebuild against for libxml2-2.6.8

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.0-3m)
- revise %files

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-2m)
- rebuild against xfce4 4.0.3

* Wed Dec 10 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.2.0-1m)
- first import
