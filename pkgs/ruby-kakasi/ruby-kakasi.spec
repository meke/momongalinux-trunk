%global momorel 16

%global rbname kakasi
Summary: Ruby/KAKASI extension module
Name: ruby-%{rbname}

Version: 020928
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://www.notwork.org/~gotoken/ruby/p/kakasi/

Source0: http://www.notwork.org/~gotoken/ruby/p/%{rbname}/%{rbname}-%{version}.tar.gz 
Patch0: ruby-kakasi-ruby19.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: kakasi-devel

%description
This extension module is a Ruby binding of ``KAKASI'', which is
developed by Hironobu Takahashi.  KAKASI is the language processing
filter to convert Kanji characters to Hiragana, Katakana or Romaji
and may be helpful to read Japanese documents.

%prep
%setup -q -n kakasi-%{version}
%patch0 -p1 -b .ruby19

mv wdcnt wdcnt.in
echo '#!/usr/bin/ruby -Ke' > wdcnt
grep -v '^#!' wdcnt.in >> wdcnt
chmod 755 wdcnt
rm wdcnt.in

%build
ruby extconf.rb
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README README.jp ChangeLog MANIFEST test.rb wdcnt*
%{ruby_sitearchdir}/kakasi.so

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (020928-16m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (020928-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (020928-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (020928-13m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (020928-12m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (020928-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (020928-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (020928-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (020928-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (020928-7m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (020928-6m)
- rebuild against ruby-1.8.6-4m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (020928-5m)
- /usr/lib/ruby

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (020928-4m)
- rebuild against ruby-1.8.2

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (020928-3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (020928-2m)
- rebuild against ruby-1.8.0.

* Fri Nov 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (020928-1m)
- version 020928

* Wed Apr 11 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (000124-4k)
- add kakasi-devel to BuildPreReq Tag

* Wed Mar 28 2001 Kenta MURATA <muraken2@nifty.com>
- (000124-3k)
- first build.
