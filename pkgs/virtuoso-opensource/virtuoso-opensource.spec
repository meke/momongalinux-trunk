%global momorel 5
%global simplename virtuoso

Summary: A high-performance object-relational SQL database
Name: virtuoso-opensource
Version: 6.1.6
Release: %{momorel}m%{?dist}
License: GPLv2 and see "LICENSE"
URL: http://virtuoso.openlinksw.com/dataspace/dav/wiki/Main/
Group: Applications/Databases
Source0: http://dl.sourceforge.net/project/%{simplename}/%{simplename}/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ImageMagick-devel >= 6.8.8.10
BuildRequires: automake
BuildRequires: bison
BuildRequires: coreutils
BuildRequires: flex
BuildRequires: gperf
BuildRequires: htmldoc
BuildRequires: java-devel
BuildRequires: libiodbc-devel
BuildRequires: libtool
BuildRequires: libxml2-devel
BuildRequires: openldap-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: zlib-devel

%description
Virtuoso is a scalable cross-platform server that combines SQL/RDF/XML
Data Management with Web Application Server and Web Services Platform
functionality.

%prep
%setup -q

%build
%configure \
	--with-layout=redhat \
	--without-internal-zlib \
	--enable-shared \
	--enable-iodbc \
	--enable-openssl \
	--enable-xml

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# move virtuoso.ini
mkdir -p %{buildroot}%{_sysconfdir}/%{simplename}
mv %{buildroot}%{_var}/lib/virtuoso/db/%{simplename}.ini %{buildroot}%{_sysconfdir}/%{simplename}/
ln -s ../../../..%{_sysconfdir}/%{simplename}/%{simplename}.ini %{buildroot}%{_var}/lib/%{simplename}/db/%{simplename}.ini

# move isql and more (conflicts with unixODBC-2.2.14-2m)
mkdir -p %{buildroot}%{_libexecdir}/%{simplename}
mv %{buildroot}%{_bindir}/inifile %{buildroot}%{_libexecdir}/%{simplename}/
mv %{buildroot}%{_bindir}/isql %{buildroot}%{_libexecdir}/%{simplename}/
mv %{buildroot}%{_bindir}/isqlw %{buildroot}%{_libexecdir}/%{simplename}/

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib%{simplename}-t.la
rm -f %{buildroot}%{_libdir}/virtodbc*.la

# get rid of static libraries
rm -f %{buildroot}%{_libdir}/lib%{simplename}-t.a
rm -f %{buildroot}%{_libdir}/virtodbc*.a

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING CREDITS ChangeLog INSTALL LICENSE NEWS README*
%doc %{_docdir}/%{simplename}
%config(noreplace) %{_sysconfdir}/virtuoso/%{simplename}.ini
%{_bindir}/virt_mail
%{_bindir}/%{simplename}-t
%{_libdir}/hibernate
%{_libdir}/jdbc-2.0
%{_libdir}/jdbc-3.0
%{_libdir}/jdbc-4.0
%{_libdir}/jena
%{_libdir}/sesame
%{_libdir}/%{simplename}
%{_libdir}/virtodbc.so
%{_libdir}/virtodbc_r.so
%{_libdir}/virtodbcu.so
%{_libdir}/virtodbcu_r.so
%{_libexecdir}/%{simplename}
%{_datadir}/%{simplename}
%{_var}/lib/%{simplename}

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1.6-5m)
- rebuild against ImageMagick-6.8.8.10

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1.6-4m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1.6-3m)
- rebuild against ImageMagick-6.8.2.10

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1.6-2m)
- rebuild against ImageMagick-6.8.0.10

* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1.6-1m)
- update to 6.1.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.2-2m)
- rebuild for new GCC 4.6

* Sun Jan 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1.2-1m)
- update to 6.1.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.1.1-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.1-2m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.1-1m)
- update to 6.1.1

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.0-4m)
- revise source URI, this version is not development version any more

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0.0-3m)
- delete __libtoolize hack

* Sat Dec 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.0.0-2m)
- add define __libtoolize :

* Thu Dec 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0.0-1m)
- initial package for KDE 4.3.80
- import and modify zlib.patch from Fedora
