%global momorel 1

Summary: The inittab file and the /etc/init.d scripts
Name: initscripts
Version: 9.54
License: GPLv2 and GPLv2+
Group: System Environment/Base
Release: %{momorel}m%{?dist}
Source0: https://fedorahosted.org/releases/i/n/initscripts/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: 0001-remove-ppp-from-translation.patch
Patch10: %{name}-%{version}-momonga-lsb.patch
URL: https://fedorahosted.org/releases/i/n/initscripts/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: mingetty, gawk, sed, mktemp, e2fsprogs >= 1.15
Requires: procps-ng , sysklogd >= 1.3.31
Requires: psmisc, grep
Requires: module-init-tools
Requires: util-linux-ng
Requires: bash >= 3.0
Requires: systemd-sysvinit
Requires: iproute, iputils, net-tools
Requires: momonga-release
Requires: ethtool >= 1:2.6.34, coreutils
Requires: udev >= 151
Requires: popt >= 1.12
Requires: chkconfig
Conflicts: mkinitrd < 4.0, kernel < 2.6.12, mdadm < 2.6.4
Conflicts: ypbind < 1.6, psacct < 6.3.2, kbd < 1.06, lokkit < 0.50
Conflicts: dhclient < 3.0.3
Conflicts: tcsh < 6.13
Conflicts: xorg-x11, glib2 < 2.11.1
Conflicts: alsa-utils < 1.0.14
Conflicts: nut < 2.2.0
Conflicts: ipsec-tools < 0.8.0
Obsoletes: rhsound sapinit
Obsoletes: hotplug
Obsoletes: initscripts-legacy
Requires(pre,preun): chkconfig, shadow-utils, sed, mktemp, fileutils, sh-utils
BuildRequires: glib2-devel popt gettext pkgconfig
Provides: /sbin/service

%description
The initscripts package contains the basic system scripts used to boot
your Red Hat or Fedora system, change runlevels, and shut the system down
cleanly.  Initscripts also contains the scripts that activate and
deactivate most network interfaces.

%package -n debugmode
Summary: Scripts for running in debugging mode
Requires: initscripts
Group: System Environment/Base

%description -n debugmode
The debugmode package contains some basic scripts that are used to run
the system in a debugging mode.

Currently, this consists of various memory checking code.

%prep
%setup -q

%patch0 -p1 -b .ppp
#%%patch10 -p1 -b .momonga

%build
make

%install
rm -rf %{buildroot}
make ROOT=%{buildroot} SUPERUSER=`id -un` SUPERGROUP=`id -gn` mandir=%{_mandir} install 

# build file list of locale stuff in with lang tags
# but it's not recommended on Momonga....:-P
rm -f trans.list
pushd %{buildroot}/%{_datadir}/locale
for foo in * ; do
	echo  "%lang($foo) %{_datadir}/locale/$foo/*/*" >> \
	%{_builddir}/%{name}-%{version}/trans.list
done
popd

rm -rf %{buildroot}/etc/init

%find_lang %{name}

%ifnarch s390 s390x
rm -f \
 $RPM_BUILD_ROOT/etc/sysconfig/network-scripts/ifup-ctc \
%else
rm -f \
 %{buildroot}/etc/rc.d/rc.sysinit.s390init \
 %{buildroot}/etc/sysconfig/init.s390
%endif

# remove patched original file
#find %{buildroot} -name "*\.orig" | xargs rm

touch $RPM_BUILD_ROOT/etc/crypttab
chmod 600 $RPM_BUILD_ROOT/etc/crypttab

rm -f $RPM_BUILD_ROOT/etc/rc.d/rc.local $RPM_BUILD_ROOT/etc/rc.local
## adapt LSB
# touch $RPM_BUILD_ROOT/etc/rc.d/rc.local
# chmod 755 $RPM_BUILD_ROOT/etc/rc.d/rc.local
touch $RPM_BUILD_ROOT/etc/rc.local
chmod 755 $RPM_BUILD_ROOT/etc/rc.local

mv $RPM_BUILD_ROOT/etc/rc.d/init.d  $RPM_BUILD_ROOT/etc/

# /etc/rc?.d conflict chkconfig.
rm -rf $RPM_BUILD_ROOT/etc/rc?.d 

%pre
/usr/sbin/groupadd -g 22 -r -f utmp

%post 
touch /var/log/wtmp /var/run/utmp /var/log/btmp
chown root:utmp /var/log/wtmp /var/run/utmp /var/log/btmp
chmod 664 /var/log/wtmp /var/run/utmp
chmod 600 /var/log/btmp

/sbin/chkconfig --add network
/sbin/chkconfig --add netconsole
if [ $1 -eq 1 ]; then
        /usr/bin/systemctl daemon-reload > /dev/null 2>&1 || :
fi

%preun
if [ $1 = 0 ]; then
  /sbin/chkconfig --del network
  /sbin/chkconfig --del netconsole
fi

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%dir /etc/sysconfig/network-scripts
%config(noreplace) %verify(not md5 mtime size) /etc/adjtime
%config(noreplace) /etc/sysconfig/init
%config(noreplace) /etc/sysconfig/netconsole
%config(noreplace) /etc/sysconfig/readonly-root
/etc/sysconfig/network-scripts/ifdown
/usr/sbin/ifdown
/etc/sysconfig/network-scripts/ifdown-post
/etc/sysconfig/network-scripts/ifup
/usr/sbin/ifup
%dir /etc/sysconfig/console
%dir /etc/sysconfig/modules
/etc/sysconfig/network-scripts/network-functions
/etc/sysconfig/network-scripts/network-functions-ipv6
/etc/sysconfig/network-scripts/init.ipv6-global
%config(noreplace) /etc/sysconfig/network-scripts/ifcfg-lo
/etc/sysconfig/network-scripts/ifup-post
/etc/sysconfig/network-scripts/ifup-routes
/etc/sysconfig/network-scripts/ifdown-routes
/etc/sysconfig/network-scripts/ifup-plip
/etc/sysconfig/network-scripts/ifup-plusb
/etc/sysconfig/network-scripts/ifup-bnep
/etc/sysconfig/network-scripts/ifdown-bnep
/etc/sysconfig/network-scripts/ifup-eth
/etc/sysconfig/network-scripts/ifdown-eth
/etc/sysconfig/network-scripts/ifup-ipv6
/etc/sysconfig/network-scripts/ifdown-ipv6
/etc/sysconfig/network-scripts/ifup-sit
/etc/sysconfig/network-scripts/ifdown-sit
/etc/sysconfig/network-scripts/ifup-tunnel
/etc/sysconfig/network-scripts/ifdown-tunnel
/etc/sysconfig/network-scripts/ifup-aliases
/etc/sysconfig/network-scripts/ifup-ippp
/etc/sysconfig/network-scripts/ifdown-ippp
/etc/sysconfig/network-scripts/ifup-wireless
/etc/sysconfig/network-scripts/ifup-isdn
/etc/sysconfig/network-scripts/ifdown-isdn
%ifarch s390 s390x
/etc/sysconfig/network-scripts/ifup-ctc
%endif
%config(noreplace) /etc/networks
/etc/rwtab
%dir /etc/rwtab.d
/etc/statetab
%dir /etc/statetab.d
/usr/lib/systemd/fedora-*
/usr/lib/systemd/system/*
/etc/inittab
%dir /etc/init.d
#%dir /etc/rc[0-9].d
#/etc/rc[0-9].d
/etc/init.d/*
%ghost %verify(not md5 size mtime) %config(noreplace,missingok) /etc/rc.local
%config(noreplace) /etc/sysctl.conf
/usr/lib/sysctl.d/00-system.conf
/etc/sysctl.d/99-sysctl.conf
%exclude /etc/profile.d/debug*
/etc/profile.d/*
/usr/sbin/sys-unconfig
/usr/bin/ipcalc
/usr/bin/usleep
%attr(4755,root,root) /usr/sbin/usernetctl
/usr/sbin/consoletype
/usr/sbin/genhostid
/usr/sbin/sushell
%attr(2755,root,root) /usr/sbin/netreport
/usr/lib/udev/rules.d/*
/usr/lib/udev/rename_device
/usr/sbin/service
%{_mandir}/man*/*
%dir %attr(775,root,root) /var/run/netreport
%dir /etc/NetworkManager
%dir /etc/NetworkManager/dispatcher.d
/etc/NetworkManager/dispatcher.d/00-netreport
%doc sysconfig.txt sysvinitfiles static-routes-ipv6 ipv6-tunnel.howto ipv6-6to4.howto changes.ipv6 COPYING
/var/lib/stateless
%ghost %attr(0600,root,utmp) /var/log/btmp
%ghost %attr(0664,root,utmp) /var/log/wtmp
%ghost %attr(0664,root,utmp) /var/run/utmp
%ghost %verify(not md5 size mtime) %config(noreplace,missingok) /etc/crypttab
%dir /usr/lib/tmpfiles.d
/usr/lib/tmpfiles.d/initscripts.conf
%dir /usr/libexec/initscripts
%dir /usr/libexec/initscripts/legacy-actions

%files -n debugmode
%defattr(-,root,root)
%config(noreplace) /etc/sysconfig/debug
/etc/profile.d/debug*

%changelog
* Mon May 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (9.54-1m)
- update 9.54

* Fri Apr 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (9.53-1m)
- update 9.53

* Sat Mar 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (9.52-1m)
- update 9.52
- support UserMove env

* Mon Jan 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (9.50-6m)
- remove sysvinit-tools. 

* Tue Jan 14 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (9.50-5m)
- replace /usr/sbin and /sbin

* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.50-4m)
- change udev path

* Mon Nov 18 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.50-3m)
- compat /sbin environment

* Sun Nov 17 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.50-2m)
- change systemd path

* Sun Nov 17 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (9.50-1m)
- update 9.50

* Wed Oct 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.37-4m)
- fix typo in patch

* Sun Oct  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.37-3m)
- fix system freeze on shutdown when NFS is used

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.37-2m)
- rebuild for glib 2.33.2

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.37-1m)
- update 9.37

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.34-1m)
- update 9.34

* Sat Oct 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.33-1m)
- update 9.33

* Fri Oct 28 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (9.32-4m)
- remove Requires: mount

* Sun Oct  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.32-3m)
- add patch

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.32-2m)
- move rc.local to correct space again

* Mon Sep 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.32-1m)
- update 9.32

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.31-1m)
- update 9.31

* Tue Jul 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.30-2m)
- correct handling of directories and symlinks for adapting to LSB rules again
- fix up %%files to avoid conflicting and correct dependencies

* Fri Jul  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.30-1m)
- update 9.30
-- Requires systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.12.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.12.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.12.1-7m)
- full rebuild for mo7 release

* Fri Aug 13 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.12.1-6m)
- fix build failure again, sorry.

* Tue Aug 10 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.12.1-5m)
- fix build failure; use sed instead of ruby

* Sun Jul 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.12.1-4m)
- remove BuildRequires: kudzu-devel

* Thu Jul  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.12.1-3m)
- disable debug mode for the moment
- desktop environments don't work with "export MALLOC_PERTURB_=$$(($$RANDOM %% 255 + 1))"
- https://bugzilla.redhat.com/show_bug.cgi?id=589378

* Thu Jul  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.12.1-2m)
- re-update to version 9.12.1

* Wed Jul  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.12-3m)
- revert to version 9.12
- can not start KDE with initscripts-9.12.1-1m

* Tue Jul  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.12.1-1m)
- update 9.12.1

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (9.12-2m)
- rebuild against ethtool-2.6.34

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.12-1m)
- update 9.12

* Wed May  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.09-1m)
- update 9.09

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.08-1m)
- update 9.08

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.03-2m)
- update 88-clock.rules for udev-151

* Fri Jan 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.03-1m)
- update 9.03

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.95-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.95-2m)
- remove initscripts-8.95-ethtool6.patch

* Sat May 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.95-1m)
- update 8.95

* Mon Mar 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.91-1m)
- update 8.91

* Mon Mar 23 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.90-2m)
- remove unused files
- add initscripts-8.90-plymouth.patch to suppress error message

* Sat Mar 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.90-1m)
- update 8.90

* Sun Mar  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.89-1m)
- update 8.89

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.76.4-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.76.4-2m)
- update Patch3,4 for fuzz=0

* Mon Nov  3 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.76.4-1m)
- update 8.76.4
- [SECURITY] CVE-2008-3524

* Sat Oct 11 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.76.2-3m)
- add Patch6: initscripts-8.76.2-ethtool6.patch

* Sun Jun  1 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (8.76.2-2m)
- fix: remove singlemode script

* Tue May 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (8.76.2-1m)
- update to 8.76.2

* Sat May 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.76.1-1m)
- update to 8.76.1
- changelog is below
-
- * Wed May  7 2008 Bill Nottingham <notting@redhat.com> - 8.76.1-1
- - NMDispatcher/05-netfs: fix check for default route (#445509)
-
- * Fri May  2 2008 Bill Nottingham <notting@redhat.com> - 8.76-1
- - fix tcsh syntax error (#444998)
- - remove debugging cruft from rcS-sulogin

* Fri May  2 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.74-1m)
- update to 8.74 for upstart
- tmp drop Patch5: initscripts-8.63-20080429.patch
- add /etc/NetworkManager/dispatcher.d/05-netfs into %%files

* Tue Apr 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.63-4m)
- add a patch for new pm-utils and fedora-ds-dsgw

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.63-3m)
- rebuild against gcc43

* Wed Feb  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.63-2m)
- release /etc/NetworkManager/dispatcher.d
- it's already provided by NetworkManager

* Tue Feb  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (8.63-1m)
- update to 8.63

* Mon Jul 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (8.54-2m)
- add Requires: chkconfig

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.54-1m)
- update to 8.54

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (8.53-1m)
- update to 8.53

* Tue Dec 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.47-2m)
- modify Patch2: momonga.patch, thanks >> 837
- http://pc8.2ch.net/test/read.cgi/linux/1092539027/837

* Sun Nov 26 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.47-1m)
- update to 8.47

* Sat Jul 29 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (8.31.1-6m)
- obsolete relink header tools (a.k.a momonga)

* Wed Jul 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (8.31.1-5m)
- fix /etc/sysinit(authconfig dialog)

* Thu Jun 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.31.1-4m)
- stop daemon (netfs)

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (8.31.1-3m)
- modified release string

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.31.1-2m)
- revise spec file for rpm-4.4.2
- /etc/rc.d/init.d and /etc/rc.d/rc[0-9].d are already provided by chkconfig

* Thu May 18 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (8.31.1-1m)
- update
- remove kmodule

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (8.17-1m)
- sync with fc5

* Thu Jun 2 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (8.04-4m)
- start netfs after network is started. (Fix nfs mount)

* Tue Mar  8 2005 Toru Hoshina <t@momonga-linux.org>
- (8.04-3m)
- revised Asuna patch.

* Sun Mar  6 2005 Toru Hoshina <t@momonga-linux.org>
- (8.04-2m)
- applied colinux patch. watch /dev/pts when halt.

* Sat Mar 05 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.04-1m)
- update to 8.04
- changelog is below
- * Wed Jan 19 2005 Bill Nottingham <notting@redhat.com> 8.04-1
- - split out ifup/ifdown general case to ifup/ifdown-eth;
-   add ifup/ifdown-bnep (<dwmw2@redhat.com>)
- - ifup-ipsec: add fwd policies (#145507)
- - fix multiple scsi_hostadapter loads (#145432)
- - enable syncookies in sysctl.conf (#145201)
- 
- * Wed Jan 12 2005 Bill Nottingham <notting@redhat.com> 8.03-1
- - use udevsend to handle hotplug events (requires recent udev)
- - remove pump, dhcpcd support
- - fix ONxxx (#136531, <cww@redhat.com>)
- - fix various fgreps to not catch commented lines (#136531, expanded
-   from <cww@redhat.com>)
- - set ETHTOOL_OPTS on addressless devices (#144682, <mpoole@redhat.com>)
- - kill dhcp client even if BOOTOPROTO is now static (#127726, others)
- - replace the use of route/ifconfig with ip in IPv6 code, remove support
-   for ipv6calc (<pb@bierenger.de>, <pekkas@netcore.fi>)
- - fix quoting in daemon() (#144634)
- - make sysctl be silent (#144483)
-   
- * Mon Jan  3 2005 Bill Nottingham <notting@redhat.com> 8.02-1
- - remove initlog, minilogd
- - add a flag to kmodule for use with kudzu's socket mode, use it
- - change setting of IPv6 default route (#142308, <pb@bieringer.de>)
- - netfs: don't unmount NFS root FS (#142169)
- 
- * Mon Dec  6 2004 Bill Nottingham <notting@redhat.com> 8.01-1
- - further bootup noise reductions
- - rc.d/rc.sysinit: do implicit unicode conversion on keymap
- 
- * Mon Nov 29 2004 Bill Nottingham <notting@redhat.com> 8.00-1
- - fix previous fix (#139656)
- 
- * Wed Nov 24 2004 Bill Nottingham <notting@redhat.com> 7.99-1
- - clear and repopulate mtab before mounting other filesystems (#139656)
- - remove more devfs compat
- 
- * Tue Nov 23 2004 Bill Nottingham <notting@redhat.com> 7.98-1
- - various kmodule speedups
- - rc.d/init.d/netfs: don't mount GFS (#140281)
- - fix various minilogd bogosities (#106338)
- 
- * Mon Nov 15 2004 Karsten Hopp <karsten@redhat.de> 7.97-1 
- - configure CTC protocol if CTCPROT is set (#133088)
- 
- * Mon Nov 15 2004 Bill Nottingham <notting@redhat.com>
- - fix check_link_down to still check negotiation if link is
-   listed as "up" on entering (#110164, <dbaron@dbaron.org>)
- 
- * Thu Nov 11 2004 Karsten Hopp <karsten@redhat.de> 7.96-1 
- - parse OPTIONS for QETH, CTC, LCS interfaces (#136256, mainframe)
- 
- * Tue Nov  9 2004 Bill Nottingham <notting@redhat.com>
- - fix typo (#134787, <bnocera@redhat.com>)
- 
- * Sun Nov  7 2004 Bill Nottingham <notting@redhat.com> 7.95-1
- - various translation updates
- 
- * Tue Nov  2 2004 Bill Nottingham <notting@redhat.com>
- - take an axe to rc.sysinit:
-   - remove delay on unclean startup
-   - remove hdparm code
-   - remove LVM1 code
-   - remove old raidtab code in favor of mdadm
-   - remove support for old isapnp tools
-   - move all block device mangling before fsck. run fsck *once*, not twice
-   - some more LC_ALL=C stuff
- 
- * Sun Oct 31 2004 Florian La Roche <laroche@redhat.com>
- - /etc/rc.d/rc: use "LC_ALL=C grep" for small speedup
- - /etc/rc.d/rc.sysinit:
- 	- do not read udev.conf, this seems to be all in the start_udev script
- 	- fix detection of "nomodules" kernel command line option
- 	- read /proc/cmdline earlier and convert rhgb to use that, too
- 	- load_module(): change redirection to /dev/null
- 	- some checks for RHGB_STARTED="" looked strange
- - /etc/sysconfig/network-scripts/ifup-ppp:
- 	- remove a call to basename with shell builtins
- - /etc/sysconfig/network-scripts/network-functions:
- 	- remove some calls to basename/sed with shell builtins

* Sat Mar 05 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.93.2-7m)
- add %%config(noreplace) at /etc/rc.local
- add %%config(noreplace) at /etc/rc.sysinit

* Sat Feb 26 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (7.93.2-6m)
- create symlinks of /etc/init.d/momonga to /etc/rc?d/
- added newt-devel, kudzu-devel to BuildPreReq

* Sat Feb 19 2005 minakami <minakami@momonga-linux.org>
- (7.93.2-5m)
- coLinux support

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.93.2-4m)
- add initscripts-7.93.2-Asuna.patch

* Fri Jan 28 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (7.93.2-3m)
  not hold rc?.d

* Wed Jan 26 2005 Kazuhiko <kazuhiko@fdiary.net>
- (7.93.2-2m)
- /etc/rc.d/ -> /etc/
- add /etc/init.d/momonga

* Wed Jan 26 2005 Toru Hoshina <t@momonga-linux.org>
- (7.93.2-1m)
- import from FC3.

* Thu Jan 20 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.55.1-11m)
- usbdevfs -> usbfs in rc.sysinit.

* Wed Oct 27 2004 Bill Nottingham <notting@redhat.com> 7.93.2-1
- fix prefdm fallback to installed display managers (#137274)
- fix incorrect rhgb temporary path (#137391)

* Mon Oct 18 2004 Bill Nottingham <notting@redhat.com> 7.93-1
- translation updates
- fix handling of GATEWAYDEV (#133575, <pekkas@netcore.fi>)

* Sun Oct 17 2004 Bill Nottingham <notting@redhat.com> 7.91-1
- rc.d/rc.sysinit: remove devlabel call
- mdadm support, now that raidtools is gone (#126636, #88785)
- call ipv6to4 scripts in /etc/ppp/(ip-up|ip-down) (#124390, <dwmw2@redhat.com>)
- cleanup a couple of nits that could affect bug #134754
- make sure we return to rhgb after fsck (#133966, #112839, #134449)
- automatically reboot when fsck calls for it, instead of requiring
  manual intervention (#117641 and duplicates)
- ifup-wireless: fix key for open vs. restricted (#135235, <dax@gurulabs.com>)
- translation updates

* Fri Oct 08 2004 Karsten Hopp <karsten@redhat.de> 7.90-1 
- fix portname for LCS devices

* Fri Oct 08 2004 Bill Nottingham <notting@redhat.com>
- remove sysconfig/rawdevices, as initscript is removed

* Thu Oct 07 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- change /etc/sysctl.conf to not allow source routed packets per default

* Fri Oct  6 2004 Bill Nottingham <notting@redhat.com> - 7.88-1
- fix requires

* Tue Oct  5 2004 Dan Walsh <dwalsh@redhat.com> - 7.87-1
- Change SELinux relabel to not remount / 

* Mon Oct  4 2004 Bill Nottingham <notting@redhat.com>
- use runuser instead of su; require it
- init.d/halt: use right file name for random seed (#134432)

* Fri Oct  1 2004 Bill Nottingham <notting@redhat.com> - 7.86-1
- use /etc/hotplug/blacklist to blacklist modules in hardware init (#132719)
- filter indic locales on the console (#134198)

* Wed Sep 29 2004 Bill Nottingham <notting@redhat.com> - 7.85-1
- ifup, network-functions: fix worked-by-accident shell quoting
- lang.csh: remove setting of dspmbyte (#89549, <mitr@redhat.com>)
- SELinux fixes
- clean up prefdm
- init.d/functions: export LC_MESSAGES (#133786)
- allow daemon to coredump if requested (#130175)
- network-functions: be more liberal in what we accept for link types (#90602, #127984)
- fix overzealousness with -range aliases (#65415)
- rc.sysinit: use s-c-keyboard, not kbdconfig (#133929)
- fix checkpid logic, clean up potential errors (#134030)
- translation updates

* Wed Sep 22 2004 Bill Nottingham <notting@redhat.com> - 7.84-1
- only start udev once

* Wed Sep 22 2004 Jeremy Katz <katzj@redhat.com> - 7.83-1
- conflict with old udev
- use udev if it's present

* Tue Sep 21 2004 Bill Nottingham <notting@redhat.com>
- don't mount usbfs without usb. also, at least be consistent in filesystem type

* Fri Sep 17 2004 Bill Nottingham <notting@redhat.com> - 7.82-1
- fix handling of nonexistent devices (#132839)
- rhgb enhancements (<veillard@redhat.com>, #132665)
- initscripts.spec: require nash (#132513)
- translation updates

* Tue Sep 14 2004 Karsten Hopp <karsten@redhat.de> 7.81-1 
- load iucv device config after /etc/sysconfig/network so that
  GATEWAY doesn't get overwritten

* Fri Sep 10 2004 Bill Nottingham <notting@redhat.com> - 7.80-1
- fix IPv6 6to4 & NAT (#118928, <pb@bieringer.de>, <pekkas@netcore.fi>)

* Fri Sep 10 2004 Karsten Hopp <karsten@redhat.com> - 7.79-1
- load ctc device config after /etc/sysconfig/network so that
  GATEWAY doesn't get overwritten

* Wed Sep  8 2004 Dan Walsh <dwalshg@redhat.com> - 7.78-2
- fix setting SELinux contexts on udev-created-in-initrd devices
- Let restorecon check if selinux is enabled.

* Wed Sep  8 2004 Bill Nottingham <notting@redhat.com> - 7.78-1
- set SELinux contexts on udev-created-in-initrd devices, if necessary

* Wed Sep  1 2004 Bill Nottingham <notting@redhat.com> - 7.77-1
- mount usbfs (#131347)
- start any automatic raid devices
- remove triggers for ancient releases, bulletproof remaining ones (#131356)

* Wed Sep  1 2004 Jeremy Katz <katzj@redhat.com> - 7.76-1
- udev uses UDEV_TMPFS now 

* Wed Sep 01 2004 Karsten Hopp <karsten@redhat.de> 7.75-1 
- fix sysfs configuration of qeth and lcs network interfaces
  (eth, tr, hsi)

* Mon Aug 30 2004 Karsten Hopp <karsten@redhat.de> 7.74-1 
- fix support for LCS portnumbers (mainframe)

* Fri Aug 27 2004 Jason Vas Dias  <jvdias@redhat.com> 7.73-1
- Add support for running the DHCPv6 client to ifup 
- (new DHCPV6C=yes/no ifcfg-${IF} variable) + update sysconfig.txt 

* Fri Aug 27 2004 Bill Nottingham <notting@redhat.com> 7.72-1
- flip the kernel conflict to a Requires:

* Thu Aug 26 2004 Karsten Hopp <karsten@redhat.de> 7.71-1
- ifcfg-iucv/ctc: drop REMIP and use GATEWAY instead

* Thu Aug 26 2004 Bill Nottingham <notting@redhat.com> 7.70-1
- autoload hardware modules on startup
- minor fsck cleanup (#115028, <leonard-rh-bugzilla@den.ottolander.nl>)
- ifup: support STP bridging (#123324)
- rc.sysinit: do a SELinux relabel if forced
- rc.sysinit: remove devfs compat and the remaining 2.4 compat
- ifup-wireless: support multiple keys (#127957)
- fix firmware loading (#129155, <bnocera@redhat.com>)
	
* Tue Aug 24 2004 Karsten Hopp <karsten@redhat.de> 7.68-1 
- execute zfcfconf.sh if available (mainframe)

* Mon Aug 20 2004 Jason Vas Dias <jvdias@redhat.com> 7.67-1
- fix change_resolv_conf: if pre-existing /etc/resolv.conf
- non-existent or empty, replace with new file contents.

* Fri Aug 20 2004 Jason Vas Dias <jvdias@redhat.com> 7.66-1
- Allow users to use generic /etc/dhclient.conf if per-device
- /etc/dhclient-${DEVICE}.conf is non-existent or empty

* Fri Aug 20 2004 Jason Vas Dias <jvdias@redhat.com> 7.66-1
- Preserve "options" settings in resolv.conf (bug 125712)

* Fri Aug 20 2004 Jeremy Katz <katzj@redhat.com> - 7.65-1
- look at /etc/udev/udev.conf, not /etc/sysconfig/udev (#130431)

* Fri Aug 20 2004 Bill Nottingham <notting@redhat.com> 7.64-1
- rc.d/rc.sysinit: check for dev file too (#130350)
	
* Thu Aug 19 2004 Than Ngo <than@redhat.com> 7.63-1
- allow CBCP with own number (#125710)

* Thu Aug 19 2004 Bill Nottingham <notting@redhat.com> 7.62-1
- fix up resolv.conf munging (#129921)
- use rngd if available
- run start_udev if necessary (#120605)
- readonly root updates (#129893, <markmc@redhat.com>)
- ifup-wireless: quote key (#129930)
- remove rawdevices (#130048)
- handle binfmt_misc in rc.sysinit for the case where it's built in (#129954)
- remove mkkerneldoth
- don't remove linguas in lang.* (part of #9733)
- fix nfs unmounting (#129765)
- fix URL (#129433)

* Tue Aug 11 2004 Jason Vas Dias <jvdias@redhat.com> 7.61-1
- fix for bug 120093: add PERSISTENT_DHCLIENT option to ifcfg files

* Tue Aug  3 2004 Karsten Hopp <karsten@redhat.de> 7.60-1 
- write peerid into sysfs for IUCV devices (mainframe)

* Tue Aug  3 2004 Bill Nottingham <notting@redhat.com>
- don't remove /dev/mapper/control - nash will do it if it has to (#127115)

* Fri Jul 30 2004 Jason Vas Dias <jvdias@redhat.com> 7.60-1
- fix for bug 125712: add 'change_resolv.conf' function

* Tue Jul 27 2004 Bill Nottingham <notting@redhat.com>
- rc.d/init.d/network: don't bring interfaces down twice (#127487)

* Wed Jul 14 2004 Bill Nottingham <notting@redhat.com>
- fix bonding + no IP (#127285)
- wrap second LVM initialization in vgscan check to avoid extraneous messages (#127639)

* Wed Jul  7 2004 Bill Nottingham <notting@redhat.com>
- move random stuff to rc.sysinit/halt; move all swap to after this.
  prereq of bug #123278

* Fri Jul  2 2004 Bill Nottingham <notting@redhat.com> 7.59-1
- set context on ICE directory after making it (#127099, <concert@europe.com>)
- don't mount GFS filesystems in rc.sysinit

* Tue Jun 29 2004 Bill Nottingham <notting@redhat.com> 7.58-1
- rc.d/rc.sysinit: hack: make ICE directory on boot (#86480)
- set devicetype for xDSL (#126194)
- ignore locking failures when starting lvm volumes (#126192, <radu@primit.ro>)
- unset LC_MESSAGES for rhgb (#126020, <ynakai@redhat.com>)
- bonding fixes
- setsysfont: remove error (#100559)
- remove duplicate setting of network routes (#125450)
- vlan fixes (#107504, <hrunting@texas.net>)
- ifup-aliases: remove bogus route setting (#120908)

* Tue May 25 2004 Bill Nottingham <notting@redhat.com> 7.57-1
- readonly root fixes (<alexl@redhat.com>)

* Tue May 25 2004 Karsten Hopp <karsten@redhat.de> 7.56-1 
- special TYPE for qeth devices to differenciate them from ethX

* Mon May 24 2004 Bill Nottingham <notting@redhat.com>
- fix pppd vs. ppp typo in conflicts (#123680)

* Fri May 21 2004 Bill Nottingham <notting@redhat.com>
- fix bridging confusing module order (#122848, <luto@myrealbox.com>)
- rc.d/rc.sysinit: don't mount cifs (#122501)

* Tue May 18 2004 Karsten Hopp <karsten@redhat.de> 7.55-1 
- add support for ccwgroup devices on mainframe

* Thu May 13 2004 Than Ngo <than@redhat.com> 7.54-1
- add patch to enable PIE build of usernetctl

* Fri May  7 2004 Jeremy Katz <katzj@redhat.com> - 7.53-1
- little lvm tweak (#121963)

* Tue May  4 2004 Bill Nottingham <notting@redhat.com> 7.52-1
- ipv4 addresses are ints, not longs (#122479)

* Tue May  4 2004 Bill Nottingham <notting@redhat.com> 7.51-1
- get rid of LVM error when no volumes are defined (#121197)
- fix selinux short-circuit test (#121143, <michal@harddata.com>)
- /dev/mapper/control is a special file, check it accordingly (#121963)
- support ETHTOOL_OPTS on bonding slaves (#119430, <hrunting@texas.net>)
- handle multiple spaces correctly in rc.sysinit, network-functions
  (#118583, <pallas@kadan.cz>)
- cleanup fd leaks, mem leaks, other bogosities
  (#119987, <linux_4ever@yahoo.com>)
- rc.d/init.d/network: remove ipv6 bogosity (#114128)
- translation updates

* Fri Apr 16 2004 Bill Nottingham <notting@redhat.com> 7.50-1
- fix LVM issues in rc.sysinit (#120458, #119975)
- deal with fixed racoon parser
- translation updates from translators
- fix USB loading (#120911)

* Fri Mar 26 2004 Bill Nottingham <notting@redhat.com> 7.49-1
- use alsa for mixer saving in halt
- don't umount /proc in halt (#118880)
- various translation updates from translators

* Wed Mar 17 2004 Bill Nottingham <notting@redhat.com> 7.48-1
- disable enforcing in emergency mode for now, relabel some commonly
  mislabeled files on boot

* Wed Mar 17 2004 Bill Nottingham <notting@redhat.com> 7.47-1
- translation: catch more input strings (#106285, <mitr@volny.cz>)
- remove autologin from prefdm (#108969)
- return to rhgb after ./unconfigured (#109807, <jkeating@j2solutions.net>)
- handle iso15 in setsysfont (#110243)
- clean up samba & vmware in rc.sysinit (#113104)
- some sysconfig.txt documentation (#110427, #118063)
- fix bug in umount-on-halt (#113088, <giardina@airlab.elet.polimi.it>)
- handle CIFS in netfs (#115691)
- make sure hotplug isn't stuck unset (#116666, <aoliva@redhat.com>)
- handle network fs better in rc.sysinit (#111290)
- nomodules applies to usb/firewire too (#113278)
- ipsec fix (#116922, <felipe_alfaro@linuxmail.org>)
- make sure rc exits cleanly (#117827, <enrico.scholz@informatik.tu-chemnitz.de>)
- fsck root FS from initrd, for dynamic majors (#117575, <sct@redhat.com>)

* Mon Feb 23 2004 Tim Waugh <twaugh@redhat.com>
- Use ':' instead of '.' as separator for chown.

* Mon Feb  2 2004 Bill Nottingham <notting@redhat.com> 7.46-1
- some more rc.sysinit tweaks and refactoring

* Fri Jan 30 2004 Bill Nottingham <notting@redhat.com> 7.45-1
- fix rc.sysinit typo
- rc.d/init.d/network: clear out environment (#113937, #111584)

* Wed Jan 28 2004 Bill Nottingham <notting@redhat.com> 7.44-1
- NFSv4 support (<chucklever@bigfoot.com>, <steved@redhat.com>)
- handle 2.6-style 'install ethX ...' lines correctly
- mount sysfs by default
- time to clean up the cruft. remove:
  - boot-time depmod
  - linking of /boot/System.map to /boot/System.map-`uname -r`
  - /var/log/ksyms.X
  - libredhat-kernel support

* Fri Jan  16 2004 Dan Walsh <dwalsh@redhat.com> 7.43-2
- Remove selinux run_init code from service script.  It is no longer needed.

* Fri Dec  5 2003 Jeremy Katz <katzj@redhat.com> 7.43-1
- basic lvm2 support

* Tue Oct 28 2003 Bill Nottingham <notting@redhat.com> 7.42-1
- show rhgb details on service failures

* Wed Oct 22 2003 Bill Nottingham <notting@redhat.com> 7.41-1
- tweak some rhgb interactions (#100894, #107725)
- fix dvorak keymap loading (#106854)

* Wed Oct 22 2003 Than Ngo <than@redhat.com> 7.40-1
- add better fix to support nickname (#105785)

* Wed Oct 22 2003 Than Ngo <than@redhat.com> 7.39-1
- add support nickname (#105785)

* Fri Oct 17 2003 Bill Nottingham <notting@redhat.com> 7.38-1
- rhgb updates, now pass 'rhgb' to use it, instead of passing 'nogui'
  to disable it

* Fri Oct 10 2003 Bill Nottingham <notting@redhat.com> 7.37-1
- bridging updates (#104421, <dwmw2@redhat.com>)

* Wed Oct  8 2003 Bill Nottingham <notting@redhat.com> 7.36-1
- mount /dev/pts before starting rhgb

* Wed Oct  1 2003 Bill Nottingham <notting@redhat.com> 7.35-1
- load acpi modules on startup if necessary
- fix typo in ipsec comments & sysconfig.txt

* Mon Sep 15 2003 Than Ngo <than@redhat.com> 7.34-1
- use upsdrvctl to start the shutdown process

* Mon Sep 15 2003 Bill Nottingham <notting@redhat.com> 7.33-1
- ipsec fixes (#104227, <harald@redhat.com>)
- ppp fixes (#104128, #97845, #85447)

* Thu Sep 11 2003 Bill Nottingham <notting@redhat.com> 7.32-1
- fix ip calls for some device names (#104187)
- ipsec fixes

* Fri Sep  5 2003 Bill Nottingham <notting@redhat.com> 7.31-1
- fix bonding + dhcp (#91399)
- fix typo (#103781)
- sysconfig/network-scripts/ifup: fix use of local

- fix shutdown with NFS root (#100556, <Julian.Blake@cern.ch>)
- remove /var/run/confirm when done with /etc/rc (#100898)
- ipcalc: fix some memory handling (#85478, <miked@ed.ac.uk>)
- handle sorting > 10 network devices (#98209)
- unset ONPARENT after use (#101384)
- random other fixes
- bridging support (<dwmw2@redhat.com>)

* Fri Aug 15 2003 Bill Nottingham <notting@redhat.com> 7.30-1
- IPv6 updates (#86210, #91375, <pekkas@netcore.fi>)

* Fri Aug  8 2003 Bill Nottingham <notting@redhat.com> 7.29-1
- setsysfont: don't echo to /dev/console (#102004)
- fix ethernet device renaming deadlock (#101566)
- consoletype: don't return 'vt' on vioconsole (#90465)
- ifup: fix short-circuit (#101445)

* Fri Jul 18 2003 Nalin Dahyabhai <nalin@redhat.com>
- ifup-routes: pass the interface name to handle_file() so that we don't try
  to use the routes file's name as an interface name

* Wed Jul  9 2003 Bill Nottingham <notting@redhat.com> 7.28-1
- switch from $CONFIG.keys to keys-$CONFIG

* Tue Jul  8 2003 Bill Nottingham <notting@redhat.com> 7.27-1
- add a check to consoletype for the current foreground console
- use it when running unicode_start (#98753)

* Wed Jul  2 2003 Bill Nottingham <notting@redhat.com> 7.26-1
- ipsec support (see sysconfig.txt, ifup-ipsec)
- read $CONFIG.keys, for non-world-readable keys
- allow default window size for routes to be set with WINDOW= (#98112)
- support setting device options with ethtool opts
- fix s390 bootup spew (#98078)
- support renaming interfaces with nameif based on hwaddr

* Mon Jun 23 2003 Bill Nottingham <notting@redhat.com> 7.25-1
- fix DNS punching in the case of other rules for the DNS server
  (#97686, <martin@zepler.org>)
- initlog, ppp-watch, and usernetctl tweaks (<linux_4ever@yahoo.com>)
- fix grep for mingetty (#97188)
- fix rhgb-client bad syntax
- change network device searching, use correct naming, fix route issues
  (<harald@redhat.com>)
- other random tweaks

* Fri May 23 2003 Bill Nottingham <notting@redhat.com> 7.24-1
- now even still yet more tweaks for graphical boot

* Thu May 22 2003 Bill Nottingham <notting@redhat.com> 7.23-1
- even still yet more tweaks for graphical boot

* Tue May 20 2003 Bill Nottingham <notting@redhat.com> 7.22-1
- still yet more tweaks for graphical boot

* Tue May 20 2003 Bill Nottingham <notting@redhat.com> 7.21-1
- yet more tweaks for graphical boot

* Fri May  2 2003 Bill Nottingham <notting@redhat.com> 7.20-1
- more tweaks for graphical boot

* Wed Apr 30 2003 Bill Nottingham <notting@redhat.com> 7.18-1
- some tweaks for graphical boot

* Mon Apr 21 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- initscripts-s390.patch: remove not needed parts about PNP=
- inittab.390: sync with normal version
- rc.sysinit: remove two further calls to /sbin/consoletype with $CONSOLETYPE

* Fri Apr 18 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- sysconfig/init.s390: set LOGLEVEL=3 as for other archs
- rc.d/init.d/network, rc.d/rc: change confirmation mode to
  not use an environment variable
- rc.d/init.d/functions: make strstr() even shorter, remove old
  "case" version that has been already commented out
- rc.d/rc.sysinit:
	- no need to set NETWORKING=no, it is not used/exported
	- do not export BOOTUP
	- delete two "sleep 1" calls that wants to add time to go
	  into confirmation mode. There is enough time to press a
	  key anyway or use "confirm" in /proc/cmdline.
	- read /proc/cmdline into a variable
	- use strstr() to search in /proc/cmdline
	- add "forcefsck" as possible option in /proc/cmdline
	- while removing lock files, no need to call `basename`
	- add unamer=`uname -r` and reduce number of forks
	- do not fork new bash to create /var/log/ksyms.0

* Thu Apr 03 2003 Karsten Hopp <karsten@redhat.de> 7.15-1
- Mainframe has no /dev/ttyX devices and no mingetty, don't 
  initialize them. This gave error messages during startup

* Mon Mar 17 2003 Nalin Dahyabhai <nalin@redhat.com>
- init.d/network: don't advertise "probe: true" in the header if we don't
  recognize "probe" as an argument

* Wed Mar 12 2003 Bill Nottingham <notting@redhat.com> 7.14-1
* - do not handle changed chain name; change was reverted

* Tue Feb 25 2003 Bill Nottingham <notting@redhat.com> 7.13-1
- handle 7.x SYSFONTACM settings in setsysfont (#84183)

* Mon Feb 24 2003 Bill Nottingham <notting@redhat.com> 7.12-1
- handle changed chain name
- init vts used in all cases

* Fri Feb 21 2003 Bill Nottingham <notting@redhat.com> 7.10-1
- handle LANGUAGE specially for zh_CN.GB18030 and gdm (#84773)

* Thu Feb 20 2003 Bill Nottingham <notting@redhat.com> 7.09-1
- initialize two ttys past # of mingettys (for GDM)
- fix zeroconf route
- redhat-config-network writes $NAME.route for some static routes
  (e.g., ppp); handle that (#84193)

* Tue Feb 18 2003 Bill Nottingham <notting@redhat.com> 7.08-1
- load keybdev & mousedev even if hid is already loaded/static
- run fewer scripts through action (#49670, #75279, #81531)

* Mon Feb 10 2003 Bill Nottingham <notting@redhat.com> 7.07-1
- fix nicknames & profiles (#82246)
- fix check_device_down (#83780, <pzb@datstacks.com>)
- vlan fixes (<tis@foobar.fi>)
- fix groff macros (#83531, <tsekine@sdri.co.jp>)
- various updated translations
- fix checkpid for multiple pids (#83401)

* Fri Jan 31 2003 Bill Nottingham <notting@redhat.com> 7.06-1
- 802.1Q VLAN support (<tis@foobar.fi>, #82593)
- update translations

* Thu Jan 30 2003 Bill Nottingham <notting@redhat.com> 7.05-1
- fix syntax error in rc.sysinit when there are fsck errors
- fix zh_TW display on console (#82235)

* Wed Jan 15 2003 Bill Nottingham <notting@redhat.com> 7.04-1
- tweak some translatable strings
- fix for rc.sysinit on machines that pass arguments to mingetty
  (<nalin@redhat.com>)

* Tue Jan 14 2003 Bill Nottingham <notting@redhat.com> 7.03-1
- move system font setting sooner (<milan.kerslager@pslib.cz>)
- fix link checking for dhcp, use both ethtool and mii-tool
- fix CJK text on the console, and locale-archive held open
  on shutdown
- IPv6 updates <pekkas@netcore.fi>, <pb@bieringer.de>
- speedup tweaks (<drepper@redhat.com>)
- use glib2 for ppp-watch (#78690, <kisch@mindless.com>)
- add zeroconf route (#81738)
- fix ifup-ppp for dial-on-demand, and onboot (<goeran@uddeborg.pp.se>)
- tweak raidtab parsing, don't worry about not-in-fstab RAID devices
  (#71087, #78467, <aja@mit.edu>)
- don't automatically bring up aliases if 'ONPARENT=no' is set (#78992)
- getkey cleanups/tweaks (#76071, <ben@enchantedforest.org>)
- rework halt_get_remaining (#76831, <michal@harddata.com>)
- ipcalc: fix calculation of /32 addresses (#76646)
- various other tweaks and fixes

* Fri Dec 20 2002 Bill Nottingham <notting@redhat.com> 7.01-1
- %%config(noreplace) inittab

* Tue Dec 17 2002 Nalin Dahyabhai <nalin@redhat.com>
- add a "nofirewire" option to /etc/rc.sysinit, analogous to "nousb"

* Tue Dec 17 2002 Bill Nottingham <notting@redhat.com> 7.00-1
- tweaks for potential GUI bootup
- loop checking for network link state, don't unilterally wait five
  seconds

* Fri Dec 14 2002 Karsten Hopp <karsten@redhat.de> 6.99-1
- remove call to /sbin/update for S/390, too

* Wed Dec 11 2002 Bill Nottingham <notting@redhat.com> 6.98-1
- remove call to /sbin/update
- fix netprofile

* Mon Dec  2 2002 Bill Nottingham <notting@redhat.com> 6.97-1
- IPv6 update (<pekkas@netcore.fi>, <pb@bieringer.de>)
- devlabel support (<Gary_Lerhaupt@Dell.com>)
- do lazy NFS umounts

* Tue Nov 19 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- correctly remove non-packaged files for mainframe

* Tue Nov 12 2002 Bill Nottingham <notting@redhat.com> 6.96-1
- fix various static-routes brokeness (#74317, #74318, #74320, #76619,
  #75604)
- fix handling of SYSFONTACM in setsysfont (#75662)
- fix lang.csh for CJK (#76908, <ynakai@redhat.com>)
- IPv6 update (<pekkas@netcore.fi>, <pb@bieringer.de>)
- other minor tweaks

* Mon Sep 16 2002 Than Ngo <than@redhat.com>
- owns directory /etc/ppp/peers (bug #74037)

* Wed Sep  4 2002 Bill Nottingham <notting@redhat.com> 6.95-1
- fix syntax error in duplicate route removal section of ifup

* Wed Sep  4 2002 Nalin Dahyabhai <nalin@redhat.com> 6.94-1
- fix syntax error calling unicode_start when SYSFONTACM isn't set

* Mon Sep  2 2002 Bill Nottingham <notting@redhat.com>
- fix calling of unicode_start in lang.{sh,csh}
- ipv6 tweak

* Wed Aug 28 2002 Bill Nottingham <notting@redhat.com>
- don't infinite loop on ifdown
- remove disabling of DMA; this can cause problems
- move swap startup to after LVM (#66588)

* Tue Aug 20 2002 Bill Nottingham <notting@redhat.com>
- don't cycle through eth0-eth9 on dhcp link check (#68127)
- don't retry indefinitely on ppp startup
- activate network profile passed on kernel commandline via netprofile=
- fix iptables invocations again
- translation refresh

* Wed Aug 14 2002 Bill Nottingham <notting@redhat.com>
- fix silly typo in rc.sysinit
- increase timeout for link to 5 seconds (#70545)

* Tue Aug 13 2002 Bill Nottingham <notting@redhat.com>
- require /etc/redhat-release (#68903)
- fix tty2-tty6 (sort of)
- fix iptables invocations (#70807, #71201, #68368)
- other minor tweaks

* Wed Jul 24 2002 Bill Nottingham <notting@redhat.com>
- fix unicode checks in rc.sysinit, lang.{sh,csh} to handle UTF-8@euro

* Tue Jul 16 2002 Bill Nottingham <notting@redhat.com>
- use iptables, not ipchains

* Tue Jul 16 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- /sbin/service: set PATH before calling startup scripts
  HOME and TERM are also set during bootup, but they should not make
  a difference for well-written daemons.

* Mon Jul 15 2002 Bill Nottingham <notting@redhat.com>
- fix boot-time cleanup of /var
- update po files

* Thu Jul 11 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- /etc/init.d/functions:
	daemon(): avoid starting another bash
	killproc(): avoid starting another bash for the default case
- do not call "insmod -p" before loading the "st" module

* Tue Jul 09 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- allow an option for ups poweroff  #68123
- change grep for ONBOOT=  #63903
- allow building with a cross-compiler  #64362,#64255
- faster check in network-functions:check_default_route()
- better checks for backup files
- drastically reduce the number of consoletype invocations
- do not export "GATEWAY" in network-functions
- code cleanups in rc.sysinit

* Fri Jul 05 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- rc.sysinit: do not load raid modules unless /etc/raidtab exists
- many cleanups for more consistent shell programming and also
  many smaller speedups within network scripts, no un-necessary sourcing
  of files etc
- nearly re-code /etc/rc.d/rc

* Thu Jun 27 2002 Bill Nottingham <notting@redhat.com>
- a couple minor unicode tweaks in rc.sysinit

* Wed Jun 26 2002 Bill Nottingham <notting@redhat.com>
- move /proc/bus/usb mount, in case USB is in the initrd

* Wed Jun 26 2002 Preston Brown <pbrown@redhat.com>
- don't try to set wireless freq/channel when in Managed mode

* Wed Jun 26 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- start some sh coding cleanups
- change to /etc/init.d/functions
- eliminate some un-necessary PATH settings
- eliminate some TEXTDOMAIN settings

* Wed Jun 12 2002 Bill Nottingham <notting@redhat.com> 6.78-1
- fix UTF-8 checks

* Wed Jun 05 2002 Than Ngo <than@redhat.com> 6.77-1
- fixed a bug in setting defaultgateway

* Thu May 30 2002 Bill Nottingham <notting@redhat.com> 6.76-1
- call unicode_start in lang.{sh,csh}, setsysfont when necessary

* Tue May 28 2002 Bill Nottingham <notting@redhat.com> 6.75-1
- add check for link for dhcp back in

* Fri Apr 19 2002 Bill Nottingham <notting@redhat.com> 6.67-1
- fix silly cut&paste bug in hdparm settings in initscripts

* Mon Apr 15 2002 Trond Eivind Glomsrod <teg@redhat.com> 6.65-1
- Update translations

* Sun Apr 14 2002 Bill Nottingham <notting@redhat.com> 6.64-1
- make sure chatdbg is set before using it (#63448, <Bertil@Askelid.com>)
- allow tweaking of more devices with hdparm (#53511), and
  tweak non-disk devices iff they explicitly have a config file
  for that device (#56575, #63415)
- some translation updates

* Fri Apr 12 2002 Bill Nottingham <notting@redhat.com> 6.63-1
- ipcalc cleanups (#58410)
- quit stripping binaries
- do LVM init after RAID init too (#63238)
- export all locale variables (#56142)
- run sysctl -p after network init as well

* Tue Apr 09 2002 Bill Nottingham <notting@redhat.com> 6.62-1
- delete X/VNC locks on startup (#63035)
- shut up DMA disabling, move it to after ide-scsi (#62873, #62956)
- use full path to /sbin/ifconfig (#59457)
- /sbin/service: change to root directory before staring/stopping;
  also sanitize environment
		
* Tue Apr 02 2002 Bill Nottingham <notting@redhat.com> 6.61-1
- when disabling DMA, don't use things in /usr

* Thu Mar 28 2002 Bill Nottingham <notting@redhat.com> 6.60-1
- disable DMA on CD-ROMs at bootup

* Wed Mar 27 2002 Bill Nottingham <notting@redhat.com> 6.59-1
- add local hook to halt

* Fri Mar 15 2002 Than Ngo <than@redhat.com> 6.58-1
- fix usernetctl for working with neat

* Thu Mar 14 2002 Bill Nottingham <notting@redhat.com> 6.57-1
- update translations

* Tue Mar 12 2002 Bill Nottingham <notting@redhat.com> 6.56-1
- use nameif for interfaces where we don't agree on HWADDR with the
  config file (<harald@redhat.com>)
- LSB support tweaks

* Tue Mar 12 2002 Mike A. Harris  <mharris@redhat.com> 6.55-1
- Removed process accounting stuff from rc.sysinit and halt scripts as it is
  now handled by the psacct initscript in the psacct package

* Thu Feb 28 2002 Bill Nottingham <notting@redhat.com>
- conflict with older psacct

* Fri Feb 22 2002 Bill Nottingham <notting@redhat.com>
- fix invocation of need_hostname (#58946), a couple other minor tweaks

* Tue Feb 12 2002 Mike A. Harris  <mharris@redhat.com>
- rc.sysinit: changed /var/log/pacct to /var/account/pacct for FHS 2.2 compliance

* Wed Jan 30 2002 Bill Nottingham <notting@redhat.com>
- run /bin/setfont, not /usr/bin/setfont (kbd)
- lots-o-random bugfixes/tweaks (see ChangeLog)

* Thu Jan 17 2002 Michael K. Johnson <johnsonm@redhat.com>
- Added support for libredhat-kernel.so.* symlink handling

* Wed Nov  7 2001 Than Ngo <than@redhat.com>
- fix bug in setting netmask on s390/s390x (bug #55421)
  nmbd daemon works now ;-)

* Fri Nov  2 2001 Than Ngo <than@redhat.com>
- fixed typo bug ifup-ippp

* Mon Oct 29 2001 Than Ngo <than@redhat.com>
- fix bug in channel bundling if MSN is missed
- support DEBUG option
	
* Wed Sep 19 2001 Than Ngo <than@redhat.com>
- don't show user name by DSL connection

* Sat Sep  8 2001 Bill Nottingham <notting@redhat.com>
- don't run hwclock --adjust on a read-only filesystem

* Thu Sep  6 2001 Than Ngo <than@redhat.com>
* update initscripts-s390.patch for s390/s390x

* Wed Sep  5 2001 Bill Nottingham <notting@redhat.com>
- translation updates
- quota and hwclock tweaks (<pbrown@redhat.com>)

* Mon Sep  3 2001 Bill Nottingham <notting@redhat.com>
- fix severe alias problems (#52882)

* Mon Sep  3 2001 Than Ngo <than@redhat.com>
- don't start pppbind if encapsulation is rawip (bug #52491)

* Sun Sep  2 2001 Than Ngo <than@redhat.com>
- add ISDN patches from pekkas@netcore.fi and pb@bieringer.de (bug #52491)
- fix handling of ISDN LSZ Compresssion

* Thu Aug 30 2001 Than Ngo <than@redhat.com>
- po/de.po: fix typo bug, lo instead 1o

* Wed Aug 29 2001 David Sainty <dsainty@redhat.com>
- fix ifdown for multiple dhcpcd interfaces

* Wed Aug 29 2001 Than Ngo <than@redhat.com>
- fix ISDN dial on demand bug
- fix typo bug in network-functions

* Tue Aug 28 2001 Nalin Dahyabhai <nalin@redhat.com>
- document /etc/sysconfig/authconfig

* Tue Aug 28 2001 Bill Nottingham <notting@redhat.com> 6.31-1
- message un-tweaks (<johnsonm@redhat.com>)
- make getkey more useful, fix some of the autofsck stuff (<johnsonm@redhat.com>)

* Mon Aug 27 2001 Bill Nottingham <notting@redhat.com>
- autofsck support, archive modules/symbol info (<johnsonm@redhat.com>)

* Mon Aug 27 2001 Than Ngo <than@redhat.com>
- fix some typo bugs in ifup-ippp <ubeck@c3pdm.com>

* Fri Aug 24 2001 Bill Nottingham <notting@redhat.com>
- sort output of halt_get_remaining (#52180)
- fix bad translation (#52503)

* Wed Aug 22 2001 Bill Nottingham <notting@redhat.com>
- fix ifup-wireless (#52135)

* Wed Aug 22 2001 Than Ngo <than@redhat.com>
- fix return code of isdnctrl (bug #52225)

* Tue Aug 21 2001 Than Ngo <than@redhat.com>
- fix Bringing up isdn device again. It works now correct.

* Tue Aug 21 2001 Than Ngo <than@redhat.com>
- fix shutdown/Bringing up isdn device

* Mon Aug 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix syntax error in lang.csh
- set codeset by echoing to /dev/tty instead of /proc/self/fd/15

* Sun Aug 19 2001 Bill Nottingham <notting@redhat.com>
- fix a broken call to check_device_down
- make all loopback addresses have host scope, not global scope.
  Fixes #49374, possibly others

* Wed Aug 15 2001 Bill Nottingham <notting@redhat.com>
- add is_available() network function, use it; cleans up ugly modprobe
  error messages
- update translation info
- fix #51787

* Wed Aug 15 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- adjust s390 patch
- fix up ifup-ctc and mkkerneldoth.s390 (both are s390 specific)

* Mon Aug 13 2001 Yukihiro Nakai <ynakai@redhat.com>
- don't display Chinese Korean if we aren't on a pty

* Sat Aug 11 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- adjust s390 patches to current sources

* Fri Aug 10 2001 Bill Nottingham <notting@redhat.com>
- use GDM_LANG if it's set in lang.sh/lang.csh (#51432, <otaylor@redhat.com>)

* Fri Aug 10 2001 Than Ngo <than@redhat.com>
- don't set MSN if it' empty (it's now optional)
- don't give login name as a cmdline-option (Bug #23066)
- remove peer device file if ppp connection is down
- fix channel bundling

* Thu Aug  9 2001 Bill Nottingham <notting@redhat.com>
- require SysVinit (#51335)

* Wed Aug  8 2001 Bill Nottingham <notting@redhat.com>
- tweak raittab grep slightly (#51231)
- allow resetting of default route for DHCP addresses (#48994)
- save resolv.conf in ifup-ppp for restoration by ifdown-post (#50759)
- when munging firewall rules for dns, only allow dest ports 1025-65535 (#44038, #40833)
- allow shell characters in ppp names (#43719)
- allow setting DHCP arguments, just kill dhcpcd instead of using -k (#46492)
- behave sanely if ifup called when dhcpcd is running (#49392, #51038)

* Mon Aug  6 2001 Bill Nottingham <notting@redhat.com>
- honor HOTPLUG=no if running under hotplug (#47483)
- use awk, not grep, for modprobe -c checks (#49616)
- don't print ugly messages for the case where the device doesn't exist,
  and there is no alias (i.e., PCMCIA ONBOOT=yes (#various))
- run kbdconfig in /.unconfigured mode (#43941)
- use a bigger buffer size argument to dmesg (#44024)
- detach loopback devices on shutdown (#43919, #45826)

* Thu Aug  2 2001 Bill Nottingham <notting@redhat.com>
- fix halt_get_remaining() (#50720)

* Tue Jul 31 2001 Bill Nottingham <notting@redhat.com>
- mount all FS types r/o at halt (#50461)
- don't use mii-tool at all (#various)

* Thu Jul 26 2001 Bill Nottingham <notting@redhat.com>
- don't use kbd commands in setsysfont now that we've switched back to
  console-tools (#50075)
- sleep in check_link_down; some devices require it
- only bring link down if check_link_down fails

* Wed Jul 25 2001 Bill Nottingham <notting@redhat.com>
- set link up before checking with mii-tool (#49949)

* Tue Jul 24 2001 Bill Nottingham <notting@redhat.com>
- update netdev stuff to use _netdev
- IPv6 updates (<pekkas@netcore.fi>)
- fix downing of devices with static IPs (#49777, #49783)
- put ifcfg-lo back in the package

* Fri Jul 20 2001 Preston Brown <pbrown@redhat.com> 6.06
- updates for quota

* Tue Jul 17 2001 Bill Nottingham <notting@redhat.com>
- own some more directories
- use -O nonetdev, require mount package that understands this
- fix do_netreport when called as non-root
- remove ip addresses from interfaces on ifdown
- oops, fix ifup/ifdown

* Mon Jul 16 2001 Than Ngo <than@redhat.com>
- fix country_code for ISDN

* Tue Jul  9 2001 Bill Nottingham <notting@redhat.com>
- fix '--check'
- prereq sh-utils (#43065)
- fix some invocations of reboot/halt (#45966)
- fix typo in ifup-wireless
- don't muck with /etc/issue each boot
- big IPv6 update (<pekkas@netcore.fi>)

* Fri Jul  6 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add new directories required by new network tool

* Thu Jul 05 2001 Karsten Hopp <karsten@redhat.de>
- disable hwclock on S390 (no such executable)
- Fix up kernel versioning on binary-only modules (S390)
- don't use newt scripts on S390 console

* Sat Jul 01 2001 Trond Eivind Glomsrod <teg@redhat.com>
- reenable pump, but make sure dhcpcd is the default. This
  way, upgrades of systems without dhcpcd has a better chance at
  working.

* Thu Jun 28 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Disable pump completely

* Wed Jun 27 2001 Than Ngo <than@redhat.com>
- fix pap/chap authentication for syncppp
- support ippp options
	
* Mon Jun 25 2001 Bill Nottingham <notting@redhat.com>
- add ifup-wireless

* Fri Jun 22 2001 Than Ngo <than@redhat.com>
- add support xDSL

* Thu Jun 21 2001 Bill Nottingham <notting@redhat.com>
- more networking script fixes (#45364)
- add stuff for unmounting /initrd

* Thu Jun 21 2001 Than Ngo <than@redhat.com>
- add support ISDN

* Wed Jun 20 2001 Bill Nottingham <notting@redhat.com>
- fix extremely broken new network scripts

* Wed Jun 20 2001 Bill Nottingham <notting@redhat.com>
- bump version to 5.89
- make it build

* Thu May 17 2001 Bill Nottingham <notting@redhat.com>
- don't run ifup ppp0 if ppp-watch gets SIGINT (#40585, ak@cave.hop.stu.neva.ru)
- fix do_netreport (#37716, #39603 <crlf@aeiou.pt>)

* Wed May 16 2001 Nalin Dahyabhai <nalin@redhat.com>
- copyright: GPL -> license: GPL
- fix a syntax error in lang.csh
- skip commented-out i18n configuration lines in lang.csh

* Fri May 11 2001 Preston Brown <pbrown@redhat.com>
- new network-scripts infrastructure; ifcfg-lo moved to /etc/sysconfig/networking

* Wed May  2 2001 Bernhard Rosenkraenzer <bero@redhat.com> 5.86-1
- support kbd in setsysfont
- bzip2 source

* Wed Apr 25 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- add further s390 changes:
	- ifup-iucv
	- mkkerneldoth.s390

* Tue Apr 24 2001 Than Ngo <than@redhat.com>
- add shutdown UPS into halt (bug #34312)

* Sat Apr  7 2001 Preston Brown <pbrown@redhat.com>
- broke out kernel.h updater from rc.sysinit into /sbin/mkkerneldoth

* Fri Apr  6 2001 Bill Nottingham <notting@redhat.com>
- be a little more careful in do_netreport (#34933)

* Tue Apr  3 2001 Bill Nottingham <notting@redhat.com>
- set umask explicitly to 022 in /etc/init.d/functions

* Mon Apr  2 2001 Bill Nottingham <notting@redhat.com>
- fix segfault in usernetctl (#34353)

* Mon Mar 26 2001 Bill Nottingham <notting@redhat.com>
- don't print errors in /etc/init.d/network if kernel.hotplug doesn't exist

* Thu Mar 22 2001 Erik Troan <ewt@redhat.com>
- take advantage of new swapon behaviors

* Wed Mar 14 2001 Bill Nottingham <notting@redhat.com>
- add cipe interfaces last (#31597)

* Tue Mar 13 2001 Bill Nottingham <notting@redhat.com>
- fix typo in ifup (#31627)
- update translation source

* Tue Mar 13 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix typo in rc.sysinit
- fix ifup-routes not setting DEVICE properly

* Mon Mar 12 2001 Preston Brown <pbrown@redhat.com>
- Work properly with new quota utilities

* Mon Mar  5 2001 Bill Nottingham <notting@redhat.com>
- IPv6 fixes (#30506)
- make static-routes handling more sane and consistent (#29500, #29549)
- handle multiple USB controllers *correctly*

* Wed Feb 28 2001 Nalin Dahyabhai <nalin@redhat.com>
- usernetctl, ppp-watch: cleanups
- netreport: use O_NOFOLLOW
- ifup-ppp: let ppp-watch watch over demand-dialed connections (#28927)

* Tue Feb 27 2001 Bill Nottingham <notting@redhat.com>
- don't run isapnp on isapnp-enabled 2.4 kernels (part of #29450)
- disable hotplug during network initscript
- don't munge wireless keys in ifup; that will be done with the
  PCMCIA wireless stuff
- run sndconfig --mungepnp for non-native-isapnp soundcards
- don't explicitly kill things in init.d/single, init will do it
- don't explicitly load usb-storage; mount the usbdevfs before initializing
  host controller modules

* Wed Feb 21 2001 Bill Nottingham <notting@redhat.com>
- initialize multiple USB controllers if necessary

* Wed Feb 21 2001 Nalin Dahyabhai <nalin@redhat.com>
- close extra file descriptors before exec()ing commands in initlog

* Mon Feb 19 2001 Bill Nottingham <notting@redhat.com>
- fix some substitions in init.d/functions (fixes various killproc issues)
- make sure ipv6 module alias is available if configured
- fix initlog segfaults in popt when called with bogus stuff (#28140)

* Thu Feb 15 2001 Nalin Dahyabhai <nalin@redhat.com>
- make pidofproc() and killproc() try to use the PID associated with the full
  pathname first before killing the daemon by its basename (for daemons that
  share the same basename, i.e. "master" in postfix and cyrus-imapd) (#19016)
- fix status() as well

* Wed Feb 14 2001 Bill Nottingham <notting@redhat.com>
- fix init.d/single to work around possible kernel problem

* Tue Feb 13 2001 Bill Nottingham <notting@redhat.com>
- fix unmounting of loopback stuff (#26439, #14672)

* Mon Feb 12 2001 Bill Nottingham <notting@redhat.com>
- fix ifup-post so that it will work right when not called from ifup

* Sat Feb 10 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- add all save changes for s390 s390x that won't break anything
  patches are from Oliver Paukstadt @ millenux.com

* Fri Feb  9 2001 Bill Nottingham <notting@redhat.com>
- muck with the font in lang.csh/lang.sh, but don't spit out errors (#26903)

* Wed Feb  7 2001 Bill Nottingham <notting@redhat.com>
- ipv6 sync ups (#26502, #25775)
- fix hangs at shutdown (#25744)
- fix ifup-ppp (#26323)

* Tue Feb  6 2001 Bill Nottingham <notting@redhat.com>
- modify firewall on ifup to allow any new DNS servers through (#25951)
- don't muck with the font in lang.csh/lang.sh (#26349)
- don't display Japanese if we aren't on a pty (#25041)
- load ide-scsi if passed on /proc/cmdline

* Mon Feb  5 2001 Trond Eivind Glomsrod <teg@redhat.com>
- i18n updates

* Fri Feb  2 2001 Bill Nottingham <notting@redhat.com>
- actually *ship* the ipv6 (and plusb) files

* Thu Feb  1 2001 Trond Eivind Glomsrod <teg@redhat.com>
- i18n updates

* Tue Jan 30 2001 Bill Nottingham <notting@redhat.com>
- various init.d/functions cleanups (#10761, from <mjt@tls.msk.ru>)
- in daemon(), only look at pidfile to determine if process is running
  (#17244, others)
- ifup-ppp enhancements (#17388, from <ayn2@cornell.edu>)
- ipv6 support (#23576, originally by Peter Bieringer <pb@bieringer.de>)
- lots of other minor fixes (see ChangeLog)

* Mon Jan 29 2001 Bill Nottingham <notting@redhat.com>
- add plusb support (#18892, patch from <eric.ayers@compgen.com>)
- don't ignore RETRYTIMEOUT when we never connect (#14071, patch from
  <ak@cave.hop.stu.neva.ru>)

* Wed Jan 24 2001 Bill Nottingham <notting@redhat.com>
- quiet LVM setup (#24841)
- fix inability to shutdown cleanly (#24889)

* Tue Jan 23 2001 Bill Nottingham <notting@redhat.com>
- new i18n mechanism

* Tue Jan 23 2001 Matt Wilson <msw@redhat.com>
- fixed typo in init.d/network - missing | in pipeline

* Mon Jan 22 2001 Bill Nottingham <notting@redhat.com>
- do LVM setup through normal initscripts mechanisms
- ignore backup files in /etc/sysconfig/network-scripts
- lots of .po file updates

* Tue Jan  2 2001 Bill Nottingham <notting@redhat.com>
- initial i18n support - originally from Conectiva

* Mon Dec 11 2000 Bill Nottingham <notting@redhat.com>
- only load sound if persistent DMA buffers are necessary
- fix lots of bugs: #18619, #21187, #21283, #12097
- integrate MAXFAIL option for ppp-watch
- don't load keymaps/fonts on a serial console

* Tue Nov 21 2000 Karsten Hopp <karsten@redhat.de>
- changed hdparm section in rc.sysinit to allow different
  parameters for each disk (if needed) by copying 
  /etc/sysconfig/harddisks to /etc/sysconfig/harddiskhda (hdb,hdc..)
- fix RFE #20967

* Tue Oct 31 2000 Than Ngo <than@redhat.com>
- fix the adding default route if GATEWAY=0.0.0.0

* Tue Oct 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- handle "gw x.x.x.x" as the last pair of flags in ifup-routes (#18804)
- fix top-level makefile install target
- make usernetctl just fall-through if getuid() == 0

* Sun Sep  3 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- /etc/init.d is already provided by chkconfig

* Wed Aug 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- set "holdoff ${RETRYTIMEOUT} ktune" for demand-dialed PPP links

* Tue Aug 22 2000 Bill Nottingham <notting@redhat.com>
- update documentation (#15475)

* Tue Aug 22 2000 Than Ngo <than@redhat.de>
- add KDE2 support to prefdm

* Mon Aug 21 2000 Bill Nottingham <notting@redhat.com>
- add usleep after kill -KILL in pidofproc, works around lockd issues (#14847)
- add some fallback logic to prefdm (#16464)

* Fri Aug 18 2000 Bill Nottingham <notting@redhat.com>
- don't load usb drivers if they're compiled statically
- don't call ifdown-post twice for ppp (#15285)

* Wed Aug 16 2000 Bill Nottingham <notting@redhat.com>
- fix /boot/kernel.h generation (#16236, #16250)

* Tue Aug 15 2000 Nalin Dahyabhai <nalin@redhat.com>
- be more careful about creating files in netreport (#16164)

* Sat Aug 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- move documentation for the DEMAND and IDLETIMEOUT values to the right
  section of sysconfig.txt

* Wed Aug  9 2000 Bill Nottingham <notting@redhat.com>
- load agpgart if necessary (hack)
- fix /boot/kernel.h stuff (jakub)

* Mon Aug  7 2000 Bill Nottingham <notting@redhat.com>
- remove console-tools requirement
- in netfs, start portmap if needed
- cosmetic cleanups, minor tweaks
- don't probe USB controllers

* Mon Aug  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix demand-dialing support for PPP devices
- change updetach back to nodetach

* Sun Aug  6 2000 Bill Nottingham <notting@redhat.com>
- add RETRYCONNECT option for ifcfg-pppX files (kenn@linux.ie)

* Wed Jul 26 2000 Bill Nottingham <notting@redhat.com>
- fix unclean shutdown

* Tue Jul 25 2000 Nalin Dahyabhai <nalin@redhat.com>
- s/nill/null/g

* Tue Jul 25 2000 Bill Nottingham <notting@redhat.com>
- unmount usb filesystem on halt
- run /sbin/ifup-pre-local if it exists

* Tue Jul 18 2000 Trond Eivind Glomsrod <teg@redhat.com>
- add "nousb" command line parameter
- fix some warnings when mounting /proc/bus/usb

* Sat Jul 15 2000 Matt Wilson <msw@redhat.com>
- kill all the PreTransaction stuff
- directory ownership cleanups, add more LSB symlinks
- move all the stuff back in to /etc/rc.d/

* Thu Jul 13 2000 Bill Nottingham <notting@redhat.com>
- fix == tests in rc.sysinit
- more %pretrans tweaks

* Thu Jul 13 2000 Jeff Johnson <jbj@redhat.com>
- test if /etc/rc.d is a symlink already in pre-transaction syscalls.

* Tue Jul 11 2000 Bill Nottingham <notting@redhat.com>
- implement the %pre with RPM Magic(tm)

* Sat Jul  8 2000 Bill Nottingham <notting@redhat.com>
- fix it to not follow /etc/rc.d

* Fri Jul  7 2000 Bill Nottingham <notting@redhat.com>
- fix %pre, again

* Thu Jul  6 2000 Bill Nottingham <notting@redhat.com>
- tweak %pre back to a mv (rpm is fun!)
- do USB initialization before fsck, so keyboard works if it fails

* Mon Jul  3 2000 Bill Nottingham <notting@redhat.com>
- rebuild; allow 'fastboot' kernel command line option to skip fsck

* Mon Jul 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix demand-dialing with PPP

* Sun Jul 02 2000 Trond Eivind Glomsrod <teg@redhat.com>
- don't use tail

* Thu Jun 28 2000 Trond Eivind Glomsrod <teg@redhat.com>
- add support for USB controllers and HID devices 
  (mice, keyboards)

* Tue Jun 27 2000 Trond Eivind Glomsrod <teg@redhat.com>
- add support for EIDE optimization

* Mon Jun 26 2000 Bill Nottingham <notting@redhat.com>
- tweak %%pre

* Wed Jun 21 2000 Preston Brown <pbrown@redhat.com>
- noreplace for adjtime file

* Fri Jun 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- ifup-ppp: add hooks for demand-dialing PPP
- functions: use basename of process when looking for its PID file

* Thu Jun 15 2000 Bill Nottingham <notting@redhat.com>
- move from /etc/rc.d/init.d -> /etc/init.d

* Tue Jun 13 2000 Bill Nottingham <notting@redhat.com>
- set soft limit, not hard, in daemon function
- /var/shm -> /dev/shm

* Thu Jun 08 2000 Preston Brown <pbrown@redhat.com>
- use dhcpcd if pump fails.
- use depmod -A (faster)

* Sun Jun  4 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- add autologin support to prefdm

* Thu Jun  1 2000 Bill Nottingham <notting@redhat.com>
- random networking fixes (alias routes, others)
- conf.modules -> modules.conf

* Thu May 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix incorrect grep invocation in rc.sysinit (bug #11267)

* Wed Apr 19 2000 Bill Nottingham <notting@redhat.com>
- fix lang.csh, again (oops)
- use /poweroff, /halt to determine whether to poweroff

* Thu Apr 14 2000 Bill Nottingham <notting@redhat.com>
- fix testing of RESOLV_MODS (which shouldn't be used anyways)

* Tue Apr 04 2000 Ngo Than <than@redhat.de>
- fix overwrite problem of resolv.conf on ippp/ppp/slip connections

* Mon Apr  3 2000 Bill Nottingham <notting@redhat.com>
- fix typo in functions file
- explicitly set --localtime when calling hwclock if necessary

* Fri Mar 31 2000 Bill Nottingham <notting@redhat.com>
- fix typo in /etc/rc.d/init.d/network that broke linuxconf (#10472)

* Mon Mar 27 2000 Bill Nottingham <notting@redhat.com>
- remove compatiblity chkconfig links
- run 'netfs stop' on 'network stop' if necessary

* Tue Mar 21 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Mount /var/shm if required (2.3.99, 2.4)

* Mon Mar 20 2000 Bill Nottingham <notting@redhat.com>
- don't create resolv.conf 0600
- don't run ps as much (speed issues)
- allow setting of MTU
- other minor fixes

* Sun Mar 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Start devfsd if installed and needed (Kernel 2.4...)

* Wed Mar  8 2000 Bill Nottingham <notting@redhat.com>
- check that network devices are up before bringing them down

* Wed Mar  8 2000 Jakub Jelinek <jakub@redhat.com>
- update sysconfig.txt

* Tue Mar  7 2000 Bill Nottingham <notting@redhat.com>
- rerun sysctl on network start (for restarts)

* Mon Feb 28 2000 Bill Nottingham <notting@redhat.com>
- don't read commented raid devices

* Mon Feb 21 2000 Bill Nottingham <notting@redhat.com>
- fix typo in resolv.conf munging

* Thu Feb 17 2000 Bill Nottingham <notting@redhat.com>
- sanitize repair prompt
- initial support for isdn-config stuff

* Mon Feb 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- add which as a package dependency (bug #9416)

* Tue Feb  8 2000 Bill Nottingham <notting@redhat.com>
- fixes for sound module loading

* Mon Feb  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- check that LC_ALL/LINGUAS and LANG are set before referencing them in lang.csh
- fix check for /var/*/news, work around for bug #9140

* Fri Feb  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix bug #9102

* Fri Feb  4 2000 Bill Nottingham <notting@redhat.com>
- if LC_ALL/LINGUAS == LANG, don't set them

* Wed Feb  2 2000 Bill Nottingham <notting@redhat.com>
- fix problems with linuxconf static routes

* Tue Feb  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- shvar cleaning
- fix wrong default route ip in network-functions

* Mon Jan 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- attempt to restore default route if PPP takes it over
- man page fix for ipcalc
- shvar cleaning
- automate maintaining /boot/System.map symlinks

* Mon Jan 31 2000 Bill Nottingham <notting@redhat.com>
- fix hanging ppp-watch
- fix issues with cleaning of /var/{run,lock}

* Fri Jan 21 2000 Bill Nottingham <notting@redhat.com>
- fix pidof calls in pidofproc

* Wed Jan 19 2000 Bill Nottingham <notting@redhat.com>
- fix ifup-ipx, don't munge resolv.conf if $DNS1 is already in it

* Thu Jan 13 2000 Bill Nottingham <notting@redhat.com>
- link popt statically

* Mon Jan 10 2000 Bill Nottingham <notting@redhat.com>
- don't try to umount /loopfs

* Mon Dec 27 1999 Bill Nottingham <notting@redhat.com>
- switch to using sysctl

* Mon Dec 13 1999 Bill Nottingham <notting@redhat.com>
- umount /proc *after* trying to turn off raid

* Mon Dec 06 1999 Michael K. Johnson <johnsonm@redhat.com>
- improvements in clone device handling
- better signal handling in ppp-watch
- yet another attempt to fix those rare PAP/CHAP problems

* Sat Nov 28 1999 Bill Nottingham <notting@redhat.com>
- impressive. Three new features, three new bugs.

* Mon Nov 22 1999 Michael K. Johnson <johnsonm@redhat.com>
- fix more possible failed CHAP authentication (with chat scripts)
- fix ppp default route problem
- added ppp-watch man page, fixed usernetctl man page
- make ifup-ppp work again when called from netcfg and linuxconf
- try to keep ppp-watch from filling up logs by respawning pppd too fast
- handle all linuxconf-style alias files with linuxconf

* Mon Nov 22 1999 Bill Nottingham <notting@redhat.com>
- load mixer settings for monolithic sound
- man page for ppp-watch
- add ARP variable for ifup
- some i18n fixes

* Wed Nov 10 1999 Bill Nottingham <notting@redhat.com>
- control stop-a separately from sysrq

* Mon Nov 08 1999 Michael K. Johnson <johnsonm@redhat.com>
- fix some failed CHAP authentication
- fix extremely unlikely, but slightly possible kill-random-process
  bug in ppp-watch
- allow DNS{1,2} in any ifcfg-* file, not just PPP, and
  add nameserver entries, don't just replace them
- don't use /tmp/confirm, use /var/run/confirm instead

* Tue Nov  2 1999 Bill Nottingham <notting@redhat.com>
- fix lang.csh /tmp race oops

* Wed Oct 27 1999 Bill Nottingham <notting@redhat.com>
- we now ship hwclock on alpha.

* Mon Oct 25 1999 Jakub Jelinek <jakub@redhat.com>
- fix check for serial console, don't use -C argument to fsck
  on serial console.

* Mon Oct 18 1999 Bill Nottingham <notting@redhat.com>
- do something useful with linuxconf 'any' static routes.

* Tue Oct 12 1999 Matt Wilson <msw@redhat.com>
- added patch from Owen to source i18n configuration before starting prefdm

* Mon Oct 11 1999 Bill Nottingham <notting@redhat.com>
- support for linuxconf alias files
- add support for Jensen clocks.

* Tue Oct  5 1999 Bill Nottingham <notting@redhat.com>
- assorted brown paper bag fixes
- check for programs/files before executing/sourcing them
- control stop-a like magic sysrq

* Thu Sep 30 1999 Bill Nottingham <notting@redhat.com>
- req. e2fsprogs >= 1.15

* Fri Sep 24 1999 Bill Nottingham <notting@redhat.com>
- munge C locale definitions to en_US
- use fsck's completion bar

* Thu Sep 23 1999 Michael K. Johnson <johnsonm@redhat.com>
- ppp-watch now always kills pppd pgrp to make sure dialers are dead,
  and tries to hang up the modem

* Tue Sep 21 1999 Bill Nottingham <notting@redhat.com>
- add a DEFRAG_IPV4 option

* Mon Sep 20 1999 Michael K. Johnson <johnsonm@redhat.com>
- changed to more modern defaults for PPP connections

* Mon Sep 20 1999 Bill Nottingham <notting@redhat.com>
- kill processes for umount in halt, too.
- fixes to remove /usr dependencies

* Fri Sep 17 1999 Bill Nottingham <notting@redhat.com>
- load/save mixer settings in rc.sysinit, halt

* Mon Sep 13 1999 Michael K. Johnson <johnsonm@redhat.com>
- add --remotename option to wvdial code
- make sure we do not have an earlier version of wvdial that doesn't
  know how handle --remotename
- make ppp-watch background itself after 30 seconds even if
  connection does not come up, at boot time only, so that a
  non-functional PPP connection cannot hang boot.

* Sun Sep 12 1999 Bill Nottingham <notting@redhat.com>
- a couple of /bin/sh -> /bin/bash fixes
- fix swapoff silliness

* Fri Sep 10 1999 Bill Nottingham <notting@redhat.com>
- chkconfig --del in %preun, not %postun
- use killall5 in halt
- swapoff non-/etc/fstab swap

* Wed Sep 08 1999 Michael K. Johnson <johnsonm@redhat.com>
- ifdown now synchronous (modulo timeouts)
- several unrelated cleanups, primarily in ifdown

* Tue Sep  7 1999 Bill Nottingham <notting@redhat.com>
- add an 'unconfigure' sort of thing

* Mon Sep 06 1999 Michael K. Johnson <johnsonm@redhat.com>
- added ppp-watch to make "ifup ppp*" synchronous

* Fri Sep  3 1999 Bill Nottingham <notting@redhat.com>
- require lsof

* Wed Sep  1 1999 Bill Nottingham <notting@redhat.com>
- add interactive prompt

* Tue Aug 31 1999 Bill Nottingham <notting@redhat.com>
- disable magic sysrq by default

* Mon Aug 30 1999 Bill Nottingham <notting@redhat.com>
- new NFS unmounting from Bill Rugolsky <rugolsky@ead.dsa.com> 
- fix ifup-sl/dip confusion
- more raid startup cleanup
- make utmp group 22

* Fri Aug 20 1999 Bill Nottingham <notting@redhat.com>
- pass hostname to pump
- add lang.csh

* Thu Aug 19 1999 Bill Nottingham <notting@redhat.com>
- more wvdial updates
- fix a *stupid* bug in process reading

* Fri Aug 13 1999 Bill Nottingham <notting@redhat.com>
- add new /boot/kernel.h boot kernel version file
- new RAID startup

* Fri Aug 13 1999 Michael K. Johnson <johnsonm@redhat.com>
- use new linkname argument to pppd to make if{up,down}-ppp
  reliable -- requires ppp-2.3.9 or higher

* Mon Aug  2 1999 Bill Nottingham <notting@redhat.com>
- fix typo.
- add 'make check'

* Wed Jul 28 1999 Michael K. Johnson <johnsonm@redhat.com>
- simple wvdial support for ppp connections

* Mon Jul 26 1999 Bill Nottingham <notting@redhat.com>
- stability fixes for initlog
- initlog now has a config file
- add alias speedup from dharris@drh.net
- move netfs links
- usleep updates

* Thu Jul  8 1999 Bill Nottingham <notting@redhat.com>
- remove timeconfig dependency
- i18n fixes from nkbj@image.dk
- move inputrc to setup package

* Tue Jul  6 1999 Bill Nottingham <notting@redhat.com>
- fix killall links, some syntax errors

* Fri Jun 25 1999 Bill Nottingham <notting@redhat.com>
- don't make module-info, System.map links
- handle utmpx/wtmpx
- fix lots of bugs in 4.21 release :)

* Thu Jun 17 1999 Bill Nottingham <notting@redhat.com>
- set clock as soon as possible
- use INITLOG_ARGS everywhere
- other random fixes in networking

* Mon Jun 14 1999 Bill Nottingham <notting@redhat.com>
- oops, don't create /var/run/utmp and then remove it.
- stomp RAID bugs flat. Sort of.

* Mon May 24 1999 Bill Nottingham <notting@redhat.com>
- clean out /var better
- let everyone read /var/run/ppp*.dev
- fix network startup so it doesn't depend on /usr

* Tue May 11 1999 Bill Nottingham <notting@redhat.com>
- various fixes to rc.sysinit
- fix raid startup
- allow for multi-processor /etc/issues

* Sun Apr 18 1999 Matt Wilson <msw@redhat.com>
- fixed typo - "Determing" to "Determining"

* Fri Apr 16 1999 Preston Brown <pbrown@redhat.com>
- updated inputrc so that home/end/del work on console, not just X

* Thu Apr 08 1999 Bill Nottingham <notting@redhat.com>
- fix more logic in initlog
- fix for kernel versions in ifup-aliases
- log to /var/log/boot.log

* Wed Apr 07 1999 Bill Nottingham <notting@redhat.com>
- fix daemon() function so you can specify pid to look for

* Wed Apr 07 1999 Erik Troan <ewt@redhat.com>
- changed utmp,wtmp to be group writeable and owned by group utmp

* Tue Apr 06 1999 Bill Nottingham <notting@redhat.com>
- fix loading of consolefonts/keymaps
- three changelogs. three developers. one day. Woohoo!

* Tue Apr 06 1999 Michael K. Johnson <johnsonm@redhat.com>
- fixed ifup-ipx mix-up over . and _

* Tue Apr 06 1999 Erik Troan <ewt@redhat.com>
- run /sbin/ifup-local after bringing up an interface (if that file exists)

* Mon Apr  5 1999 Bill Nottingham <notting@redhat.com>
- load keymaps & console font early
- fixes for channel bonding, strange messages with non-boot network interfaces

* Sat Mar 27 1999 Cristian Gafton <gafton@redhat.com>
- added sysvinitfiles as a documenattaion file

* Fri Mar 26 1999 Bill Nottingham <notting@redhat.com>
- nfsfs -> netfs

* Mon Mar 22 1999 Bill Nottingham <notting@redhat.com>
- don't source /etc/sysconfig/init if $BOOTUP is already set

* Fri Mar 19 1999 Bill Nottingham <notting@redhat.com>
- don't run linuxconf if /usr isn't mounted
- set macaddr before bootp
- zero in the /var/run/utmpx file (gafton)
- don't set hostname on ppp/slip (kills X)
			
* Wed Mar 17 1999 Bill Nottingham <notting@redhat.com>
- exit ifup if pump fails
- fix stupid errors in reading commands from subprocess

* Tue Mar 16 1999 Bill Nottingham <notting@redhat.com>
- fix ROFS logging
- make fsck produce more happy output
- fix killproc logic

* Mon Mar 15 1999 Bill Nottingham <notting@redhat.com>
- doc updates
- support for SYSFONTACM, other console-tools stuff
- add net route for interface if it isn't there.
- fix for a bash/bash2 issue

* Mon Mar 15 1999 Michael K. Johnson <johnsonm@redhat.com>
- pam_console lockfile cleanup added to rc.sysinit

* Sun Mar 14 1999 Bill Nottingham <notting@redhat.com>
- fixes in functions for 'action'
- fixes for pump

* Wed Mar 10 1999 Bill Nottingham <notting@redhat.com>
- Mmm. Must always remove debugging code. before release. *thwap*
- pump support
- mount -a after mount -a -t nfs

* Thu Feb 25 1999 Bill Nottingham <notting@redhat.com>
- put preferred support back in

* Thu Feb 18 1999 Bill Nottingham <notting@redhat.com>
- fix single-user mode (source functions, close if)

* Wed Feb 10 1999 Bill Nottingham <notting@redhat.com>
- turn off xdm in runlevel 5 (now a separate service)

* Thu Feb  4 1999 Bill Nottingham <notting@redhat.com>
- bugfixes (ifup-ppp, kill -TERM, force fsck, hwclock --adjust, setsysfont)
- add initlog support. Now everything is logged (and bootup looks different)

* Thu Nov 12 1998 Preston Brown <pbrown@redhat.com>
- halt now passed the '-i' flag so that network interfaces disabled

* Tue Nov 10 1998 Michael K. Johnson <johnsonm@redhat.com>
- handle new linuxconf output for ipaliases

* Mon Oct 15 1998 Erik Troan <ewt@redhat.com>
- fixed raid start stuff
- added raidstop to halt

* Mon Oct 12 1998 Cristian Gafton <gafton@redhat.com>
- handle LC_ALL

* Mon Oct 12 1998 Preston Brown <pbrown@redhat.com>
- adjusted setsysfont to always run setfont, even if only w/default font

* Tue Oct 06 1998 Cristian Gafton <gafton@redhat.com>
- rc.sysvinit should be working with all kernel versions now
- requires e2fsprogs (for fsck)
- set INPUTRC and LESSCHARSET on linux-lat

* Wed Sep 16 1998 Jeff Johnson <jbj@redhat.com>
- /etc/rc.d/rc: don't run /etc/rc.d/rcN.d/[KS]??foo.{rpmsave,rpmorig} scripts.
- /etc/rc.d/rc.sysinit: raid startup (Nigel.Metheringham@theplanet.net).
- /sbin/setsysfont: permit unicode fonts.

* Mon Aug 17 1998 Erik Troan <ewt@redhat.com>
- don't add 'Red Hat Linux' to /etc/issue; use /etc/redhat-release as is

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- paranoia improvements to .rhkmvtag
- if psacct with /sbin/accton, than turn off accounting

* Tue Jul  7 1998 Jeff Johnson <jbj@redhat.com>
- start/stop run levels changed.
- ipx_configure/ipx_internal_net moved to /sbin.

* Wed Jul 01 1998 Erik Troan <ewt@redhat.com>
- usernetctl didn't understand "" around USERCTL attribute

* Wed Jul  1 1998 Jeff Johnson <jbj@redhat.com>
- Use /proc/version to find preferred modules.
- Numerous buglets fixed.

* Sun Jun 07 1998 Erik Troan <ewt@redhat.com> 
- rc.sysinit looks for bootfile= as well as BOOT_IMAGE to set 
  /lib/modules/preferred symlink

* Mon Jun 01 1998 Erik Troan <ewt@redhat.com>
- ipcalc should *never* have been setgid anything
- depmod isn't run properly for non-serial numbered kernels

* Wed May 06 1998 Donnie Barnes <djb@redhat.com>
- added system font and language setting

* Mon May 04 1998 Michael K. Johnson <johnsonm@redhat.com>
- Added missing files to packagelist.

* Sat May 02 1998 Michael K. Johnson <johnsonm@redhat.com>
- Added lots of linuxconf support.  Should still work on systems that
  do not have linuxconf installed, but linuxconf gives enhanced support.
- In concert with linuxconf, added IPX support.  Updated docs to reflect it.

* Fri May 01 1998 Erik Troan <ewt@redhat.com>
- rc.sysinit uses preferred directory

* Sun Apr 05 1998 Erik Troan <ewt@redhat.com>
- updated rc.sysinit to deal with kernel versions with release numbers

* Sun Mar 22 1998 Erik Troan <ewt@redhat.com>
- use ipcalc to calculate the netmask if one isn't specified

* Tue Mar 10 1998 Erik Troan <ewt@redhat.com>
- added and made use of ipcalc

* Tue Mar 10 1998 Erik Troan <ewt@redhat.com>
- removed unnecessary dhcp log from /tmp

* Mon Mar 09 1998 Erik Troan <ewt@redhat.com>
- if bootpc fails, take down the device

* Mon Mar 09 1998 Erik Troan <ewt@redhat.com>
- added check for mktemp failure

* Thu Feb 05 1998 Erik Troan <ewt@redhat.com>
- fixed support for user manageable cloned devices

* Mon Jan 12 1998 Michael K. Johnson <johnsonm@redhat.com>
- /sbin/ isn't always in $PATH, so call /sbin/route in ifup-routes

* Wed Dec 31 1997 Erik Troan <ewt@redhat.com>
- touch /var/lock/subsys/kerneld after cleaning out /var/lock/subsys
- the logic for when  /var/lock/subsys/kerneld is touched was backwards

* Tue Dec 30 1997 Erik Troan <ewt@redhat.com>
- tried to get /proc stuff right one more time (uses -t nonfs,proc now)
- added support for /fsckoptions
- changed 'yse' to 'yes' in KERNELD= line

* Tue Dec 09 1997 Erik Troan <ewt@redhat.com>
- set domainname to "" if none is specified in /etc/sysconfig/network
- fix /proc mounting to get it in /etc/mtab

* Mon Dec 08 1997 Michael K. Johnson <johnsonm@redhat.com>
- fixed inheritance for clone devices

* Fri Nov 07 1997 Erik Troan <ewt@redhat.com>
- added sound support to rc.sysinit

* Fri Nov 07 1997 Michael K. Johnson <johnsonm@redhat.com>
- Added missing "then" clause

* Thu Nov 06 1997 Michael K. Johnson <johnsonm@redhat.com>
- Fixed DEBUG option in ifup-ppp
- Fixed PPP persistance
- Only change IP forwarding if necessary

* Tue Oct 28 1997 Donnie Barnes <djb@redhat.com>
- removed the skeleton init script
- added the ability to 'nice' daemons

* Tue Oct 28 1997 Erik Troan <ewt@redhat.com>
- touch /var/lock/subsys/kerneld if it's running, and after mounting /var
- applied dhcp fix

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- added status|restart to init scripts

* Thu Oct 23 1997 Michael K. Johnson <johnsonm@redhat.com>
- touch random seed file before chmod'ing it.

* Wed Oct 15 1997 Erik Troan <ewt@redhat.com>
- run domainname if NISDOMAIN is set 

* Wed Oct 15 1997 Michael K. Johnson <johnsonm@redhat.com>
- Make the random seed file mode 600.

* Tue Oct 14 1997 Michael K. Johnson <johnsonm@redhat.com>
- bring down ppp devices if ifdown-ppp is called while ifup-ppp is sleeping.

* Mon Oct 13 1997 Erik Troan <ewt@redhat.com>
- moved to new chkconfig conventions

* Sat Oct 11 1997 Erik Troan <ewt@redhat.com>
- fixed rc.sysinit for hwclock compatibility

* Thu Oct 09 1997 Erik Troan <ewt@redhat.com>
- run 'ulimit -c 0' before running scripts in daemon function

* Wed Oct 08 1997 Donnie Barnes <djb@redhat.com>
- added chkconfig support
- made all rc*.d symlinks have missingok flag

* Mon Oct 06 1997 Erik Troan <ewt@redhat.com>
- fixed network-scripts to allow full pathnames as config files
- removed some old 3.0.3 pcmcia device handling

* Wed Oct 01 1997 Michael K. Johnson <johnsonm@redhat.com>
- /var/run/netreport needs to be group-writable now that /sbin/netreport
  is setguid instead of setuid.

* Tue Sep 30 1997 Michael K. Johnson <johnsonm@redhat.com>
- Added network-functions to spec file.
- Added report functionality to usernetctl.
- Fixed bugs I introduced into usernetctl while adding clone device support.
- Clean up entire RPM_BUILD_ROOT directory in %clean.

* Mon Sep 29 1997 Michael K. Johnson <johnsonm@redhat.com>
- Clone device support in network scripts, rc scripts, and usernetctl.
- Disassociate from controlling tty in PPP and SLIP startup scripts,
  since they act as daemons.
- Spec file now provides start/stop symlinks, since they don't fit in
  the CVS archive.

* Tue Sep 23 1997 Donnie Barnes <djb@redhat.com>
- added mktemp support to ifup

* Thu Sep 18 1997 Donnie Barnes <djb@redhat.com>
- fixed some init.d/functions bugs for stopping httpd

* Tue Sep 16 1997 Donnie Barnes <djb@redhat.com>
- reworked status() to adjust for processes that change their argv[0] in
  the process table.  The process must still have it's "name" in the argv[0]
  string (ala sendmail: blah blah).

* Mon Sep 15 1997 Erik Troan <ewt@redhat.com>
- fixed bug in FORWARD_IPV4 support

* Sun Sep 14 1997 Erik Troan <ewt@redhat.com>
- added support for FORWARD_IPV4 variable

* Thu Sep 11 1997 Donald Barnes <djb@redhat.com>
- added status function to functions along with better killproc 
  handling.
- added /sbin/usleep binary (written by me) and man page
- changed BuildRoot to /var/tmp instead of /tmp

* Tue Jun 10 1997 Michael K. Johnson <johnsonm@redhat.com>
- /sbin/netreport sgid rather than suid.
- /var/run/netreport writable by group root.

- /etc/ppp/ip-{up|down} no longer exec their local versions, so
  now ifup-post and ifdown-post will be called even if ip-up.local
  and ip-down.local exist.

* Tue Jun 03 1997 Michael K. Johnson <johnsonm@redhat.com>
- Added missing -f to [ invocation in ksyms check.

* Fri May 23 1997 Michael K. Johnson <johnsonm@redhat.com>
- Support for net event notification:
  Call /sbin/netreport to request that SIGIO be sent to you whenever
  a network interface changes status (won't work for brining up SLIP
  devices).
  Call /sbin/netreport -r to remove the notification request.
- Added ifdown-post, and made all the ifdown scrips call it, and
  added /etc/ppp/ip-down script that calls /etc/ppp/ip-down.local
  if it exists, then calls ifdown-post.
- Moved ifup and ifdown to /sbin

* Tue Apr 15 1997 Michael K. Johnson <johnsonm@redhat.com>
- usernetctl put back in ifdown
- support for slaved interfaces

* Wed Apr 02 1997 Erik Troan <ewt@redhat.com>
- Created ifup-post from old ifup
- PPP, PLIP, and generic ifup use ifup-post

* Fri Mar 28 1997 Erik Troan <ewt@redhat.com>
- Added DHCP support
- Set hostname via reverse name lookup after configuring a networking
  device if the current hostname is (none) or localhost

* Tue Mar 18 1997 Erik Troan <ewt@redhat.com>
- Got rid of xargs dependency in halt script
- Don't mount /proc twice (unmount it in between)
- sulogin and filesystem unmounting only happened for a corrupt root 
  filesystem -- it now happens when other filesystems are corrupt as well

* Tue Mar 04 1997 Michael K. Johnson <johnsonm@redhat.com>
- PPP fixes and additions

* Mon Mar 03 1997 Erik Troan <ewt@redhat.com>
- Mount proc before trying to start kerneld so we can test for /proc/ksyms
  properly.

* Wed Feb 26 1997 Michael K. Johnson <johnsonm@redhat.com>
- Added MTU for PPP.
- Put PPPOPTIONS at the end of the options string instead of at the
  beginning so that they override other options.  Gives users more rope...
- Don't do module-based stuff on non-module systems.  Ignore errors if
  st module isn't there and we try to load it.

* Tue Feb 25 1997 Michael K. Johnson <johnsonm@redhat.com>
- Changed ifup-ppp and ifdown-ppp not to use doexec, because the argv[0]
  provided by doexec goes away when pppd gets swapped out.
- ifup-ppp now sets remotename to the logical name of the device.
  This will BREAK current PAP setups on netcfg-managed interfaces,
  but we needed to do this to add a reasonable interface-specific
  PAP editor to netcfg.

* Fri Feb 07 1997 Erik Troan <ewt@redhat.com>
- Added usernetctl wrapper for user mode ifup and ifdown's and man page
- Rewrote ppp and slip kill and retry code 
- Added doexec and man page
