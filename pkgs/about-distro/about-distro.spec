%global         momorel 1
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         kdever 4.12.3
%global         kdelibsrel 1m
%global         qtver 4.8.5
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         ftpdirver 1.0.1
%global         sourcedir %{release_dir}/%{name}/%{ftpdirver}/src

Name:           about-distro
Version:        %{ftpdirver}
Release:        %{momorel}m%{?dist}
Summary:        KCM displaying distribution and system information
# KDE e.V. may determine that future GPL versions are accepted
License:        GPLv2 or GPLv3
Group:          Applications/System
URL:            https://projects.kde.org/about-distro
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
Source1:        kcm-about-distrorc
Source2:        tobimomo.png
Patch0:         %{name}-%{version}-momonga.patch
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}
Requires:       kde-runtime >= %{kdever}
Requires:       kde-workspace >= %{kdever}

%description
%{summary}.

%prep
%setup -q
%patch0 -p1

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd
make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

## install logo and rc file
mkdir -p %{buildroot}%{_kde4_datadir}/config
install -m 644 %{SOURCE1} %{buildroot}%{_kde4_datadir}/config
install -m 644 %{SOURCE2} %{buildroot}%{_kde4_datadir}/config

%files
%defattr(-,root,root,-)
%doc COPYING README example/*
%{_kde4_libdir}/kde4/kcm_about_distro.so
%{_kde4_datadir}/config/kcm-%{name}rc
%{_kde4_datadir}/config/tobimomo.png
%{_kde4_datadir}/kde4/services/%{name}.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/kcm-%{name}.mo

%changelog
* Mon Mar 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- initial build for Momonga Linux
