%global momorel 1

Summary: Fast Light Tool Kit (FLTK)
Name: fltk
Version: 1.3.2
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: http://ftp.easysw.com/pub/fltk/%{version}/fltk-%{version}-source.tar.gz
NoSource: 0
Source1: fltk-config.sh

Patch1:  fltk-1.1.9-fltk_config.patch 
Patch3:  fltk-1.1.x-r5750-undefined.patch
Patch5:  fltk-1.1.8-fluid_desktop.patch
Patch8:  fltk-1.3.0-rh708185.patch
Patch9:  fltk-1_v4.3.x-keyboard-x11.patch
Patch10: fltk-1_v2.3.x-clipboard.patch
Patch11: fltk-1_v2.3.x-clipboard-x11.patch
Patch12: fltk-1_v3.3.x-clipboard-xfixes.patch
Patch13: fltk-1_v4.3.x-cursor.patch
Patch20: fltk-1_v4.3.x-cursor-abi.patch
Patch14: fltk-1.3.x-resize-expose.patch
Patch15: pixmap.patch
Patch16: fltk-1_v2.3.0-modal.patch
Patch17: fltk-1_v2.3.0-icons.patch
Patch18: fltk-1.3.x-screen_num.patch
Patch19: fltk-1_v2.3.x-multihead.patch
URL: http://www.fltk.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libX11-devel, libXext-devel, libXft-devel
BuildRequires: libjpeg-devel >= 8a

%description
The Fast Light Tool Kit ("FLTK", pronounced "fulltick") is a
cross-platform C++ GUI toolkit for UNIX(r)/Linux(r) (X11),
Microsoft(r) Windows(r), and MacOS(r) X.  FLTK provides modern
GUI functionality without the bloat and supports 3D graphics via
OpenGL(r) and its built-in GLUT emulation.

%package devel
Summary: FLTK - development environment
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Install fltk-devel if you need to develop FLTK applications. 
You'll need to install the fltk package if you plan to run
dynamically linked applications.

%package fluid
Summary:       Fast Light User Interface Designer
Group:         Development/Tools
Requires:      %{name}-devel = %{version}-%{release}
%description fluid
%{summary}, an interactive GUI designer for %{name}.

%prep
#'
%setup -q
%patch1 -p1 -b .fltk_config
%patch3 -p1 -b .undefined
%patch5 -p1 -b .fluid_desktop
%patch8 -p1 -b .rh708185
%patch9 -p1 -b .deadkeys
%patch10 -p1 -b .clipboard1
%patch11 -p1 -b .clipboard2
%patch12 -p1 -b .clipboard3
%patch13 -p1 -b .cursor
%patch20 -p1 -b .cursor-abi
#%patch14 -p1 -b .resize-expose
%patch15 -p0 -b .pixmap
%patch16 -p1 -b .modal
#%%patch17 -p1 -b .icons
%patch18 -p1 -b .screen_num
#%patch19 -p1 -b .multihead

%build
# using --with-optim, so unset CFLAGS/CXXFLAGS
export CFLAGS=" "
export CXXFLAGS=" "

%configure \
  --with-links \
  --with-optim="%{optflags}" \
  --enable-largefile \
  --enable-shared \
  --enable-threads \
  --enable-xdbe \
  --enable-xinerama \
  --enable-xft

make %{?_smp_mflags}

%install

make install install-desktop DESTDIR=%{buildroot}

# omit examples/games: 
make -C test uninstall-linux DESTDIR=%{buildroot}
rm -f  %{buildroot}%{_mandir}/man?/{blocks,checkers,sudoku}*

# we only apply this hack to multilib arch's
%ifarch x86_64 %{ix86} ppc64 ppc s390x s390 sparc64 sparc
%global arch %(uname -i 2>/dev/null || echo undefined)
mv %{buildroot}%{_bindir}/fltk-config \
   %{buildroot}%{_bindir}/fltk-config-%{arch}
install -p -m755 -D %{SOURCE1} %{buildroot}%{_bindir}/fltk-config
%endif

# docs
rm -rf __docs
mv %{buildroot}%{_docdir}/fltk __docs

## unpackaged files
# errant docs
rm -rf %{buildroot}%{_mandir}/cat*

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/fluid.desktop

%post
/sbin/ldconfig

%postun
if [ "$1" = 0 ]; then
    /sbin/ldconfig
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ANNOUNCEMENT CHANGES COPYING CREDITS README
%{_libdir}/libfltk.so.*
%{_libdir}/libfltk_forms.so.*
%{_libdir}/libfltk_gl.so.*
%{_libdir}/libfltk_images.so.*

%files devel
%defattr(-,root,root,-)
%doc documentation
%{_bindir}/fltk-config
%{?arch:%{_bindir}/fltk-config-%{arch}}
%{_includedir}/FL/
%{_includedir}/Fl
%{_libdir}/libfltk.so
%{_libdir}/libfltk_forms.so
%{_libdir}/libfltk_gl.so
%{_libdir}/libfltk_images.so
%{_libdir}/libfltk.a
%{_libdir}/libfltk_forms.a
%{_libdir}/libfltk_gl.a
%{_libdir}/libfltk_images.a
%{_mandir}/man1/fltk-config.1*
%{_mandir}/man3/fltk.3*

%files fluid
%defattr(-,root,root,-)
%{_bindir}/fluid
%{_mandir}/man1/fluid.1*
%{_datadir}/applications/fluid.desktop
%{_datadir}/icons/hicolor/*/*/*
# FIXME, add according to new mime spec
%{_datadir}/mimelnk/*/*.desktop

%changelog
* Fri Dec 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-1m)
- update 1.3.2

* Sun May 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.10-1m)
- update to 1.1.10 (sync with Fedora 15)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.7-16m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.7-15m)
- fix build

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-14m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-12m)
- update glibc210 patch

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-11m)
- apply glibc210 patch with gcc44 only

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-10m)
- apply glibc210 patch

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.7-9m)
- rebuild against libjpeg-7

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-8m)
- revise configure
-- build with --enable-threads for aqsis-1.4.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-7m)
- rebuild against rpm-4.6

* Sun Jun 08 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.7-6m)
- back to 1.1.7
- 1.1.8+ could not build in x86_64

* Fri Jun 06 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.8-1m)
- version down and completely resync with Fedora(fltk-1_1_8-1_fc9)
- I hope this solves the build problem in x86_64

* Fri May 30 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.9-3m)
- import patches from Fedora

* Fri May 16 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.9-2m)
- modified %%install as Fedora do for x86_64 ??

* Thu May 15 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.9-1m)
- update to 1.1.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.7-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.7-4m)
- %%NoSource -> NoSource

* Tue Jan  2 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.7-3m)
- Patch1: install fix patch

* Tue Jan  2 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.7-2m)
- build fix for 64bit arch.

* Sat Mar 25 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7

* Tue Apr 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-2m)
- remove duplicated files

* Sat Nov 06 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.5-1m)
- update to bug fix release 1.1.5

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.4-3m)
- revised spec for enabling rpm 4.2.

* Tue Dec 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.4-2m)
- add missing files to devel package
- run ldconfig

* Tue Dec 16 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.11-9m)
- add BuildPreReq: XFree86-devel
- rebuild against gcc-3.2

* Tue Jun  4 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0.11-8k)
- *-devel should equires %{name} = %{version}-%{release}

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.0.11-6k)
- cancel gcc-3.1 autoconf-2.53

* Tue May 28 2002 Toru Hoshina <t@kondara.org>
- (1.0.11-4k)
- rebuild by g++ 2.95.3.

* Fri Feb 22 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.0.11-2k)
- import from Jirai

