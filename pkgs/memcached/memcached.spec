%global momorel 1

%global username   memcached
%global groupname  memcached


Summary:        memcached is a high-performance, distributed memory object caching system
Name:           memcached
Version:        1.4.15
Release:        %{momorel}m%{?dist}
Group:          System Environment/Daemons
License:        Modified BSD
URL:            http://www.danga.com/memcached/
Source0:        http://memcached.googlecode.com/files/memcached-%{version}.tar.gz
Source1:        memcached.service
NoSource:       0
Patch001:       memcached-manpages.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       chkconfig
Requires:       libevent >= 2.0.10
BuildRequires:  libevent-devel >= 2.0.10
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
# For triggerun
Requires(post): systemd-sysv
Requires(pre):  shadow-utils

%description
memcached is a high-performance, distributed memory object caching
system, generic in nature, but intended for use in speeding up dynamic
web applications by alleviating database load.

%package devel
Summary: Files needed for development using memcached protocol
Group: Development/Libraries
Requires: %{name} = :%{version}-%{release}

%description devel
Install memcached-devel if you are developing C/C++ applications that require
access to the memcached binary include files.

%prep
%setup -q
%patch001 -p1

%build
%global optflags %{optflags} -fno-strict-aliasing

%configure --program-prefix=''
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"
# remove memcached-debug
rm -f %{buildroot}/%{_bindir}/memcached-debug

# Perl script for monitoring memcached
install -Dp -m0755 scripts/memcached-tool %{buildroot}%{_bindir}/memcached-tool
install -Dp -m0644 scripts/memcached-tool.1 \
        %{buildroot}%{_mandir}/man1/memcached-tool.1

# Unit file
install -Dp -m0644 %{SOURCE1} %{buildroot}%{_unitdir}/memcached.service

# Default configs
mkdir -p %{buildroot}/%{_sysconfdir}/sysconfig
cat <<EOF >%{buildroot}/%{_sysconfdir}/sysconfig/%{name}
PORT="11211"
USER="%{username}"
MAXCONN="1024"
CACHESIZE="64"
OPTIONS=""
EOF

# Constant timestamp on the config file.
touch -r %{SOURCE1} %{buildroot}/%{_sysconfdir}/sysconfig/%{name}

%clean
rm -rf %{buildroot}

%check
# whitespace tests fail locally on fedpkg systems now that they use git
rm -f t/whitespace.t

# Parts of the test suite only succeed as non-root.
if [ `id -u` -ne 0 ]; then
  # remove failing test that doesn't work in
  # build systems
  rm -f t/daemonize.t
fi
make test

%pre
getent group %{groupname} >/dev/null || groupadd -r %{groupname}
getent passwd %{username} >/dev/null || \
useradd -r -g %{groupname} -d /run/memcached \
    -s /sbin/nologin -c "Memcached daemon" %{username}
exit 0


%post
%systemd_post memcached.service


%preun
%systemd_preun memcached.service


%postun
%systemd_postun_with_restart memcached.service

%triggerun -- memcached < 1.4.18
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply memcached
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save memcached >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del memcached >/dev/null 2>&1 || :
/bin/systemctl try-restart memcached.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README.md doc/CONTRIBUTORS doc/*.txt
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%{_bindir}/memcached-tool
%{_bindir}/memcached
%{_mandir}/man1/memcached-tool.1*
%{_mandir}/man1/memcached.1*
%{_unitdir}/memcached.service

%files devel
%defattr(-,root,root,0755)
%{_includedir}/memcached/*

%changelog
* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.7-1m)
- update 1.4.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-6m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-5m)
- rebuild against libevent-2.0.10

* Thu Dec  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-4m)
- fix build

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (1.4.3-2m)
- add %%dir

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-1m)
- [SECURITY] CVE-2010-1152
- update to 1.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-1m)
- update 1.4.2

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.8-1m)
- [SECURITY] CVE-2009-1255
- update to 1.2.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-4m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-3m)
- update Patch1 for fuzz=0
- License: Modified BSD

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.4-2m)
- rebuild against gcc43

* Sat Jan  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4
- rebuild against libevent-1.4.1-0.1.1m

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-2m)
- rebuild against libevent-1.3e

* Fri Sep 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3
- rebuild against libevent-1.3d
- import memcached-1.2.3-save_pid_fix.patch from Fedora devel
- do not use %%NoSource macro

* Thu Aug 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2
- rebuild against libevent-1.3c

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-3m)
- Requires: chkconfig

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-2m)
- rebuild against libevent-1.3b

* Wed Feb 21 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.1-1m)
- up to 1.2.1
- rebuild against libevent-1.3a

* Tue Feb 28 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.1.12-2m)
- add init script

* Wed Feb 2 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.1.12-1m)
- Initial version
- no start script yet
