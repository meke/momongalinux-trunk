%global momorel 9

# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation version
# 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# END COPYRIGHT BLOCK

%define coolkey_module "CoolKey PKCS #11 Module"
%define nssdb %{_sysconfdir}/pki/nssdb

Name: coolkey
Version: 1.1.0
Release: %{momorel}m%{?dist}
Summary: CoolKey PKCS #11 module
License: LGPLv2
URL: http://directory.fedora.redhat.com/wiki/CoolKey
Source: http://directory.fedora.redhat.com/download/coolkey/coolkey-%{version}.tar.gz
NoSource: 0
Patch1: coolkey-cache-dir-move.patch
Patch2: coolkey-gcc43.patch
Patch3: coolkey-pcsc-lite.patch
Group: System Environment/Libraries
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: pcsc-lite-devel
BuildRequires: zlib-devel
BuildRequires: nss-devel
Requires: nss-tools
Requires: pcsc-lite 
Requires: pcsc-lite-libs
Requires: ifd-egate
Requires: ccid
Provides: CoolKey Openkey
Obsoletes: CoolKey Openkey
# 390 does not have libusb or smartCards
ExcludeArch: s390 s390x

%description
Linux Driver support for the CoolKey and CAC products. 

%package devel
Summary: CoolKey Applet libraries
Group: System Environment/Libraries

%description devel
Linux Driver support to access the CoolKey applet.

%prep
%setup -q
%patch1 -b .cache.dir.move
%patch2 -b .coolkey-gcc43
%patch3 -p1 -b .pcsc-lite~

%build
autoconf
%configure --program-transform-name="" \
  --with-debug --disable-dependency-tracking --enable-pk11install
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
ln -s pkcs11/libcoolkeypk11.so $RPM_BUILD_ROOT/%{_libdir}
mkdir -p $RPM_BUILD_ROOT/var/cache/coolkey

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig
isThere=`modutil -rawlist -dbdir %{nssdb} | grep %{coolkey_module} || echo NO`
if [ "$isThere" == "NO" ]; then
   if [ -x %{_bindir}/pk11install ]; then
      pk11install -p %{nssdb} 'name=%{coolkey_module} library=libcoolkeypk11.so' ||:
   fi
fi

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
   modutil -delete %{coolkey_module} -dbdir %{nssdb} -force || :
fi

%files
%defattr(-,root,root,-)
%doc ChangeLog LICENSE 
%{_bindir}/pk11install
%{_libdir}/libcoolkeypk11.so
%{_libdir}/pkcs11/libcoolkeypk11.so
%{_libdir}/libckyapplet.so.1
%{_libdir}/libckyapplet.so.1.0.0

%files devel
%{_libdir}/libckyapplet.so
%{_libdir}/pkgconfig/libckyapplet.pc
%{_includedir}/*.h

%changelog
* Tue Sep  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-9m)
- fix build failure; import coolkey-pcsc-lite patch from debian

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-6m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-3m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-2m)
- [BUILD FIX] change BR from mozilla-nss-devel to nss-devel

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.0-1m)
- import from Fedora

* Wed Feb 13 2008 Jack magne <jmagne@redhat.com>  - 1.1.0-6
- Clean up building with gcc 4.3.
* Thu Sep 27 2007 Jack Magne <jmagne@redhat.com>  - 1.1.0-5
- Include patch for moving the cache directory to a safe location. 
- Bug #299481.
* Fri Aug 20 2007 Bob Relyea <rrelyea@redhat.com> - 1.1.0-4
- Update License description to the new Fedora standard

* Thu Jun 21 2007 Kai Engert <kengert@redhat.com> - 1.1.0-3.1
- rebuild

* Tue Jun 5 2007 Bob Relyea <rrelyea@redhat.com> - 1.1.0-3
- add build requires, bump version number for make tag.

* Thu May 31 2007 Bob Relyea <rrelyea@redhat.com> - 1.1.0-2
- Back out RHEL-4 version of spec from CVS, add pcsc-lite-lib requires.

* Tue Feb 20 2007 Bob Relyea <rrelyea@redhat.com> - 1.1.0-1
- Pick up lates release.

* Wed Nov 1 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-15
- Don't grab the CUID on cac's. Resting the card causes it to
- logout of other applications.

* Wed Nov 1 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-14
- Shared memory directory needs to be writeable by all so
- coolkey can create caches for any user. (lack of caches
- show up in screen savers reactly slowly).

* Fri Oct 20 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-13
- fix login hw race failures

* Fri Oct 20 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-12
- add the dist flag

* Wed Oct 18 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-11
- CAC cards sometimes need to reset before they can get their
- initial transaction (problem is noticed on insertion an removal)

* Tue Oct 17 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-10
- Only run pk11install if the binary is there (multilib fun)

* Mon Oct 09 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-9
- use pk11install which does not require loading the module to install it.

* Mon Oct 09 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-8
- pcscd must be running in order to add coolkey.

* Thu Oct 4 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-7
- silence modutil warnings

* Thu Sep 30 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-5
- install and uninstall coolkey in the system secmod.db

* Thu Sep 7 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-4
- make the coolkey token caches persist over application calls.
- make a separate cache for each user.

* Sun Jul 16 2006 Florian La Roche <laroche@redhat.com> - 1.0.1-2
- fix excludearch line

* Mon Jul 10 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-1
- Don't require pthread library in coolkey

* Mon Jul 10 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.0-2
- remove s390 from the build

* Mon Jun 5 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.0-1
- Initial revision for fedora
