%global version 0.9.71
%global kernel_ver %%(uname -r)
#%%global kernel_ver 2.6.15-8m
%global momorel 10
%define kernel_trees %_topdir/BUILD/mol-kmods-%version/kernel

Summary: Mac-on-Linux kernel modules
Name: mol-kmods
Version: %{version}
Release: %{momorel}m%{?dist}
License: GPL
#Vendor: Ibrium HB
Group: Applications/Emulators
Source: ftp://ftp.nada.kth.se/home/f95-sry/Public/mac-on-linux/mol-kmods-%{version}.tgz
Source1: mol-0.9.71-config
Patch0: mol-kmods-alloc_h.patch
Patch1: mol-kmods-0.9.71-largefile.patch
Patch2: mol-0.9.71-gcc4.patch
Provides: mol-kernel-modules
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: ppc

%description

This package contains the Mac-on-Linux kernel module
needed by MOL. It also contains some networking kernel
module (used for MOL networking).

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
mkdir %kernel_trees
ln -sf /lib/modules/%{kernel_ver}/build %kernel_trees/%{kernel_ver}
ln -sf /lib/modules/%{kernel_ver}smp/build %kernel_trees/%{kernel_ver}smp

ln -sf /lib/modules/%{kernel_ver}/build/.config /usr/src/linux/.config
ln -sf /usr/src/linux/include/linux/autoconf-uni.h /usr/src/linux/include/linux/autoconf.h
ln -sf /usr/src/linux/include/linux/version-uni.h /usr/src/linux/include/linux/version.h
ln -sf /lib/modules/%{kernel_ver}/build/scripts/basic/fixdep /usr/src/linux/scripts/basic/fixdep
ln -sf /lib/modules/%{kernel_ver}/build/scripts/mod/modpost /usr/src/linux/scripts/mod/modpost
%configure




%build
make defconfig
cp %{SOURCE1} .config-ppc
make clean
pwd
mkdir -p obj-ppc/build/src/netdriver/
echo %{kernel_ver} > obj-ppc/build/src/netdriver/.kuname
make KERNEL_SOURCE=/usr/src/linux modules
%install
make KERNEL_SOURCE=/usr/src/linux install-modules DESTDIR=$RPM_BUILD_ROOT prefix=/usr

%clean
rm -rf $RPM_BUILD_ROOT
rm -f /usr/src/linux/scripts/basic/fixdep
rm -f /usr/src/linux/scripts/mod/modpost
rm -f /usr/src/linux/include/linux/autoconf.h 
rm -f /usr/src/linux/include/linux/version.h 
rm -f /usr/src/linux/.config 
#%preun

%define _mol_libdir %{_libdir}/mol/%{version}

%files
#%doc COPYRIGHT

%{_mol_libdir}/modules/


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.71-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.71-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.71-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.71-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.71-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.71-5m)
- rebuild against gcc43

* Sun Mar 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.71-4m)
- add gcc4 patch
- cleanup spec file

* Sat Apr 02 2005 mutecat <mutecat@momonga-linux.org>
- (0.9.71-3m)
- kernel ver shuusei

* Sat Mar 12 2005 mutecat <mutecat@momonga-linux.org>
- (0.9.71-2m)
- add mol-0.9.71-largefile.patch

* Sun Mar 06 2005 mutecat <mutecat@momonga-linux.org>
- (0.9.71-1m)
- import from ydl.

* Wed Aug 25 2004 Dan Buracw <dan@ydl.net>
- comment out Vendor: field from spec
* Wed Jan 12 2004 Samuel Rydh <samuel@ibrium.se>
- sheep_net removed from distribution (created dynamically)
* Wed Jul 3 2002 Samuel Rydh <samuel@ibrium.se>
- sheep_net removed from distribution (created dynamically)
* Sat Apr 27 2002 Samuel Rydh <samuel@ibrium.se>
- The explicit build of the buildtools is no longer necessary
* Wed Apr 10 2002 Samuel Rydh <samuel@ibrium.se>
- Updated to work with FHS
* Sun Mar 31 2002 Samuel Rydh <samuel@ibrium.se>
- Updated to work with automake
* Sun Aug 7 2001 Samuel Rydh <samuel@ibrium.se>
- Modified for multi-module builds
* Sun Apr 29 2001 Samuel Rydh <samuel@ibrium.se>
- created
