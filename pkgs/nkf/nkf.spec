%global momorel 2
%global dirname 59912

Summary: Network Kanji code conversion Filter
Name: nkf
Version: 2.1.3
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/Text
URL: http://nkf.sourceforge.jp/
Source0: http://dl.sourceforge.jp/%{name}/%{dirname}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 5.12.0

%description
Nkf is a yet another kanji code converter among networks, hosts and termi-
nals.  It converts input kanji code to designated kanji code such as 7-bit
JIS, MS-kanji (shifted-JIS) or EUC.

One of the most unique facicility of nkf is the guess of the input kanji
code.  It currently recognizes 7-bit JIS, MS-kanji (shifted-JIS) and EUC.
So users needn't the input kanji code specification.

By default X0201 kana is converted into X0208 kana. For X0201 kana, SO/SI,
SSO and ESC-(-I methods are supported. For automatic code detection, nkf
assumes no X0201 kana in MS-Kanji. To accept X0201 in MS-Kanji, use -X, -x
or -S.

%package -n perl-NKF
Summary: Perl extension for Network Kanji Filter
Group: Applications/Text
Requires: perl >= 5.10.0

%description -n perl-NKF
This is a Perl Extension version of nkf (Netowrk Kanji Filter ) 1.7.
It converts the last argument and return converted result. Conversion
details are specified by flags before the last argument.

%prep
%setup -q

%build
make CC="gcc" CFLAGS="%{optflags}" PERL="perl" nkf
./nkf -e -O nkf.1j
mv nkf.out nkf.1j
(cd NKF.mod;
 CFLAGS="%optflags" perl Makefile.PL
 make
 make test
)

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
install -d -m 0755 %{buildroot}%{_bindir}
install -d -m 0755 %{buildroot}%{_mandir}/{man1,ja/man1}
install -s -m 755 nkf %{buildroot}%{_bindir}
install -m 644 nkf.1 %{buildroot}%{_mandir}/man1/
install -m 644 nkf.1j %{buildroot}%{_mandir}/ja/man1/nkf.1
(cd NKF.mod;
make install DESTDIR=%{buildroot} INSTALLDIRS=vendor
)
find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name perllocal.pod -exec rm -f {} \;

# convert Japanese manual page from EUC-JP to UTF-8
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
        iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/nkf
%{_mandir}/man1/nkf.1*
%{_mandir}/ja/man1/nkf.1*

%files -n perl-NKF
%defattr(-,root,root)
%{perl_vendorarch}/NKF.pm
%{perl_vendorarch}/auto/NKF
%{_mandir}/man3/NKF.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-2m)
- rebuild against perl-5.20.0

* Sun Jun 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.3-1m)
- update 2.1.3

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-2m)
- rebuild against perl-5.14.2

* Sat Sep 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-2m)
- rebuild for new GCC 4.5

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-3m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-2m)
- rebuild against perl-5.12.0

* Tue Dec  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.9-3m)
- rebuild against perl-5.10.1

* Sat May  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.9-2m)
- update to git snapshot (2009-06-23)
- use vendor

* Sat May  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.9-1m)
- update to git snapshot (2009-04-26)
- License: Modified BSD

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.8-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.8-5m)
- rebuild against gcc43

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.8-4m)
- rebuild against perl-5.10.0-1m

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.8-3m)
- revise spec

* Sun Aug 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-2m)
- update to 2.0.8b

* Tue Jul 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.7-2m)
- convert ja.man from EUC-JP to UTF-8

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.7-1m)
- update to 2.0.7

* Sat May 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.7-0.1.1m)
- update to 2.0.7_beta1

* Thu Mar 16 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.6-0.1.1m)
- update to 2.0.6_beta1

* Mon Mar 06 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.0.5-1m)
- update to 2.0.5

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.5-0.20050102.2m)
- rebuild against perl-5.8.8

* Tue Feb  8 2005 zunda <zunda at freeshell.org>
- (2.0.5-0.20050102.1m)
- update to a CVS version
- added --guess option to guess charactor code of each input file

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.4-3m)
- build against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.0.4-2m)
- remove Epoch from BuildPrereq

* Wed Jan  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.4-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-3m)
- rebuild against perl-5.8.2

* Sat Nov 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-2m)
- use %%global

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.3-2m)
- rebuild against perl-5.8.1

* Wed Oct 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Mon Jan 27 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Fri Nov 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.1-2m)
- rebuild against perl-5.8.0

* Thu Oct 24 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (2.0.1-1m)
- version up to nkf-2.0.1 and add 20021024 cvs patch.
- change URL tag.
- check buildroot at %%clean section

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag
- fix ChangeLog to changelog

* Tue Aug 28 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.92-12k)
- fix zenkaku_space mojibake problem

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- fix for bzip2ed man

* Sun Jul  9 2000 Hidetomo Machi <mcHT@kondara.org>
- not gzip man file in spec

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Jul  2 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.6.0 

* Sat Mar 18 2000 Motonobu Ichimura <famao@kondara.org>
- up to 1.92

* Sat Mar 11 2000 Motonobu Ichimura <famao@kondara.org>
- fix .packlist problem

* Thu Dec 30 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Rebuild from shar file.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Nov 6 1999 Tenkou N. Hattori <tnh@kondara.org>
- convart nkf.1j to EUC-JP code.

* Tue Nov 2 1999 Norihito Ohmori <ohmori@flatout.org>
- update to nkf-1.90
