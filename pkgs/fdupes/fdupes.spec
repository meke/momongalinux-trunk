%global momorel 5

Summary:	Finds duplicate files in a given set of directories
Name:		fdupes
Version:	1.50
Release:	0.2.%{momorel}m%{?dist}
License:	MIT
Group:		Applications/File
URL:		http://netdial.caribe.net/~adrian2/fdupes.html
Source0:	http://netdial.caribe.net/~adrian2/programs/fdupes/beta/%{name}-%{version}-PR2.tar.gz
NoSource:       0

Patch0:		%{name}-%{version}-destdir.patch
# http://bugs.debian.org/213385
Patch1:		%{name}-%{version}-compare-file.patch
# http://bugs.debian.org/447601
Patch2:		%{name}-%{version}-lfs.patch
# http://bugs.debian.org/353789
Patch3:		%{name}-%{version}-typo.patch

%description
FDUPES is a program for identifying duplicate files residing within specified
directories.

%prep
%setup -q -n %{name}-%{version}-PR2
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

%build
make %{?_smp_mflags} COMPILER_OPTIONS="$RPM_OPT_FLAGS"

%check
./%{name} testdir
./%{name} --omitfirst testdir
./%{name} --recurse testdir
./%{name} --size testdir
# ... etc..

%install
rm -rf $RPM_BUILD_ROOT

make install INSTALL="%{__install} -p" BIN_DIR=%{_bindir} \
  MAN_BASE_DIR=%{_mandir} DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES
%doc CONTRIBUTORS
%doc README
%doc TODO
%doc %{_mandir}/man1/%{name}.1*
%{_bindir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.50-0.2.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.50-0.2.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.50-0.2.3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.50-0.2.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.50-0.2.1m)
- import from Fedora 11

* Sun Feb 01 2009 Debarshi Ray <rishi@fedoraproject.org> - 1.50-0.1.PR2
- Version bump to 1.50 PR2.
  * Added --noprompt, --recurse and --summarize options
  * Now sorts duplicates (old to new) for consistent order when listing or
    deleting duplicate files.
  * Now tests for early matching of files, which should help speed up the
    matching process when large files are involved.
  * Added warning whenever a file cannot be deleted.
  * Fixed bug where some files would not be closed after failure.
  * Fixed bug where confirmmatch() function wouldn't always deal properly with
    zero-length files.
  * Fixed bug where progress indicator would not be cleared when no files were
    found.
- Inclusion of string.h now added by upstream.
- Added patch to fix file comparisons from Debian. (Debian BTS #213385)
- Added patch to enable large file support on 32-bit systems from Debian.
  (Debian BTS #447601)
- Added patch to fix typo in the online manual page from Debian. (Debian BTS
  #353789)

* Tue Feb 19 2008 Release Engineering <rel-eng@fedoraproject.org> - 1.40-12
- Autorebuild for gcc-4.3.

* Thu Dec 27 2007 Debarshi Ray <rishi@fedoraproject.org> - 1.40-11
- Fixed Makefile to preserve timestamps using 'cp -p'.

* Thu Nov 29 2007 Debarshi Ray <rishi@fedoraproject.org> - 1.40-10
- Release bumped to overcome spurious build.

* Sun Nov 25 2007 Debarshi Ray <rishi@fedoraproject.org> - 1.40-9
- Initial build. Imported SPEC from Rawhide.
- Fixed Makefile to use DESTDIR correctly.
- Fixed sources to include string.h.
