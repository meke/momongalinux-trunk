%global momorel 1

Summary: LDAP authorization module for the Apache HTTP Server
Name: mod_authz_ldap
Version: 0.29
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Daemons
URL: http://authzldap.othello.ch/
Source0: http://authzldap.othello.ch/download/%{name}-%{version}.tar.gz
NoSource: 0
Source1: mod_authz_ldap.conf
Patch1: mod_authz_ldap-0.29-hook.patch
Patch2: mod_authz_ldap-0.29-passlog.patch
Patch4: mod_authz_ldap-0.29-subreq.patch
Patch5: mod_authz_ldap-0.29-apr1x.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: httpd-devel >= 2.4.3, openssl-devel >= 1.0.0, openldap-devel
Requires: httpd-mmn = %(cat %{_includedir}/httpd/.mmn || echo httpd-devel missing)
Requires: openldap
Obsoletes: auth_ldap

%description
The mod_authz_ldap package provides support for authenticating
users of the Apache HTTP server against an LDAP database.
mod_authz_ldap features the ability to authenticate users based on
the SSL client certificate presented, and also supports password
aging, and authentication based on role or by configured filters.

%prep
%setup -q
%patch1 -p1 -b .hook
%patch2 -p1 -b .passlog
%patch4 -p1 -b .subreq
%patch5 -p1 -b .apr1x

%build
libtoolize --copy --force && aclocal && autoconf
export CPPFLAGS="`apu-1-config --includes` -I%{_includedir}/openssl -DLDAP_DEPRECATED=1"
%configure --with-apxs=%{_httpd_apxs} --disable-static --program-transform-name=""
cd module
%{_httpd_apxs} -Wl,-export-symbols-regex -Wl,authz_ldap_module \
         -Wc,-DAUTHZ_LDAP_HAVE_SSL -c -o mod_authz_ldap.la *.c \
         -Wl,-lldap -Wl,-lcrypto

%install
rm -rf $RPM_BUILD_ROOT

make -C tools install DESTDIR=$RPM_BUILD_ROOT
make -C docs install DESTDIR=$RPM_BUILD_ROOT

# install the DSO
mkdir -p $RPM_BUILD_ROOT%{_libdir}/httpd/modules
install -m 755 module/.libs/mod_authz_ldap.so $RPM_BUILD_ROOT%{_libdir}/httpd/modules

# install the conf.d fragment
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d
install -m 644 $RPM_SOURCE_DIR/mod_authz_ldap.conf \
   $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d/authz_ldap.conf.dist

rm -f $RPM_BUILD_ROOT%{_libdir}/httpd/modules/{*.la,*.so.*}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_sysconfdir}/httpd/conf.d/authz_ldap.conf.dist
%{_libdir}/httpd/modules/*.so
%{_bindir}/cert*
%{_mandir}/man1/cert*
%doc ldap/*.schema docs/*.{html,jpg} docs/*.{HOWTO,txt} docs/README
%doc NEWS AUTHORS ChangeLog COPYING

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29
- rebuild against httpd-2.4.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.26-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.26-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.26-8m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-7m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-4m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.26-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.26-2m)
- rebuild against openldap-2.4.8

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.26-1m)
- import from Fedora

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - cat: /usr/include/httpd/.mmn: No such file or directory
- rebuild

* Mon Mar 27 2006 Joe Orton <jorton@redhat.com> 0.26-7
- don't package INSTALL
- define -DLDAP_DEPRECATED=1 in CPPFLAGS

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.26-6.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.26-6.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Dec  5 2005 Joe Orton <jorton@redhat.com> 0.26-6
- link against -lldap and -lcrypto

* Mon Dec  5 2005 Joe Orton <jorton@redhat.com> 0.26-5
- rebuild for httpd 2.2
- fix ssl_var_lookup() use in certmap.c

* Thu Nov 10 2005 Tomas Mraz <tmraz@redhat.com> 0.26-4
- rebuilt against new openssl

* Fri Mar  4 2005 Joe Orton <jorton@redhat.com> 0.26-3
- rebuild

* Mon Oct  4 2004 Joe Orton <jorton@redhat.com> 0.26-2
- fix auth failures when not configured (#134496)

* Fri Jun 18 2004 Joe Orton <jorton@redhat.com> 0.26-1
- update to 0.26, fix build
- add fix for extern/static conflict

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri May 21 2004 Joe Orton <jorton@redhat.com> 0.25-1
- update to 0.25

* Fri Sep  5 2003 Joe Orton <jorton@redhat.com> 0.22-3
- don't log the password on auth failure
- obsolete auth_ldap

* Mon Aug  4 2003 Joe Orton <jorton@redhat.com> 0.22-2
- hook up to ssl_var_lookup() in mod_ssl correctly
- include example conf file

* Mon Jun 30 2003 Joe Orton <jorton@redhat.com> 0.22-1
- Initial build.


