%global momorel 21

Summary: GNU recode
Name: recode
Version: 3.6
Release: %{momorel}m%{?dist}
License: GPLv2+ or LGPLv2+
Group: Applications/Text
URL: http://www.gnu.org/directory/recode.html
Source: ftp://ftp.gnu.org/pub/gnu/recode/recode-%{version}.tar.gz
Patch0: recode.patch
Patch1: recode-bool-bitfield.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info
BuildRequires: coreutils
BuildRequires: flex
BuildRequires: libtool

%description
The GNU recode utility convert files between various character sets.

%package devel
Summary: GNU recode devel
Group: Applications/Text
Requires: %{name} = %{version}

%description devel
cf %{name} package

%prep
%setup -q
%patch0 -p1
%patch1 -b .bitfield~

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

%build
# do not use %%configure macro due to accustomed libtool problem
./configure \
    --prefix=%{_prefix} \
    --bindir=%{_bindir} \
    --libdir=%{_libdir} \
    --includedir=%{_includedir} \
    --mandir=%{_mandir} \
    --infodir=%{_infodir}
%make

%install
rm -rf %{buildroot}

%makeinstall
%find_lang %{name}

rm -rf %{buildroot}%{_infodir}/dir

find %{buildroot} -name "*.la" -delete

%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/recode.info %{_infodir}/dir || :
exit 0

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/recode.info %{_infodir}/dir || :
fi
exit 0

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%{_mandir}/*/*
%{_infodir}/*
%{_bindir}/*
%{_libdir}/*.so.0*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_includedir}/*

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6-21m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6-18m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6-17m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6-15m)
- do not use %%configure macro

* Mon May 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6-14m)
- remove %%ifarch x86_64

* Mon May 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6-13m)
- fix build on x86_64

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6-12m)
- fix libtool issue

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6-11m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6-10m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+ or LGPLv2+

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6-9m)
- fix install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6-8m)
- rebuild against gcc43

* Thu Feb 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6-7m)
- import gcc43 patch from Fedora devel

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6-6m)
- delete libtool library

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.6-5m)
- enable x86_64.

* Sat Jan 15 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.6-4m)
- License is GPL for programs,  LGPL for library.

* Sun Jun 13 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.6-3m)
- import patch from FC2

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (3.6-2k)

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.5-6k)

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (3.5-4k)
- No docs could be excutable :-p

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Mon Sep 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (3.5-2k)
- patiri

* Thu Sep 06 2001 Stefan van der Eijk <stefan@eijk.nu> 3.5-9mdk
- BuildRequires:	flex
- Copyright --> License

* Tue Jul  3 2001 Pixel <pixel@mandrakesoft.com> 3.5-8mdk
- do not build .so's

* Tue Nov  7 2000 Pixel <pixel@mandrakesoft.com> 3.5-7mdk
- remove dir in infodir

* Tue Nov  7 2000 Pixel <pixel@mandrakesoft.com> 3.5-6mdk
- add require for -devel

* Wed Jul 19 2000 Pixel <pixel@mandrakesoft.com> 3.5-5mdk
- macorziation, BM

* Tue Apr 18 2000 Pixel <pixel@mandrakesoft.com> 3.5-4mdk
- ensure /usr/info/dir is not there

* Fri Apr 07 2000 Christopher Molnar <molnarc@mandrakesoft.com> 3.5-3mdk
- added missing doc files

* Tue Mar 28 2000 Pixel <pixel@mandrakesoft.com> 3.5-2mdk
- fix a lot of missing % files

* Sat Mar 25 2000 Pixel <pixel@mandrakesoft.com> 3.5-1mdk
- new group
- new version (is that ok Kai Nielsen? :)

* Mon Jan  3 2000 Frederic Lepied <flepied@mandrakesoft.com> 3.4-9mdk
- fix the owner of files.

* Tue Jul 22 1999 Thierry Vignaud <tvignaud@mandrakesoft.com>
- add french description

* Tue Jul 13 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>

- Bzip2 sources.

* Mon Jul 12 1999 Pablo Saratxaga <pablo@mandrakesoft.com>
- quick adaptation to Mandrake

* Fri Dec 25 1998 Peter Soos <sp@osb.hu>
- Corrected the file and directory attributes

* Tue Jun 23 1998 Peter Soos <sp@osb.hu>
- Using %attr

* Tue Dec 12 1997 Peter Soos <sp@osb.hu>
- Recompiled under RedHat Linux 5.0
- Now we use BuildRoot
