%global         momorel 1

Name:           colordiff
Version:        1.0.13
Release:        %{momorel}m%{?dist}
Summary:        Color terminal highlighter for diff files

Group:          Applications/Text
License:        GPLv2+
URL:            http://www.colordiff.org/
Source0:        http://www.colordiff.org/%{name}-%{version}.tar.gz
NoSource:       0
# Non-upstream, better default colors for Fedora default desktop themes
Patch0:         %{name}-1.0.6-colors.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
# xmlto, docbook-dtds for patch1
BuildRequires:  xmlto
BuildRequires:  docbook-dtd412-xml
Requires:       lzma
Requires:       bzip2
Requires:       gzip
Requires:       less
Requires:       diffutils
Requires:       which
Provides:       cdiff

%description
Colordiff is a wrapper for diff and produces the same output but with
pretty syntax highlighting.  Color schemes can be customized.

%prep
%setup -q
%patch0 -p1
mv colordiffrc colordiffrc-darkbg ; cp -p colordiffrc-lightbg colordiffrc
f=CHANGES ; iconv -f iso-8859-1 -t utf-8 $f > $f.utf8 ; mv $f.utf8 $f

%build
xmlto -vv man cdiff.xml

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL_DIR=%{_bindir} \
    ETC_DIR=%{_sysconfdir} MAN_DIR=%{_mandir}/man1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BUGS CHANGES colordiffrc-darkbg colordiffrc-lightbg COPYING README
%config(noreplace) %{_sysconfdir}/colordiffrc
%{_bindir}/cdiff
%{_bindir}/colordiff
%{_mandir}/man1/cdiff.1*
%{_mandir}/man1/colordiff.1*

%changelog
* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.13-1m)
- update to 1.0.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.9-2m)
- full rebuild for mo7 release

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-1m)
- import from Fedora devel

* Wed Jul 29 2009 Ville Skyttä <ville.skytta at iki.fi> - 1.0.9-3
- Update cdiff xz patch to use xz also for lzma files.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.9-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jul 21 2009 Ville Skyttä <ville.skytta at iki.fi> - 1.0.9-1
- Update to 1.0.9; wget 1.11, lzma and destdir patches applied upstream.
- Patch cdiff for xz support.

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.8a-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jan 26 2009 Ville Skyttä <ville.skytta at iki.fi> - 1.0.8a-2
- Fix man page permissions.

* Mon Jan 26 2009 Ville Skyttä <ville.skytta at iki.fi> - 1.0.8a-1
- 1.0.8a.
- Patch Makefile for DESTDIR support.
- Patch cdiff for lzma support, man page improvements.

* Thu Apr 10 2008 Ville Skyttä <ville.skytta at iki.fi> - 1.0.7-3
- Patch to work around wget 1.11 regression, prefer curl over wget (#441862).
- Drop disttag.

* Tue Nov  6 2007 Ville Skyttä <ville.skytta at iki.fi> - 1.0.7-2
- Upstream brown paper bag 1.0.7 re-release.

* Tue Nov  6 2007 Ville Skyttä <ville.skytta at iki.fi> - 1.0.7-1
- 1.0.7.

* Sat Sep 29 2007 Ville Skyttä <ville.skytta at iki.fi> - 1.0.6a-4
- Requires: which

* Mon Aug 13 2007 Ville Skyttä <ville.skytta at iki.fi> - 1.0.6a-3
- License: GPLv2+

* Sat Jul  7 2007 Ville Skyttä <ville.skytta at iki.fi> - 1.0.6a-2
- Convert docs to UTF-8.

* Sat May 12 2007 Ville Skyttä <ville.skytta at iki.fi> - 1.0.6a-1
- 1.0.6a.

* Wed Apr  4 2007 Ville Skyttä <ville.skytta at iki.fi> - 1.0.6-2
- 1.0.6.

* Wed Aug 30 2006 Ville Skyttä <ville.skytta at iki.fi> - 1.0.5-2
- Drop no longer needed Obsoletes.

* Sat May 21 2005 Ville Skyttä <ville.skytta at iki.fi> - 1.0.5-1
- Add version to cdiff Obsoletes (Matthias Saou).
- Require diffutils (Matthias Saou, Matthew Miller).

* Mon Mar 28 2005 Ville Skyttä <ville.skytta at iki.fi> - 1.0.5-0.1
- 1.0.5.

* Thu Mar 17 2005 Ville Skyttä <ville.skytta at iki.fi> - 1.0.4-0.1
- Disable banner display in default configs.
- Drop unnecessary Epochs.

* Fri Aug 13 2004 Ville Skyttä <ville.skytta at iki.fi> - 0:1.0.4-0.fdr.3
- Apply upstream fix for context diff detection.

* Thu Aug 12 2004 Ville Skyttä <ville.skytta at iki.fi> - 0:1.0.4-0.fdr.2
- Use lightbg as the default scheme and make it work better with dark
  backgrounds too.

* Sat Jul 17 2004 Ville Skyttä <ville.skytta at iki.fi> - 0:1.0.4-0.fdr.1
- First build.
- Include cdiff wrapper.
