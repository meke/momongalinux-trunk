%global         momorel 1
Name:           usbredir
Version:        0.7
Release:        %{momorel}m%{?dist}
Summary:        USB network redirection protocol libraries
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://spice-space.org/page/UsbRedir
Source0:        http://spice-space.org/download/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRequires:  libusb1-devel >= 1.0.9

%description
The usbredir libraries allow USB devices to be used on remote and/or virtual
hosts over TCP.  The following libraries are provided:

usbredirparser:
A library containing the parser for the usbredir protocol

usbredirhost:
A library implementing the USB host side of a usbredir connection.
All that an application wishing to implement a USB host needs to do is:
* Provide a libusb device handle for the device
* Provide write and read callbacks for the actual transport of usbredir data
* Monitor for usbredir and libusb read/write events and call their handlers


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        server
Summary:        Simple USB host TCP server
Group:          System Environment/Daemons
License:        GPLv2+
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    server
A simple USB host TCP server, using libusbredirhost.


%prep
%setup -q

%build
%configure --disable-static
%make  V=1


%install
make install DESTDIR=%{buildroot}
rm %{buildroot}%{_libdir}/libusbredir*.la


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING.LIB README TODO 
%{_libdir}/libusbredir*.so.*

%files devel
%defattr(-,root,root,-)
%doc usb-redirection-protocol.txt README.multi-thread
%{_includedir}/usbredir*.h
%{_libdir}/libusbredir*.so
%{_libdir}/pkgconfig/libusbredir*.pc

%files server
%defattr(-,root,root,-)
%doc COPYING
%{_sbindir}/usbredirserver
%{_mandir}/man1/usbredirserver.1*


%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-1m)
- update 0.7

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- update 0.6 for new version of spice-gtk

* Mon Oct  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-1m)
- update 0.4.4

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-1m)
- reimport from fedora

* Mon Sep 19 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3-1m)
- import from Fedora

* Thu Jul 14 2011 Hans de Goede <hdegoede@redhat.com> 0.3-1
- Initial Fedora package
