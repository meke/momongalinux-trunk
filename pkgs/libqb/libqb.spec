%global         momorel 1

Name:           libqb
Version:        0.16.0
Release:        %{momorel}m%{?dist}
Summary:        An IPC library for high performance servers

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.libqb.org
Source0:        https://fedorahosted.org/releases/q/u/quarterback/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libtool doxygen procps check-devel

#Requires: <nothing>

%description
libqb provides high performance client server reusable features.
Initially these are IPC and poll.

%prep
%setup -q

%build
%configure --disable-static
make %{?_smp_mflags}

%check
make check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'
rm -rf %{buildroot}/%{_docdir}/*

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING
%{_sbindir}/qb-blackbox
%{_libdir}/libqb.so.*

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release} pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%files          devel
%defattr(-,root,root,-)
%doc COPYING README.markdown
%{_includedir}/qb/
%{_libdir}/libqb.so
%{_libdir}/pkgconfig/libqb.pc
%{_mandir}/man3/qb*3*
%{_mandir}/man8/qb-blackbox.8*

%changelog
* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.0-1m)
- update to 0.16.0

* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.1-1m)
- update 0.11.1

* Sat Oct  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-1m)
- update 0.6.0

* Sat Sep 10 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.1-1m)
- import from Fedora

* Mon Jul 18 2011 Angus Salkeld <asalkeld@redhat.com> - 0.5.1-1
- Rebased to 0.5.1 which includes:
- LOOP: make the return more consistent in qb_loop_timer_expire_time_get()
- LOG: add string.h to qblog.h
- Add a qb_strerror_r wrapper.
- don't let an invalid time stamp provoke a NULL dereference
- LOG: move priority check up to prevent unnecessary format.
- rename README to README.markdown

* Wed Jun 8 2011 Angus Salkeld <asalkeld@redhat.com> - 0.5.0-1
- Rebased to 0.5.0 which includes:
- new logging API
- support for sparc
- coverity fixes

* Tue Feb 8 2011 Angus Salkeld <asalkeld@redhat.com> - 0.4.1-2
- SPEC: improve devel files section
- SPEC: remove global variables

* Mon Jan 31 2011 Angus Salkeld <asalkeld@redhat.com> - 0.4.1-1
- SPEC: add procps to BuildRequire
- SPEC: remove automake and autoconf from BuildRequire
- SPEC: remove call to ./autogen.sh
- SPEC: update to new upstream version 0.4.1
- LOOP: check read() return value
- DOCS: add missing @param on new timeout argument
- BUILD: only set -g and -O options if explicitly requested.
- BUILD: Remove unneccessary check for library "dl"
- BUILD: improve the release build system

* Fri Jan 14 2011 Angus Salkeld <asalkeld@redhat.com> - 0.4.0-2
- remove "." from Summary
- Add "check-devel to BuildRequires
- Add "make check" to check section
- Changed a buildroot to RPM_BUILD_ROOT
- Document alphatag, numcomm and dirty variables.

* Sun Jan 09 2011 Angus Salkeld <asalkeld@redhat.com> - 0.4.0-1
- Initial release
