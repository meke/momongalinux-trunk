%global         momorel 3

Name:           perl-MooseX-Traits
Version:        0.12
Release:        %{momorel}m%{?dist}
Summary:        Automatically apply roles at object creation time
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Traits/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/MooseX-Traits-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-Module-Build-Tiny-version.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Carp
BuildRequires:  perl-Class-Load
BuildRequires:  perl-Cwd
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-Util
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Moose
BuildRequires:  perl-MooseX-Role-Parameterized
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-Test-use-ok
BuildRequires:  perl-Sub-Exporter
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-Requires
Requires:       perl-Carp
Requires:       perl-Class-Load
Requires:       perl-Moose
Requires:       perl-namespace-autoclean
Requires:       perl-Sub-Exporter
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Often you want to create components that can be added to a class
arbitrarily. This module makes it easy for the end user to use these
components. Instead of requiring the user to create a named class with
the desired roles applied, or apply roles to the instance one-by-one, he
can just create a new class from yours with with_traits, and then
instantiate that.

%prep
%setup -q -n MooseX-Traits-%{version}
%patch0 -p1

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install --destdir=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes CONTRIBUTING dist.ini LICENSE META.json README README.md weaver.ini
%{perl_vendorlib}/MooseX/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-2m)
- rebuild against perl-5.18.2

* Sun Nov 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-15m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-14m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-13m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-12m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-11m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-10m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-9m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-8m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-7m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-5m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-4m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-2m)
- rebuild against perl-5.12.1

* Thu May 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Thu May 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-2m)
- rebuild against perl-5.12.0

* Wed Apr  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Sun Feb 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.07-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-2m)
- rebuild against perl-5.10.1

* Tue Jun 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
