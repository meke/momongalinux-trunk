%global momorel 1
Summary: The Typesafe Signal Framework for C++
Name: libsigc++
Version: 2.2.11
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://libsigc.sourceforge.net/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/2.2/%{name}-%{version}.tar.xz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glibc-devel

%description
This library implements a full callback system for use in widget libraries,
abstract interfaces, and general programming. Originally part of the Gtk--
widget set, %name is now a seperate library to provide for more general
use. It is the most complete library of its kind with the ablity to connect
an abstract callback to a class method, function, or function object. It
contains adaptor classes for connection of dissimilar callbacks and has an
ease of use unmatched by other C++ callback libraries.

%package devel
Summary: development tools for the Typesafe Signal Framework for C++ 
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: %name = %version-%{release}
Requires: glibc-devel

%description devel
The %name-devel package contains the static libraries and header files
needed for development with %name.

%prep
%setup -q

%build
%configure --enable-silent-rules \
    --disable-static \
    --enable-documentation
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS README NEWS ChangeLog TODO
%{_libdir}/libsigc-2.0.so.*
%exclude %{_libdir}/libsigc-2.0.la

%files devel
%defattr(-,root,root)
%{_libdir}/libsigc-2.0.so
%{_libdir}/pkgconfig/sigc++-2.0.pc
%dir %{_libdir}/sigc++-2.0
%dir %{_libdir}/sigc++-2.0/include
%{_libdir}/sigc++-2.0/include/sigc++config.h
%{_includedir}/sigc++-2.0

%{_datadir}/devhelp/books/libsigc++-2.0/libsigc++-2.0.devhelp2

%dir %{_datadir}/doc/libsigc++-2.0
%{_datadir}/doc/libsigc++-2.0/index.html
%{_datadir}/doc/libsigc++-2.0/reference
%{_datadir}/doc/libsigc++-2.0/tutorial
%{_datadir}/doc/libsigc++-2.0/images

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.9-3m)
- rebuild for new GCC 4.6

* Wed Mar  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.9-2m)
- remove gcc46 patch, which is no longer needed.

* Tue Mar  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.9-1m)
- update to 2.2.9
- comment out patch0 (need it?)

* Fri Feb  4 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.8-4m)
- add patch for GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.8-2m)
- full rebuild for mo7 release

* Sun Jun  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.8-1m)
- update to 2.2.8

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.7-1m)
- update to 2.2.7

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.6-1m)
- update to 2.2.6

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.5-1m)
- update to 2.2.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.4.2-1m)
- update to 2.2.4.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.3-2m)
- rebuild against rpm-4.6

* Thu Oct 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.2-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-3m)
- %%NoSource -> NoSource

* Fri Feb  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-2m)
- added gcc43 patch

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Thu Sep 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.18-1m)
- update to 2.0.18

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.17-2m)
- delete libtool library

* Tue Apr 18 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.17-1m)
- update to 2.0.17

* Mon May 16 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.12-1m)
- update to 2.0.12
- come back to MAIN for inkscape

* Mon Nov 08 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.6-1m)
- update to 2.0.6

* Mon Oct 25 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5

* Wed Jul 7 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.0.0-2m)
- rpm42 compatibility
- patch by Yoshi Doi

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Sat Mar 20 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.5-2m)
- change docdir %%defattr

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.5-1m)
- version 1.2.5

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.4-1m)
- version 1.2.4

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.3-1m)
- version 1.2.3

* Tue Dec 31 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2-1m)
- version 1.2.2

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.0-1m)
- version 1.2.0

* Thu Aug 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.13-1m)
- version 1.1.13

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.11-1m)
- version 1.1.11

* Tue Mar 12 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.0.4-2k)

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.0.3-4k)
- no more ifarch alpha.

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.0.3
- K2K

* Tue Oct 17 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.0.1

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Jun 29 2000 Toru Hoshina <t@kondara.org>
- rebild against g++ 2.96.

* Wed May  3 2000 Koichi Asano <koichi@comm.info.eng.osaka-cu.ac.jp>
- to be no source

* Wed Jan 19 2000 Dmitry V. Levin <ldv@fandra.org>
- minor attr fix
- removed unnecessary curly braces
- fixed Herbert's adjustement

* Sat Jan 15 2000 Dmitry V. Levin <ldv@fandra.org>
- minor package dependence fix

* Sat Dec 25 1999 Herbert Valerio Riedel <hvr@gnu.org>
- fixed typo of mine
- added traditional CUSTOM_RELEASE stuff
- added SMP support

* Thu Dec 23 1999 Herbert Valerio Riedel <hvr@gnu.org>
- adjusted spec file to get tests.Makefile and examples.Makefile from scripts/

* Fri Oct 22 1999 Dmitry V. Levin <ldv@fandra.org>
- split into three packages: %name, %name-devel and %name-examples

* Thu Aug 12 1999 Karl Nelson <kenelson@ece.ucdavis.edu>
- updated source field and merged conflicts between revisions.

* Tue Aug 10 1999 Dmitry V. Levin <ldv@fandra.org>
- updated Prefix and BuildRoot fields

* Thu Aug  5 1999 Herbert Valerio Riedel <hvr@hvrlab.dhs.org>
- made sure configure works on all alphas

* Wed Jul  7 1999 Karl Nelson <kenelson@ece.ucdavis.edu>
- Added autoconf macro for sigc.

* Fri Jun 11 1999 Karl Nelson <kenelson@ece.ucdavis.edu>
- Made into a .in to keep version field up to date 
- Still need to do release by hand

* Mon Jun  7 1999 Dmitry V. Levin <ldv@fandra.org>
- added Vendor and Packager fields

* Sat Jun  5 1999 Dmitry V. Levin <ldv@fandra.org>
- updated to 0.8.0

* Tue Jun  1 1999 Dmitry V. Levin <ldv@fandra.org>
- initial revision


