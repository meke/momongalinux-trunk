%global momorel 1

Summary: Symbolic Computation Program
Name: 	 maxima
Version: 5.30.0

### include local configuration
%{?include_specopt}
### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpmbuild/specopt/maxima.specopt and edit it.
%{?!do_test: %global do_test 1}


Release: %{momorel}m%{?dist}
License: GPLv2
Group:	 Applications/Engineering 
URL: 	 http://maxima.sourceforge.net/
Source:	 http://downloads.sourceforge.net/sourceforge/maxima/maxima-%{version}%{?beta}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ppc sparcv9

%define maxima_ver %{version}%{?beta}
%define emacs_sitelisp  %{_datadir}/emacs/site-lisp/
%define xemacs_sitelisp %{_datadir}/xemacs/site-packages/lisp/
%define texmf %{_datadir}/texmf

%ifarch %{ix86} x86_64
%define default_lisp clisp
#define _enable_sbcl --enable-sbcl
%define _enable_clisp --enable-clisp 
#define _enable_gcl --enable-gcl
#define _enable_ecl --enable-ecl
%endif

%ifarch ppc
%define default_lisp sbcl
%define _enable_sbcl --enable-sbcl
%if 0%{?fedora}
# clisp: http://bugzilla.redhat.com/166347 (resolved) - clisp/ppc (still) awol.
#define _enable_clisp --enable-clisp 
%define _enable_gcl --enable-gcl
%endif
%endif

%ifarch sparcv9
%define default_lisp sbcl
%define _enable_sbcl --enable-sbcl
%endif

%if "x%{?_enable_cmucl}" == "x%{nil}"
Obsoletes: %{name}-runtime-cmucl < %{version}-%{release}
%endif
%if "x%{?_enable_gcl}" == "x%{nil}"
Obsoletes: %{name}-runtime-gcl < %{version}-%{release}
%endif
%if "x%{?_enable_sbcl}" == "x%{nil}"
Obsoletes: %{name}-runtime-sbcl < %{version}-%{release}
%endif
%if "x%{?_enable_ecl}" == "x%{nil}"
Obsoletes: %{name}-runtime-ecl < %{version}-%{release}
%endif

Source1: maxima.png
Source2: xmaxima.desktop
Source6: maxima-modes.el

## Other maxima reference docs
Source10: http://starship.python.net/crew/mike/TixMaxima/macref.pdf
Source11: http://maxima.sourceforge.net/docs/maximabook/maximabook-19-Sept-2004.pdf
NoSource: 11

# Inhibit automatic compressing of info files. 
# Compressed info files break maxima's internal help.
%define __spec_install_post %{nil} 
# debuginfo.list ends up empty/blank anyway. disable
%define debug_package   %{nil}

# upstream langpack upgrades, +Provides too? -- Rex
Obsoletes: %{name}-lang-es < %{version}-%{release}
Obsoletes: %{name}-lang-es-utf8 < %{version}-%{release}
Obsoletes: %{name}-lang-pt < %{version}-%{release}
Obsoletes: %{name}-lang-pt-utf8 < %{version}-%{release}
Obsoletes: %{name}-lang-pt_BR < %{version}-%{release}
Obsoletes: %{name}-lang-pt_BR-utf8 < %{version}-%{release}

BuildRequires: desktop-file-utils
BuildRequires: time
# texi2dvi
BuildRequires: texinfo-tex
BuildRequires: tetex-latex
# /usr/bin/wish
BuildRequires: tk

Requires: %{name}-runtime%{?default_lisp:-%{default_lisp}} = %{version}-%{release}
Requires: gnuplot
Requires: rlwrap
Requires(post): /sbin/install-info
Requires(postun): /sbin/install-info

%description
Maxima is a full symbolic computation program.  It is full featured
doing symbolic manipulation of polynomials, matrices, rational
functions, integration, Todd-coxeter, graphing, bigfloats.  It has a
symbolic debugger source level debugger for maxima code.  Maxima is
based on the original Macsyma developed at MIT in the 1970's.

%package gui
Summary: Tcl/Tk GUI interface for %{name}
Group:	 Applications/Engineering 
Requires: %{name} = %{version}-%{release} 
Obsoletes: %{name}-xmaxima < %{version}-%{release}
Requires: tk
Requires: xdg-utils
%description gui
Tcl/Tk GUI interface for %{name}

%package src 
Summary: %{name} lisp source code 
Group:   Applications/Engineering
Requires: %{name} = %{version}-%{release}
%description src 
%{name} lisp source code.

%if "x%{?_enable_clisp:1}" == "x1"
# to workaround mysterious(?) "cpio: MD5 sum mismatch" errors when installing this subpkg
%define __prelink_undo_cmd %{nil}
#define _with_clisp_runtime --with-clisp-runtime=%{_libdir}/clisp/base/lisp.run
%package runtime-clisp
Summary: Maxima compiled with clisp
Group:	 Applications/Engineering
BuildRequires: clisp-devel
Requires: clisp
Requires: %{name} = %{version}-%{release}
Obsoletes: maxima-exec-clisp < %{version}-%{release}
Provides: %{name}-runtime = %{version}-%{release}
%description runtime-clisp
Maxima compiled with Common Lisp (clisp) 
%endif

%if "x%{?_enable_cmucl:1}" == "x1"
%define _with_cmucl_runtime --with-cmucl-runtime=%{_prefix}/lib/cmucl/bin/lisp
%package runtime-cmucl
Summary: Maxima compiled with CMUCL
Group:	 Applications/Engineering 
BuildRequires: cmucl 
# needed dep somewhere around cmucl-20a -- Rex
Requires: cmucl
Requires:  %{name} = %{version}-%{release}
Obsoletes: maxima-exec-cmucl < %{version}-%{release}
Provides:  %{name}-runtime = %{version}-%{release}
%description runtime-cmucl
Maxima compiled with CMU Common Lisp (cmucl) 
%endif

%if "x%{?_enable_gcl:1}" == "x1"
%package runtime-gcl
Summary: Maxima compiled with GCL
Group:   Applications/Engineering
BuildRequires: gcl
Requires:  %{name} = %{version}-%{release}
Obsoletes: maxima-exec-gcl < %{version}-%{release}
Provides:  %{name}-runtime = %{version}-%{release}
%description runtime-gcl
Maxima compiled with Gnu Common Lisp (gcl)
%endif

%if "x%{?_enable_sbcl:1}" == "x1"
%package runtime-sbcl
Summary: Maxima compiled with SBCL 
Group:   Applications/Engineering
BuildRequires: sbcl
# requires the same sbcl it was built against
%global sbcl_vr %(sbcl --version 2>/dev/null | cut -d' ' -f2)
%if "x%{?sbcl_vr}" != "x%{nil}" 
Requires: sbcl = %{sbcl_vr}
%else
Requires: sbcl
%endif
Requires: %{name} = %{version}-%{release}
Obsoletes: maxima-exec-sbcl < %{version}-%{release}
Provides: %{name}-runtime = %{version}-%{release}
%description runtime-sbcl
Maxima compiled with Steel Bank Common Lisp (sbcl).
%endif

%if "x%{?_enable_ecl:1}" == "x1"
%package runtime-ecl
Summary: Maxima compiled with ECL 
Group:   Applications/Engineering
BuildRequires: ecl
# workaround missing requires in ecl pkg(?)
BuildRequires: libffi-devel
%global ecllib %(ecl -eval "(princ (SI:GET-LIBRARY-PATHNAME))" -eval "(quit)")
Requires: ecl
Requires: %{name} = %{version}-%{release}
Obsoletes: maxima-exec-ecl < %{version}-%{release}
Provides: %{name}-runtime = %{version}-%{release}
%description runtime-ecl
Maxima compiled with Embeddable Common-Lisp (ecl).
%endif

%prep
%setup -q  -n %{name}%{!?cvs:-%{version}%{?beta}}

# Extra docs
install -p -m644 %{SOURCE10} .

sed -i -e 's|@ARCH@|%{_target_cpu}|' src/maxima.in

sed -i -e 's:/usr/local/info:/usr/share/info:' \
  interfaces/emacs/emaxima/maxima.el
sed -i -e \
  's/(defcustom\s+maxima-info-index-file\s+)(\S+)/$1\"maxima.info-16\"/' \
  interfaces/emacs/emaxima/maxima.el

# remove CVS crud
find -name CVS -type d | xargs --no-run-if-empty rm -r


%build
%configure \
  %{?default_lisp:--with-default-lisp=%{default_lisp} } \
  %{?_enable_clisp} %{!?_enable_clisp: --disable-clisp } %{?_with_clisp_runtime} \
  %{?_enable_cmucl} %{!?_enable_cmucl: --disable-cmucl } %{?_with_cmucl_runtime} \
  %{?_enable_gcl}   %{!?_enable_gcl:   --disable-gcl } \
  %{?_enable_sbcl}  %{!?_enable_sbcl:  --disable-sbcl } \
  %{?_enable_ecl}   %{!?_enable_ecl:   --disable-ecl } \
  --enable-lang-es --enable-lang-es-utf8 \
  --enable-lang-pt --enable-lang-pt-utf8 \
  --enable-lang-pt_BR --enable-lang-pt_BR-utf8 

%make 

# docs
install -D -p -m644 %{SOURCE11} doc/maximabook/maxima.pdf

pushd doc/intromax
 pdflatex intromax.tex
popd

#   Allow ecl to "require" maxima. This is required by sagemath ecl runtime.
%if "x%{?_enable_ecl:1}" == "x1"
pushd src
    mkdir ./lisp-cache
    ecl  \
	-eval '(require `asdf)' \
	-eval '(setf asdf::*user-cache* (truename "./lisp-cache"))' \
	-eval '(load "maxima-build.lisp")' \
	-eval '(asdf:make-build :maxima :type :fasl :move-here ".")' \
	-eval '(quit)' 
popd
%endif

%if %{do_test}
%check 
make -k check
%endif

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

%if "x%{?_enable_ecl:1}" == "x1"
install -D -m755 src/maxima.fasb %{buildroot}%{ecllib}/maxima.fas
%endif

# app icon
install -p -D -m644 %{SOURCE1} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/maxima.png

desktop-file-install \
  --dir="%{buildroot}%{_datadir}/applications" \
  --vendor= \
  %{SOURCE2} 

# (x)emacs
install -D -m644 -p %{SOURCE6} %{buildroot}%{_datadir}/maxima/%{maxima_ver}/emacs/site_start.d/maxima-modes.el

for dir in %{emacs_sitelisp} %{xemacs_sitelisp} ; do
  install -d -m755 %{buildroot}$dir/{,site-start.d}
  ln -s %{_datadir}/maxima/%{maxima_ver}/emacs %{buildroot}$dir/maxima
  for file in %{buildroot}%{_datadir}/maxima/%{maxima_ver}/emacs/*.el ; do
    touch `dirname $file`/`basename $file .el`.elc
  done
  ln -s %{_datadir}/maxima/%{maxima_ver}/emacs/site_start.d/maxima-modes.el %{buildroot}$dir/site-start.d/
  touch %{buildroot}$dir/site-start.d/maxima-modes.elc
done

# emaxima LaTeX style (%ghost)
install -d %{buildroot}%{texmf}/tex/latex/
ln -sf  %{_datadir}/maxima/%{maxima_ver}/emacs \
        %{buildroot}%{texmf}/tex/latex/emaxima

## unwanted/unpackaged files
rm -f %{buildroot}%{_infodir}/dir
# docs
rm -rf %{buildroot}%{_datadir}/maxima/%{maxima_ver}/doc/{contributors,implementation,misc,maximabook,EMaximaIntro.ps}

# _enable_gcl: debuginfo (sometimes?) fails to get auto-created, so we'll help out
touch debugfiles.list


%post
/sbin/install-info %{_infodir}/maxima.info %{_infodir}/dir ||:
[ -x /usr/bin/texhash ] && /usr/bin/texhash 2> /dev/null ||:

%postun
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/maxima.info %{_infodir}/dir ||:
  [ -x /usr/bin/texhash ] && /usr/bin/texhash 2> /dev/null ||:
fi

%post gui
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun gui
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &> /dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
fi

%posttrans gui
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :

%triggerin -- emacs-common
if [ -d %{emacs_sitelisp} ]; then
  rm -rf %{emacs_sitelisp}/maxima   
  ln -sf %{_datadir}/maxima/%{maxima_ver}/emacs  %{emacs_sitelisp}/maxima ||:
fi
ln -sf %{_datadir}/maxima/%{maxima_ver}/emacs/site_start.d/maxima-modes.el %{emacs_sitelisp}/site-start.d/ ||:

%triggerin -- xemacs-common
if [ -d %{xemacs_sitelisp} ]; then
  rm -rf %{xemacs_sitelisp}/maxima
  ln -sf %{_datadir}/maxima/%{maxima_ver}/emacs  %{xemacs_sitelisp}/maxima ||:
fi
ln -sf %{_datadir}/maxima/%{maxima_ver}/emacs/site_start.d/maxima-modes.el %{xemacs_sitelisp}/site-start.d/ ||:

%triggerun -- emacs-common
if [ $2 -eq 0 ]; then
 rm -f %{emacs_sitelisp}/maxima || :
 rm -f %{emacs_sitelisp}/site-start.d/maxima-modes.el* ||:
fi

%triggerun -- xemacs-common
if [ $2 -eq 0 ]; then
 rm -f %{xemacs_sitelisp}/maxima || :
 rm -f %{xemacs_sitelisp}/site-start.d/maxima-modes.el* ||:
fi

%triggerin -- tetex-latex,texlive-latex
if [ -d %{texmf}/tex/latex ]; then
  rm -rf %{texmf}/tex/latex/emaxima ||:
  ln -sf %{_datadir}/maxima/%{maxima_ver}/emacs %{texmf}/tex/latex/emaxima ||:
  %{_bindir}/texhash 2> /dev/null ||:
fi

%triggerun -- tetex-latex,texlive-latex
if [ $2 -eq 0 ]; then
  rm -f %{texmf}/tex/latex/emaxima ||:
fi


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README README.lisps
%doc doc/misc/ doc/implementation/
%doc doc/intromax/intromax.pdf
%doc doc/maximabook/maxima.pdf
%{_bindir}/maxima
%{_bindir}/rmaxima
%dir %{_datadir}/maxima
%dir %{_datadir}/maxima/%{maxima_ver}
%{_datadir}/maxima/%{maxima_ver}/[a-c,f-r,t-w,y-z,A-Z]*
%{_datadir}/maxima/%{maxima_ver}/demo/
%dir %{_datadir}/maxima/%{maxima_ver}/doc/
%dir %{_datadir}/maxima/%{maxima_ver}/doc/html/
%{_datadir}/maxima/%{maxima_ver}/doc/html/figures/
%doc %lang(en) %{_datadir}/maxima/%{maxima_ver}/doc/html/*.h*
%doc %lang(en) %{_datadir}/maxima/%{maxima_ver}/doc/share/
%doc %lang(es) %{_datadir}/maxima/%{maxima_ver}/doc/html/es/
%doc %lang(es) %{_datadir}/maxima/%{maxima_ver}/doc/html/es.utf8/
%doc %lang(pt) %{_datadir}/maxima/%{maxima_ver}/doc/html/pt/
%doc %lang(pt) %{_datadir}/maxima/%{maxima_ver}/doc/html/pt.utf8/
%doc %lang(pt_BR) %{_datadir}/maxima/%{maxima_ver}/doc/html/pt_BR/
%doc %lang(pt_BR) %{_datadir}/maxima/%{maxima_ver}/doc/html/pt_BR.utf8/
%{_datadir}/maxima/%{maxima_ver}/share/
%dir %{_libdir}/maxima/
%dir %{_libdir}/maxima/%{maxima_ver}/
%{_libexecdir}/maxima
%{_infodir}/*maxima*
%lang(es) %{_infodir}/es*
%lang(pt) %{_infodir}/pt/
%lang(pt) %{_infodir}/pt.utf8/
%lang(pt_BR) %{_infodir}/pt_BR*
%{_mandir}/man1/maxima.*
%dir %{_datadir}/maxima/%{maxima_ver}/emacs
%{_datadir}/maxima/%{maxima_ver}/emacs/emaxima.*
%{_datadir}/maxima/%{maxima_ver}/emacs/imaxima.*
%{_datadir}/maxima/%{maxima_ver}/emacs/*.el
%ghost %{_datadir}/maxima/%{maxima_ver}/emacs/*.elc
%dir %{_datadir}/maxima/%{maxima_ver}/emacs/site_start.d/
%{_datadir}/maxima/%{maxima_ver}/emacs/site_start.d/*.el
%ghost %{emacs_sitelisp}
%ghost %{xemacs_sitelisp}
%ghost %{texmf}/tex/latex/emaxima

%files src
%defattr(-,root,root,-)
%{_datadir}/maxima/%{maxima_ver}/src/

%files gui
%defattr(-,root,root,-)
%{_bindir}/xmaxima
%{_datadir}/maxima/%{maxima_ver}/xmaxima/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/*/*

%if "x%{?_enable_clisp:1}" == "x1"
%files runtime-clisp
%defattr(-,root,root,-)
%{_libdir}/maxima/%{maxima_ver}/binary-clisp
%endif

%if "x%{?_enable_cmucl:1}" == "x1"
%files runtime-cmucl
%defattr(-,root,root,-)
%{_libdir}/maxima/%{maxima_ver}/binary-cmucl
%endif

%if "x%{?_enable_gcl:1}" == "x1"
%files runtime-gcl
%defattr(-,root,root,-)
%{_libdir}/maxima/%{maxima_ver}/binary-gcl
%endif

%if "x%{?_enable_sbcl:1}" == "x1"
%files runtime-sbcl
%defattr(-,root,root,-)
%{_libdir}/maxima/%{maxima_ver}/binary-sbcl
%endif

%if "x%{?_enable_ecl:1}" == "x1"
%files runtime-ecl
%defattr(-,root,root,-)
%{_libdir}/maxima/%{version}/binary-ecl
%{ecllib}/maxima.fas
%endif


%changelog
* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.30.0-1m)
- update to 5.30.0

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.28.0-2m)
- remove fedora from vendor (desktop-file-install)

* Mon Sep  3 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.28.0-1m)
- update to 5.28.0

* Sun Jul 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.27.0-2m)
- add %%check

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.27.0-1m)
- reimport from fedora
- use clisp as default

* Mon Dec 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.25.1-1m)
- re-import from fedora
- now sbcl is default
- disable cmucl backend

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.21.1-6m)
- rename elisp-emaxima to emacs-emaxima

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.21.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.21.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.21.1-3m)
- full rebuild for mo7 release

* Sat Aug 21 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.21.1-2m)
- fix "/sbin/install-info --delete" issue

* Fri May 21 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.21.1-1m)
- update 5.21.1

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.19.2-5m)
- rebuild against readline6

* Fri Mar 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.19.2-4m)
- merge emaxima-emacs into elisp-emaxima
- kill emaxima-xemacs

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.19.2-3m)
- rebuild against libsigsegv-2.7 (clisp-2.48-5m)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.19.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 15 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.19.2-1m)
- update to 5.19.2
- enable cmucl subpackage on ix86, which seems to be faster than clisp

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.18.1-1m)
- update to 5.18.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.15.0-2m)
- rebuild against rpm-4.6

* Thu Jun 19 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.15.0-1m)
- update 5.15.0
-- delete unused patches
-- import gcl_setarch patch from fc-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.14.0-2m)
- rebuild against gcc43

* Sat Mar 01 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.14.0-1m)
- update to 5.14.0

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.12.0-4m)
- %%NoSource -> NoSource

* Thu Jan  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.12.0-3m)
- good-bye maxima-exec_cmucl

* Thu Jan  3 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.12.0-2m)
- disable cmucl support on ix86; cmucl can't work with exec-shield
- import workaround from fc-devel to avoid "cpio: MD5 sum mismatch" error

* Fri Jun 01 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.12.0-1m)
- update to 5.12.0

* Sat Jan 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11.0-1m)
- update to 5.11.0

* Tue Oct 03 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.10.0-1m)
- update to 5.10.0

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.9.3-4m)
- remove category Application

* Fri Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.9.3-3m)
- rebuild against readline-5.0

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.9.3-2m)
- delete duplicated dir

* Wed Apr 26 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.9.3-1m)
- update to 5.9.3
- made emacsen interface subpackages (elisp-emaxima, emaxima-emacs, emaxima-xemacs)
- import maxima-5.9.2-emaxima.patch, maxima-mode.el from Fedora Extra (5.9.3-1.fc6)

* Wed Jan 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.9.2-1m)
- version 5.9.2

* Thu Feb 17 2005 mutecat <mutecat@momonga-linux.org>
- (5.9.1-3m)
- arrange ppc without cmucl

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.9.1-2m)
- add Education to Categories of xmaxima.desktop for KDE

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.9.1-1m)
- version 5.9.1 has been released.
- use %%build_gcl etc. to control whether to build with specific lisps
- - set %%build_gcl to 0 because gcl_binary fails to build...Please fix!
- not make docs and download it from the web site
- - latex procedure fails because build requires some *.sty which are missing

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (5.9.0-8m)
- rebuild against ncurses 5.3.

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (5.9.0-7m)
- revised spec for enabling rpm 4.2.

* Tue Dec 30 2003 Kenta MURATA <muraken2@nifty.com>
- (5.9.0-6m)
- copy info for ? command.

* Mon Dec 29 2003 Kenta MURATA <muraken2@nifty.com>
- (5.9.0-5m)
- %%NoSource.
- --enable-gcl and --with-default-lisp=gcl.
- includes some documents.

* Mon Dec 29 2003 Kenta MURATA <muraken2@nifty.com>
- (5.9.0-4m)
- --enable-gcl
- %%NoSource.

* Thu Mar  6 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.9.0-3m)
- fix URL
- add and tmp comment out loop BuildRequires: maxima-xmaxima

* Fri Feb 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (5.9.0-2m)
- [Momonga-devel.ja:01389] thanks.

* Tue Feb 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (5.9.0-1m)
  update to 5.9.0

* Thu Nov 14 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (5.9.0-0.3.1m)
- initial import to Momonga.

* Sun Sep  8 2002 James Amundson <amundson@fnal.gov> 
- Initial build.
