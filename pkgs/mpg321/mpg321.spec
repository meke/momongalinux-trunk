%global momorel 4
%global srcname %{name}-%{version}-1

Summary: An MPEG audio player
Name: mpg321
Version: 0.2.12
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
URL: http://mpg321.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/mpg321/%{srcname}.tar.gz
NoSource: 0
Source2: mp3license
License: GPLv2+
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libao-devel >= 1.0.0
BuildRequires: libid3tag-devel
BuildRequires: libmad
BuildRequires: zlib-devel
Provides: mpg123
Obsoletes: mpg123

%description
mpg321 is a very popular command-line mp3 player. mpg321 is used for
frontends, as an mp3 player and as an mp3 to wave file decoder
(primarily for use with CD-recording software.).

%prep
%setup -q -n %{srcname}
cp %{SOURCE2} .

%build
CFLAGS="%{optflags}" %configure --with-ao
%make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS ChangeLog COPYING INSTALL README README.remote TODO THANKS
%doc mp3license NEWS
%{_bindir}/mpg321
%{_bindir}/mpg123
%{_mandir}/man1/mpg321.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.12-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.12-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.12-2m)
- full rebuild for mo7 release

* Sat Jul  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.12-1m)
- update to 0.2.12-1

* Wed May 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.11-2m)
- BuildRequires: libao-devel >= 1.0.0

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.11-1m)
- update 0.2.11-2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.10-13m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.10-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.10-11m)
- rebuild against rpm-4.6

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.10-10m)
- change BR from libid3tag to libid3tag-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.10-9m)
- rebuild against gcc43

* Mon Sep 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.2.10-8m)
- telia -> jaist

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.2.10-7m)
- requires libmad, libid3tag

* Sun May 12 2002 Toshiro HIKITA  <toshi@sodan.org>
- (0.2.10-6k)
- requires mad
- add URL:

* Sun May 12 2002 Toshiro HIKITA  <toshi@sodan.org>
- (0.2.10-4k)
- use mad rpm packages
- BuildPreReq mad-devel 0.14.2b

* Mon Mar 25 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.10-2k)

* Wed Mar 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.9-2k)

* Mon Jan 07 2002 Toshiro HIKITA  <toshi@sodan.org>
- (0.2.3-2k)
- update to 0.2.3
- update to mad-0.14.2b

* Thu Nov 08 2001 Toshiro HIKITA <toshi@sodan.org>
- (0.2.2-2k)
- update to 0.2.2
- remove patches
- avoid autoconf

* Thu Oct 25 2001 Toru Hoshina <t@kondara.org>
- (0.2.0-2k)
- 1st release.

* Mon Aug 13 2001 Bill Nottingham <notting@redhat.com>
- update to 0.1.5
- fix build with new libao

* Fri Jul 20 2001 Bill Nottingham <notting@redhat.com>
- initial build
