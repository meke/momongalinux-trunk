%global momorel 4

Summary: Nautilus extension to convert audio files.
Name:    nautilus-sound-converter
Version: 3.0.1
Release: %{momorel}m%{?dist}
Group:   User Interface/Desktops
License: GPLv2
URL:     http://code.google.com/p/nautilus-sound-converter/
Source0: http://%{name}.googlecode.com/files/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.20.1
BuildRequires: gstreamer-devel >= 0.10.22
BuildRequires: libgnome-media-profiles-devel
BuildRequires: GConf2-devel >= 2.26.0

Requires(pre): GConf2
Requires(post): GConf2
Requires(preun): GConf2
# The bare minimum plugins needed.
Requires:       gstreamer-plugins-good
Requires:       gstreamer-plugins-base


%description
The Nautilus-Sound-Converter extension allows you to convert audio
files to different formats using the GStreamer framework.

%prep
%setup -q

%build
%configure --enable-static=no --disable-schemas-install --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/nautilus-sound-converter.schemas \
  > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/nautilus-sound-converter.schemas \
    > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/nautilus-sound-converter.schemas \
    > /dev/null || :
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_sysconfdir}/gconf/schemas/nautilus-sound-converter.schemas
%{_libdir}/nautilus/extensions-?.0/libnautilus-sound-converter.la
%{_libdir}/nautilus/extensions-?.0/libnautilus-sound-converter.so
%{_datadir}/%{name}

%changelog
* Thu Jul 26 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.0.1-4m)
- fix BuildRequires

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-3m)
- rebuild for glib 2.33.2

* Tue Aug 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-2m)
- fix BuildRequires

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Tue May 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-5m)
- rebuild against nautilus-3.0.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-2m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Sun Apr 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Apr 26  2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against rpm-4.6

* Tue Oct 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-1m)
- initial build
