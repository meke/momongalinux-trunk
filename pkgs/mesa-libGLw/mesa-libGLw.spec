%global momorel 1
%global date 20110807

Summary: Xt / Motif OpenGL widgets
Name: mesa-libGLw
Version: 7.11.1
Release: %{momorel}m%{?dist}
License: MIT
Group: System Environment/Libraries
URL: http://www.mesa3d.org
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: glw-%{version}-%{date}.tar.xz

BuildRequires: libXt-devel
BuildRequires: mesa-libGL-devel
BuildRequires: lesstif-devel

%description
Mesa libGLw runtime library.

%package devel
Summary: Mesa libGLw development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: mesa-libGL-devel
Requires: lesstif-devel

%description devel
Mesa libGLw development package.

#-- prep -------------------------------------------------------------
%prep
%setup -q -n glw-%{version}

%configure --enable-motif

#-- Build ------------------------------------------------------------
%build

%make

#-- Install ----------------------------------------------------------
%install
rm -rf %{buildroot}
%makeinstall

rm -f %{buildroot}%{_libdir}/libGLw.{a,la}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README
%{_libdir}/libGLw.so.1
%{_libdir}/libGLw.so.1.0.0

%files devel
%defattr(-,root,root,-)
%{_libdir}/libGLw.so
%{_libdir}/pkgconfig/glw.pc
%{_includedir}/GL/GLwDrawA.h
%{_includedir}/GL/GLwDrawAP.h
%{_includedir}/GL/GLwMDrawA.h
%{_includedir}/GL/GLwMDrawAP.h

%changelog
* Sat Nov 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.11.1-1m)
- Initial Commit Momonga Linux
- import glw-20110807
