%global momorel 2
%global realname libgtop

Name: libgtop2
Summary: The LibGTop library

Version: 2.28.4
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/2.28/%{realname}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.19.10
Requires(pre): info

Provides: %{realname}
Obsoletes: %{realname}

%description
A library that fetches information about the running system such as CPU
and memory useage, active processes and more.

On Linux systems, this information is taken directly from the /proc
filesystem while on other systems a server is used to read that
information from other /dev/kmem, among others.

%package devel
Summary:  Libraries, includes and other files to develop LibGTop applications.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel

Provides: %{realname}-devel
Obsoletes: %{realname}-devel

%description devel
Install this package if you wish to develop applications that access
information on system statistics such as CPU and memory usage.

%prep
%setup -q -n %{realname}-%{version}

%build
%configure --with-examples \
    --with-libgtop-smp \
    --with-x \
    --enable-gtk-doc \
    --enable-hacker-mode \
    --enable-static=no
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{realname}-2.0

# do not place /usr/sbin and /sbin into PATH !!!
rm -f --preserve-root %{buildroot}%{_infodir}/dir

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
  
%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/libgtop2.info %{_infodir}/dir

%preun devel
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/libgtop2.info %{_infodir}/dir
fi

%files -f %{realname}-2.0.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/GTop-2.0.typelib

%files devel
%defattr(-, root, root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_infodir}/libgtop2.info*
%{_includedir}/libgtop-2.0
%{_datadir}/gtk-doc/html/libgtop
%{_datadir}/gir-1.0/GTop-2.0.gir

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.4-2m)
- rebuild for glib 2.33.2

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.4-1m)
- update to 2.28.4

* Tue Apr 12 2011 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.28.3-1m)
- update to 2.28.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-2m)
- rebuild for new GCC 4.5

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.28.2-1m)
- update to 2.28.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.28.1-2m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.1-2m)
- add libtoolize

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.4-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.4-1m)
- update to 2.24.4

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sat Mar  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.9-1m)
- update to 2.14.9

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.8-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.8-1m)
- update to 2.14.8

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.7-1m)
- update to 2.14.7

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.6-2m)
- rename libgtop -> libgtop2

* Mon Jan 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.6-1m)
- update to 2.14.6
- [SECURITY] CVE-2007-0235

* Mon Sep 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-1m)
- update to 2.14.4

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Fri Apr  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-1m)
- version 2.8.2
- GNOME 2.8 Desktop

* Wed Apr 28 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0
- disable libtoolize

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.5.2-1m)
- version 2.5.2
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.8-2m)
- revised spec for enabling rpm 4.2.

* Thu Jan 22 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.0.8-1m)
- version 2.0.8

* Thu Dec 18 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.7-1m)
- version 2.0.7

* Wed Oct 29 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.0.6-1m)
- version 2.0.6
- move gnome related header file from %{_libdir}/libgtop to %{_includedir}/gnome

* Tue Oct 28 2003 Kenta MURATA <muraken2@nifty.com>
- (2.0.5-1m)
- pretty spec file.

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.0.5-1m)
- version 2.0.5

* Wed Sep 10 2003 zunda <zunda at freeshell.org>
- (kossori)
- libtoolize -c -f, aclocal, autoconf added to properly make symlink to 
  /usr/lib/libgtop-2.0.so.0

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-5m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-16k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue Apr 30 2002 Kenta MURATA <muraken@kondra.org>
- (1.90.2-14k)

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-12k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-10k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-8k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-6k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-4k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.2-2k)
- version 1.90.2
* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-6k)
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-4k)
- rebuild against for glib-1.3.13

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.90.0-2k)
- created spec file
