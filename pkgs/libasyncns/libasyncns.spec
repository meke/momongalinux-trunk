%global momorel 4

Summary: Asynchronous Name Service Library
Name: libasyncns
Version: 0.8
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://0pointer.de/lennart/projects/libasyncns/
Group: System Environment/Libraries
Source0: http://0pointer.de/lennart/projects/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig

%description
A small and lightweight library that implements easy to use asynchronous
wrappers around the libc NSS functions getaddrinfo(), res_query() and related.

%package devel
Summary: Development Files for libasyncns Client Development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Development Files for libasyncns Client Development

%prep
%setup -q

%build
%configure --disable-static
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/libasyncns.la

# clean up doc
rm -rf %{buildroot}%{_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE README doc/README.html doc/style.css
%{_libdir}/libasyncns.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/asyncns.h
%{_libdir}/pkgconfig/libasyncns.pc
%{_libdir}/libasyncns.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-2m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-2m)
- rebuild against rpm-4.6

* Tue Dec  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-1m)
- version 0.7

* Sun Oct  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-1m)
- import from Fedora devel

* Sat Aug 23 2008 Lennart Poettering <lpoetter@redhat.com> 0.5-1
- New release

* Sun Jul 27 2008 Lennart Poettering <lpoetter@redhat.com> 0.4-1
- Initial packaging
