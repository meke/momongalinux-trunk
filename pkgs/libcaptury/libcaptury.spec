%global momorel 7
%global captury_version 0.3.0

# Tarfile created using git
# git clone git://gitorious.org/libcaptury/mainline.git libcaptury
# cd libcaptury
# git-archive --format=tar --prefix=libcaptury-%{captury_version}/ %{git_version} | bzip2 > libcaptury-%{captury_version}-%{gitdate}.tar.bz2

%global gitdate 20080323
%global git_version cca4e3c

Summary:        A library for X11/OpenGL video capturing framework
Name:           libcaptury
Version:        %{captury_version}
Release:        0.%{gitdate}.%{momorel}m%{?dist}
License:        GPLv2+
Group:          System Environment/Libraries
URL:            http://gitorious.org/projects/libcaptury/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  libcapseo-devel
BuildRequires:  libX11-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libogg-devel

# Fedora specific snapshot no upstream release (yet)
Source0:	%{name}-%{version}-%{gitdate}.tar.bz2

%description
Captury is a realtime multimedia capturing framework for currently
OpenGL video (to be extended to XShm and audio/alsa soon). 
Its uses are e.g. for capturing video from external OpenGL applications
(via captury itself) and is currently also used by KDE's kwin
to record your desktop efficiently (VideoRecord plugin).

Captury supports full encoding as well as incremential(!) encoding
by only regions from the screen that have actually changed.
Window managers (like kwin) do know of such areas and can make use of it.

%package devel
Summary: Developer and header files for captury
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libcapseo-devel
Requires: libXfixes-devel
Requires: pkgconfig

%description devel
Developer and header files for the captury movie capturing
framework.

%prep
%setup -q -n %{name}-%{version}
./autogen.sh

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/captury
%{_libdir}/libcaptury.so
%{_libdir}/pkgconfig/libcaptury.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-0.20080323.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-0.20080323.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-0.20080323.5m)
- full rebuild for mo7 release

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-0.20080323.4m)
- fix %%files

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-0.20080323.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-0.20080323.2m)
- rebuild against rpm-4.6

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-0.20080323.1m)
- import fron Fedora devel

* Sat May 3 2008 Shawn Starr <shawn.starr@rogers.com> 0.3.0-0.1.20080323gitcca4e3c
- Initial Fedora package.
