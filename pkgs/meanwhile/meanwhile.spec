%global cvsdate 20070409
%global momorel 8
#
# spec file for meanwhile - sametime client library
#

Name:		meanwhile
Summary:	Lotus Sametime Community Client library
License:	LGPLv2+
Group:		Applications/Internet
Version:	1.1.0
Release:        0.0.%{cvsdate}.%{momorel}m%{?dist}
Source:		meanwhile-%{version}.tar.gz
Patch0:		meanwhile-crash.patch
Patch1:		meanwhile-fix-glib-headers.patch
URL:		http://meanwhile.sourceforge.net
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel, doxygen

%description
The heart of the Meanwhile Project is the Meanwhile library, providing the
basic Lotus Sametime session functionality along with the core services;
Presence Awareness, Instant Messaging, Multi-user Conferencing, Preferences
Storage, Identity Resolution, and File Transfer. This extensible client
interface allows additional services to be added to a session at runtime,
allowing for simple integration of future service handlers such as the user
directory and whiteboard and screen-sharing.

%package devel
Summary: Header files, libraries and development documentation for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel

%description devel
This package contains the header files, static libraries and development
documentation for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.

%package doc
Summary: Documentation for the Meanwhile library
Group: Applications/Internet
License: GFDL

%description doc
Documentation for the Meanwhile library

%prep
%setup -q
%patch0 -p0
%patch1 -p1

%build
%configure --enable-doxygen
%make 

%install
rm -rf %{buildroot}
%{makeinstall}
# Remove the latex documentation.  Nobody reads it, it installs a font for
# some unknown reason, and people have to build it themselves.  Dumb.
rm -rf %{buildroot}%{_datadir}/doc/%{name}-doc-%{version}/latex
rm -rf %{buildroot}%{_libdir}/libmeanwhile.a

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, -)
%doc AUTHORS ChangeLog COPYING README TODO INSTALL LICENSE NEWS
%{_libdir}/libmeanwhile.so.*

%files devel
%defattr(-, root, root, -)
%{_includedir}/meanwhile/
%exclude %{_libdir}/libmeanwhile.la
%{_libdir}/libmeanwhile.so
%{_libdir}/pkgconfig/meanwhile.pc

%files doc
%defattr(-, root, root, -)
%{_datadir}/doc/%{name}-doc-%{version}/

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-0.0.20070409.8m)
- merge glib2 patch from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-0.0.20070409.7m)
- rebuild for glib 2.33.2

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-0.0.20070409.6m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-0.0.20070409.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-0.0.20070409.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-0.0.20070409.3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-0.0.20070409.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-0.0.20070409.1m)
- sync with Fedora 11 (1.1.0-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.2-6
- Autorebuild for GCC 4.3

* Fri Aug 03 2007 - jwboyer@jdub.homelinux.org 1.0.2-5
- Update license field

* Fri May 4 2007 - jwboyer@jdub.homelinux.org 1.0.2-4
- Rebuild for F7 to pick up ppc64

* Sun Aug 27 2006 - jwboyer@jdub.homelinux.org 1.0.2-3
- Bump for FE6 rebuild

* Tue Feb 14 2006 - jwboyer@jdub.homelinux.org 1.0.2-2
- Bump for FE5 rebuild

* Tue Jan 3 2006 - jwboyer@jdub.homelinux.org 1.0.2-1
- Update to latest release
- Fixes crash when merging buddy list with server

* Fri Dec 16 2005 - jwboyer@jdub.homelinux.org 1.0.1-1
- Update to latest release
- Fixes mpi conflict with mozilla-nss

* Wed Dec 14 2005 - jwboyer@jdub.homelinux.org 1.0.0-1
- Update to latest release
- gmp and gmp-devel are no longer required since meanwhile uses mpi now

* Sat Oct 29 2005 - jwboyer@jdub.homelinux.org 0.5.0-1
- Update to latest release

* Wed Jun 15 2005 - jwboyer@jdub.homelinux.org 0.4.2-2
- Bump release for rebuild against latest development

* Tue May 31 2005 - jwboyer@jdub.homelinux.org 0.4.2-1
- Update to latest version
- Fix typo in last changelog

* Tue May 24 2005 - jwboyer@jdub.homelinux.org 0.4.1-2
- Updates from review comments

* Mon May 23 2005 - jwboyer@jdub.homelinux.org 0.4.1-1
- Initial package, adapted from spec file by Dag Wieers
