%global momorel 5

%define luaver 5.1
%define lualibdir %{_libdir}/lua/%{luaver}
%define luapkgdir %{_datadir}/lua/%{luaver}

Name:           lua-logging
Version:        1.1.4
Release:        %{momorel}m%{?dist}
Summary:        A simple API to use logging features in Lua

Group:          Development/Libraries
License:        MIT
URL:            http://www.keplerproject.org/lualogging/
Source0:        http://luaforge.net/frs/download.php/2693/lualogging-1.1.4.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       lua = %{luaver}
Requires:       lua-socket
Requires:       lua-sql

%description
LuaLogging provides a simple API to use logging features in Lua. Its design was
based on log4j. LuaLogging currently supports, through the use of appenders,
console, file, email, socket and sql outputs.

%prep
%setup -q -n lualogging-%{version}


%build


%install
rm -rf $RPM_BUILD_ROOT
make install PREFIX=$RPM_BUILD_ROOT%{_prefix} LUA_LIBDIR=$RPM_BUILD_ROOT%{lualibdir}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc doc/us/*
%doc README
%{luapkgdir}/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-1m)
- import from Rawhide

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Apr 04 2008 Tim Niemueller <tim@niemueller.de> - 1.1.4-1
- Initial package

