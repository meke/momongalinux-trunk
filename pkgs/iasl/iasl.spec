%global momorel 4

Name:           iasl
Version:        20100428
Release:        %{momorel}m%{?dist}
Summary:        Intel ASL compiler/decompiler

Group:          Development/Languages
License:        "Intel ACPI"
URL:            http://developer.intel.com/technology/iapc/acpi/
Source0:        http://www.acpica.org/download/acpica-unix-%{version}.tar.gz
Source1:        iasl-README.Fedora
Source2:        http://ftp.debian.org/debian/pool/main/a/acpica-unix/acpica-unix_20060912-3.2.diff.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  bison patchutils flex


%description
iasl compiles ASL (ACPI Source Language) into AML (ACPI Machine Language),
which is suitable for inclusion as a DSDT in system firmware. It also can
disassemble AML, for debugging purposes.


%prep
%setup -q -n acpica-unix-%{version}
cp -p %{SOURCE1} README.Fedora
zcat %{SOURCE2} |  filterdiff -i \*iasl.1  | patch -p2


%build
export CC=gcc
export CFLAGS="$RPM_OPT_FLAGS"
export LDFLAGS="$CFLAGS"
cd compiler
# does not compile with %{?_smp_mflags}
make


%install
rm -rf %{buildroot}
install -p -D compiler/iasl %{buildroot}%{_bindir}/iasl
install -m 0644 -p -D iasl.1 %{buildroot}%{_mandir}/man1/iasl.1


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc changes.txt README README.Fedora
%{_bindir}/iasl
%{_mandir}/man1/iasl.1*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100428-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100428-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100428-2m)
- full rebuild for mo7 release

* Tue May 25 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100428-1m)
- update 20100428

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090521-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090521-1m)
- update to 20090521

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081204-2m)
- rebuild against rpm-4.6

* Sat Jan  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081204-1m)
- update to 20081204
- apply Patch0 for bison24 from paldo

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20061109-2m)
- rebuild against gcc43

* Fri Oct  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (20061109-1m)
- import to Momonga from fedora prepare for virtualbox

* Sat Aug 11 2007 Till Maas <opensource till name> - 20061109-3
- update License Tag to new Guidelines
- rebuild because of #251794

* Tue Feb 20 2007 Till Maas <opensource till name> - 20061109-2
- Make description line less than 80 instead of less that 81 characters long
- Permissions of manpage are 0644 instead of 0755 now

* Thu Feb 01 2007 Till Maas <opensource till name> - 20061109-1
- initial spec for Fedora Extras
