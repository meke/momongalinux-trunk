%global momorel 13
%{expand: %%define pyver %(python -c 'import sys;print(sys.version[0:3])')}

Summary: A multi-sourced configuration back-end.
Name: alchemist
Version: 1.0.37
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Base
Source0: %{name}-%{version}.tar.gz
Patch0: alchemist-1.0.37-py27.patch
Requires: libxml2 >= 2.3.8, libxslt >= 0.9.0
Requires: python >= %{pyver}
# Conflicts: python >= %(echo %{pyver} | awk 'BEGIN{FS=OFS="."}{print $1,$2+1}')
BuildRequires: libxml2-devel >= 2.3.9
BuildRequires: libxslt >= 0.9.0, libxslt-devel >= 0.9.0
BuildRequires: doxygen >= 1.2.7, python-devel, zlib-devel
BuildRequires: python-devel >= %{pyver}
BuildRequires: glib2-devel >= 2.0
# BuildConflicts: python-devel >= %(echo %{pyver} | awk 'BEGIN{FS=OFS="."}{print $1,$2+1}')
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The alchemist is a back-end configuration architecture, which provides
multi-sourced configuration at the data level, postponing translation to
native format until the last stage. It uses XML as an intermediary data
encoding, and can be extended to arbitrarily large configuration scenarios.

%package devel
Summary: Files needed for developing programs which use alchemist.
Group: Development/Libraries
Requires: %{name} == %{version}-%{release}

%description devel
The alchemist is a back-end configuration architecture, which provides
multi-sourced configuration at the data level, postponing translation to
native format until the last stage. It uses XML as an intermediary data
encoding, and can be extended to arbitrarily large configuration scenarios.

%prep
%setup -q
%patch0 -p 1 -b .py26

%build
CFLAGS="-Wall -DNDEBUG -fPIC %{optflags}"; export CFLAGS
[ -f  /usr/share/automake/depcomp ] && cp -f /usr/share/automake/{depcomp,ylwrap} . || :
%configure --enable-shared --enable-static
make %{?_smp_mflags}

%install
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}
rm -rf %{buildroot}
%makeinstall
mkdir -p %{buildroot}/%{_mandir}
cd src
doxygen
cp -a doc/man/* %{buildroot}/%{_mandir}/
python -O %{_libdir}/python%{pyver}/compileall.py \
    %{buildroot}%{_libdir}/python%{pyver}/site-packages

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/python%{pyver}/site-packages/*
%{_libdir}/*.so.*
%dir %{_libdir}/alchemist
%dir %{_libdir}/alchemist/blackbox
%{_libdir}/alchemist/blackbox/*.so.*
%{_sysconfdir}/alchemist/*
%{_localstatedir}/cache/alchemist
%doc README

%files devel
%defattr(-,root,root)
%{_includedir}/alchemist
%{_libdir}/*.a
%{_libdir}/*.so
%{_mandir}/*/*
%{_libdir}/alchemist/blackbox/*a
%{_libdir}/alchemist/blackbox/*.so
%doc src/doc/html

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.37-13m)
- rebuild for glib 2.33.2

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.37-12m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.37-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.37-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.37-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.37-8m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.37-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.37-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.37-5m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.37-4m)
- rebuild against python-2.6.1-1m
- add Patch0: alchemist-1.0.37-py26.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.37-3m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.37-2m)
- delete libtool library

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.37-1m)
- rebuild against python-2.5

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.36-1m)
- version 1.0.36 

* Wed Mar  9 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.34-1m)
- ver up. sync with FC3(1.0.34-1).

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.33-2m)
- enable x86_64.

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.33-1m)
- upgrade 1.0.33
- rebuild against python2.3

* Thu Jul  8 2004 zunda <zunda at freeshell.org>
- (1.0.27-3m)
- OmoiKondara friendly

* Sun Nov  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.27-2m)
- accpet python not only 2.2 but 2.2.*
- add Requires 2.2 or 2.2.*

* Thu Oct 16 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.27-1m)
- update to 1.0.27

* Fri Apr 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.23-8m)
- rebuild against zlib 1.1.4-5m

* Sat Nov 30 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.23-7m)
- add Requires: python

* Sat Apr  6 2002 Kenta MURATA <muraken@kondara.org>
- (1.0.23-6k)
- ore mo aho deshita (forget automake!!).

* Sat Apr  6 2002 Kenta MURATA <muraken@kondara.org>
- (1.0.23-5k)
- aho sugi.

* Sat Apr  6 2002 Kenta MURATA <muraken@kondara.org>
- (1.0.23-2k)
- version 1.0.23

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.22-2k)
- version 1.0.22

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.18-8k)
- nigittenu

* Mon Nov  5 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.18-4k)
- libxslt compile patch

* Mon Sep 17 2001 Motonobu Ichimura <famao@kondara.org>
- (1.0.18-3k)
- up to 1.0.18

* Fri Jul  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.0.4-3k)
- from rawhide.
- use /var/cache/alchemist instead of /var/lib/cache/alchemist.

* Mon Jun 13 2001 Crutcher Dunnavant <crutcher@redhat.com>
- added some python helpers, has_key function

* Mon Jun 06 2001 Crutcher Dunnavant <crutcher@redhat.com>
- landed major devel work. feature complete

* Mon May 21 2001 Crutcher Dunnavant <crutcher@redhat.com>
- fixed AdmList_getChildByName, AdmList_getChildByPath, and AdmToken_nget

* Mon May 21 2001 Crutcher Dunnavant <crutcher@redhat.com>
- rebuilt with new binding system (Major changes)

* Mon Apr 23 2001 Nalin Dahyabhai <nalin@redhat.com>
- Obey RPM_OPT_FLAGS.  Obey.

* Fri Apr 19 2001 Crutcher Dunnavant <crutcher@redhat.com>
- fixed readVersion in the CacheBlackBox
- added URLBlackBok

* Mon Mar 05 2001 Philipp Knirsch <pknirsch@redhat.de>
- Fixed the "_cascade_merge_" name in the switchboard to be consistent with our
  new name validation.

* Wed Feb 28 2001 Crutcher Dunnavant <crutcher@redhat.com>
- changed valid_name to the pattern /^[a-zA-Z][-a-zA-Z0-9_]?$/,
- dropping the '-' and '_' characters as legal first characters
- (because they aren't legal XML first characters)

* Tue Feb 13 2001 Crutcher Dunnavant <crutcher@redhat.com>
- rebuilt for pknirsh's moveChild fix. (He should have written this changelog!)

* Fri Feb  9 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Truncate files when opening for writing (ForgeBlackBox, FileBlackBox)

* Thu Feb 08 2001 Harald Hoyer <harald@redhat.de>
- fixed typos in ForgeBlackBox

* Wed Feb  7 2001 Crutcher Dunnavant <crutcher@redhat.com>
- tweaked the ForgeBlackBox to fix some include problems

* Wed Jan 31 2001 Crutcher Dunnavant <crutcher@redhat.com>
- check for name collides in non-anonymous lists at parse time. (oops.)

* Wed Jan 24 2001 Crutcher Dunnavant <crutcher@redhat.com>
- changed valid name to include '-' character

* Tue Jan 23 2001 Trond Eivind Glomsrod <teg@redhat.com>
- require 4Suite

* Mon Jan 22 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Fixed a bad break that I left in WriteContextToFile

* Mon Jan 22 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Fixed all occurances of Writeable to be Writable

* Thu Jan 18 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Reved the version to take into account Trond's ForgeBlackBox entry

* Sun Jan 14 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Add a CacheBlackBox, and some isNamespaceDirty logic to the alchemist
- to deal with some dirty cache nastyness

* Thu Jan 11 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Nope, Elliot had changed it to be case insensitive everywhere, reverted.

* Thu Jan 11 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Garh! Was doing case insensitive string comparisions for some reason.

* Tue Jan  9 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Fixed a bug in the import code that threw an uncaught exception

* Tue Jan  9 2001 Crutcher Dunnavant <crutcher@redhat.com>
- Changed Alchemist.py:Switchboard:writeNamespace() to return
- the context it writes, instead of None

* Wed Jan  3 2001 Crutcher Dunnavant <crutcher@redhat.com>
- initial packaging of alchemist

* Wed Dec 13 2000 Crutcher Dunnavant <crutcher@redhat.com>
- Created blank package for translation.
