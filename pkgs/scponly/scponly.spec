%global momorel 9

Summary: Limited shell for secure file transfers
Name: scponly
Version: 4.8
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Shells
URL: http://sublimation.org/scponly/

Source0: http://dl.sourceforge.net/sourceforge/scponly/scponly-%{version}.tgz 
NoSource: 0
Patch0: scponly-4.8-configure.in-sftp-server-path.patch
Patch1: scponly-4.8-configure-sftp-server-path.patch
Patch2: scponly-4.8-gcc44.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssh, openssh-server, perl, unison

%description
scponly is an alternative 'shell' for system administrators 
who would like to provide access to remote users to both 
read and write local files without providing any remote 
execution priviledges. Functionally, it is best described 
as a wrapper to the "tried and true" ssh suite of applications. 

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1 -b .gcc44~

%build
%configure --enable-winscp-compat --enable-sftp-logging-compat \
           --enable-scp-compat --enable-rsync-compat --enable-unison-compat \
           --enable-chrooted-binary

%make
# OPTS="%{optflags}"

%install
%{__rm} -rf %{buildroot}
%{__install} -Dp -m0755 scponly %{buildroot}%{_bindir}/scponly
%{__install} -Dp -m0644 scponly.8 %{buildroot}%{_mandir}/man8/scponly.8

cp build_extras/setup_chroot.sh.RH9 build_extras/setup_chroot.sh
chmod -x AUTHOR CHANGELOG CONTRIB COPYING INSTALL README TODO

%clean
%{__rm} -rf %{buildroot}

%files 
%defattr(-, root, root, 0755)
%doc AUTHOR CHANGELOG CONTRIB COPYING INSTALL README TODO
%doc build_extras/setup_chroot.sh
%doc %{_mandir}/man8/scponly.8*
%{_bindir}/scponly

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8-5m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8-3m)
- rebuild against gcc43

* Sat Feb 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (4.8-2m)
- modify Source URL

* Thu Feb 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8-1m)
- [SECURITY] CVE-2007-6350 CVE-2007-6415
- update patch0
- update patch1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6-3m)
- %%NoSource -> NoSource

* Wed Aug  9 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.6-2m)
- modify changelog option
- add chroot setup script

* Mon Feb 13 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6-1m)
- update to 4.6
- add BuildRequires: openssh-server for sftp-server
- add Patch0: scponly-4.6-configure.in-sftp-server-path.patch
- add Patch1: scponly-4.6-configure-sftp-server-path.patch

* Tue May 10 2005 Dag Wieers <dag@wwieers.com> - 4.1-1 - 3051+/dag
- Updated to release 4.1.

* Thu Mar 03 2005 Dag Wieers <dag@wwieers.com> - 4.0-1
- Initial package. (using DAR)

