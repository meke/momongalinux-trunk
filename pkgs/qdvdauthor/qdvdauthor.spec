%global momorel 1
%global src1name qdvdauthor-templates
%global src1ver 1.11.1
%global src2name dv2sub
%global src2ver 0.3

Summary: GUI frontend for dvdauthor and other related tools
Name: qdvdauthor

### include local configuration
%{?include_specopt}

### default configurations
# If you'd like to change these configurations, please copy them to
# ${HOME}/rpm/specopt/qdvdauthor.specopt and edit it.

## Configuration
%{?!with_templates:     %global with_templates             0}
%{?!with_mplayer:       %global with_mplayer               0}
%{?!with_xine:          %global with_xine                  1}

Version: 2.3.1
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
License: GPLv2
URL: http://qdvdauthor.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
%if %{with_templates}
Source1: http://dl.sourceforge.net/sourceforge/%{name}/%{src1name}-%{src1ver}.tar.bz2
NoSource: 1
%endif
Source2: http://dl.sourceforge.net/sourceforge/%{src2name}/%{src2name}-%{src2ver}.tar.gz
NoSource: 2
Source3: ffmpeg-export-snapshot.tar.bz2
Patch0: %{name}-1.6.0-qrender-qt443.patch
Patch1: %{name}-1.7.0-gcc44.patch
Patch2: %{name}-1.10.0-add-exceptions.patch 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dvdauthor
%if %{with_mplayer}
Requires: mplayer
%endif
Requires: netpbm
Requires: netpbm-progs
Requires: sox
%if %{with_xine}
Requires: xine-lib
%endif
BuildRequires: desktop-file-utils
# reserve, can not build with ffmpeg-0.4.9-0.20081203.1m
# BuildRequires: ffmpeg-devel
BuildRequires: freetype-devel
BuildRequires: libdv-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel
BuildRequires: libxml2-devel
BuildRequires: mjpegtools-devel
%if %{with_mplayer}
BuildRequires: mplayer
%endif
BuildRequires: qt-devel >= 4.7.0
BuildRequires: qt3-designer
BuildRequires: qt3-devel
%if %{with_xine}
BuildRequires: xine-lib-devel >= 1.2.4
%endif
BuildRequires: libva-devel >= 0.32

%description
QDVDAuthor is a gui frontend for using dvdauthor and dvd-slideshow 
scripts to easily build DVD menus and assemble the DVD VOB files. 
This package includes a patched copy of %{dvd_slideshow} in the 
installation and requires %{media_backend}.

%prep
# clean up templates
rm -rf %{_builddir}/%{src1name}-%{src1ver}

# clean up dv2sub
rm -rf %{_builddir}/%{src2name}-%{src2ver}

%if %{with_templates}
%setup -q -b 1 -b 2
%else
%setup -q -b 2
%endif

%patch0 -p1 -b .build-fix0
%patch1 -p1 -b .gcc44~
%patch2 -p1 -b .exceptions~

# clean up cvs
find . -type d -name CVS | xargs rm -rf

# look for ffmpeg in /usr instead of /usr/local
pushd qrender
sed -i "s:\/local::g" qrender.pro
popd

# set flags
  for PRO in */*.pro */*/*.pro; do
    echo "QMAKE_CFLAGS_RELEASE = %{optflags} -fpermissive" >> "${PRO}"
    echo "QMAKE_CXXFLAGS_RELEASE = %{optflags} -fpermissive" >> "${PRO}"
    echo "QMAKE_CFLAGS_DEBUG = %{optflags} -fpermissive" >> "${PRO}"
    echo "QMAKE_CXXFLAGS_DEBUG = %{optflags} -fpermissive" >> "${PRO}"
  done

# for docs
%if %{with_templates}
pushd %{_builddir}/%{src1name}-%{src1ver}
mv -f COPYING COPYING.templates
mv -f INSTALL INSTALL.templates
popd
%endif

pushd %{_builddir}/%{src2name}-%{src2ver}
mv -f AUTHORS AUTHORS.dv2sub
mv -f COPYING COPYING.dv2sub
mv -f ChangeLog ChangeLog.dv2sub
mv -f README README.dv2sub
mv -f TODO TODO.dv2sub
popd

# can not build with ffmpeg-0.4.9-0.20081203.1m
# install Source3
# install -m 644 %%{SOURCE3} qrender/
# clean up link
# rm -f qrender/ffmpeg

%build
# reserve, can not build with ffmpeg-0.4.9-0.20081203.1m
# set paths for ffmpeg
# export FFMPEG=$(pkg-config --variable=prefix libavformat)
# export FFMPEG_INC=$(pkg-config --variable=includedir libavformat)
# export FFMPEG_LIB=$(pkg-config --variable=libdir libavformat)

# set local ffmpeg CFALGS
%ifarch x86_64 ppc64
export FFMPEGFLAGS="%{optflags}"
%else
export FFMPEGFLAGS="%{optflags} -fno-unit-at-a-time"
%endif

%if %{with_mplayer}
%global nomediabackend --no-xine-support
%endif
%if %{with_xine}
%global nomediabackend --no-mplayer-support
%endif

./configure \
	--prefix=%{_prefix} \
	--no-configurator \
	--build-qslideshow \
	%{nomediabackend}

# reserve, can not build with ffmpeg-0.4.9-0.20081203.1m
#	--omit-local-ffmpeg \

# build dv2sub
pushd %{_builddir}/%{src2name}-%{src2ver}
CFLAGS="%{optflags}" \
./configure --prefix=%{_prefix}
make %{?_smp_mflags}
popd 

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# install qdvdauthor
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_datadir}/pixmaps
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/qdvdauthor/i18n
mkdir -p %{buildroot}%{_datadir}/qdvdauthor/html/en
mkdir -p %{buildroot}%{_datadir}/qdvdauthor/plugins
mkdir -p %{buildroot}%{_datadir}/qdvdauthor/plugins/simpledvd
mkdir -p %{buildroot}%{_datadir}/qdvdauthor/plugins/complexdvd
mkdir -p %{buildroot}%{_datadir}/qdvdauthor/lib
install -m 755 bin/qdvdauthor %{buildroot}%{_bindir}/
install -m 755 bin/qslideshow %{buildroot}%{_bindir}/
install -m 755 bin/qplayer %{buildroot}%{_bindir}/
#install -m 755 bin/qrender %%{buildroot}%%{_bindir}/
install -m 755 bin/dvd-slideshow %{buildroot}%{_bindir}/
install -m 644 qdvdauthor.png %{buildroot}%{_datadir}/pixmaps/
install -m 644 doc/html/en/*.html %{buildroot}%{_datadir}/qdvdauthor/html/en/
install -m 644 silence.mp2 %{buildroot}%{_datadir}/qdvdauthor/
install -m 644 silence.ac3 %{buildroot}%{_datadir}/qdvdauthor/
install -m 755 qdvdauthor/plugins/plugins/libcomplexdvd.so* %{buildroot}%{_datadir}/qdvdauthor/plugins/
install -m 755 qdvdauthor/plugins/plugins/libsimpledvd.so* %{buildroot}%{_datadir}/qdvdauthor/plugins
install -m 644 qdvdauthor/plugins/complexdvd/*.jpg %{buildroot}%{_datadir}/qdvdauthor/plugins/complexdvd/
install -m 644 qdvdauthor/plugins/complexdvd/*.png %{buildroot}%{_datadir}/qdvdauthor/plugins/complexdvd/
install -m 644 qdvdauthor/plugins/simpledvd/*.jpg %{buildroot}%{_datadir}/qdvdauthor/plugins/simpledvd/
install -m 644 qdvdauthor/plugins/simpledvd/*.png %{buildroot}%{_datadir}/qdvdauthor/plugins/simpledvd/
install -m 755 lib/*so* %{buildroot}%{_datadir}/qdvdauthor/lib/

%if %{with_templates}
# install templates
cp -R -p %{_builddir}/%{src1name}-%{src1ver}/* %{buildroot}%{_datadir}/qdvdauthor/
rm -f %{buildroot}%{_datadir}/qdvdauthor/COPYING.templates
rm -f %{buildroot}%{_datadir}/qdvdauthor/INSTALL.templates
%endif

# install dv2sub
pushd %{_builddir}/%{src2name}-%{src2ver}
install -m 755 src/dv2sub %{buildroot}%{_bindir}/
install -m 644 doc/dv2sub.1 %{buildroot}%{_mandir}/man1/
popd

cp -p qdvdauthor/i18n/*.qm %{buildroot}%{_datadir}/qdvdauthor/i18n/

# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
  --remove-category X-Red-Hat-Base \
  %{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGELOG COPYING INSTALL README TODO
%if %{with_templates}
%doc ../%{src1name}-%{src1ver}/*.templates
%endif
%doc ../%{src2name}-%{src2ver}/*.dv2sub
%{_bindir}/dv2sub
%{_bindir}/dvd-slideshow
%{_bindir}/qdvdauthor
%{_bindir}/qplayer
#%%{_bindir}/qrender
%{_bindir}/qslideshow
%{_datadir}/applications/qdvdauthor.desktop
%{_datadir}/qdvdauthor
%{_mandir}/man1/dv2sub.1*
%{_datadir}/pixmaps/qdvdauthor.png

%changelog
* Sun Jan 26 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-1m)
- [BUILD FIX] version 2.3.1
- update qdvdauthor-templates to 0.11.1

* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-14m)
- rebuild against xine-lib-1.2.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-13m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-12m)
- rebuild against libva-0.32

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-11m)
- fix %%files

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-10m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-9m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-8m)
- full rebuild for mo7 release

* Sun Jul  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-7m)
- rebuild against libva-0.31.1-1m

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-6m)
- rebuild against qt-4.6.3-1m

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-5m)
- explicitly link libz

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-4m)
- rebuild against libva

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-3m)
- rebuild against libjpeg-8a

* Sat Apr  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-2m)
- build with libva

* Tue Jan 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-1m)
- version 2.1.0
- update Source3: ffmpeg-export-snapshot.tar.bz2

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10.0-5m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.0-3m)
- rebuild against libjpeg-7

* Sat Sep  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.0-2m)
- fix "-fexceptions" issue, which causes build failure with gcc-4.4

* Mon Jul 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10.0-1m)
- version 1.10.0
- update Source3: ffmpeg-export-snapshot.tar.bz2

* Wed Jun 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.0-1m)
- version 1.9.0
- update Source3: ffmpeg-export-snapshot.tar.bz2
- update local-ffmpeg.patch
- update ffmpeg-headers.patch

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.0-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.0-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-1m)
- update to 1.7.0

* Sun Dec 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Fri Nov  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-2m)
- add Source3: ffmpeg-export-snapshot.tar.bz2

* Tue Nov  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-1m)
- version 1.6.0
- License: GPLv2
- [CAUTION] use http://ffmpeg.mplayerhq.hu/ffmpeg-export-snapshot.tar.bz2 for the moment

* Mon Aug 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-4m)
- add gcc43 patch

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- %%NoSource -> NoSource

* Mon Jan 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-2m)
- replace dv2sub from pre-built binary to Source1 for x86_64 dependency
- add %%description

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-1m)
- update to 1.0.0 final

* Tue May 29 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.0-0.1.1m)
- update to 1.0.0 rc1

* Thu Apr 12 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.1.5-1m)
- build for Momonga
