%global        momorel 1

Summary:       Very fast network log-on cracker
Name:          hydra
Version:       7.6
Release:       %{momorel}m%{?dist}
License:       "GPLv3 with exceptions"
Group:         Development/Libraries
URL:           http://www.thc.org/thc-hydra/
Source0:       http://freeworld.thc.org/releases/%{name}-%{version}.tar.gz
Source1:       xhydra.desktop
Source2:       xhydra.png
NoSource:      0
# Sent upstream via email 20120518
Patch0:        hydra-use-system-libpq-fe.patch
Patch1:        hydra-fix-dpl4hydra-dir.patch
Patch2:        hydra-fix-makefile.patch
Patch100:      hydra-7.6-nolibncp.patch
BuildRequires: openssl-devel
BuildRequires: apr-devel
BuildRequires: libssh-devel
BuildRequires: libidn-devel
BuildRequires: subversion-devel
BuildRequires: postgresql-devel
BuildRequires: pcre-devel
BuildRequires: gtk2-devel
BuildRequires: desktop-file-utils
BuildRequires: firebird-devel
BuildRequires: mysql-devel 

%description
Hydra is a parallelized log-in cracker which supports numerous protocols to 
attack. New modules are easy to add, beside that, it is flexible and very fast.

This tool gives researchers and security consultants the possibility to show 
how easy it would be to gain unauthorized access from remote to a system.

%package frontend
Summary: The GTK+ front end for hydra
Group: System Environment/Base
Requires: hydra = %{version}-%{release}
BuildRequires: gtk2-devel, pkgconfig
%description frontend
This package includes xhydra, a GTK+ front end for hydra. 

%prep
%setup -q

%patch0 -p0
%patch1 -p0
%patch2 -p0

%patch100 -p1

%build
export BINDIR=%{_bindir}
%configure --nostrip
make %{?_smp_mflags}

%install
make install PREFIX="%{buildroot}/usr"

mkdir -p %{buildroot}%{_datadir}/{applications,pixmaps}
install -m 644 -p %{SOURCE2} %{buildroot}%{_datadir}/pixmaps/
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE1};

%files
%doc CHANGES LICENSE LICENSE.OPENSSL README
%{_bindir}/hydra
%{_bindir}/hydra-wizard.sh
%{_bindir}/pw-inspector
%{_mandir}/man1/hydra*
%{_mandir}/man1/pw-inspector*
%{_bindir}/dpl4hydra.sh
%{_datadir}/%{name}/dpl4hydra*.csv
%dir %{_datadir}/%{name}

%files frontend
%{_bindir}/xhydra
%{_mandir}/man1/xhydra*
%{_datadir}/pixmaps/*
%{_datadir}/applications/*

%changelog
* Wed Jan 15 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.6-1m)
- update to 7.6

* Tue Jan  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4.1-1m)
- update to 7.4.1

* Tue Sep 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3-1m)
- import from Fedora for mpich2

* Mon Sep 10 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.3-12
- Remove dep on ncpfs-devel since it's a dead upstream.

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 7.3-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon May 28 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.3-10
- Fix binaries striping issue (#825860)

* Tue May 22 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.3-9
- Update to 7.3
- Drop some patches since they're included in 7.3
- Add two patches to fix makefile and dpl4hydra

* Fri May 18 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.2-8
- Add LICENSE.OPENSSL
- Add /usr/share/hydra
- Add a patch to use system provided libpq-fe headers (provided by 
  postgresql-devel)

* Tue Apr 17 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.2-7
- Add DESTDIR support
- Include dpl4hydra

* Mon Apr 16 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.2-6
- Reverse a patch because it breaks brute-forcing NTLM-enabled services 
  (upstream confirmed that it's not necessary)

* Tue Mar 13 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.2-5
- Add patch to support mysql
- Add patch to fix warnings

* Thu Mar 08 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.2-4
- Preserve timestamps on install
- Remove extra arg in desktop file install

* Sat Feb 11 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.2-3
- Add support for CFLAGS

* Sat Feb 11 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.2-2
- Clean-up the descriptions
- Add Firebird support

* Sat Feb 11 2012 Athmane Madjoudj <athmane@fedoraproject.org> 7.2-1
- Update to 7.2

* Tue Dec 27 2011 Athmane Madjoudj <athmane@fedoraproject.org> 7.1-3
- Remove rm -rf buildroot

* Thu Dec 22 2011 Athmane Madjoudj <athmane@fedoraproject.org> 7.1-2
- Update license to GPLv3 with OpenSSL exception

* Thu Dec 22 2011 Athmane Madjoudj <athmane@fedoraproject.org> 7.1-1
- Update to recent version
- Clean-up the spec file
- Add desktop file for the frontend

* Sun Jul 04 2010 Marcus Haebler <haebler@gmail.com> - 0:5.7-0
- Initial RPM build 
