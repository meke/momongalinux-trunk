%global momorel 1

%global xfce4ver 4.10.0
%global major 0.4

Summary:        A mpc plugin for the Xfce panel
Name:           xfce4-mpc-plugin
Version:        0.4.4
Release:        %{momorel}m%{?dist}

License:        GPLv2
Group:          Applications/System
URL:            http://goodies.xfce.org/projects/panel-plugins/xfce4-mpc-plugin
Source0:        http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
# http://bugzilla.xfce.org/show_bug.cgi?id=6623
# Patch0:         xfce4-mpc-plugin-0.3.5-exo-and-libxfcegui4.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  intltool >= 0.35.5
BuildRequires:  libmpd-devel >= 0.18.0
BuildRequires:  pkgconfig
BuildRequires:  xfce4-dev-tools >= 4.6.0
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:       xfce4-panel >= %{xfce4ver}

%description
A client plugin for the Music Player Daemon.

%prep
%setup -q
#%%patch0 -p1 -b .exo-and-libxfcegui4.patch


%build
# because of Patch0
NOCONFIGURE=1 xdt-autogen

%configure
%{__make}

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}
%find_lang %{name}

rm -f %{buildroot}/%{_libdir}/xfce4/panel/plugins/libmpc.la

%clean
rm -rf %{buildroot}


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(644,root,root,755)
# FIXME: add NEWS when there are some
%doc AUTHORS COPYING ChangeLog README TODO
%{_libdir}/xfce4/panel/plugins/libmpc.so
%{_datadir}/xfce4/panel/plugins/*.desktop
%{_datadir}/locale/*

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.4-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.5-5m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.5-2m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.5-1m)
- update to 0.3.5

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.3-4m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-2m)
- rebuild against libmpd-0.18.0

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.3-1m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-1m)
- initial Momonga package based on PLD
