%global momorel 14
Summary: GtkSourceView is a text widget that extends the GtkTextView.
Name: gtksourceview
Version: 1.8.5
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.8/%{name}-%{version}.tar.bz2 
NoSource: 0
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgnomeprint22-devel >= 2.18.1
BuildRequires: libgnomeprintui22-devel >= 2.18.0
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.12.0
BuildRequires: libgnome-devel >= 2.18.0
BuildRequires: libxml2-devel >= 2.6.29
BuildRequires: gnome-vfs2-devel >= 2.18.1
BuildRequires: glib2-devel >= 2.14.1
BuildRequires: gnome-common
BuildRequires: intltool
BuildRequires: gtk-doc

%description
GtkSourceView is a text widget that extends the standard gtk+ 2.x
text widget GtkTextView.
                                                                               
It improves GtkTextView by implementing syntax highlighting and other
features typical of a source editor.

%package devel
Summary: gtksourceview-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Requires: libgnome-devel
Requires: libxml2-devel

%description devel
gtksourceview-devel

%prep
%setup -q

%build
export RPM_OPT_FLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE"
export CFLAGS=$RPM_OPT_FLAGS
export CXXFLAGS=$RPM_OPT_FLAGS

gtkdocize --copy
autoreconf -fiv
%configure --enable-gtk-doc CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
find %{buildroot}%{_libdir} -maxdepth 1 -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING README ChangeLog AUTHORS MAINTAINERS NEWS HACKING
%{_libdir}/lib*.so.*
%{_datadir}/locale/*/*/*
%{_datadir}/gtksourceview-1.0

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/*.pc
%{_libdir}/lib*.so
%{_includedir}/gtksourceview-1.0
%doc %{_datadir}/gtk-doc/html/gtksourceview

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.5-14m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.5-13m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.5-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.5-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.5-9m)
- use BuildRequires

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.5-8m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.5-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (1.8.5-6m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.5-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.5-4m)
- rebuild against gcc43

* Mon Feb 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5-3m)
- fix glibc-2.8

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5-2m)
- %%NoSource -> NoSource

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.5-2m)
- back to 1.8.5

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Mar 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.1-1m)
- version 1.1.1
- GNOME 2.8 Desktop

* Tue Dec 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.1-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Thu Apr 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- version 1.0.1

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.0-1m)
- version 1.0.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.7.0-2m)
- revised spec for enabling rpm 4.2.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.0-1m)
- version 0.7.0

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.6.0-1m)
- version 0.6.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.0-1m)
- version 0.5.0

* Tue Jul 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4.0-1m)
- version 0.4.0

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.0-1m)
- version 0.3.0

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.1-1m)
- version 0.2.1

* Fri May  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.0-1m)
- version 0.2.0
