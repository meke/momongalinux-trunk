%global momorel 1

%global _x11fontdir	%(pkg-config --variable=fontdir fontutil)
%global _sysfontdir	%{_datadir}/fonts
%global _oldx11fontdir	/usr/X11R6/lib/X11/fonts
%global oldxfsconfigdir /usr/X11R6/lib/X11
%global newxfsconfigdir /etc/X11

Summary: X.Org X11 xfs font server
Name: xorg-x11-xfs
Version: 1.1.3
Release: %{momorel}m%{?dist}

License: MIT/X
Group: System Environment/Daemons
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/xfs-%{version}.tar.bz2 
NoSource: 0
Source1: %{xorgurl}/app/xfsinfo-1.0.3.tar.bz2 
NoSource: 1
Source2: %{xorgurl}/app/fslsfonts-1.0.3.tar.bz2 
NoSource: 2
Source3: %{xorgurl}/app/fstobdf-1.0.4.tar.bz2 
NoSource: 3
Source4: %{xorgurl}/app/showfont-1.0.3.tar.bz2 
NoSource: 4
Source10:  xfs.init
# NOTE: xfs config input file, processed via sed below.
Source11:  xfs.config.in
# FIXME: This is the original xfs.config from FC5 rawhide prior to modular X,
# included here for developmental comparison purposes during FC5 development,
# but can be dropped later on.
Source12:  xfs.config
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
# xfs needs 'fontsproto' to build, as indicated by ./configure
BuildRequires: xorg-x11-proto-devel >= 7.7
# FIXME: xfs needs xtrans to build, but autotools doesn't detect it missing
BuildRequires: xorg-x11-xtrans-devel
BuildRequires: libFS-devel
# FIXME: The version specification can be removed from here in the future,
# as it is not really mandatory, but forces a bugfix workaround on people who
# are using pre-rawhide modular X.
BuildRequires: libXfont-devel >= 0.99.2-3
BuildRequires: libX11-devel
# FIXME: xfs needs freetype-devel to build, but autotools doesn't detect it missing
BuildRequires: freetype-devel
# font-utils >= 1.0.0 needed for fontdir pkgconfig variable
BuildRequires: xorg-x11-font-utils >= 1.0.0

# FIXME: This Requires on libXfont can be removed from here in the future,
# as it is not really mandatory, but forces a bugfix workaround on people who
# are using pre-rawhide modular X.
Requires: libXfont >= 0.99.2-3

Obsoletes: XFree86-xfs
Requires: util-linux-ng sed coreutils
Requires: shadow-utils chkconfig
Requires(pre): shadow-utils chkconfig initscripts coreutils
Requires(post): chkconfig grep perl
Requires(preun): initscripts chkconfig
Requires(postun): initscripts

# xfs initscript runtime dependencies
Requires: initscripts, fontconfig, sed, findutils
Requires: xorg-x11-mkfontdir, xorg-x11-mkfontscale
Requires: ttmkfdir
Requires: policycoreutils
# end of xfs initscript runtime dependencies

%description
X.Org X11 xfs font server

%package utils
Summary: X.Org X11 font server utilities
Group: User Interface/X
#Requires: %{name} = %{version}-%{release}

# So upgrades from OS releases with XFree86 work, since the utilities were
# previously in the "xfs" subpackage instead of their own subpackage.
Obsoletes: XFree86-xfs

%description utils
X.Org X11 font server utilities

%prep
%setup -q -c %{name}-%{version} -a1 -a2 -a3 -a4

%build
# Comment: Separate sections for each individual tarball rather than an
# iterative for loop, so that each can be customized easily in future.

# Build xfs
{
   pushd xfs-*
   %configure 
   # FIXME: Create patch for the following, and submit it upstream.
   # <daniels> mharris: uhm, just set configdir=$(sysconfdir) in Makefile.am
   # <mharris> daniels: Do you mean, change it in Makefile.am, and submit patch to fix?  Or patch it for local use?
   # <daniels> mharris: and submit patch to fix, yeah
   make configdir=%{_sysconfdir}/X11/fs
# CFLAGS='-DDEFAULT_CONFIG_FILE="/etc/X11/fs/config"'
   popd
}

for pkg in xfsinfo fslsfonts fstobdf showfont ; do
   pushd ${pkg}-*
   %configure
   make
   popd
done

%install
rm -rf $RPM_BUILD_ROOT
# Install xfs
{
   pushd xfs-*
   # FIXME: Create patch for the following, and submit it upstream.
   # <daniels> mharris: uhm, just set configdir=$(sysconfdir) in Makefile.am
   # <mharris> daniels: Do you mean, change it in Makefile.am, and submit patch to fix?  Or patch it for local use?
   # <daniels> mharris: and submit patch to fix, yeah
   %makeinstall configdir=$RPM_BUILD_ROOT%{_sysconfdir}/X11/fs
   popd
}

for pkg in xfsinfo fslsfonts fstobdf showfont ; do
   pushd ${pkg}-*
   %makeinstall
   popd
done

# Install the Red Hat xfs config file and initscript
{
   mkdir -p $RPM_BUILD_ROOT/etc/{X11/fs,init.d}
   install -c -m 755 %{SOURCE10} $RPM_BUILD_ROOT%{_initscriptdir}/xfs
#   install -c -m 644 %{SOURCE11} $RPM_BUILD_ROOT%{_sysconfdir}/X11/fs/config
   sed -e 's#@XFONTDIR@#%{_x11fontdir}#g;s#@SYSFONTDIR@#%{_sysfontdir}#g' < %{SOURCE11} > $RPM_BUILD_ROOT%{_sysconfdir}/X11/fs/config
   chmod 0644 $RPM_BUILD_ROOT%{_sysconfdir}/X11/fs/config
}

# for flash-plugin
# old xfs config dir is hardcoded in flashplayer-plugin binary
mkdir -p %{buildroot}%{oldxfsconfigdir}
ln -s ../../../../%{newxfsconfigdir}/fs %{buildroot}%{oldxfsconfigdir}/fs

%clean
rm -rf $RPM_BUILD_ROOT

##### xfs scripts ####################################################
# triggerpostun xfs:
# - Work around a bug in the XFree86-xfs postun script, which results in
#   the special xfs user account being inadvertently removed, causing xfs
#   to run as the root user, and also resulting in xfs not being activated
#   by chkconfig,  This trigger executes right after the XFree86-xfs
#   postun script during upgrades from older OS releases that shipped
#   XFree86, and ensures that the xfs user exists, and that the xfs
#   initscript is properly activated with chkconfig (#118145,118818)
#
#   WARNING: THIS MUST BE LEFT HERE AS LONG AS WE SUPPORT UPGRADES FROM
#   Fedora Core 1, Red Hat Linux <any release>,
#   Red Hat Enterprise Linux 2.1, 3
%triggerpostun -- XFree86-xfs
{
  /usr/sbin/useradd -c "X Font Server" -r -s /sbin/nologin -u 43 -d /etc/X11/fs xfs || :
  /sbin/chkconfig --add xfs
  /sbin/service xfs condrestart || :
} &> /dev/null || :

%pre
{
  /usr/sbin/useradd -c "X Font Server" -r -s /sbin/nologin -u 43 -d /etc/X11/fs xfs || :
  # Upgrade path:
  if [ "$1" -ge "1" ] ; then
    if [ -e "/usr/X11R6/lib/X11/fs/config" ] ; then
      cat <<-EOF > "/etc/X11/fs/xfs-migrate"
	On upgrades, we now must determine if we are upgrading from monolithic
	xfs or modular xfs by checking for the existance of the old monolithic
	xfs config file.  If found, we know it is a monolith->modular upgrade,
	so we set this flag file in order for xfs.init to perform a "restart"
	instead of a "reload" in the 'condrestart'.  See bug #173271 for
	details.
EOF
    fi
  fi
} &> /dev/null || : # Silence output, and ignore errors (Bug #91822)

%post
{
  # Install section
  /sbin/chkconfig --add xfs
  #------------------------------------------------------------------------
  # Upgrade section
  if [ "$1" -ge "1" ] ; then
    XORG_CONFIG=/etc/X11/xorg.conf
    XFSCONFIG=/etc/X11/fs/config

    if [ -f $XORG_CONFIG ] ; then
      # If the X server is configured to use UNIX socket for font server
      if grep -q 'unix/:' $XORG_CONFIG &> /dev/null ; then
        # Disable legacy font path element in X server config
        if grep -q '/usr/X11R6/lib/X11/fonts/TrueType' $XORG_CONFIG &> /dev/null ; then
          perl -p -i -e 's|FontPath[ ]*"/usr/X11R6/lib/X11/fonts/TrueType"|#FontPath "/usr/X11R6/lib/X11/fonts/TrueType"|g' $XORG_CONFIG
        fi

        # Convert config to use modern unix socket
        perl -p -i -e 's#unix/:-1#unix/:7100#g' $XORG_CONFIG

        # Disable TCP listen by default for xfs
        if [ -f $XFSCONFIG ] && ! grep -q "no-listen" $XFSCONFIG &> /dev/null ; then
          echo -e "# For security, don't listen to TCP ports by default.\nno-listen = tcp\n" >> $XFSCONFIG
        fi
      fi
    fi

    # XFS config file upgrade munging
    if [ -f $XFSCONFIG ] ; then
      # Remove Speedo font directories from xfs config if present to avoid
      # bug reports about xfs complaining about empty directories in syslog.
      perl -p -i -e 's#^.*/.*/Speedo.*\n##' $XFSCONFIG

      # On upgrades, remove /usr/X11R6 font path elements from the XFS config file
      if grep -q "/usr/X11R6/lib/X11/fonts" $XFSCONFIG &> /dev/null ; then
        for fpe in misc:unscaled 75dpi:unscaled 100dpi:unscaled Type1 ; do
          perl -p -i -e "s#/usr/X11R6/lib/X11/fonts/${fpe}#%{_x11fontdir}/${fpe}#g" $XFSCONFIG
        done
      fi
    fi
  fi ; # End Upgrade section
}

%preun
{
  if [ "$1" = "0" ]; then
    /sbin/service xfs stop &> /dev/null || :
# FIXME: The chkconfig call below works properly if uninstalling the package,
#        but it will cause xfs to be de-chkconfig'd if upgrading from one X11
#        implementation to another, as witnessed in the transition from
#        XFree86 to Xorg X11.  If this call is removed however, then xfs will
#        remain visible in ntsysv and similar utilities even after xfs is
#        uninstalled from the system in non-upgrade scenarios.  Not sure how
#        to fix this yet.
    /sbin/chkconfig --del xfs || :
# userdel/groupdel removed because they cause the user/group to get destroyed
# when upgrading from one X11 implementation to another, ie: XFree86 -> Xorg
#    /usr/sbin/userdel xfs 2>/dev/null || :
#    /usr/sbin/groupdel xfs 2>/dev/null || :
  fi
}

%postun
{
  if [ "$1" -ge "1" ]; then
    /sbin/service xfs condrestart &> /dev/null || :
  fi
}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/xfs
%dir %{_sysconfdir}/X11/fs
%{oldxfsconfigdir}/fs
# NOTE: We intentionally override the upstream default config file location
# during build.
# FIXME: Create patch for the following, and submit it upstream.
# Check if this is still relevent:  set configdir=$(sysconfdir) in Makefile.am
# and if so, submit patch upstream to fix.
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/X11/fs/config
#%%dir %{_mandir}/man1
# FIXME: The manpage incorrectly points to /usr/X11R6/...
%{_mandir}/man1/xfs.1*
%{_initscriptdir}/xfs

%files utils
%defattr(-,root,root,-)
%{_bindir}/fslsfonts
%{_bindir}/fstobdf
%{_bindir}/showfont
%{_bindir}/xfsinfo
%{_mandir}/man1/fslsfonts.1*
%{_mandir}/man1/fstobdf.1*
%{_mandir}/man1/showfont.1*
%{_mandir}/man1/xfsinfo.1*

%changelog
* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-1m)
- update 1.1.3

* Thu Mar 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-2m)
- remove Requires(triggerpostun)
-- Obso rpm-4.11 

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update xfs-1.1.2

* Sun Mar  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-4m)
- fix build failure when using xorg-x11-proto-devel-7.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update xfs-1.1.1 fslsfonts-1.0.3 xfsinfo-1.0.3 fstobdf-1.0.4 showfont-1.0.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-8m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.8-7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-5m)
- remove Requires: chkfontpath-momonga

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-4m)
- [SECURITY] CVE-2007-3103
- update xfs.init from Fedora 11 (1.0.5-5)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-3m)
- rebuild against rpm-4.6

* Thu Oct  9 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.8-2m)
- update xfs.init xfs.config

* Sun May 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-2m)
- update
-- xfsinfo-1.0.2
-- fslsfonts-1.0.2
-- fstobdf-1.0.3
-- showfont-1.0.2

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-1m)
- update 1.0.6

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-2m)
- %%NoSource -> NoSource

* Wed Oct  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-1m)
- update 1.0.5
-- fix CVE-2007-4568

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-2m)
- BuildRequires: freetype2-devel -> freetype-devel

* Tue Dec 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-1m)
- update 1.0.4

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Mon Nov  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-4m)
- change Requires form mkfontdir, mkfontscale
-- to xorg-x11-mkfontdir, xorg-x11-mkfontscale

* Wed Jul 05 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.2-3m)
- made /usr/X11R6/lib/X11/fs as a symbolic link to /etc/X11/fs so that flashplayer-plugin can display fonts. 
- - the path "/usr/X11R6/lib/X11/fs" is hardcoded in libflashplayer.so
- - see ,for example, "http://www.hyde-tech.com/HideWeb/fedora_core_5_installation_notes.html#Flash"

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-2m)
- comment out duplicated dir

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update xfs-1.0.2, fstobdf-1.0.2

* Thu Apr  6 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.1-5m)
- revised packages owner

* Sun Mar 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-4m)
- delete Conflicts: xorg-x11-xfs < 6.99.99.0

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- Change Source URL
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2.1m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1:1.0.1-2.1
- bump again for double-long bug on ppc(64)

* Thu Feb  9 2006 Mike A. Harris <mharris@redhat.com> 1:1.0.1-2
- Removed invocation of fc-cache from xfs initscript for bug (#179362)
- Redirect stderr to /dev/null to squelch an unwanted error xfs.init (#155349)
- Replace "s#^/.*:[a-z]*$##g" with "s#:unscaled$##g" in xfs.init for (#179491)
- Cosmetic cleanups to spec file to satiate the banshees.

* Tue Feb  7 2006 Jesse Keating <jkeating@redhat.com> 1:1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes


* Wed Jan 16 2006 Mike A. Harris <mharris@redhat.com> 1:1.0.1-1
- Updated all tarballs to version 1.0.1 from X11R7.0

* Tue Jan 10 2006 Bill Nottingham <notting@redhat.com> 1:1.0.0-2
- fix rpm post script (#176009, <ville.skytta@iki.fi>)

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1:1.0.0-1
- Updated all tarballs to version 1.0.0 from X11R7 RC4.
- Get default X font directory with font-utils package 'fontdir' pkgconfig
  variable.
- Change manpage dir from man1x back to man1 to match upstream.

* Tue Nov 15 2005 Jeremy Katz <katzj@redhat.com> 1:0.99.2-4
- require initscripts instead of /etc/init.d/functions

* Tue Nov 15 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-3
- Updated xfs pre script to check for the existance of the old monolithic
  /usr/X11R6/lib/X11/fs/config xfs config file, and set a migration flag
  file.
- Updated xfs.init to check for the existance of the migration flag file,
  and perform an xfs 'restart' instead of a 'reload' if migrating.  Users
  will now have to restart their X server, or reconnect the xfs server to
  the X server after a migration to modular X.
- Changed upgrade comparison typo from 2 to 1 in xfs post script.

* Mon Nov 14 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Added temporary "BuildRequires: libXfont-devel >= 0.99.2-3" and
  "Requires: libXfont-devel >= 0.99.2-3" to ensure early-testers of
  pre-rawhide modular X have installed the work around for (#172997).

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated to xfs-0.99.2 and fstobdf-0.99.2 from X11R7 RC2
- Added Epoch 1 to package, and set the version number to the xfs 0.99.2
  version.

* Thu Nov 10 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.901-2
- Added showfont-0.99.1 from X11R7 RC1 release.

* Wed Nov 9 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.901-1
- Updated all packages to version 0.99.1 from X11R7 RC1.
- Bump package version to 6.99.99.901 (the RC1 CVS tag).
- Change manpage location to 'man1x' in file manifest.
- Converted xfs.config to xfs.config.in, and added code to spec file to
  generate xfs.config depending on what the system _x11fontdir is.
- Complete and total rewrite of xfs postinstall script to use "sed -i"
  and complete restructuring, which removed a lot of the super craptasticness
  that had been sitting there for years.

* Wed Oct 3 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-4
- Use Fedora-Extras style BuildRoot tag
- Update BuildRequires to use new library package names
- Remove unnecessary BuildRequires on 'install', and fix pkgconfig dep

* Thu Aug 25 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-3
- Install the initscript and xfs config file in the correct location as they
  were inadvertently interchanged in previous builds.

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-2
- Ported the xfs related rpm scripts over from monolithic packaging, and
  added up to date Requires(*) dependencies for all of them.
- Flagged xfs config file as config(noreplace)
- Added build and runtime dependencies to xfs subpackage as best as could be
  determined by analyzing ./configure output, and building in minimalized
  build root environment.

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-1
- Initial build.
