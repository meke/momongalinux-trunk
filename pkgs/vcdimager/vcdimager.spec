%global momorel 2

Summary: Video CD authoring tool
Name:    vcdimager
Version: 0.7.24
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: GPL
URL:     http://www.gnu.org/software/vcdimager/
Source0: ftp://ftp.gnu.org/gnu/vcdimager/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: vcdimager-0.7.23-remove-vcd-info-entry.patch
BuildRequires: libcdio-devel >= 0.90
Requires: libcdio
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info

%description
GNU VCDImager is a full-featured mastering suite for authoring, disassembling and analyzing Video CD's and Super Video CD's.

The core functionality consists of directly making Video CD BIN/CUE-style CD images from mpeg files, which (after being written to CDR(W) media) can be played on standalone VCD players or DVD players and on computers running GNU/Linux, MacOS, Win32 or any other OS capable of accessing VCD's. BIN/CUE images can be burned with [cdrdao] (please use a recent version, since older ones do not support BIN/CUE-style cuesheets) under GNU/Linux (and other supported platforms by cdrdao, e.g. freeBSD, Irix, Solaris and even win32) 

%package devel
Summary:        %{name} header files and development documentation
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Header files and development documentation for %{name}


%prep
%setup -q
%patch0 -p0 -b .info~

%build
%configure
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_libdir}/*.la

# remove unused file
rm -rf %{buildroot}/%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/vcd-info.info %{_infodir}/dir
/sbin/install-info %{_infodir}/vcdimager.info %{_infodir}/dir
/sbin/install-info %{_infodir}/vcdxrip.info %{_infodir}/dir
/sbin/ldconfig

%preun
if [ "$1" = 0 ] ;then
  /sbin/install-info --delete %{_infodir}/vcd-info.info %{_infodir}/dir
  /sbin/install-info --delete %{_infodir}/vcdimager.info %{_infodir}/dir
  /sbin/install-info --delete %{_infodir}/vcdxrip.info %{_infodir}/dir
fi

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc README INSTALL ChangeLog AUTHORS COPYING NEWS
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/man1/*
%{_infodir}/*.info*

%files devel
%{_includedir}/libvcd
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.24-2m)
- rebuild against libcdio-0.90

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.24-1m)
- update to 0.7.24

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.23-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.23-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.23-13m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.23-12m)
- rebuild against libcdio-0.82

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.23-11m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.23-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.23-9m)
- rebuild against libcdio-0.81

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.23-8m)
- rebuild against rpm-4.6

* Fri Nov 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.23-7m)
- change URI

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.23-6m)
- fix install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.23-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.23-4m)
- %%NoSource -> NoSource

* Fri Jan 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.23-3m)
- revise %%files to avoid conflicting

* Tue Dec 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.23-2m)
- revise %%files to avoid conflicting
- /usr/include is already provided by filesystem

* Wed Dec 06 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.23-1m)
- initial packaging for Momonga
