%global momorel 4

Summary: Jailkit is a set of utilities for chroot() enviroment.
Name: jailkit
Version: 2.11
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Base
Source0: http://olivier.sessink.nl/jailkit/jailkit-%{version}.tar.bz2
NoSource: 0
Patch1: install.patch
URL: http://olivier.sessink.nl/jailkit/ 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: procmail


%description 
Jailkit is a set of utilities to limit user accounts to specific files
using chroot() and or specific commands. Setting up a chroot shell, a
shell limited to some specific command, or a daemon inside a chroot
jail is a lot easier and can be automated using these utilities.
Jailkit is known to be used in network security appliances from
several leading IT security firms, internet servers from several large
enterprise organizations, internet servers from internet service
providers, as well as many smaller companies and private users that
need to secure cvs, sftp, shell or daemon processes.

%prep
%setup -q 
%patch1 -p0 -b .install
%configure

%build
%make

%install
DESTDIR=%{buildroot} make install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README.txt COPYRIGHT INSTALL.txt
%config(noreplace) %{_sysconfdir}/jailkit/*.ini
%attr(4755,root,root) %{_bindir}/jk_uchroot
%{_sbindir}/jk_addjailuser
%{_sbindir}/jk_check
%{_sbindir}/jk_chrootlaunch
%attr(4755,root,root) %{_sbindir}/jk_chrootsh
%{_sbindir}/jk_cp
%{_sbindir}/jk_init
%{_sbindir}/jk_jailuser
%{_sbindir}/jk_list
%{_sbindir}/jk_lsh
%attr(4755,root,root) %{_sbindir}/jk_procmailwrapper
%{_sbindir}/jk_socketd
%{_sbindir}/jk_update
%{_datadir}/%{name}/*
%{_mandir}/man8/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11-2m)
- full rebuild for mo7 release

* Tue Feb 9 2010 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.11-1m)
- Initial version


