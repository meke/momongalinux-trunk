#!/bin/sh

kernelver=$1

DESTDIR=/usr/src/zfs-0.6.1/$kernelver

rm -rf $DESTDIR.old
mv -f $DESTDIR $DESTDIR.old
mkdir -p $DESTDIR

cp zfs_config.h $DESTDIR
cp module/Module.symvers $DESTDIR
ln -fs ../include $DESTDIR/include

exit 0
