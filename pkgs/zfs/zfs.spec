%global momorel 1
%global zfs_ver 0.6.2
%global spl_ver 0.6.2

Summary:         ZFS Library and Utils
Group:           System Environment/Base
Name:            zfs
Version:         0.6.2
Release:         %{momorel}m%{?dist}
License:         "CDDL"
URL:		 http://zfsonlinux.org/
Source0:         http://archive.zfsonlinux.org/downloads/zfsonlinux/zfs/%{name}-%{zfs_ver}.tar.gz
NoSource:        0
Source1:	 dkms.conf.in
# script for dkms
Source4:	 install_src.sh
BuildRoot:       %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id} -un)
BuildRequires:   e2fsprogs-devel
BuildRequires:   zlib-devel libuuid-devel libblkid-devel
BuildRequires:   libunicode-devel
Requires:        e2fsprogs udev
Requires:        zlib libuuid libblkid
Requires:        libunicode



%description
The %{name} package contains the libzfs library and support utilities
for the zfs file system.

%package devel
Summary:         ZFS File System User Headers
Group:           Development/Libraries
Requires:	 %{name} = %{version}-%{release}
Requires:        zlib e2fsprogs

%description devel
The %{name}-devel package contains the header files needed for building
additional userland applications against the %{name} libraries.

%package test
Summary:         ZFS File System Test Infrastructure
Group:           System Environment/Base
Requires:	 %{name} = %{version}-%{release}

%description test
The %{name}-test package contains a test infrastructure for zpios which
can be used to simplfy the benchmarking of various hardware and software
configurations.  The test infrastructure additionally integrates with
various system profiling tools to facilitate an in depth analysis.

%package dracut
Summary:         ZFS Dracut Module
Group:           System Environment/Base
Requires:        dracut

%description dracut
The %{name}-dracut package allows dracut to construct initramfs images
which are ZFS aware.

%package dkms
Summary:	dksm support for ZFS File System
Group:		System Environment/Base
Requires:	%{name} = %{version}-%{release}
Requires:	dkms
BuildArch:	noarch
# zfs kernel module needs some header files provided by spl-dkms but zfs-devel
Requires:	spl-dkms >= %{spl_ver}
Obsoletes:	zfs-modules
Obsoletes:	zfs-modules-devel

%description dkms
This package provides the dkms support for %{name} kernel modules.

%prep
%setup -q -n zfs-%{zfs_ver}

%build

%configure --with-config=user --without-blkid --with-udevdir=/lib/udev

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_libdir}/*.{a,la}
# !!FIXME!!
rm -f %{buildroot}/%{_libdir}/libunicode.so

# copy some source files for DKMS
mkdir -p %{buildroot}/usr/src/%{name}-%{version}
pushd %{buildroot}/usr/src/%{name}-%{version}
tar xfz %{SOURCE0} 
mv */* .
install -m 0755 %{SOURCE4} install_src.sh
sh ./autogen.sh
popd

# setup dkms.conf
cat %{SOURCE1} \
    | sed -e 's,@@SRCNAME@@,%{name},g' -e 's,@@VERSION@@,%{version},g' \
    > %{buildroot}/usr/src/%{name}-%{version}/dkms.conf

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post dkms
/usr/sbin/dkms add -m %{name} -v %{version} || :
/usr/sbin/dkms build -m %{name} -v %{version} || :
/usr/sbin/dkms install -m %{name} -v %{version} || :

%preun dkms
/usr/sbin/dkms remove -m %{name} -v %{version} --all || :

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYRIGHT DISCLAIMER
%doc OPENSOLARIS.LICENSE README.markdown
/sbin/mount.zfs
%{_sbindir}/*
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man8/*
%{_mandir}/man5/*
%{_initscriptdir}/%{name}
%dir %{_sysconfdir}/%{name}
%{_sysconfdir}/%{name}/*
#exclude /lib/udev/rules.d
/lib/udev/rules.d/69-vdev.rules
#/lib/udev/rules.d/60-zpool.rules
/lib/udev/rules.d/60-zvol.rules
/lib/udev/rules.d/90-zfs.rules
#/lib/udev/sas_switch_id
#/lib/udev/zpool_id
/lib/udev/zvol_id
/lib/udev/vdev_id
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/libzfs
%{_includedir}/libzfs/*
%dir %{_includedir}/libspl
%{_includedir}/libspl/*
%{_libdir}/lib*.so
#%%{_libdir}/lib*.a

%files test
%defattr(-,root,root,-)
%dir %{_datadir}/zfs
%{_datadir}/zfs/*


%files dracut
%defattr(-,root,root,-)
##%%{_datadir}/dracut/*
/usr/lib/dracut/*

%files dkms
%defattr(-,root,root,-)
%dir /usr/src/%{name}-%{version}
/usr/src/%{name}-%{version}/*

%changelog
* Mon Sep  2 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Tue Apr  9 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1
 
* Mon Sep  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.13m)
- update 0.6.0-rc10

* Sun Mar 18 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-0.12m)
- update source0

* Sun Jan 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-0.11m)
- merge from T4R/dkms-test/zfs 
- add dkms support

* Mon Oct 17 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-0.10m)
- exclude /lib/udev/rules.d provided by udev

* Mon Oct 17 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-0.9m)
- add subpackage dracut

* Sat Oct 15 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-0.8m)
- update 0.6.0-rc6
- set --with-udevdir=/lib/udev in configure

* Sat Jul 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-0.7m)
- add %%post and %%postun for libs

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.6m)
- fix file conflict
- remove static library

* Thu Jul 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.5m)
- update 0.6.0-rc5

* Sun May  8 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-0.4m)
- update 0.6.0-rc4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-0.3m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.2m)
- update 0.6.0-rc3

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.1m)
- update 0.6.0-rc2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.2-1m)
- update 0.5.2

* Tue Sep 14 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-1m)
- update

* Sat Sep 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-3m)
- drop ix86 build only x86_64

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-2m)
- full rebuild for mo7 release

* Mon Aug 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-1m)
- update
- add Requires: libunicode

* Sat Jul 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-2m)
- modify %%files
- add Requires: udev

* Sun Jun  6 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.9-1m)
- initial made for Momonga Linux
