%global momorel 1

Name: sparse
Version: 0.5.0
Release: %{momorel}m%{?dist}
Summary:    A semantic parser of source files
Group:      Development/Tools
License:    "OSL 1.1"
URL:        http://kernel.org/pub/software/devel/sparse/
Source0:    http://kernel.org/pub/software/devel/sparse/dist/sparse-%{version}.tar.xz
NoSource:   0
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: gtk2-devel
BuildRequires: libxml2-devel

%description
Sparse is a semantic parser of source files: it's neither a compiler
(although it could be used as a front-end for one) nor is it a
preprocessor (although it contains as a part of it a preprocessing
phase).

It is meant to be a small - and simple - library.  Scanty and meager,
and partly because of that easy to use.  It has one mission in life:
create a semantic parse tree for some arbitrary user for further
analysis.  It's not a tokenizer, nor is it some generic context-free
parser.  In fact, context (semantics) is what it's all about - figuring
out not just what the grouping of tokens are, but what the _types_ are
that the grouping implies.

Sparse is primarily used in the development and debugging of the Linux kernel.

%package devel
Summary: Build environment for sparse-enabled apps
Group: Development/Libraries
Requires: pkgconfig
Provides: %{name}-static = %{version}-%{release}

%description devel
Development headers headers and static lib for sparse-enabled apps

%prep
%setup -q

%define make_destdir \
make DESTDIR="%{buildroot}" PREFIX="%{_prefix}" \\\
     BINDIR="%{_bindir}" LIBDIR="%{_libdir}" \\\
     INCLUDEDIR="%{_includedir}" PKGCONFIGDIR="%{_libdir}/pkgconfig" \\\
     LDFLAGS="`pkg-config gtk+-2.0 --libs`" \\\
%{nil}

%build
RPM_OPT_FLAGS="%{optflags} -Wwrite-strings `pkg-config pkg-config gtk+-2.0 --cflags`"
%ifarch sparcv9 sparc64
%make_destdir %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS -fPIC -DGCC_BASE=\\\"`gcc --print-file-name=`\\\""
%else
%make_destdir %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS -fpic -DGCC_BASE=\\\"`gcc --print-file-name=`\\\""
%endif

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}
%make_destdir install
rm %{buildroot}%{_bindir}/test-inspect

%check
make check

%clean
rm -rf %{buildroot}
make clean


%files
%defattr(-,root,root,-)
%doc LICENSE README FAQ
%{_bindir}/sparse
%{_bindir}/sparsec
%{_bindir}/sparse-llvm
%{_bindir}/cgcc
%{_bindir}/c2xml
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%doc LICENSE
%{_includedir}/*
%{_libdir}/libsparse.a
%{_libdir}/pkgconfig/%{name}.pc


%changelog
* Wed May 14 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Thu Dec 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-1m)
- update 0.4.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-2m)
- rebuild for new GCC 4.6

* Mon Feb 21 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-1m)
- update 0.4.3
- add patch for gcc46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.2-1m)
- update 0.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-2m)
- rebuild against gcc43

* Wed Nov 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-1m)
- update 0.4.1

* Sun Oct 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-1m)
- update 0.4

* Wed Jun 13 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3-1m)
- import from Fedora

* Thu May  3 2007 Roland McGrath <roland@redhat.com> - 0.3-1
- Upgrade to 0.3

* Tue Dec 05 2006 Matt Domsch <Matt_Domsch@dell.com> 0.2-1
- Upgrade to 0.2, add -devel package

* Thu Nov 09 2006 Matt Domsch <Matt_Domsch@dell.com> 0.1-1
- Upgrade to 0.1, no need for snapshots, yea!  New upstream maintainer.
- cgcc now installed

* Thu Oct 26 2006 Matt Domsch <Matt_Domsch@dell.com> 0-0.1.20061026git
- Initial packaging for Fedora Extras
