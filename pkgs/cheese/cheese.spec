%global momorel 2

Name:           cheese
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        Application for taking pictures and movies from a webcam

Group:          Amusements/Graphics
License:        GPLv2+
URL:            http://projects.gnome.org/cheese/
#VCS: git:git://git.gnome.org/cheese
Source0:        http://download.gnome.org/sources/cheese/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires: gtk3-devel >= 3.4.4
BuildRequires: gstreamer1-devel
BuildRequires: gstreamer1-plugins-base-devel
BuildRequires: gstreamer1-plugins-bad-devel
BuildRequires: cairo-devel >= 1.4.0
BuildRequires: librsvg2-devel >= 2.36.2
BuildRequires: evolution-data-server-devel >= 3.5.90
BuildRequires: libXxf86vm-devel
BuildRequires: libXtst-devel
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: gnome-doc-utils
BuildRequires: intltool
BuildRequires: gnome-desktop3-devel >= 3.5.90
BuildRequires: libgudev1-devel
BuildRequires: libcanberra-devel
BuildRequires: scrollkeeper
BuildRequires: clutter-devel
BuildRequires: clutter-gtk-devel
BuildRequires: clutter-gst2-devel
BuildRequires: libmx-devel
BuildRequires: vala-devel
BuildRequires: pkgconfig(gee-1.0)
BuildRequires: gnome-video-effects
BuildRequires: gnome-desktop3-devel
BuildRequires: chrpath
BuildRequires: itstool

Requires: %{name}-libs = %{version}-%{release}
Requires: gstreamer1-plugins-base
Requires: gstreamer1-plugins-bad
Requires: gnome-video-effects

Obsoletes: cheese-devel <= 3.2.2

%description
Cheese is a Photobooth-inspired GNOME application for taking pictures and
videos from a webcam. It can also apply fancy graphical effects.

%package libs
Summary:	Webcam display and capture widgets
Group:		System Environment/Libraries
License:	GPLv2+

%description libs
This package contains libraries needed for applications that
want to display a webcam in their interface.

%package libs-devel
Summary:	Development files for %{name}-libs
Group:		Development/Libraries
License:	GPLv2+
Requires:	%{name}-libs = %{version}-%{release}

Provides:  cheese-devel = %{version}-%{release}

%description libs-devel
This package contains the libraries and header files that are needed
for writing applications that require a webcam display widget.

%prep
%setup -q

%build
%configure --disable-static
%make 


%install
make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/libcheese.{a,la}
rm -f %{buildroot}%{_libdir}/libcheese-gtk.{a,la}

desktop-file-install --delete-original --vendor="" 	\
 	--dir=%{buildroot}%{_datadir}/applications 	\
	--add-category X-AudioVideoImport		\
	%{buildroot}%{_datadir}/applications/cheese.desktop

%find_lang %{name} --with-gnome

chrpath --delete %{buildroot}%{_bindir}/cheese
chrpath --delete %{buildroot}%{_libdir}/libcheese-gtk.so.*

%post
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :


%postun
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :

%post libs
/sbin/ldconfig
if [ $1 -eq 1 ] ; then
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%postun libs
/sbin/ldconfig
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files
%doc AUTHORS COPYING README
%{_bindir}/cheese
%{_datadir}/applications/cheese.desktop
%{_datadir}/cheese
%{_datadir}/icons/hicolor/*/apps/cheese.png
%{_datadir}/icons/hicolor/*/actions/*.png
%{_datadir}/icons/hicolor/scalable/actions/*.svg
%{_mandir}/man1/cheese.1.bz2
# FIXME find-lang is supposed to pick these up
%doc %{_datadir}/help/*/cheese

%files -f %{name}.lang libs
%{_libdir}/libcheese.so.*
%{_libdir}/libcheese-gtk.so.*
%{_datadir}/glib-2.0/schemas/org.gnome.Cheese.gschema.xml
%{_libdir}/girepository-1.0/Cheese-3.0.typelib

%files libs-devel
%doc COPYING
%{_libdir}/libcheese.so
%{_libdir}/libcheese-gtk.so
%{_includedir}/cheese/
%{_datadir}/gtk-doc/
%{_libdir}/pkgconfig/cheese.pc
%{_libdir}/pkgconfig/cheese-gtk.pc
%{_datadir}/gir-1.0/Cheese-3.0.gir

%changelog
* Mon Nov 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-2m)
- rebuild against gstreamer1-plugins-bad-1.0.2

* Fri Oct  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-2m)
- rebuild for librsvg2 2.36.1

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- reimport from fedora

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-4m)
- rebuild for evolution-data-server-devel-3.5.3

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-3m)
- rebuild for glib 2.33.2

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-2m)
- remove BR hal

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91.1-1m)
- rebuild against many package ;-p

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91.1-1m)
- update to 3.1.91.1

* Mon May  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-3m)
- add BuildRequires

* Tue May  3 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- add BR

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-4m)
- add buildrequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-4m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-3m)
- split libs

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-2m)
- rebuild against gnome-desktop

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0.1-1m)
- update to 2.28.0.1

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.3-2m)
- add BuildPrereq

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-2m)
- welcome evolution-data-server

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0
- add patch0 (good-bye evolution-data-server)

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.3-2m)
- change BuildPrereq: gst-plugins-base-devel to gstreamer-plugins-base-devel 

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Wed Jun 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-2m)
- add Require: gnome-media

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1-2m)
- rebuild against librsvg2-2.22.2-3m

* Wed Apr 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0.20080329-2m)
- rebuild against gcc43

* Sat Mar 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0.20080329-1m)
- svn version (2008-03-29)

* Sat Mar 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-3m)
- add patch0 (fix clash but not work)

* Tue Mar 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-2m)
- add Req gst-ffmpeg

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- initial build
