%global         momorel 3

Name:           perl-Module-Signature
Version:        0.73
Release:        %{momorel}m%{?dist}
Summary:        Module signature file manipulation
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Module-Signature/
Source0:        http://www.cpan.org/authors/id/A/AU/AUDREYT/Module-Signature-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.005
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO
BuildRequires:  perl-Test-Simple
Requires:       perl-IO
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Module::Signature adds cryptographic authentications to CPAN distributions,
via the special SIGNATURE file.

%prep
%setup -q -n Module-Signature-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor --installdeps
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUDREY2006.pub AUTHORS Changes PAUSE2003.pub README
%{_bindir}/cpansign
%{perl_vendorlib}/Module/Signature.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-2m)
- rebuild against perl-5.18.2

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-1m)
- update to 0.73

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-2m)
- rebuild against perl-5.16.3

* Fri Nov 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Sat Nov  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.69-1m)
- update to 0.69

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-6m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-5m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-4m)
- rebuild against perl-5.16.0

* Tue Oct 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.68-3m)
- add "--installdeps" to skip prompt

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-2m)
- rebuild against perl-5.14.2

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sat Apr 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.66-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.66-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-1m)
- update to 0.66

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.64-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-2m)
- rebuild against perl-5.12.1

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-1m)
- update to 0.64

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-3m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.55-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.55-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.55-4m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.55-3m)
- use vendor

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.55-2m)
- delete dupclicate directory

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.55-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
