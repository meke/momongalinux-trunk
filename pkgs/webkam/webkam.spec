%global momorel 7
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Summary: A simple webcam application
Name: webkam
Version: 0.3.2
Release: %{momorel}m%{?dist}
License: GPLv3
URL: http://code.google.com/p/webkam-kde4/
Group: Applications/Multimedia
Source0: http://webkam-kde4.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: ruby-gstreamer
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: gstreamer-devel

%description
This is simple webcam application. Basically it is kopate video config dialog
with one additional feature, saving image to file. It going to be cheese clone
(witch is Apple PhotoBooth inspired).

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# override _bindir/webkam
mkdir -p %{buildroot}%{_bindir}
cat <<EOT > %{buildroot}%{_bindir}/webkam
#!/bin/sh
cd %{_kde4_prefix}/lib/webkam
ruby main.rb
EOT
chmod 755 %{buildroot}%{_bindir}/webkam

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --remove-category Application \
  --remove-category Utility \
  --add-category Graphics \
  %{buildroot}%{_datadir}/applications/kde4/webkam.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc ChangeLog INSTALL LICENSE README
%{_kde4_bindir}/webkam
%{_kde4_prefix}/lib/webkam
%{_kde4_datadir}/applications/kde4/webkam.desktop
%{_kde4_iconsdir}/hicolor/*/apps/webkam.png
%{_kde4_datadir}/webkam

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-6m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jan 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-1m)
- version 0.3.2

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-2m)
- fix build on x86_64

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-1m)
- version 0.3.1
- update desktop.patch
- remove merged fix-build.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-5m)
- rebuild against rpm-4.6

* Mon Dec  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-4m)
- fix build

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-2m)
- rebuild against gcc43

* Mon Mar 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-1m)
- version 0.2

* Thu Mar 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1-1m)
- initial package for webcam fans using Momonga Linux
