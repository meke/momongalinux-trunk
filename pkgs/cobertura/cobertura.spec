%global momorel 5

# Prevent brp-java-repack-jars from being run
%global __jar_repack %{nil}

# Links the system JAR
# %1 - the sys jar
# %2 - the symlink (optional)
%global lnSysJAR() %__ln_s -f %{_javadir}/%{*} ;

Name:           cobertura
Version:        1.9
Release:        %{momorel}m%{?dist}
Summary:        Java tool that calculates the percentage of code accessed by tests

Group:          Development/Libraries
License:        "ASL 1.1" and GPLv2+
URL:            http://cobertura.sourceforge.net/

Source0:        http://dl.sourceforge.net/project/cobertura/cobertura/%{version}/cobertura-%{version}-src.tar.gz
NoSource:       0
Source1:        %{name}-%{version}.pom
Source2:        %{name}-runtime-%{version}.pom
Patch0:         cobertura-build_xml.patch
Patch1:         cobertura-scripts.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ant
BuildRequires:  ant-junit
BuildRequires:  ant-trax
BuildRequires:  asm2
BuildRequires:  dos2unix
BuildRequires:  jpackage-utils
BuildRequires:  java-devel
BuildRequires:  jakarta-oro
BuildRequires:  jaxen
BuildRequires:  jdom
BuildRequires:  junit
BuildRequires:  log4j
BuildRequires:  xalan-j2
BuildRequires:  xerces-j2
BuildRequires:  xml-commons-jaxp-1.3-apis

Requires:       ant
Requires:       asm2
Requires:       jpackage-utils
Requires:       java
Requires:       jakarta-oro
Requires:       junit
Requires:       log4j

Requires(post): jpackage-utils
Requires(postun): jpackage-utils

BuildArch:      noarch

%description
Cobertura is a free Java tool that calculates the percentage of code 
accessed by tests. It can be used to identify which parts of your 
Java program are lacking test coverage. It is based on jcoverage.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Requires:       %{name} = %{version}-%{release}
Requires:       jpackage-utils

%description    javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q
%patch0 -b .sav
%patch1 -b .sav
find . -type f -name '*.jar' | xargs -t rm

dos2unix -k ChangeLog COPYING COPYRIGHT README

%build
pushd lib
%lnSysJAR asm2/asm2.jar
%lnSysJAR asm2/asm2-tree.jar
%lnSysJAR jaxen.jar
%lnSysJAR jdom.jar
%lnSysJAR junit.jar
%lnSysJAR oro.jar
%lnSysJAR log4j.jar
%lnSysJAR xalan-j2.jar
pushd xerces
%lnSysJAR xerces-j2.jar
%lnSysJAR xml-commons-jaxp-1.3-apis.jar
popd
popd

export CLASSPATH=
export OPT_JAR_LIST="junit ant/ant-junit jaxp_transform_impl ant/ant-trax xalan-j2-serializer"
%ant compile test jar javadoc

%install
%__rm -rf %{buildroot}

# jar
%__mkdir_p %{buildroot}%{_javadir}
%__cp -a %{name}.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
(cd %{buildroot}%{_javadir} && for jar in *-%{version}*; do \
%__ln_s ${jar} ${jar/-%{version}/}; done)
%add_to_maven_depmap cobertura cobertura %{version} JPP %{name}
%add_to_maven_depmap cobertura cobertura-runtime %{version} JPP %{name}
%add_to_maven_depmap net.sourceforge.cobertura cobertura %{version} JPP %{name}
%add_to_maven_depmap net.sourceforge.cobertura cobertura-runtime %{version} JPP %{name}

# pom
%__mkdir_p %{buildroot}%{_datadir}/maven2/poms
%__cp -a %{SOURCE1} %{buildroot}%{_datadir}/maven2/poms/JPP-%{name}.pom
%__cp -a %{SOURCE2} %{buildroot}%{_datadir}/maven2/poms/JPP-%{name}-runtime.pom

%__mkdir_p  %{buildroot}%{_sysconfdir}/ant.d
%__cat > %{buildroot}%{_sysconfdir}/ant.d/%{name} << EOF
ant cobertura junit log4j oro xerces-j2
EOF

# bin
%__mkdir_p %{buildroot}%{_bindir}
%__install -m 755 cobertura-check.sh %{buildroot}%{_bindir}/cobertura-check
%__install -m 755 cobertura-instrument.sh %{buildroot}%{_bindir}/cobertura-instrument
%__install -m 755 cobertura-merge.sh %{buildroot}%{_bindir}/cobertura-merge
%__install -m 755 cobertura-report.sh %{buildroot}%{_bindir}/cobertura-report

# javadoc
%__mkdir_p %{buildroot}%{_javadocdir}/%{name}
%__cp -a build/api/* %{buildroot}%{_javadocdir}/%{name}

%clean
%__rm -rf %{buildroot}

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING COPYRIGHT README
%attr(0755,root,root) %{_bindir}/*
%{_javadir}/*.jar
%config(noreplace) %{_sysconfdir}/ant.d/%{name}
%{_datadir}/maven2/poms/JPP-%{name}*.pom
%config(noreplace) %{_mavendepmapfragdir}/cobertura

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9-3m)
- full rebuild for mo7 release

* Fri Feb  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9-2m)
- release %%{_mavendepmapfragdir}
- it's already provided by jpackage-utils and cobertura Requires: jpackage-utils
- and specify a target of %%config(noreplace)

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-1m)
- import from Rawhide for netbeans-6.7.1

* Wed Aug 19 2009  Victor Vasilyev <victor.vasilyev@sun.com> 1.9-3
- Fix B(R) according to guidelines
- Use the  lnSysJAR macro
- Prevent brp-java-repack-jars from being run
* Sun Aug 09 2009  Victor Vasilyev <victor.vasilyev@sun.com> 1.9-2
- The license tag is changed according to http://cobertura.sourceforge.net/license.html
* Fri Jun 19 2009  Victor Vasilyev <victor.vasilyev@sun.com> 1.9-1
- release 1.9
