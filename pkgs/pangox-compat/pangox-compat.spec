%global momorel 1
Name:           pangox-compat
Version:        0.0.2
Release:        %{momorel}m%{?dist}
Summary:        Compatibility library for pangox

License:        LGPLv2+
Group:          System Environment/Libraries
URL:            http://ftp.gnome.org/pub/GNOME/sources/pangox-compat/0.0/
Source0:        http://ftp.gnome.org/pub/GNOME/sources/pangox-compat/0.0/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires:  pango-devel

%description
This is a compatibility library providing the obsolete pangox library
that is not shipped by Pango itself anymore.  

%package devel
Summary: Development files for pangox-compat
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q


%build
%configure --disable-static
%make

%install
%makeinstall

find %{buildroot} -name '*.la' -exec rm -f {} ';'

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc README COPYING NEWS AUTHORS
%{_libdir}/libpango*-*.so.*
%config %{_sysconfdir}/pango/pangox.aliases

%files devel
%{_libdir}/libpango*.so
%{_includedir}/*
%{_libdir}/pkgconfig/*

%changelog
* Wed Oct 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2-1m)
- Initial commit Momonga Linux
