%global momorel 4

Name:           jcip-annotations
Version:        1
Release:        0.0.20060626.%{momorel}m%{?dist}
Summary:        Java annotations for multithreaded software

Group:          Development/Libraries
License:        "CC-BY"
URL:            http://www.jcip.net/
Source0:        http://www.jcip.net/%{name}-src.jar
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# There is no point in building native libraries, as the sources contain only
# annotation definitions, so no code would be generated.
BuildArch:      noarch
BuildRequires:  java-devel >= 1.5.0, jpackage-utils

Requires:       java >= 1.5.0, jpackage-utils

%description
This package provides class, field, and method level annotations for
describing thread-safety policies.  These annotations are relatively
unintrusive and are beneficial to both users and maintainers.  Users can see
immediately whether a class is thread-safe, and maintainers can see
immediately whether thread-safety guarantees must be preserved.  Annotations
are also useful to a third constituency: tools.  Static code-analysis tools
may be able to verify that the code complies with the contract indicated by
the annotation, such as verifying that a class annotated with @Immutable
actually is immutable.

%package javadoc
Group:          Documentation
Summary:        Javadoc for jcip-annotations
Requires:       %{name} = %{version}-%{release}, jpackage-utils

%description javadoc
Javadoc documentation for the jcip-annotations package.
On systems where javadoc is sinjdoc, this package contains nothing useful
since sinjdoc does not understand annotations.

%prep
%setup -q -c

# Get rid of the manifest created upstream with ant
rm -fr META-INF

# Fix DOS line endings
sed -i 's/\r//' net/jcip/annotations/package.html

%build
mkdir classes
find . -name '*.java' | xargs %javac -g -source 1.5 -target 1.5 -d classes
cd classes
%jar cf ../%{name}-%{version}.jar net
cd ..
ln -s %{name}-%{version}.jar %{name}.jar
%javadoc -d docs -source 1.5 net.jcip.annotations

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_javadir}
mv *.jar $RPM_BUILD_ROOT%{_javadir}
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr docs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}
%add_to_maven_depmap net.jcip.annotations jcip-annotations %{version} JPP/jcip-annotations jcip-annotations

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_javadir}/%{name}-%{version}.jar
%{_javadir}/%{name}.jar
%{_mavendepmapfragdir}/%{name}

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-0.0.20060626.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-0.0.20060626.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1-0.0.20060626.2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1-0.0.20060626.1m)
- sync with Fedora 13 (1-1.20060626)

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.0.20060627.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.0.20060627.1m)
- import from Fedora 11 for findbugs

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-20060627.4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon May  5 2008 Jerry James <loganjerry@gmail.com> - 0-20060626.4
- Don't package source or HTML files in the jar

* Fri Apr 18 2008 Jerry James <loganjerry@gmail.com> - 0-20060626.3
- Changes required by new Java packaging guidelines

* Wed Nov 14 2007 Jerry James <loganjerry@gmail.com> - 0-20060626.2
- Don't make the javadocs appear in a docs subdirectory

* Tue Sep 18 2007 Jerry James <loganjerry@gmail.com> - 0-20060626.1
- Initial RPM
