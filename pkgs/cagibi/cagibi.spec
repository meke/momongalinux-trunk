%global         momorel 1
%global         srcver 0.2.0

%global         qtver 4.7.2
%global         qtrel 1m
%global         cmakever 2.6.4
%global         cmakerel 1m
%global         sourcedir stable/%{name}

Name:           cagibi
Version:        %{srcver}
Release:        %{momorel}m%{?dist}
Summary:        SSDP (UPnP discovery) cache/proxy daemon
Group:          System Environment/Base
License:        GPLv2+ and LGPLv2+
URL:            http://www.kde.org/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel
BuildRequires:  cmake
BuildRequires:  automoc

%description
Cagibi is a cache/proxy daemon for SSDP (the discovery part of UPnP).

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} ..
popd

make %{?_smp_mflags}  -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING COPYING.LIB
%config(noreplace) %attr(644,root,root) %{_sysconfdir}/cagibid.conf
%{_sysconfdir}/dbus-1/system.d/org.kde.Cagibi.conf
%{_bindir}/cagibid
%{_datadir}/dbus-1/interfaces/org.kde.Cagibi.*
%{_datadir}/dbus-1/system-services/org.kde.Cagibi.service

%changelog
* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-4m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-3m)
- specify PATH for Qt4

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-2m)
- fix build failure

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-1m)
- import from Fedora devel

* Tue Aug 10 2010 Jaroslav Reznik <jreznik@redhat.com> - 0.1.1-1
- Update to 0.1.1
- Added pkgconfig file

* Wed Aug 04 2010 Jaroslav Reznik <jreznik@redhat.com> - 0.1.0-2
- Fixed changelog entry
- COPYING.LIB in docs

* Wed Jul 28 2010 Jaroslav Reznik <jreznik@redhat.com> - 0.1.0-1
- Initial package
