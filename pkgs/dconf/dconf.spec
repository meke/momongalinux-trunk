%global momorel 1
%define glib2_version 2.33.6
%define vala_version 0.17.4

Name:           dconf
Version:        0.14.1
Release: %{momorel}m%{?dist}
Summary:        A configuration system

Group:          System Environment/Base
License:        LGPLv2+
URL:            http://live.gnome.org/dconf
#VCS:           git:git://git.gnome.org/dconf
Source0:        http://download.gnome.org/sources/dconf/0.14/dconf-%{version}.tar.xz
NoSource: 0

BuildRequires:  glib2-devel >= %{glib2_version}
BuildRequires:  gtk3-devel
BuildRequires:  libxml2-devel
BuildRequires:  dbus-devel
BuildRequires:  vala-devel >= %{vala_version}
BuildRequires:  gtk-doc

Requires:       dbus
Requires:	glib2 >= %{glib2_version}

%description
dconf is a low-level configuration system. Its main purpose is to provide a
backend to the GSettings API in GLib.

%package devel
Summary: Header files and libraries for dconf development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel >= %{glib2_version}

%description devel
dconf development package. Contains files needed for doing software
development using dconf.

%package editor
Summary: Configuration editor for dconf
Group:   Applications/System
Requires: %{name} = %{version}-%{release}

%description editor
dconf-editor allows you to browse and modify dconf databases.


%prep
%setup -q

%build
%configure --disable-static
%make 


%install
make install DESTDIR=%{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
  /sbin/ldconfig
  gio-querymodules-%{__isa_bits} %{_libdir}/gio/modules
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans
/sbin/ldconfig
gio-querymodules-%{__isa_bits} %{_libdir}/gio/modules
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :


%files
%doc COPYING
%{_libdir}/gio/modules/libdconfsettings.so
%{_libexecdir}/dconf-service
%{_datadir}/dbus-1/services/ca.desrt.dconf.service
%{_bindir}/dconf
%{_libdir}/libdconf.so.*
%{_libdir}/libdconf-dbus-1.so.*
%{_datadir}/bash-completion/completions/dconf
%{_datadir}/glib-2.0/schemas/ca.desrt.dconf-editor.gschema.xml
%{_datadir}/locale/*/*/*
%{_datadir}/man/man?/dconf*

%files devel
%{_includedir}/dconf
%{_libdir}/libdconf.so
%{_libdir}/pkgconfig/dconf.pc
%{_includedir}/dconf-dbus-1
%{_libdir}/libdconf-dbus-1.so
%{_libdir}/pkgconfig/dconf-dbus-1.pc
%{_datadir}/gtk-doc/html/dconf
%{_datadir}/vala

%files editor
%{_bindir}/dconf-editor
%{_datadir}/applications/dconf-editor.desktop
%dir %{_datadir}/dconf-editor
%{_datadir}/dconf-editor/dconf-editor.ui
%{_datadir}/dconf-editor/dconf-editor-menu.ui
%{_datadir}/icons/hicolor/*/apps/dconf-editor.png

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.1-1m)
- update to 0.14.1

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.0-1m)
- update to 0.14.0

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.90-1m)
- update to 0.13.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.5-1m)
- update to 0.13.5

* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.4-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.0-4m)
- rebuild for glib 2.33.2

* Thu Nov  3 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10.0-3m)
- modify spec

* Thu Oct 27 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.10.0-2m)
- require adjustment

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Sun May 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.5-2m)
- revised BR

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.4-1m)
- update to 0.7.4

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-5m)
- rebuild for new GCC 4.6

* Sat Mar 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-4m)
- add patch for gcc46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-3m)
- rebuild for new GCC 4.5

* Sun Oct 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-2m)
- add gio-querymodules to script

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-1m)
- initial build
