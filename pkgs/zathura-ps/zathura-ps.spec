%global momorel 1
Name:           zathura-ps
Version:        0.2.2
Release: %{momorel}m%{?dist}
Summary:        PS support for zathura via libspectre
Group:          Applications/Publishing
License:        "zlib"
URL:            http://pwmt.org/projects/zathura/plugins/%{name}
Source0:        http://pwmt.org/projects/zathura/plugins/download/%{name}-%{version}.tar.gz
NoSource: 0
Requires:       zathura
BuildRequires:  libspectre-devel
BuildRequires:  zathura-devel >= 0.2.3

%description
The zathura-ps plugin adds PostScript support to zathura by
using the libspectre library.

%prep
%setup -q
# Don't rebuild during install phase
sed -i 's/^install:\s*all/install:/' Makefile

%build
CFLAGS='%{optflags}' make %{?_smp_mflags} LIBDIR=%{_libdir} debug
mv ps-debug.so ps.so

%install
make DESTDIR=%{buildroot} LIBDIR=%{_libdir} install

%files
%doc AUTHORS LICENSE
%{_libdir}/zathura/ps.so
%{_datadir}/applications/%{name}.desktop

%changelog
* Wed Feb 19 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-1m)
- import from fedora

