%global         momorel 1
%global         realver 2.21

Name:           perl-AnyEvent-HTTP
Version:        %{realver}
Release:        %{momorel}m%{?dist}
Summary:        Simple but non-blocking HTTP/HTTPS client
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/AnyEvent-HTTP/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/AnyEvent-HTTP-%{realver}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-AnyEvent >= 5
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl-AnyEvent >= 5
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module is an AnyEvent user, you need to make sure that you use and run
a supported event loop.

%prep
%setup -q -n AnyEvent-HTTP-%{realver}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING README
%{perl_vendorlib}/AnyEvent/HTTP*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-1m)
- rebuild against perl-5.20.0
- update to 2.21

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-2m)
- rebuild against perl-5.16.3

* Thu Nov 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-1m)
- update to 2.15

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-1m)
- update to 2.14
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-1m)
- update to 2.13

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-1m)
- update to 2.12

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-2m)
- rebuild against perl-5.14.0-0.2.1m

* Fri Apr 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-1m)
- update to 2.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-2m)
- rebuild for new GCC 4.6

* Sat Feb 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02

* Tue Jan 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-1m)
- update to 2.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-1m)
- update to 1.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.46-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-1m)
- update to 1.46

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-2m)
- full rebuild for mo7 release

* Thu Jun 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-1m)
- update to 1.45

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-2m)
- rebuild against perl-5.12.0

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.43-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.43-3m)
- rebuild against perl-5.10.1

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.43-2m)
- use pacakge name

* Thu Aug 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.43-1m)
- update to 1.43

* Sun Jul 19 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (1.4-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
