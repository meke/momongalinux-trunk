%global momorel 1
%define tarball xf86-input-vmmouse
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/input

Summary:   Xorg X11 vmmouse input driver
Name:      xorg-x11-drv-vmmouse
Version: 13.0.0
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
Patch1:    vmmouse-12.6.9-iopl-revert.patch
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64 alpha sparc sparc64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires: xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 vmmouse input driver.

%prep
%setup -q -n %{tarball}-%{version}

%patch1 -p1 -b .remove-call-to-iopl

%build
autoreconf -ivf
%configure --disable-static --disable-silent-rules --with-xorg-conf-dir='%{_datadir}/X11/xorg.conf.d'
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

# Don't need HAL no more
rm -rf $RPM_BUILD_ROOT/%{_libdir}/hal/hal-probe-vmmouse
rm -rf $RPM_BUILD_ROOT/%{_datadir}/hal/fdi/

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/vmmouse_detect
%{driverdir}/vmmouse_drv.so
%exclude %{driverdir}/vmmouse_drv.la
%{_mandir}/man1/vmmouse_detect.1.*
%{_mandir}/man4/vmmouse.4.*

/lib/udev/rules.d/69-xorg-vmmouse.rules
%{_datadir}/X11/xorg.conf.d/50-vmmouse.conf

%changelog
* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (13.0.0-1m)
- update 13.0.0

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.9.0-1m)
- update 12.9.0

* Fri Mar 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.8.0-1m)
- update 12.8.0

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.7.0-5m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.7.0-4m)
- no use hal

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.7.0-3m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (12.7.0-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.7.0-1m)
- update to 12.7.0

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.6.10-1m)
- update to 12.6.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (12.6.9-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (12.6.9-3m)
- full rebuild for mo7 release

* Sun Aug 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (12.6.9-2m)
- [BUG FIXES] import 2 patches from Fedora
 - single-udev-match.patch
  - [PATCH] Only match against event[0-9] in udev rules
 - iopl-revert.patch
  - https://bugzilla.novell.com/show_bug.cgi?id=604966

* Fri May 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.6.9-1m)
- update 12.6.9

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (12.6.7-1m)
- update 12.6.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (12.6.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.6.5-1m)
- update 12.6.5

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.6.3-2m)
- rebuild against xorg-x11-server 1.6

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.6.3-1m)
- update 12.6.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (12.6.2-2m)
- rebuild against rpm-4.6

* Mon Nov 17 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.6.2-1m)
- update 12.6.2

* Sun Oct 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (12.5.2-1m)
- update 12.5.2

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.5.1-1m)
- update 12.5.1

* Sun May  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (12.5.0-1m)
- update 12.5.0
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (12.4.3-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (12.4.3-2m)
- %%NoSource -> NoSource

* Wed Oct  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.4.3-1m)
- update 12.4.3

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (12.4.2-1m)
- update 12.4.2
- rebuild against xorg-x11-server-1.4

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.4.1-1m)
- update to 12.4.1

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (12.4.0-1m)
- initial build
