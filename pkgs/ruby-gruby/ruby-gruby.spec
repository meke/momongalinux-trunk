%global momorel 12

%global rbname gruby

Summary: gRuby
Name: ruby-%{rbname}

Version: 0.0.1
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: LGPL
URL: http://gruby.sourceforge.jp/

Source0: http://osdn.dl.sourceforge.jp/gruby/7814/gruby-%{version}.tar.gz 
NoSource: 0

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby-GD

Requires: ruby-GD, ruby-uconv

%description
gRuby is wrapper of Ruby/GD.

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q  -n gruby-%{version}

%build
ruby setup.rb --quiet config \
    --site-ruby=%{buildroot}%{ruby_libdir}

ruby setup.rb --quiet setup

%install
rm -rf %{buildroot}
ruby setup.rb --quiet install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LGPL README.en.rd README.ja.rd example
%{ruby_libdir}/grb

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.1-12m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1-9m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1-8m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1-3m)
- %%NoSource -> NoSource

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.1-2m)
- BuilArch: noarch

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.0.1-1m)
- version 0.0.1

* Tue Aug  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0-0.20020924.4m)
- be quiet

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0-0.20020924.3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0-0.20020924.2m)
- rebuild against ruby-1.8.0.

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0-0.20020924.1m)
- 2002-09-24 snap.
