%global momorel 2

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

%define talloc_version 2.0.8
%define tdb_version 1.2.13
%define tevent_version 0.9.18

Name: libldb
Version: 1.1.16
Release: %{momorel}m%{?dist}
Group: Development/Libraries
Summary: A schema-less, ldap like, API and database
Requires: libtalloc >= %{talloc_version}
Requires: libtdb >= %{tdb_version}
Requires: libtevent >= %{tevent_version}
License: LGPLv3+
URL: http://ldb.samba.org/
Source: http://samba.org/ftp/ldb/ldb-%{version}.tar.gz
NoSource: 0

BuildRequires: libtalloc-devel >= %{talloc_version}
BuildRequires: libtdb-devel >= %{tdb_version}
BuildRequires: libtevent-devel >= %{tevent_version}
BuildRequires: python-tevent >= %{tevent_version}
BuildRequires: popt-devel
BuildRequires: autoconf
BuildRequires: libxslt
BuildRequires: docbook-style-xsl
BuildRequires: python-devel
BuildRequires: python-tdb
BuildRequires: pytalloc-devel

%description
An extensible library that implements an LDAP like API to access remote LDAP
servers, or use local tdb databases.

%package -n ldb-tools
Group: Development/Libraries
Summary: Tools to manage LDB files
Requires: libldb = %{version}-%{release}

%description -n ldb-tools
Tools to manage LDB files

%package devel
Group: Development/Libraries
Summary: Developer tools for the LDB library
Requires: libldb = %{version}-%{release}
Requires: libtdb-devel >= %{tdb_version}
Requires: libtalloc-devel >= %{talloc_version}
Requires: libtevent-devel >= %{tevent_version}
Requires: pkgconfig

%description devel
Header files needed to develop programs that link against the LDB library.

%package -n pyldb
Group: Development/Libraries
Summary: Python bindings for the LDB library
Requires: libldb = %{version}-%{release}
Requires: python-tdb = %{tdb_version}

%description -n pyldb
Python bindings for the LDB library

%package -n pyldb-devel
Group: Development/Libraries
Summary: Development files for the Python bindings for the LDB library
Requires: pyldb = %{version}-%{release}

%description -n pyldb-devel
Development files for the Python bindings for the LDB library

%prep
%setup -q -n ldb-%{version}

%build
%configure --disable-rpath \
           --disable-rpath-install \
           --bundled-libraries=NONE \
           --builtin-libraries=replace \
           --with-modulesdir=%{_libdir}/ldb/modules \
           --with-privatelibdir=%{_libdir}/ldb

# Turn off parallel make; it will fail because of waf's bug
make V=1 -j1

%install
make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/libldb.a

# Shared libraries need to be marked executable for
# rpmbuild to strip them and include them in debuginfo
find %{buildroot} -name "*.so*" -exec chmod -c +x {} \;

# LDB 1.1.8 bug: remove manpage named after full
# file path
rm -f $RPM_BUILD_ROOT/%{_mandir}/man3/_*

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%dir %{_libdir}/ldb
%{_libdir}/libldb.so.*
%dir %{_libdir}/ldb/modules
%dir %{_libdir}/ldb/modules/ldb
%{_libdir}/ldb/modules/ldb/*.so

%files -n ldb-tools
%defattr(-,root,root,-)
%{_bindir}/ldbadd
%{_bindir}/ldbdel
%{_bindir}/ldbedit
%{_bindir}/ldbmodify
%{_bindir}/ldbrename
%{_bindir}/ldbsearch
%{_libdir}/ldb/libldb-cmdline.so
%{_mandir}/man1/ldbadd.1.*
%{_mandir}/man1/ldbdel.1.*
%{_mandir}/man1/ldbedit.1.*
%{_mandir}/man1/ldbmodify.1.*
%{_mandir}/man1/ldbrename.1.*
%{_mandir}/man1/ldbsearch.1.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/ldb_module.h
%{_includedir}/ldb_handlers.h
%{_includedir}/ldb_errors.h
%{_includedir}/ldb_version.h
%{_includedir}/ldb.h
%{_libdir}/libldb.so

%{_libdir}/pkgconfig/ldb.pc
%{_mandir}/man3/ldb.3.*

%files -n pyldb
%defattr(-,root,root,-)
%{python_sitearch}/ldb.so
%{_libdir}/libpyldb-util.so.1*

%files -n pyldb-devel
%defattr(-,root,root,-)
%{_includedir}/pyldb.h
%{_libdir}/libpyldb-util.so
%{_libdir}/pkgconfig/pyldb-util.pc

%post -n pyldb -p /sbin/ldconfig
%postun -n pyldb -p /sbin/ldconfig

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.16-2m)
- rebuild against tdb-1.2.13

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.16-1m)
- update to 1.1.16

* Wed Feb  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.15-1m)
- update to 1.1.15

* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.14-1m)
- update to 1.1.14

* Wed Oct 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.13-1m)
- update to 1.1.13

* Thu Sep 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.12-1m)
- update to 1.1.12

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.8-1m)
- update to 1.1.8

* Wed Jan 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-3m)
- fix build failure
-- disable paralle build to avoid a possible dead lock of waf

* Thu Jan  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-2m)
- add BuildRequires

* Sat Dec 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Sun Aug 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- Initial Commit Momonga Linux

* Tue Aug  2 2011 Simo Sorce <ssorce@redhat.com> - 1.1.0-1
- Update to 1.1.0
  (dependency for samba4 alpha16 snapshot)

* Tue Feb 22 2011 Simo Sorce <ssorce@redhat.com> - 1.0.2-1
- Update to 1.0.2
  (dependency for samba4 alpha15 snapshot)

* Fri Feb 11 2011 Stephen Gallagher <sgallagh@redhat.com> - 1.0.0-2
- Disable rpath

* Fri Feb 11 2011 Stephen Gallagher <sgallagh@redhat.com> - 1.0.0-1
- New upstream release 1.0.0
- SOname bump to account for module loading changes
- Rename libldb-tools to ldb-tools to make upgrades easier

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.22-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri Feb 04 2011 Stephen Gallagher <sgallagh@redhat.com> - 0.9.22-8
- Fixes from package review
- Change Requires: on tools subpackage to be the exact version/release
- Remove unnecessary BuildRoot directive

* Mon Jan 17 2011 Stephen Gallagher <sgallagh@redhat.com> - 0.9.22-7
- Update to 0.9.22 (first independent release of libldb upstream)

