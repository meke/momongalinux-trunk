%global momorel 1

Summary: udev wrapping and abstraction library
Name: eeze
Version: 1.7.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://www.enlightenment.org/
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2 
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ecore-devel >= 1.7.7
BuildRequires: systemd-devel >= 187

%description
Eeze is a library for manipulating devices through udev with a simple and fast
api. It interfaces directly with libudev, avoiding such middleman daemons as
udisks/upower or hal, to immediately gather device information the instant it
becomes known to the system.  This can be used to determine such things as:
  * If a cdrom has a disk inserted
  * The temperature of a cpu core
  * The remaining power left in a battery
  * The current power consumption of various parts
  * Monitor in realtime the status of peripheral devices


%package devel
Summary: Development files for %{name}
Group: System Environment/Libraries
Requires: %{name} = %{version}
Requires: libudev-devel ecore-devel pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static
%{__make} %{?_smp_mflags} %{?mflags}

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
chrpath --delete %{buildroot}%{_libdir}/*.so.*
find %{buildroot} -name '*.la' -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root,-)
%doc AUTHORS COPYING README
%{_libdir}/*.so.*
%exclude %{_bindir}/*

%files devel
%defattr(-, root, root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%exclude %{_bindir}/*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-2m)
- rebuild against systemd-187

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- initial import for Momonga Linux
