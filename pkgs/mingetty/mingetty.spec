%global momorel 7

Summary: A compact getty program for virtual consoles only.
Name: mingetty
Version: 1.08
License: GPLv2+
Release: %{momorel}m%{?dist}
Group: System Environment/Base
Source: mingetty-%{version}.tar.gz
Patch0: mingetty-1.00-opt.patch
Patch1: mingetty-1.08-check_chroot_chdir_nice.patch
Patch2: mingetty-1.08-openlog_authpriv.patch
Patch3: mingetty-1.08-limit_tty_length.patch
Patch4: mingetty-1.08-Allow-login-name-up-to-LOGIN_NAME_MAX-length.patch
Patch5: mingetty-1.08-Clear-scroll-back-buffer-on-clear-screen.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The mingetty program is a lightweight, minimalist getty program for
use only on virtual consoles.  Mingetty is not suitable for serial
lines (you should use the mgetty program in that case).

%prep
%setup -q
%patch0 -p1 -b .opt
%patch1 -p1 -b .chroot
%patch2 -p1 -b .openlog
%patch3 -p1 -b .tty_length
%patch4 -p1 -b .loginname_length
%patch5 -p1 -b .clear_buffer

%build
make "RPM_OPTS=%{optflags}"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}%{_mandir}/man8

install -m 0755 mingetty %{buildroot}/sbin/
install -m 0644 mingetty.8 %{buildroot}%{_mandir}/man8/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
/sbin/mingetty
%{_mandir}/man8/mingetty.*

%changelog
* Wed Jul 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.08-8m)
- add patch. import from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.08-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.08-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.08-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08, sync with Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.07-2m)
- rebuild against gcc43

* Fri Jan 30 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.07-1m)
- update to 1.07 - imported from Fedora (1.07-1)
- %%global momorel

* Tue Jul 29 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.06-1m)
- update to 1.06
- use %%{momorel} macro
- sync with RawHide(mingetty-1.06-2)
- remove patches:
-  mingetty-0.9.4-make.patch, mingetty-0.9.4-glibc.patch,
-  mingetty-0.9.4-isprint.patch, mingetty-0.9.4-wtmplock.patch
- remove -s option from install mingetty
- remove ANNOUNCE TODO from %%doc

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.4-16k)
- NoSource statement was removed.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Apr 07 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.9.4-11).

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 10)

* Wed Jan 06 1999 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Fri May 01 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- fixed build problems on intel and alpha for manhattan

* Tue Oct 21 1997 Donnie Barnes <djb@redhat.com>
- spec file cleanups

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
~- built against glibc
