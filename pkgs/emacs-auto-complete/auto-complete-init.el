;;; Set up auto-complete for Emacs.
;;;
;;; This file is automatically loaded by emacs's site-start.el
;;; when you start a new emacs session.

(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories
  "/usr/share/emacs/site-lisp/auto-complete/ac-dict")
(ac-config-default)

