%global momorel 2
%global pkg     auto-complete
%global pkgname Auto Complete Mode


Name:           emacs-%{pkg}
Version:        1.3.1
Release:        %{momorel}m%{?dist}
Summary:        Intelligent auto-complete extension for GNU Emacs

Group:          Applications/Editors
# documentation files are under GFDL
License:        GPLv3 and GFDL
URL:            http://cx4a.org/software/auto-complete/
Source0:        http://cx4a.org/pub/auto-complete/auto-complete-%{version}.tar.bz2
NoSource:	0
Source1:        auto-complete-init.el

BuildArch:      noarch
BuildRequires:  emacs
Requires:       emacs(bin) >= %{_emacs_version}

%description
%{pkgname} is an intelligent auto-completion extension for GNU
Emacs. Auto Complete Mode renews an old completion interface and
provides an environment that makes users could be more concentrate on
their own works.

It comes with built-in completion support for C, C++, Clojure, CSS,
Java, JavaScript, PHP, Python, Ruby, Scheme and TCL.

%package -n %{name}-el
Summary:        Elisp source files for %{pkgname} under GNU Emacs
Group:          Applications/Editors
Requires:       %{name} = %{version}-%{release}

%description -n %{name}-el
This package contains the elisp source files for %{pkgname}
under GNU Emacs. You do not need to install this package to run
%{pkgname}. Install the %{name} package to use
%{pkgname} with GNU Emacs.

%prep
%setup -q -n %{pkg}-%{version}

%build
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{pkg}
mkdir -p $RPM_BUILD_ROOT%{_emacs_sitestartdir}
make install DIR=$RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{pkg}
cp -p %{SOURCE1} $RPM_BUILD_ROOT%{_emacs_sitestartdir}

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING.* README.txt TODO.txt doc/*
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitelispdir}/%{pkg}/ac-dict
%{_emacs_sitestartdir}/*.el
%dir %{_emacs_sitelispdir}/%{pkg}

%files -n %{name}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-2m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-1m)
- import from fedora