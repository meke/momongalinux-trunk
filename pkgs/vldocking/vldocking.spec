%global momorel 2

Name:		vldocking
Version:	2.1.5
Release:	%{momorel}m%{?dist}
Summary:	A Java ™ docking system for JFC Swing applications
Group:		Development/Tools
License:	see "Licence_CeCILL-V1.1_VA.pdf"
URL:		http://www.vlsolutions.com/en/download/
# the zip file can be downloaded from the redirect 
# from  http://www.vlsolutions.com/en/download/downloader2_0.php
 
Source0:	vldocking_%{version}.zip
Patch1:		vldocking-build.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch

BuildRequires:	java-devel >= 1.5 
BuildRequires:	jpackage-utils, ant

Requires:	java >= 1.5
Requires:	jpackage-utils


%description 
Docking windows allow the user to reorganize the application's workspace
according to his needs:

	* Drag and Drop parts of the application ("Dockables")
	* Hide the dockables not often used to save screen space
	* Detach some dockables and have them floating outside the window
	* Easily switch between different workspaces
	* And much more... 

%package	javadoc
Summary:	Javadocs for %{name}
Group:		Documentation
Requires:	%{name} = %{version}-%{release}
Requires:	jpackage-utils
%description	javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}_%{version}

find -name '*.jar' -o -name '*.class' -exec rm -f '{}' \;

%build

ant jar
ant javadoc

%install
rm -rf $RPM_BUILD_ROOT

install -D jar/%{name}_%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar

pushd $RPM_BUILD_ROOT%{_javadir}
	ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar
popd

mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}
cp -rp doc/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Licence_CeCILL-V1_VF.pdf Licence_CeCILL-V1.1_VA.pdf
%{_javadir}/*

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.5-2m)
- rebuild for new GCC 4.6

* Fri Feb 25 2011 Yasuo Ohgaki <yohgaki@ohgaki.net> 
- 2.1.5-1m
- Import from Fedora

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Aug 22 2010 Ismael Olea <ismael@olea.org> 2.1.5-1
- updating to 2.1.5
- vldocking-build.patch not needed anymore

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.6e-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.6e-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep 17 2008 Ismael Olea <ismael@olea.org> 2.0.6e-4olea
- Cosmetic changes.

* Sun Aug 31 2008 Ismael Olea <ismael@olea.org> 2.0.6e-3olea
- Cosmetic changes.

* Fri Aug 29 2008 Ismael Olea <ismael@olea.org> 2.0.6e-2olea
- QA revision

* Wed Aug 27 2008 Ismael Olea <ismael@olea.org> 2.0.6e-1olea
- first version
