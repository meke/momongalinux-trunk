%global momorel 8

%global rbname rttool
Summary: RTtool is very simple table generator
Name: ruby-%{rbname}

Version: 1.0.3
Release: %{momorel}m%{?dist}
Group:  Applications/Text
License: Ruby
URL: http://www.rubyist.net/~rubikitch/computer/rttool/

Source0: http://www.rubyist.net/~rubikitch/archive/%{rbname}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: ruby

Requires: ruby

%description
This name is associated with RDtool by Tosh.
Using RTtool it is VERY EASY to write RD with table.

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q -n rttool-%{version}

%build

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
ruby setup.rb config \
    --bin-dir=%{buildroot}%{_bindir} \
    --rb-dir=%{buildroot}%{ruby_libdir}
ruby setup.rb install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog GPL rttool.en.* rttool.ja.* examples
%{_bindir}/rdrt2
%{_bindir}/rt2
%{ruby_libdir}/rd
%{ruby_libdir}/rt

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.3-8m)
- use RbConfig 

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-5m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-4m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.8-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.8-6m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.8-5m)
- rebuild against ruby-1.8.6-4m

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.8-4m)
- apply 'rttool-0.1.8-ruby1.8.patch' to remove warnings in ruby-1.8

* Tue Dec 21 2004 Toru Hoshina <toruhosh@k3.dion.ne.jp>
- (0.1.8-3m)
- change md5sum at %%NoSource 0 again.

* Sat Sep 04 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.8-2m)
- change md5sum at %%NoSource 0 (tarball rt/rdrt2 was changed)

* Thu May 20 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.8-1m)
- verup

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.1.7-5m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.1.7-4m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.1.7-3m)
- rebuild against ruby-1.8.0.

* Wed Apr 24 2002 Junichiro Kita <kita@kondara.org>
- (0.1.7-2k)
