%global momorel 1
Name: gnome-themes-standard
Version: 3.6.0.2
Release: %{momorel}m%{?dist}
Summary: Standard themes for GNOME applications

Group: User Interface/Desktops
License: LGPLv2+
URL: http://git.gnome.org/browse/gnome-themes-standard
Source0: http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
Source1: settings.ini
Source2: gtkrc

Obsoletes: gnome-themes <= %{version}
Provides: gnome-themes = %{version}

Obsoletes: fedora-gnome-theme <= 15.0
Provides: fedora-gnome-theme = 1:%{version}-%{release}

Obsoletes: gnome-background-standard < 3.0.0-2
Provides: gnome-background-standard = %{version}-%{release}

BuildRequires: gtk3-devel >= 3.5.17
BuildRequires: librsvg2-devel
BuildRequires: intltool gettext autoconf automake
# for gtk-update-icon-cache
BuildRequires: gtk2
Requires: gtk2-engines gnome-icon-theme abattis-cantarell-fonts
Requires: adwaita-cursor-theme = %{version}-%{release}
Requires: adwaita-gtk2-theme = %{version}-%{release}
Requires: adwaita-gtk3-theme = %{version}-%{release}

%description
The gnome-themes-standard package contains the standard theme for the GNOME
desktop, which provides default appearance for cursors, desktop background,
window borders and GTK+ applications.

%package -n adwaita-cursor-theme
Summary: Adwaita cursor theme
Group: User Interface/Desktops
BuildArch: noarch

%description -n adwaita-cursor-theme
The adwaita-cursor-theme package contains a modern set of cursors originally
designed for the GNOME desktop.

%package -n adwaita-gtk2-theme
Summary: Adwaita gtk2 theme
Group: User Interface/Desktops

%description -n adwaita-gtk2-theme
The adwaita-gtk2-theme package contains a gtk2 theme for presenting widgets
with a GNOME look and feel.

%package -n adwaita-gtk3-theme
Summary: Adwaita gtk3 theme
Group: User Interface/Desktops

%description -n adwaita-gtk3-theme
The adwaita-gtk3-theme package contains a gtk3 theme for rendering widgets
with a GNOME look and feel.

%prep
%setup -q

%build
%configure
%make 

%install
make install DESTDIR=%{buildroot}

for t in HighContrast; do
  rm -f %{buildroot}%{_datadir}/icons/$t/icon-theme.cache
  touch %{buildroot}%{_datadir}/icons/$t/icon-theme.cache 
done

rm %{buildroot}%{_libdir}/gtk-3.0/3.0.0/theming-engines/libadwaita.la

mkdir -p %{buildroot}%{_sysconfdir}/gtk-3.0
cp $RPM_SOURCE_DIR/settings.ini %{buildroot}%{_sysconfdir}/gtk-3.0/settings.ini
mkdir -p %{buildroot}%{_sysconfdir}/gtk-2.0
cp $RPM_SOURCE_DIR/gtkrc %{buildroot}%{_sysconfdir}/gtk-2.0/gtkrc

%find_lang %{name}

%post
for t in HighContrast; do 
  touch --no-create %{_datadir}/icons/$t &>/dev/null || :
done

%posttrans
for t in HighContrast; do
  gtk-update-icon-cache %{_datadir}/icons/$t &>/dev/null || :
done

%files -f %{name}.lang
%doc COPYING NEWS

# Background and WM
%{_datadir}/themes/Adwaita
%exclude %{_datadir}/themes/Adwaita/gtk-2.0
%exclude %{_datadir}/themes/Adwaita/gtk-3.0
%exclude %{_datadir}/themes/Adwaita/gtk-3.0

# A11y themes
%ghost %{_datadir}/icons/HighContrast/icon-theme.cache
%{_datadir}/icons/HighContrast
%{_datadir}/themes/HighContrast

# Background
%{_datadir}/gnome-background-properties/*

%files -n adwaita-cursor-theme
%defattr(-,root,root,-)
# Cursors
%{_datadir}/icons/Adwaita

%files -n adwaita-gtk2-theme
# gtk2 Theme
%{_datadir}/themes/Adwaita/gtk-2.0
# Default gtk2 settings
%{_sysconfdir}/gtk-2.0/gtkrc

%files -n adwaita-gtk3-theme
# gtk3 Theme and engine
%{_libdir}/gtk-3.0/3.0.0/theming-engines/libadwaita.so
%{_datadir}/themes/Adwaita/gtk-3.0
# Default gtk3 settings
%{_sysconfdir}/gtk-3.0/settings.ini


%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0.2-1m)
- update to 3.6.0.2

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-2m)
- revise %%files

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0.2-1m)
- update to 3.2.0.2

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-2m)
- obsoletes gnome-themes

* Sun Apr 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- initial build

