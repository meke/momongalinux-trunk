%global momorel 1
%define tarball xf86-input-wacom
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/input

Summary: Xorg X11 wacom driver
Name: xorg-x11-drv-wacom
Version: 0.17.0
Release: %{momorel}m%{?dist}
URL: http://www.x.org/
# %global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: http://dl.sourceforge.net/sourceforge/linuxwacom/%{tarball}/%{tarball}-%{version}.tar.bz2 
NoSource: 0
License: MIT/X
Group: User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires:  xorg-x11-server-Xorg >= 1.13.0

Obsoletes: linuxwacom
Provides: linuxwacom

%description 
X.Org X11 wacom driver.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: linuxwacom-devel
Provides: linuxwacom-devel

%description devel
%{name}-devel

%prep
%setup -q -n %{tarball}-%{version}

%build
%configure --disable-static \
    --enable-silent-rules \
    --with-xorg-conf-dir=%{_sysconfdir}/X11/xorg.conf.d

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog GPL README
%{_sysconfdir}/X11/xorg.conf.d/50-wacom.conf
%{_bindir}/xsetwacom
%{_bindir}/isdv4-serial-debugger
%{driverdir}/wacom_drv.so
%exclude  %{driverdir}/wacom_drv.la
%{_mandir}/man4/wacom.4.*
%{_mandir}/man1/xsetwacom.1.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/xorg/Xwacom.h
%{_includedir}/xorg/wacom-properties.h
%{_includedir}/xorg/wacom-util.h
%{_includedir}/xorg/isdv4.h
%{_libdir}/pkgconfig/xorg-wacom.pc

%changelog
* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17.0-1m)
- udpate 0.17.0

* Sun Jan  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.0-1m)
- udpate 0.12.0

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.10-3m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.10-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-1m)
- update to 0.10.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.7-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-1m)
- update to 0.10.7

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4

* Thu Dec 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Wed Dec  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-2m)
- obsoletes linuxwacom

* Wed Nov  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- initial build
