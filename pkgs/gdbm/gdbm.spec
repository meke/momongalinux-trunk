%global momorel 17

Summary: A GNU set of database routines which use extensible hashing
Name: gdbm

Version: 1.8.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.gnu.org/directory/gdbm.html

Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0

Patch0: gdbm-1.8.0-jbj.patch
Patch1: gdbm-1.8.3-fhs.patch
Patch2: gdbm-1.8.3-cflags.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Gdbm is a GNU database indexing library, including routineswhich use 
extensible hashing.  Gdbm works in a similar way to standard UNIX dbm
routines.  Gdbm is useful for developers who write C applications and
need access to a simple and efficient database or who are building C
applications which will use such a database.

If you're a C developer and your programs need access to simple
database routines, you should install gdbm.  You'll also need to
install gdbm-devel.

%package devel
Summary: Development libraries and header files for the gdbm library
Group: Development/Libraries
Requires(post): info
Requires(preun): info
Requires: %{name} = %{version}-%{release}

%description devel
Gdbm-devel contains the development libraries and header files for
gdbm, the GNU database system.  These libraries and header files are
necessary if you plan to do development using the gdbm database.

Install gdbm-devel if you are developing C programs which will use the
gdbm database library.  You'll also need to install the gdbm package.
#'

%prep
%setup -q
%patch0 -p 1 -b .jbj
%patch1 -p 1 -b .fhs
%patch2 -p 1 -b .cflags

%build
# refresh config.sub, the original one does not recognize "redhat"
# as vendorname:
for f in /usr/share/automake-1.1?/config.sub; do
  :
done
cp -p $f .
libtoolize --force --copy
aclocal
autoconf
%configure
make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

# XXX --includedir dinna work
make install install-compat INSTALL_ROOT=%{buildroot} \
  includedir=%{_includedir}/gdbm LIBTOOL="/usr/bin/libtool --mode=install"

%__ln_s gdbm/gdbm.h %{buildroot}%{_includedir}/gdbm.h

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/gdbm.info %{_infodir}/dir

%postun -p /sbin/ldconfig

%preun devel
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/gdbm.info %{_infodir}/dir
fi

%files
%defattr(644,root,root,755)
%doc COPYING ChangeLog NEWS README
%defattr(-,root,root)
%{_libdir}/libgdbm.so.*
%{_libdir}/libgdbm_compat.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/libgdbm.so
%{_libdir}/libgdbm.a
%{_libdir}/libgdbm_compat.so
%{_libdir}/libgdbm_compat.a
%{_prefix}/include/*
%{_infodir}/*.info*
%{_mandir}/man3/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.3-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.3-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.3-15m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.3-14m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.3-11m)
- support libtool-2.2.x

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-10m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-9m)
- update Patch1 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.3-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.3-8m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.3-7m)
- delete libtool library

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.3-6m)
- revised docdir permission

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (1.8.3-5m)
- enable x86_64.

* Mon Dec 21 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.8.3-4m)
- 1.8.3 again
- add libgdbm_compat.* to %%files

* Wed Dec 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.0-20m)
- modify include 

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.0-19m)
- version down

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (1.8.3-3m)
- pretty spec file.

* Wed Aug 27 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.3-3m)
- import patch from rawhide

* Sun Jul 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.8.3-2m)
- revise spec

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.8.3-1m)
  update to 1.8.3

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.8.0-18k)
- /sbin/install-info -> info in PreReq.

* Thu Jan 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.8.0-16k)
- autoconf 1.5

* Thu Oct 18 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.0-14k)
- sync with Jirai

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add url tag
- fix ChangeLog to changelog

* Fri Sep 21 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.0-13k)
- fix libtool (add prefix to make)

* Mon Aug 27 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8.0-11k)
- add %undefine __libtoolize
- add %doc

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.8.0-3).

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Mon Jan 17 2000 Takaaki Tabuchi <tab@kondara.org>
- be able to rebuild non-root user 

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Aug 10 1999 Jeff Johnson <jbj@redhat.com>
- make sure created database header is initialized (#4457).

* Tue Jun  1 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.8.0.
- repackage to include /usr/include/gdbm/*dbm.h compatibility includes.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 19)

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build against glibc 2.1

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- gdbm-devel moved to Development/Libraries

* Wed Apr 08 1998 Cristian Gafton <gafton@redhat.com>
- buildroot and built for Manhattan

* Tue Oct 14 1997 Donnie Barnes <djb@redhat.com>
- spec file cleanups

* Thu Jun 12 1997 Erik Troan <ewt@redhat.com>
- built against glibc
