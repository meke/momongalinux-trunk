%global momorel 2

%global with_audacious 0
%global with_mpd 0
%global with_rss 1
%global with_wlan 1
%global with_tolua 1
%global with_weather 1
#needs more logic to determine??
%global with_nvidia 0

Name:           conky 
Version:        1.9.0
Release:        %{momorel}m%{?dist}
Summary:        A system monitor for X 

Group:          User Interface/X
License:        GPLv3
URL:            http://conky.sf.net/
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  expat-devel
BuildRequires:  fontconfig-devel
BuildRequires:  freetype-devel
BuildRequires:  glib2-devel
BuildRequires:  libX11-devel
BuildRequires:  libXau-devel
BuildRequires:  libXdamage-devel
BuildRequires:  libXdmcp-devel
BuildRequires:  libXext-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libXft-devel
BuildRequires:  libXrender-devel
BuildRequires:  libxcb-devel
BuildRequires:  zlib-devel
%if %{with_audacious}
BuildRequires:  audacious-devel
BuildRequires:  dbus-glib-devel
%endif
%if %{with_rss}
BuildRequires:  curl-devel
%endif
%if %{with_wlan}
BuildRequires:  wireless-tools-devel
%endif
%if %{with_tolua}
BuildRequires:  tolua++
%endif

%description
A system monitor for X originally based on the torsmo code. but more kickass. 
It just keeps on given'er. Yeah.

%prep
%setup -q

for i in AUTHORS ChangeLog; do
    iconv -f iso8859-1 -t utf8 -o ${i}{_,} && touch -r ${i}{,_} && mv -f ${i}{_,}
done

%build
%configure \
    --with-gnu-ld \
    --disable-static \
%if %{with_weather}
    --enable-weather-metar \
    --enable-weather-xoap \
%endif
%if %{with_audacious}
    --enable-audacious=yes \
%endif
%if ! %{with_mpd}
    --disable-mpd \
%endif
%if %{with_rss}
    --enable-rss \
%endif
%if %{with_wlan}
    --enable-wlan \
%endif
%if %{with_tolua}
    --enable-lua-cairo \
%endif
%if %{with_nvidia}
    --enable-nvidia \
%endif
    ;

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING TODO README NEWS extras/* doc/docs.html
%dir %{_sysconfdir}/%{name}
%config %{_sysconfdir}/%{name}/%{name}.conf
%config %{_sysconfdir}/%{name}/%{name}_no_x11.conf
%{_bindir}/%{name}
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/libcairo.so*
%{_mandir}/man1/%{name}.1*

%changelog
* Tue Aug 28 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.9.0-2m)
- added specfile options

* Sun Aug 26 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.9.0-1m)
- new upstream release

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0-5m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1.-1m)
- update to 1.6.1

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1
- NoSource

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.9-3m)
- rebuild against gcc43

* Mon Jan  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9-2m)
- revise and sort BPRs

* Fri Jan  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.9-1m)
- import to Momonga from fedora

* Thu Dec 27 2007 Patrice Dumas <pertusus@free.fr> - 1.4.9-1.1
- Enable support for Audacious 1.4.0

* Tue Nov 27 2007 Miroslav Lichvar <mlichvar@redhat.com> - 1.4.9-1
- Update to 1.4.9

* Sun Oct 21 2007 Miroslav Lichvar <mlichvar@redhat.com> - 1.4.8-1
- Update to 1.4.8
- Enable mpd, rss and wireless support
- Update license tag

* Wed Apr 18 2007 Michael Rice <errr[AT]errr-online.com> - 1.4.5-4
- Rebuild to match audacious lib in fc6 bug: 236989

* Mon Apr 09 2007 Michael Rice <errr[AT]errr-online.com> - 1.4.5-3
- Rebuild for devel

* Thu Dec 14 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.5-2
- Ship NEWS
- Add patch for license of timed_thread and NEWS

* Tue Dec 12 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.5-1
- version bump
- change group
 
* Wed Dec 06 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.4-3
- rebuild for new audacious lib version

* Thu Nov 30 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.4-2
- Move nano and vim files into docs
- remove unneeded BR's

* Tue Nov 21 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.4-1
- Version bump
- Add vim and nano syntax files to package

* Thu Oct 05 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.3-1
- Version bump
- Remove Install file from docs

* Mon Oct 02 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.2-4
- moved to configure macro from ./configure
- clean up changelog and make more informative entrys
- Fixed sumary in spec file
- remove NEWS file since it was empty
- remove xmms support due to possible security issue
- remove bmp support due to possible security issue
- add missing BR for libXext-devel and remove unneeded libX11-devel  

* Thu Sep 28 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.2-3
- use the GPL as licence since the whole package is GPL

* Thu Sep 28 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.2-2
- remove unneeded deps

* Tue Sep 26 2006 Michael Rice <errr[AT]errr-online.com> - 1.4.2-1
- Initial RPM release
