%global momorel 14

Summary: Graphical frontend to SANE
Name: sane-frontends
Version: 1.0.14
Release: %{momorel}m%{?dist}
License: GPL
URL: http://www.sane-project.org/
Group: Applications/System
Source0: https://alioth.debian.org/frs/download.php/1140/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-badcode.patch
Patch1: %{name}-%{version}-sane-back-1.0.21.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: sane-backends
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gimp-devel >= 2.0
BuildRequires: gtk2-devel >= 2.0.0
BuildRequires: libX11-devel
BuildRequires: libXcursor-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXft-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libieee1284-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libusb-devel
BuildRequires: pkgconfig >= 0.7
BuildRequires: sane-backends-devel
Obsoletes: sane
Provides: sane

%description
This is the xscanimage program, used to scan images using SANE, either
standalone or as a gimp plugin. Also includes xcam.

%prep
%setup -q

%patch0 -p1 -b .badcode
%patch1 -p1 -b .sane-backends-1.0.21

%build
%configure --with-gnu-ld
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# Not xscanimage; use xsane instead.
rm -f %{buildroot}%{_bindir}/xscanimage %{buildroot}%{_mandir}/man1/xscanimage*

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING Changelog NEWS PROBLEMS README
%{_bindir}/scanadf
%{_bindir}/xcam
%{_mandir}/man1/scanadf.1*
%{_mandir}/man1/xcam.1*
%{_datadir}/sane/sane-style.rc

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.14-14m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.14-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.14-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.14-11m)
- full rebuild for mo7 release

* Sat Jun 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.14-10m)
- add patch1 (sane-backends-1.0.21)

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.14-9m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.14-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.14-7m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.14-6m)
- rebuild against rpm-4.6

* Sun Jun 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-5m)
- import badcode.patch from Fedora
- remove obsoleted patch and clean up spec file
- sort %%files
- change URL
- add %%doc

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.14-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.14-3m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-2m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Sun Jun  4 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0.14-1m)
- update to 1.0.14
- change source URI

* Thu Oct 28 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.13-1m)
- ver up

* Wed Jun  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.12-1m)
- verup

* Tue Jan 27 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.11-7m)
- fix for gimp-2.0

* Fri Jan 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.11-6m)
- rebuild against gimp-2.0

* Fri Oct 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.11-5m)
- rebuild against gimp-1.3.21

* Wed Sep 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.11-4m)
- rebuild against gimp-1.3.20

* Fri Jun 27 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.11-3m)
- rebuild against for gimp-1.3.16

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.11-2m)
- rebuild against for gimp-1.3.15

* Sat May  3 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.0.11-1m)
- update to 1.0.11
- add support for gimp-1.3/gtk2

* Mon Feb 10 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.0.10-1m)
- update to 1.0.10
- remove sane-backends requirement
- fix BuildPrereq's

* Wed Dec 11 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.9-1m)
- update to 1.0.9

* Wed Jul 24 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Mon Feb 20 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0.7-2k)
- update to 1.0.7

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.5-4k)
- rebuild against for gimp-1.2.3

* Fri Oct 25 2001 Toru Hoshina <t@kondara.org>
- (1.0.5-2k)
- merge from rawhide. [1.0.5-2]
- sane obsoleted.

* Sun Jul  1 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-1
- 1.0.5.
- Change Copyright: to License:.

* Thu Jun  7 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-0.20010605
- CVS snapshot 2001-06-05.
- Don't install xscanimage plug-in symlinks.  The old sane package never
  used to do this, and it looks confusing in gimp if you also have
  xsane-gimp (which is better) installed.  xscanimage works stand-alone
  anyhow.

* Sun Jun  3 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-0.20010603.1000
- CVS snapshot 2001-06-03 10:00.

* Sat Jun  2 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-0.20010530
- Built for Red Hat Linux.
- CVS snapshot 2001-05-30.

* Mon Jan 08 2001 Francis Galiegue <fg@mandrakesoft.com> 1.0.4-2mdk

- Summary now capitalised
- BuildRequires: sane (for sane-config)
