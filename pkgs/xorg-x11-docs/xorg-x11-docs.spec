%global momorel 1
%global tarname xorg-docs
Summary:   X.Org X11 documentation
Name:      xorg-x11-docs
Version:   1.7
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/doc/%{tarname}-%{version}.tar.bz2 
NoSource: 0
License:   MIT/X
Group:     Documentation
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: xorg-x11-sgml-doctools >= 1.6
Obsoletes: xorg-docs

%description
X.Org X11 documentation

%prep
%setup -q -n %{tarname}-%{version}

%build
%configure
make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc README
%{_datadir}/doc/xorg-docs
%{_mandir}/man7/*

%changelog
* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7-1m)
- update to 1.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-2m)
- rebuild for new GCC 4.6

* Sun Dec 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.99.901-2m)
- rebuild for new GCC 4.5

* Fri Nov 12 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.99.901-1m)
- update to 1.5.99.901 (RC1)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4-5m)
- build fix (No %%make)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-2m)
- %%NoSource -> NoSource

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4-1m)
- update to 1.4

* Thu Nov  9 2006 Futoshi <futoshi@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Sat Aug 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-3m)
- to main

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-2m)
- rename package

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- initial build
