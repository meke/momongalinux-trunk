%global momorel 8
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global origname yaml-mode

Summary: A Simple mode for editing files in the YAML data serialization format
Name: emacs-%{origname}
Version: 0.0.7
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source: %{origname}-%{version}.tar.gz
URL: http://github.com/yoshiki/yaml-mode/tree/master
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: yaml-mode-emacs
Obsoletes: yaml-mode-xemacs

Obsoletes: elisp-yaml-mode
Provides: elisp-yaml-mode

%description
A Simple mode for editing files in the YAML data serialization format.

%prep
%setup -q -n %{origname}-%{version}

%build
emacs -q -no-site-file -batch -f batch-byte-compile %{origname}.el
%{__mv} %{origname}.elc %{origname}.elc.emacs

%install
rm -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{e_sitedir}/%{origname}
%{__install} -m 644 %{origname}.el %{buildroot}%{e_sitedir}/%{origname}
%{__install} -m 644 %{origname}.elc.emacs %{buildroot}%{e_sitedir}/%{origname}/%{origname}.elc

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{e_sitedir}/%{origname}

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-8m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-7m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.7-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.7-4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.7-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.7-1m)
- update to 0.0.7

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-2m)
- merge yaml-mode-emacs to elisp-yaml-mode
- kill yaml-mode-xemacs

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-1m)
- update to 0.0.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4-7m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4-6m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-5m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-4m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-3m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-2m)
- rebuild against emacs-23.0.93

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-1m)
- update to 0.0.4
- License: GPLv2+

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-10m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-9m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-8m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-7m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-6m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.3-5m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-4m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.3-3m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.3-2m)
- rebuild against emacs-22.0.96

* Sun Mar 11 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-1m)
- update to 0.0.3

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-2m)
- rebuild against emacs-22.0.90

* Mon Oct  2 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.2-1m)
- initial package
