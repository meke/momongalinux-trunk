%global         momorel 10

Name:           perl-Number-Compare
Version:        0.03
Release:        %{momorel}m%{?dist}
Summary:        Numeric comparisons
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Number-Compare/
Source0:        http://www.cpan.org/authors/id/R/RC/RCLAMP/Number-Compare-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Number::Compare compiles a simple comparison to an anonymous subroutine,
which you can call with a value to be tested again.

%prep
%setup -q -n Number-Compare-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Number/Compare.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-2m)
- rebuild against perl-5.14.2

* Thu Sep 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- update to 0.03

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-23m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-22m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.01-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.01-20m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-19m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.01-18m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-17m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-16m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.01-15m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.01-14m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.01-13m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.01-12m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.01-11m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.01-10m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.01-9m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.01-8m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.01-7m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.01-6m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.01-5m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.01-4m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.01-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Tue Dec 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.01-2m)
- 'BuildArch: noarch'

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.01-1m)
- spec file was autogenerated
