%global         momorel 4

Name:           perl-Catalyst-Devel
Version:        1.39
Release:        %{momorel}m%{?dist}
Summary:        Catalyst Development Tools
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Devel/
Source0:        http://www.cpan.org/authors/id/I/IL/ILMARI/Catalyst-Devel-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Catalyst-Runtime >= 5.80015
BuildRequires:  perl-Catalyst-Action-RenderView >= 0.10
BuildRequires:  perl-Catalyst-Plugin-ConfigLoader >= 0.30
BuildRequires:  perl-Catalyst-Plugin-Static-Simple >= 0.28
BuildRequires:  perl-Config-General >= 2.42
BuildRequires:  perl-ExtUtils-MakeMaker >= 6.11
BuildRequires:  perl-File-ChangeNotify >= 0.07
BuildRequires:  perl-File-Copy-Recursive
BuildRequires:  perl-File-ShareDir
BuildRequires:  perl-Module-Install >= 0.91
BuildRequires:  perl-Moose
BuildRequires:  perl-MooseX-Daemonize
BuildRequires:  perl-MooseX-Emulate-Class-Accessor-Fast
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-namespace-clean
BuildRequires:  perl-Path-Class >= 0.09
BuildRequires:  perl-Starman
BuildRequires:  perl-Template-Toolkit >= 2.14
BuildRequires:  perl-Test-Simple >= 0.94
Requires:       perl-Catalyst-Runtime >= 5.80015
Requires:       perl-Catalyst-Action-RenderView >= 0.10
Requires:       perl-Catalyst-Plugin-ConfigLoader >= 0.30
Requires:       perl-Catalyst-Plugin-Static-Simple >= 0.28
Requires:       perl-Config-General >= 2.42
Requires:       perl-ExtUtils-MakeMaker >= 6.11
Requires:       perl-File-ChangeNotify >= 0.07
Requires:       perl-File-Copy-Recursive
Requires:       perl-File-ShareDir
Requires:       perl-Module-Install >= 0.91
Requires:       perl-Moose
Requires:       perl-MooseX-Daemonize
Requires:       perl-MooseX-Emulate-Class-Accessor-Fast
Requires:       perl-namespace-autoclean
Requires:       perl-namespace-clean
Requires:       perl-Path-Class >= 0.09
Requires:       perl-Starman
Requires:       perl-Template-Toolkit >= 2.14
Requires:       perl-Test-Simple >= 0.94
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Catalyst-Devel distribution includes a variety of modules useful for
the development of Catalyst applications, but not required to run them.
This is intended to make it easier to deploy Catalyst apps. The runtime
parts of Catalyst are now known as Catalyst-Runtime.

%prep
%setup -q -n Catalyst-Devel-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Catalyst/*
%{perl_vendorlib}/auto/share/dist/Catalyst-Devel
%{perl_vendorlib}/Module/Install/Catalyst.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-2m)
- rebuild against perl-5.18.1

* Sat Jun 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-1m)
- update to 1.39

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-2m)
- rebuild against perl-5.18.0

* Fri Apr 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37
- rebuild against perl-5.16.0

* Tue Oct 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-2m)
- rebuild against perl-5.14.2

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33-2m)
- rebuild for new GCC 4.6

* Fri Mar 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Tue Mar 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1 30

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.28-2m)
- full rebuild for mo7 release

* Wed Jun 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-2m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-5m)
- rebuild against perl-5.12.0

* Sun Mar 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-4m)
- Catalyst::Devel does not own %%{perl_vendorlib}/Module/Install

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.21-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Tue Jun 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.18-2m)
- BuildRequires: perl-Linux-Inotify2

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18
- modify BuildRequires: and Requires:

* Sun May 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-2m)
- rebuild against rpm-4.6

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06
- add BuildRequires: and Requires: perl-parent

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.03-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03-2m)
- %%NoSource -> NoSource

* Tue Aug 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.02-2m)
- use vendor

* Tue Dec 13 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.01-2m)
- delete dupclicate directory

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.01-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
