%global momorel 10
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)

# From previous experience it seemed that all of the following
# were necessary to prevent stripping of bytecode executables
%global __os_install_post /usr/lib/rpm/momonga/brp-compress %{nil}
%global _enable_debug_package 0
%global debug_package %{nil}

Name:           ocaml-mlgmpidl
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        OCaml interface to GMP and MPFR libraries
Group:          Development/Libraries
License:        LGPLv2
URL:            http://www.inrialpes.fr/pop-art/people/bjeannet/mlxxxidl-forge/mlgmpidl/index.html
Source0:        http://gforge.inria.fr/frs/download.php/20228/mlgmpidl-%{version}.tgz
NoSource:       0
Source1:        mlgmpidl_test.ml
Source2:        mlgmpidl_test_result
Source3:        mlgmpidl-META
Patch0:         mlgmpidl-1.1-Makefile.patch
Patch10:        ocaml-mlgmpidl-mpfr3.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ocaml >= %{ocamlver}, ocaml-ocamldoc, ocaml-camlidl-devel, prelink
BuildRequires:  gmp-devel >= 5.0.0, mpfr-devel >= 3.0.0 
# BuildRequires for documentation build
BuildRequires:  tetex-latex, tetex-common, ghostscript

%description
MLGMPIDL is an OCaml interface to the GMP and MPFR rational and real
number math libraries. Although there is another such interface, this
one is different in that it provides a more imperative (rather than
functional) interface to conserve memory and that this one uses
CAMLIDL to take care of the C/OCaml interface in a convenient and
modular way.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%package        doc
Summary:        Documentation files for %{name}
Group:          Documentation

%description    doc
The %{name}-doc package contains documentation for using %{name}.

%prep
%setup -q -n mlgmpidl
cp %{SOURCE1} %{SOURCE2} .
cp %{SOURCE3} ./META

# Patch makefile to take custom locations for libraries and installing
%patch0

%patch10 -p1 -b .mpfr3

%build
mv Makefile.config.model Makefile.config
make rebuild

%global ocaml_lib_dir %{_libdir}/ocaml
%global my_ocaml_lib_dir %{ocaml_lib_dir}/mlgmpidl

make PREFIX=%{_prefix} GMP_LIBDIR=%{_libdir} MPFR_LIBDIR=%{_libdir} \
     CAML_LIBDIR=%{ocaml_lib_dir} CAMLIDL_LIBDIR=%{ocaml_lib_dir} \
     MLGMPIDL_LIBDIR=%{_libdir} all gmptop
make mlgmpidl.dvi
make html
dvipdf mlgmpidl.dvi

# Put the C library location in the META file
sed -e 's|LIBDIR|%{_libdir}|' META > META.tmp && mv META.tmp META

%check
ocamlc -ccopt -L. -custom -dllib %{_libdir}/libgmp.so gmp.cma bigarray.cma mlgmpidl_test.ml
./a.out > mlgmpidl_test_myresult
diff mlgmpidl_test_myresult mlgmpidl_test_result

%install
rm -rf %{buildroot}

make INSTALL_INCLUDEDIR=%{buildroot}/%{_includedir} \
     INSTALL_OCAML_LIBDIR=%{buildroot}/%{my_ocaml_lib_dir} \
     INSTALL_BINDIR=%{buildroot}/%{_bindir} install

cp META %{buildroot}/%{my_ocaml_lib_dir}

# Install Documentation
%global doc_dir %{_docdir}/%{name}
mkdir -p %{buildroot}%{doc_dir}
cp -p *.pdf %{buildroot}%{doc_dir}
cp -pr html %{buildroot}%{doc_dir}

# Make sure that prelink does not foul up our bytecode executables by
# stripping them with a cron job. This is done in install to ensure
# that exactly the files that are eventually installed are in the
# list, not all of the files in the bin directory of the build

%global prelinkfilename %{name}-prelink.conf
cd %{buildroot}%{_bindir}
for f in *; do
file $f | grep "not stripped" | sed -e 's/:.*//' -e 's!^!-b %{_bindir}/!' >> %{prelinkfilename}
done

%global prelinkconfdir %{_sysconfdir}/prelink.conf.d
mkdir -p %{buildroot}%{prelinkconfdir}
mv %{prelinkfilename} %{buildroot}%{prelinkconfdir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{my_ocaml_lib_dir}
%if %opt
%exclude %{my_ocaml_lib_dir}/*.cmx*
%endif
%exclude %{my_ocaml_lib_dir}/*.idl
%exclude %{my_ocaml_lib_dir}/*.mli
%exclude %{my_ocaml_lib_dir}/*.a
%{_bindir}/gmptop
%doc COPYING README
%config %{prelinkconfdir}/%{prelinkfilename}

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%if %opt
%{my_ocaml_lib_dir}/*.cmx*
%endif
%{my_ocaml_lib_dir}/*.idl
%{my_ocaml_lib_dir}/*.mli
%{my_ocaml_lib_dir}/*.a

%files doc
%defattr(-,root,root,-)
%{_docdir}/%{name}

%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-10m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-8m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-6m)
- rebuild against gmp-5.0.1 and mpfr-3.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-5m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against ocaml-3.11.2

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- modify __os_install_post for new momonga-rpmmacros

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- import from Fedora 11

* Thu Apr 02 2009 Alan Dunn <amdunn@gmail.com> 1.1-1
- New upstream version incorporates functional interface to Mpfr.
* Sat Mar 28 2009 Alan Dunn <amdunn@gmail.com> 1.0-1
- Initial Fedora RPM version.
