%global momorel 2

Summary: The Light Display Manager
Name: lightdm
Version: 1.0.5
Release: %{momorel}m%{?dist}
License: GPLv3
Group: User Interface/X
Source0: http://people.ubuntu.com/~robert-ancell/%{name}/releases/%{name}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: gtk3-devel
BuildRequires: libX11-devel
BuildRequires: libXdmcp-devel
BuildRequires: libxcb-devel
BuildRequires: libxklavier-devel
# qt support is not built
# BuildRequires: qt-devel

%description
LightDM is a cross-desktop display manager that aims is to be the standard display manager 
for the X.org X server. The motivation for this project is there have been many new display 
managers written since XDM (often based on the XDM source). The main difference between these 
projects is in the GUIs (e.g. different toolkits) and performance - this could be better 
accomplished with a common display manager that allows these differences. 

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
	--disable-static \
	--enable-silent-rules \
	--enable-introspection=yes \
	--enable-liblightdm-gobject \
	--disable-liblightdm-qt \
	--disable-qt-greeter \
	--enable-gtk-greeter \
	--enable-gtk-doc \
	LIBS="-lX11"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/apparmor.d/lightdm-guest-session
%{_sysconfdir}/dbus-1/system.d/org.freedesktop.DisplayManager.conf
%{_sysconfdir}/init/lightdm.conf
%{_sysconfdir}/%{name}
%{_sbindir}/lightdm
%{_sbindir}/lightdm-gtk-greeter
%{_bindir}/dm-tool
%{_libexecdir}/lightdm-guest-session-wrapper
%{_libexecdir}/lightdm-set-defaults
%dir %{_libexecdir}/%{name}
%{_libexecdir}/lightdm/gdmflexiserver
%{_libdir}/girepository-1.0/LightDM-1.typelib
%{_libdir}/liblightdm-gobject-1.so.*
%exclude %{_libdir}/liblightdm-gobject-1.la
%{_datadir}/lightdm-gtk-greeter
%{_datadir}/xgreeters/lightdm-gtk-greeter.desktop
%{_mandir}/man1/lightdm.1.*

%files devel
%defattr(-,root,root)
%{_includedir}/lightdm-gobject-1
%{_libdir}/liblightdm-gobject-1.so
%{_libdir}/pkgconfig/liblightdm-gobject-1.pc
%{_datadir}/gir-1.0/LightDM-1.gir
%{_datadir}/vala/vapi/liblightdm-gobject-1.vapi
%doc %{_datadir}/gtk-doc/html/lightdm-gobject-1

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-2m)
- rebuild for glib 2.33.2

* Wed Nov  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-1m)
- initial build
