%global momorel 4

Name:		kitutuki
Version:	0.9.9c
Release:	%{momorel}m%{?dist}
Summary:	Shell script language

Group:		System Environment/Shells
License:	GPL+
URL:		http://ab25cq.web.fc2.com/
Source0:	http://ab25cq.web.fc2.com/%{name}-%{version}.tgz
NoSource:	0
BuildRequires:	cmigemo-devel
BuildRequires:	ncurses-devel
BuildRequires:	oniguruma-devel
BuildRequires:	readline-devel

%description
Kitutuki is a shell script language.

%package	devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description	devel
%{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

# Again fix timestamp
touch timestamp
find . -type f -print0 | xargs -0 touch -r timestamp
sleep 3

# Makefile
sed -i.strip -e '/install/s| -s | |' Makefile.in
sed -i.stamp -e 's|\([ \t][ \t]*install \)|\1 -p |' Makefile.in

# configure
sed -i.cflags -e '/CFLAGS=/s|-fPIC|-fPIC %{optflags}|' configure

# Miscs
iconv -f EUC-JP -t UTF-8 README.ja.txt > README.ja.txt.utf8
touch -r README.ja.txt{,.utf8}
mv -f README.ja.txt{.utf8,}

%build
%configure \
	--sysconfdir=%{_libdir}/%{name} \
	--includedir=%{_includedir}/%{name} \
	--with-migemo \
	--with-system-migemodir=%{_datadir}/cmigemo

make %{?_smp_mflags} \
	docdir=%{_defaultdocdir}/%{name}-%{version}/


%install
rm -rf %{buildroot}
# make install DESTDIR=%%{buildroot}
# Above does not work...
rm -rf ./Trash
%makeinstall \
	sysconfdir=%{buildroot}%{_libdir}/%{name}/ \
	includedir=%{buildroot}%{_includedir}/%{name}/ \
	docdir=$(pwd)/Trash/

# Move kitutuki.ksh to %%{_sysconfdir}/%{name}
mkdir -p %{buildroot}%{_sysconfdir}/%{name}
mv %{buildroot}%{_libdir}/%{name}/kitutuki.ksh \
	%{buildroot}%{_sysconfdir}/%{name}
ln -sf ../../../%{_sysconfdir}/%{name}/kitutuki.ksh \
	%{buildroot}%{_libdir}/%{name}/

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc	AUTHORS
%doc	GPL
%lang(ja)	%doc	README.ja.txt
%doc	usage.en.txt
%lang(ja)	%doc	usage.ja.txt

%dir %{_sysconfdir}/%{name}
# In case that kitutuki.ksh changes much as this is quite a
# new package, rather mark this as no-noreplace
%config	%{_sysconfdir}/%{name}/kitutuki.ksh
%{_bindir}/%{name}
%{_libdir}/libkitutuki.so.*
%{_libdir}/%{name}/

%files	devel
%defattr(-,root,root,-)
%{_libdir}/lib%{name}.so
%{_includedir}/%{name}/


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9c-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9c-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9c-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9c-1m)
- import from Fedora 13

* Thu Mar 11 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.9c-1
- 0.9.9c

* Tue Mar  9 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.9b-1
- 0.9.9b

* Sat Mar  6 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.9-1
- 0.9.9

* Sat Mar  6 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.8-1
- 0.9.8

* Tue Mar  2 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.7-1
- 0.9.7
- All patches went upstream

* Thu Feb 25 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.6-1
- Update to 0.9.6

* Tue Feb 23 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.5-2.respin1
- 0.9.5 respun

* Tue Feb 23 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.5-1
- Update to 0.9.5

* Sat Feb 20 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.3-1
- Update to 0.9.3

* Wed Feb 17 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.1-2
- Add Japanese summary / description
- Add more comments about kitutuki.ksh
- Fix kitutuki.ksh for kitutuki_help

* Tue Feb 16 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.1-1
- Initial packaging
