%global         momorel 12

Name:           perl-Net-Daemon
Version:        0.48
Release:        %{momorel}m%{?dist}
Summary:        Perl extension for portable daemons
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-Daemon/
Source0:        http://www.cpan.org/authors/id/M/MN/MNOONING/Net-Daemon-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Net::Daemon is an abstract base class for implementing portable server
applications in a very simple way. The module is designed for Perl 5.005
and threads, but can work with fork() and Perl 5.004.

%prep
%setup -q -n Net-Daemon-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README regexp-threads
%{perl_vendorlib}/Net/Daemon.pm
%{perl_vendorlib}/Net/Daemon
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-12m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-11m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-10m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-9m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-8m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-7m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-6m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-5m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sat Apr 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-11m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-10m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-9m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-8m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-7m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.43-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-5m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.43-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.43-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-2m)
- %%NoSource -> NoSource

* Tue Jul 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.39-2m)
- use vendor

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.39-1m)
- update to 0.39

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.38-6m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.38-5m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.38-4m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.38-3m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.38-2m)
- revised spec for rpm 4.2.

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.38-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.37-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.37-3m)
- rebuild against perl-5.8.1

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.37-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.37-1m)
- rebuild against perl-5.8.0

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.36-4m)
- fix %files (Changes -> ChangeLog)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.36-3m)
- remove BuildRequires: gcc2.95.3

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (0.36-2k)
- create
