%global momorel 37

Summary: A software wavetable MIDI synthesizer
Name: TiMidity++
Version: 2.13.2
Release: %{momorel}m%{?dist}
License: GPLv2+
URL:   http://timidity.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/timidity/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: ftp://ftp.netscape.com/pub/sdk/plugin/unix/unix-sdk-3.0b5.tar.Z
Source2: ump_with_volume.tar.bz2
Source3: timdiffs.tar.bz2
Source4: TiMidity++.desktop
Patch0: %{name}-%{version}-freepats-cfg.patch
# Patch3-7 were imported from Fedora
Patch3: %{name}-2.13.0-detect.patch
Patch5: %{name}-2.13.0-64bit.patch
Patch6: %{name}-2.13.0-warnings.patch
# Patch8-9 were imported from cooker
Patch8: %{name}-%{version}+flac-1.1.3-partial.patch
Patch9: timidity-2.13.2-speex-header-path.patch
# patches for ump
Patch10: http://www.onicos.com/staff/iz/timidity/dist/ump-patch-1.gz
Patch11: http://www.onicos.com/staff/iz/timidity/dist/ump-patch-2.gz
Patch12: http://www.onicos.com/staff/iz/timidity/dist/ump-patch-3.gz
Patch14: http://www.onicos.com/staff/iz/timidity/dist/ump-patch-4.gz
Patch20: timidity++-2.13.2-gcc4.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: openmotif, alsa-lib >= 0.5.6, tcl >= 8.4.5-1m, tk >= 8.4.5-1m, gtk2
Requires: freepats-cfg >= 20060219-2m
BuildRequires: alsa-lib-devel, esound-devel
BuildRequires: audiofile-devel >= 0.2.0, libpng-devel >= 1.2.2
BuildRequires: desktop-file-utils
BuildRequires: tcl-devel >= 8.5.2, tk-devel >= 8.5.2
BuildRequires: ncurses-devel, slang-devel >= 2.0.5-1m, gtk2-devel
BuildRequires: libogg-devel, flac-devel >= 1.1.4, speex-devel >= 1.2
BuildRequires: openmotif-devel >= 2.3.0
BuildRequires: emacs >= %{emacsver}
BuildRequires: autoconf

%description
TiMidity++ is a software synthesizer. It can play MIDI files by converting them into PCM waveform data; give it a MIDI data along with digital instrument data files, then it synthesizes them in real-time, and plays. It can not only play sounds, but also can save the generated waveforms into hard disks as various audio file formats.

TiMidity++ is a free software, distributed under the terms of GNU general public license.

%package ump
Summary: MIDI plugins for mozilla
Group: Applications/Multimedia
BuildRequires: openmotif-devel
Requires: %{name} = %{version}-%{release}
Obsoletes: TiMidity++UMP

%description ump
This is the MIDI plugins for mozilla.

%package -n emacs-%{name}
Summary: TiMidity interface for Emacs
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{_emacs_version}
BuildArch: noarch
Obsoletes: elisp-TiMidity++

%description -n emacs-%{name}
This package contains a major mode for TiMidity.

%prep
%setup -q

# needs freepats-cfg
%patch0 -p1 -b .freepats

# Autodetect whether we should use aRts, esd, or neither
%patch3 -p1 -b .detect
# fix for x86_64 and s390x
%patch5 -p1 -b .64bit
%patch6 -p1 -b .warnings

# from cooker
%patch8 -p1 -b .flac-114
%patch9 -p1 -b .speex-12

mkdir ump
pushd ump > /dev/null
tar zxf %SOURCE1
tar zxf %SOURCE2
tar zxf %SOURCE3 plugin_c.c
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch14 -p1
cp PluginSDK30b5/include/*.h .
cp PluginSDK30b5/common/npunix.c .
popd > /dev/null

%patch20 -p1 -b .gcc4

# for Patch9
autoconf

%build
# build UMP
DEF_PKGDATADIR=%{_datadir}/timidity ./configure --prefix=%{_prefix} --enable-ump --enable-audio=oss --with-includes=../libunimod
make clean
CONFIG_HEADERS='' CONFIG_FILES=ump/Makefile DEF_PKGDATADIR=%{_datadir}/timidity /bin/sh ./config.status

aclocal-1.7 -I autoconf
make %{?_smp_mflags} ump -C ump

# build TiMidity again
make clean
CFLAGS="%{optflags}" \
%configure \
  --with-module-dir=%{_libdir}/timidity \
  --enable-audio=default,oss,alsa,esd,alsa,esd,vorbis,flac,speex \
  --enable-ncurses \
  --enable-slang \
  --enable-motif \
  --enable-tcltk \
  --enable-emacs \
  --enable-vt100 \
  --enable-xaw \
  --enable-xskin \
  --enable-gtk \
  --enable-network \
  --enable-spectrogram \
  --enable-spline=lagrange \
  --enable-wrd \
  --enable-alsaseq \
  --with-default-output=default

%make

%install
rm -rf --preserve-root %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/timidity
mkdir -p %{buildroot}%{_datadir}/X11/{ja/app-defaults,app-defaults}
mkdir -p %{buildroot}%{_mandir}/{man1,man5,ja/{man1,man5}}
mkdir -p %{buildroot}%{_libdir}/mozilla/plugins

%makeinstall libdir=%{buildroot}%{_datadir} pkglibdir=%{buildroot}%{_libdir}/timidity transform=s,^,,
make DESTDIR=%{buildroot} install.tk

install -m 0644 TiMidity.ad %{buildroot}%{_datadir}/X11/app-defaults/TiMidity
install -m 0644 TiMidity-uj.ad %{buildroot}%{_datadir}/X11/ja/app-defaults/TiMidity
install -m 755 ump/ump.so %{buildroot}%{_libdir}/mozilla/plugins/
install -m 644 doc/ja_JP.eucJP/timidity.1 %{buildroot}%{_mandir}/ja/man1/
install -m 644 doc/ja_JP.eucJP/timidity.cfg.5 %{buildroot}%{_mandir}/ja/man5/
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
        iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

sed -e "s,%{buildroot},,g" %{buildroot}%{_libdir}/timidity/tkmidity.tcl > tkmidity.tcl.tmp
install -m 644 tkmidity.tcl.tmp %{buildroot}%{_libdir}/timidity/tkmidity.tcl

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 interface/pixmaps/timidity.xpm %{buildroot}%{_datadir}/pixmaps/

mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category AudioVideo \
  --add-category Audio \
  --add-category Player \
  %{SOURCE4}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog* INSTALL* NEWS README* TODO
%{_bindir}/timidity
%{_libdir}/timidity
%{_mandir}/man1/timidity.1*
%{_mandir}/man5/timidity.cfg.5*
%{_mandir}/ja/man1/timidity.1*
%{_mandir}/ja/man5/timidity.cfg.5*
%{_datadir}/X11/app-defaults/TiMidity
%{_datadir}/X11/ja/app-defaults/TiMidity
%{_datadir}/applications/*.desktop
%{_datadir}/pixmaps/timidity.xpm

%files ump
%defattr(-,root,root)
%{_libdir}/mozilla/plugins/ump.so

%files -n emacs-%{name}
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/timidity.el*

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.2-37m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.2-36m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.2-35m)
- rename elisp- to emacs-

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.2-34m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13.2-33m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.2-32m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.2-31m)
- rebuild against emacs-23.2
- split out elisp-TiMidity++

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.2-30m)
- remove xemacs stuff

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.2-29m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.2-28m)
- rebuild against rpm-4.6

* Thu Jan  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.2-27m)
- update Patch3 from Rawhide (2.13.2-16)
-- this patch includes Patch7
- License: GPLv2+

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-26m)
- rebuild against tcl-8.5.2 and tk-8.5.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13.2-25m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.2-24m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13.2-23m)
- rebuild against libvorbis-1.2.0-1m

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.13.2-22m)
- convert ja.man(UTF-8)

* Fri Mar 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-21m)
- add freepats-cfg.patch

* Fri Feb 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-20m)
- import timidity-2.13.2-speex-header-path.patch from cooker
 +* Sun Feb 20 2005 Abel Cheung <deaddog@mandrake.org> 2.13.2-2mdk
 +- P1: Fix speex header detection

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-19m)
- rebuild against flac-1.1.4
- import TiMidity++-2.13.2+flac-1.1.3-partial.patch from cooker
 +* Wed Dec 13 2006 Per Karlsen <pkarlsen@mandriva.com> 2.13.2-17mdv2007.0
 ++ Revision: 96138
 +- Fix flac mess
 +- add new flac patch

* Tue Nov 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.2-18m)
- good-bye mozilla

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.13.2-17m)
- rebuild against openmotif-2.3.0-beta2

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.2-16m)
- remove category Application

* Fri Jul 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-15m)
- revise TiMidity++.desktop, Exec=timidity -ig -Os

* Sun May 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.13.2-14m)
- rebuild against slang-2.0.5

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.13.2-13m)
- revised installdir

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.2-12m)
- revise for xorg-7.0
- FIX ME! comment out xorg-x11-deprecated-libs-devel

* Fri Dec 23 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.2-11m)
- delete patch16, now automake work

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.2-10m)
- add gcc4 patch. import from Gentoo
- Patch20: timidity++-2.13.2-gcc4.patch

* Fri Sep 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.13.2-9m)
- add BuildPreReq: emacs, xemacs
- enable x86_64

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-8m)
- rebuild against flac-1.1.2

* Sun Apr  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-7m)
- change Source0 URI

* Thu Mar 24 2005 Toru Hoshina <t@momonga-linux.org>
- (2.13.2-6m)
- rebuild against mozilla-1.7.6
- install to {_libdir}/mozilla/plugins.

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.13.2-5m)
- enable x86_64.

* Sat Dec 25 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-4m)
- rebuild against mozilla-1.7.5

* Wed Dec 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13.2-3m)
- add BuildPreReq: xorg-x11-deprecated-libs-devel
  /usr/X11R6/include/Xm/Xm.h require X11/extensions/Print.h

* Wed Dec  8 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.13.2-2m)
- enable ALSA sequencer server interface
- install icon and modify desktop file
- mv %%{_datadir}/timidity %%{_libdir}/timidity
  to avoid Segmentation fault of timidity -ik

* Wed Dec 08 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.13.2-1m)
- update to 2.13.2
- import patches from Fedora Core
- FIXME: ump plugin is still unusable because of "undefined symbol: xmFormWidgetClass" (But mozplugger seems to play MIDI using TiMidity++)

* Thu Sep 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.11.3-32m)
- rebuild against mozilla-1.7.3

* Sat Aug  7 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.11.3-31m)
- rebuild against mozilla-1.7.2

* Tue Jul  6 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.11.3-30m)
- rebuild against mozilla-1.7-2m

* Tue Jul  6 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.11.3-29m)
- rebuild against mozilla-1.7

* Mon Jun 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.11.3-28m)
- rebuild against tcl,tk

* Fri Apr  2 2004 Toru Hoshina <t@momonga-linux.org>
- (2.11.3-27m)
- Epoch, the company that is known as "the ballpark" game vendor.

* Sat Jan 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.11.3-26m)
- rebuild against mozilla-1.6

* Wed Jan  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.11.3-25m)
- add patch for alsa-1.0

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.11.3-24m)
- put unix-sdk-3.0b5.tar.Z into repository

* Fri Oct 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.11.3-23m)
- rebuild against mozilla-1.5

* Wed Jul  2 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.11.3-22m)
- rebuild against mozilla-1.4

* Fri May 16 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.11.3-21m)
- rebuild against for mozilla-1.3.1

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.11.3-20m)
- rebuild against slang(utf8 version)
- use %%{?_smp_mflags}

* Fri Mar 14 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (2.11.3-19m)
- rebuild against for mozilla-1.3

* Tue Mar 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11.3-18m)
- change URL, Source0, Patch0, Patch1, Patch2, Patch4

* Fri Mar  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.11.3-17m)
- rebuild against for mozilla-1.3b

* Fri Feb 28 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11.3-16m)
- change Source0 URI
- change Patch0, Patch1, Patch2, Patch4 URI

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.11.3-15m)
- update alsa driver from 2.12.0-pre1

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.11.3-14k)
- rebuild against openmotif-2.2.2-2k.

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.11.3-12k)
- rebuild against libpng-1.2.2

* Tue Jan 22 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11.3-10k)
- mou wakaran

* Tue Jan 22 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11.3-8k)
- revert behaviour of 2.11.3-4k

* Sun Jan 20 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11.3-6k)
- enable to build

* Sun Jan 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (2.11.3-4k)
- change about ump
- We can enjoy midi on galeon

* Fri Jan 18 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11.3-2k)
- update to 2.11.3

* Sat Jan 12 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (2.11.2-2k)
- up to 2.11.2

* Sat Dec 29 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11.1-2k)
- update to 2.11.1

* Thu Dec 27 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.11.0-2k)
- update to 2.11.0

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (2.10.4-12k)
- rebuild against mozilla 0.9.6.

* Wed Nov 14 2001 Shingo Akagaki <dora@Kondara.org>
- (2.10.4-10k)
- change plugin dir

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.10.4-8k)
- nigittenu

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (2.10.4-6k)
- rebuild against libpng 1.2.0.

* Mon Jul 30 2001 Toru Hoshina <toru@df-usa.com>
- (2.10.4-4k)
- add alphaev5 support.

* Wed Mar 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.4-2k)
- update to 2.10.4
- remove TiMidity++-2.10.3.mumumu.patch because original source is modified

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.3-3k)
- update to 2.10.3
- add TiMidity++-2.10.3.mumumu.patch

* Sun Jan 28 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.2-11k)
- modified the bug with tcltk mode
- modifed spec file with macros

* Mon Jan 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.2-9k)
- added gnome desktop file

* Mon Jan 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.2-7k)
- obsoletes: kde-multimedia becuase of conflicts
- add japanese X default resource

* Thu Dec 28 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.2-5k)
- build against audiofile-0.2.0
- changed binary file name to timidity++

* Thu Dec 15 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.2-3k)
- update to 2.10.2 (Yoshito Komatsu <yoshito.komatsu@nifty.com>)
- fixed Requires (Yoshito Komatsu <yoshito.komatsu@nifty.com>)

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.10.1-1k)
- update to 2.10.1

* Mon Jul 17 2000 AYUHANA Tomonori <l@kondara.org>
- (2.9.5-2k)
- rebuild against rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5, Xaw3d-1.5-2k
- Source .Z -> .bz2
- change PKGDATADIR=... -> DEF_PKGDATADIR=...

* Fri Jul 07 2000 NORIKANE Seiichiro <no_ri@kf6.so-net.ne.jp>
- update to 2.9.5

* Fri Jun 30 2000 MATSUDA, Daiki <dyky@df-usa.com>
- update to 2.9.4
- enable to build if uninstalled TiMidity++
- enable esound
- enable tcltk
- modify %files about man for rpm-3.0.5

* Wed Apr 12 2000 MATSUDA, Daiki <dyky@df-usa.com>
- Version up -> 2.9.2
- disable tcltk because of unable to build on not X environment
- fixed bug not to load ump on netsape

* Mon Apr  9 2000 MATSUDA, Daiki <dyky@df-usa.com>
- Version up -> 2.9.1
- errase ALSA support on UMP

* Sun Mar 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group, Copyright, BuildRoot, Summary )

* Sat Mar  4 2000 MATSUDA, Daiki <dyky@df-usa.com>
- Version up -> 2.9.0
- modify the ja man directory

* Thu Jan 13 2000 MATSUDA, Daiki <dyky@df-usa.com>
- fixed for netscape-common-4.7-3k6

* Tue Dec 28 1999 Kondara Project
- First release.
