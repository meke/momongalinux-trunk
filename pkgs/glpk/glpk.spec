%global momorel 1

Name:           glpk
Version:        4.51
Release:	%{momorel}m%{?dist}
Summary:        GNU Linear Programming Kit

Group:          System Environment/Libraries
License:        GPL
URL:            http://www.gnu.org/software/glpk/glpk.html
Source0:	ftp://ftp.gnu.org/gnu/glpk/%{name}-%{version}.tar.gz 
NoSource:	0
BuildRequires:	gmp-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The GLPK (GNU Linear Programming Kit) package is intended for solving
large-scale linear programming (LP), mixed integer programming (MIP),
and other related problems. It is a set of routines written in ANSI C
and organized in the form of a callable library.

GLPK supports the GNU MathProg language, which is a subset of the AMPL
language.

The GLPK package includes the following main components:

 * Revised simplex method.
 * Primal-dual interior point method.
 * Branch-and-bound method.
 * Translator for GNU MathProg.
 * Application program interface (API).
 * Stand-alone LP/MIP solver. 


%package devel
Summary:        Development headers and files for GLPK
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
The glpk-devel package contains libraries and headers for developing
applications which use GLPK (GNU Linear Programming Kit).


%package utils
Summary:        GLPK-related utilities and examples
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description utils
The glpk-utils package contains the standalone solver programs glpksol
and tspsol that use GLPK (GNU Linear Programming Kit).


%prep
%setup -q
#%patch0 -p1 -b .linkmath

%build
CFLAGS="$RPM_OPT_FLAGS -fPIC" %configure
%make

%install
rm -rf %{buildroot}
make install prefix=%{buildroot}%{_prefix} \
	bindir=%{buildroot}%{_bindir} libdir=%{buildroot}%{_libdir} \
	includedir=%{buildroot}%{_includedir}/%name
## Clean up directories that are included in docs
make clean
rm -Rf examples/.deps examples/Makefile* doc/*.dvi doc/*.latex

rm -rf %{buildroot}/%{_libdir}/*.la

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_libdir}/*.so*

%files devel
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README doc
%{_includedir}/glpk
%{_libdir}/*.a

%files utils
%defattr(-,root,root)
%doc COPYING examples
%{_bindir}/*

%changelog
* Sun Jun 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.51-1m)
- update to 4.51

* Sun May  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-1m)
- update to 4.48

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.46-1m)
- update 4.46

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.44-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.44-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.44-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.44-1m)
- update to 4.44

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.38-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.38-1m)
- update 4.38

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.28-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.28-1m)
- update to 4.28

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.17-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.17-2m)
- %%NoSource -> NoSource

* Thu May 31 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.17-1m)
- update to 4.17

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.12-1m)
- build for Momonga for Octave 2.9.9
- based on the package in Fedora Extra (glpk-4.11-2.fc6.src.rpm)

* Tue Aug 29 2006 Quentin Spencer <qspencer@users.sf.net> 4.11-2
- Rebuild for FC6.

* Tue Jul 25 2006 Quentin Spencer <qspencer@users.sf.net> 4.11-1
- New release.

* Fri May 12 2006 Quentin Spencer <qspencer@users.sf.net> 4.10-1
- New release.

* Tue Feb 14 2006 Quentin Spencer <qspencer@users.sf.net> 4.9-2
- Add dist tag

* Tue Feb 14 2006 Quentin Spencer <qspencer@users.sf.net> 4.9-1
- New release.

* Tue Aug 09 2005 Quentin Spencer <qspencer@users.sf.net> 4.8-3
- Remove utils dependency on base package, since it doesn't exist until
  shared libraries are enabled.

* Tue Aug 09 2005 Quentin Spencer <qspencer@users.sf.net> 4.8-2
- Add -fPIC to compile flags.

* Fri Jul 22 2005 Quentin Spencer <qspencer@users.sf.net> 4.8-1
- First version.
