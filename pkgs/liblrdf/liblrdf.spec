%global momorel 13

Summary: Library for handling RDF descriptions of audio plugins
Name: liblrdf
Version: 0.4.0
Release: %{momorel}m%{?dist}
License: GPL
URL: http://sourceforge.net/projects/lrdf/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/lrdf/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: raptor-devel >= 1.4.9-2m, ladspa-devel
BuildRequires: openssl-devel >= 0.9.8a
BuildRequires: curl-devel >= 7.16.0

%description
liblrdf is a library for handling RDF (http://www.w3.org/RDF/)
descriptions of LADSPA (and potentially other format) plugins.

It allows grouping of plugins into trees for user slection and finer
description of plugins and ports than the .so format allows (for example
to indicatate textual equivalents of integer port values). It also
provides named and described defaults and presets, metadata and general
semnatic goodness. 

%package devel
Summary: Headers for developing programs that will use liblrdf
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the headers that programmers will need to develop
applications which will use libraries from liblrdf.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/liblrdf.so.*
%{_datadir}/ladspa

%files devel
%defattr(-,root,root)
%{_includedir}/lrdf*.h
%{_libdir}/pkgconfig/lrdf.pc
%{_libdir}/liblrdf.a
%{_libdir}/liblrdf.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-9m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-8m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-7m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-6m)
- %%NoSource -> NoSource

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.0-5m)
- rebuild against curl-7.16.0

* Sat Aug 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-4m)
- delete libtool library

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-3m)
- rebuild against openssl-0.9.8a and raptor-1.4.8

* Mon Jan  2 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.0-2m)
- rebuild against raptor-1.4.7

* Wed Feb 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- import from cooker
- update to 0.4.0

* Mon Jul 19 2004 Austin Acton <austin@mandrake.org> 0.3.7-1mdk
- 0.3.7

* Fri Jul 02 2004 Michael Scherer <misc@mandrake.org> 0.3.5-2mdk 
- rebuild for new libcurl
- remove libtool hack

* Mon Feb 16 2004 Austin Acton <austin@mandrake.org> 0.3.5-1mdk
- 0.3.5
- major 2

* Tue Jan 13 2004 Charles A Edwards <eslrahc@mandrske.org> 0.3.2-2mdk
- use /bin/true
- drop require in liblrdf0 pkg
- drop perl script

* Fri Sep 12 2003 Michael Scherer <scherer.michael@free.fr> 0.3.2-1mdk
- 0.3.2
- mklibname
- split common from library, rpmlint compliance

* Tue May 6 2003 Austin Acton <aacton@yorku.ca> 0.3.1-1mdk
- 0.3.1

* Fri Apr 25 2003 Austin Acton <aacton@yorku.ca> 0.2.4-2mdk
- update buildrequires

* Mon Mar 17 2003 Austin Acton <aacton@yorku.ca> 0.2.4-1mdk
- 0.2.4

* Tue Feb 18 2003 Austin Acton <aacton@yorku.ca> 0.2.3-1mdk
- initial package
