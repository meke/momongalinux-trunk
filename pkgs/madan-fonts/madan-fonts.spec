%global momorel 4

%global fontname madan
%global fontconf 65-0-%{fontname}.conf

Name: %{fontname}-fonts
Version: 2.000
Release: %{momorel}m%{?dist}
Summary: Font for Nepali language
Group: User Interface/X
License: GPL+
URL: http://madanpuraskar.org/
Source0: madan.zip
Source1: %{fontconf}
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: fontpackages-devel
Requires: fontpackages-filesystem

%description
This package provides the Madan font for Nepali made by the
Madan Puraskar Pustakalaya project.

%prep
%setup -c -q
for file in madan/license.txt; do
 sed "s|\r||g" $file > $file.new && \
 touch -r $file $file.new && \
 mv $file.new $file
done

%build
echo "Nothing to do in Build."

%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{fontname}/*.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf %{buildroot}

%_font_pkg -f %{fontconf} *.ttf
%doc %{fontname}/license.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.000-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.000-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.000-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.000-1m)
- sync with Fedora 13 (2.000-3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Mon Oct 15 2007 Rahul Bhalerao <rbhalera@redhat.com> - 1.0-6.fc8
- Spec update as per review

* Thu Oct 11 2007 Rahul Bhalerao <rbhalera@redhat.com> - 1.0-5.fc8
- Spec update as per reveiw

* Wed Sep 26 2007 Rahul Bhalerao <rbhalera@redhat.com> - 1.0-4.fc8
- Spec update as per review

* Fri Sep 21 2007 Rahul Bhalerao <rbahlera@redhat.com> - 1.0-3.fc8
- Added LICENSE as Source1

* Thu Sep 20 2007 Rahul Bhalerao <rbhalera@redhat.com> - 1.0-2.fc8
- Removed use of tarball and ghost files

* Thu Sep 13 2007 Rahul Bhalerao <rbhalera@redhat.com> - 1.0-1.fc8
- Initial packaging
