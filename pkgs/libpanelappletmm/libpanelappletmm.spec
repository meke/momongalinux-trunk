%global momorel 3

Summary: C++ wrappers for libpanelappletmm
Name: libpanelappletmm
Version: 2.26.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/2.26/%{name}-%{version}.tar.bz2 
NoSource: 0
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtkmm-devel >= 2.12.4
BuildRequires: gconfmm-devel >= 2.22.0
BuildRequires: libgnomemm-devel >= 2.22.0
BuildRequires: gnome-panel-devel >= 2.20.3
BuildRequires: glibmm-devel >= 2.14.2

%description
C++ wrappers for libpanelappletmm


%package devel
Summary: libpanelappletmm-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtkmm-devel
Requires: gconfmm-devel
Requires: libgnomemm-devel
Requires: gnome-panel-devel

%description    devel
libpanelappletmm-devel

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/libpanelappletmm-2.6.so.*
%exclude %{_libdir}/*.la
%dir %{_libdir}/%{name}-2.6
%dir %{_libdir}/%{name}-2.6/proc
%dir %{_libdir}/%{name}-2.6/proc/m4
%{_libdir}/%{name}-2.6/proc/m4/convert.m4
%{_libdir}/%{name}-2.6/proc/m4/convert_libpanelappletmm.m4

%files  devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/%{name}-2.6/include
%{_libdir}/pkgconfig/libpanelappletmm-2.6.pc
%{_includedir}/%{name}-2.6

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-2m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.22.0-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-8m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-5m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- rebuild against gcc43

* Sat Mar  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-2m)
- fix %%files

* Fri Mar  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- initial build
