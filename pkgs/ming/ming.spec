%global momorel 5

%if %{!?_without_python:1}0
%define python_sitearch %(%{__python} -c 'from distutils import sysconfig; print sysconfig.get_python_lib(1)')
%define python_sitelib %(%{__python} -c 'from distutils import sysconfig; print sysconfig.get_python_lib()')
%define python_includedir %(%{__python} -c 'from distutils import sysconfig; print sysconfig.get_python_inc()')
#%define python_version %(%{__python} -c 'import sys; print sys.version.split(" ")[0]')
%define python_version 2.7.4
%define python_libdir %(%{__python} -c 'import distutils.sysconfig, os.path; print os.path.dirname(distutils.sysconfig.get_python_lib(1))')
%endif

Summary: SWF output library
Name: ming
Version: 0.4.4
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.opaque.net/ming/

Source0: http://dl.sourceforge.net/sourceforge/ming/ming-%{version}.tar.bz2
NoSource: 0
Patch0: ming-0.4.4-perl5140.patch
Patch1: ming-0.4.4-perl-shared.patch
#tmp for Mo3
Obsoletes: jaming
Obsoletes: jaming-devel

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: zlib-devel, perl >= 5.10.0, bison
%{!?_without_python:BuildRequires: python-devel}

%description
Ming is a c library for generating SWF ("Flash") format movies. This
package only contains the basic c-based library - not yet extensions for
Python, Ruby, etc.

%package -n libming
Summary: Ming - an SWF output library
Group: System Environment/Libraries
Obsoletes: %{name} < %{version}
Provides: %{name} = %{version}

%description -n libming
Ming is a c library for generating SWF ("Flash") format movies.
This package only contains the basic c-based library.

%package -n libming-devel
Summary: Ming development files
Group: Development/Libraries
Requires: libming = %{version}-%{release}
Obsoletes: %{name}-devel < %{version}
Provides: %{name}-devel = %{version}

%description -n libming-devel
The %{name}-devel package contains the header files
and static libraries necessary for developing programs using the
%{name}-devel library (C and C++)..

%package -n perl-SWF
Summary: Ming perl module
Group: Development/Libraries
Requires: libming = %{version}-%{release}
Obsoletes: perl-ming
Provides: perl-ming

%description -n perl-SWF
Ming perl module - perl wrapper for the Ming library.

%package -n python-SWF
Summary: Ming python module
Group: Development/Libraries
Requires: libming = %{version}-%{release}
Obsoletes: python-ming
Provides: python-ming

%description -n	python-SWF
Python module - python wrapper for the Ming library.

%package -n %{name}-utils
Summary: Ming utilities
Group: System Environment/Libraries
Requires: libming = %{version}-%{release}

%description -n %{name}-utils
This package contains various ming utilities.


%prep
%setup -q
%patch0 -p1
%patch1 -p1

# fix python
perl -pi -e "s|/usr/local/include\b|%{_includedir}|g;s|/usr/local/lib\b|%{_libdir}|g" py_ext/setup.py


%build
autoreconf -fi
%configure --enable-shared

%make

### for perl
pushd perl_ext
    perl Makefile.PL LIBS="-L%{_libdir} -ljpeg `pkg-config --libs libpng` -lz -lm -lgif" INSTALLDIRS=vendor </dev/null
    make
popd

### for python
pushd py_ext
    env CFLAGS="%{optflags}" python setup.py build
popd

%install
%{__rm} -rf --preserve-root %{buildroot}

make DESTDIR=%{buildroot} install

### for perl
make pure_install -C perl_ext PERL_INSTALL_ROOT=%{buildroot}

### for python
pushd py_ext
    python setup.py install --root=%{buildroot}
popd

# cleanup
rm -rf %{buildroot}%{perl_vendorlib}/*/auto/SWF/include
find %{buildroot} -name '*.la' -delete
find %{buildroot} -name '*.a' -delete

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
%{__rm} -rf %{buildroot}

%files -n %{name}-utils
%defattr(-, root, root, 0755)
%doc AUTHORS COPYING HISTORY INSTALL LICENSE LICENSE_GPL2 README NEWS TODO
%{_bindir}/dbl2png
%{_bindir}/gif2dbl
%{_bindir}/gif2mask
%{_bindir}/listaction
%{_bindir}/listaction_d
%{_bindir}/listfdb
%{_bindir}/listjpeg
%{_bindir}/listmp3
%{_bindir}/listswf
%{_bindir}/listswf_d
%{_bindir}/makefdb
%{_bindir}/makeswf
%{_bindir}/png2dbl
%{_bindir}/raw2adpcm
%{_bindir}/swftocxx
%{_bindir}/swftoperl
%{_bindir}/swftophp
%{_bindir}/swftopython
%{_bindir}/swftotcl

%files -n libming
%defattr(-, root, root, 0755)
%{_libdir}/libming.so.*

%files -n libming-devel
%defattr(-, root, root, 0755)
%{_bindir}/ming-config
%{_libdir}/libming.so
%{_libdir}/pkgconfig/libming.pc
%{_includedir}/ming.h
%{_includedir}/mingpp.h

%files -n perl-SWF
%defattr(-, root, root, 0755)
%doc perl_ext/README perl_ext/TODO
%doc %{_mandir}/man3/SWF*
%{perl_vendorarch}/SWF.pm
%{perl_vendorarch}/SWF
%{perl_vendorarch}/auto/SWF

%if %{!?_without_python:1}0
%files -n python-SWF
%defattr(-, root, root, 0755)
%doc py_ext/README py_ext/TODO
%{python_sitearch}/_mingc.so
%{python_sitearch}/ming.py
%{python_sitearch}/ming.pyc
%ghost %{python_sitearch}/ming.pyo
%{python_sitearch}/mingc.py
%{python_sitearch}/mingc.pyc
%ghost %{python_sitearch}/mingc.pyo
%{python_sitearch}/*.egg-info
%endif

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-2m)
- rebuild against perl-5.18.0

* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Thu May  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-39m)
- rebuild against python-2.7.4

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-38m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-37m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-36m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-35m)
- rebuild against perl-5.16.0

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-34m)
- rebuild against python-2.7.3

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-33m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-32m)
- rebuild against perl-5.14.1

* Thu Jun 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-31m)
- rebuild against python-2.7.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.0-30m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-29m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-28m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-27m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-26m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-25m)
- rebuild against perl-5.12.1

* Sun May  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-24m)
- rebuild against python-2.6.5

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-23m)
- rebuild against perl-5.12.0

* Thu Mar 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-22m)
- correct permission

* Fri Jan 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-21m)
- rebuild against python-2.6.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-19m)
- rebuild against perl-5.10.1

* Thu Jun 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-18m)
- rebuild against python-2.6.2

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-17m)
- use vendor

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-16m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.0-15m)
- rebuild agaisst python-2.6.1-1m

* Fri Nov 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-14m)
- apply bison24 patch

* Mon Oct 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-13m)
- rebuild against python-2.5.2

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-12m)
- modify %%files

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-11m)
- rebuild against gcc43

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-10m)
- rebuild against perl-5.10.0

* Tue Jul 31 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-9m)
- not multi build

* Mon Jun  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.0-8m)
- rebuild against python 2.5.1

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-7m)
- increase release for upgrading from STABLE_3

* Sat Jan 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-6m)
- fix ming-config script

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-5m)
- rebuild against python-2.5

* Wed Dec 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-4m)
- rebuild against python-2.4.4

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.0-3m)
- rebuild against db4-4.5.20-1m

* Thu Aug 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.0-2m)
- modify install dir

* Thu Jul 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-1m)
- import momonga from http://dag.wieers.com/packages/ming/ming.spec
- Momonganize
  - NoSource
  - add -q at %%setup
  - add
    Obsoletes: jaming
    Obsoletes: jaming-devel
  - fix Group: Development/Languages/Perl -> Development/Languages

* Mon May 29 2006 Dag Wieers <dag@wieers.com> - 0.3.0-2
- Added perl and python bindings.

* Sun May 28 2006 Dries Verachtert <dries@ulyssis.org> - 0.3.0-1
- Updated to release 0.3.0.
- Bindings (ming-perl, ming-php, ..) are distributed in separate files now.

* Tue Mar 15 2005 Dag Wieers <dag@wieers.com> - 0.2a-2
- Made libming libraries executable.

* Fri May 14 2004 Dag Wieers <dag@wieers.com> - 0.2a-1
- Initial package. (using DAR)
