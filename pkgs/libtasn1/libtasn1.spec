%global          momorel 1
Summary:        The ASN.1 library used in GNUTLS
Name:           libtasn1
Version:        3.2
Release:        %{momorel}m%{?dist}

# The libtasn1 library is LGPLv2+, utilities are GPLv3+
License:        "GPLv3+ and LGPLv2+"
Group:          System Environment/Libraries
URL:            http://www.gnu.org/software/libtasn1/
Source0:        http://ftp.gnu.org/gnu/libtasn1/%name-%version.tar.gz
NoSource:       0
Source1:        http://ftp.gnu.org/gnu/libtasn1/%name-%version.tar.gz.sig
NoSource:       1
Patch1:         libtasn1-2.12-rpath.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  bison, pkgconfig
%ifarch %ix86 x86_64 ppc ppc64
BuildRequires:  valgrind
%endif


%package devel
Summary:        Files for development of applications which will use libtasn1
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires(post):  /sbin/install-info
Requires(postun): /sbin/install-info


%package tools
Summary:        Some ASN.1 tools
Group:          Applications/Text
License:        GPLv3+
Requires:       %{name} = %{version}-%{release}


%description
This is the ASN.1 library used in GNUTLS.  More up to date information can
be found at http://www.gnu.org/software/gnutls and http://www.gnutls.org

%description devel
This is the ASN.1 library used in GNUTLS.  More up to date information can
be found at http://www.gnu.org/software/gnutls and http://www.gnutls.org

This package contains files for development of applications which will
use libtasn1.


%description tools
This is the ASN.1 library used in GNUTLS.  More up to date information can
be found at http://www.gnu.org/software/gnutls and http://www.gnutls.org

This package contains tools using the libtasn library.


%prep
%setup -q

%patch1 -p1 -b .rpath

%build
%configure --disable-static
%make

%install
rm -rf "$RPM_BUILD_ROOT"

make DESTDIR="$RPM_BUILD_ROOT" install

rm -f $RPM_BUILD_ROOT{%_libdir/*.la,%_infodir/dir}


%check
make check


%clean
rm -rf "$RPM_BUILD_ROOT"


%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%post devel
test -f %_infodir/%name.info.gz && \
	/sbin/install-info --info-dir=%_infodir %_infodir/%name.info || :

%preun devel
test "$1" = 0 -a -f %_infodir/%name.info.gz && \
	/sbin/install-info --info-dir=%_infodir --delete %_infodir/%name.info || :

%files
%defattr(-,root,root,-)
%doc doc/TODO doc/*.pdf
%doc AUTHORS COPYING* ChangeLog NEWS README THANKS
%_libdir/*.so.*

%files tools
%defattr(-,root,root,-)
%_bindir/asn1*
%_mandir/man1/asn1*

%files devel
%defattr(-,root,root,-)
%_libdir/*.so
%_libdir/pkgconfig/*.pc
%_includedir/*
%_infodir/*.info.*
%_mandir/man3/*asn1*


%changelog
* Fri Jan  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13-1m)
- update 2.13
- re-import from fedora
-- add sub-package; libtasn1-tools 

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-1m)
- [SECURITY] CVE-2012-1589 CVE-2012-1573
- update to 2.12

* Sat Dec  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-2m)
- rebuild for new GCC 4.6

* Mon Jan  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9-1m)
- update to 2.9

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.7-1m)
- update to 2.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3-1m)
- update to 2.3

* Sun Mar  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8-2m)
- change source url

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8-1m)
- update to 1.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-3m)
- rebuild against rpm-4.6

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- run install-info

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-2m)
- rebuild against gcc43

* Sat Mar  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- %%NoSource -> NoSource

* Tue Oct 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8-2m)
- retrived libtool library

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8-1m)
- initial build
