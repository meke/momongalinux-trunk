%global momorel 1

Summary: SDL portable TrueType font library
Name: SDL_ttf
Version: 2.0.11
Release: %{momorel}m%{?dist}
Source0: http://www.libsdl.org/projects/%{name}/release/%{name}-%{version}.tar.gz 
NoSource: 0
URL: http://www.libsdl.org/projects/SDL_ttf/
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel >= 1.2.15
BuildRequires: freetype-devel >= 2.3.1-2m

%description
This is a sample library which allows you to use TrueType fonts in
your SDL applications.

%package devel
Summary: Libraries and includes to develop SDL applications are using TrueType fonts
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This is a sample library which allows you to use TrueType fonts in
your SDL applications.

This is the libraries and include files you can use to develop SDL
applications are using TrueType fonts.

%prep
%setup -q 

%build
%configure \
	--disable-dependency-tracking \
	--disable-static
make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name '*.la' -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README CHANGES COPYING
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%doc README CHANGES COPYING
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/SDL/*

%changelog
* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.11-1m)
- update to 2.0.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.10-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.10-1m)
- update to 2.0.10

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.9-5m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.9-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.9-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.9-1m)
- update to 2.0.9
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.8-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.8-5m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.8-4m)
- BuildPreReq: freetype2 -> freetype-devel

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.8-3m)
- delete libtool library

* Sun Jan 28 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.8-2m)
- add SDL_ttf-2.0.8-noftinternals.patch

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.7-1m)
- version up

* Wed Nov 24 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.0.6-4m)
- added a patch to fix build problems
- use %%NoSource macro

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.6-3m)
- build against SDL-1.2.7-4m

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.0.6-2m)
- revised spec for enabling rpm 4.2.

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.6-1m)
  update to 2.0.6

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.5-3m)
- reubuild aginst SDL-1.2.4-3m

* Thu May 20 2002 Kenta MURATA <muraken@kondara.org>
- (2.0.5-2k)
- first spec file.
