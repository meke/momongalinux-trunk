%global momorel 11

Summary:	Base Library for the Java-GNOME libraries 
Name:		glib-java
Version:	0.2.6
Release:	%{momorel}m%{?dist}
URL:		http://java-gnome.sourceforge.net
Source0:	http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.2/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:		%{name}-gjavah.patch
License:	LGPL
Group:		Development/Libraries
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:	glibc
Requires:	glib2 >= 2.7.0
BuildRequires:	java-devel >= 1.4.2 glib2-devel >= 2.7.0
BuildRequires:	gcc-java >= 4.1.1, docbook-utils
BuildRequires:	pkgconfig

%description 
Glib-java is a base framework for the Java-GNOME libraries.  Allowing the use
of GNOME through Java.

%package        devel
Summary:	Compressed Java source files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	glib2-devel
Requires:	pkgconfig

%description    devel
Glib-java is a base framework for the Java-GNOME libraries.  Allowing the use
of GNOME through Java.

Development part of %{name}.


%prep
%setup -q -n %{name}-%{version}

%patch0 -p0
touch aclocal.m4
touch configure Makefile.in

%build
# we need POSIX.2 grep
export POSIXLY_CORRECT=1

# Two workarounds:
# 1) libtool.m4 calls gcj with $CFLAGS and gcj seems to choke on -Wall.
# 2) libtool does not use pic_flag when compiling, so we have to force it.
RPM_OPT_FLAGS=${RPM_OPT_FLAGS/-Wall /}
%configure CFLAGS="$RPM_OPT_FLAGS" GCJFLAGS="-O2 -fPIC" CPPFLAGS=-DGLIB_COMPILATION

make %{?_smp_mflags}

# pack up the java source
find src/java -name \*.java -newer ChangeLog | xargs touch -r ChangeLog
(cd src/java && find . -name \*.java | sort | xargs zip -X -9 src.zip)
touch -r ChangeLog src/java/src.zip


%install
rm -rf $RPM_BUILD_ROOT

make  DESTDIR=$RPM_BUILD_ROOT  install %{?_smp_mflags}

# Remove unpackaged files:
rm $RPM_BUILD_ROOT/%{_libdir}/*.la

# install the src zip and make a sym link
jarversion=$(expr '%{version}' : '\([^.]*\.[^.]*\)')
jarname=%{name}
jarname=${jarname%%-*}
zipname=${jarname#lib}-$jarversion-src
zipfile=$zipname-%{version}.zip
install -m 644 src/java/src.zip $RPM_BUILD_ROOT%{_datadir}/java/$zipfile
(cd $RPM_BUILD_ROOT%{_datadir}/java &&
  ln -sf $zipfile $zipname.zip)

# Is the NEWS file still empty?  See the file list below.
test -f NEWS -a ! -s NEWS


%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig 
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
# Do not distribute the NEWS file, it is empty.
%doc AUTHORS ChangeLog COPYING README 
%{_libdir}/libglibjava-*.so
%{_libdir}/libglibjni-*.so
%{_datadir}/java/*.jar
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/*

%files devel
%defattr(-,root,root)
%doc doc/api
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*
%{_libdir}/libglibjava.so
%{_libdir}/libglibjni.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/java/*.zip

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-11m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.6-10m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-8m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-7m)
- fix build failure by adding "export POSIXLY_CORRECT=1"

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.6-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.6-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.6-4m)
- rebuild against rpm-4.6

* Tue May 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.6-3m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.6-2m)
- rebuild against gcc43

* Sun Jun 10 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.6-1m)
- import from Fedora

* Fri Feb  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Mon Jan  1 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4.1-2m)
- add "-fPIC" to GCJFLAGS in spec file

* Mon Jan  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- initial build
