%global         momorel 3

Name:           perl-String-CRC32
Version:        1.5
Release:        %{momorel}m%{?dist}
Summary:        Perl interface for cyclic redundency check generation
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/String-CRC32/
Source0:        http://www.cpan.org/authors/id/S/SO/SOENKE/String-CRC32-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The CRC32 module calculates CRC sums of 32 bit lenghts. It generates the
same CRC values as ZMODEM, PKZIP, PICCHECK and many others.

%prep
%setup -q -n String-CRC32-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc MANIFEST README
%dir %{perl_vendorarch}/String
%dir %{perl_vendorarch}/auto/String/CRC32
%{perl_vendorarch}/String/CRC32.*
%{perl_vendorarch}/auto/String/CRC32/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-23m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-22m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-21m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-20m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-19m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-18m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-17m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-16m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-15m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-14m)
- rebuild for new GCC 4.6

* Fri Apr 08 2011 McLellan Daniel <daniel.mclellan@gmail.com>
- 1.4-13m
- added BuildRequires: perl-ExtUtils-Manifest perl-ExtUtils-Command libbsd-devel
 
* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4-2m)
- use vendor

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4-1m)
- update to 1.4

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3-3m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.3-2m)
- remove unwanted file (.packlist)

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.3-1m)
- initial import to Momonga Linux
