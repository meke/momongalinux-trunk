%global         momorel 2

Name:           perl-DateTime-Format-Flexible
Version:        0.26
Release:        %{momorel}m%{?dist}
Summary:        DateTime::Format::Flexible - Flexibly parse strings and turn them into DateTime objects
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DateTime-Format-Flexible/
Source0:        http://www.cpan.org/authors/id/T/TH/THINC/DateTime-Format-Flexible-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-DateTime
BuildRequires:  perl-DateTime-Format-Builder >= 0.74
BuildRequires:  perl-DateTime-TimeZone
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Module-Pluggable
BuildRequires:  perl-Test-MockTime
BuildRequires:  perl-Test-Simple >= 0.44
Requires:       perl-DateTime
Requires:       perl-DateTime-Format-Builder >= 0.74
Requires:       perl-DateTime-TimeZone
Requires:       perl-List-MoreUtils
Requires:       perl-Module-Pluggable
Requires:       perl-Test-MockTime
Requires:       perl-Test-Simple >= 0.44
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
If you have ever had to use a program that made you type in the date a
certain way and thought "Why can't the computer just figure out what date I
wanted?", this module is for you.

%prep
%setup -q -n DateTime-Format-Flexible-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README TODO
%{perl_vendorlib}/DateTime/Format/Flexible*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-2m)
- rebuild against perl-5.20.0

* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-2m)
- rebuild against perl-5.16.3

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Tue Nov 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-2m)
- update to 0.23
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-2m)
- rebuild against perl-5.14.2

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-2m)
- rebuild for new GCC 4.6

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Jan  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-2m)
- rebuild for new GCC 4.5

* Wed Oct 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-9m)
- rebuild against perl-5.12.1

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.09-8m)
- add BuildRequires:  perl-Params-Validate

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-7m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-6m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.09-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-3m)
- rebuild against perl-5.10.1

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-2m)
- modify BuildRequires

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
