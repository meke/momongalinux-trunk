%global momorel 1

%define modular_conf  1

%define default_text  %{_datadir}/doc/momonga-release-%{momonga}/RELEASE-NOTES
%define default_URL   http://www.momonga-linux.org/

%define pam_ver       0.80-7
%define autoconf_ver  2.53
%define update_po     1

Summary: X screen saver and locker
Name: xscreensaver
Version: 5.19
Release: %{momorel}m%{?dist}
License: MIT
Group: Amusements/Graphics
URL: http://www.jwz.org/xscreensaver/
Source0: http://www.jwz.org/xscreensaver/xscreensaver-%{version}.tar.gz
NoSource: 0
%if %{modular_conf}
Source10: update-xscreensaver-hacks
%endif
Source11: xscreensaver-autostart
Source12: xscreensaver-autostart.desktop
##
## Patches
##
# bug 129335
# sanitize the names of modes in barcode
Patch1:          xscreensaver-5.00b5-sanitize-hacks.patch
# Change webcollage not to access to net
# Also see bug 472061
Patch21:         xscreensaver-5.16-webcollage-default-nonet.patch
#
## Patches already sent to the upsteam
# Remove "AC_PROG_CC' was expanded before it was required" warning
Patch30:         xscreensaver-5.11-conf264.patch
#
## Patches which must be discussed with upstream
#
# Update Japanese po file
Patch32:         xscreensaver-5.13-dpmsQuickoff-japo.patch
# Fix segv on lament with -wireframe option 
# or -no-texture option (bug 849961)
Patch33:         xscreensaver-5.19-lament-wireframe.patch
# Fix improper and operator on flurry detected by llvm-clang
Patch34:         xscreensaver-5.19-flurry-clangwarn.patch
# Remove warning from calling glLighti with float argument in engine.c
Patch35:         xscreensaver-5.19-engine-clangwarn.patch
# driver/test-passwd tty segfaults
Patch41:         xscreensaver-5.12-test-passwd-segv-tty.patch
# patch to compile driver/test-xdpms
Patch42:         xscreensaver-5.12-tests-miscfix.patch
# patch to compile driver/test-passwd.c with
# xscreensaver-5.17-2 and above
Patch43:         xscreensaver-5.17-blurb-hndl-test-passwd.patch
# 
# Patches end

# Momonga patches
Patch50: xscreensaver-5.19-no-webcollage.patch

Requires: xscreensaver-base = %{version}-%{release}
Requires: xscreensaver-extras = %{version}-%{release}
Requires: xscreensaver-gl-extras = %{version}-%{release}

%package base
Summary: A minimal installation of xscreensaver
Group: Amusements/Graphics
BuildRequires: autoconf
BuildRequires: bc
BuildRequires: desktop-file-utils
BuildRequires: gawk
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: pam-devel > %{pam_ver}
BuildRequires: sed
BuildRequires: xdg-utils
BuildRequires: xorg-x11-proto-devel
BuildRequires: libX11-devel
BuildRequires: libXScrnSaver-devel
BuildRequires: libXext-devel
BuildRequires: libXinerama-devel
BuildRequires: libXmu-devel
BuildRequires: libXpm-devel
BuildRequires: libXt-devel
BuildRequires: libXxf86misc-devel
BuildRequires: libXxf86vm-devel
BuildRequires: gtk2-devel	
BuildRequires: libjpeg-devel
BuildRequires: libglade2-devel
#BuildRequires: momonga-release-notes
Requires: pam > %{pam_ver}
Requires: xdg-utils
Requires: xorg-x11-resutils
Requires: xorg-x11-fonts-ISO8859-1-100dpi

%package extras
Summary: An enhanced set of screensavers
Group: Amusements/Graphics
BuildRequires: desktop-backgrounds-basic
Requires: %{name}-base = %{version}-%{release}

%package gl-base
Summary: A base package for screensavers that require OpenGL
Group: Amusements/Graphics
Requires: %{name}-base = %{version}-%{release}

%package gl-extras
Summary: An enhanced set of screensavers that require OpenGL
Group: Amusements/Graphics
Provides: xscreensaver-gl = %{version}-%{release}
Obsoletes: xscreensaver-gl < %{version}-%{release}
BuildRequires: libGL-devel
BuildRequires: libGLU-devel
%if %{modular_conf}
Requires: %{name}-gl-base = %{version}-%{release}
%else
Requires: %{name}-base = %{version}-%{release}
%endif

%package extras-gss
Summary: Desktop files of extras for gnome-screensaver
Group: Amusements/Graphics
Requires: %{name}-extras = %{version}-%{release}
Requires: gnome-screensaver

%package gl-extras-gss
Summary: Desktop files of gl-extras for gnome-screensaver
Group: Amusements/Graphics
Requires: %{name}-gl-extras = %{version}-%{release}
Requires: gnome-screensaver

%description
A modular screen saver and locker for the X Window System.
More than 200 display modes are included in this package.

This is a metapackage for installing all default packages
related to XScreenSaver.

%description base
A modular screen saver and locker for the X Window System.
This package contains the bare minimum needed to blank and
lock your screen.  The graphical display modes are the
"xscreensaver-extras" and "xscreensaver-gl-extras" packages.

%description extras
A modular screen saver and locker for the X Window System.
This package contains a variety of graphical screen savers for
your mind-numbing, ambition-eroding, time-wasting, hypnotized
viewing pleasure.

%description gl-base
A modular screen saver and locker for the X Window System.
This package contains minimal files to make screensaver hacks
that require OpenGL work for XScreenSaver.

%description gl-extras
A modular screen saver and locker for the X Window System.
This package contains a variety of OpenGL-based (3D) screen
savers for your mind-numbing, ambition-eroding, time-wasting,
hypnotized viewing pleasure.

%description extras-gss
This package contains desktop files of extras screensavers
for gnome-screensaver compatibility.

%description gl-extras-gss
This package contains desktop files of gl-extras screensavers
for gnome-screensaver compatibility.

%prep
%setup -q

%patch1 -p1 -b .sanitize-hacks
#patch21 -p1 -b .nonet
%patch32 -p1 -b .dpmsoff_japo
%patch33 -p1 -b .wire
%patch34 -p1 -b .andand
%patch35 -p1 -b .fl_int
%patch41 -p1 -b .test_passwd
%patch42 -p1 -b .test_misc
%patch43 -p1 -b .hndl_extra

%patch50 -p1 -b .no-webcollage

#remove problematic webcollage screensaver
rm -rf hacks/webcollage*
rm -rf hacks/config/webcollage*

change_option(){
   set +x
   ADFILE=$1
   if [ ! -f ${ADFILE}.opts ] ; then
      cp -p $ADFILE ${ADFILE}.opts
   fi
   shift

   for ARG in "$@" ; do
      TYPE=`echo $ARG | sed -e 's|=.*$||'`
      VALUE=`echo $ARG | sed -e 's|^.*=||'`

      eval sed -i \
         -e \'s\|\^\\\(\\\*$TYPE\:\[ \\t\]\[ \\t\]\*\\\)\[\^ \\t\]\.\*\$\|\\1$VALUE\|\' \
         $ADFILE
   done
   set -x
}

silence_hack(){
   set +x
   ADFILE=$1
   if [ ! -f ${ADFILE}.hack ] ; then
      cp -p $ADFILE ${ADFILE}.hack
   fi
   shift

   for hack in "$@" ; do
      eval sed -i \
         -e \'\/\^\[ \\t\]\[ \\t\]\*$hack\/s\|\^\|-\|g\' \
         -e \'s\|\^@GL_\.\*@.*\\\(GL\:\[ \\t\]\[ \\t\]\*$hack\\\)\|-\\t\\1\|g\' \
         $ADFILE
   done
   set -x
}

# change some files to UTF-8
for f in \
   driver/XScreenSaver.ad.in \
   hacks/glx/sproingies.man \
   ; do
   iconv -f ISO-8859-1 -t UTF-8 $f > $f.tmp || cp -p $f $f.tmp
   touch -r $f $f.tmp
   mv $f.tmp $f
done

# Change some options
# For grabDesktopImages, lock, see bug 126809
change_option driver/XScreenSaver.ad.in \
   captureStderr=False \
   passwdTimeout=0:00:15 \
   grabDesktopImages=False \
   lock=True \
   splash=False \
   ignoreUninstalledPrograms=True \
   textProgram=fortune\ -s \
   textURL=%{default_URL}

# Disable the following hacks by default
# (disable, not remove)
silence_hack driver/XScreenSaver.ad.in \
   bsod flag

# Record time, EVR
eval sed -i.ver \
   -e \'s\|version \[45\]\.\[0-9a-z\]\[0-9a-z\]\*\|version %{version}-`echo \
      %{release} | sed -e '/IGNORE THIS/s|\.[a-z][a-z0-9].*$||'`\|\' \
      driver/XScreenSaver.ad.in

eval sed -i.date \
   -e \'s\|\[0-9\].\*-.\*-20\[0-9\]\[0-9\]\|`LANG=C date -u +'%%d-%%b-%%Y'`\|g\' \
   driver/XScreenSaver.ad.in

eval sed -i.ver \
   -e \'s\|\(\[0-9\].\*-.\*-20\[0-9\]\[0-9\]\)\|\(`LANG=C \
      date -u +'%%d-%%b-%%Y'`\)\|g\' \
   -e \'s\|\\\(5.\[0-9\]\[0-9\]\\\)[a-z]\[0-9\]\[0-9\]\*\|\\\1\|\' \
   -e \'s\|5.\[0-9\]\[0-9\]\|%{version}-`echo %{release} | \
      sed -e '/IGNORE THIS/s|\.[a-zA-Z][a-zA-Z0-9].*$||'`\|\' \
   utils/version.h

# Move man entry to 6x (bug 197741)
for f in `find hacks -name Makefile.in` ; do
   sed -i.mansuf \
      -e '/^mansuffix/s|6|6x|'\
      $f
done

# Search first 6x entry, next 1 entry for man pages
sed -i.manentry -e 's@man %%s@man 6x %%s 2>/dev/null || man 1 %%s @' \
   driver/XScreenSaver.ad.in

# Suppress rpmlint warnings.
# suppress about pam config (although this is 
# not the fault of xscreensaver.pam ......).
%if 0
sed -i.rpmlint -n -e '1,5p' driver/xscreensaver.pam 
%endif

if [ -x %{_datadir}/libtool/config.guess ]; then
  # use system-wide copy
   cp -p %{_datadir}/libtool/config.{sub,guess} .
fi

# Fix for desktop-file-utils 0.14+
sed -i.icon -e 's|xscreensaver\.xpm|xscreensaver|' \
   driver/screensaver-properties.desktop.in

# Disable (don't build) some tests
# apm: doesn't compile
# passwd: causes segv
# mlstring: causes OOM
sed -i.test \
   -e 's|test-apm[ \t][ \t]*t|t|' \
   -e 's|test-passwd[ \t][ \t]*t|t|' \
   -e 's|test-mlstring[ \t][ \t]*t|t|' \
   driver/Makefile.in
sed -i.dir -e '/TEST_FADE_OBJS =/s|UTILS_SRC|UTILS_BIN|' driver/Makefile.in

# test-fade: give more time between fading
sed -i.delay -e 's| delay = 1| delay = 3|' driver/test-fade.c
# test-grab: testing time too long, setting time 15 min -> 20 sec
sed -i.delay -e 's|60 \* 15|20|' driver/test-grab.c

autoconf
autoheader

%build

archdir=`./config.guess`
[ -d $archdir ] || mkdir $archdir
cd $archdir

export CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}"

CONFIG_OPTS="--prefix=%{_prefix} --with-pam --without-shadow --without-kerberos"
CONFIG_OPTS="$CONFIG_OPTS --without-setuid-hacks"
CONFIG_OPTS="$CONFIG_OPTS --with-text-file=%{default_text}"
CONFIG_OPTS="$CONFIG_OPTS --with-x-app-defaults=%{_datadir}/X11/app-defaults"
CONFIG_OPTS="$CONFIG_OPTS --disable-root-passwd"
CONFIG_OPTS="$CONFIG_OPTS --with-browser=xdg-open"

# This is flaky:
# CONFIG_OPTS="$CONFIG_OPTS --with-login-manager"

unlink configure || :
ln -s ../configure .
%configure $CONFIG_OPTS
rm -f configure

%if %{update_po}
( cd po ; make generate_potfiles_in update-po )
%endif

make %{?_smp_mflags} -k \
	GMSGFMT="msgfmt --statistics"

%if %{modular_conf}
# Make XScreenSavar.ad modular (bug 200881)
CONFD=xscreensaver
rm -rf $CONFD
mkdir $CONFD

# Preserve the original adfile
cp -p driver/XScreenSaver.ad $CONFD

# First split XScreenSaver.ad into 3 parts
cat driver/XScreenSaver.ad | \
   sed -n -e '1,/\*programs/p' > $CONFD/XScreenSaver.ad.header
cat driver/XScreenSaver.ad | sed -e '1,/\*programs/d' | \
   sed -n -e '1,/\\n$/p' > $CONFD/XScreenSaver.ad.hacks
cat driver/XScreenSaver.ad | sed -e '1,/\\n$/d' > $CONFD/XScreenSaver.ad.tail

# Seperate XScreenSaver.ad.hacks into each hacks
cd $CONFD
mkdir hacks.conf.d ; cp -p XScreenSaver.ad.hacks hacks.conf.d/xscreensaver.conf
cd ..

%endif

%install
archdir=`./config.guess`
cd $archdir

rm -rf ${RPM_BUILD_ROOT}

# We have to make sure these directories exist,
# or nothing will be installed into them.
#
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pam.d

make install_prefix=$RPM_BUILD_ROOT INSTALL="install -c -p" install

# remove problematic webcollage screensaver
find %{buildroot} -name "webcollage*" | xargs rm -f

desktop-file-install --vendor "" --delete-original    \
   --dir $RPM_BUILD_ROOT%{_datadir}/applications         \
%if 0
   --add-only-show-in GNOME                              \
%endif
   --add-category    DesktopSettings                     \
%if 0
   --add-category X-Red-Hat-Base                         \
%else
   --remove-category Appearance                          \
   --remove-category AdvancedSettings                    \
   --remove-category Application                         \
%endif
   $RPM_BUILD_ROOT%{_datadir}/applications/*.desktop

# add NotShowIn=GNOME;KDE;XFCE
echo "NotShowIn=GNOME;KDE;XFCE;" >> \
    $RPM_BUILD_ROOT%{_datadir}/applications/xscreensaver-properties.desktop

# This function prints a list of things that get installed.
# It does this by parsing the output of a dummy run of "make install".
list_files() {
   echo "%%defattr(-,root,root,-)"
   make -s install_prefix=${RPM_BUILD_ROOT} INSTALL=true "$@"  \
      | sed -n -e 's@.* \(/[^ ]*\)$@\1@p'                      \
      | sed    -e "s@^${RPM_BUILD_ROOT}@@"                     \
               -e "s@/[a-z][a-z]*/\.\./@/@"                    \
      | sed    -e 's@\(.*/man/.*\)@%%doc \1\*@'                      \
               -e 's@\(.*/pam\.d/\)@%%config(noreplace) \1@'    \
      | sort  \
      | uniq
}

# Generate three lists of files for the three packages.
#
dd=%{_builddir}/%{name}-%{version}
(  cd hacks     ; list_files install ) >  $dd/extras.files
(  cd hacks/glx ; list_files install ) >  $dd/gl-extras.files
(  cd driver    ; list_files install ) >  $dd/base.files

# In case rpm -bi --short-circuit is tried multiple times:
rm -f $dd/*.files

(  cd hacks     ; list_files install ) >  $dd/extras.files
(  cd hacks/glx ; list_files install ) >  $dd/gl-extras.files
(  cd driver    ; list_files install ) >  $dd/base.files

# Move %%{_bindir}/xscreensaver-gl-helper to gl-base
# (bug 336331).
%if %{modular_conf}
echo "%%defattr(-,root,root,-)" >> $dd/gl-base.files

sed -i -e '/xscreensaver-gl-helper/d' $dd/gl-extras.files
pushd $RPM_BUILD_ROOT
for dir in `find . -name \*xscreensaver-gl-helper\*` ; do
   echo "${dir#.}" >> $dd/gl-base.files
done
popd
sed -i -e 's|^\(%{_mandir}.*\)$|\1*|' $dd/gl-base.files
%endif

%if %{modular_conf}
# Install update script
mkdir -p $RPM_BUILD_ROOT%{_sbindir}
install -cpm 755 %{SOURCE10} $RPM_BUILD_ROOT%{_sbindir}
echo "%{_sbindir}/update-xscreensaver-hacks" >> $dd/base.files

# Make hack conf modular
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/xscreensaver
mkdir -p $RPM_BUILD_ROOT%{_datadir}/xscreensaver/hacks.conf.d
cp -p xscreensaver/XScreenSaver.ad* \
   $RPM_BUILD_ROOT%{_sysconfdir}/xscreensaver
cp -p xscreensaver/hacks.conf.d/xscreensaver.conf \
   $RPM_BUILD_ROOT%{_datadir}/xscreensaver/hacks.conf.d/

for adfile in xscreensaver/XScreenSaver.ad.* ; do
   filen=`basename $adfile`
   echo "%%config(noreplace) %{_sysconfdir}/xscreensaver/$filen" >> $dd/base.files
done
echo -n "%%verify(not size md5 mtime) " >> $dd/base.files
echo "%{_sysconfdir}/xscreensaver/XScreenSaver.ad" >> \
   $dd/base.files
echo "%{_datadir}/xscreensaver/hacks.conf.d/xscreensaver.conf" \
   >> $dd/base.files

# Check symlink
rm -f $RPM_BUILD_ROOT%{_datadir}/X11/app-defaults/XScreenSaver

pushd $RPM_BUILD_ROOT%{_datadir}/X11/app-defaults
pushd ../../../..
if [ ! $(pwd) == $RPM_BUILD_ROOT ] ; then
   echo "Possibly symlink broken"
   exit 1
fi
popd
popd

ln -sf ../../../..%{_sysconfdir}/xscreensaver/XScreenSaver.ad \
   $RPM_BUILD_ROOT%{_datadir}/X11/app-defaults/XScreenSaver

%endif

# Add documents
pushd $dd &> /dev/null
for f in README* ; do
   echo "%%doc $f" >> $dd/base.files
done
popd

# Add directory
pushd $RPM_BUILD_ROOT
for dir in `find . -type d | grep xscreensaver` ; do
   echo "%%dir ${dir#.}" >> $dd/base.files
done
popd

%find_lang %{name}
cat %{name}.lang | uniq >> $dd/base.files

# Suppress rpmlint warnings
# sanitize path in script file
for f in ${RPM_BUILD_ROOT}%{_bindir}/xscreensaver-getimage-* \
   ${RPM_BUILD_ROOT}%{_libexecdir}/xscreensaver/vidwhacker \
   ${RPM_BUILD_ROOT}%{_bindir}/xscreensaver-text ; do
   if [ -f $f ] ; then
      sed -i -e 's|%{_prefix}//bin|%{_bindir}|g' $f
   fi
done

# Install desktop application autostart stuff
# Add OnlyShowIn=GNOME (bug 517391)
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/xdg/autostart
install -cpm 0755 %{SOURCE11} ${RPM_BUILD_ROOT}%{_libexecdir}/
desktop-file-install \
   --vendor "" \
   --dir ${RPM_BUILD_ROOT}%{_sysconfdir}/xdg/autostart \
   --add-only-show-in=GNOME \
   %{SOURCE12}
chmod 0644 ${RPM_BUILD_ROOT}%{_sysconfdir}/xdg/autostart/xscreensaver*.desktop

echo "%{_libexecdir}/xscreensaver-autostart" >> $dd/base.files
echo '%{_sysconfdir}/xdg/autostart/xscreensaver*.desktop' >> $dd/base.files

# Create desktop entry for gnome-screensaver
# bug 204944, 208560
create_desktop(){
   COMMAND=`cat $1 | sed -n -e 's|^<screen.*name=\"\([^ ][^ ]*\)\".*$|\1|p'`
# COMMAND must be full path (see bug 531151)
# Check if the command actually exists
   COMMAND=%{_libexecdir}/xscreensaver/$COMMAND
   if [ ! -x $RPM_BUILD_ROOT/$COMMAND ] ; then
      echo
      echo "WARNING:"
      echo "$COMMAND could not be found under $RPM_BUILD_ROOT"
      #exit 1
   fi
   NAME=`cat $1 | sed -n -e 's|^<screen.*_label=\"\(.*\)\">.*$|\1|p'`
   ARG=`cat $1 | sed -n -e 's|^.*<command arg=\"\([^ ][^ ]*\)\".*$|\1|p'`
   ARG=$(echo "$ARG" | while read line ; do echo -n "$line " ; done)
   COMMENT="`cat $1 | sed -e '1,/_description/d' | \
     sed -e '/_description/q' | sed -e '/_description/d'`"
   COMMENT=$(echo "$COMMENT" | while read line ; do echo -n "$line " ; done)

# webcollage treatment
## changed to create wrapper script
%if 0
   if [ "x$COMMAND" = "xwebcollage" ] ; then
      ARG="$ARG -directory %{_datadir}/backgrounds/images"
   fi
%endif

   if [ "x$NAME" = "x" ] ; then NAME=$COMMAND ; fi

   rm -f $2
   echo "[Desktop Entry]" >> $2
#   echo "Encoding=UTF-8" >> $2
   echo "Name=$NAME" >> $2
   echo "Comment=$COMMENT" >> $2
   echo "TryExec=$COMMAND" >> $2
   echo "Exec=$COMMAND $ARG" >> $2
   echo "StartupNotify=false" >> $2
   echo "Type=Application" >> $2
   echo "Categories=GNOME;Screensaver;" >> $2
}

cd $dd

SAVERDIR=%{_datadir}/applications/screensavers
mkdir -p ${RPM_BUILD_ROOT}${SAVERDIR}
echo "%%dir $SAVERDIR" >> base.files

for list in *extras.files ; do

   glist=gnome-$list
   rm -f $glist

   echo "%%defattr(-,root,root,-)" > $glist
##  move the owner of $SAVERDIR to -base
##   echo "%%dir $SAVERDIR" >> $glist

   set +x
   for xml in `cat $list | grep xml$` ; do
      file=${RPM_BUILD_ROOT}${xml}
      desktop=xscreensaver-`basename $file`
      desktop=${desktop%.xml}.desktop

      echo + create_desktop $file  ${RPM_BUILD_ROOT}${SAVERDIR}/$desktop
      create_desktop $file  ${RPM_BUILD_ROOT}${SAVERDIR}/$desktop
      echo ${SAVERDIR}/$desktop >> $glist
   done
   set -x
done

# Momonga totally destroys webcollage
%if 0
# Create wrapper script for webcollage to use nonet option
# by default, and rename the original webcollage
# (see bug 472061)
pushd ${RPM_BUILD_ROOT}%{_libexecdir}/%{name}
mv -f webcollage webcollage.original

cat > webcollage <<EOF
#!/bin/sh
PATH=%{_libexecdir}/%{name}:\$PATH
exec webcollage.original \\
	-directory %{_datadir}/backgrounds/images \\
	"\$@"
EOF
chmod 0755 webcollage
echo "%%{_libexecdir}/%%{name}/webcollage.original" >> \
	$dd/extras.files
%endif

# Make sure all files are readable by all, and writable only by owner.
#
chmod -R a+r,u+w,og-w ${RPM_BUILD_ROOT}

%clean
rm -rf ${RPM_BUILD_ROOT}

%if %{modular_conf}
%post base
%{_sbindir}/update-xscreensaver-hacks
exit 0
%endif

%files
%defattr(-,root,root,-)

%files -f base.files base
%defattr(-,root,root,-)

%files -f extras.files extras
%defattr(-,root,root,-)

%if %{modular_conf}
%files -f gl-base.files gl-base
%defattr(-,root,root,-)
%endif

%files -f gl-extras.files gl-extras
%defattr(-,root,root,-)

%files -f gnome-extras.files extras-gss
%defattr(-,root,root,-)

%files -f gnome-gl-extras.files gl-extras-gss
%defattr(-,root,root,-)

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.19-1m)
- update to 5.19 (sync with Fedora)

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.15-1m)
- update 5.15

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.11-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.11-1m)
- update to 5.11 based on Fedora 13 (1:5.11-7.respin1)

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.10-3m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.10-1m)
- update to 5.10

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.08-3m)
- rebuild against libjpeg-7

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.08-2m)
- add NotShowIn=GNOME;KDE;XFCE;

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.08-1m)
- update to 5.08
-- drop Source20 (ja.po.gz), merged
-- unapply Patch21, since webcollage is totally removed
-- import Patch50,51 from Fedora 11 (1:5.08-9)
- License: MIT

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.07-2m)
- rebuild against rpm-4.6

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.07-1m)
- update 5.07

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.05-2m)
- rebuild against gcc43

* Fri Mar 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.05-1m)
- update to 5.05
- add Source10
- add Patch3
- add Patch21
- delete Patch8
- delete Patch21
- update Patch22
- tmp comment out "BuildRequires:   fedora-release-notes" for default_text %%{_datadir}/doc/HTML/README-Accessibility
- tmp comment out "BuildRequires:   desktop-backgrounds-basic" for package extras

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.02-6m)
- rebuild against perl-5.10.0-1m

* Fri Aug  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.02-5m)
- remove webcollage again

* Wed Jul  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.02-4m)
- fix Provides and Obsoletes
- add some packages to BuildRequires
- get rid of tab

* Mon Jun 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (5.02-3m)
- unhold /usr/share/man/man6x

* Tue Jun  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.02-2m)
- modify %%files to avoid conflicting

* Mon Jun  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.02-1m)
- update to 5.02
- based on fedora
- [SECURITY] CVE-2007-1859

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.24-3m)
- remove category X-Red-Hat-Base AdvancedSettings Appearance Application

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.24-2m)
- delete duplicated dir

* Sun May  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.24-1m)
- version up
- based on fc-devel

* Sun Mar 26 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.18-6m)
- revise spec file for x86_64
- change stand_in_path value
- add configure option , --with-hackdir

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.18-5m)
- revise for xorg-7.0
- change install dir

* Fri Jul 1 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.18-4m)
- remove more screensavers just like rawhide

* Fri Jul 1 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.18-3m)
- remove webcollege

* Mon Apr 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.18-2m)
- import xscreensaver-4.18-vroot.patch from Fedora Core
 +* Fri Nov 26 2004 Than Ngo <than@redhat.com> 1:4.18-13
 +- add patch to fix vroot bug and make xscreensaver working in KDE again.

* Sun Oct  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.18-1m)
- minor bugfixes
- rebuild against gcc-c++-3.4.2

* Tue May 18 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.16-1m)
- update to 4.16
- enable starwars for test
- revise configure options

* Sun Apr 11 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.14-2m)
- disable starwars
  using i830/i865G with DRI, if starwars is started, a system will freeze.

* Sat Apr 10 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.14-1m)
- import from FC2

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.13-5m)
- rebuild against for libxml2-2.6.8

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (4.13-4m)
- revised spec for rpm 4.2.

* Mon Dec 08 2003 Kenta MURATA <muraken2@nifty.com>
- (4.13-3m)
- use gtk+-2.0.

* Mon Dec  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (4.13-2m)
- rebuild against perl-5.8.2-2m

* Mon Sep 22 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (4.13-1m)
- version 4.13

* Thu Jun 26 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.11-1m)
  update to 4.11

* Tue Jun  3 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.10-1m)
  update to 4.10

* Wed Mar 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.09-2m)
- rebuild against for XFree86-4.3.0

* Wed Mar 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.09-1m)
  update to 4.09

* Mon Nov 11 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.05-3m)
- add BuildPreReq: bc

* Fri Jun 14 2002 Tsutomu Yasuda <tom@kondara.org>
- (4.05-2k)
  update to 4.05

* Thu Jun  6 2002 Shingo Akagaki <dora@kondara.org>
- (4.01-8k)
- remove kondara scr

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (4.01-6k)
- add BuildPreReq: imlib-devel

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (4.01-4k)
- rebuild against libpng 1.2.2.

* Mon Mar 18 2002 Shingo Akagaki <dora@kondara.org>
- (4.01-2k)
- version 4.01

* Fri Oct 26 2001 Toru Hoshina <t@kondara.org>
- (3.34-4k)
- BuildPreReq XFree86-devel >= 4.1.0.

* Thu Oct 25 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.34-2k)
- update to 3.34

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (3.33-4k)
- rebuild against libpng 1.2.0.

* Fri Aug 24 2001 Shingo Akagaki <dora@kondara.org>
- (3.33-2k)
- mazema-ze from rawhide 3.33-3

* Fri Jun  8 2001 Akira Higuchi <a@kondara.org>
- (3.31-8k)
- eschew.patch

* Sat Jun  2 2001 Toru Hoshina <toru@df-usa.com>
- avoid freeze on alpha (;_;)

* Fri Jun  1 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- add yuuwaku

* Thu Mar 29 2001 Toru Hoshina <toru@df-usa.com>
- (3.31-2k)

* Mon Feb 19 2001 Bill Nottingham <notting@redhat.com>
- update to 3.29 (#27437)

* Tue Jan 23 2001 Bill Nottingham <notting@redhat.com>
- update to 3.27

* Fri Dec 01 2000 Bill Nottingham <notting@redhat.com>
- rebuild because of broken fileutils

* Fri Nov 10 2000 Bill Nottingham <notting@redhat.com>
- 3.26

* Fri Aug 11 2000 Jonathan Blandford <jrb@redhat.com>
- Up Epoch and release

* Wed Jul 26 2000 Bill Nottingham <notting@redhat.com>
- hey, vidmode works again

* Fri Jul 21 2000 Bill Nottingham <notting@redhat.com>
- update to 3.25

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jun 17 2000 Bill Nottingham <notting@redhat.com>
- xscreensaver.kss is not a %%config file.

* Sun Jun 11 2000 Bill Nottingham <notting@redhat.com>
- tweak kss module (#11872)

* Thu Jun  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- modify PAM configuration to use system-auth

* Thu May 18 2000 Preston Brown <pbrown@redhat.com>
- added Red Hat screensaver (waving flag has logo now).

* Fri May  5 2000 Bill Nottingham <notting@redhat.com>
- tweaks for ia64

* Mon Apr 10 2000 Bill Nottingham <notting@redhat.com>
- turn off xf86vidmode ext, so that binaries built against XFree86 4.0
  work on 3.x servers

* Wed Apr  5 2000 Bill Nottingham <notting@redhat.com>
- turn off gnome support for now

* Mon Apr  3 2000 Bill Nottingham <notting@redhat.com>
- update to 3.24

* Wed Feb 09 2000 Preston Brown <pbrown@redhat.com>
- wmconfig entry gone.

* Mon Jan 31 2000 Bill Nottingham <notting@redhat.com>
- update to 3.23

* Fri Jan 14 2000 Bill Nottingham <notting@redhat.com>
- rebuild to fix GL depdencies

* Tue Dec 14 1999 Bill Nottingham <notting@redhat.com>
- everyone in GL
- single package again

* Fri Dec 10 1999 Bill Nottingham <notting@redhat.com>
- update to 3.22
- turn off xf86vmode on alpha

* Tue Dec  7 1999 Bill Nottingham <notting@redhat.com>
- mmm... hardware accelerated GL on i386. :) :)

* Mon Nov 22 1999 Bill Nottingham <notting@redhat.com>
- 3.21
- use shm on alpha, let's see what breaks

* Tue Nov 16 1999 Bill Nottingham <notting@redhat.com>
- update to 3.20

* Wed Nov  3 1999 Bill Nottingham <notting@redhat.com>
- update to 3.19

* Thu Oct 14 1999 Bill Nottingham <notting@redhat.com>
- update to 3.18

* Sat Sep 25 1999 Bill Nottingham <notting@redhat.com>
- add a '-oneshot' single time lock option.

* Mon Sep 20 1999 Bill Nottingham <notting@redhat.com>
- take webcollage out of random list (for people who pay for bandwidth)

* Fri Sep 10 1999 Bill Nottingham <notting@redhat.com>
- patch webcollage to use xloadimage
- in the random list, run petri with -size 2 to save memory
- extend RPM silliness to man pages, too.

* Mon Jul 19 1999 Bill Nottingham <notting@redhat.com>
- update to 3.17
- add a little RPM silliness to package GL stuff if it's built

* Thu Jun 24 1999 Bill Nottingham <notting@redhat.com>
- update to 3.16

* Mon May 10 1999 Bill Nottingham <notting@redhat.com>
- update to 3.12

* Tue May  4 1999 Bill Nottingham <notting@redhat.com>
- remove security problem introduced earlier

* Wed Apr 28 1999 Bill Nottingham <notting@redhat.com>
- update to 3.10

* Thu Apr 15 1999 Bill Nottingham <notting@redhat.com>
- kill setuid the Right Way(tm)

* Mon Apr 12 1999 Bill Nottingham <notting@redhat.com>
- fix xflame on alpha

* Mon Apr 12 1999 Preston Brown <pbrown@redhat.com>
- upgrade to 3.09, fixes vmware interaction problems.

* Mon Apr  5 1999 Bill Nottingham <notting@redhat.com>
- remove setuid bit. Really. I mean it.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Fri Mar 19 1999 Bill Nottingham <notting@redhat.com>
- kill setuid, since pam works OK

* Tue Mar 16 1999 Bill Nottingham <notting@redhat.com>
- update to 3.08

* Wed Feb 24 1999 Bill Nottingham <notting@redhat.com>
- wmconfig returns, and no one is safe...

* Tue Feb 23 1999 Bill Nottingham <notting@redhat.com>
- remove bsod from random list because it's confusing people???? *sigh*

* Tue Jan 12 1999 Cristian Gafton <gafton@redhat.com>
- call libtoolize to get it to compile cleanely on the arm

* Tue Jan  5 1999 Bill Nottingham <notting@redhat.com>
- update to 3.07

* Mon Nov 23 1998 Bill Nottingham <notting@redhat.com>
- update to 3.06

* Tue Nov 17 1998 Bill Nottingham <notting@redhat.com>
- update to 3.04

* Thu Nov 12 1998 Bill Nottingham <notting@redhat.com>
- update to 3.02
- PAMify

* Tue Oct 13 1998 Cristian Gafton <gafton@redhat.com>
- take out Noseguy module b/c of possible TMv
- install modules in /usr/X11R6/lib/xscreensaver
- don't compile support for xshm on the alpha
- properly buildrooted
- updated to version 2.34

* Fri Aug  7 1998 Bill Nottingham <notting@redhat.com>
- update to 2.27

* Wed Jun 10 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Mon Jun 08 1998 Erik Troan <ewt@redhat.com>
- added fix for argv0 buffer overflow

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat Apr 11 1998 Donnie Barnes <djb@redhat.com>
- updated from 2.10 to 2.16
- added buildroot

* Wed Oct 25 1997 Marc Ewing <marc@redhat.com>
- wmconfig

* Thu Oct 23 1997 Marc Ewing <marc@redhat.com>
- new version, configure

* Fri Aug 22 1997 Erik Troan <ewt@redhat.com>
- built against glibc
