%global momorel 4

%define fontname paktype-naqsh
%define fontconf 67-paktype
%define fontdir %{_datadir}/fonts/%{fontname}

Name:	%{fontname}-fonts
Version:     3.0
Release:     %{momorel}m%{?dist}
Summary:     Fonts for Arabic from PakType

Group:	User Interface/X
License:     "GPLv2 with exceptions"
URL:	https://sourceforge.net/projects/paktype/
Source0:     http://downloads.sourceforge.net/project/paktype/Naqsh-3.0.tar.gz
Source1:	%{fontconf}-naqsh.conf
BuildArch:   noarch
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	fontpackages-devel
Requires:   fontpackages-filesystem
Obsoletes: paktype-fonts

%description 
The paktype-naqsh-fonts package contains fonts for the display of \
Arabic from the PakType by Lateef Sagar.

%prep
%setup -q -c
rm -rf Naqsh-3.0/Project\ files/
# get rid of the white space (' ')
mv Naqsh-3.0/Ready*/PakType\ Naqsh.ttf PakType_Naqsh.ttf
mv Naqsh-3.0/License\ files/PakType\ Naqsh\ License.txt PakType_Naqsh_License.txt

%{__sed} -i 's/\r//' PakType_Naqsh_License.txt

for txt in Naqsh-3.0/Readme.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\x92//g' $txt.new
   sed -i 's/\x93//g' $txt.new
   sed -i 's/\x94//g' $txt.new
   sed -i 's/\x96//g' $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done

%build
echo "Nothing to do in Build."

%install
rm -rf $RPM_BUILD_ROOT
install -m 0755 -d $RPM_BUILD_ROOT%{_fontdir}
install -m 0644 -p PakType_Naqsh.ttf $RPM_BUILD_ROOT%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
		%{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
	%{buildroot}%{_fontconfig_templatedir}/%{fontconf}-naqsh.conf

ln -s %{_fontconfig_templatedir}/%{fontconf}-naqsh.conf \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}-naqsh.conf

%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg -f %{fontconf}-naqsh.conf PakType_Naqsh.ttf

%doc PakType_Naqsh_License.txt Naqsh-3.0/Readme.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-1m)
- import from Fedora 13
- Obsoletes: paktype-fonts

* Tue May 10 2010 Pravin Satpute <psatpute@redhat.com> - 3.0-3
- improved .conf file, bug 586785

* Thu Mar 04 2010 Pravin Satpute <psatpute@redhat.com> - 3.0-2
- upstream new release with license fix, rh bug 567302
- added .conf file as well

* Fri Feb 05 2010 Pravin Satpute <psatpute@redhat.com> - 3.0-1
- Initial build
- Split from paktype fonts
