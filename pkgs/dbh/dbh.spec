%global momorel 13

%global xfcever 4.8
%global xfce4ver 4.8.0

Name:		dbh
Version: 	1.0.24
Release:	%{momorel}m%{?dist}
Summary:        Disk based hash library 

Group: 		Applications/Databases
License:	QPL
URL:            http://dbh.sourceforge.net/
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource:       0
Patch0: 	dbh-1.0.22-rpath.patch
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description 
Disk based hashes is a method to create multidimensional binary trees on disk.
This library permits the extension of database concept to a plethora of 
electronic data, such as graphic information. With the multidimensional binary 
tree it is possible to mathematically prove that access time to any 
particular record is minimized (using the concept of critical points from 
calculus), which provides the means to construct optimized databases for 
particular applications.  

%package devel
Summary: Header files for disk based hash library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package includes the static libraries and header files you will need
to compile applications for dbh.

%prep
%setup -q
%patch0 -p1 -b .rpath

%build
export POSIXLY_CORRECT=1
%configure --disable-rpath --disable-static
%make

%install
rm -rf %{buildroot}
#make install DESTDIR=$RPM_BUILD_ROOT 
%makeinstall

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%doc examples/*.c examples/Makefile* doc/*.html
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.24-13m)
- change Source0 URI again

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.24-12m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.24-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.24-10m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.24-9m)
- fix build failure by adding "export POSIXLY_CORRECT=1"

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.24-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.24-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.24-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.24-5m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.24-4m)
- rebuild against gcc43

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.24-3m)
- separate devel pakcage
- add Patch0: dbh-1.0.22-rpath.patch

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.24-2m)
- delete duplicated dir

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.24-1m)
- update to 1.0.24, Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.22-1m)
- update to 1.0.22

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.20-2m)
- enable x86_64.

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.20-1m)

* Tue Apr 20 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.18-2m)
- tar ball seems to have been corrected so autogen.sh is included.
- updated spec file accordingly.

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.18-1m)
- version update to 1.0.18
- autogen.sh is missing from the tar ball and copied from 1.0-17 tar ball.

* Thu Sep 11 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.17-1m)
- version 1.0-17

* Sun Mar 30 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.14-0.0.1m)
- version 1.0-14

* Sat Mar 1 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.11-0.0.1m)
- build for Momonga Linux
- modified src.rpm at project homepage
