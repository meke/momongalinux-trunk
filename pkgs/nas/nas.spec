%global momorel 1
%global daemon nasd
%define libdir_x11 %{_libdir}/X11

Summary: The Network Audio System (NAS)
Name: nas	
Version: 1.9.4
Release: %{momorel}m%{?dist}
License:  "Public Domain"
Group: Development/Libraries
URL: http://nas.codebrilliance.com
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.src.tar.gz
NoSource: 0
Source1: %{daemon}.service
Source2: %{daemon}.sysconfig
Patch0: %{name}-1.9.3-Move-AuErrorDB-to-SHAREDIR.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post):  systemd-sysv systemd-units
Requires(preun): systemd-units
Requires(postun):systemd-units
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: bison
BuildRequires: flex
BuildRequires: imake
BuildRequires: libXaw-devel
BuildRequires: libXext-devel
BuildRequires: libXp-devel
BuildRequires: libXpm-devel
BuildRequires: libXt-devel
BuildRequires: sed
BuildRequires: systemd-units


%package libs
Summary: Runtime libraries for NAS
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of NAS.

%package devel
Summary: Development and doc files for the NAS
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description
In a nutshell, NAS is the audio equivalent of an X display  server.
The Network Audio System (NAS) was developed by NCD for playing,
recording, and manipulating audio data over a network.  Like the
X Window System, it uses the client/server model to separate
applications from the specific drivers that control audio input
and output devices.
Key features of the Network Audio System include:
	o  Device-independent audio over the network
	o  Lots of audio file and data formats
	o  Can store sounds in server for rapid replay
	o  Extensive mixing, separating, and manipulation of audio data
	o  Simultaneous use of audio devices by multiple applications
	o  Use by a growing number of ISVs
	o  Small size
	o  Free!  No obnoxious licensing terms

%description devel
Development files and the documentation

%prep
%setup -q

%patch0 -p1 -b .move_AuErrorDB

%build
xmkmf

find . -name Makefile \
| xargs sed -i -e 's/^\(\s*CDEBUGFLAGS\s*=.*\)/\1 $(RPM_OPT_FLAGS)/'

make %{?_smp_mflags} World

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make DESTDIR=%{buildroot} \
	BINDIR=%{_bindir} \
	INCROOT=%{_includedir} \
	LIBDIR=%{libdir_x11} \
	SHLIBDIR=%{_libdir} \
	USRLIBDIR=%{_libdir} \
	MANPATH=%{_mandir} \
	install install.man

install -p -m644 -D %{SOURCE1} %{buildroot}%{_unitdir}/%{daemon}.service
install -p -m644 -D %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/%{daemon}

# remove satic lib
rm %{buildroot}%{_libdir}/*.a

# rename cofigfile
mv %{buildroot}%{_sysconfdir}/%{name}/nasd.conf{.eg,}

%post
if [ $1 -eq 1 ] ; then 
  # Initial installation 
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable %{daemon}.service > /dev/null 2>&1 || :
  /bin/systemctl stop %{daemon}.service > /dev/null 2>&1 || :
fi      
        
%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart %{daemon}.service >/dev/null 2>&1 || :
fi

%triggerun -- %{name} < 1.9.3-1m
echo '%{name}: User must migrate to systemd target manually by runnig:'
echo '  systemd-sysv-convert --apply %{daemon}'
# Save the current service runlevel info
/usr/bin/systemd-sysv-convert --save %{daemon} >/dev/null 2>&1 ||:
# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del %{daemon} >/dev/null 2>&1 || :
/bin/systemctl try-restart %{daemon}.service >/dev/null 2>&1 || :

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc BUILDNOTES FAQ HISTORY README* RELEASE TODO
%dir /etc/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/nasd.conf
%config(noreplace) %{_sysconfdir}/sysconfig/%{daemon}
%{_unitdir}/%{daemon}.service
%{_bindir}/*
%{_mandir}/man1/*.1x*
%{_mandir}/man5/nasd.conf.5x*

%files libs
%defattr(-,root,root)
%{_libdir}/libaudio.so.*
%{_datadir}/X11/AuErrorDB

%files devel
%defattr(-,root,root)
%{_includedir}/audio
%{_libdir}/libaudio.so
%{_mandir}/man3/*.3x*

%changelog
* Mon Dec 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.4-1m)
- update 1.9.4

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-1m)
- update 1.9.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.1-6m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.1-5m)
- split package libs

* Sun May  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.1-4m)
- fix build with new gcc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.1-1m)
- update to 1.9.1
- NoSource

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-3m)
- rebuild against gcc43
 
* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-1m)
- import from f7 to Momonga

* Sun Apr 08 2007 Frank Buttner  <frank-buettner@gmx.net> - 1.9-1
- update to 1.9
- remove old patch file

* Mon Mar 26 2007 Frank Buttner  <frank-buettner@gmx.net> - 1.8b-1
- update to 1.8b

* Thu Mar 22 2007 Frank Buttner  <frank-buettner@gmx.net> - 1.8a-2
- use the SVN version of 1.8a

* Wed Mar 21 2007 Frank Buttner  <frank-buettner@gmx.net> - 1.8a-1
- fix bug 233353 

* Thu Feb 09 2007 Frank Buttner  <frank-buettner@gmx.net> - 1.8-13
- use the corrected patch

* Thu Feb 08 2007 Frank Buttner  <frank-buettner@gmx.net> - 1.8-11
- fix bug 227759

* Tue Sep 19 2006 Rex Dieter <rexdieter[AT]users.sf.net> - 1.8-10
- don't rely-on/use potentially broken %%_libdir/X11 symlink (#207180)

* Mon Sep 11 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-9
- second rebuild for FC6

* Mon Jul 24 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-8
- fix ugly output when starting the daemon

* Fri Jul 21 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-7
- disable build for EMT64 on FC4

* Thu Jul 13 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-6
- fix build on EMT64 

* Wed Jul 12 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-5
- fix include dir

* Fri Jul 7 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-4
- add Requires(preun): chkconfig /sbin/service
- add Requires(post):  chkconfig
- add remarks for FC4

* Fri Jul 7 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-3
- move man3 to devel
- rename nasd.conf.eg to .conf
- add build depend for libXext-devel libXt-devel
- change license to Public Domain
- add path to make intall
- add rc.d/sysconfig  files 

* Fri Jul 7 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-2
- move libaudio.so.2 to main package
- switch package name from NAS to nas
- fix depend for devel package
- fix version
- add nas subdir in etc to main package
- set license to Distributable
- add readme file

* Fri Jul 7 2006 Frank Buttner  <frank-buettner@gmx.net> - 1.8-1
- start
