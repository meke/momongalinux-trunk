%global momorel 3

Summary: A C library for the arithmetic of complex numbers
Name: libmpc

### include local configuration
%{?include_specopt}
# If you'd like to change these configurations, please copy them to
# ${HOME}/rpmbuild/specopt/libmpc.specopt and edit it.
%{?!do_test:            %global do_test 1}

Version: 0.8.2
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: GPL
URL: http://www.multiprecision.org/
Source0: http://www.multiprecision.org/mpc/download/mpc-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gmp-devel >= 5.0 , mpfr-devel >= 3.0

%description 
Mpc is a C library for the arithmetic of complex numbers
with arbitrarily high precision and correct rounding of the result. It
is built upon and follows the same principles as Mpfr.

%package devel
Summary: Development files for libmpc
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The header-files and static libraries for libmpc

%prep
%setup -q -n mpc-%{version}

%build
%configure
%make

%check
%if %{do_test}
make check
%endif

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

rm -f %{buildroot}/%{_infodir}/dir

# get rid of la file
find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post devel
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/mpc.info

%preun devel
if [ $1 = 0 ]; then
    /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/mpc.info
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING.LIB ChangeLog INSTALL NEWS README TODO
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/mpc.h
%{_libdir}/lib*.a
%{_libdir}/lib*.so
%{_infodir}/mpc.info*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- update 0.8.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-2m)
- full rebuild for mo7 release

* Mon Jan 11 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1 (bug fix release)

* Wed Dec  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-2m)
- release %%{_includedir}, it's provided by filesystem

* Fri Nov 27 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-1m)
- Initial import
