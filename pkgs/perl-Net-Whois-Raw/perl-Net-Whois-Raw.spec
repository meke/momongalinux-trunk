%global         momorel 1

Name:           perl-Net-Whois-Raw
Version:        2.70
Release:        %{momorel}m%{?dist}
Summary:        Get Whois information for domains
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-Whois-Raw/
Source0:        http://www.cpan.org/authors/id/N/NA/NALOBIN/Net-Whois-Raw-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008001
BuildRequires:  perl-Encode
BuildRequires:  perl-File-Temp
BuildRequires:  perl-Getopt-Long >= 2
BuildRequires:  perl-HTTP-Message
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Net-IDN-Encode >= 1
BuildRequires:  perl-Regexp-IPv6
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-URI
Requires:       perl-Encode
Requires:       perl-File-Temp
Requires:       perl-Getopt-Long >= 2
Requires:       perl-HTTP-Message
Requires:       perl-libwww-perl
Requires:       perl-Net-IDN-Encode >= 1
Requires:       perl-Regexp-IPv6
Requires:       perl-Test-Simple
Requires:       perl-URI
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Net::Whois::Raw queries WHOIS servers about domains. The module supports
recursive WHOIS queries. Also queries via HTTP is supported for some TLDs.

%prep
%setup -q -n Net-Whois-Raw-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYRIGHT pwhois README
%{_bindir}/pwhois
%{perl_vendorlib}/Net/Whois/Raw*
%{_mandir}/man1/pwhois.1*
%{_mandir}/man3/Net::Whois::Raw.3pm**

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.70-1m)
- rebuild against perl-5.20.0
- update to 2.70

* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.63-1m)
- update to 2.63

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.60-1m)
- update to 2.60

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.59-1m)
- update to 2.59

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.57-1m)
- update to 2.57

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.51-1m)
- update to 2.51
- rebuild against perl-5.18.2

* Sat Sep 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.48-1m)
- update to 2.48

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.46-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.46-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.46-2m)
- rebuild against perl-5.16.3

* Tue Feb 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.46-1m)
- update to 2.46

* Thu Jan 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.45-1m)
- update to 2.45

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.43-2m)
- rebuild against perl-5.16.2

* Tue Aug 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.43-1m)
- update to 2.43

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-2m)
- rebuild against perl-5.16.1

* Wed Jul 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.42-1m)
- update to 2.42

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-2m)
- rebuild against perl-5.16.0

* Fri Mar 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-1m)
- update to 2.41

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.40-1m)
- update to 2.40

* Tue Dec 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.39-1m)
- update to 2.39

* Fri Nov 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.37-1m)
- update to 2.37

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.33-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.33-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.33-1m)
- update to 2.33

* Thu May 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32-1m)
- update to 2.32

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31-2m)
- rebuild for new GCC 4.6

* Mon Feb 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-1m)
- update to 2.31

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28-1m)
- update to 2.28

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.27-2m)
- rebuild for new GCC 4.5

* Fri Nov 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.27-1m)
- update to 2.27

* Fri Oct  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.26-1m)
- update to 2.26

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25-1m)
- update to 2.25

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.22-5m)
- full rebuild for mo7 release

* Tue Aug 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22-4m)
- get.tj is not reliable

* Mon Aug 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-3m)
- control execution of test by specopt

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-2m)
- rebuild against perl-5.12.1

* Mon Apr 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-1m)
- update to 2.22

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-1m)
- update to 2.20

* Fri Mar 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-1m)
- update to 2.18

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17-1m)
- update to 2.17

* Sat Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-1m)
- update to 2.14

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-1m)
- update to 2.13
- enable test again

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.06-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Nov  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.06-2m)
- disable %%check

* Sun Nov 08 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.06-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
