%global momorel 1

Summary:	Small, fast daemon to serve DNSBLs
Name:		rbldnsd
Version:	0.997a
Release:	%{momorel}m%{?dist}
License:	GPLv2+
Group:		System Environment/Daemons
URL:		http://www.corpit.ru/mjt/rbldnsd.html
Source0:	http://www.corpit.ru/mjt/rbldnsd/rbldnsd-%{version}.tar.gz
NoSource:   0
Source1:	rbldnsd.init
Source2:	rbldnsd.conf
Source3:	rbldnsctl
Source4:	README.systemd
Patch0:		rbldnsd-0.997a-format.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	gawk, zlib-devel

Requires(pre):	shadow-utils
BuildRequires:	systemd-units
Requires(pre):	systemd-sysv, /sbin/chkconfig
Requires(post):	systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
Rbldnsd is a small, authoritative-only DNS nameserver designed to serve
DNS-based blocklists (DNSBLs). It may handle IP-based and name-based
blocklists.

%prep
%setup -q -n %{name}-%{version}
# Use format string in dslog() invocation
%patch0

sed -i	-e 's@/var/lib/rbldns\([/ ]\)@%{_localstatedir}/lib/rbldnsd\1@g' \
	-e 's@\(-r/[a-z/]*\) -b@\1 -q -b@g' debian/rbldnsd.default
cp -p %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4} ./

%build
# this is not an autotools-generated configure script, and does not support --libdir
CFLAGS="%{optflags}" \
LDFLAGS="%{?__global_ldflags}" \
./configure
make

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}{%{_sbindir},%{_mandir}/man8,%{_sysconfdir}/sysconfig}
mkdir -p %{buildroot}{/etc/systemd,%{_localstatedir}/lib/rbldnsd}
install -p -m 755 rbldnsd			%{buildroot}%{_sbindir}/
install -p -m 644 rbldnsd.8			%{buildroot}%{_mandir}/man8/
install -p -m 644 debian/rbldnsd.default	%{buildroot}%{_sysconfdir}/sysconfig/rbldnsd
install -p -m 644 rbldnsd.conf			%{buildroot}/etc/systemd/
install -p -m 755 rbldnsctl			%{buildroot}%{_sbindir}/

%clean
rm -rf %{buildroot}

%pre
/usr/bin/getent group rbldns >/dev/null || /usr/sbin/groupadd -r rbldns
/usr/bin/getent passwd rbldns >/dev/null || \
	/usr/sbin/useradd -r -g rbldns -d %{_localstatedir}/lib/rbldnsd \
		-s /sbin/nologin -c "rbldns daemon" rbldns
# SysV-to-systemd migration
if [ $1 -gt 1 -a ! -e /etc/systemd/rbldnsd.conf -a -e %{_initscriptdir}/rbldnsd ]; then
	/usr/bin/systemd-sysv-convert --save rbldnsd &>/dev/null || :
	/sbin/chkconfig --del rbldnsd &>/dev/null || :
fi
exit 0

%post
/bin/systemctl daemon-reload &>/dev/null || :

%preun
if [ $1 -eq 0 ]; then
	# Package removal, not upgrade
	%{_sbindir}/rbldnsctl stop &>/dev/null || :
	%{_sbindir}/rbldnsctl disable &>/dev/null || :
fi

%postun
/bin/systemctl daemon-reload &>/dev/null || :

%files
%defattr (-,root,root,-)
%doc README.user NEWS TODO debian/changelog CHANGES-0.81
%{_sbindir}/rbldnsd
%{_mandir}/man8/rbldnsd.8*
%dir %{_localstatedir}/lib/rbldnsd/
%config(noreplace) %{_sysconfdir}/sysconfig/rbldnsd
%doc README.systemd
%config(noreplace) %{_sysconfdir}/systemd/rbldnsd.conf
%{_sbindir}/rbldnsctl

%changelog
* Sun Mar 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.997a-1m)
- update 0.997a
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.996b-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.996b-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.996b-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.996b-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.996b-2m)
- rebuild against rpm-4.6

* Thu May  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.996b-1m)
- import from Fedora to Momonga
- use %%_initscriptdir

* Mon Mar 31 2008 Paul Howarth <paul@city-fan.org> 0.996b-1
- update to 0.996b
- _GNU_SOURCE no longer needed

* Wed Feb 20 2008 Paul Howarth <paul@city-fan.org> 0.996a-6
- fix exit codes for reload, stop, and try-restart actions of initscript

* Wed Feb 13 2008 Paul Howarth <paul@city-fan.org> 0.996a-5
- define _GNU_SOURCE for NI_MAXHOST symbol visibility
- LSB-ize initscript (#247043)

* Thu Aug 23 2007 Paul Howarth <paul@city-fan.org> 0.996a-4
- add buildreq gawk

* Thu Aug 23 2007 Paul Howarth <paul@city-fan.org> 0.996a-3
- upstream released a new version without changing the version number (the
  only changes are in debian/control and debian/changelog, neither of which
  are used in the RPM package)
- unexpand tabs in spec
- use the standard scriptlet for user/group creation in %%pre
- drop scriptlet dependencies on /sbin/service by calling initscript directly
- clarify license as GPL version 2 or later

* Wed Aug 30 2006 Paul Howarth <paul@city-fan.org> 0.996a-2
- FE6 mass rebuild

* Fri Jul 28 2006 Paul Howarth <paul@city-fan.org> 0.996a-1
- update to 0.996a

* Tue Feb 21 2006 Paul Howarth <paul@city-fan.org> 0.996-1
- update to 0.996
- use /usr/sbin/useradd instead of %%{_sbindir}/useradd
- add buildreq zlib-devel to support gzipped zone files

* Wed Feb 15 2006 Paul Howarth <paul@city-fan.org> 0.995-5
- license text not included in upstream tarball, so don't include it

* Tue Jun 28 2005 Paul Howarth <paul@city-fan.org> 0.995-4
- include gpl.txt as %%doc

* Mon Jun 27 2005 Paul Howarth <paul@city-fan.org> 0.995-3
- fix /etc/sysconfig/rbldnsd references to /var/lib/rbldns to point to
  %%{_localstatedir}/lib/rbldnsd instead
- don't enable daemons in any runlevel by default
- add -q option to sample entries in /etc/sysconfig/rbldnsd

* Fri Jun 17 2005 Paul Howarth <paul@city-fan.org> 0.995-2
- first Fedora Extras build, largely based on upstream spec file
