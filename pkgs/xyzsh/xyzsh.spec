%global         momorel 1
%global         repoid 58477

Name:           xyzsh
Version:        1.4.2
Release:        %{momorel}m%{?dist}
Summary:        "xyzsh" shell script language
Group:          System Environment/Shells
License:        MIT
URL:            http://ab25cq.sakura.ne.jp/
Source0:        http://dl.sourceforge.jp/%{name}/%{repoid}/%{name}-%{version}.tgz
NoSource:       0
Requires:       %{name}-libs >= %{version}
BuildRequires:  cmigemo-devel
BuildRequires:  ncurses-devel
BuildRequires:  oniguruma-devel
BuildRequires:  readline-devel

%description
a shell script language under linux, OSX, cygwin, and FreeBSD.

%package        libs
Summary:        Runtime libraries for %{name}
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description    libs
%{summary}.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains header files for
developing applications that use %{name}.

%prep
%setup -q

# Embed soname anyway
SOVER=$(cat configure.in | sed -n -e 's|^SO_VERSION=\([^\.][^\.]*\)\..*$|\1|p')
sed -i.soname \
	-e "/[ \t]/s|\( -o libxyzsh\.so\)| -Wl,-soname,libxyzsh.so.$SOVER \1|" \
	Makefile.in

# Don't strip binary
sed -i.strip -e '/INSTALL/s|-s -m |-m |' Makefile.in

# CRLF line terminators
touch -r README{,.stamp}
sed -i -e 's|\r||g' README
touch -r README{.stamp,}
rm -f README.stamp

# Change docdir
sed -i.docdir \
	-e '/^CFLAGS=.*DATAROOTDIR=/s|doc/xyzsh/|doc/xyzsh-%{version}/|' \
	configure

# Kill -O3
sed -i.optflags \
	-e 's|-O3|-O2|' \
	configure

%build
%configure \
    --with-migemo \
    --with-system-migemodir=%{_datadir}/cmigemo/

make %{?_smp_mflags} -k \
    CC="gcc %optflags" \
    docdir=%{_datadir}/doc/%{name}-%{version}

%install
make install \
    DESTDIR=%{buildroot} \
    INSTALL="install -p" \
    docdir=%{_datadir}/doc/%{name}-%{version}
    
%clean
rm -rf %{buildroot}

%post	-p /sbin/ldconfig
%postun	-p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc CHANGELOG README USAGE*
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/*.xyzsh
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_mandir}/man1/*.1*

%files  libs
%defattr(-,root,root,-)
%{_libdir}/lib*.so.*

%files	devel
%defattr(-,root,root,-)
%{_includedir}/%{name}
%{_libdir}/lib*.so

%changelog
* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sun Feb 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8

* Sat Jan 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.9-1m)
- initial build for Momonga Linux
