%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global xscreensaverver 5.19
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global strigiver 0.7.8

#define include_crystalsvg 0
%define kdeclassic 1
%define nuvola 1
%define mono 1

Name: kdeartwork
Summary: Additional artwork (themes, sound themes, etc.) for KDE
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPLv2
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: kdeartwork-4.6.95-kdeclassic.patch
Patch1: kdeartwork-4.3.75-template.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: kdelibs >= %{version}
Requires: kde-workspace >= %{version}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  kdelibs-devel >= %{version}
BuildRequires:  kde-workspace-devel >= %{version}
BuildRequires:  eigen2-devel
BuildRequires:  libkexiv2-devel >= %{version}
BuildRequires:  strigi-devel >= %{strigiver}

Obsoletes:     kdeartwork-sounds < %{version}

%if 0%{!?kdeclassic}
Obsoletes:     kdeclassic-icon-theme < %{version}
Provides:      kdeclassic-icon-theme = %{version}-%{release}
%endif

%description
Additional artwork (themes, sound themes, screensavers ...) for KDE.

%package -n kde-style-phase
Summary: Classic GUI style for KDE
Group:   User Interface/Desktops

%description -n kde-style-phase
%{summary}.

%if 0%{?include_crystalsvg}
%package -n crystalsvg-icon-theme
Summary: The crystalsvg icon theme
Group: User Interface/Desktops
Obsoletes: %{name}-icons-crystalsvg < %{version}-%{release}
Provides:  %{name}-icons-crystalsvg = %{version}-%{release}
BuildArch: noarch

%description -n crystalsvg-icon-theme
This package contains the crystalsvg icon theme.
It was the standard theme in KDE3 and is needed for KDE3 applications.
%endif

%if 0%{?kdeclassic}
%package -n kdeclassic-icon-theme
Summary: KDE classic icon theme
Group: User Interface/Desktops
Obsoletes: %{name}-icons < %{version}-%{release}
Provides:  %{name}-icons = %{version}-%{release}
BuildArch: noarch

%description -n kdeclassic-icon-theme
%{summary}.
%endif

%if 0%{?nuvola}
%package -n nuvola-icon-theme
Summary: KDE nuvola icon theme
Group: User Interface/Desktops
BuildArch: noarch

%description -n nuvola-icon-theme
%{summary}.
%endif

%package screensavers
Summary: Extra screensavers for KDE
Group: Amusements/Graphics
Obsoletes: %{name}-extras < %{version}-%{release}
Provides:  %{name}-extras = %{version}-%{release}
BuildRequires: xscreensaver-extras xscreensaver-gl-extras
Requires:      xscreensaver-extras xscreensaver-gl-extras
Requires: %{name}-kxs = %{version}-%{release}

%description screensavers
%{summary}.

%package kxs
Summary: Support for xscreensaver-based screensavers
Group: Amusements/Graphics
Requires: kde-workspace >= %{version}
Requires: xscreensaver-base

%description kxs
%{summary}.

%package wallpapers
Summary: Additional wallpapers
Group: Amusements/Graphics
# for directory ownership
Requires: homura-backgrounds-kde >= 8.0.0-1m
BuildArch: noarch

%description wallpapers
%{summary}.

%if 0%{?mono}
%package -n mono-icon-theme
Summary: KDE monochrome icon theme
Group:   User Interface/Desktops
BuildArch: noarch

%description -n mono-icon-theme
%{summary}.
%endif

%prep
%setup -q -n %{name}-%{version}
%if 0%{?kdeclassic}
%patch0 -p1 -b .classic
%endif
%patch1 -p1 -b .template

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

%if ! 0%{?include_crystalsvg}
rm -rf %{buildroot}%{_kde4_iconsdir}/crystalsvg/
%endif

%if 0%{!?kdeclassic}
rm -rf %{buildroot}%{_kde4_iconsdir}/kdeclassic/
%endif
%if 0%{!?nuvola}
rm -rf %{buildroot}%{_kde4_iconsdir}/nuvola/
%endif
%if 0%{!?mono}
rm -rf %{buildroot}%{_kde4_iconsdir}/mono/
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%if 0%{?kdeclassic}
%post -n kdeclassic-icon-theme
touch --no-create %{_kde4_iconsdir}/kdeclassic &> /dev/null || :

%posttrans -n kdeclassic-icon-theme
gtk-update-icon-cache %{_kde4_iconsdir}/kdeclassic &> /dev/null || :

%postun -n kdeclassic-icon-theme
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/kdeclassic &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/kdeclassic &> /dev/null || :
fi
%endif

%if 0%{?include_crystalsvg}
%post -n crystalsvg-icon-theme
touch --no-create %{_kde4_iconsdir}/crystalsvg &> /dev/null || :

%posttrans -n crystalsvg-icon-theme
gtk-update-icon-cache %{_kde4_iconsdir}/crystalsvg &> /dev/null || :

%postun -n crystalsvg-icon-theme
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/crystalsvg &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/crystalsvg &> /dev/null || :
fi
%endif

%if 0%{?nuvola}
%post -n nuvola-icon-theme
touch --no-create %{_kde4_iconsdir}/nuvola &> /dev/null || :

%posttrans -n nuvola-icon-theme
gtk-update-icon-cache %{_kde4_iconsdir}/nuvola &> /dev/null || :

%postun -n nuvola-icon-theme
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/nuvola &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/nuvola &> /dev/null || :
fi
%endif

%if %{?mono}
%post -n mono-icon-theme
touch --no-create %{_kde4_iconsdir}/mono &> /dev/null || :

%posttrans -n mono-icon-theme
gtk-update-icon-cache %{_kde4_iconsdir}/mono-icon-theme &> /dev/null || :

%postun -n mono-icon-theme
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/mono &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/mono &> /dev/null || :
fi
%endif

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_kde4_appsdir}/color-schemes
%{_kde4_appsdir}/desktoptheme/Androbit
%{_kde4_appsdir}/desktoptheme/Aya
%{_kde4_appsdir}/desktoptheme/Produkt
%{_kde4_appsdir}/desktoptheme/slim-glow
%{_kde4_appsdir}/desktoptheme/Tibanna
%{_kde4_appsdir}/kwin/*
%{_kde4_libdir}/kde4/*.so
%exclude %{_kde4_libdir}/kde4/kstyle_phase_config.so
%{_kde4_datadir}/emoticons/*

%files -n kde-style-phase
%defattr(-,root,root,-)
%{_kde4_appsdir}/kstyle/themes/phase.themerc
%{_kde4_libdir}/kde4/kstyle_phase_config.so
%{_kde4_libdir}/kde4/plugins/styles/phasestyle.so

%if 0%{?include_crystalsvg}
%files -n crystalsvg-icon-theme
%defattr(-,root,root,-)
%{_kde4_iconsdir}/crystalsvg
%endif

%if 0%{?kdeclassic}
%files -n kdeclassic-icon-theme
%defattr(-,root,root,-)
%{_kde4_iconsdir}/kdeclassic
%endif

%if 0%{?nuvola}
%files -n nuvola-icon-theme
%defattr(-,root,root,-)
%{_kde4_iconsdir}/nuvola
%endif

%files kxs
%defattr(-,root,root,-)
%{_kde4_bindir}/kxsconfig
%{_kde4_bindir}/kxsrun

%files screensavers
%defattr(-,root,root,-)
%{_kde4_appsdir}/kfiresaver
%{_kde4_appsdir}/kscreensaver
%{_kde4_bindir}/*.kss
%{_kde4_datadir}/kde4/services/ScreenSavers/*

%files wallpapers
%defattr(-,root,root,-)
%{_kde4_datadir}/wallpapers/Aghi
%{_kde4_datadir}/wallpapers/Air
%{_kde4_datadir}/wallpapers/Atra_Dot
%{_kde4_datadir}/wallpapers/Autumn/contents/images/*
%{_kde4_datadir}/wallpapers/Beach_Reflecting_Clouds
%{_kde4_datadir}/wallpapers/Blue_Curl
%{_kde4_datadir}/wallpapers/Blue_Wood/contents/images/*
%{_kde4_datadir}/wallpapers/Chess
%{_kde4_datadir}/wallpapers/City_at_Night
%{_kde4_datadir}/wallpapers/Code_Poets_Dream
%{_kde4_datadir}/wallpapers/Colorado_Farm
%{_kde4_datadir}/wallpapers/Curls_on_Green
%{_kde4_datadir}/wallpapers/Damselfly
%{_kde4_datadir}/wallpapers/EOS
%{_kde4_datadir}/wallpapers/Emotion
%{_kde4_datadir}/wallpapers/Ethais
%{_kde4_datadir}/wallpapers/Evening
%{_kde4_datadir}/wallpapers/Field
%{_kde4_datadir}/wallpapers/Fields_of_Peace
%{_kde4_datadir}/wallpapers/Finally_Summer_in_Germany
%{_kde4_datadir}/wallpapers/Flower_drops
%{_kde4_datadir}/wallpapers/Fresh_Morning
%{_kde4_datadir}/wallpapers/Golden_Ripples
%{_kde4_datadir}/wallpapers/Grass/contents/images/*
%{_kde4_datadir}/wallpapers/Green_Concentration
%{_kde4_datadir}/wallpapers/Hanami/contents/images/*
%{_kde4_datadir}/wallpapers/HighTide
%{_kde4_datadir}/wallpapers/Holiday_Cactus
%{_kde4_datadir}/wallpapers/Icy_Tree
%{_kde4_datadir}/wallpapers/JK_Bridge_at_Night
%{_kde4_datadir}/wallpapers/Korea
%{_kde4_datadir}/wallpapers/Ladybuggin
%{_kde4_datadir}/wallpapers/Leafs_Labyrinth
%{_kde4_datadir}/wallpapers/Media_Life/contents/images/*
%{_kde4_datadir}/wallpapers/Midnight_in_Karelia
%{_kde4_datadir}/wallpapers/Plasmalicious
%{_kde4_datadir}/wallpapers/Quadros
%{_kde4_datadir}/wallpapers/Red_Leaf
%{_kde4_datadir}/wallpapers/Skeeter_Hawk
%{_kde4_datadir}/wallpapers/Spring_Sunray
%{_kde4_datadir}/wallpapers/Storm
%{_kde4_datadir}/wallpapers/The_Rings_of_Saturn
%{_kde4_datadir}/wallpapers/There_is_Rain_on_the_Table
%{_kde4_datadir}/wallpapers/Vector_Sunset
%{_kde4_datadir}/wallpapers/Winter_Track
%{_kde4_datadir}/wallpapers/Yellow_Flowers

%if 0%{?mono}
%files -n mono-icon-theme
%defattr(-,root,root,-)
%{_kde4_iconsdir}/mono
%endif

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Feb 10 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-2m)
- change wallpapers package name

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-3m)
- modify %%files to avoid conflicting

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Wed Sep 29 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.1-2m)
- rebuild against eigen2-devel-2.0.15

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-3m)
- full rebuild for mo7 release

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-2m)
- package wallpapers Requires: natsuki-backgrounds-kde >= 7.0.0-3m

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sun Jan 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.90-2m)
- fix %%postun of nuvola-icon-theme

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Wed Dec 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.85-2m)
- fix up Summary without period
- remove a lot of spaces

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-2m)
- set BR: eigen2-devel >= 2.0.52-0.20090729.2m for automatic building of STABLE_6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rebuild with new source
- - separate kdeclassic-icon-theme sub package

- * Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sat Apr 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- Requires: kdeartwork-sounds on stable5
- Requires: kdeartwork-wallpapers on stable5
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Thu Jan  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-2m)
- rebuild against strigi-0.6.2

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Mon Sep 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Mon Sep 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- rebuild against xscreensaver-5.07

- * Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.65-1m)
- - update to 4.1.65

- * Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.64-1m)
- - update to 4.1.64

- * Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.63-1m)
- - update to 4.1.63

- * Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.62-1m)
- - update to 4.1.62

- * Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.61-1m)
- - update to 4.1.61

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Fri Jun 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-2m)
- remove crystalsvg-icon-theme, it's not perfect for applications of KDE3
- use kdelibs3's crystalsvg icons instead of kdeartwork's crystalsvg-icon-theme for KDE3

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to KDE4

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Sat Aug  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- Requires: and BuildPreReq: xscreensaver-gl-extras

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- delete unused upstream patches

* Mon Mar  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-3m)
- import upstream patch to fix following problem
  #142071, Screensaver crashes when trying to switch media

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against kdelibs etc.

* Sun Jan 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6

* Sat Oct 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-2m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m kdebase-3.5.4-11m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3

* Sun May  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-2m)
- rebuild against xscreensaver-4.24

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Sun Mar 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-2m)
- rebuild against xscreensaver-4.18-6m

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- remove nokdelisuff.patch
- add %%doc

* Tue Jun 28 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.1-2m)
- fixed build error gcc4. import FC patch.(admin-visibility.patch)

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- modify %%files section

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- rebuild against kdebase-3.3.0-2m (libstdc++-3.4.1)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- modified %%files
- cleanup %%install

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Fri Dec 19 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-3m)
- rebuild against for qt-3.2.3

* Mon Nov 10 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-2m)
- rebuild against for qt-3.2.3

* Wed Oct 1 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Thu Aug 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- changes are:
     Fixed a potential printf() format string attack in the slideshow screensaver

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    no changelog...

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-3m)
- add URL

* Sun Feb 23 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-2m)
- fix add some files to %files (by muradaikan)

* Wed Feb  5 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Wed Dec 11 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-4m)
- rebuild against qt-3.0.5.

* Tue Jul 16 2002 Puntium <puntium@momonga-linux.org>
- (3.0.2-3m)
- fixed hardcoded version number
- nigirinaoshi

* Tue Jul 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.2-2m)
- revise the Source version

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.

* Tue Jun 11 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.1-2k)
  update to 3.0.1

* Fri Apr  5 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- revised spec file.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- rebuild against libpng 1.2.0.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Sat Sep  1 2001 Toru Hoshina <t@kondara.org>
- (2.2.0-4k)
- NOT noarch ... sumaso.

* Wed Aug 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)
- based on 2.2alpha2.

* Sat May 26 2001 Bernhard Rosenkraenzer <bero@redhat.com> 2.2-0.alpha2.1
- 2.2 alpha 2

* Thu Apr 26 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Initial release

