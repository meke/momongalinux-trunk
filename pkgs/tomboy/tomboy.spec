%global momorel 1
Name:           tomboy
Version:        1.11.3
Release: %{momorel}m%{?dist}
Summary:        Note-taking application
Group:          User Interface/Desktops
License:        LGPLv2+ and GPLv2+ and MIT
# Tomboy itself is LGPLv2+
# libtomboy contains GPL+ code
# Mono.Addins is MIT
URL:            http://projects.gnome.org/tomboy/
Source0:        http://download.gnome.org/sources/%{name}/1.11/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  pkgconfig(atk)
BuildRequires:  pkgconfig(gconf-sharp-2.0)
BuildRequires:  pkgconfig(gdk-2.0)
BuildRequires:  pkgconfig(gtk+-2.0)
BuildRequires:  pkgconfig(gtk-sharp-2.0)
BuildRequires:  pkgconfig(gtkspell-2.0)
BuildRequires:  pkgconfig(libgnomeprintui-2.2)
BuildRequires:  pkgconfig(mono)
BuildRequires:  pkgconfig(mono-addins)
BuildRequires:  pkgconfig(mono-addins-gui)
BuildRequires:  pkgconfig(mono-addins-setup)
BuildRequires:  pkgconfig(dbus-sharp-1.0)
BuildRequires:  pkgconfig(dbus-sharp-glib-1.0)
BuildRequires:  mono(Mono.Cairo) = 2.0.0.0
BuildRequires:  GConf2
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  gnome-doc-utils
BuildRequires:  intltool
BuildRequires:  libX11-devel

Requires: gtkspell
Requires(pre): GConf2
Requires(post): GConf2
Requires(preun): GConf2

# Mono only available on these:
ExclusiveArch: %ix86 x86_64 ppc ppc64 ia64 %{arm} sparcv9 alpha s390x


%description
Tomboy is a desktop note-taking application which is simple and easy to use.
It lets you organise your notes intelligently by allowing you to easily link
ideas together with Wiki style interconnects.


%package devel
Summary: Support for developing addings for tomboy
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}


%description devel
Tomboy is a desktop note-taking application. This package allows you
to develop addins that add new functionality to tomboy.


%prep
%setup -q

# Convert to utf-8
for file in ChangeLog ; do
    iconv -f ISO-8859-1 -t UTF-8 -o $file.new $file && \
    touch -r $file $file.new && \
    mv $file.new $file
done


%build
%configure --disable-scrollkeeper --disable-static \
	   --disable-galago --disable-evolution
mkdir bin
%make 


%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot} INSTALL="install -p"
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

find %{buildroot} -name '*.la' -delete

chmod a+x %{buildroot}%{_libdir}/%{name}/*.exe
chmod a+x %{buildroot}%{_libdir}/%{name}/addins/*.dll

desktop-file-validate %{buildroot}%{_datadir}/applications/tomboy.desktop

%find_lang %name --with-gnome

for i in %{buildroot}%{_datadir}/mime/*; do
  if [ ! "${i##*/}" = "packages" ]; then
    rm -rf $i
  fi
done


%post
update-mime-database >&/dev/null || :
%gconf_schema_upgrade tomboy
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :


%pre
%gconf_schema_prepare tomboy


%preun
%gconf_schema_remove tomboy


%postun
update-mime-database >&/dev/null || :
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%dir %{_libdir}/%{name}
%{_bindir}/tomboy
%{_libdir}/tomboy/*
%{_datadir}/dbus-1/services/org.gnome.Tomboy.service
%{_mandir}/man1/tomboy.1.bz2
%{_datadir}/tomboy
%{_datadir}/icons/hicolor/*/apps/tomboy.*
%{_datadir}/icons/hicolor/*/mimetypes/*
%{_datadir}/mime/packages/tomboy.xml
%{_datadir}/applications/tomboy.desktop
%{_sysconfdir}/gconf/schemas/tomboy.schemas


%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/*.pc


%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.3-1m)
- reimport from fedora
- update to 1.11.3
- disable evolution and galago supports

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-3m)
- rebuild for mono-2.10.9

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-2m)
- fix BR

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Tue Apr 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-4m)
- fix BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-6m)
- fix BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-5m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-3m)
- add many Requires:

* Sun Jun 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-3m)
- fix BuildRequires

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-2m)
- delete conflict file

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Sat Dec 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.1-2m)
- add autoreconf, for libtool 2.2.6b

* Thu Nov 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- add BuildPrereq

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2m)
-- good-bye search option from desktop file again

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Mon Sep 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.3-5m)
- rebuild against mono-2.4.2.3
-- good-bye search option from desktop file

* Sat Aug  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.3-4m)
- add Req:gnome-desktop-sharp

* Sun Jul 12 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.3-3m)
- add BuildPrereq

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.3-2m)
- add devel package

* Sat Jun 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.3-1m)
- update to 0.14.3

* Fri May 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-1m)
- update to 0.14.2

* Tue Apr 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.1-1m)
- update to 0.14.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.6-1m)
- update to 0.13.6

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.5-1m)
- update to 0.13.5

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.4-1m)
- update to 0.13.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.2-2m)
- rebuild against rpm-4.6

* Thu Dec 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.2-1m)
- update to 0.12.2

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-3m)
- good-bye debug symbol

* Fri Nov  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-2m)
- add patch0 (for menu)

* Thu Oct  9 2008 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Thu Sep 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.2-3m)
- add Requires: mono-addins

* Sat Aug  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-2m)
- add Requires: gtk2-common hicolor-icon-theme

* Tue May 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0
- delete patch0 and patch1(merged)

* Tue Feb 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-3m)
- add patch1 for gnome-sharp-2.16.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.2-2m)
- %%NoSource -> NoSource

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Sun Sep  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-4m)
- add patch1 for mono-1.2.5

* Mon Jul  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-3m)
- add patch0 for Tomboy.exe.config

* Tue May 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-2m)
- fix BuildPrereq

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Tue Mar  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.9-1m)
- update to 0.5.9

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.8-3m)
- rewind %%post script (gconftool-2)

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-3m)
- add patch1 for mono-1.2.3

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.8-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.8-1m)
- update to 0.5.8 (unstable)

* Sun Sep 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-2m)
- remove category Application

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Mon Sep  4 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3.9-4m)
- add Requires: galago-daemon, eds-feed

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.9-3m)
- enable galago-sharp

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.9-2m)
- add patch0 for dbus

* Thu Aug 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.9-1m)
- initial build
