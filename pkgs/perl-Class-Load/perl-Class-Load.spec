%global         momorel 2

Name:           perl-Class-Load
Version:        0.21
Release:        %{momorel}m%{?dist}
Summary:        Working (require "Class::Name") and more
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Class-Load/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/Class-Load-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Module-Runtime
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-Simple
Requires:       perl-Module-Runtime
Requires:       perl-Scalar-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
require EXPR only accepts Class/Name.pm style module names, not
Class::Name. How frustrating! For that, we provide load_class
'Class::Name'.

%prep
%setup -q -n Class-Load-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Class/Load.pm
%{perl_vendorlib}/Class/Load
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-2m)
- rebuild against perl-5.16.1

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19
- rebuild against perl-5.16.0

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Wed Oct 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-2m)
- rebuild against perl-5.14.2

* Wed Sep  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Wed Sep  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-2m)
- rebuild for new GCC 4.5

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
