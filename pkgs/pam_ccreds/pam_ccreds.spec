%global momorel 7

Summary: Pam module to cache login credentials
Name: pam_ccreds
Version: 10
Release: %{momorel}m%{?dist}
License: GPL+
Group: System Environment/Base
URL: http://www.padl.com/OSS/pam_ccreds.html
Source0: http://www.padl.com/download/%{name}-%{version}.tar.gz
NoSource: 0
Patch1: pam_ccreds-3-inst-no-root.patch
Patch2: pam_ccreds-7-no-filename.patch
Patch3: pam_ccreds-7-open.patch

BuildRequires: automake 
BuildRequires: libdb-devel >= 5.3.15
BuildRequires: libgcrypt-devel 
BuildRequires: pam-devel >= 1.1.5-2m
Requires: pam
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The pam_ccreds module provides a mechanism for caching
credentials when authenticating against a network
authentication service, so that authentication can still
proceed when the service is down. Note at present no
mechanism is provided for caching _authorization_ 
information, i.e. whether you are allowed to login once
authenticated.

%prep
%setup -q
%patch1 -p1 -b .inst-no-root
%patch2 -p1 -b .no-filename
%patch3 -p1 -b .open
touch compile
autoreconf

%build
%configure --enable-gcrypt
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

install -m 755 cc_test $RPM_BUILD_ROOT%{_sbindir}
install -m 755 cc_dump $RPM_BUILD_ROOT%{_sbindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS README
%{_libdir}/security/pam_ccreds.so
%attr(4755,root,root) %{_sbindir}/ccreds_chkpwd
%{_sbindir}/cc_test
%{_sbindir}/cc_dump

%changelog
* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (10-7m)
- support UerMove env

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10-6m)
- rebuild against libdb-5.3.15

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (10-5m)
- rebuild against libdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (10-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10-1m)
- update to 10

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-5m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-3m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7-2m)
- rebuild against db4-4.7.25-1m

* Sun Jul 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (7-1m)
- sync Fedora

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4-3m)
- rebuild against gcc43

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4-2m)
- rebuild against db4-4.6.21

* Wed Mar 14 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4-1m)
- import from fc

* Mon Feb  5 2007 Tomas Mraz <tmraz@redhat.com> - 4-1
- new upstream version

* Tue Aug 22 2006 Tomas Mraz <tmraz@redhat.com> - 3-5
- add cc_test and cc_dump utilities

* Fri Jul 21 2006 Tomas Mraz <tmraz@redhat.com> - 3-4
- fixed mistake in chkpwd patch causing update creds to fail

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 3-3.2.1
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 3-3.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 3-3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Tomas Mraz <tmraz@redhat.com> - 3-3
- don't change ownership in make install
- build ccreds_validate as PIE

* Wed Jan  4 2006 Tomas Mraz <tmraz@redhat.com> - 3-2
- the path to ccreds_validate helper was wrong

* Wed Jan  4 2006 Tomas Mraz <tmraz@redhat.com> - 3-1
- new upstream version
- added patch (slightly modified) by W. Michael Petullo to support
  operation from non-root accounts (#151914)

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Nov  9 2005 Tomas Mraz <tmraz@redhat.com> - 1-8
- rebuilt against new openssl

* Thu Jun  9 2005 John Dennis <jdennis@redhat.com> - 1-7
- fix bug #134674, change BuildPrereq openssl to openssl-devel

* Wed Mar 16 2005 John Dennis <jdennis@redhat.com> 1-6
- bump rev for gcc4 build

* Wed Mar 16 2005 Dan Williams <dcbw@redhat.com> pam_ccreds-1-5
- rebuild to pick up new libcrypto.so.5

* Mon Feb 14 2005 Nalin Dahyabhai <nalin@redhat.com> pam_ccreds-1-4
- change install dir from /lib/security to /%%{_lib}/security

* Tue Oct 12 2004 Miloslav Trmac <mitr@redhat.com> pam_ccreds-1-3
- BuildRequire: automake16, openssl (from Maxim Dzumanenko, #134674)

* Wed Sep  1 2004 John Dennis <jdennis@redhat.com> pam_ccreds-1-2
- change install dir from /%%{_lib}/security to /lib/security

* Sun Jun 13 2004 John Dennis <jdennis@redhat.com> ccreds-1
- Initial build.
