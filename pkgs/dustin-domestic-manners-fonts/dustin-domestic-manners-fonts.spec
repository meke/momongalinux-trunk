%global momorel 6

%define fontname dustin-domestic-manners
%define fontconf 63-%{fontname}.conf

Name:          %{fontname}-fonts
Version:       20030527
Release:       %{momorel}m%{?dist}
Summary:       Handwriting font by Dustin Norlander

Group:         User Interface/X
License:       GPLv2+
URL:           http://www.dustismo.com
# Actual download URL
#URL:           http://ftp.de.debian.org/debian/pool/main/t/ttf-dustin/ttf-dustin_20030517.orig.tar.gz 
Source0:       Domestic_Manners.zip
Source1:       %{name}-fontconfig.conf
BuildRoot:     %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem

%description 

This font mimics the authors handwriting. The name comes from the book 
Domestic Manners of the Americans, by Fanny Trollope.

Font contains, letters, numbers, punctuation, accented characters and 
some special characters (most European Latin characters).

%prep
%setup -q -c %{name}
sed -i 's/\r//' license.txt

%build

%install
rm -fr %{buildroot}
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg -f %{fontconf} *.ttf

%doc license.txt
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20030527-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20030527-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20030527-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030527-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030527-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20030527-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20030527-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb 01 2009 Sven Lankes <sven@lank.es> - 20030527-2
- Fix typo in description 

* Sun Jan 25 2009 Sven Lankes <sven@lank.es> - 20030527-1
- Initial packaging

