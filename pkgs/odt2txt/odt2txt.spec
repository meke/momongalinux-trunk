%global momorel 4

Name:           odt2txt
Version:        0.4
Release:        %{momorel}m%{?dist}
Summary:        Converts an OpenDocument to plain text

Group:          Applications/Text
License:        GPLv2
URL:            http://stosberg.net/odt2txt/
Source0:        http://stosberg.net/odt2txt/odt2txt-%{version}.tar.gz
NoSource:       0
Patch0:         odt2txt-makefile.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  zlib-devel

%description

odt2txt is a command-line tool which extracts the text out of
OpenDocument Texts produced by OpenOffice.org, StarOffice, KOffice and
others.

odt2txt is ...

* small
* supports multiple output encodings
* adopts to your locale
* able to substitute common characters which the output charset does
  not contain with ascii look-a-likes
* written in C, has few dependencies
* portable (runs on Linux, *BSD, Solaris, HP-UX, Windows, Cygwin)


%prep
%setup -q
%patch0 -p0 -b .makefile


%build
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS"


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT PREFIX=%{_prefix}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc GPL-2
%{_bindir}/odt2txt
%{_mandir}/man1/odt2txt.1*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-1m)
- import from Fedora 13

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Aug 31 2008 Michel Salim <salimma@fedoraproject.org> - 0.4-1
- Update to 0.4

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.3-2
- Autorebuild for GCC 4.3

* Fri Sep 21 2007 Michel Salim <michel.sylvan@gmail.com> - 0.3-1
- Initial package
