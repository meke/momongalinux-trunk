%global		momorel 1
Summary: Backends for the gio framework in GLib
Name: gvfs
Version: 1.21.2
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.gtk.org

Source0: http://download.gnome.org/sources/gvfs/1.21/%{name}-%{version}.tar.xz
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.37.0
# for post-install update-gio-modules and overall functionality
Requires: glib2 >= 2.37.0
BuildRequires: dbus-glib-devel
BuildRequires: openssh-clients
BuildRequires: libcdio-devel >= 0.90
BuildRequires: libcdio-paranoia-devel >= 10.2+0.90
BuildRequires: libgudev1-devel 
BuildRequires: libsoup-devel >= 2.42.0
BuildRequires: avahi-glib-devel >= 0.6
BuildRequires: libgnome-keyring-devel
BuildRequires: libsecret-devel
BuildRequires: intltool
BuildRequires: gettext-devel
BuildRequires: libudisks2-devel
Requires: udisks2
BuildRequires: expat-devel
BuildRequires: libbluray-devel
BuildRequires: systemd-devel >= 187
BuildRequires: libxslt-devel
BuildRequires: gtk3-devel
BuildRequires: docbook-style-xsl

Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils

# The patch touches Makefile.am files:
BuildRequires: automake autoconf
BuildRequires: libtool

# http://bugzilla.gnome.org/show_bug.cgi?id=567235
Patch0: gvfs-archive-integration.patch

Obsoletes: gnome-mount <= 0.8
Obsoletes: gnome-mount-nautilus-properties <= 0.8

%description
The gvfs package provides backend implementations for the gio
framework in GLib. It includes ftp, sftp, cifs.


%package devel
Summary: Development files for gvfs
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The gvfs-devel package contains headers and other files that are
required to develop applications using gvfs.


%package fuse
Summary: FUSE support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: fuse-devel
Requires: fuse

%description fuse
This package provides support for applications not using gio
to access the gvfs filesystems.


%package smb
Summary: Windows fileshare support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: libsmbclient-devel >= 3.2.0-1.pre2.8
BuildRequires: libtalloc-devel >= 1.3.0-0

%description smb
This package provides support for reading and writing files on windows
shares (SMB) to applications using gvfs.


%package archive
Summary: Archiving support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: libarchive-devel >= 2.7.1-1

%description archive
This package provides support for accessing files inside Zip and Tar archives,
as well as ISO images, to applications using gvfs.


%ifnarch s390 s390x
%package obexftp
Summary: ObexFTP support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: obex-data-server >= 0.3.4-6
BuildRequires: bluez-libs-devel >= 3.12
Obsoletes: gnome-vfs2-obexftp <= 0.4

%description obexftp
This package provides support for reading files on Bluetooth mobile phones
and devices through ObexFTP to applications using gvfs.
%endif


%package gphoto2
Summary: gphoto2 support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: libgphoto2-devel >= 2.5.0
BuildRequires: libusb-devel
BuildRequires: libexif-devel

%description gphoto2
This package provides support for reading and writing files on
PTP based cameras (Picture Transfer Protocol) and MTP based
media players (Media Transfer Protocol) to applications using gvfs.


%ifnarch s390 s390x
%package afc
Summary: AFC support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: usbmuxd
BuildRequires: libimobiledevice-devel >= 1.1.5

%description afc
This package provides support for reading files on mobile devices
including phones and music players to applications using gvfs.
%endif


%package afp
Summary: AFP support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: libgcrypt-devel >= 1.2.2
# this should ensure having this new subpackage installed on upgrade from older versions
Obsoletes: %{name} < 1.9.4-1

%description afp
This package provides support for reading and writing files on
Mac OS X and original Mac OS network shares via Apple Filing Protocol
to applications using gvfs.


%package mtp
Summary: MTP support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: libmtp-devel >= 1.1.0

%description mtp
This package provides support for reading and writing files on
MTP based devices (Media Transfer Protocol) to applications using gvfs.


%package goa
Summary: GOA support for gvfs
Group: System Environment/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: gnome-online-accounts-devel >= 3.7.1

%description goa
This package provides seamless integration with gnome-online-accounts
file services.


%prep
%setup -q
%patch0 -p1 -b .archive-integration

# Needed for gvfs-0.2.1-archive-integration.patch
libtoolize --force  || :
aclocal  || :
autoheader  || :
automake  || :
autoconf  || :

%build
%configure \
        --disable-hal \
        --disable-gdu \
        --enable-keyring \
        --enable-udisks2

make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

rm $RPM_BUILD_ROOT%{_libdir}/gvfs/*.la
rm $RPM_BUILD_ROOT%{_libdir}/gio/modules/*.la

# trashlib is GPLv3, include the license
cp -p daemon/trashlib/COPYING COPYING.GPL3

%find_lang gvfs

%post
/sbin/ldconfig
# Reload .mount files:
killall -USR1 gvfsd >&/dev/null || :
update-desktop-database &> /dev/null || :
gio-querymodules-%{__isa_bits} %{_libdir}/gio/modules &> /dev/null || :

%postun
/sbin/ldconfig
update-desktop-database &> /dev/null ||:
gio-querymodules-%{__isa_bits} %{_libdir}/gio/modules &> /dev/null || :
if [ $1 -eq 0 ] ; then
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


# Reload .mount files when single subpackage is installed:
%post smb
killall -USR1 gvfsd >&/dev/null || :
%post gphoto2
killall -USR1 gvfsd >&/dev/null || :
%post mtp
killall -USR1 gvfsd >&/dev/null || :
%post goa
killall -USR1 gvfsd >&/dev/null || :
%ifnarch s390 s390x
%post obexftp
killall -USR1 gvfsd >&/dev/null || :
%post afc
killall -USR1 gvfsd >&/dev/null || :
%endif

%post archive
update-desktop-database >&/dev/null || :
killall -USR1 gvfsd >&/dev/null || :

%postun archive
update-desktop-database >&/dev/null || :

%files -f gvfs.lang
%doc AUTHORS COPYING COPYING.GPL3 NEWS README
%dir %{_datadir}/gvfs
%dir %{_datadir}/gvfs/mounts
%{_datadir}/gvfs/mounts/sftp.mount
%{_datadir}/gvfs/mounts/trash.mount
%{_datadir}/gvfs/mounts/cdda.mount
%{_datadir}/gvfs/mounts/computer.mount
%{_datadir}/gvfs/mounts/dav.mount
%{_datadir}/gvfs/mounts/dav+sd.mount
%{_datadir}/gvfs/mounts/http.mount
%{_datadir}/gvfs/mounts/localtest.mount
%{_datadir}/gvfs/mounts/burn.mount
%{_datadir}/gvfs/mounts/dns-sd.mount
%{_datadir}/gvfs/mounts/network.mount
%{_datadir}/gvfs/mounts/ftp.mount
%{_datadir}/gvfs/mounts/recent.mount
%{_datadir}/dbus-1/services/org.gtk.Private.UDisks2VolumeMonitor.service
%{_datadir}/dbus-1/services/gvfs-daemon.service
%{_datadir}/dbus-1/services/gvfs-metadata.service
%{_datadir}/gvfs/remote-volume-monitors/udisks2.monitor
%{_datadir}/GConf/gsettings/*.convert
%{_datadir}/glib-2.0/schemas/*.xml
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/gvfs
%{_libdir}/gvfs/libgvfscommon.so
%{_libdir}/gvfs/libgvfsdaemon.so
%{_libdir}/gio/modules/libgioremote-volume-monitor.so
%{_libdir}/gio/modules/libgvfsdbus.so
%{_libexecdir}/gvfsd
%{_libexecdir}/gvfsd-ftp
%{_libexecdir}/gvfsd-sftp
%{_libexecdir}/gvfsd-trash
%{_libexecdir}/gvfsd-cdda
%{_libexecdir}/gvfsd-computer
%{_libexecdir}/gvfsd-dav
%{_libexecdir}/gvfsd-http
%{_libexecdir}/gvfsd-localtest
%{_libexecdir}/gvfsd-burn
%{_libexecdir}/gvfsd-dnssd
%{_libexecdir}/gvfsd-network
%{_libexecdir}/gvfsd-metadata
%{_libexecdir}/gvfsd-recent
%{_libexecdir}/gvfs-udisks2-volume-monitor
%{_bindir}/gvfs-cat
%{_bindir}/gvfs-copy
%{_bindir}/gvfs-info
%{_bindir}/gvfs-less
%{_bindir}/gvfs-ls
%{_bindir}/gvfs-mime
%{_bindir}/gvfs-mkdir
%{_bindir}/gvfs-monitor-dir
%{_bindir}/gvfs-monitor-file
%{_bindir}/gvfs-mount
%{_bindir}/gvfs-move
%{_bindir}/gvfs-open
%{_bindir}/gvfs-rename
%{_bindir}/gvfs-rm
%{_bindir}/gvfs-save
%{_bindir}/gvfs-trash
%{_bindir}/gvfs-tree
%{_bindir}/gvfs-set-attribute
%doc %{_mandir}/man1/gvfs-*
%doc %{_mandir}/man1/gvfsd.1*
%doc %{_mandir}/man1/gvfsd-metadata.1.*
%doc %{_mandir}/man7/gvfs.7.*

%files devel
%dir %{_includedir}/gvfs-client
%dir %{_includedir}/gvfs-client/gvfs
%{_includedir}/gvfs-client/gvfs/gvfsurimapper.h
%{_includedir}/gvfs-client/gvfs/gvfsuriutils.h


%files fuse
%{_libexecdir}/gvfsd-fuse
%{_prefix}/lib/tmpfiles.d/gvfsd-fuse-tmpfiles.conf
%doc %{_mandir}/man1/gvfsd-fuse.1.*

%files smb
%{_libexecdir}/gvfsd-smb
%{_libexecdir}/gvfsd-smb-browse
%{_datadir}/gvfs/mounts/smb-browse.mount
%{_datadir}/gvfs/mounts/smb.mount


%files archive
%{_datadir}/applications/mount-archive.desktop
%{_libexecdir}/gvfsd-archive
%{_datadir}/gvfs/mounts/archive.mount


%files gphoto2
%{_libexecdir}/gvfsd-gphoto2
%{_datadir}/gvfs/mounts/gphoto2.mount
%{_libexecdir}/gvfs-gphoto2-volume-monitor
%{_datadir}/dbus-1/services/org.gtk.Private.GPhoto2VolumeMonitor.service
%{_datadir}/gvfs/remote-volume-monitors/gphoto2.monitor

%ifnarch s390 s390x
%files obexftp
%{_libexecdir}/gvfsd-obexftp
%{_datadir}/gvfs/mounts/obexftp.mount

%files afc
%{_libexecdir}/gvfsd-afc
%{_datadir}/gvfs/mounts/afc.mount
%{_libexecdir}/gvfs-afc-volume-monitor
%{_datadir}/dbus-1/services/org.gtk.Private.AfcVolumeMonitor.service
%{_datadir}/gvfs/remote-volume-monitors/afc.monitor
%endif

%files afp
%{_libexecdir}/gvfsd-afp
%{_libexecdir}/gvfsd-afp-browse
%{_datadir}/gvfs/mounts/afp.mount
%{_datadir}/gvfs/mounts/afp-browse.mount

%files mtp
%{_libexecdir}/gvfsd-mtp
%{_datadir}/gvfs/mounts/mtp.mount
%{_libexecdir}/gvfs-mtp-volume-monitor
%{_datadir}/dbus-1/services/org.gtk.Private.MTPVolumeMonitor.service
%{_datadir}/gvfs/remote-volume-monitors/mtp.monitor

%files goa
%{_libexecdir}/gvfs-goa-volume-monitor
%{_datadir}/dbus-1/services/org.gtk.Private.GoaVolumeMonitor.service
%{_datadir}/gvfs/remote-volume-monitors/goa.monitor

%changelog
* Mon Jun  2 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.21.2-1m)
- update to 1.21.2

* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14.2-3m)
- rebuild against libimobiledevice-1.1.5

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14.2-2m)
- rebuild against libcdio-0.90

* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.14.2-1m)
- update to 1.14.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.14.1-1m)
- update to 1.14.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.0-1m)
- update to 1.14.0

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13.9-1m)
- update to 1.13.9

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13.8-1m)
- update to 1.13.8

* Wed Aug 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.7-2m)
- rebuild against libimobiledevice

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13.7-1m)
- update to 1.13.7

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.3-2m)
- rebuild against systemd-187

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13.3-1m)
- update to 1.13.3

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13.2-2m)
- switch to use udisks2 instead of udisks

* Sat Jun 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13.2-1m)
- update to 1.13.2
- reimport from fedora

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.1-2m)
- rebuild against libarchive-3.0.4

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-1m)
- update to 1.10.1

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.0-2m)
- remove BR hal-devel

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.5-1m)
- update to 1.9.5

* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.2-2m)
- support AFC backend

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-1m)
- upgate to 1.8.2

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-1m)
- upgate to 1.8.1

* Sat Apr 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-3m)
- add glib-compile-schemas %%posttrans and %%postun

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.0-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- upgate to 1.8.0

* Sun Feb 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.6-2m)
- add --disable-gconf (glib-2.28.1 does not support)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.6-2m)
- rebuild for new GCC 4.5

* Sun Nov 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.6-1m)
- update to 1.6.6

* Wed Nov  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.5-1m)
- update to 1.6.5

* Sun Oct 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.4-2m)
- add gio-querymodules to script

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.2-5m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-4m)
- split off some subpackages (dependencies are too deep)

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-liunx.org>
- (1.6.2-3m)
- rebuild against libcdio-0.82

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-2m)
- rebuild against libimobiledevice-1.0.2

* Sat May 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Fri Feb 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-2m)
- enable AFC support explicitly

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-2m)
- add BuildPrereq

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Fri Apr  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.8-1m)
- update to 1.1.8

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-3m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-2m)
- rebuild against bluez-4.22

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-2m)
- rebuild against gnutls-2.4.1 (libsoup)

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.5-1m)
- update to 0.2.5

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.4-1m)
- update to 0.2.4

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.3-1m)
- update to 0.2.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-2m)
- rebuild against gcc43

* Sat Mar 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Tue Mar 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0.1-1m)
- initial build
