%global momorel 36
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global confsamp %{_datadir}/config-sample
%global elispname tdtd
%global srcver 071

Summary: Major mode for editing SGML and XML DTDs
Name: emacs-%{elispname}
Version: 0.7.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
URL: http://www.menteith.com/wiki/tdtd
Source0: http://www.menteith.com/raw-attachment/wiki/%{elispname}/data/%{elispname}%{srcver}.zip
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: unzip
Requires: emacs >= %{emacsver}
Obsoletes: tdtd-emacs
Obsoletes: tdtd-xemacs

Obsoletes: elisp-tdtd
Provides: elisp-tdtd

%description
This package contains an emacs major mode for editing SGML and XML DTDs.

%prep
%setup -q -c -n %{elispname}-%{version}
%{__mkdir} emacs

%build
for f in *.el;
  do emacs -batch -q -no-site-file -eval "(add-to-list 'load-path \".\")" \
    -f batch-byte-compile $f
done

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{e_sitedir}/%{elispname}
%{__install} -m 0644 *.el *.elc %{buildroot}%{e_sitedir}/%{elispname}

%{__mkdir_p} %{buildroot}%{confsamp}/%{name}
%{__install} -m 0644 dot_emacs %{buildroot}%{confsamp}/%{name}/%{elispname}.dot.emacs

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc changelog.txt TODO readme.txt tutorial.txt
%{confsamp}/%{name}/*.dot.emacs
%dir %{e_sitedir}/%{elispname}
%{e_sitedir}/%{elispname}/*.el*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-36m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-35m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.1-34m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-33m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-32m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-31m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-30m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-29m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-28m)
- merge tdtd-emacs to elisp-tdtd
- kill tdtd-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-27m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-26m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-25m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-24m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-23m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-22m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-21m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-20m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-19m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-18m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-17m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-16m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-15m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-14m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-13m)
- change URL
- no %%NoSource 0

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-12m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-11m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-10m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-9m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-8m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-7m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-6m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-5m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.7.1-4m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-3m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.7.1-2m)
- rebuild against emacs-21.3.50

* Sun Sep 21 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.7.1-1m)
- first import for momonga.
