%global momorel 5

Name: mtx
Version: 1.3.12
Release: %{momorel}m%{?dist}
Summary: SCSI media changer control program
License: GPLv2
Group: Applications/System
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://mtx.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
The MTX program controls the robotic mechanism in autoloaders and tape
libraries such as the HP SureStore DAT 40x6, Exabyte EZ-17, and
Exabyte 220. This program is also reported to work with a variety of
other tape libraries and autochangers from ADIC, Tandberg/Overland,
Breece Hill, HP, and Seagate.

If you have a backup tape device capable of handling more than one
tape at a time, you should install MTX.


%prep
%setup -q

# remove exec permission
chmod a-x contrib/config_sgen_solaris.sh contrib/mtx-changer


%build
export CFLAGS="$RPM_OPT_FLAGS"
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
%makeinstall


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc CHANGES COMPATABILITY contrib FAQ LICENSE
%doc mtx.doc mtxl.README.html README TODO
%{_mandir}/man1/*
%{_sbindir}/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.12-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.12-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.12-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Dec 21 2008 Dan Horák <dan[at]danny.cz> 1.3.12-2
- spec file cleanup for better compliance with the guidelines

* Mon Aug 25 2008 Dan Horák <dan[at]danny.cz> 1.3.12-1
- update to mtx-1.3.12

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.3.11-3
- Autorebuild for GCC 4.3

* Thu Aug 23 2007 Jindrich Novy <jnovy@redhat.com> 1.3.11-2
- update License
- rebuild for BuildID

* Wed Mar 28 2007 Jindrich Novy <jnovy@redhat.com> 1.3.11-1
- update to 1.3.11 (adds new scsieject utility, bugfixes)
- sync nostrip patch

* Tue Feb 06 2007 Jindrich Novy <jnovy@redhat.com> 1.3.10-1
- update to mtx-1.3.10
- update URL, Source0
- don't strip debuginfo

* Tue Dec 12 2006 Jindrich Novy <jnovy@redhat.com> 1.2.18-9
- spec cleanup

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.2.18-8.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.2.18-8.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.2.18-8.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Mar  7 2005 Jindrich Novy <jnovy@redhat.com> 1.2.18-8
- fix type confusion in SCSI_writet(), SCSI_readt(), slow_memcopy()
  and slow_bzero()
- rebuilt with gcc4

* Thu Feb 10 2005 Jindrich Novy <jnovy@redhat.com> 1.2.18-7
- remove -D_FORTIFY_SOURCE=2 from CFLAGS, present in RPM_OPT_FLAGS

* Wed Feb  9 2005 Jindrich Novy <jnovy@redhat.com> 1.2.18-6
- rebuilt with -D_FORTIFY_SOURCE=2

* Wed Aug 11 2004 Jindrich Novy <jnovy@redhat.com> 1.2.18-5
- dead code elimination
- updated spec link to recent source
- removed spec link to obsolete URL
- rebuilt

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jan 13 2004 Than Ngo <than@redhat.com> 1.2.18-2
- rebuild

* Fri Sep 26 2003 Harald Hoyer <harald@redhat.de> 1.2.18-1
- 1.2.18

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> 1.2.16-6
- rebuild on all arches

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Jun 19 2002 Than Ngo <than@redhat.com> 1.2.16-4
- don't forcibly strip binaries

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Feb 26 2002 Than Ngo <than@redhat.com> 1.2.16-2
- rebuild

* Tue Feb 19 2002 Bernhard Rosenkraenzer <bero@redhat.com> 1.2.16-1
- 1.2.16

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Fri Dec 14 2001 Than Ngo <than@redhat.com> 1.2.15-1
- update to 1.2.15

* Mon Aug 13 2001 Preston Brown <pbrown@redhat.com> 1.2.13-1
- 1.2.13 fixes "+ Too many Data Transfer Elements Reported" problem (#49258)

* Mon Jun 25 2001 Preston Brown <pbrown@redhat.com>
- 1.2.12
- moved binaries to /usr/sbin from /sbin

* Wed Feb 14 2001 Michael Stefaniuc <mstefani@redhat.com>
- 1.2.10
- updated %%doc

* Mon Dec 11 2000 Preston Brown <pbrown@redhat.com>
- 1.2.9

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jun 15 2000 Preston Brown <pbrown@redhat.com>
- 1.2.7

* Tue May 23 2000 Preston Brown <pbrown@redhat.com> 
- adopted for Winston

* Fri May 12 2000 Kenneth Porter <shiva@well.com>
- 1.2.6
- Fixed 'eepos' stuff to use | rather than || (whoops!)
- Accept a 4-byte element descriptor for the robot arm for certain older
- autochangers. 

* Mon May 8 2000 Kenneth Porter <shiva@well.com>
- Spell sourceforge right so the link at rpmfind.net will work.

* Thu May 4 2000 Kenneth Porter <shiva@well.com>
- 1.2.5

* Thu Oct 29 1998  Ian Macdonald <ianmacd@xs4all.nl>
- moved mtx from /sbin to /bin, seeing as mt is also located there

* Fri Oct 23 1998  Ian Macdonald <ianmacd@xs4all.nl>
- first RPM release
