%global momorel 14
%global php_ver 5.4.1

%global pkg_name syck

Summary: YAML module for PHP
Name: php-%{pkg_name}
Version: 0.55
Release: %{momorel}m%{?dist}
License: BSD
Group: Development/Languages
URL: http://whytheluckystiff.net/syck/
#Source0: http://pecl.php.net/get/%{pkg_name}-%{version}.tgz
#NoSource: 0
Source0:   %{pkg_name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source10: %{pkg_name}.ini

Patch1: sycklib.patch
Patch2: syck-0.55-glibc210.patch
Patch3: %{name}-%{version}-php54.patch

Requires: php >= %{php_ver}
BuildRequires: php-devel >= %{php_ver}

%description
YAML is a new language for data. A readable, friendly language for
storing lists, dictionaries, text, numerics and more. YAML 1.0 is out,
in working draft. It is time to start using YAML.

Syck is an extension for reading and writing YAML swiftly in popular
scripting languages. As Syck loads the YAML, it stores the data
directly in your language symbol table. This means speed. This means
power. This means Do not disturb Syck because it is so focused on the
task at hand that it will slay you mortally if you get in its way.

%prep
# Setup main module
%setup -q -n %{pkg_name}-%{version}

%patch1 -p0
%patch2 -p1 -b .glibc210
%patch3 -p1 -b .php54

# build libsyck.a
%configure
%make

cp lib/syck.h ext/php
cp lib/syck_st.h ext/php
cp lib/libsyck.a ext/php 

# prepare php module
cd ext/php
%{_bindir}/phpize
%configure --with-syck=./

%build
# Build main module
%make

%install
rm -rf %{buildroot}
cd ext/php
%makeinstall INSTALL_ROOT=%{buildroot}
%{__install} -m 755 -d %{buildroot}%{_sysconfdir}/php.d
%{__install} -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/php.d

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGELOG COPYING README* RELEASE TODO
%{_sysconfdir}/php.d/%{pkg_name}.ini
%{_libdir}/php/modules/%{pkg_name}.so

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-14m)
- rebuild against php-5.4.1

* Sat Jul 16 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.55-13m)
- INI comments char fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.55-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.55-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.55-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.55-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.55-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.55-7m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.55-6m)
- rebuild against rpm-4.6

* Mon May 05 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.55-5m)
- rebuild against php 5.2.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.55-4m)
- rebuild against gcc43

* Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.55-3m)
- rebuild against php-5.2.1

* Sat May 10 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.55-2m)
- use %{pkg_name}

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.55-2m)
- rebuild against PHP 5.1.4

* Wed Mar 1 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- initial version
