%global momorel 1

Summary:	Top like utility for I/O
Name:		iotop
Version:	0.6
Release:	%{momorel}m%{?dist}
License:	GPLv2
Group:		Applications/System
Source0:	http://guichaz.free.fr/iotop/files/%{name}-%{version}.tar.bz2
NoSource:       0

URL:		http://guichaz.free.fr/iotop/
BuildRequires:	python-devel
BuildRequires:	rpm-python
Requires:	python >= 2.7
BuildArch:	noarch
BuildRoot:	%{tmpdir}/%{name}-%{version}-root-%(id -u -n)

%description
Linux has always been able to show how much I/O was going on (the bi
and bo columns of the vmstat 1 command). iotop is a Python program
with a top like UI used to show of behalf of which process is the I/O
going on.

%prep
%setup -q

%build
python ./setup.py build

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_mandir}/man8

python ./setup.py install \
        --optimize 2 \
        --root=%{buildroot}

install iotop.8 %{buildroot}%{_mandir}/man8

%clean
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
%doc NEWS THANKS
%attr(755,root,root) %{_sbindir}/iotop
/usr/lib/python*/site-packages/iotop
/usr/lib/python*/site-packages/*.egg-info
%{_mandir}/man8/iotop.8*

%changelog
* Sat Oct 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-1m)
- update 0.6
- 

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-1m)
- update 0.4.4

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.3-1m)
- update 0.4.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.2-2m)
- fix changelog section

* Fri Nov  6 2009 Yasuo Ohagki <yohgaki@momonga-linux.org>
- (0.3.2-1m)
- import from PLD

* Thu Jun  6 2009 PLD Team <feedback@pld-linux.org>
All persons listed below can be reached at <cvs_login>@pld-linux.org

$Log: iotop.spec,v $
Revision 1.11  2009/06/11 18:31:28  uzsolt
- hu fixing

Revision 1.10  2009/06/11 14:44:56  uzsolt
- 0.3.1
- hu

Revision 1.9  2008/10/03 17:44:34  arekm
- release 2

Revision 1.8  2008/08/04 11:40:04  zbyniu
- up to 0.2.1

Revision 1.7  2008/06/28 19:34:17  adamg
- pl description
- adapterized

Revision 1.6  2008/06/09 12:10:21  charles
- BR: rpm-pythonprov

Revision 1.5  2008-06-08 10:34:39  arekm
- rel 2; add man page

Revision 1.4  2008-06-08 10:31:54  arekm
- up to 0.2

Revision 1.3  2008-03-05 10:09:50  glen
- needs python 2.5; rel 2

Revision 1.2  2008-02-28 17:53:37  arekm
- rel 1; cleaned up

Revision 1.1  2008-02-28 17:42:15  czarny
- new
- stolen from FC
