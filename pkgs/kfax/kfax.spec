%global momorel 31
%global qtver 4.8.2
%global kdever 4.9.0
%global kdelibsrel 2m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global sourcedir stable/extragear

Summary: A fax viewer for KDE4
Name: kfax
Version: 3.3.6
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Communications
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}-kde4.4.0.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-link.patch
Patch1: %{name}-%{version}-kde4.4.0-kio.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}

%description
A fax viewer for KDE4.

%prep
%setup -q -n %{name}-%{version}-kde4.4.0
%patch0 -p1 -b .link
%patch1 -p1 -b .kio

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog NOTES README TODO
%{_bindir}/%{name}
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/%{name}
%{_datadir}/icons/*/*/*/%{name}.*
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-31m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.6-30m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.6-29m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-28m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.6-27m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-26m)
- enable to build with KDE 4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-25m)
- rebuild against qt-4.6.3-1m

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-24m)
- fix build with gcc-4.4.4

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.6-23m)
- touch up spec file

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-22m)
- new source for KDE 4.4.0

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-21m)
- new source for KDE 4.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.6-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-19m)
- NoSource again

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-18m)
- no NoSource for STABLE_6

* Fri Sep  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-17m)
- new source for KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-16m)
- new source for KDE 4.3.0

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-15m)
- new source for KDE 4.2.4

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-14m)
- new source for KDE 4.2.3

* Wed Apr 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-13m)
- new source for KDE 4.2.2

* Sun Feb  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-12m)
- new source for KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.6-11m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-10m)
- new source for KDE 4.2 RC

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-9m)
- rebuild from new source

* Sat Aug  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-8m)
- new source for KDE 4.1

* Fri Jul 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-7m)
- new source for KDE 4.1 RC 1

* Sun Jun  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-6m)
- source tarball was renamed for KDE 4.1 beta 1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-5m)
- rebuild against qt-4.4.0-1m
- source tarball was renamed

* Tue Apr  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-4m)
- update source tar ball

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.6-3m)
- rebuild against gcc43

* Sat Feb 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-2m)
- update source tar ball

* Mon Jan 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-1m)
- initial build for Momonga Linux 4
