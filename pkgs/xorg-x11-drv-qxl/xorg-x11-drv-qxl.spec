%global momorel 4

%global tarball xf86-video-qxl
%global moduledir %(pkg-config xorg-server --variable=moduledir )
%global driverdir %{moduledir}/drivers

Summary:   Xorg X11 qxl video driver
Name:      xorg-x11-drv-qxl

# This is hack since a driver got built with the version number 0.0.20.f14b
# The next upstream version will have version 0.2.0
Version:   0.1.1

Release:   %{momorel}m%{?dist}
URL:       http://www.x.org
Source0:   http://xorg.freedesktop.org/releases/individual/driver/%{tarball}-%{version}.tar.bz2
NoSource:  0
#Source0: %{tarball}-%{gitdate}.tar.bz2
#Source0: %{tarball}-%{gitdate}.tar.bz2
Patch1:    qxl-kms-disable-composite.patch

# This should go away with a spice server containing 1d18b7e98ab268c755933061d77ccc7a981815e2
Patch2:        0005-spiceqxl_display-only-use-qxl-interface-after-it-is-.patch

Patch3: no-surfaces-kms.patch
Patch4: 0001-worst-hack-of-all-time-to-qxl-driver.patch

# Fixes for running with Xorg suid, which is the only way we ship in fedora
Patch6: 0006-spiceqxl_spice_server-no-need-to-call-spice_server_s.patch
Patch7: 0007-xspice-chown-both-files-used-by-vdagent-for-suid-Xor.patch
Patch8: 0008-Xspice-cleanup-non-regular-files-too.patch
Patch9: 0009-Xspice-fix-cleanup-when-some-processes-are-already-d.patch
Patch10: 0010-Xspice-cleanup-vdagent-files.patch

# Support for server managed fds
Patch11:           0001-Add-support-for-XSERVER_PLATFORM_BUS.patch
Patch12:           0002-Fix-qxl_driver_func-to-adhere-to-the-API.patch
Patch13:           0003-Add-support-for-server-managed-fds.patch

# Support for old revision 1 qxl device (which won't go upstream)
# These aren't currently being applied, because they're not compatible with
# xserver 1.15.  They could be if someone wanted, but it's been 2.5 years,
# probably nobody's running that old of a RHEV anymore...
Patch20:	   0002-Add-old-driver-in-as-a-compatibility-layer.patch
Patch21:	   0003-Link-in-the-compat-driver-various-renamings.patch
Patch22:	   0004-compat-bump-to-new-server-API-changes.patch

License: MIT
Group:     User Interface/X Hardware Support

ExcludeArch: s390 s390x

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0
BuildRequires: spice-protocol >= 0.12.1
BuildRequires: glib2-devel
BuildRequires: libtool

Requires: xorg-x11-server-Xorg

%description 
X.Org X11 qxl video driver.

%package -n    xorg-x11-server-Xspice
Summary:       XSpice is an X server that can be accessed by a Spice client
Requires:      xorg-x11-server-Xorg
Requires:      python >= 2.6

%description -n xorg-x11-server-Xspice
XSpice is both an X and a Spice server.

%prep
%setup -q -n %{tarball}-%{version}
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1

%build
# fix build on i686 20140618 rev. 66287
%global optflags %{optflags} -Wno-error=pointer-to-int-cast

autoreconf -f -i
%configure --disable-static --enable-xspice
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} INSTALL='install -p'

# FIXME: Remove all libtool archives (*.la) from modules directory.  This
# should be fixed in upstream Makefile.am or whatever.
find %{buildroot} -regex ".*\.la$" | xargs rm -f --

mkdir -p %{buildroot}%{_sysconfdir}/X11
install -p -m 644 examples/spiceqxl.xorg.conf.example \
    %{buildroot}%{_sysconfdir}/X11/spiceqxl.xorg.conf
# FIXME: upstream installs this file by default, we install it elsewhere.
# upstream should just not install it and let dist package deal with
# doc/examples.
rm %{buildroot}/usr/share/doc/xf86-video-qxl/spiceqxl.xorg.conf.example

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README
%{driverdir}/qxl_drv.so

%files -n xorg-x11-server-Xspice
%defattr(-,root,root,-)
%doc COPYING README.xspice README examples/spiceqxl.xorg.conf.example
%config(noreplace) %{_sysconfdir}/X11/spiceqxl.xorg.conf
%{_bindir}/Xspice
%{driverdir}/spiceqxl_drv.so

%changelog
* Wed Jun 18 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-4m)
- fix build on i686

* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-3m)
- import fedora patches

* Sun Mar 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-2m)
- import fedora patches

* Fri Aug  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-1m)
- update 0.1.1-20130708

* Mon Oct  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.0-1m)
- update 0.1.0

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.22-3m)
- rebuild against xorg-x11-server

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.22-2m)
- rebuild for glib 2.33.2

* Tue May  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.22-1m)
- update 0.0.22

* Tue Jan 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.21-8m)
- add patch

* Sun Jan  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.21-7m)
- add patch

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.21-6m)
- rebuild against xorg-x11-server-1.11.99.901

* Mon Jan  2 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.21-5m)
- add patch20 (Build Fix only)

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.21-4m)
- add surface-bound-fix.patch

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.21-3m)
- base src update 0.0.16
- create xorg-x11-server-Xspice package

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.21-2m)
- rebuild against xorg-x11-server-1.10.99.901

* Sun Jun 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.21-1m)
- Initial commit Momonga Linux

* Wed Apr 20 2011 Hans de Goede <hdegoede@redhat.com> 0.0.21-3
- Add various bugfixes from upstream git
- Fixes VT-switching (rhbz#696711)
- Add support for old qxl device (from rhel6 branch) (rhbz#642153)

* Mon Mar 07 2011 Dave Airlie <airlied@redhat.com> 0.0.21-2
- Bump to for abi rebuild

* Sat Feb 12 2011 Soren Sandmann <ssp@redhat.com> 0.0.21-1
- New version number to make sure upgrading works

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0.13-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Jan 26 2011 Soren Sandmann <ssp@redhat.com> 0.0.13-1
- Update to 0.0.13 with surfaces

* Mon Dec 06 2010 Adam Jackson <ajax@redhat.com> 0.0.20.f14b-10
- Rebuild for new server ABI.

* Wed Oct 27 2010 Adam Jackson <ajax@redhat.com> 0.0.20.f14b-8
- Add ABI requires magic (#542742)

* Sun Oct 17 2010 Hans de Goede <hdegoede@redhat.com> 0.0.20.f14b-7
- Fix notification bubbles under gnome not showing (backport from the
  surface-fixes branch)

* Sun Oct 17 2010 Hans de Goede <hdegoede@redhat.com> 0.0.20.f14b-6
- Fix a pointer casting bug which causes the qxl driver to trigger an
  assertion in the qxl device terminating the entire virtual machine

* Mon Oct 11 2010 Hans de Goede <hdegoede@redhat.com> 0.0.20.f14b-5
- Don't access the qxl device when our vt is not focussed, this fixes
  Xorg crashing when switching to a text vc

* Sun Oct 10 2010 Hans de Goede <hdegoede@redhat.com> 0.0.20.f14b-4
- Fix the driver not working on qxl devices with a framebuffer of 8MB

* Sat Oct  9 2010 Hans de Goede <hdegoede@redhat.com> 0.0.20.f14b-3
- Add support for using resolutions > 1024x768 without needing an xorg.conf
- Restore textmode font when switching back to a textmode virtual console

* Fri Oct 08 2010 Jesse Keating <jkeating@redhat.com> - 0.0.20.f14b-2.1
- Rebuild for gcc bug 634757

* Tue Sep 14 2010 Soren Sandmann <ssp@redhat.com> 0.0.20.f14b-2
- Patch to fix it up for the new privates ABI (I had apparently been
  testing with a too old X server).

* Tue Sep 14 2010 Soren Sandmann <ssp@redhat.com> 0.0.20.f14b-1
- Add support for new device

* Sat Mar 13 2010 Dave Airlie <airlied@redhat.com> 0.0.12-2
- fix bug in qxl with asserts

* Sat Mar 13 2010 Dave Airlie <airlied@redhat.com> 0.0.12-1
- rebase to 0.0.12 release - fix some 16-bit bugs

* Mon Jan 11 2010 Dave Airlie <airlied@redhat.com> 0.0.9-0.1
- Initial public release 0.0.9
