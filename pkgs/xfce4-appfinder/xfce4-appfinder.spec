%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0
%global thunerver 1.6.3

Name: 		xfce4-appfinder
Version: 	4.11.0
Release:	%{momorel}m%{?dist}
Summary: 	Appfinder for the XFce4 Desktop Environment

Group: 		User Interface/Desktops
License:	GPL
URL: 		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/xfce/%{name}/%{xfcever}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires: libxfcegui4-devel >= %{xfce4ver}
BuildRequires: Thunar-devel >= %{thunerver}
BuildRequires: desktop-file-utils >= 0.12
BuildRequires: startup-notification-devel >= 0.9
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: garcon-devel
#Requires: libxfcegui4 >= %{xfce4ver}


%description
xfce-appfinder shows system wide installed applications

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}

# install desktop file (revise xfce4-appfinder.desktop)
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-only-show-in XFCE \
  --remove-category X-XFCE \
  %{buildroot}%{_datadir}/applications/xfce4-appfinder.desktop

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files
%defattr(-,root,root)
%doc README TODO ChangeLog NEWS INSTALL COPYING AUTHORS
%{_bindir}/*
%{_datadir}/applications/*
%{_datadir}/locale/*/*/*
#%%{_datadir}/icons/hicolor/48x48/apps/xfce4-appfinder.png

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.0-1m)
- update to 4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to 4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-3m)
- change Source0 URI

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-2m)
- fix build failure; add BuildRequires

* Wed May  4 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update
- rebuild against xfce4-4.8

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-2m)
- full rebuild for mo7 release

* Wed Aug  4 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-1m)
- update

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%define __libtoolize :

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- Xfce 4.4.0

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3-2m)
- remove-category X-XFCE

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3-2m)
- rebuild against libxfcegui4-4.2.3-2m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.2.3-2m)
- rebuild against expat-2.0.0-1m

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3-1m)
- Xfce 4.2.3.2

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1-1m)                                                   
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.0-1m)                                                   
- Xfce 4.2

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.99.3-2m)
- revise xfce4-appfinder.desktop
- BuildRequires: desktop-file-utils

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90
