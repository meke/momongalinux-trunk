%global momorel 1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%global alphatag a1
Name:           pyusb
Version:        1.0.0
Release:        0.%{alphatag}.%{momorel}m%{?dist}
Summary:        Python bindings for libusb
Group:          Development/Languages
License:        BSD
URL:            http://pyusb.sourceforge.net/
Source0:        pyusb-%{version}-%{alphatag}.tar.gz
#Source0:	http://downloads.sourceforge.net/project/%{name}/PyUSB%200.x/%{version}/%{name}-%{version}.tar.gz
#Nosource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel
BuildRequires: libusb-devel
BuildArch: noarch

%description
PyUSB provides easy USB access to python. The module contains classes and 
methods to support most USB operations.

%prep
%setup -q -n %{name}-%{version}-%{alphatag}


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build
sed -i -e 's/\r//g' README

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README LICENSE
%{python_sitelib}/*


%changelog
* Mon Sep 19 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-0.a1.1m)
- verion up 1.0.0-a1

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.3-1m)
- update 0.4.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-2m)
- full rebuild for mo7 release

* Wed Mar 11 2010 Daniel McLellan <daniel.mclellan@gmail.com> 0.4.2
- first spec file
