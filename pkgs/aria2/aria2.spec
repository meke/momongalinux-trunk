%global         momorel 1

Name:           aria2
Version:        1.17.1
Release:        %{momorel}m%{?dist}
Summary:        High speed download utility with resuming and segmented downloading
Group:          Applications/Internet
License:        GPLv2+
URL:            http://aria2.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.xz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  bison
BuildRequires:  c-ares-devel cppunit-devel
BuildRequires:  gettext gnutls-devel >= 3.2.0
BuildRequires:  libgcrypt-devel libxml2-devel
BuildRequires:  sqlite-devel
BuildRequires:  gettext
Requires:       bash-completion

%description
aria2 is a download utility with resuming and segmented downloading.
Supported protocols are HTTP/HTTPS/FTP/BitTorrent. It also supports Metalink
version 3.0.

Currently it has following features:
- HTTP/HTTPS GET support
- HTTP Proxy support
- HTTP BASIC authentication support
- HTTP Proxy authentication support
- FTP support(active, passive mode)
- FTP through HTTP proxy(GET command or tunneling)
- Segmented download
- Cookie support
- It can run as a daemon process.
- BitTorrent protocol support with fast extension.
- Selective download in multi-file torrent
- Metalink version 3.0 support(HTTP/FTP/BitTorrent).
- Limiting download/upload speed

%prep
%setup -q

%build
%configure --enable-bittorrent \
           --enable-metalink \
           --enable-epool \
           --disable-rpath \
           --with-gnutls \
           --with-libcares \
           --with-libxml2 \
           --with-openssl \
           --with-libz \
           --with-sqlite3 \
           --with-ca-bundle=%{_sysconfdir}/pki/tls/certs/ca-bundle.crt \
           --with-bashcompletiondir=%{_sysconfdir}/bash_completion.d

make %{?_smp_mflags}
# %make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang aria2
rm -f %{buildroot}%{_datadir}/locale/locale.alias
rm -rf %{buildroot}%{_datadir}/doc/aria2

%clean
rm -rf %{buildroot}

%files -f aria2.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README doc
%{_bindir}/aria2c
%{_sysconfdir}/bash_completion.d/aria2c
%{_mandir}/man1/%{name}*.1*
%{_mandir}/*/man1/%{name}*.1*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17.1-1m)
- update to 1.17.1
- rebuild against gnutls-3.2.0

* Thu May  2 2013 NARITA Koichi <pulsar[momonga-linux.org>
- (1.17.0-1m)
- update to 1.17.0

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16.5-1m)
- update to 1.16.5

* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16.3-1m)
- update to 1.16.3

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16.1-1m)
- update to 1.16.1

* Thu Sep 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15.2-1m)
- update to 1.15.2

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14.2-1m)
- update to 1.14.2

* Sat Jan 14 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (1.14.0-2m)
- sync Fedora 16
- use ca-bundle.crt
- add bash_completion

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14.0-1m)
- update to 1.14.0

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13.0-1m)
- update to 1.13.0

* Tue Aug 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.1-1m)
- update to 1.12.1

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.0-1m)
- update to 1.12.0

* Mon May 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.2-1m)
- update to 1.11.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.1-2m)
- rebuild for new GCC 4.6

* Sun Apr 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.1-1m)
- update to 1.11.1

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.0-1m)
- update to 1.11.0

* Tue Feb  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.9-1m)
- update to 1.10.9

* Tue Dec 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.8-1m)
- update to 1.10.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.6-2m)
- rebuild for new GCC 4.5

* Sat Nov 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.6-1m)
- update to 1.10.6

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.3-1m)
- update to 1.10.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.0-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.5-1m)
- update to 1.9.5

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3-1m)
- [SECURITY] CVE-2010-1512
- update to 1.9.3

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.1-1m)
- update to 1.9.1

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Wed Jan 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-1m)
- [SECURITY] CVE-2009-3617
- update to 1.6.2

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update to 1.5.1

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Sun Apr 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- [SECURITY] CVE-2009-3575 (fixed in 1.2.0)
- update to 1.3.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-2m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2
-- drop Patch0, not needed
-- build with --with-sqlite3, without --with-libcares
- License: GPLv2+

* Wed Nov 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-1m)
- update 1.0.1

* Wed Sep 10 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.3-1m)
- update 0.15.3

* Fri Aug 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.2-1m)
- update 0.15.2

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.0-2m)
- rebuild aginst gnutls-2.4.1

* Sun Jun 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.0-1m)
- update 0.14.0

* Sun May 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.2.1-1m)
- update 0.13.2+1

* Tue May 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.1.2-1m)
- update 0.13.1+2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13.1-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.1-1m)
- update 0.13.1

* Wed Mar  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.0-1m)
- Initial Commit Momonga Linux. based Fedora SPEC
