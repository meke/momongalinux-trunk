%global momorel 1

Summary: A C++ interface for the GTK+ (a GUI library for X).
Name: gtkmm
Version: 2.24.2
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.gtkmm.org/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/2.24/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: gtkmm-2.18.2-demo.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: perl
BuildRequires: glib2-devel >= 2.23.1
BuildRequires: gtk2-devel >= 2.19.6
BuildRequires: atk-devel >= 1.28.0
BuildRequires: pango-devel >= 1.26.0
BuildRequires: libsigc++-devel >= 2.2.4.2
BuildRequires: glibmm-devel >= 2.24.0
BuildRequires: cairomm-devel >= 1.8.2
BuildRequires: pangomm-devel >= 2.26.0
BuildRequires: atkmm-devel
BuildRequires: mm-common

%description
This package provides a C++ interface for GTK+ (the Gimp ToolKit) GUI
library.  The interface provides a convenient interface for C++
programmers to create GUIs with GTK+'s flexible object-oriented framework.
Features include type safe callbacks, widgets that are extensible using
inheritance and over 110 classes that can be freely combined to quickly
create complex user interfaces.
#'

%package devel
Summary: Headers for developing programs that will use Gtk--.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: gtk2-devel
Requires: atk-devel
Requires: pango-devel
Requires: libsigc++-devel
Requires: glibmm-devel
Requires: pangomm-devel
Requires: cairomm-devel >= 1.9.2
Requires: gtkmm-documentation

%description devel
This package contains the headers that programmers will need to develop
applications which will use Gtk--, the C++ interface to the GTK+
(the Gimp ToolKit) GUI library.

%prep
%setup -q

%build
%configure --enable-silent-rules --disable-static --enable-documentation \
	MMDOCTOOLDIR=%{_datadir}/mm-common/doctool
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/libgdkmm-2.4.so.*
%{_libdir}/libgtkmm-2.4.so.*
%exclude %{_libdir}/lib*.la

%files  devel
%defattr(-, root, root)
%{_libdir}/*.so
%{_libdir}/%{name}-2.4
%dir %{_libdir}/gdkmm-2.4
%dir %{_libdir}/gdkmm-2.4/include
%{_libdir}/gdkmm-2.4/include/gdkmmconfig.h
%{_libdir}/pkgconfig/*.pc
%{_includedir}/gtkmm-2.4
%{_includedir}/gdkmm-2.4
%doc %{_datadir}/devhelp/books/gtkmm-2.4/gtkmm-2.4.devhelp2
%doc %{_datadir}/doc/gtkmm-2.4

%changelog
* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-5m)
- rebuild for glib 2.33.2

* Sun Jul  3 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-4m)
- build fix with glibmm-2.28.2

* Tue Apr 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-3m)
- enable-documentation

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0
--disable-documentation

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-3m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-2m)
- add BuildRequires

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0
- good-bye demo tool

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.3-2m)
- full rebuild for mo7 release

* Tue May  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.6-1m)
- update to 2.19.6

* Sun Feb  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19.4-1m)
- update and rebuild against new gtk-2.19.4

* Wed Jan 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.2-1m)
- update to 2.19.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.2-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.2-4m)
- add BuildPrereq: mm-common

* Wed Oct  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-3m)
- add patch0 (install demos)

* Wed Oct  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-2m)
- delete gtkmm-demo pixbuf-demo (does not work ;-)

* Sun Oct  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-2m)
- add Req: gtkmm-documentation
- exclude conflict files

* Thu Sep 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.16.0-1m)
- add ./autogen.sh. need libtool-2.2.x
- change URL

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.5-1m)
- update to 2.15.5

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.3-1m)
- update to 2.15.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.3-2m)
- rebuild against rpm-4.6

* Sat Nov 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Tue Nov 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Fri Oct 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Thu Oct  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12-7-4m)
- [CRITICAL] fix Requires of devel

* Tue Oct  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.7-3m)
- add Requires: cairomm-devel

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0
* Sun Sep 14 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.7-2m)
- add gtk-2.14 patch

* Fri Apr  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.7-1m)
- update to 2.12.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.6-2m)
- rebuild against gcc43

* Wed Apr  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.6-1m)
- update to 2.12.6

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.5-1m)
- update to 2.12.5

* Sun Jan 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.4-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-1m)
- update to 2.12.4

* Thu Nov  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Tue Nov  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2

* Fri Oct  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0

* Thu May  3 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.10-1m)
- update to 2.10.10

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.8-1m)
- update to 2.10.8

* Fri Feb  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.7-1m)
- update to 2.10.7

* Thu Nov 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.5-1m)
- update to 2.10.5

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-1m)
- update to 2.10.4

* Sat Nov 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Sat Sep 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Sun Aug 27 2006 NARITA Koichi<pulsar@sea.plala.or.jp>
- (2.8.8-2m)
- rebuild against expat-2.0.0-1m

* Sat May 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.8-1m)
- update to 2.8.8

* Sat Jun 11 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Mon May 16 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2
- come back to MAIN for inkscape

* Mon Nov 08 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.7-1m)
- update to 2.4.7

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0-1m)
- version 2.4.0

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.8-2m)
- revised spec for enabling rpm 4.2.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.8-1m)
- version 2.2.8

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.7-1m)
- version 2.2.7

* Tue Jul 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.5-1m)
- version 2.2.5

* Sun Jul  6 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.3-2m)
- add gtkmm-gcc33.patch

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.3-1m)
- version 2.2.3

* Sun Mar 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Sun Mar 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Mon Jan 06 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Dec 16 2002 Shingo AKagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Mon Nov 18 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Thu Nov 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.26-1m)
- version 1.3.26

* Sat Oct  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.23-1m)
- version 1.3.23

* Mon Aug 19 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.21-1m)
- version 1.3.21

* Thu Aug 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.20-1m)
- version 1.3.20

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.18-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.18-1m)
- version 1.3.18

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.14-8k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.14-6k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.3.14-6k)
- cancel gcc-3.1 autoconf-2.53

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.14-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.14-2k)
- version 1.3.14

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-2k)
- version 1.3.13

* Fri Apr 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.11-4k)
- perl path patch

* Fri Apr 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.11-2k)
- version 1.3.11

* Fri Dec 14 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.8-2k)
- version 1.2.8

* Tue Sep  4 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.7-2k)
- version 1.2.7

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.5-6k)
- no more ifarch alpha.

* Thu Apr  5 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (1.2.5-5k)
  Change compiler from egcs++ to c++

* Mon Apr  2 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.5-3k)

* Tue May 02 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- Added defattr for doc package

* Tue Apr 25 2000 Koichi Asano <koichi@comm.info.eng.osaka-cu.ac.jp>
- separate documant packages

* Sun Feb 20 2000 Herbert Valerio Riedel <hvr@gnu.org>
- gnome-- and gtk-- are packaged separately

* Fri Jan 28 2000 Herbert Valerio Riedel <hvr@gnu.org>
- adapted to the new docs

* Sun Jan  2 2000 Herbert Valerio Riedel <hvr@gnu.org>
- examples should be makeable now

* Sun Dec 26 1999 Herbert Valerio Riedel <hvr@gnu.org>
- commented out manpages for now...

* Sat Dec 25 1999 Herbert Valerio Riedel <hvr@gnu.org>
- added dependancies on libsigc++

* Sat Nov  6 1999 Herbert Valerio Riedel <hvr@gnu.org>
- cleanup for 1.1.x
- changed rpm package name from Gtk-- to gtkmm
- removed that static hack

* Sat Oct 21 1999 Karl Einar Nelson <kenelson@ece.ucdavis.edu>
- Changed dist from Gtk--- to gtkmm-

* Sat Sep 11 1999 Herbert Valerio Riedel <hvr@gnu.org>
- added SMP support
- added custom release feature

* Sun Aug  1 1999 Herbert Valerio Riedel <hvr@gnu.org>
- Updated to gtk---1.1.x

* Thu Jul 29 1999 Herbert Valerio Riedel <hvr@gnu.org>
- Updated to gtk---1.0.x
- Merged in changes from redhat's gtk--.spec
- conditional build of static libraries by define 'STATIC'

* Thu May 10 1998 Bibek Sahu <scorpio@dodds.net>
- Upgraded to gtk---0.9.3

* Thu Apr 30 1998 Bibek Sahu <scorpio@dodds.net>
- Fixed problem with gtk---devel requiring libgtk-- (not gtk--).  Oops.

* Thu Apr 30 1998 Bibek Sahu <scorpio@dodds.net>
- Fixed problem with most of the headers not being included.

* Thu Apr 30 1998 Bibek Sahu <scorpio@dodds.net>
- Upgraded to gtk---0.9.1

* Tue Apr 28 1998 Bibek Sahu <scorpio@dodds.net>
- Fixed to build gtk-- and gtk---devel packages.

* Tue Apr 28 1998 Bibek Sahu <scorpio@dodds.net>
- First (s)rpm build.


