%global momorel 1

%define lang es
%define langrelease 2
Summary: Spanish dictionaries for Aspell
Name: aspell-%{lang}
Version: 1.11
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell6-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Buildrequires: aspell >= 0.60.6-8m
Requires: aspell >= 0.60.6-8m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for the following: Spanish

%prep
%setup -q -n aspell6-%{lang}-%{version}-%{langrelease}
iconv -f windows-1252 -t utf-8 Copyright > Copyright.aux
mv Copyright.aux Copyright

%build
./configure
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING Copyright README
%{_libdir}/aspell-0.60/*

%changelog
* Wed Sep  7 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.11-1m)
- version up 1.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.50-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.50-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.50-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.50-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.50-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.50-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.50-1m)
- import from Fedora

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 50:0.50-17
- Autorebuild for GCC 4.3

* Thu Mar 29 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50-16
- add documentation

* Thu Mar 29 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50-15
- use configure script to create Makefile
- update default buildroot

* Wed Jan 24 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50-14
- spec file cleanup
- fix 224147 - rawhide rebuild fails

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-13.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-13.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-13.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Aug 25 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-13
- add castellano alias (bug 166286)

* Mon Jul 18 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-12
- build with aspell-0.60.3

* Mon Apr 11 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-11
- rebuilt

* Wed Sep 29 2004 Adrian Havill <havill@redhat.com> 50:0.50-10
- remove debuginfo, convert latin1 filename to utf-8

* Wed Aug 11 2004 Adrian Havill <havill@redhat.com> 50:0.50-2
- synced epoch with other aspell dicts

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jun 23 2003 Adrian Havill <havill@redhat.com> 0.50-5
- remove buildarch; data files are platform dependent

* Fri Jun 20 2003 Adrian Havill <havill@redhat.com> 0.50-4
- first build for new aspell (0.50)
