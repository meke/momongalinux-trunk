%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%{!?python_version: %global python_version %(%{__python} -c "from distutils.sysconfig import get_python_version; print(get_python_version())")}

%global momorel 1
%global src_name tdb

Name:           libtdb
Version:        1.2.13
Release:        %{momorel}m%{?dist}
Group:          System Environment/Daemons
Summary:        The tdb library
License:        LGPLv3+
URL:            http://tdb.samba.org/
Source:         http://samba.org/ftp/%{src_name}/%{src_name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  autoconf

%description
A library that implements a trivial database.

%package devel
Group: Development/Libraries
Summary: Header files need to link the Tdb library
Requires: libtdb = %{version}-%{release}
Requires: pkgconfig

%description devel
Header files needed to develop programs that link against the Tdb library.

%package -n tdb-tools
Group: Development/Libraries
Summary: Developer tools for the Tdb library
Requires: libtdb = %{version}-%{release}

%description -n tdb-tools
Tools to manage Tdb files

%package -n python-tdb
Group: Development/Libraries
Summary: Python bindings for the Tdb library
Requires: libtdb = %{version}-%{release}

%description -n python-tdb
Python bindings for libtdb

%prep
%setup -q -n %{src_name}-%{version}

%build
#./autogen.sh
%configure --disable-rpath --bundled-libraries=NONE
make %{?_smp_mflags} V=1

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# Shared libraries need to be marked executable for
# rpmbuild to strip them and include them in debuginfo
find $RPM_BUILD_ROOT -name "*.so*" -exec chmod -c +x {} \;

rm -f $RPM_BUILD_ROOT%{_libdir}/libtdb.a

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libtdb.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/tdb.h
%{_libdir}/libtdb.so
%{_libdir}/pkgconfig/tdb.pc

%files -n tdb-tools
%defattr(-,root,root,-)
%{_bindir}/tdbbackup
%{_bindir}/tdbdump
%{_bindir}/tdbrestore
%{_bindir}/tdbtool
%{_mandir}/man8/tdbbackup.8*
%{_mandir}/man8/tdbdump.8*
%{_mandir}/man8/tdbrestore.8*
%{_mandir}/man8/tdbtool.8*

%files -n python-tdb
%defattr(-,root,root,-)
%{python_sitearch}/tdb.so

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post -n python-tdb -p /sbin/ldconfig

%postun -n python-tdb -p /sbin/ldconfig

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.13-1m)
- update to 1.2.13

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.12-1m)
- update to 1.2.12

* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.11-1m)
- update to 1.2.11

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.10-1m)
- update to 1.2.10

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.9-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.9-1m)
- update to 1.2.9
- separate python-tdb subpackage

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-2m)
- full rebuild for mo7 release

* Wed Apr  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Wed Dec 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-1m)
- import from Fedora devel

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Jun 17 2009 Simo Sorce <ssorce@redhat.com> - 1.1.5-1
- Original tarballs had a screw-up, rebuild with new fixed tarballs from
  upstream.

* Tue Jun 16 2009 Simo Sorce <ssorce@redhat.com> - 1.1.5-0
- New upstream release

* Wed May 6 2009 Simo Sorce <ssorce@redhat.com> - 1.1.3-15
- First public independent release from upstream
