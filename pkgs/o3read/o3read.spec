%global momorel 6

Summary:	Standalone converter for OpenOffice.org documents
Name:		o3read
Version:	0.0.4
Release:	%{momorel}m%{?dist}
Group:		Development/Tools
License:	GPLv2+
URL:		http://siag.nu/o3read/
Source0:	http://siag.nu/pub/o3read/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:		o3read-makefile.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Conflicts:	tracker < 0.6.4-4

%description
o3read is a standalone converter for the OpenOffice.org writer and calc
documents to text, html, and a dump of the parse tree.
It doesn't depend on Open Office or any other external tools or libraries.

%prep
%setup -q
%patch0 -p0 -b .mak

%build
make %{?_smp_mflags} CFLAGS="%{optflags}" 

%install
rm -rf %buildroot
make install DESTDIR=%buildroot PREFIX=%{_prefix} MANNDIR=%{_mandir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING README TODO
%{_bindir}/o3*
%{_bindir}/utf*
%{_mandir}/man1/*.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.4-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.4-1m)
- import from Fedora

* Sun Feb 10 2008 Deji Akingunola <dakingun@gmail.com> - 0.0.4-2
- Rebuild for gcc43

* Sun Jan 20 2008 Deji Akingunola <dakingun@gmail.com> - 0.0.4-1
- Initial build for Fedora
