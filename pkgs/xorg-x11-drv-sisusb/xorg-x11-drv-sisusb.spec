%global momorel 1
%define tarball xf86-video-sisusb
# All packages should own all dirs they lead up to, for saner
# rpm packaging, in particular if the leading dirs are not owned by
# the "filesystem" package.
%define moduledir %(pkg-config xorg-server --variable=moduledir )
# define driverdir to the appropriate type for this driver class
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 sisusb video driver
Name:      xorg-x11-drv-sisusb
Version: 0.9.6
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# FIXME:  If you can see this comment, then the ExclusiveArch entry below
# is set to the spec file template default.  Please examine the monolithic
# xorg-x11 package for each architecture for this driver, to determine which
# architectures it should be built or not built on, and update the list
# below.  When doing this, take "sparc, sparc64, alpha" also into
# consideration if you know a given driver was built for those arches in
# the past.  That makes it easier for the community Alphacore/AuroraLinux
# projects to rebuild our rpm packages.  If you're not sure if a given
# driver should be on any of those arches however, just leave it out of
# the list, and the community can let us know which drivers they are
# missing.  Remove this comment once the ExclusiveArch line is updated and
# considered reliable/correct.
ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires:  xorg-x11-server-Xorg >= 1.13.0
%description 
X.Org X11 sisusb video driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
autoreconf -ivf
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
# FIXME: This should be using makeinstall macro instead.  Please test
# makeinstall with this driver, and if it works, check it into CVS. If
# it fails, fix it in upstream sources and file a patch upstream.
make install DESTDIR=$RPM_BUILD_ROOT

# FIXME: Remove all libtool archives (*.la) from modules directory.  This
# should be fixed in upstream Makefile.am or whatever.
find $RPM_BUILD_ROOT -regex ".*.la$" | xargs rm -f --

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
# FIXME: A glob is used here in the specfile template to make it easy to
# generate spec files automatically.  When everything is in CVS, if you're
# updating the spec file, please change the glob to a list of explicit
# filenames, so that rpm tracks individual files, and we know when a new
# one gets added.  Explicitly naming files also helps avoid random
# unexpected .so files (or others) from getting included in a shipping
# product.  Ditto for the manpages.
%{driverdir}/*.so
# NOTE: Uncomment when multimedia drivers are avail
#%%dir Mon Jul  5 21:06:50 JST 2010
#%%{moduledir}/multimedia
#%%{moduledir}/multimedia/*.so
%{_mandir}/man4/*.4*

%changelog
* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6-1m)
- update 0.9.6

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-7m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-6m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-5m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.4-4m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-2m)
- full rebuild for mo7 release

* Mon Jul  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.4-1m)
- update to 0.9.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.3-2m)
- rebuild against xorg-x11-server-1.7.1

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3-1m)
- update to 0.9.3

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-1m)
- update 0.9.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-2m)
- rebuild against rpm-4.6

* Sun May  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-1m)
- update 0.9.0
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-4m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-3m)
- rebuild against xorg-x11-server-1.4

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-1m)
- update 0.8.1(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.1.3-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1.3-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.7.1.3-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.7.1.3-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 0.7.1.3-1
- Updated xorg-x11-drv-sisusb to version 0.7.1.3 from X11R7.0
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 0.7.1.2-1
- Updated xorg-x11-drv-sisusb to version 0.7.1.2 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 0.7.1-1
- Updated xorg-x11-drv-sisusb to version 0.7.1 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 0.7.0.1-1
- Updated xorg-x11-drv-sisusb to version 0.7.0.1 from X11R7 RC1
- Fix *.la file removal.
* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 0.7.0-0
- Initial spec file for sisusb video driver generated automatically
  by my xorg-driverspecgen script.
