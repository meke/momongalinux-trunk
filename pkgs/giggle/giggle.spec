%global momorel 1
%global with_evo 0

Summary: graphical frontend for the git directory tracker
Name: giggle
Version: 0.6.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Productivity
URL: http://live.gnome.org/giggle

Source0: http://ftp.gnome.org/pub/GNOME/sources/giggle/0.6/giggle-%{version}.tar.xz
NoSource: 0
Patch0: giggle-0.5-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.14.1
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 1.12.0
BuildRequires: gtksourceview2-devel
BuildRequires: libglade2-devel >= 2.6.2
%if %{with_evo}
BuildRequires: evolution-data-server-devel >= 3.1
%endif
BuildRequires: intltool
BuildRequires: git

%description
Giggle is a Gtk frontend to the git directory tracker. With Giggle you
will be able to visualize and browse easily the revision tree, view
changed files and differences between revisions, visualize summarized
info for the project, commit changes and other useful tasks for any
git managed projects contributor.

%package devel
Summary: Libraries and headers for developing Giggle plugins
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package provides the libraries and headers for developing Giggle
plugins.

%prep
%setup -q
#%patch0 -p1 -b .linking

%build
# giggle 0.6 requires this
export LDFLAGS="`pkg-config gmodule-2.0 --libs`"
%configure --enable-silent-rules \
%if %{with_evo}
	--enable-evolution-data-server
%else
	--disable-evolution-data-server
%endif

%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
update-desktop-database &> /dev/null || :
%{_bindir}/gtk-update-icon-cache --quiet --force --ignore-theme-index %{_datadir}/icons/hicolor || :

%postun
sbin/ldconfig
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
%{_bindir}/gtk-update-icon-cache --quiet --force --ignore-theme-index %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
%{_libdir}/libgiggle.so.*
%{_libdir}/libgiggle-git.so.*
%exclude %{_libdir}/*.la

%dir %{_libdir}/giggle
%dir %{_libdir}/giggle/plugins
%dir %{_libdir}/giggle/plugins/%{version}
%{_libdir}/giggle/plugins/%{version}/libpersonal-details.*
%{_libdir}/giggle/plugins/%{version}/libterminal-view.*
%{_libdir}/giggle/plugins/%{version}/personal-details.xml
%{_libdir}/giggle/plugins/%{version}/terminal-view.xml

%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/apps/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/giggle
%{_libdir}/libgiggle.so
%{_libdir}/libgiggle-git.so
%doc %{_datadir}/help/*/giggle

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-1m)
- update 0.6.2

* Tue Jul 31 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-3m)
- disable evolution support

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-2m)
- fix build failure with glib 2.33+

* Fri Oct 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-10m)
- rebuild against evolution-data-server-3.1.12

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-9m)
- rebuild against evolution-data-server-3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-7m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-6m)
- rebuild against evolution-2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-5m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-4m)
- rebuild against evolution-2.30.2

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-3m)
- build fix once more (Patch0) and remove -lgiggle from LIBS (niwatama)

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-2m)
- build fix gcc-4.4.4

* Thu Apr 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-1m)
- update 0.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.91-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.91-1m)
- update to 0.4.91
-- remove ja.po, merged upstream
- split off devel package

* Sat Jan 24 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-3m)
- fix BuildPreReq:

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-2m)
- rebuild against rpm-4.6

* Tue Jul 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-1m)
- update 0.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-3m)
- %%NoSource -> NoSource

* Thu Oct 11 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BUildPreReq: git

* Mon Oct  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-2m)
- add ja.po

* Mon Oct  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3-1m)
- initial build
