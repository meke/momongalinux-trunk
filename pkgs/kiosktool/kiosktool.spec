%global momorel 13
%global date 20070413
%global svndate 13.04.07
%global artsver 1.5.10
%global artsrel 2m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: KIOSK administration tool for KDE
Name: kiosktool
Version: 1.0
Release: 0.%{date}.%{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Base
URL: http://extragear.kde.org/apps/kiosktool/
Source0: %{name}-%{svndate}.tar.bz2
Source10: admin.tar.bz2
Patch0: %{name}-%{svndate}-es-doc.patch
Patch10: %{name}-%{svndate}-automake111.patch
Patch11: %{name}-%{svndate}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: libxml2

%description
A Point and Click tool for system administrators to enable KDE's KIOSK 
features or otherwise preconfigure KDE for groups of users. 

%prep
%setup -q -n %{name}-%{svndate}

%patch0 -p1 -b .fix-doc

# fix for new automake
rm -rf admin
tar xf %{SOURCE10}

%patch10 -p1 -b .automake111
%patch11 -p1 -b .automake265

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# for profiles
mkdir -p %{buildroot}%{_sysconfdir}/kde-profile

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING ChangeLog README TODO src/COPYING-DOCS
%dir %{_sysconfdir}/kde-profile
%{_bindir}/%{name}
%{_bindir}/%{name}-kdedirs
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/apps/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.20070413.13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.20070413.12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.20070413.11m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.20070413.10m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Tue Mar 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.20070413.9m)
- revise Requires: kdelibs3 >= %%{kdever}-%%{kdelibsrel}

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.20070413.8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.20070413.7m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.20070413.6m)
- rebuild against rpm-4.6

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.20070413.5m)
- revise %%{_docdir}/HTML/*/kiosktool/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.20070413.4m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.20070413.3m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.20070413.2m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Fri Apr 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.20070413.1m)
- initial package for Momonga Linux
