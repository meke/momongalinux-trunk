%global momorel 3

Name: sound-juicer
Summary: Clean and lean CD ripper
Version: 2.32.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.32/%{name}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libmusicbrainz3
Requires: libgnomeui
Requires: glib2
Requires: gtk2
Requires: gstreamer
Requires: GConf2
Requires: gstreamer-plugins-base
Requires: gstreamer-plugins-good
Requires: gstreamer-plugins-ugly
Requires: cdparanoia
Requires: gnome-media
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: dbus-glib-devel >= 0.80
BuildRequires: gstreamer-devel >= 0.10.22
BuildRequires: gstreamer-plugins-base-devel >= 0.10.22
BuildRequires: gnome-media-devel >= 2.26.0
BuildRequires: libmusicbrainz3-devel
BuildRequires: libcdio-devel >= 0.82
BuildRequires: taglib-devel >= 1.5
BuildRequires: brasero-devel >= 2.32.0
BuildRequires: libcanberra-devel >= 0.11

BuildRequires: hal-devel
BuildRequires: rarian
BuildRequires: gcc-c++
Requires(pre): hicolor-icon-theme gtk2

# http://bugzilla.gnome.org/show_bug.cgi?id=157772
ExcludeArch: s390
ExcludeArch: s390x

%description
GStreamer-based CD ripping tool. Saves audio CDs to Ogg/vorbis.

%prep
%setup -q

%build
%configure --disable-scrollkeeper --disable-schemas-install --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_datadir}/icons/hicolor/icon-theme.cache

%clean
rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source` 
gconftool-2 --makefile-install-rule \
 %{_sysconfdir}/gconf/schemas/sound-juicer.schemas \
 > /dev/null || :
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
rarian-sk-update

%preun
if [ "$1" -eq 0 ]; then
 export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source` 
 gconftool-2 --makefile-uninstall-rule \
 %{_sysconfdir}/gconf/schemas/sound-juicer.schemas \
 > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
 export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
 gconftool-2 --makefile-uninstall-rule \
 %{_sysconfdir}/gconf/schemas/sound-juicer.schemas \
 > /dev/null || :
fi


%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README NEWS TODO
%{_bindir}/sound-juicer
%{_sysconfdir}/gconf/schemas/sound-juicer.schemas
%{_datadir}/sound-juicer
%{_datadir}/applications/sound-juicer.desktop
%{_datadir}/gnome/help/sound-juicer
%{_datadir}/omf/sound-juicer
%{_datadir}/locale/*/*/*
%{_datadir}/icons/hicolor/*/apps/sound-juicer.*
%{_mandir}/man1/%{name}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.31.6-4m)
- full rebuild for mo7 release

* Thu Aug 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.6-3m)
- add Req gstreamer-plugins-ugly
- [BTS 281]

* Sun Aug 15 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.6-2m)
- revise Requires: and BuildRequires:

* Sat Aug 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.6-1m)
- update to 2.31.6 (unstable) (2.28.2 is crush with libcdio-0.82)

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.28.2-3m)
- rebuild against libcdio-0.82

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-2m)
- delete __libtoolize hack

* Sat Nov 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.1-3m)
- rebuild against libcdio-0.81

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.26.1-2m)
- define __libtoolize (build fix)

* Sat Apr 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.3-1m)
- update to 2.25.3
- del Requires: nautilus-cd-burner

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.2-2m)
- add BuildRequires: brasero-devel >= 2.25.90

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-1m)
- update to 2.25.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Thu Oct 2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.0-4m)
- change %%preun script

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- change Requires: gst-plugins-base to gstreamer-plugins-base
- change Requires: gst-plugins-good to gstreamer-plugins-good

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.1-2m)
- %%NoSource -> NoSource

* Mon Oct 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.4-1m)
- update to 2.16.4

* Sat Feb 3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Thu Oct 5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep 7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.5-1m)
- update to 2.14.5

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-1m)
- update to 2.14.4

* Tue May 2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.3-3m)
- rebuild against dbus-0.61

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.3-2m)
- rebuild against libmusicbrainz-2.1.2

* Sun Apr 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Sat Apr 8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Tue Jan 3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-2m)
- rebuild against dbus-0.60-2m

* Mon Dec 5 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3
- GNOME 2.12.2 Desktop

* Tue Nov 22 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- inport from FC
- version down ;-P
- GNOME 2.12.1 Desktop

* Wed Aug 17 2005 Matthias Clasen <mclasen@redhat.com> 2.13.1-1
- Update to 2.13.1

* Wed Aug 17 2005 Matthias Clasen <mclasen@redhat.com> 2.11.91-1
- Newer upstream version

* Tue Jul 12 2005 Matthias Clasen <mclasen@redhat.com> 2.11.3-1
- Newer upstream version

* Mon Apr 04 2005 John (J5) Palmieri <johnp@redhat.com> 2.10.1-1
- update to upstream 2.10.1 which should fix crashes when clicking
 extract

* Wed Mar 23 2005 John (J5) Palmieri <johnp@redhat.com> 2.10.0-2
- Rebuild for libmusicbrainz-2.1.1

* Fri Mar 11 2005 John (J5) Palmieri <johnp@redhat.com> 2.10.0-1
- Update to upstream version 2.10.0 

* Tue Mar 08 2005 John (J5) Palmieri <johnp@redhat.com> 2.9.91-3
- Build in rawhide
- Disable build on s390 and s390x

* Fri Feb 25 2005 John (J5) Palmieri <johnp@redhat.com> 2.9.91-2
- Reenabled BuildRequires for hal-devel >= 0.5.0
- Added (Build)Requires for nautilus-cd-burner(-devel) >= 2.9.6

* Wed Feb 23 2005 John (J5) Palmieri <johnp@redhat.com> 2.9.91-1
- New upstream version (version jump resulted from sound-juicer using gnome
 versioning scheme)
 
* Fri Feb 04 2005 Colin Walters <walters@redhat.com> 0.6.0-1
- New upstream version
- Remove obsoleted sound-juicer-idle-safety.patch
- BR latest gnome-media

* Fri Nov 12 2004 Warren Togami <wtogami@redhat.com> 0.5.14-5
- minor spec cleanups
- req cdparanoia and gstreamer-plugins

* Tue Nov 09 2004 Colin Walters <walters@redhat.com> 0.5.14-4
- Add sound-juicer-idle-safety.patch (bug 137847)

* Wed Oct 27 2004 Colin Walters <walters@redhat.com> 0.5.14-2
- Actually enable HAL
- BR hal-devel

* Wed Oct 13 2004 Colin Walters <walters@redhat.com> 0.5.14-1
- New upstream
- This release fixes corruption on re-read, upstream 153085
- Remove upstreamed sound-juicer-0.5.13-prefs-crash.patch

* Mon Oct 04 2004 Colin Walters <walters@redhat.com> 0.5.13-2
- Apply patch to avoid prefs crash

* Tue Sep 28 2004 Colin Walters <walters@redhat.com> 0.5.13-1
- New upstream 0.5.13

* Mon Sep 27 2004 Colin Walters <walters@redhat.com> 0.5.12.cvs20040927-1
- New upstream CVS snapshot, 20040927

* Mon Sep 20 2004 Colin Walters <walters@redhat.com> 0.5.12-1
- New upstream version 0.5.12
- Delete upstreamed patch sound-juicer-0.5.9-pref-help.patch
- Delete upstreamed patch sound-juicer-0.5.10-gstreamer.patch
- Delete call to autoconf

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 16 2004 Jeremy Katz <katzj@redhat.com> 0.5.10.1-8
- rebuild for new gstreamer

* Thu Mar 11 2004 Brent Fox <bfox@redhat.com> 0.5.10.1-5
- rebuild

* Fri Feb 27 2004 Brent Fox <bfox@redhat.com> 0.5.10.1-3
- rebuild

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Feb 5 2004 Brent Fox <bfox@redhat.com> 0.5.10.1-1
- new version

* Wed Jan 28 2004 Alexander Larsson <alexl@redhat.com> 0.5.9-4
- rebuild to use new gstreamer

* Fri Jan 16 2004 Brent Fox <bfox@redhat.com> 0.5.9-3
- add %preun to clean up GConf entries on uninstall

* Wed Jan 14 2004 Brent Fox <bfox@redhat.com> 0.5.9-2
- create init patch to make help work

* Tue Jan 13 2004 Brent Fox <bfox@redhat.com> 0.5.9-1
- update to 0.5.9

* Mon Dec 15 2003 Christopher Blizzard <blizzard@redhat.com> 0.5.8-1
- Add upstream patch that fixes permissions of created directories.

* Wed Dec 03 2003 Christopher Blizzard <blizzard@redhat.com> 0.5.8-0
- Update to 0.5.8

* Tue Oct 21 2003 Brent Fox <bfox@redhat.com> 0.5.5-1
- update to 0.5.5-1

* Mon Sep 1 2003 Jonathan Blandford <jrb@redhat.com>
- warning dialog fix
- add a quality option

* Fri Aug 29 2003 Elliot Lee <sopwith@redhat.com> 0.5.2-5
- scrollkeeper stuff should be removed

* Wed Aug 27 2003 Brent Fox <bfox@redhat.com> 0.5.2-4
- remove ExcludeArches since libmusicbrainz is building on all arches now

* Wed Aug 27 2003 Brent Fox <bfox@redhat.com> 0.5.2-3
- bump relnum

* Wed Aug 27 2003 Brent Fox <bfox@redhat.com> 0.5.2-2
- spec file cleanups
- add exclude arch for ia64, x86_64, ppc64, and s390x
- add %file macros 
- remove Requires for gstreamer-cdparanoia and gstreamer-vorbis

* Tue Apr 22 2003 Frederic Crozat <fcrozat@mandrakesoft.com>
- Use more macros

* Sun Apr 20 2003 Ronald Bultje <rbultje@ronald.bitfreak.net>
- Make spec file for sound-juicer (based on netRB spec file)
