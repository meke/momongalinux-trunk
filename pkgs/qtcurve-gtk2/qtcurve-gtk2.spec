%global         momorel 1
%global         srcname QtCurve-Gtk2

Name:		qtcurve-gtk2
Version:	1.8.16
Release:	%{momorel}m%{?dist}
Summary:	This is a set of widget styles for Gtk2 based apps
Group:		User Interface/Desktops
License:	GPLv2+
URL:		http://www.kde-look.org/content/show.php?content=40492
Source0:	http://craigd.wikispaces.com/file/view/%{srcname}-%{version}.tar.bz2
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk2-devel
BuildRequires:  kdebase-workspace-devel

%description
QtCurve is a desktop theme for the GTK+ and Qt widget toolkits,
allowing users to achieve a uniform look between these widget toolkits.

%prep
%setup -q -n %{srcname}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast -C %{_target_platform} DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%{_kde4_libdir}/gtk-2.0/*/engines/libqtcurve.so
%{_kde4_datadir}/themes/QtCurve

%changelog
* Sat Jan 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.16-1m)
- update to 1.8.16

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.15-1m)
- update to 1.8.15

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.14-1m)
- update to 1.8.14

* Tue Oct 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.11-1m)
- update to 1.8.11

* Mon Jul  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.9-1m)
- update to 1.8.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.8-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.8-1m)
- update to 1.8.8

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.7-1m)
- update to 1.8.7

* Wed Feb 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.6-1m)
- update to 1.8.6

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Mon Jan  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Sat Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Tue Dec  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-1m)
- update to 1.7.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- import from Fedora devel and update to 1.5.2

* Thu Jul 08 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.5.1-1
- qtcurve-gtk2 1.5.1 bugfix release

* Thu Jul 01 2010 thomas Janssen <thomasj@fedoraproject.org> 1.5.0-1
- qtcurve-gtk2 1.5.0

* Sun Jun 06 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.4.1-1
- qtcurve-gtk2 1.4.1

* Mon May 17 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.3.0-1
- qtcurve 1.4.0

* Mon Apr 12 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.3.0-1
- qtcurve 1.3.0

* Tue Mar 02 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.1.1-1
- New upstream source 1.1.1

* Tue Feb 23 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.1.0-1
- New upstream source 1.1.0

* Mon Feb 01 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.0.2-1
- Update to new upstream source 1.0.2

* Mon Jan 11 2010 Thomas Janssen <thomasj@fedoraproject.org> 1.0.1-1
- Update to source 1.0.1

* Sun Nov 01 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.69.2-1
- New upstream source 0.69.2

* Mon Sep 28 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.69.0-1
- Updated source to version 0.69.0
- Dropped unneeded buildrequires
- Dropped requires qtcurve-kde4
- removed post and postun
- removed _kde4 macros from files

* Mon Sep 28 2009 Thomas Janssen <thomasj@fedoraproject.org> 0.68.1-1
- Initial RPM release 0.68.1-1
