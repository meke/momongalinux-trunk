%global         momorel 7

Name:           perl-Chart
Version:        2.4.6
Release:        %{momorel}m%{?dist}
Summary:        Series of charting modules
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Chart/
Source0:        http://www.cpan.org/authors/id/C/CH/CHARTGRP/Chart-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-GD >= 2
Requires:       perl-GD >= 2
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
These man-pages give you the most important information about Chart. There
is also a complete documentation (Documentation.pdf) within the Chart
package. Look at it to get more information. This module is an attempt to
build a general purpose graphing module that is easily modified and
expanded. I borrowed most of the API from Martien Verbruggen's GIFgraph
module. I liked most of GIFgraph, but I thought it was to difficult to
modify, and it was missing a few things that I needed, most notably
legends. So I decided to write a new module from scratch, and I've designed
it from the bottom up to be easy to modify. Like GIFgraph, Chart uses
Lincoln Stein's GD module for all of its graphics primitives calls.

%prep
%setup -q -n Chart-%{version}
chmod -c 644 Chart/*.pm TODO Documentation.pdf
rm -f pm_to_blib

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README TODO Documentation.pdf
%{perl_vendorlib}/Chart*
%{_mandir}/man3/Chart.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-7m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-6m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-2m)
- rebuild against perl-5.16.2

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-1m)
- update to 2.4.6

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-2m)
- rebuild against perl-5.16.0

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.5-1m)
- update to 2.4.5

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-7m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-2m)
- rebuild against perl-5.12.2

* Fri Sep 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.1-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-3m)
- rebuild against perl-5.10.1

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-2m)
- fix BuildRequres

* Tue Feb  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.1-1m)
- import from Fedora to Momonga for dnssec-tools depends on perl-QWizard

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 2.4.1-6
Rebuild for new perl

* Mon Apr 09 2007 Steven Pritchard <steve@kspei.com> 2.4.1-5
- Fix find option order.
- Use fixperms macro instead of our own chmod incantation.
- BR ExtUtils::MakeMaker.
- Minor spec cleanup to more closely resemble cpanspec output.

* Mon Aug 27 2006 Michael J. Knox <michael[AT]knox.net.nz> - 2.4.1-4
- Rebuild for FC6

* Mon May 29 2006 Michael J. Knox <michael[AT]knox.net.nz> - 2.4.1-3
- rebuilt and reimported in to devel

* Wed Jan 25 2006 Ville Skytta <ville.skytta at iki.fi> - 2.4.1-1
- 2.4.1.
- Don't ship rgb.txt in docs.
- Specfile cleanups.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 2.3-3
- rebuilt

* Sun Jul 11 2004 Ville Skytta <ville.skytta at iki.fi> - 0:2.3-2
- Bring up to date with current fedora.us Perl Spec template.

* Thu Jan 15 2004 Ville Skytta <ville.skytta at iki.fi> - 0:2.3-0.fdr.1
- Update to 2.3.
- Fix file permissions.

* Sat Oct 11 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.2-0.fdr.1
- First build.
