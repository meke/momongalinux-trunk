%global momorel 6

%define luaver 5.1
%define lualibdir %{_libdir}/lua/%{luaver}
%define luapkgdir %{_datadir}/lua/%{luaver}

Name:           lua-sql
Version:        2.1.1
Release:        %{momorel}m%{?dist}
Summary:        Database connectivity for the Lua programming language

Group:          Development/Libraries
License:        MIT
URL:            http://www.keplerproject.org/luasql/
Source0:        http://luaforge.net/frs/download.php/2686/luasql-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  lua >= %{luaver}, lua-devel >= %{luaver}
BuildRequires:  pkgconfig
BuildRequires:  sqlite-devel >= 3.0
BuildRequires:  mysql-devel >= 5.5.10
BuildRequires:  postgresql-devel

Requires:       lua-sql-sqlite, lua-sql-mysql, lua-sql-postgresql, lua-sql-doc

%description
LuaSQL is a simple interface from Lua to a DBMS. This package of LuaSQL
supports MySQL, SQLite and PostgreSQL databases. You can execute arbitrary SQL
statements and it allows for retrieving results in a row-by-row cursor fashion.

%package doc
Summary:        Documentation for LuaSQL
Group:          Documentation
Requires:       lua >= %{luaver}
%description doc
LuaSQL is a simple interface from Lua to a DBMS. This package contains the
documentation for LuaSQL.


%package sqlite
Summary:        SQLite database connectivity for the Lua programming language
Group:          Development/Libraries
Requires:       lua >= %{luaver}
%description sqlite
LuaSQL is a simple interface from Lua to a DBMS. This package provides access
to SQLite databases.


%package mysql
Summary:        MySQL database connectivity for the Lua programming language
Group:          Development/Libraries
Requires:       lua >= %{luaver}
%description mysql
LuaSQL is a simple interface from Lua to a DBMS. This package provides access
to MySQL databases.


%package postgresql
Summary:        PostgreSQL database connectivity for the Lua programming language
Group:          Development/Libraries
Requires:       lua >= %{luaver}
%description postgresql
LuaSQL is a simple interface from Lua to a DBMS. This package provides access
to PostgreSQL databases.


%prep
%setup -q -n luasql-%{version}


%build
make %{?_smp_mflags} PREFIX=%{_prefix} DRIVER_INCS="`pkg-config --cflags sqlite3`" DRIVER_LIBS="`pkg-config --libs sqlite3`" T=sqlite3 DEFS="%{optflags} -fPIC"
make %{?_smp_mflags} PREFIX=%{_prefix} DRIVER_INCS="" DRIVER_LIBS="-lpq" T=postgres DEFS="%{optflags} -fPIC" WARN=
make %{?_smp_mflags} PREFIX=%{_prefix} DRIVER_INCS="-I%{_prefix}/include/mysql" DRIVER_LIBS="-L%{_libdir}/mysql -lmysqlclient" T=mysql DEFS="%{optflags} -fPIC"


%install
rm -rf $RPM_BUILD_ROOT
make install PREFIX=$RPM_BUILD_ROOT%{_prefix} LUA_LIBDIR=$RPM_BUILD_ROOT%{lualibdir} LUA_DIR=$RPM_BUILD_ROOT%{luapkgdir} T=sqlite3
make install PREFIX=$RPM_BUILD_ROOT%{_prefix} LUA_LIBDIR=$RPM_BUILD_ROOT%{lualibdir} LUA_DIR=$RPM_BUILD_ROOT%{luapkgdir} T=postgres
make install PREFIX=$RPM_BUILD_ROOT%{_prefix} LUA_LIBDIR=$RPM_BUILD_ROOT%{lualibdir} LUA_DIR=$RPM_BUILD_ROOT%{luapkgdir} T=mysql


%clean
rm -rf $RPM_BUILD_ROOT


%files
# There are no files in the main package

%files doc
%defattr(-,root,root,-)
%doc README
%doc doc/us/*

%files sqlite
%defattr(-,root,root,-)
#%%dir %{lualibdir}/luasql
%{lualibdir}/luasql/sqlite3.so

%files mysql
%defattr(-,root,root,-)
#%%dir %{lualibdir}/luasql
%{lualibdir}/luasql/mysql.so

%files postgresql
%defattr(-,root,root,-)
#%%dir %{lualibdir}/luasql
%{lualibdir}/luasql/postgres.so


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-6m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-5m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-1m)
- import from Rawhide

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.1-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jan 23 2009 Tim Niemueller <tim@niemueller.de> - 2.1.1-5
- Rebuild for MySQL 5.1

* Tue Apr 08 2008 Tim Niemueller <tim@niemueller.de> - 2.1.1-4
- Main package is now pure meta package to pull in everything else, README
  moved to doc sub-package.

* Sat Apr 05 2008 Tim Niemueller <tim@niemueller.de> - 2.1.1-3
- Do not use pg_config and mysql_config, they are not good for what you think
  they should be used for, cf. #440673

* Fri Apr 04 2008 Tim Niemueller <tim@niemueller.de> - 2.1.1-2
- Fixed lua-sql-postgres requires
- Own %{lualibdir}/luasql directory in all sub-packages

* Fri Apr 04 2008 Tim Niemueller <tim@niemueller.de> - 2.1.1-1
- Initial package

