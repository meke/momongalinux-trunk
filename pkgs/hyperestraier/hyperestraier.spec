%global         momorel 19

%{!?ruby_sitelib:	%define ruby_sitelib	%(ruby -rrbconfig -e 'puts RbConfig::CONFIG["sitelibdir"]')}
%{!?ruby_arch:	%define ruby_arch	%(ruby -rrbconfig -e "puts RbConfig::CONFIG['archdir']")}

Name:		hyperestraier
Summary:	full-text search system
Version:	1.4.13
Release:	%{momorel}m%{?dist}
License:	LGPLv2+
Group:		System Environment/Libraries
URL:		http://hyperestraier.sourceforge.net/
Source0:	http://hyperestraier.sourceforge.net/%{name}-%{version}.tar.gz
NoSource:	0
Source1:	estmaster.init
Patch0:		http://writequit.org/blog/wp-content/uploads/2009/01/hyperestraier-ruby191.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	bzip2-devel zlib-devel
BuildRequires:	gcc-java libgcj-devel
BuildRequires:	lzo-devel >= 2.02
BuildRequires:	mecab-devel >= 0.99
BuildRequires:	qdbm-devel >= 1.8.77
BuildRequires:	ruby-rdoc
Requires:	qdbm >= 1.8.77
Requires:	mecab >= 0.99
Requires(pre):  shadow-utils
Requires(post): chkconfig sudo sed coreutils
Requires(preun): chkconfig
Requires(postun): shadow-utils

%description
Hyper Estraier is a full-text search system. You can search 
lots of documents for some documents including specified words. 
If you run a web site, it is useful as your own search engine 
for pages in your site. Also, it is useful as search utilities 
of mail boxes and file servers.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This is the development package that provides header files and libraries
for Hyper Estraier.

%package java
Summary:	Hyper Estraier library for Java
Group:		System Environment/Libraries
Requires:	%{name} = %{version}-%{release}

%description java
This package contains a Java interface for Hyper Estraier

%package perl
Summary:	Hyper Estraier library for Perl
Group:		System Environment/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description perl
This package contains a Perl interface for Hyper Estraier

%package -n ruby-hyperestraier
Summary:	Hyper Estraier Library for Ruby
Group:		System Environment/Libraries
Requires:	%{name} = %{version}-%{release}
#Requires:	ruby(abi) = %{rubyver}
# $ rpm -q --whatprovides 'ruby(abi)'
# ruby-libs-1.9.3.125-1m.mo8.x86_64
Requires:	ruby-libs
Provides:	ruby(hyperestraier) = %{version}-%{release}

%description -n ruby-hyperestraier
This package contains a Ruby interface for Hyper Estraier.


%prep
%setup -q
%patch0 -p0

%build
## 0. First:
## - remove rpath
## - fix pkgconfig file to hide header files
## - fix Makefile to keep timestamps
%{__sed} -i.rpath -e '/^LDENV/d' `find . -name Makefile.in`
%{__sed} -i.misc \
	 -e '/^Libs/s|@[A-Z][A-Z]*@||g' \
	 -e '/Cflags/s|^\(.*\)|\1 -I\${includedir}/%{name}|' \
	 %{name}.pc.in

%{__sed} -i.path \
	-e '/^cflags/s|^\(.*\)\"$|\1 -I%{_datadir}/qdbm -I%{_datadir}/%{name}\"|' \
	estconfig.in

%{__sed} -i.stamp \
	 -e 's|cp \(-R*f \)|cp -p \1| ' \
	 -e 's|^CP =.*$|CP = cp -p|' \
	`find . -name Makefile.in -or -name \*[mM]akefile`

## 1. For main
%{__sed} -i.flags \
	-e '/^CFLAGS/s|^\(.*\)$|\1 %{optflags}|' Makefile.in
%configure \
	--enable-devel \
	--enable-zlib \
	--enable-bzip \
	--enable-lzo

%{__make} %{?_smp_mflags}

## 2. For java
pushd javanative/
%{__sed} -i.flags -e '/^MYCFLAGS/s|-O2.*|%{optflags}\"|' configure
%configure
# Failed with -j8 on Matt's mass build
%{__make} -j1 JAR=%{_bindir}/fastjar JAVAC="%{_bindir}/gcj -C"
popd

## 3. For perl:
pushd perlnative
%configure
%{__make} %{?_smp_mflags} \
	CC="gcc %optflags $(pkg-config --cflags qdbm)" \
	OPTIMIZE="" \
	LDDLFLAGS="-shared"
popd

##4. For ruby
pushd rubynative

# Workaround for ruby side bug (bug 226381 c11)
%{__cp} -p %{ruby_arch}/rbconfig.rb .
%{__sed} -i.static -e 's|-static||g' rbconfig.rb
export RUBYLIB=$(pwd)

%{__sed} -i.path -e 's|-O3.*|\`pkg-config --cflags qdbm\`\"|' src/extconf.rb
%configure
%{__make} %{?_smp_mflags}
popd


%install
%{__rm} -rf $RPM_BUILD_ROOT

## 1. For main
%{__make} install DESTDIR=$RPM_BUILD_ROOT

# clean up
%{__rm} -f $RPM_BUILD_ROOT%{_libdir}/lib*.a
%{__rm} -rf $RPM_BUILD_ROOT%{_datadir}/%{name}/doc/
%{__rm} -f $RPM_BUILD_ROOT%{_datadir}/%{name}/[A-Z]*

# hide header files to name specific directory
pushd $RPM_BUILD_ROOT%{_includedir}
mkdir %{name}
for f in *.h ; do
	for g in *.h ; do
		eval sed -i -e \'s\|include \<$g\>\|include \"$g\"\|\' $f
	done
done
%{__mv} *.h %{name}/
popd
#
# for backward compatibility
pushd $RPM_BUILD_ROOT%{_includedir}
for f in %{name}/*.h ; do
	%{__ln_s} $f .
done
popd


## 2. For java
pushd javanative/
%{__make} DESTDIR=$RPM_BUILD_ROOT install
popd
%{__mkdir_p} $RPM_BUILD_ROOT%{_javadir}
%{__mv} -f $RPM_BUILD_ROOT%{_libdir}/*.jar \
	$RPM_BUILD_ROOT%{_javadir}

## 3. For perl
pushd perlnative
%{__make} install DESTDIR=$RPM_BUILD_ROOT INSTALLDIRS=vendor
popd
# clean up
%{__rm} $RPM_BUILD_ROOT%{perl_archlib}/perllocal.pod
find $RPM_BUILD_ROOT%{perl_vendorarch} \
	-name \*.bs -or -name .packlist | \
	xargs rm -f
find $RPM_BUILD_ROOT%{perl_vendorarch} \
	-name \*.so | \
	xargs chmod 0755

## 4. For ruby
pushd rubynative/
%{__make} DESTDIR=$RPM_BUILD_ROOT install
popd


%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%post java -p /sbin/ldconfig
%postun java -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog THANKS
%doc example/
%doc doc/*guide-en.html doc/*.png doc/*.css
%lang(ja) %doc doc/*guide-ja.html

%{_libdir}/libestraier.so.*
%{_bindir}/est*
%exclude %{_bindir}/estconfig
%exclude %{_bindir}/*.pl
%exclude %{_bindir}/*.rb
%{_libexecdir}/*.cgi
%{_datadir}/%{name}/

%{_mandir}/man1/*.1*

%files devel
%defattr(-,root,root,-)

%{_bindir}/estconfig
%dir %{_includedir}/%{name}/
%{_includedir}/%{name}/*.h
%{_includedir}/*.h
%{_libdir}/libestraier.so
%{_libdir}/pkgconfig/*.pc

%{_mandir}/man3/est*.3*

%files java
%defattr(-,root,root,-)
%doc doc/javanativeapi/*
%doc javanative/overview.html
%doc javanative/example/

%{_javadir}/*.jar
%{_libdir}/libj*.so*

%files perl
%defattr(-,root,root,-)
%doc doc/perlnativeapi/index.html
%doc perlnative/example/

%{_bindir}/*.pl
%{perl_vendorarch}/*.p*
%{perl_vendorarch}/auto/*/
%{_mandir}/man3/*.3pm*

%files -n ruby-hyperestraier
%defattr(-,root,root,-)
%doc doc/rubynativeapi/*
%doc rubynative/example/

%{_bindir}/*.rb
%{ruby_sitelib}/*.so


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-19m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-18m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-17m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-16m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-15m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-14m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-13m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-12m)
- rebuild against perl-5.16.0

* Mon Mar 19 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.13-11m)
- add sub packages
- add Patch0: hyperestraier-ruby191.patch

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.13-10m)
- rebuild against mecab-0.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.13-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.13-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.13-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.13-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.13-5m)
- add Requires(pre,post,preun,postun)

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.13-4m)
- add devel package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.13-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.13-2m)
- rebuild against gcc43

* Thu Feb 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.13-1m)
- update

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.12-2m)
- %%NoSource -> NoSource

* Thu Nov 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.12-1m)
- update to 1.4.12

* Sun Nov 18 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.11-2m)
- rebuild against qdbm-1.8.77

* Sun Nov 18 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.11-1m)
- up to 1.4.11

* Wed Jun 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.10-3m)
- use 398 for UID and GID, because ID# 98 is used and conflicted with authd

* Sun Apr  1 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.10-2m)
- correct init script
- don't delete search database

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.10-1m)
- update to 1.4.10

* Tue Feb 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.9-2m)
- use %%{_localstatedir}/cache/hyperestraier insted of %%{_localstatedir}/hyperestraier

* Mon Nov 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.9-1m)
- update to 1.4.9

* Sat Oct 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6

* Wed Sep 27 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Sun Sep 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Wed Aug 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7
- update BuildRequires:	qdbm-devel >= 1.8.65

* Tue Aug  1 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.3.3-2m)
- enable mecab

* Tue Jul 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3
- update BuildRequires:	qdbm >= 1.8.60

* Sun Jun 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8

* Thu May 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7

* Mon Apr 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Thu Mar 09 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4
- use %%make

* Thu Feb 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Sun Dec 18 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Sun Dec 11 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1
- update BuildRequires:	qdbm >= 1.8.35

* Fri Nov 18 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Sat Nov 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Wed Nov 09 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Fri Nov 04 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Fri Oct 28 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Fri Oct 21 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Mon Oct 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sat Oct 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Sun Oct 02 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1
- add man at files

* Fri Sep 23 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Thu Sep 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.7-1m)
- update to 0.5.7

* Thu Sep 08 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.6-1m)
- update to 0.5.6

* Thu Sep 08 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.5-3m)
- adapt for momonga
- - comment out BuildRequires: libconv
- - comment out Requires: libconv

* Thu Sep 08 2005 Uta <webmaster@u-jp.com>
- pre/post script bug fix

* Fri Sep 02 2005 Uta <webmaster@u-jp.com>
- initial release

