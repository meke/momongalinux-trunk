%global         momorel 10

Name:           perl-CGI-Session
Version:        4.48
Release:        %{momorel}m%{?dist}
Summary:        Persistent session data in CGI applications
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/CGI-Session/
Source0:        http://www.cpan.org/authors/id/M/MA/MARKSTOS/CGI-Session-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-CGI >= 3.26
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-Digest-MD5
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Simple
Requires:       perl-CGI >= 3.26
Requires:       perl-Data-Dumper
Requires:       perl-Digest-MD5
Requires:       perl-Scalar-Util
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
CGI::Session provides an easy, reliable and modular session management
system across HTTP requests.

%prep
%setup -q -n CGI-Session-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes examples README
%{perl_vendorlib}/CGI/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-2m)
- rebuild against perl-5.14.2

* Tue Jul 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.48-1m)
- update to 4.48

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.46-1m)
- update to 4.46

* Sat Jul  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.45-1m)
- update to 4.45

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.43-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.43-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.43-2m)
- rebuild for new GCC 4.6

* Sun Dec 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.43-1m)
- update to 4.43

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.42-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.42-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.42-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.42-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.42-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.42-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.42-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.42-1m)
- update to 4.42

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.41-2m)
- rebuild against perl-5.10.1

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.41-1m)
- update to 4.41

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.40-2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.40-1m)
- update to 4.40

* Thu Dec 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.39-1m)
- update to 4.39

* Sat Nov  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.38-1m)
- update to 4.38

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.37-1m)
- update to 4.37

* Sun Sep 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.36-1m)
- update to 4.36

* Wed Jul 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.35-1m)
- update to 4.35

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.34-1m)
- update to 4.34
- use NoSource
- change BuildRequries from perl module name to package name

* Fri Jul 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.20-1m)
- import from Fedora to Momonga for ikiwiki -> perl-CGI-FormBuilder

* Wed Mar  5 2008 Tom "spot" Callaway <tcallawa@redhat.com> 4.20-4
- rebuild for new perl

* Sun Jan 27 2008 Andreas Thienemann <andreas@bawue.net> 4.20-3
- Added Test::More to the BuildReqs

* Sat Mar 17 2007 Andreas Thienemann <andreas@bawue.net> 4.20-2
- Fixed perl-devel req

* Sat Mar 10 2007 Andreas Thienemann <andreas@bawue.net> 4.20-1
- Cleaned up for FE
- Specfile autogenerated by cpanspec 1.69.1.
