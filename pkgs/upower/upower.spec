%global momorel 2

Summary: upower
Name: upower
Version: 0.9.23
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://upower.freedesktop.org/
Source0: http://upower.freedesktop.org/releases/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: glib2-devel
BuildRequires: intltool
BuildRequires: libgudev1-devel >= 145
BuildRequires: pkgconfig
BuildRequires: polkit-devel
BuildRequires: libimobiledevice-devel >= 1.1.5
BuildRequires: libusb1-devel
#BuildRequires: gobject-introspection-devel >= 0.9.10
Obsoletes:     DeviceKit-power
Provides:      DeviceKit-power
Obsoletes:     UPower
Provides:      UPower
Requires:      pm-utils
%description
UPower is an abstraction for enumerating power devices,
listening to device events and querying history and statistics.  Any
application or service on the system can access the
org.freedesktop.DeviceKit.Power service via the system message bus.

%package devel
Summary: Headers and libraries for DeviceKit-power
Group: Development/Libraries
Requires:  %{name} = %{version}-%{release}
Obsoletes: DeviceKit-power-devel
Provides:  DeviceKit-power-devel
Obsoletes: UPower-devel
Provides:  UPower-devel

%description devel
Headers and libraries for UPower.

%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --enable-static=no \
    --enable-man-pages \
    --enable-gtk-doc \
    --enable-introspection=yes
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/dbus-1/system.d/org.freedesktop.UPower.conf
%{_sysconfdir}/UPower/UPower.conf
/lib/udev/rules.d/95-upower-battery-recall-dell.rules
/lib/udev/rules.d/95-upower-battery-recall-fujitsu.rules
/lib/udev/rules.d/95-upower-battery-recall-gateway.rules
/lib/udev/rules.d/95-upower-battery-recall-ibm.rules
/lib/udev/rules.d/95-upower-battery-recall-lenovo.rules
/lib/udev/rules.d/95-upower-battery-recall-toshiba.rules
/lib/udev/rules.d/95-upower-csr.rules
/lib/udev/rules.d/95-upower-hid.rules
/lib/udev/rules.d/95-upower-wup.rules
%{_bindir}/upower
%{_libdir}/libupower-glib.so.*
%exclude %{_libdir}/libupower-glib.la
%{_libexecdir}/upowerd
%{_libdir}/girepository-1.0/UPowerGlib-1.0.typelib
%{_unitdir}/upower.service
/lib/systemd/system-sleep/notify-upower.sh

%{_datadir}/polkit-1/actions/org.freedesktop.upower.policy
%{_datadir}/polkit-1/actions/org.freedesktop.upower.qos.policy
%{_datadir}/dbus-1/interfaces/org.freedesktop.UPower.Device.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.UPower.KbdBacklight.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.UPower.QoS.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.UPower.Wakeups.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.UPower.xml
%{_datadir}/dbus-1/system-services/org.freedesktop.UPower.service
%{_mandir}/man1/upower.1.*
%{_mandir}/man7/UPower.7.*
%{_mandir}/man8/upowerd.8.*

%files devel
#%{_includedir}/DeviceKit-power
%{_includedir}/libupower-glib
#%{_libdir}/libdevkit-power-gobject.so
%{_libdir}/libupower-glib.so
#%{_libdir}/pkgconfig/devkit-power-gobject.pc
%{_libdir}/pkgconfig/upower-glib.pc
%{_datadir}/gtk-doc/html/UPower
%{_datadir}/gir-1.0/UPowerGlib-1.0.gir

%changelog
* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.23-2m)
- rebuild against libimobiledevice-1.1.5

* Tue Nov 19 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.23-1m)
- update 0.9.23

* Wed Aug 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.18-1m)
- update 0.9.18

* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.15-3m)
- add Requires and BuildRequires

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.15-2m)
- rebuild for glib 2.33.2

* Wed Dec  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.15-1m)
- update 0.9.15

* Wed Oct  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.14-1m)
- update 0.9.14

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.13-1m)
- update 0.9.13

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.12-2m)
- rebuild against libimobiledevice-1.1.1

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.12-1m)
- update 0.9.12

* Wed May  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.9-1m)
- enable introspection

* Thu Apr 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-1m)
- update 0.9.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.7-1m)
- update 0.9.7

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.5-3m)
- disable introspection

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.5-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.5-1m)
- update 0.9.5

* Sat May 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-2m)
- Obsoletes and Provides: UPower-devel

* Fri May 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-1m)
- update 0.9.4

* Wed Apr 21 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-2m)
- update source url

* Thu Mar 18 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1-1m)
- devicekit-power was renamed UPower

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (014-2m)
- --enable-gtk-doc

* Sat Jan 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (014-1m)
- update to 014

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (013-2m)
- --disable-gtk-doc

* Tue Dec  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (013-1m)
- update to 013

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (012-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (012-1m)
- update to 012

* Sun Oct 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (011-1m)
- update to 011

* Sun Sep 27 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (010-2m)
- replace %%{_lib} in %%files section to lib for x86_64

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (010-1m)
- update to 010

* Tue Jun  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (008-1m)
- update 008

* Fri May 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (007-1m)
- update 007

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (006-1m)
- initial build
