%global momorel 6

Summary: D-Bus binding for GObject
Name: eggdbus
Version: 0.6
Release: %{momorel}m%{?dist}
URL: http://cgit.freedesktop.org/~david/eggdbus
Source0: http://hal.freedesktop.org/releases/%{name}-%{version}.tar.gz 
NoSource: 0

License: GPL
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: glib2-devel
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel

%description
D-Bus binding for GObject

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
gtkdocize --copy
autoreconf -vfi

%configure \
    --enable-static=no \
    --enable-man-pages \
    --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HACKING NEWS README
%{_bindir}/eggdbus-binding-tool
%{_bindir}/eggdbus-glib-genmarshal
%{_libdir}/libeggdbus-1.so.*
%exclude %{_libdir}/libeggdbus-1.la
%{_mandir}/man1/eggdbus-binding-tool.1.*

%files devel
%defattr(-,root,root)
%{_includedir}/eggdbus-1
%{_libdir}/libeggdbus-1.so
%{_libdir}/pkgconfig/eggdbus-1.pc
%{_datadir}/gtk-doc/html/eggdbus
%exclude %{_datadir}/gtk-doc/html/tests

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-2m)
- use BuildRequires

* Thu Dec 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-1m)
- initial build
