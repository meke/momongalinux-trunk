%global momorel 1

Summary: PCI bus related utilities
Name: pciutils
Version: 3.2.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://mj.ucw.cz/pciutils.html
Source0: ftp://atrey.karlin.mff.cuni.cz/pub/linux/pci/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1: pciutils-2.2.1-idpath.patch
Patch2: pciutils-dir-d.patch
ExclusiveOS: Linux
Requires: hwdata
BuildRequires: sed
BuildRequires: zlib-devel

%description
The pciutils package contains various utilities for inspecting and
setting devices connected to the PCI bus. The utilities provided
require kernel version 2.1.82 or newer (which support the
/proc/bus/pci interface).

%package devel
Summary: Linux PCI development library
Group: Development/Libraries
Requires: zlib-devel
Requires: %{name} = %{version}-%{release}

%description devel
This package contains a library for inspecting and setting
devices connected to the PCI bus.

%package libs
Summary: Linux PCI library
Group: System Environment/Libraries

%description libs
This package contains a library for inspecting and setting
devices connected to the PCI bus.

%prep
%setup -q
%patch1 -p1 -b .idpath
%patch2 -p1 -b .dird

sed -i -e 's/^SRC=.*/SRC="http:\/\/pciids.sourceforge.net\/pci.ids"/' update-pciids.sh

%build
make SHARED="no" ZLIB="no" STRIP="" OPT="$RPM_OPT_FLAGS" PREFIX="/usr" IDSDIR="/usr/share/hwdata" PCI_IDS="pci.ids" %{?_smp_mflags}
mv lib/libpci.a lib/libpci.a.toinstall

make clean

make SHARED="yes" ZLIB="no" STRIP="" OPT="$RPM_OPT_FLAGS" PREFIX="/usr" IDSDIR="/usr/share/hwdata" PCI_IDS="pci.ids" %{?_smp_mflags}

#fix lib vs. lib64 in libpci.pc (static Makefile is used)
mv lib/libpci.pc lib/libpci.pc.old
sed <lib/libpci.pc.old >lib/libpci.pc "s|^libdir=.*$|libdir=%{_libdir}|"
rm lib/libpci.pc.old

%install
rm -rf %{buildroot}

install -d %{buildroot}/{%{_sbindir},%{_mandir}/man8,%{_libdir},%{_libdir}/pkgconfig,%{_includedir}/pci}

install -p lspci setpci update-pciids %{buildroot}/%{_sbindir}/
install -p lspci.8 setpci.8 update-pciids.8 %{buildroot}%{_mandir}/man8
install -p  lib/libpci.so.*.*.* %{buildroot}%{_libdir}
ln -s $(basename %{buildroot}%{_libdir}/*.so.*.*.*) %{buildroot}%{_libdir}/libpci.so

mv lib/libpci.a.toinstall lib/libpci.a
install -p lib/libpci.a %{buildroot}%{_libdir}
/sbin/ldconfig -N %{buildroot}%{_libdir}
install -p lib/pci.h %{buildroot}%{_includedir}/pci
install -p lib/header.h %{buildroot}%{_includedir}/pci
install -p lib/config.h %{buildroot}%{_includedir}/pci
install -p lib/types.h %{buildroot}%{_includedir}/pci
install -p lib/libpci.pc %{buildroot}%{_libdir}/pkgconfig

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(0644, root, root, 0755)
%{_mandir}/man8/*
%attr(0755, root, root) %{_sbindir}/*
%doc README ChangeLog pciutils.lsm

%files libs
%{_libdir}/libpci.so.*

%files devel
%defattr(0644, root, root, 0755)
%{_libdir}/pkgconfig/libpci.pc
%{_libdir}/libpci.a
%{_libdir}/libpci.so
%{_includedir}/pci

%changelog
* Wed Mar 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.1-1m)
- update 3.2.1

* Sat Oct  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.8-1m)
- update 3.1.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.7-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.7-1m)
- update 3.1.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2 based on Fedora 11 (3.1.2-5)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3
-- update Patch0,1,6 for fuzz=0 or others
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.6-2m)
- %%NoSource -> NoSource

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linuc.org>
- (2.2.6-1m)
- update 2.2.6

* Mon Jul  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.4-3m)
- remove pciutils-2.2.4-link.patch

* Wed Jun 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.4-2m)
- import pciutils-2.2.4-link.patch from gentoo
 +- 07 Jan 2007; Mike Frysinger <vapier@gentoo.org>
 +- files/pciutils-2.2.4-link.patch:
 +- Remove over engineering attempt at LDLIBS since it isnt actually needed
 +- #160561 Mark Glines.

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linuc.org>
- (2.2.4-1m)
- sync FC7 
- update 2.2.4

* Sun Oct 15 2006 Yohsuke Ooi <meke@momonga-linuc.org>
- (2.2.3-3m)
- add pciutils-2.2.3-sata.patch

* Mon Jul 31 2006 Nishio Futoshi <futoshi@momonga-linuc.org>
- (2.2.3-2m)
- change make order (Is it OK?)

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2.3-1m)
- update to 2.2.3
- delete patch4 (no longer needed)
- modify other patches

* Fri Feb 18 2005 Toru Hoshina <t@momonga-linux.org>
- (2.1.99-0.8.3m)
- revised spec so the static library avoid stack protection.
- the libpci_loader.a is provided again, for the anaconda loader2.

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.1.99-0.8.2m)
- fix hwdata path.

* Wed Dec  8 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.99-0.8.1m)
  update to 2.1.99-test8, based FC development 2.1.99.test8-4

* Mon Nov 22 2004 Jeremy Katz <katzj@redhat.com> - 2.1.99.test8-4
- don't use dietlibc on x86 anymore

* Thu Sep  2 2004 Bill Nottingham <notting@redhat.com> 2.1.99.test8-3
- change sysfs access for detecting devices who get fixed up in the
  kernel (#115522, #123802)

* Tue Aug 31 2004 Bill Nottingham <notting@redhat.com> 2.1.99.test8-2
- update to test8
- fix headers

* Fri Aug 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.11-3m)
  added libpci-nossp.a

* Fri Jul  9 2004 Bill Nottingham <notting@redhat.com> 2.1.99.test7-1
- update to test7
- fix segfault on some x86-64 boxen

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.11-2m)
- revised spec for rpm 4.2.

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Dec  8 2003 Bill Nottingham <notting@redhat.com> 2.1.11-4
- fix paths for pci.ids, etc. (#111665)

* Tue Nov 25 2003 Bill Nottingham <notting@redhat.com> 2.1.11-3
- remove a few calls to ->error() in the sysfs code

* Fri Nov 21 2003 Jeremy Katz <katzj@redhat.com> 2.1.11-2
- build a diet libpci_loader.a on i386
- always assume pread exists, it does with diet and all vaguely recent glibc

* Fri Nov 21 2003 Bill Nottingham <notting@redhat.com> 2.1.11-1
- update to 2.1.11
- add patch for sysfs & pci domains support (<willy@debian.org>)

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Sun Feb 16 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.11-1m)
  update to 2.1.11

* Wed Feb 12 2003 Bill Nottingham <notting@redhat.com>
- don't segfault when there's no pci bus (#84146)

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Thu Dec 05 2002 Elliot Lee <sopwith@redhat.com> 2.1.10-5
- Add patch4 for ppc64. The basic rule seems to be that on any platform
where it is possible to be running a 64-bit kernel, we need to always 
print out 64-bit addresses.

* Mon Nov  4 2002 Bill Nottingham <notting@redhat.com> 2.1.10-4
- fix dir perms on /usr/include/pci

* Tue Oct 15 2002 Bill Nottingham <notting@redhat.com> 2.1.10-3
- use %%{_libdir}
- own /usr/include/pci
- build library with -fPIC

* Thu Jul  8 2002 Bill Nottingham <notting@redhat.com> 2.1.10-2
- don't build with -fomit-frame-pointer

* Mon Jun 24 2002 Bill Nottingham <notting@redhat.com> 2.1.10-1
- update to 2.1.10

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Jun 17 2002 Bill Nottingham <notting@redhat.com> 2.1.9-4
- don't forcibly strip binaries

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun May 19 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (2.1.10-2k)
- ver up to 2.1.10.
- Skip patch & patch5. (already applied on 2.1.10.)
- modify URL.

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (2.1.9-2k)
- ver up.

* Fri Feb 22 2002 Bill Nottingham <notting@redhat.com>
- rebuild

* Wed Jan 30 2002 Bill Nottingham <notting@redhat.com>
- require hwdata now that pci.ids is there

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun Dec 30 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- man page is now owned by root

* Mon Nov 19 2001 Shingo Akagaki <dora@kondara.org>
- (2.1.8-18k)
- add nForce

* Mon Oct 22 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.1.8-16k)
- modify permission /usr/include/pci and /usr/include/pci/*

* Thu Oct 18 2001 Motonobu Ichimura <famao@kondara.org>
- sync with Jirai (add URL)
- owned %{_includedir}/pci

* Wed Oct 17 2001 Bill Nottingham <notting@redhat.com>
- dump all the patches, ship pci.ids direct out of sourceforge CVS

* Wed Sep 26 2001 Bill Nottingham <notting@redhat.com>
- broadcom bcm5820 id (#53592)

* Wed Aug 29 2001 Shingo Akagaki <dora@kondara.org>
- add via.patch

* Tue Aug 28 2001 Motonobu Ichimura <famao@kondara.org>
- add URL

* Fri Aug 10 2001 Bill Nottingham <notting@redhat.com>
- more ids

* Tue Jul 17 2001 Bill Nottingham <notting@redhat.com>
- add newline in printf in PCI-X patch (#49277)

* Mon Jul  9 2001 Bill Nottingham <notting@redhat.com>
- update broadcom patch
- add new ids from 2.4.6

* Mon May 28 2001 Bill Nottingham <notting@redhat.com>
- add a couple of e1000 ids

* Thu Mar 22 2001 Bill Nottingham <notting@redhat.com>
- another megaraid id

* Wed Mar 21 2001 Bill Nottingham <notting@redhat.com>
- another megaraid id

* Wed Mar 14 2001 Preston Brown <pbrown@redhat.com>
- LSI SCSI PCI id

* Wed Feb 21 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix formatting problems

* Wed Feb 21 2001 Preston Brown <pbrown@redhat.com>
- add IBM ServeRAID entries

* Tue Feb 20 2001 Preston Brown <pbrown@redhat.com>
- i860 entries.

* Mon Feb 19 2001 Helge Deller <hdeller@redhat.de>
- added various pci ids 

* Fri Feb  2 2001 Bill Nottingham <notting@redhat.com>
- fix mishap in fixing mishap

* Thu Feb  1 2001 Bill Nottingham <notting@redhat.com>
- fix apparent mishap in pci.ids update from kernel (#25520)

* Tue Jan 23 2001 Bill Nottingham <notting@redhat.com>
- pci.ids updates

* Tue Dec 12 2000 Bill Nottingham <notting@redhat.com>
- big pile of pci.ids updates

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Jul 25 2000 Nalin Dahyabhai <nalin@redhat.com>
- clean up patches to not generate badly-formatted files

* Tue Jul 25 2000 Preston Brown <pbrown@redhat.com>
- Vortex fixes laroche originally applied on kudzu moved here.

* Fri Jul 14 2000 Preston Brown <pbrown@redhat.com>
- pci ids for i815, new ati hardware

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jul 11 2000 Bill Nottingham <notting@redhat.com>
- yet more IDs
- PCI-X support from Matt Domsch

* Fri Jul  7 2000 Bill Nottingham <notting@redhat.com>
- some more QLogic ids

* Mon Jun 26 2000 Bill Nottingham <notting@redhat.com>
- more IDs from Dell

* Sat Jun 10 2000 Bill Nottingham <notting@redhat.com>
- update to 2.1.8

* Fri Apr 21 2000 Bill Nottingham <notting@redhat.com>
- update to 2.1.7

* Mon Apr 17 2000 Bill Nottingham <notting@redhat.com>
- update to 2.1.6

* Fri Mar  3 2000 Bill Nottingham <notting@redhat.com>
- add a couple of ids

* Mon Feb 14 2000 Bill Nottingham <notting@redhat.com>
- update to 2.1.5

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Mon Jan 24 2000 Bill Nottingham <notting@redhat.com>
- update to 2.1.4

* Thu Jan 20 2000 Bill Nottingham <notting@redhat.com>
- update to 2.1.3

* Fri Dec 24 1999 Bill Nottingham <notting@redhat.com>
- update to 2.1.2

* Tue Jun 29 1999 Bill Nottingham <notting@redhat.com>
- add -devel package

* Thu May 20 1999 Bill Nottingham <notting@redhat.com>
- update to 2.0

* Mon Apr 19 1999 Jakub Jelinek  <jj@ultra.linux.cz>
- update to 1.99.5
- fix sparc64 operation

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Thu Feb  4 1999 Bill Nottingham <notting@redhat.com>
- initial build
