%global momorel 12
%global reldir 36733
%global cvsdate 20090410
%global with_system_ca_cert_file 1
%global system_ca_cert_file %{_sysconfdir}/pki/tls/cert.pem
%global help_url file://%{_datadir}/doc/%{name}-%{version}/index.html

Summary: a GTK+ 2ch.net BBS browser
Name: ochusha
Version: 0.6.0.1
Release: 0.0.%{cvsdate}.%{momorel}m%{?dist}
License: Modified BSD
URL: http://ochusha.sourceforge.jp/
Group: Applications/Internet
#Source0: http://dl.sourceforge.jp/ochusha/%{reldir}/%{name}-%{version}.tar.bz2
#NoSource: 0
Source0: ochusha-0.6.0.1.tar.lzma
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1: ochusha-0.6.0.1-gtk220.patch
Patch2: ochusha-0.6.0.1-gtk-2.22.patch
Requires(post): gtk2
Requires(postun): gtk2
BuildRequires: ImageMagick
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: gettext >= 0.15-9m
BuildRequires: glib2-devel >= 2.6
BuildRequires: gtk2-devel >= 2.6
BuildRequires: libxml2-devel >= 2.5.0
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: sed
BuildRequires: sqlite-devel
BuildRequires: zlib-devel
Requires(post): gnome-icon-theme

%description
The ochusha is BBS, especially 2ch.net, browser with GUI.
It uses the GTK+ toolkit for all of its interface needs.
The ochusha offers a sort of features such as multi-level
popup view of `response's, inlining and popup view of
images that helps users to interact with BBSs.
#`

%prep
%setup -q

# for doc
install -m 644 oniguruma/AUTHORS AUTHORS.oniguruma
install -m 644 oniguruma/COPYING COPYING.oniguruma
install -m 644 oniguruma/HISTORY HISTORY.oniguruma
install -m 644 libochushagtk_lgpl/AUTHORS AUYHORS.libochushagtk_lgpl
install -m 644 libochushagtk_lgpl/BUGS BUGS.libochushagtk_lgpl
install -m 644 libochushagtk_lgpl/COPYING COPYING.libochushagtk_lgpl
install -m 644 libochushagtk_lgpl/ChangeLog ChangeLog.libochushagtk_lgpl
install -m 644 libochushagtk_lgpl/INSTALL INSTALL.libochushagtk_lgpl
install -m 644 libochushagtk_lgpl/NEWS NEWS.libochushagtk_lgpl
install -m 644 libochushagtk_lgpl/README README.libochushagtk_lgpl

# modify desktop file for icon
%{__sed} -i -e 's|Icon.*$|Icon=ochusha|' %{name}/%{name}.desktop.in

%patch1 -p1 -b .gtk220~
%patch2 -p1 -b .gtk222~

%build
./autogen.sh
%configure \
%if %{with_system_ca_cert_file}
	--with-ca-cert-file=%{system_ca_cert_file} \
%endif
	--with-help-url=%{help_url} \
	CFLAGS="-DG_CONST_RETURN=const"

%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall

# link icon
%{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,scalable}/apps
%{__mkdir_p} %{buildroot}%{_datadir}/pixmaps
convert -scale 16x16 %{name}/%{name}48.png %{buildroot}%{_datadir}/%{name}/%{name}16.png
%{__ln_s} ../../../../%{name}/%{name}16.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
%{__ln_s} ../../../../%{name}/%{name}32.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{__ln_s} ../../../../%{name}/%{name}48.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png

%{__ln_s} ../../../../%{name}/%{name}48.svg %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{__ln_s} ../%{name}/%{name}48.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/%{name}/ \
  --remove-category Application \
  %{buildroot}%{_datadir}/%{name}/%{name}.desktop

# link desktop file
%{__mkdir_p} %{buildroot}%{_datadir}/applications
%{__ln_s} %{_datadir}/%{name}/%{name}.desktop %{buildroot}/%{_datadir}/applications/

# get rif of la files
find %{buildroot} -name "*.la" -delete

# additional clean up
%if %{with_system_ca_cert_file}
%{__rm} -f %{buildroot}%{_datadir}/%{name}/ca-bundle.crt
%endif

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
/sbin/ldconfig
if test -d %{_datadir}/icons/hicolor ; then
  %{_bindir}/gtk-update-icon-cache --force --quiet %{_datadir}/icons/hicolor
fi

%postun
/sbin/ldconfig
if test -d %{_datadir}/icons/hicolor ; then
  %{_bindir}/gtk-update-icon-cache --force --quiet %{_datadir}/icons/hicolor
fi

%files
%defattr(-,root,root)
%doc ABOUT-NLS ACKNOWLEDGEMENT AUTHORS* BUGS* COPYING* ChangeLog* HISTORY*
%doc INSTALL INSTALL.ja INSTALL.libochushagtk_lgpl NEWS* README* TODO
%doc doc/%{name}-help.css doc/*.gif doc/*.html doc/*.png
%doc %{name}/%{name}-accel-map.rc.* %{name}/%{name}-gtkrc* 
%doc %{name}/%{name}-init.scm.* %{name}/%{name}-ui.xml.*
%{_bindir}/ochusha
%{_libdir}/*.a
%{_libdir}/*.so*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.svg
%{_datadir}/locale/*/LC_MESSAGES/%{name}*.mo
%{_datadir}/ochusha
%{_datadir}/pixmaps/%{name}.png
%{_mandir}/man1/ochusha.1*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0.1-0.0.20090410.12m)
- rebuild for glib 2.33.2

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0.1-0.0.20090410.11m)
- build with new glib

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0.1-0.0.20090410.10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0.1-0.0.20090410.9m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0.1-0.0.20090410.8m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0.1-0.0.20090410.7m)
- full rebuild for mo7 release

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0.1-0.0.20090410.6m)
- add patch for gtk-2.20 by gengtk220patch

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0.1-0.0.20090410.5m)
- rebuild against openssl-1.0.0

* Sat Feb 13 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0.1-0.0.20090410.4m)
- add patch for gtk-2.20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0.1-0.0.20090410.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0.1-0.0.20090410.2m)
- add Requires(post): gnome-icon-theme

* Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0.1-0.0.20090410.1m)
- update to cvs snapshot

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-5m)
- rebuild against openssl-0.9.8k

* Tue Mar 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-4m)
- import upstream patch (Patch1) for following Machi-BBS changes

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-2m)
- rebuild against rpm-4.6

* Thu Dec 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-1m)
- version 0.6
- Merry Christmas 2channellers

* Fri Sep 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.8.2-13m)
- update new-2channelers.patch to enable posting after the BBS system change

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.8.2-12m)
- add gtk-2.14 support

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.8.2-11m)
- remove gtk-deprecated patches and apply gentoo's gtk+ patches
 +- 05 Mar 2008; MATSUU Takuto <matsuu@gentoo.org>
 +- +files/ochusha-0.5.8.2-glibc2.diff, +files/ochusha-0.5.8.2-gtk-2.12.diff,
 +- +files/ochusha-0.5.8.2-type-punning.diff, +ochusha-0.5.8.2-r3.ebuild:
 +- Fixed to work with gtk+-2.12.1, bug #199495.
 +- 01 Aug 2006; Mamoru KOMACHI <usata@gentoo.org>
 +- +files/ochusha-0.5.8.2-glib-2.10.diff, ochusha-0.5.8.2-r1.ebuild:
 +- Fixed compile error against newer glib. Added ~amd64 keyword.

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8.2-10m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.8.2-9m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.8.2-8m)
- %%NoSource -> NoSource

* Fri Sep 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.8.2-7m)
- remove -DGTK_DISABLE_DEPRECATED from gtk2/Makefile.am

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.8.2-6m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.5.8.2-5m)
- rebuild against expat-2.0.0-1m

* Mon May 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.8.2-4m)
- import ochusha-0.5.8.2-new-2channelers.patch from 2ch
  http://pc8.2ch.net/test/read.cgi/linux/1148809116/11-15

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.8.2-3m)
- rebuild against openssl-0.9.8a

* Wed Mar 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.8.2-2m)
- add Patch0: ochusha-0.5.8.2-deprecated.patch due to glib-2.10.1

* Mon Sep 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.8.2-1m)
- version 0.5.8.2
- add %%{_datadir}/pixmaps/ochusha.png
- modify ochusha.desktop

* Tue Aug  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.8.1-1m)
- version 0.5.8.1

* Sat Jun 11 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.8-1m)
- version 0.5.8

* Fri Dec 31 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.7.2-1m)
- version 0.5.7.2

* Thu Dec 30 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.7.1-1m)
- version 0.5.7.1

* Tue Dec  7 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.7-2m)
- BuildRequires: openssl-devel
- Requires: openssl

* Sun Nov 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.7-1m)
- version 0.5.7

* Mon Nov 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.6-1m)
- version 0.5.6

* Thu Aug 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.5-3m)
- import ochusha.desktop from linux.matchy.net
  http://linux.matchy.net/OchushaRpm.html
- correct Requires and BuildRequires

* Thu Aug  5 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.5-2m)
- change make install-strip to install

* Fri Jul  2 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.5-1m)
- version 0.5.5

* Sat Jun 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.10-2m)
- use make install-strip

* Sun Jun 20 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.10-1m)
- version 0.5.4.10

* Wed Jun 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.9-1m)
- version 0.5.4.9

* Tue Jun 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.8-1m)
- version 0.5.4.8

* Mon Jun 14 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.7-1m)
- version 0.5.4.7

* Mon Jun 14 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.6-1m)
- version 0.5.4.6

* Sat Jun 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.5-1m)
- version 0.5.4.5

* Fri Jun 11 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.4-1m)
- version 0.5.4.4

* Sun Jun  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.3-1m)
- version 0.5.4.3

* Tue May 25 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.2-1m)
- version 0.5.4.2
- remove --with-gtk22-api option from configure

* Sun May 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4.1-1m)
- version 0.5.4.1
- add --with-gtk22-api option to configure

* Thu May  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-1m)
- version 0.5.4

* Wed May  5 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-1m)
- version 0.5.3

* Sun May  2 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-1m)
- version 0.5.2

* Fri Apr  9 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-1m)
- version 0.5.1
- add man file

* Sun Feb 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-1m)
- version 0.5

* Wed Jan 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.10.3-1m)
- version 0.4.10.3

* Thu Jan 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.10.1-1m)
- version 0.4.10.1

* Wed Jan 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.10-1m)
- version 0.4.10

* Sat Jan 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.7-1m)
- version 0.4.9.7

* Sat Jan 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.6-1m)
- version 0.4.9.6

* Thu Jan 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.5-1m)
- version 0.4.9.5

* Wed Jan 14 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.4-1m)
- version 0.4.9.4
- s/%%define/%%global/g

* Wed Jan 14 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.3-1m)
- version 0.4.9.3

* Tue Jan 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.2-1m)
- version 0.4.9.2
- use rpm macros in desktop entry

* Mon Jan 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.1-1m)
- version 0.4.9.1

* Sun Jan 11 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-1m)
- version 0.4.9

* Tue Jan  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8.2-1m)
- version 0.4.8.2

* Wed Dec 31 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8.1-2m)
- add %%post and %%postun sections

* Wed Dec 31 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8.1-1m)
- version 0.4.8.1

* Tue Dec 30 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8-1m)
- version 0.4.8

* Tue Dec 30 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7.1-1m)
- version 0.4.7.1

* Wed Dec 24 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-1m)
- version 0.4.7
- Merry Christmas

* Mon Dec 22 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-2m)
- version 0.4.6 official release

* Sun Dec 21 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-1m)
- version 0.4.6

* Sun Dec 21 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.11-1m)
- version 0.4.5.11

* Sat Dec 20 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.10-1m)
- version 0.4.5.10

* Sat Dec 20 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.9-1m)
- version 0.4.5.9

* Sat Dec 20 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.8-1m)
- version 0.4.5.8
- fix Requires

* Wed Dec 17 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.7-1m)
- version 0.4.5.7

* Tue Dec 16 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.6-1m)
- version 0.4.5.6

* Mon Dec 15 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.5-1m)
- version 0.4.5.5

* Sat Dec 13 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.4-1m)
- version 0.4.5.4

* Fri Dec 12 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.2-1m)
- version 0.4.5.2

* Tue Dec  9 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.1-1m)
- version 0.4.5.1

* Sat Dec  6 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5-1m)
- version 0.4.5

* Tue Dec  2 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4.8-2m)
- add desktop entry

* Sat Nov 29 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4.8-1m)
- version 0.4.4.8
- License: Modified BSD

* Thu Nov 27 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4.7-1m)
- version 0.4.4.7

* Wed Nov 26 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4.6-1m)
- import

* Fri Nov 21 2003 <yuhei@users.sourceforge.jp> 0.4.4.3-1
- Version up.
* Fri Nov 14 2003 <yuhei@users.sourceforge.jp> 0.4.4.2-1
- Version up.
- Added some libraries. 
* Mon Jun 23 2003 <yuhei@users.sourceforge.jp> 0.4.3.6-1
- Version up.
* Wed Jun 11 2003 <yuhei@users.sourceforge.jp> 0.4.3.4-1
- Version up.
- changed prefix enviromental valuable.
- changed URL.
* Mon May 19 2003 <yuhei@users.sourceforge.jp> 0.4.3.3-1
- Version up.
* Mon May 12 2003 <yuhei@users.sourceforge.jp> 0.4.3.2-1
- Version up.
- Fixed Group.
* Mon May 12 2003 <yuhei@users.sourceforge.jp> 0.4.2.1-1
- Initial build.
