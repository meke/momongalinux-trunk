%global         momorel 1

Name:           pigz
Version:        2.2.5
Release:        %{momorel}m%{?dist}
Summary:        Parallel implementation of gzip

Group:          Applications/File
License:        "zlib"
URL:            http://www.zlib.net/pigz/
Source0:        http://www.zlib.net/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  zlib-devel

%description
pigz, which stands for parallel implementation of gzip,
is a fully functional replacement for gzip that exploits
multiple processors and multiple cores to the hilt when compressing data.

%prep
%setup -q


%build
make %{?_smp_mflags} CFLAGS='%{optflags}'


%install
rm -rf $RPM_BUILD_ROOT
install -p -D pigz $RPM_BUILD_ROOT%{_bindir}/pigz
install -p -D unpigz $RPM_BUILD_ROOT%{_bindir}/unpigz
install -p -D pigz.1 -m 0644 $RPM_BUILD_ROOT%{_datadir}/man/man1/pigz.1

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc pigz.pdf README
%{_bindir}/pigz
%{_bindir}/unpigz
%{_datadir}/man/man1/pigz.*


%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.5-1m)
- update to 2.2.5

* Tue Apr  3 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.4-1m)
- import from Fedora devel to Momonga

* Wed Mar 14 2012 Adel Gadllah <adel.gadllah@gmail.com> - 2.2.4-1
- New upstream release

* Wed Jan 18 2012 Adel Gadllah <adel.gadllah@gmail.com> - 2.2.3-1
- New upstream release

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Apr 26 2010 Adel Gadllah <adel.gadllah@gmail.com> - 2.1.6-1
- New upstream release

* Sat Sep 26 2009 Adel Gadllah <adel.gadllah@gmail.com> - 2.1.5-3
- Preserve timestamps  

* Sat Sep 26 2009 Adel Gadllah <adel.gadllah@gmail.com> - 2.1.5-2
- Fix license tag

* Fri Sep 25 2009 Adel Gadllah <adel.gadllah@gmail.com> - 2.1.5-1
- Initial package

