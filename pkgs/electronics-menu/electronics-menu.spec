%global momorel 6

%{?!_icondir:%define _icondir   %{_datadir}/icons}

Name:       electronics-menu
Version:    1.0
Release:    %{momorel}m%{?dist}
Summary:    Electronics Menu for the Desktop

License:    GPLv2
Group:      User Interface/Desktops

URL:        http://geda.seul.org/
Source0:    http://geda.seul.org/dist/%{name}-%{version}.tar.gz

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(pre): gtk2

BuildArchitectures: noarch

%description
The programs from the category Electronics are currently located
in the Edutainment directory.
This Package adds a Electronics menu to the xdg menu structure.

%{name} is listed among Fedora Electronic Lab (FEL) packages.

%prep
%setup -q

# allowing timestamps
sed -i 's|install|install -p|g' Makefile

%build

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

%clean
%{__rm} -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :


%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :


%files
%defattr(-,root,root,-)
%doc COPYING README
%{_icondir}/hicolor/??x??/categories/applications-electronics.png
%{_icondir}/hicolor/scalable/categories/applications-electronics.svg
%{_sysconfdir}/xdg/menus/applications-merged/electronics.menu
%{_datadir}/desktop-directories/Electronics.directory



%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-6m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jul 06 2008 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> - 1.0-2
- Corrected yum install with requires(pre)

* Fri Feb 01 2008 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> - 1.0-1
- Initial package for Fedora
