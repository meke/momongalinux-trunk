%global momorel 1
%define tarball xf86-input-evdev
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/input

Summary:   Xorg X11 evdev input driver
Name:      xorg-x11-drv-evdev
Version: 2.8.4
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64 alpha sparc sparc64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 evdev input driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
autoreconf -ivf
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/evdev_drv.so
%{_includedir}/xorg/evdev-properties.h
%{_libdir}/pkgconfig/xorg-evdev.pc
%{_mandir}/man4/*.4*

%changelog
* Fri May 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.8.4-1m)
- update to 2.8.4

* Sun Mar 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.0-1m)
- update 2.8.0

* Sat Sep  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.3-2m)
- add fedora's patch

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.3-1m)
- update 2.7.3

* Thu Mar  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.0-1m)
- update 2.7

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.99.901-1m)
- update 2.6.99.901-20120125

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.0-4m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.0-3m)
- rebuild for xorg-x11-server-1.10.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-2m)
- rebuild for new GCC 4.6

* Wed Jan 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.0-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.0-1m)
- update 2.4.0

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-2m)
- rebuild against xorg-server-1.8

* Wed Jan  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.2-1m)
- update to 2.3.2

* Tue Dec  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-1m)
- update to 2.3.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.6-1m)
- update 2.2.6

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.5-1m)
- update 2.2.5

* Sun Aug  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.4-1m)
- update 2.2.4

* Fri May  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.2-1m)
- update 2.2.2

* Tue Mar 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.1-1m)
- update 2.2.1

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.0-1m)
- update 2.2.0

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.99.1-1m)
- update 2.1.99.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.8-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.8-1m)
- update 2.0.8

* Sun Oct 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.7-1m)
- update 2.0.7

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.6-1m)
- update 2.0.6

* Mon Sep 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.5-1m)
- update 2.0.5

* Fri Sep 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-3m)
- [CRASH FIX]
- fix KVM switching issue, re-import Fedora patches

* Sat Sep  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-2m)
- add fedora patches

* Fri Aug 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-1m)
- update 2.0.4

* Sat Aug  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.3-2m)
- fix default XkbLayout

* Mon Aug  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.3-1m)
- update to 2.0.3

* Fri Jul 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.2-1m)
- update to 2.0.2

* Wed Jun 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Tue Jun 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- update 2.0.0

* Fri Jun 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.4-1m)
- update 1.99.4

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.99.2-1m)
- update to 1.99.2
- comment out patch1 (merged)

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-5m)
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-4m)
- rebuild against gcc43

* Sat Mar 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-3m)
- update man page
-- see https://bugs.freedesktop.org/show_bug.cgi?id=12655

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-2m)
- rebuild against xorg-x11-server-1.4

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Sun Nov 12 2006  Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-1m)
- update 1.1.3

* Sat Aug  5 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-2m)
- added xorg-x11-drv-evdev-1.1.2-set_bit.patch

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2
- delete duplicated dir

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0.5-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0.5-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0.5-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0.5-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.0.5-1
- Updated xorg-x11-drv-evdev to version 1.0.0.5 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.4-1
- Updated xorg-x11-drv-evdev to version 1.0.0.4 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.2-1
- Updated xorg-x11-drv-evdev to version 1.0.0.2 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.1-1
- Updated xorg-x11-drv-evdev to version 1.0.0.1 from X11R7 RC1
- Fix *.la file removal.

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-0
- Initial spec file for evdev input driver generated automatically
  by my xorg-driverspecgen script.
