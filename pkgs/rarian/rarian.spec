%global momorel 9

### Abstract ###

Name: rarian
Version: 0.8.1
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Base
Summary: Documentation meta-data library
URL: http://rarian.freedesktop.org/
Source: ftp://ftp.gnome.org/pub/GNOME/sources/rarian/0.8/rarian-%{version}.tar.bz2
NoSource: 0
Source1: scrollkeeper-omf.dtd
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

### Patch ###

# RH bug #453342
Patch1: rarian-0.8.1-categories.patch

### Dependencies ###

Requires(post): libxml2
Requires(postun): libxml2
# for /usr/bin/xmlcatalog

Requires: libxslt
# for /usr/bin/xsltproc
Requires: coreutils, util-linux, gawk
# for basename, getopt, awk, etc

### Build Dependencies ###

BuildRequires: libxslt-devel

%description
Rarian is a documentation meta-data library that allows access to documents,
man pages and info pages.  It was designed as a replacement for scrollkeeper.

%package compat
License: GPLv2+
Group: System Environment/Base
Summary: Extra files for compatibility with scrollkeeper
Requires: rarian = %{version}-%{release}
Requires(post): rarian
# The scrollkeeper version is arbitrary.  It just
# needs to be greater than what we're obsoleting.
Provides: scrollkeeper = 0.4
Obsoletes: scrollkeeper <= 0.3.14

%description compat
This package contains files needed to maintain backward-compatibility with
scrollkeeper.

%package devel
Group: Development/Languages
Summary: Development files for Rarian
Requires: rarian = %{version}-%{release}
Requires: pkgconfig

%description devel
This package contains files required to develop applications that use the
Rarian library ("librarian").

%prep
%setup -q
%patch1 -p1 -b .categories

%build

%configure --disable-skdb-update
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_datadir}/xml/scrollkeeper/dtds
cp %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/xml/scrollkeeper/dtds

rm -rf $RPM_BUILD_ROOT%{_libdir}/librarian.a
rm -rf $RPM_BUILD_ROOT%{_libdir}/librarian.la

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%post compat
%{_bindir}/rarian-sk-update

# Add OMF DTD to XML catalog.
CATALOG=/etc/xml/catalog
/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
  "http://scrollkeeper.sourceforge.net/dtds/scrollkeeper-omf-1.0/scrollkeeper-omf.dtd" \
  "/usr/share/xml/scrollkeeper/dtds/scrollkeeper-omf.dtd" $CATALOG >& /dev/null || :
/usr/bin/xmlcatalog --noout --add "rewriteURI" \
  "http://scrollkeeper.sourceforge.net/dtds/scrollkeeper-omf-1.0/scrollkeeper-omf.dtd" \
  "/usr/share/xml/scrollkeeper/dtds/scrollkeeper-omf.dtd" $CATALOG >& /dev/null || :

%postun -p /sbin/ldconfig

%postun compat

# Delete OMF DTD from XML catalog.
if [ $1 = 0 ]; then
  CATALOG=/etc/xml/catalog
  /usr/bin/xmlcatalog --noout --del \
    "/usr/share/xml/scrollkeeper/dtds/scrollkeeper-omf.dtd" $CATALOG >& /dev/null || :
fi

%files
%defattr(-,root,root,-)
%doc README COPYING COPYING.LIB COPYING.UTILS ChangeLog NEWS AUTHORS
%{_bindir}/rarian-example
%{_libdir}/librarian.so.*
%{_datadir}/librarian
%{_datadir}/help

%files compat
%defattr(-,root,root,-)
%{_bindir}/rarian-sk-*
%{_bindir}/scrollkeeper-*
%{_datadir}/xml/scrollkeeper

%files devel
%defattr(644,root,root,755)
%{_includedir}/rarian
%{_libdir}/librarian.so
%{_libdir}/pkgconfig/rarian.pc

%changelog
* Thu Sep  8 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.1-9m)
- sync Fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-6m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-5m)
- disable-static

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-2m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- update 0.8.1

* Sun Jun  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-3m)
- add Requires: util-linux-ng

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- update 0.8.0

* Sun Sep 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- change package scrollkeeper to rarian

* Tue Feb  6 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildPreReq: perl-XML-Parser

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.14-4m)
- GNOME 2.12.1 Desktop

* Tue Nov 1 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
-  (0.3.14-3m)
- rebuild against libxslt-1.1.12-3m

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (0.3.14-2m)
- revised spec for rpm 4.2.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.3.14-1m)
- version 0.3.14
- GNOME 2.6 Desktop
- import from FC1
- adjustment BuildPreReq

* Wed Apr  7 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.12-3m)
- add PreReq: cml-common.

* Tue Oct 28 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3.12-2m)
- pretty spec file.
- add patch for cancel scrollkeeper-rebuilddb in %%makeinstall.

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.12-1m)
- version 0.3.12

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.11-3m)
- romania support

* Wed Sep  4 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.3.11-2m)
- [security] fix tempfile vulnerability. See: http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2002-0662
  add scrollkeeper-0.3.11-tempfiles.patch

* Fri Jul 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.11-1m)
- version 0.3.11

* Mon Jul 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.10-4m)
- enc.patch: bug fix
- postun script: bug fix

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.10-3m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Thu Jul 11 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3-10-2m)
- remove "%dir %{_localstatedir}/lib/scrollkeeper"

* Thu Jul 11 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.10-1m)
- upgrade to 0.3.10
- add BuildPreReq: docbook-dtds 
- rewrite %post %postun base on rawhide's spec

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.9-2k)
- version 0.3.9

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.6-12k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Wed May  8 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.6-10k)
- remove docbook-utils depends

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.6-8k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Tue Apr 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.6-6k)
- modify scrollkeeper.conf

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.6-4k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.6-2k)
- version 0.3.6

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.4-12k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.4-10k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.4-8k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.3.4-6k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.4-4k)
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12

* Mon Jan 28 2002 Shingo Akagaki <dora@kondara.org>
- (0.3.4-2k)
- version 0.3.4

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-12k)
- automake autoconf 1.5

* Thu Dec 20 2001 Shingo Akagaki <dora@kondara.org>
- (0.3-10k)
- update enc patch

* Fri Dec 20 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.3-8k)
- add BuildPrereq: libxslt-devel >= 1.0.9

* Thu Dec 20 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.3-6k)
- add BuildPrereq: intltool

* Wed Dec 19 2001 Shingo Akagaki <dora@kondara.org>
- (0.3-4k)
- add /etc/sysconfig/firsttime/scrollkeeper.sh

* Mon Dec 17 2001 Shingo Akagaki <dora@kondara.org>
- (0.3-2k)
- version 0.3

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.2-14k)
- nigittenu

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.2-12k)
- add scrollkeeper-0.2-get-index-from-docpath.patch

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (0.2-10k)
- rebuild against gettext 0.10.40.

* Thu Apr  5 2001 Shingo Akagaki <dora@kondara.org>
- fix files section

* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- version 0.2

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Mon Mar 5 2001 Dan Mueth <dan@eazel.com>
- Added %postun to remove $datadir/scrollkeeper/templates
  to compensate for breakage in upgrade from 0.1.1 to 0.1.2

* Sun Mar 4 2001 Dan Mueth <dan@eazel.com>
- Added cleaner symbolic link section suggested by Karl 
  Eichwalder <keichwa@users.sourceforge.net>
- Have it blow away the database dir on first install, just
  in case an old tarball version had been installed
- Fixing the Source0 line at the top

* Tue Feb 15 2001 Dan Mueth <dan@eazel.com>
- added line to include the translations .mo file

* Tue Feb 06 2001 Dan Mueth <dan@eazel.com>
- fixed up pre and post installation scripts

* Tue Feb 06 2001 Laszlo Kovacs <laszlo.kovacs@sun.com>
- added all the locale directories and links for the template
  content list files

* Wed Jan 17 2001 Gregory Leblanc <gleblanc@cu-portland.edu>
- converted to scrollkeeper.spec.in

* Sat Dec 16 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- help files added

* Fri Dec 8 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- various small fixes added

* Thu Dec 7 2000 Laszlo Kovacs <laszlo.kovacs@sun.com>
- fixing localstatedir problem
- adding postinstall and postuninstall scripts

* Tue Dec 5 2000 Gregory Leblanc <gleblanc@cu-portland.edu>
- adding COPYING, AUTHORS, etc
- fixed localstatedir for the OMF files

* Fri Nov 10 2000 Gregory Leblanc <gleblanc@cu-portland.edu>
- Initial spec file created.
