%global momorel 27

Summary: A library of handy utility functions
Name: glib1
Version: 1.2.10
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
Source0: ftp://ftp.gimp.org/pub/gtk/v1.2/glib-%{version}.tar.gz
NoSource: 0
Patch0: glib-1.2.10-g_printf_string_upper_bound.patch
Patch1: glib1-1.2.10-x86_64.patch
Patch2: glib.m4.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glibc-devel
BuildRequires: pkgconfig
URL: http://www.gtk.org/

%description
Glib is a handy library of utility functions. This C
library is designed to solve some portability problems
and provide other useful functionality which most
programs require.

Glib is used by GDK, GTK+ and many applications.
You should install Glib because many of your applications
will depend on this library.

%package devel
Summary: GIMP Toolkit and GIMP Drawing Kit support library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibc-devel
Requires: pkgconfig
Requires(post): info
Requires(postun): info

%description devel
Static libraries and header files for the support library for the GIMP's X
libraries, which are available as public libraries.  GLIB includes generally
useful data structures.

%prep
%setup -q -n glib-%{version}
%patch0 -p1 -b .g_printf_string_upper_bound
%patch1 -p1
%patch2 -p0 -b .m4~

%build
%ifarch ppc64
cp %{_datadir}/libtool/config.* .
%endif
# do not use %%configure macro due to accustomed libtool problem
./configure \
    --prefix=%{_prefix} \
    --bindir=%{_bindir} \
    --libdir=%{_libdir} \
    --includedir=%{_includedir} \
    --datadir=%{_datadir} \
    --mandir=%{_mandir} \
    --infodir=%{_infodir}
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

# remove
rm -f %{buildroot}/usr/share/info/dir

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/glib.info.* %{_infodir}/dir 

%postun -p /sbin/ldconfig

%postun devel
if [ "$1" = 0 ] ; then
    /sbin/install-info --delete %{_infodir}/glib.info.* %{_infodir}/dir
fi

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libglib-1.2.so.*
%{_libdir}/libgthread-1.2.so.*
%{_libdir}/libgmodule-1.2.so.*

%files devel
%defattr(-, root, root)
%{_prefix}/bin/glib-config
%{_libdir}/glib
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man1/*
%{_infodir}/glib.info*
%{_prefix}/share/aclocal/*
%{_prefix}/include/glib-1.2

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-27m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-26m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.10-25m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.10-24m)
- use BuildRequires and Requires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-23m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-21m)
- do not use %%configure macro

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-20m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-19m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.10-18m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.10-17m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.10-16m)
- enable ppc64

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.10-15m)
- suppress AC_DEFUN warning.

* Sat Jan 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.10-14m)
- enable x86_64.

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.10-13m)
- revised spec for enabling rpm 4.2.

* Sun Jul 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.10-13m)
- revise spec

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.10-12k)
- /sbin/install-info -> info in PreReq.

* Wed Mar  6 2002 Shingo Akagaki <dora@Kondara.org>
- (1.2.10-10k)
- change name to glib1

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- arrangement spec file

* Fri May 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- g_printf_string_upper_bound patch

* Thu Apr 26 2001 Shingo Akagaki <dora@kondara.org>
- add require pkgconfig

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.2.10

* Sun Mar 18 2001 Shingo Akagaki <dora@kondara.org>
- for Kondara2K
 
* Thu Mar  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.9-3k)
- update to 1.2.9

* Fri Dec 15 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.8-13k)
- fixed Jitter #807

* Tue Nov 21 2000 Shingo Akagaki <dora@kondara.org>
- we want rebuild glib with gcc-2.95.2 (__va_copy)

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.8-5k)
- fixed for FHS

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.8

* Thu Feb 17 2000 Akira Higuchi <a@kondara.org>
- version 1.2.7

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Oct 11 1999 Tsutomu Yasuda <tom@kondara.org>
- version 1.2.6

* Sat Sep 25 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- version 1.2.5

* Wed Aug 25 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- version 1.2.4

* Mon Jun 7 1999 Owen Taylor <otaylor@redhat.com>
- version 1.2.3

* Thu Mar 25 1999 Michael Fulbright <drmike@redhat.com>
- version 1.2.1

* Fri Feb 26 1999 Michael Fulbright <drmike@redhat.com>
- Version 1.2

* Thu Feb 25 1999 Michael Fulbright <drmike@redhat.com>
- version 1.2.0pre1

* Tue Feb 23 1999 Cristian Gafton <gafton@redhat.com>
- new description tags 

* Sun Feb 21 1999 Michael Fulbright <drmike@redhat.com>
- removed libtoolize from %build

* Thu Feb 11 1999 Michael Fulbright <drmike@redhat.com>
- added libgthread to file list

* Fri Feb 05 1999 Michael Fulbright <drmike@redhat.com>
- version 1.1.15

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- version 1.1.14

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- version 1.1.13

* Wed Jan 06 1999 Michael Fulbright <drmike@redhat.com>
- version 1.1.12

* Wed Dec 16 1998 Michael Fulbright <drmike@redhat.com>
- updated in preparation for the GNOME freeze

* Mon Apr 13 1998 Marc Ewing <marc@redhat.com>
- Split out glib package
