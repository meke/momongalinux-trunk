%global momorel 12
%global scim_ver 1.4.9
%global scim_major_version 1.4.0

Summary: An On Screen Input Pad for SCIM
Name: scim-input-pad
Version: 0.1.1
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://www.scim-im.org/
Group: Applications/System
Source0: http://dl.sourceforge.net/sourceforge/scim/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0: %{name}-rpath.patch
Patch1: %{name}-libexecdir.patch
Requires: scim >= %{scim_ver}
BuildRequires: scim-devel >= %{scim_ver}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: expat-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: libX11-devel
BuildRequires: libXrender-devel
BuildRequires: libpng-devel
BuildRequires: libtool
BuildRequires: libxcb-devel
BuildRequires: pango-devel
BuildRequires: zlib-devel

%description
Scim-input-pad is an on-screen input pad used to input symbols easily.

%prep
%setup -q

%patch0 -p0 -b .rpath
%patch1 -p1 -b .libexec

# build fix and for patches
autoreconf -fi

%build
%configure --disable-static
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/scim-1.0/%{scim_major_version}/Helper/input-pad.la
rm -f %{buildroot}%{_libdir}/lib%{name}.{la,so}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING ChangeLog INSTALL README TODO
%{_bindir}/%{name}
%{_libdir}/scim-1.0/%{scim_major_version}/Helper/input-pad.so
%{_libdir}/lib%{name}.so.*
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/scim/icons/input-pad.png
%{_datadir}/scim/input-pad

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-12m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-9m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-8m)
- touch up spec file

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-5m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-2m)
- %%NoSource -> NoSource

* Wed Apr 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-1m)
- initial package for Momonga Linux
- import rpath.patch and libexecdir.patch from Fedora
