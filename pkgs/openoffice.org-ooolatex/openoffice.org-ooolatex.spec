%global momorel 8
%global oxtfilename OOoLatex.oxt

%global emfsrcname OOoLatexEmf_noarch_src
#%%global emfdate 2007.03.20
%global realname ooolatex
%global ooodir %{_libdir}/openoffice.org3.0

Summary: LaTeX extensions to OpenOffice.org
Name:    openoffice.org-ooolatex
Version: 4.0.0
%global betaver 2
%global srcver %{version}-beta-%{betaver}
Release: 0.%{betaver}.%{momorel}m%{?dist}
Group:   Applications/Productivity
License: GPL
URL:     http://ooolatex.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{realname}/OOoLatex-%{srcver}-linux.oxt
NoSource: 0
Source1: http://dl.sourceforge.net/sourceforge/%{realname}/%{emfsrcname}.tar.gz 
NoSource: 1
Source2: OOoLatex.sh
Source3: temporal-icons.tar.bz2
Patch0: ooolatex-4.0.0-scriptpath.patch
Patch1: ooolatex-4.0.0-defaultpath.patch
Patch2: ooolatex-4.0.0-enable_addon_menu_in_tool_menu.patch
Patch3: ooolatex-4.0.0-fix_position_in_toolbar.patch
Patch4: ooolatex-4.0.0-enable_in_draw.patch
Patch10: ooolatexemf-20050919.makefile.patch
Requires: openoffice.org-core
Requires: tetex-latex, ghostscript >= 8.60
Requires: epstool, libemf
Requires(post,preun): openoffice.org-core
BuildRequires: libemf-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
OOoLatex is an OpenOffice.org marco. It intends to provide Latex support like inserting equations as images (png, eps and emf). The latex code is saved in the image attribute to allow equation edition.

%prep
%setup -q -c -T -a 1 -a 3
mkdir -p OOoLatex
(cd OOoLatex
unzip -o %{SOURCE0} -d .
rm -rf bin
rm -rf lib
%patch0 -p0
%patch1 -p0
%patch2 -p0
%patch3 -p0
%patch4 -p0
)
(cd %{emfsrcname}
%patch10 -p1 -b .orig
)

%build
(cd %{emfsrcname}/Linux
 make all
)
# temporal icons
mkdir -p OOoLatex/icons
cp -f ooolatex-equation.bmp OOoLatex/icons/image1_16.bmp
cp -f ooolatex-equation.bmp OOoLatex/icons/image1_26.bmp
cp -f ooolatex-expand.bmp OOoLatex/icons/image2_16.bmp
cp -f ooolatex-expand.bmp OOoLatex/icons/image2_26.bmp
cp -f ooolatex.bmp OOoLatex/icons/image3_16.bmp
cp -f ooolatex.bmp OOoLatex/icons/image3_26.bmp

(cd OOoLatex
zip -r %{oxtfilename} .
)

cp %{emfsrcname}/README README.%{emfsrcname}

%install
rm -rf %{buildroot}
# copy extension file
mkdir -p %{buildroot}%{_datadir}/%{name}
cp -pf OOoLatex/%{oxtfilename} %{buildroot}%{_datadir}/%{name}
# install script and binaries
mkdir -p %{buildroot}%{_bindir}
install -m 755 %{SOURCE2} %{buildroot}%{_bindir}
install -m 755 %{emfsrcname}/Linux/latex2emf %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%post
# registration to OOo
# for detailed usage of unopkg, see a following link
# http://docs.sun.com/app/docs/doc/819-1346/6n3n8r056?l=ja&a=view
# From 4.0.0, we should always do remove-add process for every upgrade to register extension file
if [ "$1" -ne 1 ]; then
    if [ -x %{ooodir}/program/unopkg ]; then
# for upgrade from 3.0
	%{ooodir}/program/unopkg remove -f --shared OOoLatex > /dev/null 2>&1
# if upgrade, uninstall first
	%{ooodir}/program/unopkg remove -f --shared %{oxtfilename}  || \
	    echo "ERROR: Extension registering may fail if concurrent OOo process(es) are running. Plese quit all OOo process(es) before update/remove."
    fi
fi
# then install
if [ -x %{ooodir}/program/unopkg ]; then
    (echo yes | %{ooodir}/program/unopkg add -f --shared %{_datadir}/%{name}/%{oxtfilename} > /dev/null )&& \
	echo "Notice: Installing Bakoma and STIX font is required to use EMF mode." && \
	echo  "Notice: For more information, see http://ooolatex.sf.net/" \	    || echo "ERROR: Extension registering may fail if concurrent OOo process(es) are running. Plese quit all OOo process(es) before update/remove."
fi

%preun
# for uninstall
if [ "$1" -eq 0 ]; then
    if [ -x %{ooodir}/program/unopkg ]; then
# uninstall first
	%{ooodir}/program/unopkg remove -f --shared %{oxtfilename}  || \
	    echo "ERROR: Extension registering may fail if concurrent OOo process(es) are running. Plese quit all OOo process(es) before update/remove."
    fi
fi

%files
%defattr(-,root,root)
%doc OOoLatex/Description.txt OOoLatex/README OOoLatex/ChangeLogs.txt OOoLatex/pkg-licence/gpl_GB.txt
%doc README* %{emfsrcname}/License 
%{_bindir}/*
%{_datadir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.0-0.2.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.0-0.2.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.0-0.2.6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-0.2.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.0.0-0.2.4m)
- rebuild against OOo-3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-0.2.3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-0.2.2m)
- rebuild against gcc43

* Fri Mar 28 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.0.0-0.2.1m)
- update to 4.0.0-beta-2
- add Menu in Tools->Addon
- use icons in toolbar (add temporal icons)

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-0.5.2m)
- %%NoSource -> NoSource

* Tue May 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0-0.5.1m)
- 3.0-test5 release

* Wed Nov 22 2006  Masayuki SANO <nosanosa@momonga-linux.org>
- (0-0.4.2m)
- update ooolatexemf-20050919.makefile.patch
- - for make it enable to build in x86_64, please test.

* Wed Nov 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0-0.4.1m)
- imported to Momonga
- OOoLatexMacro-test4 and OOoLatexEmf-2005.09.19

* Wed May 31 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (0-0.0.0.test4.1m)
- build for Momonga
- converter for PNG image is changed to "convert" from "gs" since ghostscript in Momonga is still 7.0x
