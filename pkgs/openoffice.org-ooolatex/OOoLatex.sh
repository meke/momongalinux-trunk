#!/bin/sh
#
# This script is part of the OOoLatex package
# http://ooolatex.sourceforge.net
#
# Geoffroy Piroux (gpiroux@gmail.com)


### Some error function definitions ###########################################
error(){
    echo "Error during the processing of ${1}" > error.log
    cat error.log
    exit 1
}

SystemLog(){
# Set the execution permission on latex2emf
chmod 775 "${PkgDir}bin/${osType}${cpuType}/latex2emf"

syslog=${PkgDir}System.out

uname -a > $syslog
echo "" >> $syslog
echo "osType=$osType" >> $syslog
echo "cpuType=$cpuType" >> $syslog
echo "" >> $syslog
echo "PATH=$PATH" >> $syslog
echo "" >> $syslog
echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH" >> $syslog
echo "DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH" >> $syslog
}


### Evironment variable definition ###########################################
# Option for the latex2emf => from config window
emfopt=pdf


# Definition of PATH and binary paths
gs=gs
cpu=$(uname -p)
mac=$(uname -m)
sys=$(uname -s)
[ "$cpu" = "powerpc" -o "$mac" = "ppc" ] && cpuType=ppc || cpuType=i386 
[ "$sys" = "Darwin" ] && osType=MacOSX || osType=Linux 
#PkgDir="/usr/lib/openoffice.org2.0/program/../share/uno_packages/cache/uno_packages/WZIliP_/OOoLatex-4.0.0-beta-2-linux.oxt/"

#PATH="${PkgDir}bin/${osType}${cpuType}:$PATH"
#PATH="/usr/bin/:$PATH"
#PATH="/usr/bin/:$PATH"
#export PATH

if [ "$osType" = "MacOSX" ]
then
   DYLD_LIBRARY_PATH="${PkgDir}lib/${cpuType}/"
   export DYLD_LIBRARY_PATH
#else
#   LD_LIBRARY_PATH="${PkgDir}lib/${cpuType}/"
#   export LD_LIBRARY_PATH
fi


#ls "${PkgDir}System.out" || SystemLog

### Process the options...####################################################
ext=$1
trs=$2
dpi=$3
tmpPath="$4"
filename=tmpfile

### go to the tmp directory ##################################################
if [ "$tmpPath" != "" ] 
then
   [ ! -d "$tmpPath" ]  && mkdir -p "$tmpPath"
   cd "$tmpPath"
fi


### Remove old files... #####################################################
rm ${filename}.{ps,dvi,out,bbx,log,aux,eps,png,ascii,emf} > /dev/null 2>&1
rm error.log &> /dev/null


### Creatation of the ps file ... ###########################################
latex -interaction=nonstopmode ${filename}.tex  > ${filename}.out  2>&1 \
      || exit 1

[ "${ext}" = "emf" ] && opts='-G1' || opts=""
dvips ${opts} -E ${filename} -o > /dev/null 2>&1 || error dvips

$gs -q -dNOPAUSE -dBATCH -sDEVICE=bbox ${filename}.ps  > ${filename}.bbx 2>&1
bbx=$(grep '%%BoundingBox' ${filename}.ps |awk '{print $2,$3,$4,$5}')
BBx=$(grep '%%HiResBoundingBox' ${filename}.bbx |awk '{print $2,$3,$4,$5}')
echo $bbx $BBx > ${filename}.bbx 

### Conversion of the image ##################################################
##
if [ "${ext}" = "png" ]
then
   [ "${trs}" = "-notransp" ] && device=png16m || device=pngalpha 
   $gs -dSAFTER -dBATCH -dNOPAUSE -sDEVICE=$device -dEPSCrop -r${dpi} \
      -sOutputFile=${filename}.png ${filename}.ps
##
elif [ "${ext}" = "emf" ]
then
   [ "${emfopt}"   =  "pdf" ] && ps2pdf ${filename}.ps
   [ "${trs}" = "-notransp" ] && trs=
   $gs -q -dBATCH -dNODISPLAY -dSAFER -dDELAYBIND -dWRITESYSTEMDICT -dCOMPLEX \
      ps2ascii.ps -f ${filename}.${emfopt} | \
      sed 's/\(^F[^(]*\)(......+/\1(/' | \
      sed 's/\(^F[^?]*\)?/\1/' > ${filename}.ascii
   latex2emf ${trs} -bb ${bbx} ${filename}.ascii ${filename}.emf
fi