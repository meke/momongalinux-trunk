%global enable_debug 1
%global momorel 10

Name: libaal
Version: 1.0.5
Release: %{momorel}m%{?dist}
Summary: Abstraction library for ReiserFS utilities
License: GPL
Group: Development/Libraries
URL: http://www.namesys.com/
Source0: ftp://ftp.namesys.com/pub/reiser4progs/%{name}-%{version}.tar.gz 
#NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a library that provides application abstraction mechanism.
It include device abstraction, libc independence code, etc.

%package devel
Summary: Headers and static libraries for developing with libaal.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package includes the headers and static libraries for developing
with the libaal library.

%prep
%setup -q

%build
%if %{enable_debug}
BUILD_OPTS="$BUILD_OPTS --enable-debug"
%endif

%configure \
%if %{enable_debug}
        --enable-debug \
%endif
        --enable-libminimal
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING CREDITS INSTALL NEWS README THANKS TODO
%{_libdir}/libaal-1.0.so.*
%{_libdir}/libaal-minimal.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/libaal.so
%{_libdir}/libaal.*a
%{_libdir}/libaal-minimal.so
%{_libdir}/libaal-minimal.a
%dir %{_includedir}/aal
%{_includedir}/aal/*
%{_datadir}/aclocal/libaal.m4

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-8m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-2m)
- delete libtool library

* Tue Nov  1 2005 Yohsuke Ooi <meme@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Wed Mar  2 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.4-1m)
  update to 1.0.4

* Thu Jan 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-2m)
- make libaal-minimal files

* Tue Dec 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.3-1m)
  update to 1.0.3

* Fri Sep 17 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.0-1m)
  imported

* Fri Aug 29 2003 Yury V Umanets <umka@namesys.com>
- Different cleanups in this spec file
* Wed Aug 27 2003 David T Hollis <dhollis@davehollis.com>
- RPM package created

