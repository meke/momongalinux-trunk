%global momorel 1

Name:           pixman
Version:        0.32.4
Release:        %{momorel}m%{?dist}
Summary:        Pixel manipulation library

Group:          System Environment/Libraries
License:        MIT
URL:            http://gitweb.freedesktop.org/?p=pixman.git;a=summary
Source0: http://xorg.freedesktop.org/releases/individual/lib/pixman-%{version}.tar.bz2 
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  automake autoconf libtool pkgconfig

%description
Pixman is a pixel manipulation library for X and cairo.

%package devel
Summary: Pixel manipulation library development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Development library for pixman.

%prep
%setup -q

%build
%configure --disable-static
%make 

%install
rm --preserve-root -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm --preserve-root -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libpixman-1.so.*
%exclude %{_libdir}/libpixman-1.la

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/pixman-1
%{_includedir}/pixman-1/pixman.h
%{_includedir}/pixman-1/pixman-version.h
%{_libdir}/libpixman-1.so
%{_libdir}/pkgconfig/pixman-1.pc

%changelog
* Sun Jan 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.32.4-1m)
- update to 0.32.4

* Tue Nov 19 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31.2-1m)
- update to 0.31.2

* Fri Aug  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.30.2-1m)
- update to 0.30.2

* Mon May 13 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.30.0-1m)
- update to 0.30.0

* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.28.2-1m)
- update to 0.28.2

* Fri Nov  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.28.0-1m)
- update to 0.28.0

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.26.2-1m)
- update to 0.26.2

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.24.4-1m)
- update to 0.24.4

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.24.2-1m)
- update to 0.24.2

* Thu Nov 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.24.0-1m)
- update to 0.24.0

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.23.8-1m)
- update to 0.23.8

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.23.6-1m)
- update to 0.23.6

* Wed Sep 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.23.4-1m)
- update to 0.23.4

* Tue Jul  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.22.2-1m)
- update to 0.22.2

* Mon May  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.22.0-1m)
- update to 0.22.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.2-2m)
- rebuild for new GCC 4.6

* Thu Jan 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20.2-1m)
- update to 0.20.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.0-1m)
- update to 0.20.0

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19.6-1m)
- update 0.19.6

* Thu Sep 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.18.4-1m)
- update 0.18.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18.2-2m)
- full rebuild for mo7 release

* Wed May 19 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18.2-1m)
- update 0.18.2

* Fri Apr  2 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18.0-1m)
- update 0.18.0

* Wed Dec 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16.4-1m)
- update 0.16.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16.2-1m)
- update 0.16.2

* Wed Sep  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16.0-1m)
- update 0.16.0

* Tue Jun 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15.10-3m)
- revert to 0.15.10
- 0.15.14 breaks tray-icons of KDE4 too

* Tue Jun 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.14-1m)
- update 0.15.14

* Thu Jun 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15.10-2m)
- revert to 0.15.10
- 0.15.12 breaks tray-icons of KDE4

* Thu Jun 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.12-1m)
- update 0.15.12

* Fri Jun  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.10-1m)
- update 0.15.10

* Tue Jun  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.8-1m)
- update 0.15.8

* Mon May 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.6-1m)
- update 0.15.6

* Sun May 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.4-1m)
- update 0.15.4

* Mon Apr 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.2-1m)
- update 0.15.2

* Tue Feb 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.0-1m)
- update 0.14.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.0-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Wed Sep 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.8-2m)
- revert to 0.11.8
- 0.11.10 breaks input areas of gtk2-windows
- modify %%post and %%postun

* Tue Sep  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.10-1m)
- update to 0.11.10

* Fri Jul 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.8-1m)
- update to 0.11.8

* Fri Jun 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.6-1m)
- update to 0.11.6

* Fri Jun 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.4-1m)
- update to 0.11.4

* Mon Apr  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-2m)
- %%NoSource -> NoSource

* Sat Oct 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6

* Wed Sep  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Fri Aug 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-1m)
- initial commit

