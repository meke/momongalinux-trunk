%global momorel 5

Name:           lxterminal
Version:        0.1.8
Release:        %{momorel}m%{?dist}
Summary:        Desktop-independent VTE-based terminal emulator

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
Patch1:         lxterminal-0.1.5-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.6
BuildRequires:  desktop-file-utils intltool gettext
BuildRequires:  vte028-devel >= 0.20.5

%description
LXterminal is a VTE-based terminal emulator with support for multiple tabs. 
It is completely desktop-independent and does not have any unnecessary 
dependencies. In order to reduce memory usage and increase the performance 
all instances of the terminal are sharing a single process.

%prep
%setup -q
%patch1 -p1 -b .desktop

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
desktop-file-install --vendor=                             \
  --delete-original                                        \
  --remove-category=Utility                                \
  --add-category=System                                    \
  --dir=${RPM_BUILD_ROOT}%{_datadir}/applications          \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
%{_mandir}/man1/%{name}*.1*


%changelog
* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-5m)
- fix BuildRequires; use vte028

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.8-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.8-1m)
- update to 0.1.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.6-1m)
- update to 0.1.6
-- drop Patch1, merged upstream

* Thu Jun 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5-4m)
- add patch1 (add NotShowIn=GNOME;)

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5-3m)
- rebuild against vte-0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5-2m)
- rebuild against vte-0.20.4

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.5-1m)
- update to 0.1.5

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.4-1m)
- import from Rawhide

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Dec 26 2008 Christoph Wickert <fedora christoph-wickert de> - 0.1.4-1
- Update to 0.1.4.

* Sat Jun 28 2008 Christoph Wickert <fedora christoph-wickert de> - 0.1.3-1
- Update to 0.1.3
- Add the new manpage

* Fri Jun 20 2008 Christoph Wickert <fedora christoph-wickert de> - 0.1.2-1
- Initial Fedora package
