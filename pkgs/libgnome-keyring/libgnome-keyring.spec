%global momorel 1
%define glib2_version 2.16.0
%define dbus_version 1.0
%define gcrypt_version 1.2.2

Summary: Framework for managing passwords and other secrets
Name: libgnome-keyring
Version: 3.6.0
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: System Environment/Libraries
#VCS: git:git://git.gnome.org/libgnome-keyring
Source: http://download.gnome.org/sources/libgnome-keyring/3.6/libgnome-keyring-%{version}.tar.xz
NoSource: 0
URL: http://live.gnome.org/GnomeKeyring


BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: dbus-devel >= %{dbus_version}
BuildRequires: libgcrypt-devel >= %{gcrypt_version}
BuildRequires: intltool
BuildRequires: gobject-introspection-devel
Conflicts: gnome-keyring < 2.29.4


%description
gnome-keyring is a program that keep password and other secrets for
users. The library libgnome-keyring is used by applications to integrate
with the gnome-keyring system.

%package devel
Summary: Development files for libgnome-keyring
License: LGPLv2+
Group: Development/Libraries
Requires: %name = %{version}-%{release}
Requires: glib2-devel
Conflicts: gnome-keyring-devel < 2.29.4

%description devel
The libgnome-keyring-devel package contains the libraries and
header files needed to develop applications that use libgnome-keyring.


%prep
%setup -q -n libgnome-keyring-%{version}


%build
%configure --disable-gtk-doc --enable-introspection=yes

# avoid unneeded direct dependencies
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' libtool

%make

%install
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT%{_libdir}/*.la

%find_lang libgnome-keyring


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f libgnome-keyring.lang
%doc AUTHORS NEWS README COPYING HACKING
%{_libdir}/lib*.so.*
%{_libdir}/girepository-1.0

%files devel
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*
%{_datadir}/gir-1.0
%doc %{_datadir}/gtk-doc/
%{_datadir}/vala/vapi/gnome-keyring-1.vapi

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.6-1m)
- update to 3.5.6

* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Sat Jun 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for glib 2.33.2

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.4-2m)
- fix scripts

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.4-1m)
- initial build
