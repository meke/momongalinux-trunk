%global momorel 4

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?ruby_sitearch: %global ruby_sitearch %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"] ')}

Name:		libprelude
Version:	1.0.1
Release:	%{momorel}m%{?dist}
Summary:	The prelude library        

Group:		System Environment/Libraries 
License:	GPLv2+
URL:		http://prelude-ids.org/
Source0:        https://www.prelude-ids.org/attachments/download/241/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:         libprelude-1.0.0-libtool-2.4.patch
Patch1:         libprelude-1.0.0-gcc46.patch
Patch2:         libprelude-1.0.1-ltdl.patch
Patch3:         libprelude-1.0.1-gets.patch
Patch4:         libprelude-1.0.1-swig.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: libtool-ltdl-devel >= 2.4
BuildRequires: python-devel >= 2.7
BuildRequires: ruby18-devel
BuildRequires: lua-devel
BuildRequires: swig chrpath

%description
Libprelude is a library that guarantees secure connections between
all sensors and the Prelude Manager. Libprelude provides an 
Application Programming Interface (API) for the communication with
Prelude sub-systems, it supplies the necessary functionality for
generating and emitting IDMEF events with Prelude and automates the
saving and re-transmission of data in times of temporary interruption
of one of the components of the system.

%package devel
Summary: 	Header files and libraries for libprelude development
Group:		System Environment/Libraries    
Requires: 	libprelude = %{version}-%{release}, automake, gnutls-devel

%description devel
Libraries, include files, etc you can use to develop Prelude IDS
sensors using the Prelude Library.

%package python
Summary:	Python bindings for libprelude
Group:		System Environment/Libraries
Requires:	libprelude = %{version}-%{release}

%description python
Python bindings for libprelude.

%package perl
Summary:	Perl bindings for libprelude
Group:		System Environment/Libraries
BuildRequires:	perl-devel
Requires:	libprelude = %{version}-%{release}
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description perl
Perl bindings for libprelude.

%package ruby
Summary:	Ruby bindings for libprelude
Group:		System Environment/Libraries
Requires:	libprelude = %{version}-%{release}
Requires:	ruby(abi) = 1.8

%description ruby
Ruby bindings for libprelude.

%prep
%setup -q
%patch0 -p1 -b .libtool
%patch1 -p1 -b .gcc46~
%patch2 -p1 -b .ltdl
%patch3 -p1 -b .gets
%patch4 -p1 -b .swig

%build
RUBY="%{_bindir}/ruby18"
export RUBY
%configure	--disable-static \
		--with-html-dir=%{_defaultdocdir}/%{name}-%{version}/html \
		--with-perl-installdirs=vendor \
		--enable-easy-bindings \
		--without-included-ltdl

# removing rpath
sed -i.rpath -e 's|LD_RUN_PATH=""||' bindings/Makefile.in
sed -i.rpath -e 's|^sys_lib_dlsearch_path_spec="/lib /usr/lib|sys_lib_dlsearch_path_spec="/%{_lib} %{_libdir}|' libtool

make %{?_smp_mflags} 

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_defaultdocdir}/%{name}-%{version}
mkdir -p %{buildroot}%{perl_vendorarch}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -c -p" transform='s,x,x,'
cp -p AUTHORS ChangeLog README NEWS COPYING LICENSE.README HACKING.README \
	%{buildroot}%{_defaultdocdir}/%{name}-%{version}
rm -f %{buildroot}/%{_libdir}/libprelude.la
chmod 755 %{buildroot}%{python_sitearch}/_prelude.so
find %{buildroot} -type f \( -name .packlist -o -name perllocal.pod \) -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{ruby_sitearch}/PreludeEasy.la
chmod +w %{buildroot}%{perl_vendorarch}/auto/Prelude/Prelude.so
chrpath -d %{buildroot}%{perl_vendorarch}/auto/Prelude/Prelude.so
chmod -w %{buildroot}%{perl_vendorarch}/auto/Prelude/Prelude.so

# Fix time stamp for both 32 and 64 bit libraries
touch -r ./configure.in %{buildroot}%{_sysconfdir}/prelude/default/*

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_bindir}/prelude-admin
%{_bindir}/prelude-adduser
%{_libdir}/*.so.*
%{_mandir}/man1/prelude-admin.1*
%config(noreplace) %{_sysconfdir}/*
%{_localstatedir}/spool/*
%dir %{_defaultdocdir}/%{name}-%{version}/
%doc %{_defaultdocdir}/%{name}-%{version}/*

%files devel
%defattr(-,root,root)
%{_bindir}/libprelude-config
%{_libdir}/*.so
%dir %{_includedir}/libprelude/
%{_includedir}/libprelude/*
%{_datadir}/aclocal/libprelude.m4
%{_libdir}/pkgconfig/libprelude.pc

%files python
%defattr(-,root,root)
%{python_sitearch}/*

%files perl
%defattr(0755,root,root)
%attr(0644,root,root) %{perl_vendorarch}/Prelude*.pm
%{perl_vendorarch}/auto/Prelude*/

%files ruby
%defattr(-,root,root)
%{ruby_sitearch}/PreludeEasy.so

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-2m)
- rebuild against perl-5.18.1

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1
- rebuild against gnutls-3.2.0

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-17m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-16m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-15m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-14m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-13m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-10m)
- rebuild against perl-5.14.0-0.2.1m

* Sat Apr 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-8m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-7m)
- add patch for gcc46, generated by gen46patch

* Sun Feb 20 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-6m)
- fix BR ruby18-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-5m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-4m)
- rebuild against perl-5.12.2

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-3m)
- build fix with libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0 with ruby18

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.22-7m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.22-6m)
- rebuild against perl-5.12.0

* Sat Jan  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.22-5m)
- [SECURITY] CVE-2009-3736
- add --without-included-ltdl explicitly

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.22-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.22-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.22-2m)
- rebuild against perl-5.10.1

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.22-1m)
- update to 0.9.22 based on Fedora 11 (0.9.21.2-9)

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.17.2-6m)
- rebuild against libtool-ltdl-2.2.6-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.17.2-5m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.17.2-4m)
- rebuild against python-2.6.1-2m

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.17.2-3m)
- rebuild against gnutls-2.4.1

* Sun Jul  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.17.2-2m)
- change Source0 URL

* Thu May 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.17.2-1m)
- update 0.9.17.2
- Initial commit Momonga Linux

* Fri May 02 2008 Steve Grubb <sgrubb@redhat.com> 0.9.17.1-1
- New upstream version

* Thu Apr 24 2008 Steve Grubb <sgrubb@redhat.com> 0.9.17-1
- New upstream version

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.16.2-2
- Autorebuild for GCC 4.3

* Wed Jan 23 2008 Steve Grubb <sgrubb@redhat.com> 0.9.16.2-1
- New upstream version

* Mon Jan 14 2008 Steve Grubb <sgrubb@redhat.com> 0.9.16.1-1
- moved to new upstream version 0.9.16.1

* Tue Feb 20 2007 Thorsten Scherf <tscherf@redhat.com> 0.9.13-1
- moved to new upstream version 0.9.13-1

* Fri Jan 05 2007 Thorsten Scherf <tscherf@redhat.com> 0.9.12.1-1
- moved to new upstream version 0.9.12.1

* Tue Dec 30 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.12-6
- fixed x86_86 arch problem

* Tue Dec 30 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.12-5
- added ExcludeArch

* Tue Dec 29 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.12-4
- resolved permission problems
- added new docs 

* Tue Dec 25 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.12-3
- changed dir owner and preserved timestamps when building the package
- resolved rpath problems

* Fri Dec 22 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.12-2
- moved perl_sidearch into perl_vendorarch
- minor corrections in the spec file

* Fri Dec 22 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.12-1
- upgrade to latest upstream version 0.9.12
- minor corrections in the spec file

* Wed Dec 20 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.11-4
- removing smp-flag to debug perl- and python-problems
- added perl-bindings again

* Wed Dec 20 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.11-3
- disabled perl-bindings

* Mon Nov 20 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.11-2
- Some minor fixes in requirements

* Tue Oct 24 2006 Thorsten Scherf <tscherf@redhat.com> 0.9.11-1
- New Fedora build based on release 0.9.11
