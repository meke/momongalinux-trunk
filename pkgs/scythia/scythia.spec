%global momorel 7

Name:           scythia
Version:        0.9.3
Release:        %{momorel}m%{?dist}
Summary:        Just a small ftp client
Group:          Applications/Internet
License:        GPLv3+
URL:            http://%{name}.free.fr/
Source0:        http://%{name}.free.fr/wp-content/%{name}_%{version}-2-src.tar.gz
Source1:        %{name}.desktop
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= 4.7.0
BuildRequires:  desktop-file-utils
Requires:       hicolor-icon-theme

%description
Scythia project is an simple and portable Ftp client. It does not claim to be
able to replace the biggest (no SSH etc.), but only to satisfy some persons
and to give us a bigger experience in programming.

%prep
%setup -q -n %{name}
chmod a-x AUTHORS COPYING

%build
qmake-qt4 scythia.pro
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot} INSTALL="install -p"
rm %{buildroot}%{_datadir}/applnk/Internet/scythia.desktop
rm %{buildroot}%{_docdir}/scythia/html/aide_8h-source.html

desktop-file-install --vendor=                      \
  --dir=%{buildroot}%{_datadir}/applications         \
  %{SOURCE1}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_bindir}/%{name}
%{_docdir}/%{name}
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
%{_datadir}/pixmaps/%{name}.xpm
%{_datadir}/applications/%{name}.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3-6m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momong-liux.org>
- (0.9.3-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-3m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-1m)
- import from Fedora devel

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.3-4.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.3-3.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 06 2008 Krzysztof Kurzawski <kurzawax at gmail.com> - 0.9.3-2.2
- Correct license
- Correct .desktop file

* Wed Feb 06 2008 Krzysztof Kurzawski <kurzawax at gmail.com> - 0.9.3-2.1
- First release
