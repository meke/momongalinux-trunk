%global momorel 1

Summary: Network traffic recorder
Name: tcpflow
Version: 1.2.3
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source0: http://afflib.org/downloads/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://afflib.org/software/tcpflow/
BuildRequires: libpcap-devel >= 1.1.1
Requires: libpcap
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
tcpflow is a program that captures data transmitted as part of TCP connections (flows), and stores the data in a way that is convenient for protocol analysis or debugging. A program like 'tcpdump' shows a summary of packets seen on the wire, but usually doesn't store the data that's actually being transmitted. In contrast, tcpflow reconstructs the actual data streams and stores each flow in a separate file for later analysis.

%prep
%setup

%build
chmod -x COPYING
%configure --localstatedir=/var/lib
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_mandir}/man*/*

%changelog
* Sat Mar 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Mon Mar 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.21-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.21-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.21-4m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-3m)
- rebuild against libpcap-1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.21-1m)
- welcome to Momonga

* Thu Apr 22 1999 Ross Golder <rossigee@bigfoot.com>
- Wrote for version 0.12
