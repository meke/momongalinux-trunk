%global momorel 7

Summary: gnugo - GNU Go, a Go program
Name: gnugo
Version: 3.8
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Amusements/Games
URL: http://www.gnu.org/software/gnugo/gnugo.html
Source0: ftp://ftp.gnu.org/gnu/gnugo/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel
BuildRequires: readline-devel
Requires(post): info
Requires(preun): info

%description
This is GNU Go, a Go program. Development versions of GNU Go may be
found at http://www.gnu.org/software/gnugo/devel.html. Consult TODO if
you are interested in helping.

%prep
rm -rf %{buildroot}

%setup -q

%build
%configure --enable-color --with-readline
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

# remove
rm -f %{buildroot}%{_bindir}/debugboard
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/gnugo.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/gnugo.info %{_infodir}/dir
fi

%files
%defattr(- ,root,root,-)
%{_bindir}/gnugo
%{_infodir}/gnugo.info*
%{_mandir}/man6/gnugo.6*
%doc AUTHORS COPYING ChangeLog NEWS README

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8-5m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8-4m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8-3m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8-1m)
- update to 3.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-5m)
- rebuild against gcc43

* Tue Feb  8 2005 Toru Hoshina <t@momonga-linux.org>
- (3.2-4m)
- force compiling with gcc 3.2. rebuild against new environment.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2-3m)
- revised spec for enabling rpm 4.2.

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Mon Oct  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.0-2k)
- update to 3.0.0

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.6-5k)
- imported gnugo.desktop

* Wed Nov 22 2000 Kenichi Matsubara <m@kondara.org>
- (2.6-1k)
- specfile many bugfix. (-_-)

* Tue Oct 20 2000 KURASHIKI Satoru<ouka@fx.sakura.ne.jp>
- 1st release.
