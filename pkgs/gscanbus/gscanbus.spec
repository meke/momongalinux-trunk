%global momorel 20
Name: gscanbus
Version: 0.7.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
Source0: %{name}/%{name}-%{version}.tgz
Source1: gscanbus.desktop
Patch0: installdir.patch
Patch1: gscanbus-0.7.1-gcc34.patch

URL: http://gscanbus.berlios.de/
BuildRequires: libraw1394-devel >= 2.0.2, gtk+1-devel >= 1.2
Requires: libraw1394 >= 1.1.0, gtk+1 >= 1.2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Summary: a little bus scanning, testing and topology visualizing tool for the Linux IEEE1394 subsystem

%description 
gscanbus is a little bus scanning, testing and topology visualizing tool for
the Linux IEEE1394 subsystem, with some AV/C support, especially for
controlling Camcorders and VCRs. It is intended as a debugging tool in
IEEE1394 development, but can also be used to simply check your IEEE1394 setup
on Linux.

%prep
rm -rf %{buildroot}

%setup -q
%patch0 -p1 -b .installdir
%patch1 -p1 -b .gcc34

%build
%configure
%make

%install
%makeinstall

# install desktop file
install -m 0755 -d %{buildroot}%{_datadir}/applications
install -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

# install icon
install -m 0755 -d %{buildroot}%{_datadir}/pixmaps
install -m 0644 gnome-qeye.xpm %{buildroot}%{_datadir}/pixmaps/gscanbus.xpm

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%config %{_sysconfdir}/guid-resolv.conf
%config %{_sysconfdir}/oui-resolv.conf
%{_bindir}/gscanbus
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.xpm

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-18m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-17m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-15m)
- rebuild against libraw1394-2.0.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-14m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-13m)
- rebuild against gcc43

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.7.1-12m)
- no NoSource

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (0.7.1-11m)
- rebuild against libraw1394.

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-10m)
- move gscanbus.desktop to %%{_datadir}/applications/
- update Source1: gscanbus.desktop
- install icon
- change URL

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-9m)
- add Patch1: gscanbus-0.7.1-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.1-8m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.1-7m)
- rebuild against for libraw1394

* Thu Mar 24 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.7.1-6k)
- add gscanbus.desktop

* Thu Mar 24 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.7.1-4k)
- add Requires: dev >= 3.2-8k, gtk+1 >= 1.2

* Thu Mar 24 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.7.1-2k)
- wrote spec file for kondara
- add installdir.patch
