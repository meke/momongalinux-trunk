%global      momorel 3

Summary:     Meta package for Graphical Internet
Name:        internet-suite
Version:     8.0
Release:     %{momorel}m%{?dist}
License:     GPL and LGPL and MPL and Modified BSD
Group:       Applications/Internet
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:    balsa
Requires:    bittorrent-gui
Requires:    bluefish
Requires:    bogofilter
Requires:    ekiga
Requires:    evolution
Requires:    firefox
# Requires:    galeon
Requires:    gftp
Requires:    gnomeicu
Requires:    hotwayd
Requires:    jd
Requires:    kompozer
# Requires:    loqui
Requires:    ochusha
# Requires:    pidgin
Requires:    qwit
Requires:    seamonkey
Requires:    sylpheed
Requires:    thunderbird
Requires:    xchat

%description
This is meta package for Graphical Internet.
Listed packages are independent of all desktop environments.
(NOT GNOME, NOT KDE, NOT XFCE4)

%package plugin
Summary:     Meta package for browser-plugins
Group:       Applications/Internet
Requires:    %{name} = %{version}-%{release}

%ifnarch ia64 x86_64
# Requires:    HelixPlayer-mozilla-plugin
%endif
Requires:    TiMidity++-ump
Requires:    gxine-mozplugin
Requires:    mozplugger
# Requires:    mplayerplug-in
Requires:    vlc-plugin

%description plugin
This is meta package for browser-plugins.

%files

%files plugin

%changelog

* Tue Jul 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0-3m)
- remove loqui

* Tue Jul 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0-2m)
- remove pidgin

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0-1m)
- version 8.0

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0-7m)
- remove Requires: mplayerplug-in

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-6m)
- rebuild for new GCC 4.6

* Sat Feb 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-5m)
- drop galeon

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-3m)
- full rebuild for mo7 release

* Fri Feb 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-2m)
- change Requires from seamonkey-suite to seamonkey

* Sun Feb  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-1m)
- version 7.0

* Sat Feb  6 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.0-3m)
- remove HelixPlayer-mozilla-plugin

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0-1m)
- version 6.0

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-12m)
- add Requires: qwit

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-11m)
- rebuild against rpm-4.6

* Fri Sep 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-10m)
- welcome back ochusha

* Wed Sep 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-9m)
- adios ochusha

* Sun Jun 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-8m)
- revive Requires: mplayerplug-in

* Sat May 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-7m)
- disable require mplayerplug-in temporary

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-6m)
- rebuild against gcc43

* Sat Oct 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-5m)
- add Requires: kompozer

* Sun Jul  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-4m)
- add Requires: vlc-plugin

* Tue Jun  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-3m)
- add Requires: gxine-mozplugin

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- good-bye gaim and hello pidgin

* Tue Apr 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- initial package for Momonga Linux
