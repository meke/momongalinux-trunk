%global momorel 1
Name:           zathura-pdf-poppler
Version:        0.2.4
Release: %{momorel}m%{?dist}
Summary:        PDF support for zathura via poppler
Group:          Applications/Publishing
License:        "zlib"
URL:            http://pwmt.org/projects/zathura/plugins/%{name}
Source0:        http://pwmt.org/projects/zathura/plugins/download/%{name}-%{version}.tar.gz
NoSource: 0
Requires:       zathura
BuildRequires:  poppler-glib-devel
BuildRequires:  zathura-devel >= 0.2.3

%description
The zathura-pdf-poppler plugin adds PDF support to zathura by using
the poppler rendering engine.

%prep
%setup -q
# Don't rebuild during install phase
sed -i 's/^install:\s*all/install:/' Makefile

%build
CFLAGS='%{optflags}' make %{?_smp_mflags} LIBDIR=%{_libdir} debug
mv pdf-debug.so pdf.so

%install
make DESTDIR=%{buildroot} LIBDIR=%{_libdir} install

%files
%doc AUTHORS LICENSE
%{_libdir}/zathura/pdf.so
%{_datadir}/applications/%{name}.desktop

%changelog
* Wed Feb 19 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-1m)
- import from fedora

