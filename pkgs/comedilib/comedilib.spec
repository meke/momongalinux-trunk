%global momorel 6

%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Name:		comedilib
Version:	0.8.1
Release:	%{momorel}m%{?dist}
Summary:	Data Acquisition library for the Comedi driver
License:	LGPLv2
Group:		System Environment/Kernel
URL:		http://www.comedi.org/
Source:		http://www.comedi.org/download/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel >= 2.7
BuildRequires:	docbook-utils
BuildRequires:	swig
BuildRequires:	flex
Requires:	flex


%description
Comedilib is a user-space library that provides a developer-friendly
interface to Comedi devices. Included in the Comedilib distribution
is documentation, configuration and calibration utilities,
and demonstration programs.

%package devel
Summary:	Libraries/include files for Comedi
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig

%description devel
Comedilib is a library for using Comedi, a driver interface for data
acquisition hardware.

%prep
%setup -q

%build
%configure --disable-dependency-tracking
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install 
#rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL README
%{_bindir}/comedi_*
%{_sbindir}/comedi_*
%{_sysconfdir}/pcmcia/comedi
%config(noreplace)%{_sysconfdir}/pcmcia/comedi.*
%{_libdir}/libcomedi.so.*
%{python_sitelib}/comedi*
%exclude %{python_sitelib}/*.pyc
%exclude %{python_sitelib}/*.pyo
%{_mandir}/man7/*
%{_mandir}/man8/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libcomedi.so
%{_libdir}/libcomedi.a
%{_libdir}/pkgconfig/*
%{python_sitelib}/_comedi.so
%exclude %{_libdir}/*.la
%exclude %{python_sitelib}/*.la
%exclude %{python_sitelib}/*.a
%{_includedir}/comedi*
%{_datadir}/doc/%{name}
%{_mandir}/man3/*

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-6m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-3m)
- full rebuild for mo7 release

* Fri Aug 27 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-2m)
- fix x86_64 build

* Thu Aug 26 2010 Mclellan Daniel <daniel.mclellan@gmail.com>
- (0.8.1-1m)
- initial momonga release

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jan  7 2009 Marek Mahut <mmahut@fedoraproject.org> - 0.8.1-3
- RHBZ#473642 Unowned directories

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.8.1-2
- Rebuild for Python 2.6

* Wed Apr 23 2008 Marek Mahut <mmahut@fedoraproject.org> - 0.8.1-1
- Spec file rewrite
- Upstream release

* Tue Jun 03 2002 David Schleef <ds@schleef.org>
- update for new build system

* Wed Feb 21 2002 Tim Ousley <tim.ousley@ni.com>
- initial build of comedilib RPM

