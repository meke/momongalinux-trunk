%global         momorel 1

Name:           nikto
Version:        2.1.4
Release:        %{momorel}m%{?dist}
Summary:        Web server scanner 

Group:          Applications/Internet
License:        GPLv2+
URL:            http://www.cirt.net/code/nikto.shtml
Source0:        http://www.cirt.net/nikto/%{name}-%{version}.tar.bz2
NoSource:       0
Source1:        nikto-database-license.txt
#use system libwhisker2
Patch0:         nikto-libwhisker2.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
Requires:       nmap


%description
Nikto is a web server scanner which performs comprehensive tests against web
servers for multiple items, including over 3300 potentially dangerous
files/CGIs, versions on over 625 servers, and version specific problems
on over 230 servers. Scan items and plugins are frequently updated and
can be automatically updated (if desired).

%prep
%setup -q
%patch0 -p1 -b .libwhisker

#change configfile path
sed -i "s:/etc/nikto.conf:%{_sysconfdir}/nikto/config:" nikto.pl

#enable nmap by default and set plugindir path
sed -i "s:# EXECDIR=/opt/nikto:EXECDIR=%{_datadir}/nikto:;
        s:# PLUGINDIR=/opt/nikto/plugins:PLUGINDIR=%{_datadir}/nikto/plugins:;
        s:# TEMPLATEDIR=/opt/nikto/templates:TEMPLATEDIR=%{_datadir}/nikto/templates:;
        s:# DOCDIR=/opt/nikto/docs:DOCDIR=%{_datadir}/nikto/docs:" nikto.conf

#Disable RFIURL by default - let users configure it themselves to trustworthy source
sed -i "s:^RFIURL=:#RFIURL=:" nikto.conf

cp %{SOURCE1} docs/database-license.txt

%build
#no build required


%install
rm -rf %{buildroot}
install -pD nikto.pl %{buildroot}%{_bindir}/nikto
install -m 0644 -pD docs/nikto.1 %{buildroot}%{_mandir}/man1/nikto.1
mkdir -p %{buildroot}%{_datadir}/nikto/plugins/
install -m 0644 -p plugins/* %{buildroot}%{_datadir}/nikto/plugins/
mkdir -p %{buildroot}%{_datadir}/nikto/templates/
install -m 0644 -p templates/* %{buildroot}%{_datadir}/nikto/templates/
install -m 0644 -pD nikto.conf %{buildroot}%{_sysconfdir}/nikto/config

#remove unneeded files
rm -f %{buildroot}%{_datadir}/nikto/plugins/LW2.pm

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc docs/CHANGES.txt docs/LICENSE.txt docs/database-license.txt
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/nikto
%{_datadir}/nikto
%{_mandir}/man?/*


%changelog
* Wed Jul 13 2011 Yasuo Ohgaki <yohgaki@momonga-linux.org> 
- (2.1.4-1m)
- import from fedora

* Sun Apr 9 2011 Michal Ambroz <rebus AT seznam.cz> - 1:2.1.4-2
- Fix the default config file

* Sun Mar 28 2011 Michal Ambroz <rebus AT seznam.cz> - 1:2.1.4-1
- Version bump

* Sun Sep 12 2010 Michal Ambroz <rebus AT seznam.cz> - 1:2.1.3-1
- Version bump

* Mon Mar 22 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> - 1:2.1.1-3
- Add missing changelog
- Version bump

* Mon Mar 22 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> - 1:2.1.1-2
- Update version to 2.1.1 and fix version collisions, 
  based on SPEC provided by Michal Ambroz <rebus at, seznam.cz> 

* Mon Feb 08 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> - 2.03-3
- Resolve rhbz #515871

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.03-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Apr 01 2009 Sindre Pedersen Bjørdal <sindrepb@fedoraproject.org> - 2.03-1
- New upstream release

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.36-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Aug 26 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.36-4
- fix license tag

* Wed May 30 2007 Sindre Pedersen Bjørdal <foolish[AT]guezz.net> - 1.36-3
- Add sed magic to really replace nikto-1.36-config.patch
* Mon May 28 2007 Sindre Pedersen Bjørdal <foolish[AT]guezz.net> - 1.36-2
- Remove libwhisker Requires
- Replace configfile patch with sed magic
- Update License
- Add database-license.txt to %%doc
* Fri May 04 2007 Sindre Pedersen Bjørdal <foolish[AT]guezz.net> - 1.36-1
- Initial build
