%global momorel 1
%global quirkdbver 20100619

Summary: Power management utilities and scripts
Name: pm-utils
Version: 1.4.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Base
URL: http://pm-utils.freedesktop.org/
Source0: http://pm-utils.freedesktop.org/releases/%{name}-%{version}.tar.gz
Source1: http://pm-utils.freedesktop.org/releases/pm-quirks-%{quirkdbver}.tar.gz
NoSource: 0
NoSource: 1
%ifnarch s390 s390x
Requires: kbd
%if %{with power_d}
# power.d/disable_wol
Requires: ethtool
# power.d/harddisk
Requires: hdparm
# power.d/wireless
Requires: wireless-tools
%endif
%endif

Source0: http://pm-utils.freedesktop.org/releases/pm-utils-%{version}.tar.gz
Source1: http://pm-utils.freedesktop.org/releases/pm-quirks-%{quirkdbver}.tar.gz

Source23: pm-utils-bugreport-info.sh
Source24: pm-utils-bugreport-info.sh.8

# Use append instead of write for init_logfile (#660329)
Patch0: pm-utils-1.4.1-init-logfile-append.patch
# Fix typo in 55NetworkManager (#722759)
Patch1: pm-utils-1.4.1-networkmanager-typo-fix.patch
# Add support for grub2 in 01grub hook
Patch2: pm-utils-1.4.1-grub2.patch
# Fix hooks exit code logging
Patch3: pm-utils-1.4.1-hook-exit-code-log.patch
# Fix line spacing in logs to be easier to read (#750755)
Patch4: pm-utils-1.4.1-log-line-spacing-fix.patch
# Fix NetworkManager dbus methods (fd.o #42500 / RH #740342)
Patch5: pm-utils-1.4.1-nm_method.patch
# Add support for in-kernel (from kernel 3.6) suspend to both (#843657)
Patch6: pm-utils-1.4.1-add-in-kernel-suspend-to-both.patch
# Patch sent upstream
Patch7: pm-utils-1.4.1-man-fix.patch

%description
The pm-utils package contains utilities and scripts useful for tasks related
to power management.

%package devel
Summary: Files for development using %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This package contains the pkg-config files for development
when building programs that use %{name}.

%prep
%setup -q
tar -xzf %{SOURCE1}
%patch0 -p1 -b .init-logfile-append
%patch1 -p1 -b .network-manager-typo-fix.patch
%patch2 -p1 -b .grub2
%patch3 -p1 -b .hook-exit-code-log
%patch4 -p1 -b .log-line-spacing-fix
%patch5 -p1 -b .nm_method
%patch6 -p1 -b .add-in-kernel-suspend-to-both
%patch7 -p1 -b .man-fix

%build
%configure --docdir=%{_docdir}/%{name}
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

install -D -m 0600 /dev/null $RPM_BUILD_ROOT%{_localstatedir}/log/pm-suspend.log
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/run/pm-utils/{locks,pm-suspend,pm-powersave}
touch $RPM_BUILD_ROOT%{_localstatedir}/run/pm-utils/locks/{pm-suspend.lock,pm-powersave.lock}
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/run/pm-utils/{pm-suspend,pm-powersave}/storage
install -D -m 0755 %{SOURCE23} $RPM_BUILD_ROOT%{_sbindir}/pm-utils-bugreport-info.sh
install -D -m 0644 %{SOURCE24} $RPM_BUILD_ROOT%{_mandir}/man8/pm-utils-bugreport-info.sh.8

# Install quirks
cp -r video-quirks $RPM_BUILD_ROOT%{_libdir}/pm-utils

%if ! %{with power_d}
rm $RPM_BUILD_ROOT%{_libdir}/pm-utils/power.d/*
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%preun
# Clean storage to prevent left-behind files. These files are dynamically
# created in runtime (also with dynamic names), thus it is hard to track
# them individually.
rm -rf %{_localstatedir}/run/pm-utils/{pm-suspend,pm-powersave}/storage/*

%files
%defattr(-,root,root)
%{_docdir}/%{name}
%{_libdir}/pm-utils/bin/
%{_libdir}/pm-utils/defaults
%{_libdir}/pm-utils/functions
%{_libdir}/pm-utils/module.d/*
%{_libdir}/pm-utils/pm-functions
%if %{with power_d}
%{_libdir}/pm-utils/power.d/*
%endif
%{_libdir}/pm-utils/sleep.d/*
%{_bindir}/on_ac_power
%{_bindir}/pm-is-supported
%{_sbindir}/pm-utils-bugreport-info.sh
%{_sbindir}/pm-hibernate
%{_sbindir}/pm-powersave
%{_sbindir}/pm-suspend
%{_sbindir}/pm-suspend-hybrid
%{_mandir}/man1/*.1.*
%{_mandir}/man8/*.8.*
%ghost %{_localstatedir}/run/pm-utils
%{_libdir}/pm-utils/video-quirks

# no logrotate needed, because only one run of pm-utils is stored
# in the logfile
%ghost %verify(not md5 size mtime) %{_localstatedir}/log/pm-suspend.log

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/pm-utils.pc

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-1m)
- update 1.4.1

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.6.1-7m)
- remove BR hal

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6.1-6m)
- release directories provided by filesystem again

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6.1-5m)
- release directories provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6.1-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6.1-1m)
- version 1.2.6.1
- import nouveau.patch from Fedora

* Wed Jan 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.6-1m)
- version 1.2.6
- update Source21: pm-utils-99hd-apm-restore from Fedora devel
- remove merged fix-kms.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.5-2m)
- enable --short-circuit -ba and -bi

* Fri May 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.5-1m)
- version 1.2.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against rpm-4.6

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-1m)
- version 1.1.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.3-2m)
- rebuild against gcc43

* Fri Apr 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.3-1m)
- version 0.99.3
- sync with Fedora

* Thu Feb 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19.1-2m)
- update 60sysfont.hook
 +* Fri Feb  2 2007 Peter Jones <pjones@redhat.com>
 +- Fix setsysfont hook to actually hit tty0, not the pty of the current task.

* Tue Feb  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19.1-1m)
- import from Fedora Core devel

* Tue Jan 30 2007 Jeremy Katz <katzj@redhat.com> - 0.19.1-6
- build so that hooks run properly on resume; fix syntax error in 
  functions-intel (pjones)

* Fri Jan 26 2007 Phil Knirsch <pknirsch@redhat.com> - 0.19.1-5
- Fixed problem with changes in 10NetworkManager hook (#224556)

* Wed Jan 24 2007 Phil Knirsch <pknirsch@redhat.com> - 0.19.1-4
- Start/stop correct services in 10NetworkManager hook (#215253)
- Fixed check for /sys/power/disk and /sys/power/state (#214407)
- Added proper error messages in case /sys/power/disk or /sys/power/state are
  missing (#215386)
- Removed service calls and module load/unload for bluetooth hook (#213387)
- Added hook file to restore the sysfont after resume (#215391)
- Added the possibility to disable hibernate and suspend completely via the
  config file (#216459)
- Symlinked the config file to /etc/sysconfig/power-management (#216459)
- Fixed pm-powersave permission check bug (#222819)
- Small specfile cleanups

* Sun Oct  1 2006 Peter Jones <pjones@redhat.com> - 0.19.1-3
- Disable bluetooth suspend/reusme hook by default; the kernel modules seem
  to support this correctly these days.

* Thu Sep 28 2006 Peter Jones <pjones@redhat.com> - 0.19.1-2
- Ignore emacs backup files in config directories (#185979)

* Tue Aug  8 2006 Peter Jones <pjones@redhat.com> - 0.19.1-1
- Hopefully fix Centrino ThinkPad suspend+resume
- Hopefully fix Intel Mac Mini/MacBook suspend+resume

* Mon Jul 31 2006 Jeremy Katz <katzj@redhat.com> - 0.19-3
- doing the vbestate save/restore on intel video with the modesetting 
  intel xorg driver is broken.  so don't do it.

* Tue Jul 18 2006 John (J5) Palmieri <johnp@redhat.com> - 0.19-2
- requier a newer version of D-Bus and rebuild

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.19-1.1
- rebuild

* Tue Jun 13 2006 Peter Jones <pjones@redhat.com> - 0.19-1
- update from CVS
- move pam and consolehelper stuff here.
- move video hooks here since HAL isn't ready

* Tue Apr 25 2006 Peter Jones <pjones@redhat.com> - 0.18-1
- Make it work cross-distro

* Mon Apr 17 2006 Peter Jones <pjones@redhat.com> - 0.17-1
- add more helper functions
- rework things that were forking an extra subshell
- fix the suspend lock
- work around bluetooth/usb suspend wackiness

* Fri Mar 17 2006 Peter Jones <pjones@redhat.com> - 0.16-1
- rework the difference between hibernate and suspend; get rid of PM_MODE
- add 00clear script
- move default_resume_kernel from "functions" to 01grub's hibernate section

* Sat Mar 11 2006 Peter Jones <pjones@redhat.com> - 0.15-1
- fix hibernate check in a way that doesn't break "sleep".

* Fri Mar 10 2006 Peter Jones <pjones@redhat.com> - 0.14-1
- fix hibernate check in /etc/pm/hooks/20video

* Fri Mar 03 2006 Phil Knirsch <pknirsch@redhat.com> - 0.13-1
- Revert last changes for ATI graphics chips as they seem to cause more
  problems than they solved.

* Wed Mar 01 2006 Phil Knirsch <pknirsch@redhat.com> - 0.12-1
- Use vbetool post instead of vbetool dpms on for ATI cards.

* Tue Feb 28 2006 Jeremy Katz <katzj@redhat.com>
- allow building on all x86 arches (#183175)

* Tue Feb 28 2006 Jeremy Katz <katzj@redhat.com> - 0.11-1
- fix display on resume with nvidia graphics
- add infrastructure to tell what pm-util is running; don't resume 
  video on return from hibernate as the BIOS has already re-initialized it

* Fri Feb 24 2006 Phil Knirsch <pknirsch@redhat.com> - 0.10-1
- Added missing pciutils-devel BuildRequires (#182566) 
- Fixed missing vbestata save/restore calls for video suspend/resume (#182167, 
  #169494)
- Renamed hook scripts to allow local pre and post inserts (#179421)
- Added support for blinking led lights on Thinkpad Laptops during suspend
  (#179420)
- Added pm-powersave script for powersaving via HAL (#169054)
- Added symlinks for pm-shutdown and pm-restart (#165953)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.09-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.09-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 24 2006 Jeremy Katz <katzj@redhat.com> - 0.09-1
- Remove button module on suspend
- Set default kernel in grub to current one when hibernating 
  so that resume works

* Thu Dec 22 2005 Peter Jones <pjones@redhat.com> - 0.08-1
- Fix scripts for new pciutils

* Fri Dec  9 2005 Dave Jones <davej@redhat.com>
- Update to latest vbetool (0.5-1)
  Now also built on x86-64 too.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Dec 01 2005 John (J5) Palmieri <johnp@redhat.com> - 0.07-3
- rebuild for the new dbus

* Wed Nov 30 2005 Peter Jones <pjones@redhat.com> - 0.07-2
- restart ntpd in the background
- switch terminals early so we don't wake the screen back up

* Fri Nov 18 2005 Bill Nottingham <notting@redhat.com> - 0.06-3
- nix that, wait for the kernel to settle down

* Wed Nov 16 2005 Bill Nottingham <notting@redhat.com> - 0.06-2
- fix LRMI usage in vbetool

* Thu Nov 10 2005 Peter Jones <pjones@redhat.com> - 0.06-1
- kill acpi_video_cmd calls in functions-ati
- fix lcd_on in functions-ati

* Fri Sep 30 2005 Bill Nottingham <notting@redhat.com> - 0.05-1
- check for presence of various tools/files before using them (#169560, #196562)

* Fri Aug 12 2005 Jeremy Katz <katzj@redhat.com> - 0.04-1
- add pm-hibernate

* Tue Jul 05 2005 Bill Nottingham <notting@redhat.com> - 0.03-1
- fix path to video functions in video hook

* Mon Jul 04 2005 Bill Nottingham <notting@redhat.com> - 0.02-1
- add a pm-suspend (#155613)

* Wed Apr 13 2005 Bill Nottingham <notting@redhat.com> - 0.01-1
- initial version - package up vbetool, radeontool, new on_ac_power
