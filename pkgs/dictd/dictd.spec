%global momorel 11
Summary:	Dictionary database server
Name:		dictd
Version:	1.9.15
Release: %{momorel}m%{?dist}
License:	GPL
Group:		System Environment/Daemons
Source0:	http://dl.sourceforge.net/sourceforge/dict/%{name}-%{version}.tar.gz
NoSource:       0

Source1:	%{name}.init
Source2:	%{name}.sysconfig
#Patch0:		%{name}-no_libnsl.patch
#Patch2:		%{name}-opt.patch

# perl -p -i -e 's/-d 755/-d -m755/' Makefile.in
Patch3:		dictd-install.patch
Patch10:	dictd-1.9.15-cflags.patch
Patch11:        dictd-1.9.15-gcc43.patch

URL:		http://www.dict.org/
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	flex
BuildRequires:	bison
BuildRequires:	zlib-devel >= 1.1.4-5m
Requires(post): chkconfig initscripts
Requires(preun): initscripts chkconfig
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Server for the Dictionary Server Protocol (DICT), a TCP transaction
based query/response protocol that allows a client to access
dictionary definitions from a set of natural language dictionary
databases.

%package -n dict
Summary:	DICT Protocol Client
Group:		Applications/Text
Requires:	recode

%description -n dict
Client for the Dictionary Server Protocol (DICT), a TCP transaction
based query/response protocol that provides access to dictionary
definitions from a set of natural language dictionary databases.

%package -n dictfmt
Summary:	dictfmt utility to convert databases in various formats into dict format
Group:		Applications/Text

%description -n dictfmt
dictfmt utility is designed to convert databases in various formats
into working databases and indexes for the DICT server.
This package also includes other tools for formating databases:
dictfmt_{index2suffix,index2word,plugin,virtual} and dictunformat.

%package -n dictzip
Summary:	Compress (or expand) files, allowing random access
Group:		Applications/Archiving

%description -n dictzip
dictzip compresses files using the gzip(1) algorithm (LZ77) in a
manner which is completely compatible with the gzip file format. An
extension to the gzip file format (Extra Field, described in 2.3.1.1
of RFC 1952) allows extra data to be stored in the header of a
compressed file. Dictd, the DICT protocol dictionary server will make
use of this data to perform pseudo-random access on the file.

%prep
%setup -q
#patch0 -p1 -b .nsl
#patch2 -p1 -b .opt
%patch3 -p0 -b .install
%patch10 -p1 -b .cflags
%patch11 -p1 -b .gcc43~

%build
aclocal
%{__autoconf}
pushd libmaa; aclocal; autoconf; popd
CFLAGS="-DUID_NOBODY=99 -DGID_NOBODY=99"
%configure

make
make samples

%install
rm -rf %{buildroot}
make install install.samples DESTDIR=%{buildroot}

echo "server localhost" > dict.conf
echo -e "access {\n\tallow 127.0.0.1\n\tdeny *\n}\n" > %{name}-main.conf

mkdir -p %{buildroot}%{_initscriptdir}
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
mkdir -p %{buildroot}%{_sysconfdir}/%{name}
mkdir -p %{buildroot}%{_datadir}/%{name}

install -m644 dict.conf %{buildroot}%{_sysconfdir}/dict.conf
install -m644 dictd-main.conf %{buildroot}%{_sysconfdir}/%{name}/dictd-main.conf
touch %{buildroot}%{_sysconfdir}/%{name}.conf
chmod a-x %{buildroot}%{_sysconfdir}/%{name}.conf
install -m754 %{SOURCE1} %{buildroot}%{_initscriptdir}/%{name}
install -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/%{name}

cp libmaa/README README.libmaa

# remove
rm -f %{buildroot}/usr/libexec/search_man
rm -f %{buildroot}/usr/share/man_popen.dict
rm -f %{buildroot}/usr/share/man_popen.index

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add %{name}
if [ -f /var/lock/subsys/%{name} ]; then
        /sbin/service %{name} restart >&2
else
        echo "Run \"/sbin/service %{name} start\" to start %{name} daemon."
fi

%preun
if [ "$1" = "0" ]; then
	if [ -f /var/lock/subsys/%{name} ]; then
		/sbin/service %{name} stop >&2
	fi
	/sbin/chkconfig --del %{name}
fi

%files
%defattr(-,root,root)
%doc ANNOUNCE NEWS README* TODO dictd.conf example*
%doc doc/security.doc
%ghost %{_sysconfdir}/%{name}.conf
%{_sbindir}/%{name}
%{_initscriptdir}/%{name}
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/sysconfig/%{name}
%dir %{_datadir}/%{name}
%dir %{_sysconfdir}/%{name}
%{_sysconfdir}/%{name}/%{name}-main.conf
%{_mandir}/man8/%{name}*
%{_includedir}/dictdplugin.h
%{_bindir}/dictdplugin-config
%defattr(755,root,root,-)
%{_libexecdir}/dictdplugin_*.so

%files -n dict
%defattr(-,root,root)
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/dict.conf
%{_bindir}/dict
%{_bindir}/dictl
%{_bindir}/colorit
%{_mandir}/man1/dict.1*
%{_mandir}/man1/dictl.1*
%{_mandir}/man1/colorit.1*

%files -n dictfmt
%defattr(-,root,root)
%{_bindir}/dictfmt*
%{_bindir}/dictunformat
%{_mandir}/man1/dictfmt*.1*
%{_mandir}/man1/dictunformat.1*

%files -n dictzip
%defattr(-,root,root)
%{_bindir}/dictzip
%{_mandir}/man1/dictzip.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.15-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.15-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.15-9m)
- full rebuild for mo7 release

* Thu May  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.15-8m)
- add execute bit

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.15-7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.15-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.15-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.15-4m)
- rebuild against gcc43

* Fri Jan  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.15-3m)
- add patch for gcc43

* Wed Jun 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.15-2m)
- import patch10 to fix build problem on x86_64

* Wed Jan 19 2005 TAKAHASHI Tamotsu <tamo>
- (1.9.15-1m)
- update (see NEWS)
 non-ASCII and non-UTF8 dictionaries need rebuilding
- dictl Requires: recode
- simplify init script

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.9.11-2m)
- revised spec for enabling rpm 4.2.

* Fri Dec 12 2003 TAKAHASHI Tamotsu <tamo>
- (1.9.11-1m)
- many bugfixes including serious ones
- fix permission
- add dictfmt
- add dictdplugin.h and dictdplugin-config
- add dictl and colorit
- add NEWS removing ChangeLog
- comment out patches (no_nsl and opt)

* Sun Apr 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.7.1-4m)
- use /sbin/service instead of /etc/init.d/dictd in %%post

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.1-3m)
- rebuild against zlib 1.1.4-5m

* Tue Jul 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.7.1-2m)
- use %{_initscriptdir} instead of '/etc/rc.d/init.d'
- init script does not run in any run level by default
- spec file cleanup

* Mon Jul 15 2002 Kikutani Makoto <poo@momonga-linux.org>
- (1.7.1-1m)
- 1st Momonga version
- I made this based on PLD Linux's spec
