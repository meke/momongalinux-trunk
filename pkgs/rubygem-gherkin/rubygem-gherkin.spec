# Generated from gherkin-2.11.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname gherkin

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: gherkin-2.11.0
Name: rubygem-%{gemname}
Version: 2.11.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/cucumber/gherkin
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(json) >= 1.4.6
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
A fast Gherkin lexer/parser based on the Ragel State Machine Compiler.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            -V \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.0-1m)
- update 2.11.0

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.3-1m)
- update 2.9.3

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.2-1m)
- update 2.7.2

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.2-1m)
- update 2.6.2

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.1-1m)
- update 2.6.1

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.4-1m)
- update 2.5.4

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.21-1m)
- update 2.4.21

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.18-1m)
- update 2.4.18

* Sat Apr 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.9-4m)
- modify CFLAGS to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.9-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.9-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.9-1m)
- update 2.2.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.0-1m)
- Initial package for Momonga Linux
