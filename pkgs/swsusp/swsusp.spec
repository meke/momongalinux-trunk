%global momorel 8
Name: swsusp
Version: 1.03
Release: %{momorel}m%{?dist}
Summary: software suspend 2 hibernate script
License: GPL
Group: Applications/System
URL: http://softwaresuspend.berlios.de/
Source0: http://download.berlios.de/softwaresuspend/hibernate-script-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch
Requires: initscripts

%description
hibernate is a shell script that handles the process of getting ready
to suspend to disk and to resume from disk. It requires the Software
Suspend 2 patches available at http://softwaresuspend.berlios.de/
After installing you will want to run 'hibernate -h' to see available
options and modify your /etc/hibernate/hibernate.conf to set them. 

%prep
%setup -q -n hibernate-script-%{version}

%build

%install
rm -rf %{buildroot}
export BASE_DIR=%{buildroot}
export PREFIX=%{_prefix}
export MAN_DIR=%{buildroot}%{_mandir}
sh install.sh

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README CHANGELOG COPYING SCRIPTLET-API TODO
%{_sbindir}/hibernate
%{_datadir}/hibernate/*
%dir %{_sysconfdir}/hibernate
%config(noreplace) /etc/hibernate/*
%{_mandir}/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.03-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03-2m)
- %%NoSource -> NoSource

* Fri Jan 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Sun Dec 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sat Nov 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.00-6m)
- use recent script tarball

* Mon Jan 02 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-5m)
- set 755 permission of hibernate, for use shell completion

* Sun Jul 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0-4m)
- set LANG=C in hibernate

* Sun Jul 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0-3m)
- use more absolute path in /usr/sbin/hibernate

* Sun Jul 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0-2m)
- use absolute path in /usr//sbin/hibernate

* Sun Jul 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0-1m)
- initial version
