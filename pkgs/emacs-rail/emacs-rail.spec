%global momorel 42
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Replace Agent-string Internal Library for Emacs
Name: emacs-rail
Version: 1.2.6
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source0: ftp://ftp.fan.gr.jp/pub/elisp/rail/rail-%{version}.tar.gz
Source1: LIMIT_VERSION
Source2: SEMI_VERSION
Source3: dot.emacs
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: rail-emacs
Obsoletes: rail-xemacs

Obsoletes: elisp-rail
Provides: elisp-rail

%description
Rail has some features as followed:

1. Compatible with genjis.el within tm package:
   (Japanizes the variable mule-version)
2. Japanizes the code name of FLIM, SEMI, XEmacs, UTF-2000 Mule and Meadow.
   Then applies it to the User-Agent: field of the messages.
3. Japanizes the code name of Mule, XEmacs and Meadow
   when irchat-pj sends back at receiving CTCP VERSION.

%prep
%setup -q -n rail-%{version}

cp -f %{SOURCE1} contrib/FLIM_VERSION
cp -f %{SOURCE2} contrib/SEMI_VERSION

%build
make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir}/rail \
  VERSION_SPECIFIC_LISPDIR=%{buildroot}%{e_sitedir}/emu

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{e_sitedir}/rail
make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir}/rail \
  VERSION_SPECIFIC_LISPDIR=%{buildroot}%{e_sitedir}/emu \
  install

chmod -x contrib/*.perl

rm -f %{buildroot}%{sitepdir}/etc/rail/00README
rm -f %{buildroot}%{sitepdir}/pkginfo/MANIFEST.rail

%{__mkdir_p} %{buildroot}%{_datadir}/config-sample/%{name}
%{__install} -m 644 %{SOURCE3} %{buildroot}%{_datadir}/config-sample/%{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc 00FAQ 00README CHANGELOG
%doc contrib
%{_datadir}/config-sample/%{name}
%{e_sitedir}/rail

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6-42m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6-41m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.6-40m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6-39m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-38m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6-37m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6-36m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-35m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-34m)
- merge rail-emacs to elisp-rail
- kill rail-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-33m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-32m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-31m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-30m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-29m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-28m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-27m)
- rebuild against emacs-23.0.93

* Fri Apr  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-26m)
- add workaroud for xemacs-sumo's dired faulty

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-25m)
- rebuild against emacs-23.0.92

* Fri Mar  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-24m)
- revise for emacs23
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-23m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-22m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-21m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-20m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-19m)
- rebuild against gcc43

* Mon Jul  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-18m)
- add Source2: SEMI_VERSION
- add config-sample

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-17m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-16m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-15m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-14m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-13m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-12m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-11m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-10m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-9m)
- rebuild against emacs-22.0.90

* Sun Oct 15 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-8m)
- update Source1

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.6-7m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-6m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.6-5m)
- rebuild against emacs-21.3.50

* Sat Aug 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-4m)
- No NoSource0

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.6-3m)
- revised spec for enabling rpm 4.2.

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.6-2m)
- rebuild against emacs-21.3
- define emikover
- update emiko version
- use %%{_prefix} macro

* Sun Jan 12 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.2.6-1m)
- Version up to 1.2.6
- remove requires: apel, limit

* Fri Aug 16 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.2.5-13m)
- rebuild against elisp-apel-10.3-21m, elisp-limit-1.14.7-19m,
   elisp-emiko-1.14.1-21m, and emacs-21.2

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (1.2.5-12k)
- No docs could be excutable :-p

* Tue Oct 30 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.2.5-10k)
- add LIMIT-VERSION and rebuild against it

* Tue Oct 23 2001 Hidetomo Machi <mcHT@kondara.org>
- (eilsp-rail-1.2.5-8k)
- merge rail-{emacs, xemacs}

* Thu Nov  9 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- Change BuildPreReq & Requires TAG

* Mon Oct 23 2000 Hidetomo Machi <mcHT@kondara.org>
- (1.2.5-5k)
- modify specfile (License)
- remove dir /usr/lib/xemacs/site-packages/etc/rail

* Sat Jun 10 2000 Hidetomo Machi <mcHT@kondara.org>
- version 1.2.5
- add "-q" at %setup
- modified "BuildRoot"
- add "%doc contrib"

* Sat Apr 15 2000 Hidetomo Machi <mcHT@kondara.org>
- version 1.2.4

* Fri Mar 10 2000 Hidetomo Machi <mcHT@kondara.org>
- version 1.2.3
- kondarize ;)

* Thu Dec 30 1999 SAKA Toshihide <saka@yugen.org>
- First release.
