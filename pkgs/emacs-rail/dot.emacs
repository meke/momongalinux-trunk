;; ~/.emacs

(setq rail-emulate-genjis t)
(require 'rail)
(add-to-list 'rail-semi-codename-alist
	     '("Choanoflagellata" . "襟鞭毛虫")) ;; for emiko-easypg
