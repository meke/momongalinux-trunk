%global momorel 4

%define namedversion 1.0-alpha-2

Name:       maven-enforcer-rule-api
Version:    1.0
Release:    0.1.%{momorel}m%{?dist}
Summary:    Generic interfaces needed by maven-enforcer-plugin

Group:      Development/Libraries
License:    "ASL 2.0"
URL:        http://svn.apache.org/repos/asf/maven/shared/tags/maven-enforcer-rule-api-1.0-alpha-2

# svn export \
#http://svn.apache.org/repos/asf/maven/shared/tags/maven-enforcer-rule-api-1.0-alpha-2
# tar czf maven-enforcer-rule-api-1.0-alpha-2-src.tar.gz \
# maven-enforcer-rule-api-1.0-alpha-2
Source0:    maven-enforcer-rule-api-%{namedversion}-src.tar.gz
Source1:    %{name}-jpp-depmap.xml

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  java-devel >= 0:1.5.0
BuildRequires:  ant >= 0:1.6.5
BuildRequires:  ant-junit
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-ant
BuildRequires:  maven2-plugin-assembly
BuildRequires:  maven2-plugin-checkstyle
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-plugin
BuildRequires:  maven2-plugin-pmd
BuildRequires:  maven2-plugin-project-info-reports
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-site
BuildRequires:  maven2-plugin-surefire = 2.3
BuildRequires:  maven-surefire-provider-junit = 2.3
BuildRequires:  maven-doxia
BuildRequires:  checkstyle
BuildRequires:  plexus-maven-plugin >= 1.3.5-2m
BuildRequires:  qdox
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api

Requires:       maven2
Requires(post):   jpackage-utils >= 0:1.7.2
Requires(postun): jpackage-utils >= 0:1.7.2


%description
This component provides the generic interfaces needed to implement custom
rules for the maven-enforcer-plugin.

%package javadoc
Summary:    Javadoc for maven-enforcer-rule-api
Group:      Documentation

%description javadoc
Java API documentation for enforcer-rule-api.

%prep
%setup -q -n %{name}-%{namedversion}

%build

export MAVEN_REPO_LOCAL=`pwd`/.m2
mkdir $MAVEN_REPO_LOCAL

mvn-jpp \
    -Dmaven.test.failure.ignore=true \
    -Dmaven2.jpp.depmap.file=%{SOURCE1} \
    -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
    install javadoc:javadoc

%install
rm -rf $RPM_BUILD_ROOT

# install jar
install -dm 755 $RPM_BUILD_ROOT/%{_javadir}
cp -p target/%{name}-%{namedversion}.jar \
      $RPM_BUILD_ROOT/%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT/%{_javadir}/%{name}.jar

#install pom
install -dm 755 $RPM_BUILD_ROOT/%{_datadir}/maven2/poms
cp -p pom.xml \
  $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP-maven-enforcer-rule-api.pom

#depmap entry
%add_to_maven_depmap org.apache.maven.shared maven-enforcer-rule-api %{namedversion} JPP maven-enforcer-rule-api

# install javadoc
install -dm 755 $RPM_BUILD_ROOT/%{_javadocdir}/%{name}
cp -a target/site/* $RPM_BUILD_ROOT/%{_javadocdir}/%{name}/

%post
%update_maven_depmap

%postun
%update_maven_depmap

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_javadir}/*
%{_datadir}/maven2/poms/JPP-maven-enforcer-rule-api.pom
%{_mavendepmapfragdir}/%{name}

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.1.2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.1.1m)
- import from Fedora 13

* Mon Aug 31 2009 Alexander Kurtakov <akurtako@redhat.com> 1.0-0.1.a2.1.5
- Fix line lenght.
- Remove with/without maven.

* Wed May 20 2009 Fernando Nasser <fnasser@redhat.com> 1.0-0.1.a2.1.4
- Fix license

* Mon Apr 27 2009 Yong Yang <yyang@redhat.com> 1.0-0.1.a2.1.3
- Add missing BRs maven-doxia*, checkstyle, plexus-maven-plugin, qdox
- Add jpp-depmap
- Rebuild with maven2-2.0.8 built in non-bootstrap mode

* Mon Mar 09 2009 Yong Yang <yyang@redhat.com> 1.0-0.1.a2.1.2
- Import from dbhole's maven2 2.0.8 packages, Initial build on JPP6

* Wed Dec 03 2008 Deepak Bhole <dbhole@redhat.com> 1.0-1.1
- Initial build.
