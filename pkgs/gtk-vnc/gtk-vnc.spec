%global momorel 2
%global geckover 1.9.2
# -*- rpm-spec -*-

# Plugin isn't ready for real world use yet - it needs
# a security audit at very least
%define with_plugin 0

%define with_gir 1
%define with_gtk3 1

Summary: A GTK2 widget for VNC clients
Name: gtk-vnc
Version: 0.4.4
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Development/Libraries
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.4/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://live.gnome.org/gtk-vnc
BuildRequires: gtk2-devel
BuildRequires: pygtk2-devel python-devel zlib-devel
BuildRequires: gnutls-devel >= 3.2.0 cyrus-sasl-devel intltool
%if %{with_gir}
# Temp hack for missing dep
# https://bugzilla.redhat.com/show_bug.cgi?id=613466
BuildRequires: libtool
BuildRequires: gobject-introspection-devel
%endif
%if %{with_plugin}
BuildRequires: xulrunner-devel >= %{geckover}
%endif
%if %{with_gtk3}
BuildRequires: gtk3-devel
%endif
Obsoletes: %{name}-plugins

%description
gtk-vnc is a VNC viewer widget for GTK2. It is built using coroutines
allowing it to be completely asynchronous while remaining single threaded.

%package devel
Summary: Development files to build GTK2 applications with gtk-vnc
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: gtk2-devel

%description devel
gtk-vnc is a VNC viewer widget for GTK2. It is built using coroutines
allowing it to be completely asynchronous while remaining single threaded.

Libraries, includes, etc. to compile with the gtk-vnc library

%package python
Summary: Python bindings for the gtk-vnc library
Group: Development/Libraries
Requires: %{name} = %{version}

%description python
gtk-vnc is a VNC viewer widget for GTK2. It is built using coroutines
allowing it to be completely asynchronous while remaining single threaded.

A module allowing use of the GTK-VNC widget from python

%package -n gvnc
Summary: A GObject for VNC connections

%description -n gvnc
gvnc is a GObject for managing a VNC connection. It provides all the
infrastructure required to build a VNC client without having to deal
with the raw protocol itself.

%package -n gvnc-devel
Summary: Libraries, includes, etc. to compile with the gvnc library
Group: Development/Libraries
Requires: gvnc = %{version}-%{release}
Requires: pkgconfig

%description -n gvnc-devel
gvnc is a GObject for managing a VNC connection. It provides all the
infrastructure required to build a VNC client without having to deal
with the raw protocol itself.

Libraries, includes, etc. to compile with the gvnc library

%package -n gvnc-tools
Summary: Command line VNC tools
Group: Applications/Internet

%description -n gvnc-tools
Provides useful command line utilities for interacting with
VNC servers. Includes the gvnccapture program for capturing
screenshots of a VNC desktop

%if %{with_gtk3}
%package -n gtk-vnc2
Summary: A GTK3 widget for VNC clients
Group: Applications/Internet

%description -n gtk-vnc2
gtk-vnc is a VNC viewer widget for GTK2. It is built using coroutines
allowing it to be completely asynchronous while remaining single threaded.

%package -n gtk-vnc2-devel
Summary: Development files to build GTK3 applications with gtk-vnc
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: gtk3-devel

%description -n gtk-vnc2-devel
gtk-vnc is a VNC viewer widget for GTK3. It is built using coroutines
allowing it to be completely asynchronous while remaining single threaded.

Libraries, includes, etc. to compile with the gtk-vnc library
%endif

%prep
%setup -q -n gtk-vnc-%{version} -c
%if %{with_gtk3}
cp -a gtk-vnc-%{version} gtk-vnc2-%{version}
%endif

%build
%if %{with_gir}
%define gir_arg --enable-introspection=yes
%else
%define gir_arg --enable-introspection=no
%endif

%if %{with_plugin}
%define plugin_arg --enable-plugin=yes
%else
%define plugin_arg --enable-plugin=no
%endif

cd gtk-vnc-%{version}
%configure --with-gtk=2.0 %{plugin_arg} %{gir_arg}
%__make %{?_smp_mflags} V=1
cd ..

%if %{with_gtk3}
cd gtk-vnc2-%{version}
%configure --with-gtk=3.0 %{gir_arg}
%__make %{?_smp_mflags} V=1
cd ..
%endif

%install
rm -fr %{buildroot}
cd gtk-vnc-%{version}
%__make install DESTDIR=%{buildroot}
cd ..

%if %{with_gtk3}
cd gtk-vnc2-%{version}
%__make install DESTDIR=%{buildroot}
cd ..
%endif

rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/python*/site-packages/*.a
rm -f %{buildroot}%{_libdir}/python*/site-packages/*.la
%if %{with_plugin}
rm -f %{buildroot}%{_libdir}/mozilla/plugins/%{name}-plugin.a
rm -f %{buildroot}%{_libdir}/mozilla/plugins/%{name}-plugin.la
%endif

%find_lang %{name}

%clean
rm -fr %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%{_libdir}/libgtk-vnc-1.0.so.*
%if %{with_gir}
%{_libdir}/girepository-1.0/GtkVnc-1.0.typelib
%endif

%files devel
%defattr(-, root, root)
%doc gtk-vnc-%{version}/examples/gvncviewer.c
%if %{with_gir}
%doc gtk-vnc-%{version}/examples/gvncviewer.js
%endif
%{_libdir}/libgtk-vnc-1.0.so
%dir %{_includedir}/%{name}-1.0/
%{_includedir}/%{name}-1.0/*.h
%{_libdir}/pkgconfig/%{name}-1.0.pc
%if %{with_gir}
%{_datadir}/gir-1.0/GtkVnc-1.0.gir
%endif

%files python
%defattr(-, root, root)
%doc gtk-vnc-%{version}/examples/gvncviewer.py
%{_libdir}/python*/site-packages/gtkvnc.so

%files -n gvnc -f %{name}.lang
%defattr(-, root, root)
%{_libdir}/libgvnc-1.0.so.*
%if %{with_gir}
%{_libdir}/girepository-1.0/GVnc-1.0.typelib
%endif

%files -n gvnc-devel
%defattr(-, root, root)
%{_libdir}/libgvnc-1.0.so
%dir %{_includedir}/gvnc-1.0/
%{_includedir}/gvnc-1.0/*.h
%{_libdir}/pkgconfig/gvnc-1.0.pc
%if %{with_gir}
%{_datadir}/gir-1.0/GVnc-1.0.gir
%endif
%{_datadir}/vala/vapi/gvnc-1.0.vapi

%files -n gvnc-tools
%defattr(-, root, root)
%doc gtk-vnc-%{version}/AUTHORS
%doc gtk-vnc-%{version}/ChangeLog
%doc gtk-vnc-%{version}/ChangeLog-old
%doc gtk-vnc-%{version}/NEWS
%doc gtk-vnc-%{version}/README
%doc gtk-vnc-%{version}/COPYING.LIB
%{_bindir}/gvnccapture
%{_mandir}/man1/gvnccapture.1*

%if %{with_gtk3}
%files -n gtk-vnc2
%defattr(-, root, root)
%{_libdir}/libgtk-vnc-2.0.so.*
%if %{with_gir}
%{_libdir}/girepository-1.0/GtkVnc-2.0.typelib
%endif

%files -n gtk-vnc2-devel
%defattr(-, root, root)
%doc gtk-vnc2-%{version}/examples/gvncviewer.c
%if %{with_gir}
%doc gtk-vnc2-%{version}/examples/gvncviewer.js
%endif
%{_libdir}/libgtk-vnc-2.0.so
%dir %{_includedir}/%{name}-2.0/
%{_includedir}/%{name}-2.0/*.h
%{_libdir}/pkgconfig/%{name}-2.0.pc
%if %{with_gir}
%{_datadir}/gir-1.0/GtkVnc-2.0.gir
%endif
%endif
%{_datadir}/vala/vapi/gtk-vnc-2.0.deps
%{_datadir}/vala/vapi/gtk-vnc-2.0.vapi



%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-2m)
- rebuild against gnutls-3.2.0

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-1m)
- update 0.4.4

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-8m)
- delete conflict file

* Mon Sep 19 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.3-7m)
- sync Fedora

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-6m)
- build with gtk3 and gtk2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-5m)
- build with gtk3

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.3-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-3m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-2m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2
- delete plugins package

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (0.4.1-1m)
- update to 0.4.1
- disable-plugins

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.10-4m)
- full rebuild for mo7 release

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.10-3m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.10-1m)
- update to 0.3.10

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.9-1m)
- update to 0.3.9
-- change source url

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-2m)
- import xulrunner191 patch from OpenSolaris

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8-1m)
- update to 0.3.8
- delete patch0 (fixed)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.7-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.7-2m)
- rebuild against python-2.6.1-2m

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.7-1m)
- update to 0.3.7

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-2m)
- rebuild against gnutls-2.4.1

* Wed May  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.3-3m)
- rebuild against gcc43

* Sun Mar  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-2m)
- add patch0 (fix build)

* Sat Mar  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.3-1m)
- update 0.3.3
- add mozilla plugin

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-2m)
- %%NoSource -> NoSource

* Mon Jan  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-1m)
- update 0.3.2

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-1m)
- update 0.2.0

* Sun Sep  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-1m)
- initial commit Momonga Linux

* Wed Aug 29 2007 Daniel P. Berrange <berrange@redhat.com> - 0.1.0-5.fc8
- Fixed handling of mis-matched client/server colour depths

* Wed Aug 22 2007 Daniel P. Berrange <berrange@redhat.com> - 0.1.0-4.fc8
- Fix mixed endian handling & BGR pixel format (rhbz #253597)
- Clear widget areas outside of framebuffer (rhbz #253599)
- Fix off-by-one in python demo

* Thu Aug 16 2007 Daniel P. Berrange <berrange@redhat.com> - 0.1.0-3.fc8
- Tweaked post scripts
- Removed docs from sub-packages
- Explicitly set license to LGPLv2+
- Remove use of macro for install rule

* Wed Aug 15 2007 Daniel P. Berrange <berrange@redhat.com> - 0.1.0-2.fc8
- Added gnutls-devel requirement to -devel package

* Wed Aug 15 2007 Daniel P. Berrange <berrange@redhat.com> - 0.1.0-1.fc8
- Initial official release
