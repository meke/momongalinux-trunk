%global         momorel 2

Name:           perl-Data-Compare
Version:        1.24
Release:        %{momorel}m%{?dist}
Summary:        Compare perl data structures
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Data-Compare/
Source0:        http://www.cpan.org/authors/id/D/DC/DCANTRELL/Data-Compare-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Find-Rule >= 0.1
BuildRequires:  perl-List-Util
Requires:       perl-File-Find-Rule >= 0.1
Requires:       perl-List-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Compare two perl data structures recursively. Returns 0 if the structures
differ, else returns 1.

%prep
%setup -q -n Data-Compare-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ARTISTIC.txt CHANGELOG GPL2.txt MAINTAINERS-NOTE NOTES README TODO
%{perl_vendorlib}/Data/Compare.pm
%{perl_vendorlib}/Data/Compare
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-2m)
- rebuild against perl-5.20.0

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-2m)
- rebuild against perl-5.18.2

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-2m)
- rebuild against perl-5.14.2

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
