%global momorel 6
%define py_ver %(python -c 'import sys;print(sys.version[0:3])')

Summary: Python bindings for the clutter library
Name: pyclutter
Version: 1.3.2
Release: %{momorel}m%{?dist}
URL: http://www.clutter-project.org/
Source0: http://www.clutter-project.org/sources/%{name}/1.3/%{name}-%{version}.tar.bz2
Patch0: %{name}-%{version}-clutter-1.9.2-compat.patch
Patch1: %{name}-%{version}-clutter-1.9.14-compat.patch
License: LGPL
Group: Development/Languages
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python >= 2.7
BuildRequires: python-devel
BuildRequires: pygtk2-devel
BuildRequires: pycairo-devel
BuildRequires: clutter-devel >= 1.11.8
BuildRequires: cogl-devel >= 1.10.2
BuildRequires: pygobject-devel
Requires: clutter
Obsoletes: pyclutter-gtk

%description
This archive contains the Python modules that allow you to use the
Clutter toolkit in Python programs.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: pyclutter-gtk-devel

%description devel
%{name}-devel
 
%prep
%setup -q

%patch0 -p1 -b .compat
%patch1 -p1 -b .compat_1.9.14

%build
export CFLAGS="%{optflags} -DCLUTTER_DISABLE_DEPRECATION_WARNINGS"

%configure --enable-docs
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README TODO
%{_libdir}/python%{py_ver}/site-packages/clutter
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_includedir}/pyclutter-1.0
%{_libdir}/pkgconfig/pyclutter-1.0.pc
%{_datadir}/gtk-doc/html/pyclutter

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-6m)
- add source

* Fri Jul 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.2-5m)
- fix build
- rebuild against clutter-1.11.8 and cogl-1.10.2
- import two patches from fedora

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-3m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-2m)
- Obsoletes pyclutter-gtk

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-3m)
- build fix (add patch0)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- initial build
