%global momorel 3

Summary: small web server
Name: xsp
Version: 2.10.2
Release: %{momorel}m%{?dist}
URL: http://www.mono-project.com/
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
License: BSD
Group: System Environment/Daemons
Patch0: %{name}-2.4.2-lib64.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mono-devel >= 2.10.2
BuildRequires: monodoc-devel
BuildRequires: pkgconfig
BuildRequires: mono-data-sqlite
Requires: mono-core >= 2.10.2

%description
The XSP server is a small web server that hosts the Mono System.Web
classes for running what is commonly known as ASP.NET.

%prep
%setup -q
%if %{_lib} == "lib64"                                        
%patch0 -p1
%endif

%build
%configure --program-prefix="" --enable-tracing
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.mdb" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README
%{_bindir}/asp-state*
%{_bindir}/dbsessmgr*
%{_bindir}/fastcgi-mono-server*
%{_bindir}/mod-mono-server*
%{_bindir}/xsp*

%{_libdir}/pkgconfig/xsp-?.pc

%{_mandir}/man1/*.*

%{_prefix}/lib/mono/2.0/*.dll
%{_prefix}/lib/mono/2.0/*.exe
%{_prefix}/lib/mono/4.0/*.dll
%{_prefix}/lib/mono/4.0/*.exe

%{_prefix}/lib/mono/gac/Mono.WebServer2
%{_prefix}/lib/mono/gac/mod-mono-server*
%{_prefix}/lib/mono/gac/fastcgi-mono-server*
%{_prefix}/lib/mono/gac/xsp*

# maybe %%{_libdir}
%{_prefix}/lib/%{name}
%{_prefix}/lib/monodoc/sources/*

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.2-3m)
- change Source0 URI

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.2-2m)
- rebuild for mono-2.10.9

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.5-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5

* Mon Jul 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-2m)
- add BuildRequires

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6-2m)
- use BuildRequires

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Fri Dec 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-2m)
- update lib64.patch

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Mon Mar 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4-2m)
- update lib64.patch to enable build

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Thu Jan 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-3m)
- update lib64.patch to enable build with rpm-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-4m)
- good-bye debug symbol

* Tue Nov  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-3m)
- fix %%files

* Tue Nov  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2m)
- change source url (NoSource)

* Thu Aug 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update 2.0

* Wed Apr 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.1-1m)
- update to 1.9.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-3m)
- rebuild against gcc43

* Sun Mar 16 2008 Masanobu Sato <satoshiga@momonga-linux>
- (1.9-2m)
- update lib64 patch

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Thu Dec 13 2007 Masanobu Sato <satoshiga@momonga-linux.or>
- (1.2.6-2m)
- add lib64 patch 

* Thu Dec 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Sun Sep  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Wed May 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Fri Feb  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Tue Oct 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.18-1m)
- update to 1.1.18

* Tue Sep 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.17.1-1m)
- update to 1.1.17.1

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.17-1m)
- update to 1.1.17

* Mon Aug 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.16.1-1m)
- update to 1.1.16.1

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.15-2m)
- revised for multilibarch

* Tue May  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.15-1m)
- rebuild mono -> mono-core

* Fri Apr 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.15-1m)
- update 1.1.15

* Wed Feb 01 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.1.13-3m)
- revised spec file for autoreconf
- revised patch0
- add patch1: xsp-1.1.13-lib64.patch for x86_64

* Mon Jan 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.13-2m)
- add patch0 for @libdir@

* Sun Jan 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.13-1m)
- start.
