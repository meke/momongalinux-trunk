%global momorel 1

Summary: X.Org X11 libdmx runtime library
Name: libdmx
Version: 1.1.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-proto-devel >= 7.5-0.6m
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXau-devel
BuildRequires: libXfixes-devel

#Obsoletes: XFree86-libs, xorg-x11-libs

%description
X.Org X11 libdmx runtime library

%package devel
Summary: X.Org X11 libdmx development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3

#Obsoletes: XFree86-devel, xorg-x11-devel

%description devel
X.Org X11 libdmx development package

%prep
%setup -q

# Disable static library creation by default.
%define with_static 0

%build
%configure \
%if ! %{with_static}
       --disable-static
%endif
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# We intentionally don't ship *.la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog
#%dir %{_libdir}
%{_libdir}/libdmx.so.1
%{_libdir}/libdmx.so.1.0.0

%files devel
%defattr(-,root,root,-)
%if %{with_static}
%{_libdir}/libdmx.a
%endif
%{_libdir}/libdmx.so
#%dir %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/dmx.pc
%{_includedir}/X11/extensions/dmxext.h
#FIXME: manpages should be in man3x?
#%dir %{_mandir}
#%dir %{_mandir}/man3/
%{_mandir}/man3/*.3*

%changelog
* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-7m)
- add BR libXfixes-devel

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-4m)
- %%NoSource -> NoSource

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-3m)
- delete duplicated files

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-2m)
- delete duplicated dir

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated libdmx to version 1.0.1 from X11R7.0

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated libdmx to version 1.0.0 from X11R7 RC4

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated libdmx to version 0.99.2 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.

* Tue Nov 15 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-3
- Added "BuildRequires: libXau-devel, pkgconfig" missing deps.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2
- Changed 'Conflicts: XFree86-devel, xorg-x11-devel' to 'Obsoletes'
- Changed 'Conflicts: XFree86-libs, xorg-x11-libs' to 'Obsoletes'

* Mon Oct 24 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated libdmx to version 0.99.1 from X11R7 RC1

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Renamed package to remove "xorg-x11" from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
