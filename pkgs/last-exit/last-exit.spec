%global momorel 12

Name: last-exit
Summary: Last.fm player
Version: 6
Release: %{momorel}m%{?dist}
License: GPL and LGPL
Group: Applications/Multimedia
Source0: http://lastexit-player.org/releases/%{name}-%{version}.tar.bz2

URL: http://www.o-hand.com/~iain/last-exit/
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool, pkgconfig, GConf2-devel, intltool, glib2-devel
BuildRequires: GConf2-devel >= 2.14.0
BuildRequires: gtk2-devel >= 2.10.6
BuildRequires: gstreamer-devel >= 0.10.11
BuildRequires: gstreamer-plugins-base-devel >= 0.10.11
BuildRequires: libsexy-devel
BuildRequires: dbus-devel >= 0.92
BuildRequires: dbus-glib-devel >= 0.71
BuildRequires: gtk-sharp2-devel >= 2.12.4
BuildRequires: gnome-sharp-devel >= 2.24.0
BuildRequires: gnome-sharp-gnome-vfs2
BuildRequires: mono-core >= 1.2.6
BuildRequires: ndesk-dbus-glib-devel
BuildRequires: libxcb-devel >= 1.2
Requires: mono-core
Requires(pre): hicolor-icon-theme gtk2

%description
Last.fm player for GNOME

%prep
%setup -q

%build
%configure --disable-schemas-install
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/last-exit.schemas \
    %{_sysconfdir}/gconf/schemas/lastfm.schemas \
    > /dev/null || :
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/last-exit.schemas \
	%{_sysconfdir}/gconf/schemas/lastfm.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/last-exit.schemas \
	%{_sysconfdir}/gconf/schemas/lastfm.schemas \
	> /dev/null || :
fi

%files
%defattr (-, root, root)
%doc AUTHORS ChangeLog NEWS README
%{_sysconfdir}/gconf/schemas/last-exit.schemas
%{_sysconfdir}/gconf/schemas/lastfm.schemas
%{_bindir}/last-exit
%{_libdir}/%{name}
%{_datadir}/applications/last-exit.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/%{name}/icons/hicolor/*/actions/*.png
%{_datadir}/%{name}/icons/hicolor/*/actions/*.svg
%{_datadir}/locale/*/*/*

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6-12m)
- rebuild for mono-2.10.9

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6-11m)
- rebuild for glib 2.33.2

* Thu Aug 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6-10m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6-9m)
- rebuild for new GCC 4.6

* Sun Dec 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6-8m)
- no NoSource, official web page has gone

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6-7m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6-6m)
- fix BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6-5m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (6-4m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (6-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6-1m)
- update v6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5-7m)
- rebuild against rpm-4.6

* Mon Oct  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5-6m)
- rebuild against gtk-sharp2-2.12.4
- rebuild against gnome-sharp-2.24.0

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5-5m)
- change BuildPrereq: gst-plugins-base-devel to gstreamer-plugins-base-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5-4m)
- rebuild against gcc43

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5-3m)
- rebuild against gtk-sharp2-2.12.0
- rebuild against gnome-sharp-2.20.0

* Mon Dec 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5-2m)
- use ndesk-dbus
- add patch0 (GNOME BUG #99023)

* Thu Aug  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5-1m)
- update to 5

* Fri Aug  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4-2m)
- add intltoolize (intltool-0.36.0)

* Sat Jan 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4-3m)
- fix icons

* Thu Jan 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4-2m)
- %%make -> make

* Thu Jan 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4-1m)
- update to 4

* Fri Oct 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3-1m)
- update to 3

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-3m)
- rebuild against dbus 0.92

* Sun Aug 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2m)
- rebuild against mono-core

* Fri Jul 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Wed Jul 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-3m)
- change license (GPL and LGPL)
- NoSource -> Source

* Mon Jul 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-2m)
- add icon for menu

* Mon Jul 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-1m)
- initial build
