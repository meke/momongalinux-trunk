%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define debug_package %{nil}

Name: bzr-git
Summary: A plugin for bzr to read git trees
Version: 0.6.8
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
URL: http://bazaar-vcs.org/BzrForeignBranches/Git
# and see https://launchpad.net/bzr-git
Source0: http://samba.org/~jelmer/bzr/bzr-git-%{version}.tar.gz
NoSource: 0

BuildRequires: bzr python-devel >= 2.7
Requires: bzr python-dulwich

%description
A plugin for bzr to read git trees.

%prep
%setup -q

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --root %{buildroot} --skip-build

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING HACKING INSTALL NEWS README TODO
%doc notes/mapping.txt notes/roundtripping.txt
%{_bindir}/bzr-receive-pack
%{_bindir}/bzr-upload-pack
%{_bindir}/git-remote-bzr
%{python_sitelib}/bzrlib/plugins/git/
%{python_sitelib}/bzr_git-*.egg-info

%changelog
* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.8-1m)
- update 0.6.8

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.6-1m)
- update 0.6.6

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-1m)
- update 0.6.2

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-1m)
- update 0.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.4-1m)
- update to 0.5.4

* Tue Feb  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.3-1m)
- update to 0.5.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.2-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.2-1m)
- update to 0.5.2

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.2-1m)
- update 0.4.2

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.1-1m)
- update 0.4.1

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-1m)
- initial packaging
