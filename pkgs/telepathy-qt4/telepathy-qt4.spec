%global         momorel 1
%global         src_name telepathy-qt

Name:           telepathy-qt4
Version:        0.9.3
Release:        %{momorel}m%{?dist}
Summary:        a library for Qt-based Telepathy clients
Group:          System Environment/Libraries
License:        LGPLv2
URL:            http://telepathy.freedesktop.org/wiki/
Source0:        http://telepathy.freedesktop.org/releases/%{src_name}/%{src_name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= 4.8.2
BuildRequires:  dbus-glib-devel
BuildRequires:  farstream-devel
BuildRequires:  glib2-devel
BuildRequires:  gstreamer-devel
BuildRequires:  kdelibs-devel
BuildRequires:  telepathy-farstream-devel
BuildRequires:  telepathy-filesystem
#Requires:       
## old KDE realtime communication frameworks are obsoleted
Obsoletes:      telepathy-qt
Obsoletes:      telepathy-qt-devel
Obsoletes:      tapioca-qt
Obsoletes:      tapioca-qt-devel
Obsoletes:      decibel
Obsoletes:      decibel-devel

%description
This is a library for Qt-based Telepathy clients.

THIS LIBRARY IS NOT YET STABLE. THERE ARE NO ABI OR API GUARANTEES.
Use it at your own risk! Version 0.2 will start the first stable branch.

Telepathy is a D-Bus framework for unifying real time communication,
including instant messaging, voice calls and video calls. It abstracts
differences between protocols to provide a unified interface for
applications.

%package devel
Group:    Development/Libraries
Summary:  Header files for %{name}
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
%description devel
Header files for developing applications using %{name}.

%prep
%setup -q -n %{src_name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog HACKING NEWS README
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/cmake/TelepathyQt4*
%{_libdir}/pkgconfig/*

%changelog
* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-3m)
- build with farstream
- build without telepathy-farsight, is this okay?

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-2m)
- rebuild for glib 2.33.2

* Sat Mar 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0
- source tarball name was renamed from telepathy-qt4 to telepathy-qt

* Fri Nov 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Wed May 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Mon May  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.16-1m)
- update to 0.5.16

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.15-1m)
- update to 0.5.15

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.14-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.14-1m)
- update to 0.5.14

* Wed Mar 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.13-1m)
- update to 0.5.13

* Sat Mar 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.12-1m)
- update to 0.5.12

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.11-1m)
- update to 0.5.11

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.10-1m)
- update to 0.5.10

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.9-1m)
- update to 0.5.9

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-1m)
- update to 0.5.8

* Fri Jan 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-1m)
- update to 0.5.4

* Tue Jan 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Wed Jan  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-2m)
- specify PATH for Qt4

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.14-1m)
- update to 0.3.14

* Wed Nov  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.13-1m)
- update to 0.3.13

* Fri Oct 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.12-1m)
- update to 0.3.12

* Fri Oct  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.11-1m)
- update to 0.3.11
- switch to cmake

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.10-1m)
- update to 0.3.10
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.7-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.7-1m)
- update to 0.3.7

* Mon Jul  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.4-2m)
- rebuild against qt-4.6.3-1m

* Fri Jun 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.4-1m)
- update to 0.3.4

* Wed May 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2
- add BuildRequires: telepathy-farsight-devel

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- initial build for Momonga Linux
