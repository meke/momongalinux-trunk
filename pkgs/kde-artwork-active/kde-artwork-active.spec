%global         momorel 1
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         kdever 4.9.90
%global         kdelibsrel 1m
%global         qtver 4.8.4
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         ftpdirver 3.0
%global         sourcedir %{release_dir}/active/%{ftpdirver}/src

Name:           kde-artwork-active
Version:        0.3
Release:        %{momorel}m%{?dist}
Summary:        Artwork for Plasma Active
License:        GPLv2+
Group:          User Interface/Desktops
URL:            http://plasma-active.org/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildArch:      noarch
BuildRequires:  kdelibs-devel >= %{kdever}
Requires:       kdelibs >= %{kdever}

%description
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%doc
%{_kde4_appsdir}/ksplash/Themes/ActiveAir
%{_kde4_appsdir}/ksmserver/screenlocker/org.kde.active.slide
%{_kde4_datadir}/wallpapers/*

%changelog
* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- import from Fedora

* Sat Oct 27 2012 Rex Dieter <rdieter@fedoraproject.org> 0.3-1
- 0.3

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Feb 18 2012 Jaroslav Reznik <jreznik@redhat.com> - 0.2-1
- update to 0.2 (PA 2)

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sat Nov 19 2011 Rex Dieter <rdieter@fedoraproject.org> 0.1-3
- BR: kde-filesystem
- add %%dir ownerships to avoid dep on kde-workspace

* Fri Nov 18 2011 Jaroslav Reznik <jreznik@redhat.com> - 0.1-2
- noarch package
- fix ksplash installation path

* Mon Nov 07 2011 Jaroslav Reznik <jreznik@redhat.com> - 0.1-1
- initial try
