%global         momorel 1
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         kdever 4.10.3
%global         ftpdirver 0.8.0
%global         sourcedir %{release_dir}/%{name}/%{ftpdirver}/src
%global         qtver 4.8.4
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m

Name:           telepathy-logger-qt
Version:        %{ftpdirver}
Release:        %{momorel}m%{?dist}
Summary:        Telepathy framework logging daemon for Qt
Group:          Applications/Communications
License:        LGPLv2+
URL:            http://www.kde.org
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  telepathy-logger-devel >= 0.6.0
BuildRequires:  telepathy-qt4-devel >= 0.9.3

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%postun
/sbin/ldconfig
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog HACKING TODO
%{_kde4_libdir}/libtelepathy-logger-qt4.so.*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/telepathy-logger-0.2/TelepathyLoggerQt4
%{_kde4_libdir}/libtelepathy-logger-qt4.so
%{_kde4_libdir}/cmake/TelepathyLoggerQt4
%{_kde4_libdir}/pkgconfig/TelepathyLoggerQt4.pc

%changelog
* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Tue Apr  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Thu Feb 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Tue Dec 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-2m)
- rebuild against telepathy-logger-0.6.0

* Fri Oct  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Fri Jul 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-1m)
- initial build for Momonga Linux
