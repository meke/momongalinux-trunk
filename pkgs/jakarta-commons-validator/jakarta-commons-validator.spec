%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define with_tests %{?_with_tests:1}%{!?_without_tests:0}

%define base_name  validator
%define short_name commons-%{base_name}
%define section    free

Summary:        Jakarta Commons Validator
Name:           jakarta-%{short_name}
Version:        1.3.1
Release:        %{momorel}m%{?dist}
Epoch:          0
License:        "ASL 2.0"
Group:          Development/Libraries
Source0:        http://www.apache.org/dist/jakarta/commons/validator/source/commons-validator-%{version}-src.tar.gz
Source1:        %{name}.catalog
URL:            http://jakarta.apache.org/commons/validator/
BuildRequires:  jpackage-utils >= 0:1.5
BuildRequires:  ant >= 1.6.2
BuildRequires:  jakarta-commons-beanutils >= 0:1.5
BuildRequires:  jakarta-commons-collections >= 0:2.1
BuildRequires:  jakarta-commons-digester >= 0:1.3
BuildRequires:  jakarta-commons-logging >= 0:1.0.2
BuildRequires:  oro >= 0:2.0.6
BuildRequires:  junit >= 0:3.7
BuildRequires:  xml-commons-apis
Requires:       jakarta-commons-beanutils >= 0:1.5
Requires:       jakarta-commons-collections >= 0:2.1
Requires:       jakarta-commons-digester >= 0:1.3
Requires:       jakarta-commons-logging >= 0:1.0.2
Requires:       oro >= 0:2.0.6
Requires:       xml-commons-apis
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:       %{short_name} = %{epoch}:%{version}
Obsoletes:      %{short_name} <= %{epoch}:%{version}

%description
A common issue when receiving data either electronically or from user
input is verifying the integrity of the data. This work is repetitive
and becomes even more complicated when different sets of validation
rules need to be applied to the same set of data based on locale for
example. Error messages may also vary by locale. This package attempts
to address some of these issues and speed development and maintenance
of validation rules.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.

# -----------------------------------------------------------------------------

%prep
%setup -q -n %{short_name}-%{version}-src

# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;

cp -p %{SOURCE1} conf/share/catalog

# -----------------------------------------------------------------------------

%build
export CLASSPATH=$(build-classpath \
xml-commons-apis oro junit jakarta-commons-logging jakarta-commons-digester \
jakarta-commons-beanutils jakarta-commons-collections)

%if %{with_tests}
ant -Dskip.download=true -Dbuild.sysclasspath=first test
%endif

ant -Dskip.download=true -Dbuild.sysclasspath=first dist

# -----------------------------------------------------------------------------

%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p dist/%{short_name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|jakarta-||g"`; done)
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# dtds and catalog
mkdir -p $RPM_BUILD_ROOT%{_datadir}/sgml/%{name}
cp -p conf/share/{*.dtd,catalog} $RPM_BUILD_ROOT%{_datadir}/sgml/%{name}

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr dist/docs/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} %{buildroot}%{_javadocdir}/%{name}

# -----------------------------------------------------------------------------

%clean
rm -rf $RPM_BUILD_ROOT

# -----------------------------------------------------------------------------

%post
# remove broken entries in %{_sysconfdir}/sgml/catalog
touch %{_sysconfdir}/sgml/catalog
sed --in-place=.rpmsave -e '/^CATALOG.*\%{_sysconfdir}\/sgml\/%{name}-[0-9].*/d' %{_sysconfdir}/sgml/catalog

%{_bindir}/xmlcatalog --sgml --noout --add \
    %{_sysconfdir}/sgml/%{name}-%{version}-%{release}.cat \
    %{_datadir}/sgml/%{name}/catalog > /dev/null || :

%preun
%{_bindir}/xmlcatalog --sgml --noout --del \
    %{_sysconfdir}/sgml/%{name}-%{version}-%{release}.cat \
    %{_datadir}/sgml/%{name}/catalog > /dev/null || :

%files
%defattr(-,root,root,-)
%doc LICENSE.txt NOTICE.txt
%{_javadir}/*
%{_datadir}/sgml/%{name}

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

# -----------------------------------------------------------------------------

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-5m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-4m)
- fix bug in /etc/sgml/catalog handling
- add workaround to fix broken /etc/sgml/catalog

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- full rebuild for mo7 release

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-1m)
- sync with Fedora 13 (0:1.3.1-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-5jpp.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jan 24 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-5jpp.5m)
- fix %%preun

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-5jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-5jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-5jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.4-5jpp.1m)
- import from Fedora

* Fri Aug 18 2006 Deepak Bhole <dbhole at redhat.com> -  0:1.1.4-5jpp.1
- Move rebuild-gcj-db to correct postun section

* Tue Aug 15 2006 Deepak Bhole <dbhole at redhat.com> -  0:1.1.4-4jpp.1
- Added missing post/postun for javadoc.

* Tue Jul 25 2006 Deepak Bhole <dbhole at redhat.com> -  0:1.1.4-3jpp_1fc
- Added conditional native compilation.
- Converted spec file to UTF8.

* Wed Apr 26 2006 Fernando Nasser <fnasser at redhat.com> - 0:1.1.4-2jpp
- First JPP 1.7 build

* Tue Jul 26 2005 Fernando Nasser <fnasser at redhat.com> - 0:1.1.4-1jpp
- Upgrade to 1.1.4

* Tue Sep 07 2004 Fernando Nasser <fnasser at redhat.com> - 0:1.1.3-1jpp
- Upgrade to 1.1.3

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:1.0.2-3jpp
- Rebuild with ant-1.6.2

* Fri Apr 18 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.0.2-2jpp
- Move DTDs from %%{_datadir}/%{name} to %%{_datadir}/sgml/%%{name} for FHS
  compliance, <http://www.pathname.com/fhs/2.2/fhs-4.11.html#4.11.7>.

* Fri Apr 18 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.0.2-1jpp
- Update to 1.0.2 and JPackage 1.5.
- Move DTDs from %%doc to %%{_datadir}/%%{name}.
- Include catalog for DTDs, and install it if %%{_bindir}/install-catalog
  is available.

* Mon Dec 16 2002 Ville Skytta <ville.skytta at iki.fi> - 1.0.1-1jpp
- 1.0.1.
- Include the DTD in the package (as documentation).

* Sat Nov  2 2002 Ville Skytta <ville.skytta at iki.fi> - 1.0-1jpp
- 1.0, initial JPackage release.
