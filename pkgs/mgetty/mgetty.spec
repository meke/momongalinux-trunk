%global momorel 8
%global date Jun15

Summary: A getty replacement for use with data and fax modems
Name: mgetty
Version: 1.1.36
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://mgetty.greenie.net/
Group: Applications/Communications
Source0: ftp://alpha.greenie.net/pub/mgetty/source/1.1/mgetty%{version}-%{date}.tar.gz 
NoSource: 0
Source2: logrotate.mgetty
Source3: logrotate.sendfax
Source4: logrotate.vgetty
Source5: logrotate.vm
Source6: vgetty.service
Source7: mgetty.service

Patch0: mgetty-1.1.29-config.patch
Patch1: mgetty-1.1.26-policy.patch
Patch4: mgetty-1.1.25-voiceconfig.patch
Patch5: mgetty-1.1.26-issue.patch
Patch6: mgetty-1.1.31-issue-doc.patch
Patch7: mgetty-1.1.29-helper.patch
Patch8: mgetty-1.1.30-mktemp.patch
Patch9: mgetty-1.1.30-unioninit.patch
Patch11: mgetty-1.1.31-helper2.patch
Patch12: mgetty-1.1.31-no-acroread.patch
Patch14: mgetty-1.1.31-sendmail_path.patch
Patch15: mgetty-1.1.31-lfs.patch
Patch16: mgetty-1.1.31-162174_tcflush.patch
Patch18: mgetty-1.1.33-bug_63843.patch
Patch19: mgetty-1.1.33-167830_tty_access.patch
Patch20: mgetty-1.1.33-167830.patch
Patch21: mgetty-1.1.33-turn.patch
Patch22: mgetty-1.1.33-time_range.patch
# man pages corrections
Patch23: mgetty-1.1.36-handle_spaces.patch
# updates info about starting vgetty tgrough systemd
Patch24: mgetty-1.1.36-man.patch
Patch25: mgetty-1.1.36-sd.patch
# patch updates makefiles, it removes hardcoded -s parameter of /usr/bin/install
# thus .debug files for all binaries will be generated properly
Patch26: mgetty-1.1.36-makefiles.patch
Patch27: mgetty-1.1.36-lockdev.patch
Patch28: mgetty-1.1.36-hardening.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info
Requires: mktemp
BuildRequires: groff, tetex, tetex-latex, texinfo

%package sendfax
Summary: Provides support for sending faxes over a modem
Requires: mgetty = %{version}
Group: Applications/Communications
Requires: netpbm-progs
Requires(pre): shadow-utils

%package voice
Summary: A program for using your modem and mgetty as an answering machine
Requires: mgetty = %{version}
Group: Applications/Communications

%package viewfax
Summary: An X Window System fax viewer
Group: Applications/Communications

%description
The mgetty package contains a "smart" getty which allows logins over a
serial line (i.e., through a modem).  If you're using a Class 2 or 2.0
modem, mgetty can receive faxes.  If you also need to send faxes,
you'll need to install the sendfax program.

If you'll be dialing in to your system using a modem, you should
install the mgetty package.  If you'd like to send faxes using mgetty
and your modem, you'll need to install the mgetty-sendfax program.  If
you need a viewer for faxes, you'll also need to install the
mgetty-viewfax package.

%description sendfax
Sendfax is a standalone backend program for sending fax files.  The
mgetty program (a getty replacement for handling logins over a serial
line) plus sendfax will allow you to send faxes through a Class 2
modem.

If you'd like to send faxes over a Class 2 modem, you'll need to
install the mgetty-sendfax and the mgetty packages.

%description voice
The mgetty-voice package contains the vgetty system, which enables
mgetty and your modem to support voice capabilities.  In simple terms,
vgetty lets your modem act as an answering machine.  How well the
system will work depends upon your modem, which may or may not be able
to handle this kind of implementation.

Install mgetty-voice along with mgetty if you'd like to try having
your modem act as an answering machine.
#'

%description viewfax
Viewfax displays the fax files received using mgetty in an X11 window.
Viewfax is capable of zooming in and out on the displayed fax.

If you're installing the mgetty-viewfax package, you'll also need to
install mgetty.

%prep
%setup -q
mv policy.h-dist policy.h
%patch0 -p1 -b .config
%patch1 -p1 -b .policy
%patch4 -p1 -b .voiceconfig
%patch5 -p1 -b .issue
%patch6 -p1 -b .issue-doc
%patch7 -p1 -b .helper
%patch8 -p1 -b .mktemp
%patch9 -p1 -b .unioninit
%patch11 -p1 -b .helper2
%patch12 -p1 -b .no-acroread
%patch14 -p1 -b .sendmail_path
%patch15 -p1 -b .lfs
%patch16 -p1 -b .162174_tcflush
%patch18 -p1 -b .bug_63843
%patch19 -p1 -b .167830_tty_access
%patch20 -p1 -b .167830
%patch21 -p1 -b .turn
%patch22 -p1 -b .time_range
%patch23 -p1 -b .handle_spaces
%patch24 -p1 -b .man
%patch25 -p1 -b .sd
%patch26 -p1 -b .makefile
%patch27 -p1 -b .lockdev
%patch28 -p1 -b .hardening

%build
PATH=$PATH:/usr/X11R6/bin
export PATH

%define makeflags CFLAGS="%{optflags} -Wall -DAUTO_PPP -D_FILE_OFFSET_BITS=64" prefix=%{_prefix} spool=%{_var}/spool BINDIR=%{_bindir} SBINDIR=%{_sbindir} LIBDIR=%{_libdir}/mgetty+sendfax HELPDIR=%{_libdir}/mgetty+sendfax CONFDIR=%{_sysconfdir}/mgetty+sendfax MANDIR=%{_mandir} MAN1DIR=%{_mandir}/man1 MAN4DIR=%{_mandir}/man4 MAN5DIR=%{_mandir}/man5 MAN8DIR=%{_mandir}/man8 INFODIR=%{_infodir} ECHO='"echo -e"' INSTALL=%{__install}
make %{makeflags}
make -C voice %{makeflags}
make -C tools %{makeflags}

pushd frontends/X11/viewfax
make OPT="%{optflags}" CONFDIR=%{_sysconfdir}/mgetty+sendfax
popd

%install
rm -rf --preserve-root %{buildroot}

mkdir -p %{buildroot}{%{_bindir},%{_infodir},%{_libdir}/mgetty+sendfax}
mkdir -p %{buildroot}{%{_mandir},%{_sbindir},/sbin,/var/spool}

%define instflags CFLAGS="%{optflags} -Wall -DAUTO_PPP" prefix=$RPM_BUILD_ROOT%{_prefix} spool=$RPM_BUILD_ROOT%{_var}/spool BINDIR=$RPM_BUILD_ROOT%{_bindir} SBINDIR=$RPM_BUILD_ROOT%{_sbindir} LIBDIR=$RPM_BUILD_ROOT%{_libdir}/mgetty+sendfax HELPDIR=$RPM_BUILD_ROOT%{_libdir}/mgetty+sendfax CONFDIR=$RPM_BUILD_ROOT%{_sysconfdir}/mgetty+sendfax MANDIR=$RPM_BUILD_ROOT%{_mandir} MAN1DIR=$RPM_BUILD_ROOT%{_mandir}/man1 MAN4DIR=$RPM_BUILD_ROOT%{_mandir}/man4 MAN5DIR=$RPM_BUILD_ROOT%{_mandir}/man5 MAN8DIR=$RPM_BUILD_ROOT%{_mandir}/man8 INFODIR=$RPM_BUILD_ROOT%{_infodir} ECHO='echo -e' INSTALL=%{__install}

make install %instflags
install -m700 callback/callback %{buildroot}%{_sbindir}
install -m4711 callback/ct %{buildroot}%{_bindir}

mv %{buildroot}%{_sbindir}/mgetty %{buildroot}/sbin

# this conflicts with efax
mv %{buildroot}%{_mandir}/man1/fax.1 %{buildroot}%{_mandir}/man1/mgetty_fax.1

# tools
make -C tools install %instflags

# voice mail extensions
mkdir -p %{buildroot}%{_var}/spool/voice/{messages,incoming}
make -C voice install %instflags
mv %{buildroot}%{_sbindir}/vgetty %{buildroot}/sbin
install -m 600 -c voice/voice.conf-dist %{buildroot}%{_sysconfdir}/mgetty+sendfax/voice.conf

# don't ship documentation that is executable...
find samples -type f -exec chmod 644 {} \;

make -C frontends/X11/viewfax install %instflags MANDIR=$RPM_BUILD_ROOT%{_mandir}/man1

# install unit file template for vgetty
mkdir -p %{buildroot}%{_unitdir}

# install logrotate control files
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d
install -m 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/logrotate.d/mgetty
install -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/sendfax
install -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/logrotate.d/vgetty
install -m 0644 %{SOURCE5} %{buildroot}%{_sysconfdir}/logrotate.d/vm
install -m 0644 %{SOURCE6} %{buildroot}%{_unitdir}/vgetty@.service
install -m 0644 %{SOURCE7} %{buildroot}%{_unitdir}/mgetty@.service


# remove file droppings from $RPM_BUILD_ROOT
rm -f %{buildroot}%{_bindir}/cutbl

# conflicts with netpbm-progs
rm -f %{buildroot}%{_bindir}/g3topbm

%clean
rm -rf --preserve-root %{buildroot}

%post
if [ $1 -eq 1 ]; then
    systemctl daemon-reload >/dev/null 2>&1 || :
fi

if [ -f %{_infodir}/mgetty.inf* ]; then
    install-info %{_infodir}/mgetty.info.bz2 %{_infodir}/dir \
    --entry="* mgetty: (mgetty). Package to handle faxes, voicemail and more." || :
fi

%preun
if [ -f %{_infodir}/mgetty.inf* ]; then
    install-info --delete %{_infodir}/mgetty.info.bz2 %{_infodir}/dir \
    --entry="* mgetty: (mgetty). Package to handle faxes, voicemail and more." || :
fi

%postun
%systemd_postun

%pre sendfax
getent group fax >/dev/null || groupadd -g %SENDFAX_UID -r fax
getent passwd fax >/dev/null || useradd -r -u %SENDFAX_UID -g fax -d /var/spool/fax -s /sbin/nologin -c "mgetty fax spool user" fax

%post voice
if [ $1 -eq 1 ]; then
    systemctl daemon-reload >/dev/null 2>&1 || :
fi

%postun voice
%systemd_postun


%files
%defattr(-,root,root)
%doc BUGS ChangeLog README.1st Recommend THANKS doc/modems.db samples 
%doc doc/mgetty.ps doc/*.txt
/sbin/mgetty
%{_sbindir}/callback
%{_mandir}/man4/mgettydefs.4*
%{_mandir}/man8/mgetty.8*
%{_mandir}/man8/callback.8*
%{_infodir}/mgetty.info*
%dir %{_sysconfdir}/mgetty+sendfax
%config(noreplace) %{_sysconfdir}/mgetty+sendfax/login.config
%config(noreplace) %{_sysconfdir}/mgetty+sendfax/mgetty.config
%config(noreplace) %{_sysconfdir}/mgetty+sendfax/dialin.config
%config(noreplace) %{_sysconfdir}/logrotate.d/mgetty
%dir %{_libdir}/mgetty+sendfax

%files sendfax
%defattr(-,root,root)
%dir %{_var}/spool/fax
%attr(0755,fax,root) %dir %{_var}/spool/fax/incoming
%attr(0755,fax,root) %dir %{_var}/spool/fax/outgoing
%attr(0755,root,root) %{_bindir}/ct
%{_bindir}/faxq
%{_bindir}/faxrm
%{_bindir}/faxrunq
%{_bindir}/faxspool
%{_bindir}/g3cat
%{_bindir}/g32pbm
%{_bindir}/kvg
%{_bindir}/newslock
%{_bindir}/pbm2g3
%{_bindir}/sff2g3
%{_sbindir}/faxrunqd
%{_sbindir}/sendfax
#%%dir %%{_libdir}/mgetty+sendfax
%{_libdir}/mgetty+sendfax/cour25.pbm
%{_libdir}/mgetty+sendfax/cour25n.pbm
%attr(04711,fax,root) %{_libdir}/mgetty+sendfax/faxq-helper
%{_mandir}/man1/g32pbm.1*
%{_mandir}/man1/pbm2g3.1*
%{_mandir}/man1/g3cat.1*
%{_mandir}/man1/mgetty_fax.1*
%{_mandir}/man1/faxspool.1*
%{_mandir}/man1/faxrunq.1*
%{_mandir}/man1/faxq.1*
%{_mandir}/man1/faxrm.1*
%{_mandir}/man1/coverpg.1*
%{_mandir}/man1/sff2g3.1*
%{_mandir}/man5/faxqueue.5*
%{_mandir}/man8/faxq-helper.8*
%{_mandir}/man8/faxrunqd.8*
%{_mandir}/man8/sendfax.8*
#%%dir %%{_sysconfdir}/mgetty+sendfax
%config(noreplace) %{_sysconfdir}/mgetty+sendfax/sendfax.config
%config(noreplace) %{_sysconfdir}/mgetty+sendfax/faxrunq.config
%config %{_sysconfdir}/mgetty+sendfax/faxspool.rules.sample
%config(noreplace) %{_sysconfdir}/mgetty+sendfax/faxheader
%config(noreplace) %{_sysconfdir}/logrotate.d/sendfax

%files voice
%defattr(-,root,root)
%doc voice/doc/* voice/Announce voice/ChangeLog voice/Readme
%dir %{_var}/spool/voice
%dir %{_var}/spool/voice/incoming
%dir %{_var}/spool/voice/messages
%{_unitdir}/vgetty@.service
%{_unitdir}/mgetty@.service
/sbin/vgetty
%{_bindir}/vm
%{_bindir}/pvfamp
%{_bindir}/pvfcut
%{_bindir}/pvfecho
%{_bindir}/pvffft
%{_bindir}/pvffile
%{_bindir}/pvffilter
%{_bindir}/pvfmix
%{_bindir}/pvfnoise
%{_bindir}/pvfreverse
%{_bindir}/pvfsine
%{_bindir}/pvfspeed
%{_bindir}/rmdfile
%{_bindir}/pvftormd
%{_bindir}/rmdtopvf
%{_bindir}/pvftovoc
%{_bindir}/voctopvf
%{_bindir}/pvftolin
%{_bindir}/lintopvf
%{_bindir}/pvftobasic
%{_bindir}/basictopvf
%{_bindir}/pvftoau
%{_bindir}/autopvf
%{_bindir}/pvftowav
%{_bindir}/wavtopvf
%{_mandir}/man1/zplay.1*
%{_mandir}/man1/pvf.1*
%{_mandir}/man1/pvfamp.1*
%{_mandir}/man1/pvfcut.1*
%{_mandir}/man1/pvfecho.1*
%{_mandir}/man1/pvffile.1*
%{_mandir}/man1/pvffft.1*
%{_mandir}/man1/pvfmix.1*
%{_mandir}/man1/pvfreverse.1*
%{_mandir}/man1/pvfsine.1*
%{_mandir}/man1/pvfspeed.1*
%{_mandir}/man1/pvftormd.1*
%{_mandir}/man1/pvffilter.1*
%{_mandir}/man1/pvfnoise.1*
%{_mandir}/man1/rmdtopvf.1*
%{_mandir}/man1/rmdfile.1*
%{_mandir}/man1/pvftovoc.1*
%{_mandir}/man1/voctopvf.1*
%{_mandir}/man1/pvftolin.1*
%{_mandir}/man1/lintopvf.1*
%{_mandir}/man1/pvftobasic.1*
%{_mandir}/man1/basictopvf.1*
%{_mandir}/man1/pvftoau.1*
%{_mandir}/man1/autopvf.1*
%{_mandir}/man1/pvftowav.1*
%{_mandir}/man1/wavtopvf.1*
%{_mandir}/man8/vgetty.8*
#%%dir %%{_sysconfdir}/mgetty+sendfax
%config %{_sysconfdir}/mgetty+sendfax/voice.conf
%config %{_sysconfdir}/logrotate.d/vgetty
%config %{_sysconfdir}/logrotate.d/vm

%files viewfax
%defattr(-,root,root)
%doc frontends/X11/viewfax/C* frontends/X11/viewfax/README
%{_bindir}/viewfax
#%%dir %%{_libdir}/mgetty+sendfax
%{_libdir}/mgetty+sendfax/viewfax.tif
%{_mandir}/man1/viewfax.1*

%changelog
* Tue Nov 19 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.36-9m)
- update any patch and source

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.36-8m)
- add systemd service

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.36-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.36-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.36-5m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.36-4m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.36-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.36-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.36-1m)
- update to 1.1.36-Jun15
-- import Patch21,22,23 from Rawhide (1.1.36-1)
-- update Patch0,1,9,14 for fuzz=0
-- drop 3,13,17, merged upstream
-- License: GPLv2+

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.33-6m)
- remove --entry option of install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.33-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.33-4m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.33-3m)
- rebuild against perl-5.10.0-1m

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.33-2m)
- delete duplicate dir %%dir %%{_sysconfdir}/mgetty+sendfax
- delete duplicate dir %%dir %%{_libdir}/mgetty+sendfax
  -> move to mgetty pkg

* Tue Jun  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.33-1m)
- update to 1.1.33
-- add patch14 to 20 from FC

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.31-2m)
- revise spec file for rpm-4.4.2

* Sat Mar 12 2005 Toru Hoshina <t@momonga-linux.org>
- (1.1.31-1m)
- ver up. synv with FC3(1.1.31-2).

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.30-4m)
- revised docdir permission

* Wed Mar 31 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.30-3m)
- revised spec. g3topbm.

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.30-2m)
- revised spec for enabling rpm 4.2.

* Wed Apr  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.30-1m)
- ver up.

* Sun May 12 2002 Toru Hoshina <t@Kondara.org>
- (1.1.28-2k)
- ver up.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.1.26-6k)

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (1.1.26-4k)
- nigirisugi.txt
- revised spec file.

* Thu May 10 2001 Motonobu Ichimura <famao@kondara.org>
- (1.1.26-2k)
- import to Mary (includes Bugfix)

* Wed May 02 2001 Motonobu Ichimura <famao@kondara.org>
- (1.1.26-3k)
- merged rawhide one to Jirai

* Wed Apr 18 2001 Nalin Dahyabhai <nalin@redhat.com>
- define _sysconfdir, not sysconfdir

* Mon Apr 16 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.1.26
- add logrotate.vm and logrotate.vgetty (note from Heiner Kordewiner)
- add voice/{Announce,Changelog,Readme} to documentation set

* Tue Apr 10 2001 Nalin Dahyabhai <nalin@redhat.com>
- define CNDFILE in policy.h

* Tue Mar 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- change the default group in the vgetty configuration file from phone to uucp,
  which matches the settings for faxes

* Tue Mar 13 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.1.25
- ditch the elsa patch in favor of the current vgetty patch
- don't need to strip binaries, buildroot policies do that
- add docs to the voice subpackage

* Tue Jan 16 2001 Nalin Dahyabhai <nalin@redhat.com>
- use mkdtemp() when printing faxes

* Mon Jan 15 2001 Preston Brown <pbrown@redhat.com>
- fix misdetection of USR voice modem detection <cjj@u.washington.edu>

* Mon Jan 08 2001 Preston Brown <pbrown@redhat.com>
- 1.1.24 includes tmpfile security enhancements, some of our patches

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Tue Sep 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- back out quoting patch
- change Copyright: distributable to License: GPL
- add URL
- remove logging changes from excl patch, based on input from Gert
- rework ia64 patch, break out gets/fgets change based on input from Gert

* Thu Sep  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- make sure all scripts quote variables where possible (#17179)
- make sure all scripts use mktemp for generating temporary files

* Sat Aug 26 2000 Bill Nottingham <notting@redhat.com>
- update to 1.1.22; fixes security issues

* Mon Aug  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix excl patch to keep everything from segfaulting all the time (#11523,11590)

* Mon Jul 24 2000 Nalin Dahyabhai <nalin@redhat.com>
- forcibly strip binaries (#12431)
- change dependency on libgr-progs (which is gone) to netgr-progs (#10819)
- change dependency on giftoppm to giftopnm (#8088)
- attempt to plug some potential security problems (#11874)

* Thu Jul 12 2000 Than Ngo <than@redhat.de>
- add new V250modem patch from ELSA (thanks to Jrgen Kosel)

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Jun 23 2000 Than Ngo <than@redhat.de>
- add support ELSA Microlink 56k

* Sun Jun  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- Overhaul for FHS fixes.
- Stop removing logs in postun.
- Stop stripping everything.
- ia64 fixes.

* Wed May 17 2000 Ngo Than <than@redhat.de>
- updated the new vgetty (#bug 10440)

* Sat May  6 2000 Bill Nottingham <notting@redhat.com>
- fix compilation with new gcc, or ia64, or something...

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Thu Feb 03 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed

* Tue Sep  7 1999 Jeff Johnson <jbj@redhat.com>
- add fax print command (David Fox).

* Tue Sep 07 1999 Cristian Gafton <gafton@redhat.com>
- version 1.1.21

* Tue Aug 31 1999 Jeff Johnson <jbj@redhat.com>
- move callback to base package (#4799).

* Wed Jun  2 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.1.20 (#3216).

* Tue Apr  6 1999 Bill Nottingham <notting@redhat.com>
- strip setuid bit from ct

* Tue Mar 23 1999 Preston Brown <pbrown@redhat.com>
- better log handling

* Wed Jan 06 1999 Cristian Gafton <gafton@redhat.com>
- rebuild for glibc 2.1

* Sat Aug 22 1998 Jos Vos <jos@xos.nl>
- Use a patch for creating policy.h using policy.h-dist.
- Add viewfax subpackage (X11 fax viewing program).
- Add logrotate config files for mgetty and sendfax log files.
- Properly define ECHO in Makefile for use with bash.
- Add optional use of dialin.config (for modems supporting this).
- Change default notification address to "root" (was "faxadmin").
- Change log file names according to better defaults.
- Change default notify program to /etc/mgetty+sendfax/new_fax (was
  /usr/local/bin/new_fax).

* Fri Aug 21 1998 Jeff Johnson <jbj@redhat.com>
- add faxrunqd man page (problem #850)
- add missing pbm2g3 (and man page); remove unnecessary "rm -f pbmtog3"
- delete redundant ( cd tools; make ... )

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Apr 10 1998 Cristian Gafton <gafton@redhat.com>
- updated to 1.1.14
- AutoPPP patch
 
* Thu Dec 18 1997 Mike Wangsmo <wanger@redhat.com>
- added more of the documentation files to the rpm

* Wed Oct 29 1997 Otto Hammersmith <otto@redhat.com>
- added install-info support

* Tue Oct 21 1997 Otto Hammersmith <otto@redhat.com>
- updated version

* Wed Oct 15 1997 Erik Troan <ewt@redhat.com>
- now requires libgr-progs instead of netpbm

* Mon Aug 25 1997 Erik Troan <ewt@redhat.com>
- built against glibc
