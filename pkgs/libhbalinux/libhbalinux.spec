%global momorel 2

Name:           libhbalinux
Version:        1.0.16
Release:        %{momorel}m%{?dist}
Summary:        FC-HBAAPI implementation using scsi_transport_fc interfaces

Group:          System Environment/Libraries
License:        LGPLv2
URL:            http://www.open-fcoe.org
# This source was cloned from upstream git
# To get the tar package, just run:
# git clone git://open-fcoe.org/openfc/libhbalinux.git && cd libhbalinux
# git archive --prefix=libhbalinux-%{version}/ v%{version} > ../libhbalinux-%{version}.tar
# cd .. && xz libhbalinux-%{version}.tar
Source0:        %{name}-%{version}.tar.xz
Patch0:		libhbalinux-1.0.13-conf.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libhbaapi-devel libpciaccess-devel libtool automake
Requires:       libhbaapi

%description
SNIA HBAAPI vendor library built on top of the scsi_transport_fc interfaces

%package devel
Summary:            A file needed for libhbalinux application development
Group:              Development/Libraries
Requires:           %{name}%{?_isa} = %{version}-%{release}
Requires:           pkgconfig

%description devel
The libhbalinux-devel package contains the library pkgconfig file.

%prep
%setup -q
%patch0 -p1 -b .conf

%build
./bootstrap.sh
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'


%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
ORG=org.open-fcoe.libhbalinux
LIB=%{_libdir}/libhbalinux.so.2.0.2
STR="$ORG       $LIB"
CONF=%{_sysconfdir}/hba.conf
if test -f $CONF; then
  grep -E -q ^[[:space:]]*$ORG[[:space:]]+$LIB $CONF
  if test $? -ne 0; then
    echo $STR >> $CONF;
  fi
fi

%postun
/sbin/ldconfig
ORG=org.open-fcoe.libhbalinux
CONF=%{_sysconfdir}/hba.conf
if test -f $CONF; then
  grep -v $ORG $CONF > %{_sysconfdir}/hba.conf.new
  mv %{_sysconfdir}/hba.conf.new %{_sysconfdir}/hba.conf
fi

%files
%defattr(-,root,root,-)
%doc README COPYING
%{_libdir}/*.so.*

%files devel
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so

%changelog
* Thu Oct 24 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.16-2m)
- split -devel package
 
* Thu Oct 24 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.16-1m)
- update 1.0.16

* Fri Apr 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.10-1m)
- Initial Commit MomongaLinux

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.10-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon May 24 2010 Jan Zeleny <jzeleny@redhat.com> - 1.0.10-1
- rebased to 1.0.10, bugfix release (see git changelog for more info)

* Fri Dec 04 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.9-20091204git
- rebased to the latest version in upstream git

* Thu Jul 30 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.8-1
- rebase of libhbalinux, spec file adjusted to match changes

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.7-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Apr 01 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.7-3
- replaced unofficial 1.0.7 source tarball with official one
- update of Makefile, part of it moved to postinstall section
  of spec file

* Tue Mar 31 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.7-2
- minor changes in spec file

* Mon Mar 2 2009 Chris Leech <christopher.leech@intel.com> - 1.0.7-1
- initial build

