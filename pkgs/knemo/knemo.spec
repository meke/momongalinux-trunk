%global momorel 1
%global srcrel 12956
%global qtver 4.8.3
%global kdever 4.9.3
%global kdelibsrel 1m

Summary: A KDE network monitoring tool
Name: knemo
Version: 0.7.6
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Internet
URL: http://extragear.kde.org/apps/knemo/
Source0: http://www.kde-apps.org/CONTENT/content-files/%{srcrel}-%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.7.4-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: wireless-tools
Requires: net-tools
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: net-tools
BuildRequires: wireless-tools >= 29

%description
KNemo displays for every network interface an icon in the systray. Tooltips
and an info dialog provide further information about the interface. Passive
popups inform about interface changes. A traffic plotter is also integrated.
It polls the network interface status every second using the ifconfig, route
and iwconfig tools.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --remove-category Network \
  --add-category System \
  %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_kde4_bindir}/knemo
%{_kde4_libdir}/kde4/kcm_knemo.so
%{_kde4_datadir}/applications/kde4/knemo.desktop
%{_kde4_appsdir}/knemo
%{_kde4_appsdir}/kconf_update/knemo*
%{_kde4_datadir}/autostart/knemo.desktop
%{_kde4_iconsdir}/hicolor/*/apps/knemo.png
%{_kde4_iconsdir}/hicolor/scalable/apps/knemo.svgz
%{_kde4_iconsdir}/hicolor/scalable/status/knemo-monochrome-*.svgz
%{_kde4_iconsdir}/hicolor/22x22/status/knemo-*.png
%{_kde4_datadir}/kde4/services/kcm_knemo.desktop
%{_datadir}/locale/*/LC_MESSAGES/*knemo.mo

%changelog
* Wed Dec  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.6-1m)
- update to 0.7.6

* Fri Nov  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Wed Oct 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.4-1m)
- update to 0.7.4

* Sat Jan  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Mon Aug  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Mon Dec  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.80-2m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.80-1m)
- update to 0.6.80
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-4m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.3-3m)
- move knemo.desktop from Network to System on menu
- add desktop.patch

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-2m)
- rebuild against qt-4.6.3-1m

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Fri Apr  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- version 0.6.2

* Tue Jan 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-2m)
- replace source tar-ball (replaced by upstream)

* Mon Jan 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- version 0.6.1

* Sun Dec  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-1m)
- version 0.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Apr  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-1m)
- version 0.5.2

* Tue Mar 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-1m)
- version 0.5.1

* Tue Feb 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-1m)
- version 0.5.0
- remove desktop.patch
- good-bye KDE3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-7m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.8-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-5m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.8-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.8-3m)
- %%NoSource -> NoSource

* Wed Dec 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8-2m)
- rebuild against wireless-tools-29-1m

* Wed May 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8-1m)
- version 0.4.8

* Tue Apr  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-1m)
- version 0.4.7
- update desktop.patch

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-1m)
- initial package for Momonga Linux
