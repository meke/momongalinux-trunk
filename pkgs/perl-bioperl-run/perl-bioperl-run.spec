%global         momorel 3

Name:           perl-bioperl-run
Version:        1.006900
Release:        %{momorel}m%{?dist}
Summary:        Modules to provide a Perl interface to various bioinformatics applications

Group:          Development/Libraries
License:        GPL+ or Artistic
URL:            http://bioperl.org
Source0:        http://www.cpan.org/authors/id/C/CJ/CJFIELDS/BioPerl-Run-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  perl-bioperl >= 1.6.921
BuildRequires:  perl-Algorithm-Diff >= 1
BuildRequires:  perl-File-Sort
BuildRequires:  perl-libxml-perl
BuildRequires:  perl-IPC-Run
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-Module-Build
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
Bioperl-run contain modules that provide a Perl interface to various
bioinformatics applications. This allows various applications to be
used with common Bioperl objects.

%prep
# note that archive and tarball version numbers don't quite match in this release
%setup -q -n BioPerl-Run-%{version}

# remove all execute bits from the doc stuff
chmod -x INSTALL INSTALL.PROGRAMS

cat << \EOF > %{_builddir}/BioPerl-Run-%{version}/%{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Bio::FeatureIO)/d' | \
  sed -e '/^perl(Bio::Tools::Run::StandAloneBlastPlus::BlastMethods/d' | \
  sed -e '/^perl(Bio::Tools::Run::WrapperBase::CommandExts/d'
EOF
 
%define __perl_requires %{_builddir}/BioPerl-Run-%{version}/%{name}-req
chmod +x %{__perl_requires}

%build
%{__perl} Build.PL --installdirs vendor << EOF
a
EOF

./Build

%install
rm -rf %{buildroot}
perl Build pure_install --destdir=%{buildroot}

# remove some spurious files
find %{buildroot} -type f -a \( -name .packlist \
  -o \( -name '*.bs' -a -empty \) \) -exec rm -f {} ';'
# remove errant execute bit from the .pm's
find %{buildroot} -type f -name '*.pm' -exec chmod -x {} 2>/dev/null ';'
# remove empty directory
rm -fr %{buildroot}%{perl_vendorarch}

%check
%{?_with_check:./Build test || :}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
## don't distribute "doc" "scripts" subdirectories, they don't contain docs
%doc AUTHORS Changes DEPENDENCIES INSTALL INSTALL.PROGRAMS LICENSE README
%{perl_vendorlib}/Bio/DB/ESoap
%{perl_vendorlib}/Bio/DB/ESoap.pm
%{perl_vendorlib}/Bio/DB/SoapEUtilities
%{perl_vendorlib}/Bio/DB/SoapEUtilities.pm
%{perl_vendorlib}/Bio/Factory/EMBOSS.pm
%dir %{perl_vendorlib}/Bio/Installer
%{perl_vendorlib}/Bio/Installer/*.pm
%{perl_vendorlib}/Bio/Tools/Run/*
%{_mandir}/man3/*.3*    

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006900-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006900-2m)
- rebuild against perl-5.18.2

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006900-1m)
- update to 1.006900
- rebuild against perl-bioperl-1.6.921

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-3m)
- rebuild against perl-5.10.1

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-2m)
- remove duplicate directories

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.1-1m)
- import from Fedora

* Sat Feb 28 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.6.1-1
- Update to final 1.6.1 release

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5.9-0.3.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 11 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.5.9-0.2.2
- Update to release candidate 2 for 1.6.0 should fix file conflicts 
  with ConfigData (#484495)

* Tue Feb  3 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.5.9-0.1.1
- Update to release candidate 1 for 1.6.0 
- Remove examples subdirectory, no longer distributed
- Deprecated modules no longer need removing
- Add BR: perl(IPC::Run)

* Tue Mar  4 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.5.2_100-3
- rebuild for new perl

* Tue Apr 18 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_100-2
- Remove deprecated modules that depend on non-existent
  Bio::Root::AccessorMaker

* Tue Apr 17 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_100-1
- Initial Fedora package.
