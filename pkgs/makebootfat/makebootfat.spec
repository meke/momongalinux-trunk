%global momorel 6

%define new_ldlinux	0

Summary: Utility for creation bootable FAT disk
Name: makebootfat
Version: 1.4
Release: %{momorel}m%{?dist}
Group: 	Applications/System
License: GPLv2+
URL: http://advancemame.sourceforge.net/doc-makebootfat.html
Source0: http://dl.sourceforge.net/sourceforge/advancemame/%{name}-%{version}.tar.gz
NoSource: 0
Source1: makebootfat-README.usbboot

%if %{new_ldlinux}
#  Get syslinux-VERSION.tar.bz2 from
#	ftp://ftp.kernel.org/pub/linux/utils/boot/syslinux/
#  or
#	ftp://ftp.kernel.org/pub/linux/utils/boot/syslinux/Old/
#  Then
#	bunzip2 -cd syslinux-VERSION.tar.bz2 | tar -xvf -
#	cp syslinux-VERSION/ldlinux.bss ldlinux.bss-VERSION
#	cp syslinux-VERSION/ldlinux.sys ldlinux.sys-VERSION
#	rm -rf syslinux-VERSION
#
Source2: ldlinux.bss-3.36
Source3: ldlinux.sys-3.36
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
This utility creates a bootable FAT filesystem and populates it
with files and boot tools.

It was mainly designed to create bootable USB and Fixed disk
for the AdvanceCD project (http://advancemame.sourceforge.net), but
can be successfully used separately for any purposes.


%prep
%setup -q

install -p -m644 %{SOURCE1} README.usbboot


%build

%configure
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -d %{buildroot}%{_datadir}/%{name}/x86
install -p -m644 mbrfat.bin %{buildroot}%{_datadir}/%{name}/x86
%if %{new_ldlinux}
install -p -m644 %{SOURCE2} %{buildroot}%{_datadir}/%{name}/x86/ldlinux.bss
install -p -m644 %{SOURCE3} %{buildroot}%{_datadir}/%{name}/x86/ldlinux.sys
%else
install -p -m644 test/ldlinux.bss %{buildroot}%{_datadir}/%{name}/x86
install -p -m644 test/ldlinux.sys %{buildroot}%{_datadir}/%{name}/x86
%endif


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root)
%doc AUTHORS COPYING HISTORY README README.usbboot
%{_bindir}/*
%{_datadir}/%{name}
%{_mandir}/*/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against rpm-4.6

* Wed Apr 23 2008 Yohsuke Ooi <meke@fedoraproject.org>
- (1.4-1m)
- Initial commit Momonga Linux

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.4-7
- Autorebuild for GCC 4.3

* Thu Sep 27 2007 Dmitry Butskoy <Dmitry@Butskoy.name> - 1.4-6
- always distribute own ldlinux.sys as well as ldlinux.bss
- add conditional macro %%{new_ldlinux} (default off) to build the package
  with ldlinux.bss and ldlinux.sys taken from some syslinux source directly.
- Update README.usbboot .

* Fri Aug 17 2007 Dmitry Butskoy <Dmitry@Butskoy.name>
- Change License tag to GPLv2+

* Fri Sep  1 2006 Dmitry Butskoy <Dmitry@Butskoy.name> - 1.4-5
- rebuild for FC6

* Tue Aug  1 2006 Dmitry Butskoy <Dmitry@Butskoy.name> - 1.4-4
- avoid world-writable docs (#200829)

* Wed Feb 15 2006 Dmitry Butskoy <Dmitry@Butskoy.name> - 1.4-3
- rebuild for FC5

* Mon Dec 26 2005 Dmitry Butskoy <Dmitry@Butskoy.name> - 1.4-2
- place mbrfat.bin and ldlinux.bss under %%{_datadir}/%%{name}/x86

* Mon Dec 24 2005 Dmitry Butskoy <Dmitry@Butskoy.name> - 1.4-1
- accepted for Fedora Extra (review by John Mahowald <jpmahowald@gmail.com>)

* Mon Oct  3 2005 Dmitry Butskoy <Dmitry@Butskoy.name> - 1.4-1
- initial release
- install mbrfat.bin and ldlinux.bss binary files, they are
  actually needed to create something useful here.
- add README.usbboot -- instruction how to make diskboot.img more helpful
  (written by me).

