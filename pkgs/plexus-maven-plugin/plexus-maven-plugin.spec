%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define parent plexus
%define subname maven-plugin

%define maven_settings_file %{_builddir}/%{name}/settings.xml

Name:           %{parent}-%{subname}
Version:        1.3.5
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Plexus Maven plugin
License:        MIT
Group:          Development/Libraries
URL:            http://plexus.codehaus.org/

# svn export \
#   http://svn.codehaus.org/plexus/archive/plexus-maven-plugin/tags/plexus-maven-plugin-1.3.5/
# tar czf plexus-maven-plugin-1.3.5-src.tar.gz plexus-maven-plugin-1.3.5
Source0:        %{name}-%{version}-src.tar.gz

Patch0:         %{name}-maven-doxia.patch
Patch1:         %{name}-add-jdom-dep.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-plugin
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-surefire
BuildRequires:  maven2-common-poms >= 1.0
BuildRequires:  maven-doxia-sitetools
BuildRequires:  maven-surefire-provider-junit
BuildRequires:  plexus-appserver >= 1.0-0.5.4m
BuildRequires:  plexus-cdc >= 1.0-0.5.1m
BuildRequires:  plexus-container-default
BuildRequires:  plexus-runtime-builder >= 1.0-0.9.3m

Requires:       maven2 >= 2.0.8-2m
Requires:       maven2-common-poms >= 1.0
Requires:       plexus-appserver >= 1.0-0.5.4m
Requires:       plexus-cdc >= 1.0-0.5.1m
Requires:       plexus-container-default
Requires:       plexus-runtime-builder >= 1.0-0.9.3m

Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2

%description
Plexus Maven Plugin helps create plexus component descriptions
from within Maven.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q
%patch0 -b .sav
%patch1 -b .sav

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp \
        -e \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        install javadoc:javadoc

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/plexus
install -pm 644 target/*.jar \
        $RPM_BUILD_ROOT%{_javadir}/%{parent}/%{subname}-%{version}.jar
%add_to_maven_depmap org.codehaus.plexus %{name} 1.2 JPP/%{parent} %{subname}
(cd $RPM_BUILD_ROOT%{_javadir}/%{parent} && for jar in *-%{version}*; \
  do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# pom
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -pm 644 pom.xml \
        $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}.pom

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

cp -pr target/site/apidocs/* \
        $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/

ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_javadir}/plexus/*
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/%{name}

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.5-3m)
- full rebuild for mo7 release

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-2m)
- BuildRequires:  maven-doxia-sitetools
- BuildRequires:  maven-surefire-provider-junit

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-1m)
- sync with Fedora 13 (0:1.3.5-1.3)
- temporarily drop these dependencies
-- BuildRequires: maven-doxia-sitetools
-- BuildRequires: maven-surefire-provider-junit

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- remove duplicate directories

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-1m)
- import from Fedora 11

* Tue Mar 24 2009 Deepak Bhole <dbhole@redhat.com> 1.2-3.7
- Adjust tomcat requirements

* Mon Mar 23 2009 Deepak Bhole <dbhole@redhat.com> - 0:1.2-3.6
- Fix cdc requirement version
- Added tomcat BR

* Mon Mar 23 2009 Deepak Bhole <dbhole@redhat.com> - 0:1.2-3.5
- Build on ppc64

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.2-3.4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jul  9 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.2-2.4
- drop repotag

* Thu Feb 28 2008 Deepak Bhole <dbhole@redhat.com> 1.2-2jpp.3
- Rebuild

* Fri Sep 21 2007 Deepak Bhole <dbhole@redhat.com> 0:1.2-2jpp.2
- ExcludeArch ppc64

* Fri Feb 23 2007 Tania Bento <tbento@redhat.com> 0:1.2-2jpp.1
- Fixed %%Release.
- Fixed %%BuildRoot.
- Removed %%Vendor.
- Removed %%Distribution.
- Removed %%post and %%postun sections for javadoc.
- Fixed %%License to correct license.
- Edited instructions on how to generate source drop.
- Edited %%description.
- Added gcj support option.

* Tue Oct 17 2006 Deepak Bhole <dbhole@redhat.com> 1.2-2jpp
- Update for maven2 9jpp.

* Mon Jun 12 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.2-1jpp
- Initial build
