%global momorel 2

Name:           gssdp
Version:        0.12.0
Release:        %{momorel}m%{?dist}
Summary:        Resource discovery and announcement over SSDP

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.gupnp.org/
Source0:        ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.12/%{name}-%{version}.tar.xz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: libsoup-devel >= 2.4
BuildRequires: dbus-glib-devel
BuildRequires: glib2-devel >= 2.18
BuildRequires: libxml2-devel
BuildRequires: GConf2-devel
BuildRequires: gtk2-devel
BuildRequires: gtk-doc
BuildRequires: NetworkManager-devel
BuildRequires: libglade2-devel
BuildRequires: gobject-introspection-devel >= 0.9.10

Requires: dbus

%description
GSSDP implements resource discovery and announcement over SSDP and is part 
of gUPnP.  GUPnP is an object-oriented open source framework for creating 
UPnP devices and control points, written in C using GObject and libsoup. The 
GUPnP API is intended to be easy to use, efficient and flexible.

%package devel
Summary: Development package for gssdp
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libsoup-devel >= 2.4
Requires: glib2-devel >= 2.18
Requires: pkgconfig

%description devel
Files for development with gssdp.

%package docs
Summary: Documentation files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk-doc
BuildArch: noarch

%description docs
This package contains developer documentation for %{name}.

%prep
%setup -q

%build
%configure --disable-static \
	--enable-gtk-doc \
	--enable-introspection=yes \
	--enable-silent-rules \
	LIBS="-lgobject-2.0 -lglib-2.0"
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
rm %{buildroot}%{_libdir}/libgssdp-1.0.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README NEWS
%dir %{_datadir}/gssdp
%{_bindir}/gssdp-device-sniffer
%{_libdir}/libgssdp-1.0.so.*
%{_datadir}/gssdp/gssdp-device-sniffer.ui
%{_libdir}/girepository-1.0/GSSDP-1.0.typelib

%files devel
%defattr(-,root,root,-)
%{_libdir}/libgssdp-1.0.so
%{_libdir}/pkgconfig/gssdp-1.0.pc
%{_includedir}/gssdp-1.0
%{_datadir}/gir-1.0/GSSDP-1.0.gir

%files docs
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gssdp

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-2m)
- rebuild for glib 2.33.2

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.0-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-3m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-2m)
- rebuild against gobject-introspection-0.9.10

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-2m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Wed Jan  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- initial commit Momonga Linux

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Mar  4 2009 Peter Robinson <pbrobinson@gmail.com> 0.6.4-3
- Move docs to noarch subpackage

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 23 2009 Peter Robinson <pbrobinson@gmail.com> 0.6.4-1
- New upstream release

* Thu Dec 18 2008 Peter Robinson <pbrobinson@gmail.com> 0.6.3-3
- Add gtk-doc build req

* Sat Nov 22 2008 Peter Robinson <pbrobinson@gmail.com> 0.6.3-2
- Fix summary

* Mon Oct 27 2008 Peter Robinson <pbrobinson@gmail.com> 0.6.3-1
- New upstream version

* Sun Aug 31 2008 Peter Robinson <pbrobinson@gmail.com> 0.6.2-1
- New upstream version

* Tue Aug 26 2008 Peter Robinson <pbrobinson@gmail.com> 0.6.1-4
- Move glade files from devel to main rpm

* Tue Aug 12 2008 Peter Robinson <pbrobinson@gmail.com> 0.6.1-3
- Patch to fix the build in rawhide

* Fri Aug 8 2008 Peter Robinson <pbrobinson@gmail.com> 0.6.1-2
- Updates based on feedback

* Mon May 19 2008 Peter Robinson <pbrobinson@gmail.com> 0.6.1-1
- Initial package 
