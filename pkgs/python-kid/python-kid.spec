%global momorel 9

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-kid
Version:        0.9.6
Release:        %{momorel}m%{?dist}
Summary:        Kid - A simple and pythonic XML template language

Group:          Applications/Publishing
License:        MIT
URL:            http://www.kid-templating.org/
##              http://pypi.python.org/pypi/kid/0.9.6
Source0:        http://pypi.python.org/packages/source/k/kid/kid-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel >= 0.6c9-2m
BuildRequires:  python-docutils


%description
Kid is a simple Python based template language for generating and
transforming XML vocabularies. Templates are compiled to native Python 
byte-code and may be imported and used like normal Python modules.


%prep
%setup -q -n kid-%{version}


%build
%{__python} setup.py build
pushd doc
sed -i -e 's|rst2html\.py|rst2html|' makefile
make
popd


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT \
    --single-version-externally-managed
rm -rf $RPM_BUILD_ROOT%{python_sitelib}/kid/test
rm -f $RPM_BUILD_ROOT%{python_sitelib}/*egg-info/requires.txt
# Avoid requiring setuptools
chmod 0755 $RPM_BUILD_ROOT%{python_sitelib}/kid/{run,compile}.py
rm -f $RPM_BUILD_ROOT%{_bindir}/*
ln -s ../..%{python_sitelib}/kid/run.py $RPM_BUILD_ROOT%{_bindir}/kid
ln -s ../..%{python_sitelib}/kid/compile.py $RPM_BUILD_ROOT%{_bindir}/kidc


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING HISTORY README doc/*.txt doc/*.css doc/*.html test
%{python_sitelib}/kid*
%{_bindir}/*


%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-9m)
- change Source0 URI

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.6-2m)
- rebuild against python-2.6.1

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.6-1m)
- version up 0.9.6
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-2m)
- rebuild against gcc43

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.5-1m)
- version 0.9.5
- remove Requires: python-elementtree, it's obsoleted by python-2.5-11m

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-2m)
- rebuild against python-2.5

* Sun Jul 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.2-1m)
- import from fedora extra

* Tue Jun 27 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.9.2-1
- Version 0.9.2
- BR python-setuptools >= 0.6a11

* Tue May 23 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.9.1-3
- Fix 'elementtree requried' regression

* Sat May 20 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.9.1-2
- Update project URL

* Fri May 19 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.9.1-1
- Version 0.9.1

* Mon Feb 27 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.9-1
- Version 0.9
- Switch to using setuptools.
- Handle .egg data.
- Don't list python-abi namely -- FC4 and above does it automatically.

* Fri Dec 02 2005 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.8-1
- Version 0.8

* Fri Nov 11 2005 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.7.1-2
- Rebuild.

* Thu Nov 10 2005 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.7.1-1
- Version 0.7.1
- Avoid setuptools using a patch to use standard distutils
- Avoid cruft in doc dir

* Mon Jun 13 2005 Konstantin Ryabitsev <icon@linux.duke.edu> - 0.6.4-1
- Version 0.6.4
- Disttagging

* Sat Apr 16 2005 Seth Vidal <skvidal at phy.duke.edu> 0.6.3-2
- BuildRequires python-elementtree

* Tue Mar 29 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 0.6.3-2
- Add docs and list files instead of using INSTALLED_FILES (safer)
- Trim description a little
- Require python-abi
- BuildRequire python-devel
- Use python_sitelib
- Remove test directory from site_packages
- Use ghosting for .pyo

* Mon Mar 14 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 0.6.3-1
- Version 0.6.3

* Thu Mar 10 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 0.6.2-1
- Initial build in Fedora Extras format.
