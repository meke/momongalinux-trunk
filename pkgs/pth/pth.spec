%global momorel 8

Summary: GNU Portable Threads.
Name: pth
Version: 2.0.7
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://www.ossp.org/pkg/lib/pth/
# http://www.gnu.org/software/pth/
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Pth is a very portable POSIX/ANSI-C based library for Unix platforms
which provides non-preemptive priority-based scheduling for multiple
threads of execution ("multithreading") inside server applications.
All threads run in the same address space of the server application,
but each thread has it's own individual program-counter, run-time
stack, signal mask and errno variable.

%package devel
Summary: Header files, libraries and development documentation for %{name}.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files, static libraries and development
documentation for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.

%prep
%setup -q

%build
%configure
%make
make test

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc ANNOUNCE AUTHORS ChangeLog COPYING HISTORY NEWS README SUPPORT
%doc TESTS THANKS USERS
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root, 0755)
%doc AUTHORS COPYING HACKING PORTING
%doc %{_mandir}/man?/*
%{_bindir}/*
%{_includedir}/*.h
%{_libdir}/*.a
%{_libdir}/*.so
%{_datadir}/aclocal/*.m4
%exclude %{_libdir}/*.la

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-2m)
- %%NoSource -> NoSource

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-1m)
- updatee to 2.0.7

* Mon May 22 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.6-1m)
- update to 2.0.6
- change source URI

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.4-1m)
  update to 2.0.4
  
* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (2.0.3-1m)
- import

* Wed Jul 14 2004 Dag Wieers <dag@wieers.com> - 2.0.1-1
- Updated to release 2.0.1.

* Tue Apr 06 2004 Dag Wieers <dag@wieers.com> - 2.0.0-1
- Initial package. (using DAR)
