%global momorel 1
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Summary: Library for manipulating Apple Binary and XML Property Lists
Name: libplist
Version: 1.8
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2+
Group: System Environment/Libraries
URL: http://www.libimobiledevice.org/
Source0: http://www.libimobiledevice.org/downloads/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1: libplist-1.3-gcc46.patch
BuildRequires: cmake
BuildRequires: glib2-devel
BuildRequires: libxml2-devel
BuildRequires: pkgconfig
BuildRequires: python-devel >= 2.7
BuildRequires: swig

%description
libplist is a library for manipulating Apple Binary and XML Property Lists.

%package devel
Summary: Header files from libplist
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libplist.

%package python
Summary: Python package for libplist
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: python

%description python
Python libraries and support of libplist.

%prep
%setup -q

%patch1 -p1 -b .gcc46~

%build
mkdir -p %{_target_platform} 
pushd %{_target_platform}
%{cmake} ..
popd

## not smp safe...
make -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* README
%{_bindir}/plutil
%{_bindir}/plutil-%{version}
%{_libdir}/%{name}++.so.*
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/plist
%{_libdir}/pkgconfig/%{name}++.pc
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}++.so
%{_libdir}/%{name}.so

%files python
%defattr(-,root,root)
%{python_sitearch}/plist
%{python_sitearch}/plist.so

%changelog
* Wed Aug 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8-1m)
- update 1.8

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-3m)
- rebuild for glib 2.33.2

* Wed Mar 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7-2m)
- disable parallel build

* Wed Mar 21 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (1.7-1m)
- update 1.7

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-1m)
- update 1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-6m)
- rebuild for new GCC 4.6

* Sun Feb 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-5m)
- add patch for gcc46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-3m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-2m)
- touch up spec file

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3
- change to new URL

* Mon Feb 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-1m)
- initial package for libimobiledevice and libgpod-0.7.90
