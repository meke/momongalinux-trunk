%global momorel 1

%global xfce4ver 4.10.0
%global major 3.4

Name:		xfce4-genmon-plugin
Version:	3.4.0
Release:	%{momorel}m%{?dist}
Summary:	Generic monitor plugin for the Xfce panel

Group:		User Interface/Desktops
License:	LGPLv2+
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext, perl-XML-Parser
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	libxml2-devel
BuildRequires:	xfce4-dev-tools
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
The GenMon plugin cyclically spawns the indicated script/program,
captures its output and displays it as a string into the panel.

%prep
%setup -q -n %{name}-%{major}
# Fix icon in 'Add new panel item' dialog
# sed -i 's|Icon=xfce-mouse|Icon=system-run|g' panel-plugin/genmon.desktop.in.in

%build
xdt-autogen
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%find_lang %{name}

rm -f %{buildroot}/%{_libdir}/xfce4/panel/plugins/libgenmon.la

%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/xfce4/panel/plugins/libgenmon.so
%{_datadir}/locale/*
%{_datadir}/xfce4/panel/plugins/*.desktop


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.3.4-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.0-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-9m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-6m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-5m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-3m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-4m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-1m)
- update to 3.1
- import to Momonga from fc

* Mon Jan 20 2007 Christoph Wickert <fedora christoph-wickert de> - 3.0-3
- Rebuild for XFCE 4.4.
- Patch to compile with -Wl,--as-needed (bugzilla.xfce.org #2722)

* Sat Nov 11 2006 Christoph Wickert <fedora christoph-wickert de> - 3.0-2
- Rebuild for XFCE 4.3.99.2.

* Thu Oct 31 2006 Christoph Wickert <fedora christoph-wickert de> - 3.0-1
- Update to 3.0.
- BR xfce4-dev-tools (bugzilla.xfce.org #2435) and gettext.

* Thu Oct 05 2006 Christoph Wickert <fedora christoph-wickert de> - 2.0-3
- Bump release for devel checkin.

* Wed Sep 13 2006 Christoph Wickert <fedora christoph-wickert de> - 2.0-2
- Rebuild for XFCE 4.3.99.1.
- BR perl(XML::Parser).

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 2.0-1
- Update to 2.0 on XFCE 4.3.90.2.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 1.1-7
- Mass rebuild for Fedora Core 6.

* Tue Apr 11 2006 Christoph Wickert <fedora wickert at arcor de> - 1.1-6
- Require xfce4-panel.

* Thu Feb 16 2006 Christoph Wickert <fedora wickert at arcor de> - 1.1-5
- Rebuild for Fedora Extras 5.

* Thu Dec 01 2005 Christoph Wickert <fedora wickert at arcor de> - 1.1-4
- Add libxfcegui4-devel BuildReqs.
- Fix %%defattr.

* Mon Nov 14 2005 Christoph Wickert <fedora wickert at arcor de> - 1.1-3
- Initial Fedora Extras version.
- Rebuild for XFCE 4.2.3.
- disable-static instead of removing .a files.

* Fri Sep 23 2005 Christoph Wickert <fedora wickert at arcor de> - 1.1-2.fc4.cw
- Add libxml2 BuildReqs.

* Sat Jul 09 2005 Christoph Wickert <fedora wickert at arcor de> - 1.1-1.fc4.cw
- Rebuild for Core 4.

* Wed Apr 13 2005 Christoph Wickert <fedora wickert at arcor de> - 1.1-1.fc3.cw
- Initial RPM release.
