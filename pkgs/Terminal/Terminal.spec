%global momorel 1

%global xfce4ver 4.10.0
%global exover 0.9.0
%global dirname xfce4-terminal
%global minorversion 0.6


Summary:	 X Terminal Emulator for the Xfce Desktop environment
Name:		 Terminal
Version:	 0.6.3
Release:	 %{momorel}m%{?dist}
Summary:	 X Terminal Emulator

URL:		 http://terminal.os-cillation.com/
License:	 GPLv2+
Group:		 User Interface/X
#Source0:	 http://www.xfce.org/archive/src/apps/%{dirname}/%{major}/%{name}-%{version}.tar.bz2
Source0:        http://archive.xfce.org/src/apps/%{dirname}/%{minorversion}/%{dirname}-%{version}.tar.bz2
NoSource:	 0
BuildRoot:	 %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:   vte028-devel >= 0.20.5
BuildRequires:   exo-devel >= %{exover}
BuildRequires:   libSM-devel
BuildRequires:   gettext
BuildRequires:   w3c-libwww-devel
BuildRequires:   startup-notification-devel
BuildRequires:   dbus-glib-devel
Requires:        w3c-libwww
# required for Terminal-default-apps.xml
#Requires:	 control-center-filesystem

%description
Terminal is a lightweight and easy to use terminal emulator application
for the X windowing system, with some new ideas and features that make 
it unique among X terminal emulators. 

%prep
%setup -q -n %{dirname}-%{version}

%build
%configure
make %{?_smp_mflags} V=1

%install
rm -rf %{buildroot}
%make install DESTDIR=%{buildroot}

desktop-file-install                                       \
  --delete-original                                        \
  --add-category="GTK"                                     \
  --dir=%{buildroot}%{_datadir}/applications          \
  %{buildroot}%{_datadir}/applications/%{dirname}.desktop

%clean
rm -rf %{buildroot}

##%%post
##touch --no-create %{_datadir}/icons/hicolor || :
##%%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

##%%postun
##if [ $1 -eq 0 ] ; then
##    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
##    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
##fi

# workaround for # 447156 - rpm cannot change directory to symlink
##%%pre
##for dir in ca da fr gl id ja; do
#    [ -d "%{_datadir}/doc/Terminal/$dir/images" -a ! -L "%{_datadir}/doc/Terminal/$dir/images" ] && \
##    [ ! -L "%{_datadir}/doc/Terminal/$dir/images" ] && \
##    rm -rf %{_datadir}/doc/Terminal/$dir/images  || :
##done

##%%posttrans
##gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files
%defattr(-,root,root)
%doc README ChangeLog NEWS COPYING AUTHORS HACKING THANKS
%{_bindir}/xfce4-terminal
%{_datadir}/applications/xfce4-terminal.desktop
%dir %{_datadir}/xfce4/terminal
%{_datadir}/xfce4/terminal/terminal-preferences.ui
%{_datadir}/xfce4/terminal/colorschemes/*
%{_datadir}/gnome-control-center/default-apps/xfce4-terminal-default-apps.xml
%{_datadir}/locale/*/*/*
%{_mandir}/man1/xfce4-terminal.1*
%{_mandir}/*/man1/xfce4-terminal.1*


%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Wed Feb 20 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Wed Nov  7 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.8-4m)
- rebuild against exo-0.9.0-1m

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.8-3m)
- rebuild against libxfce4util-4.10.0-1m (xfce4-4.10.0)

* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-2m)
- fix BuildRequires; use vte028

* Tue Oct 18 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-1m)
- update
- tmp comment out "Requires:	 control-center-filesystem"

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-1m)
- update
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.5-3m)
- full rebuild for mo7 release

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.5-2m)
- rebuild against xfce4 4.6.2

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.5-1m)
- update to 0.4.5

* Tue Apr 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.12-5m)
- good-bye autoreconf

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.12-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.12-3m)
- rebuild against vte-0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.12-2m)
- rebuild against vte-0.20.4

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.12-1m)
- update
- rebuild against xfce4-4.6.1

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.10-1m)
- update
- rebuild against xfce4-4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.8-4m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.8-3m)
- add autoreconf for  libtool-1.5.26

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.8-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.8-1m)
- update
- rebuild against xfce4 4.4.2

* Thu Jul 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.6-3m)
- Requires: w3c-libwww, BuildRequires: w3c-libwww-devel

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.6-2m)
- rebuild against xfce4 4.4.1

* Sun Jan 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.6-1m)
- import to Momonga from fc

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 0.2.6-1
- Upgrade to 0.2.6

* Thu Nov 16 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.8-0.2.rc2
- Add startup-notification-devel and dbus-glib-devel to BuildRequires

* Fri Nov 10 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.8-0.1.rc2
- Update to 0.2.5.8rc2

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.6-0.4.rc1
- Added gtk-update-icon-cache to post/postun

* Wed Oct  4 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.6-0.3.rc1
- Bump release for devel checkin

* Thu Sep  7 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.6-0.2.rc1
- Bump release for xfce rc repo

* Sun Sep  3 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.6-0.1.rc1
- Upgrade to 0.2.5.6-0.1.rc1

* Sun Aug 13 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.4-0.2.beta2
- Bump release for 4.4 beta repo

* Wed Aug  2 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.4-0.1.beta2
- Fix release

* Wed Jul 12 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.4-0.beta2
- Update to 0.2.5.4-0.beta2

* Fri Jun 23 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.5.1-0.beta1.fc6
- Update to 0.2.5.1-0.beta1

* Thu Feb 16 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.4-6.fc5
- Rebuild for fc5

* Wed Aug 17 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.4-5.fc5
- Rebuild for new libcairo and libpixman

* Thu Aug  4 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.4-4.fc5
- Add dist tag

* Mon May 30 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.4-3
- Removed incorrect Requires
- Changed the description text

* Fri May 27 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.4-2
- Fix group to be User Interface/X

* Sat Mar 19 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.4-1
- Upgraded to 0.2.4 version
- Added Terminal/apps desktops files. 

* Tue Mar  8 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.2-2
- Fixed to use %%find_lang
- Removed generic INSTALL from %%doc
- Change description wording: "makes it" to "make it"
- Fixed to include terminal.css 

* Sun Mar  6 2005 Kevin Fenzi <kevin@tummy.com> - 0.2.2-1
- Inital Fedora Extras version
