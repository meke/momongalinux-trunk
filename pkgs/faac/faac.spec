%global momorel 6

Summary: Reference encoder and encoding library for MPEG2/4 AAC
Name: faac
Version: 1.28
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: Applications/Multimedia
URL: http://www.audiocoding.com/
Source0: http://dl.sourceforge.net/sourceforge/faac/faac-%{version}.tar.gz 
NoSource: 0
Patch0: faac-1.28-external-libmp4v2.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libmp4v2-devel >= 1.9.1
BuildRequires: libtool

%description
FAAC is an AAC audio encoder. It currently supports MPEG-4 LTP, MAIN and LOW
COMPLEXITY object types and MAIN and LOW MPEG-2 object types. It also supports
multichannel and gapless encoding.

%package devel
Summary: Development libraries of the FAAC AAC encoder
Group: Development/Libraries
Requires: %{name} = %{version}
Requires: libmp4v2-devel

%description devel
FAAC is an AAC audio encoder. It currently supports MPEG-4 LTP, MAIN and LOW
COMPLEXITY object types and MAIN and LOW MPEG-2 object types. It also supports
multichannel and gapless encoding.

This package contains development files and documentation for libfaac.

%prep
%setup -q 
%patch0 -p1 -b .external-libmp4v2~
# for Patch0
autoreconf -fi

%build
# disable internal mp4v2
%configure --without-mp4v2
%make

%install
%{__rm} -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
%{__rm} -rf %{buildroot}

%post
/sbin/ldconfig 2>/dev/null

%postun
/sbin/ldconfig 2>/dev/null

%files
%defattr(-, root, root, 0755)
%doc AUTHORS COPYING ChangeLog COPYING NEWS README TODO
%{_bindir}/faac
%{_libdir}/libfaac.so.*
%{_mandir}/man1/faac.1.bz2

%files devel
%defattr(-, root, root, 0755)
%{_includedir}/faac*.h
%{_libdir}/libfaac.a
%{_libdir}/libfaac.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.28-4m)
- full rebuild for mo7 release

* Tue Dec 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28-3m)
- fix build failure
-- switch to use external libmp4v2 again
- drop unused patches

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.28-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.28-1m)
- version 1.28
- use internal libmp4v2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26-2m)
- rebuild against rpm-4.6

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.26-1m)
- version 1.26
- use external libmp4v2
- import external-libmp4v2.patch from gentoo
- package devel Requires: libmp4v2-devel to keep compatibility of packages depending on faac
- License: LGPLv2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.25-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.25-2m)
- %%NoSource -> NoSource

* Mon Jan  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25
- bootstrap and configure.in was written in dos format...so add patch0
- faac-1.24-build-mp4v2.patch modify for version 1.25 as faac-1.25-build-mp4v2.patch

* Mon Jan  9 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.24-3m)
- configure with '--with-mp4v2'
- apply 'faac-1.24-build-mp4v2.patch'

* Sat May 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.24-2m)
- update to snapshot

* Sat May 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.24-1m)
- import to Momonga from http://dag.wieers.com/packages/faac/faac.spec

* Mon May  3 2004 Matthias Saou <http://freshrpms.net/> 1.24-1
- Update to 1.24.

* Thu Feb 26 2004 Matthias Saou <http://freshrpms.net/> 1.23.5-1
- Update to 1.23.5.
- Changed license tag to GPL.

* Mon Nov 17 2003 Matthias Saou <http://freshrpms.net/> 1.23.1-1
- Initial rpm release.

