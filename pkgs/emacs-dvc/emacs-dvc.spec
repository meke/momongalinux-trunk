%global momorel 11
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Distributed Version Control for Emacs
Name: emacs-dvc
Version: 0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
URL: http://www.xsteve.at/prg/emacs_dvc/dvc.html
Source0: http://download.gna.org/dvc/download/dvc-snapshot.tar.gz
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Requires(post): info
Requires(preun): info
Obsoletes: dvc-emacs

Obsoletes: elisp-dvc
Provides: elisp-dvc

%description
DVC is a common Emacs front-end for a number of distributed version
control systems.

%prep
%setup -q -n dvc-snapshot

%build
autoreconf -fi
%{configure}
make

%install
rm -rf %{buildroot}
make install prefix=%{buildroot}%{_prefix} \
     	     info_dir=%{buildroot}%{_infodir} \
	     lispdir=%{buildroot}%{e_sitedir}/dvc \
	     MKDIR_P="mkdir -p"

rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/dvc.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/dvc.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc COPYING INSTALL
%dir %{e_sitedir}/dvc
%{e_sitedir}/dvc/*.el*
%{_infodir}/dvc.info*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-11m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-10m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0-9m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-8m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-7m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-5m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-4m)
- update to latest snapshot

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-2m)
- merge dvc-emacs to elisp-dvc
- update to today's snapshot

* Sun Jan  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-1m)
- initial packaging
- revision 570
