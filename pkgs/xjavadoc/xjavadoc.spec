%global momorel 8

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define section		free

Name:		xjavadoc
Version:	1.1
Release:	4jpp.%{momorel}m%{?dist}
Epoch:		0
Summary:	The XJavaDoc engine
License:	BSD
URL:		http://xdoclet.sourceforge.net/xjavadoc/
Group:		Development/Tools
Source0:	xjavadoc-src-1.1-RHCLEAN.tar.bz2
# cvs -d:pserver:anonymous@cvs.sourceforge.net:/cvsroot/xdoclet login
# cvs -z3 -d:pserver:anonymous@cvs.sourceforge.net:/cvsroot/xdoclet export -r XJAVADOC_1_1 xjavadoc
Patch0:		%{name}-build_xml.patch
BuildRequires:	java
BuildRequires:	java-devel >= 1.4.2
BuildRequires:	jpackage-utils
BuildRequires:	junit
BuildRequires:	ant >= 0:1.6
BuildRequires:	ant-nodeps >= 0:1.6
BuildRequires:	ant-junit >= 0:1.6
BuildRequires:	jakarta-commons-logging
BuildRequires:	jakarta-commons-collections
BuildRequires:	xml-commons-apis
BuildRequires:	log4j
BuildRequires:	javacc
BuildRequires:	xalan-j2
BuildRequires:	jrefactory
Requires:	java
Requires:	jakarta-commons-logging
Requires:	jakarta-commons-collections
Requires:	xml-commons-apis
Requires:	log4j
Requires:	xalan-j2
Requires:	jrefactory
%if ! %{gcj_support}
BuildArch:	noarch
%endif
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
The XJavaDoc engine is a complete rewrite of Sun's 
JavaDoc engine that is faster and more suited for 
XDoclet (although it is completely standalone). It 
scans java source code and makes information about 
a class available via special java beans that are 
part of the XJavaDoc core. These beans provide the 
same information about a class as Sun's JavaDoc API, 
and some nice extra features. 

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Requires(post):   coreutils
Requires(postun): coreutils

%description    javadoc
%{summary}.

%prep
%setup -q -n %{name}
find . -name "*.zip" -exec rm {} \;
find . -name "*.jar" -exec rm {} \;

%patch0 -b .sav

%build
build-jar-repository -s -p lib \
xalan-j2 \
junit \
javacc \
log4j \
commons-logging \
commons-collections \
xml-commons-apis \
jrefactory \
ant \


#Fix these binary deps
#BINCLASSPATH=$PWD/lib/ConfigLog4j.jar

ant -Djavacchome=/usr/share/java javadoc

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_javadir}
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}
mkdir -p $RPM_BUILD_ROOT%{_docdir}
install -m 644 target/%{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}

# version less symlinks
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

install -d -m 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 LICENSE.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 docs/architecture.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}

#javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr dist/docs/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name} # ghost symlink

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%postun javadoc
if [ "$1" = "0" ]; then
    rm -f %{_javadocdir}/%{name}
fi

%post
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%{_javadir}/*
%{_docdir}/%{name}-%{version}

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}-%{version}
%ghost %doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4jpp.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4jpp.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-4jpp.6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4jpp.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-4jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-4jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-4jpp.1m)
- import from Fedora

* Mon Mar 26 2007 Deepak Bhole <dbhole@redhat.com> 1.1-4jpp.2
- Fixed incorrect entry in the files section

* Thu Aug 10 2006 Deepak Bhole <dbhole@redhat.com> 1.1-4jpp.1
- Added missing postun section for javadoc.
- Added missing requirements.

* Mon Jul 24 2006 Deepak Bhole <dbhole@redhat.com> 1.1-3jpp_1fc
- Added conditional native compilation.
- BR fixes.

* Thu Apr 06 2005 Ralph Apel <r.apel at r-apel.de> 1.1-2jpp
- First JPP-1.7 release

* Tue Feb 15 2005 Ralph Apel <r.apel at r-apel.de> 1.1-1jpp
- upgrade to 1.1
- replace requirement of xml-commons by xml-commons-apis

* Thu Aug 26 2004 Ralph Apel <r.apel at r-apel.de> 1.0.3-2jpp
- Build with ant-1.6.2

* Fri Jul 02 2004 Ralph Apel <r.apel at r-apel.de> 1.0.3-1jpp
- upgrade to 1.0.3
- just eliminate __GENERATED__ tests because no sources for old xdoclet 
- add xjavadoc javadoc subpackage

* Tue Dec 16 2003 Paul Nasrat <pauln at truemesh.com> 1.0-2jpp
- fix non-versioned symlink typo

* Mon Dec 15 2003 Paul Nasrat <pauln at truemesh.com> 1.0-1jpp
- Initial Release
