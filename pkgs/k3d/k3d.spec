%global momorel 23
%global srcname %{name}-source

Summary: K-3D open-source 3D modeling, animation, and rendering system
Name: k3d
Version: 0.8.0.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.k-3d.org/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{srcname}-%{version}.tar.bz2
NoSource: 0
Source1: %{name}.desktop
Patch0: %{name}-%{version}-gcc-4.6.diff
Patch1: %{name}-%{version}-cmake.diff
# From http://slackbuilds.org/slackbuilds/13.37/graphics/k3d/k3d_gtkmm224.patch
Patch2: %{name}-%{version}-gtkmm2.24.diff
Patch3: %{name}-%{version}-no-Profiler.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: aqsis
Requires: YafaRay
# reserved
# Requires: povray
BuildRequires: CGAL-devel >= 4.1
BuildRequires: ImageMagick >= 6.8.8.10
BuildRequires: ImageMagick-c++-devel >= 6.8.8.10
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: YafaRay
BuildRequires: boost-devel >= 1.55.0
BuildRequires: bzip2-devel
BuildRequires: chrpath
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: doxygen
BuildRequires: e2fsprogs-devel
BuildRequires: expat-devel
BuildRequires: freetype-devel
BuildRequires: glew-devel >= 1.9.0
BuildRequires: gnome-vfs2-devel
BuildRequires: graphviz-devel
BuildRequires: gtkglext-devel
BuildRequires: gtkmm-devel
BuildRequires: gts-devel
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: librsvg2-devel >= 2.22.2-3m
BuildRequires: libsigc++-devel >= 2.1.1
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libuuid-devel
BuildRequires: mesa-libGL-devel
BuildRequires: plib
BuildRequires: python-devel >= 2.7

%description
K-3D is the free-as-in-freedom 3D modeling, animation, and rendering
system for GNU / Linux, Posix, and Win32 operating systems. K-3D features a
robust, object-oriented plugin architecture, designed to scale to the needs of
professional artists, and is designed from-the-ground-up to generate
motion-picture-quality animation using RenderMan-compliant render engines.

%package devel
Summary: Headers for rendering models written by K-3D
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the libraries and header files needed for
rendering a model written by K-3D.

%prep
%setup -q -n %{srcname}-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

# binaries in source tarball
rm tests/*.pyc

find . \( -type f -a -executable \) -exec chmod -x "{}" \;

rm -f CMakeCache.txt

%build
CFLAGS="$RPM_OPT_FLAGS  -frounding-math"
CXXFLAGS="$RPM_OPT_FLAGS  -frounding-math"
export CFLAGS CXXFLAGS
export LD_LIBRARY_PATH=%{_builddir}/%{srcname}-%{version}/%{_target_platform}/%{_lib}:$LD_LIBRARY_PATH

mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake \
	-DK3D_BUILD_GTS_MODULE:BOOL=ON \
	-DPYTHON_INCLUDE_DIR:PATH=%{_includedir}/python2.7 \
	-DPYTHON_LIBRARY:FILEPATH=%{_libdir}/libpython2.7.so \
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48,64x64,128x128}/apps
convert -scale 16x16 share/icons/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 22x22 share/icons/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
convert -scale 32x32 share/icons/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
install -m 644 share/icons/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
convert -scale 64x64 share/icons/%{name}_large.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
convert -scale 128x128 share/icons/%{name}_large.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{name}.png

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
%{_bindir}/update-desktop-database %{_datadir}/applications > /dev/null 2>&1 || :

%postun
/sbin/ldconfig
%{_bindir}/update-desktop-database %{_datadir}/applications > /dev/null 2>&1 || :

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README
%{_bindir}/%{name}
%{_bindir}/%{name}-renderframe
%{_bindir}/%{name}-renderjob
%{_bindir}/%{name}-sl2xml
%{_bindir}/%{name}-uuidgen
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/plugins
# these are not links
%{_libdir}/lib%{name}*.so.*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/shaders
%{_datadir}/%{name}/*.k3d
%{_datadir}/%{name}/documents
%{_datadir}/%{name}/fonts
%{_datadir}/%{name}/geometry
%{_datadir}/%{name}/icons
%{_datadir}/%{name}/locale
%{_datadir}/%{name}/logo
%{_datadir}/%{name}/lsystem
%{_datadir}/%{name}/ngui
%{_datadir}/%{name}/scripts
%{_datadir}/%{name}/shaders/displacement
%{_datadir}/%{name}/shaders/glsl
%{_datadir}/%{name}/shaders/imager
%{_datadir}/%{name}/shaders/light
%{_datadir}/%{name}/shaders/surface
%{_datadir}/%{name}/shaders/volume
%{_datadir}/%{name}/textures
%{_mandir}/man1/%{name}*.1*
%{_datadir}/pixmaps/%{name}.png

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}
%{_libdir}/%{name}/include
%{_libdir}/lib%{name}*.so
%{_datadir}/%{name}/shaders/*.h

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-23m)
- rebuild against ImageMagick-6.8.8.10

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.0.2-22m)
- rebuild against graphviz-2.36.0-1m

* Thu Jan 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-21m)
- rebuild against boost-1.55.0

* Thu Sep 19 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0.2-20m)
- rebuild against ImageMagick-6.8.6.10

* Sun Aug 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-19m)
- fix startup problem

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-18m)
- rebuild against ImageMagick-6.8.6

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-17m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-16m)
- rebuild against ImageMagick-6.8.2.10

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-15m)
- rebuild against CGAL-4.1, boost-1.52.0

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-14m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-13m)
- rebuild against ImageMagick-6.8.0.10

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-12m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0.2-11m)
- rebuild for glew-1.9.0

* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-10m)
- specify the path to python2.7 headers and libraries

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0.2-9m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0.2-8m)
- rebuild for CGAL 4.0.2 and boost 1.50.0

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0.2-7m)
- rebuild for librsvg2 2.36.1

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-6m)
- rebuild agains libtiff-4.0.1

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0.2-5m)
- rebuild against glew-1.7.0
- rebuild against CGAL 4.0

* Sun Jan  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0.2-4m)
- rebuild for CGAL 3.9

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0.2-3m)
- rebuild for boost-1.48.0

* Thu Oct  6 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0.2-2m)
- rebuild against ImageMagick-6.7.2.10

* Sat Aug 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0.2-1m)
- update to 0.8.0.2 (sync Fedora)

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.11.0-19m)
- rebuild for boost

* Fri Aug  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11.0-18m)
- rebuild against CGAL-3.8

* Wed May  4 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.11.0-17m)
- rebuild against python-2.7
-- FIXME: maybe can't build k3d!

* Fri Apr 22 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11.0-16m)
- add a patch to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.11.0-15m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.11.0-14m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.11.0-13m)
- rebuild against boost-1.46.0

* Fri Feb 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.11.0-12m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.11.0-11m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.11.0-10m)
- rebuild against CGAL-3.7

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.11.0-9m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.11.0-8m)
- full rebuild for mo7 release

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11.0-7m)
- revert to version 0.7.11.0
- can not build version 0.8.0.1 on x86_64

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0.1-1m)
- version 0.8.0.1
- remove k3d-google-perftools.module to avoid crashing
- clean up patches

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11.0-6m)
- rebuild against ImageMagick-6.6.2.10

* Sun Jun 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.11.0-5m)
- add BuildRequires: CGAL-devel >= 3.6.0

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11.0-4m)
- rebuild against boost-1.43.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.11.0-3m)
- rebuild against libjpeg-8a

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11.0-2m)
- rebuild against ImageMagick-6.5.9.10

* Sun Dec 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11.0-1m)
- version 0.7.11.0
- import 3 patches from cooker
 - k3d-0.7.11.0-fix-potfiles.patch
 - k3d-0.7.11.0-libdl.patch
 - k3d-0.7.11.0-glx-linkage.patch
- update sigc.patch and gcc44.patch
- remove gcc43.patch
- good-bye autotools and hello cmake!

* Sun Nov 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.7.0-26m)
- rebuild agaisnt boost-1.41.0

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.7.0-25m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.7.0-24m)
- rebuild agaisnt boost-1.40.0

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.7.0-23m)
- rebuild against libjpeg-7

* Mon May 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.7.0-22m)
- fix build with new libtool

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.7.0-21m)
- rebuilt for boost-1.39.0

* Tue Jan 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.7.0-20m)
- apply gcc44 patch and sed script

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.7.0-19m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.7.0-18m)
- rebuild against boost-1.37.0
- License: GPLv2+

* Mon Jan 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.7.0-17m)
- rebuild against ImageMagick-6.4.8.5-1m

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.7.0-16m)
- rebuild against python-2.6.1-2m

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.7.0-15m)
- rebuild against ImageMagick-6.4.2.1

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.7.0-14m)
- rebuild against graphviz-2.18

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.7.0-13m)
- rebuild against librsvg2-2.22.2-3m

* Sun Apr  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.7.0-12m)
- build with external-boost on x86_64 again

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.7.0-11m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.7.0-10m)
- rebuild against OpenEXR-1.6.1

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.7.0-9m)
- %%NoSource -> NoSource

* Thu Feb  7 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.7.0-8m)
- add gcc43.patch
- add -fpermissive for gcc43

* Sun Dec  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.7.0-7m)
- move libk3dngui.so from devel to main

* Sun Sep 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.7.0-6m)
- add patch11 for libsigc++-2.1.1

* Thu Aug 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.7.0-5m)
- build without external-boost on x86_64 for the moment

* Thu Aug 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.7.0-4m)
- rebuild against boost-1.34.1-1m

* Tue Aug 14 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.7.0-1m)
- update to 0.6.7.0

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.6.0-3m)
- BuildRequires: freetype2-devel -> freetype-devel

* Wed Mar  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.6.0-2m)
- Requires: aqsis, yafray

* Wed Mar  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.6.0-1m)
- initial package for Momonga Linux
