%global momorel 1
Name: libisoburn
Summary: libisoburn
Version: 1.2.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
Source0: http://files.libburnia-project.org/releases/%{name}-%{version}.tar.gz
NoSource: 0
URL: http://libburnia-project.org/

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: libburn-devel >= 1.2.2
BuildRequires: libisofs-devel >= 1.2.2

%description
libisofs is a library to create an ISO-9660 filesystem, and supports
extensions like RockRidge or Joliet. It is also a full featured
ISO-9660 editor, allowing you to modify an ISO image or multisession
disc, including file addition and removal, change of file names and
attributes, etc

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static \
  --enable-libreadline \
  --enable-libacl \
  --enable-xattr \
  --enable-zlib \
  --enable-libcdio \
  --enable-external-filters \
  CFLAGS=-DLIBISOFS_MISCONFIGURATION=0
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/xorrecord.info  %{_infodir}/dir || :
/sbin/install-info %{_infodir}/xorriso.info  %{_infodir}/dir || :
/sbin/install-info %{_infodir}/xorrisofs.info %{_infodir}/dir || :
%postun
/sbin/ldconfig

%preun
if [ $1 = 0 ]; then
   /sbin/install-info --delete %{_infodir}/xorrecord.info  %{_infodir}/dir || :
   /sbin/install-info --delete %{_infodir}/xorriso.info  %{_infodir}/dir || :
   /sbin/install-info --delete %{_infodir}/xorrisofs.info %{_infodir}/dir || :
fi

%files
%defattr (-, root, root)
%doc AUTHORS CONTRIBUTORS COPYING COPYRIGHT README TODO
%{_bindir}/osirrox
%{_bindir}/xorrecord
%{_bindir}/xorriso
%{_bindir}/xorrisofs
%{_libdir}/%{name}.so.*
%exclude %{_libdir}/*.la
%{_infodir}/xorrecord.info.*
%{_infodir}/xorriso.info.*
%{_infodir}/xorrisofs.info.*
%exclude %{_infodir}/dir
%{_mandir}/man1/xorrecord.1.*
%{_mandir}/man1/xorriso.1.*
%{_mandir}/man1/xorrisofs.1.*

%files devel
%defattr (-, root, root)
%{_includedir}/%{name}
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/libisoburn-1.pc

%changelog
* Thu Jun 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update to 1.2.2

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.8-1m)
- update to 1.1.8

* Sun Oct  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6
- use trick CFLAGS=-DLIBISOFS_MISCONFIGURATION=0

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-2m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-2m)
- full rebuild for mo7 release

* Tue Jul  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.8-1m)
- initial build pl00

