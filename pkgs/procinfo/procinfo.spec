%global momorel 12
Summary: A tool for gathering and displaying system information.
Name: procinfo
Version: 18
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source: ftp://ftp.cistron.nl/pub/people/svm/%{name}-%{version}.tar.gz
Patch0: procinfo-14-misc.patch
Patch3: procinfo-17-mandir.patch
Patch5: procinfo-17-uptime.patch
Patch6: procinfo-17-lsdev.patch
Patch7: procinfo-18-acct.patch
Patch8: procinfo-18-mharris-use-sysconf.patch
Patch9: procinfo-18-maxdev.patch
Patch10: procinfo-18-ranges.patch
Patch11: procinfo-18-cpu-steal.patch
Patch12: procinfo-18-intr.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: ncurses-devel

%description
The procinfo command gets system data from the /proc directory (the
kernel filesystem), formats it and displays it on standard output.
You can use procinfo to acquire information about your system from the
kernel as it is running.

Install procinfo if you'd like to use it to gather and display system
data.

%prep
%setup -q
%patch0 -p1 -b .misc
%patch3 -p1 -b .mandir
%patch5 -p1 -b .uptime
%patch6 -p1 -b .lsdev
%patch7 -p1 -b .acct
%patch8 -p1 -b .mharris-use-sysconf
%patch9 -p1 -b .maxdev
%patch10 -p1 -b .ranges
%patch11 -p1 -b .steal
%patch12 -p1 -b .intr

%build
make RPM_OPT_FLAGS="%{optflags} -I/usr/include/ncurses" LDFLAGS= LDLIBS=-lncurses

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/%{_mandir}/man8
make install prefix=%{buildroot}/usr mandir=%{buildroot}/%{_mandir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README CHANGES
%{_bindir}/procinfo
%{_bindir}/lsdev
%{_bindir}/socklist
%{_mandir}/man8/procinfo.8*
%{_mandir}/man8/lsdev.8*
%{_mandir}/man8/socklist.8*

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (18-12m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (18-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (18-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (18-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (18-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (18-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (18-6m)
- rebuild against gcc43

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (18-5m)
- use ncurses

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (18-4m)
- rebuild against libtermcap and ncurses

* Tue Mar 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (18-3m)
- delete TODO from %%doc
- delete duplicate BuildRoot: 
- use macro

* Fri Oct 19 2001 Toru Hoshina <t@Kondara.org>
- (18-2k)
- merge from Jirai.

* Thu Aug 23 2001 Motonobu Ichimura <famao@kondara.org>
- up to 18 (based on rawhide 18-2)

* Sat Jul 21 2001 Bernhard Rosenkraenzer <bero@redhat.com> 18-2
- Add BuildRequires (#49561)
- s/Copyright/License/

* Wed Apr 25 2001 Bernhard Rosenkraenzer <bero@redhat.com> 18-1
- 18

* Mon Dec 11 2000 Erik Troan <ewt@redhat.com>
- built on all archs

* Thu Nov 16 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- fix up lsdev (Bug #10295 and a couple of unreported bugs)
- fix up calculation of uptime milliseconds (introduced by gcc acting
  differently from previous releases, t/100*100 != t*100/100)
  (Bug #20741)

* Mon Oct 16 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix uptime calculation (Bug #18673)
  This problem was introduced by gcc acting differently from previous
  releases (t * 100 / HZ --> overflow; t / HZ * 100 ok).

* Mon Oct  2 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix CPU stats after very long uptimes (Bug #17391)

* Tue Aug  8 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix reported number of CPUs on sparc (Bug #9597)

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Matt Wilson <msw@redhat.com>
- FHS man paths (patch3)
- buildable as non-root (patch3)

* Fri Feb 18 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up the CPU detection patch (Bug #9497)

* Sat Feb  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- handle compressed man pages

* Mon Oct 04 1999 Michael K. Johnson <johnsonm@redhat.com>
- fix cpu detection on sparc and alpha

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- upgraded to r17, which incorporates several of our patches + smp fixes
- fix bug #1959

* Tue Mar 23 1999 Preston Brown <pbrown@redhat.com>
- patched to work with kernels with LOTS of IRQs. (bug 1616)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Fri Mar 12 1999 Michael Maher <mike@redhat.com>
- updated to version 16
- closed bug 1349

* Fri Nov 20 1998 Michael K. Johnson <johnsonm@redhat.com>
- updated to version 15 to fix bugzilla 70.

* Fri Oct  2 1998 Jeff Johnson <jbj@redhat.com>
- calculate time per-cent on non-{alpha,i386} correctly.

* Thu Sep 10 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to version 14
- fixed the spec file 

* Thu Apr 30 1998 Donnie Barnes <djb@redhat.com>
- updated from 0.11 to 13
- added socklist program

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 23 1997 Michael K. Johnson <johnsonm@redhat.com>
- updated to version 0.11

* Tue Jun 17 1997 Erik Troan <ewt@redhat.com>
- built against glibc
