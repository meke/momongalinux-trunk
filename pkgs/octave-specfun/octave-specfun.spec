%global momorel	1
%global octpkg specfun

Name:           octave-%{octpkg}
Version:        1.1.0
Release:        %{momorel}m%{?dist}
Summary:        Specfun for Octave
Group:          Applications/Engineering
License:        GPLv2+
URL:            http://octave.sourceforge.net/specfun/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
# avoid stripping binaries to get useful debuginfo packages
# fedora specific - not upstreamed
Patch0:         %{name}-nostrip.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  octave-devel

Requires:       octave
Requires(post): octave
Requires(postun): octave


Obsoletes:      octave-forge <= 20090607


%description
Special functions including elliptic functions, sine/cosine integral functions,
complementary error functions and exponential integrals, Heaviside and Dirac
functions, Riemann zeta function and others.

%prep
%setup -q -n %{octpkg}
%patch0 -p0 -b .nostrip

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install

%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo
%doc %{octpkgdir}/packinfo/COPYING
%{octpkglibdir}

%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0
- build against octave-3.6.1-1m


* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-1m)
- initial import from fedora
