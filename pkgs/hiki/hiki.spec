%global momorel 5

%global dir 43141
#%%global preview 1

Summary: Hiki is a powerful and fast wiki clone written by Ruby.
Name: hiki
Version: 0.8.8.1
Release: %{?preview:0.%{preview}.}%{momorel}m%{?dist}
Source0: http://dl.sourceforge.jp/hiki/%{dir}/hiki-%{version}%{?preview:-preview%{preview}}.tar.gz 
NoSource: 0
Source1: hiki-setup
URL: http://hikiwiki.org/
License: GPL
Group: Applications/Internet
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: ruby

%description
Hiki is a powerful and fast wiki clone written by Ruby.

%prep
%setup -q

%build
%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_bindir}
%__mkdir_p %{buildroot}%{_datadir}/%{name}
%__install -m 755 %{SOURCE1} %{buildroot}%{_bindir}/
%__install -m 755 hiki.cgi %{buildroot}%{_datadir}/%{name}/
%__install -m 644 hikiconf.rb.sample %{buildroot}%{_datadir}/%{name}/
%__install -m 644 dot.htaccess %{buildroot}%{_datadir}/%{name}/
%__cp -a data hiki messages misc plugin style template theme %{buildroot}%{_datadir}/%{name}/

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog doc/*
%{_datadir}/%{name}
%{_bindir}/hiki-setup

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.8.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 17 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.8.1-1m)
- [SECURITY] http://sourceforge.jp/projects/hiki/lists/archive/dev/2009-August/001291.html
- update to 0.8.8.1

* Sun Aug  9 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-2m)
- %%NoSource -> NoSource

* Thu Jun 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.7-1m)
- [SECURITY] CVE-2007-3395
- update to 0.8.7

* Sun Jul 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.8.6-1m)
- update to 0.8.6
- [SECURITY] fixes a DoS vulnerability (CVE-2006-3379)

* Fri Sep 30 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.4-1m)
- minor bugfixes

* Thu Aug  4 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.3-1m)
- including fixes of XSS vulnerability

* Thu Jul 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.2-1m)
- including fixes of XSS vulnerability

* Thu Jul 14 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.1-1m)
- including fixes of CSRF vulnerability

* Wed Jun 29 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.0-1m)
- including fixes of XSS vulnerability

* Tue Jun 14 2005 zunda <zunda at freeshell.org>
- (kossori)
- -N option should not have been specified on OmoiKondara to prevent
  dropping of `-preview'. sumaso.

* Tue Jun 14 2005 zunda <zunda at freeshell.org>
- (0.8.0-0.1.2m)
- less macro in the NoSource line: OmoiKondara and ruby-rpm-1.2.0-16m
  drops the string `-preview' from the following:
  %{?preview:-preview%{preview}}

* Sun Jun  5 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.8.0-0.1.1m)
- update to 0.8.0 preview1

* Sun Jun  5 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6
- SECURITY FIXES

* Sat Sep  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.5-2m)
- apply hiki-0.6.5-bugfix.patch
- add 'Requires: ruby-amrita'

* Mon Jun 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.5-1m)
- update to 0.6.5
- SECURITY FIXES

* Mon Jun 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.4-1m)
- update to 0.6.4
- SECURITY FIXES

* Wed Mar 24 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2
- revise specfile

* Mon Aug  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.2a-3m)
- rebuild against ruby-1.8

* Wed Apr  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.2a-2m)
- fix a bug in 'hiki-setup symlink'
- never copy/symlink /usr/share/hiki/data to the target dir

* Tue Mar 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.2a-1m)

* Thu Feb 27 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.1.1-1m)
- initial package for momonga
