%global momorel 2
%global ft1ver 1.3.1
%global ft1 %{name}-%{ft1ver}
%global demoname ft2demos
%global base_version 2.5.3

Summary: A free and portable TrueType font rendering engine
Name: freetype

%{?include_specopt}
#%%{?!embeded_bitmap:		%global embeded_bitmap	1}
%{?!with_freetype1:		%global with_freetype1	1}
%{!?with_x11:                   %global with_x11        1}

Version: 2.5.3
Release: %{momorel}m%{?dist}
License: BSD or GPLv2+
Group: System Environment/Libraries
URL: http://www.freetype.org/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-doc-%{base_version}.tar.bz2
NoSource: 1
Source2: http://dl.sourceforge.net/sourceforge/%{name}/%{demoname}-%{base_version}.tar.bz2
NoSource: 2
Source3: http://dl.sourceforge.net/project/%{name}/%{name}/%{ft1ver}/%{ft1}.tar.gz
NoSource: 3
Source4: ftconfig.h
# Patch0-Patch3 from opensuse
Patch0: %{name}2-bc.patch
Patch1: %{name}2-bitmap-foundry.patch
#Patch2: ft2-stream-compat.diff
Patch3: %{name}2-subpixel.patch

# security fix from opensuse
Patch10: bugzilla-159166-reduce-embolden-distance.patch

Patch20: %{demoname}-2.1.9-mathlib.patch
Patch21: %{demoname}-2.1.9-lib64.patch

Patch30: freetype-2.3.9-enable-valid-modules.patch
Patch40: freetype-2.5.2-pkgconfig.patch

%if %{with_freetype1}
# ft1 patches
Patch50: %{name}-%{ft1ver}-ttf2bdf.patch
Patch51: %{name}-%{ft1ver}-gcc33.patch
Patch52: %{name}-%{ft1ver}-configure.patch
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: libtool
Provides: %{name}2
Obsoletes: %{name}2

%description
The FreeType engine is a free and portable TrueType font rendering
engine, developed to provide TrueType support for a variety of
platforms and environments. FreeType is a library which can open and
manages font files as well as efficiently load, hint and render
individual glyphs. FreeType is not a font server or a complete
text-rendering library.

%package devel
Summary: Header files and static library for development with FreeType
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Provides: %{name}2-devel
Obsoletes: %{name}2-devel

%description devel
The freetype-devel package contains the header files and static
library needed to develop or compile applications which use the
FreeType TrueType font rendering library.

Install freetype-devel if you want to develop FreeType
applications. If you simply want to run existing applications, you
won't need this package.

%package utils
Summary: Several utilities to manipulate and examine TrueType fonts
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}
Provides: %{name}2-utils
Obsoletes: %{name}2-utils

%description utils
This package contains several utilities which allow you to view and
manipulate TrueType fonts.  They are mainly useful for debugging and
testing purposes, and are not required for using the FreeType
library.

%package demos
Summary: Several demos of FreeType Library
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}
Provides: %{name}2-demos
Obsoletes: %{name}2-demos

%description demos
This package contains several demo programs of FreeType Libarary.
These are  not required for using the FreeType library.

%prep
%setup -q -a 1 -a 2 -a 3

#%%patch0 -p1 -b .bytecode
#%%patch1 -p1 -b .bitmap-foundry
#%%patch2 -p1 -b .ft2-stream-compat
%patch3 -p1 -b .subpixel

# security fix
%patch10 -p1 -b .reduce-embolden-distance

%if %{with_freetype1}
# ft1 patches and configure
pushd %{ft1}
%patch50 -p1 -b .ttf2bdf
%patch51 -p1 -b .gcc
%patch52 -p1 -b .AC_PROG_MAKE_SET
install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .
popd
%endif

pushd %{demoname}-%{base_version}
%patch20 -p1 -b .mathlib
%if %{_lib} == "lib64"
%patch21 -p1 -b .lib64
%endif
popd

%patch30 -p1 -b .valid-module
%patch40 -p1 -b .pkgconfig

%build
# Work around code generation problem with strict-aliasing
# https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=118021
export CFLAGS="%{optflags} -fno-strict-aliasing"
export CXXFLAGS="%{optflags} -fno-strict-aliasing"

%configure --disable-static
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' builds/unix/libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' builds/unix/libtool
%make

%if %{with_freetype1}
pushd %{ft1}
%configure \
	--prefix=%{_prefix} \
	--disable-debug \
	--enable-static \
	--enable-shared \
	--with-locale-dir=%{_datadir}/locale

# parallel build fails
make LIBTOOL=%{_bindir}/libtool

pushd contrib/ttf2bdf
./configure --prefix=%{_prefix}

# parallel build fails
make
popd
popd
%endif

%if %{with_x11}
# Build freetype 2 demos
pushd %{demoname}-%{base_version}
make TOP_DIR=".."
popd
%endif

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall gnulocaledir=%{buildroot}%{_datadir}/locale

%if %{with_freetype1}
pushd %{ft1}
%makeinstall gnulocaledir=%{buildroot}%{_datadir}/locale LIBTOOL=%{_bindir}/libtool
pushd contrib/ttf2bdf
%makeinstall
popd
popd

# fix multilib issues
%ifarch x86_64 s390x ia64 ppc64 ppc64le alpha sparc64 aarch64
%define wordsize 64
%else
%define wordsize 32
%endif

mv %{buildroot}%{_includedir}/freetype2/config/ftconfig.h \
   %{buildroot}%{_includedir}/freetype2/config/ftconfig-%{wordsize}.h
install -p -m 644 %{SOURCE4} %{buildroot}%{_includedir}/freetype2/config/ftconfig.h

# clean up
rm -rf %{buildroot}%{_includedir}/freetype
rm -f  %{buildroot}%{_libdir}/libttf.a
rm -f  %{buildroot}%{_libdir}/libttf.la
rm -rf %{buildroot}%{_datadir}/locale

# for doc
cp -a %{ft1}/README %{ft1}/README-1.3.1
cp -a %{ft1}/announce %{ft1}/announce-1.3.1
cp -a %{ft1}/docs %{ft1}/docs-1.3.1
%endif

%if %{with_x11}
# Install freetype 2 demos
for ftdemo in ftbench ftdiff ftdump ftgamma ftgrid ftlint ftmulti ftstring ftvalid ftview ;do
      builds/unix/libtool --mode=install install -m 755 %{demoname}-%{base_version}/bin/$ftdemo %{buildroot}/%{_bindir}
done
%endif

# Don't package static a or .la files
rm -f %{buildroot}%{_libdir}/*.{a,la}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
#%%doc docs/* ChangeLog* README %{name}-%{version}/docs/*
%{_libdir}/libfreetype.so.*
%if %{with_freetype1}
%doc %{ft1}/README-1.3.1 %{ft1}/announce-1.3.1
%doc %{ft1}/docs-1.3.1
%{_libdir}/libttf.so.*
%endif

%files devel
%defattr(-,root,root)
%{_bindir}/freetype-config
%{_includedir}/freetype2
%{_libdir}/pkgconfig/freetype2.pc
%{_libdir}/libfreetype.so
#{_libdir}/libfreetype.a
#{_libdir}/libfreetype.la
%{_libdir}/libttf.so
%{_datadir}/aclocal/freetype2.m4
%{_mandir}/man1/freetype-config.1*

%if %{with_freetype1}
%files utils
%defattr(-,root,root)
%{_bindir}/fterror
%{_bindir}/ftmetric
%{_bindir}/ftsbit
%{_bindir}/ftstrpnm
%{_bindir}/ftstrtto
%{_bindir}/ftzoom
%{_bindir}/ttf2bdf
%{_mandir}/man1/ttf2bdf.1*
%endif

%files demos
%defattr(-,root,root)
%{_bindir}/ftdump
%{_bindir}/ftlint
%if %{with_x11}
%{_bindir}/ftbench
%{_bindir}/ftdiff
%{_bindir}/ftgamma
%{_bindir}/ftgrid
%{_bindir}/ftmulti
%{_bindir}/ftstring
%{_bindir}/fttimer
%{_bindir}/ftvalid
%{_bindir}/ftview
%endif

%changelog
* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-2m)
- almost sync with Fedora

* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-1m)
- [SECURITY] CVE-2014-2240
- update 2.5.3

* Thu Aug 22 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.0.1-1m)
- update 2.5.0.1

* Mon May 13 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.12-1m)
- update 2.4.12 
- enabled adobe engine

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.11-1m)
- [SECURITY] CVE-2012-5668 CVE-2012-5669 CVE-2012-5670
- update 2.4.11

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.10-2m)
- fix source url

* Mon Jun 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.10-1m)
- update 2.4.10
-- this is minor release, fixing mainly a problem for GhostScript

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.9-1m)
- update 2.4.9

* Wed Nov 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.8-1m)
- update 2.4.8
- [SECURITY] CVE-2011-3439

* Wed Oct 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.7-1m)
- update 2.4.7
- [SECURITY] CVE-2011-3256

* Sat Aug  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.6-1m)
- [SECURITY] CVE-2011-0226
- drop ft2-stream-compat patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.4-2m)
- rebuild for new GCC 4.6

* Fri Dec  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.4-1m)
- update 2.4.4
-- fix many broken rendering

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.3-3m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-2m)
- [SECURITY] CVE-2010-3855
- [SECURITY] patch from http://git.savannah.gnu.org/cgit/freetype/freetype2.git/commit/?id=59eb9f8cfe7d1df379a2318316d1f04f80fba54a

* Tue Oct 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.3-1m)
- update 2.4.3
-- fix any rendering issue

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.2-1m)
- [SECURITY] CVE-2010-1797 CVE-2010-2805 CVE-2010-2806 CVE-2010-2807
- [SECURITY] CVE-2010-2808
- update 2.4.2

* Fri Jul 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-1m)
- update 2.4.1

* Wed Jul 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.0-1m)
- [SECURITY] CVE-2010-2497 CVE-2010-2498 CVE-2010-2499 CVE-2010-2500
- [SECURITY] CVE-2010-2519 CVE-2010-2520
- update 2.4.0

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.9-7m)
- enable gxvalid and otvalid modules
-- http://pc11.2ch.net/test/read.cgi/linux/1188293074/649-651n

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.9-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.9-5m)
- [SECURITY] CVE-2009-0946
- import a security patch from Gentoo

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.9-4m)
- remove Requires: ghostscript-resource

* Sun May 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.9-3m)
- fix build on x86_64
- clean up spec file

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.9-2m)
- support libtool-2.2.x

* Tue Mar 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.9-1m)
- update to 2.3.9
-- 2.3.8 has a ABI breaking bug.

* Sat Feb  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.8-1m)
- update to 2.3.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.7-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.7-2m)
- update Patch2 for fuzz=0
- License: BSD or GPLv2+

* Mon Oct 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.7-1m)
- update to 2.3.7

* Thu Jun 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.6-1m)
- [SECURITY] CVE-2008-1806 CVE-2008-1807 CVE-2008-1808
- update to 2.3.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.5-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.5-2m)
- %%NoSource -> NoSource

* Mon Jul  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.5-1m)
- update 2.3.5

* Thu May 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.4-2m)
- [SECURITY] CVE-2007-2754
  import CVE-2007-2754.patch from gentoo

* Wed Apr 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.4-1m)
- update 2.3.4

* Sat Apr  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.3-1m)
- [SECURITY] CVE-2007-1351
- update 2.3.3

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-3m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-2m)
- rename package from freetype2 to freetype

* Sat Mar 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-1m)
- version 2.3.2
- remove rejected bitmap-foundry.patch
- clean up spec file

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-3m)
- retrived libtool library

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-2m)
- delete libtool library

* Sun Feb  4 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-1m)
- update 2.3.1

* Thu Jan 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-1m)
- update 2.3.0
- any patch import from SUSE

* Mon Jun 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.10-6m)
- [SECURITY] import bugzilla-154928-integer-overflows.patch from opensuse
 +* Thu Jun 01 2006 - mfabian@suse.de
 +- Bugzilla #154928: fix several integer overflows.
  https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=183676
  https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=190593

* Tue May 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1.10-5m)
- revised %%files to resolve a issue of duplicated files

* Fri May 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.10-4m)
- revise %%files for rpm-4.4.2

* Tue Mar 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.10-3m)
- update autofit module to 20060322 cvs snapshot
 - 2006-03-21  David Turner  <david@freetype.org>
 - * src/autofit/aflatin.c (af_latin_metrics_scale): Fix small bug
   that crashes the auto-hinter (introduced by previous patch).
 - 2006-03-20  David Turner  <david@freetype.org>
 - * src/autofit/afcjk.c, src/autofit/aflatin.c, src/base/ftobjs.c,
   src/cff/cffobjs.c, src/cid/cidobjs.c, src/pfr/pfrobjs.c,
   src/sfnt/sfobjs.c, src/sfnt/ttmtx.c, src/type1/t1afm.c,
   src/type1/t1objs.c: Remove compiler warnings when building with
   Visual C++ 6 and /W4.
 - * src/autofit/aflatin.c (af_latin_hints_init): Disable horizontal
   hinting for italic/oblique fonts.
- include freetype-doc-2.1.10

* Sun Mar 12 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.1.10-2m)
- Add patch25 for x86_64

* Sun Mar 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.1.10-1m)
- Update to 2.1.10
- Add demos subpackage as Fedora do.
- Update bytecode patch (freetype-2.1.10-enable-bci.patch)
- Update autofit module (only) to that of version 2.2.0rc4 in cvs(freetype-2.1.10-autofit-20060310cvs.patch).
- - This patch can replace "akito patch" which was applied to previous versions (and cannot be applied from 2.1.10 because of a change which replaces autohint module by autofit module) since new autofit cjk module in upstream cvs version is said to be based on "akito patch". (See http://lists.gnu.org/archive/html/freetype-devel/2006-02/msg00018.html)
- - (Notice: this patch conflicts ft2-autofit-*.diff of openSuSE package.)
- Import several patches from Fedora (freetype-2.1.10-5.2.1.src.rpm). (Explanations of patches are extracted from freetyppe.spec.)
- - freetype-2.1.10-cvsfixes.patch (CVS bug fixes, mostly for embolding)
- - freetype-2.1.10-fixkerning.patch (fix kerning wrongly disabled)
- - freetype-2.1.10-memleak.patch (fix memleak)
- - freetype-2.1.10-xorgfix.patch (put back internal API, used by xorg)
- - ft2demos-2.1.9-mathlib.patch (Add -lm when linking X demos)
- - (Notice: freetype-2.1.10-fixautofit.patch and freetype-2.1.10-fixaliasing.patch are not imported since the changes by these patches are included in autofit module of cvs version, i.e. freetype-2.1.10-autofit-20060310cvs.patch.)
- - (Notice: bugzilla-97202-fix-x-crash.patch in openSuSE package freetype2-2.1.10-9.src.rpm is not imported since it is equivalent to freetype-2.1.10-xorgfix.patch of Fedora. Also enable_ft_optimize_memory.patch in openSuSE is not imported since it is said to conflict with bugzilla-97202-fix-x-crash.patch acording to %%changelog in freetype.spec of openSuSE package.)
- Reorder all (new and existing) patches and patch numbers are changed.
- Add comments of patches from SuSE to previous changelog entries.

* Tue Feb 15 2005 Toru Hoshina <t@momonga-linux.org>
- (2.1.9-3m)
- embed freetype 1.3.1 and utils.

* Thu Nov 18 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.9-2m)
- import following 3 patches from
  ftp.suse.com/pub/projects/m17n/9.1/src/freetype2-2.1.9-1.1.src.rpm
  freetype2-bitmap-foundry.patch
  - (from SuSE changelog)
  - * Fri Mar 07 2003 - mfabian@suse.de
  - - Bug #24755: further improvement: add " Wide" to the family name
  -   for fonts which have an average width >= point size, i.e. fonts
  -   which have glyphs which are at least square (or maybe even
  -   wider). This makes fonts which contain only double width
  -   characters (for example the "misc-fixed" 18x18ja.bdf) clearly
  -   distinct from single width fonts of the same family and
  -   avoids that they get accidentally selected via freetype2/Xft2.
  - * Fri Mar 07 2003 - mfabian@suse.de
  - - Bug #24775: partly fixed by a patch to freetype which returns
  -   "FOUNDRY FAMILY_NAME" as the family_name for bitmap fonts.
  -   Without that change, all bitmap fonts which have "Fixed"
  -   in FAMILY_NAME also had the same family_name "Fixed" via
  -   freetype/Xft2 and it was not possible to distinguish between
  -   them, therefore the selection of "Fixed" bitmap fonts produced
  -   quite surprising results. After this change, the fonts show
  -   up for example as "Misc Fixed", "Etl Fixed", etc. via
  -   freetype2/Xft2, which makes it easy to select the right one.  
  ft2-stream-compat.diff
  - (from SuSE changelog)
  - * Wed Mar 24 2004 - mfabian@suse.de
  - - Add ft2-stream-compat.diff for binary compatibility when
  -   upgrading from SLES8 to SLES9.
  -   The FT_Stream functions have been renamed and although these
  -   functions were declared for internal use only by the freetype
  -   developers, they have been used in Qt (and possibly elsewhere).
  -   Therefore, 3rd party which linked statically against Qt might
  -   not work after upgrading from SLES8 to SLES9.
  -   Fix this problem with a patch by Kurt Garloff <garloff@suse.de>
  -   which defines appropriate weak symbols.
  freetype-autohint-cjkfonts.patch
  - (from SuSE changelog)
  - - Bugzilla #105626: add patches by Takashi IWAI <tiwai@suse.de>
  -   to improve the autohinting (mainly for CJK fonts).
- modify %%post and %%postun sections

* Tue Nov 16 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.1.9-1m)
- update to 2.1.9

* Sat Nov 13 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.1.7-1m)
- update to 2.1.7

* Fri Nov  7 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.1.4-7m)
- update patch3. (revise URI)

* Fri Nov  7 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.1.4-6m)
- roll back to 2.1.4. sumanu...

* Fri Nov  7 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.1.5-1m)
- update to 2.1.5.

* Sat Oct 11 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (2.1.4-5m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue May 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-4m)
- apply cid cmap patch.

* Mon Apr 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-3m)
- update patch3

* Fri Apr 11 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.1.4-2m)
- update patch3

* Wed Apr  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- update to 2.1.4

* Mon Jan 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.3-3m)
- add Requires freetype2 = %{version}-%{release} to -devel

* Fri Dec 13 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.1.3-2m)
- update patch(Patch2. ttcmap4 patch)

* Fri Dec  6 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.1.3-1m)
- update to 2.1.3
- remove some patches.

* Mon Oct  7 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.1.2-2m)
- add some patches.
  fix ttcmap bug for Dyna font.
  fix italic bug. (from cvs)
- add build option switch for embeded bitmap.(osukina hou wo douzo)

* Sun Oct 06 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.0.9-8m)
- revert to 2.0.9.. keep 2.1.2 patches around for further expirmentation

* Sat Oct 05 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.1.2-1m)
- upgrade to "stable" dev version

* Wed May 22 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.9-6k)
- increment Epoch

* Mon May 20 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.0.9-4k)
- remove patch (freetype2-2.0.9-no_embeded_bitmap.patch)
- add patch freetype2-2.0.9-bytecodeinterpreter.patch

* Sun May 19 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (2.0.9-4k)
- 2.1.0 ha mada-dame-----.

* Sun May 19 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (2.1.0-2k)
- ver up to 2.1.0.
- modify URL. (for Super Kondara Antenna.)

* Sun Mar 31 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.0.9-2k)
- update to 2.0.9 (THIS IS TEST!!)
- remake no embeded bitmap font patch.

* Sat Mar 23 2002 Toru Hoshina <t@kondara.org>
- (2.0.5-4k)
- no embeded bitmap font...

* Fri Feb 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.5-2k)
- rewind to 2.0.5 because xft probrem.

* Wed Feb 20 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.0.8-2k)
- update to 2.0.8

* Wed Jan  2 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.5-2k)
- version 2.0.5

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (2.0.4-2k)
- merge from Jirai.

* Wed Aug 29 2001 KUSUNOKI Masanori <nori@kondara.org>
- (2.0.4-3k)
- version 2.0.4

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- (2.0.3-2k)
- version 2.0.3

* Thu Apr  5 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.0.2-5k)
  included ftdocs

* Thu Apr  5 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.0.2-3k)
  update to 2.0.2

* Sun Mar 25 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.1-5k)
- move libfreetype.la from freetype2 to freetype2-devel

* Thu Dec 21 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.0.1

* Fri Nov 10 2000 Shingo Akagaki <dora@kondara.org>
- freetype2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.3.1-5).

* Fri Feb 25 2000 Shingo Akagaki <dora@kondara.org>
- change files section

* Thu Feb 17 2000 Preston Brown <pbrown@redhat.com>
- revert spaces patch, fix up some foundry names to match X ones

* Wed Feb 16 2000 Toru Hoshina <t@kondara.org>
- We still have a problem on alpha platform, freetype libs have to be compiled
- with -O0 option, because any optimization will make it happen:-P

* Mon Feb 07 2000 Norihito Ohmori <nono@kondara.org>
- be a nosrc :-P
- add %defattr
- add /usr/bin/{ftmetric,ftsbit,ftstrtto} to freetype-utils (jitterbug #88)

* Mon Feb 07 2000 Nalin Dahyabhai <nalin@redhat.com>
- add defattr, ftmetric, ftsbit, ftstrtto per bug #9174

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description and summary

* Wed Jan 12 2000 Preston Brown <pbrown@redhat.com>
- make ttmkfdir replace spaces in family names with underscores (#7613)

* Tue Jan 11 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.3.1
- handle RPM_OPT_FLAGS

* Wed Nov 10 1999 Preston Brown <pbrown@redhat.com>
- fix a path for ttmkfdir Makefile

* Thu Aug 19 1999 Preston Brown <pbrown@redhat.com>
- newer ttmkfdir that works better, moved ttmkfdir to /usr/bin from /usr/sbin
- freetype utilities moved to subpkg, X dependency removed from main pkg
- libttf.so symlink moved to devel pkg

* Mon Mar 22 1999 Preston Brown <pbrown@redhat.com>
- strip binaries

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 5)

* Thu Mar 18 1999 Cristian Gafton <gafton@redhat.com>
- fixed the %doc file list

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Mon Feb 15 1999 Preston Brown <pbrown@redhat.com>
- added ttmkfdir

* Tue Feb 02 1999 Preston Brown <pbrown@redhat.com>
- update to 1.2

* Thu Jan 07 1999 Cristian Gafton <gafton@redhat.com>
- call libtoolize to sanitize config.sub and get ARM support
- dispoze of the patch (not necessary anymore)

* Wed Oct 21 1998 Preston Brown <pbrown@redhat.com>
- post/postun sections for ldconfig action.

* Tue Oct 20 1998 Preston Brown <pbrown@redhat.com>
- initial RPM, includes normal and development packages.
