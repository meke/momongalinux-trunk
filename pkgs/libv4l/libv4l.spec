%global         momorel 1

Name:           libv4l
Version:        0.9.3
Release:        %{momorel}m%{?dist}
Summary:        Collection of video4linux support libraries 
Group:          System Environment/Libraries
# Some of the decompression helpers are GPLv2, the rest is LGPLv2+
License:        LGPLv2+ and GPLv2
URL:            http://www.linuxtv.org/downloads/v4l-utils/
# Note that this is a new upstream bundle release which also includes
# various utilities. When time allows it I'll do a new v4l-utils package
# which also packages the utils and obsoletes libv4l
Source0:        http://www.linuxtv.org/downloads/v4l-utils/v4l-utils-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kernel-headers
Obsoletes:      v4l-utils

%description
libv4l is a collection of libraries which adds a thin abstraction layer on
top of video4linux2 devices. The purpose of this (thin) layer is to make it
easy for application writers to support a wide variety of devices without
having to write separate code for different devices in the same class. libv4l
consists of 3 different libraries: libv4lconvert, libv4l1 and libv4l2.

libv4lconvert offers functions to convert from any (known) pixel-format
to V4l2_PIX_FMT_BGR24 or V4l2_PIX_FMT_YUV420.

libv4l1 offers the (deprecated) v4l1 API on top of v4l2 devices, independent
of the drivers for those devices supporting v4l1 compatibility (which many
v4l2 drivers do not).

libv4l2 offers the v4l2 API on top of v4l2 devices, while adding for the
application transparent libv4lconvert conversion where necessary.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}, pkgconfig
Obsoletes:      v4l-utils-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q -n v4l-utils-%{version}


%build
%configure

pushd lib
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS" \
  PREFIX=%{_prefix} LIBDIR=%{_libdir}
popd


%install
rm -rf $RPM_BUILD_ROOT
pushd lib
make install PREFIX=%{_prefix} LIBDIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT
popd

find %{buildroot} -name '*.la' -exec rm -f {} ';'
find %{buildroot} -name '*.a' -exec rm -f {} ';'

%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYING.LIB COPYING ChangeLog README.lib TODO
%{_libdir}/libv4l*.so*
%{_libdir}/v4l1compat.so
%{_libdir}/v4l2convert.so
%{_libdir}/libdvbv5.so*
%{_libdir}/libv4l

%files devel
%defattr(-,root,root,-)
%doc README.lib-multi-threading
%{_includedir}/dvb-*.h
%{_includedir}/libv4l*.h
%{_libdir}/pkgconfig/libv4l*.pc
%{_libdir}/pkgconfig/libdvbv5.pc

%changelog
* Wed Feb 27 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.5-1m)
- update to 0.8.5

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.4-1m)
- update to 0.8.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3-2m)
- rebuild for new GCC 4.6

* Sun Mar 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.3-1m)
- update to 0.8.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-5m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-4m)
- update to 0.8.0

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.91-3m)
- re-renamed from v4l-utils and version down to 0.7.91

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0
- no NoSource

* Thu Apr  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.91-2m)
- *-ctl are using ceilf function in libm

* Thu Apr  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.91-1m)
- update to 0.7.91
- rename from libv4l to v4l-utils

* Mon Jan 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sun Oct 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-2m)
- no NoSource

* Fri Oct 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Sun Oct  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Fri Jul 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.99-1m)
- update to 0.5.99

* Sun Mar 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.9-1m)
- update to 0.5.9

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-1m)
- import from Fedora devel

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.8-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jan 11 2009 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.8-1
- New upstream release 0.5.8

* Tue Dec  2 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.7-1
- New upstream release 0.5.7, fixing rh 473771

* Fri Nov 21 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.6-1
- New upstream release 0.5.6, working around rh 472468

* Thu Nov 20 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.5-1
- New upstream release 0.5.5, fixing rh 472217

* Mon Nov 17 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.4-1
- New upstream release 0.5.4

* Mon Oct 27 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.3-1
- New upstream release 0.5.3

* Thu Oct 23 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.2-1
- New upstream release 0.5.2

* Mon Oct 13 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.1-1
- New upstream release 0.5.1

* Mon Sep 15 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.5.0-1
- New upstream release 0.5.0

* Fri Aug 29 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.4.2-1
- New upstream release 0.4.2

* Tue Aug 26 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.4.1-1
- New upstream release 0.4.1

* Sun Aug  3 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.4.0-1
- New upstream release 0.4.0

* Tue Jul 30 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.3.8-1
- New upstream release 0.3.8

* Sat Jul 26 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.3.7-1
- Initial Fedora package
