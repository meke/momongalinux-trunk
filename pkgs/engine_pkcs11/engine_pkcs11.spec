%global momorel 1

Name:           engine_pkcs11
Version:        0.1.8
Release:        %{momorel}m%{?dist}
Summary:        A PKCS#11 engine for use with OpenSSL

Group:          Development/Libraries
License:        BSD
URL:            http://www.opensc-project.org/engine_pkcs11/
Source0:        http://www.opensc-project.org/files/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  openssl-devel >= 1.0.0 pkgconfig libp11-devel >= 0.2.8
Requires:       openssl >= 1.0.0

%description
Engine_pkcs11 is an implementation of an engine for OpenSSL. It can be loaded
using code, config file or command line and will pass any function call by
openssl to a PKCS#11 module. Engine_pkcs11 is meant to be used with smart
cards and software for using smart cards in PKCS#11 format, such as OpenSC.


%prep
%setup -q


%build
%configure --disable-static --libdir %{_libdir}/openssl
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_datadir}/doc/%{name}/*
%{_libdir}/openssl/engines/engine_pkcs11.so


%changelog
* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.8-1m)
- update 0.1.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.4-2m)
- rebuild against openssl-1.0.0

* Thu Mar 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.4-1m)
- import from Fedora to Momonga for bind-9.7.0

* Sat Aug 22 2009 Kalev Lember <kalev@smartlink.ee> - 0.1.4-7
- Rebuilt with new libp11

* Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 0.1.4-6
- rebuilt with new openssl

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.4-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jan 16 2009 Tomas Mraz <tmraz@redhat.com> - 0.1.4-3
- rebuild with new openssl

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.1.4-2
- Autorebuild for GCC 4.3

* Thu Dec 06 2007 Matt Anderson <mra@hp.com> - 0.1.4-1
- Update to latest upstream sources

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.1.3-5
- Rebuild for selinux ppc32 issue.


* Thu Jun 28 2007 Matt Anderson <mra@hp.com> - 0.1.3-4
- tibbs@math.uh.edu pointed out that OpenSSL engines go in /usr/lib/openssl

* Thu Jun 28 2007 Matt Anderson <mra@hp.com> - 0.1.3-3
- Feedback from tibbs@math.uh.edu, Source0 URL and directory ownership

* Wed Jun 27 2007 Matt Anderson <mra@hp.com> - 0.1.3-2
- Applied changes based on feedback from michael@gmx.net

* Thu Jun 21 2007 Matt Anderson <mra@hp.com> - 0.1.3-1
- Initial RPM packaging

