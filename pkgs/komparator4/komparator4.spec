%global        momorel 1
%global        kdever 4.10.2
%global        kdelibsrel 1m
%global        qtver 4.8.4
%global        qtrel 1m
%global        cmakever 2.8.5
%global        cmakerel 2m
%global        content 116039

Summary:       Komparator4 is a KDE4 port of Komparator.
Name:          komparator4
Version:       1.0
Release:       %{momorel}m%{?dist}
License:       GPLv2
Group:         Development/Tools
URL:           http://kde-apps.org/content/show.php/komparator4?content=116039
Source0:       http://kde-apps.org/CONTENT/content-files/%{content}-%{name}-%{version}.tar.bz2
NoSource:      0
Patch0:        %{name}-%{version}-desktop.patch
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: gettext

%description
Komparator is an application that searches and synchronizes two directories.
It discovers duplicate, newer or missing files and empty folders.
It works on local and network / kioslave protocol folders (like smb:/, ftp://, media:/)

%prep
%setup -q
%patch0 -p1 -b .desktop

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_docdir}/HTML/en/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}*.png
%{_kde4_iconsdir}/hicolor/*/apps/%{name}*.mng
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Wed Apr 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Tue Jul 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Fri Mar 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-1m)
- update to 0.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-1m)
- initial build for Momonga Linux
