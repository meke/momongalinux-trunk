%global momorel 2

%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}
%global pkgdocdir %{_datadir}/doc/%{name}

Summary: The GNU Bourne Again shell
Version: 4.3
Name: bash
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: System Environment/Shells
URL: http://www.gnu.org/software/bash
Source0: ftp://ftp.gnu.org/gnu/bash/bash-%{version}.tar.gz
NoSource: 0

# For now there isn't any doc
# Source2: ftp://ftp.gnu.org/gnu/bash/bash-doc-%{version}.tar.gz

Source1: dot-bashrc
Source2: dot-bash_profile
Source3: dot-bash_logout
Source4: tab.dot.bashrc

# Official upstream patches
# ftp://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/
Patch1: http://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/bash43-001
Patch2: http://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/bash43-002
Patch3: http://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/bash43-003
Patch4: http://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/bash43-004
Patch5: http://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/bash43-005
Patch6: http://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/bash43-006
Patch7: http://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/bash43-007
Patch8: http://ftp.gnu.org/pub/gnu/bash/bash-4.3-patches/bash43-008

# Other patches
Patch101: bash-2.02-security.patch
Patch102: bash-2.03-paths.patch
Patch103: bash-2.03-profile.patch
Patch104: bash-2.05a-interpreter.patch
Patch105: bash-2.05b-debuginfo.patch
Patch106: bash-2.05b-manso.patch
Patch107: bash-2.05b-pgrp_sync.patch
Patch108: bash-2.05b-readline-oom.patch
Patch109: bash-2.05b-xcc.patch
Patch110: bash-3.2-audit.patch
Patch111: bash-3.2-ssh_source_bash.patch
Patch112: bash-bashbug.patch
Patch113: bash-infotags.patch
Patch114: bash-requires.patch
Patch115: bash-setlocale.patch
Patch116: bash-tty-tests.patch

# 484809, check if interp section is NOBITS
Patch117: bash-4.0-nobits.patch

# Do the same CFLAGS in generated Makefile in examples
Patch118: bash-4.1-examples.patch

# Builtins like echo and printf won't report errors
# when output does not succeed due to EPIPE
Patch119: bash-4.1-broken_pipe.patch

# Enable system-wide .bash_logout for login shells
Patch120: bash-4.2-rc2-logout.patch

# Static analyzis shows some issues in bash-2.05a-interpreter.patch
Patch121: bash-4.2-coverity.patch

# Don't call malloc in signal handler
Patch122: bash-4.1-defer-sigchld-trap.patch

# 799958, updated info about trap
Patch123: bash-4.2-manpage_trap.patch

# https://www.securecoding.cert.org/confluence/display/seccode/INT32-C.+Ensure+that+operations+on+signed+integers+do+not+result+in+overflow
Patch125: bash-4.2-size_type.patch

# fix deadlock in trap, backported from devel branch
Patch127: bash-4.2-trap.patch


Requires(post): ncurses-libs
Requires(postun): grep coreutils

Provides: /bin/sh
Provides: /bin/bash
BuildRequires: texinfo bison
BuildRequires: ncurses-devel
BuildRequires: autoconf, gettext

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
The GNU Bourne Again shell (Bash) is a shell or command language
interpreter that is compatible with the Bourne shell (sh). Bash
incorporates useful features from the Korn shell (ksh) and the C shell
(csh). Most sh scripts can be run by bash without modification.

%package doc
Summary: Documentation files for %{name}
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description doc
This package contains documentation files for %{name}.

%prep

%setup -q

# Official upstream patches

# Other patches
%patch101 -p1 -b .security
%patch102 -p1 -b .paths
%patch103 -p1 -b .profile
%patch104 -p1 -b .interpreter
%patch105 -p1 -b .debuginfo
%patch106 -p1 -b .manso
%patch107 -p1 -b .pgrp_sync
%patch108 -p1 -b .readline_oom
%patch109 -p1 -b .xcc
%patch110 -p1 -b .audit
%patch111 -p1 -b .ssh_source_bash
%patch112 -p1 -b .bashbug
%patch113 -p1 -b .infotags
%patch114 -p1 -b .requires
%patch115 -p1 -b .setlocale
%patch116 -p1 -b .tty_tests
%patch117 -p1 -b .nobits
%patch118 -p1 -b .examples
%patch119 -p1 -b .broken_pipe
%patch120 -p1 -b .logout
%patch121 -p1 -b .coverity
%patch122 -p1 -b .defer_sigchld_trap
%patch123 -p1
%patch125 -p1 -b .size_type

echo %{version} > _distribution
echo %{release} > _patchlevel

%build
autoconf
%configure --with-bash-malloc=no --with-afs
# Recycles pids is neccessary. When bash's last fork's pid was X
# and new fork's pid is also X, bash has to wait for this same pid.
# Without Recycles pids bash will not wait.
make "CPPFLAGS=-D_GNU_SOURCE -DRECYCLES_PIDS `getconf LFS_CFLAGS`"

#%%check
#%%make check

%install
rm -rf %{buildroot}

if [ -e autoconf ]; then
  # Yuck. We're using autoconf 2.1x.
  export PATH=.:$PATH
fi

# Fix bug #83776
perl -pi -e 's,bashref\.info,bash.info,' doc/bashref.info

make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}%{_sysconfdir}

# make manpages for bash builtins as per suggestion in DOC/README
pushd doc
sed -e '
/^\.SH NAME/, /\\- bash built-in commands, see \\fBbash\\fR(1)$/{
/^\.SH NAME/d
s/^bash, //
s/\\- bash built-in commands, see \\fBbash\\fR(1)$//
s/,//g
b
}
d
' builtins.1 > man.pages
for i in echo pwd test kill; do
  perl -pi -e "s,$i,,g" man.pages
  perl -pi -e "s,  , ,g" man.pages
done

install -c -m 644 builtins.1 %{buildroot}%{_mandir}/man1/builtins.1

for i in `cat man.pages` ; do
  echo .so man1/builtins.1 > %{buildroot}%{_mandir}/man1/$i.1
  chmod 0644 %{buildroot}%{_mandir}/man1/$i.1
done
popd

# Link bash man page to sh so that man sh works.
ln -s bash.1 %{buildroot}%{_mandir}/man1/sh.1

# Not for printf, true and false (conflict with coreutils)
rm -f %{buildroot}%{_mandir}/man1/printf.1
rm -f %{buildroot}%{_mandir}/man1/true.1
rm -f %{buildroot}%{_mandir}/man1/false.1

ln -sf bash %{buildroot}%{_bindir}/sh
rm -f %{buildroot}%{_infodir}/dir

mkdir -p $RPM_BUILD_ROOT/etc/skel
install -c -m644 %SOURCE1 $RPM_BUILD_ROOT/etc/skel/.bashrc
install -c -m644 %SOURCE2 $RPM_BUILD_ROOT/etc/skel/.bash_profile
install -c -m644 %SOURCE3 $RPM_BUILD_ROOT/etc/skel/.bash_logout

mkdir -p %{buildroot}%{_datadir}/config-sample/bash
install -c -m644 %SOURCE4 %{buildroot}%{_datadir}/config-sample/bash/tab.dot.bashrc

LONG_BIT=$(getconf LONG_BIT)
mv %{buildroot}%{_bindir}/bashbug \
   %{buildroot}%{_bindir}/bashbug-"${LONG_BIT}"
ln -s bashbug-"${LONG_BIT}" $RPM_BUILD_ROOT%{_bindir}/bashbug
ln -s bashbug.1 $RPM_BUILD_ROOT/%{_mandir}/man1/bashbug-"$LONG_BIT".1

# Fix missing sh-bangs in example scripts (bug #225609).
for script in \
  examples/scripts/shprompt
# I don't know why these are gone in 4.3
  #examples/scripts/krand.bash \
  #examples/scripts/bcsh.sh \
  #examples/scripts/precedence \
do
  cp "$script" "$script"-orig
  echo '#!/bin/bash' > "$script"
  cat "$script"-orig >> "$script"
  rm -f "$script"-orig
done

# bug #820192, need to add execable alternatives for regular built-ins
for ea in alias bg cd command fc fg getopts jobs read umask unalias wait
do
  cat <<EOF > "$RPM_BUILD_ROOT"/%{_bindir}/"$ea"
#!/bin/sh
builtin $ea "\$@"
EOF
chmod +x "$RPM_BUILD_ROOT"/%{_bindir}/"$ea"
done

%find_lang %{name}

# copy doc to /usr/share/doc
cat /dev/null > %{name}-doc.files
mkdir -p $RPM_BUILD_ROOT/%{_pkgdocdir}/doc
cp -p COPYING $RPM_BUILD_ROOT/%{_pkgdocdir}
# loadables aren't buildable
rm -rf examples/loadables
for file in CHANGES COMPAT NEWS NOTES POSIX RBASH README examples
do
  cp -rp "$file" $RPM_BUILD_ROOT/%{_pkgdocdir}/"$file"
  echo "%%doc %{_pkgdocdir}/$file" >> %{name}-doc.files
done
echo "%%doc %{_pkgdocdir}/doc" >> %{name}-doc.files

%clean
rm -rf %{buildroot}

# ***** bash doesn't use install-info. It's always listed in %{_infodir}/dir
# to prevent Requires loops

# post is in lua so that we can run it without any external deps.  Helps
# for bootstrapping a new install.
# Jesse Keating 2009-01-29 (code from Ignacio Vazquez-Abrams)
%post -p <lua>
bashfound = false;
shfound = false;
 
f = io.open("/etc/shells", "r");
if f == nil
then
  f = io.open("/etc/shells", "w");
else
  repeat
    t = f:read();
    if t == "/bin/bash"
    then
      bashfound = true;
    end
    if t == "/bin/sh"
    then
      shfound = true;
    end
  until t == nil;
end
f:close()
 
f = io.open("/etc/shells", "a");
if not bashfound
then
  f:write("/bin/bash\n")
end
if not shfound
then
  f:write("/bin/sh\n")
end
f:close()

%postun
if [ "$1" = 0 ]; then
    /bin/grep -v '^/bin/bash$' < /etc/shells | \
      /bin/grep -v '^/bin/sh$' > /etc/shells.new
    /bin/mv /etc/shells.new /etc/shells
fi

%files -f %{name}.lang
%defattr(-,root,root)
%config(noreplace) /etc/skel/.b*
%{_bindir}/sh
%{_bindir}/bash
%{_bindir}/alias
%{_bindir}/bg
%{_bindir}/cd
%{_bindir}/command
%{_bindir}/fc
%{_bindir}/fg
%{_bindir}/getopts
%{_bindir}/jobs
%{_bindir}/read
%{_bindir}/umask
%{_bindir}/unalias
%{_bindir}/wait
%dir %{pkgdocdir}/
#%%doc %{pkgdocdir}/COPYING
%doc %{pkgdocdir}/CHANGES
%doc %{pkgdocdir}/COMPAT
%doc %{pkgdocdir}/NEWS
#%%doc %{pkgdocdir}/NOTES
%doc %{pkgdocdir}/POSIX
%doc %{pkgdocdir}/*.html
%doc %{pkgdocdir}/FAQ
%doc %{pkgdocdir}/INTRO
%doc %{pkgdocdir}/README
%doc %{pkgdocdir}/RBASH
%attr(0755,root,root) %{_bindir}/bashbug[-.]*
%{_bindir}/bashbug
%{_infodir}/bash.info*
%{_mandir}/*/*
%{_mandir}/*/..1*
%doc RBASH README
%doc doc/{FAQ,INTRO,README,bash{,ref}.html}
%{_datadir}/config-sample/bash/tab.dot.bashrc

#%%files doc # -f %{name}-doc.files
%files doc
%defattr(-, root, root)
%doc doc/*.ps doc/*.0 doc/*.html doc/article.txt

%changelog
* Tue Apr 08 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3-2m)
- update 4.3-008

* Sat Mar 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3-1m)
- update 4.3
- support UserMove env

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2-9m)
- apply upstream patches (030-045) (add %%patch030 - %%patch037)

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2-8m)
- add /usr/bin/bash, /usr/bin/sh. 
-- newer shell script use /usr/bin/bash

* Wed Sep 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2-7m)
- apply upstream patches (30-37)

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2-6m)
- apply upstream patches (21-29)

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2-5m)
- apply upstream patches (11-20)

* Sat May 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2-4m)
- apply upstream patches (009,010)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2-3m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2-2m)
- apply upstream patches (001-008)

* Mon Feb 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2-1m)
- update 4.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-9m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1-8m)
- update 4.1patch009

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1-7m)
- full rebuild for mo7 release

* Tue Jun 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1-6m)
- update 4.1patch007

* Sat May  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1-5m)
- re-packaging doc to avoid conflicting and correct %%doc of main package

* Wed May  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-4m)
- add Patch106: bash-2.05b-manso.patch
- drop Patch114: bash-cond-rmatch.patch

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-3m)
- use Requires

* Wed Apr 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1-2m)
- update 4.1patch005

* Thu Jan  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1-1m)
- update 4.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0-3m)
- update bash-4.0 patch33

* Wed Jul  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0-2m)
- Provides: /bin/sh and /bin/bash again

* Wed Jul  8 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0-1m)
- sync Fedora 11
- version up 4.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-12m)
- rebuild against rpm-4.6

* Mon Dec 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2-11m)
- add patch bash32-40 - 48
- import fedora patches

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-10m)
- add patch bash32-34 - bash32-39

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-9m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-8m)
- remove BPR libtermcap-devel

* Wed Feb 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-7m)
- add again Patch132, Patch133, Patch136 and Patch137 not guilty patch.
  if your bash is not load ~/.bashrc, you type sudo rpm -ev bash-completion
- update bashrc from setup-2.6.10-1.fc8
- fix TAB width

* Mon Feb 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-6m)
- rollback feature
-- .bash* was installed /etc/skel/
- unpatch any Fedora patches

* Mon Feb 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-5m)
- .bash* was installed /etc/skel/

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-4m)
- %%NoSource -> NoSource

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-3m)
- remove -fprofile-generate option...

* Wed Feb  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-2m)
- update bash-3.2-33
- import Fedora Patch(bash-3_2-20_fc9)
- libtermcap was obsolutes...

* Sun Feb 04 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-1m)
- merge from fc bash-3.2-4
- separate completion pacakge
- add bash32-001 .. bash32-009
- delete bash31-001 .. bash31-017
- update dot*bash* files
- delete inputrc
- delete unused sources
 - Patch105: bash-aq.patch
 - Patch106: bash-login.patch
 - Patch112: bash-ulimit.patch
 - Patch127: bash-sighandler.patch
 - Patch129: bash-read-memleak.patch
 - Patch1106: bash-brace.patch
 - Patch1107: bash-2.05a-shellfunc.patch
 - Patch1108: bash-3.1-ia64.patch
 - Patch1111: bash-2.05a-loadables.patch
 - Patch1117: bash-2.05b-mbinc.patch
 - Patch1150: bash-completion-configure.patch
 - Patch1151: bash-completion-tar2.patch
- add Patch130: bash-infotags.patch
- add Patch131: bash-cond-rmatch.patch
- update Patch107: bash-2.05a-interpreter.patch
- revive bashbug-*
-
- fc changelog is below
- * Tue Jan 23 2007 Tim Waugh <twaugh@redhat.com> 3.2-4
- - Slightly better .bash_logout (bug #223960).
-
- * Fri Jan 19 2007 Tim Waugh <twaugh@redhat.com> 3.2-3
- - Back out rmatch change introduced in 3.2 (bug #220087).
- 
- * Tue Jan 16 2007 Miroslav Lichvar <mlichvar@redhat.com> 3.2-2
- - Link with ncurses.
- 
- * Fri Dec 15 2006 Tim Waugh <twaugh@redhat.com> 3.2-1
- - Build requires autoconf and gettext.
- - 3.2.  No longer need aq, login, ulimit, sighandler or read-memleak
-   patches.

* Sun Jul 13 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-1m)
- update to 3.1
- delete /bin/bash2 from files and make symlink for bash2
- renumber patches <1-99> +100
- delete included Patch102: bash-fixes.patch
- delete included Patch104: bash-array.patch
- arrange Patch108: bash-3.1-ia64.patch
- delete included Patch109: bash-multilinePS1.patch
- delete Patch110: bash-changechar.patch
- delete Patch112: bash-2.05a-interpreter.patch
- delete included Patch113: bash-arrayvar.patch
- delete confilcts Patch114: bash-trap.patch
- delete included Patch116: bash-2.05b-utf8.patch
- delete included Patch118: bash-pipefail.patch
- delete Patch119: bash-multibyteifs.patch
- delete included Patch120: bash-history.patch
- delete included Patch121: bash-ulimit.patch
- delete Patch126: bash-2.05b-xcc.patch
- delete Patch128: bash-jobs.patch
- delete Patch142: bash-sigpipe.patch
- update Source3 to http://www.caliban.org/files/bash/bash-completion-20060301.tar.bz2
  and no NoSource
- update Patch151: bash-completion-tar2.patch for new bash-completion patch
- renumber Patch103 -> Patch102: bash-2.03-profile.patch
- add Patch103: bash-requires.patch from FC
  and delete Patch105: bash-2.05a-requires.patch
- add Patch105: bash-aq.patch from FC
- renumber Patch130: -> Patch116: bash-2.05b-manso.patch
- renumber Patch131: -> Patch117: bash-2.05b-debuginfo.patch
- renumber Patch127: -> Patch115: bash-2.05b-pgrp_sync.patch
- add Patch106: bash-login.patch
- update Patch107: bash-2.05a-interpreter.patch
- renumber Patch115: -> Patch108: bash-2.05b-readline-oom.patch
- update Patch112: bash-ulimit.patch
- update Patch114: bash-2.05b-xcc.patch
- add patches below from FC
  Patch118: bash-tty-tests.patch
  Patch126: bash-setlocale.patch
  Patch127: bash-sighandler.patch
  Patch129: bash-read-memleak.patch
- renumber againt +1000
  Patch106: bash-brace.patch
  Patch107: bash-2.05a-shellfunc.patch
  Patch108: bash-3.1-ia64.patch
  Patch111: bash-2.05a-loadables.patch
  Patch117: bash-2.05b-mbinc.patch
  Patch150: bash-completion-configure.patch
  Patch151: bash-completion-tar2.patch

* Tue Apr 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-4m)
- change bashrc for use bash_completion (bts#113)

* Wed Mar 16 2005 Toru Hoshina <t@momonga-linux.org>
- (3.0-3m)
- applied bash-jobs.patch from FC3.

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (3.0-2m)
- rebuild against libtermcap-2.0.8-38m.

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0-1m)
- version up

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.05b-21m)
- revised docdir permission

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org.
- (2.05b-20m)
- enable x86_64.
- run-jobs and run-trap is neber ending check on x86_64...

* Fri Oct 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.05b-19m)
- remove patch18.
- add BuildConflicts: glibc-devel = 2.3.3-(1|2)m.

* Wed Oct 20 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.05b-18m)
- add patch18 fot gcc 3.4??

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.05b-17m)
- update Patch50: bash-completion-configure.patch
- update Patch51: bash-completion-tar.patch

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.05b-16m)
- update bash_completion_ver to 20041017
- add --with-bash-malloc=no at configure
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Sat Jul 31 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.05b-15m)
- update Source14: tab.dot.bashrc

* Tue May 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.05b-14m)
- add patch 17 ([debian-devel:15370] bash initialization is wrong.)

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.05b-13m)
- revised spec for enabling rpm 4.2.

* Sat Dec 20 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.05b-12m)
- set prompt for screen in dot-bashrc
- update bash-completion to 20031215

* Thu Nov 06 2003 Kenta MURATA <muraken2@nifty.com>
- (2.05b-11m)
- add DISABLE_BASH_COMPLETION in %%{_sysconfdir}/bashrc.

* Fri Oct 31 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.05b-10m)
- renumber source
- rename dot files in config-sample dot-* => dot.*
- delete define prefix, why used this?
- update bashcompletion patch to 20031022

* Wed Aug 27 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.05b-9m)
- revise dot-inputrc

* Wed Aug 26 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.05b-8m)
- import official patches for 2.05b
- update bashcompletion patch to 20030821
- change my e-mail address from uomaster@nifty.com to zaki@zakky.org
- remove bash-2.05b-complete.patch. this bug is fixed by official patch

* Fri Mar 21 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.05b-7m)
- bash_completion: tar *x*f: include *.tar.bz2 *.tbz *.tb2 *.tbz2

* Sun Mar 16 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.05b-6m)
- autoconf before configure
- fix bashrc(by kaz)

* Sun Mar 16 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.05b-5m)
- source /etc/bash_completion from /etc/bashrc if interactive shell

* Sun Mar 16 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.05b-4m)
- update bash-completion to 20030227
- install bash-completion files to /etc (should be %%config?)

* Sat Nov 16 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (2.05b-3m)
- add bash-2.05b-avoid-loop.patch

* Fri Aug 02 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (2.05b-2m)
- add bash-2.05b-complete.patch
- this patch fixes a problem when do completion. 
  ex) '../[TAB]'

* Fri Jul 26 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (2.05b-1m)
- up to 2.05b
- sync with rawhide (2.05b-1)
- add bash-2.05b-strpbrk.patch for more i18n support.
- add BuildPrereq: libtermcap-devel

* Sun May 9 2002 Toru Hoshina <t@kondara.org>
- (2.0.5-34k)
- revised bashrc.

* Wed May  1 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.5-32k)
- add PreReq: libtermcap

* Tue Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.5-30k)
- Provides: /bin/bash

* Mon Apr 29 2002 Motonobu Ichimura <famao@kondara.org>
- (2.0.5-28k)
- update i18n patch (readline 0.4)

* Mon Apr 29 2002 Kenta MURATA <muraken@kondara.org>
- (2.0.5-26k)
- Provides: /bin/sh

* Sun Jan 20 2002 Motonobu Ichimura <famao@kondara.org>
- (2.0.5-24k)
- i18n patch (up to bash 0.5 readline 0.3)
- my patches included in it

* Wed Jan 16 2002 Motonobu Ichimura <famao@kondara.org>
- (2.0.5-22k)
- fix segfault when use Ctrl(t) with some locales

* Tue Dec 11 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.5-20k)
- modify i18n patch (TT)

* Mon Dec 10 2001 TABUCHI Takaaki <tab@kondara.org>
- (2.0.5-18k)
- add tab.dot.bashrc

* Mon Dec 10 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.5-16k)
- modify i18n-diff.patch

* Sun Dec 09 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.5-14k)
- re-enabled i18n patch
- added bash-i18n-diff.patch
- and re-added Source6 Source7 (still needed)

* Fri Nov 30 2001 Shingo Akagaki <dora@kondara.org>
- (2.0.5-12k)
- remove i18n patch

* Mon Nov 12 2001 Motonobu Ichimura <famao@kondara.org>
- (2.05-8k)
- add %dir %{_datadir}/config-sample/bash

* Thu Oct 25 2001 Toru Hoshina <t@kondara.org>
- (2.05-6k)
- dot files located properly.

* Thu Oct 25 2001 Motonobu Ichimura <famao@kondara.org>
- (2.05-4k)
- added IBM's I18n patches for bash-2.05
- '

* Mon Oct 22 2001 Toru Hoshina <t@kondara.org>
- (2.05-2k)
- version up. no more etcskel.

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Sun Aug 26 2001 Toru Hoshina <t@kondara.org>
- (2.04-28k)
- add kterm entry to /etc/bashrc...

* Mon May  7 2001 Akira Higuchi <a@kondara.org>
- (2.04-26k)
- removed IBM patches and re-enabled patch #6.

* Sun Mar 18 2001 Toru Hoshina <toru@df-usa.com>
- (2.04-24k)
- add IBM's I18n patches.
- turned off prereq grep.

* Sun Feb 18 2001 Kenichi Matsubara <m@kondara.org>
- (2.04-22k)
- backport 2.04-23k(Jirai).

* Sun Feb 18 2001 Kenichi Matsubara <m@kondara.org>
- (2.04-23k)
- Obsoletes bash2-doc.

* Thu Feb  1 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.04-21k)
- added '%post' (#854)

* Tue Jan 30 2001 Kenichi Matsubara <m@kondara.org>
- (2.04-19k)
- bugfix %files section.

* Fri Dec  1 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.04-17k)
- modified to use %{_mandir} and %{_infodir} macro

* Tue Nov  7 2000 Toru Hoshina <toru@df-usa.com>
- Obsolete bash and replace bas with bash2.

* Wed Oct 25 2000 Kenichi Matsubara <m@kondara.org>
- remove create man.pages file section.
- modified %files section.

* Tue Aug 22 2000 Kenichi Matsubara <m@kondara.org>
- Change PATH (FHS).
- Obsolete jman files.

* Mon Aug 14 2000 Akira Higuchi <a@kondara.org>
- some changes in readline patch

* Tue Aug 01 2000 Akira Higuchi <a@kondara.org>
- some changes in readline patch

* Sun May 07 2000 Fumihiko Takayama <tekezo@catv296.ne.jp>
- i18n lib/readline. 

* Sat May 06 2000 Fumihiko Takayama <tekezo@catv296.ne.jp>
- now temporary change, del jp-patch and configure --with-installed-readilne

* Fri May 05 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- up tp 2.04 
- added jp-patch 

* Mon Apr 03 2000 TABUCHI Takaaki <tab@kondara.org>
- merge redhat-6.2 (2.0.3-8).

* Tue Mar  7 2000 Tenkou N. Hattori <tnh@kondara.org>
- add Prereq: grep.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja.

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed
- fix description

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_bash-20000115

* Sun Jan 22 2000 TABUCHI Takaaki <tab@kondara.org>
- be able to rebuild non-root user.

* Thu Dec  2 1999 Ken Estes <kestes@staff.mail.com>
- updated patch to detect what executables are required by a script.

* Wed Nov 24 1999 Tenkou N. Hattori <tnh@kondara.org>
- add man-pages-ja-GNU_bash-19991115

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Sep 14 1999 Dale Lovelace <dale@redhat.com>
- Remove annoying ^H's from documentation

* Fri Jul 16 1999 Ken Estes <kestes@staff.mail.com>
- patch to detect what executables are required by a script.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Fri Mar 19 1999 Jeff Johnson <jbj@redhat.com>
- strip binaries.
- include bash-doc correctly.

* Thu Mar 18 1999 Preston Brown <pbrown@redhat.com>
- fixed post/postun /etc/shells work.

* Thu Mar 18 1999 Cristian Gafton <gafton@redhat.com>
- updated again text in the spec file

* Mon Feb 22 1999 Jeff Johnson <jbj@redhat.com>
- updated text in spec file.
- update to 2.03.

* Fri Feb 12 1999 Cristian Gafton <gafton@redhat.com>
- build it as bash2 instead of bash

* Tue Feb  9 1999 Bill Nottingham <notting@redhat.com>
- set 'NON_INTERACTIVE_LOGIN_SHELLS' so profile gets read

* Thu Jan 14 1999 Jeff Johnson <jbj@redhat.com>
- rename man pages in bash-doc to avoid packaging conflicts (#606).

* Wed Dec 02 1998 Cristian Gafton <gafton@redhat.com>
- patch for the arm
- use $RPM_ARCH-redhat-linux as the build target

* Tue Oct  6 1998 Bill Nottingham <notting@redhat.com>
- rewrite %pre, axe %postun (to avoid prereq loops)

* Wed Aug 19 1998 Jeff Johnson <jbj@redhat.com>
- resurrect for RH 6.0.

* Sun Jul 26 1998 Jeff Johnson <jbj@redhat.com>
- update to 2.02.1

* Thu Jun 11 1998 Jeff Johnson <jbj@redhat.com>
- Package for 5.2.

* Mon Apr 20 1998 Ian Macdonald <ianmacd@xs4all.nl>
- added POSIX.NOTES doc file
- some extraneous doc files removed
- minor .spec file changes

* Sun Apr 19 1998 Ian Macdonald <ianmacd@xs4all.nl>
- upgraded to version 2.02
- Alpha, MIPS & Sparc patches removed due to lack of test platforms
- glibc & signal patches no longer required
- added documentation subpackage (doc)

* Fri Nov 07 1997 Donnie Barnes <djb@redhat.com>
- added signal handling patch from Dean Gaudet <dgaudet@arctic.org> that
  is based on a change made in bash 2.0.  Should fix some early exit
  problems with suspends and fg.

* Mon Oct 20 1997 Donnie Barnes <djb@redhat.com>
- added %clean

* Mon Oct 20 1997 Erik Troan <ewt@redhat.com>
- added comment explaining why install-info isn't used
- added mips patch 

* Fri Oct 17 1997 Donnie Barnes <djb@redhat.com>
- added BuildRoot

* Tue Jun 03 1997 Erik Troan <ewt@redhat.com>
- built against glibc
