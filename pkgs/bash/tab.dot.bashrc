# .bashrc

# User specific aliases and functions

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

export PATH=/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin:/usr/sbin:/sbin:${HOME}/bin:$PATH

alias a=alias
alias j='jobs -l'
alias ncftplsx="ncftpls -x'-ltr'"
alias google='LANG=ja w3m http://www.google.net/'
alias g='LANG=ja w3m http://www.google.net/'
alias w3m='LANG=ja w3m'
alias wgetm='wget -m -np'
alias d='display'

alias mew='emacs -nw -f mew'
alias navi2ch='emacs -nw -f navi2ch'

export CVS_RSH=ssh

export CCACHE_DIR=${HOME}/.ccache
#default
#
# comment out for speedup: export CCACHE_LOGFILE=%{HOME}/.ccache/log

export CACHECC1_DIR=${HOME}/.cachecc1

export IGNOREEOF=100

export repo='http://svn.momonga-linux.org/svn/pkgs'
export SVN_EDITOR=jed
