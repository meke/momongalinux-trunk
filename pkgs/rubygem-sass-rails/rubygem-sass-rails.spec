# Generated from sass-rails-3.2.5.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname sass-rails

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Sass adapter for the Rails asset pipeline
Name: rubygem-%{gemname}
Version: 3.2.5
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubygems.org/gems/sass
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(sass) >= 3.1.10
Requires: rubygem(railties) => 3.2.0
Requires: rubygem(railties) < 3.3
Requires: rubygem(tilt) => 1.3
Requires: rubygem(tilt) < 2
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Sass adapter for the Rails asset pipeline.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.5-1m)
- update 3.2.5

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.3-1m)
- update 3.2.3

* Sat Dec 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-1m)
- update 3.2.2

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.4-1m)
- Initial commit Momonga Linux

