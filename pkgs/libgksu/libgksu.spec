%global momorel 13

Summary: Gtk+ frontend to su and sudo (library)
Name: libgksu

Version: 2.0.7
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Libraries
URL: http://www.nongnu.org/gksu/

Source0: http://people.debian.org/~kov/gksu/%{name}-%{version}.tar.gz
NoSource: 0
Source1: ja.po
Patch0: libgksu-2.0.5-ja.patch
Patch1: libgksu-2.0.5-schemas.patch
Patch2: libgksu-2.0.6-desktop.patch
Patch3: libgksu-2.0.7-gtk-doc-1.11.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: GConf2
BuildRequires: automake >= 1.11.2-4m
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: gtk2-devel
BuildRequires: GConf2-devel
BuildRequires: startup-notification-devel
BuildRequires: gcr-devel
BuildRequires: libgtop2-devel
BuildRequires: libglade2-devel
Requires(post): GConf2

%description
GKSu is a library that provides a Gtk+ frontend to su and sudo. It
supports login shells and preserving environment when acting as a su
frontend. It is useful to menu items or other graphical programs that
need to ask a user's password to run another program as another user.
#'

%package devel
Summary: libgksu-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
gksu-devel

%prep
%setup -q

## fix x86_64 problem
%{__sed} -i 's:\(gchar auxcommand\[\] = PREFIX "/\)lib\(/" PACKAGE "/gksu-run-helper";\):\1%{_lib}\2:' libgksu/libgksu.c

cp %{SOURCE1} po
%patch0 -p1 -b .ja
%patch1 -p1 -b .schemas
%patch2 -p1 -b .desktop
%patch3 -p1 -b .gtk-doc

%build
touch README NEWS
intltoolize --force --copy
gtkdocize --copy --docdir docs
autoreconf -vfi
%configure \
    --disable-schemas-install \
    --enable-gtk-doc \
    LIBS="-lX11"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
for S in gksu.schemas
do
    gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/$S > /dev/null
done

%postun
/sbin/ldconfig

%files
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog
%{_sysconfdir}/gconf/schemas/gksu.schemas
%{_bindir}/gksu-properties
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/gksu-run-helper
%{_libdir}/libgksu2.so.*
%exclude %{_libdir}/libgksu2.la
%{_datadir}/applications/gksu-properties.desktop
%{_datadir}/%{name}
%{_datadir}/locale/*/*/*
%{_datadir}/pixmaps/gksu.png

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}
%{_libdir}/libgksu2.so
%{_libdir}/libgksu2.a
%{_libdir}/pkgconfig/libgksu2.pc
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-13m)
- use gcr-devel instaed of gnome-keyring-devel

* Fri Jan 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-12m)
- fix x86_64 problem
-- see http://d.hatena.ne.jp/kakurasan/20081224/p1 for more details

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-11m)
- BuildRequires: automake >= 1.11.2-4m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.7-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.7-8m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.7-7m)
- fix build

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-6m)
- use BuildRequires and Requires

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.7-5m)
- --enable-gtk-doc

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.7-4m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-2m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.6-2m)
- fix desktop

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.6-1m)
- update to 2.0.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.5-4m)
- rebuild against gcc43

* Sun Dec 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.5-3m)
- add patch1 (default to no display-no-pass-info)
-- clash window

* Fri Dec 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.5-2m)
- add ja.po (to be continued)

* Wed Dec 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.5-1m)
- initial build

