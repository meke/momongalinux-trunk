%global momorel 1

Summary: The Automated Text and Program Generation Tool
Name: autogen
Version: 5.17.4
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Development/Tools
Source0: http://ftp.gnu.org/gnu/%{name}/rel%{version}/%{name}-%{version}.tar.xz
NoSource: 0
URL: http://autogen.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: guile-devel >= 2.0.9
BuildRequires: libxml2-devel >= 2.6.16
Requires(post): info
Requires(preun): info

%description
AutoGen is a tool designed to simplify the creation and maintenance of
programs that contain large amounts of repetitious text. It is especially
valuable in programs that have several blocks of text that must be kept
synchronized.

%package devel
Summary: The Automated Text and Program Generation Tool
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The autogen-devel package contains the header files, static libraries,
and developer docs for AutoGen.

%prep
%setup -q 

%build
%configure
%undefine _smp_mflags
make -k

%install
rm -rf --preserve-root %{buildroot}
%makeinstall transform='s,x,x,'
rm -f %{buildroot}%{_infodir}/dir

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
if [ "$1" = 1 ]; then
    /sbin/install-info %{_infodir}/autogen.info %{_infodir}/dir
fi

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/autogen.info %{_infodir}/dir
fi

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README THANKS TODO VERSION
%{_bindir}/autogen
%{_bindir}/columns
%{_bindir}/getdefs
%{_bindir}/xml2ag
%{_libdir}/libopts.so.*
%{_datadir}/autogen
%{_infodir}/autogen.info*

%files devel
%defattr(-,root,root,-)
%{_bindir}/autoopts-config
%{_includedir}/autoopts
%{_libdir}/libopts.a
%{_libdir}/libopts.so
%{_datadir}/aclocal/autoopts.m4
%{_datadir}/pkgconfig/autoopts.pc
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.17.4-1m)
- update to 5.17.4

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.16.2-2m)
- rebuild against guile-2.0.9

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.16.2-1m)
- update to 5.16.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.8-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.9.8-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.9.8-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9.8-1m)
- update 5.9.8

* Thu May 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.9.7-1m)
- update 5.9.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.9.4-2m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5.9.4-1m)
- version up 5.9.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.9-2m)
- rebuild against gcc43

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.9-1m)
- version 5.9
- and build against guile-1.8.1
- move _datadir/autogen and _infodir/* from autogen-devel to autogen

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.7.3-3m)
- delete libtool library

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.7.3-2m)
- delete duplicated dir

* Mon Oct 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (5.7.3-1m)
- up to 5.7.3

* Sun Jul 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.7.1-2m)
- nigirisugi
- install info 

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.7.1-1m)
- up to 5.7.1

* Tue May 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.7-1m)
- initial import to Momonga

