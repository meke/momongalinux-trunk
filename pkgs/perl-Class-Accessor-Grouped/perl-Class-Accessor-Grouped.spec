%global         momorel 5

Name:           perl-Class-Accessor-Grouped
Version:        0.10010
Release:        %{momorel}m%{?dist}
Summary:        Lets you build groups of accessors
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Class-Accessor-Grouped/
Source0:        http://www.cpan.org/authors/id/R/RI/RIBASUSHI/Class-Accessor-Grouped-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.1
BuildRequires:  perl-Class-Inspector
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MRO-Compat
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Sub-Name >= 0.04
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Class-XSAccessor
Requires:       perl-Class-Inspector
Requires:       perl-MRO-Compat
Requires:       perl-Scalar-Util
Requires:       perl-Sub-Name >= 0.04
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This class lets you build groups of accessors that will call different
getters and setters.

%prep
%setup -q -n Class-Accessor-Grouped-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Class/Accessor/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10010-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10010-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10010-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10010-2m)
- rebuild against perl-5.18.0

* Wed Apr 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10010-1m)
- update to 0.10010

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10009-2m)
- rebuild against perl-5.16.3

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10009-1m)
- update to 0.10009

* Thu Nov 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10008-1m)
- update to 0.10008

* Thu Nov  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10007-1m)
- update to 0.10007

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10006-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10006-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10006-2m)
- rebuild against perl-5.16.0

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10006-1m)
- update to 0.10006

* Tue Dec 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10005-1m)
- update to 0.10005

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10004-1m)
- update to 0.10004

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10003-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10003-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10003-2m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10003-1m)
- update to 0.10003

* Mon Apr 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10002-3m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10002-2m)
- rebuild for new GCC 4.6

* Sun Dec 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10002-1m)
- update to 0.10002

* Sun Dec 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10001-1m)
- update to 0.10001

* Sun Nov 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10000-1m)
- update to 0.10000

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09009-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09009-1m)
- update to 0.09009

* Mon Oct 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09008-1m)
- update to 0.09008

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09007-1m)
- update to 0.09007

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09006-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09006-1m)
- updateto 0.09006

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.09003-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-2m)
- rebuild against perl-5.12.1

* Sat Apr 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09003-1m)
- update to 0.09003

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09002-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09002-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09002-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09002-1m)
- update to 0.09002

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09001-1m)
- update to 0.09001

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09000-1m)
- update to 0.09000

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08003-2m)
- rebuild against perl-5.10.1

* Sun Mar 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08003-1m)
- update to 0.08003

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08002-2m)
- rebuild against rpm-4.6

* Fri Nov 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08002-1m)
- update to 0.08002

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.08001-2m)
- rebuild against gcc43

* Fri Jan 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-1m)
- update to 0.08001

* Wed Jan  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08000-1m)
- update to 0.08000

* Sat Jul 07 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07000-1m)
- Specfile autogenerated by cpanspec 1.70 for Momonga Linux.
