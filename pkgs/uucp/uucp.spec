%global momorel 8

%global	_newconfigdir	%{_sysconfdir}/uucp
%global	_oldconfigdir	%{_sysconfdir}/uucp/oldconfig
%global	_varlogdir	%{_localstatedir}/log/uucp
%global	_varlockdir	%{_localstatedir}/lock/uucp
%global	_varspooldir	%{_localstatedir}/spool

Summary: The uucp utility for copying files between systems.
Name: uucp
Version: 1.07
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Communications
Source0: ftp://ftp.gnu.org/gnu/uucp/%{name}-%{version}.tar.gz
NoSource: 0
Source1: uucp.log
Patch0: uucp-1.07-config.patch
Patch3: uucp-1.07-sigfpe.patch
Patch6: uucp-1.07-baudboy.patch
Patch8: uucp-1.06.1-pipe.patch

BuildRequires: autoconf213
BuildRequires: tetex
BuildRequires: lockdev-devel >= 1.0.0-2m
Requires: lockdev >= 1.0.1-2m
Requires(post): coreutils, info
Requires(preun): info

URL: http://www.airs.com/ian/uucp.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The uucp command copies files between systems. Uucp is primarily used
by remote machines downloading and uploading email and news files to
local machines.

Install the uucp package if you need to use uucp to transfer files
between machines.

%prep
%setup -q
%patch0 -p1 -b .config
%patch3 -p1 -b .sigfpe
%patch6 -p1 -b .baudboy
%patch8 -p1 -b .pipe

#autoconf-2.13
%build
%configure --with-newconfigdir=%{_newconfigdir} --with-oldconfigdir=%{_oldconfigdir}
make all

%install
rm -rf %{buildroot}

%makeinstall install-info

mkdir -p %{buildroot}%{_varlogdir}
mkdir -p %{buildroot}%{_varlockdir}

mkdir -p %{buildroot}%{_varspooldir}/uucp
mkdir -p %{buildroot}%{_varspooldir}/uucppublic
mkdir -p %{buildroot}%{_oldconfigdir}

mkdir -p %{buildroot}%{_prefix}/%{_lib}/uucp
ln -sf ../../sbin/uucico %{buildroot}%{_prefix}/%{_lib}/uucp/uucico

mkdir -p %{buildroot}/etc/logrotate.d
install -m 644 $RPM_SOURCE_DIR/uucp.log %{buildroot}/etc/logrotate.d/uucp

# Create ghost files
for n in Log Stats Debug; do
    touch %{buildroot}%{_varlogdir}/$n
done

# the following is kind of gross, but it is effective
for i in dial passwd port dialcode sys call ; do
cat > %{buildroot}%{_newconfigdir}/$i <<EOF 
# This is an example of a $i file. This file is syntax compatible
# with Taylor UUCP (not HDB, not anything else). Please check uucp
# documentation if you are not sure how to configure Taylor UUCP config files.
# Edit the file as appropriate for your system, there are sample files
# in %{_datadir}/doc/uucp-%{version}/samples.

# Everything after a '#' character is a comment.
EOF
done

rm -f %{buildroot}%{_infodir}/dir

# some more documentation
texi2html -monolithic uucp.texi

chmod -x contrib/*

%clean
rm -rf %{buildroot}

%post
# Create initial log files so that logrotate doesn't complain
for n in Log Stats Debug; do
	[ -f %{_varlogdir}/$n ] || touch %{_varlogdir}/$n
	chown uucp:uucp /var/log/uucp/$n
done
chmod 644 %{_varlogdir}/Log %{_varlogdir}/Stats
chmod 600 %{_varlogdir}/Debug

/sbin/install-info %{_infodir}/uucp.info %{_infodir}/dir

%preun
/sbin/install-info --del %{_infodir}/uucp.info %{_infodir}/dir

%files
%defattr(-,root,root)
%doc README COPYING ChangeLog NEWS TODO
%doc sample contrib uucp.html

%attr(6555,uucp,uucp)		%{_bindir}/cu
%attr(4555,uucp,uucp)		%{_bindir}/uucp
%attr(0755,root,root)		%{_bindir}/uulog
%attr(6555,uucp,uucp)		%{_bindir}/uuname
%attr(0755,root,root)		%{_bindir}/uupick
%attr(4555,uucp,uucp)		%{_bindir}/uustat
%attr(0755,root,root)		%{_bindir}/uuto
%attr(4555,uucp,uucp)		%{_bindir}/uux

%attr(6555,uucp,uucp)		%{_sbindir}/uucico
%attr(6555,uucp,uucp)		%{_sbindir}/uuxqt
%attr(0755,uucp,uucp)		%{_sbindir}/uuchk
%attr(0755,uucp,uucp)		%{_sbindir}/uuconv
%attr(0755,root,root)		%{_sbindir}/uusched

%attr(755,uucp,uucp)	%dir	%{_prefix}/%{_lib}/uucp
%{_prefix}/%{_lib}/uucp/uucico

%{_mandir}/man[18]/*
%{_infodir}/uucp.info*

%attr(0755,uucp,uucp)	%dir	%{_varlogdir}
%attr(0644,uucp,uucp)	%ghost	%{_varlogdir}/Log
%attr(0644,uucp,uucp)	%ghost	%{_varlogdir}/Stats
%attr(0600,uucp,uucp)	%ghost	%{_varlogdir}/Debug
%attr(755,uucp,uucp)	%dir	%{_varlockdir}
%attr(755,uucp,uucp)	%dir	%{_varspooldir}/uucp
%attr(755,uucp,uucp)	%dir	%{_varspooldir}/uucppublic

%config(noreplace)	/etc/logrotate.d/uucp

%dir	%{_newconfigdir}
%dir	%{_oldconfigdir}
%attr(0640,root,uucp)	%config(noreplace)	%{_newconfigdir}/call
%config(noreplace)	%{_newconfigdir}/dial
%config(noreplace)	%{_newconfigdir}/dialcode
%attr(0640,root,uucp)	%config(noreplace)	%{_newconfigdir}/passwd
%config(noreplace)	%{_newconfigdir}/port
%config(noreplace)	%{_newconfigdir}/sys

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.07-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.07-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.07-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.07-5m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.07-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.07-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.07-2m)
- rebuild against gcc43

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.06.2-10m)
- enable x86_64.

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.06.2-9m)
- revised spec for rpm 4.2.

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.06.2-8m)
- revise %%post and %%preun

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.06.2-7m)
- add URL

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.06.2-6k)
- Prereq: /sbin/install-info -> info

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (1.06.2-4k)
- add BuildPreReq: autoconf >= 2.52-8k

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.06.2-2k)
- update to 1.06.2

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (1.06.1-46k)
- autoconf 1.5

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (1.06.1-44k)
- No docs could be excutable :-p

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.06.1-42k)
- nigittenu

* Mon Sep 24 2001 Toru Hoshina <t@konddara.org>
- (1.06.1-40k)
- the confdir should be /etc/uucp.
- the log files should be owned by uucp.

* Wed Apr 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- fix oldconfigdir and newconfigdir

* Sun Jan 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.06.1-35k)
- added uucp for xinetd

* Fri Jan 12 2001 Kenichi Matsubara <m@kondara.org>
- (1.06.1-33k)
- BuildPreReq tag texi2html to tetex.

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.06.1-31k)
- modified uucp.fhs.patch and spec file for compatibility

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.06.1-23).
- add -q at %setup.

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-GNU_uucp-20000115

* Sun Feb 13 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- change perms to be root:root for the config files and
  0640,root:uucp for config files containing passwords
- add patch from #6151 (division by zero, SIGFPE)
- make %post work also for simple sh-versions

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Sat Jan 08 2000 Shingo Akagaki <dora@kondara.org>
- add panpi.patch
- add %defattr 

* Thu Nov 25 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_uucp-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Aug 23 1999 Jeff Johnson <jbj@redhat.com>
- add notifempty/missingok to logrotate config file (#4138).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 19)

* Tue Dec 22 1998 Bill Nottingham <notting@redhat.com>
- expunge /usr/local/bin/perl reference in docs

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat Apr 11 1998 Cristian Gafton <gafton@redhat.com>
- manhattan rebuild
- added sample config files in /etc/uucp

* Sun Oct 19 1997 Erik Troan <ewt@redhat.com>
- spec file cleanups
- added install-info support
- uses a build root

* Fri Oct 10 1997 Erik Troan <ewt@redhat.com>
- patched uureroute to find perl in /usr/bin instead of /usr/local/bin
- made log files ghosts

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Apr 22 1997 Erik Troan <ewt@redhat.com>
- Brian Candler fixed /usr/lib/uucp/uucico symlink
- Added "create" entries to log file rotation configuration
- Touch log files on install if they don't already exist to allow proper
  rotation

* Tue Mar 25 1997 Erik Troan <ewt@redhat.com>
- symlinked /usr/sbin/uucico into /usr/lib/uucp
- (all of these changes are from Brian Candler <B.Candler@pobox.com>)
- sgid bit added on uucico so it can create lock files
- log files moved to /var/log/uucp/ owned by uucp (so uucico can create them)
- log rotation added
- uses /etc/uucp/oldconfig instead of /usr/lib/uucp for old config files
- package creates /etc/uucp and /etc/uucp/oldconfig directories
- man pages reference the correct locations for spool and config files

