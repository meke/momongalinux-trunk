%global momorel 1
Name:           gnome-contacts
Version:        3.6.2
Release: %{momorel}m%{?dist}
Summary:        Contacts manager for GNOME
Group: 		Applications/Internet
License:        GPLv2+
URL:            https://live.gnome.org/ThreePointOne/Features/Contacts
#VCS: http://git.gnome.org/browse/gnome-contacts/
Source0:        http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  folks-devel >= 0.7.3
BuildRequires:  gtk3-devel
BuildRequires:  vala-devel >= 0.17
BuildRequires:  intltool
BuildRequires:  libnotify-devel
BuildRequires:  gnome-desktop3-devel >= 3.5.90
BuildRequires:  desktop-file-utils
BuildRequires:  cheese-libs-devel >= 3.6.0

%description
%{name} is a standalone contacts manager for GNOME desktop.

%prep
%setup -q

%build
%configure
%make 

%install
make install DESTDIR=%{buildroot}
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop
%find_lang %{name}

%postun
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%files -f %{name}.lang
%doc AUTHORS COPYING README NEWS ChangeLog
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.Contacts.gschema.xml
%{_libexecdir}/gnome-contacts-search-provider
%{_datadir}/dbus-1/services/org.gnome.Contacts.SearchProvider.service
%{_datadir}/gnome-shell/search-providers/gnome-contacts-search-provider.ini

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4.1-1m)
- update to 3.5.4.1

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- reimport from fedora

* Fri Oct 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0.1-1m)
- update to 3.2.0.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5.2-1m)
- add ja.po from http://l10n.gnome.org/vertimus/gnome-contacts/master/po/ja

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5.2-1m)
- update to 0.1.5.2

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5-1m)
- update to 0.1.5

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.4.1-3m)
- rebuild against gnome-desktop-3.1

* Tue Sep 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4.1-2m)
- fix BuildRequires

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.4.1-1m)
- initial build

