%global momorel 10

Summary: EPWING CD-ROM dictionary viewer
Name: ebview
Version: 0.3.6.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Text
#Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
#NoSource: 0
# mhatta unofficial release
# http://www.mhatta.org/diary/?date=20090819#p01
Source0: http://ftp.debian.org/debian/pool/main/e/ebview/ebview_0.3.6.2.orig.tar.gz
Source1: %{name}.desktop
Source2: %{name}.png
Patch0: ebview-0.3.6_pango_with_cairo.patch
URL: http://ebview.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automake16
BuildRequires: coreutils
BuildRequires: gtk2-devel
BuildRequires: eb-devel >= 4.4.3
BuildRequires: expat-devel >= 2.0.0
BuildRequires: libtool

%description
EBView is an EPWING CD-ROM dictionary viewer. It has several search
methods, handling multiple dictionaries. In addition, X selection will
be automatically used as keyword. This is useful when reading foreign
Web pages.

%prep
%setup -q
%patch0 -p0 -b .pango

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

%build
autoreconf -fi
%configure --with-eb-conf=/etc/eb.conf
%make

%install
rm -rf %{buildroot}

%makeinstall

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/pixmaps/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_datadir}/ebview
%{_datadir}/applications/ebview.desktop
%{_datadir}/locale/*/LC_MESSAGES/ebview.mo
%{_datadir}/pixmaps/ebview.png

%changelog
* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6.2-10m)
- fix Require

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6.2-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6.2-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6.2-7m)
- full rebuild for mo7 release

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6.2-6m)
- rebuild against eb-4.4.3

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6.2-5m)
- rebuild against eb-4.4.2

* Wed Dec 23 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.6.2-4m)
- add autoreconf, for libtool 2.2.6b

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6.2-2m)
- rebuild against eb-4.4.1-3m

* Wed Aug 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.6.2-1m)
- update 0.3.6.2(mhatta's Unofficial Release)
-- http://www.mhatta.org/diary/?date=20090819#p01

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-17m)
- apply patch (Patch3) for pango from Gentoo

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-16m)
- fix build on x86_64

* Sat Mar 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-15m)
- rebuild against eb-4.4.1

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-14m)
- remove "Application" from "Categories" of ebview.desktop

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-13m)
- rebuild against rpm-4.6

* Thu Jul 10 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3.6-12m)
- import ebview-0.3.6-64bits-from-suse.patch from FC

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.6-11m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6-10m)
- %%NoSource -> NoSource

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-9m)
- rebuild against eb-4.3.2

* Sun Jan 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-8m)
- rebuild against eb-4.3.1
- License: GPLv2

* Wed Aug 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-7m)
- rebuild against expat-2.0.0

* Tue Mar 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.6-6m)
- build with -O0 on x86_64

* Sun Nov  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-5m)
- update ebview.desktop

* Wed Jun  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.6-4m)
- rebuild against eb-4.2

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.6-3m)
- rebuild against eb-4.1.3

* Sat Dec  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.6-2m)
- rebuild against eb-4.1.2

* Sun Oct 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-1m)
- update to 0.3.6
- update Patch1: ebview-gtk2.4-DEPRECATED.patch
- add desktop file

* Sat Jun 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.5-2m)
- rebuild against eb-4.1

* Wed Apr 21 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.5-1m)
- version up.
- applied gtk2.4-DEPRECATED.patch. The patch is temporally and tentative.

* Thu Feb 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.3-1m)

* Wed Jan  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2-2m)
- revise for eb-4.0

* Mon Jan  5 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.3.2-1m)
- update to 0.3.2

* Sat Dec 20 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Mon Oct 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.0-2m)
- rebuild against eb-4.0

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.0-1m)

* Sun Mar  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.5-7m)
- rebuild against eb-3.3.1

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.5-6m)
- rebuild against eb-3.3

* Mon Jul 15 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.1.5-5m)
- Require: eb >= 3.2.2-2k
- BuildPreReq: eb-devel >= 3.2.2-2k

* Tue May 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.1.5-4k)
- add missing files (report from zoe@kasumi.sakura.ne.jp>

* Mon May 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.1.4-2k)
- ver up.

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.1.4-4k)
- rebuild against libpng-1.2.2

* Sat Mar 16 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.1.4-2k)

* Thu Feb 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.1.3-2k)
