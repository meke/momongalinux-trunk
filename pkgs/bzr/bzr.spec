%global momorel 1

# All package versioning is found here:
# the actual version is composed from these below, including leading 0 for release candidates
#   bzrmajor:  main bzr version
#   Version: bzr version, add subrelease version here
#   bzrrc: release candidate version, if any, line starts with % for rc, # for stable releas (no %).
#   release: rpm subrelease (0.N for rc candidates, N for stable releases)
%global bzrmajor 2.5
%global bzrminor .1
#global bzrrc b5
%global release 1

# Magics to get the dots in Release string correct per the above
%global subrelease %{?bzrrc:.}%{?bzrrc}

Name:           bzr
Version:        2.5.1
%global majorver 2.5
Release:	%{momorel}m%{?dist}
Summary:        Friendly distributed version control system

Group:          Development/Tools
License:        GPLv2+
URL:            http://www.bazaar-vcs.org/
Source0:        http://launchpad.net/%{name}/%{bzrmajor}/%{version}%{?bzrrc}/+download/%{name}-%{version}%{?bzrrc}.tar.gz
NoSource:	0
Source1:        https://launchpad.net/%{name}/%{bzrmajor}/%{version}%{?bzrrc}/+download/%{name}-%{version}%{?bzrrc}.tar.gz.sig
NoSource:       1
Source2:        bzr-icon-64.png
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel zlib-devel
# For building documents
BuildRequires:  python-sphinx
# We're using an old version of Pyrex, use the pregenerated C files instead
# of rebuilding
#BuildRequires: Pyrex
Requires:   python-paramiko
# Workaround Bug #230223 otherwise this would be a soft dependency
Requires:   python-pycurl

%description
Bazaar is a distributed revision control system that is powerful, friendly,
and scalable.  It is the successor of Baz-1.x which, in turn, was
a user-friendly reimplementation of GNU Arch.

%package doc
Summary:        Documentation for Bazaar
Group:          Documentation
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}

%description doc
This package contains the documentation for the Bazaar version control system.

%prep
%setup -q -n %{name}-%{version}%{?bzrrc}


sed -i '1{/#![[:space:]]*\/usr\/bin\/\(python\|env\)/d}' bzrlib/_patiencediff_py.py
sed -i '1{/#![[:space:]]*\/usr\/bin\/\(python\|env\)/d}' bzrlib/weave.py

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

chmod a-x contrib/bash/bzrbashprompt.sh

# Build documents
make docs-sphinx

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --install-data %{_datadir} --root $RPM_BUILD_ROOT
chmod -R a+rX contrib
chmod 0644 contrib/debian/init.d
chmod 0644 contrib/bzr_ssh_path_limiter
chmod 0644 contrib/bzr_access
chmod 0755 $RPM_BUILD_ROOT%{python_sitearch}/bzrlib/*.so

install -d $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d/
install -m 0644 contrib/bash/bzr $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d/
rm contrib/bash/bzr

# This is included in %doc, remove redundancy here
#rm -rf $RPM_BUILD_ROOT%{python_sitearch}/bzrlib/doc/

# Use independently packaged python-elementtree instead
rm -rf $RPM_BUILD_ROOT%{python_sitearch}/bzrlib/util/elementtree/

# Install documents
install -d $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}-doc-%{version}/
install -d $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}-doc-%{version}/pdf
cd doc
for dir in *; do
    if [ -d $dir/_build/html ]; then
        cp -R $dir/_build/html $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}-doc-%{version}/$dir
        rm -f $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}-doc-%{version}/$dir/.buildinfo 
        rm -f $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}-doc-%{version}/$dir/_static/$dir/Makefile
        find $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}-doc-%{version}/$dir -name '*.pdf' | while read pdf; do
            ln $pdf $RPM_BUILD_ROOT%{_defaultdocdir}/%{name}-doc-%{version}/pdf/
        done
    fi
done
cd ..

install -d $RPM_BUILD_ROOT%{_datadir}/pixmaps
install -m 0644 %{SOURCE2} $RPM_BUILD_ROOT%{_datadir}/pixmaps/bzr.png

%find_lang %{name}

%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc NEWS README TODO COPYING.txt contrib/
%{_bindir}/bzr
%{_mandir}/man1/*
%{python_sitearch}/bzrlib/
%{_sysconfdir}/bash_completion.d/*
%{_datadir}/pixmaps/bzr.png
%{python_sitearch}/*.egg-info

%files doc
%defattr(-,root,root,-)
%doc %{_defaultdocdir}/%{name}-doc-%{version}/*

%changelog
* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.1-1m)
- update to 2.5.1

* Wed Apr 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.0-1m)
- update to 2.5.0

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.2-1m)
- update to 2.4.2

* Sat Oct 29 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.1-2m)
- modify spec

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-1m)
- update to 2.4.1

* Mon Sep 19 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.0-1m)
- update 2.4.0
- sync Fedora

* Sun Jun  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.3-1m)
- update to 2.3.3

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-3m)
- rebuild against python-2.7rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-2m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-1m)
- update to 2.3.1

* Tue Feb  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-1m)
- update to 2.3.0

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.3-1m)
- update to 2.2.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.2-1m)
- update to 2.2.2

* Fri Nov  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.1-1m)
- update to 2.2.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.0-1m)
- update to 2.2.0

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.2-1m)
- update to 2.1.2

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sat Jan  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.1-1m)
- Update to 2.0.1

* Mon Sep 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-1m)
- Update to 2.0.0

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.18-1m)
- update to 1.18

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.16.1-1m)
- update to 1.16.1

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Sat May 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.15-1m)
- update 1.15

* Fri May 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.15-0.1.1m)
- update 1.15-rc1

* Tue Mar 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13-0.1.1m)
- update

* Mon Feb 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-1m)
- update

* Fri Feb 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-2m)
- bzr is arch dependent

* Fri Feb  5 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-3m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10-2m)
- Obsoletes: bazaar

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.10-1m)
- update to 1.10
- rebuild against python-2.6.1-1m

* Fri May 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-1m)
- update

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3-3m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-2m)
- rebuild against gcc43

* Wed Mar 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-1m)
- update

* Mon Feb 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-3m)
- comment out %%{py_sitescriptdir}/bzr-1.1.0-py2.5.egg-info

* Thu Feb  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-2m)
- modify spec file for x86_64

* Mon Feb  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-1m)
- update

* Mon Feb  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1m)
- update

* Wed Jan 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.9-1m)
- import to Momonga from pld
