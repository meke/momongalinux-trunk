%global momorel 5

Summary:        Tool to translate x86-64 CPU Machine Check Exception data
Name:           mcelog
Version:        0.9pre1
Release:        %{momorel}m%{?dist}
Epoch:		1
Group:          System Environment/Base
License:        GPLv2
Source0:        %{name}-%{version}.tar.gz
Patch0:		mcelog-0.9-pre1-record-length.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch:	x86_64

%description
mcelog is a daemon that collects and decodes Machine Check Exception data
on x86-64 machines.
ftp://ftp.x86-64.org/pub/linux/tools/mcelog/

%prep
%setup -q -n %{name}-0.9-pre1
%patch0 -p1 -b .record-length 

%build
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_mandir}

make CFLAGS="$RPM_OPT_FLAGS -fpie -pie"

%install
mkdir -p %{buildroot}%{_mandir}/man{1,8}
mkdir -p %{buildroot}/etc/cron.hourly
install mcelog %{buildroot}%{_sbindir}/mcelog
install mcelog.cron %{buildroot}/etc/cron.hourly/mcelog.cron
cp mcelog.8 %{buildroot}%{_mandir}/man8
chmod -R a-s %{buildroot}

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && [ -d $RPM_BUILD_ROOT ] && rm -rf $RPM_BUILD_ROOT;

%files
%defattr(-,root,root)
%{_sbindir}/mcelog
/etc/cron.hourly/mcelog.cron
%attr(0644,root,root) %{_mandir}/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.9pre1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.9pre1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.9pre1-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.9pre1-2m)
- add epoch to %%changelog

* Tue Jan 14 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1:0.9pre1-1m)
- update to 0.9pre1
- sync with Fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-2m)
- rebuild against gcc43

* Tue Mar 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-1m)
- import to Momonga from fc-devel

* Mon Jul 17 2006 Jesse Keating <jkeating@redhat.com>
- Rebuild.

* Fri Jun 30 2006 Dave Jones <davej@redhat.com>
- Rebuild. (#197385)

* Wed May 17 2006 Dave Jones <davej@redhat.com>
- Update to upstream 0.7
- Change frequency to hourly instead of daily.

* Thu Feb 09 2006 Dave Jones <davej@redhat.com>
- rebuild.

* Wed Feb  8 2006 Dave Jones <davej@redhat.com>
- Update to upstream 0.6

* Mon Dec 19 2005 Dave Jones <davej@redhat.com>
- Update to upstream 0.5

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4

* Wed Feb  9 2005 Dave Jones <davej@redhat.com>
- Update to upstream 0.4

* Thu Jan 27 2005 Dave Jones <davej@redhat.com>
- Initial packaging.

