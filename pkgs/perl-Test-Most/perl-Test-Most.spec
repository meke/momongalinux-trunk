%global         momorel 2

Name:           perl-Test-Most
Version:        0.33
Release:        %{momorel}m%{?dist}
Summary:        Most commonly needed test functions and features
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Test-Most/
Source0:        http://www.cpan.org/authors/id/O/OV/OVID/Test-Most-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Data-Dumper-Names >= 0.03
BuildRequires:  perl-Exception-Class >= 1.14
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Deep >= 0.106
BuildRequires:  perl-Test-Differences >= 0.61
BuildRequires:  perl-Test-Exception >= 0.31
BuildRequires:  perl-Test-Harness >= 3.21
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Test-Warn >= 0.23
Requires:       perl-Data-Dumper-Names >= 0.03
Requires:       perl-Exception-Class >= 1.14
Requires:       perl-Test-Deep >= 0.106
Requires:       perl-Test-Differences >= 0.61
Requires:       perl-Test-Exception >= 0.31
Requires:       perl-Test-Harness >= 3.21
Requires:       perl-Test-Simple >= 0.88
Requires:       perl-Test-Warn >= 0.23
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Test::Most exists to reduce boilerplate and to make your testing life
easier. We provide "one stop shopping" for most commonly used testing
modules. In fact, we often require the latest versions so that you get bug
fixes through Test::Most and don't have to keep upgrading these modules
separately.

%prep
%setup -q -n Test-Most-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Test/Most*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-2m)
- rebuild against perl-5.16.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-2m)
- rebuild against perl-5.14.2

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-2m)
- rebuild against perl-5.14.1

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-3m)
- rebuild against perl-5.10.1

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-2m)
- modify BuildRequires

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
