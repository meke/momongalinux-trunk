%global momorel 2

Summary: Displays where a particular program in your path is located.
Name: which
Version: 2.20
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/System
Source0: ftp://ftp.gnu.org/gnu/which/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: which-2.sh
Source2: which-2.csh
Patch0: which-2.19-afs.patch
URL: http://www.xs4all.nl/~carlo17/which/
Requires(post): info
Requires(preun): info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The which command shows the full pathname of a specified program, if
the specified program is in your PATH.

%prep
%setup -q
%patch0 -p1 -b .afs

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall
%__mkdir_p %{buildroot}%{_sysconfdir}/profile.d
%__install -m 755 $RPM_SOURCE_DIR/which-2.sh $RPM_SOURCE_DIR/which-2.csh %{buildroot}%{_sysconfdir}/profile.d

# clean up
rm -f %{buildroot}%{_infodir}/dir

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/which.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/which.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root)
%doc EXAMPLES README
%{_bindir}/which
%config %{_sysconfdir}/profile.d/which-2.*
%{_mandir}/man1/which.1*
%{_infodir}/which.info.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20-2m)
- rebuild for new GCC 4.6

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-1m)
- update to 2.20

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.19-8m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.19-7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19-4m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv3+

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19-3m)
- run install-info

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.19-2m)
- fix build on x86_64

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19-1m)
- update to 2.19
- update afs patch (import from Fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.16-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16-4m)
- %%NoSource -> NoSource

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (2.16-3m)
- revised spec for rpm 4.2.

* Sun Mar 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.16-2m)
- change %%attr
- revise specfile

* Fri Oct 17 2003 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.16-1m)

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.14-2m)
- add URL tag
- delete Prefix
- use macros

* Mon Jul 29 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.14-1m)
  update to 2.14

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.13-2k)
- ver up.

* Thu Apr 12 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 2.12

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Apr 03 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.9-2).

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- rebuild to gzip man page

* Sun Jan 16 2000 Preston Brown <pbrown@redhat.com>
- newer stuff rom Carlo (2.10).  Author's email: carlo@gnu.org

* Thu Jan 13 2000 Preston Brown <pbrown@redhat.com>
- adopted Carlo's specfile.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Sep 24 1999 Carlo Wood <carlo@gnu.org>
- There should not be a reason anymore to include README.alias in the rpm docs.
- Don't install as root.root in RPM_BUILD_ROOT, in order to allow to build
  rpm as non-root.
- Bug fix
- Added /etc/profile.d for automatic alias inclusion.

* Wed Aug 25 1999 Carlo Wood <carlo@gnu.org>
- Added README.alias.

* Wed Aug 11 1999 Carlo Wood <carlo@gnu.org>
- Typo in comment.

* Mon Aug  9 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.8.

* Fri Jul 16 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.7.

* Sat Jun 12 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.6.

* Thu May 27 1999 Carlo Wood <carlo@gnu.org>
- Typo fix
- Moved maintainer targets from makefile to Makefile.am.

* Tue May 18 1999 Carlo Wood <carlo@gnu.org>
- Typo in appended changelog.
- Appended the old change log of `which-2.0.spec' to (this) %changelog,
  which is generated from the CVS log of `which-2.0.spec.in'.
- Generate which-2.spec from which-2.spec.in with automatic VERSION
  and CHANGELOG substitution.

* Tue May 14 1999 Carlo Wood <carlo@gnu.org>
- Moved assignment of CFLAGS to the configure line, using RPM_OPT_FLAGS now.
- Corrected Source: line to point to ftp.gnu.org.

* Sat Apr 17 1999 Carlo Wood <carlo@gnu.org>
- Started to use automake and autoconf

* Fri Apr 09 1999 Carlo Wood <carlo@gnu.org>
- Renamed which-2.0.spec to which-2.spec

