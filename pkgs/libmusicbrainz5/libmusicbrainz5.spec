%global momorel 1

# Fedora package review: http://bugzilla.redhat.com/718395

Summary: Library for accessing MusicBrainz servers
Name: libmusicbrainz5
Version: 5.0.1
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: System Environment/Libraries
URL: http://www.musicbrainz.org/
Source0: https://github.com/downloads/metabrainz/libmusicbrainz/libmusicbrainz-%{version}.tar.gz
NoSource: 0

BuildRequires: cmake
BuildRequires: doxygen
BuildRequires: neon-devel

%description
The MusicBrainz client library allows applications to make metadata
lookup to a MusicBrainz server, generate signatures from WAV data and
create CD Index Disk ids from audio CD roms.

%package devel
Summary: Headers for developing programs that will use %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
%description devel
This package contains the headers that programmers will need to develop
applications which will use %{name}.


%prep
%setup -q -n libmusicbrainz-%{version}

# omit "Generated on ..." timestamps that induce multilib conflicts
# this is *supposed* to be the doxygen default in fedora these days, but
# it seems there's still a bug or 2 there -- Rex
echo "HTML_TIMESTAMP      = NO" >> Doxyfile.cmake


%build
%{cmake} .

%make  V=1
%make  docs


%install

make install/fast DESTDIR=%{buildroot}

rm -f docs/installdox


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc AUTHORS.txt COPYING.txt NEWS.txt README.md
%{_libdir}/libmusicbrainz5.so.0*

%files devel
%doc docs/*
%{_includedir}/musicbrainz5/
%{_libdir}/libmusicbrainz5.so
%{_libdir}/pkgconfig/libmusicbrainz5.pc


%changelog
* Fri Oct  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.1-1m)
- initial import from fedora
