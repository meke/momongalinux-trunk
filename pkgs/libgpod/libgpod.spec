%global momorel 6

%define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: Library to access an iPod audio player
Name: libgpod
Version: 0.8.2
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.gtkpod.org/libgpod.html
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/gtkpod/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: udev
BuildRequires: coreutils
BuildRequires: dbus-devel >= 0.92
BuildRequires: dbus-glib-devel
BuildRequires: eject
BuildRequires: gettext
BuildRequires: gtk-doc
BuildRequires: gtk-sharp2-devel
BuildRequires: gtk2-devel >= 2.10.3
BuildRequires: intltool
BuildRequires: libffi-devel
BuildRequires: libimobiledevice-devel >= 1.1.5
BuildRequires: libplist-devel
BuildRequires: libusb1-devel
BuildRequires: libxml2-devel
BuildRequires: libxslt
BuildRequires: mono-devel
BuildRequires: perl-XML-Parser
BuildRequires: pkgconfig
BuildRequires: pygobject-devel
BuildRequires: python-devel >= 2.7
BuildRequires: python-eyed3
BuildRequires: python-mutagen
BuildRequires: sg3_utils-devel >= 1.27
Buildrequires: sqlite-devel
BuildRequires: swig

%description
libgpod is a library meant to abstract access to an iPod content. It
provides an easy to use API to retrieve the list of files and playlist
stored on an iPod, to modify them and to save them back to the iPod.

%package devel
Summary: Headers for developing programs that will use libgpod
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Headers for developing programs that will use libgpod.

%package -n python-gpod
Summary: A python module to access iPod content
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: python-eyed3

%description -n python-gpod
A python module to access iPod content.  This module provides bindings
to the libgpod library.

%package sharp
Summary: C#/.NET library to access iPod content
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description sharp
C#/.NET library to access iPod content.  Provides bindings to the libgpod
library.

%package sharp-devel
Summary: Development files for libgpod-sharp
Summary: C#/.NET library to access iPod content
Group: Development/Languages
Requires: %{name}-sharp = %{version}-%{release}
Requires: pkgconfig

%description sharp-devel
This package contains the files required to develop programs that will use
libgpod-sharp.

%prep
%setup -q

# remove execute perms on the python examples as they'll be installed in %doc
chmod 644 bindings/python/examples/*.py

%build
%configure \
	--without-hal \
	--enable-udev \
	--with-temp-mount-dir=%{_localstatedir}/run/%{name}
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of la,file
find %{buildroot} -name "*.la" -delete

# remove Makefiles from the examples dir
rm -rf bindings/python/examples/Makefile*

# preparing...
mkdir -p %{buildroot}%{_localstatedir}/run/%{name}

# Setup tmpfiles.d config
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d
echo "D /var/run/%{name} 0755 root root -" > \
    %{buildroot}%{_sysconfdir}/tmpfiles.d/%{name}.conf

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README* TROUBLESHOOTING
%config(noreplace) %{_sysconfdir}/tmpfiles.d/%{name}.conf
/lib/udev/rules.d/90-%{name}.rules
/lib/udev/iphone-set-info
/lib/udev/ipod-set-info
%{_bindir}/ipod-read-sysinfo-extended
%{_libdir}/%{name}.so.*
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%dir %{_localstatedir}/run/%{name}

%files devel
%defattr(-,root,root)
%{_includedir}/gpod-1.0
%{_libdir}/pkgconfig/%{name}-1.0.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so
%{_datadir}/gtk-doc/html/%{name}

%files -n python-gpod
%defattr(-, root, root, 0755)
%doc COPYING bindings/python/README bindings/python/examples
%{python_sitearch}/gpod

%files sharp
%defattr(-, root, root, 0755)
%{_libdir}/%{name}/%{name}-sharp*

%files sharp-devel
%defattr(-, root, root, 0755)
%{_libdir}/pkgconfig/%{name}-sharp.pc

%changelog
* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-6m)
- rebuild against libimobiledevice-1.1.5

* Wed Aug 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-5m)
- rebuild for libimobiledevice

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-4m)
- rebuild for mono-2.10.9

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-3m)
- remove BR hal

* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-2m)
- add tmpfiles.d file

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- version 0.8.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.94-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.94-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.94-2m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.94-1m)
- version 0.7.94
- add packages sharp and sharp-devel
- import 2 upstream patches from Fedora

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.93-4m)
- full rebuild for mo7 release

* Tue Aug  3 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.7.93-3m)
- Fix temp mount dir configure option typo

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.93-2m)
- rebuild against libimobiledevice-1.0.2

* Mon Apr 19 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.93-1m)
- version 0.7.93
-- /lib/udev/rules.d/80-libgpod.rules -> 90-libgpod.rules

* Wed Mar 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.92-1m)
- version 0.7.92
- remove merged Make-the-temporary-mount-point-configurable.patch

* Fri Mar 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.91-1m)
- version 0.7.91
- import Make-the-temporary-mount-point-configurable.patch from Fedora
- good-bye hal and hello udev

* Mon Feb 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.90-1m)
- version 0.7.90
- change hal-callouts-dir from %%{_libdir}/hal/scripts to %%{_libexecdir}/scripts
- sort %%files

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-2m)
- rebuild against sg3_utils-1.27

* Sat Apr 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-1m)
- version 0.7.2

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.0-1m)
- version 0.7.0
- import Use-var-run-hald-as-mount-dir-for-hal-callout.patch from Fedora
 +* Wed Jan 14 2009 Todd Zullinger <tmz@pobox.com> - 0.6.0-10
 +- Fix path to hal callout (this should help setup the SysInfoExtended
 +  file automagically)
 +- Use /var/run/hald as mount dir for hal callout
 +- Require hal
- remove merged gcc43.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-6m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.0-5m)
- rebuild against python-2.6.1-1m

* Thu Jun  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-4m)
- fix install (dose not use %%makeinstall)
- add BuildRequires: libffi-devel BuildRequires: sg3_utils-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-3m)
- rebuild against gcc43

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Wed Nov 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-1m)
- version 0.6.0

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-1m)
- version 0.5.2

* Sun Apr 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-3m)
- python-gpod.spec was merged into this package

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-2m)
- libgpod-devel Requires: pkgconfig

* Fri Jan 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-1m)
- version 0.4.2

* Tue Oct 24 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- version 0.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.2-3m)
- rebuild against gtk+-2.10.3
- delete libtool library

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.2-2m)
- rebuild against dbus 0.92

* Mon Mar  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-1m)
- version 0.3.2

* Tue Feb 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- initial package for amarok-1.4
