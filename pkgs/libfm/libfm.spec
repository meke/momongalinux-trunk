%global momorel 1
%global         build_doc   1

# Review: https://bugzilla.redhat.com/show_bug.cgi?id=567257

# Upstream git:
# git://pcmanfm.git.sourceforge.net/gitroot/pcmanfm/libfm

Name:           libfm
Version:        1.2.0
Release:        %{momorel}m%{?dist}
Summary:        GIO-based library for file manager-like programs

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://pcmanfm.sourceforge.net/
Source0:        http://downloads.sourceforge.net/pcmanfm/%{name}-%{version}.tar.xz
NoSource:       0
# Fedora specific patches
#Patch0:         libfm-0.1.9-pref-apps.patch

BuildRequires:  libexif-devel
BuildRequires:  gtk3-devel
BuildRequires:  menu-cache-devel >= 0.3.2

BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  desktop-file-utils

BuildRequires:  gtk-doc
BuildRequires:  libxslt
BuildRequires:  dbus-glib-devel
BuildRequires:  vala

%if 0%{?build_doc} < 1
Obsoletes:      %{name}-devel-docs < 0.1.15
%endif

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig


%description
LibFM is a GIO-based library used to develop file manager-like programs. It is
developed as the core of next generation PCManFM and takes care of all file-
related operations such as copy & paste, drag & drop, file associations or 
thumbnails support. By utilizing glib/gio and gvfs, libfm can access remote 
file systems supported by gvfs.

This package contains the generic non-gui functions of libfm.


%package        gtk
Summary:        File manager-related GTK+ widgets of %{name}
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gvfs

%description    gtk
libfm is a GIO-based library used to develop file manager-like programs. It is
developed as the core of next generation PCManFM and takes care of all file-
related operations such as copy & paste, drag & drop, file associations or 
thumbnail support. By utilizing glib/gio and gvfs, libfm can access remote 
file systems supported by gvfs.

This package provides useful file manager-related GTK+ widgets.

%package        gtk-utils
Summary:        GTK+ related utility package for %{name}
Group:          User Interface/Desktops
Requires:       %{name}-gtk%{?isa} = %{version}-%{release}
Obsoletes:      lxshortcut < 0.1.3
Provides:       lxshortcut = %{version}-%{release}
Provides:       lxshortcut%{?_isa} = %{version}-%{release}

%description    gtk-utils
This package contains some GTK+ related utility files for
%{name}.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        gtk-devel
Summary:        Development files for %{name}-gtk
Group:          Development/Libraries
Requires:       %{name}-gtk = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}

%description    gtk-devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}-gtk.

%package        devel-docs
Summary:        Development documation for %{name}
Group:          Development/Libraries

%description    devel-docs
This package containg development documentation files for %{name}.

%prep
%setup -q

#%patch0 -p1 -b .orig

# ???
mkdir -p docs/reference/libfm
touch docs/{reference{/libfm/,},}/Makefile.in

# treak rpath
sed -i.libdir_syssearch -e \
  '/sys_lib_dlsearch_path_spec/s|/usr/lib |/usr/lib /usr/lib64 /lib /lib64 |' \
  configure

%build
%configure \
    --enable-gtk-doc \
    --enable-udisks \
    --with-gtk=3 \
%if 0
    --enable-demo \
%endif
	--disable-silent-rules \
    --disable-static

# To show translation status
make -C po -j1 GMSGFMT="msgfmt --statistics"
make %{?_smp_mflags} -k

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'

rm -f $RPM_BUILD_ROOT%{_libdir}/pkgconfig/libfm-gtk.pc
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

for i in %{buildroot}/%{_datadir}/applications/*.desktop; do
desktop-file-validate $i
done

%find_lang %{name}

echo '%%defattr(-,root,root,-)' > base-header.files
echo '%%defattr(-,root,root,-)' > gtk-header.files

for f in $RPM_BUILD_ROOT%_includedir/%name-1.0/*.h
do
  bf=$(basename $f)
  for dir in actions base job extra .
  do
    if [ -f src/$dir/$bf ]
    then
      echo %_includedir/%name-1.0/$bf >> base-header.files
    fi
  done
  for dir in gtk
  do
    if [ -f src/$dir/$bf ]
    then
      echo %_includedir/%name-1.0/$bf >> gtk-header.files
    fi
  done
done

/usr/lib/rpm/check-rpaths

%check
make check

%clean
rm -rf $RPM_BUILD_ROOT


%post
/sbin/ldconfig
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :
gio-querymodules %{_libdir}/gio/modules || :

%pre devel
# Directory -> symlink
if [ -d %{_includedir}/libfm ] ; then
  rm -rf %{_includedir}/libfm
fi

%postun
/sbin/ldconfig
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :
gio-querymodules %{_libdir}/gio/modules || :

%post gtk -p /sbin/ldconfig

%postun gtk -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
# FIXME: Add ChangeLog if not empty
%doc AUTHORS
%doc COPYING
%doc NEWS
%doc README
%dir %{_sysconfdir}/xdg/libfm/
%config(noreplace) %{_sysconfdir}/xdg/libfm/libfm.conf
%{_libdir}/%{name}.so.4*
%{_libdir}/%{name}-extra.so.4*
%dir %{_libdir}/libfm
%dir %{_libdir}/libfm/modules
%{_libdir}/libfm/modules/vfs-*.so
%{_datadir}/mime/packages/libfm.xml


%files gtk
%defattr(-,root,root,-)
#%{_bindir}/libfm-pref-apps
%{_libdir}/%{name}-gtk3.so.*
#dir #{_libdir}/libfm/
#{_libdir}/libfm/gnome-terminal
%{_datadir}/libfm/
%{_libdir}/libfm/modules/gtk-*.so

%files gtk-utils
%defattr(-,root,root,-)
%{_mandir}/man1/libfm-pref-apps.1.*
%{_mandir}/man1/lxshortcut.1.*
%{_bindir}/libfm-pref-apps
%{_bindir}/lxshortcut
%{_datadir}/applications/libfm-pref-apps.desktop
%{_datadir}/applications/lxshortcut.desktop


%files devel -f base-header.files
%defattr(-,root,root,-)
%doc TODO
%{_includedir}/libfm
%dir %{_includedir}/libfm-1.0/
%{_libdir}/%{name}.so
%{_libdir}/%{name}-extra.so
%{_libdir}/pkgconfig/libfm.pc

%files gtk-devel -f gtk-header.files
%defattr(-,root,root,-)
%{_includedir}/libfm-1.0/fm-gtk.h
%{_libdir}/%{name}-gtk3.so
%{_libdir}/pkgconfig/libfm-gtk3.pc

%if 0%{?build_doc}
%files devel-docs
%defattr(-,root,root,-)
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/%{name}
%endif

%changelog
* Mon May 19 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.17-1m)
- update to 0.1.17

* Wed Apr 13 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.12-6m)
- modify Makefile to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.12-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.12-4m)
- rebuild for new GCC 4.5

* Sun Oct 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.12-3m)
- add gio-querymodules to script

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.12-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.12-1m)
- import from Rawhide

* Fri Jun 25 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.12-4
- Fix crash with --desktop mode when clicking volume icon
  (bug 607069)

* Thu Jun 10 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.12-3
- Fix an issue that pcmanfm // crashes (upstream bug 3012747)

* Fri Jun  4 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.12-2
- Fix an issue in sorting by name in cs_CZ.UTF-8 (upstream bug 3009374)

* Sat May 29 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.12-1
- Update to 0.1.12, drop upstreamed patches

* Sat May 29 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.11-7
- Fix crash of gnome-terminal wrapper with certain path settings
  (bug 596598, 597270)

* Tue May 25 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.11-5
- Translation update from git
- Fix an issue in sorting by name in ja_JP.UTF-8 (upstream bug 3002788)

* Sun May  9 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.11-4
- Translation update from git

* Fri May  7 2010 Mamrou Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.11-3
- Remove runpath_var=... trick on libtool which causes internal
  linkage error,
  and treak sys_lib_dlsearch_path_spec instead for rpath issue on x86_64

* Fri May  7 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.11-2
- Fix crash of wrapper of gnome-terminal when libfm.conf doesn't exist or so
  (bug 589730)

* Thu Apr 29 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.1.11-1
- Update to 0.1.11

* Sun Apr 18 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.10-1
- Update to 0.1.10

* Sun Mar 21 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.9-2
- Own %%{_libdir}/libfm
- Validate desktop file

* Fri Mar 19 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.9-1
- Update to 0.1.9 (Beta 1)

* Sat Mar 13 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.5-1
- Update to 0.1.5 (Alpha 2)

* Fri Mar 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.1-1
- Update to 0.1.1 (Alpha 1)

* Mon Feb 22 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-1
- Initial packaging

