%global momorel 7

Name:           sinjdoc
Version:        0.5
Release:        %{momorel}m%{?dist}
Summary:        Documentation generator for Java source code

Group:          Development/Tools
License:        GPL
URL:            http://cscott.net/Projects/GJ/index.html#sinjdoc
Source0:        sinjdoc-0.5.tar.gz
Patch0:         sinjdoc-annotations.patch
Patch1:         sinjdoc-autotools-changes.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: autoconf
BuildRequires: automake16
BuildRequires: eclipse-ecj >= 3.2.1
BuildRequires: gcc-java >= 4.0.2
BuildRequires: java-gcj-compat-devel >= 1.0.70
BuildRequires: java_cup >= 0.10

Requires:         java_cup >= 0.10
Requires:         libgcj >= 4.1.2
Requires(post):   java-gcj-compat >= 1.0.70
Requires(postun): java-gcj-compat >= 1.0.70

Obsoletes: gjdoc <= 0.7.7-14.fc7

%description
This package contains Sinjdoc a tool for generating Javadoc-style
documentation from Java source code

%prep
%setup -q
%patch0 -p0
%patch1 -p0

%build
automake-1.6
autoconf
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
cat > sinjdoc << EOF
#!/bin/sh
%{_bindir}/gij -classpath \
  %{_javadir}/java_cup-runtime.jar:%{_javadir}/sinjdoc.jar \
  net.cscott.sinjdoc.Main "\$@"
EOF
install -d 755 $RPM_BUILD_ROOT%{_bindir}
install -m 655 sinjdoc $RPM_BUILD_ROOT%{_bindir}/sinjdoc
install -d 755 $RPM_BUILD_ROOT%{_javadir}
install -D -m 644 sinjdoc.jar $RPM_BUILD_ROOT%{_javadir}/sinjdoc.jar
aot-compile-rpm

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi

%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README
%{_bindir}/sinjdoc
%{_javadir}/sinjdoc.jar
%{_libdir}/gcj/%{name}/sinjdoc*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-2m)
- rebuild against gcc43

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5-1m)
- import from Fedora

* Tue Apr  3 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 0.5-4
- Obsolete gjdoc.

* Tue Mar 27 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 0.5-3
- Fix wrapper script argument quoting.

* Mon Mar 19 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 0.5-2
- Initial build in Fedora Core.

* Mon Mar 15 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 0.5-1
- Initial release.
