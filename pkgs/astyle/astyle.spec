%global momorel 1

Name:           astyle
Version:        2.01
Release:        %{momorel}m%{?dist}
Summary:        Source code formatter for C-like programming languages
Group:          Development/Languages
License:        LGPLv3+
URL:            http://astyle.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/astyle/%{name}_%{version}_linux.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Artistic Style is a source code indenter, source code formatter, and
source code beautifier for the C, C++, C# and Java programming
languages.


%prep
%setup -q -n %{name}

%build
g++ -o astyle %{optflags} src/*.cpp
chmod a-x src/*
chmod a-x doc/*


%install
rm -rf %{buildroot}
install -p -D -m 755 astyle %{buildroot}%{_bindir}/astyle


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_bindir}/astyle

%doc doc/*.html

%changelog
* Sun Sep 11 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.01-1m)
- version up 2.01

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.24-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.23-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.21-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.21-2m)
- rebuild against gcc43

* Mon Jul 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Mon Jul 17 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.18-1m)
- initial package
