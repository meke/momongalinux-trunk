%global momorel 23

%define dbdir db.1.85
%define dbinclude %{dbdir}/include
%define dblib %{dbdir}/PORT/linux

Summary: The libraries needed to run the GNOME GUI desktop environment.
Name: gnome-libs
Version: 1.4.2
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
Source: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.4/%{name}-%{version}.tar.bz2
Nosource: 0
Source1: db.1.85.tar.gz
Source2: fdl.sgml
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: utempter
BuildRequires: utempter gettext >= 0.10.40
BuildRequires: esound-devel, esound
BuildRequires: audiofile-devel, audiofile
BuildRequires: imlib-devel >= 1.9.15-8m, imlib
BuildRequires: libjconv >= 1.3
BuildRequires: glib1-devel >= 1.2.10-17m
BuildRequires: gtk+1-devel >= 1.2.10-41m
BuildRequires: ORBit-devel >= 0.5.17-12m
BuildRequires: autoconf213, automake14
BuildRequires: libjpeg-devel >= 8a

# Red Hat-specific patches are prefaced with RH, please.
Patch0: RH-gnome-libs-rhsnddefs.patch
Patch3: gnome-libs-1.4.2-mod_esd.patch
Patch4: gnome-libs-1.4.1.1-zvtterm-mb.patch
Patch5: gnome-libs-1.4.2-kondara.patch
Patch7: gnome-libs-dentry.patch
Patch9: gnome-libs-1.4.2-term.patch
Patch11: gnome-libs-1.0.55-i18nfix.patch
Patch12: gnome-libs-1.2.11-mime.patch
Patch16: gnome-libs-1.2.13-iconv.patch
Patch17: gnome-libs-1.0.61-ungrab.patch
patch25: gnome-libs-gtkrc.patch
Patch100: gonome-libs-soundoff.patch
Patch101: gnome-libs-no-xalf.patch
Patch104: gnome-libs-1.2.13-notearoffs.patch
Patch109: gnome-libs-1.4.1.1-icon.patch
Patch111: gnome-libs-1.2.13-bghack.patch
Patch113: gnome-libs-1.4.1.1-esdmake.patch

Patch200: gnome-libs-1.4.1.2.90-db1.patch

## db1 patches
Patch280: db.1.85.patch
Patch281: db.1.85.s390.patch
Patch282: db.1.85.nodebug.patch

Patch300: gnome-libs-1.4.2-nopara-openjade-1.3.2-iyasugi.patch
Patch301: libart.m4.patch

Patch400: gnome-libs-1.4.2-gcc4.patch

Patch500: gnome-libs-1.4.2-ppc64.patch

%description
GNOME (GNU Network Object Model Environment) is a user-friendly set of
GUI applications and desktop tools to be used in conjunction with a
window manager for the X Window System.  The gnome-libs package
includes libraries that are needed to run GNOME.

%package devel
Summary: Libraries and include files for developing GNOME applications.
Group: Development/Libraries
Requires: %name = %{version}-%{release}
Requires: imlib-devel
Requires: ORBit-devel
Requires: gtk+1-devel
Requires: esound-devel
Obsoletes: gnome-devel

%description devel
GNOME (GNU Network Object Model Environment) is a user-friendly set of
GUI applications and desktop tools to be used in conjunction with a
window manager for the X Window System. The gnome-libs-devel package
includes the libraries and include files that you will need to develop
GNOME applications.

You should install the gnome-libs-devel package if you would like to
develop GNOME applications.  You don't need to install gnome-libs-devel
if you just want to use the GNOME desktop environment.
#'

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-23m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-22m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-21m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-20m)
- rebuild against libjpeg-8a

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-19m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-18m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-16m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-15m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-14m)
- update Patch0,3,5,9 for fuzz=0
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2-13m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-12m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.2-11m)
- enable ppc64

* Fri Apr 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-10m)
- delete document (gtk-doc >= 1.5 cannot create document)

* Mon Feb 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-9m)
- fix conflict directories

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-8m)
- add gcc4 patch.
- Patch400: gnome-libs-1.4.2-gcc4.patch

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-7m)
- suppress AC_DEFUN warning.

* Fri Feb 18 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.2-6m)
- avoid conflict with libgnomeui.

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (1.4.2-5m)
- x86_64.

* Wed Apr 21 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.2-4m)
- add fdl.spec and nopara.patch.

* Sun Apr  4 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.2-3m)
- embeded db1. No more db1 dependency please ;-<

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.2-2m)
- revised spec for enabling rpm 4.2.

* Sun Aug 25 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.4.2-1m)
- version 1.4.2

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.4.1.7-2k)
- version 1.4.1.7

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.4.1.4-6k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Mon Feb  4 2002 Shingo Akagaki <dora@kondara.org>
- (1.4.1.4-4k)
- disable-gtkdoc

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.4.1.4-2k)
- version 1.4.1.4

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (1.4.1.2-24k)
- autoconf automake 1.5

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.2-22k)
- move includefile to %{_prefix}/include/gnome-libs-1

* Fri Dec  7 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.2-20k)
- use galeon

* Thu Dec  6 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.2-18k)
- change gnome-libs-mod_esd.patch

* Tue Dec 04 2001 Motonobu Ichimura <famao@kondara.org>
- (1.4.1.2-16k)
- zh_CN.GB2312 => zh_CN
- zh_TW.Big5 => zh_TW

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.2-14k)
- mada nigirisugi

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.2-8k)
- nigirisugi /var/lib/games

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.4.1.2-6k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (1.4.1.2-4k)
- rebuild against libpng 1.2.0.

* Thu Sep 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.4.1.2-2k)
- version 1.4.1.2

* Tue Sep  4 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-8k)
- 6k is broken!
- we can run autoconf
- esd define patch

* Wed Aug 29 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-6k)
- autoconf ...

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.1.1-4k)
- add patches from RawHide

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.13-14k)
- no more ifarch alpha.

* Tue May 31 2001 Akira Higuchi <a@kondara.org>
- (1.2.13-12k)
- gnome-libs-iconfont.patch

* Mon May 28 2001 Akira Higuchi <a@kondara.org>
- (1.2.13-10k)
- gnome-libs-no-xalf.patch

* Wed Apr 18 2001 Akira Higuchi <a@kondara.org>
- (1.2.13-9k)
- gnome-libs-1.2.13-iconv.patch

* Tue Apr 17 2001 Shingo Akagaki <dora@kondara.org>
- dentry codeset patch

* Mon Apr 16 2001 Shingo Akagaki <dora@kondara.org>
- add gnome canvas aa patch

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.2.13

* Sun Mar 16 2001 Shingo Akagaki <dora@kondara.org>
- K2K
- version 1.2.12

* Thu Jan 25 2001 Yoshito Komatsu Komatsu <yoshito.komatsu@nifty.com>
- (1.2.11-3k)
- update to 1.2.11.
- errased gnome-libs-applix-mime.patch because of unnecessary

* Mon Jan 15 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- sound off.

* Thu Oct 26 2000 Kenichi Matsubara <m@kondara.org>
- man.gz to man.bz2.

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.4-5k)
- fixed for FHS

* Mon Aug 20 2000 Shingo Akagaki <dora@kondara.org>
- just rebuild for Jirai

* Mon Jul 17 2000 tom <tom@kondara.org>
- add configure option --with-kde-datadir. thanks super bros. toru

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- just rebuild for kondara 1.2

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Jun 22 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.3

* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.0

* Fri May 12 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.61

* Fri May 12 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.60

* Sat May  6 2000 Akira Higuchi <a@kondara.org>
- fixed some bugs in gnome-calc.c

* Mon Apr 10 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.58

* Fri Mar 24 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.57

* Sun Mar 12 2000 Akira Higuchi <a@kondara.org>
- removed application/x-kde-app-info. kdelnk files are recognized as of type
application/x-gnome-app-info instead.

* Tue Mar  3 2000 Shingo Akagaki <dora@kondara.org>
- gtkxmhtml handle button press event button1 only

* Fri Mar  3 2000 Akira Higuchi <a@kondara.org>
- some fixes in gtkxmhtml.patch

* Wed Mar  1 2000 Akira Higuchi <a@kondara.org>
- merged 3 gtkxmhtml patches into one
- changed the default fontset pattern for gtkxmhtml

* Sat Feb 26 2000 Akira Higuchi <a@kondara.org>
- use gnome-term instead of gnome-terminal

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file
- version 1.0.56

* Tue Feb 22 2000 Shingo Akagaki <dora@kondara.org>
- rewrite files section

* Thu Feb 17 2000 Shingo Akagaki <dora@kondara.org>
- fix dentry
- fix i18n

* Wed Feb 16 2000 Shingo Akagaki <dora@kondara.org>
- new gnome-libs-1.0.55-term.patch
- add gnome-libs-1.0.55-config.patch

* Tue Feb  8 2000 Shingo Akagaki <dora@kondara.org>
- gtk-xmhtml can browse page with form
- gtk-xmhtml use convert_kanji_auto

* Wed Feb  2 2000 Shingo Akagaki <dora@kondara.org>
- default font pxlsz 10 -> 12
- xmhtml table layout fix from a@kondara.org
- add kdelnk to application/x-gnome-app-info mime type
- modify gnome-about calc w size code

* Fri Jan 26 2000 Shingo Akagaki <dora@kondara.org>
- gnome-i18n quick hack for read .desktop file
- add urllink (x-url/url) mime type

* Fri Jan 21 2000 Shingo Akagaki <dora@kondara.org>
- gtkxmhtml default bgcolor was set white

* Thu Jan 20 2000 Shigno Akagaki <dora@kondara.org>
- libzvt mb patch update to 0.6
- change terminal (xterm -> gnome-term)

* Wed Jan 12 2000 Shingo Akagaki <dora@kondara.org>
- update to 1.0.55

* Sat Jan 10 2000 Shingo Akagaki <dora@kondara.org>
- change druid i18n code
- add gtkxmhtml japanese font select patch

* Thu Jan 06 2000 Shingo Akagaki <dora@kondara.org>
- update esd dynamic code (Tyuuta #270)

* Thu Dec 23 1999 Akira Higuchi <a@kondara.org>
- added dentry patch

* Thu Nov 25 1999 Shingo Akagaki <dora@kondara.org>
- libzvt mb patch...
- gtk-xmhtml nbsp patch
- merge some i18n patch to kondara patch

* Sun Nov 21 1999 Shingo Akagaki <dora@kondara.org>
- K12n
- gnome-about i18n patch

* Thu Nov 9 1999 Akira Higuchi <a@kondara.org>
- gtk-xmhtml-misc-991105.patch: bug fixes

* Thu Nov 4 1999 Shingo Akagaki <dora@kondara.org>
- include additional esd wrapper functions

* Sun Oct 31 1999 Shingo Akagaki <dora@kondara.org>
- update to 1.0.54
- update esd dynamic code

* Thu Oct 28 1999 Akira Higuchi <a-higuti@math.sci.hokudai.ac.jp>
- Added wordwrap patch

* Tue Oct 26 1999 Shingo Akagaki <dora@kondara.org>
- adjust apec file for Kondara
- esd dynamic loading patch

* Tue Oct 12 1999 Shingo Akagaki <dora@kondara.org>
- Update to 1.0.53
- GNOME druid fontset patch

* Tue Aug 31 1999 Elliot Lee <sopwith@redhat.com>
- Update to 1.0.15

* Mon Aug 30 1999 Elliot Lee <sopwith@redhat.com>
- Merge in various minor things from RHL

* Mon Jun 14 1999 Gregory McLean <gregm@comstar.net>

- Added the -q option to the setup stage, quiet please!

* Tue Mar 2  1999 Gregory McLean <gregm@comstar.net>

- Added some hackage in for the brain dead libtool on the alpha
- Cleaned up the spec file abit to be more consistant.

* Wed Feb 17 1999 Elliot Lee <sopwith@redhat.com>

- Add debugging disabling flags to $CFLAGS

* Fri Nov 20 1998 Pablo Saratxaga <srtxg@chanae.alphanet.ch>

- use --localstatedir=/var/lib in config state (score files for games
  for exemple will go there).
- added several more files to %files section, in particular language
  files and corba IDLs

* Wed Sep 23 1998 Michael Fulbright <msf@redhat.com>

- Updated to version 0.30

* Mon Apr 13 1998 Marc Ewing <marc@redhat.com>
- Added %{prefix}/lib/gnome-libs

* Fri Mar 13 1998 Marc Ewing <marc@redhat.com>

- Integrate into gnome-libs source tree

%prep
%setup -q -a 1

%patch0 -p1 -b .rhsnddefs
%patch3 -p1 -b .mod_esd
%patch5 -p1 -b .kondara
%patch7 -p1 -b .dentry
%patch9 -p1 -b .term
%patch11 -p1 -b .i18nfix
%patch12 -p1 -b .mime
%patch16 -p1 -b .iconv
%patch17 -p1 -b .ungrab
%patch25 -p1
%patch100 -p1
%patch101 -p1 -b .noxalf
%patch104 -p1 -b .notearoffs
%patch109 -p1 -b .icon
%patch111 -p1 -b .bghack
%patch113 -p1 -b .esdmake

%patch200 -p1 -b .db1

%patch300 -p1 -b .nopara
cp %{S:2} libart_lgpl/doc

cd %{dbdir}

%patch280 -p1 -b .patch
%patch281 -p1 -b .fPIC
%patch282 -p1 -b .nodebug

## back to main dir
cd %{_builddir}/%{name}-%{version}

%patch301 -p0 -b .m4~

%patch400 -p1 -b .gcc4

%ifarch ppc64
%patch500 -p1
%endif

%build
## to db 
cd %{dbdir}

gzip -9 docs/*.ps
(cd include && ln -sf db.h db_185.h)
cd PORT/linux
make OORG="%{optflags}"
sover=`echo libdb.so.* | sed 's/libdb.so.//'`
ln -sf libdb1.so.$sover 		libdb1.so
ln -sf libdb.so.$sover			libdb1.so.$sover

## back to main dir
cd %{_builddir}/%{name}-%{version}

autoconf-2.13
automake-1.4
perl -pi -e 's@\$CPPFLAGS@\$CPPFLAGS'" -I%{_builddir}/%{name}-%{version}/%{dbinclude}"@g configure
perl -pi -e 's@\$LDFLAGS@\$LDFLAGS'" -L%{_builddir}/%{name}-%{version}/%{dblib}"@g configure
%configure --enable-prefer-db1 --enable-gtk-doc=no

perl -pi -e 's/-ldb1//g' gnome-config
perl -pi -e 's/-ldb//g' gnome-config

%ifarch ppc64
%make LIBTOOL=/usr/bin/libtool
%else
%make
%endif

%install
rm -rf %{buildroot}
## to db 
cd %{dbdir}
mkdir -p %{buildroot}{%{_includedir}/db1,%{_bindir},%{_libdir}}

sed -n '/^\/\*-/,/^ \*\//s/^.\*.\?//p' include/db.h | grep -v '^@.*db\.h' > LICENSE

perl -pi -e 's/<db.h>/<db1\/db.h>/' PORT/include/ndbm.h

cd PORT/linux
sover=`echo libdb.so.* | sed 's/libdb.so.//'`
install -m644 libdb.a			%{buildroot}/%{_libdir}/libdb1.a
install -m755 libdb.so.$sover		%{buildroot}/%{_libdir}/libdb1.so.$sover
ln -sf libdb1.so.$sover 		%{buildroot}/%{_libdir}/libdb1.so
ln -sf libdb1.so.$sover			%{buildroot}/%{_libdir}/libdb.so.$sover
install -m644 ../include/ndbm.h		%{buildroot}/%{_includedir}/db1/
install -m644 ../../include/db.h	%{buildroot}/%{_includedir}/db1/
install -m644 ../../include/mpool.h	%{buildroot}/%{_includedir}/db1/
install -s -m755 db_dump185		%{buildroot}/%{_bindir}/db1_dump185

## back to main dir
cd %{_builddir}/%{name}-%{version}

SAVE_LLP=$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=%{_builddir}/%{name}-%{version}/%{dbdir}/PORT/linux:$SAVE_LLP
%ifarch ppc64
%makeinstall LIBTOOL=/usr/bin/libtool
%else
%makeinstall
%endif
export LD_LIBRARY_PATH=$SAVE_LLP

mv %{buildroot}%{_prefix}/share/doc/gnome-doc %{buildroot}%{_prefix}/bin
chmod a+x %{buildroot}%{_prefix}/bin/gnome-doc
mv %{buildroot}%{_prefix}/share/doc/mkstub %{buildroot}%{_bindir}
chmod a+x %{buildroot}%{_prefix}/bin/mkstub
mkdir -p %{buildroot}%{_datadir}/emacs/site-lisp
mv %{buildroot}%{_prefix}/share/doc/gnome-doc.el %{buildroot}%{_datadir}/emacs/site-lisp

mv -f %{buildroot}%{_prefix}/doc .

perl -pi -e 's/-ldb1//g' %{buildroot}%{_libdir}/*.{sh,la}

# remove
rm -rf %{buildroot}/%{_includedir}/db1
rm -f %{buildroot}/%{_libdir}/libdb1.{a,so}
rm -f %{buildroot}/usr/share/doc/gnome-doc.1
rm -f %{buildroot}/usr/share/doc/gnome-mkstub.1

# remove depulicated files
rm -rf %{buildroot}%{_datadir}/pixmaps/gnome-default-dlg.png
rm -rf %{buildroot}%{_datadir}/pixmaps/gnome-error.png
rm -rf %{buildroot}%{_datadir}/pixmaps/gnome-info.png
rm -rf %{buildroot}%{_datadir}/pixmaps/gnome-question.png
rm -rf %{buildroot}%{_datadir}/pixmaps/gnome-warning.png

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README doc
%{_sysconfdir}/gnome
%{_sysconfdir}/sound
%config %{_sysconfdir}/mime-magic.dat
%config %{_sysconfdir}/paper.config
%config %{_sysconfdir}/mime-magic
%{_bindir}/gnome-bug
%{_bindir}/gnome-name-service
%{_bindir}/gnome-gen-mimedb
%{_bindir}/gnome-dump-metadata
%{_bindir}/gconfigger
%{_bindir}/gnome_segv
%{_bindir}/goad-browser
%{_bindir}/loadshlib
%{_bindir}/new-object
%{_bindir}/gnome-moz-remote
%{_bindir}/dns-helper
%{_libdir}/lib*.so.*
%attr(2755, root, utmp) %{_sbindir}/gnome-pty-helper
%{_mandir}/man1/gnome*.1*
%{_mandir}/man1/dns-helper.1*
%{_mandir}/man1/gconfigger.1*
%{_mandir}/man1/goad-browser.1*
%{_mandir}/man1/libart-config.1*
%{_mandir}/man1/loadshlib.1*
%{_mandir}/man1/new-object.1*
%{_mandir}/man5/*
%{_datadir}/type-convert
%{_datadir}/mime-info/gnome.mime
%{_datadir}/emacs/site-lisp/gnome-doc.el
%{_datadir}/gnome/html
%{_datadir}/locale/*/*/*
%{_datadir}/pixmaps/*.png
%{_datadir}/pixmaps/*.xpm

## db1 pieces
# already listed above
#%{_libdir}/libdb1.so.*
#%{_libdir}/libdb.so.*
%{_bindir}/db1_dump185

%files devel
%defattr(-, root, root)
%{_bindir}/gnome-doc
%{_bindir}/gnome-config
%{_bindir}/libart-config
%{_bindir}/mkstub
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_libdir}/*.sh
%{_libdir}/gnome-libs
%{_datadir}/idl/*
#%{_datadir}/man/man1/gnome-config.1.*
%doc %{_datadir}/gnome/help/gnome-dev-info
%{_datadir}/aclocal/*
%doc %{_datadir}/gtk-doc/html/*
%{_includedir}/*
