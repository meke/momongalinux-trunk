%global momorel 9
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-openin
Version:        20070524
Release:        %{momorel}m%{?dist}
Summary:        OCaml syntax to locally open modules

Group:          Development/Libraries
License:        Public Domain
URL:            http://alain.frisch.fr/soft#openin
Source0:        http://alain.frisch.fr/info/openin-20070524.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel

%global __ocaml_requires_opts -i Asttypes -i Parsetree

%description
This package implements a Camlp4 syntax extension for Objective
Caml. It adds the syntactic construction:

open M in e

that can appear in any context where an expression is expected. M is
an arbitrary module expression (not only qualified names as for usual
open statements) and e is an expression.


%prep
%setup -q -n openin-%{version}


%build
make

cat > META <<EOF
name = "openin"
version = "%{version}"
requires = "camlp4"
archive(syntax,toploop) = "pa_openin.cmo"
archive(syntax,preprocessor) = "pa_openin.cmo"
EOF


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs

ocamlfind install openin META pa_openin.cmo


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README
%{_libdir}/ocaml/openin


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (20070524-9m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070524-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070524-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20070524-6m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070524-5m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070524-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070524-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070524-2m)
- rebuild against ocaml-3.11.0 

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070524-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 20070524-3
- Rebuild for OCaml 3.10.2

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 20070524-2
- Remove ExcludeArch ppc64.

* Thu Feb 28 2008 Richard W.M. Jones <rjones@redhat.com> - 20070524-1
- Initial RPM release.
