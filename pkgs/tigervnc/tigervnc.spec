%global momorel 3
%global	snap 20121126svn5015

Name:		tigervnc
Version:	1.2.80
Release:	0.%{snap}.%{momorel}m%{?dist}
Summary:	A TigerVNC remote display system

Group:		User Interface/Desktops
License:	GPLv2+
URL:		http://www.tigervnc.com/

Source0:	%{name}-%{version}-%{snap}.tar.bz2
#Source0:	http://dl.sourceforge.net/sourceforge/tigervnc/%{name}-%{version}.tar.gz
#NoSource:	0
Source1:        vncserver.service
Source2:        vncserver.sysconfig
Source6:        vncviewer.desktop
Source7:        xserver110.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	libX11-devel, automake, autoconf, libtool, gettext, cvs
BuildRequires:	libXext-devel, xorg-x11-server-source, libXi-devel
BuildRequires:	xorg-x11-xtrans-devel, xorg-x11-util-macros, libXtst-devel
BuildRequires:	libdrm-devel, libXt-devel, pixman-devel libXfont-devel
BuildRequires:	libxkbfile-devel, openssl-devel >= 1.0.0, libpciaccess-devel
BuildRequires:	mesa-libGL-devel, libXinerama-devel, ImageMagick
BuildRequires:  fltk-devel >= 1.3
BuildRequires:  freetype-devel
BuildRequires:	desktop-file-utils
BuildRequires:  gnutls-devel >= 3.2.0
Requires(pre):  hicolor-icon-theme gtk2
Requires:      tigervnc-license
BuildRequires:	systemd-units

%ifarch %ix86 x86_64
BuildRequires: nasm
%endif

Requires(post): systemd-units systemd-sysv chkconfig coreutils
Requires(preun):systemd-units
Requires(postun):systemd-units coreutils

Provides:	vnc = 4.1.3, vnc-libs = 4.1.3
Obsoletes:	vnc < 4.1.3, vnc-libs < 4.1.3
Provides:	tightvnc = 1.5.0
Obsoletes:	tightvnc < 1.5.0

Patch4:         tigervnc-cookie.patch
Patch10:        tigervnc11-ldnow.patch
Patch11:        tigervnc11-gethomedir.patch
Patch13:        tigervnc11-rh692048.patch
Patch14:        tigervnc12-xorg113-glx.patch

%description
Virtual Network Computing (VNC) is a remote display system which
allows you to view a computing 'desktop' environment not only on the
machine where it is running, but from anywhere on the Internet and
from a wide variety of machine architectures.  This package contains a
client which will allow you to connect to other desktops running a VNC
server.

%package server
Summary:	A TigerVNC server
Group:		User Interface/X
Provides:	vnc-server = 4.1.3, vnc-libs = 4.1.3
Obsoletes:	vnc-server < 4.1.3, vnc-libs < 4.1.3
Provides:	tightvnc-server = 1.5.0
Obsoletes:	tightvnc-server < 1.5.0
Requires(post):	chkconfig
Requires(preun):chkconfig
Requires(preun):initscripts
Requires(postun):initscripts

# Check you don't reintroduce #498184 again
Requires:	xorg-x11-fonts-misc

%description server
The VNC system allows you to access the same desktop from a wide
variety of platforms.  This package is a TigerVNC server, allowing
others to access the desktop on your machine.

%package server-minimal
Summary:        A minimal installation of TigerVNC server
Group:          User Interface/X
Requires(post): chkconfig
Requires(preun):chkconfig
Requires(preun):initscripts
Requires(postun):initscripts

Requires:       mesa-dri-drivers, xkeyboard-config, xorg-x11-xkb-utils
Requires:       tigervnc-license

%description server-minimal
The VNC system allows you to access the same desktop from a wide
variety of platforms. This package contains minimal installation
of TigerVNC server, allowing others to access the desktop on your
machine. 

%ifnarch s390 s390x
%package server-module
Summary:        TigerVNC module to Xorg
Group:          User Interface/X
Provides:       vnc-server = 4.1.3, vnc-libs = 4.1.3
Obsoletes:      vnc-server < 4.1.3, vnc-libs < 4.1.3
Provides:       tightvnc-server-module = 1.5.0
Obsoletes:      tightvnc-server-module < 1.5.0
Requires:       xorg-x11-server-Xorg
Requires:       tigervnc-license

%description server-module
This package contains libvnc.so module to X server, allowing others
to access the desktop on your machine.
%endif

%package server-applet
Summary:        Java TigerVNC viewer applet for TigerVNC server
Group:          User Interface/X
Requires:       tigervnc-server
BuildArch:      noarch

%description server-applet 
The Java TigerVNC viewer applet for web browsers. Install this package to allow
clients to use web browser when connect to the TigerVNC server.

%package license
Summary:        License of TigerVNC suite
Group:          User Interface/X
BuildArch:      noarch

%description license
This package contains license of the TigerVNC suite

%prep
#%%setup -q
%setup -q -n %{name}-%{version}-%{snap}

%patch4 -p1 -b .cookie
%patch10 -p1 -b .ldnow
%patch11 -p1 -b .gethomedir
%patch13 -p1 -b .rh692048

cp -r /usr/share/xorg-x11-server-source/* unix/xserver
pushd unix/xserver
for all in `find . -type f -perm -001`; do
        chmod -x "$all"
done
patch -p1 -b --suffix .vnc < ../xserver113.patch
# FIXME
touch hw/dmx/doxygen/doxygen.conf.in xserver.ent.in man/Xserver.man
popd

%patch14 -p1 -b .glx

%build
%ifarch sparcv9 sparc64 s390 s390x
export CFLAGS="$RPM_OPT_FLAGS -fPIC"
%else
export CFLAGS="$RPM_OPT_FLAGS -fpic"
%endif
export CXXFLAGS="$CFLAGS"

%{cmake} .
make %{?_smp_mflags}

pushd unix/xserver
autoreconf -fiv
%configure \
        --disable-xorg --disable-xnest --disable-xvfb --disable-dmx \
        --disable-xwin --disable-xephyr --disable-kdrive --with-pic \
        --disable-static --disable-xinerama \
        --with-default-font-path="catalogue:%{_sysconfdir}/X11/fontpath.d,built-ins" \
        --with-fontdir=%{_datadir}/X11/fonts \
        --with-xkb-output=%{_localstatedir}/lib/xkb \
        --enable-install-libxf86config \
        --enable-glx --disable-dri --enable-dri2 \
        --disable-config-dbus \
        --disable-config-hal \
        --disable-config-udev \
        --with-dri-driver-path=%{_libdir}/dri \
        --without-dtrace \
        --disable-unit-tests \
        --disable-devel-docs \
        --disable-selective-werror

make %{?_smp_mflags}
popd

# Build icons
pushd media
make
popd 

# Build Java applet
pushd java 
%{cmake} . 
make
popd

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

pushd unix/xserver/hw/vnc
make install DESTDIR=$RPM_BUILD_ROOT
popd

# Install systemd unit file
mkdir -p %{buildroot}%{_unitdir}
install -m644 %{SOURCE1} %{buildroot}%{_unitdir}/vncserver@.service
rm -rf %{buildroot}%{_initscriptdir}

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -m644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/vncservers

# Install desktop stuff
mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/{16x16,24x24,48x48}/apps

pushd media/icons
for s in 16 24 48; do
install -m644 tigervnc_$s.png $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${s}x$s/apps/tigervnc.png
done 
popd

mkdir $RPM_BUILD_ROOT%{_datadir}/applications
desktop-file-install \
        --dir $RPM_BUILD_ROOT%{_datadir}/applications \
        %{SOURCE6}

# Install Java applet
pushd java
mkdir -p $RPM_BUILD_ROOT%{_datadir}/vnc/classes
install -m755 VncViewer.jar $RPM_BUILD_ROOT%{_datadir}/vnc/classes
install -m644 com/tigervnc/vncviewer/index.vnc $RPM_BUILD_ROOT%{_datadir}/vnc/classes
popd

%find_lang %{name} %{name}.lang

# remove unwanted files
rm -f  $RPM_BUILD_ROOT%{_libdir}/xorg/modules/extensions/libvnc.la

%clean
rm -rf $RPM_BUILD_ROOT

%post
touch -c %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
        %{_bindir}/gtk-update-icon-cache -q %{_datadir}/icons/hicolor || :
fi

%postun
touch -c %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
        %{_bindir}/gtk-update-icon-cache -q %{_datadir}/icons/hicolor || :
fi

%post server
/bin/systemctl daemon-reload > /dev/null 2>&1

%triggerun -- tigervnc-server < 1.1.0-1m
%{_bindir}/systemd-sysv-convert --save vncserver >/dev/null 2>&1 ||:
/sbin/chkconfig --del vncserver >/dev/null 2>&1 || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README.txt
%{_bindir}/vncviewer
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/applications/*
%{_mandir}/man1/vncviewer.1*

%files server
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/sysconfig/vncservers
%{_unitdir}/vncserver@.service
%{_bindir}/x0vncserver
%{_bindir}/vncserver
%{_mandir}/man1/vncserver.1*
%{_mandir}/man1/x0vncserver.1*

%files server-minimal
%defattr(-,root,root,-)
%{_bindir}/vncconfig
%{_bindir}/vncpasswd
%{_bindir}/Xvnc
%{_mandir}/man1/Xvnc.1*
%{_mandir}/man1/vncpasswd.1*
%{_mandir}/man1/vncconfig.1*

%ifnarch s390 s390x
%files server-module
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/extensions/libvnc.so
%endif

%files server-applet
%defattr(-,root,root,-)
%doc java/com/tigervnc/vncviewer/README
%{_datadir}/vnc/classes/*

%files license
%doc LICENCE.TXT

%changelog
* Sun Mar  9 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.80-0.20121126svn5015.3m)
- fix BuildRequires for recent gnutls

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.80-0.20121126svn5015.2m)
- rebuild against gnutls-3.2.0

* Fri Dec 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.80-0.20121126svn5015.1m)
- update 1.2.80-0.20121126svn5015

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0

* Sat Jan 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-9m)
- fix link error

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-6m)
- full rebuild for mo7 release

* Wed May 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-5m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-4m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-2m)
- use xorg-server-1.6.5 instead of xorg-x11-server-source-1.7.1 or later
- import upstream patches
- revise configure options

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- update to 1.0.0

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.91-1m)
- update to 0.0.91

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.90-3m)
- now --without-dtrace

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.90-2m)
- remove duplicate directories

* Mon May 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.90-1m)
- import from Fedora.
- update 0.0.90-release

* Thu Apr 30 2009 Adam Tkac <atkac redhat com> 0.0.90-0.7.20090427svn3789
- server package now requires xorg-x11-fonts-misc (#498184)

* Mon Apr 27 2009 Adam Tkac <atkac redhat com> 0.0.90-0.6.20090427svn3789
- update to r3789
  - tigervnc-rh494801.patch merged
- tigervnc-newfbsize.patch is no longer needed
- fix problems when vncviewer and Xvnc run on different endianess (#496653)
- UltraVNC and TightVNC clients work fine again (#496786)

* Wed Apr 08 2009 Adam Tkac <atkac redhat com> 0.0.90-0.5.20090403svn3751
- workaround broken fontpath handling in vncserver script (#494801)

* Fri Apr 03 2009 Adam Tkac <atkac redhat com> 0.0.90-0.4.20090403svn3751
- update to r3751
- patches merged
  - tigervnc-xclients.patch
  - tigervnc-clipboard.patch
  - tigervnc-rh212985.patch
- basic RandR support in Xvnc (resize of the desktop)
- use built-in libjpeg (SSE2/MMX accelerated encoding on x86 platform)
- use Tight encoding by default
- use TigerVNC icons

* Tue Mar 03 2009 Adam Tkac <atkac redhat com> 0.0.90-0.3.20090303svn3631
- update to r3631

* Tue Mar 03 2009 Adam Tkac <atkac redhat com> 0.0.90-0.2.20090302svn3621
- package review related fixes

* Mon Mar 02 2009 Adam Tkac <atkac redhat com> 0.0.90-0.1.20090302svn3621
- initial package, r3621
