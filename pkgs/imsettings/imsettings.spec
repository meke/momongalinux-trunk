%global momorel 1
Name:		imsettings
Version:	1.3.1
Release: %{momorel}m%{?dist}
License:	LGPLv2+
URL:		http://code.google.com/p/imsettings/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	desktop-file-utils
BuildRequires:	intltool gettext-devel
BuildRequires:	libtool automake autoconf
BuildRequires:	glib2-devel >= 2.26.0, gobject-introspection-devel, gtk3-devel >= 3.3.3
BuildRequires:	libnotify-devel
BuildRequires:	libX11-devel, libgxim-devel >= 0.3.1
%if !0%{?rhel}
BuildRequires:	xfconf-devel
%endif
Source0:	http://imsettings.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource:	0
Source1:	im-cedilla.conf
Patch0:		imsettings-constraint-of-language.patch
Patch1:		imsettings-disable-xim.patch
Patch2:		imsettings-xinput-xcompose.patch

Summary:	Delivery framework for general Input Method configuration
Group:		Applications/System
Requires:	xorg-x11-xinit >= 1.0.2-22.fc8
Requires:	imsettings-libs = %{version}-%{release}
Requires:	imsettings-desktop-module = %{version}-%{release}
Requires(post):	/bin/dbus-send %{_sbindir}/alternatives
Requires(postun):	/bin/dbus-send %{_sbindir}/alternatives

%description
IMSettings is a framework that delivers Input Method
settings and applies the changes so they take effect
immediately without any need to restart applications
or the desktop.

This package contains the core DBus services and some utilities.

%package	libs
Summary:	Libraries for imsettings
Group:		Development/Libraries

%description	libs
IMSettings is a framework that delivers Input Method
settings and applies the changes so they take effect
immediately without any need to restart applications
or the desktop.

This package contains the shared library for imsettings.

%package	devel
Summary:	Development files for imsettings
Group:		Development/Libraries
Requires:	%{name}-libs = %{version}-%{release}
Requires:	pkgconfig
Requires:	glib2-devel >= 2.26.0

%description	devel
IMSettings is a framework that delivers Input Method
settings and applies the changes so they take effect
immediately without any need to restart applications
or the desktop.

This package contains the development files to make any
applications with imsettings.

%package	xim
Summary:	XIM support on imsettings
Group:		Applications/System
Requires:	%{name} = %{version}-%{release}
Requires:	im-chooser

%description	xim
IMSettings is a framework that delivers Input Method
settings and applies the changes so they take effect
immediately without any need to restart applications
or the desktop.

This package contains a module to get this working with XIM.

%package	gnome
Summary:	GNOME support on imsettings
Group:		Applications/System
Requires:	%{name} = %{version}-%{release}
Requires:	im-chooser dconf
Provides:	imsettings-desktop-module = %{version}-%{release}

%description	gnome
IMSettings is a framework that delivers Input Method
settings and applies the changes so they take effect
immediately without any need to restart applications
or the desktop.

This package contains a module to get this working on
GNOME.

%package	qt
Summary:	Qt support on imsettings
Group:		Applications/System
Requires:	%{name} = %{version}-%{release}
Requires:	im-chooser
Provides:	imsettings-desktop-module = %{version}-%{release}

%description	qt
IMSettings is a framework that delivers Input Method
settings and applies the changes so they take effect
immediately without any need to restart applications
or the desktop.

This package contains a module to get this working on Qt
applications.

%if !0%{?rhel}
%package	xfce
Summary:	Xfce support on imsettings
Group:		Applications/System
Requires:	%{name} = %{version}-%{release}
Requires:	im-chooser
Requires:	xfce4-settings >= 4.5.99.1-2
Provides:	imsettings-desktop-module = %{version}-%{release}

%description	xfce
IMSettings is a framework that delivers Input Method
settings and applies the changes so they take effect
immediately without any need to restart applications
or the desktop.

This package contains a module to get this working on Xfce.  

%package	lxde
Summary:	LXDE support on imsettings
Group:		Applications/System
Requires:	%{name} = %{version}-%{release}
Requires:	lxde-settings-daemon
Requires:	lxsession
Provides:	imsettings-desktop-module = %{version}-%{release}

%description	lxde
IMSettings is a framework that delivers Input Method
settings and applies the changes so they take effect
immediately without any need to restart applications
or the desktop.

This package contains a module to get this working on LXDE.
%endif

%prep
%setup -q
%patch0 -p1 -b .0-lang
%patch1 -p1 -b .1-xim
%patch2 -p1 -b .2-xcompose

%build
%configure	\
	--with-xinputsh=50-xinput.sh \
	--disable-static \
	--disable-schemas-install

%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# change the file attributes
chmod 0755 %{buildroot}%{_libexecdir}/xinputinfo.sh
chmod 0755 %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d/50-xinput.sh

# clean up the unnecessary files
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/imsettings/*.la
%if 0%{?rhel}
rm -f %{buildroot}%{_libdir}/imsettings/libimsettings-{lxde,xfce}.so
%endif

install -m0644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/X11/xinit/xinput.d/im-cedilla.conf

%find_lang %{name}

%clean
rm -rf %{buildroot}


%post
alternatives --install %{_sysconfdir}/X11/xinit/xinputrc xinputrc %{_sysconfdir}/X11/xinit/xinput.d/none.conf 10
alternatives --install %{_sysconfdir}/X11/xinit/xinputrc xinputrc %{_sysconfdir}/X11/xinit/xinput.d/xcompose.conf 20
alternatives --install %{_sysconfdir}/X11/xinit/xinputrc xinputrc %{_sysconfdir}/X11/xinit/xinput.d/xim.conf 30
dbus-send --system --type=method_call --dest=org.freedesktop.DBus / org.freedesktop.DBus.ReloadConfig > /dev/null 2>&1 || :

%postun
if [ "$1" = 0 ]; then
	alternatives --remove xinputrc %{_sysconfdir}/X11/xinit/xinput.d/none.conf
	alternatives --remove xinputrc %{_sysconfdir}/X11/xinit/xinput.d/xcompose.conf
	alternatives --remove xinputrc %{_sysconfdir}/X11/xinit/xinput.d/xim.conf
	dbus-send --system --type=method_call --dest=org.freedesktop.DBus / org.freedesktop.DBus.ReloadConfig > /dev/null 2>&1 || :
fi

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%files	-f %{name}.lang
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%dir %{_libdir}/imsettings
%{_bindir}/imsettings-check
%{_bindir}/imsettings-info
%{_bindir}/imsettings-list
%{_bindir}/imsettings-reload
%{_bindir}/imsettings-switch
%{_libexecdir}/imsettings-daemon
%{_libexecdir}/xinputinfo.sh
%{_datadir}/dbus-1/services/*.service
%{_datadir}/pixmaps/*.png
%{_sysconfdir}/X11/xinit/xinitrc.d/50-xinput.sh
%{_sysconfdir}/X11/xinit/xinput.d
%{_sysconfdir}/xdg/autostart/imsettings-start.desktop

%files	libs
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libimsettings.so.*

%files	devel
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_includedir}/imsettings
%{_libdir}/libimsettings.so
%{_libdir}/pkgconfig/imsettings.pc
%{_libdir}/girepository-*/IMSettings-*.typelib
%{_datadir}/gir-*/IMSettings-*.gir
%{_datadir}/gtk-doc/html/imsettings

%files	xim
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/imsettings-xim
%{_libdir}/imsettings/libimsettings-xim.so

%files	gnome
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/imsettings/libimsettings-gsettings.so
%{_libdir}/imsettings/libimsettings-gconf.so

%files	qt
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/imsettings/libimsettings-qt.so

%if !0%{?rhel}
%files	xfce
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/imsettings/libimsettings-xfce.so

%files	lxde
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/imsettings/libimsettings-lxde.so
%endif


%changelog
* Sat Sep  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-1m)
- reimport from fedora

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9-2m)
- add im-cedilla.conf 

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9-1m)
- import from fedora
