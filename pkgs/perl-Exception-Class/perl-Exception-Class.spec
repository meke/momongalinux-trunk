%global         momorel 1

Name:           perl-Exception-Class
Version:        1.38
Release:        %{momorel}m%{?dist}
Summary:        Module that allows you to declare real exception classes in Perl
License:        "Artistic 2.0"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Exception-Class/
Source0:        http://www.cpan.org/authors/id/D/DR/DROLSKY/Exception-Class-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008001
BuildRequires:  perl-Class-Data-Inheritable >= 0.02
BuildRequires:  perl-Devel-StackTrace >= 1.20
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-Class-Data-Inheritable >= 0.02
Requires:       perl-Devel-StackTrace >= 1.20
Requires:       perl-Scalar-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Exception::Class allows you to declare exception hierarchies in your
modules in a "Java-esque" manner.

%prep
%setup -q -n Exception-Class-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE
%{perl_vendorlib}/Exception/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- rebuild against perl-5.20.0
- update to 1.38

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-2m)
- rebuild against perl-5.16.3

* Mon Feb 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37

* Wed Dec 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-2m)
- rebuild against perl-5.16.2

* Tue Sep 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Mon Sep 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Mon Sep 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-6m)
- rebuild against perl-5.14.0-0.2.1m
- remove BuildRequires: perl-Test-Most

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.32-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.32-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.32-2m)
- full rebuild for mo7 release

* Tue Jun 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Sun Jun 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.29-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.29-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-2m)
- rebuild against perl-5.10.1

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26-2m)
- rebuild against rpm-4.6

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Thu Oct 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.24-2m)
- rebuild against gcc43

* Tue Apr  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.23-2m)
- use vendor

* Sun Apr 08 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- Specfile autogenerated by cpanspec 1.69.1 for Momonga Linux.
