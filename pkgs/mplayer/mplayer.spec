%global momorel 11
# %%global rcver 3

%global src_year 2011
%global src_month 08
%global src_day 03
%global src_date %{src_year}-%{src_month}-%{src_day}
%global rel 0.%{prever}.%{src_year}%{src_month}%{src_day}.%{momorel}
%global src_extract_name %{name}-export-%{src_year}-%{src_month}-%{src_day}
%global prever 995.0.%{src_year}%{src_month}%{src_day}

# The default font size for subtitles : 14, 18, 24 or 28
# the default font encoding, you may easily change to iso-8859-2 if needed
%global fontdir font-arial-iso-8859-1/font-arial-18-iso-8859-1

# default skin
%global skin_name   Blue
%global skin_ver    1.7

Summary: MPlayer, the Movie Player for Linux.
Name: mplayer
Version: 1.0
Release: 0.%{prever}.%{momorel}m%{?dist}
License: see "Copyright"
Group: Applications/Multimedia
URL: http://www.mplayerhq.hu/homepage/
# http://www.mplayerhq.hu/homepage/design6/news.html
# Source0: http://www2.mplayerhq.hu/MPlayer/releases/MPlayer-%%{version}rc%%{rcver}.tar.bz2
Source0: mplayer-export-snapshot.tar.bz2
# svn cp ../ffmpeg/ffmpeg-snapshot.tar.bz2 .
Source1: ffmpeg-snapshot.tar.bz2
# NoSource: 0
Source2: http://www1.mplayerhq.hu/MPlayer/releases/fonts/font-arial-iso-8859-1.tar.bz2
NoSource: 2
Source3: http://www1.mplayerhq.hu/MPlayer/releases/fonts/font-arial-iso-8859-2.tar.bz2
NoSource: 3
Source4: http://www.mplayerhq.hu/MPlayer/Skin/%{skin_name}-%{skin_ver}.tar.bz2
# mplayer-patches-0.97 README
Source1000: mplayer-patches-0.97-readme.tar.gz
Patch1:  MPlayer-1.0rc4pre-configure-xvid.patch
Patch12: %{name}-20090606-lib64-xmmsdir.patch
# patch to fix build failure with GCC4.6
# please remove this when fixed
Patch9999: mplayer-export-2011-01-11-workaournd-for-gcc46.patch
# mplayer-patches-0.97
# Modify the http://2sen.dip.jp/cgi-bin/dtvup/source/up0719.zip
Patch1001: 0001-make-AAC-playout-robust-against-irregular-input.patch
Patch1002: 0002-add-support-for-audio-channel-reconfiguration.patch
Patch1003: 0003-add-switching-auto-selection-features-for-dual-mono-AAC-audio.patch
Patch1004: 0004-restrict-the-switching-of-the-selected-elementary-streams-to-the-ones-within-the-same-program.patch
Patch1005: 0005-extend-DVB-options.patch
Patch1006: 0006-add-support-for-DVB-S2API-with-a-new-format-of-channels.conf..patch
Patch1007: 0007-add-BCAS-descrambling-feature.patch
Patch1008: 0008-support-PMT-contents-change.patch
Patch1009: 0009-support-video-frame-size-change.patch
Patch1010: 0010-fix-sample-rate-calculation-in-SBR-AAC-HE-AAC.patch
Patch1011: 0011-detect-channel-config-change-on-a-stereo-dmono-switching.patch
Patch1012: 0012-fix-the-problem-of-not-allocated-channel-element-in-AAC.patch
Patch1013: 0013-decrease-verbosity-of-an-error-message.patch
Patch1014: 0014-debug.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: cdparanoia
Requires: desktop-file-utils
Requires: gpm
Requires: lirc
Requires: live
Requires: lzo
Requires: xmms
BuildRequires: SDL-devel >= 1.2.7-11m
BuildRequires: arts-devel >= 1.3.0-2m
BuildRequires: cdparanoia-devel
BuildRequires: desktop-file-utils
BuildRequires: esound-devel
BuildRequires: faad2-devel >= 2.7
BuildRequires: glib1-devel
BuildRequires: gpm-devel
BuildRequires: gtk+1-devel
BuildRequires: lame-devel
BuildRequires: libvdpau-devel
BuildRequires: libdv-devel
BuildRequires: libdvdnav-devel >= 0.1.10-3m
BuildRequires: libdvdread-devel
BuildRequires: libfame-devel >= 0.9.1-5m
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libmad
BuildRequires: libmpcdec-devel >= 1.2.6
BuildRequires: libogg-devel
BuildRequires: libpng-devel
BuildRequires: libsmbclient-devel >= 4.0
BuildRequires: libva-devel >= 0.32
BuildRequires: libvorbis-devel
BuildRequires: libvpx-devel >= 1.0.0
BuildRequires: lirc-devel
BuildRequires: live-devel >= 0.20041223
BuildRequires: lzo-devel >= 2.01
BuildRequires: ncurses-devel
BuildRequires: openjpeg-devel >= 1.5.0
BuildRequires: openal-soft-devel
BuildRequires: speex-devel >= 1.2
BuildRequires: x264-devel >= 0.0.2377
BuildRequires: xmms-devel
BuildRequires: xvid

%description
MPlayer is a movie player. It plays most MPEG, VOB, AVI, VIVO, ASF/WMV,
QT/MOV, FLI, NuppelVideo, yuv4mpeg, FILM, RoQ, OGG and some RealMedia
files, supported by many native, XAnim, and Win32 DLL codecs. You can
watch VideoCD, SVCD, DVD, 3ivx, FLI, and even DivX movies too (and you
don't need the avifile library at all!). The another big feature of
mplayer is the wide range of supported output drivers. It works with
X11, Xv, DGA, OpenGL, SVGAlib, fbdev, AAlib, but you can use SDL (and
this way all drivers of SDL), VESA (on every VESA compatible card, even
without X!), and some lowlevel card-specific drivers (for Matrox, 3Dfx
and Radeon) too! Most of them supports software or hardware scaling, so
you can enjoy movies in fullscreen. MPlayer supports displaying through
some hardware MPEG decoder boards, such as the DVB and DXR3/Hollywood+ !
And what about the nice big antialiased shaded subtitles (9 supported
types!!!)

%prep
# %%setup -q -n MPlayer-%%{version}rc%%{rcver}
%setup -q -n %{src_extract_name} -a 1

tar zxvf %{SOURCE1000}

%patch1 -p1 -b .xvidcore

%ifarch x86_64
%patch12 -p1 -b .xmmslibdir
%endif

%patch9999 -p1 -b .gcc46~

%patch1001 -p1
%patch1002 -p1
%patch1003 -p1
%patch1004 -p1
%patch1005 -p1
%patch1006 -p1
%patch1007 -p1
%patch1008 -p1
%patch1009 -p1
%patch1010 -p1
%patch1011 -p1
%patch1012 -p1
%patch1013 -p1
%patch1014 -p1

# mv `ls -d ffmpeg-*` ffmpeg

%build
find . -name "CVS" | xargs rm -rf
%ifarch ppc
CFLAGS="-maltivec -mabi=altivec" ; export CFLAGS
%else
CFLAGS="" ; export CFLAGS
%endif

export CFLAGS="%{optflags} -fomit-frame-pointer"
./configure \
%ifnarch x86_64 ppc64
	--target=%{_target_platform} \
%endif
	--prefix=%{_prefix} \
	--datadir=%{_datadir}/%{name} \
	--confdir=%{_sysconfdir}/%{name} \
	--enable-gui \
	--disable-tv-v4l1 \
	--enable-tv-v4l2 \
	--disable-live \
	--enable-xvid \
	--enable-joystick \
	--disable-directfb \
	--enable-joystick \
	--enable-lirc \
	--enable-smb \
	--enable-menu \
	--enable-vdpau \
%ifarch %{ix86}
	--enable-runtime-cpudetection \
	--enable-win32dll \
	--codecsdir=%{_libdir}/win32 \
	--enable-qtx \
	--enable-real \
	--disable-dvdnav \
	--disable-dvdread-internal \
	--enable-dvdread \
%endif
%ifarch ppc
	--enable-altivec \
%endif
%ifarch ppc64
	--disable-altivec \
%endif
	--enable-xmms \
	--enable-liblzo \
	--language=all \
	--extra-cflags="-I%{_includedir}/samba-4.0" \
	--extra-libs-mplayer="-lgif" \
	--extra-libs-mencoder="-lgif -ldl"

#	--enable-live \

%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} \
	     DATADIR=%{buildroot}%{_datadir}/%{name} \
	     CONFDIR=%{buildroot}%{_sysconfdir}/%{name} \
	     LIBDIR=%{buildroot}%{_libdir} \
	     MANDIR=%{buildroot}%{_mandir} \
	     BINDIR=%{buildroot}%{_bindir}

# The default Skin is Blue
mkdir -p %{buildroot}%{_datadir}/%{name}/skins
pushd %{buildroot}%{_datadir}/%{name}/skins
        tar -x -j -f %{SOURCE4}
	# default is Blue
	ln -s Blue default
popd

# The font for the subtitles
pushd %{buildroot}%{_datadir}/%{name}
	rmdir font || :
	tar -x -j -f %{SOURCE2}
	tar -x -j -f %{SOURCE3}
	ln -s -f %{fontdir} font
popd

# The icon used in the menu entry
#install -D -m 644 gui/%{name}/pixmaps/logo.xpm \
#	%{buildroot}%{_datadir}/pixmaps/%{name}-logo.xpm
install -D -m 644 etc/mplayer.png \
	%{buildroot}%{_datadir}/pixmaps/

%pre
# We have a link... some have an empty dir, this fixes it!
[ -d %{_datadir}/%{name}/font ] && \
	rmdir %{_datadir}/%{name}/font 2>/dev/null || :

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root, 755)
%doc AUTHORS LICENSE README DOCS mplayer-patches-0.97
%dir %{_sysconfdir}/%{name}
%{_bindir}/*
# %{_libdir}/lib*
# %{_libdir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
#%{_datadir}/pixmaps/%{name}-logo.xpm
#%{_datadir}/pixmaps/%{name}.xpm
%{_datadir}/pixmaps/%{name}.png
%{_mandir}/man1/*
%{_mandir}/*/man1/*

%changelog
* Wed Jan 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.995.0.20110803.11m)
- rebuild with x264

* Fri Feb 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.995.0.20110803.10m)
- rebuild with samba-4.0.x
- 8m was skipped...

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110803.9m)
- rebuild against x264-0.0.2200

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.995.0.20110803.7m)
- rebuild against openjpeg-1.5.0

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.995.0.20110803.6m)
- rebuild against libvpx-1.0.0

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110803.5m)
- rebuild against x264-2120

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110803.4m)
- rebuild against x264-2106

* Wed Oct 26 2011 Satoshi Hiruta <minakami@momonga-linux.org> 
- (1.0-0.995.0.20110803.3m)
- add mplayer-patches-0.97
-  from 2sen <http://2sen.dip.jp/cgi-bin/dtvup/source/up0719.zip>

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110803.2m)
- change skin directory

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110803.1m)
- update 20110803

* Sat Jul 30 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (1.0-0.995.0.20110705.2m)
- add config option --disable-dvdread-internal, --enable-dvdread  
-  for gnome-mplayer (playlist crashed without libdvdread support)

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110705.1m)
- update 20110705

* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.995.0.20110515.3m)
- drop v4l support

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110515.2m)
- rebuild against openjpeg-1.4

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110515.1m)
- update 20110515

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.995.0.20110403.3m)
- rebuild for new GCC 4.6

* Sun Apr 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.995.0.20110403.2m)
- add workaround patch for GCC 4.6

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110403.1m)
- update 20110403

* Sun Mar  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110305.1m)
- update 20110305

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20110112.1m)
- update 20110112

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20101209.1m)
- update 20101209

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.995.0.20101111.2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20101111.1m)
- update 20101111
- enable vdpau support

* Tue Nov  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20101026.2m)
- fix BuildRequires

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20101026.1m)
- update 20101026

* Fri Oct 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20101014.1m)
- update 20101014

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.995.0.20100809.2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.995.0.20100809.1m)
- update 20100809

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20100719.1m)
- update 20100719

* Sun Jul  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20100617.2m)
- rebuild against libva-0.31.1-1m

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20100617.1m)
- update 20100617

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.994.0.20100508.1m)
- update 20100508

* Mon May  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.994.0.20100411.3m)
- fix build with new gcc on i686

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.994.0.20100411.2m)
- build with --extra-libs-mplayer="-lgif" and --extra-libs-mencoder="-lgif"

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.994.0.20100411.1m)
- update 1.0-0.994.0.20100411

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.994.0.20100313.2m)
- rebuild against libjpeg-8a

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.994.0.20100313.1m)
- update 1.0rc4-pre20100313

* Thu Feb 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0.994.0.20091225.3m)
- rebuild against x264-0.0.1406-0.20100222.1m

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0.994.0.20091225.2m)
- rebuild against openal-soft-1.10.622

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20091225.1m)
- update 1.0rc4-pre20091225

* Tue Nov 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20091124.1m)
- update 1.0rc4-pre20091124

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.994.0.20091011.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20091011.1m)
- update 1.0rc4-pre20091011

* Sun Sep 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20090920.1m)
- update 1.0rc4-pre20090920

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20090824.2m)
- rebuild against libjpeg-7

* Mon Aug 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.994.0.20090824.1m)
- update 1.0rc4-pre20090802

* Mon Aug  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.993.0.20090802.1m)
- update 20090802

* Sat Aug  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.993.0.20090630.2m)
- [SECURITY] CVE-2010-2062
- import upstream patch (Patch500)

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.993.0.20090630.1m)
- update 20090630

* Sun Jun  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.993.0.20090606.2m)
- update lib64-xmmsdir.patch (to enable build on x86_64)

* Sun Jun  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.993.0.20090606.1m)
- update 20090606 snapshot
-- maybe unsupport DVD-play

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.992.17m)
- rebuild against faad2-2.7

* Wed Mar 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.992.16m)
- rebuild against x264

* Thu Feb 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.992.15m)
- rebuild against x264

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.992.14m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.992.13m)
- [SECURITY] CVE-2008-5616
- apply Patch551 from upstream svn r28150
- License: see "Copyright"

* Wed Dec 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.992.12m)
- apply Patch14 for correct ivtv checking

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-0.992.11m)
- rebuild against x264 and ffmpeg

* Thu Oct  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-0.992.10m)
- [SECURITY] CVE-2008-3827

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.992.9m)
- rebuild against x264

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.992.8m)
- rebuild against libmpcdec-1.2.6
- sort BR

* Wed Jun 18 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-0.992.7m)
- fix args to configure for playing realvideo stream

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.992.6m)
- rebuild against x264

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.992.5m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.992.4m)
- remove BPR libtermcap-devel

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.992.3m)
- [SECURITY] fix following 4 issues
- stack overflow in demux_audio.c
- buffer overflow in demux_mov.c
- buffer overflow in url.c
- buffer overflow in stream_cddb.c
- http://www.mplayerhq.hu/design7/news.html
- thanks, JW

* Fri Dec 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.992.2m)
- fix build on x86_64

* Fri Dec 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.992.1m)
- update 1.0-rc2

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.9.9m)
- revised spec for debuginfo

* Thu Jun  7 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-0.9.8m)
- [SECURITY] CVE-2007-1246 CVE-2007-1387
- add Patch202 mplayer-1.0rc1-CVE-2007-1246.patch
- add Patch203 mplayer-1.0rc1-CVE-2007-1387.patch

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.9.7m)
- [SECURITY] CVE-2007-2948, stack overflow in stream_cddb.c
- add patch201 (cddb_fix_20070605.diff)

* Wed Apr 25 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-0.9.6m)
- add BuildRequires libdvdnav-devel >= 0.1.10-3m

* Wed Apr 25 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-0.9.5m)
-  add BuildRequires libmpcdec-devel, openal-devel

* Thu Apr 05 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-0.9.4m)
- rebuild against x264

* Fri Feb 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.9.3m)
- BuildPreReq: speex-devel

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0-0.9.2m)
- enable ppc64

* Mon Jan  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.9.1m)
- update to 1.0rc1
- [SECURITY] CVE-2006-6172
-  add Patch200: asmrules_fix_20061231.diff
- revise Patch0, Patch4, Patch7
- revise configure option
- use -fomit-frame-pointer, as i386 build fails

* Wed Nov 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-0.8.4m)
-  add Source4 Blue-1.6.tar.bz2

* Tue Nov 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-0.8.3m)
- update Blue skin md5sum

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.8.2m)
- update default skin (Blue) to 1.6 and use %%NoSource

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0-0.8.1m)
- update to 1.0pre8
- do not apply some patches because these are already included upstream
- modify some patches for 1.0pre8

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-0.7.2.6m)
- fixes for build error (why..?)
- - mplayer-1.0pre7try2-func_conflict.patch
- - mplayer-1.0pre7try2-static_declare_round.patch (http://lists.zerezo.com/mplayer-dev-eng/msg00640.html)

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-0.7.2.5m)
- rebuild against x264-0.0.523-0.20060523.2m 
- x264 support can be enabled now
- - add mplayer-1.0pre7try2-x264.patch(http://linuxfromscratch.org/pipermail/patches/2006-January/002764.html)
- disbable-shared-pp since libpostproc is provided by ffmpeg
- - remove include/postproc since this is created even if disable-shared-pp is given

* Sun May 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-0.7.2.4m)
- add xorg7-lib64.patch

* Sat May 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-0.7.2.3m)
- add mplayer-1.0pre7try2-libdvdreadheader_in_cpp.patch for build issue by libdvdread-0.9.6
- update lzo2 patch to mplayer-1.0pre7try2-liblzo2.patch to link liblzo correctly.

* Sun Mar 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0-0.7.2.2m)
- enable ppc, ppc64

* Tue Mar  7 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-0.7.2.1m)
- version up to 1.0pre7try2
- [SECURITY] CVE-2006-0579
-  add  Patch13: demuxer_h_fix_20060212.patch

* Fri Jan  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.7.7m)
- import mplayer-1.0_pre7-gcc4.patch gentoo-x86-portage
 +* 25 Apr 2005; Martin Schlemmer <azarah@gentoo.org>
 +- +files/mplayer-1.0_pre7-gcc4.patch,
 +- Fix building with gcc4. Patch from Genady Okrain (Mafteah) 's overlay (plus
 +- some added fixes to libvo/aclib_template.c that whoever seemed to have
 +  missed).
- import mplayer-1.0_pre7-gcc4-amd64.patch from gentoo-x86-portage
 +* 10 Oct 2005; Luca Barbato <lu_zero@gentoo.org>
 +- +files/mplayer-1.0_pre7-gcc4-amd64.patch, mplayer-1.0_pre7-r1.ebuild:
 +- Fix for gcc-4.0.1 and amd64
- BuildPreReq: libfame-devel >= 0.9.1-5m

* Wed Jan  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.7.6m)
- update playlist.patch for patch-2.5.9-1m

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.7.5m)
- add gcc4 patch
- Patch10: MPlayer-1.0pre7-gcc4.patch

* Wed Oct 19 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0-0.7.4m)
- removed a portion of the libpostproc due to avoid conflict with the ffmpeg.

* Thu Jul 21 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0-0.7.3m)
- rebuild against lzo-2.01

* Sat Jun 11 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-0.7.2m)
- rebuild against lzo-2.00

* Mon May  2 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0-0.7.1m)
- up to 1.0pre7
- [SECURITY] MMST heap overflow, Real RTSP heap overflow

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.0-0.6.9m)
- rebuild against libtermcap-2.0.8-38m.

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-0.6.8m)
- rebuild against SDL

* Thu Feb 24 2005 mutecat <mutecat@momonga-linux.org>
- (1.0.0-0.6.7m)
- arrange ppc add "-maltivec -mabi=altivec"

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.6.6m)
- remove old desktop file

* Thu Feb 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.0-0.6.5m)
- enable x86_64 and ppc.

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.6.4m)
- disable DirectFB

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.0-0.6.2m)
- rebuild with lame, xvid.

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.6.1m)
- rebuild against libfame-0.9.1

* Fri Dec 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.6.1m)
- version 1.0pre6

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.0.0-0.5.4m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sun Aug 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.5.3m)
- rebuild against DirectFB-0.9.21

* Sun Aug 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.5.2m)
- only include Blue skin
- add '--enable-tv-v4l2'
- always add '--with-win32libdir' and '--with-reallibdir'

* Tue Aug 03 2004 TAKAHASHI Tamotsu <tamo>
- (1.0.0-0.5.1m)
- [SECURITY] http://www.open-security.org/advisories/5

* Fri Jun 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.0-0.4.7m)
- rebuild against libdv-0.102

* Thu Jun 17 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.0-0.4.6m)
- DivX handling is missed.

* Wed Jun 16 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.0-0.4.5m)
- revised specopt to build Nonfree version of mplayer.

* Thu Jun  2 2004 Masahoro Takahata <takahata@momonga-linux.org>
- (1.0.0-0.4.4m)
- change source

* Tue May  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.4.3m)
- specfile clean up

* Sun May  2 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.0-0.4.2m)
- rebuild against xvid-1.0.0rc4 (apiversion 4)

* Wed Apr 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.4.1m)
- 1.0pre4 including security fixes

* Wed Mar 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.3.4m)
- apply http://www.mplayerhq.hu/MPlayer/patches/vuln02-fix.diff

* Fri Mar 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.3.3m)
- remove 'BuildPreReq: lame-devel, xvid'

* Thu Jan  8 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0-0.3.2m)
- Add patch0 (mplayer-1.0pre3-nofastmemcpy_in_libpostproc.patch)
- - don't use fast_memcpy in libpostproc.c since it causes "undefined 
  reference to `fast_memcpy'" error when linking against libpostproc.so.

* Fri Dec 12 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0-0.3.1m)
- update to 1.0pre3

* Tue Dec  9 2003 zunda <zunda at freeshell.org>
- (1.0-0.2.3m)
- rebuild against DirectFB

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0-0.2.3m)
- use libmad

* Tue Nov 4 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (1.0-0.2.2m)
- rebuild against samba-3.0.0 (libsmbclient)

* Wed Oct 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.2.1m)
- update to 1.0pre2
- delete Patch0:, this patch was included to 1.0pre2 tarball

* Mon Sep 29 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.1.2m)
- add security fix patch (Patch0)
- add BuildPreReq: gtk+1-devel for gtk-config
- add BuildPreReq: glib1-devel for glib-config

* Thu Sep  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.1.1m)

* Thu Aug 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.91-1m)
- update to 0.91
- no need version fix patch
- move codecs.conf to doc dir

* Wed Jul 9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.90-6m)
- delete Requires: arts-devel

* Tue Jul  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-5m)
- '--disable-divx4linux'

* Tue Jul 8 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.90-4m)
- fix sample %define macro in # line
- the defalut skin becomes Blue
- add --enable-sharedpp option to ./configure

* Fri May 30 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.90-3m)
- add Requires: arts-devel for require /usr/lib/libartscbackend.la
- add define momorel
- update IPv6 patch to mplayer-0.9.0-v6-20030430.diff.gz
- use gcc 3.2.2 instead of 2.95.3

* Thu May 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-2m)
- rebuild against slang(utf8 version)

* Thu Apr 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.90-1m)
- version up to 0.90
- update IPv6 patch (Patch0)
- add BuildPreReq: unzip
- add patch10: change version to 0.90

* Thu Mar 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.15.1m)
- 0.90rc5
- minor bugfixes

* Tue Feb 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.14.1m)
- 0.90rc4
- major bugfixes

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.90-0.13.2m)
- update mplayer patch
  thanks to Shingo TAKEDA ([Momonga-devel.ja:01321])

* Tue Jan 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.13.1m)
- 0.90rc3
- enable directfb again

* Fri Jan 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.12.3m)
- add %%{?include_specopt} to include local configuration

* Thu Jan 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.12.2m)
- temporary? disable directfb

* Wed Dec 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.12.1m)
- 0.90rc2

* Sat Dec 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.11.1m)
- 0.90rc1

* Sat Dec 14 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.90-0.10.2m)
- ipv6 support

* Wed Nov 13 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.10.1m)
- 0.9.0-pre10

* Mon Oct 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.9.1m)
- 0.9.0-pre9

* Wed Sep 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.8.1m)
- 0.9.0-pre8

* Sat Sep 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.7.2m)
- lame support

* Sat Sep  7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-0.7.1m)
- initial import to Momonga
- use gcc_2_95_3
- use xvid instead of divx4linux because Momonga loves opensource solution

* Thu Aug 29 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Added ALSA support.

* Tue Aug  6 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.90pre6.
- Added aalib and lirc build dependencies to get a full-featured binary.
- Updated %%description.
- Added iso-8859-2 font in the package.
- Cleaned up old unuseful hacks.

* Mon Jun 10 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.90pre5.

* Tue May 14 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.90pre4.
- Overwrite the absolute link for the mencoder.1 man page.

* Sat May 11 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Added the mime types for the menu entry.

* Thu May  2 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Added CFLAGS for configure and i18n.
- Rebuilt against Red Hat Linux 7.3.
- Added the %%{?_smp_mflags} expansion.

* Tue Apr 30 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.90pre2.
- Fixed the libGLcore.so.1 and lirc dependencies.
- Build with gcc 2.96 instead of gcc3.

* Fri Apr 26 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.90pre1, fully GPL at last! Here come the binary packages :-)

* Fri Feb  1 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to today's current version.
- Added 3 skins + removed workaround since skin archives are fixed.

* Wed Jan  2 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- %doc cleanup and update to today's build.
- Modified for the new CONFDIR stuff.

* Wed Dec 12 2001 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Initial RPM release.
