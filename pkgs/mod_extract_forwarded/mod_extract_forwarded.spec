%global         momorel 8

Name:           mod_extract_forwarded
Version:        2.0.2
Release:        %{momorel}m%{?dist}
Summary:        Extract real source IP for forwarded HTTP requests

Group:          System Environment/Daemons
License:        Apache
URL:            http://www.openinfo.co.uk/apache/
Source0:        http://www.openinfo.co.uk/apache/extract_forwarded-%{version}.tar.gz
NoSource:       0
Source1:	mod_extract_forwarded.conf
Patch0:         mod_extract_forwarded-2.0.2-httpd24.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  httpd-devel >= 2.4.3
Requires:       httpd httpd-mmn = %([ -a %{_includedir}/httpd/.mmn ] && cat %{_includedir}/httpd/.mmn || echo missing)

%description
mod_extract_forwarded hooks itself into Apache's header parsing phase and looks 
for the X-Forwarded-For header which some (most?) proxies add to the proxied 
HTTP requests. It extracts the IP from the X-Forwarded-For and modifies the 
connection data so to the rest of Apache the request looks like it came from 
that IP rather than the proxy IP.

%prep
%setup -q -n extract_forwarded
%patch0 -p1 -b .httpd24


%build
%{_httpd_apxs} -Wc,"%{optflags}" -c mod_extract_forwarded.c


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/httpd/modules/
mkdir -p %{buildroot}/%{_sysconfdir}/httpd/conf.d/
install -p .libs/mod_extract_forwarded.so %{buildroot}/%{_libdir}/httpd/modules/
install -m644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/httpd/conf.d/mod_extract_forwarded.conf.dist

# Docs don't need to be executable
chmod -x INSTALL README

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc INSTALL README
%{_libdir}/httpd/modules/mod_extract_forwarded.so
%config(noreplace) /etc/httpd/conf.d/mod_extract_forwarded.conf.dist


%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-8m)
- rebuild against httpd-2.4.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-2m)
- rebuild against gcc43

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.2-1m)
- import from Fedora

* Sat Sep 09 2006 Tim Jackson <rpm@timj.co.uk> 2.0.2-2
- Rebuild for FE6

* Wed Jan 11 2006 Tim Jackson <rpm@timj.co.uk> 2.0.2-1
- Initial build
