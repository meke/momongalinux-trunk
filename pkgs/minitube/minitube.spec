%global         momorel 1
%global         qtver 4.7.2
%global         qtdir %{_libdir}/qt4

Name:		minitube
Version:	1.4.2
Release:	%{momorel}m%{?dist}
Summary:	A native YouTube client
Group:		Applications/Internet
License:	LGPL
URL:		http://flavio.tordini.org/minitube
Source0:	http://flavio.tordini.org/files/%{name}/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:		%{name}-1.2-locale-datadir.patch
Patch1:         %{name}-1.4-desktop.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	qt-devel >= %{qtver}
Requires:	qt-x11

%description
Minitube is a native YouTube client. With it you can watch YouTube videos in a 
new way: you type a keyword, Minitube gives you an endless video stream. 
Minitube does not require the Flash Player.

Minitube is not about cloning the original YouTube web interface, it strives to 
create a new TV-like experience.

%prep
%setup -q -n %{name}

%patch0 -p1 -b .localedir
%patch1 -p1 -b .desktop

%build
qmake-qt4 PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_iconsdir}/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    update-desktop-database -q &> /dev/null
    touch --no-create %{_iconsdir}/hicolor &> /dev/null
    gtk-update-icon-cache %{_iconsdir}/hicolor &> /dev/null || :
fi

%posttrans
update-desktop-database -q &> /dev/null
gtk-update-icon-cache %{_iconsdir}/hicolor &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc AUTHORS CHANGES COPYING INSTALL TODO
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_iconsdir}/hicolor/*/apps/*.png
%{_iconsdir}/hicolor/scalable/apps/*.svg

%changelog
* Sat Apr 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-2m)
- rebuild for new GCC 4.6

* Sat Mar 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sat Feb 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-1m)
- update to 1.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-4m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-3m)
- apply desktop.patch

* Tue Oct 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-2m)
- fix up for translations

* Tue Oct 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-2m)
- rebuild against qt-4.6.3-1m

* Wed May  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Sun Jan 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Sat Nov 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Tue Nov 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-1m)
- update to 0.7

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Tue Sep  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- initial build for Momonga Linux
