%global momorel 21

Summary: EB extension for Ruby
Name: ruby-eb
Version: 2.6
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPLv2
URL: http://rubyeb.sourceforge.net/
Source: http://rubyeb.sourceforge.net/rubyeb-%{version}.tar.gz
NoSource: 0
Patch0: ruby-eb-ruby19.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: eb-devel >= 4.4.3
BuildRequires: ruby-devel >= 1.9.2
Requires: ruby

%description
RubyEB is a ruby extension library for "EB Library" to read EB, EBG,
EBXA, EBXA-C and EPWING CD-ROMs.

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q -n rubyeb-%{version}

%patch0 -p1 -b .ruby19

%build
ruby extconf.rb
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING eb.html hook2.rb test.rb
%{ruby_sitearchdir}/eb.so

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6-21m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6-18m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6-17m)
- rebuild against ruby-1.9.2

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-16m)
- rebuild against eb-4.4.3

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-15m)
- rebuild against eb-4.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-13m)
- rebuild against eb-4.4.1-3m

* Sat Mar 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-12m)
- rebuild against eb-4.4.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6-10m)
- rebuild against gcc43

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-9m)
- rebuild against eb-4.3.2

* Sun Jan 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6-8m)
- rebuild against eb-4.3.1
- License: GPLv2

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6-7m)
- rebuild against ruby-1.8.6-4m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6-6m)
- /usr/lib/ruby

* Wed Jun  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.6-5m)
- rebuild against eb-4.2

* Thu Jan 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.6-4m)
- rebuild against eb-4.1.3

* Sat Dec  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6-3m)
- rebuild against eb-4.1.2

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6-2m)
- rebuild against ruby-1.8.2

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6-1m)
- version up for eb-4.1

* Sat Jun 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.5a-2m)
- rebuild against eb-4.1

* Tue Mar 23 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.5a-1m)
- fixed the mispatched eb_set_appendix_subbook()

* Sun Mar 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.5-1m)

* Fri Dec 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.3-3m)
- support decoration
- enable pthread explicitly

* Mon Oct 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.3-2m)
- rebuild agianst eb-4.0

* Tue Aug 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.3-1m)

* Tue Aug 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.2-6m)
- add 'ruby-eb.ruby-1.8.patch'

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2-5m)
- merge from ruby-1_8-branch.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2-4m)
- rebuild against ruby-1.8.0.

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.2-3m)
- rebuild against eb-3.3.2

* Sun Mar  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.2-2m)
- rebuild against eb-3.3.1

* Mon Jan 27 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.2-1m)

* Thu Jan 23 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.1-1m)

* Sat Jan 18 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0-1m)

* Wed Nov 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.7-1m)
- revise URL tag

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-10m)
- rebuild against eb-3.3

* Mon Jul 15 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.6-9m)
- Requires: eb >= 3.2.2-2k
- BuildRequires: eb-devel >= 3.2.2-2k

* Thu Feb 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.6-8k)
- revise a patch for eb-3.2

* Sat Dec 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6-6k)
- rebuild aginst eb-3.2
- add a patch for eb-3.2

* Fri Aug 31 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6-4k)
- Requires eb >= 3.1

* Thu Jul  5 2001 Toru Hoshina <toru@df-usa.com>
- (1.6-2k)

* Sat May 19 2001 Toru Hoshina <toru@df-usa.com>
- (1.5-2k)

* Fri Dec 15 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (14-14k)
- modified BuildPreReq Tag to BuildRequires tag and added version to eb

* Thu Nov  2 2000 Toru Hoshina <toru@df-usa.com>
- no need ruby-eb-extconf.diff.

* Thu Oct 19 2000 Toru Hoshina <toru@df-usa.com>
- it should be suitable to both ruby 1.4.x and 1.6.x.

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against ruby 1.6.1.

* Wed Oct 11 2000 Toru Hoshina <toru@df-usa.com>
- muriyari suitable eb3.0alpha...

* Sat Jun 24 2000 Yuichiro Orino <yuu_@pop21.odn.ne.jp>
- version 1.4

* Sun Apr 23 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group, BuildRoot, Summary )

* Wed Apr 12 2000 Yuichiro Orino <yuu_@pop21.odn.ne.jp>
- initialize
