%global momorel 1

Summary: Portable Sound Event Library
Name: libcanberra
Version: 0.29
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://0pointer.de/lennart/projects/libcanberra/
Source0: http://0pointer.de/lennart/projects/%{name}/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: pkgconfig
BuildRequires: alsa-lib-devel >= 1.0.17a
BuildRequires: pulseaudio-libs-devel >= 0.9.11
BuildRequires: gstreamer-devel >= 0.10.20
BuildRequires: gtk2-devel >= 2.14.2
BuildRequires: gtk3-devel
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: libtdb-devel >= 1.1.1
BuildRequires: libvorbis-devel >= 1.2.0
BuildRequires: systemd-units >= 30
BuildRequires: vala
BuildRequires: libtool-ltdl-devel
BuildRequires: GConf2
BuildRequires: GConf2-devel
BuildRequires: gettext-devel
BuildRequires: systemd-devel >= 187

# for chromium
Provides: libcanberra-gtk2

Requires: sound-theme-freedesktop
Requires: pulseaudio-libs >= 0.9.15
Requires: systemd-units
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units


%description
A small and lightweight implementation of the XDG Sound Theme Specification
(http://0pointer.de/public/sound-theme-spec.html).

%package gtk2
Summary: Gtk+ 2.x Bindings for libcanberra
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
# GConf and other stuff is included in the gtk3 package, so always pull that in.
Requires: %{name}-gtk3 = %{version}-%{release}
Requires(pre): GConf2
Requires(preun): GConf2
Requires(post): GConf2

%description gtk2
Gtk+ 2.x bindings for libcanberra

%package gtk3
Summary: Gtk+ 3.x Bindings for libcanberra
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description gtk3
Gtk+ 3.x bindings for libcanberra

%package devel
Summary: Development Files for libcanberra Client Development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel

%description devel
Development Files for libcanberra Client Development

%prep
%setup -q

%build
%configure \
    --enable-gtk-doc \
    --disable-schemas-install
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
if [ $1 -eq 1 ]; then
        /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ "$1" -eq 0 ]; then
        /bin/systemctl --no-reload disable canberra-system-bootup.service canberra-system-shutdown.service canberra-system-shutdown-reboot.service >/dev/null 2>&1 || :
        /bin/systemctl stop canberra-system-bootup.service canberra-system-shutdown.service canberra-system-shutdown-reboot.service >/dev/null 2>&1 || :
fi

%postun
/sbin/ldconfig
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%pre gtk2
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/libcanberra.schemas \
	> /dev/null || :
fi

%post gtk2
/sbin/ldconfig
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/libcanberra.schemas \
    > /dev/null || :

%preun gtk2
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/libcanberra.schemas \
	> /dev/null || :
fi

%postun gtk2 -p /sbin/ldconfig

%pre gtk3 -p /sbin/ldconfig
%post gtk3 -p /sbin/ldconfig
%preun gtk3 -p /sbin/ldconfig
%postun gtk3 -p /sbin/ldconfig


%files
%defattr(-,root,root)
%doc LGPL README
%{_libdir}/libcanberra.so.*
%dir %{_libdir}/libcanberra-%{version}
%{_libdir}/libcanberra-%{version}/*.so
%{_libdir}/libcanberra-%{version}/*.la
/lib/systemd/system/canberra-system-*.service
%{_bindir}/canberra-boot
%exclude %{_libdir}/*.la

%files gtk2
%defattr(-,root,root)
%{_libdir}/libcanberra-gtk.so.*
%{_libdir}/gtk-2.0/modules/libcanberra-gtk-module.la
%{_libdir}/gtk-2.0/modules/libcanberra-gtk-module.so
#%{_sysconfdir}/gconf/schemas/libcanberra.schemas
%exclude %{_libdir}/*.la

%files gtk3
%defattr(-,root,root)
%{_libdir}/libcanberra-gtk3.so.*
%{_libdir}/gtk-3.0/modules/libcanberra-gtk3-module.la
%{_libdir}/gtk-3.0/modules/libcanberra-gtk3-module.so
%{_libdir}/gtk-3.0/modules/libcanberra-gtk-module.so
%{_bindir}/canberra-gtk-play
%{_datadir}/gnome/autostart/libcanberra-login-sound.desktop
%{_datadir}/gnome/shutdown/libcanberra-logout-sound.sh
%dir %{_datadir}/gdm/
%dir %{_datadir}/gdm/autostart/
%dir %{_datadir}/gdm/autostart/LoginWindow/
%{_datadir}/gdm/autostart/LoginWindow/libcanberra-ready-sound.desktop
%dir %{_libdir}/gnome-settings-daemon-3.0/gtk-modules
%{_libdir}/gnome-settings-daemon-3.0/gtk-modules/canberra-gtk-module.desktop
%exclude %{_libdir}/*.la

%files devel
%defattr(-, root, root)
%doc %{_datadir}/gtk-doc/html/libcanberra
%doc %{_datadir}/doc/libcanberra/README
%{_libdir}/libcanberra-gtk.so
%{_libdir}/libcanberra-gtk3.so
%{_libdir}/libcanberra.so
%{_libdir}/pkgconfig/libcanberra-gtk.pc
%{_libdir}/pkgconfig/libcanberra-gtk3.pc
%{_libdir}/pkgconfig/libcanberra.pc
%{_includedir}/canberra-gtk.h
%{_includedir}/canberra.h

# vala
%dir %{_datadir}/vala
%dir %{_datadir}/vala/vapi
%{_datadir}/vala/vapi/*.vapi

%changelog
* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.29-1m)
- update 0.29

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.28-5m)
- rebuild for glib 2.33.2

* Wed Jan 25 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.28-4m)
- sync fedora

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-3m)
- enable to build with systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.28-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Fri Feb 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-2m)
- rebuild for new GCC 4.5

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.24-4m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.24-3m)
- vapi and gir to devel

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.24-2m)
- add Requires

* Sat Apr 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22-1m)
- update to 0.22
- enable gtk-doc

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18-3m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.0-1m)
- update to 0.18

* Sun May 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sun Feb  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9-1m)
- initial build
