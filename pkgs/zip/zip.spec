%global momorel 4

%global zip_version 30

Summary: A file compression and packaging utility compatible with PKZIP.
Name: zip
Version: 3.0
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Archiving
Source0: http://dl.sourceforge.net/infozip/zip%{zip_version}.tar.gz 
NoSource: 0
URL: http://www.info-zip.org/pub/infozip/Zip.html
# This patch will probably be merged to zip 3.1
# http://www.info-zip.org/board/board.pl?m-1249408491/
Patch1: zip-3.0-exec-shield.patch
# Not upstreamed.
Patch2: zip-3.0-currdir.patch
# Not upstreamed.
Patch3: zip-3.0-time.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The zip program is a compression and file packaging utility.  Zip is
analogous to a combination of the UNIX tar and compress commands and
is compatible with PKZIP (a compression and file packaging utility for
MS-DOS systems).

Install the zip package if you need to compress files using the zip
program.

%prep
%setup -q -n zip30
%patch1 -p1 -b .exec-shield
%patch2 -p1 -b .currdir
%patch3 -p1 -b .time

%build
make -f unix/Makefile prefix=%{_prefix} "CFLAGS_NOOPT=-I. -DUNIX $RPM_OPT_FLAGS" generic_gcc  %{?_smp_mflags}

%install
rm -rf %{buildroot}
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir} 
mkdir -p $RPM_BULD_ROOT%{_mandir}/man1

make -f unix/Makefile prefix=%{buildroot}%{_prefix} \
        MANDIR=%{buildroot}%{_mandir}/man1 install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README CHANGES TODO WHATSNEW WHERE LICENSE README.CR
%doc proginfo/algorith.txt
%{_bindir}/zipnote
%{_bindir}/zipsplit
%{_bindir}/zip
%{_bindir}/zipcloak
%{_mandir}/man1/zip.1*
%{_mandir}/man1/zipcloak.1*
%{_mandir}/man1/zipnote.1*
%{_mandir}/man1/zipsplit.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-2m)
- full rebuild for mo7 release

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-1m)
- update 3.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.31-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.31-6m)
- rebuild against rpm-4.6

* Sun Sep 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.31-5m)
- import patches from Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.31-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.31-3m)
- %%NoSource -> NoSource

* Sat May 27 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.31-2m)
- import patch from fc

* Sat Mar 12 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.31-1m)
  update to 2.31

* Tue Dec 7 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.3-14m)
- [Security] http://www.hexview.com/docs/20041103-1.txt
  patch from
  http://www.st.ryukoku.ac.jp/~kjm/security/ml-archive/full-disclosure/2004.11/msg00211.html
  http://www.st.ryukoku.ac.jp/~kjm/security/ml-archive/full-disclosure/2004.11/msg00217.html

* Sun Oct 26 2003 Kenta MURATA <muraken2@nifty.com>
- (2.3-13m)
- change source url.
- pretty spec file.
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft).

* Fri Dec 6 2002 Nguyen Hung Vu <vuhung@momonga-linux.org>
- (2.3-12m)
- Apply ftp://ftp.freesoftware.com/pub/infozip/src/zcrypt29.tar.gz patch. Now zip support encryption with -e or -P

* Mon Oct 7 2002 Nguyen Hung Vu <vuhung@momonga-linux.org>
- (2.3-11m)
- Nothing changed, just testing my first commit

* Sat Dec 2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3-10k)
- modified spec file and errased zip.fhs.patch for compatibility

* Fri Nov 10 2000 Kenichi Matsubara <m@kondara.org>
- bugfix Source:.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Apr 03 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.3-4)

* Fri Feb 11 2000 Tenkou N. Hattori <tnh@kondara.org>
- remove /usr/bin/zipcloak.

* Mon Feb 7 2000 Bill Nottingham <notting@redhat.com>
- fix some perms

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description
- man pages are compressed

* Tue Jan 11 2000 Bill Nottingham <notting@redhat.com>
- update to 2.3

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Jul 30 1999 Bill Nottingham <notting@redhat.com>
- update to 2.2

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 8)

* Thu Mar 18 1999 Cristian Gafton <gafton@redhat.com>
- updated text in the spec file

* Fri Jan 15 1999 Cristian Gafton <gafton@redhat.com>
- patch top build on the arm

* Mon Dec 21 1998 Michael Maher <mike@redhat.com>
- built package for 6.0

* Mon Aug 10 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
