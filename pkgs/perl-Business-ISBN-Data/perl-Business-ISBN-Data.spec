%global         momorel 7

Name:           perl-Business-ISBN-Data
Version:        20120719.001
Release:        %{momorel}m%{?dist}
Summary:        Data pack for Business::ISBN
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Business-ISBN-Data/
Source0:        http://www.cpan.org/authors/id/B/BD/BDFOY/Business-ISBN-Data-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
You don't need to load this module yourself in most cases. Business::ISBN
will load it when it loads.

%prep
%setup -q -n Business-ISBN-Data-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/Business/ISBN/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719.001-7m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719.001-6m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719.001-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719.001-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719.001-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719.001-2m)
- rebuild against perl-5.16.2

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719.001-1m)
- update to 20120719.001

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719-2m)
- rebuild against perl-5.16.1

* Tue Jul 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20120719-1m)
- update to 20120719

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20081208-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20081208-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20081208-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081208-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081208-2m)
- rebuild against rpm-4.6

* Sun Dec  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20081208-1m)
- update to 20081208

* Tue Oct 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (20081020-1m)
- update to 20081020

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17-2m)
- rebuild against gcc43

* Sun Oct 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Fri Oct 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Fri Aug 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.13-2m)
- use vendor

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Thu Jun  1 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.11-1m)
- update to 1.11

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.10-2m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.10-1m)
- version up to 1.10
- rebuilt against perl-5.8.7

* Wed Dec 22 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.09-2m)
- switch build arch to noarch

* Sat Dec 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.09-1m)
- spec file was semi-autogenerated
