%global momorel 8

Summary: Chemical MIMEs
Name: chemical-mime-data
Version: 0.1.94
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
Source0: http://dl.sourceforge.net/sourceforge/chemical-mime/%{name}-%{version}.tar.bz2 
NoSource: 0
URL: http://chemical-mime.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: libxml2-devel
BuildRequires: libxslt-devel
BuildRequires: ImageMagick
BuildRequires: shared-mime-info
BuildRequires: gnome-mime-data

%description
The chemical-mime-data package is a collection of data files to add
support for various chemical MIME types on Linux/UNIX desktops, such
as KDE and GNOME.

Chemical MIMEs were proposed in 1995, though it seems they have never
been registered with IANA. But they are widely used and the project's
aim is, to support these important, but unofficial MIME types.
#'

%prep
%setup -q

%build
%configure \
    --enable-convert \
    --enable-update-database=no
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%post
update-mime-database %{_datadir}/mime > /dev/null
gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_datadir}/doc/%{name}
%{_datadir}/icons/hicolor/*/mimetypes/*
%{_datadir}/locale/*/*/*
%{_datadir}/mime-info/*
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/mimelnk/chemical/*
%{_datadir}/pixmaps/*.png
%{_datadir}/pkgconfig/%{name}.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.94-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.94-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.94-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.94-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.94-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.94-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.94-2m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.94-2m)
- %%NoSource -> NoSource

* Sun Mar 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.94-1m)
- initial build
