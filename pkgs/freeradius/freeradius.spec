%global         momorel 2

Summary:        High-performance and highly configurable free RADIUS server
Name:           freeradius
Version:        2.2.3
Release:        %{momorel}m%{?dist}
License:        GPLv2+ and LGPLv2+
Group:          System Environment/Daemons
URL:            http://www.freeradius.org/
Source0:        ftp://ftp.freeradius.org/pub/radius/%{name}-server-%{version}.tar.bz2
NoSource:       0
Source100:      radiusd.service
Source102:      freeradius-logrotate
Source103:      freeradius-pam-conf
Source104:      freeradius-tmpfiles.conf
Patch1:         freeradius-cert-config.patch
Patch2:         freeradius-include.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  autoconf
BuildRequires:  gdbm-devel
BuildRequires:  libtool
BuildRequires:  libtool-ltdl-devel
BuildRequires:  net-snmp-devel
BuildRequires:  net-snmp-utils
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  pam-devel
BuildRequires:  zlib-devel
BuildRequires:  libpcap-devel >= 1.1.1
Requires:       net-snmp
Requires:       net-snmp-utils
Requires(pre):  shadow-utils
Requires(postun): glibc
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Obsoletes:      freeradius-dialupadmin >= 2.0
Obsoletes:      freeradius-dialupadmin-ldap >= 2.0
Obsoletes:      freeradius-dialupadmin-mysql >= 2.0
Obsoletes:      freeradius-dialupadmin-postgresql >= 2.0
Obsoletes:      freeradius-libs
Obsoletes:      freeradius-devel

%description
The FreeRADIUS Server Project is a high performance and highly configurable 
GPL'd free RADIUS server. The server is similar in some respects to 
Livingston's 2.0 server.  While FreeRADIUS started as a variant of the 
Cistron RADIUS server, they don't share a lot in common any more. It now has 
many more features than Cistron or Livingston, and is much more configurable.

FreeRADIUS is an Internet authentication daemon, which implements the RADIUS 
protocol, as defined in RFC 2865 (and others). It allows Network Access 
Servers (NAS boxes) to perform authentication for dial-up users. There are 
also RADIUS clients available for Web servers, firewalls, Unix logins, and 
more.  Using RADIUS allows authentication and authorization for a network to 
be centralized, and minimizes the amount of re-configuration which has to be 
done when adding or deleting new users.

%package utils
Group:          System Environment/Daemons
Summary:        FreeRADIUS utilities
Requires:       %{name} = %{version}

%description utils
The FreeRADIUS server has a number of features found in other servers,
and additional features not found in any other server. Rather than
doing a feature by feature comparison, we will simply list the features
of the server, and let you decide if they satisfy your needs.

Support for RFC and VSA Attributes Additional server configuration
attributes Selecting a particular configuration Authentication methods

%package ldap
Summary:        LDAP support for freeradius
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
Requires:       openldap
BuildRequires:  openldap-devel

%description ldap
This plugin provides the LDAP support for the FreeRADIUS server project.

%package krb5
Summary:        Kerberos 5 support for freeradius
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
Requires:       krb5-libs
BuildRequires:  krb5-devel

%description krb5
This plugin provides the Kerberos 5 support for the FreeRADIUS server project.

%package perl
Summary:        Perl support for freeradius
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
Requires:       perl-libs
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
BuildRequires:  perl-devel
BuildRequires:  perl-ExtUtils-Embed

%description perl
This plugin provides the Perl support for the FreeRADIUS server project.

%package python
Summary:        Python support for freeradius
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
Requires:       python-libs
BuildRequires:  python-devel >= 2.7

%description python
This plugin provides the Python support for the FreeRADIUS server project.

%package mysql
Summary:        MySQL support for freeradius
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
Requires:       mysql
BuildRequires:  mysql-devel >= 5.5.10

%description mysql
This plugin provides the MySQL support for the FreeRADIUS server project.

%package postgresql
Summary:        postgresql support for freeradius
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
Requires:       postgresql
BuildRequires:  postgresql-devel

%description postgresql
This plugin provides the postgresql support for the FreeRADIUS server project.

%package unixODBC
Summary:        unixODBC support for freeradius
Group:          System Environment/Daemons
Requires:       %{name} = %{version}-%{release}
Requires:       unixODBC
BuildRequires:  unixODBC-devel >= 2.2.14

%description unixODBC
This plugin provides the unixODBC support for the FreeRADIUS server project.


%prep
%setup -q -n %{name}-server-%{version}
%patch1 -p1 -b .cert-config
%patch2 -p1 -b .include
# Some source files mistakenly have execute permissions set
find $RPM_BUILD_DIR/freeradius-server-%{version} \( -name '*.c' -o -name '*.h' \) -a -perm /0111 -exec chmod a-x {} +

%build
%ifarch s390 s390x
export CFLAGS="$RPM_OPT_FLAGS -fPIC"
%else
export CFLAGS="$RPM_OPT_FLAGS -fpic"
%endif

export LIBTOOL="/usr/bin/libtool --tag=CXX"
%configure \
	--libdir=%{_libdir}/freeradius \
	--with-system-libtool \
	--disable-ltdl-install \
	--with-gnu-ld \
	--with-threads \
	--with-thread-pool \
	--with-docdir=%{_docdir}/freeradius-%{version} \
	--with-rlm-sql_postgresql-include-dir=/usr/include/pgsql \
	--with-rlm-sql-postgresql-lib-dir=%{_libdir} \
	--with-rlm-sql_mysql-include-dir=/usr/include/mysql \
	--with-mysql-lib-dir=%{_libdir}/mysql \
	--with-unixodbc-lib-dir=%{_libdir} \
	--with-rlm-dbm-lib-dir=%{_libdir} \
	--with-rlm-krb5-include-dir=/usr/kerberos/include \
        --with-modules="rlm_wimax" \
	--without-rlm_eap_ikev2 \
	--without-rlm_sql_iodbc \
	--without-rlm_sql_firebird \
	--without-rlm_sql_db2 \
	--without-rlm_sql_oracle

%if "%{_lib}" == "lib64"
perl -pi -e 's:sys_lib_search_path_spec=.*:sys_lib_search_path_spec="/lib64 /usr/lib64 /usr/local/lib64":' libtool
%endif

# dirty hack!
##sed -i 's/LIBTOOL.*/LIBTOOL=\/usr\/bin\/libtool --tag=CXX/g' Make.inc

make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/var/run/radiusd
mkdir -p %{buildroot}/%{_sysconfdir}/tmpfiles.d
mkdir -p %{buildroot}/%{_sysconfdir}/{logrotate.d,pam.d}
mkdir -p %{buildroot}/var/lib/radiusd
# fix for bad libtool bug - can not rebuild dependent libs and bins
#FIXME export LD_LIBRARY_PATH=%{buildroot}/%{_libdir}
make install R=%{buildroot}
# modify default configuration
RADDB=%{buildroot}%{_sysconfdir}/raddb
perl -i -pe 's/^#user =.*$/user = radiusd/'   $RADDB/radiusd.conf
perl -i -pe 's/^#group =.*$/group = radiusd/' $RADDB/radiusd.conf
#ldconfig -n %{buildroot}/usr/lib/freeradius
# logs
mkdir -p %{buildroot}/var/log/radius/radacct
touch %{buildroot}/var/log/radius/{radutmp,radius.log}

install -D -m 755 %{SOURCE100} %{buildroot}/%{_unitdir}/radiusd.service
install -m 644 %{SOURCE102} %{buildroot}/%{_sysconfdir}/logrotate.d/radiusd
install -m 644 %{SOURCE103} %{buildroot}/%{_sysconfdir}/pam.d/radiusd

install -m 0644 %{SOURCE104} %{buildroot}%{_sysconfdir}/tmpfiles.d/%{name}.conf

# remove unneeded stuff
rm -rf doc/00-OLD
rm -f %{buildroot}/usr/sbin/rc.radiusd
rm -rf %{buildroot}/%{_libdir}/freeradius/*.a
rm -rf %{buildroot}/%{_libdir}/freeradius/*.la
rm -rf %{buildroot}/%{_sysconfdir}/raddb/sql/mssql
rm -rf %{buildroot}/%{_sysconfdir}/raddb/sql/oracle
rm -rf %{buildroot}/%{_datadir}/dialup_admin/sql/oracle
rm -rf %{buildroot}/%{_datadir}/dialup_admin/lib/sql/oracle
rm -rf %{buildroot}/%{_datadir}/dialup_admin/lib/sql/drivers/oracle

# remove header files, we don't ship devel package and
# the headers have multilib conflicts
rm -rf %{buildroot}/%{_includedir}

# remove unsupported config files
rm -f %{buildroot}/%{_sysconfdir}/raddb/experimental.conf

# add Red Hat specific documentation
cat >> %{buildroot}/%{_docdir}/freeradius-%{version}/REDHAT << EOF

Red Hat, RHEL, Fedora, and CentOS specific information can be found on the
FreeRADIUS Wiki in the Red Hat FAQ.

http://wiki.freeradius.org/Red_Hat_FAQ

Please reference that document.

EOF

%clean
rm -rf %{buildroot}


%pre
getent group  radiusd >/dev/null || /usr/sbin/groupadd -r -g 95 radiusd
getent passwd radiusd >/dev/null || /usr/sbin/useradd  -r -g radiusd -u 95 -c "radiusd user" -s /sbin/nologin radiusd > /dev/null 2>&1
exit 0

%post
if [ $1 -eq 1 ]; then           # install
  # Initial installation
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
  if [ ! -e /etc/raddb/certs/server.pem ]; then
    /sbin/runuser -g radiusd -c 'umask 007; /etc/raddb/certs/bootstrap' > /dev/null 2>&1
  fi
fi
exit 0

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable radiusd.service > /dev/null 2>&1 || :
    /bin/systemctl stop radiusd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart radiusd.service >/dev/null 2>&1 || :
elif [ $1 -eq 0 ]; then           # uninstall
  getent passwd radiusd >/dev/null && /usr/sbin/userdel  radiusd > /dev/null 2>&1
  getent group  radiusd >/dev/null && /usr/sbin/groupdel radiusd > /dev/null 2>&1
fi
exit 0

%triggerun -- freeradius < 2.1.11-2m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply radiusd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save radiusd >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del radiusd >/dev/null 2>&1 || :
/bin/systemctl try-restart radiusd.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}
%config(noreplace) %{_sysconfdir}/pam.d/radiusd
%config(noreplace) %{_sysconfdir}/logrotate.d/radiusd
%{_unitdir}/radiusd.service
%dir %{_localstatedir}/run/radiusd/
%config(noreplace) %{_sysconfdir}/tmpfiles.d/%{name}.conf
%dir %attr(755,radiusd,radiusd) /var/lib/radiusd
# configs
%dir %attr(755,root,radiusd) /etc/raddb
%defattr(-,root,radiusd)
%attr(644,root,radiusd) %config(noreplace) /etc/raddb/dictionary
%config(noreplace) /etc/raddb/acct_users
%config(noreplace) /etc/raddb/attrs
%config(noreplace) /etc/raddb/attrs.access_challenge
%config(noreplace) /etc/raddb/attrs.access_reject
%config(noreplace) /etc/raddb/attrs.accounting_response
%config(noreplace) /etc/raddb/attrs.pre-proxy
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/clients.conf
%config(noreplace) /etc/raddb/hints
%config(noreplace) /etc/raddb/huntgroups
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/sqlippool.conf
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/preproxy_users
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/proxy.conf
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/radiusd.conf
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/sql.conf
%dir %attr(750,root,radiusd) /etc/raddb/sql
#%attr(640,root,radiusd) %config(noreplace) /etc/raddb/sql/oracle/*
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/users
%dir %attr(770,root,radiusd) /etc/raddb/certs
%config(noreplace) /etc/raddb/certs/Makefile
%config(noreplace) /etc/raddb/certs/README
%config(noreplace) /etc/raddb/certs/xpextensions
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/certs/*.cnf
%attr(750,root,radiusd) /etc/raddb/certs/bootstrap
%dir %attr(750,root,radiusd) /etc/raddb/sites-available
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/sites-available/*
%dir %attr(750,root,radiusd) /etc/raddb/sites-enabled
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/sites-enabled/*
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/eap.conf
%config(noreplace) %attr(640,root,radiusd) /etc/raddb/example.pl
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/policy.conf
%config(noreplace) /etc/raddb/policy.txt
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/templates.conf
%dir %attr(750,root,radiusd) /etc/raddb/modules
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/acct_unique
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/always
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/attr_filter
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/attr_rewrite
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/cache
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/chap
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/checkval
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/counter
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/cui
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/detail
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/detail.example.com
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/detail.log
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/dhcp_sqlippool
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/digest
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/dynamic_clients
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/echo
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/etc_group
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/exec
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/expiration
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/expr
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/files
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/inner-eap
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/ippool
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/logintime
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/linelog
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/mac2ip
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/mac2vlan
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/mschap
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/ntlm_auth
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/opendirectory
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/otp
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/pam
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/pap
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/perl
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/passwd
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/policy
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/preprocess
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/radutmp
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/realm
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/redis
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/rediswho
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/radrelay
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/replicate
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/smbpasswd
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/smsotp
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/soh
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/sql_log
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/sqlcounter_expire_on_login
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/sradutmp
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/unix
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/wimax
%dir %attr(755,radiusd,radiusd) /var/run/radiusd/
# binaries
%defattr(-,root,root)
/usr/sbin/checkrad
/usr/sbin/raddebug
/usr/sbin/radiusd
/usr/sbin/radwatch
/usr/sbin/radmin
# man-pages
%doc %{_mandir}/man5/*
%doc %{_mandir}/man8/*
%exclude %{_mandir}/man8/radsqlrelay.8*
%exclude %{_mandir}/man8/rlm_ippool_tool.8*
# dictionaries
%dir %attr(755,root,root) /usr/share/freeradius
/usr/share/freeradius/*
# logs
%dir %attr(700,radiusd,radiusd) /var/log/radius/
%dir %attr(700,radiusd,radiusd) /var/log/radius/radacct/
%ghost %attr(644,radiusd,radiusd) /var/log/radius/radutmp
%ghost %attr(600,radiusd,radiusd) /var/log/radius/radius.log
# RADIUS shared libs
%attr(755,root,root) %{_libdir}/freeradius/lib*.so*
# RADIUS Loadable Modules
%dir %attr(755,root,root) %{_libdir}/freeradius
#%attr(755,root,root) %{_libdir}/freeradius/rlm_*.so*
#%{_libdir}/freeradius/rlm_acctlog*.so
%{_libdir}/freeradius/rlm_acct_unique.so
%{_libdir}/freeradius/rlm_acct_unique-%{version}.so
%{_libdir}/freeradius/rlm_acctlog.so
%{_libdir}/freeradius/rlm_acctlog-%{version}.so
%{_libdir}/freeradius/rlm_always.so
%{_libdir}/freeradius/rlm_always-%{version}.so
%{_libdir}/freeradius/rlm_attr_filter.so
%{_libdir}/freeradius/rlm_attr_filter-%{version}.so
%{_libdir}/freeradius/rlm_attr_rewrite.so
%{_libdir}/freeradius/rlm_attr_rewrite-%{version}.so
%{_libdir}/freeradius/rlm_cache.so
%{_libdir}/freeradius/rlm_cache-%{version}.so
%{_libdir}/freeradius/rlm_chap.so
%{_libdir}/freeradius/rlm_chap-%{version}.so
%{_libdir}/freeradius/rlm_checkval.so
%{_libdir}/freeradius/rlm_checkval-%{version}.so
%{_libdir}/freeradius/rlm_copy_packet.so
%{_libdir}/freeradius/rlm_copy_packet-%{version}.so
%{_libdir}/freeradius/rlm_counter.so
%{_libdir}/freeradius/rlm_counter-%{version}.so
## FIX ME !!
#{_libdir}/freeradius/rlm_dbm.so
#{_libdir}/freeradius/rlm_dbm-#{version}.so
%{_libdir}/freeradius/rlm_detail.so
%{_libdir}/freeradius/rlm_detail-%{version}.so
%{_libdir}/freeradius/rlm_digest.so
%{_libdir}/freeradius/rlm_digest-%{version}.so
%{_libdir}/freeradius/rlm_dynamic_clients.so
%{_libdir}/freeradius/rlm_dynamic_clients-%{version}.so
%{_libdir}/freeradius/rlm_eap.so
%{_libdir}/freeradius/rlm_eap-%{version}.so
%{_libdir}/freeradius/rlm_eap_gtc.so
%{_libdir}/freeradius/rlm_eap_gtc-%{version}.so
%{_libdir}/freeradius/rlm_eap_leap.so
%{_libdir}/freeradius/rlm_eap_leap-%{version}.so
%{_libdir}/freeradius/rlm_eap_md5.so
%{_libdir}/freeradius/rlm_eap_md5-%{version}.so
%{_libdir}/freeradius/rlm_eap_mschapv2.so
%{_libdir}/freeradius/rlm_eap_mschapv2-%{version}.so
%{_libdir}/freeradius/rlm_eap_peap.so
%{_libdir}/freeradius/rlm_eap_peap-%{version}.so
%{_libdir}/freeradius/rlm_eap_sim.so
%{_libdir}/freeradius/rlm_eap_sim-%{version}.so
%{_libdir}/freeradius/rlm_eap_tls.so
%{_libdir}/freeradius/rlm_eap_tls-%{version}.so
%{_libdir}/freeradius/rlm_eap_ttls.so
%{_libdir}/freeradius/rlm_eap_ttls-%{version}.so
%{_libdir}/freeradius/rlm_exec.so
%{_libdir}/freeradius/rlm_exec-%{version}.so
%{_libdir}/freeradius/rlm_expiration.so
%{_libdir}/freeradius/rlm_expiration-%{version}.so
%{_libdir}/freeradius/rlm_expr.so
%{_libdir}/freeradius/rlm_expr-%{version}.so
%{_libdir}/freeradius/rlm_fastusers.so
%{_libdir}/freeradius/rlm_fastusers-%{version}.so
%{_libdir}/freeradius/rlm_files.so
%{_libdir}/freeradius/rlm_files-%{version}.so
%{_libdir}/freeradius/rlm_ippool.so
%{_libdir}/freeradius/rlm_ippool-%{version}.so
%{_libdir}/freeradius/rlm_linelog.so
%{_libdir}/freeradius/rlm_linelog-%{version}.so
%{_libdir}/freeradius/rlm_logintime.so
%{_libdir}/freeradius/rlm_logintime-%{version}.so
%{_libdir}/freeradius/rlm_mschap.so
%{_libdir}/freeradius/rlm_mschap-%{version}.so
%{_libdir}/freeradius/rlm_otp.so
%{_libdir}/freeradius/rlm_otp-%{version}.so
%{_libdir}/freeradius/rlm_pam.so
%{_libdir}/freeradius/rlm_pam-%{version}.so
%{_libdir}/freeradius/rlm_pap.so
%{_libdir}/freeradius/rlm_pap-%{version}.so
%{_libdir}/freeradius/rlm_passwd.so
%{_libdir}/freeradius/rlm_passwd-%{version}.so
%{_libdir}/freeradius/rlm_policy.so
%{_libdir}/freeradius/rlm_policy-%{version}.so
%{_libdir}/freeradius/rlm_preprocess.so
%{_libdir}/freeradius/rlm_preprocess-%{version}.so
%{_libdir}/freeradius/rlm_radutmp.so
%{_libdir}/freeradius/rlm_radutmp-%{version}.so
%{_libdir}/freeradius/rlm_realm.so
%{_libdir}/freeradius/rlm_realm-%{version}.so
%{_libdir}/freeradius/rlm_replicate.so
%{_libdir}/freeradius/rlm_replicate-%{version}.so
%{_libdir}/freeradius/rlm_soh.so
%{_libdir}/freeradius/rlm_soh-%{version}.so
%{_libdir}/freeradius/rlm_sql.so
%{_libdir}/freeradius/rlm_sql-%{version}.so
%{_libdir}/freeradius/rlm_sql_log.so
%{_libdir}/freeradius/rlm_sql_log-%{version}.so
%{_libdir}/freeradius/rlm_sqlcounter.so
%{_libdir}/freeradius/rlm_sqlcounter-%{version}.so
%{_libdir}/freeradius/rlm_sqlippool.so
%{_libdir}/freeradius/rlm_sqlippool-%{version}.so
%{_libdir}/freeradius/rlm_unix.so
%{_libdir}/freeradius/rlm_unix-%{version}.so
%{_libdir}/freeradius/rlm_wimax.so
%{_libdir}/freeradius/rlm_wimax-%{version}.so

%files utils
%defattr(-,root,root,-)
/usr/bin/*
# man-pages
%doc %{_mandir}/man1/*
%doc %{_mandir}/man8/radsqlrelay.8*
%doc %{_mandir}/man8/rlm_ippool_tool.8*

%files krb5
%defattr(-,root,root,-)
%{_libdir}/freeradius/rlm_krb5.so
%{_libdir}/freeradius/rlm_krb5-%{version}.so
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/krb5

%files perl
%defattr(-,root,root,-)
%{_libdir}/freeradius/rlm_perl.so
%{_libdir}/freeradius/rlm_perl-%{version}.so

%files python
%defattr(-,root,root,-)
%{_libdir}/freeradius/rlm_python.so
%{_libdir}/freeradius/rlm_python-%{version}.so

%files mysql
%defattr(-,root,root,-)
%dir %attr(750,root,radiusd) /etc/raddb/sql/mysql
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/sql/mysql/*
%dir %attr(750,root,radiusd) /etc/raddb/sql/ndb
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/sql/ndb/*
%{_libdir}/freeradius/rlm_sql_mysql.so
%{_libdir}/freeradius/rlm_sql_mysql-%{version}.so

%files postgresql
%defattr(-,root,root,-)
%dir %attr(750,root,radiusd) /etc/raddb/sql/postgresql
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/sql/postgresql/*
%{_libdir}/freeradius/rlm_sql_postgresql.so
%{_libdir}/freeradius/rlm_sql_postgresql-%{version}.so

%files ldap
%defattr(-,root,root,-)
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/ldap.attrmap
%{_libdir}/freeradius/rlm_ldap.so
%{_libdir}/freeradius/rlm_ldap-%{version}.so
%attr(640,root,radiusd) %config(noreplace) /etc/raddb/modules/ldap

%files unixODBC
%defattr(-,root,root,-)
%{_libdir}/freeradius/rlm_sql_unixodbc.so
%{_libdir}/freeradius/rlm_sql_unixodbc-%{version}.so

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-2m)
- rebuild against perl-5.20.0

* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-1m)
- [SECURITY] CVE-2014-2015
- update to 2.2.3

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-6m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-2m)
- rebuild against perl-5.16.2

* Fri Sep 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- [SECURITY] CVE-2012-3547
- update to 2.2.0

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.12-1m)
- update to 2.1.12

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.11-6m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.11-5m)
- rebuild against perl-5.16.0

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.11-4m)
- rebuild against net-snmp-5.7.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.11-3m)
- rebuild against perl-5.14.2

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.11-2m)
- support systemd

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.11-1m)
- update 2.1.11

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.10-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.10-5m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.10-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.10-3m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.10-2m)
- rebuild against mysql-5.5.10

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.10-1m)
- update to 2.1.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.9-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.9-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.9-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.9-1m)
- update to 2.1.9
- almost sync with Fedora devel

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-9m)
- rebuild against perl-5.12.1

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.3-8m)
- rebuild against readline6

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-7m)
- rebuild against perl-5.12.0

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.3-6m)
- rebuild against libpcap-1.1.1

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.3-5m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-3m)
- rebuild against perl-5.10.1

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.4-2m)
- rebuild against unixODBC-2.2.14

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.4-1m)
- update 2.1.4

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-8m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-7m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-6m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.2-5m)
- rebuild against python-2.6.1-2m

* Wed Dec 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-4m)
- [SECURITY] CVE-2008-4474
- apply Patch1 based on upstream patch and Debian unstable (2.0.4+dfsg-6)
-- http://bugs.freeradius.org/show_bug.cgi?id=605

* Tue Oct  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-3m)
- fix %%files utils package

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-2m)
- modify %%files for smart handling of a directory

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.2-1m)
- verion up 2.0.2
- sync Fedora

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.7-3m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.7-2m)
- rebuild against openldap-2.4.8

* Tue Jan  8 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.7-1m)
- sync from fedora 8
- add gdbm183 patch for rla_dbm module

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.6-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.6-2m)
- rebuild against net-snmp

* Sun Jun  3 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.6-1m)
- [SECURITY] CVE-2007-2028
- sync FC-devel (freeradius-1.1.5-1)

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-7m)
- change Source URL

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-6m)
- rebuild against postgresql-8.2.3

* Tue Jan  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.1-5m)
- add BuildRequires: libtool-ltdl-devel

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-4m)
- rebuild against python-2.5

* Wed Aug  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.1-3m)
- rebuild against net-snmp 5.3.1.0

* Tue May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.1.1-1m)
- rebuild against mysql 5.0.22-1m

* Sun Apr 23 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.1-1m)
- [SECURITY] CVE-2006-1354 CVE-2005-1454 CVE-2005-1455
- http://www.freeradius.org/security.html
- add Patch10: freeradius-1.1.1-libradius_install.patch (from Gentoo)
-     Patch11: freeradius-1.1.1-versionless-la-files.patch (from Gentoo)
-     Patch12: freeradius-1.1.1-docdir.patch
- update Patch2: freeradius-1.1.1-libdir.patch
-        Patch5: freeradius-1.1.1-pie.patch
- remove Patch1: freeradius-1.0.0-ltdl_no_la.patch
-        Patch6: freeradius-0.9.3-gcc34.patch
-        Patch7: freeradius-1.0.2-sasl2.patch
- remove --disable-static option for specific BUG at version 1.1.1 
- add REMOVE.PLEASE

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2-6m)
- rebuild against openldap-2.3.19

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2-5m)
- rebuild against openldap-2.3.11

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.2-4m)
- rebuild against postgresql-8.1.0

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.0.2-3m)
- rebuild against for MySQL-4.1.14

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.2-2m)
- rebuild against postgresql-8.0.2.

* Thu Mar 10 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.2-1m)
- version up since 1.0.1 tarball moved to old/
- Provides and Obsoletes freeradius-pgsql
- Provides and Obsoletes freeradius-unixodbc

* Thu Mar 10 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.1-1m)
- sync with FC3. specopt is fully disabled.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.3-7m)
- enable x86_64.

* Wed Oct 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.9.3-6m)
- add gcc34 patch

* Mon Aug 23 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.3-5m)
  revised source url

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.3-4m)
- stop daemon

* Thu Apr  8 2004 Hiroyuki Koga <kuma@momonga-inux.org>
- (0.9.3-3m)
- correct comment out for _initscriptdir macro
- use %%NoSource and %%make
- add Prereq: chkconfig, initscripts

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9.3-2m)
- revised spec for enabling rpm 4.2.
- add postgresql and unixodbc support packages.

* Mon Jan 26 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Tue Nov 18 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.9.1-3m)
- add BuildConflicts: db2-devel
- add patch to be compiled with gdbm-1.8.3(libgdbm_compat)
- fix install directory of README.rpm

* Mon Nov 10 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.1-2m)
- rebuild against gdbm-1.8.0-19m

* Mon Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.1-1m)
- version 0.9.1

* Wed Sep  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.8.1-6m)
- rebuild against for MySQL-4.0.14-1m
- License: GPL
- BuildRoot: %{_tmppath}/%{name}-%{version}-root

* Thu Jun 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.8.1-5m)
  rebuild against cyrus-sasl2

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.8.1-4m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.1-3m)
- rebuild against for gdbm

* Sun Feb  9 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.8.1-2m)
- %%install: rm -rf %{buildroot} before install

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.8.1-1m)
  update to 0.8.1

* Sun Jan 05 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (0.7.1-2m)
- fix Source URI.

* Fri Nov 01 2002 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- This RPM were created by Shawn K. O'Shea <pkg@eth0.net>
- get from http://volcano.boulderhill.net/freeradius-rpm/
- see http://volcano.boulderhill.net/freeradius-rpm/README.rpm.txt
- Momongized.
- NoSource.
- update to 0.7.1
- enable support mysql and ldap as default
