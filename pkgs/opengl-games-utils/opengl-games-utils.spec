%global momorel 6

Name:           opengl-games-utils
Version:        0.1
Release:        %{momorel}m%{?dist}
Summary:        Utilities to check proper 3d support before launching 3d games
Group:          Amusements/Games
License:        Public Domain
URL:            http://fedoraproject.org/wiki/SIGs/Games
Source0:        opengl-game-wrapper.sh
Source1:        opengl-game-functions.sh
Source2:        README
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
Requires:       zenity 
Requires:	glx-utils

%description
This package contains various shell scripts which are intented for use by
3D games packages. These shell scripts can be used to check if direct rendering
is available before launching an OpenGL game. This package is intended for use
by other packages and is not intended for direct end user use!

%prep
%setup -c -T
cp %{SOURCE2} .

%build
# nothing to build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
install -p -m 755 %{SOURCE0} $RPM_BUILD_ROOT%{_bindir}
install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README
%{_bindir}/opengl-game-wrapper.sh
%{_datadir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-2m)
- rebuild against rpm-4.6

* Sun Jun 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-1m)
- import from Fedora devel

* Fri Jan 11 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.1-5
- Fix DRI detection to work with dual head configurations

* Tue Oct 16 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.1-4
- Fix a minor spelling error in the dialog shown when DRI is not available

* Mon Sep 24 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.1-3
- Put DRI checking functionality in a checkDriOK bash function in
  opengl-game-functions.sh, for usage from existing wrapper scripts

* Sun Sep 16 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.1-2
- Various spelling fixes thanks to Rahul Sundaram

* Sun Sep 16 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 0.1-1
- Initial Fedora package
