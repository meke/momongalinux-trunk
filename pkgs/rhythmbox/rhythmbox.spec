%global momorel 4

Summary:     A music management application, originally inspired by Apple's iTunes
Name:        rhythmbox
Version:     2.98
Release:     %{momorel}m%{?dist}
License:     GPL
Group:       Applications/Multimedia
URL:         http://projects.gnome.org/rhythmbox/
Source0:     http://ftp.gnome.org/pub/GNOME/sources/%{name}/%{version}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): gtk2
Requires(pre): hicolor-icon-theme
Requires(post): coreutils
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: gnome-icon-theme-extras
Requires: gnome-icon-theme-legacy
Requires: gstreamer-plugins-base
Requires: gstreamer-plugins-good
Requires: gstreamer-python
Requires: gtk3
Requires: gvfs-afc
Requires: media-player-info
Requires: gnome-python2-gconf
Requires: pygobject3
Requires: python-mako
Requires: pywebkitgtk
BuildRequires: GConf2-devel
BuildRequires: avahi-glib-devel
BuildRequires: brasero-devel >= 2.32.0
BuildRequires: clutter-gtk-devel
BuildRequires: clutter-gst-devel
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: evolution-data-server >= 3.0
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: gnome-common
BuildRequires: gnome-doc-utils
BuildRequires: gobject-introspection-devel
BuildRequires: grilo-devel
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: gstreamer-python-devel
BuildRequires: gtk3-devel
BuildRequires: gvfs-devel >= 1.8.0
BuildRequires: json-glib-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel >= 1.2
# BuildRequires: libdmapsharing-devel
BuildRequires: libgnome-keyring-devel
BuildRequires: libgnome-media-profiles-devel
BuildRequires: libgpod-devel >= 0.7.0
BuildRequires: libgudev1-devel
BuildRequires: libimobiledevice-devel >= 1.1.5
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: libmtp-devel >= 1.1.0
BuildRequires: libmusicbrainz-devel
BuildRequires: libmx-devel
BuildRequires: libpeas-devel
BuildRequires: libsoup-devel
BuildRequires: libtdb-devel
BuildRequires: libxcb-devel >= 1.2
BuildRequires: lirc-devel
BuildRequires: perl-XML-Parser
BuildRequires: pkgconfig
BuildRequires: pygtk2
BuildRequires: pygobject3-devel
BuildRequires: python >= 2.7
BuildRequires: scrollkeeper
BuildRequires: totem >= 2.23.2-1m
BuildRequires: totem-pl-parser-devel >= 2.29.1
BuildRequires: usbmuxd-devel >= 1.0.8
BuildRequires: webkitgtk3-devel >= 1.3.4
BuildRequires: zlib-devel
Obsoletes: %{name}-upnp

%description
Rhythmbox is an integrated music management application based on the powerful
GStreamer media framework. It has a number of features, including an easy to
use music browser, searching and sorting, comprehensive audio format support
through GStreamer, Internet Radio support, playlists and more.

Rhythmbox is extensible through a plugin system.

%package lirc
Summary: LIRC (Infrared remote) plugin for Rhythmbox
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description lirc
This package contains a plugin to add LIRC (Infrared remote) support
to Rhythmbox.

%package devel
Summary: Development files for Rhythmbox plugins
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This package contains the development files necessary to create
a Rhythmbox plugin.

%prep
%setup -q

%build

# work around a gstreamer bug
/usr/bin/gst-inspect-0.10 --print-all >& /dev/null || :

%configure \
    --with-ipod \
    --with-mdns=avahi \
    --disable-scrollkeeper \
    --with-gnome-keyring \
    --disable-vala \
    --without-hal

%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

# get rid of *a files
rm -f %{buildroot}%{_libdir}/rhythmbox/plugins/*.{a,la}
rm -f %{buildroot}%{_libdir}/rhythmbox/plugins/*/*.{a,la}
rm -f %{buildroot}%{_libdir}/librhythmbox-core.{a,la}
rm -f %{buildroot}%{_libdir}/mozilla/plugins/*.{a,la}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
update-desktop-database -q
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
/sbin/ldconfig
update-desktop-database -q
if [ $1 -eq 0 ]; then
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :

%files
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog DOCUMENTERS INSTALL INTERNALS MAINTAINERS* NEWS README THANKS
%{_bindir}/%{name}
%{_bindir}/%{name}-client
%{_libdir}/girepository-1.0/*-3.0.typelib
%{_libdir}/mozilla/plugins/lib%{name}-itms-detection-plugin.so
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/plugins
%{_libdir}/%{name}/plugins/artsearch
%{_libdir}/%{name}/plugins/audiocd
%{_libdir}/%{name}/plugins/audioscrobbler
%{_libdir}/%{name}/plugins/cd-recorder
%{_libdir}/%{name}/plugins/context
%{_libdir}/%{name}/plugins/dbus-media-server
%{_libdir}/%{name}/plugins/fmradio
%{_libdir}/%{name}/plugins/generic-player
%{_libdir}/%{name}/plugins/grilo
%{_libdir}/%{name}/plugins/im-status
%{_libdir}/%{name}/plugins/ipod
%{_libdir}/%{name}/plugins/iradio
%{_libdir}/%{name}/plugins/lyrics
%{_libdir}/%{name}/plugins/magnatune
%{_libdir}/%{name}/plugins/mmkeys
%{_libdir}/%{name}/plugins/mpris
%{_libdir}/%{name}/plugins/mtpdevice
%{_libdir}/%{name}/plugins/notification
%{_libdir}/%{name}/plugins/power-manager
%{_libdir}/%{name}/plugins/python-console
%{_libdir}/%{name}/plugins/rb
%{_libdir}/%{name}/plugins/rbzeitgeist
%{_libdir}/%{name}/plugins/replaygain
%{_libdir}/%{name}/plugins/sendto
%{_libdir}/%{name}/plugins/visualizer
%{_libdir}/lib%{name}-core.so.*
%{_libexecdir}/%{name}-metadata
%{_datadir}/applications/%{name}.desktop
%{_datadir}/applications/%{name}-device.desktop
%{_datadir}/dbus-1/services/org.gnome.Rhythmbox3.service
%{_datadir}/glib-2.0/schemas/org.gnome.%{name}.gschema.xml
%{_datadir}/gnome/help/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/icons/hicolor/*/places/music-library.png
%{_datadir}/icons/hicolor/*/status/%{name}-missing-artwork.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}-symbolic.svg
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}-client.1.*
%{_mandir}/man1/%{name}.1.*
%{_datadir}/omf/%{name}
%{_datadir}/%{name}

%files lirc
%defattr(-, root, root)
%{_libdir}/%{name}/plugins/rblirc

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}-core.so
%{_libdir}/%{name}/sample-plugins
%{_datadir}/gir-1.0/*-3.0.gir
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.98-4m)
- rebuild against libimobiledevice-1.1.5

* Mon Jan 28 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.98-3m)
- remove --disable-grilo

* Mon Jan 28 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.98-2m)
- add --disable-grilo

* Sun Jan 27 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.98-1m)
- update 2.98

* Wed Aug 29 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.97-2m)
- rebuild against libimobiledevice-1.1.4 and usbmuxd-1.0.8

* Fri Jul 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.97-1m)
- version 2.97
- remove package upnp
- sort Requires, BRs and %%files
- modify %%post, %%postun and %%posttrans
- build without grilo and libdmapsharing for the moment

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.3-8m)
- rebuild for glib 2.33.2

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.3-7m)
- rebuild against libmtp-2.7

* Mon Jun 27 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.3-6m)
- add BuildRequires

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.3-5m)
- rebuild against evolution-data-server
-- delete cd-recorder ?

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.3-4m)
- rebuild against python-2.7

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.3-3m)
- rebuild against libnotify-0.7.2

* Sat Apr 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.3-2m)
- do not use libnotify-0.7.2

* Fri Apr 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.3-1m)
- update to 0.13.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.1-5m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.1-4m)
- rebuild against gvfs

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.1-3m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.1-2m)
- rebuild against webkitgtk-1.3.4

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.1-1m)
- update to 0.13.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.0-3m)
- full rebuild for mo7 release

* Wed Aug  4 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.13.0-2m)
- sync Fedora

* Sat Jul  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.0-1m)
- update to 0.13.0

* Mon May 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.8-2m)
- add BuildRequires

* Mon Apr 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.8-1m)
- update to 0.12.8

* Mon Mar  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.7-1m)
- update to 0.12.7

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.6-1m)
- rebuild against totem-pl-parser-2.29.1

* Mon Nov 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.6-1m)
- update to 0.12.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.5-2m)
- good-bye nautilus-cd-burner

* Sat Sep 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.5-1m)
- update to 0.12.5

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.3-1m)
- update to 0.12.3

* Sun May 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.2-1m)
- update to 0.12.2

* Tue Apr 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Thu Mar 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Wed Mar 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.99.2-1m)
- update 0.12 pre

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.6-10m)
- rebuild against libgpod-0.7.0

* Wed Feb 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.6-9m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.6-8m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11.6-7m)
- rebuild against python-2.6.1-2m

* Tue Nov  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.6-6m)
- rebuild against libmtp-0.3.3
- import libmtp-0.3.0-API.patch from gentoo
 +- 06 Jul 2008; Peter Alfredsen <loki_val@gentoo.org>
 +- +files/rhythmbox-0.11.5-libmtp-0.3.0-API.patch, rhythmbox-0.11.5.ebuild,
 +- +rhythmbox-0.11.5-r1.ebuild:
 +- Revbump with patch for libmtp-0.3.0 API change

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.6-5m)
- rebuild against evolution-data-server-2.24.0

* Wed Sep  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.6-4m)
- add Requires: gstreamer-python

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.6-3m)
- change Requires: gst-plugins-base to gstreamer-plugins-base
- change Requires: gst-plugins-good to gstreamer-plugins-good

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.6-2m)
- rebuild against gnutls-2.4.1 (libsoup-2.2)

* Sat Jul 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.6-1m)
- update to 0.11.6
- comment out many patches

* Sun Jun 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.5-1m)
- update 0.11.5

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.3-6m)
- rebuild against totem-2.23.2-1m for firefox-3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.3-6m)
- rebuild against gcc43

* Fri Mar 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.3-5m)
- rebuild against totem-2.22.0

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.3-4m)
- rebuild against libsoup-2.2.105
- BPR libsoup-2.2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.3-3m)
- %%NoSource -> NoSource

* Wed Nov 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.3-2m)
- rebuild against libgpod-0.6.0

* Fri Nov  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.3-1m)
- update to 0.11.3

* Fri Oct 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.2-4m)
- rebuild against libmtp-0.2.3

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.2-3m)
- rebuild against libgpod-0.5.2 and libmtp-0.2.1

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.2-2m)
- rebuild against totem-2.20.0

* Fri Aug 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.2-1m)
- update to 0.11.2

* Mon May 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update 0.10.1

* Mon Apr  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.0-1m)
- update 0.10.0

* Sat Mar  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.8-2m)
- update patch (eel_strdup_strftime)

* Sat Feb 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Fri Jan 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.7-2m)
- rebuild against libgpod-0.4.2

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Sun Oct  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6
-- delete patch1 (for dbus)

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.9.5-7m)
- rebuild against gnutls-1.4.4-1m

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.5-6m)
- rebuild against nautilus-cd-burner-2.16.0 libgpod-devel-0.3.2-3m

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.5-5m)
- rebuild against dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.9.5-4m)
- rebuild against expat-2.0.0-1m

* Fri Jul 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.5-3m)
- add BuildPrereq: libgpod-devel
- add patch0 (eel_strdup_strftime)

* Wed Jul 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.5-2m)
- add --with-check=no to configure

* Wed Jul 12 2006 imuchin <imuchin777@m3.dion.ne.jp>
- (0.9.5-1m)
- initial build
