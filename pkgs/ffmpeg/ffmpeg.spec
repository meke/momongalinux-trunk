%global momorel 1

Summary: FFmpeg Streaming Multimedia System
Name: ffmpeg
Version: 2.2.2
%global src_name %{name}-%{version}

#Release: %{rel}.%{momorel}m%{?dist}
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: Development/Libraries
Source0: http://www.ffmpeg.org/releases/ffmpeg-%{version}.tar.bz2
NoSource: 0

URL: http://ffmpeg.mplayerhq.hu/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libvorbis-devel, a52dec-devel, lame-devel
BuildRequires: libvpx-devel >= 1.0.0
BuildRequires: libtheora-devel, faac-devel, speex-devel
BuildRequires: x264-devel >= 0.0.2377, libdca, dirac-devel
BuildRequires: zlib-devel >= 1.1.4-5m, SDL-devel, freetype-devel, imlib2-devel
BuildRequires: texi2html
BuildRequires: libva >= 1.0
BuildRequires: schroedinger-devel
# BuildRequires: opencv-devel >= 2.1.0
BuildRequires: openjpeg-devel >= 1.5.0
BuildRequires: gsm-devel
BuildRequires: perl-Unicode-Normalize
Requires: SDL >= 1.2.7-9m, libpostproc
%ifarch x86_64 ia64 ppc64 sparc64
Provides: libavcodec.so()(64bit)
Provides: libavformat.so()(64bit)
%else
Provides: libavcodec.so
Provides: libavformat.so
%endif

### include local configuration
%{?include_specopt}
# just a sample
# %%define extraconfopt --enable-mp3lame
# --enable-libgsm --enable-dv1394

%description
ffmpeg is a hyper fast realtime audio/video encoder, a streaming
server and a generic audio and video file converter.

It can grab from a standard Video4Linux video source and convert it
into several file formats based on DCT/motion compensation
encoding. Sound is compressed in MPEG audio layer 2 or using an AC3
compatible stream.

%package devel
Summary: Header files and static library for the ffmpeg codec library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: imlib2-devel, SDL-devel, freetype-devel, zlib-devel, libogg-devel
Requires: libtheora-devel, libvorbis-devel

%description devel
FFmpeg is a very fast video and audio converter. It can also grab from a
live audio/video source.
The command line interface is designed to be intuitive, in the sense that
ffmpeg tries to figure out all the parameters, when possible. You have
usually to give only the target bitrate you want. FFmpeg can also convert
from any sample rate to any other, and resize video on the fly with a high
quality polyphase filter.

Install this package if you want to compile apps with ffmpeg support.

%package -n libpostproc
Summary: Video postprocessing library from ffmpeg
Group: System Environment/Libraries
# We need to override version here... when libpostproc was built from
# MPlayer, it got up to 1.0-0.11.x) - mach barfs! :-(
#Version: 1.0.1
Provides: libpostproc-devel = %{version}-%{release}

%description -n libpostproc
FFmpeg is a very fast video and audio converter. It can also grab from a
live audio/video source.

This package contains only ffmpeg's libpostproc post-processing library which
other projects such as transcode may use. Install this package if you intend
to use MPlayer, transcode or other similar programs.


%prep
%setup -q 

%build
find . -name "CVS" | xargs rm -rf
%ifarch x86_64 ppc64
CFLAGS="%optflags" \
%else
CFLAGS="%optflags -fno-unit-at-a-time" \
%endif
./configure \
    --prefix=%{_prefix} \
    --libdir=%{_libdir} \
    --mandir=%{_mandir} \
    --incdir=%{_includedir}/ffmpeg \
%if 1
    --extra-libs="-lX11" \
%endif
%ifarch x86_64 ppc64
    --extra-cflags="-fPIC -DHAVE_LRINTF" \
%endif
    --enable-nonfree \
    --enable-libvorbis \
    --enable-libmp3lame \
    --enable-libtheora \
    --enable-libfaac \
    --enable-libgsm \
    --enable-libopenjpeg \
    --enable-libschroedinger \
    --enable-libspeex \
    --enable-libvpx \
    --enable-libx264 \
    --enable-postproc \
    --enable-shared \
    --enable-pthreads \
    --enable-gpl \
    --enable-avfilter \
    --enable-libpulse \
    --enable-libv4l2 \
    --enable-libcdio \
    --enable-libwavpack \
    --enable-openal \
    %{?extraconfopt}

%{__make} %{?_smp_mflags}
%{__make} documentation

%install
rm -rf %{buildroot}
%makeinstall \
    DESTDIR=%{buildroot} \
    BINDIR=%{buildroot}/%{_bindir} \
    SHLIBDIR=%{buildroot}%{_libdir} \
    LIBDIR=%{buildroot}%{_libdir}

sed -i 's/includedir=\/usr\/include\/ffmpeg/includedir=\/usr\/include/g' %{buildroot}%{_libdir}/pkgconfig/*.pc

# Remove unwanted files from the included docs
%{__cp} -a doc _docs
%{__rm} -rf _docs/{CVS,Makefile,*.1,*.texi,*.pl}

cd %{buildroot}%{_includedir}
 ln -sf       ffmpeg/libpostproc postproc
 ln -sf       postproc/postprocess.h postprocess.h
cd -

# ffmpeg compat path
cd %{buildroot}%{_includedir}/
for inc in ffmpeg/*
do
  ln -sf $inc .
done
cd -

# move preset files.
# ffmpeg bug
mv %{buildroot}/%{buildroot}/%{_datadir}/ffmpeg %{buildroot}/%{_datadir}/ffmpeg
mkdir -p %{buildroot}/%{_datadir}/doc/
mv %{buildroot}/%{buildroot}/%{_datadir}/doc/ffmpeg %{buildroot}/%{_datadir}/doc/

%clean
%{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
if [ "$1" = 0 ]; then
    /sbin/ldconfig
fi

%pre -n libpostproc
# for upgrading from STABLE_4, dirty...
rm -rf %{_includedir}/postproc

%post -n libpostproc
/sbin/ldconfig

%postun -n libpostproc
/sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc Changelog CREDITS README
%{_bindir}/*
%{_libdir}/*.so.*
%exclude %{_libdir}/libpostproc.so*
%{_mandir}/man1/*
%{_mandir}/man3/*
%{_datadir}/%{name}/*
%{_datadir}/doc/%{name}/*

%files devel
%defattr(-, root, root, 0755)
%doc doc/*
%{_includedir}/ffmpeg/
%{_includedir}/libavcodec
%{_includedir}/libavdevice
%{_includedir}/libavfilter
%{_includedir}/libavformat
%{_includedir}/libavutil
%{_includedir}/libswresample
%{_includedir}/libswscale
%exclude %{_includedir}/ffmpeg/libpostproc
%exclude %{_includedir}/libpostproc
%{_libdir}/*.a
%{_libdir}/*.so
%exclude %{_libdir}/libpostproc.so
%{_libdir}/pkgconfig/*.pc
%exclude %{_libdir}/pkgconfig/libpostproc.pc

%files -n libpostproc
%defattr(-, root, root, 0755)
%{_includedir}/postprocess.h
%{_includedir}/postproc
%{_includedir}/ffmpeg/libpostproc
%{_includedir}/libpostproc
%{_libdir}/libpostproc.so*
%{_libdir}/pkgconfig/libpostproc.pc

%changelog
* Thu May 15 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Wed Apr 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-1m)
- update 2.2.1

* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-1m)
- update 2.2

* Tue Feb 25 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.4-1m)
- update 2.1.4

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.3-1m)
- update 2.1.3

* Tue Jan 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.4-2m)
- rebuild against x264

* Tue Jun 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.4-1m)
- update 0.10.4

* Thu May 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.3-1m)
- update 0.10.3
- [SECURITY] CVE-2011-3929 CVE-2011-3936 CVE-2011-3945
-            CVE-2011-3952 CVE-2011-3940 CVE-2011-3947
-            CVE-2011-3946 CVE-2011-3934 CVE-2011-3951
-            and many fix

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-0.20120112.3m)
- rebuild against openjpeg-1.5.0

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-0.20120112.2m)
- rebuild against libvpx-1.0.0

* Thu Jan 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-0.20120112.1m)
- update 20120112
- [SECURITY] CVE-2011-3893 CVE-2011-3895

* Tue Jan  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-0.20120104.1m)
- update 20120104

* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-0.20111222.1m)
- update 20111222

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-0.20111217.1m)
- update 20111217

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-0.20111128.1m)
- update 20111128

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-0.20111027.1m)
- update 20111027

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-0.20111009.2m)
- rebuild against x264-0.0.2106

* Mon Oct 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-0.20111009.1m)
- update 20111009

* Mon Aug 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-0.20110803.3m)
- remove unneeded "BuildRequires: opencv-devel"

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.20110803.2m)
- rebuild against libva

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.20110803.1m)
- update 0.7.0-0.20110803

* Tue Jul  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.20110705.1m)
- update 0.7.0-0.20110705

* Sun Jun 26 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.6.1-0.20110514.3m)
- revised br

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20110514.2m)
- rebuild against openjpeg-1.4

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20110514.1m)
- update 0.6.1-0.20110514

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-0.20110403.2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20110403.1m)
- update 0.6.1-0.20110403

* Sun Mar 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20110326.1m)
- update 0.6.1-0.20110326
-- ffmpeg-mt merged!

* Fri Mar 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20110311.1m)
- update 0.6.1-0.20110311

* Sun Mar  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20110305.1m)
- update 0.6.1-0.20110305

* Tue Jan 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20110111.1m)
- update 0.6.1-0.20110111

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20101209.1m)
- update 0.6.1-0.20101209

* Tue Dec  7 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (0.6.1-0.20101111.4m)
- disable xvid, becase xvid is nonfree

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-0.20101111.3m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-0.20101111.2m)
- rebuild against opencv-2.1.0

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20101111.1m)
- update 20101111
-- single stream LATM/LOAS decoder
-- setpts filter added
- enable libschroedinger, opencv support

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20101026.1m)
- update 20101026
-- demuxer for receiving raw rtp:// URLs without an SDP description

* Wed Oct 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-0.20101019.1m)
- update 20101019
- Obsoletes Compat Headers

* Thu Oct 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.20101014.1m)
- update 20101014
-- RTP depacketization of MP4A-LATM
-- RTP packetization and depacketization of VP8
-- hflip filter
-- Apple HTTP Live Streaming demuxer
-- a64 codec
-- MMS-HTTP support
-- G.722 ADPCM audio encoder/decoder
-- R10k video decoder
-- ocv_smooth filter
-- frei0r wrapper filter
-- change crop filter syntax to width:height:x:y
-- make the crop filter accept parametric expressions
-- make ffprobe accept AVFormatContext options
-- yadif filter
-- blackframe filter
-- Demuxer for Leitch/Harris' VR native stream format (LXF)
-- RTP depacketization of the X-QT QuickTime format
-- SAP (Session Announcement Protocol, RFC 2974) muxer and demuxer
-- cropdetect filter

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-0.20100809.3m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-0.20100809.2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.20100809.1m)
- update 20100809

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.20100719.1m)
- update 20100719

* Mon Jul  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-0.20100617.2m)
- rebuild against libva-0.31.1

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-0.20100617.1m)
- update 0.6-20100617

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-1.20100508.1m)
- update 20100508

* Tue Apr 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-1.20100411.4m)
- add BuildRequires

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-1.20100411.3m)
- rebuild against libva

* Tue Apr 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-1.20100411.2m)
- --enable-libopenjpeg

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-1.20100411.1m)
- update 20100411

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-1.20100302.1m)
- update 20100302

* Tue Feb 23 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-1.20100223.1m)
- update 20100223
-- Adobe Filmstrip muxer and demuxer
-- RTP depacketization of H.263
-- Bink demuxer and audio/video decoders
-- enable symbol versioning by default for linkers that support it
-- IFF PBM/ILBM bitmap decoder
-- concat protocol
-- Indeo 5 decoder
-- RTP depacketization of AMR
-- WMAVoice decoder
-- FFprobe tool
-- AMR-NB decoder
-- RTSP muxer

* Wed Jan 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20100112.1m)
- update 20100112

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20091225.1m)
- update 20091225

* Tue Nov 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20091124.1m)
- update 20091124
-- MPEG-4 Audio Lossless Coding (ALS) decoder
-- - formats option split into -formats, -codecs, -bsfs, and -protocols

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1.20091011.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20091011.1m)
- [SECURITY] CVE-2009-4640 CVE-2009-4639 CVE-2009-4638 CVE-2009-4637
- [SECURITY] CVE-2009-4636 CVE-2009-4635 CVE-2009-4634 CVE-2009-4633
- [SECURITY] CVE-2009-4632 CVE-2009-4631
- update 20091011
-- Atrac1 decoder
-- MD STUDIO audio demuxer
-- RF64 support in WAV demuxer

* Sun Sep 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20090920.1m)
- update 20090920
-- Bluray (PGS) subtitle decoder
-- LPCM support in MPEG-TS (HDMV RID as found on Blu-ray disks)
-- Wmapro decoder
-- Core Audio Format demuxer

* Mon Aug 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20090824.1m)
- update 20090824
-- noX handling for OPT_BOOL X options
-- Wave64 demuxer
-- IEC-61937 compatible Muxer
-- TwinVQ decoder

* Tue Aug  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1.20090802.2m)
- devel package requires %%{name} = %%{version}-%%{release}

* Mon Aug  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20090802.1m)
- update 20090802

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20090630.1m)
- update 20090630

* Mon Jun 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1.20090622.1m)
- update 20090622

* Sat Jun 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-2m)
- fix "output buffer too small" issue
- http://wiki.crav-ing.com/index.php?%E5%90%8C%E6%A2%B1ffmpeg%E3%81%AElame%E3%82%A8%E3%83%B3%E3%82%B3%E3%83%BC%E3%83%89%E3%81%AE%E4%B8%8D%E5%85%B7%E5%90%88

* Wed Mar 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5-1m)
- update 0.5.0

* Thu Feb 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-0.20090218.1m)
- [SECURITY] CVE-2009-0385
- update 20090218
-- QCELP / PureVoice decoder
-- hybrid WavPack support
-- R3D REDCODE demuxer
-- ALSA support for playback and record
-- Electronic Arts TQI decoder
-- NC (NC4600) cameras file demuxer
-- Gopher client support
-- MXF D-10 muxer

* Wed Jan 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-0.20090107.4m)
- update to 20090107
-- support RV30 decoder 

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.9-0.20090107.3m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.9-0.20090107.2m)
- update Patch0 for fuzz=0
- License: GPLv2+ or LGPLv2+

* Wed Jan  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-0.20090107.1m)
- update to 20090107
-- support RV30 decoder 

* Thu Dec  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-0.20081203.1m)
- update to 20081203
- support any codecs
-- Electronic Arts TGQ decoder
-- RV40 decoder
-- QCELP / PureVoice decoder


* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-0.20081025.1m)
- [SECURITY] CVE-2008-4866 CVE-2008-4867 CVE-2008-4868 CVE-2008-4869
- update to 20081025

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-0.20080727.1m)
- update to 20080727

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20080704.1m)
- [SECURITY] CVE-2008-3162
- update to 20080704

* Sun Jun  1 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20080601.1m)
- update to 20080601

* Mon May  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-0.20080505.2m)
- remove pkgconfig64.patch to enable build on x86_64

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20080505.1m)
- update to 20080505

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.9-0.20080318.4m)
- rebuild against gcc43

* Mon Mar 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-0.20080318.3m)
- add %%pre -n libpostproc for upgrading from STABLE_4

* Thu Mar 20 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4.9-0.20080318.2m)
- add pkgconfig64 patch for lib64

* Tue Mar 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20080318.1m)
- header made compatible. 
- support IPhone/IPod mp4 mixer

* Tue Mar 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20080316.3m)
- fix *.pc CFLAGS

* Sun Mar 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20080316.2m)
- enable avfilter

* Sun Mar 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20080316.1m)
- update to 20080316

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.9-0.20080128.3m)
- add --enable-swscaler to configure

* Thu Jan 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-0.20080128.2m)
- remove "integer.h" from avutil.h to build libquicktime-1.0.2

* Tue Jan 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20080128.1m)
- update to 20080129
- add BuildReq

* Fri Dec 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20071219.3m)
- rebuild against x264-0.0.712-0.20071219

* Wed Dec 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-0.20071219.2m)
- fix build on x86_64

* Wed Dec 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20071219.1m)
- update to 20071219

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.9-0.20070507.2m)
- rebuild against libvorbis-1.2.0-1m

* Mon May  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4.9-0.20070507.1m)
- update to 20070507
- delete x86_64-shared.patch

* Tue Apr 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4.9-0.20070330.4m)
- revised x86_64-shared.patch with Yuta's advice

* Sun Apr 22 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4.9-0.20070330.3m)
- add x86_64-shared.patch for --enable-shared problem on x86_64
-- this patch is from http://www.momo-i.org and modifided a little bit

* Thu Apr 12 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.9-0.20070330.2m)
- BuildPreReq: libdca and build with "--enable-dts"

* Thu Apr 05 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.9-0.20070330.1m)
- update to 20070330
- resync with freshrpms.net
- * Tue Jan  9 2007 Matthias Saou <http://freshrpms.net/> 0.4.9-0.8.20070109
- - Update to today's SVN codebase, fixes the non existing ffmpeg.pc refrence.
- - Add faad2 patch.
- * Fri Dec 15 2006 Matthias Saou <http://freshrpms.net/> 0.4.9-0.7.20061215
- - Update to today's SVN codebase.
- - Update gsm patch so that it still applies.
- - Remove no longer needed x264 patch.
- - Remove --disable-opts option for now, as the build fails without. It seems
-   like an option from -O3 might be required for inline asm to work on x86.
- * Tue Oct 24 2006 Matthias Saou <http://freshrpms.net/> 0.4.9-0.7.20060918
- - Try to update, but stick with 20060918 as linking "as needed" is causing
-   too much trouble for now (libdca and libmp3lame with libm functions).
- - Include patch to work with latest x264.
- - Update URL and remove non-%%date stuff since there are no more releases.
- - Change group to Applications/Multimedia since ffmpeg is an application.
- - Remove dc1394 support since the lib isn't available anymore anyway.
- * Mon Sep 18 2006 Matthias Saou <http://freshrpms.net/> 0.4.9-0.6.20060918
- - Update to today's SVN codebase.
- - Remove theora support, it seems to be gone...
- - Remove a52 patch as ffmpeg doesn't link against it anyway.
- - Make installlib works again, so don't manually install anymore.
- - Remove all prever stuff that hasn't been useful in ages.
- - Change postproc/postprocess.h to be a symlink.


* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-0.20060517.7m)
- ffmpeg-devel Requires: freetype2-devel -> freetype-devel

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.9-0.20060517.6m)
- enable ppc64

* Sun Jul 30 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.9-0.20060517.5m)
- remove ffmpeg-0.4.8-kernel2681.patch

* Tue May 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.9-0.20060517.4m)
- %%exclude %%{_libdir}/libpostproc.so in devel (libpostproc owns it)

* Tue May 30 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.9-0.20060517.3m)
- added -DHAVE_LRINTF in --extra-cflags for x86_64 configure option
- added a link for postprocess.h in /usr/include

* Fri May 26 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.9-0.20060517.2m)
- added --extra-libs="-lX11" to a configure option.
- - (a little mysterious... This is needed when compiling with x264-0.0.523-1m and this causes "undefined symbol" error at runtime. Then I removed this flag and it was built fine with x264-0.0.523-2m without "-lX11" the day before yesterday. But today, it can be build if and only if with "-lX11" today and ffmpeg works fine...what has changed??? )

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.9-0.20060517.1m)
- update to cvs snapshot 20060517
- - Since CVS server at mplayerhq.hu is unreachable now, we get cvs tarball from gentoo mirror :-P (http://mirror.gentoo.gr.jp/distfiles/)
- resync with freshrpms.net (ffmpeg-0.4.9-0.5.20060317.src.rpm)

* Sat Mar 11 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.9-0.20050208.6m)
- enable ppc.

* Sat Mar  4 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.9-0.20050208.5m)
- enable x86_64.

* Thu Nov  3 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-0.20050208.4m)
- add gcc4patch. import Gentoo patch.
- Patch20: ffmpeg-0.4.9-gcc4.patch

* Wed Oct 19 2005 Toru Hoshina <t@momonga-linux.org>
- (0.4.9-0.20050208.3m)
- enable ia64.
- libpostproc should be installed prior to the ffmpeg.
- avoid conflict with the libpostproc from the mplayer.

* Sat May 07 2005 Toru Hoshina <t@momonga-linux.org>
- (0.4.9-0.20050208.2m)
- enable x86_64. no more lib64 patch.

* Sat May 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.9-0.20050208.1m)
- update to cvs version
- * Sat Jan 15 2005 Dag Wieers <dag@wieers.com> - 0.4.9-0.20041110.3
- - Exclude libpostproc.so* from ffmpeg package.

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.8-7m)
- add lib64 patch.

* Tue Feb 16 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.8-6m)
- roll buck to 0.4.8

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.9-0.0.1.4m)
- rebuild against SDL-1.2.7-9m

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.9-0.0.1.3m)
- renew lib64 patch

* Wed Feb  9 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.9-0.0.1.2m)
- enable lame, faad, and a52 support

* Mon Feb  7 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.9-0.0.1.1m)
- update 0.4.9 pre1

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.8-5m)
- enable x86_64.

* Mon Oct 25 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.4.8-4m)
- append CFLAGS fno-unit-at-a-time for compilation problems.

* Sat Aug 28 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.8-3m)
- add Patch0: ffmpeg-0.4.8-kernel2681.patch

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (0.1-7m)
- explicitly provides libavcodec.so and libavformat.so for anaconda.

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4.8-2m)
- revised spec for enabling rpm 4.2.

* Wed Nov  5 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.4.8-1m)
- update to 0.4.8

* Tue Aug 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.7-1m)

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.6-0.20021210.2m)
- rebuild against zlib 1.1.4-5m

* Wed Dec 11 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.6-0.20021210.1m)

* Sun Nov 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.6-0.20021110.1m)

* Sat Sep  7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.6-0.20020907.1m)
- initial import to Momonga
