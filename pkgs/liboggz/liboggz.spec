%global momorel 4

Summary: Simple programming interface for Ogg files and streams
Name: liboggz
Version: 1.1.1
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Libraries
URL: http://www.annodex.net/
Source0: http://downloads.xiph.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
# Always have oggz_off_t == loff_t even on 64-bit platforms
Patch0: %{name}-0.9.8-multilib.patch 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: docbook-utils
BuildRequires: doxygen
BuildRequires: libogg-devel >= 1.0

%description
Oggz provides a simple programming interface for reading and writing
Ogg files and streams. Ogg is an interleaving data container developed
by Monty at Xiph.Org, originally to support the Ogg Vorbis audio
format.

%package devel
Summary: Files needed for development using liboggz
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libogg-devel >= 1.0
Requires: pkgconfig

%description devel
Oggz provides a simple programming interface for reading and writing
Ogg files and streams. Ogg is an interleaving data container developed
by Monty at Xiph.Org, originally to support the Ogg Vorbis audio
format.

This package contains the header files and documentation needed for
development using liboggz.

%package doc
Summary: Documentation for liboggz
Group: Documentation
Requires: %{name} = %{version}-%{release}

%description doc
Oggz provides a simple programming interface for reading and writing
Ogg files and streams. Ogg is an interleaving data container developed
by Monty at Xiph.Org, originally to support the Ogg Vorbis audio
format.

This package contains HTML documentation needed for development using
liboggz.

%prep
%setup -q

%patch0 -p1 -b .multilib

%build
%configure --disable-static
%make

%check
make check

%install[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall \
	docdir=%{buildroot}%{_datadir}/doc/%{name}-doc-%{version} \
	INSTALL="%{__install} -p"

# remove unpackaged files from the buildroot
rm -f %{buildroot}%{_libdir}/*.la

# not particularly interested in the tex docs, the html version has everything
rm -rf %{buildroot}%{_datadir}/doc/%{name}-doc-%{version}/latex

# Multilib fix: ensure generated headers have timestamps
# independent of build time
(cd include/oggz &&
    touch -r oggz_off_t_generated.h.in.multilib \
      %{buildroot}%{_includedir}/oggz/oggz_off_t_generated.h
)


%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig
                                                                           
%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL PATCHES README TODO
%{_bindir}/oggz*
%{_libdir}/%{name}.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root)
%{_includedir}/oggz
%{_libdir}/pkgconfig/oggz.pc
%{_libdir}/%{name}.so

%files doc
%defattr(-,root,root)
%doc %{_docdir}/%{name}-doc-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1
- sort %%files
- remove mixed tabs and spaces from spec
- set NoSource: 0

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.8-1m)
- import from Fedora

* Thu Feb 04 2010 Adam Jackson <ajax@redhat.com> 0.9.8-5
- --disable-static, drop the .a files

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.8-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.8-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Dec 30 2008 Michel Salim <salimma@fedoraproject.org> - 0.9.8-2
- Multilib fixes (bugs #342291, #477291)

* Mon Jul  7 2008 Michel Alexandre Salim <salimma@fedoraproject.org> - 0.9.8-1
- Update to 0.9.8

* Wed May 21 2008 Michel Alexandre Salim <salimma@fedoraproject.org> - 0.9.7-1
- Update to 0.9.7

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.5-2
- Autorebuild for GCC 4.3

* Fri Jan 12 2007 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.5-1
- new upstream release

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 0.9.4-3
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Wed Sep 20 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.4-2
- rebuilt

* Sun Mar 05 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.4-1
- new upstream release
- removed patch, was applied upstream

* Sat Nov 12 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.3-1
- new upstream release

* Mon Jul 18 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.2-1
- new upstream version
- removed patches
- moved devel docs to versioned location

* Mon Jun 13 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.1-2: rpmlint cleanup

* Fri Jun 03 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.1-1: initial package
