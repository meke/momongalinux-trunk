%global momorel 3

Summary:	QT GUI for bitcoin client
Name:		bitcoin-qt
Version:	0.4.0rc1
Release:	%{momorel}m%{?dist}
License:	MIT
Group:		Applications/Productivity
URL:		https://github.com/laanwj/bitcoin-qt
# git clone git://github.com/laanwj/%%{name}
#Source0:	%%{name}-%%{version}.tar.bz2
Source0:	%{name}.tar.bz2
Patch0:		Makefile-boost.patch
Patch1:		Makefile-boost-64.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	git qt-devel
BuildRequires:	boost boost-thread boost-system boost-filesystem  
BuildRequires:	boost-program-options db4-cxx = 4.8.30
BuildRequires:	openssl-nf-devel 
ExclusiveArch:	i686 x86_64



%description
Bitcoin is an open-source project to develop a decentralized, peer-to-peer network that tracks and verifies transactions. Bitcoin-qt is a QT GUI 
for the Bitcoin client.



%prep


%setup -qn %{name}
 
qmake "USE_DBUS=1"
# future miniupnp support (for NAT) add "USE_UPNP=1" to qmake line 
# Patch Makefile for boost-thread  
%ifarch x86_64
%patch1 -p1
%else
%patch0 -p1
%endif


%build



make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# install binary
mkdir -p %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_bindir}/%{name}

%changelog
* Sun Sep 04 2011 Daniel Mclellan <daniel.mclellan@gmail.com>
- (0.4.0rc1-3m)
- added i686

* Sun Sep 04 2011 Daniel Mclellan <daniel.mclellan@gmail.com>
- (0.4.0rc1-2m) 
- added ExclusiveArch 

* Sat Sep 03 2011 Daniel Mclellan <daniel.mclellan@gmail.com>
- (0.4.0rc1-1m) 
- first spec, initial momonga release
