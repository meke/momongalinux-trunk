%global momorel 1

%define byaccdate 20140422

Summary:        Berkeley Yacc, a parser generator
Name:           byacc
Version:        1.9.%{byaccdate}
Release:        %{momorel}m%{?dist}
License:        Public Domain
Group:          Development/Tools
URL:            http://invisible-island.net/byacc/byacc.html
Source:         ftp://invisible-island.net/byacc/byacc-%{byaccdate}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Byacc (Berkeley Yacc) is a public domain LALR parser generator which
is used by many programs during their build process.

If you are going to do development on your system, you will want to install
this package.

%prep
%setup -q -n byacc-%{byaccdate}

%build
%configure --disable-dependency-tracking
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install transform='s,x,x,'
ln -s yacc $RPM_BUILD_ROOT/%{_bindir}/byacc
ln -s yacc.1 $RPM_BUILD_ROOT/%{_mandir}/man1/byacc.1

%check
echo ====================TESTING=========================
make check
echo ====================TESTING END=====================

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc ACKNOWLEDGEMENTS CHANGES NEW_FEATURES NOTES NO_WARRANTY README
%defattr(-,root,root,-)
%{_bindir}/yacc
%{_bindir}/byacc
%{_mandir}/man1/yacc.1*
%{_mandir}/man1/byacc.1*

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.20140422-1m)
- update to 20140422

* Sun Mar 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.20140101-1m)
- update to 20140101

* Sat Oct 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.20130925-1m)
- update to 20130925

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.20130304-1m)
- update to 20130304

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.20111219-1m)
- update to 20111219

* Sat Oct  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.20110908-1m)
- update to 20110908

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.20101229-4m)
- rebuild for new GCC 4.6

* Mon Jan  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.20101229-1m)
- update to 20101229

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.20100216-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.20100216-2m)
- full rebuild for mo7 release

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.20100216-1m)
- update to 20100216

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.20091027-1m)
- update to 20091027

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.20090221-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.20090221-1m)
- update 20090221

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.20070509-2m)
- rebuild against rpm-4.6

* Wed Jul 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.20070509-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-22m)
- rebuild against gcc43

* Tue Dec 27 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9-21m)
- sync with fc

* Thu Oct  9 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9-20m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Oct 12 2002 Nguyen Hung Vu <vuhung@momonga-linux.org>
- (1.9-19m)
- Change the prefix k to m. Still being the latest version

* Fri Dec  1 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.9-18k)
- modified to use %{_mandir} macro

* Tue Aug 22 2000 Kenichi Matsubara <m@kondara.org>
- Change PATH (FHS)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.9-12).

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Fri Feb 11 2000 Takaaki Tabuchi <tab@kondara.org>
- fix %clean part don't rm test directory problem.

* Fri Feb  4 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild to compress man pages
- fix up manpage symlink

 Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-byacc-20000115

* Sun Jan 23 2000 Takaaki Tabuchi <tab@kondara.org>
- resolve cannot unlink problem at %clean section.

 Sun Nov 28 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-byacc-19991115

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Apr 07 1999 Preston Brown <pbrown@redhat.com>
- man page fixed.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 10)

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- various spec file cleanups

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
