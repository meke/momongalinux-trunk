%global momorel 2

# Set --with testsuite or %bcond_without to run the Samba torture testsuite.
%bcond_with testsuite

%define samba_version 4.1.9
%define talloc_version 2.1.0
%define ntdb_version 0.9
%define tdb_version 1.2.13
%define tevent_version 0.9.21
%define ldb_version 1.1.16

%define samba_release %{momorel}m%{?dist}

%global with_libsmbclient 1
%global with_libwbclient 1

%global with_pam_smbpass 0
%global with_talloc 0
%global with_tevent 0
%global with_tdb 0
%global with_ntdb 1
%global with_ldb 0

%global with_mitkrb5 1
%global with_dc 0

%if %{with testsuite}
# The testsuite only works with a full build right now.
%global with_mitkrb5 0
%global with_dc 1
%endif

%global with_clustering_support 1

%{!?python_libdir: %define python_libdir %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1,1)")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           samba
Version:        %{samba_version}
Release:        %{samba_release}

Summary:        Server and Client software to interoperate with Windows machines
License:        GPLv3+ and LGPLv3+
Group:          System Environment/Daemons
URL:            http://www.samba.org/

Source0:        http://samba.org/%{name}/ftp/stable/%{name}-%{version}.tar.gz
NoSource:       0

# Red Hat specific replacement-files
Source1: samba.log
Source2: samba.xinetd
Source4: smb.conf.default
Source5: pam_winbind.conf
Source6: samba.pamd

Source200: README.dc
Source201: README.downgrade

#Patch0: samba-4.0.1-nmb.service.patch
#Patch1: %{name}-%{version}-smbd-options.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(pre): /usr/sbin/groupadd
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

Requires(pre): %{name}-common = %{samba_version}-%{release}
Requires: %{name}-libs = %{samba_version}-%{release}

Provides: samba4 = %{samba_version}-%{release}
Obsoletes: samba4 < %{samba_version}-%{release}
Obsoletes: samba-domainjoin-gui
Obsoletes: samba-doc
Obsoletes: samba-winbind-devel
Obsoletes: samba-swat

BuildRequires: autoconf
%if %with_clustering_support
BuildRequires: ctdb-devel >= 1.2.39
%endif
BuildRequires: cups-devel
BuildRequires: dmapi-devel >= 2.2.12-4m
BuildRequires: docbook-style-xsl
BuildRequires: e2fsprogs-devel
BuildRequires: gawk
BuildRequires: iniparser-devel
BuildRequires: krb5-devel >= 1.10
BuildRequires: libacl-devel
BuildRequires: libaio-devel
BuildRequires: libattr-devel
BuildRequires: libcap-devel
BuildRequires: libuuid-devel
BuildRequires: libxslt
BuildRequires: ncurses-devel
BuildRequires: openldap-devel
BuildRequires: pam-devel
BuildRequires: perl-ExtUtils-MakeMaker
BuildRequires: perl-Parse-Yapp
BuildRequires: popt-devel
BuildRequires: python-devel
BuildRequires: python-tevent
BuildRequires: quota-devel
BuildRequires: readline-devel
BuildRequires: sed
BuildRequires: zlib-devel >= 1.2.3
BuildRequires: libbsd-devel

%if ! %with_talloc
BuildRequires: libtalloc-devel >= %{talloc_version}
BuildRequires: pytalloc-devel >= %{talloc_version}
%endif

%if ! %with_tevent
BuildRequires: libtevent-devel >= %{tevent_version}
BuildRequires: python-tevent >= %{tevent_version}
%endif

%if ! %with_ldb
BuildRequires: libldb-devel >= %{ldb_version}
BuildRequires: pyldb-devel >= %{ldb_version}
%endif

%if ! %with_tdb
BuildRequires: libtdb-devel >= %{tdb_version}
BuildRequires: python-tdb >= %{tdb_version}
%endif

%if %{with testsuite}
BuildRequires: ldb-tools
%endif

# UGLY HACK: Fix 'Provides' for libsmbclient and libwbclient
%if ! %{with_libsmbclient} && ! %{with_libwbclient}
%{?filter_setup:
%filter_from_provides /libsmbclient.so.0()/d; /libwbclient.so.0()/d
%filter_from_requires /libsmbclient.so.0()/d; /libwbclient.so.0()/d
%filter_setup
}
%endif

### SAMBA
%description
Samba is the standard Windows interoperability suite of programs for Linux and Unix.

### CLIENT
%package client
Summary: Samba client programs
Group: Applications/System
Requires: %{name}-common = %{samba_version}-%{release}
Requires: %{name}-libs = %{samba_version}-%{release}

Provides: samba4-client = %{samba_version}-%{release}
Obsoletes: samba4-client < %{samba_version}-%{release}

%description client
The samba4-client package provides some SMB/CIFS clients to complement
the built-in SMB/CIFS filesystem in Linux. These clients allow access
of SMB/CIFS shares and printing to SMB/CIFS printers.

### COMMON
%package common
Summary: Files used by both Samba servers and clients
Group: Applications/System
Requires: %{name}-libs = %{samba_version}-%{release}
Requires(post): systemd
Requires: logrotate

Provides: samba4-common = %{samba_version}-%{release}
Obsoletes: samba4-common < %{samba_version}-%{release}

%description common
samba4-common provides files necessary for both the server and client
packages of Samba.

### DC
%package dc
Summary: Samba AD Domain Controller
Group: Applications/System
Requires: %{name}-libs = %{samba_version}-%{release}
Requires: %{name}-dc-libs = %{samba_version}-%{release}
Requires: %{name}-python = %{samba_version}-%{release}

Provides: samba4-dc = %{samba_version}-%{release}
Obsoletes: samba4-dc < %{samba_version}-%{release}

%description dc
The samba-dc package provides AD Domain Controller functionality

### DC-LIBS
%package dc-libs
Summary: Samba AD Domain Controller Libraries
Group: Applications/System
Requires: %{name}-common = %{samba_version}-%{release}
Requires: %{name}-libs = %{samba_version}-%{release}

Provides: samba4-dc-libs = %{samba_version}-%{release}
Obsoletes: samba4-dc-libs < %{samba_version}-%{release}

%description dc-libs
The samba4-dc-libs package contains the libraries needed by the DC to
link against the SMB, RPC and other protocols.

### DEVEL
%package devel
Summary: Developer tools for Samba libraries
Group: Development/Libraries
Requires: %{name}-libs = %{samba_version}-%{release}

Provides: samba4-devel = %{samba_version}-%{release}
Obsoletes: samba4-devel < %{samba_version}-%{release}

%description devel
The samba4-devel package contains the header files for the libraries
needed to develop programs that link against the SMB, RPC and other
libraries in the Samba suite.

### LIBS
%package libs
Summary: Samba libraries
Group: Applications/System
Requires: krb5-libs >= 1.10
%if %with_libwbclient
Requires: libwbclient = %{samba_version}-%{release}
%endif

Provides: samba4-libs = %{samba_version}-%{release}
Obsoletes: samba4-libs < %{samba_version}-%{release}

%description libs
The samba4-libs package contains the libraries needed by programs that
link against the SMB, RPC and other protocols provided by the Samba suite.

### LIBSMBCLIENT
%if %with_libsmbclient
%package -n libsmbclient
Summary: The SMB client library
Group: Applications/System
Requires: %{name}-common = %{samba_version}-%{release}

%description -n libsmbclient
The libsmbclient contains the SMB client library from the Samba suite.

%package -n libsmbclient-devel
Summary: Developer tools for the SMB client library
Group: Development/Libraries
Requires: libsmbclient = %{samba_version}-%{release}

%description -n libsmbclient-devel
The libsmbclient-devel package contains the header files and libraries needed to
develop programs that link against the SMB client library in the Samba suite.
%endif # with_libsmbclient

### LIBWBCLIENT
%if %with_libwbclient
%package -n libwbclient
Summary: The winbind client library
Group: Applications/System

%description -n libwbclient
The libwbclient package contains the winbind client library from the Samba suite.

%package -n libwbclient-devel
Summary: Developer tools for the winbind library
Group: Development/Libraries
Requires: libwbclient = %{samba_version}-%{release}

%description -n libwbclient-devel
The libwbclient-devel package provides developer tools for the wbclient library.
%endif # with_libwbclient

### PYTHON
%package python
Summary: Samba Python libraries
Group: Applications/System
Requires: %{name} = %{samba_version}-%{release}
Requires: %{name}-libs = %{samba_version}-%{release}
Requires: python-tevent
Requires: python-tdb
Requires: pyldb
Requires: pytalloc

Provides: samba4-python = %{samba_version}-%{release}
Obsoletes: samba4-python < %{samba_version}-%{release}

%description python
The samba4-python package contains the Python libraries needed by programs
that use SMB, RPC and other Samba provided protocols in Python programs.

### PIDL
%package pidl
Summary: Perl IDL compiler
Group: Development/Tools
Requires: perl-Parse-Yapp
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

Provides: samba4-pidl = %{samba_version}-%{release}
Obsoletes: samba4-pidl < %{samba_version}-%{release}

%description pidl
The samba4-pidl package contains the Perl IDL compiler used by Samba
and Wireshark to parse IDL and similar protocols

### TEST
%package test
Summary: Testing tools for Samba servers and clients
Group: Applications/System
Requires: %{name} = %{samba_version}-%{release}
Requires: %{name}-common = %{samba_version}-%{release}
Requires: %{name}-dc = %{samba_version}-%{release}
Requires: %{name}-libs = %{samba_version}-%{release}
Requires: %{name}-winbind = %{samba_version}-%{release}

Provides: samba4-test = %{samba_version}-%{release}
Obsoletes: samba4-test < %{samba_version}-%{release}

%description test
samba4-test provides testing tools for both the server and client
packages of Samba.

### TEST-DEVEL
%package test-devel
Summary: Testing devel files for Samba servers and clients
Group: Applications/System
Requires: %{name}-test-devel = %{samba_version}-%{release}

%description test-devel
samba-test-devel provides testing devel files for both the server and client
packages of Samba.

### WINBIND
%package winbind
Summary: Samba winbind
Group: Applications/System
Requires: %{name}-common = %{samba_version}-%{release}
Requires: %{name}-libs = %{samba_version}-%{release}

Provides: samba4-winbind = %{samba_version}-%{release}
Obsoletes: samba4-winbind < %{samba_version}-%{release}

%description winbind
The samba-winbind package provides the winbind NSS library, and some
client tools.  Winbind enables Linux to be a full member in Windows
domains and to use Windows user and group accounts on Linux.

### WINBIND-CLIENTS
%package winbind-clients
Summary: Samba winbind clients
Group: Applications/System
Requires: %{name}-common = %{samba_version}-%{release}
Requires: %{name}-libs = %{samba_version}-%{release}
Requires: %{name}-winbind = %{samba_version}-%{release}
%if %with_libwbclient
Requires: libwbclient = %{samba_version}-%{release}
%endif
Requires: pam

Provides: samba4-winbind-clients = %{samba_version}-%{release}
Obsoletes: samba4-winbind-clients < %{samba_version}-%{release}

%description winbind-clients
The samba-winbind-clients package provides the NSS library and a PAM
module necessary to communicate to the Winbind Daemon

### WINBIND-KRB5-LOCATOR
%package winbind-krb5-locator
Summary: Samba winbind krb5 locator
Group: Applications/System
%if %with_libwbclient
Requires: libwbclient = %{samba_version}-%{release}
Requires: %{name}-winbind = %{samba_version}-%{release}
%else
Requires: %{name}-libs = %{samba_version}-%{release}
%endif

Provides: samba4-winbind-krb5-locator = %{samba_version}-%{release}
Obsoletes: samba4-winbind-krb5-locator < %{samba_version}-%{release}

# Handle winbind_krb5_locator.so as alternatives to allow
# IPA AD trusts case where it should not be used by libkrb5
# The plugin will be diverted to /dev/null by the FreeIPA
# freeipa-server-trust-ad subpackage due to higher priority
# and restored to the proper one on uninstall
Requires(post): chkconfig
Requires(postun): chkconfig
Requires(preun): chkconfig

%description winbind-krb5-locator
The winbind krb5 locator is a plugin for the system kerberos library to allow
the local kerberos library to use the same KDC as samba and winbind use

%prep
%setup -q
##%patch0 -p1 -b .nmbd
##%patch1 -p1 -b .smbd-options

%build
%global _talloc_lib ,talloc,pytalloc,pytalloc-util
%global _tevent_lib ,tevent,pytevent
%global _tdb_lib ,tdb,pytdb
%global _ldb_lib ,ldb,pyldb

%if ! %{with_talloc}
%global _talloc_lib ,!talloc,!pytalloc,!pytalloc-util
%endif

%if ! %{with_tevent}
%global _tevent_lib ,!tevent,!pytevent
%endif

%if ! %{with_tdb}
%global _tdb_lib ,!tdb,!pytdb
%endif

%if ! %{with_ldb}
%global _ldb_lib ,!ldb,!pyldb
%endif

%global _samba4_libraries heimdal,!zlib,!popt%{_talloc_lib}%{_tevent_lib}%{_tdb_lib}%{_ldb_lib}

%global _samba4_idmap_modules idmap_ad,idmap_rid,idmap_adex,idmap_hash,idmap_tdb2
%global _samba4_pdb_modules pdb_tdbsam,pdb_ldap,pdb_ads,pdb_smbpasswd,pdb_wbc_sam,pdb_samba4
%global _samba4_auth_modules auth_unix,auth_wbc,auth_server,auth_netlogond,auth_script,auth_samba4

%global _samba4_modules %{_samba4_idmap_modules},%{_samba4_pdb_modules},%{_samba4_auth_modules}

%global _libsmbclient %nil
%global _libwbclient %nil

%if ! %with_libsmbclient
%global _libsmbclient smbclient,smbsharemodes,
%endif

%if ! %with_libwbclient
%global _libwbclient wbclient,
%endif

%global _samba4_private_libraries %{_libsmbclient}%{_libwbclient}

%configure \
        --enable-fhs \
        --with-piddir=/run \
        --with-sockets-dir=/run/samba \
        --with-modulesdir=%{_libdir}/samba \
        --with-pammodulesdir=%{_libdir}/security \
        --with-lockdir=/var/lib/samba \
        --disable-gnutls \
        --disable-rpath-install \
        --with-shared-modules=%{_samba4_modules} \
        --bundled-libraries=%{_samba4_libraries} \
        --with-pam \
%if (! %with_libsmbclient) || (! %with_libwbclient)
        --private-libraries=%{_samba4_private_libraries} \
%endif
%if %with_mitkrb5
        --with-system-mitkrb5 \
%endif
%if ! %with_dc
        --without-ad-dc \
%endif
%if %with_clustering_support
        --with-cluster-support \
        --enable-old-ctdb \
%endif
%if %{with testsuite}
        --enable-selftest \
%endif
%if ! %with_pam_smbpass
        --without-pam_smbpass
%endif

export WAFCACHE=/tmp/wafcache
mkdir -p $WAFCACHE
make %{?_smp_mflags}

# Build PIDL for installation into vendor directories before
# 'make proto' gets to it.
(cd pidl && %{__perl} Makefile.PL INSTALLDIRS=vendor )

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -d -m 0755 %{buildroot}/usr/{sbin,bin}
install -d -m 0755 %{buildroot}%{_libdir}/security
install -d -m 0755 %{buildroot}/var/lib/samba
install -d -m 0755 %{buildroot}/var/lib/samba/private
install -d -m 0755 %{buildroot}/var/lib/samba/winbindd_privileged
install -d -m 0755 %{buildroot}/var/lib/samba/scripts
install -d -m 0755 %{buildroot}/var/lib/samba/sysvol
install -d -m 0755 %{buildroot}/var/log/samba/old
install -d -m 0755 %{buildroot}/var/spool/samba
install -d -m 0755 %{buildroot}/var/run/samba
install -d -m 0755 %{buildroot}/var/run/winbindd
install -d -m 0755 %{buildroot}/%{_libdir}/samba
install -d -m 0755 %{buildroot}/%{_libdir}/pkgconfig

# Undo the PIDL install, we want to try again with the right options.
rm -rf %{buildroot}/%{_libdir}/perl5
rm -rf %{buildroot}/%{_datadir}/perl5

# Install PIDL.
( cd pidl && make install PERL_INSTALL_ROOT=%{buildroot} )

# Install other stuff
install -d -m 0755 %{buildroot}%{_sysconfdir}/logrotate.d
install -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/samba

install -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/samba/smb.conf

install -d -m 0755 %{buildroot}%{_sysconfdir}/security
install -m 0644 %{SOURCE5} %{buildroot}%{_sysconfdir}/security/pam_winbind.conf

echo 127.0.0.1 localhost > %{buildroot}%{_sysconfdir}/samba/lmhosts

install -d -m 0755 %{buildroot}%{_sysconfdir}/openldap/schema
install -m644 examples/LDAP/samba.schema %{buildroot}%{_sysconfdir}/openldap/schema/samba.schema

install -m 0744 packaging/printing/smbprint %{buildroot}%{_bindir}/smbprint

install -d -m 0755 %{buildroot}%{_sysconfdir}/tmpfiles.d/
install -m644 packaging/systemd/samba.conf.tmp %{buildroot}%{_sysconfdir}/tmpfiles.d/samba.conf

install -d -m 0755 %{buildroot}%{_sysconfdir}/sysconfig
install -m 0644 packaging/systemd/samba.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/samba

install -d -m 0755 %{buildroot}%{_defaultdocdir}/%{name}
install -m 0644 %{SOURCE201} %{buildroot}%{_defaultdocdir}/%{name}/README.downgrade

%if ! %with_dc
install -m 0644 %{SOURCE200} %{buildroot}%{_defaultdocdir}/%{name}/README.dc
install -m 0644 %{SOURCE200} %{buildroot}%{_defaultdocdir}/%{name}/README.dc-libs
%endif

install -d -m 0755 %{buildroot}%{_unitdir}
for i in nmb smb winbind ; do
    cat packaging/systemd/$i.service | sed -e 's@Type=forking@Type=forking\nEnvironment=KRB5CCNAME=/run/samba/krb5cc_samba@g' >tmp$i.service
    install -m 0644 tmp$i.service %{buildroot}%{_unitdir}/$i.service
done

# NetworkManager online/offline script
install -d -m 0755 %{buildroot}%{_sysconfdir}/NetworkManager/dispatcher.d/
install -m 0755 packaging/NetworkManager/30-winbind-systemd \
            %{buildroot}%{_sysconfdir}/NetworkManager/dispatcher.d/30-winbind

# winbind krb5 locator
install -d -m 0755 %{buildroot}%{_libdir}/krb5/plugins/libkrb5
touch %{buildroot}%{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so

# Clean out crap left behind by the PIDL install.
find %{buildroot} -type f -name .packlist -exec rm -f {} \;
rm -f %{buildroot}%{perl_vendorlib}/wscript_build
rm -rf %{buildroot}%{perl_vendorlib}/Parse/Yapp

# This makes the right links, as rpmlint requires that
# the ldconfig-created links be recorded in the RPM.
/sbin/ldconfig -N -n %{buildroot}%{_libdir}

# Fix up permission on perl install.
%{_fixperms} %{buildroot}%{perl_vendorlib}

%if %{with testsuite}
%check
TDB_NO_FSYNC=1 make %{?_smp_mflags} test
%endif

%post
%systemd_post smb.service
%systemd_post nmb.service

%preun
%systemd_preun smb.service
%systemd_preun nmb.service

%postun
%systemd_postun_with_restart smb.service
%systemd_postun_with_restart nmb.service

%post common
/sbin/ldconfig
/usr/bin/systemd-tmpfiles --create %{_sysconfdir}/tmpfiles.d/samba.conf

%postun common -p /sbin/ldconfig

%if %with_dc
%post dc-libs -p /sbin/ldconfig

%postun dc-libs -p /sbin/ldconfig
%endif # with_dc

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%if %with_libsmbclient
%post -n libsmbclient -p /sbin/ldconfig

%postun -n libsmbclient -p /sbin/ldconfig
%endif # with_libsmbclient

%if %with_libwbclient
%post -n libwbclient -p /sbin/ldconfig

%postun -n libwbclient -p /sbin/ldconfig
%endif # with_libwbclient

%post test -p /sbin/ldconfig

%postun test -p /sbin/ldconfig

%pre winbind
/usr/sbin/groupadd -g 88 wbpriv >/dev/null 2>&1 || :

%post winbind
%systemd_post winbind.service

%preun winbind
%systemd_preun winbind.service

%postun winbind
%systemd_postun_with_restart smb.service
%systemd_postun_with_restart nmb.service

%post winbind-clients -p /sbin/ldconfig

%postun winbind-clients -p /sbin/ldconfig

%postun winbind-krb5-locator
if [ "$1" -ge "1" ]; then
        if [ "`readlink %{_sysconfdir}/alternatives/winbind_krb5_locator.so`" == "%{_libdir}/winbind_krb5_locator.so" ]; then
                %{_sbindir}/update-alternatives --set winbind_krb5_locator %{_libdir}/winbind_krb5_locator.so
        fi
fi

%post winbind-krb5-locator
%{_sbindir}/update-alternatives --install %{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so \
                                winbind_krb5_locator.so %{_libdir}/winbind_krb5_locator.so 10

%preun winbind-krb5-locator
if [ $1 -eq 0 ]; then
        %{_sbindir}/update-alternatives --remove winbind_krb5_locator.so %{_libdir}/winbind_krb5_locator.so
fi

%clean
rm -rf %{buildroot}

### SAMBA
%files
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/smbstatus
%{_bindir}/smbtar
%{_bindir}/eventlogadm
%{_sbindir}/nmbd
%{_sbindir}/smbd
%{_libdir}/samba/auth
%{_libdir}/samba/vfs
%{_unitdir}/nmb.service
%{_unitdir}/smb.service
%attr(1777,root,root) %dir /var/spool/samba
%dir %{_sysconfdir}/openldap/schema
%{_sysconfdir}/openldap/schema/samba.schema
%doc %{_defaultdocdir}/%{name}/README.downgrade
%{_mandir}/man1/smbstatus.1*
%{_mandir}/man8/eventlogadm.8*
%{_mandir}/man8/smbd.8*
%{_mandir}/man8/nmbd.8*
%{_mandir}/man8/vfs_*.8*

### CLIENT
%files client
%defattr(-,root,root)
%{_bindir}/cifsdd
%{_bindir}/dbwrap_tool
%{_bindir}/nmblookup
%{_bindir}/nmblookup4
%{_bindir}/oLschema2ldif
%{_bindir}/regdiff
%{_bindir}/regpatch
%{_bindir}/regshell
%{_bindir}/regtree
%{_bindir}/rpcclient
%{_bindir}/samba-regedit
%{_bindir}/sharesec
%{_bindir}/smbcacls
%{_bindir}/smbclient
%{_bindir}/smbclient4
%{_bindir}/smbcquotas
%{_bindir}/smbget
#%{_bindir}/smbiconv
%{_bindir}/smbpasswd
%{_bindir}/smbprint
%{_bindir}/smbspool
%{_bindir}/smbta-util
%{_bindir}/smbtree
%{_libdir}/samba/libldb-cmdline.so
%{_mandir}/man1/dbwrap_tool.1*
%{_mandir}/man1/nmblookup.1*
%{_mandir}/man1/oLschema2ldif.1*
%{_mandir}/man1/regdiff.1*
%{_mandir}/man1/regpatch.1*
%{_mandir}/man1/regshell.1*
%{_mandir}/man1/regtree.1*
%exclude %{_mandir}/man1/findsmb.1*
%{_mandir}/man1/log2pcap.1*
%{_mandir}/man1/nmblookup4.1*
%{_mandir}/man1/rpcclient.1*
%{_mandir}/man1/sharesec.1*
%{_mandir}/man1/smbcacls.1*
%{_mandir}/man1/smbclient.1*
%{_mandir}/man1/smbcquotas.1*
%{_mandir}/man1/smbget.1*
%{_mandir}/man3/ntdb.3*
%{_mandir}/man5/smbgetrc.5*
%exclude %{_mandir}/man1/smbtar.1*
%{_mandir}/man1/smbtree.1*
%{_mandir}/man8/ntdbbackup.8*
%{_mandir}/man8/ntdbdump.8*
%{_mandir}/man8/ntdbrestore.8*
%{_mandir}/man8/ntdbtool.8*
%{_mandir}/man8/samba-regedit.8*
%{_mandir}/man8/smbpasswd.8*
%{_mandir}/man8/smbspool.8*
%{_mandir}/man8/smbta-util.8*

## we don't build it for now
%if %{with_ntdb}
%{_bindir}/ntdbbackup
%{_bindir}/ntdbdump
%{_bindir}/ntdbrestore
%{_bindir}/ntdbtool
%endif

%if %{with_tdb}
%{_bindir}/tdbbackup
%{_bindir}/tdbdump
%{_bindir}/tdbrestore
%{_bindir}/tdbtool
%{_mandir}/man8/tdbbackup.8*
%{_mandir}/man8/tdbdump.8*
%{_mandir}/man8/tdbrestore.8*
%{_mandir}/man8/tdbtool.8*
%endif

%if %with_ldb
%{_bindir}/ldbadd
%{_bindir}/ldbdel
%{_bindir}/ldbedit
%{_bindir}/ldbmodify
%{_bindir}/ldbrename
%{_bindir}/ldbsearch
%{_mandir}/man1/ldbadd.1*
%{_mandir}/man1/ldbdel.1*
%{_mandir}/man1/ldbedit.1*
%{_mandir}/man1/ldbmodify.1*
%{_mandir}/man1/ldbrename.1*
%{_mandir}/man1/ldbsearch.1*
%endif

### COMMON
%files common
%defattr(-,root,root)
#%{_libdir}/samba/charset ???
%{_sysconfdir}/tmpfiles.d/samba.conf
%{_bindir}/net
%{_bindir}/pdbedit
%{_bindir}/profiles
%{_bindir}/smbcontrol
%{_bindir}/testparm
%{_datadir}/samba/codepages
%config(noreplace) %{_sysconfdir}/logrotate.d/samba
%attr(0700,root,root) %dir /var/log/samba
%attr(0700,root,root) %dir /var/log/samba/old
%ghost %dir /var/run/samba
%ghost %dir /var/run/winbindd
%attr(700,root,root) %dir /var/lib/samba/private
%attr(755,root,root) %dir %{_sysconfdir}/samba
%config(noreplace) %{_sysconfdir}/samba/smb.conf
%config(noreplace) %{_sysconfdir}/samba/lmhosts
%config(noreplace) %{_sysconfdir}/sysconfig/samba
%{_mandir}/man1/profiles.1*
%{_mandir}/man1/smbcontrol.1*
%{_mandir}/man1/testparm.1*
%{_mandir}/man5/lmhosts.5*
%{_mandir}/man5/smb.conf.5*
%{_mandir}/man5/smbpasswd.5*
%{_mandir}/man7/samba.7*
%{_mandir}/man8/net.8*
%{_mandir}/man8/pdbedit.8*

# common libraries
%{_libdir}/samba/libpopt_samba3.so
%{_libdir}/samba/pdb

%if %with_pam_smbpass
%{_libdir}/security/pam_smbpass.so
%endif

### DC
%files dc
%defattr(-,root,root)
%{_libdir}/samba/ldb
%{_libdir}/samba/libdfs_server_ad.so
%{_libdir}/samba/libdsdb-module.so

%if %with_dc
%{_bindir}/samba-tool
%{_sbindir}/samba
%{_sbindir}/samba_kcc
%{_sbindir}/samba_dnsupdate
%{_sbindir}/samba_spnupdate
%{_sbindir}/samba_upgradedns
%{_sbindir}/samba_upgradeprovision
%{_libdir}/mit_samba.so
%{_libdir}/samba/bind9/dlz_bind9.so
%{_libdir}/samba/libheimntlm-samba4.so.1
%{_libdir}/samba/libheimntlm-samba4.so.1.0.1
%{_libdir}/samba/libkdc-samba4.so.2
%{_libdir}/samba/libkdc-samba4.so.2.0.0
%{_libdir}/samba/libpac.so
%{_libdir}/samba/gensec
%dir /var/lib/samba/sysvol
%{_datadir}/samba/setup
%{_mandir}/man8/samba.8*
%{_mandir}/man8/samba-tool.8*
%else # with_dc
%doc %{_defaultdocdir}/%{name}/README.dc
%exclude %{_mandir}/man8/samba.8*
%exclude %{_mandir}/man8/samba-tool.8*
%endif # with_dc

### DC-LIBS
%files dc-libs
%defattr(-,root,root)
%if %with_dc
%{_libdir}/samba/libprocess_model.so
%{_libdir}/samba/libservice.so
%{_libdir}/samba/process_model
%{_libdir}/samba/service
%{_libdir}/libdcerpc-server.so.*
%{_libdir}/samba/libntvfs.so
%{_libdir}/samba/libposix_eadb.so
%{_libdir}/samba/bind9/dlz_bind9_9.so
%else
%doc %{_defaultdocdir}/%{name}/README.dc-libs
%endif # with_dc

### DEVEL
%files devel
%defattr(-,root,root)
%{_includedir}/samba-4.0/charset.h
%{_includedir}/samba-4.0/core/doserr.h
%{_includedir}/samba-4.0/core/error.h
%{_includedir}/samba-4.0/core/ntstatus.h
%{_includedir}/samba-4.0/core/werror.h
%{_includedir}/samba-4.0/credentials.h
%{_includedir}/samba-4.0/dcerpc.h
%{_includedir}/samba-4.0/dlinklist.h
%{_includedir}/samba-4.0/domain_credentials.h
%{_includedir}/samba-4.0/gen_ndr/atsvc.h
%{_includedir}/samba-4.0/gen_ndr/auth.h
%{_includedir}/samba-4.0/gen_ndr/dcerpc.h
%{_includedir}/samba-4.0/gen_ndr/epmapper.h
%{_includedir}/samba-4.0/gen_ndr/krb5pac.h
%{_includedir}/samba-4.0/gen_ndr/lsa.h
%{_includedir}/samba-4.0/gen_ndr/mgmt.h
%{_includedir}/samba-4.0/gen_ndr/misc.h
%{_includedir}/samba-4.0/gen_ndr/nbt.h
%{_includedir}/samba-4.0/gen_ndr/drsblobs.h
%{_includedir}/samba-4.0/gen_ndr/drsuapi.h
%{_includedir}/samba-4.0/gen_ndr/ndr_drsblobs.h
%{_includedir}/samba-4.0/gen_ndr/ndr_drsuapi.h
%{_includedir}/samba-4.0/gen_ndr/ndr_atsvc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_atsvc_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_dcerpc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_epmapper.h
%{_includedir}/samba-4.0/gen_ndr/ndr_epmapper_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_krb5pac.h
%{_includedir}/samba-4.0/gen_ndr/ndr_mgmt.h
%{_includedir}/samba-4.0/gen_ndr/ndr_mgmt_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_misc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_nbt.h
%{_includedir}/samba-4.0/gen_ndr/ndr_samr.h
%{_includedir}/samba-4.0/gen_ndr/ndr_samr_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_svcctl.h
%{_includedir}/samba-4.0/gen_ndr/ndr_svcctl_c.h
%{_includedir}/samba-4.0/gen_ndr/netlogon.h
%{_includedir}/samba-4.0/gen_ndr/samr.h
%{_includedir}/samba-4.0/gen_ndr/security.h
%{_includedir}/samba-4.0/gen_ndr/server_id.h
%{_includedir}/samba-4.0/gen_ndr/svcctl.h
%{_includedir}/samba-4.0/gensec.h
%{_includedir}/samba-4.0/ldap-util.h
%{_includedir}/samba-4.0/ldap_errors.h
%{_includedir}/samba-4.0/ldap_message.h
%{_includedir}/samba-4.0/ldap_ndr.h
%{_includedir}/samba-4.0/ldb_wrap.h
%{_includedir}/samba-4.0/lookup_sid.h
%{_includedir}/samba-4.0/machine_sid.h
%{_includedir}/samba-4.0/ndr.h
%{_includedir}/samba-4.0/ndr/ndr_drsblobs.h
%{_includedir}/samba-4.0/ndr/ndr_drsuapi.h
%{_includedir}/samba-4.0/ndr/ndr_svcctl.h
%{_includedir}/samba-4.0/ndr/ndr_nbt.h
%{_includedir}/samba-4.0/netapi.h
%{_includedir}/samba-4.0/param.h
%{_includedir}/samba-4.0/passdb.h
%{_includedir}/samba-4.0/policy.h
%{_includedir}/samba-4.0/read_smb.h
%{_includedir}/samba-4.0/registry.h
%{_includedir}/samba-4.0/roles.h
%{_includedir}/samba-4.0/rpc_common.h
%{_includedir}/samba-4.0/samba/session.h
%{_includedir}/samba-4.0/samba/version.h
%{_includedir}/samba-4.0/share.h
%{_includedir}/samba-4.0/smb2.h
%{_includedir}/samba-4.0/smb2_constants.h
%{_includedir}/samba-4.0/smb2_create_blob.h
%{_includedir}/samba-4.0/smb2_lease.h
%{_includedir}/samba-4.0/smb2_signing.h
%{_includedir}/samba-4.0/smb_cli.h
%{_includedir}/samba-4.0/smb_cliraw.h
%{_includedir}/samba-4.0/smb_common.h
%{_includedir}/samba-4.0/smb_composite.h
%{_includedir}/samba-4.0/smbconf.h
%{_includedir}/samba-4.0/smb_constants.h
%{_includedir}/samba-4.0/smb_ldap.h
%{_includedir}/samba-4.0/smbldap.h
%{_includedir}/samba-4.0/smb_raw.h
%{_includedir}/samba-4.0/smb_raw_interfaces.h
%{_includedir}/samba-4.0/smb_raw_signing.h
%{_includedir}/samba-4.0/smb_raw_trans2.h
%{_includedir}/samba-4.0/smb_request.h
%{_includedir}/samba-4.0/smb_seal.h
%{_includedir}/samba-4.0/smb_signing.h
%{_includedir}/samba-4.0/smb_unix_ext.h
%{_includedir}/samba-4.0/smb_util.h
%{_includedir}/samba-4.0/tdr.h
%{_includedir}/samba-4.0/tsocket.h
%{_includedir}/samba-4.0/tsocket_internal.h
%{_includedir}/samba-4.0/samba_util.h
%{_includedir}/samba-4.0/util/attr.h
%{_includedir}/samba-4.0/util/byteorder.h
%{_includedir}/samba-4.0/util/data_blob.h
%{_includedir}/samba-4.0/util/debug.h
%{_includedir}/samba-4.0/util/memory.h
%{_includedir}/samba-4.0/util/safe_string.h
%{_includedir}/samba-4.0/util/string_wrappers.h
%{_includedir}/samba-4.0/util/talloc_stack.h
%{_includedir}/samba-4.0/util/tevent_ntstatus.h
%{_includedir}/samba-4.0/util/tevent_unix.h
%{_includedir}/samba-4.0/util/tevent_werror.h
%{_includedir}/samba-4.0/util/time.h
%{_includedir}/samba-4.0/util/xfile.h
%{_includedir}/samba-4.0/util_ldb.h
%{_libdir}/libdcerpc-atsvc.so
%{_libdir}/libdcerpc-binding.so
%{_libdir}/libdcerpc-samr.so
%{_libdir}/libdcerpc.so
%{_libdir}/libgensec.so
%{_libdir}/libndr-krb5pac.so
%{_libdir}/libndr-nbt.so
%{_libdir}/libndr-standard.so
%{_libdir}/libndr.so
%{_libdir}/libnetapi.so
%{_libdir}/libregistry.so
%{_libdir}/libsamba-credentials.so
%{_libdir}/libsamba-hostconfig.so
%{_libdir}/libsamba-policy.so
%{_libdir}/libsamba-util.so
%{_libdir}/libsamdb.so
%{_libdir}/libsmbclient-raw.so
%{_libdir}/libsmbconf.so
%{_libdir}/libtevent-util.so
%{_libdir}/pkgconfig/dcerpc.pc
%{_libdir}/pkgconfig/dcerpc_atsvc.pc
%{_libdir}/pkgconfig/dcerpc_samr.pc
%{_libdir}/pkgconfig/gensec.pc
%{_libdir}/pkgconfig/ndr.pc
%{_libdir}/pkgconfig/ndr_krb5pac.pc
%{_libdir}/pkgconfig/ndr_nbt.pc
%{_libdir}/pkgconfig/ndr_standard.pc
%{_libdir}/pkgconfig/netapi.pc
%{_libdir}/pkgconfig/registry.pc
%{_libdir}/pkgconfig/samba-credentials.pc
%{_libdir}/pkgconfig/samba-hostconfig.pc
%{_libdir}/pkgconfig/samba-policy.pc
%{_libdir}/pkgconfig/samba-util.pc
%{_libdir}/pkgconfig/samdb.pc
%{_libdir}/pkgconfig/smbclient-raw.pc
%{_libdir}/libpdb.so
%{_libdir}/libsmbldap.so

%if %with_dc
%{_includedir}/samba-4.0/dcerpc_server.h
%{_libdir}/libdcerpc-server.so
%{_libdir}/pkgconfig/dcerpc_server.pc
%endif

%if %with_talloc
%{_includedir}/samba-4.0/pytalloc.h
%endif

%if ! %with_libsmbclient
%{_includedir}/samba-4.0/libsmbclient.h
%{_includedir}/samba-4.0/smb_share_modes.h
%endif # ! with_libsmbclient

%if ! %with_libwbclient
%{_includedir}/samba-4.0/wbclient.h
%endif # ! with_libwbclient

### LIBS
%files libs
%defattr(-,root,root)
%{_libdir}/libdcerpc-atsvc.so.*
%{_libdir}/libdcerpc-binding.so.*
%{_libdir}/libdcerpc-samr.so.*
%{_libdir}/libdcerpc.so.*
%{_libdir}/libgensec.so.*
%{_libdir}/libndr-krb5pac.so.*
%{_libdir}/libndr-nbt.so.*
%{_libdir}/libndr-standard.so.*
%{_libdir}/libndr.so.*
%{_libdir}/libnetapi.so.*
%{_libdir}/libregistry.so.*
%{_libdir}/libsamba-credentials.so.*
%{_libdir}/libsamba-hostconfig.so.*
%{_libdir}/libsamba-policy.so.*
%{_libdir}/libsamba-util.so.*
%{_libdir}/libsamdb.so.*
%{_libdir}/libsmbclient-raw.so.*
%{_libdir}/libsmbconf.so.*
%{_libdir}/libtevent-util.so.*
%{_libdir}/libpdb.so.*
%{_libdir}/libsmbldap.so.*

# libraries needed by the public libraries
%{_libdir}/samba/libCHARSET3.so
%{_libdir}/samba/libMESSAGING.so
%{_libdir}/samba/libLIBWBCLIENT_OLD.so
%{_libdir}/samba/libaddns.so
%{_libdir}/samba/libads.so
%{_libdir}/samba/libasn1util.so
%{_libdir}/samba/libauth.so
%{_libdir}/samba/libauth4.so
%{_libdir}/samba/libauth_sam_reply.so
%{_libdir}/samba/libauth_unix_token.so
%{_libdir}/samba/libauthkrb5.so
%{_libdir}/samba/libccan.so
%{_libdir}/samba/libcli-ldap-common.so
%{_libdir}/samba/libcli-ldap.so
%{_libdir}/samba/libcli-nbt.so
%{_libdir}/samba/libcli_cldap.so
%{_libdir}/samba/libcli_smb_common.so
%{_libdir}/samba/libcli_spoolss.so
%{_libdir}/samba/libcliauth.so
#%{_libdir}/samba/libclidns.so
%{_libdir}/samba/libcluster.so
%{_libdir}/samba/libcmdline-credentials.so
%{_libdir}/samba/libdbwrap.so
%{_libdir}/samba/libdcerpc-samba.so
%{_libdir}/samba/libdcerpc-samba4.so
%{_libdir}/samba/liberrors.so
%{_libdir}/samba/libevents.so
%{_libdir}/samba/libflag_mapping.so
%{_libdir}/samba/libgpo.so
%{_libdir}/samba/libgse.so
%{_libdir}/samba/libinterfaces.so
%{_libdir}/samba/libkrb5samba.so
%{_libdir}/samba/libldbsamba.so
%{_libdir}/samba/liblibcli_lsa3.so
%{_libdir}/samba/liblibcli_netlogon3.so
%{_libdir}/samba/liblibsmb.so
%{_libdir}/samba/libsmb_transport.so
%{_libdir}/samba/libmsrpc3.so
%{_libdir}/samba/libndr-samba.so
%{_libdir}/samba/libndr-samba4.so
%{_libdir}/samba/libnet_keytab.so
%{_libdir}/samba/libnetif.so
%{_libdir}/samba/libnon_posix_acls.so
%{_libdir}/samba/libnpa_tstream.so
%{_libdir}/samba/libprinting_migrate.so
%{_libdir}/samba/libreplace.so
%{_libdir}/samba/libsamba-modules.so
%{_libdir}/samba/libsamba-net.so
%{_libdir}/samba/libsamba-security.so
%{_libdir}/samba/libsamba-sockets.so
%{_libdir}/samba/libsamba_python.so
%{_libdir}/samba/libsamdb-common.so
%{_libdir}/samba/libsecrets3.so
%{_libdir}/samba/libserver-role.so
%{_libdir}/samba/libshares.so
%{_libdir}/samba/libsamba3-util.so
%{_libdir}/samba/libsmbd_base.so
%{_libdir}/samba/libsmbd_conn.so
%{_libdir}/samba/libsmbd_shim.so
%{_libdir}/samba/libsmbldaphelper.so
%{_libdir}/samba/libsmbpasswdparser.so
%{_libdir}/samba/libsmbregistry.so
%{_libdir}/samba/libtdb-wrap.so
%{_libdir}/samba/libtdb_compat.so
%{_libdir}/samba/libtrusts_util.so
%{_libdir}/samba/libutil_cmdline.so
%{_libdir}/samba/libutil_ntdb.so
%{_libdir}/samba/libutil_reg.so
%{_libdir}/samba/libutil_setid.so
%{_libdir}/samba/libutil_tdb.so
%{_libdir}/samba/libxattr_tdb.so

%if %with_dc
%{_libdir}/samba/libdb-glue.so
%{_libdir}/samba/libHDB_SAMBA4.so
%{_libdir}/samba/libasn1-samba4.so.8
%{_libdir}/samba/libasn1-samba4.so.8.0.0
%{_libdir}/samba/libgssapi-samba4.so.2
%{_libdir}/samba/libgssapi-samba4.so.2.0.0
%{_libdir}/samba/libhcrypto-samba4.so.5
%{_libdir}/samba/libhcrypto-samba4.so.5.0.1
%{_libdir}/samba/libhdb-samba4.so.11
%{_libdir}/samba/libhdb-samba4.so.11.0.2
%{_libdir}/samba/libheimbase-samba4.so.1
%{_libdir}/samba/libheimbase-samba4.so.1.0.0
%{_libdir}/samba/libhx509-samba4.so.5
%{_libdir}/samba/libhx509-samba4.so.5.0.0
%{_libdir}/samba/libkrb5-samba4.so.26
%{_libdir}/samba/libkrb5-samba4.so.26.0.0
%{_libdir}/samba/libroken-samba4.so.19
%{_libdir}/samba/libroken-samba4.so.19.0.1
%{_libdir}/samba/libwind-samba4.so.0
%{_libdir}/samba/libwind-samba4.so.0.0.0
%endif

%if %{with_ldb}
%{_libdir}/samba/libldb.so.1
%{_libdir}/samba/libldb.so.%{ldb_version}
%{_libdir}/samba/libpyldb-util.so.1
%{_libdir}/samba/libpyldb-util.so.%{ldb_version}
%endif
%if %{with_talloc}
%{_libdir}/samba/libtalloc.so.2
%{_libdir}/samba/libtalloc.so.%{talloc_version}
%{_libdir}/samba/libpytalloc-util.so.2
%{_libdir}/samba/libpytalloc-util.so.%{talloc_version}
%endif
%if %{with_tevent}
%{_libdir}/samba/libtevent.so.0
%{_libdir}/samba/libtevent.so.%{tevent_version}
%endif
%if %{with_tdb}
%{_libdir}/samba/libtdb.so.1
%{_libdir}/samba/libtdb.so.%{tdb_version}
%endif
## we don't build it for now
%if %{with_ntdb}
%{_libdir}/samba/libntdb.so.0
%{_libdir}/samba/libntdb.so.%{ntdb_version}
%endif

%if ! %with_libsmbclient
%{_libdir}/samba/libsmbclient.so.*
%{_libdir}/samba/libsmbsharemodes.so.*
%{_mandir}/man7/libsmbclient.7*
%endif # ! with_libsmbclient

%if ! %with_libwbclient
%{_libdir}/samba/libwbclient.so.*
%{_libdir}/samba/libwinbind-client.so
%endif # ! with_libwbclient

### LIBSMBCLIENT
%if %with_libsmbclient
%files -n libsmbclient
%defattr(-,root,root)
%attr(755,root,root) %{_libdir}/libsmbclient.so.*
%attr(755,root,root) %{_libdir}/libsmbsharemodes.so.*

### LIBSMBCLIENT-DEVEL
%files -n libsmbclient-devel
%defattr(-,root,root)
%{_includedir}/samba-4.0/libsmbclient.h
%{_includedir}/samba-4.0/smb_share_modes.h
%{_libdir}/libsmbclient.so
%{_libdir}/libsmbsharemodes.so
%{_libdir}/pkgconfig/smbclient.pc
%{_libdir}/pkgconfig/smbsharemodes.pc
%{_mandir}/man7/libsmbclient.7*
%endif # with_libsmbclient

### LIBWBCLIENT
%if %with_libwbclient
%files -n libwbclient
%defattr(-,root,root)
%{_libdir}/libwbclient.so.*
%{_libdir}/samba/libwinbind-client.so

### LIBWBCLIENT-DEVEL
%files -n libwbclient-devel
%defattr(-,root,root)
%{_includedir}/samba-4.0/wbclient.h
%{_libdir}/libwbclient.so
%{_libdir}/pkgconfig/wbclient.pc
%endif # with_libwbclient

### PIDL
%files pidl
%defattr(-,root,root,-)
%{perl_vendorlib}/Parse/Pidl*
%{_mandir}/man1/pidl*
%{_mandir}/man3/Parse::Pidl*
%attr(755,root,root) %{_bindir}/pidl

### PYTHON
%files python
%defattr(-,root,root,-)
%{python_sitearch}/*

### TEST
%files test
%defattr(-,root,root)
%{_bindir}/gentest
%{_bindir}/locktest
%{_bindir}/masktest
%{_bindir}/ndrdump
%{_bindir}/smbtorture
%{_libdir}/libtorture.so.*
%{_libdir}/samba/libsubunit.so
%if %with_dc
%{_libdir}/samba/libdlz_bind9_for_torture.so
%endif
%{_mandir}/man1/gentest.1*
%{_mandir}/man1/locktest.1*
%{_mandir}/man1/masktest.1*
%{_mandir}/man1/ndrdump.1*
%{_mandir}/man1/smbtorture.1*
%{_mandir}/man1/vfstest.1*

%if %{with testsuite}
# files to ignore in testsuite mode
%{_libdir}/samba/libnss_wrapper.so
%{_libdir}/samba/libsocket_wrapper.so
%{_libdir}/samba/libuid_wrapper.so
%endif

### TEST-DEVEL
%files test-devel
%defattr(-,root,root)
%{_includedir}/samba-4.0/torture.h
%{_libdir}/libtorture.so
%{_libdir}/pkgconfig/torture.pc

### WINBIND
%files winbind
%defattr(-,root,root)
#%{_bindir}/wbinfo3
%{_libdir}/samba/idmap
%{_libdir}/samba/nss_info
%{_libdir}/samba/libnss_info.so
%{_libdir}/samba/libidmap.so
%{_sbindir}/winbindd
%attr(750,root,wbpriv) %dir /var/lib/samba/winbindd_privileged
%{_unitdir}/winbind.service
%{_sysconfdir}/NetworkManager/dispatcher.d/30-winbind
%{_mandir}/man8/winbindd.8*
%{_mandir}/man8/idmap_*.8*

### WINBIND-CLIENTS
%files winbind-clients
%defattr(-,root,root)
%{_bindir}/ntlm_auth
%{_bindir}/wbinfo
%{_libdir}/libnss_winbind.so*
%{_libdir}/libnss_wins.so*
%{_libdir}/security/pam_winbind.so
%config(noreplace) %{_sysconfdir}/security/pam_winbind.conf
%{_mandir}/man1/ntlm_auth.1*
#exclude %%{_mandir}/man1/ntlm_auth4.1*
%{_mandir}/man1/wbinfo.1*
%{_mandir}/man5/pam_winbind.conf.5*
%{_mandir}/man8/pam_winbind.8*

### WINBIND-KRB5-LOCATOR
%files winbind-krb5-locator
%defattr(-,root,root)
%ghost %{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so
%{_libdir}/winbind_krb5_locator.so
%{_mandir}/man7/winbind_krb5_locator.7*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.9-2m)
- rebuild against perl-5.20.0

* Wed Jun 25 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.9-1m)
- [SECURITY] CVE-2014-0244 CVE-2014-3493
- update to 4.1.9

* Sun May 25 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.7-2m)
- add patch1 to fix startup smbd problem

* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.7-1m)
- update to 4.1.7

* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.6-1m)
- [SECURITY] CVE-2013-4496 CVE-2013-6442
- update to 4.1.6

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.4-2m)
- rebuild against perl-5.18.2

* Sat Jan 11 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.4-1m)
- update to 4.1.4

* Thu Dec 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-1m)
- [SECURITY] CVE-2012-6150 CVE-2013-4408
- update to 4.1.3

* Sun Nov 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.2-1m)
- update to 4.1.2

* Tue Nov 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.1-1m)
- [SECURITY] CVE-2013-4475 CVE-2013-4476
- update to 4.1.1

* Fri Oct 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to 4.1.0
- swat was OBSOLETED

* Tue Oct  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.10-1m)
- update to 4.0.10

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.9-1m)
- update to 4.0.9

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.8-2m)
- rebuild against perl-5.18.1

* Tue Aug  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.8-1m)
- [SECURITY] CVE-2013-4124
- update to 4.0.8

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.7-1m)
- update to 4.0.7

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.6-1m)
- update to 4.0.6
- rebuild against perl-5.18.0

* Tue Apr  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.5-1m)
- update to 4.0.5

* Tue Mar 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- [SECURITY] CVE-2013-1863
- update to 4.0.4

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-2m)
- rebuild against perl-5.16.3

* Wed Feb  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- [SECURITY] CVE-2013-0213 CVE-2013-0214
- update to 4.0.2

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-2m)
- apply mnb.service patch, many thanks to toshiharu san

* Wed Jan 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- [SECURITY] CVE-2013-0172
- update to 4.0.1

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to 4.0.0 (sync Fedora)

* Tue Dec 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.10-1m)
- update to 3.6.10

* Wed Oct 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-1m)
- update to 3.6.9

* Tue Aug  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.7-1m)
- update to 3.6.7

* Thu Jul 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.6-1m)
- update to 3.6.6

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.5-1m)
- [SECURITY] CVE-2012-2111
- update to 3.6.5

* Wed Apr 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.4-1m)
- [SECURITY] CVE-2012-1182
- update to 3.6.4

* Tue Mar 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.3-2m)
- better support systemd
-- post section support native systemd

* Mon Jan 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.3-1m)
- [SECURITY] CVE-2012-0817
- update to 3.6.3

* Fri Oct 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update 3.6.1

* Sun Oct  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-3m)
- support systemd

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.0-2m)
- release a directory provided by openldap-servers

* Thu Aug 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.11-1m)
- update to 3.5.11

* Wed Jul 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-1m)
- [SECURITY] CVE-2011-2522 CVE-2011-2694
- update to 3.5.10

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-1m)
- update to 3.5.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-2m)
- rebuild for new GCC 4.6

* Mon Mar  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to 3.5.8

* Tue Mar  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- [SECURITY] CVE-2011-0719
- update to 3.5.7

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.6-2m)
- rebuild for new GCC 4.5

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to 3.5.6

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- [SECURITY] CVE-2010-3069
- update to 3.5.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.4-3m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.4-2m)
- separate out cifs-utils to cifs-utils.spec

* Thu Jun 24 2010 MARITA Koichi <pulsar@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Thu May 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.2-2m)
- rebuild against readline6

* Wed Apr  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.2-1m)
- [SECURITY] CVE-2010-1635 CVE-2010-1642
- update to 3.5.2

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.1-1m)
- [SECURITY] CVE-2010-0728
- update to 3.5.1

* Tue Mar  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-1m)
- update to 3.5.0
- sync with Fedora devel

* Fri Feb 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.6-1m)
- [SECURITY] CVE-2010-0926
- update to 3.4.6

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.5-2m)
- [SECURITY] CVE-2009-3297 CVE-2010-0547 CVE-2010-0787
- import security patch from F-12
- import #541267 patch (fix crash in pdbedit) from F-12

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.5-1m)
- update to 3.4.5

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.4-1m)
- update to 3.4.4

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.3-1m)
- use external talloc and tdb
- update to 3.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.9-1m)
- updateto 3.3.9

* Sat Oct  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.8-1m)
- [SECURITY] CVE-2009-2813 CVE-2009-2948 CVE-2009-2906
- update to 3.3.8

* Sat Aug  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.7-1m)
- update to 3.3.7

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-1m)
- [SECURITY] CVE-2009-1886 CVE-2009-1888
- update to 3.3.6

* Thu Jun 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-2m)
- set tdb_rel and talloc_rel
- DO NOT DECREASE Release without Version upgrade

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.5-1m)
- update to 3.3.5

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.4-2m)
- rebuild against libunwind-0.99

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.4-1m)
- update to 3.3.4
- import patches from Fedora devel

* Sat Feb  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.0-1m)
- update to 3.3.0
-- update Source1,5,8,999 from Rawhide (3.3.0-0.25)
-- import an upstream patch (Patch201)
- License: GPLv3+ and LGPLv3+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.7-2m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.7-1m)
- [SECURITY] CVE-2009-0022
- update to 3.2.7

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.5-1m)
- [SECURITY] CVE-2008-4314
- update to 3.2.5

* Fri Sep 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.4-1m)
- update to 3.2.4

* Sat Sep 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Sun Aug 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2
- cifs.spnego was renamed to cifs.upcall

* Wed Aug  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Jul 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-2m)
- modify Requires

* Sat Jul 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0 and sync with Fedora devel

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.30-1m)
- [SECURITY] CVE-2008-1105
- update to 3.0.30

* Sun May 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.29-1m)
- update to 3.0.29
- delete unused patch

* Sun Apr 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.28a-3m)
- add patch1001 (for kernel 2.6.25) build fix only

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.28a-2m)
- rebuild against gcc43

* Mon Mar 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.28a-1m)
- update to 3.0.28a

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.28-3m)
- rebuild against openldap-2.4.8
- fix glibc-2.8

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.28-2m)
- revised hyper links from SWAT top page

* Tue Dec 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.28-1m)
- [SECURITY] CVE-2007-6015
- version up 3.0.28

* Thu Nov 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.27a-1m)
- [SECURITY] CVE-2007-4572 CVE-2007-5398

* Sat Nov 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.27-1m)
- [SECURITY] CVE-2007-5398
- version up 3.0.27
- add samba-3.0.27-smbfs-mount-regression.patch, this patch will not need 3.0.27a

* Thu Sep 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.26a-1m)
- [SECURITY] CVE-2007-4138
- version up 3.0.26a

* Sat Jun 16 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.0.25a-3m)
- rebuild against cups-1.2.11-3m for lib64 issue

* Sun May 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.25a-2m)
- modify Requires for mph-get-check

* Sun May 27 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.25a-1m)
- [SECURITY] CVE-2007-2444 CVE-2007-2446 CVE-2007-2447
- version up 3.0.25a
- sync Fedora

* Thu May 10 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (3.0.24-1m)
- [SECURITY] CVE-2007-0452 CVE-2007-0454
- version up 3.0.24

* Sat Nov 25 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (3.0.23d-1m)
- version up 3.0.23d
- remove samba-3.0.20pre1-man.patch

* Wed Sep 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.23c-2m)
- modify Prereq for mph-get-check

* Fri Sep 08 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (3.0.23c-1m)
- version up 3.0.23c

* Sun Aug 13 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.23b-1m)
- version up 3.0.23b

* Tue Jul 11 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.0.22-4m)
- [SECURITY] CVE-2006-3403
- http://www.samba.org/samba/security/CAN-2006-3403.html

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.22-3m)
- rebuild against readline-5.0

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.22-2m)
- rebuild against openssl-0.9.8a

* Mon Mar 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.0.22-1m)
- update to 3.0.22
- [SECURITY] CVE-2006-1059
- http://us1.samba.org/samba/security/CVE-2006-1059.html

* Sun Jan 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.21a-2m)
- modify CFLAGS to avoid crashing arts with xine-lib-smb (gcc-4.1.0-0.8m)

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.21a-1m)
- upgrade samba-3.0.21a
- sync with fc-devel

* Tue Apr 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.14a-2m)
- modify smb.conf

* Tue Apr 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.14a-1m)
- update

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.0.10-2m)
- revised docdir permission

* Sat Feb 5 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.0.10-1m)
- security fix. http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-1154
- import all patches and spec from FC devel

* Mon Nov 22 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (3.0.9-1m)
- minor bug fix release.

* Wed Nov 10 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (3.0.8-1m)
- update to 3.0.8
  including security fixes.
  [references]
  http://www.st.ryukoku.ac.jp/~kjm/security/ml-archive/bugtraq/2004.11/msg00096.html
  http://us1.samba.org/samba/ftp/WHATSNEW-3-0-8.txt

* Sun Sep 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.7-1m)
- update to 3.0.7

* Fri Sep  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.6-1m)
- including security fixes

* Fri Jul 30 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.5-2m)
- add International Patch

* Fri Jul 30 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (3.0.5-1m)
- verion up (SECURITY FIX)
- See CAN-2004-0600 and CAN-2004-0686

* Tue May 11 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (3.0.4-1m)

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (3.0.2a-3m)
- revised spec for rpm 4.2.

* Sun Mar  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.2a-2m)
- generate i586 binary if target is i686 (-march=i686 causes internal compiler error on gcc-3.2.3-12m)

* Sun Feb 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.2a-1m)

* Sun Feb 15 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (3.0.2-1m)
- Version 3.0.2
- added mount.cifs
- added /usr/bin/tdbdump
- removed testparm patch as it's already merged
- updated logfiles patch
- removed old patch files from repository

* Wed Jan  7 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (3.0.0-5m)
- add some binaries to "%%files common" section
  (smbcquotas, profiles, ntlm_auth and pdbedit)
- remove "%%makeinstall installclientlib" from %%install section
  because "make installclientlib" is included in "make install"

* Fri Jan  2 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.0-4m)
- fix mount option problem
  see http://www.samba.gr.jp/ml/article/sugj-tech/msg05996.html

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.0-3m)
- rebuild against openldap

* Wed Nov  5 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (3.0.0-2m)
- remove %%{with_gcc296} macros.
  It is not necessary any longer.(may be...)

* Tue Nov 4 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (3.0.0-1m)
- version 3.0.0
- remove old patches
- add samba-3.0.0-logfiles.patch (rawhide)
- add samba-3.0.0beta1-pipedir.patch (rawhide)
- add samba-3.0.0rc3-nmbd-netbiosname.patch (rawhide)
- add samba-3.0.0rc4-testparm.patch (rawhide)
- modify %%files section and %%install section

* Fri Jul 25 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.8a-7m)
- update to beta7

* Wed Jul 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.8a-6m)
- include libsmbclient

* Fri Jul 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.8a-5m)
- update to ja-1.1-beta6
- enable smbwrapper

* Mon Jul 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.8a-4m)
- update to ja-1.1-beta5
- glab some man pages

* Mon Jul  7 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.8a-3m)
- update to ja-1.1-beta4

* Thu Jun 19 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.8a-2m)
- update to ja-1.1-beta3
- revise configure options

* Tue May 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.8a-1m)
- update  to 2.2.8a release
- add %%{?_smp_mflags}

* Thu Apr 24 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2.8a-0.1.2m)
- enable --with-ldapsam and install smbldap-tools when %%{with_ldapsam} == 1.

* Wed Apr  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.8a-0.1.1m)
  update to 2.2.8a-ja-1.0-alpha1

* Tue Apr  8 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.7a-3m)
- apply security patch from 2.2.8a

* Tue Mar 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.7a-2m)
- update  to 2.2.7a-ja-1.1
  [security]  This release includes secutiry update
  see http://www.samba.gr.jp/ml/samba-jp/htdocs/200303.month/14349.html
- use gcc-3.2.2 by default

* Sun Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.7a-1m)
  rebuild against openssl 0.9.7a

* Sat Dec 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.5-5m)
- revise for ppc

* Fri Dec 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.5-4m)
- only samba-swat requires xinetd

* Mon Nov 25 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.5-3m)
-  include_specopt
-  use gcc-2.96 by default.
   if with_gcc296 is defined as 0, use gcc-3.2 instead.

* Thu Nov  7 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.5-2m)
- delete BuildPreReq: gcc2.95.3

* Thu Sep  5 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.2.5-1m)
- Version up to 2.2.5
- ja 1.0beta3
- samba-2.2.5-lprng.patch (fromrawhide)

* Mon Aug 12 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.2.4-1m)
- Ver. up
- *.pid is in /var/run/samba
- add samba-2.2.3-smbadduserloc.patch (from rawhide)
- add samba-2.2.4-logname.patch

* Fri Apr 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.2.2-6k)
- '%{initdir}' -> %{_initscriptdir}

* Thu Apr 25 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.2.2-4k)
- 'chmod -x' docs to avoid suspicious requirements

* Mon Apr 22 2002 Masahiro Takahata <takahata@kondara.org>
- (2.2.2-2k)
- upgrade to 2.2.2-ja-1.1
- separate swat package
- This spec file is rewritten so that
-      it might be based as much as possible on rawhide.

* Fri Nov 30 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0.10-16k)
- update ja-1.2
- fix installing documents

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.0.10-14k)
- nigittenu

* Mon Nov  5 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.10-12k)
- add samba-2.0.10-kernel-2.4.13-fix.patch for building on kernel 2.4.3

* Sun Nov  4 2001 Toru Hoshina <t@kondara.org>
- (2.0.10-10k)
- rebuild against new environment. because gcc 2.95.3 doesn't recognize
  -fno-merge-constants, use gcc_2_95_3 instead of gcc -V 2.95.3.

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (2.0.10-8k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.10-6k)
- errased the process concerning with /etc/service

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Tue Sep 09 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0.10-4k)
- update to samba-2.0.10-ja1.1

* Sat Jul  8 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0.10-2k)
- update to samba-2.0.10-ja1.0
- disable samba-2.0.7-wildcard.patch

* Thu Jul  5 2001 Toru Hoshina <toru@df-usa.com>
- (2.0.9-8k)
- fix privatedir path /etc to /etc/samba. (Jitterbug #905)

* Tue Jun 19 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0.9-6k)
- fix path /bin/paswd to /usr/bin/passwd (samba-fixpath.patch)
- delete smbrun.1*

* Mon May 28 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0.9-4k)
- update to samba-2.0.9-ja-1.0
- add BuildPrereq: pam-devel

* Sat May 12 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0.9-2k)
- update to samba-2.0.9-ja-1.0beta1

* Tue May  8 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.0.7-38k)
- remove %triggerpre
- update samba-2.0.7-ja.2.2

* Fri May  4 2001 Toru Hoshina <toru@df-usa.com>
- (2.0.7-36k)
- rebuild by gcc 2.95.3 :-P

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (2.0.7-34k)
- rebuild against openssl 0.9.6.

* Wed Feb  7 2001 WATABE Toyokazu <toy2@kondara.org>
- (2.0.7-32k)
- update to samba-2.0.7-ja.2.2b4.
- bugfix %files section.

* Mon Feb  5 2001 WATABE Toyokazu <toy2@kondara.org>
- (2.0.7-30k)
- add man-pages of smbumount.

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.7-29k)
- rebuild againt rpm-3.0.5-39k
- modified samba-fixinit.patch

* Thu Jan 26 2001 WATABE Toyokazu <toy2@kondara.org>
- (2.0.7-27k)
- update to 2.0.7-ja-2.2b3.
- remove using-samba and using_samba from docdir.
- be selectable inetd or xinetd. (default is xinetd)
- use logfilename log.smb and log.nmb.
- change dirctory of codepages to /etc/samba/codepages.

* Mon Jan 08 2001 WATABE Toyokazu <toy2@kondara.org>
- (2.0.7-25k)
- merge samba-ja to samba

* Fri Dec 01 2000 Kenichi Matsubara <m@kondara.org>
- package name change sambaJP to samba-ja.

* Mon Nov 07 2000 Toyokazu WATABE <toy2@kondara.org>
- add samba-2.0.7-swat.patch including security fix.

* Mon Nov 04 2000 Toyokazu WATABE <toy2@kondara.org>
- update for 2.0.7_ja_2.1-1k
- use macros.

* Mon Oct 16 2000 Toyokazu WATABE <toy2@kondara.org>
- 2.0.7_ja_2.0-1k

* Sun Sep 17 2000 Toyokazu WATABE <toy2@kondara.org>
- 2.0.7_ja_1.3-1k

* Tue Sep 05 2000 Toyokazu WATABE <toy2@kondara.org>
- 2.0.7_ja_20000904-1k

* Thu Aug 31 2000 Toyokazu WATABE <toy2@kondara.org>
- 2.0.7_ja_20000828-1k
- add samba-2.0.7-ja-cgi-security.patch
  (patch from samba-2.0.7-ja-1.2 to samba-2.0.7-ja-1.2a)
- adapt to FHS-2.1
- add /usr/sbin/tcpd to configuration of swat in inetd.conf.
- replace samba-2.0.3-fixinit.patch to samba-2.0.7-fixinit.patch

* Thu Aug 17 2000 Toyokazu Watabe <toy2@kondara.org>
- 2.0.7_ja_20000814-1k
- add Provides samba

* Sun Aug 14 2000 Toyokazu Watabe <toy2@kondara.org>
- 2.0.7_ja_20000814-0k (2.0.7-ja-1.3 beta release)

* Sun Aug 13 2000 Toyokazu Watabe <toy2@kondara.org>
- 2.0.7_ja_20000807-0k (2.0.7-ja-1.3 beta release)

* Sat Jul 8 2000 TAKAHASHI Motonobu <monyo@samba.gr.jp>
- Added smb-useradd and smb-userdel scripts
- Fixed some Japanese expression
- SWAT is invoked via tcp
- create STATUS..LCK during installation

* Fri Jul 7 2000 Hiroshi MIURA <miura@samba.gr.jp>
- Added Japanese manual pages

* Sun May 28 2000 AYUHANA Tomonori <l@kondara.org>
- Obsoletes: samba

* Sat May 27 2000 Hiroshi MIURA <miura@samba.gr.jp>
- Added support for "HowToUseSamba" Document
- Update spec file Japanese description.

* Sat May 6 2000 Hiroshi MIURA <miura@samba.gr.jp>
- Added support for "i18n SWAT"

* Mon Mar 13 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group )
- locale chenged ja_JP.ujis -> ja_JP.eucJP

* Tue Feb  8 2000 Toru Hoshina <t@kondara.org>
- fix alpha problem.

* Tue Jan 25 2000 Takaaki Tabuchi <tab@kondara.org>
- tiny change Version , Source URL.

* Mon Jan 24 2000 Tenkou N. Hattori <tnh@kondara.org>
- kondarize.

*Mon Jan 10 2000 kenichiro Hamano <hamano@samba.gr.jp>
- rebuild for Red Hat Linux 6.1(J) without smbsh

*Mon Jan 10 2000 Kenichiro Hamano <hamano@samba.gr.jp>
- rebuild for Laser5 Linux 6.0

*Fri Jan 07 2000 Kenichiro Hamano <hamano@samba.gr.jp>
- rebuild for Red Hat Linux 6.1(J)

*Mon Nov 12 1999 Motonobu Takahashi <monyo@samba.gr.jp>
- support japanese common names (translated by nakaya)

*Mon Sep 20 1999 Koji Odagiri <odagiri@samba.gr.jp>
- japanize swat (translated by nakaya)

*Mon Sep 20 1999 Satoshi Osawa <osawa@po.fes.co.jp>
- add japanese parts to spec (translated by nakaya)

* Sun Jul 24 1999 Hideya Hane <Vine@flatout.org>
- rebuild for Vine Linux 1.1

* Wed Jul 21 1999 Bill Nottingham <notting@redhat.com>
- update to 2.0.5
- fix mount.smb - smbmount options changed again.........
- fix postun. oops.
- update some stuff from the samba team's spec file.

* Fri Jun 18 1999 Bill Nottingham <notting@redhat.com>
- split off clients into separate package
- don't run samba by default

* Mon Jun 14 1999 Bill Nottingham <notting@redhat.com>
- fix one problem with mount.smb script
- fix smbpasswd on sparc with a really ugly kludge

* Thu Jun 10 1999 Dale Lovelace <dale@redhat.com>
- fixed logrotate script

* Tue May 25 1999 Bill Nottingham <notting@redhat.com>
- turn of 64-bit locking on 32-bit platforms

* Thu May 20 1999 Bill Nottingham <notting@redhat.com>
- so many releases, so little time
- explicitly uncomment 'printing = bsd' in sample config

* Tue May 18 1999 Bill Nottingham <notting@redhat.com>
- update to 2.0.4a
- fix mount.smb arg ordering

* Fri Apr 16 1999 Bill Nottingham <notting@redhat.com>
- go back to stop/start for restart (-HUP didn't work in testing)

* Fri Mar 26 1999 Bill Nottingham <notting@redhat.com>
- add a mount.smb to make smb mounting a little easier.
- smb filesystems apparently don't work on alpha. Oops.

* Thu Mar 25 1999 Bill Nottingham <notting@redhat.com>
- always create codepages

* Tue Mar 23 1999 Bill Nottingham <notting@redhat.com>
- logrotate changes

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 3)

* Fri Mar 19 1999 Preston Brown <pbrown@redhat.com>
- updated init script to use graceful restart (not stop/start)

* Tue Mar  9 1999 Bill Nottingham <notting@redhat.com>
- update to 2.0.3

* Thu Feb 18 1999 Bill Nottingham <notting@redhat.com>
- update to 2.0.2

* Mon Feb 15 1999 Bill Nottingham <notting@redhat.com>
- swat swat

* Tue Feb  9 1999 Bill Nottingham <notting@redhat.com>
- fix bash2 breakage in post script

* Fri Feb  5 1999 Bill Nottingham <notting@redhat.com>
- update to 2.0.0

* Mon Oct 12 1998 Cristian Gafton <gafton@redhat.com>
- make sure all binaries are stripped

* Thu Sep 17 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.9.18p10.
- fix %triggerpostun.

* Tue Jul 07 1998 Erik Troan <ewt@redhat.com>
- updated postun triggerscript to check $0
- clear /etc/codepages from %preun instead of %postun

* Mon Jun 08 1998 Erik Troan <ewt@redhat.com>
- made the %postun script a tad less agressive; no reason to remove
  the logs or lock file (after all, if the lock file is still there,
  samba is still running)
- the %postun and %preun should only exectute if this is the final
  removal
- migrated %triggerpostun from Red Hat's samba package to work around
  packaging problems in some Red Hat samba releases

* Sun Apr 26 1998 John H Terpstra <jht@samba.anu.edu.au>
- minor tidy up in preparation for release of 1.9.18p5
- added findsmb utility from SGI package

* Wed Mar 18 1998 John H Terpstra <jht@samba.anu.edu.au>
- Updated version and codepage info.
- Release to test name resolve order

* Sat Jan 24 1998 John H Terpstra <jht@samba.anu.edu.au>
- Many optimisations (some suggested by Manoj Kasichainula <manojk@io.com>
- Use of chkconfig in place of individual symlinks to /etc/rc.d/init/smb
- Compounded make line
- Updated smb.init restart mechanism
- Use compound mkdir -p line instead of individual calls to mkdir
- Fixed smb.conf file path for log files
- Fixed smb.conf file path for incoming smb print spool directory
- Added a number of options to smb.conf file
- Added smbadduser command (missed from all previous RPMs) - Doooh!
- Added smbuser file and smb.conf file updates for username map
