%global momorel 9
%global javer 1.5.19
%global patver ja.1
%global sfjp 40751
%global manver 1.4
%global manrel 1
%global mandate 20050813
%global pover 1-5-11.0924
%global withpo 1
%global patpath mutt-%{javer}-%{patver}

Name: mutt
%{?include_specopt}
%{?!sasl:    %global sasl 1}
%{?!slang:   %global slang 0}
%{?!hcache:  %global hcache 1}
%{?!idn:     %global idn 1}
%{?!gpgme:   %global gpgme 1}
%{?!dotlock: %global dotlock 1}
%{?!dlsgid:  %global dlsgid 0}
%if %dlsgid
%global dlpm 2755
%else
%global dlpm 0755
%endif

Summary: A text mode mail user agent.
Version: 1.5.19
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
URL: http://www.mutt.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: ftp://ftp.mutt.org/mutt/devel/mutt-%{version}.tar.gz 
NoSource: 0
Source1: http://dl.sourceforge.jp/mutt-j/%{sfjp}/mutt-%{javer}-%{patver}.tar.gz
NoSource: 1
Source3: dot.muttrc
Source5: http://www.emaillab.org/mutt/contrib/maildirnewmail
Source6: http://www.emaillab.org/mutt/contrib/mutt_ldap_query_ja.pl
Source7: http://www.emaillab.org/mutt/contrib/mutt_ldap_query_u.pl
Source8: http://www.spinnaker.de/mutt/view-x-face
Source9: view-x-face.w3mimg
Source10: http://www.emaillab.org/mutt/1.4/manual_ja-%{manver}i-%{manrel}.tar.gz
Source11: http://mutt-j.sourceforge.jp/manual.ja-%{mandate}.tar.gz

# Patch1 was made from Source1.
Patch1: mutt-1.5.19-ja.1.patch
Patch2: mutt-1.5.19-group.patch

Patch10: mutt-CVE-2009-3765.patch
Patch11: mutt-1.5.19-openssl.patch

BuildRequires: rpm-build >= 4.0.4-33m
BuildRequires: perl
BuildRequires: autoconf >= 2.59
BuildRequires: automake >= 1.9
BuildRequires: libxslt
BuildRequires: w3m
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: tokyocabinet >= 1.4.45
Requires:    smtpdaemon
Requires:    openssl >= 1.0.0

%if %{slang}
## 1.4.9-3m is utf8 version
BuildRequires: slang-devel >= 1.4.9-3m
Requires:    slang       >= 1.4.9-3m
%else
BuildRequires: ncurses-devel
Requires:    ncurses
%endif

%if %{sasl}
BuildRequires: cyrus-sasl-devel > 2
Requires:    cyrus-sasl > 2
%endif

%if %{hcache}
BuildRequires: qdbm-devel >= 1.8.77
Requires:    qdbm >= 1.8.77
%endif

%if %{idn}
BuildRequires: libidn-devel
Requires: libidn
%endif

%if %{gpgme}
BuildRequires: gpgme-devel > 0.4
Requires: gpgme > 0.4
%endif

%description
Mutt is a small but very powerful text-based MIME mail client.
Mutt is highly configurable, and is well suited to the mail power user
with advanced features like key bindings, keyboard macros, mail threading,
regular expression searches and a powerful pattern matching language
for selecting groups of messages.

You should install mutt if you've used mutt in the past and you prefer
it, or if you're new to mail programs and you haven't decided which
one you're going to use.

%prep
%setup -q -n mutt-%{version} -a 1 -a 10 -a 11
%patch1 -p1 -b .ja.1~
%patch2 -p1 -b .group~
find . -perm 000 -exec chmod u+wr '{}' ';'

%patch10 -p0 -b .CVE-2009-3765
%patch11 -p1 -b .openssl100

%build
autoreconf -fi
%if %{slang}
export CPPFLAGS=-I%{_includedir}/slang
%endif
%configure \
 --enable-locales-fix \
 --enable-pop \
 --enable-imap \
 --with-ssl \
 --without-wc-funcs \
%if %{slang}
 --with-slang=/usr/include/slang \
 --with-slanglib=slang \
%endif
%if %{sasl}
 --with-sasl \
%endif
%if %{hcache}
 --enable-hcache \
%endif
%if %{idn}
 --with-idn \
%else
 --without-idn \
%endif
%if %{gpgme}
 --enable-gpgme \
%endif
%if %{dotlock}
 --enable-external-dotlock
%else
 --disable-external-dotlock
%endif
# reserve this line...

%make

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_datadir}/config-sample/mutt
rm -rf mutt-doc-%{version}

%makeinstall \
 sharedir=%{buildroot}%{_sysconfdir} \
 docdir=%{_builddir}/mutt-%{version}/mutt-doc-%{version}

perl -pi -e 's|\${prefix}/doc/mutt|%{_docdir}/%{name}-%{version}|' \
 %{buildroot}%{_sysconfdir}/Muttrc

# docs
install -m 644 manual{_,.}ja.html       mutt-doc-%{version}
install -m 644 manual_ja.{sgml,tex,txt} mutt-doc-%{version}
install -m 644 manual.ja.{sgml,txt}     mutt-doc-%{version}

# samples
install -m 644 contrib/language*.txt mutt-doc-%{version}/samples/
rm -f dot.mutt.gpg dot.mutt-ja.rc
echo '# ~/.mutt.gpg' | cat - contrib/gpg.rc > dot.mutt.gpg
echo '# ~/.mutt-ja.rc' | cat - %{patpath}/samples/mutt-ja.rc > dot.mutt-ja.rc
perl -pi -e 's|/usr/.*/samples/|%{_datadir}/config-sample/mutt/dot.|' \
        dot.mutt-ja.rc
install -m 644 %{SOURCE3} dot.mutt.gpg \
 %{buildroot}%{_datadir}/config-sample/mutt
install -m 755 %{SOURCE5} %{SOURCE6} %{SOURCE7} %{SOURCE8} %{SOURCE9} \
 %{buildroot}%{_datadir}/config-sample/mutt
cat contrib/smime.rc >> \
 %{buildroot}%{_datadir}/config-sample/mutt/dot.muttrc

# remove conflicting file.
rm -f %{buildroot}%{_sysconfdir}/mime.types*
rm -f %{buildroot}%{_sysconfdir}/Muttrc.dist

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/Muttrc
%doc %{patpath}/samples
%doc %{patpath}/*.ja
%doc %{patpath}/*.ja.txt
%doc mutt-doc-%{version}/*
%docdir %{_datadir}/config-sample
%{_datadir}/config-sample/mutt
%attr(0755,root,mail) %{_bindir}/mutt
%if %{dotlock}
%attr(%{dlpm},root,mail) %{_bindir}/mutt_dotlock
%endif
%{_bindir}/flea
%{_bindir}/muttbug
%{_bindir}/pgpewrap
%{_bindir}/pgpring
%{_bindir}/smime_keys
%{_mandir}/man1/mutt.1*
%{_mandir}/man1/mutt_dotlock.1*
%{_mandir}/man1/flea.1*
%{_mandir}/man1/muttbug.1*
%{_mandir}/man5/mbox.5*
%{_mandir}/man5/mmdf.5*
%{_mandir}/man5/muttrc.5*
%{_datadir}/locale/*/LC_MESSAGES/mutt.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.19-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.19-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.19-7m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.19-6m)
- rebuild againt tokyocabinet

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.19-5m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.19-4m)
- apply openssl100 patch

* Sun Jan 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.19-3m)
- [SECURITY] CVE-2009-3765
- merge the upstream changes (6016:dc09812e63a3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.19-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.19-1m)
- [SECURITY] CVE-2009-3766
- update to 1.5.19

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.14-7m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.14-6m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.14-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.14-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.14-3m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.14-2m)
- rebuild against qdbm-1.8.77

* Wed Jun  7 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.14-1m)
- [SECURITY] CVE-2007-1558 CVE-2007-2683
- update to 1.5.14
- delete unused patches
- clean up spec
- revival SSP

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.13-3m)
- rebuild against qdbm-1.8.75

* Wed Dec 27 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.5.13-2m)
- enable to build on not i686 arch

* Wed Dec 27 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.5.13-1m)
- up to 1.5.13

* Wed Aug 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.11-0.20050924.10m)
- rebuild against qdbm-1.8.68

* Tue Jul 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.11-0.20050924.9m)
- rebuild against qdbm-1.8.61

* Tue Jun 27 2006 TAKAHASHI Tamotsu <tamo>
- (1.5.11-0.20050924.8m)
- [SECURITY] Fix IMAP "NAMESPACE" response buffer overflow
 http://secunia.com/advisories/20810/ (patch9)
- Fix xsltproc error: Maybe I installed docbook-dtds incorrectly,
 but I believe this patch is safe anyway (patch10)
- correct <F1> macro in /etc/Muttrc (/usr/doc -> \${prefix}/doc)

* Sun Jun 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.11-0.20050924.7m)
- rebuild against qdbm-1.8.59

* Thu May 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.11-0.20050924.6m)
- rebuild against qdbm-1.8.56

* Mon Apr 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.11-0.20050924.5m)
- rebuild against qdbm-1.8.48

* Thu Mar 09 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.11-0.20050924.4m)
- rebuild against qdbm-1.8.46

* Thu Feb 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.11-0.20050924.3m)
- rebuild against qdbm-1.8.45

* Sun Dec 11 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.11-0.20050924.2m)
- rebuild against qdbm-1.8.35

* Sat Sep 24 2005 TAKAHASHI Tamotsu <tamo>
- (1.5.11-0.20050924.1m)
- This is a very stable snapshot ;)
- w3m is required for conversion from HTML to TXT.
 lynx is better (because it outputs bold/italic text),
 but Momonga doesn't have it. (Orphan)
  see: http://marc.theaimsgroup.com/?l=mutt-dev&m=112644211706111&w=2

* Thu Sep 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.10i-2m)
- rebuild against qdbm-1.8.33

* Fri Aug 12 2005 TAKAHASHI Tamotsu <tamo>
- (1.5.10i-1m)
- 1.5.10! A lot of bugs fixed, a few bugs added :)
 This is still kinda experimental
- enable gpgme by default
- fix handling of insecure mailto URLs (patch7)

* Tue Jun  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.9i-4m)
- rebuild against qdbm-1.8.29

* Thu May 26 2005 TAKAHASHI Tamotsu <tamo>
- (1.5.9i-3m)
- bug118: config(noreplace) /etc/Muttrc

* Mon May 23 2005 TAKAHASHI Tamotsu <tamo>
- (1.5.9i-2m)
- use qdbm (patch6: hcache.30)
 + of course, you can't use old DB files any longer
 + new option: header_cache_compress
- fix IMAP bug (patch7)
 http://marc.theaimsgroup.com/?l=mutt-dev&m=111650433231927&w=2
- use ncursesw by default

* Wed Mar 23 2005 TAKAHASHI Tamotsu <tamo>
- (1.5.9i-1m)
- update to the CVS code:
 + minor bugfixes
 + scanf("%u") for uint64_t was wrong - now using "%lu" for unsigned long
 + Maildir hcache is included
- BuildPreReq: automake >= 1.9, autoconf >= 2.59
- disable inode_sort if hcache is enabled
 (http://marc.theaimsgroup.com/?l=mutt-dev&m=110872387611134&w=2)
- prepare for gpgme (still disabled by default, not because it's
 unstable, but because it requires gpgme installed)
- prepare for ncursesw
- update manual.ja -> 20050213
- update ja patch -> ja-beta1 (+ tamo's mod)
 + ignore_linear_white_space is added
 + BE CAREFUL! It's still beta!

* Sun Feb 13 2005 TAKAHASHI Tamotsu <tamo>
- (1.5.8i-1m)
- new features: menu_context, pgp-auto-decode
- update hcache -> 28
- update manual.ja -> 20050201
- update ja patch -> tt+tamo.0
 + strict_mime is obsolete (Now it only affects lws)
 + pgp_charsethack is added
- gpgme capability is not enabled yet

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (1.5.7i-1m)
- [SECURITY]
  Now decrypt-save doesn't delete original
  message when wrong passphrase is given.
  However, PLEASE DON'T RELY ON THAT FEATURE.
  Decode-save is worse. DON'T USE IT.
- modify ja patch (slightly incompatible with the original patch)
- unset $smart_wrap for Japanese language (dot.muttrc)

* Wed Dec  8 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6i-10m)
- add maildir_inode_sort patch, which optimizes reading of
 Maildirs on filesystems with hashed directory feature:
 I experimented it on reiserfs (r5) and ext3 (dir_index tea)

* Fri Dec  3 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6i-9m)
- add -Wno-long-long to CFLAGS because hcache uses long long
- update source (20041119) and hcache (25)
 - remove patch18 and patch19
 - from ChangeLog:
  +	* browser.c, buffy.c, check_sec.sh, commands.c, edit.c,
  +	imap/auth_cram.c, imap/auth_gss.c, imap/imap.c, imap/message.c,
  +	lib.c, lib.h, mutt_ssl.c, muttlib.c, recvcmd.c, url.c:
  +	safe_strcat, safe_strncat.  Thanks to Ulf H. for noting the
  +	wrong use of strncat in part of the mutt code base.
  (potential security problem)
  
  +	* smime.c: Fix bad code in smime_get_field_from_db.  Problem noted
  +	by Ulf Harnhammar <Ulf.Harnhammar.9485@student.uu.se>.
  (dunno)
  
  +	* recvattach.c: Remove *two* layers of S/MIME from messages
  +	before building the attachment tree.  A generic solution would
  +	remove intransparent encodings all the way down the tree.
  (some MUAs send S/MIME in S/MIME)
  
  +	* globals.h, init.h, mutt_sasl.c, mutt_ssl.c: Here's a patch
  +	to allow mutt to use SSL client certificates to authenticate
  +	itself. To use, set ssl_client_cert to the path to your
  +	certificate file (containing both the certificate and the private
  +	key). It works with the SASL EXTERNAL authentication mechanism,
  +	so you'll need to have SASL enabled as well.

* Mon Aug 30 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6i-8m)
- update source, hcache and ja.po
- modify ja patch (patch5)
- the author of hcache patch prefers gdbm, so use it (;_;
- rewrite spec to make it small and easy-to-read
 (move manual_ja.tex and manual_ja.sgml to upper directory)
- change sample dot.muttrc configuration:
 set assumed_charset="iso-2022-jp" (utf-8 can cause garbles)
 color bold black white (http://securityfocus.com/bid/10929/solution/)

* Thu Jul  8 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6-7m)
- enable hcache by default!
- add manual.ja.{html,sgml,txt} (translated by tamo)

* Mon Jun 14 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6-6m)
- fix ja.1 remote attack vulnerability
- CVS current version
 - add mmdf.5 manual
 - cp flea.1 muttbug.1 (instead of ".so")
- hcache.18
 - Note: hcache.18 requires db-4.2 when installed

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (1.5.6-5m)
- revised spec for enabling rpm 4.2.

* Mon Mar 15 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6-4m)
- hcache patch ver. 8
 NOTE: don't forget to wipe your old hcache db

* Mon Feb 16 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6-3m)
- update to current CVS
- update ja patch to ja.1
- in sample muttrc;
 unset write_bcc (insecure if MTA does not strip BCC tag)
 unset bounced_delivered (bad for postfix?)
 subscribe momonga lists
 add comments
 remove the comment and the macro related to xface
- update hcache patch to version 5
 - configurable with --enable-hcache option
 - Note: set maildir_cache=/some/path in muttrc to use it
- remove Patch19: mutt-1.5.6-invalidchar.patch
 (included in cvs update)

* Sun Feb 08 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6i-2m)
- update
 (command stat bug (patch18) is fixed)
 (ja.po is included)
- update ja patch
- update hcache patch (useable!! try it out!)
 - seems not requiring db4
 - not configurable with --disable-cache option

* Sat Feb 07 2004 TAKAHASHI Tamotsu <tamo>
- (1.5.6i-1m)
- update
  Note: you should modify your muttrc:
 set alternates '(A|B)'
  to
 alternates 'A' 'B'
- remove "if" around Source4: ja.po
- remove Patch18: pkcs7sig (fixed)
- for Maildir...
 global homespool Maildir
 global dotlock 1
 global dlsgid 0
  (I remember that mutt_dotlock is needed by
  NFS users even when their home spool is Maildir.)
- add Patch18: mutt-1.5.6-stat.patch
  to avoid stat(2)-ing commands
  (e.g. source "echo mailboxes * |")
- add Patch19: mutt-1.5.6-invalidchar.patch
  to cover iconv's bug

* Wed Dec 17 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.5.1i-4m)
- update (only two minor bugs are fixed)
- prepare for Maildir
 (if you prefer Maildir, modify these macros:
  homespool 0 -> Maildir
  dotlock   1 -> 0
 it's good to disable mutt_dotlock because
 mutt_dotlock is setgid)

* Mon Nov 17 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.5.1i-3m)
- update to current CVS version. (minor fixes)
 remove Patch13: mutt-1.5.4-config_idn.patch (fixed)
 remove Patch15: mutt-1.5.4-need_md5.patch (fixed)
- update to ja-1.beta (comment out patch4: japatch.patch)
- rebuild against libidn-0.3
- update ja.po

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.5.1i-2m)
- rebuild against cyrus-sasl

* Thu Nov  6 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.5.1i-1m)
- update. still using ja-patch for 1.5.4
 (changing the type of safe_realloc with patch4)
- remove patch14: bug1516 was fixed
- add patch16: 080_Md.gpg_import_nonverbose (from debian)
- add patch17: patch-1.5.4.ametzler.pgp_good_sign (from debian)
- add patch18: mutt-1.5.5.1-pkcs7sig.patch (bug#1685)

* Fri Oct  3 2003 Toru Hoshina <t@momonga-linux.org>
- (1.5.4i-9m)
- mutt_ldap_query_ja.pl and mutt_ldap_query_u.pl were revised.

* Thu Sep 11 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.4i-8m)
- update to CVS HEAD (many minor bugfixes)
- use make macro if available
- use NoSource macro
 (Do s/^.NoSource\s+([0-9]+)\s+(\w+)\s+\w+/Source\1: \2/
 if you don't have the macro)
- enable idn
 (Note: libidn 0.2 is not backward compatible.
  And I tested it only with Japanese domain names)
- define -> global
- add bg.po because currently bg.po is not in mutt.org repository
 and configure.in has "bg" entry
- remove "--disable-warnings"

* Sat Jun 28 2003 TAKAHASHI Tamotsu <tamo>
- (not committed)
- comment out "set xface" (sample muttrc)
- add Source8: view-x-face
- add Source9: view-x-face.w3mimg

* Sat Jun 14 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.4i-7m)
- update to CVS HEAD (20030613)
- use cyrus-sasl > 2
 - patch14: (bug1516) don't assume NI_WITHSCOPEID is defined
 - patch15: (bug1515) SASL has nothing to do with md5c.o, doesn't it?
- remove slrnface

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5.4i-6m)
- rebuild against slang(utf8 version)

* Wed May 14 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.4i-5m)
- update to cvs HEAD (20030514)
 without manual changes because linuxdoc-tools
 is not available. wait for the next official release.
- no longer export CC=gcc
- still require cyrus-sasl (not cyrus-sasl1)
 we can "configure --with-sasl2" if 2.x comes.
- make with smp_mflags

* Sat Apr 19 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.4i-4m)
- update to cvs HEAD (20030415)
 - minor bugfixes in compose.c
- correct the previous entry date (14 -> 13)

* Sun Apr 13 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.4i-3m)
- update to cvs HEAD (20030411)
 - patch12(check-pgp-exit) no longer needed
- apply ja.1 patch
- add japanese doc (patpath/*.ja.txt)
- declare config-sample as docdir
- install -m 755 *.pl (config-sample)

* Mon Mar 24 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.4i-2m)
- apply ja.1.beta patch
- apply patch-1.5.4.dms.pgp_check_exit.1 (Patch12)
- apply koma2's patch to avoid libidn dependencies
  (Patch13 [Momonga-devel.ja:01520]) tnx!

* Thu Mar 20 2003 TAKAHASHI Tamotsu <tamo>
- (1.5.4i-1m)
- many many fixes

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.5.3i-7m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5.3i-6m)
- rebuild against for gdbm

* Fri Feb  7 2003 Tsutomu Yasuda
- (1.5.3i-5m)
  reubild against libgdbm

* Mon Jan 27 2003 TAKAHASHI Tamotsu
- (1.5.3i-4m)
- ja.1 patch (use sfjp macro)
- use slang-utf8 shared lib
  (use ncurses if slang == 0)
- update to 20030126 cvs

* Wed Jan 01 2003 TAKAHASHI Tamotsu
- (1.5.3i-3m)
- ja-patch: 1.5.3i-ja.1.beta on sourceforge.jp
- ja.po: 1-5-3.1218
- some more docs

* Wed Dec 18 2002 TAKAHASHI Tamotsu
- (1.5.3i-2m)
- hcachepatch adjusted
- BuildPreReq: rpm-build >= 4.0.4-33m
  install -m644 mutt_ldap_query_{u,ja}.pl

* Wed Dec 18 2002 TAKAHASHI Tamotsu
- (1.5.3i-1m)
- BuildPreReq: rpm-build >= 4.0.4-43m
  install -m755 mutt_ldap_query_{u,ja}.pl
- patch4 to japatch152beta1

* Mon Dec  9 2002 TAKAHASHI Tamotsu
- (1.5.2i-2m)
- autoconf 2.53 -> 2.56-4m (or higher)
- "BuildPreReq: item1, item2" -> "BuildPreReq: item1\nBuildPreReq: item2"
  (to get more readable diff)
- slang-utf8 prepared in "prepare" section, and built in "build" section
- %%slang -> %%{slang}
- cvspatch (patch3) updated

* Sun Dec  8 2002 TAKAHASHI Tamotsu
- (1.5.2i-1m)
- ja_patch 1.5.2-1.beta applied
- cvspatch (patch3) renamed to mutt-1.5.2-cvs.patch (up-to-date)
- japatchpatch (patch4) commented out
- ja.po 1207
- autoconf-2.53 forced (why 2.56 fails?)
- comments about "./prepare" removed
- hcache (patch11) supported if specified

* Sun Nov 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5.1i-26m)
- autoconf 2.53 or later

* Tue Nov 12 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (1.5.1i-25m)
- patch5 removed
- mutt-1.5.1-cvs.patch (patch3) modified (because cvs has been updated)
  - changes from 24m (me.2) are all in  patch-1.5-tlr.alias-parse.1,
    which is an extension of patch-1.5.1-me.aliascheck.1 (see Changelog)
- ja.po (source4) updated (only one message)

* Mon Nov 11 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (1.5.1i-24m)
- patch-cvs-20021106-me.2 (patch5) added
  [mutt-1.5.1-cvs.patch (patch3) modified]

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5.1i-23m)
- autoconf-2.53 and gcc-3.1 safe again

* Mon Oct 28 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (1.5.1i-22m)
- ja.po updated (1025)
- slang 1.4.5 un-NoSource'd (by poo)

* Sun Oct 13 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (1.5.1i-21m)
- update (patch3 = cvs 20021011)
  - with doc fixes
  - with po
    (removing po/*.gmo)
  - without changes in curs_lib.c (see 1.5.1i-15m changelog)
    (possibly fixed. but i'm not sure)
  - modifying ja-patch (patch4)
- remove S/MIME-related perl-processes for sample-config

* Sun Sep 29 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.5.1i-20m)
- add %{_bindir}/smime_keys (sumaso!)
- support specopt

* Thu Sep 26 2002 Kikutani Makoto <poo@momonga-linux.org>
- (1.5.1i-19m)
- add Requires: smtpdaemon

* Fri Sep 06 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.5.1i-18m)
- ja.po updated (incorrect timestamp should be fixed with this)

* Wed Sep 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.5.1i-17m)
- slrnface.sl and slrnface-2.0.sl added as docs

* Wed Sep 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.5.1i-16m)
- slrnface 2.1.1 added
- slrnface-patch applied
  - "set xface=yes" added to sample
- License: GPL (to avoid warning while running speclint)

* Wed Aug 07 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.5.1i-15m)
- no longer build slang-elf
- updated (cvs 20020807)
  - with doc fixes
  - without po
  - without curs_lib.c because it doesn't show question sometimes
    - the comment is:
     * In order to prevent the default answer to the question to wrapped
     * around the screen in the even the question is wider than the screen,
     * ensure there is enough room for the answer and truncate the question
     * to fit.
- japanese manual updated (1.4-1)

* Tue Jun 11 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.5.1-14k)
- new ja.po (1.5.1-0611)
- new manual_ja (1.4)
- without static slang, "-a 8" failed -> fixed

* Sat Jun 01 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.5.1i-12k)
- new slang-utf8 by TAKIZAWA
- cancel ccache-related parts
- License: GPL2 or later
- ja-patch = ja.1

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.5.1i-10k)
- cancel gcc-3.1 autoconf-2.53

* Fri May 24 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.5.1i-8k)
- autoconf-2.53 and gcc-3.1 safe

* Mon May 06 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.5.1i-6k)
- Takizawa's patch has come!

* Sat May 04 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.5.1i-4k)
- just for 1.3.99i-ja.1.beta patch... NOT applied

* Fri May 03 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.5.1i-2k)
- removed section related to cvs
- ja-manual for 1.3.28 (html to "html" dir)
- CC=gcc

* Fri Apr 26 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.5.0i-0.020020426002k)
- CVS update
- slang-utf8(1.4.5) is statically linked if "%slang" is defined
- now openssl is required by S/MIME feature
- if ccache is available, use it
- CVS version requires ./prepare before %configure
- ja-patch is not an official version yet (TAKE CARE!!!)
- if ja.po newer than CVS is available, define "%withpo" and "%pover"
- for OmoiKondara, "%if" is used instead of "%{?hoge:hogehoge}"
- install -m644 mutt_ldap_query-family (I don't want rpm4 to require
  perl-ldap, perl-Unicode-Map and perl-Unicode-String)

* Sat Mar 16 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.28i-2k)
- ja_patch = ja.1.beta
- ja.po = 1-3-27.0314 (from [mutt-j:01992])
- removed S/MIME patch. if you wanna use this feature, try 1.5.x.
- removed comments for debugging. instead, try "OmoiKondara -G"

* Wed Feb 06 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.27i-8k)
- ja_patch = ja.2

* Mon Jan 28 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.27i-6k)
- ja.po = 1-3-27.0128

* Fri Jan 25 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.27i-4k)
- configure --without-wc-funcs (maybe good for UTF-8 environment)

* Fri Jan 25 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.27i-2k)
- ja_patch = ja.1.beta, ja.po = 1-3-27.0123, man_ja = 1.3.23i-0

* Sat Jan 19 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.26i-2k)
- patch = ja.1.beta, ja.po = none, man = 1.3.23i-0

* Sun Jan 13 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.25i-6k)
- patch = ja.1

* Mon Jan  7 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.25i-4k)
- patch = ja.1.beta

* Fri Jan  4 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.25i-1k)(2k)
- patch = ja.-1, ja.po = none, man = 1.3.23i-0
- fix the location of manual.txt in _sysconfdir/Muttrc
- fix the permission of some files (use install instead of umask)
- added SSL, SASL, S/MIME (when defined)

* Wed Dec 19 2001 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.24i-2k)
- patch = ja.1, ja.po = 1-3-24.1206, man = 1.3.23i-0
- made it easier to verup. now you can verup by defining the version
- modified description (cf. "Introduction" of manual.txt  :)
- modified sample.muttrc-kondara
- added maildirnewmail to config-sample
- added mutt-ja.rc to config-sample
- added mutt_ldap_query_{u,ja}.pl
- relocated documents

* Fri Nov  9 2001 Toru Hoshina <t@kondara.org>
- (1.3.23i-6k)
- fixed man muttbug issue. See "[mutt-j:01796] man muttbug"

* Fri Oct 26 2001 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.3.23i-4k)
- for sample-config
- add japanese manual for 1.3.23i

* Thu Oct 18 2001 Toru Hoshina <toru@df-usa.com>
- (1.3.23i-2k)
- version up.
- rebuild against gettext 0.10.40.

* Sun Sep  9 2001 Toru Hoshina <toru@df-usa.com>
- (1.3.22.1i-2k)
- version up.

* Thu Jun  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.3.18i-2k)
- version up.

* Mon Mar 26 2001 Toru Hoshina <toru@df-usa.com>
- (1.3.16i-4k)
- add sample.muttrc-kondara.

* Sun Mar 25 2001 Toru Hoshina <toru@df-usa.com>
- (1.3.16i-2k)

* Mon Mar 12 2001 Kenichi Matsubara <m@kondara.org>
- (1.2.5i-4k)
- %build section modified.
- (add --mandir --infodir)

* Tue Dec  5 2000 Toru Hoshina <toru@df-usa.com>
- R.I.P slang-ja.

* Wed May 17 2000 MATSUDA, Daiki <dyky@df-usa.com>
- remake because of slang-ja is modified

* Thu Feb 24 2000 Toru Hoshina <t@kondara.org>
- version up. 1.0i -> 1.0.1i

* Sun Jan 30 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)

* Wed Jan 12 2000 Toru Hoshina <t@kondara.org>
- Rebuild against sanp shot. move message catalog to ja.

* Sun Dec 19 1999 Tenkou N. Hattori <tnh@kondara.org>
- Package name is changed to slang-ja.

* Sat Nov 27 1999 Toru Hoshina <t@kondara.org>
- version up.
- patch made by Taro Funaki <taro@KU3G.org>, Thanx! :-)

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Oct 8 1999 Toru Hoshina <t@kondara.org>
- version up 1.0pre3i officially.

* Mon Sep 27 1999 Toru Hoshina <tohohoru@kondara.org>
- version up 1.0pre3i.

* Sun Sep 19 1999 Toru Hoshina <tohohoru@kondara.org>
- version up 1.0pre2i.

* Thu Aug 26 1999 Toru Hoshina<hoshina@best.com>
- version up 1.0pre1i.

* Mon Jun 14 1999 Toru Hoshina<hoshina@best.com>
- version down 0.95.6ijp0 :-P

* Mon Jun 7 1999 Toru Hoshina<hoshina@best.com>
- version up 0.95.6.

* Mon May 24 1999 Toru Hoshina<hoshina@best.com>
- version up 0.95.5.

* Wed Apr 28 1999 Toru Hoshina<hoshina@best.com>
- added some patches so that it can handle mh style mailbox, avoid to use
  iso2022-jp in case of no japanese contents.

* Sun Apr 11 1999 Toru Hoshina<hoshina@best.com>
- abandoned pgcc :-P
- rebuild against rawhide 1.3.3.

* Sun Apr 04 1999 Toru Hoshina<hoshina@best.com>
- added mutt_dotlock, i completely forgot to add it :-P

* Sat Mar 20 1999 Toru Hoshina<hoshina@best.com>
- rebuild against rawhide 1.3.0.

* Mon Mar  8 1999 Toru Hoshina <hoshina@best.com>
- version up to 0.95.4i-jp1.

* Tue Mar  3 1999 Toru Hoshina <hoshina@best.com>
- version up to 0.95.3i-jp1.

* Tue Feb 13 1999 Toru Hoshina <hoshina@best.com>
- version up to 0.95.3i-jp0.

* Tue Feb 09 1999 Toru Hoshina <hoshina@best.com>
- version up to 0.95.1i-jp0.

* Wed Jan 06 1999 Toru Hoshina <hoshina@best.com>
- version up to 0.95i-jp0.

* Fri Nov 20 1998 Toru Hoshina <hoshina@best.com>
- added patch for pgp 2.6.3.

* Wed Oct 21 1998 Toru Hoshina <hoshina@best.com>
- version up to 0.93.2i-jp2.

* Sat Sep 26 1998 Toru Hoshina <hoshina@best.com>
- version up to 0.93.2i-jp1.

* Mon Sep 21 1998 Toru Hoshina <hoshina@best.com>
- fixed version numder that was displayed as 0.93i.

* Sun Aug  23 1998 Toru Hoshina <hoshina@best.com>
- version up to 0.93.2i in order to "Buffer Overrun" issue.
- added jp0 patchies that are maintained by Makoto Kikutani
  <kikutani@debian.or.jp>, and for some reasons, the S-Lang library is static linked at this time.

* Mon Jul  6 1998 Toru Hoshina <hoshina@best.com>
- initial release
- added jp1 patchies that are maintained by Makoto Kikutani
  <kikutani@debian.or.jp>, and for some reasons, the S-Lang library is static linked at this time.
