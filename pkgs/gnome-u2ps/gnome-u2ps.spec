%global momorel 10

Summary: Handy library of utility functions
Name: gnome-u2ps
Version: 0.0.4
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: GPL
URL: http://bonobo.gnome.gr.jp/~nakai/u2ps/
Source0: http://bonobo.gnome.gr.jp/~nakai/u2ps/gnome-u2ps-%{version}.tar.gz
NoSource: 0
Patch0: gnome-u2ps-0.0.4-ja_font.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
A2ps is as same as nothing for UTF-8, and other multibyte languages.
Gnome-u2ps aims a better alternative powered by GNOME libraries.

%prep
%setup -q
%patch0 -p1

%build
%configure --program-prefix="" LIBS="-lgnomevfs-2"
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_bindir}/u2ps
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.4-8m)
- full rebuild for mo7 release

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4-7m)
- build fix gcc-4.4.4

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.0.4-4m)
- add __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4-2m)
- rebuild against gcc43

* Thu Feb 15 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.4-1m)
- initial build
