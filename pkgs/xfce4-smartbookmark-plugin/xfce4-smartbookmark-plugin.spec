%global momorel 1

%global xfce4ver 4.11.0
%global major 0.4

Name:		xfce4-smartbookmark-plugin
Version:	0.4.5
Release:	%{momorel}m%{?dist}
Summary:	Smart bookmarks for the Xfce panel
Group:		User Interface/Desktops
License:	GPL
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
# vendor specific patches
Patch10:	%{name}-redhat.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
#BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	libxml2-devel, gettext, perl-XML-Parser
BuildRequires:	libXt-devel
Requires:	xfce4-panel >= %{xfce4ver}
Requires:	webclient
#Provides:       webclient

%description
A plugin which allows you to do a search directly on Internet on sites like 
Google or Red Hat Bugzilla. It allows you to send requests directly to your 
browser and perform custom searches.

%prep
%setup -q
%patch10 -b .redhat

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'
%find_lang %{name}

%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/xfce4/panel/plugins/*.so
%{_datadir}/xfce4/panel/plugins/*.desktop

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.5-1m)
- update to 0.4.5
- build against xfce4-4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.4-1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-14m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-13m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.2-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.2-10m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-9m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-7m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-5m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-4m)
- rebuild against xfce4 4.4.2

* Thu May 24 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.4.2-3m)
- provide webclient as a virtual package.

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-1m)
- import to Momonga from fc

* Mon Jan 22 2007 Christoph Wickert <fedora christoph-wickert de> - 0.4.2-2
- Rebuild for Xfce 4.4.

* Sat Sep 23 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.2-1
- Initial Fedora Extras version.
