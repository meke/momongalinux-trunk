%global momorel 4

%global fontname vemana2000
%global fontconf 69-%{fontname}.conf

Name: %{fontname}-fonts
Version: 1.1.2
Release: %{momorel}m%{?dist}
Summary: Unicode compliant OpenType font for Telugu

Group: User Interface/X
License: "GPLv2+ with exceptions"
URL: http://www.kavya-nandanam.com/dload.htm

Source0: Vemana2k-Li.zip
Source1: %{name}-fontconfig.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch
BuildRequires: fontpackages-devel
Requires: fontpackages-filesystem

%description
A free OpenType font for Telugu created by
Dr. Tirumala Krishna Desikacharyulu. 

%prep
%setup -q -c -n %{name}
sed -i 's/\r//' gpl.txt

%build

%install
rm -fr %{buildroot}
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p vemana2000.ttf %{buildroot}%{_fontdir}
install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
 %{buildroot}%{_fontconfig_confdir}
install -m 0644 -p %{SOURCE1} \
 %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
 %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -fr %{buildroot}

%_font_pkg -f %{fontconf} vemana2000.ttf

%doc gpl.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-1m)
- import from Fedora 13

* Mon Dec 14 2009 <sshedmak@redhat.com> - 1.1.2-1
- Fixed FSType, Preferred Styles, UniqueID and Fullname
- Fixed Invalid Glyph Names reported by fontlint
- with exceptions string added in License

* Tue Aug 31 2009 <sshedmak@redhat.com> - 1.1.1-2
- Changed the Pothana2000 strings to Vemana2000

* Tue Jun 23 2009 <sshedmak@redhat.com> - 1.1.1-1
- Initial packaging
