%global         momorel 3

Name:           perl-BerkeleyDB
Version:        0.54
Release:        %{momorel}m%{?dist}
Summary:        Perl extension for Berkeley DB version 2, 3, 4 or 5
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/BerkeleyDB/
Source0:        http://www.cpan.org/authors/id/P/PM/PMQS/BerkeleyDB-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
# For "make test".
BuildRequires:  perl-MLDBM
BuildRequires:  perl-Test-Pod
BuildRequires:  libdb-devel >= 5.3.15
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
BerkeleyDB is a module that allows Perl programs to make use of the
facilities provided by Berkeley DB. Berkeley DB is a C library that
provides a consistent interface to a number of database formats.
BerkeleyDB provides an interface to all four of the database types
(hash, btree, queue and recno) currently supported by Berkeley DB.

%prep
%setup -q -n BerkeleyDB-%{version}
%{__perl} -pi -e 's,/local/,/, if ($. == 1)' dbinfo
%{__perl} -pi -e 's,/local/bin/perl5,/bin/perl, if ($. == 1)' mkpod
chmod -x Changes README

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

install -D -m755 dbinfo %{buildroot}%{_bindir}/dbinfo

%{__mv} %{buildroot}%{perl_vendorarch}/*.pl %{buildroot}%{_bindir}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BerkeleyDB.pod.P Changes dbinfo mkpod README Todo
%{_bindir}/*
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/BerkeleyDB*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-2m)
- rebuild against perl-5.18.2

* Mon Nov 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53
- rebuild against perl-5.18.1

* Sat Jun  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-3m)
- rebuild against perl-5.16.0

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-2m)
- rebuild against libdb-5.3.15

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Sun Dec 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-2m)
- rebuild against perl-5.14.2

* Mon Aug  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-2m)
- rebuild against perl-5.14.1

* Sun Jun 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-2m)
- rebuild against perl-5.12.0

* Mon Mar 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.41-2m)
- rebuild against db-4.8.26

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Thu Jan  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.39-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-2m)
- rebuild against perl-5.10.1

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Sun Feb 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Fri Feb 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37
- add patch0 to succeed tests

* Thu Feb 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-2m)
- adjust BuildRequires 

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.34-1m)
- import from Fedora to Momonga for amavisd-new

* Thu May 15 2008 Steven Pritchard <steve@kspei.com> 0.34-1
- Update to 0.34.

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.33-3
Rebuild for new perl

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.33-2
- Autorebuild for GCC 4.3

* Mon Jan 28 2008 Steven Pritchard <steve@kspei.com> 0.33-1
- Update to 0.33.
- Update License tag.
- BR Test::More.

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.32-2
- Rebuild for selinux ppc32 issue.

* Fri Jul 13 2007 Steven Pritchard <steve@kspei.com> 0.32-1
- Update to 0.32.

* Tue Apr 17 2007 Steven Pritchard <steve@kspei.com> 0.31-3
- BR ExtUtils::MakeMaker.

* Mon Oct 16 2006 Steven Pritchard <steve@kspei.com> 0.31-2
- Rebuild.

* Mon Oct 16 2006 Steven Pritchard <steve@kspei.com> 0.31-1
- Update to 0.31.
- Use fixperms macro instead of our own chmod incantation.

* Wed Sep 13 2006 Steven Pritchard <steve@kspei.com> 0.30-1
- Update to 0.30.

* Mon Aug 28 2006 Steven Pritchard <steve@kspei.com> 0.29-2
- Minor spec cleanup.

* Fri Jul 07 2006 Steven Pritchard <steve@kspei.com> 0.29-1
- Update to 0.29.

* Fri Jun 30 2006 Steven Pritchard <steve@kspei.com> 0.28-1
- Update to 0.28

* Sat Feb 18 2006 Steven Pritchard <steve@kspei.com> 0.27-2
- Rebuild

* Tue Jan 10 2006 Steven Pritchard <steve@kspei.com> 0.27-1
- Update to 0.27

* Wed Oct 12 2005 Steven Pritchard <steve@kspei.com> 0.26-6
- Another rebuild

* Sat Sep 24 2005 Steven Pritchard <steve@kspei.com> 0.26-5
- Rebuild for new db4 in rawhide

* Mon Sep 05 2005 Steven Pritchard <steve@kspei.com> 0.26-4
- Spec cleanup
- Include COPYING and Artistic

* Wed Aug 03 2005 Steven Pritchard <steve@kspei.com> 0.26-3
- Move OPTIMIZE to Makefile.PL instead of make

* Mon Aug 01 2005 Steven Pritchard <steve@kspei.com> 0.26-2
- Various fixes from Paul Howarth:
  - Add description
  - Fix permissions on docs (also Paul Howarth)
  - Add OPTIMIZE to make
  - Don't own perl_vendorarch/auto/
  - BuildRequire Test::Pod and MLDBM

* Wed Jul 06 2005 Steven Pritchard <steve@kspei.com> 0.26-1
- Specfile autogenerated.
- Add BuildRequires db4-devel.
- Install dbinfo script.
