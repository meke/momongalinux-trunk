%global momorel 4
%global _default_patch_fuzz 2

Summary: converts Postscript(TM) and PDF files to other vector graphic formats
Name: pstoedit
Version: 3.62
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Publishing
URL: http://www.helga-glunz.homepage.t-online.de/pstoedit/
#Source0: http://home.t-online.de/home/helga.glunz/wglunz/pstoedit/pstoedit-%{version}.tar.gz
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: pstoedit-3.45-gcc43.patch
Requires: ghostscript libemf >= 1.0.3-2m
Requires: plotutils
BuildRequires: libpng-devel >= 1.2.2 
BuildRequires: ImageMagick-Magick++-devel >= 6.8.8.10
BuildRequires: ming-devel >= 0.4.4
BuildRequires: libemf-devel >= 1.0.3-2m
BuildRequires: plotutils-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
pstoedit converts Postscript(TM) and PDF files to other vector graphic
formats so that they can be edited graphically. See pstoedit.htm or
index.htm for more details on which formats are supported by pstoedit.

%prep
%setup -q
%patch0 -p1 -b .gcc43

%build
%configure
%make

%install
%makeinstall

find %{buildroot} -name "*.la" -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean 
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc copying
%{_bindir}/*
%{_datadir}/pstoedit
%{_datadir}/aclocal/pstoedit.m4
%{_includedir}/pstoedit
%{_libdir}/pkgconfig/*
%{_libdir}/libpstoedit*
%{_libdir}/pstoedit
%{_mandir}/man1/pstoedit.1*

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.62-4m)
- rebuild against ImageMagick-6.8.8.10

* Thu Sep 19 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.62-3m)
- rebuild against ImageMagick-6.8.6.10

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.62-2m)
- rebuild against ImageMagick-6.8.6

* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.62.1m)
- update to 3.62
- rebuild against ming-0.4.4

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.45-18m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.45-17m)
- rebuild against ImageMagick-6.8.2.10

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.45-16m)
- rebuild against ImageMagick-6.8.0.10

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.45-15m)
- rebuild against ImageMagick-6.7.2.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.45-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.45-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.45-12m)
- full rebuild for mo7 release

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.45-11m)
- rebuild against ImageMagick-6.6.2.10

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.45-10m)
- rebuild against ImageMagick-6.5.9.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.45-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.45-8m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.45-7m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.45-6m)
- rebuild against ImageMagick-6.4.8.5-1m

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.45-5m)
- %%global _default_patch_fuzz 2
- License: GPLv2+

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.45-4m)
- rebuild against ImageMagick-6.4.2.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.45-3m)
- rebuild against gcc43

* Mon Mar  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.45-2m)
- fix gcc-4.3

* Sun Mar  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.45-1m)
- update to 3.45

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.44-7m)
- %%NoSource -> NoSource

* Fri Jun 15 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.44-6m)
- rebuild with plotutils

* Fri Nov 10 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.44-5m)
- BuildRequires: libemf-devel and Requires: libemf to enable emf support

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.44-4m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.44-3m)
- rebuild against expat-2.0.0-1m

* Thu Jul 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.44-2m)
- rebuild against ming instead of jaming

* Wed Apr 26 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.44-1m)
- update to 3.44
- revise URL
- removed unneeded patches

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (3.42-1m)
- update to 3.42

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.33-4m)
- suppress AC_DEFUN warning.

* Sun Aug 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.33-3m)
- rebuild against gcc-c++-3.4.1
- add BuildRequires: gcc-c++
- add gcc34 patch

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (3.33-2m)
- revised spec for rpm 4.2.

* Wed Jan 21 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.33-1m)
- update to 3.33 [Momonga-devel.ja:01276]

* Fri Sep 27 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.32-2m)
- enable ming support again
- add BuildRequires: jaming-devel >= 0.1.5-4m
  build problem has been fixed in jaming-devel-0.1.5-4m

* Fri Sep 13 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.32-1m)
- temporary disable ming support

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.31-8k)
- cancel gcc-3.1 autoconf-2.53

* Tue May 28 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.31-6k)
- gcc-3.1

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.31-4k)
- rebuild against libpng 1.2.2.

* Wed Feb 20 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.31-2k)
- update to 3.31

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against gcc 2.96.

* Wed Feb 3 2000 Norihito Ohmori <nono@kondara.org>
- update to version 3.16
- fix defattr

* Fri Jun 21 1999 Norihito Ohmori <ohmori@p.chiba-u.ac.jp>
 - first release
