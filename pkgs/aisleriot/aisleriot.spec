%global momorel 1

Summary: Aisleriot
Name: aisleriot
Version: 3.6.2
Release: %{momorel}m%{?dist}
License: LGPL
Group: Amusements/Games
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): GConf2
Requires(pre): rarian
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: libcanberra-devel
BuildRequires: glib2-devel
BuildRequires: gtk3-devel
BuildRequires: GConf2-devel
BuildRequires: librsvg2-devel
BuildRequires: cairo-devel
BuildRequires: libSM-devel
BuildRequires: libICE-devel
BuildRequires: guile-devel >= 2.0.9
Requires: gnome-games
Requires(pre): hicolor-icon-theme gtk2

%description
Aisleriot

%prep
%setup -q

%build
%configure \
	--disable-static \
	--enable-silent-rules \
	--enable-sound \
	--disable-schemas-install
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
update-desktop-database -q 2> /dev/null || :
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/aisleriot.schemas \
    > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/aisleriot.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/aisleriot.schemas \
	> /dev/null || :
fi

%postun
rarian-sk-update
update-desktop-database -q 2> /dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING.GFDL COPYING.GPL3 COPYING.LGPL3 ChangeLog NEWS TODO
%{_sysconfdir}/gconf/schemas/aisleriot.schemas
%{_bindir}/sol
%{_libdir}/%{name}
%{_libdir}/valgrind/aisleriot.supp
%{_libexecdir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/sol.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.Patience.WindowState.gschema.xml
%{_datadir}/icons/hicolor/*/apps/*
%{_mandir}/man6/sol.6.*
%doc %{_datadir}/help/*/%{name}

%changelog
* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2
- rebuild againsr guile-2.0.9

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-3m)
- rebuild for librsvg2 2.36.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for glib 2.33.2

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- initial build



