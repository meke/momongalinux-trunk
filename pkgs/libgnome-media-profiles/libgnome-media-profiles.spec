%global momorel 3

Summary: GNOME Media Profile
Name: libgnome-media-profiles
Version: 3.0.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Multimedia
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.0/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: gtk3-devel
BuildRequires: gstreamer-devel
BuildRequires: GConf2-devel
BuildRequires: glib2-devel
BuildRequires: gstreamer-plugins-base-devel

Requires: %{name}-libs = %{version}-%{release}

%description
GNOME Media Profile

%package libs
Summary: %{name}-libs
Group: System Environment/Libraries

%description libs
%{name}-libs

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --disable-schemas-install \
    --disable-scrollkeeper \
    --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/gnome-media-profiles.schemas \
    > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gnome-media-profiles.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gnome-media-profiles.schemas \
	> /dev/null || :
fi

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files -f %{name}.lang
%defattr(-, root, root)
%doc COPYING ChangeLog NEWS README
%{_sysconfdir}/gconf/schemas/gnome-media-profiles.schemas
# include me when gnome-media version up
%exclude %{_bindir}/gnome-audio-profiles-properties
%doc %{_datadir}/gnome/help/*
%{_datadir}/%{name}

%files libs
%defattr(-,root,root)
%{_libdir}/libgnome-media-profiles-3.0.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/libgnome-media-profiles-3.0.pc
%{_includedir}/libgnome-media-profiles-3.0
%{_datadir}/omf/gnome-audio-profiles

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-3m)
- rebuild for glib 2.33.2

* Fri Sep  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-2m)
- remove conflict file (Need Include when gnome-media version up)

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- initial build

