%global momorel 7

Summary: enhanced gettext po file editor
Name: gtranslator
Version: 2.90.7
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.90/%{name}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://gtranslator.sourceforge.net/

Requires(post): desktop-file-utils rarian GConf2
Requires(preun): GConf2
Requires(pre): GConf2
Requires(postun): desktop-file-utils rarian

BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: GConf2-devel
BuildRequires: gtksourceview2-devel
BuildRequires: glib2-devel >= 2.33.10
BuildRequires: libgdl-devel >= 3.6.0
BuildRequires: gtkspell-devel
BuildRequires: libsoup-devel
BuildRequires: gucharmap-devel
BuildRequires: apr-devel
BuildRequires: apr-util-devel
BuildRequires: neon-devel
BuildRequires: subversion-devel >= 1.6.3-2m
BuildRequires: db4-devel >= 4.8.26-1m
BuildRequires: libgda4-devel

%description
Gtranslator is an enhanced gettext po file editor for the GNOME
desktop environment. It handles all forms of gettext po files and
include very useful features like Find/Replace, Translation Memory,
different Translator Profiles, Messages Table (for having an overview
of the translations/messages in the po file), Easy Navigation and
Editing of translation messages & comments of the translation where
accurate. Gtranslator includes also a plugin system with many
interesting plugins like Alternate Language, Insert Tags, Open Tran,
Integration with Subversion, Source Code Viewer ...

%package devel
Summary: gEdit include files for plugins.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtksourceview2-devel

%description devel
gEdit plugins include files.

%prep
%setup -q

%build
%configure --enable-silent-rules \
    --disable-static \
    --disable-scrollkeeper \
    --enable-gtk-doc \
    --disable-schemas-install \
    --with-gtkspell \
    --with-dictionary
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install UPDATE_DESKTOP=:
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
update-desktop-database -q 2> /dev/null || :
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
update-desktop-database -q 2> /dev/null || :
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog ChangeLog-* HACKING NEWS README THANKS
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_libdir}/libgtranslator-private.so.*
%exclude %{_libdir}/*.la
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/gnome/help/%{name}
%{_datadir}/omf/%{name}
%{_datadir}/pixmaps/%{name}
%{_mandir}/man1/%{name}.1.*
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/glib-2.0/schemas/org.gnome.gtranslator.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gtranslator.plugins.codeview.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gtranslator.plugins.dictionary.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gtranslator.plugins.open-tran.gschema.xml

%files devel
%defattr(-, root, root)
%{_includedir}/gtranslator-3.0
%{_libdir}/*.so
%{_libdir}/pkgconfig/gtranslator.pc
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Sun Nov 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.90.7-7m)
- rebuild against libgdl-3.6.0

* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.90.7-6m)
- rebuild against libgdl-3.5.5

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.90.7-5m)
- update %%files

* Tue Jul 31 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.90.7-4m)
- fix build failure

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.90.7-3m)
- rebuild for libgdl-3.5.3

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.90.7-2m)
- rebuild for glib 2.33.2

* Thu Oct 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.90.7-1m)
- update to 2.90.7

* Wed Oct 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.90.6-1m)
- update to 2.90.6

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.90.5-1m)
- update to 2.90.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.11-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.11-6m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.11-5m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.11-4m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.11-2m)
- fix update-desktop-database

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.11-2m)
- change BuildRequires: libgdl-devel

* Thu May 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.11-1m)
- update to 1.9.11

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.6-5m)
- use BuildRequires and Requires

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.6-4m)
- rebuild against db-4.8.26

* Wed Dec 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.6-3m)
- build fix

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.6-2m)
- add patch1 (for pixmap)

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.6-1m)
- update to 1.9.6

* Sun Aug 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.5-3m)
- add ja patch

* Mon Aug  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.5-2m)
- rebuild against subversion-1.6.3-2m

* Sat Apr 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.5-1m)
- initial build
