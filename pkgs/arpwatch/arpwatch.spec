%global momorel 14
%global _vararpwatch %{_localstatedir}/lib/arpwatch

Name: arpwatch
Version: 2.1a15
Release: %{momorel}m%{?dist}
Summary: Network monitoring tools for tracking IP addresses on a network
Group: Applications/System
License: BSD
URL: http://ee.lbl.gov/
Requires(pre): shadow-utils 
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires: smtpdaemon
BuildRequires: smtpdaemon libpcap-devel >= 1.1.1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: ftp://ftp.ee.lbl.gov/arpwatch-%{version}.tar.gz
Source1: arpwatch.service
Source2: arpwatch.sysconfig
NoSource: 0
Source3: ethercodes-20110707.dat.bz2
Patch1: arpwatch-2.1a4-fhs.patch
Patch2: arpwatch-2.1a10-man.patch
Patch3: arpwatch-drop.patch
Patch4: arpwatch-drop-man.patch
Patch5: arpwatch-addr.patch
Patch6: arpwatch-dir-man.patch
Patch7: arpwatch-scripts.patch
Patch8: arpwatch-2.1a15-nolocalpcap.patch
Patch9: arpwatch-2.1a15-bogon.patch
Patch10: arpwatch-2.1a15-extraman.patch
Patch11: arpwatch-exitcode.patch
Patch12: arpwatch-2.1a15-dropgroup.patch
Patch13: arpwatch-2.1a15-devlookup.patch
Patch14: arpwatch-2.1a15-lookupiselect.patch
Patch15: arpwatch-2.1a15-lookupiinvalid.patch
Patch16: arpwatch-201301-ethcodes.patch
Patch17: arpwatch-pie.patch
Patch18: arpwatch-aarch64.patch 

%description
The arpwatch package contains arpwatch and arpsnmp.  Arpwatch and
arpsnmp are both network monitoring tools.  Both utilities monitor
Ethernet or FDDI network traffic and build databases of Ethernet/IP
address pairs, and can report certain changes via email.

Install the arpwatch package if you need networking monitoring devices
which will automatically keep track of the IP addresses on your
network.

%prep
%setup -q

%patch1 -p1 -b .fhs
%patch2 -p1 -b .arpsnmpman
%patch3 -p1 -b .droproot
%patch4 -p0 -b .droprootman
%patch5 -p1 -b .mailuser
%patch6 -p1 -b .dirman
%patch7 -p1 -b .scripts
%patch8 -p1 -b .nolocalpcap
%patch9 -p1 -b .bogon
%patch10 -p1 -b .extraman
%patch11 -p1 -b .exitcode
%patch12 -p1 -b .dropgroup 
%patch13 -p1 -b .devlookup
%patch14 -p1 -b .iselect
%patch15 -p1 -b .iinval
%patch16 -p1 -b .ethcode
%patch17 -p1 -b .pie
%patch18 -p1 -b .aarch64

%build
%configure
make ARPDIR=%{_vararpwatch}

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_mandir}/man8
mkdir -p $RPM_BUILD_ROOT%{_sbindir}
mkdir -p $RPM_BUILD_ROOT%{_vararpwatch}
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig

make DESTDIR=$RPM_BUILD_ROOT install install-man

# prepare awk scripts
perl -pi -e "s/\'/\'\\\'\'/g" *.awk

# and embed them
for i in arp2ethers massagevendor massagevendor-old; do
	cp -f $i $RPM_BUILD_ROOT%{_sbindir}
	for j in *.awk; do
		sed "s/-f\ *\(\<$j\>\)/\'\1\n\' /g" \
			< $RPM_BUILD_ROOT%{_sbindir}/$i \
			| sed "s/$j\$//;tx;b;:x;r$j" \
			> $RPM_BUILD_ROOT%{_sbindir}/$i.x
		mv -f $RPM_BUILD_ROOT%{_sbindir}/$i{.x,}
	done
	chmod 755 $RPM_BUILD_ROOT%{_sbindir}/$i
done

install -p -m644 *.dat $RPM_BUILD_ROOT%{_vararpwatch}
install -p -m644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/arpwatch.service
install -p -m644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/arpwatch
install -p -m644 %{SOURCE3} $RPM_BUILD_ROOT%{_vararpwatch}/ethercodes.dat.bz2
bzip2 -df $RPM_BUILD_ROOT%{_vararpwatch}/ethercodes.dat.bz2

%clean
rm -rf $RPM_BUILD_ROOT

%post
/bin/systemctl daemon-reload &> /dev/null
:
        
%pre            
if ! getent group arpwatch &> /dev/null; then
  getent group pcap 2> /dev/null | grep -q 77 &&
    /usr/sbin/groupmod -n arpwatch pcap 2> /dev/null ||
    /usr/sbin/groupadd -g 77 arpwatch 2> /dev/null
fi              
if ! getent passwd arpwatch &> /dev/null; then
  getent passwd pcap 2> /dev/null | grep -q 77 &&
    /usr/sbin/usermod -l arpwatch -g 77 \
      -d %{_vararpwatch} pcap 2> /dev/null ||
    /usr/sbin/useradd -u 77 -g 77 -s /sbin/nologin \
      -M -r -d %{_vararpwatch} arpwatch 2> /dev/null
fi
:
        
%postun
/bin/systemctl daemon-reload &> /dev/null
if [ "$1" -ge 1 ]; then
  /bin/systemctl try-restart arpwatch.service &> /dev/null
fi 
:
        
%preun
if [ "$1" -eq 0 ]; then
  /bin/systemctl --no-reload disable arpwatch.service &> /dev/null
  /bin/systemctl stop arpwatch.service &> /dev/null
fi
:

%files
%defattr(-,root,root)
%doc README CHANGES arpfetch
%{_sbindir}/arpwatch
%{_sbindir}/arpsnmp
%{_sbindir}/arp2ethers
%{_sbindir}/massagevendor
%{_sbindir}/massagevendor-old
%{_mandir}/man8/arpwatch.8*
%{_mandir}/man8/arpsnmp.8*
%{_mandir}/man8/arp2ethers.8.*
%{_mandir}/man8/massagevendor.8.*
%{_unitdir}/arpwatch.service
%config(noreplace) %{_sysconfdir}/sysconfig/arpwatch
%defattr(-,arpwatch,arpwatch)
%dir %{_vararpwatch}
%verify(not md5 size mtime) %config(noreplace) %{_vararpwatch}/arp.dat
%verify(not md5 size mtime) %config %{_vararpwatch}/ethercodes.dat

%changelog
* Mon Mar 31 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1a15-14m)
- fix for UserMove

* Mon Dec 02 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1a15-13m)
- add patches

* Sat Jun  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1a15-12m)
- [SECURITY] CVE-2012-2653

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1a15-11m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1a15-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1a15-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1a15-8m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1a15-7m)
- rebuild against libpcap-1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1a15-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1a15-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1a15-4m)
- rebuild against gcc43

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1a15-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1a15-2m)
- NoSource: 0

* Thu Jun  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1a15-1m)
- Prereq: /sbin/chkconfig -> chkconfig
- Prereq: /sbin/service   -> initscripts
- Requires: /usr/sbin/sendmail -> smtpdaemon


* Thu May 24 2007 Miroslav Lichvar <mlichvar@redhat.com> 14:2.1a15-4
- fix return codes in init script (#237781)

* Mon Jan 15 2007 Miroslav Lichvar <mlichvar@redhat.com> 14:2.1a15-3
- rename pcap user to arpwatch

* Tue Nov 28 2006 Miroslav Lichvar <mlichvar@redhat.com> 14:2.1a15-2
- split from tcpdump package (#193657)
- update to 2.1a15
- clean up files in /var
- force linking with system libpcap
