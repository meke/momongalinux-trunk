%global		momorel 1
Name:           libgweather
Version:        3.6.2
Release: %{momorel}m%{?dist}
Summary:        A library for weather information

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://www.gnome.org
#VCS: git:git://git.gnome.org/libgweather
Source0:        http://download.gnome.org/sources/libgweather/3.6/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires:  GConf2-devel >= 2.8.0
BuildRequires:  dbus-devel
BuildRequires:  gtk3-devel >= 2.90.0
BuildRequires:  gtk-doc
BuildRequires:  libsoup-devel >= 2.4
BuildRequires:  libxml2-devel >= 2.6
BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  autoconf automake libtool
BuildRequires:  gobject-introspection-devel >= 0.10
BuildRequires:  gnome-common

# for directories
Requires: gnome-icon-theme

%description
libgweather is a library to access weather information from online
services for numerous locations.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
# libgweather used to be part of gnome-applets, and
# gnome-applets-devel only had the libgweather-devel parts in it
Obsoletes:	gnome-applets-devel < 1:2.21.4-1
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires:       gtk-doc

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

%build
%configure --disable-static --disable-gtk-doc
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

%find_lang %{name} --all-name

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/gnome &>/dev/null || :

%preun

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/gnome &>/dev/null
  gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas
fi

%posttrans
gtk-update-icon-cache -q %{_datadir}/icons/gnome &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/libgweather-3.so.*
%{_libdir}/girepository-1.0/GWeather-3.0.typelib
%dir %{_datadir}/libgweather
%{_datadir}/libgweather/Locations.xml
%{_datadir}/libgweather/Locations.*.xml
%{_datadir}/libgweather/locations.dtd
%{_datadir}/icons/gnome/*/status/*
%{_datadir}/glib-2.0/schemas/org.gnome.GWeather.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.GWeather.gschema.xml

%files devel
%defattr(-,root,root,-)
%{_includedir}/libgweather-3.0
%{_libdir}/libgweather-3.so
%{_libdir}/pkgconfig/gweather-3.0.pc
%{_datadir}/gir-1.0/GWeather-3.0.gir
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/libgweather-3.0


%changelog
* Wed Jan 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Tue Sep  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.1-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.3-1m)
- update to 3.1.3

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.30.3-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.3-2m)
- rebuild for new GCC 4.5

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.30.3-1m)
- update to 2.30.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-3m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- build fix gcc-4.4.4

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2.1-1m)
- update to 2.26.2.1

* Wed May 27 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.26.1-2m)
- libtool build fix

* Tue Apr 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.5-1m)
- update to 2.25.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild against python-2.6.1-1m

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Wed Oct 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0
- add python module (unstable)

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.1-1m)
- update to 2.22.1.1
- 2.22.1.2 was broken (intltool-merge)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-3m)
- rebuild against gcc43

* Fri Mar 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-2m)
- Obsoletes gnome-applets-devel

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- initial build
