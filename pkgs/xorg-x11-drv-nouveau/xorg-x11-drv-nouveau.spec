%global momorel 1
%define tarball xf86-video-nouveau
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir %{moduledir}/drivers
%global xorgurl http://xorg.freedesktop.org/releases/individual

Summary:   Xorg X11 nouveau video driver for NVIDIA graphics chipsets
Name:      xorg-x11-drv-nouveau
Version:   1.0.9
Release:   %{momorel}m%{?dist}
URL:       http://www.x.org
License:   MIT
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2
NoSource: 0

ExcludeArch: s390 s390x

BuildRequires: libtool automake autoconf
BuildRequires: xorg-x11-server-devel >= 1.13.0
BuildRequires: libdrm-devel >= 2.4.37
BuildRequires: mesa-libGL-devel >= 8.1.0-0.20120707.2m
BuildRequires: kernel-headers >= 2.6.31
BuildRequires: systemd-devel >= 187

Requires:  hwdata
Requires:  xorg-x11-server-Xorg >= 1.13.0
Requires:  libdrm >= 2.4.37

%description 
X.Org X11 nouveau video driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
autoreconf -v --install
%configure --disable-static --with-kms=yes
%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -regex ".*\.la$" | xargs rm -f --

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/nouveau_drv.so
%{_mandir}/man4/nouveau.4*

%changelog
* Fri Aug  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.9-1m)
- update 1.0.9

* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.8-1m)
- update 1.0.8

* Sun Mar 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.7-1m)
- update 1.0.7

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6

* Fri Nov  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-1m)
- update 1.0.4

* Wed Nov  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-1m)
- update 1.0.3

* Mon Sep 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-1m)
- update 1.0.2

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-3m)
- rebuild against xorg-x11-server-1.13.0

* Sat Aug  4 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- rebuild against systemd-187

* Sat Jun 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-1m)
- update 1.0.1

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.16-0.20110619.4m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.16-0.20110619.3m)
- update 20110619

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.16-0.20110303.3m)
- rebuild for xorg-x11-server-1.10.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.16-0.20110303.2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.16-0.20110303.1m)
- update 20110303git92db2bc

* Tue Dec 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.16-0.20101130.1m)
- update 20101130

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.16-0.20100615.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.16-0.20100615.2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.16-0.20100615.1m)
- update 20100615

* Mon Apr 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.16-0.20100410.1m)
- support libdrm-2.4.20

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.16-0.20100218.1m)
- Source0: xf86-video-nouveau-0.0.16-20100218git2964702.tar.bz2
- sync with fc devel

* Sat Jan  9 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.15-0.20091217.1m)
- Source0: xf86-video-nouveau-0.0.15-20091217gitbb19478.tar.bz2
- sync with fc devel

* Tue Nov 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.12-0.20090528.3m)
- rebuild with new xserver

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.12-0.20090528.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul  4 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.0-0.20090528-1m)
- update 20090528

* Thu Mar 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.0-0.20090326-1m)
- update 20090326

* Fri Mar 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.0-0.20090313-2m)
- import nouveau-fedora.patch from Fedora
 - * Fri Mar 13 2009 Ben Skeggs <bskeggs@redhat.com> 0.0.12-11.20090313git79d23d8
 - - kms: dpms fixes
 - - kms: nicer reporting of output properties to users
 - - improve init paths, more robust
 - - support for multiple xservers (fast user switching)
- import nouveau-eedid.patch from Fedora
 - * Fri Feb 27 2009 Adam Jackson <ajax@redhat.com> 0.0.12-6.20090224gitd91fc78
 - - nouveau-eedid.patch: Do EEDID.

* Fri Mar 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.0-0.20090313-1m)
- update git-20090313

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.0-0.20080915.2m)
- rebuild against rpm-4.6

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.0-0.20080914-1m)
- update git-20080914

* Mon Jul 14 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.0-0.20080714-1m)
- update git-20080714

* Thu Jul  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20080703-1m)
- update git-20080703

* Sun May  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20080504-1m)
- update git-20080504
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Sun Apr 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20080413-1m)
- update 20080413

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.0-0.20080304.2m)
- rebuild against gcc43

* Tue Mar  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20080304-1m)
- update 20080304

* Mon Jan 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20080128-1m)
- update 20080128

* Sun Jan  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20080106-1m)
- update 20080106

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20070418-2m)
- rebuild against xorg-x11-server-1.4

* Thu Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20070418-1m)
- update 20070418

* Sat Mar  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20070303-1m)
- update 20070303

* Thu Jan 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20070110-1m)
- update 20070110

* Mon Jan  1 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.0-0.20061231-1m)
- Initial Commit

