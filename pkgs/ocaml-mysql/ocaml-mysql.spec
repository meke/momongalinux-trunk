%global momorel 15
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Summary: Bindings for interacting with MySQL databases from ocaml
Name: ocaml-mysql
Version: 1.0.4
Release: %{momorel}m%{?dist}
License: "LGPLv2+ with exceptions"
Group: System Environment/Libraries
URL: http://raevnos.pennmush.org/code/ocaml-mysql/
Source: http://raevnos.pennmush.org/code/%{name}/%{name}-%{version}.tar.gz
Patch0: ocaml-mysql-1.0.4-CVE-2009-2942-missing-escape.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: ocaml-camlp4-devel >= %{ocamlver}
BuildRequires: ocaml-findlib >= 1.2.1-2m
BuildRequires: mysql-devel >= 5.1.32
Requires: mysql-libs


%description
ocaml-mysql is a package for ocaml that provides access to mysql
databases. It consists of low level functions implemented in C and a
module Mysql intended for application development.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: mysql-devel

%description devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%setup -q
%patch0 -p1 -b .CVE-2009-2942

%build
%configure
%__make
%if %opt
%__make opt
%endif

strip dll*.so
chrpath --delete dll*.so

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_libdir}/ocaml/mysql
%__mkdir_p %{buildroot}%{_libdir}/ocaml/stublibs
%__make install OCAMLFIND_INSTFLAGS="-destdir %{buildroot}%{_libdir}/ocaml"
%__rm -f %{buildroot}%{_libdir}/ocaml/stublibs/*.owner

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/ocaml/mysql/META
%{_libdir}/ocaml/mysql/mysql.cma
%{_libdir}/ocaml/mysql/mysql.cmi
%{_libdir}/ocaml/stublibs/dllmysql_stubs.so

%files devel
%defattr(-,root,root,-)
%doc COPYING CHANGES doc/html README VERSION
%if %opt
%{_libdir}/ocaml/mysql/libmysql_stubs.a
%{_libdir}/ocaml/mysql/mysql.a
%{_libdir}/ocaml/mysql/mysql.cmx
%{_libdir}/ocaml/mysql/mysql.cmxa
%endif
%{_libdir}/ocaml/mysql/mysql.mli

%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-15m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-12m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-11m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-9m)
- [SECURITY] CVE-2009-2942
- import a security patch from Rawhide (1.0.4-11)

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-8m)
- Requires: mysql-libs

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-7m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-6m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-5m)
- rewrite %%opt and %%files in the Fedora style
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-4m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- rebuild against ocaml-3.10.2

* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-2m)
- rebuild against ocaml-3.10.1-1m

* Thu Nov 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-1m)
- initial packaging
