%global momorel 3

Name: libvisio
Version: 0.0.16
Release: %{momorel}m%{?dist}
Summary: A library providing ability to interpret and import visio diagrams

Group: System Environment/Libraries
License: GPL+ or LGPLv2+ or MPLv1.1
URL: http://www.freedesktop.org/wiki/Software/libvisio
Source: http://dev-www.libreoffice.org/src/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: boost-devel
BuildRequires: doxygen
BuildRequires: libwpd-devel
BuildRequires: libwpg-devel

%description
Libvisio is library providing ability to interpret and import visio
diagrams into various applications. You can find it being used in
libreoffice.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package doc
Summary: Documentation of %{name} API
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
The %{name}-doc package contains documentation files for %{name}.

%package tools
Summary: Tools to transform Visio diagrams into other formats
Group: Applications/Publishing
Requires: %{name}%{?_isa} = %{version}-%{release}

%description tools
Tools to transform Visio diagrams into other formats.
Currently supported: XHTML, raw.


%prep
%setup -q


%build
%configure --disable-static --disable-werror
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}/%{_libdir}/*.la


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING.*
%{_libdir}/%{name}-0.0.so.*


%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}-0.0
%{_libdir}/%{name}-0.0.so
%{_libdir}/pkgconfig/%{name}-0.0.pc


%files doc
%defattr(-,root,root,-)
%dir %{_docdir}/%{name}
%{_docdir}/%{name}/html


%files tools
%defattr(-,root,root,-)
%{_bindir}/vsd2raw
%{_bindir}/vsd2xhtml


%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.16-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.0.16-2m)
- rebuild for boost 1.50.0

* Thu May 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16-1m)

* Wed Feb 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15-1m)
- Initial commit Momonga Linux
