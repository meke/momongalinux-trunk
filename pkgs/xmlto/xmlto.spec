%global momorel 1

Summary: A tool for converting XML files to various formats
Name: xmlto
Version: 0.0.26
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
#Older versions up to xmlto-0.0.20
#URL: http://cyberelk.net/tim/xmlto/
#Source0: http://cyberelk.net/tim/data/xmlto/stable/%{name}-%{version}.tar.bz2
URL: https://fedorahosted.org/xmlto/
Source0: https://fedorahosted.org/releases/x/m/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch1: http://www.momonga-linux.org/~kuma/xmlto-0.0.15-docbookx.dtd.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: docbook-xsl >= 1.56.0
BuildRequires: libxslt
BuildRequires: util-linux, flex

# We rely heavily on the DocBook XSL stylesheets!
Requires: docbook-xsl >= 1.56.0
Requires: text-www-browser
# We need w3m or elinks or lynx for text output.  Let's go for w3m,
# since this one is preferred upstream.
Requires: w3m
Requires: libxslt
Requires: docbook-dtds
Requires: util-linux, flex

%description
This is a package for converting XML files to various formats using XSL
stylesheets.

%package tex
Group: Applications/System
License: GPLv2
Summary: A set of xmlto backends with TeX requirements 
# For full functionality, we need passivetex.
Requires: passivetex >= 1.11
# We require main package
Requires: xmlto

%description tex
This subpackage contains xmlto backend scripts which do require
PassiveTeX/TeX for functionality.

%package xhtml
Group: Applications/System
License: GPLv2+
Summary: A set of xmlto backends for xhtml1 source format
# For functionality we need stylesheets xhtml2fo-style-xsl
Requires: xhtml2fo-style-xsl
# We require main package
Requires: xmlto = %{version}-%{release}

%description xhtml
This subpackage contains xmlto backend scripts for processing
xhtml1 source format.

%prep
%setup -q
#%patch1 -p1 -b .kuma

%build
touch doc/xmlto.xml doc/xmlif.xml
%configure

make

%check
make check

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/*/*
%{_datadir}/xmlto
%exclude %{_datadir}/xmlto/format/fo/dvi
%exclude %{_datadir}/xmlto/format/fo/ps
%exclude %{_datadir}/xmlto/format/fo/pdf
%exclude %dir %{_datadir}/xmlto/format/xhtml1/
%exclude %{_datadir}/xmlto/format/xhtml1

%files tex
%defattr(-,root,root)
%{_datadir}/xmlto/format/fo/dvi
%{_datadir}/xmlto/format/fo/ps
%{_datadir}/xmlto/format/fo/pdf

%files xhtml
%defattr(-,root,root)
%dir %{_datadir}/xmlto/format/xhtml1/
%{_datadir}/xmlto/format/xhtml1

%changelog
* Mon Jun 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.26-1m)
- update 0.0.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.23-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.23-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.23-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.23-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.23-1m)
- update to 0.0.23

* Thu Mar 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.22-1m)
- update to 0.0.22
- split xmlto-xhtml package
- drop xmlto-0.0.15-docbookx.dtd.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.21-2m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.21-1m)
- update to 0.0.21

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.18-4m)
- rebuild against gcc43

* Wed Jun 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0.18-3m)
- resync with Fedora Core (replace xmlto-0.0.12-encoding.patch with xmlto-basename.patch)
- * Thu Jun  8 2006 Tim Waugh <twaugh@redhat.com> 0.0.18-13
- - Removed debugging.
- * Thu Jun  8 2006 Tim Waugh <twaugh@redhat.com> 0.0.18-12
- - Debug build.
- * Thu Jun  8 2006 Tim Waugh <twaugh@redhat.com> 0.0.18-11
- - Rebuilt.
- * Mon Jun  5 2006 Tim Waugh <twaugh@redhat.com> 0.0.18-10
- - Rebuilt.
- * Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.0.18-9.2.1
- - bump again for double-long bug on ppc(64)
- * Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.0.18-9.2
- - rebuilt for new gcc4.1 snapshot and glibc changes
- * Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- - rebuilt
- * Mon Aug  8 2005 Tim Waugh <twaugh@redhat.com> 0.0.18-9
- - Fixed quoting in scripts (bug #165338).
- * Mon Aug  1 2005 Tim Waugh <twaugh@redhat.com> 0.0.18-8
- - Requires w3m (bug #164798).
- * Mon Jul 25 2005 Tim Waugh <twaugh@redhat.com> 0.0.18-7
- - Rebuild for new man-pages stylesheet.
- * Mon Apr  4 2005 Tim Waugh <twaugh@redhat.com>
- - Requires util-linux and flex, as does the build.
- * Wed Mar  2 2005 Tim Waugh <twaugh@redhat.com> 0.0.18-6
- - Rebuild for new GCC.
- * Wed Feb  9 2005 Tim Waugh <twaugh@redhat.com> 0.0.18-5
- - Rebuilt.
- * Thu Jul  1 2004 Tim Waugh <twaugh@redhat.com> 0.0.18-4
- - Magic encoding is enabled again (bug #126921).

* Tue Nov 1 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
-  (0.0.18-2m)
- rebuild against libxslt-1.1.12-3m

* Tue Dec  7 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.0.18-1m)
  update to 0.0.18

* Tue Apr 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.0.15-2m)
- applied kuma's patch.

* Wed Apr 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.0.15-1m)
- import from FC1 for anaconda.

* Tue Oct  7 2003 Tim Waugh <twaugh@redhat.com> 0.0.15-1
- 0.0.15.

* Tue Sep 23 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- allow compiling without tetex(passivetex) dependency

* Tue Jun 17 2003 Tim Waugh <twaugh@redhat.com> 0.0.14-3
- Rebuilt.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri May 23 2003 Tim Waugh <twaugh@redhat.com> 0.0.14-1
- 0.0.14.

* Sun May 11 2003 Tim Waugh <twaugh@redhat.com> 0.0.13-1
- 0.0.13.

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Fri Jan  3 2003 Tim Waugh <twaugh@redhat.com> 0.0.12-2
- Disable magic encoding detection, since the stylesheets don't handle
  it well at all (bug #80732).

* Thu Dec 12 2002 Tim Waugh <twaugh@redhat.com> 0.0.12-1
- 0.0.12.

* Wed Oct 16 2002 Tim Waugh <twaugh@redhat.com> 0.0.11-1
- 0.0.11.
- xmlto.mak no longer needed.
- CVS patch no longer needed.
- Update docbook-xsl requirement.
- Ship xmlif.
- Run tests.
- No longer a noarch package.

* Tue Jul  9 2002 Tim Waugh <twaugh@redhat.com> 0.0.10-4
- Ship xmlto.mak.

* Thu Jun 27 2002 Tim Waugh <twaugh@redhat.com> 0.0.10-3
- Some db2man improvements from CVS.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com> 0.0.10-2
- automated rebuild

* Tue Jun 18 2002 Tim Waugh <twaugh@redhat.com> 0.0.10-1
- 0.0.10.
- No longer need texinputs patch.

* Tue Jun 18 2002 Tim Waugh <twaugh@redhat.com> 0.0.9-3
- Fix TEXINPUTS problem with ps and dvi backends.

* Thu May 23 2002 Tim Powers <timp@redhat.com> 0.0.9-2
- automated rebuild

* Wed May  1 2002 Tim Waugh <twaugh@redhat.com> 0.0.9-1
- 0.0.9.
- The nonet patch is no longer needed.

* Fri Apr 12 2002 Tim Waugh <twaugh@redhat.com> 0.0.8-3
- Don't fetch entities over the network.

* Thu Feb 21 2002 Tim Waugh <twaugh@redhat.com> 0.0.8-2
- Rebuild in new environment.

* Tue Feb 12 2002 Tim Waugh <twaugh@redhat.com> 0.0.8-1
- 0.0.8.

* Fri Jan 25 2002 Tim Waugh <twaugh@redhat.com> 0.0.7-2
- Require the DocBook DTDs.

* Mon Jan 21 2002 Tim Waugh <twaugh@redhat.com> 0.0.7-1
- 0.0.7 (bug #58624, bug #58625).

* Wed Jan 16 2002 Tim Waugh <twaugh@redhat.com> 0.0.6-1
- 0.0.6.

* Wed Jan 09 2002 Tim Powers <timp@redhat.com> 0.0.5-4
- automated rebuild

* Wed Jan  9 2002 Tim Waugh <twaugh@redhat.com> 0.0.5-3
- 0.0.6pre2.

* Wed Jan  9 2002 Tim Waugh <twaugh@redhat.com> 0.0.5-2
- 0.0.6pre1.

* Tue Jan  8 2002 Tim Waugh <twaugh@redhat.com> 0.0.5-1
- 0.0.5.

* Mon Dec 17 2001 Tim Waugh <twaugh@redhat.com> 0.0.4-2
- 0.0.4.
- Apply patch from CVS to fix silly typos.

* Sat Dec  8 2001 Tim Waugh <twaugh@redhat.com> 0.0.3-1
- 0.0.3.

* Wed Dec  5 2001 Tim Waugh <twaugh@redhat.com>
- Built for Red Hat Linux.

* Fri Nov 23 2001 Tim Waugh <twaugh@redhat.com>
- Initial spec file.
