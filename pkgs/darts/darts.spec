%global momorel 6

Summary: Double ARray Trie System
Name: darts
Version: 0.32
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://www.chasen.org/~taku/software/darts/
Group: Development/Languages
Source0: http://www.chasen.org/~taku/software/darts/src/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel
BuildRequires: gcc-c++ >= 3.4.1-1m
Requires: libstdc++ >= 3.2.3
Requires: zlib

%description
Darts is simple C++ Template Library for building Double-Array.

%prep

%setup -q

%build
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"
%configure
make
make check

%install
%makeinstall

%clean
rm -rf  %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc README AUTHORS COPYING NEWS doc/*.html doc/*.css
%{_includedir}/darts.h
%{_libexecdir}/darts

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.32-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.32-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.32-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.32-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.32-2m)
- rebuild against rpm-4.6

* Tue Apr 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31-2m)
- %%NoSource -> NoSource

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Mon Dec 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.3-1m)
- up to 0.3
- modify URI

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.2-2m)
- rebuild against gcc-c++-3.4.1

* Mon Aug 25 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.2-1m)
- first import for momonga.
