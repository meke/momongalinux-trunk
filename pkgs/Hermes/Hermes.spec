%global momorel 13

Summary: HERMES pixel format conversion library
Name: Hermes
Version: 1.3.3
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Development/Libraries
Source0: http://clanlib.org/download/legacy/%{name}-%{version}.tar.bz2
Nosource: 0
Patch0: Hermes-1.3.3-build.patch
Patch1: Hermes.gcc4.patch
Patch2: Hermes-1.3.3-ar_flags.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://clanlib.org/

%description
HERMES is a library designed to convert a source buffer with a specified 
pixel format to a destination buffer with possibly a different format at
the maximum possible speed.

On x86 and MMX architectures, handwritten assembler routines are taking over
the job and doing it lightning fast.

On top of that, HERMES provides fast surface clearing, stretching and some
dithering. Supported platforms are basically all that have an ANSI C
compiler as there is no platform specific code but those are supported: DOS,
Win32 (Visual C), Linux, FreeBSD (IRIX, Solaris are on hold at the moment),
some BeOS support.

%package devel
Summary: hermes-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
hermes-devel

%prep
%setup -q
%patch0 -p1
%patch1 -p1 -b .gcc4
%patch2 -p1 -b .ar_flags

%build
libtoolize -c -f
#aclocal-1.7
aclocal
#automake-1.7 -c -f -a
automake -c -f -a
autoconf
%configure
make


%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING INSTALL.DOS INSTALL.unix TODO TODO.conversion docs/api/*.htm
%{_libdir}/libHermes.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/libHermes.so
%{_libdir}/libHermes.a
%{_includedir}/Hermes

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.3-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.3-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-9m)
- remove AR_FLAGS (Patch2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-7m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-6m)
- rebuild against gcc43

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-5m)
- change Source URL

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.3-4m)
- delete libtool library

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-3m)
- add gcc4.patch
- Patch1: Hermes.gcc4.patch

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.3-2m)
- rebuild against automake

* Sun Nov 07 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3
- update Patch0

* Sat Mar 20 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.2-3m)
- change docdir %%defattr

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-2m)
- s/Copyright:/License:/
- where is Hermes-1.3.2-1m changeloog?
