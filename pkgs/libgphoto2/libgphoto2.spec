%global momorel 1

Summary: Library that can be used by applications to access various digital cameras
Name: libgphoto2
Version: 2.5.1
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.gphoto.org/
Group: Development/Libraries
Source0: http://dl.sourceforge.net/sourceforge/gphoto/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: gphoto2-pkgcfg.patch
Patch1: gphoto2-storage.patch
Patch2: gphoto2-ixany.patch
Patch3: gphoto2-device-return.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dbus >= 0.92
Requires: lockdev
Requires: udev
BuildRequires: gtk-doc
BuildRequires: libtool >= 2.2.0
BuildRequires: libtool-ltdl-devel >= 2.2.0
BuildRequires: libexif-devel >= 0.6.13-2m
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libusb-devel >= 0.1.6a
BuildRequires: transfig
BuildRequires: cmpskit
BuildRequires: openjade
BuildRequires: lockdev-devel
BuildRequires: pkgconfig

%description
libgphoto2 itself is not a GUI application, opposed to gphoto. There are
GUI frontends for the gphoto2 library, however, such as gtkam for
example.

libgphoto2 can only talk to cameras the language of those it understands.
That is, if you own a camera that speaks a language that isn't published
anywhere and nobody has been able to figure out the meaning of the sentences,
libgphoto2 cannot communicate with those cameras.

%package devel
Summary: Headers and links to compile against the libgphoto2 library.
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Obsoletes: gphoto2-devel
Group: Development/Libraries

%description devel
This package contains the files needed to compile applications that
use libgphoto2.

%prep
%setup -q

%patch0 -p1 -b .pkgcfg
%patch1 -p1 -b .storage
%patch2 -p1 -b .ixany
%patch3 -p1 -b .device-return

%build
%configure \
  --with-drivers=all \
  --with-doc-dir=%{_docdir}/%{name} \
  --disable-rpath

%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

pushd packaging/linux-hotplug/
mkdir -p %{buildroot}%{_datadir}/hal/fdi/information/20thirdparty
export LIBDIR=%{buildroot}%{_libdir}
export CAMLIBS=%{buildroot}%{_libdir}/%{name}/%{version}
export LD_LIBRARY_PATH=%{buildroot}%{_libdir}

mkdir -p %{buildroot}/lib/udev/rules.d
%{buildroot}%{_libdir}/%{name}/print-camera-list udev-rules version 136 > %{buildroot}/lib/udev/rules.d/40-%{name}.rules
popd

cp -a %{buildroot}%{_docdir}/%{name} installed-doc
rm -rf %{buildroot}%{_docdir}/%{name}

# DO NOT REMOVE %%{_libdir}/%{name}*/*/*.la, digikam needs them
find %{buildroot}%{_libdir} -maxdepth 1 -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog HACKING
%doc INSTALL MAINTAINERS NEWS README TESTERS
%doc doc/DAEMON
/lib/udev/rules.d/40-%{name}.rules
%{_libdir}/%{name}
%{_libdir}/%{name}_port
%{_libdir}/*.so.*
%{_libdir}/udev/check-ptp-camera
%{_datadir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_mandir}/man3/%{name}.3*
%{_mandir}/man3/%{name}_port.3*

%files devel
%defattr(-,root,root)
%doc installed-doc/*
%{_bindir}/gphoto2-config
%{_bindir}/gphoto2-port-config
%{_includedir}/gphoto2
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/pkgconfig/%{name}_port.pc
%{_libdir}/*.so

%changelog
* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Tue Aug 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Thu Mar  8 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.11-2m)
- delete file for hal

* Sun Feb 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.11-2m)
- rebuild against HAL removed env

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.11-2m)
- remove BR hal

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.11-1m)
- update to 2.4.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.10.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.10.1-2m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.10.1-1m)
- update to 2.4.10.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.9.1-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.9.1-1m)
- update to 2.4.9.1

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.8-2m)
- rebuild against libjpeg-8a

* Wed Mar 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.8-1m)
- version 2.4.8
- import device-return.patch from Fedora
- remove PreReq

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.7-2m)
- generate and install 40-libgphoto2.rules for udev
- import ixany.patch and maxentries.patch from Fedora
- update storage.patch
- License: LGPLv2+

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.7-1m)
- update to 2.4.7

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.5-1m)
- update to 2.4.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.3-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Tue Dec 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-4m)
- revive %%{_libdir}/libgphoto2*/*/*.la for digikam

* Tue Apr 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-3m)
- remove gphoto-set-procperm
- remove 90-gphoto-camera-policy.fdi
- import gphoto2-storage.patch from Fedora
 +* Tue Nov 27 2007 Jindrich Novy <jnovy@redhat.com> 2.4.0-4
 +- fix permission problems while accessing camera (#400491)
 +* Tue Sep 18 2007 Jindrich Novy <jnovy@redhat.com> 2.4.0-3
 +- avoid tagging mass storage devices as cameras (#295051)
- import gphoto2-data-phase.patch from Fedora
 +* Thu Apr 17 2008 Jindrich Novy <jnovy@redhat.com> 2.4.0-7
 +- backport patch from upstream to avoid segfault when
 +   data phase is skipped for certain devices (#435413)
- re-enable-doc

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0
- disable-doc (openjade does not work)

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-3m)
- libtool-1.5.24

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-2m)
- rebuild against libtool-2.2

* Thu Mar 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-1m)
- version 2.3.1
- remove gcc4.patch
- remove merged config.patch
- remove dbus.patch

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-4m)
- delete libtool library

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-3m)
- add patch2 for dbus

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-2m)
- rebuild against dbus 0.92

* Mon Jul 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1-1m)
- version 2.2.1

* Sun Jun 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-1m)
- version 2.2.0
- import gphoto2-gcc4.patch from Fedora Core devel
- import gphoto2-2.1.99-config.patch from Fedora Core devel

* Sun Jun 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.6-3m)
- revise 10-camera-libgphoto2.fdi

* Sun Jun 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.6-2m)
- remove PreReq: hotplug
- import following 3 sources from Fedora Core devel
  gen-libgphoto-hal-fdi
  90-gphoto-camera-policy.fdi
  gphoto-set-procperm

* Sun May 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.6-1m)
- update to 2.1.6
- delete patch1

* Wed May  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.5-4m)
- disable document

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.5-3m)
- rebuild against libexif-0.6.12-1m

* Fri Feb  4 2005 Toru Hoshina <t@momonga-linux.org>
- (2.1.5-2m)
- enable x86_64.

* Thu Jan 20 2005 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.5-1m)
- version 2.1.5
- remove libgphoto2-ac_fix.patch
- gphoto2-hotplug.patch (fedora 2.1.4-5)
- use lockdev (BuildPreReq, Requires)
- clean up %%build section

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.4-2m)
- revised spec for enabling rpm 4.2.

* Sat Jan 24 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.4-1m)
- version 2.1.4
- remove libgphoto2-2.1.3-makefile_am.patch

* Fri Oct 31 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.3-3m)
- version 2.1.3
- add libgphoto2-2.1.3-makefile_am.patch

* Fri Sep 26 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.2-2m)
- modify %%build section
- add libgphoto2-ac_fix.patch from PLD
- add print-usb-usermap to %%files
- add %%post

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.1.2-1m)
- version 2.1.2

* Wed Aug 6 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.1-3m)
- rebuild against for libexif-0.5.12
- use %%NoSource macro
- define momorel

* Thu Feb 13 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-2m)
- rebuild against for libexif

* Mon Dec 9 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (2.1.1-1m)
- New packages
- gphoto2 command line interface was split from the libgphoto2 library
