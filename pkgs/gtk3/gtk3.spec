%global momorel 1
%global glib2_version 2.24.0
%global pango_version 1.36.3
%global gdk_pixbuf_version 2.30.7
%global atk_version 2.13.1
%global cairo_version 1.12.6
%global xrandr_version 1.4.2

%global bin_version 3.0.0

Summary: The GIMP ToolKit (GTK+), a library for creating GUIs for X
Name: gtk3
Version: 3.12.1
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.gtk.org
#VCS: git:git://git.gnome.org/gtk+
Source: http://download.gnome.org/sources/gtk+/3.12/gtk+-%{version}.tar.xz
NoSource: 0
#Source1: im-cedilla.conf

BuildRequires: gnome-common autoconf automake intltool gettext
BuildRequires: at-spi2-atk-devel
BuildRequires: atk-devel >= %{atk_version}
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: cairo-devel >= %{cairo_version}
BuildRequires: cairo-gobject-devel >= %{cairo_version}
BuildRequires: pango-devel >= %{pango_version}
BuildRequires: gdk-pixbuf2-devel >= %{gdk_pixbuf_version}
BuildRequires: gtk2-devel
BuildRequires: libXi-devel
BuildRequires: gettext-devel
BuildRequires: gtk-doc
BuildRequires: cups-devel
BuildRequires: libXrandr-devel >= %{xrandr_version}
BuildRequires: libXrender-devel
BuildRequires: libXcursor-devel
BuildRequires: libXfixes-devel
BuildRequires: libXinerama-devel
BuildRequires: libXcomposite-devel
BuildRequires: libXdamage-devel
BuildRequires: libXi-devel
BuildRequires: gobject-introspection-devel
BuildRequires: colord-devel

# required for icon theme apis to work
Requires: hicolor-icon-theme

# We need to prereq these so we can run gtk-query-immodules-3.0
Requires(post): glib2 >= %{glib2_version}
Requires(post): atk >= %{atk_version}
Requires(post): pango >= %{pango_version}
Requires: libXrandr >= %{xrandr_version}

# gtk3 no longer provides the GtkThemeEngine interface used there
Obsoletes: gtk3-engines <= 2.91.5-5.fc15

%description
GTK+ is a multi-platform toolkit for creating graphical user
interfaces. Offering a complete set of widgets, GTK+ is suitable for
projects ranging from small one-off tools to complete application
suites.

This package contains version 3 of GTK+.

%package immodules
Summary: Input methods for GTK+
Group: System Environment/Libraries
Requires: gtk3%{?_isa} = %{version}-%{release}
# for /etc/X11/xinit/xinput.d and im-cedilla.conf
Requires: imsettings

%description immodules
The gtk3-immodules package contains standalone input methods that
are shipped as part of GTK+ 3.

%package immodule-xim
Summary: XIM support for GTK+
Group: System Environment/Libraries
Requires: gtk3%{?_isa} = %{version}-%{release}

%description immodule-xim
The gtk3-immodule-xim package contains XIM support for GTK+ 3.

%package devel
Summary: Development files for GTK+
Group: Development/Libraries
Requires: gtk3 = %{version}-%{release}
Requires: gdk-pixbuf2-devel
Requires: libX11-devel, libXcursor-devel, libXinerama-devel
Requires: libXext-devel, libXi-devel, libXrandr-devel
Requires: libXfixes-devel, libXcomposite-devel
# for /usr/share/aclocal
Requires: automake

Obsoletes: gtk3-engines-devel <= 2.91.5-5.fc15

%description devel
This package contains the libraries and header files that are needed
for writing applications with version 3 of the GTK+ widget toolkit. If
you plan to develop applications with GTK+, consider installing the
gtk3-devel-docs package.

%package devel-docs
Summary: Developer documentation for GTK+
Group: Development/Libraries
Requires: gtk3 = %{version}-%{release}

%description devel-docs
This package contains developer documentation for version 3 of the GTK+
widget toolkit.

%prep
%setup -q -n gtk+-%{version}

%build

(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; CONFIGFLAGS=--enable-gtk-doc; fi;
 %configure $CONFIGFLAGS \
        --enable-gtk2-dependency \
        --enable-xkb \
        --enable-xinerama \
        --enable-xrandr \
        --enable-xfixes \
        --enable-xcomposite \
        --enable-xdamage \
        --enable-x11-backend \
        --enable-colord \
)

# fight unused direct deps
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

make # %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT        \
             RUN_QUERY_IMMODULES_TEST=false

%find_lang gtk30
%find_lang gtk30-properties

(cd $RPM_BUILD_ROOT%{_bindir}
 mv gtk-query-immodules-3.0 gtk-query-immodules-3.0-%{__isa_bits}
)

# im-cedilla.conf is now provided by imsettings package
# Input method frameworks want this
#install -D %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/X11/xinit/xinput.d/im-cedilla.conf

# Remove unpackaged files
rm $RPM_BUILD_ROOT%{_libdir}/*.la
rm $RPM_BUILD_ROOT%{_libdir}/gtk-3.0/%{bin_version}/*/*.la

touch $RPM_BUILD_ROOT%{_libdir}/gtk-3.0/%{bin_version}/immodules.cache

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/gtk-3.0
mkdir -p $RPM_BUILD_ROOT%{_libdir}/gtk-3.0/modules
mkdir -p $RPM_BUILD_ROOT%{_libdir}/gtk-3.0/immodules
mkdir -p $RPM_BUILD_ROOT%{_libdir}/gtk-3.0/%{bin_version}/theming-engines

%post
/sbin/ldconfig
gtk-query-immodules-3.0-%{__isa_bits} --update-cache
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
[ -L %{_bindir}/gtk-query-immodules-3.0 ] || rm -f %{_bindir}/gtk-query-immodules-3.0
%{_sbindir}/alternatives \
  --install %{_bindir}/gtk-query-immodules-3.0 \
  gtk-query-immodules-3.0 \
  %{_bindir}/gtk-query-immodules-3.0-%{__isa_bits} %{__isa_bits}

%post devel
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%post immodules
gtk-query-immodules-3.0-%{__isa_bits} --update-cache

%post immodule-xim
gtk-query-immodules-3.0-%{__isa_bits} --update-cache

%postun
/sbin/ldconfig
if [ $1 -gt 0 ]; then
  gtk-query-immodules-3.0-%{__isa_bits} --update-cache
fi
[ -e %{_bindir}/gtk-query-immodules-3.0-%{__isa_bits} ] || \
  %{_sbindir}/alternatives --remove gtk-query-immodules-3.0 \
  %{_bindir}/gtk-query-immodules-3.0-%{__isa_bits} 
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%postun devel
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%postun immodules
gtk-query-immodules-3.0-%{__isa_bits} --update-cache

%postun immodule-xim
gtk-query-immodules-3.0-%{__isa_bits} --update-cache

%files -f gtk30.lang
%doc AUTHORS COPYING NEWS README
%{_bindir}/gtk-query-immodules-3.0*
%{_bindir}/gtk-launch
%{_libdir}/libgtk-3.so.*
%{_libdir}/libgdk-3.so.*
%{_libdir}/libgailutil-3.so.*
%dir %{_libdir}/gtk-3.0
%dir %{_libdir}/gtk-3.0/%{bin_version}
%{_libdir}/gtk-3.0/%{bin_version}/theming-engines
%dir %{_libdir}/gtk-3.0/%{bin_version}/immodules
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-cedilla.so
%{_libdir}/gtk-3.0/%{bin_version}/printbackends
%{_libdir}/gtk-3.0/modules
%{_libdir}/gtk-3.0/immodules
%{_datadir}/themes/Default
%{_datadir}/themes/Emacs
%{_libdir}/girepository-1.0
%dir %{_sysconfdir}/gtk-3.0
#%{_sysconfdir}/X11/xinit/xinput.d/im-cedilla.conf
%ghost %{_libdir}/gtk-3.0/%{bin_version}/immodules.cache
%{_mandir}/man1/broadwayd.1.*
%{_mandir}/man1/gtk-query-immodules-3.0.1.*
%{_mandir}/man1/gtk-launch.1.*
%exclude %{_mandir}/man1/gtk-update-icon-cache.1.*
%{_datadir}/glib-2.0/schemas/org.gtk.exampleapp.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gtk.Settings.FileChooser.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gtk.Settings.ColorChooser.gschema.xml

%files immodules
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-am-et.so
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-cyrillic-translit.so
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-inuktitut.so
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-ipa.so
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-multipress.so
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-thai.so
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-ti-er.so
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-ti-et.so
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-viqr.so
%config(noreplace) %{_sysconfdir}/gtk-3.0/im-multipress.conf

%files immodule-xim
%{_libdir}/gtk-3.0/%{bin_version}/immodules/im-xim.so

%files devel -f gtk30-properties.lang
%{_libdir}/lib*.so
%{_includedir}/*
%{_datadir}/aclocal/*
%{_libdir}/pkgconfig/*
%{_bindir}/gtk3-demo
%{_bindir}/gtk3-demo-application
%{_bindir}/gtk3-widget-factory
%{_datadir}/applications/gtk3-demo.desktop
%{_datadir}/applications/gtk3-widget-factory.desktop
%{_datadir}/icons/hicolor/*/apps/gtk3-demo.*
%{_datadir}/icons/hicolor/*/apps/gtk3-widget-factory.*
%{_datadir}/gtk-3.0
%{_datadir}/gir-1.0
%{_datadir}/glib-2.0/schemas/org.gtk.Demo.gschema.xml

%files devel-docs
%{_datadir}/gtk-doc

%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.12.1-1m)
- update to 3.12.1

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.4-1m)
- update to 3.6.4

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Fri Sep 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.18-1m)
- update to 3.5.18

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.16-1m)
- update to 3.5.16

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.12-1m)
- update to 3.5.12

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-1m)
- update to 3.5.10

* Sun Jul 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-2m)
- fix %%{__isa_bits} issue

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-1m)
- update to 3.5.8

* Mon Jul 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.5.6-3m)
- add BR at-spi2-atk-devel

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.6-2m)
- remove im-cedilla.conf, which is now provided by imsettings

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.6-1m)
- update to 3.5.6

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4
- reimport from fedora

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.4-1m)
- update to 3.2.4

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.3-1m)
- update 3.2.3

* Fri Dec 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-2m)
- add broadway support (epipahny support WebSocket)
-- $ GDK_BACKEND=broadway nautilus & epiphany http://localhost:8080

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Sat Oct 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.90-1m)
- update 3.1.90

* Tue Sep 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.18-2m)
- update BuildRequires

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.18-1m)
- update to 3.1.18

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.12-1m)
- update to 3.0.12

* Tue Jun  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.11-1m)
- update to 3.0.11

* Thu May 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.10-1m)
- update to 3.0.10

* Fri May 13 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.9-2m)
- add many enable flags to configure script

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.9-1m)
- update to 3.0.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.8-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.8-1m)
- update to 3.0.8

* Sat Apr  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.7-1m)
- update to 3.0.7

* Thu Mar 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3 (bug fix release)

* Tue Feb 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Feb 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-2m)
- avoid conflict

* Fri Feb 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0
- package name gtk3 (start)

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.0-2m)
- modify SPEC file

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Thu Sep 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-7m)
- build fix libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.1-6m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.20.1-5m)
- change patch gtk-lib64.patch

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.20.1-4m)
- revival Provides: gtk+-devel Obsolete: gtk+-devel

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.20.1-3m)
- split package immodules immodule-xim
- remove package static and remve file *.la
- add update-gdk-pixbuf-loaders update-gtk-immodules im-cedilla.conf
- add patches

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.1-2m)
- split off devel-docs subpackage

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.0-2m)
- rebuild against libjpeg-8a

* Wed Apr  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri Feb 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.6-1m)
- update to 2.19.6

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.5-1m)
- update to 2.19.5

* Thu Jan 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.4-1m)
- update to 2.19.4

* Sat Jan 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.3-1m)
- update to 2.19.3

* Wed Jan 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.6-1m)
- update to 2.18.6

* Sat Dec 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.5-1m)
- [SECURITY] https://bugzilla.gnome.org/show_bug.cgi?id=598476
- update to 2.18.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Tue Oct  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2
-- add static package

* Thu Oct  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0
-- two files were deleted ?
-- %%{_sysconfdir}/gtk-2.0/gdk-pixbuf.loaders
-- %%{_sysconfdir}/gtk-2.0/gtk.immodules

* Tue Sep 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.6-3m)
- add patch5 for jpeg7

* Mon Aug 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.16.6-2m)
- rebuild against libjpeg-7

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.6-1m)
- update to 2.16.6
- delete patch30

* Sun Aug  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-3m)
- add Patch30 (no error patch, so remove please)
- add BPR: automake17 (http://pc11.2ch.net/test/read.cgi/linux/1188293074/609)

* Sat Aug  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-2m)
- add BPR:jasper-devel

* Sun Jul 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-1m)
- update to 2.16.5

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.4-1m)
- update to 2.16.4

* Mon Jun 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3
- delete patch0 (merged)

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16.2-2m)
- add workaround for anaconda
-- disable gio mime

* Sun May 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2
- comment out patch20

* Sun Apr 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sat Mar 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0
- delete patch30 (merged)

* Thu Mar 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-2m)
- fix search button (Patch30)
-- http://bugzilla.gnome.org/show_bug.cgi?id=574059

* Tue Mar  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15.5-1m)
- update tp 2.15.5

* Fri Feb 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.15.4-1m)
- update tp 2.15.4

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.3-1m)
- update tp 2.15.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.7-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.7-1m)
- update to 2.14.7

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.6-2m)
- update Patch0,1 for fuzz=0
- License: LGPLv2+

* Tue Dec 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.6-1m)
- update to 2.14.6
-- add configure option --with-included-loaders=png 

* Sat Nov 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-2m)
- back to 2.14.4
- /etc/gtk-2.0/gdk-pixbuf.loaders was broken

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.5-1m)
- update to 2.14.5

* Sat Oct 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.4

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Mon Sep 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Mon Sep 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.12-1m)
- update to 2.12.12

* Mon Sep 15 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.11-6m)
- rollback to 2.12.11

* Sun Sep  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14.1-1m)
- update 2.14.1
- Obso & Provide gail, gail-devel

* Thu Sep  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.11-5m)
- mv libgdk-x11-2.0.so from devel to main (for last-exit)

* Wed Aug 20  2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.11-4m)
- good-bye gtk2-common

* Wed Aug 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.11-3m)
- revise %%files to avoid conflicting
- gtk2-common should be merged into gtk2 to resolve dependencies of many packages

* Tue Aug 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.11-2m)
- mv libgdk-x11-2.0.so.* and libgtk-x11-2.0.so.* from gtk2 to gtk2-common

* Wed Jul  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.11-1m)
- update to 2.12.11

* Wed Jun  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.10-1m)
- update to 2.12.10

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.9-3m)
- rebuild against gcc43

* Mon Mar 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.9-2m)
- gtk2-devel need glib2-devel

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.9-1m)
- update to 2.12.9

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.8-1m)
- update to 2.12.8

* Wed Jan 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.7-1m)
- update to 2.12.7

* Tue Jan 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.6-1m)
- update to 2.12.6

* Wed Jan  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.5-1m)
- update to 2.12.5

* Wed Dec  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Thu Nov  8 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.1-2m)
- remove gtk+2.12.0-autotest.patch (avoid autoreconf)
- add Patch3: system-log-crash.patch
-     Patch4: firefox-print-preview.patch

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Fri Sep 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0
- add patch40 (do not build tests dir), i want to remove this patch.

* Tue Sep 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.14-5m)
- fix konqueror lockup issue (with flash)
- http://bugzilla.gnome.org/show_bug.cgi?id=463773

* Mon Sep 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.14-4m)
- Fix BuildRequires

* Sun Sep  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.14-3m)
- add any Requires

* Mon Aug 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.14-2m)
- import patch13 from Fedora (cups)

* Sun Jul 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.14-1m)
- version 2.10.14 (bug-fix release)

* Mon Jun 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.13-2m)
- unhold directory /usr/share/theme

* Thu Jun 14 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.13-1m)
- version 2.10.13 (bug-fix release)

* Tue Jun 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.12-2m)
- add patch11 (icon-theme.cache)
-- from http://svn.gnome.org/viewcvs/gtk%2B/trunk/gtk/gtkiconcachevalidator.c?r1=17767&r2=17982&view=patch

* Thu May  3 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.12-1m)
- version 2.10.12 (bug-fix release)

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.11-3m)
- set --enable-gtk-doc again

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.11-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3
- --disable-gtk-doc for the moment

* Thu Mar 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.11-1m)
- version 2.10.11 (bug-fix release)

* Tue Feb  6 2007 zunda  <zunda at freeshell.org>
- (kossori)
- added BUildPrereq: gamin-devel for fam.h

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.9-2m)
- rename gtk+ -> gtk2

* Tue Jan 23 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.9-1m)
- version 2.10.9 (bug-fix release)

* Sat Jan 20 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.8-2m)
- add patches from FC

* Fri Jan 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.8-1m)
- version 2.10.8 (bug-fix release)

* Mon Jan  1 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.10.6-2m)
- enable ppc64.

* Thu Oct  5 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.6-1m)
- version 2.10.6 (bug-fix release)

* Sun Sep 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-1m)
- update to 2.10.4
-- add patch2 from FC

* Thu Sep 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-2m)
- add patch 9,10 from FC

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3
- delete libtool library

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.8.20-3m)
- rebuild against expat-2.0.0-1m

* Wed Jul 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.20-2m)
- /usr/lib/libgdk-x11-2.0.so to gtk+
-- youtranslate requires this file.

* Mon Jul  3 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.20-1m)
- version 2.8.20 (bug-fix release)

* Thu Jun 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.19-1m)
- version 2.8.19 (bug-fix release)

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.18-2m)
- add %%exclude %%dir %%{_libdir}/gtk-2.0/include at %%files comon

* Sat May 27 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.18-1m)
- version 2.8.18 (bug-fix release)

* Sun Apr  9 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.17-1m)
- version 2.8.17 (bug-fix release)

* Thu Mar 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.16-1m)
- version 2.8.16 (bug-fix release)

* Wed Mar 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.15-1m)
- version 2.8.15 (bug-fix release)

* Sun Feb 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.13-1m)
- version 2.8.13 (bug-fix release)

* Sun Jan 29 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.11-1m)
- version 2.8.11 (bug-fix release)

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.9-1m)
- version 2.8.9 (bug-fix release)

* Mon Dec 5 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.8-2m)
- required Patch0: gtk+-2.8.3-gdk-2.0_pc-pango.patch by firefox-1.5

* Sat Dec 3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.8-1m)
- version up 2.8.8
- GNOME 2.12.2 Platform

* Wed Nov 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.7-1m)
- version 2.8.7 (bug-fix release)

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.6-5m)
- comment out unnessesaly autoreconf and make check

* Fri Nov 18 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.6-4m)
- enable fbmanager gtk-doc man

* Tue Nov 15 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.6-3m)
- rebuild against cairo-1.0.2

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.6-2m)
- add autoreconf
- GNOME 2.12.1 Desktop

* Thu Oct  6 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.6-1m)
- version 2.8.6 (bug-fix release)
- revised spec file

* Wed Oct  5 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.4-1m)
- version 2.8.4 (bug-fix release)

* Thu Sep 22 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.3-2m)
- add Patch0 gtk+-2.8.3-gdk-2.0_pc-pango.patch

* Sat Sep 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.3-1m)
- update 2.8.3

* Tue Aug 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.10-1m)
- version 2.6.10 (bug-fix release)

* Thu Aug  4 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.9-1m)
- version 2.6.9 (bug-fix release)

* Fri Jul  1 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.8-1m)
- version 2.6.8 (bug-fix release)

* Wed May  4 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.7-2m)
- Cancel deletion of the conflict file(gtk-engines)
- reported by jkuchi [Momonga-devel.ja:03095]

* Thu Apr 14 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.7-1m)
- reverts two problematic changes which were made in GTK+ 2.6.5.

* Mon Apr 11 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.6-1m)
- bugfix release to fix problems with stock images in GTK+ 2.6.5.

* Sun Apr 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.5-1m)
- version 2.6.5 (bugfix release)

* Sat Mar 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.4-1m)
- version 2.6.4 (bugfix release)

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.1-3m)
- remove 'Requires: DirectFB-devel' in gtk+-devel sub package

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.1-2m)
- enable x86_64.

* Mon Jan 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1

* Mon Oct 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.13-1m)
- version 2.4.13

* Sun Aug 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.7-2m)
- add noreplace at %%{_sysconfdir}/gtk-2.0/gtk.immodules

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.7-1m)
- version 2.4.7

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.6-1m)
- version 2.4.6

* Thu Jun 17 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.3-1m)
- version 2.4.3

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.0-3m)
- adjustment BuildPreReq

* Tue Apr 13 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.4.0-2m)
- corrected BuildPreReq

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0
- GNOME 2.6 Desktop
- remove patch0
- use aclocal-1.7, automake-1.7

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2.4-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.2.4-1m)
- version 2.2.4
- obsolete patch1

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2-2m)
- apply imcontextxim patch for Infinite loop (baatari-).

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2-1m)
- version 2.2.2

* Wed Apr  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-5m)
- change handle key_press order.

* Sun Apr 06 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-4m)
- rebuild against zlib 1.1.4-5m

* Fri Mar  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-3m)
- rebuild against for XFree86-4.3.0

* Wed Mar 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-2m)
- fix URL
- delete doc TODO

* Tue Feb 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Wed Dec  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Sat Oct 19 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-4m)
- fix filelist

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-3m)
- add gtkimcontextxim.patch (see bugzilla.gnome.org#87482)

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-2m)
- fixup file list

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-2m)
- use old gtkimcontextxim

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0
- wait wait for DirectFB..

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-3m)
- update gdk-directfb to 2002-08-26

* Mon Aug 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-2m)
- support DirectFB

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-1m)
- version 2.0.6

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-4m)
- uuppddaattee xxiimm mmoodduullee ppaattcchh

* Mon Jul 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-3m)
- add gtk+-2.0.5-xim_keycode.patch ("<")

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.5-2k)
- version 2.0.5

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.4-2k)
- version 2.0.4

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.3-2k)
- version 2.0.3

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.2-4k)
- rebuild against libpng-1.2.2.

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.2-2k)
- version 2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0.rc1-4k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0.rc1-2k)
- version 2.0.0.rc1
- change name to gtk+

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.15-4k)
- modify dependency list

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.15-2k)
- version 1.3.15

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.14-2k)
- version 1.3.14

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-10k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-8k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-6k)
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-2k)
- version 1.3.13
- rebuild against for pango-0.24
- rebuild against for glib-1.3.13
- rebuild against for atk-0.10

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (1.3.12-2k)
- version 1.3.12

* Mon Jul 16 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.3.6

* Thu Apr 26 2001 Shingo Akagaki <dora@kondara.org>
- fork from gtk+ spec
