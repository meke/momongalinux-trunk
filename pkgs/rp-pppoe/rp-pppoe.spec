%global momorel 1

Summary: A PPP over Ethernet client (for xDSL support)
Name: rp-pppoe
Version: 3.11
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://www.roaringpenguin.com/pppoe/
Source0: http://www.roaringpenguin.com/files/download/rp-pppoe-%{version}.tar.gz
NoSource: 0
Source1: pppoe-connect
Source2: pppoe-setup
Source3: pppoe-start
Source4: pppoe-status
Source5: pppoe-stop
Source6: pppoe-server.service

Patch0: rp-pppoe-3.8-redhat.patch
Patch1: rp-pppoe-3.11-ip-allocation.patch

Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(post): chkconfig fileutils
Requires(postun): chkconfig fileutils
Requires: ppp >= 2.4.2
Requires: initscripts >= 5.92
Requires: iproute >= 2.6
Requires: mktemp
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(post): systemd-sysv

BuildRequires: libtool
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: coreutils
BuildRequires: ppp

%description
PPPoE (Point-to-Point Protocol over Ethernet) is a protocol used by
many ADSL Internet Service Providers. This package contains the
Roaring Penguin PPPoE client, a user-mode program that does not
require any kernel modifications. It is fully compliant with RFC 2516,
the official PPPoE specification.

%prep
%setup -q
%patch0 -p1 -b .config
%patch1 -p1 -b .ip-allocation

%build
cd src
autoconf
export CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE -fno-strict-aliasing"
%configure
make

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_sbindir} %{buildroot}%{_unitdir}

make -C src install DESTDIR=%{buildroot}

install -m 0755 %{SOURCE1} %{buildroot}%{_sbindir}
install -m 0755 %{SOURCE2} %{buildroot}%{_sbindir}
install -m 0755 %{SOURCE3} %{buildroot}%{_sbindir}
install -m 0755 %{SOURCE4} %{buildroot}%{_sbindir}
install -m 0755 %{SOURCE5} %{buildroot}%{_sbindir}
install -m 0755 %{SOURCE6} %{buildroot}%{_unitdir}/pppoe-server.service

ln -sf pppoe-stop %{buildroot}%{_sbindir}/adsl-stop
ln -sf pppoe-start %{buildroot}%{_sbindir}/adsl-start

rm -rf %{buildroot}/etc/ppp/pppoe.conf \
       %{buildroot}/etc/rc.d/init.d/pppoe \
       %{buildroot}/usr/doc \
       %{buildroot}%{_sysconfdir}/ppp/plugins

%clean
rm -rf %{buildroot}

%preun
if [ "$1" = "0" ]; then
   # Package removal, not upgrade
   /bin/systemctl disable pppoe-server.service > /dev/null 2>&1 || :
   /bin/systemctl stop pppoe-server.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart pppoe-server.service >/dev/null 2>&1 || :
fi

triggerun -- %{name} < 3.10
# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del pppoe-server >/dev/null 2>&1 || :
/bin/systemctl try-restart pppoe-server.service >/dev/null 2>&1 || :


%files
%defattr(-,root,root)
%doc doc/LICENSE scripts/pppoe-connect scripts/pppoe-setup scripts/pppoe-init
%doc scripts/pppoe-start scripts/pppoe-status scripts/pppoe-stop
%doc configs
%config(noreplace) %{_sysconfdir}/ppp/pppoe-server-options
%config(noreplace) %{_sysconfdir}/ppp/firewall*
%{_unitdir}/pppoe-server.service
%{_sbindir}/*
%{_mandir}/man?/*

%changelog
* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.11-1m)
- update 3.11

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10-5m)
- support systemd
- remove old triggerun

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.10-1m)
- sync with Rawhide (3.10-7)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8-6m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8-2m)
- %%NoSource -> NoSource

* Sat Apr 14 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.8-1m)
- up to 3.8
- sync to Fedora devel

* Fri Jun  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5-5m)
- import patch from FC2
- obsolete rp-pppoe-gui

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (3.5-4m)
- revised spec for rpm 4.2.

* Mon Sep 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.5-3m)
- rebuild against ppp-2.4.2

* Fri Feb 21 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.5-2m)
  fix build error

* Wed Oct 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.5-1m)
- build against ppp-2.4.1-17m

* Sun Apr 14 2002 Masahiro Takahata <takahata@kondara.org>
- (3.3-10k)
- modified adsl-setup script ('s/hoge/fuga/' -> 's|hoge|fuga|')

* Fri Apr 12 2002 Shingo Akagaki <dora@kondara.org>
- (3.3-8k)
- kernel-mode support in adsl-setup command

* Thu Apr 11 2002 Shingo Akagaki <dora@kondara.org>
- (3.3-6k)
- add kernel-mode plugin

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (3.3-4k)
- nigittenu

* Fri Jun  8 2001 Shingo Akagaki <dora@kondara.org>
- (3.0-6k)
- add BuildPrereq: ppp tag

* Wed May  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0-4k)
- errased PreReq: libtool tag

* Thu Apr 26 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0-2k)
- update to 3.0

* Sat Feb 10 2001 Shinya Tomobe <betiz@bigfoot.com>
- update to 2.8 .

* Sat Feb 05 2001 Shinya Tomobe <betiz@bigfoot.com>
- changed spec file for kondara.

