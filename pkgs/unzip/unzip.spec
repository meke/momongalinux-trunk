%global momorel 6

Summary: unpacks .zip files such as those made by pkzip under DOS
Name: unzip
Version: 6.0
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/Archiving
URL: http://www.info-zip.org/pub/infozip/UnZip.html
Source: http://dl.sourceforge.net/infozip/unzip60.tar.gz
NoSource: 0
Patch1: unzip-6.0-bzip2-configure.patch
Patch2: unzip-6.0-exec-shield.patch
Patch3: unzip-6.0-close.patch
Patch4: unzip-6.0-attribs-overflow.patch
Patch5: unzip-6.0-nostrip.patch
Patch6: unzip-6.0-manpage-fix.patch
Patch7: unzip-6.0-fix-recmatch.patch
Patch8: unzip-6.0-symlink.patch
Patch10: unzip-6.0-japanese_charset.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The unzip utility is used to list, test, or extract files from a zip
archive.  Zip archives are commonly found on MS-DOS systems.  The zip
utility, included in the zip package, creates zip archives.  Zip and
unzip are both compatible with archives created by PKWARE(R)'s PKZIP
for MS-DOS, but the programs' options and default behaviors do differ
in some respects.

Install the unzip package if you need to list, test or extract files from
a zip archive.

%prep
%setup -q -n unzip60
%patch1 -p1 -b .bzip2-configure
%patch2 -p1 -b .exec-shield
%patch3 -p1 -b .close
%patch4 -p1 -b .attribs-overflow
%patch5 -p1 -b .nostrip
%patch6 -p1 -b .manpage-fix
%patch7 -p1 -b .recmatch

%patch10 -p1 -b .japanese

%build
make -f unix/Makefile CF_NOOPT="-I. -DUNIX %{optflags} -D_MBCS " generic_gcc %{?_smp_mflags}

%install
rm -rf %{buildroot}
make -f unix/Makefile \
     prefix=%{buildroot}%{_prefix} \
     MANDIR=%{buildroot}%{_mandir}/man1 \
     INSTALL="cp -p" install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BUGS History.600 LICENSE README ToDo WHERE
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Mar 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.0-6m)
- import fedora patches

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-5m)
- rebuild for new GCC 4.6

* Wed Jan 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.0-4m)
- add unzip-6.0-japanese_charset.patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.0-2m)
- full rebuild for mo7 release

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-1m)
- update to 6.0
- import patches from Rawhide (6.0-2)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.52-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.52-10m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.52-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.52-8m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.52-7m)
- [SECURITY] CVE-2008-0888
- import security patch from Fedora devel

* Tue Jul 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.52-6m)
- import unzip-5.52-ubuntuja.patch from ebuildJP
- http://ebuild.gentoo.gr.jp/view.php?cat=app-arch&app=unzip&visible=

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.52-5m)
- fix 2GB limit problem in ix86
- import patch from FC unzip-5.52-2.2.1
- renumber patch1->patch11

* Wed Apr 26 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.52-4m)
- update patch1 from Turbo Linux
- * Tue Apr 25 2006 Makoto Dei <makoto@turbolinux.co.jp> - 5.50-13
- - fixed the bug that files with Japanese file names can not be extracted
  -  when their file names are specified in the command line.
- sync patch numbering with TL
- add -b <name> at %%setup
- note: Patch3: unzip-5.52-toctou.patch is patch for CAN-2005-2475

* Wed Aug 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.52-3m)
- [SECURITY] unzip TOCTOU file-permissions vulnerability
  http://www.st.ryukoku.ac.jp/~kjm/security/ml-archive/bugtraq/2005.08/msg00023.html

* Tue Aug 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.52-2m)
- for japanese file name support(from TurboLinux)

* Sat Mar 12 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (5.52-1m)
  update to 5.52

* Wed Jun  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (5.51-1m)
- verup

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (5.50-5m)
- revised spec for rpm 4.2.

* Mon Jul 14 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.50-4m)
- fix trojan vulnerability
- http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2003-0282

* Mon Oct 7 2002 Nguyen Hung Vu <vuhung@momonga-linux.org>
- (5.50-3m)
- Changed the k prefix to m.

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (5.50-2k)
- version up.

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (5.42-2k)
- version up.

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (5.41-4k)
- errased unzip.fhs.patch and modified spec file for compatibility

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sat Apr 29 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( description )
- Added %{optflags}

* Mon Apr 24 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- version 5.41

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (5.40-2).

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Jul 30 1999 Bill Nottingham <notting@redhat.com>
- update to 5.40

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Thu Dec 17 1998 Michael Maher <mike@redhat.com>
- built for 6.0

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Oct 21 1997 Erik Troan <ewt@redhat.com>
- builds on non i386 platforms

* Mon Oct 20 1997 Otto Hammersmith <otto@redhat.com>
- updated the version

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
