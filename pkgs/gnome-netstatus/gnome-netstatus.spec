%global momorel 3

Summary: gnome-netstatus
Name: gnome-netstatus
Version: 2.28.2
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.28/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: libgnomeui-devel >= 2.24.0
BuildRequires: libglade2-devel >= 2.6.3
BuildRequires: gnome-panel-devel >= 2.14.0
BuildRequires: gtk2-devel >= 2.14.3
BuildRequires: openssl-devel >= 0.9.8a
Requires(pre): hicolor-icon-theme gtk2
Requires(pre):          GConf2
Requires(post):         GConf2
Requires(preun):        GConf2

%description
gnome-netstatus contains an applet which provides information
about a network interface on your panel.
                                                                                
%prep
%setup -q

%build
%configure --disable-static --disable-schemas-install --disable-scrollkeeper \
  "LIBS=-lm"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/netstatus.schemas \
    > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/netstatus.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/netstatus.schemas \
	> /dev/null || :
fi

%postun
rarian-sk-update

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog NEWS README
%{_sysconfdir}/gconf/schemas/netstatus.schemas
%{_libdir}/bonobo/servers/GNOME_NetstatusApplet_Factory.server
%{_datadir}/gnome-2.0/ui/GNOME_NetstatusApplet.xml
%{_datadir}/gnome-netstatus
%{_libexecdir}/gnome-netstatus-applet
%{_datadir}/locale/*/*/*
%{_datadir}/gnome/help/gnome-netstatus/*/*
%{_datadir}/omf/gnome-netstatus/*
%{_datadir}/icons/hicolor/48x48/apps/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.28.1-6m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.28.1-5m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.28.1-4m)
- add Requires: GConf2

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-3m)
- build fix

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun Mar  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.2-2m)
- rebuild against rpm-4.6

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.1-2m)
- rebuild against gcc43

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.12.0-4m)
- rebuild against expat-2.0.0-1m

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-3m)
- rebuild against openssl-0.9.8a

* Mon Feb 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-2m)
- fix conflict directories

* Mon Nov 21 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.0-1m)
- version 2.8.0
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (2.6.0.1-2m)
- revised spec for rpm 4.2.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0.1-1m)
- version 2.6.0.1
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.13-2m)
- revised spec for enabling rpm 4.2.

* Tue Jan 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.13-1m)
- version 0.13

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-2m)
- s/Copyright:/License:/

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11-1m)
- version 0.11

* Fri Jun 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10-1m)
- version 0.10
