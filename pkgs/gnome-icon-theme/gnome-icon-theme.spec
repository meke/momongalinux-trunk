%global momorel 1
Summary: GNOME icon theme
Name: gnome-icon-theme
Version: 3.6.0
Release: %{momorel}m%{?dist}
#VCS: git:git://git.gnome.org/gnome-icon-theme
Source0: http://download.gnome.org/sources/gnome-icon-theme/3.6/%{name}-%{version}.tar.xz
NoSource: 0
Source1: legacy-icon-mapping.xml
License: GPL+
BuildArch: noarch
Group: User Interface/Desktops
BuildRequires: icon-naming-utils >= 0.8.90
BuildRequires: gettext
BuildRequires: librsvg2
BuildRequires: intltool
BuildRequires: gtk2
Requires: hicolor-icon-theme
Requires: pkgconfig

%description
This package contains the default icon theme used by the GNOME desktop.

%package legacy
Summary: Old names for icons in gnome-icon-theme
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}

%description legacy
This package contains symlinks to make the icons in gnome-icon-theme
available under old names.

%prep
%setup -q

%build
%configure --enable-icon-mapping

%install
make install DESTDIR=%{buildroot}

rm %{buildroot}%{_datadir}/icons/gnome/icon-theme.cache
touch %{buildroot}%{_datadir}/icons/gnome/icon-theme.cache

cp %{SOURCE1} .
export INU_DATA_DIR=$PWD
(cd %{buildroot}%{_datadir}/icons/gnome
for size in 8x8 16x16 22x22 24x24 32x32 48x48 256x256; do
        cd $size || continue;
        echo -e "Adding rtl variants for $size"
        for dir in `find . -type d`; do
                context="`echo $dir | cut -c 3-`"
                if [ $context ]; then
                        icon-name-mapping -c $context
                fi
        done
        cd ..
done
)

# Add scalable directories for symbolic icons
(cd %{buildroot}%{_datadir}/icons/gnome

mkdir -p scalable/actions
mkdir -p scalable/apps
mkdir -p scalable/devices
mkdir -p scalable/emblems
mkdir -p scalable/mimetypes
mkdir -p scalable/places
mkdir -p scalable/status
)

touch files.txt

(cd %{buildroot}%{_datadir}
 echo "%%defattr(-,root,root)"
 find icons/gnome \( -name *-rtl.png -or -name *-ltr.png -or -type f \) -printf "%%%%{_datadir}/%%p\n"
 find icons/gnome -type d -printf "%%%%dir %%%%{_datadir}/%%p\n"
) > files.txt

(cd %{buildroot}%{_datadir}
 echo "%%defattr(-,root,root)"
 find icons/gnome \( -type l -and -not -name *-rtl.png -and -not -name *-ltr.png \) -printf "%%%%{_datadir}/%%p\n"
) > legacy.txt

%post
touch --no-create %{_datadir}/icons/gnome &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/gnome &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :

%post legacy
touch --no-create %{_datadir}/icons/gnome &>/dev/null || :

%postun legacy
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/gnome &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
fi

%posttrans legacy
gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :

%files -f files.txt
%doc COPYING AUTHORS
%{_datadir}/pkgconfig/gnome-icon-theme.pc
%ghost %{_datadir}/icons/gnome/icon-theme.cache

%files legacy -f legacy.txt

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-1m)
- reimport from fedora

* Thu Oct 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1.2-1m)
- update to 3.2.1.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1.1-1m)
- update to 3.2.1.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.0-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.0-2m)
- delete icons avoid conflicts

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.0-1m)
- update to 2.31.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.3-2m)
- full rebuild for mo7 release

* Thu May 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Tue May 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.30.2.1-2m)
- update lost icons
- %%post script is still failing

* Fri Apr 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2.1-1m)
- update to 2.30.2.1

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Wed Apr 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- delete conflict icons

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- delete conflict icons

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-2m)
-- comment out legacy icon hack
-- need more Hack !!!

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.25.92-2m)
- revive many important lost icons (using Fedora's hack)

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Wed Feb 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-2m)
- add touch $dir in %%post script

* Thu Feb 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-3m)
- modify %%install to avoid conflicting with GNOME

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-2m)
- modify %%install to avoid conflicting with KDE

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.0-2m)
- modify %%install to avoid conflicting with KDE and XFCE4

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0.1-2m)
- specify automake version 1.9

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0.1-1m)
- update to 2.16.0.1

* Mon Aug 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-7m)
- clean up spec

* Wed Aug  2 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.1-6m)
- add PreReq: gtk+-common

* Sat Jul  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-5m)
- delete duplicate files

* Sat Jul 01 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.14.1-4m)
- copy /usr/share/icons/gnome to /usr/share/icons/hicolor to fix the issue that some icons of gnome apps are not displayed in KDE, fluxbox, twm etc.

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-3m)
- delete duplicated dir

* Sat Apr  8 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2-2m)
- add web-browser.png (Source1)
- add BuildRequires: icon-naming-utils
- add patch0

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Sun Feb 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-4m)
- fix conflict files

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-3m)
- comment out autoreconf

* Sun Nov 20 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.1-2m)
- enable x86_64

* Thu Nov 17 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Sat Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.0-1m)
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (1.2.3-1m)
- version 1.2.3

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-1m)
- version 1.2.1

* Sun Apr 18 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-3m)
- add --disable-hicolor-check at configure
  for not check index.theme, becase there is no need check
  /usr/share/icons/gnome/index.theme
  that file is provide by kdelibs-3.2.1-3m

* Fri Apr 16 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.0-2m)
- change %%{_datadir}/icons/* (add icons/hicolor directory)

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.0-1m)
- version 1.2.0
- GNOME 2.6 Desktop
- add %%{_libdir}/pkgconfig/*.pc

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.9-2m)
- s/Copyright:/License:/

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.9-1m)
- version 1.0.9

* Thu Jul 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.6-1m)
- version 1.0.6

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.5-1m)
- version 1.0.5

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.4-1m)
- version 1.0.4

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.3-1m)
- version 1.0.3

* Tue Apr 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.2-1m)
- version 1.0.2

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- version 1.0.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.0-1m)
- version 1.0.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.91-1m)
- version 0.91

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.5-1m)
- version 0.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.4-1m)
- version 0.1.4

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.3-1m)
- version 0.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.1-1m)
- version 0.1.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.0-1m)
- import
