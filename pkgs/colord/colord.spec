%global    momorel 1

Summary:   Color daemon
Name:      colord
Version:   0.1.34
Release:   %{momorel}m%{?dist}
License:   GPLv2+ and LGPLv2+
Group:     Development/Libraries
URL:       http://www.freedesktop.org/software/colord/
Source0:   http://www.freedesktop.org/software/colord/releases/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires: dbus-devel
BuildRequires: docbook-utils
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: intltool
BuildRequires: lcms2-devel >= 2.2
BuildRequires: libgudev1-devel
BuildRequires: polkit-devel >= 0.103
BuildRequires: sane-backends-devel
BuildRequires: sqlite-devel
BuildRequires: gobject-introspection-devel
BuildRequires: vala-tools
BuildRequires: libgusb-devel
BuildRequires: gtk-doc

Requires: shared-color-profiles
Requires: color-filesystem
Requires: systemd-units
Requires(pre): glib2
Requires(post): shadow-utils
Requires(postun): glib2
%systemd_requires

%description
colord is a low level system activated daemon that maps color devices
to color profiles in the system context.

%package devel
Summary: Development package for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
Files for development with %{name}.

%package devel-docs
Summary: Developer documentation package for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description devel-docs
Documentation for development with %{name}.

%package gtk
Summary: GTK helper library for %{name}
Requires: %{name} = %{version}-%{release}

%description gtk
This package contains extra functionality for %{name} that can be used
when running GTK applications.

%prep
%setup -q

%build
%configure \
        --with-daemon-user=colord \
        --enable-gtk-doc \
        --enable-vala \
        --disable-static \
        --disable-rpath \
        --disable-examples \
        --disable-dependency-tracking

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Remove static libs and libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'
find %{buildroot} -name '*.a' -exec rm -f {} ';'

# databases
touch %{buildroot}%{_localstatedir}/lib/%{name}/mapping.db
touch %{buildroot}%{_localstatedir}/lib/%{name}/storage.db

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || \
    useradd -r -g %{name} -d %{_localstatedir}/l/lib/%{name} -s /sbin/nologin \
    -c "User for %{name}" %{name}
exit 0

%post
/sbin/ldconfig
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
/sbin/ldconfig
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
%systemd_postun %{name}.service

%files
%defattr(-, root, root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog INSTALL MAINTAINERS NEWS README TODO
%{_sysconfdir}/dbus-1/system.d/org.freedesktop.ColorManager.conf
%config %{_sysconfdir}/%{name}.conf
/lib/systemd/system/%{name}.service
/lib/udev/rules.d/69-cd-sensors.rules
/lib/udev/rules.d/95-cd-devices.rules
%{_bindir}/cd-create-profile
%{_bindir}/cd-fix-profile
%{_bindir}/cd-iccdump
%{_bindir}/colormgr
%{_libdir}/%{name}-plugins
%{_libdir}/%{name}-sensors
%{_libdir}/girepository-1.0/ColorHug-1.0.typelib
%{_libdir}/girepository-1.0/Colord-1.0.typelib
%{_libdir}/lib%{name}.so.*
%{_libdir}/lib%{name}private.so.*
%{_libdir}/libcolorhug.so.*
%{_libexecdir}/%{name}
%{_libexecdir}/%{name}-session
%{_datadir}/bash-completion/completions/colormgr
%{_datadir}/color/icc/%{name}
%{_datadir}/%{name}
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorHelper.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorManager.Device.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorManager.Profile.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorManager.Sensor.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorManager.xml
%{_datadir}/dbus-1/services/org.freedesktop.ColorHelper.service
%{_datadir}/dbus-1/system-services/org.freedesktop.ColorManager.service
%{_datadir}/glib-2.0/schemas/org.freedesktop.ColorHelper.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/cd-create-profile.1*
%{_mandir}/man1/cd-fix-profile.1*
%{_mandir}/man1/colormgr.1*
%{_datadir}/polkit-1/actions/org.freedesktop.color.policy
%attr(755,colord,colord) %dir %{_localstatedir}/lib/%{name}
%attr(755,colord,colord) %dir %{_localstatedir}/lib/%{name}/icc
%verify(not size md5 mtime) %attr(-,colord,colord) %{_localstatedir}/lib/%{name}/*.db

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}-1
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/pkgconfig/colorhug.pc
%{_libdir}/lib%{name}.so
%{_libdir}/lib%{name}private.so
%{_libdir}/libcolorhug.so
%{_datadir}/gir-1.0/ColorHug-1.0.gir
%{_datadir}/gir-1.0/Colord-1.0.gir
%{_datadir}/vala/vapi/%{name}.vapi

%files devel-docs
%defattr(-, root, root)
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Mon May 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.34-1m)
- version 0.1.34
- add some Requires
- set --enable-vala to %%configure
- modify %%install
- add %%clean section
- modify %%pre, %%post, %%preun and %%postun
- rewrite %%files

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.22-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.15-2m)
- rebuild for glib 2.33.2

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.15-1m)
- [SECURITY] CVE-2011-4349
- update to 0.1.15

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.14-1m)
- update to 0.1.14

* Wed Oct  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.13-1m)
- update to 0.1.13

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.12-1m)
- update to 0.1.12

* Tue Sep 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.10-2m)
- fix build failure on x86_64

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.10-1m)
- initial build
