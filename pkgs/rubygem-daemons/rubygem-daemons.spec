# Generated from daemons-1.1.8.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname daemons

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: A toolkit to create and control daemons in different ways
Name: rubygem-%{gemname}
Version: 1.1.8
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://daemons.rubyforge.org
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Daemons provides an easy way to wrap existing ruby scripts (for example a
self-written server)  to be run as a daemon and to be controlled by simple
start/stop/restart commands.  You can also call blocks as daemons and control
them from the parent or just daemonize the current process.  Besides this
basic functionality, daemons offers many advanced features like exception 
backtracing and logging (in case your ruby script crashes) and monitoring and
automatic restarting of your processes if they crash.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README
%doc %{geminstdir}/Releases
%doc %{geminstdir}/TODO
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.8-1m)
- update 1.1.8

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-1m)
- update 1.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.10-1m)
- update 1.0.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.9-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.9-1m)
- Initial package for Momonga Linux
