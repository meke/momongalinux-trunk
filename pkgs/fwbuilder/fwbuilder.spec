%global         momorel 1

%global         qtver 4.8.0
%global         qtdir %{_libdir}/qt4
%global         rel 3599

Summary:        Firewall Builder
Name:           fwbuilder
Version:        5.1.0
Release:        %{momorel}m%{?dist}
License:        GPLv2
Group:          Applications/System
Url:            http://www.fwbuilder.org/
Source0:        http://dl.sourceforge.net/project/%{name}/Current_Packages/%{version}/%{name}-%{version}.%{rel}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libxml2-devel
BuildRequires:  libxslt-devel
BuildRequires:  net-snmp-devel >= 5.7.1
BuildRequires:  openssl-devel
BuildRequires:  qt-devel >= %{qtver}
Obsoletes:      libfwbuilder
Obsoletes:      libfwbuilder-devel

%description
Firewall Builder consists of a GUI and set of policy compilers for
various firewall platforms. It helps users maintain a database of
objects and allows policy editing using simple drag-and-drop
operations. GUI generates firewall description in the form of XML
file, which compilers then interpret and generate platform-specific
code. Several algorithms are provided for automated network objects
discovery and bulk import of data. The GUI and policy compilers are
completely independent, this provides for a consistent abstract model
and the same GUI for different firewall platforms. 

%prep
%setup -q -n %{name}-%{version}.%{rel}

%build
case "`gcc -dumpversion`" in
4.4.*)
CXXFLAGS="`echo %{optflags} | sed -e 's/-O2/-O0/'`"
;;
esac
./autogen.sh --with-qmake=qmake-qt4

%configure --with-qmake=qmake-qt4
make all %{?_smp_mflags}

%install
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}
make INSTALL_ROOT="%{buildroot}/" install
rm -fr %{buildroot}/usr/share/doc/%{name}-%{version}.%{rel}

%clean
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc doc/AUTHORS doc/COPYING doc/Credits doc/ChangeLog doc/PatchAcceptancePolicy.txt
%doc doc/README.* doc/FWBuilder-Routing-LICENSE.txt
%dir %{_datadir}/%{name}-%{version}.%{rel}
%{_bindir}/fwbuilder
%{_bindir}/fwbedit
%{_bindir}/fwb_iosacl
%{_bindir}/fwb_ipf
%{_bindir}/fwb_ipfw
%{_bindir}/fwb_ipt
%{_bindir}/fwb_pf
%{_bindir}/fwb_pix
%{_bindir}/fwb_procurve_acl
%{_mandir}/man1/fwbuilder.1*
%{_mandir}/man1/fwbedit.1*
%{_mandir}/man1/fwb_iosacl.1*
%{_mandir}/man1/fwb_ipf.1*
%{_mandir}/man1/fwb_ipfw.1*
%{_mandir}/man1/fwb_ipt.1*
%{_mandir}/man1/fwb_pf.1*
%{_mandir}/man1/fwb_pix.1*
%{_datadir}/%{name}-%{version}.%{rel}/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png

%changelog
* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.0-1m)
- update to 5.1.0

* Sat Dec 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.1-1m)
- update to 5.0.1

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.0-2m)
- rebuild against net-snmp-5.7.1

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.0-1m)
- update to 5.0.0

* Sun Jul 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- rebuild against net-snmp-5.7

* Fri May 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- libfwbuilder was merged into fwbuilder
- update to 4.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.3-3m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-2m)
- rebuild against net-snmp-5.6.1

* Tue Dec  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-1m)
- update to 4.1.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.2-2m)
- rebuild for new GCC 4.5

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.2-1m)
- update to 4.1.2

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.1-1m)
- update to 4.1.1
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.2-4m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.2-3m)
- update source

* Sat Jul 10 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.0.2-2m)
- update source

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-2m)
- rebuild against qt-4.6.3-1m

* Fri Jun  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Wed May 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-3m)
- fix build with qt-4.7.0

* Wed May  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-2m)
- source archive was replced, update sources

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to 4.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.7-4m)
- rebuild against net-snmp-5.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.7-2m)
- build with -O0 when use gcc44 x86_64 too

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.7-1m)
- [SECURITY] CVE-2009-4664
- update to 3.0.7

* Mon Sep 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.6-2m)
- build with -O0 when use gcc44 on i686 because g++ can not stop

* Sun Sep  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.6-1m)
- update to 3.0.6

* Sat Aug  8 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.0.5-2m)
- src sha256sum change.

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.5-1m)
- Initial build for Momonga Linux
