%global momorel 3
%global gitver cbaa666d4f21988164068a38ac915f8b4f3c4da3
%bcond_with multiuser

Summary: A screen manager that supports multiple logins on one terminal
Name: screen
Version: 4.1.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://www.gnu.org/software/screen
Requires(pre): shadow-utils
Requires(preun): /sbin/install-info
Requires(post): /sbin/install-info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel pam-devel libutempter-devel autoconf texinfo
BuildRequires: automake

#Source0: ftp://ftp.uni-erlangen.de/pub/utilities/screen/screen-%{version}.tar.gz
#NoSource: 0
# snapshot from git://git.savannah.gnu.org/screen.git
Source0: screen-%{gitver}.tar.xz
Source1: screen.pam
Source100: tab.dot.screenrc

Patch1: screen-4.0.3-libs.patch
Patch2: screen-4.0.3-screenrc.patch
Patch3: screen-ipv6.patch
Patch4: screen-cc.patch
Patch5: screen-E3.patch

%description
The screen utility allows you to have multiple logins on just one
terminal. Screen is useful for users who telnet into a machine or are
connected via a dumb terminal, but want to use more than just one
login.

Install the screen package if you need a screen manager that can
support multiple logins on one terminal.


%prep
%setup -q -n screen-%{gitver}/src

%patch1 -p1 -b .libs~
%patch2 -p1 -b .screenrc~
%patch3 -p2 -b .ipv6~
%patch4 -p2 -b .cc~
%patch5 -p2 -b .E3~

%build
./autogen.sh

%configure \
	--enable-pam \
	--enable-colors256 \
	--enable-rxvt_osc \
	--enable-use-locale \
	--enable-telnet \
	--with-pty-mode=0620 \
	--with-pty-group=$(getent group tty | cut -d : -f 3) \
	--with-sys-screenrc="%{_sysconfdir}/screenrc" \
	--with-socket-dir="%{_localstatedir}/run/screen"

# We would like to have braille support.
sed -i -e 's/.*#.*undef.*HAVE_BRAILLE.*/#define HAVE_BRAILLE 1/;' config.h

sed -i -e 's/\(\/usr\)\?\/local\/etc/\/etc/g;' doc/screen.{1,texinfo}

for i in doc/screen.texinfo; do
    iconv -f iso8859-1 -t utf-8 < $i > $i.utf8 && mv -f ${i}{.utf8,}
done

rm -f doc/screen.info*

# fails with %{?_smp_mflags}
make

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}
mv -f %{buildroot}%{_bindir}/screen{-%{version},}

mkdir -p %{buildroot}%{_sysconfdir}
install -m 0644 etc/etcscreenrc %{buildroot}%{_sysconfdir}/screenrc
cat etc/screenrc >> %{buildroot}%{_sysconfdir}/screenrc

# Better not forget to copy the pam file around
mkdir -p %{buildroot}%{_sysconfdir}/pam.d
install -p -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/pam.d/screen

# Create the socket dir
mkdir -p %{buildroot}%{_localstatedir}/run/screen

# And tell systemd to recreate it on start with tmpfs
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d
cat <<EOF > %{buildroot}%{_sysconfdir}/tmpfiles.d/screen.conf
# screen needs directory in /var/run
%if %{with multiuser}
d %{_localstatedir}/run/screen 0755 root root
%else
d %{_localstatedir}/run/screen 0775 root screen
%endif
EOF

# Remove files from the buildroot which we don't want packaged
rm -f %{buildroot}%{_infodir}/dir

# prepare configuration sample
mkdir -p %{buildroot}%{_datadir}/config-sample/screen
install -m 644 %{SOURCE100} %{buildroot}%{_datadir}/config-sample/screen/

%clean
rm -rf %{buildroot}

%pre
/usr/sbin/groupadd -g 84 -r -f screen
:

%post
/sbin/install-info %{_infodir}/screen.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/screen.info %{_infodir}/dir
fi
:

%files
%defattr(-,root,root,-)
%doc NEWS README doc/FAQ doc/README.DOTSCREEN COPYING
%{_mandir}/man1/screen.*
%{_infodir}/screen.info*
%{_datadir}/screen
%config(noreplace) %{_sysconfdir}/screenrc
%config(noreplace) %{_sysconfdir}/pam.d/screen
%config %{_datadir}/config-sample/screen/*
%{_sysconfdir}/tmpfiles.d/screen.conf
%if %{with multiuser}
%attr(4755,root,root) %{_bindir}/screen
%attr(755,root,root) %{_localstatedir}/run/screen
%else
%attr(2755,root,screen) %{_bindir}/screen
%attr(775,root,screen) %{_localstatedir}/run/screen
%endif

%changelog
* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1.0-3m)
- update to the latest snapshot

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- modify Requires

* Mon Aug 29 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.0-1m)
- update to the latest snapshot
- merge from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.3-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.3-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.3-11m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-10m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.3-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.3-8m)
- rebuild against rpm-4.6

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.3-7m)
- remove --entry option of install-info

* Fri Apr  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-6m)
- stropts.h was removed from glibc; don't #include it anymore

* Fri Apr  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.3-5m)
- revise BuildRequires:

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-4m)
- rebuild against gcc43

* Mon Jun 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.0.3-3m)
- rebuilt against pam-0.99.7.1-1m
- update screen.pam 

* Mon Feb 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-2m)
- use cvs source
- delete Patch4: screen-3.9.11-utf8-install.patch
- update Patch4: screen-4.0.3-utf8-install.patch
- comment out Patch7: screen-4.0.1-args.patch
- add Patch9: screen-4.0.3-terminfo-install.patch
- tmp comment out Patch9: screen-4.0.3-terminfo-install.patch

* Wed Oct 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-1m)
- [SECURITY] CVE-2006-4573
  GNU Screen UTF-8 Character Handling Vulnerabilities
- http://lists.gnu.org/archive/html/screen-users/2006-10/msg00028.html

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (4.0.2-2m)
- rebuild against libtermcap and ncurses

* Tue Mar  8 2005 Toru Hoshina <t@momonga-linux.org>
- (4.0.2-1m)
- sync with FC3(4.0.2-5).

* Sat Jul 31 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9.15-5m)
- update Source100: tab.dot.screenrc

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (3.9.15-4m)
- rebuild against ncurses 5.3.

* Tue Mar 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.9.15-3m)
- revise %%files

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (3.9.15-2m)
- revised spec for rpm 4.2.

* Fri Jun 13 2003 zunda <zunda at freeshell.org>
- (kossori)
- BuildPreReq texinfo, Thank you YAA.

* Sat Mar 22 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.9.15-1m)
  update to 3.9.15

* Fri Mar  7 2003 zunda <zunda at freeshell.org>
- (3.9.13-3m)
- some more sample configurations and doc files

* Thu Jan 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.9.13-2m)
- [security fix]
   DON'T USE 3.9.13-1m which includes local vulnerability

* Wed Jan 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9.13-1m)
- update to 3.9.13
- delete Patch8:  screen-3.9.11.encoding-patch
- adapt Patch11: screen-3.9.11.seteuid-patch to screen-3.9.13

* Thu Jul  4 2002 zunda <zunda@momonga-linux.org>
- (3.9.11-14m)
- pipe reopen race condition
  http://groups.yahoo.com/group/gnu-screen/message/1118

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.9.11-12k)

* Tue Apr 23 2002 zunda <zunda@kondara.org>
- (3.9.11-10k)
- SECURITY UPDATE: HAVE_BRAILLE disabled due to a local root exploit
  http://groups.yahoo.com/group/gnu-screen/message/981

* Wed Apr  3 2002 zunda <zunda@kondara.org>
- (3.9.11-8k)
- patch8-11 from http://www.dekaino.net/screen/

* Wed Mar 13 2002 TABUCHI Takaaki <tab@kondara.org>
- (3.9.11-6k)
- add tab.dot.screenrc at files section

* Thu Mar  7 2002 zunda <zunda@kondara.org>
- (3.9.11-4k)
- chmod 755 /tmp/screens

* Thu Mar  7 2002 zunda <zunda@kondara.org>
- (3.9.11-2k)
- source update
- added the URL

* Mon Mar  4 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.9.10-18k)
- fix a broken symlink of FAQ

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (3.9.10-16k)                      
- add BuildPreReq: autoconf >= 2.52-8k

* Mon Feb  4 2002 Shingo Akagaki <dora@kondara.org>
- (3.9.10-14k)
- autoconf 1.5

* Wed Jan 30 2002 TABUCHI Takaaki <tab@kondara.org>
- (3.9.10-12k)
- fix tab.dot.screenrc is not included

* Mon Dec 10 2001 TABUCHI Takaaki <tab@kondara.org>
- (3.9.10-10k)
- add tab.dot.screenrc as sample

* Mon Nov 12 2001 Motonobu Ichimura <famao@kondara.org>
- (3.9.10-8k)
- add %dir %{_datadir}/config-sample/screen

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (3.9.10-6k)
- add BuildRequires,Requires utempter

* Thu Oct 25 2001 TABUCHI Takaaki <tab@kondara.org>
- (3.9.10-4k)
- obsolete /etc/skel/, use /usr/share/config-sample
- add BuildRequires, mkdir
- use %{_datadir} macro

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (3.9.10-2k)
- merge from Jirai.

* Mon Sep 17 2001 Motonobu Ichimura <famao@kondara.org>
- (3.9.10-3k)
- up to 3.9.10

* Wed Oct 25 2000 Daiki Matsuda <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org>
- Kodaraized.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jun 10 2000 Bill Nottingham <notting@redhat.com>
- rebuild, FHS tweaks

* Sat May  6 2000 Bill Nottingham <notting@redhat.com>
- fix build for ia64

* Mon Apr  3 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild with new ncurses

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Tue Feb 15 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix MD5 password support (Bug #9463)

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Fri Dec 10 1999 Bill Nottingham <notting@redhat.com>
- update to 3.9.5

* Wed Oct 20 1999 Bill Nottingham <notting@redhat.com>
- you know, we weren't just patching in Unix98 pty support for fun.

* Wed Aug 18 1999 Bill Nottingham <notting@redhat.com>
- put screendir in ~

* Wed Aug 18 1999 Jeff Johnson <jbj@redhat.com>
- update to 3.9.4.

* Wed Jun 16 1999 Bill Nottingham <notting@redhat.com>
- force tty permissions/group

* Wed Jun 5 1999 Dale Lovelace <dale@redhat.com>
- permissions on /etc/skel/.screenrc to 644

* Mon Apr 26 1999 Bill Nottingham <notting@redhat.com>
- take out warning of directory permissions so root can still use screen

* Wed Apr 07 1999 Bill Nottingham <notting@redhat.com>
- take out warning of directory ownership so root can still use screen

* Wed Apr 07 1999 Erik Troan <ewt@redhat.com>
- patched in utempter support, turned off setuid bit

* Fri Mar 26 1999 Erik Troan <ewt@redhat.com>
- fixed unix98 pty support

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Thu Mar 11 1999 Bill Nottingham <notting@redhat.com>
- add patch for Unix98 pty support

* Mon Dec 28 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.7.6.

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- upgraded to 3.7.4

* Wed Oct 08 1997 Erik Troan <ewt@redhat.com>
- removed glibc 1.99 specific patch

* Tue Sep 23 1997 Erik Troan <ewt@redhat.com>
- added install-info support

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc
