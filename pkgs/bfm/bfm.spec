%global momorel 11

Summary: bubblefishymon -  DUCK CPU MEMSCREEN FISH TIME
Name: bfm
Version: 0.6.4
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://pigeond.net/bfm/
Source0: http://www.jnrowe.ukfsn.org/_downloads/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gkrellm

%description
bubblemon is a system load application. Its little window will show a little
duck swimming on the water. The water level represents the memory usage of
the system, the number of bubbles in the water represents the cpu load.
The color of water will turn darker when more swap spaces is being used.
If you move your mouse over bubblemon, it will fade out into a system load
graph, damn nice :)

And for wmfishtime, it's a analog clock application, with some cute fish
swimming around. You can scare the fish away by moving the mouse over it. Also
wmfishtime can check your e-mails by showing seaweeds in the water.

So, what I've done basically is to have those fish and seaweed in wmfishtime
in bubblemon. Right now these are still kind of ugly hack. But for those who
just want something funny to run, here you go :)

%package gkrellm
Summary: bubblefishymon gkrellm plugin
Group: User Interface/Desktops
Requires: gkrellm

%description gkrellm
the Bubblefissymon plugin for the Gkrellm.

%prep
%setup -q
%patch0 -p1 -b .linking

%build
make
make gkrellm

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/gkrellm2/plugins
install -m 755 bubblefishymon %{buildroot}%{_bindir}
install -m 755 gkrellm-bfm.so %{buildroot}%{_libdir}/gkrellm2/plugins

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-   ,root,root)
%doc ChangeLog COPYING README README.bubblemon doc/Xdefaults.sample
%{_bindir}/bubblefishymon

%files gkrellm
%defattr(-   ,root,root)
%{_libdir}/gkrellm2/plugins/gkrellm-bfm.so

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-11m)
- change Source0 URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-8m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-7m)
- fix build error

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.4-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.4-3m)
- rebuild against gcc43

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.4-2m)
- change installdir (/usr/X11R6 -> /usr)

* Tue Dec 21 2004 Toru Hoshina <toruhosh@k3.dion.ne.jp>
- (0.6.4-1m)
- ver up.

* Wed Mar  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.5.1-10m)
- fix plugin location gkrellm -> gkrellm2
- set a+x to .so

* Wed Mar  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.5.1-9m)
- change plugin location to obey FHS

* Wed Feb 25 2004 Toru Hoshina <t@www.momonga-linux.com>
- (0.5.1-8m)
- bfm-gkrellm come back.

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.5.1-7m)
- omit bfm-gkrellm sub package (not ready for gkrellm2...)

* Wed Jan  9 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.5.1-6k)
- Should BuildPreReq: gkrellm.

* Thu Dec  6 2001 Toru Hoshina <t@kondara.org>
- (0.5.1-4k)
- gkrell plugins should be installed into /usr/share.

* Tue Dec  4 2001 Toru Hoshina <t@kondara.org>
- (0.5.1-2k)
- 1st release.
