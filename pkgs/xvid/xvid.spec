%global momorel 1

Summary: XVID MPEG-4 Video Codec
Name: xvid
Version: 1.3.2
%global srcname xvidcore-%{version}
%global dirname xvidcore
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
Source0: http://downloads.xvid.org/downloads/%{srcname}.tar.bz2 
NoSource: 0
Patch0: xvidcore-1.1.0-beta2-ia64.patch
URL: http://www.xvid.org/
BuildRequires: autoconf >= 2.57
BuildRequires: yasm-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: usoxvid

%description
XVID MPEG-4 Video Codec.

%prep
%setup -q -n %{dirname}
%ifarch ia64
%patch0 -p1
%endif

%build
pushd build/generic

%configure \
%ifarch x86_64 
	--disable-assembly
%endif

%make
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
pushd build/generic
make libdir=%{buildroot}%{_libdir} includedir=%{buildroot}%{_includedir} install
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc LICENSE AUTHORS CodingStyle README ChangeLog TODO doc examples 
%{_libdir}/lib*
%{_includedir}/*

%changelog
* Tue Jul  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-1m)
- update 1.3.2

* Thu Apr 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-4m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-3m)
- be Nonfree
- remove source archive

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- [SECURITY] CVE-2009-0893 CVE-2009-0894
- update 1.2.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against rpm-4.6

* Wed Dec 10 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Thu Dec  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0

* Mon Sep 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-5m)
- [BUILD FIX] set --disable-assembly on x86_64
- change BR from nasm to yasm-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.3-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-3m)
- %%NoSource -> NoSource

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-2m)
- revised spec for debuginfo

* Sun Jul 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3
-- This release fixes a potential security problem

* Thu Nov 09 2006 Masayuki SANO <nosanosa@@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Thu Nov  3 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-0.0.2.3m)
- add gcc4 patch
- Patch0: xvid-1.1.0_beta2-amd64-gcc4.patch

* Mon Oct 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.0-0.0.2.2m)
- enable ia64

* Sat May 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-0.0.2.1m)
- Version: 1.1.0-beta2

* Wed Feb 09 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-0.0.1.1m)
- Version: 1.1.0-beta1

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.2-2m)
- include source tarball as a document.

* Mon Aug 30 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.2-1m)
- bug fix

* Wed Jun 09 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.1-1m)
- major bugfixes

* Fri May 28 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.0-1m)
- 1.0.0 Release!

* Sun May  2 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.0-0.4.1m)
- version 1.0.0-rc4

* Fri Dec 12 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Mon Sep 29 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-2m)
- rebuild against autoconf-2.57-1m
- add BuildRequires: autoconf

* Sun Feb 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-1m)
- version up to 0.9.1

* Sun Feb 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-2m)
- add BuildRequires: nasm

* Thu Dec 19 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-1m)
- TO.Nonfree (T T)
  because xvid can't be distributed in Japan and U.S.A.

* Mon Oct 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20020822.2m)
- support ppc

* Sun Sep  8 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20020822.1m)
- initial import to Momonga
