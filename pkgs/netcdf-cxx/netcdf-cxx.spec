%global         momorel 1

Name:           netcdf-cxx
Version:        4.2
Release:        %{momorel}m%{?dist}
Summary:        Legacy netCDF C++ library
Group:          Development/Libraries
License:        "NetCDF"
URL:            http://www.unidata.ucar.edu/software/netcdf/
Source0:        ftp://ftp.unidata.ucar.edu/pub/netcdf/%{name}-4.2.tar.gz
NoSource:       0
BuildRequires:  netcdf-devel

%description
Legacy netCDF C++ library.  This library is provided for backward
compatibility only. New C++ development should be done with the netCDF
CXX4 C++ library.

%package devel
Summary:        Development files legacy netCDF C++ library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       netcdf-devel

%description devel
This package contains the legacy netCDF C++ library header files and shared
devel library.

%package static
Summary:        Static libraries for legacy netCDF C++ library
Group:          Development/Libraries
Requires:       %{name}-devel%{?_isa} = %{version}-%{release}

%description static
This package contains the netCDF static libraries.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
/bin/rm %{buildroot}%{_libdir}/*.la
/bin/rm ${RPM_BUILD_ROOT}%{_infodir}/dir

%check
make check

%post
/sbin/ldconfig
/sbin/install-info %{_infodir}/%{name}.info \
    %{_infodir}/dir 2>/dev/null || :

%postun
/sbin/ldconfig
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/%{name}.info \
    %{_infodir}/dir 2>/dev/null || :
fi

%files
%doc COPYRIGHT cxx/README
%{_libdir}/libnetcdf_c++.so.*

%files devel
%doc examples man4/%{name}.pdf
%{_includedir}/ncvalues.h
%{_includedir}/netcdf.hh
%{_includedir}/netcdfcpp.h
%{_libdir}/libnetcdf_c++.so
%{_infodir}/%{name}.info*

%files static
%{_libdir}/libnetcdf_c++.a

%changelog
* Thu Jan 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2-1m)
- import from Fedora

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Wed Mar 21 2012 Orion Poplawski <orion@cora.nwra.com> - 4.2-6
- Add Requires netcdf-devel

* Tue Feb 28 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2-5
- Rebuilt for c++ ABI breakage

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon Dec 5 2011 Orion Poplawski <orion@cora.nwra.com> - 4.2-3
- Move info file to -devel
- Add examples and pdf to -devel documentation
- Drop %%defattr, %%clean, BuildRoot

* Mon Oct 3 2011 Orion Poplawski <orion@cora.nwra.com> - 4.2-2
- Use %%{?_isa} in Requires
- Change -static to require the -devel package

* Fri Sep 30 2011 Orion Poplawski <orion@cora.nwra.com> - 4.2-1
- Initial package
