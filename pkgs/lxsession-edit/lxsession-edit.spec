%global momorel 7

Summary:        Simple GUI to configure what automatically started in LXDE
Name:           lxsession-edit
Version:        0.1.1
Release:        %{momorel}m%{?dist}

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.org
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-0.1.1-desktop-file.patch
Patch1:         lxsession-edit-0.1.1-high-resolution-time-fs.patch
# http://lxde.svn.sourceforge.net/viewvc/lxde/trunk/lxsession-edit/src/lxsession-edit.c?r1=1797&r2=1812
Patch11:        %{name}-0.1.1-new-config-file.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel, desktop-file-utils, gettext
Requires:       lxsession

%description
LXSession-edit is a tool to manage freedesktop.org compliant desktop session 
autostarts. Currently adding and removing applications from the startup list 
is not yet available, but it will be support in the next release.


%prep
%setup -q
%patch0 -p1 -b .fixes
%patch1 -p1 -b .high-resolution-time-fs

%patch11 -p2 -b .old

%build
autoreconf -fi
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
desktop-file-install                                       \
  --delete-original                                        \
  --dir=${RPM_BUILD_ROOT}%{_datadir}/applications          \
  --vendor=""                                              \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/%{name}
%{_datadir}/applications/lxsession-edit.desktop
%{_datadir}/lxsession-edit/


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-5m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-4m)
- support new config file (Patch11)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-2m)
- fix infinite loop when build on tmpfs, ext4, xfs, ...
-- import Patch1 from Gentoo

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1
- NO.TMPFS

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-2m)
- no src

* Tue Mar  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-1m)
- import from Fedora to Momonga

* Sun Dec 07 2008 Christoph Wickert <fedora christoph-wickert de> - 0.1-1
- Initial Fedora package
