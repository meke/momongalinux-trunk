%global momorel 1

#define gitrev 4c9ff978

Name:           openni
Version:        1.3.2.1
Release:        %{momorel}m%{?dist}
Summary:        Library for human-machine Natural Interaction

Group:          System Environment/Libraries
License:        LGPLv3+
URL:            http://www.openni.org
# No official releases, yet. To reproduce tarball (adapt version and gitrev):
# git clone git://github.com/OpenNI/OpenNI.git
# cd OpenNI.git
# rm -rf Platform/Win32 Platform/Android
# git archive --format tar --prefix=openni-1.3.2.1/ HEAD | gzip > ../openni-1.3.2.1.tar.gz
Source0:        openni-%{version}.tar.gz
Patch0:         openni-1.3.2.1-willow.patch
Patch1:         openni-1.3.2.1-fedora.patch
Patch2:         openni-1.3.2.1-disable-sse.patch
Patch3:         openni-1.3.2.1-silence-assert.patch
Patch4:         openni-1.3.2.1-fedora-java.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  freeglut-devel, tinyxml-devel, libjpeg-devel, dos2unix, libusb1-devel
BuildRequires:  python, doxygen

%description
OpenNI (Open Natural Interaction) is a multi-language, cross-platform
framework that defines APIs for writing applications utilizing Natural
Interaction. OpenNI APIs are composed of a set of interfaces for writing NI
applications. The main purpose of OpenNI is to form a standard API that
enables communication with both:
 * Vision and audio sensors
 * Vision and audio perception middleware

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        java
Summary:        %{name} Java library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
BuildRequires:  java-devel
BuildRequires:  jpackage-utils
Requires:       java
Requires:       jpackage-utils

%description    java
The %{name}-java package contains a Java JNI library for
developing applications that use %{name} in Java.

%package        doc
Summary:        API documentation for %{name}
Group:          Documentation
BuildArch:      noarch

%description    doc
The %{name}-doc package contains the automatically generated API documentation
for OpenNI.

%package        samples
Summary:        Sample programs for %{name}
Group:          Development/Tools
Requires:       %{name} = %{version}-%{release}

%description    samples
The %{name}-sample package contains sample programs for OpenNI.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .willow
%patch1 -p1 -b .fedora
%patch2 -p1 -b .disable-sse
%patch3 -p1 -b .silence-assert
%patch4 -p1 -b .fedora-java

rm -rf Source/External
rm -rf Platform/Linux-x86/Build/Prerequisites/*
find Samples -name GL -prune -exec rm -rf {} \;
find Samples -name Libs -prune -exec rm -rf {} \;

for ext in c cpp; do
  find Samples -name "*.$ext" -exec \
    sed -i -e 's|#define SAMPLE_XML_PATH "../../../../Data/SamplesConfig.xml"|#define SAMPLE_XML_PATH "%{_sysconfdir}/%{name}/SamplesConfig.xml"|' {} \;
done

dos2unix README
dos2unix GPL.txt
dos2unix LGPL.txt

%build
cd Platform/Linux-x86/CreateRedist
# {?_smp_mflags} omitted, not supported by OpenNI Makefiles
sed -i "s|make -C ../Build|make -C ../Build CFLAGS_EXT=\\\\\"%{optflags}\\\\\" SSE_GENERATION=2 DEBUG=1|" Redist_OpenNi.py
./RedistMaker

%install
rm -rf %{buildroot}
pushd Platform/Linux-x86/Redist
INSTALL_LIB=%{buildroot}%{_libdir} \
INSTALL_BIN=%{buildroot}%{_bindir} \
INSTALL_INC=%{buildroot}%{_includedir}/ni \
INSTALL_VAR=%{buildroot}%{_var}/lib/ni \
INSTALL_JAR=%{buildroot}%{_libdir}/%{name} \
./install.sh -n

install -m 0755 Samples/Bin/Release/libSample-NiSampleModule.so %{buildroot}%{_libdir}/libNiSampleModule.so
install -m 0755 Samples/Bin/Release/NiViewer %{buildroot}%{_bindir}
install -m 0755 Samples/Bin/Release/Sample-NiAudioSample %{buildroot}%{_bindir}/NiAudioSample
install -m 0755 Samples/Bin/Release/Sample-NiBackRecorder %{buildroot}%{_bindir}/NiBackRecorder
install -m 0755 Samples/Bin/Release/Sample-NiConvertXToONI %{buildroot}%{_bindir}/NiConvertXToONI
install -m 0755 Samples/Bin/Release/Sample-NiCRead %{buildroot}%{_bindir}/NiCRead
install -m 0755 Samples/Bin/Release/Sample-NiRecordSynthetic %{buildroot}%{_bindir}/NiRecordSynthetic
install -m 0755 Samples/Bin/Release/Sample-NiSimpleCreate %{buildroot}%{_bindir}/NiSimpleCreate
install -m 0755 Samples/Bin/Release/Sample-NiSimpleRead %{buildroot}%{_bindir}/NiSimpleRead
install -m 0755 Samples/Bin/Release/Sample-NiSimpleViewer %{buildroot}%{_bindir}/NiSimpleViewer
install -m 0755 Samples/Bin/Release/Sample-NiUserTracker %{buildroot}%{_bindir}/NiUserTracker

popd

mkdir -p %{buildroot}%{_datadir}/%{name}-doc
cp -a Source/DoxyGen/html/* %{buildroot}%{_datadir}/%{name}-doc

mkdir -p %{buildroot}%{_sysconfdir}/%{name}
install -p -m 0644 Data/SamplesConfig.xml %{buildroot}%{_sysconfdir}/%{name}

mkdir -p %{buildroot}%{_var}/lib/ni
echo "<Modules/>" > %{buildroot}%{_var}/lib/ni/modules.xml


%clean
rm -rf %{buildroot}


%post
/sbin/ldconfig
if [ $1 == 1 ]; then
  niReg -r %{_libdir}/libnimMockNodes.so
  niReg -r %{_libdir}/libnimCodecs.so
  niReg -r %{_libdir}/libnimRecorder.so
fi


%preun
if [ $1 == 0 ]; then
  niReg -u %{_libdir}/libnimMockNodes.so
  niReg -u %{_libdir}/libnimCodecs.so
  niReg -u %{_libdir}/libnimRecorder.so
fi


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc GPL.txt LGPL.txt README
%dir %{_sysconfdir}/%{name}
%{_libdir}/*.so
%{_bindir}/ni*
%{_var}/lib/ni


%files devel
%defattr(-,root,root,-)
%doc Documentation/OpenNI_UserGuide.pdf
%{_includedir}/*
#{_libdir}/*.so


%files java
%defattr(-,root,root,-)
%{_libdir}/%{name}


%files samples
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/%{name}/SamplesConfig.xml
%{_bindir}/Ni*


%files doc
%defattr(-,root,root,-)
%{_datadir}/%{name}-doc


%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2.1-1m)
- update to 1.3.2.1 (sync with Fedora)

* Tue Oct  4 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.25-0.git4c9ff978.3m)
- fix image corruption bug caused by misuse of memcpy

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0.25-0.git4c9ff978.2m)
- rebuild for new GCC 4.6

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0.25-0.git4c9ff978.1m)
- Initial Commit Momonga Linux
- 

* Tue Feb 01 2011 Tim Niemueller <tim@niemueller.de> - 1.0.0.25-0.2git4c9ff978.fc14
- Incorporate review suggestions

* Thu Jan 20 2011 Tim Niemueller <tim@niemueller.de> - 1.0.0.25-0.1git4c9ff978.fc14
- Initial revision

