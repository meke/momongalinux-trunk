%global momorel 6

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-fpconst
Version:        0.7.3
Release:        %{momorel}m%{?dist}
Summary:        Python module for handling IEEE 754 floating point special values

Group:          Development/Languages
License:        "ASL 2.0"
URL:            http://research.warnes.net/statcomp/projects/RStatServer/fpconst
Source0:        http://dl.sourceforge.net/sourceforge/rsoap/fpconst-%{version}.tar.gz
NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python-devel >= 2.7

%description
This python module implements constants and functions for working with
IEEE754 double-precision special values.  It provides constants for
Not-a-Number (NaN), Positive Infinity (PosInf), and Negative Infinity
(NegInf), as well as functions to test for these values.


%prep
%setup -qn fpconst-%{version}
chmod -x README pep-0754.txt


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc CHANGELOG COPYING README pep-0754.txt
%{python_sitelib}/*.py
%{python_sitelib}/*.pyc
%{python_sitelib}/*.pyo
%{python_sitelib}/*.egg-info


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.3-6m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-1m)
- import from Fedora 11

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.3-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.7.3-4
- Rebuild for Python 2.6

* Sat Mar 08 2008 Christopher Stone <chris.stone@gmail.com> 0.7.3-3
- Add egg-info to %%files

* Sat Mar 08 2008 Christopher Stone <chris.stone@gmail.com> 0.7.3-2
- Fix %%Source0 URL

* Sun Sep 30 2007 Christopher Stone <chris.stone@gmail.com> 0.7.3-1
- Upstream sync
- Update source URL
- Some spec file cleanups

* Fri Dec 08 2006 Christopher Stone <chris.stone@gmail.com> 0.7.2-3.2
- Add python-devel to BR

* Fri Dec 08 2006 Christopher Stone <chris.stone@gmail.com> 0.7.2-3.1
- python(abi) = 0:2.5

* Wed Sep 06 2006 Christopher Stone <chris.stone@gmail.com> 0.7.2-3
- No longer %%ghost pyo files.  Bug #205414

* Wed Aug 30 2006 Christopher Stone <chris.stone@gmail.com> 0.7.2-2
- Shorten summary
- Remove unnecessary requires

* Sat Mar 18 2006 Christopher Stone <chris.stone@gmail.com> 0.7.2-1
- Initial Release of python-fpconst, changes from fpconst include:
- Renamed package from fpconst to python-fpconst
- Removed macros in URL
- Removed python-devel from BR
- Droped the second paragraph in %%description
- Droped PKG-INFO from %%doc
- Added pep-0754.txt to %%doc
