%global         momorel 1
%global         srcname %{name}-%{version}
%global         dirname Scribus

Summary:        DeskTop Publishing application written in Qt
Name:           scribus
Version:        1.4.1
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/Publishing
URL:            http://www.scribus.net/
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{srcname}.tar.xz
NoSource:       0
Patch0:         %{name}-1.4.0-docdir.patch
Patch1:         %{name}-to-double.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(post): shared-mime-info
Requires(postun): desktop-file-utils
Requires(postun): shared-mime-info
Requires:       ghostscript
Requires:       python
Requires:       python-imaging
Requires:       tkinter
BuildRequires:  cmake
BuildRequires:  qt-devel >= 4.8.2
BuildRequires:  aspell-devel
BuildRequires:  cairo-devel
BuildRequires:  cups-devel
BuildRequires:  desktop-file-utils
BuildRequires:  ghostscript-devel
BuildRequires:  freetype-devel >= 2.3.1-2m
BuildRequires:  fontconfig-devel
BuildRequires:  lcms-devel >= 1.15-4m
BuildRequires:  libICE-devel
BuildRequires:  libSM-devel
BuildRequires:  libXcursor-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libXi-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  libxml2-devel >= 2.7.2-3m
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  perl
BuildRequires:  podofo-devel >= 0.9.1
BuildRequires:  python-devel >= 2.7

%description
Scribus is an open-source program that brings award-winning
professional page layout to Linux/Unix, MacOS X and Windows desktops
with a combination of "press-ready" output and new approaches to page
layout.

Underneath the modern and user friendly interface, Scribus supports
professional publishing features, such as CMYK color, separations, ICC
color management and versatile PDF creation.

%package devel
Summary: Header files for scribus
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Header files for Scribus.

%package doc
Summary: Documentation files for Scribus
Group: Development/Tools
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
%{summary}

%prep
%setup -q -n %{dirname}
%patch0 -p1 -b .docdir
%patch1 -p1 -b .double

# recode man page to UTF-8
pushd scribus/manpages
iconv -f ISO8859-2 -t UTF-8 scribus.1.pl > tmp
touch -r scribus.1.pl tmp
mv tmp scribus.1.pl
popd

# fix permissions
chmod a-x scribus/pageitem_latexframe.h

# drop shebang lines from python scripts
for f in scribus/plugins/scriptplugin/{samples,scripts}/*.py
do
    sed '1{/#!\/usr\/bin\/env\|#!\/usr\/bin\/python/d}' $f > $f.new
    touch -r $f $f.new
    mv $f.new $f
done

%build
mkdir build
pushd build
PATH="%{_qt4_bindir}:$PATH" \
    %{cmake} \
    -DCMAKE_SKIP_RPATH=YES \
    -DWANT_DISTROBUILD=YES \
    -DPYTHON_INCLUDE_DIR:PATH=%{_includedir}/python2.7 \
    -DPYTHON_LIBRARY:FILEPATH=%{_libdir}/libpython2.7.so \
    ..
make %{?_smp_mflags} VERBOSE=1
popd

%install
rm -rf %{buildroot}
pushd build
make install DESTDIR=%{buildroot}
popd

install -p -D -m0644 %{buildroot}%{_datadir}/scribus/icons/scribus.png %{buildroot}%{_datadir}/pixmaps/scribus.png
install -p -D -m0644 %{buildroot}%{_datadir}/scribus/icons/scribusdoc.png %{buildroot}%{_datadir}/pixmaps/x-scribus.png

# desktopfile
rm -f %{buildroot}%{_datadir}/mimelnk/application/*scribus.desktop

desktop-file-install --vendor= \
    --dir %{buildroot}%{_datadir}/applications \
    --remove-category Graphics \
    --add-category Office \
    ./%{name}.desktop

rm -f %{buildroot}%{_defaultdocdir}/scribus/{AUTHORS,BUILDING,COPYING,ChangeLog,ChangeLogSVN,LINKS,NEWS,PACKAGING,README,README.MacOSX,TODO,TRANSLATION}

%clean
rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:
%{_bindir}/update-mime-database %{_datadir}/mime > /dev/null 2>&1 || :

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:
%{_bindir}/update-mime-database %{_datadir}/mime > /dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc AUTHORS BUILDING COPYING ChangeLog LINKS NEWS PACKAGING README TODO TRANSLATION
%{_bindir}/scribus
%{_datadir}/applications/scribus.desktop
%{_datadir}/mime/packages/scribus.xml
%{_datadir}/pixmaps/*
%{_datadir}/scribus/
%{_libdir}/scribus/
%{_mandir}/man1/*
%{_mandir}/pl/man1/*
%{_mandir}/de/man1/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/scribus/

%files doc
%defattr(-,root,root,-)
%lang(de) %{_defaultdocdir}/scribus/de
%lang(en) %{_defaultdocdir}/scribus/en
%lang(it) %{_defaultdocdir}/scribus/it

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-2m)
- rebuild against libtiff-4.0.1
- rebuild against podofo-0.9.1

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Thu Aug 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.9-4m)
- fix build failure; import scribus-1.3.9-cups.patch from fedora

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.9-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.9-2m)
- rebuild for new GCC 4.6

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.9-1m)
- update to 1.3.9

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-6m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-5m)
- specify PATH for Qt4

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.8-3m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.8-2m)
- move menu from Graphics to Office

* Mon Aug  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.8-1m)
- version 1.3.8

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-2m)
- rebuild against qt-4.6.3-1m

* Fri Jun  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.6-2m)
- import qstring patch from Fedora devel

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-1m)
- update to 1.3.6
- explicitly link libdl

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5.1-6m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5.1-5m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5.1-3m)
- revive tutorials

* Sun Sep 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5.1-2m)
- release %%dir %%{_datadir}/doc/%%{srcname} from package doc
- it's already provided by main package: scribus and scribus-doc Requires: scribus

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.5.1-1m)
- update 1.3.5.1

* Mon Jul 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-0.3.1m)
- update to 1.3.5.rc3

* Tue Jun  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-0.2.1m)
- update to 1.3.5.rc2
-- use qt4 and cmake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-2m)
- rebuild against python-2.6.1

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3.11-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3.11-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.11-1m)
- update to 1.3.3.11

* Sun Feb 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3.9-3m)
- specify qt3 libs

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3.9-2m)
- %%NoSource -> NoSource

* Wed May 23 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.9-1m)
- update to 1.3.3.9

* Sun Apr 01 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.8-1m)
- update to 1.3.3.8

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3.7-4m)
- Requires: freetype2 -> freetype

* Thu Feb 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3.7-3m)
- revive *.la

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.3.7-2m)
- delete libtool library

* Wed Jan 10 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.7-1m)
- update to 1.3.3.7

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3.6-2m)
- rebuild against python-2.5

* Fri Dec 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.6-1m)
- update to 1.3.3.6

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.5-1m)
- update to 1.3.3.5

* Wed Oct 04 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.4-1m)
- update to 1.3.3.4

* Tue Sep 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.3-1m)
- 1.3.3.3 has been released.

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.3.2-3m)
- remove category Application

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.3.3.2-2m)
- rebuild against expat-2.0.0-1m

* Tue May 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.2-1m)
- update to bug fix release 1.3.3.2

* Sun May 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3.1-1m)
- import to Momonga (at last!)

* Wed Apr 12 2006 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.3.3.1-0.0.1m)
- update to 1.3.3.1

* Tue Apr 04 2006 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.3.3-0.0.1m)
- update to 1.3.3
- japanese messages seems to be merged in upstream

* Wed Feb 01 2006 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.3.2-0.0.1m)
- update to 1.3.2
- add japanese message files at http://blog.good-day.net/~kurose/

* Tue Jul 26 2005 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.3.0-0.0.1m)
- version 1.3.0
- scribus-1.3.0-no_remake_docitemattrprefs_cpp.patch since uic causes segmentation fault during remake docitemattrprefs.cpp

* Thu May 19 2005 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.2.1-0.0.1m)
- version 1.2.1

* Sun Nov 07 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.2-0.0.1m)
- version 1.2

* Mon Jun 07 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.1.7-0.0.1m)
- version 1.1.7

* Tue Apr 27 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.1.6-0.0.1m)
- version 1.1.6

* Wed Feb 18 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.1.5-0.0.1m)
- version 1.1.5

* Sat Jan 10 2004 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.1.4-0.0.1m)
- version 1.1.4

* Sun Nov  2 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.1.2-0.0.1m)
- version 1.1.2

* Sun Oct  5 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.1.1-0.0.1m)
- version 1.1.1

* Mon Aug 11 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.0.1-0.0.1m)
- version 1.0.1

* Thu Jul 17 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (1.0-0.0.1m)
- version 1.0

* Sun Jun 15 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.10-0.0.1m)
- version 0.9.10

* Sat Mar 8 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.7-0.0.1m)
- version 0.9.7

* Sun Dec 22 2002 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.4-0.0.1m)
- version 0.9.4

* Sun Nov 24 2002 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.9.2-0.0.1m)
- version up

* Mon Nov 4 2002 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.8-0.2m)
- add Patch for the Measurements Palette (patch1)
- add Patch for the Fontmenu on newer Systems (patch2)

* Sat Oct 12 2002 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.8-0.1m)
- version 0.8

* Fri Sep 20 2002 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (0.7.8-0.1m)
- build for Momonga
