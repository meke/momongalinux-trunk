%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-react
Version:        0.9.2
Release:        %{momorel}m%{?dist}
Summary:        OCaml framework for Functional Reactive Programming (FRP)

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Group:          Development/Libraries
License:        BSD
URL:            http://erratique.ch/software/react

Source0:        http://erratique.ch/software/react/releases/react-%{version}.tbz
NoSource:       0
Source1:        react-LICENSE

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  chrpath


%description
React is an OCaml module for functional reactive programming (FRP). It
provides support to program with time varying values : applicative
events and signals. React doesn't define any primitive event or
signal, this lets the client chooses the concrete timeline.

React is made of a single, independent, module and distributed under
the new BSD license.

Given an absolute notion of time Rtime helps you to manage a timeline
and provides time stamp events, delayed events and delayed signals.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n react-%{version}
cp %{SOURCE1} LICENSE

## permission
chmod +x ./build

%build
./build


%install
rm -rf $RPM_BUILD_ROOT
export INSTALLDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml/react
mkdir -p $INSTALLDIR
./build install


%check
%if %opt
./build test.native
./_build/test/test.native
./build clock.native
#./_build/test/clock.native
./build breakout.native
#./_build/test/breakout.native
%endif


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/react
%if %opt
%exclude %{_libdir}/ocaml/react/*.cmx
%endif
%exclude %{_libdir}/ocaml/react/*.mli
%exclude %{_libdir}/ocaml/react/*.ml


%files devel
%defattr(-,root,root,-)
%doc CHANGES README
%if %opt
%{_libdir}/ocaml/react/*.cmx
%endif
%{_libdir}/ocaml/react/*.mli
%{_libdir}/ocaml/react/*.ml


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-1m)
- import from Fedora 13

* Wed Dec 30 2009 Richard W.M. Jones <rjones@redhat.com> - 0.9.0-3
- Rebuild for OCaml 3.11.2.

* Thu Oct  8 2009 Richard W.M. Jones <rjones@redhat.com> - 0.9.0-2
- Initial RPM release.
- Use global instead of define (Till Maas).
