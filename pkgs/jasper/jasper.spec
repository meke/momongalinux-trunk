%global momorel 16

Summary: JPEG-2000 utilities 
Name: jasper
Version: 1.900.1 
Release: %{momorel}m%{?dist}
License: see "COPYRIGHT" and "LICENSE"
Group: Applications/Multimedia
URL: http://www.ece.uvic.ca/~mdadams/jasper/
Source0: http://www.ece.uvic.ca/~mdadams/jasper/software/%{name}-%{version}.zip 
NoSource: 0
Patch1: jasper-1.900.1-fixes-20081208.patch.bz2
Patch2: jasper-1.900.1-CVE-2011-4516-CVE-2011-4517-CERT-VU-887409.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libjpeg freeglut
BuildRequires: libjpeg-devel >= 8a

Requires: %{name}-libs = %{version}-%{release}

%description
JasPer is a collection
of software (i.e., a library and application programs) for the coding
and manipulation of images.  This software can handle image data in a
variety of formats.  One such format supported by JasPer is the JPEG-2000
format defined in ISO/IEC 15444-1:2000.

%package libs
Summary: %{name}-libs
Group: System Environment/Libraries


%description libs
%{name}-libs

%package -n %{name}-devel
Summary: Include Files and Documentation
Group: Development/Libraries
Requires: %{name} = %{version}

%description -n %{name}-devel
JasPer is a collection
of software (i.e., a library and application programs) for the coding
and manipulation of images.  This software can handle image data in a
variety of formats.  One such format supported by JasPer is the JPEG-2000
code stream format defined in ISO/IEC 15444-1:2000.

%prep
%setup -q
%patch1 -p1 -b .security-fixes~
%patch2 -p1 -b .CVE-2011-4516-CVE-2011-4517

%build
%configure --enable-shared --program-prefix="" --disable-opengl

%make

%install
rm -rf --preserve-root %{buildroot}

%makeinstall

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc COPYRIGHT ChangeLog INSTALL LICENSE
%doc NEWS README doc/jasper.pdf doc/jpeg2000.pdf 
%{_bindir}/imgcmp
%{_bindir}/imginfo
%{_bindir}/jasper
%{_bindir}/tmrdemo
%{_mandir}/man1/*.1*

%files libs
%defattr(-,root,root)
%{_libdir}/libjasper.so.*
%exclude %{_libdir}/*.la

%files -n %{name}-devel
%defattr(-, root, root)
%{_includedir}/jasper
%{_libdir}/lib*.so
%{_libdir}/lib*.a

%changelog
* Thu Dec 22 2011 NARRITA Koichi <pulsar@momonga-linux.org>
- (1.900.1-16m)
- [SECURITY] CVE-2011-4516 CVE-2011-4517

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.900.1-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.900.1-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.900.1-13m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.900.1-12m)
- fix req

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.900.1-11m)
- split libs

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.900.1-10m)
- rebuild against libjpeg-8a

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.900.1-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.900.1-8m)
- rebuild against libjpeg-7

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.900.1-7m)
- do not specify man file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.900.1-6m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.900.1-5m)
- [SECURITY] CVE-2008-3520 CVE-2008-3522
- apply Patch1 from Gentoo
- drop Patch0, included in Patch1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.900.1-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.900.1-3m)
- %%NoSource -> NoSource

* Thu Nov  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.900.1-2m)
- [SECURITY] CVE-2007-2721
- add patch0 (from debian)

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.900.1-1m)
- update to 1.900.1

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.900.0-2m)
- delete libtool library

* Wed Jan 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.900.0-1m)
- update to 1.900.0
- revise %%files section

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.701.0-4m)
- rebuild against freeglut

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.701.0-3m)
- delete ifarch
- delete executable jiv

* Sun Jun 12 2005 mutecat <mutecat@momonga-linux.org>
- (1.701.0.2m)
- add ifarch

* Sun Jun 12 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (jasper-1.701.0-1m)
- (jasper-devel-1.701.0-1m)
- rename binary file.(???)

* Thu Jun  9 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (jasper-1.701.0-0m)
- (jasper-devel-1.701.0-0m)
- create.
