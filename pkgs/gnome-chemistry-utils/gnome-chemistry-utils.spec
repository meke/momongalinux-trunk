%global		momorel 2
Name:           gnome-chemistry-utils
Version:        0.14.8
Release: %{momorel}m%{?dist}
Summary:        A set of chemical utilities
Group: 		User Interface/Desktops
#openbabel/* is GPLv2+
License:        GPLv3+ and GPLv2+
URL:            http://www.nongnu.org/gchemutils/
Source0:        http://download.savannah.nongnu.org/releases/gchemutils/0.14/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  bodr
BuildRequires:  chemical-mime-data
BuildRequires:  chrpath
BuildRequires:  desktop-file-utils
BuildRequires:  doxygen
BuildRequires:  gettext
BuildRequires:  gecko-devel
BuildRequires:  gnome-doc-utils
BuildRequires:  gnumeric-devel
BuildRequires:  goffice-devel >= 0.10.16
BuildRequires:  gtkglext-devel
BuildRequires:  intltool
BuildRequires:  man-pages-reader
BuildRequires:  openbabel-devel >= 2.1.0
BuildRequires:  perl-XML-Parser

Requires:       bodr
Requires:       chemical-mime-data
Requires:       hicolor-icon-theme
Obsoletes:      %{name}-devel

%description
This package is a set of chemical utils. Several programs are available:
* A 3D molecular structure viewer (GChem3D).
* A Chemical calculator (GChemCalc).
* A 2D structure editor (GChemPaint).
* A periodic table of the elements application (GChemTable).
* A crystalline structure editor (GCrystal).
* A spectra viewer (GSpectrum).

%package        gnumeric
Summary:        Gnome Chemistry Utils plugin for Gnumeric
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    gnumeric
This package is a set of chemical utils. Several programs are available:
* A 3D molecular structure viewer (GChem3D).
* A Chemical calculator (GChemCalc).
* A 2D structure editor (GChemPaint).
* A periodic table of the elements application (GChemTable).
* A crystalline structure editor (GCrystal).
* A spectra viewer (GSpectrum).
This package contains a plugin adding a few chemistry-related functions to
gnumeric.

%package        mozplugin
Summary:        Gnome Chemistry Utils plugin for Mozilla
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       mozilla-filesystem

%description    mozplugin
This package is a set of chemical utils. Several programs are available:
* A 3D molecular structure viewer (GChem3D).
* A Chemical calculator (GChemCalc).
* A 2D structure editor (GChemPaint).
* A periodic table of the elements application (GChemTable).
* A crystalline structure editor (GCrystal).
* A spectra viewer (GSpectrum).
This package contains the mozilla plugin.


%prep
%setup -q

%build
%configure --disable-update-databases \
           --disable-scrollkeeper \
           --with-mozilla-libdir=%{_libdir}/mozilla \
           --disable-silent-rules
%make 


%install
rm -rf %{buildroot}
make INSTALL="install -p" install DESTDIR=%{buildroot}
desktop-file-install --vendor= \
       --delete-original --dir %{buildroot}%{_datadir}/applications \
       %{buildroot}%{_datadir}/applications/{gchem3d,gchemcalc,gchempaint,gchemtable,gcrystal,gspectrum}-0.14.desktop
%find_lang gchemutils-0.14

#kill libtool archives
find %{buildroot} -name '*.la' -exec rm -f {} ';'

#kill intrusive docs
rm -rf %{buildroot}%{_docdir}/gchemutils

#kill rpaths
chrpath --delete %{buildroot}%{_bindir}/gchem3d-0.14
chrpath --delete %{buildroot}%{_bindir}/gchemcalc-0.14
chrpath --delete %{buildroot}%{_bindir}/gchempaint-0.14
chrpath --delete %{buildroot}%{_bindir}/gchemtable-0.14
chrpath --delete %{buildroot}%{_bindir}/gcrystal-0.14
chrpath --delete %{buildroot}%{_bindir}/gspectrum-0.14
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/cdx/cdx.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/cdxml/cdxml.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/cif/cif.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/cml/cml.so
# chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/ctfiles/ctfiles.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/nuts/nuts.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/paint/arrows.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/paint/atoms.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/paint/bonds.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/paint/cycles.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/paint/residues.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/paint/selection.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/paint/templates.so
chrpath --delete %{buildroot}%{_libdir}/gchemutils/0.14/plugins/paint/text.so
chrpath --delete %{buildroot}%{_libdir}/gnumeric/1.12/plugins/gchemutils/plugin.so
chrpath --delete %{buildroot}%{_libdir}/goffice/0.10/plugins/gchemutils/gchemutils.so
chrpath --delete %{buildroot}%{_libdir}/libgcp-0.14.so.%{version}
chrpath --delete %{buildroot}%{_libdir}/libgcrystal-0.14.so.%{version}
chrpath --delete %{buildroot}%{_libdir}/libgcugtk-0.14.so.%{version}
chrpath --delete %{buildroot}%{_libexecdir}/chem-viewer

#kill KDE MIME .desktop files
rm -rf %{buildroot}%{_datadir}/mimelnk

#kill the unnecessary .so symlinks
rm -rf %{buildroot}%{_libdir}/libgccv-0.14.so
rm -rf %{buildroot}%{_libdir}/libgcp-0.14.so
rm -rf %{buildroot}%{_libdir}/libgcrystal-0.14.so
rm -rf %{buildroot}%{_libdir}/libgcu-0.14.so
rm -rf %{buildroot}%{_libdir}/libgcugtk-0.14.so


%post
update-desktop-database &> /dev/null ||:
update-mime-database %{_datadir}/mime &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/sbin/ldconfig


%postun
if [ $1 -eq 0 ] ; then
     glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi
update-desktop-database &> /dev/null ||:
update-mime-database %{_datadir}/mime &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
/sbin/ldconfig


%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f gchemutils-0.14.lang
%doc AUTHORS ChangeLog COPYING INSTALL NEWS README TODO
%{_bindir}/gchem3d-0.14
%{_bindir}/gchem3d
%{_bindir}/gchemcalc-0.14
%{_bindir}/gchemcalc
%{_bindir}/gchempaint-0.14
%{_bindir}/gchempaint
%{_bindir}/gchemtable-0.14
%{_bindir}/gchemtable
%{_bindir}/gcrystal-0.14
%{_bindir}/gcrystal
%{_bindir}/gspectrum-0.14
%{_bindir}/gspectrum
%{_libdir}/gchemutils
%{_libdir}/goffice/*/plugins/gchemutils
%{_libdir}/libgccv-0.14.so.*
%{_libdir}/libgcp-0.14.so.*
%{_libdir}/libgcrystal-0.14.so.*
%{_libdir}/libgcu-0.14.so.*
%{_libdir}/libgcugtk-0.14.so.*
%{_libexecdir}/babelserver
%{_libexecdir}/chem-viewer
%{_datadir}/applications/gchem3d-0.14.desktop
%{_datadir}/applications/gchemcalc-0.14.desktop
%{_datadir}/applications/gchempaint-0.14.desktop
%{_datadir}/applications/gchemtable-0.14.desktop
%{_datadir}/applications/gcrystal-0.14.desktop
%{_datadir}/applications/gspectrum-0.14.desktop
%{_datadir}/gchemutils
%{_datadir}/glib-2.0/schemas/org.gnome.gchemutils.*
%{_datadir}/gnome/help/gchem3d-0.14
%{_datadir}/gnome/help/gchemcalc-0.14
%{_datadir}/gnome/help/gchempaint-0.14
%{_datadir}/gnome/help/gchemtable-0.14
%{_datadir}/gnome/help/gcrystal-0.14
%{_datadir}/gnome/help/gspectrum-0.14
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/icons/hicolor/scalable/mimetypes/application-x-gchempaint.svg
%{_datadir}/icons/hicolor/scalable/mimetypes/application-x-gcrystal.svg
%{_mandir}/man1/gchem3d.1*
%{_mandir}/man1/gchemcalc.1*
%{_mandir}/man1/gchempaint.1*
%{_mandir}/man1/gchemtable.1*
%{_mandir}/man1/gcrystal.1*
%{_mandir}/man1/gspectrum.1*
%{_datadir}/mime/packages/gchemutils.xml
%{_datadir}/omf/gchem3d-0.14
%{_datadir}/omf/gchemcalc-0.14
%{_datadir}/omf/gchempaint-0.14
%{_datadir}/omf/gchemtable-0.14
%{_datadir}/omf/gcrystal-0.14
%{_datadir}/omf/gspectrum-0.14

%files gnumeric
%{_libdir}/gnumeric/*/plugins/gchemutils

%files mozplugin
%{_libdir}/mozilla/plugins/libmozgcu.so


%changelog
* Wed Jun 25 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.14.8-2m)
- rebuild against gnumeric-1.12.17

* Sat May 31 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.14.8-1m)
- update to 0.14.8
- build against gnumeric-1.12.6-1m and goffice-0.10.16-1m

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.2-1m)
- update to 0.14.2
- remove fedora from vendor (desktop-file-install)
- rebuild against goffice-0.10.1 and gnumeric-1.12.1

* Tue Dec 11 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.13.99-1m)
- update to 0.13.99 to build against gnumeric-1.11.90

* Sat Oct 13 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13.98-1m)
- update to version 0.13.98 to rebuild against gnumeric-1.11.6

* Sat Jul 21 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13.91-2m)
- set Obsoletes: gnome-chemistry-utils-devel

* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.91-1m)
- reimport from fedora

* Sat Jun 30 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.9-10m)
- add a patch to enable build

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.9-9m)
- rebuild for glib 2.33.2

* Tue Aug 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.9-1m)
- update to 0.12.9

* Thu Apr 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.2-8m)
- modify CXXFLAGS to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.2-7m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.2-6m)
- rebuild against openbabel-2.3.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.2-4m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.12.2-3m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.12.2-2m)
- add Requires: GConf2

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.2-1m)
- update to 0.12.2

* Fri Jun 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-6m)
- rebuild against gnumeric-1.10.6
- rebuild against goffice-0.8.6

* Sat Jun 12 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-5m)
- rebuild against gnumeric-1.10.5

* Mon May 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-4m)
- rebuild against goffice-0.8.5

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-3m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-2m)
- build fix goffice-0.8.4

* Mon May 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0
- disable --enable-mozilla-plugin

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-5m)
- build fix gcc-4.4.4

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.8-4m)
- add patch for gtk-2.20 by gengtk220patch

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.8-3m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.4-3m)
- rebuild against xulrunner-1.9.1

* Tue May 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.4-2m)
- rebuild against goffice-0.6.6

* Sun May 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.3-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Tue Dec  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Mon Nov 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-4m)
- rebuild against openbabel-2.2.0

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-3m)
- rebuild against firefox-3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-2m)
- rebuild against gcc43

* Tue Mar 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6

* Thu Jan  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.5-2m)
- rebuild against goffice-0.4

* Sun Dec 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.5-1m)
- update to 0.8.5

* Tue Oct 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-1m)
- update to 0.8.4

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-1m)
- update to 0.8.3

* Sun Aug 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-2m)
- fix %%post script

* Sun Aug 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Fri Aug  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-5m)
- add intltoolize (intltool-0.36.0)

* Mon Jun 25 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-4m)
- added BuildPrereq: firefox-devel

* Fri Mar 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-3m)
- move libmozgcu.so to %%{_libdir}/mozilla/plugins/

* Wed Mar 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-2m)
- no NoSource

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- initial build
