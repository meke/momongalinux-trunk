%global momorel 6

Summary: A perfect hash function generator
Name: gperf
Version: 3.0.4
Release: %{momorel}m%{?dist}
License: GPLv3+
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Group: Development/Tools
Requires(post): info
Requires(preun): info
BuildRequires: gcc-c++ >= 3.4.1-1m
URL: http://www.gnu.org/software/gperf/gperf.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Gperf is a perfect hash function generator written in C++. Simply
stated, a perfect hash function is a hash function and a data
structure that allows recognition of a key word in a set of words
using exactly one probe into the data structure.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_mandir}
mkdir -p %{buildroot}%{_infodir}

%makeinstall

rm -rf %{buildroot}%{_mandir}/{dvi,html}
strip -R .comments %{buildroot}%{_bindir}/gperf
rm -f %{buildroot}/usr/share/doc/gperf.html

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/gperf.info %{_infodir}/dir

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/gperf.info %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc README NEWS doc/gperf.html
%{_bindir}/gperf
%{_mandir}/man1/gperf.1*
%{_infodir}/gperf.info*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.4-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.4-3m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-4m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-3m)
- drop Patch0 for fuzz=0, already merged upstream
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.3-2m)
- rebuild against gcc43

* Sat Mar 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.3-1m)
- update 3.0.3
- change Source0 URI

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.2-1m)
- update 3.0.2

* Fri Nov  4 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.0.1-2m)
- import gcc4 patch (Patch0) from Fedora

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.0.1-1m)
  update to 3.0.1

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.7.2-8m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.7.2-7m)
- revised spec for enabling rpm 4.2.

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.2-6m)
- add URL tag

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.7.2-4k)
- /sbin/install-info -> info in PreReq.

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.7.2-2k)

* Wed Sep 12 2001 Tim Powers <timp@redhat.com>
- rebuild with new gcc and binutils

* Tue Apr 24 2001 Bernhard Rosenkraenzer <bero@redhat.com> 2.7.2-1
- Update to 2.7.2

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jul  4 2000 Jakub Jelinek <jakub@redhat.com>
- Rebuild with new C++

* Sun Jun 18 2000 Matt Wilson <msw@redhat.com>
- user infodir and mandir macros for FHS
- use %%makeinstall

* Fri Feb  4 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild to gzip manpage
- don't use CC=egcs
- fix description

* Wed Mar 24 1999 Cristian Gafton <gafton@redhat.com>
- added patches for egcs from UP

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Thu Oct 29 1998 Bill Nottingham <notting@redhat.com>
- patch for latest egcs

* Sat Oct 10 1998 Cristian Gafton <gafton@redhat.com>
- strip binary

* Tue Jul 28 1998 Jeff Johnson <jbj@redhat.com>
- create.
