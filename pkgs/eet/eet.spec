%global momorel 1

Name: eet
Version: 1.7.7
Release: %{momorel}m%{?dist}
Summary: Library for speedy data storage, retrieval, and compression

Group: System Environment/Libraries
License: Modified BSD
URL: http://www.enlightenment.org/
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: eet-fix-DSO.patch 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext pkgconfig doxygen chrpath 
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: zlib-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: eina-devel >= 1.7.7
Requires: libjpeg
Requires: zlib

%description
Eet is a tiny library designed to write an arbitary set of chunks of
data to a file and optionally compress each chunk (very much like a
zip file) and allow fast random-access reading of the file later
on. It does not do zip as a zip itself has more complexity than is
needed, and it was much simpler to implement this once here.

It also can encode and decode data structures in memory, as well as
image data for saving to eet files or sending across the network to
other machines, or just writing to arbitary files on the system. All
data is encoded in a platform independent way and can be written and
read by any architecture.


%package devel
Summary: Eet headers, static libraries, documentation and test programs
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release} pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q
%patch0 -p0 

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p' transform='s,x,x,'
chrpath --delete %{buildroot}%{_bindir}/%{name}
find %{buildroot} -name '*.la' -delete
chrpath --delete %{buildroot}%{_libdir}/libeet.so.%{version}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/%{name}
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/examples

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7
- rebuild against gnutls-3.2.0

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.0-1m)
- update to 1.0 release of core EFL

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-2m)
- rebuild against libjpeg-8a

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-2m)
- rebuild against libjpeg-7

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu May 21 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.0-4m)
- define __libtoolize  (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-1m)
- update

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- merge from T4R

* Fri May 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.10.037-0.20061231.3m)
- rebuild against gcc43

* Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.10.042-1m)
- update to 0.9.10.042
- delete %%{_bindir}/eet-config
- add %%{_bindir}/eet

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.10.037-0.20061231.2m)
- %%NoSource -> NoSource

* Mon Jan  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.10.037-0.20061231.1m)
- version 0.9.10.037-20061231

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.10.022-0.20051209.1m)
- version 0.9.10.022-20051209

* Tue Jun 28 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.10.010-0.20050627-1m)
- version 0.9.10.010-20050627

* Thu Feb 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9-0.20050215-1m)
- version 0.9.9-0.20050215

* Mon Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.9-0.20041218-2m)
- enable x86_64.

* Tue Dec 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9-0.20041218-1m)
- first import to Momonga

* Sat Jun 23 2001 The Rasterman <raster@rasterman.com>
- Created spec file
