%global momorel 2

Summary: fast FrameBuffer based TERMinal emulator for linux
Name: fbterm
Version: 1.7.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://code.google.com/p/fbterm/
Source: http://fbterm.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: freetype-devel
BuildRequires: fontconfig-devel
BuildRequires: gpm-devel
BuildRequires: ncurses >= 5.9-10m

%description
FbTerm is a fast terminal emulator for linux with frame buffer
device. Features include:

* mostly as fast as terminal of linux kernel while accelerated
scrolling is enabled on framebuffer device
* select font with fontconfig and draw text with freetype2, same as
Qt/Gtk+ based GUI apps
* dynamicly create/destroy up to 10 windows initially running default
shell
* record scrollback history for every window
* auto-detect current locale and convert text encoding, support double
width scripts like Chinese, Japanese etc
* switch between configurable additional text encoding with hot keys
on the fly
* copy/past selected text between windows with mouse when gpm server
is running

%prep
%setup -q -n %{name}-1.7

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*

%changelog
* Sun Oct 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-2m)
- fbterm requires patched ncurses to build

* Fri Oct 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.0-1m)
- update to 1.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-2m)
- full rebuild for mo7 release

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-2m)
- rebuild against rpm-4.6

* Sat Dec 27 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3-1m)

* Wed Sep 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1-1m)
- initial build
