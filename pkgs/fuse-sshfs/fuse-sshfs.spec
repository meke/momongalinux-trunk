%global momorel 1
%global srcname sshfs-fuse

Summary: mounting the filesystem is as easy as logging into the server with ssh
Name: fuse-sshfs
Version: 2.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Kernel
URL: http://fuse.sourceforge.net/sshfs.html
Source0: http://dl.sourceforge.net/sourceforge/fuse/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: fuse
BuildRequires: fuse-devel
Provides: sshfs = %{version}-%{release}
Obsoletes: sshfs < %{version}-%{release}

%description
This is a filesystem client based on the SSH File Transfer Protocol.
Since most SSH servers already support this protocol it is very easy
to set up: i.e. on the server side there's nothing to do.  On the
client side mounting the filesystem is as easy as logging into the
server with ssh.

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog FAQ.txt NEWS README
%{_bindir}/sshfs
%{_mandir}/man1/sshfs.1*

%changelog
* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3-1m)
- update 2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-4m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2-3m)
- rename package from sshfs to fuse-sshfs
- Provides and Obsoletes: sshfs
- License: GPLv2
- remove %%post and %%postun

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2-1m)
- update 2.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7-3m)
- %%NoSource -> NoSource

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-2m)
- revised spec for debuginfo

* Fri Feb 23 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.7-1m)
- initial commit
