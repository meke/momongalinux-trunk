%global momorel 4

Name:           swingx
Version:        0.9.4
Release:        %{momorel}m%{?dist}
Summary:        A collection of Swing components
License:        LGPLv2
Group:          Development/Libraries
Url:            https://swingx.dev.java.net/
Source0:        https://swingx.dev.java.net/files/documents/2981/110622/%{name}-%{version}-src.zip
# Remove external dependency that's now included in JDK 1.6
# See http://forums.java.net/jive/thread.jspa?messageID=318384
Patch0:         swingx-0.9.4-LoginService.patch
# Remove build dependencies on included binary jars and add system jars
# Remove main class from manifest
Patch1:         swingx-0.9.4-project-properties.patch
# Don't do the "demo taglet" stuff
Patch2:         swingx-0.9.4-swinglabs-build-impl.patch

Requires: java >= 0:1.6.0

BuildRequires:  ant, ant-nodeps
BuildRequires:  java-devel >= 0:1.6.0
BuildRequires:  jpackage-utils >= 0:1.5
BuildRequires:  batik
BuildArch:      noarch
Requires(post): jpackage-utils
Requires(postun): jpackage-utils

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
SwingX contains a collection of powerful, useful, and just plain fun Swing
components. Each of the Swing components have been extended, providing
data-aware functionality out of the box. New useful components have been
created like the JXDatePicker, JXTaskPane, and JXImagePanel.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
%description javadoc
Documentation for the SwingX widgets

%prep
%setup -q -n %{name}-%{version}-src
%patch0 -p1
%patch1 -p1
%patch2 -p1
rm -rf lib/

%build
ant jar javadoc

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}%{_javadocdir}/
cp -r dist/javadoc ${RPM_BUILD_ROOT}%{_javadocdir}/%{name}-%{version}
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p dist/%{name}.jar  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar
%add_to_maven_depmap org.jdesktop.swingx %{name} %{version} JPP %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_javadir}/*.jar
%config(noreplace) %{_mavendepmapfragdir}/%{name}

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-2m)
- full rebuild for mo7 release

* Fri Feb 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-1m)
- import from Fedora 13

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.4-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.4-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Dec  9 2008 Mary Ellen Foster <mefoster at gmail.com> - 0.9.4-5
- Add dist to version
- Fix BuildRoot to follow the latest guidelines

* Mon Nov 24 2008 Mary Ellen Foster <mefoster at gmail.com> - 0.9.4-4
- Add comments explaining the patches
- Update Summary following discussion on fedora-devel list

* Thu Nov  6 2008 Mary Ellen Foster <mefoster at gmail.com> - 0.9.4-3
- Flag Maven depmap as "config"

* Wed Nov  5 2008 Mary Ellen Foster <mefoster at gmail.com> - 0.9.4-2
- Fix cosmetic issues in spec file
- Add ant-nodeps to BuildRequires

* Tue Nov  4 2008 Mary Ellen Foster <mefoster at gmail.com> - 0.9.4-1
- Initial package
