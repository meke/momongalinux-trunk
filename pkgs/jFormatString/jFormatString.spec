%global momorel 5

%define with_gcj %{!?_without_gcj:1}%{?_without_gcj:0}

Name:           jFormatString
Version:        0
Release:        0.0.20081016.%{momorel}m%{?dist}
Summary:        Java format string compile-time checker

Group:          Development/Libraries
License:        "GPLv2 with exceptions"
URL:            https://jformatstring.dev.java.net/
# There has been no official release yet.  This is a snapshot of the Subversion
# repository as of 16 Oct 2008.  Use the following commands to generate the
# tarball:
#   svn export -r 8 https://jformatstring.dev.java.net/svn/jformatstring/trunk \
#     jformatstring --username guest
#   (The password is "guest".)
#   tar -cjvf jFormatString-0.tar.bz2 jformatstring
Source0:        %{name}-%{version}.tar.bz2
# This patch has not been sent upstream, since it is Fedora specific.  This
# gives the build system the path to the appropriate junit jar.
Patch0:         %{name}-build.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ant, java-devel, java-javadoc, jpackage-utils, junit4
Requires:       java, jpackage-utils

%if %{with_gcj}
BuildRequires:  java-gcj-compat-devel >= 1.0.31
Requires(post): java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
%else
BuildArch:      noarch
%endif

%description
This project is derived from Sun's implementation of java.util.Formatter.  It
is designed to allow compile time checks as to whether or not a use of a
format string will be erroneous when executed at runtime.

%package javadoc
Summary:        Javadoc documentation for %{name}
Group:          Documentation
Requires:       %{name} = %{version}-%{release}, java-javadoc

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n jformatstring
%patch0 -p1

%build
# Build the JAR
cd jFormatString
ant
cd ..

# Create the javadocs
mkdir docs
javadoc -d docs -source 1.5 -sourcepath jFormatString/src/java \
  -classpath jFormatString/build/classes:%{_javadir}/junit4.jar \
  -link file://%{_javadocdir}/java edu.umd.cs.findbugs.formatStringChecker

%install
rm -rf $RPM_BUILD_ROOT

# JAR files
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p %{name}/build/%{name}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

# Javadocs
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -rp docs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

# Precompiled bits
%if %{with_gcj}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]; then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]; then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(-,root,root,-)
%doc www/index.html jFormatString/LICENSE
%{_javadir}/%{name}*
%if %{with_gcj}
%{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.0.20081016.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.0.20081016.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.0.20081016.3m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.0.20081016.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.0.20081016.1m)
- import from Fedora 11 for findbugs

* Thu Mar  5 2009 Jerry James <loganjerry@gmail.com> - 0-0.2.20081016svn
- Clean up minor issues raised in package review

* Tue Dec  9 2008 Jerry James <loganjerry@gmail.com> - 0-0.1.20081016svn
- Initial RPM
