%global momorel 1

Summary: a set of backgrounds packaged with the GNOME desktop
Name: gnome-backgrounds
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://gnome-backgrounds.sourceforge.net

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl-XML-Parser
BuildArchitectures: noarch
BuildRequires: homura-backgrounds-gnome
Requires: homura-backgrounds-gnome

# TODO homura-backgrounds-gnome is now default ?
#BuildRequires: momonga-backgrounds >= 8.0.0
#Requires: momonga-backgrounds >= 8.0.0
Requires: momonga-logos >= 7.994


Obsoletes: momonga-desktop

%description
This module contains a set of backgrounds packaged with the GNOME desktop.

%prep
%setup -q

%build
%configure 
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-, root, root)
%doc NEWS README AUTHORS ChangeLog COPYING
%{_datadir}/gnome-background-properties/*
%{_datadir}/backgrounds/*

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Fri Feb 10 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-2m)
- change wallpaper package name

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- fix %%files

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-4m)
- full rebuild for mo7 release

* Wed Aug 18 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-3m)
- remove momonga-backgrounds

* Sat Aug 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- add Req: natsuki-backgrounds-gnome

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-2m)
- fix conflict dir

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Wed Jul 15 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.24.1-4m)
- remove default backgrounds images

* Sun Jun 28 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.24.1-3m)
- change default background image for Momonga Linux 6 alpha

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- release %%{_datadir}/backgrounds for filesystem-2.4.21

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Sun Aug 31 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-6m)
- fix %%files

* Thu Aug 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.0-5m)
- change default background image for Momonga Linux 5

* Sun Jul 20 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.0-4m)
- change default background image for Momonga Linux 5 beta

* Mon Jun 16 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.0-3m)
- change default background image for Momonga Linux 5 alpha

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Jul 31 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.3-4m)
- modify momonga-backgrounds.xml
-    remove MO2 and MO3 default image entry
-    add bluevortex, sunflower, sunsetwave image entry

* Sun Jul 15 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.3-3m)
- modify momonga-backgrounds.xml (add wide display images)

* Thu Jul  5 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.3-2m)
- change default background inage
- modify momonga-backgrounds.xml

* Mon Jul  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update 2.16.2

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Wed Jul 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2.1-3m)
- modify momonga-backgrounds.xml
- add background image entry (mobox, moglassflower)

* Wed Jun  7 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2.1-2m)
- add default image file
- add SOURCE1

* Thu Jun  1 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2.1-1m)
- update 2.14.2.1

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update 2.14.0

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Mon Nov 21 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- start
- GNOME 2.12.1 Desktop
