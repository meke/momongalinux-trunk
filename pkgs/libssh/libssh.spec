%global         momorel 1
%global         major 0.6
%global         minor 3
%global         attach_no 87

Name:           libssh
Version:        %{major}.%{minor}
Release:        %{momorel}m%{?dist}
Summary:        A library implementing the SSH2 protocol (0xbadc0de version)
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.libssh.org/
Source0:        https://red.libssh.org/attachments/download/%{attach_no}/%{name}-%{version}.tar.xz
NoSource:       0
Patch0:         %{name}-%{version}-no-pedantic.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  zlib-devel   

%description
The SSH library with
-Full C library functions for manipulating a client-side SSH connection                                                                     
-Fully configurable sessions                                                                                                                
-Support for AES-128,AES-192,AES-256,blowfish, in cbc mode                                                                                  
-use multiple SSH connections in a same process, at same time.                                                                              
-usable SFTP implementation                                                                                                                 
-Public key and password authentication                                                                                                     

%package devel
Summary:         Development files for %{name}
Group:           Development/Libraries
Requires:         %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q
%patch0 -p1 -b .no-pedantic

%build
mkdir -p %{_target_platform}
pushd %{_target_platform} 
%{cmake} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

find %{buildroot} -name '*.la' -exec rm -f {} + 

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS BSD COPYING ChangeLog INSTALL README
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/cmake/%{name}-config-version.cmake
%{_libdir}/cmake/%{name}-config.cmake
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/pkgconfig/%{name}_threads.pc

%changelog
* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- [SECURITY] CVE-2014-0017
- update to 0.6.3

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-1m)
- [SECURITY] CVE-2013-0176
- update to 0.5.4

* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- [SECURITY] CVE-2012-4559 CVE-2012-4560 CVE-2012-4561 CVE-2012-4562
- update to 0.5.3

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Thu Sep 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Fri Jun  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Mon May 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.91-1m)
- update to 0.5.0rc1 (0.4.91)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-2m)
- rebuild for new GCC 4.6

* Mon Mar 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.8-1m)
- update to 0.4.8

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.7-1m)
- update to 0.4.7

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-2m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.6-1m)
- update to 0.4.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.5-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.5-1m)
- update to 0.4.5

* Sun Jun 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Wed May 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-2m)
- rebuild against openssl-1.0.0

* Sun Feb 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-2m)
- delete __libtoolize hack

* Sat Dec 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Thu Nov 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.92-1m)
- update to 0.3.92

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.91-1m)
- import from Fedora devel and version up to 0.3.91

* Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 0.2-4
- rebuilt with new openssl

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jun 02 2009 Jan F. Chadima <jchadima@redhat.com> - 0.2-2
- Small changes during review

* Mon Jun 01 2009 Jan F. Chadima <jchadima@redhat.com> - 0.2-1
- Initial build
