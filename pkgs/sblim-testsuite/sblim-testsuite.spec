%global momorel 5

%define debug_package %{nil}

Name:           sblim-testsuite
Version:        1.2.5
Release:        %{momorel}m%{?dist}
Summary:        SBLIM testsuite

Group:          Applications/System
License:        CPL
URL:            http://sblim.wiki.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/sblim/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       perl >= 5.6
Requires:       sblim-wbemcli >= 1.5

%description
SBLIM automated testsuite scripts.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc %{_datadir}/doc/%{name}-%{version}
%{_datadir}/%{name}
%{_localstatedir}/lib/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-1m)
- sync with Fedora 11 (1.2.5-3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-2m)
- rebuild against rpm-4.6

* Wed Sep 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.4-1m)
- import from Fedora

* Thu Oct 28 2005 Viktor Mihajlovski <mihajlov@de.ibm.com> 1.2.4-1
  - New release

* Thu Jul 28 2005 Viktor Mihajlovski <mihajlov@de.ibm.com> 1.2.3-0
  - Updates for rpmlint complaints
