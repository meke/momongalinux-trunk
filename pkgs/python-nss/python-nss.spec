# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%global momorel 1
%global build_api_doc 1

Name:           python-nss
Version:        0.14.0
Release:        %{momorel}m%{?dist}
Summary:        Python bindings for Network Security Services (NSS)

Group:          Development/Languages
License:        MPLv1.1 or GPLv2+ or LGPLv2+
URL:            :pserver:anonymous@cvs-mirror.mozilla.org:/cvsroot/mozilla/security/python/nss
Source0:        ftp://ftp.mozilla.org/pub/mozilla.org/security/python-nss/releases/PYNSS_RELEASE_0_14_0/src/python-nss-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch1: python-nss-0.14.1.patch

%define docdir %{_docdir}/%{name}-%{version}

BuildRequires: python-devel >= 2.7
BuildRequires: python-setuptools-devel
BuildRequires: epydoc
BuildRequires: python-docutils
BuildRequires: nspr-devel
BuildRequires: nss-devel


%description
This package provides Python bindings for Network Security Services
(NSS) and the Netscape Portable Runtime (NSPR).

NSS is a set of libraries supporting security-enabled client and
server applications. Applications built with NSS can support SSL v2
and v3, TLS, PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME, X.509 v3
certificates, and other security standards. Specific NSS
implementations have been FIPS-140 certified.

%package doc
Group: Documentation
Summary: API documentation and examples
Requires: %{name} = %{version}-%{release}

%description doc
API documentation and examples

%prep
%setup -q

%patch1 -p1 

%build
CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing" %{__python} setup.py build
%if %build_api_doc
%{__python} setup.py build_doc
%endif


%install
rm -rf %{buildroot}
%{__python} setup.py install  -O1 --install-platlib %{python_sitearch} --skip-build --root $RPM_BUILD_ROOT
%{__python} setup.py install_doc --docdir %{docdir} --skip-build --root $RPM_BUILD_ROOT

# Remove execution permission from any example/test files in docdir
find $RPM_BUILD_ROOT/%{docdir} -type f | xargs chmod a-x

# Set correct permissions on .so files
chmod 0755 $RPM_BUILD_ROOT/%{python_sitearch}/nss/*.so

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{python_sitearch}/*
%doc %{docdir}/ChangeLog
%doc %{docdir}/LICENSE.gpl
%doc %{docdir}/LICENSE.lgpl
%doc %{docdir}/LICENSE.mpl
%doc %{docdir}/README

%files doc
%defattr(-,root,root,-)
%doc %{docdir}/examples
%doc %{docdir}/test
%if %build_api_doc
%doc %{docdir}/api
%endif

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.0-1m)
- update 0.14.0

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-1m)
- initial commit Momonga Linux . import from fedora

