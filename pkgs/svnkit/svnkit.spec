%global momorel 4

%define svn_revision     6648

%define eclipse_name     eclipse
%define eclipse_base     %{_libdir}/%{eclipse_name}
%define install_loc      %{_datadir}/eclipse/dropins
%global local_dropins    %{install_loc}/svnkit/eclipse
%global local_plugins    %{local_dropins}/plugins
%global local_features   %{local_dropins}/features
%global core_plugin_name org.tmatesoft.svnkit_%{version}
%global core_plugin_dir  %{local_plugins}/%{core_plugin_name}
%global jna_plugin_name  com.sun.jna_3.2.3
%global jna_plugin_dir   %{local_plugins}/%{jna_plugin_name}

Name:           svnkit
Version:        1.3.3
Release:        %{momorel}m%{?dist}
Summary:        Pure Java Subversion client library

Group:          Development/Tools
# License located at http://svnkit.com/license.html
License:        "TMate License" and "ASL 1.1"
URL:            http://www.svnkit.com/
# original source located at: http://www.svnkit.com/org.tmatesoft.svn_%{version}.src.zip
# repackaged removing binary dependencies using:
# zip $FILE -d \*.jar
Source0:        org.tmatesoft.svn_%{version}.src-CLEAN.zip
Patch0:         svnkit-1.3.3-dependencies.patch
Patch1:         svnkit-1.3.3-ISVNStatusFileProvider.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:          ant
BuildRequires:          jpackage-utils >= 0:1.6
BuildRequires:          eclipse >= 3.5.2-1m
Requires:               eclipse >= 3.5.2-1m

BuildRequires:          subversion-javahl >= 1.5
Requires:               subversion-javahl >= 1.5
BuildRequires:          jna >= 3.0
BuildRequires:          trilead-ssh2 >= 213
BuildRequires:          sqljet
BuildRequires:          antlr3-java
Requires:               jna >= 3.0
Requires:               trilead-ssh2 >= 213
Requires:               sqljet
Obsoletes:              javasvn <= 1.1.0


%description
SVNKit is a pure Java Subversion client library. You would like to use SVNKit
when you need to access or modify Subversion repository from your Java
application, be it a standalone program, plugin or web application. Being a
pure Java program, SVNKit doesn't need any additional configuration or native
binaries to work on any OS that runs Java.

%package javadoc
Summary:        Javadoc for SVNKit
Group:          Documentation

%description javadoc
Javadoc for SVNKit - Java Subversion client library.

%package -n eclipse-svnkit
Summary:        Eclipse feature for SVNKit
Group:          Development/Tools
Requires:       svnkit = %{version}

%description -n eclipse-svnkit
Eclipse feature for SVNKit - Java Subversion client library.


%prep
%setup -q -n %{name}-src-%{version}.%{svn_revision}
%patch0 

# delete the jars that are in the archive
JAR_files=""
for j in $(find -name \*.jar); do
if [ ! -L $j ] ; then
JAR_files="$JAR_files $j"
fi
done
if [ ! -z "$JAR_files" ] ; then
echo "These JAR files should be deleted and symlinked to system JAR files: $JAR_files"
exit 1
fi
find contrib -name \*.jar -exec rm {} \;

# delete src packages for dependencies
rm contrib/trilead/trileadsrc.zip

# relinking dependencies
ln -s /usr/share/java/svn-javahl.jar contrib/javahl
ln -sf %{_javadir}/jna.jar contrib/jna/jna.jar
ln -sf %{_javadir}/trilead-ssh2.jar contrib/trilead/trilead.jar
ln -sf %{_javadir}/sqljet.jar contrib/sqljet/sqljet.jar
ln -sf %{_javadir}/antlr3-runtime.jar contrib/sqljet/antlr-runtime-3.1.3.jar

# fixing wrong-file-end-of-line-encoding warnings
sed -i 's/\r//' README.txt doc/javadoc/package-list
find doc/javadoc -name \*.html -exec sed -i 's/\r//' {} \;


%build
ECLIPSE_HOME=%{eclipse_base} ant

%install
rm -rf $RPM_BUILD_ROOT

# jar
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 build/lib/%{name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
install -m 644 build/lib/%{name}-javahl.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-javahl-%{version}.jar

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr doc/javadoc/* \
  $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

# eclipse
mkdir -p $RPM_BUILD_ROOT%{local_dropins}
cp -R build/eclipse/features $RPM_BUILD_ROOT%{local_dropins}

# extracting plugin jars
mkdir $RPM_BUILD_ROOT%{local_plugins}
unzip build/eclipse/site/plugins/%{jna_plugin_name}.jar -d $RPM_BUILD_ROOT%{jna_plugin_dir}
unzip build/eclipse/site/plugins/%{core_plugin_name}.jar -d $RPM_BUILD_ROOT%{core_plugin_dir}
 
# removing plugin internal jars and sources
rm -f $RPM_BUILD_ROOT%{jna_plugin_dir}/jna.jar
rm -fR $RPM_BUILD_ROOT%{jna_plugin_dir}/com
rm -f $RPM_BUILD_ROOT%{core_plugin_dir}/{svnkitsrc.zip,trilead.jar,svnkit.jar,svnkit-javahl.jar,sqljet.1.0.1.jar,antlr-runtime-3.1.3.jar}

# main library links
pushd $RPM_BUILD_ROOT%{_javadir}/
ln -s %{name}-%{version}.jar %{name}.jar
ln -s %{name}-javahl-%{version}.jar %{name}-javahl.jar
popd

# We need to setup the symlink because the ant copy task doesn't preserve symlinks
# TODO file a bug about this
ln -s %{_javadir}/svn-javahl.jar $RPM_BUILD_ROOT%{core_plugin_dir}
ln -s %{_javadir}/trilead-ssh2.jar $RPM_BUILD_ROOT%{core_plugin_dir}/trilead.jar
ln -s %{_javadir}/svnkit.jar $RPM_BUILD_ROOT%{core_plugin_dir}
ln -s %{_javadir}/jna.jar $RPM_BUILD_ROOT%{jna_plugin_dir}
ln -s %{_javadir}/sqljet.jar $RPM_BUILD_ROOT%{core_plugin_dir}/sqljet.1.0.1.jar
ln -s %{_javadir}/antlr3.jar $RPM_BUILD_ROOT%{core_plugin_dir}/antlr-runtime-3.1.3.jar


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root)
%{_javadir}/*
%doc README.txt changelog.txt


%files -n eclipse-svnkit
%{install_loc}/svnkit


%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-1m)
- sync with Rawhide (1.3.3-3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0 for subversion-1.6.2
- we do not sync with Fedora, because we do not need eclipse stuff

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.4-1m)
- import from Fedora

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.1.4-3
- Autorebuild for GCC 4.3

* Thu Sep 20 2007 Robert Marcano <robert@marcanoonline.com> - 1.1.4-2
- Fix Obsoletes to include javasvn = 1.1.0

* Mon Sep 10 2007 Robert Marcano <robert@marcanoonline.com> - 1.1.4-1
- Update to upstream 1.1.4
- Build for all supported arquitectures 

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 1.1.2-4
- Rebuild for selinux ppc32 issue.

* Mon Jun 18 2007 Robert Marcano <robert@marcanoonline.com> 1.1.2-2
- Package review fixes

* Sun Apr 15 2007 Robert Marcano <robert@marcanoonline.com> 1.1.2-1
- Update to upstream 1.1.2
- Add obsoletes of javasvn

* Tue Feb 06 2007 Robert Marcano <robert@marcanoonline.com> 1.1.1-1
- Rename to svnkit
- Update to SVNKit 1.1.1

* Mon Aug 28 2006 Robert Marcano <robert@marcanoonline.com> 1.1.0-0.3.beta4
- Rebuild

* Thu Aug 03 2006 Robert Marcano <robert@marcanoonline.com> 1.1.0-0.2.beta4
- Fix bad relase tag

* Mon Jul 31 2006 Robert Marcano <robert@marcanoonline.com> 1.1.0-0.beta4
- Update to upstream version 1.1.0.beta4, required by subclipse 1.1.4

* Fri Jul 28 2006 Robert Marcano <robert@marcanoonline.com> 1.0.6-2
- Rebuilt to pick up the changes in GCJ (bug #200490)

* Mon Jun 26 2006 Robert Marcano <robert@marcanoonline.com> 1.0.6-1
- Update to upstream version 1.0.6

* Sun Jun 25 2006 Robert Marcano <robert@marcanoonline.com> 1.0.4-4
- created javadoc subpackage
- dependency changed from ganymed to ganymed-ssh2

* Sun Jun 11 2006 Robert Marcano <robert@marcanoonline.com> 1.0.4-3
- rpmlint fixes and debuginfo generation workaround
- doc files added

* Sun May 28 2006 Robert Marcano <robert@marcanoonline.com> 1.0.4-2
- review updates

* Sun May 07 2006 Robert Marcano <robert@marcanoonline.com> 1.0.4-1
- initial version
