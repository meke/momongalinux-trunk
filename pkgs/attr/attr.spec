%global momorel 1
Summary: Utilities for managing filesystem extended attributes
Name: attr
Version: 2.4.47
Release: %{momorel}m%{?dist}
Conflicts: xfsdump < 2.0.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://download.savannah.gnu.org/releases-noredirect/attr/attr-%{version}.src.tar.gz
NoSource: 0

%{?include_specopt}
%{?!do_test: %global do_test 1}

# silence compile-time warnings
Patch1: 0001-attr-2.4.47-warnings.patch

# install /etc/xattr.conf
Patch2: 0002-attr-2.4.47-xattr-conf.patch

# refer to ENODATA instead of ENOATTR in man pages (#1055933)
Patch3: 0003-attr-2.4.47-man-ENOATTR.patch

License: GPLv2+
URL: http://acl.bestbits.at/
Group: System Environment/Base
BuildRequires: gettext
BuildRequires: libtool
Requires: libattr = %{version}-%{release}

%description
A set of tools for manipulating extended attributes on filesystem
objects, in particular getfattr(1) and setfattr(1).
An attr(1) command is also provided which is largely compatible
with the SGI IRIX tool of the same name.

%package -n libattr
Summary: Dynamic library for extended attribute support
Group: System Environment/Libraries
License: LGPLv2+

%description -n libattr
This package contains the libattr.so dynamic library which contains
the extended attribute system calls and library functions.

%package -n libattr-devel
Summary: Extended attribute shared libraries and headers
Group: Development/Libraries
License: LGPLv2+
Requires: libattr = %{version}-%{release}

%description -n libattr-devel
This package contains the libraries and header files needed to
develop programs which make use of extended attributes.
For Linux programs, the documented system call API is the
recommended interface, but an SGI IRIX compatibility interface
is also provided.

Currently only ext2, ext3 and XFS support extended attributes.
The SGI IRIX compatibility API built above the Linux system calls is
used by programs such as xfsdump(8), xfsrestore(8) and xfs_fsr(8).

You should install libattr-devel if you want to develop programs
which make use of extended attributes.  If you install libattr-devel,
you'll also want to install attr.

%package -n libattr-static
Summary: Extended attribute static libraries
Group: Development/Libraries
License: LGPLv2+
Requires: libattr = %{version}-%{release}
Requires: libattr-devel = %{version}-%{release}

%description -n libattr-static
This package contains the static library of libattr.
%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1

%build
# attr abuses libexecdir
%configure

# uncomment to turn on optimizations
# sed -i 's/-O2/-O0/' libtool include/builddefs
# unset CFLAGS

make %{?_smp_mflags} LIBTOOL="libtool --tag=CC"

%if %{do_test}
%check
if ./setfattr/setfattr -n user.name -v value .; then
    make tests || exit $?

    # FIXME: root-tests are not ready for the SELinux
    #if test 0 = `id -u`; then
    #    make root-tests || exit $?
    #fi
else
    echo '*** xattrs are probably not supported by the file system,' \
         'the test-suite will NOT run ***'
fi
%endif

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
make install-dev DESTDIR=$RPM_BUILD_ROOT
make install-lib DESTDIR=$RPM_BUILD_ROOT

# get rid of libattr.la
rm -f $RPM_BUILD_ROOT/%{_lib}/libattr.a
rm -f $RPM_BUILD_ROOT/%{_lib}/libattr.la
# dar-2.3.10 still needs libattr.a
#rm -f $RPM_BUILD_ROOT%{_libdir}/libattr.a
rm -f $RPM_BUILD_ROOT%{_libdir}/libattr.la

# drop already installed documentation, we will use an RPM macro to install it
rm -rf $RPM_BUILD_ROOT%{_docdir}/%{name}*

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post -n libattr -p /sbin/ldconfig

%postun -n libattr -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc doc/{CHANGES,COPYING*}
%{_bindir}/attr
%{_bindir}/getfattr
%{_bindir}/setfattr
%{_mandir}/man1/attr.1*
%{_mandir}/man1/getfattr.1*
%{_mandir}/man1/setfattr.1*
%{_mandir}/man5/attr.5*

%files -n libattr-devel
%defattr(-,root,root,-)
%{_libdir}/libattr.so
%{_libdir}/libattr.so
%{_includedir}/attr
%{_mandir}/man2/*attr.2*
%{_mandir}/man3/attr_*.3.*

%files -n libattr
%defattr(-,root,root,-)
%{_libdir}/libattr.so.*
%config(noreplace) %{_sysconfdir}/xattr.conf

%files -n libattr-static
%defattr(-,root,root,-)
%{_libdir}/libattr.a

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.47-1m)
- update to 2.4.47

* Sun Aug 21 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.46-1m)
- update to 2.4.46
- merge from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.44-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.44-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.44-3m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.44-2m)
- revive static library which is needed by dar

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.44-1m)
- update to 2.4.44
- remove static library

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.43-5m)
- attr could be built without removing xfsdump

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.43-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.43-3m)
- fix memleak when exec cp -a (Patch4)
-- https://bugzilla.redhat.com/show_bug.cgi?id=485473

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.43-2m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.43-1m)
- update to 2.4.43
-- drop Patch1,10, not needed
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.41-2m)
- rebuild against gcc43

* Sat Feb 24 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.4.41-1m)
- update 2.4.41

* Tue Jan 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.39-1m)
- update 2.4.39

* Tue Mar 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.32-1m)
- separate libattr and libattr-devel

* Fri Dec 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.32-1m)
- update
- chmod 0755
- update Patch1: attr-2.4.32-shlibexe.patch

* Sun May 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.28-1m)
- update to 2.4.28

* Sun Dec 25 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.20-3m)
- no NoSource

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.20-2m)
- get rid of *.la files, libtool doesn't handle well .a and .so.*
  in separates directories

* Thu May 19 2005 Kazuya Iwamoto <iwapi@momonga-linux.org>
- (2.4.20-1m)
- update to 2.4.20

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (2.4.19-2m)
- enable x86_64.
- libexec isn't appropriate.

* Wed Oct 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.4.19-1m)
  update to 2.4.19

* Fri May 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.16-1m)
- upgrade

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.12-2m)
- revised spec for enabling rpm 4.2.

* Tue Nov 25 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.12-1m)
- update to 2.4.12

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.4.11-1m)
- update to 2.4.11

* Wed Oct  1 2003 zunda <zunda at freeshell.org>
- (2.2.0-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Jun 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.0-2m)
- remove conflicts

* Thu Feb 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0-1m)
  update to 2.2.0

* Tue Nov 26 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.1.1-1m)
- update to 2.1.1 ([Momonga-devel.ja:00833] thanks, nosanosa)
- add BuildPrereq: libtool
- add missing man pages
- add execute permissions to shlibs
- delete NoSource

* Tue Sep  3 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.9-1m)
  update to 2.0.9

* Wed Jul  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.8-1m)

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.0.7-6k)
- omit /sbin/ldconfig in PreReq.

* Sun Apr 28 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.0.7-4k)
- add %{_mandir}/man5/* to %files

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (2.0.7-2k)
- update to 2.0.7

* Fri Apr 12 2002 MATSUDA, Daiki <dyky@dyky.org>
- (2.0.5-2k)
- update to 2.0.5

* Sun Mar 17 2002 Toru Hoshina <t@Kondara.org>
- (2.0.1-4k)
- include 'errno.h' !!!

* Sun Mar 17 2002 MATSUDA, Daiki <dyky@dyky.org>
- (2.0.1-2k)
- update to 2.0.1

* Thu Nov  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1.3-4k)
- modify source URL

* Fri Sep 28 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1.3-2k)
- First Kondarization
