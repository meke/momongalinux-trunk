%global momorel 2

Summary: Convenience library for kernel netlink sockets
Group: Development/Libraries
License: LGPL
Name: libnl
Version: 1.1.4
Release: %{momorel}m%{?dist}
URL: http://people.suug.ch/~tgr/libnl/
Source: http://people.suug.ch/~tgr/libnl/files/libnl-%{version}.tar.gz
NoSource: 0
Patch0: libnl-1.0-pre8-more-build-output.patch
Patch1: libnl-1.1-doc-inlinesrc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen

%description
This package contains a convenience library to simplify
using the Linux kernel's netlink sockets interface for
network manipulation

%package devel
Summary: Libraries and headers for using libnl
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains various headers for using libnl


%prep
%setup -q -n %{name}-%{version}

%patch0 -p1 -b .more-build-output
%patch1 -p1 -b .doc-inlinesrc

# a quick hack to make doxygen stripping builddir from html outputs.
sed -i.org -e "s,^STRIP_FROM_PATH.*,STRIP_FROM_PATH = `pwd`," doc/Doxyfile.in

%build
%configure
%make
%make gendoc

%install
%{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm %{buildroot}%{_libdir}/libnl.a

for l in %{buildroot}%{_libdir}/libnl.so; do
    ln -sf $(echo %{_libdir} | \
        sed 's,\(^/\|\)[^/][^/]*,..,g')/%{_libdir}/$(readlink $l) $l
done

%clean
%{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/%{name}.so.*
%doc COPYING

%files devel
%defattr(-,root,root,0755)
%{_includedir}/netlink/
%doc doc/*
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}-1.pc

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-2m)
- support UserMove env

* Mon Jul 29 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-1m)
- update 1.1.4

* Tue Feb  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-9m)
- add patch
-- remove static lib
- libnl.so.1.1 move to /lib*/

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-6m)
- full rebuild for mo7 release

* Sun Mar 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-6m)
- add any patch. from fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-3m)
- rebuild against gcc43

* Sun Mar  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- fix gcc43

* Mon Jan 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-1m)
- update 1.1

* Sat Jan 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.11.0.8.1m)
- sync fedora

* Fri Jun 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.5.3m)
- add libnl.a. need anaconda-11.1

* Thu May 11 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-0.5.2m)
- import lib64 patch from opensuse

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-0.5.1m)
- import from fc

* Tue Feb 12 2006 Christopher Aillon <caillon@redhat.com> 1.0-0.8.pre5
- Rebuild

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> 1.0-0.7.pre5.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan 16 2006 Christopher Aillon <caillon@redhat.com> 1.0-0.7.pre5
- Add patch to not chown files to root.root during make install; it
  happens normally.

* Mon Jan  9 2006 Christopher Aillon <caillon@redhat.com> 1.0-0.6.pre5
- Correctly install the pkgconfig file

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Nov  2 2005 Christopher Aillon <caillon@redhat.com> 1.0-0.5.pre5
- Fix 64bit LIBDIR issue; use $LIBDIR instead of $PREFIX/lib

* Wed Nov  2 2005 Christopher Aillon <caillon@redhat.com> 1.0-0.4.pre5
- Update to 1.0-pre5

* Tue Nov  1 2005 Christopher Aillon <caillon@redhat.com> 1.0-0.3.pre4
- Update to 1.0-pre4

* Tue Nov  1 2005 Christopher Aillon <caillon@redhat.com> 1.0-0.2.pre3
- Minor specfile cleanup

* Thu Oct 27 2005 Dan Williams <dcbw@redhat.com> 1.0-0.1.pre3
- Split into main and devel packages

* Thu Oct 27 2005 Dan Williams <dcbw@redhat.com> 1.0-0.0.pre3
- initial build
