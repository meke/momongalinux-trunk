%global momorel 22
Summary: Berkeley SPICE 3 Circuit Simulator
Name: spice3
Version: 3f5sfix
Release: %{momorel}m%{?dist}
License: BSD
URL: http://www.ibiblio.org/pub/Linux/apps/circuits/
Group: Applications/Engineering
Source0: http://www.ibiblio.org/pub/Linux/apps/circuits/spice%{version}.tar.gz 
NoSource: 0
Patch0: spice-linux.conf.patch.bz2
Patch1: spice3f5sfix-spice3.patch
Patch2: spice3f5sfix-gcc41.patch
patch3: spice-xpath.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libXaw-devel
Obsoletes: spice = 3f5sfix

%description
SPICE 3 is a general-purpose circuit simulation program for nonlinear
dc, nonlinear transient, and linear ac analyses.  Circuits may contain
resistors, capacitors, inductors, mutual inductors, independent
voltage and current sources, four types of dependent sources,
transmission lines, and the four most common semiconductor devices:
diodes, BJT's, JFET's, and MOSFET's.
#'

This version includes support for the Bsim3 model (V3.1)

%prep
rm -rf --preserve-root %{buildroot}

%setup -n spice%{version} -q

%patch0 -p1
%patch1 -p1 -b .spice3~
%patch2 -p1 -b .gcc41~
%patch3 -p1 -b .xpath~

%if %{_lib} == "lib64"
perl -pi -e "s,lib,lib64," conf/linux
%endif

# use ncurses
sed -i "s/-ltermcap/-lncurses/g" conf/*

%build
CFLAGS="%{optflags}" ./util/build linux gcc

obj/bin/makeidx lib/helpdir/spice.txt

# install has to be done by hand, because we are not assured
# that the builder has root privileges - things would be easier
# if spice had autoconf!

%install
rm -rf --preserve-root %{buildroot}
install -d  %{buildroot}%{_bindir}
install -d  %{buildroot}%{_libdir}/spice3/scripts
install -d  %{buildroot}%{_libdir}/spice3/helpdir

install -d  %{buildroot}%{_libdir}/spice3/examples

install -d  %{buildroot}%{_mandir}/man1
install -d  %{buildroot}%{_mandir}/man3
install -d  %{buildroot}%{_mandir}/man5

install -s obj/bin/spice3 %{buildroot}%{_bindir}
#ln -s spice3 %{buildroot}%{_bindir}/spice
install -s obj/bin/help %{buildroot}%{_bindir}
install -s obj/bin/nutmeg %{buildroot}%{_bindir}
install -s obj/bin/sconvert %{buildroot}%{_bindir}
install -s obj/bin/multidec %{buildroot}%{_bindir}
install -s obj/bin/proc2mod %{buildroot}%{_bindir}

rm lib/make*
cp -r lib/* %{buildroot}%{_libdir}/spice3

install -m 644 man/man1/* %{buildroot}%{_mandir}/man1

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,0755)
%doc readme readme.Linux Linux.changes notes/spice2 
%doc 3f5patches/README*
%{_libdir}/spice3
%{_bindir}/*
%{_mandir}/man1/*


%changelog
* Sat May  7 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3f5sfix-22m)
- rename package spice -> spice3
- add BuildRequires: libXaw-devel

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3f5sfix-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3f5sfix-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3f5sfix-19m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3f5sfix-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3f5sfix-17m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3f5sfix-16m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3f5sfix-15m)
- %%NoSource -> NoSource

* Mon Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3f5sfix-14m)
- no use libtermcap

* Tue Mar 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3f5sfix-13m)
- add patch3 for xorg-7.0

* Thu Jan 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3f5sfix-12m)
- gcc-4.1 build fix

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (3f5sfix-11m)
- rebuild against libtermcap and ncurses

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3f5sfix-10m)
- enable x86_64.

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (3f5sfix-9m)
- rebuild against new environment.
- what a version it is...

* Mon May 20 2002 Kenta MURATA <muraken@kondara.rog>
- (3f5sfix-8k)
- Spice -> spice.

* Sun May 19 2002 Yoshito Ohta <PXL07130@nifty.ne.jp>
- (3f5sfix-6k)
- Change dir.name (Spice -> spice)

* Sat Apr 20 2002 Yoshito Ohta <PXL07130@nifty.ne.jp>
- (3f5sfix-4k)
- fix library directry

* Sat Apr 20 2002 Yoshito Ohta <PXL07130@nifty.ne.jp>
- (3f5sfix-2k)
- release (change version number)

* Tue Apr 16 2002 Yoshito Ohta <PXL07130@nifty.ne.jp>
- (3f5sfix-0.0000001k)
- add URL tag in Source0
- change source type form tar.bz2 to tar.gz

* Tue Apr 16 2002 Yoshito Ohta <PXL07130@nifty.ne.jp>
- (3f5sfix-0.0000000k)
- port from Mandarake

* Mon Apr 08 2002 Lenny Cartier <lenny@mandrakesoft.com> 3f5-4mdk
- patch0: give a fix to hardcoded paths ( appreciate some feedback )

* Thu Mar 21 2002 Lenny Cartier <lenny@mandrakesoft.com> 3f5-3mdk 
- updated to 3f5sfix
- fix files/install section

* Tue Sep 04 2001 Lenny Cartier <lenny@mandrakesoft.com> 3f5-2mdk
- rebuild

* Mon Jan 08 2001 Lenny Cartier <lenny@mandrakesoft.com> 3f5-1mdk
- used srpm from Blaise Tramier <meles@linux-mandrake.com> :
	- initial Mandrake release
	- repackaging to match Mandrake's standards
	- fixed bug in files proceeding (/usr/man/man?/* to /usr/man/man?/*.bz2)
- fix location of man pages
- fix files section
- add url

* Sun Jun 27 1999 Emmanuel Rouat <emmanuel.rouat@wanadoo.fr>

- added 'provides' tag
- fixed silly bug (spice couldn't find its 'help' file)
- updated to full spice3f5 code (includes bsim3 support)

* Thu Feb  4 1999 Emmanuel Rouat <emmanuel.rouat@wanadoo.fr>

- changed version number to 3f5
- added 'Obsoletes: spice' tag
- completed ftp search-path for patches

* Sat Jan 23 1999 Emmanuel Rouat <emmanuel.rouat@wanadoo.fr>

- spec file mostly based on A. Veliath's spec file
- initialization of spec file.

# end of file
