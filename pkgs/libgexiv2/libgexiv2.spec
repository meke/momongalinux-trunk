%global         momorel 1

Name:           libgexiv2
Version:        0.4.1
Release:        %{momorel}m%{?dist}
Summary:        Gexiv2 is a GObject-based wrapper around the Exiv2 library

Group:          System Environment/Libraries
License:        GPLv2
URL:            http://trac.yorba.org/wiki/gexiv2 
Source0:        http://yorba.org/download/gexiv2/0.4/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  exiv2-devel >= 0.23 gobject-introspection-devel libtool

%description
libgexiv2 is a GObject-based wrapper around the Exiv2 library. 
It makes the basic features of Exiv2 available to GNOME applications.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       vala

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
## configure fails with the error:
## configure: Invalid argument --program-prefix=
## http://trac.yorba.org:8000/ticket/2001

echo '%{configure}' | sed '/--program-prefix=/d' >build.tmp
echo 'make %{?_smp_mflags}' >>build.tmp
sh build.tmp

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} LIB=%{_lib}

find %{buildroot} -name '*.la' -exec rm -f {} ';'
find %{buildroot} -name '*.a' -exec rm -f {} ';'
 
%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING MAINTAINERS 
%{_libdir}/libgexiv2.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/gexiv2/
%{_libdir}/libgexiv2.so
%{_libdir}/pkgconfig/gexiv2.pc
%{_datadir}/vala/vapi/gexiv2.vapi

%changelog
* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update 0.4.1
- rebuild against exiv2-0.23

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-2m)
- rebuild against exiv2-0.22

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-1m)
- update 0.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update 0.3.0
- rebuild against exiv2-0.21.1

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.2-1m)
- update 0.2.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-2m)
- full rebuild for mo7 release

* Wed Jul  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.0-1m)
- Initial commit Momonga Linux
