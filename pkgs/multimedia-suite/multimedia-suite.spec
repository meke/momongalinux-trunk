%global momorel 1

Summary:     Meta package for Sound and Video
Name:        multimedia-suite
Version:     8.0
Release:     %{momorel}m%{?dist}
License:     GPL and LGPL and MPL and Modified BSD
Group:       Applications/Multimedia
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:    TiMidity++
%ifnarch ia64 x86_64
# Requires:    HelixPlayer
%endif
Requires:    alsa-utils
Requires:    ardour
Requires:    audacity
Requires:    aumix
Requires:    avidemux
Requires:    cdlabelgen
Requires:    cdparanoia
Requires:    dvd+rw-tools
Requires:    dvdauthor
Requires:    dvdbackup
Requires:    dvdrip
Requires:    dvdstyler
Requires:    dvdwizard
Requires:    easytag
Requires:    genisoimage
Requires:    grip
Requires:    gxine
Requires:    icedax
Requires:    kino
Requires:    last-exit
Requires:    last.fm
Requires:    lastagent
Requires:    lmms
Requires:    lsdvd
Requires:    lxdvdrip
Requires:    mikmod
Requires:    mplayer
Requires:    mplayer-skins
Requires:    mythtv
Requires:    mythtv-frontend
Requires:    mythtv-backend
Requires:    qdvdauthor
Requires:    qtractor
Requires:    sox
Requires:    tovid
Requires:    tvtime
Requires:    vlc
Requires:    vorbis-tools
Requires:    wodim
Requires:    xawtv
Requires:    xcdroast
Requires:    xine-suite
Requires:    xmms
Requires:    xmms-alsa
Requires:    xmms-cdread
Requires:    xmms-esd
Requires:    xmms-flac
%ifnarch ia64 x86_64
Requires:    xmms-goom
%endif
Requires:    xmms-kjofol-skins
Requires:    xmms-mad
Requires:    xmms-mesa
Requires:    xmms-mikmod
Requires:    xmms-modplug
Requires:    xmms-pulse
Requires:    xmms-scrobbler
Requires:    xmms-skins
Requires:    xmms-vorbis
Requires:    xmms-vumeter
Requires:    xmms-xosd
Requires:    zisofs-tools

%description
This is meta package for Sound and Video.
Listed packages are independent of all desktop environments.
(NOT GNOME, NOT KDE, NOT XFCE4)

%files

%changelog
* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0-1m)
- version 8.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0-4m)
- rebuild for new GCC 4.5

* Tue Oct 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0-3m)
- good-bye bmpx

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-2m)
- full rebuild for mo7 release

* Sun Feb  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0-1m)
- version 7.0

* Sat Feb  6 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.0-3m)
- remove HelixPlayer

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0-1m)
- version 6.0

* Sun Jun 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-13m)
- add Requires: qtractor

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-12m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-11m)
- add Requires: last.fm

* Sat Oct 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-10m)
- add Requires: lastagent

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-9m)
- rebuild against gcc43

* Tue Mar 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-8m)
- add Requires: xmms-pulse

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-7m)
- good-bye cdrtools and welcome cdrkit

* Tue Jun  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-6m)
- add Requires: gxine

* Tue May 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-5m)
- xawtv is back to 64bit arch

* Tue May 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-4m)
- exclude xawtv in 64bit arch

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-3m)
- add Requires: ardour
- add Requires: vlc

* Sat Apr 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0-2m)
- exclude xmms-goom in 64bit arch

* Tue Apr 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- initial package for Momonga Linux
