%global momorel 5

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name: python-krbV
Version: 1.0.90
Release: %{momorel}m%{?dist}
Summary: Python extension module for Kerberos 5

Group: Development/Languages
License: LGPLv2

URL: http://people.redhat.com/mikeb/python-krbV
Source: http://people.redhat.com/mikeb/python-krbV/python-krbV-%{version}.tar.bz2

Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: python-devel >= 2.7
BuildRequires: krb5-devel >= 1.2.2
BuildRequires: /bin/awk

%description
python-krbV allows python programs to use Kerberos 5 authentication/security.

%prep
%setup -q

%build
export LIBNAME="%{_lib}"
export CFLAGS="%{optflags} -Wextra"
%configure
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%makeinstall
%{__rm} -f %{buildroot}/%{python_sitearch}/*.la

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README COPYING krbV-code-snippets.py
%{python_sitearch}/krbVmodule.so

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.90-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.90-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.90-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.90-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.90-1m)
- update to 1.0.90

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.13-7m)
- revise for rpm48

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.13-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.13-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.13-4m)
- add %%define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.13-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.13-2m)
- rebuild against python-2.6.1

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.13-1m)
- import from Fedora

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.13-7
- Autorebuild for GCC 4.3

* Tue Aug 21 2007 Mike Bonnet <mikeb@redhat.com> - 1.0.13-6
- rebuild for F8

* Mon Dec 11 2006 Mike Bonnet <mikeb@redhat.com> - 1.0.13-5
- rebuild for python 2.5
- remove obsolete python-abi Requires:

* Wed Sep 13 2006 Mike Bonnet <mikeb@redhat.com> - 1.0.13-4
- support building against krb5-1.5, where the headers have been moved to /usr/include/krb5

* Mon Sep 11 2006 Mike Bonnet <mikeb@redhat.com> - 1.0.13-3
- rebuild for FC6

* Sun May 21 2006 Mike Bonnet <mikeb@redhat.com> - 1.0.13-2
- spec file cleanup

* Wed May 21 2006 Mike Bonnet <mikeb@redhat.com> - 1.0.13-1
- AuthContext.addrs can now be set manually, rather than calling genaddrs()

* Sun May 21 2006 Mike Bonnet <mikeb@redhat.com> - 1.0.12-3
- use macros consistently

* Thu Apr 27 2006 Mike Bonnet <mikeb@redhat.com> - 1.0.12-2
- configure.in: parse version number out of spec file
- add URL tag
- add LGPL text
- remove Requires: krb5-libs, let rpm pick up library dependencies
- bump revision

* Mon Apr 24 2006 Mike Bonnet <mikeb@redhat.com> - 1.0.12-1
- bump version number due to API changes

* Fri Mar 24 2006 Mike Bonnet <mikeb@redhat.com>
- fix typo in error definition
- change the return value of recvauth() from ac to (ac, princ), where princ is the principal sent by sendauth()
- rename the package and reorganize the BuildRequires, to be more Extras-friendly

* Tue Sep 25 2001 Elliot Lee <sopwith@redhat.com>
- Initial version
