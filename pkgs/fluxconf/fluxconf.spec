%global momorel 5

Name:		fluxconf
Version:	0.9.9
Release:	%{momorel}m%{?dist}

Summary:	Configuration utility for fluxbox

Group:		User Interface/Desktops
License:	GPLv2+
URL:		http://devaux.fabien.free.fr/flux
Source0:	http://devaux.fabien.free.fr/flux/fluxconf-0.9.9.tar.gz
NoSource:       0
Patch0:         fluxconf-Makefile.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gtk2-devel
Requires:	fluxbox

%description
Fluxbox graphical configuration utilities:
fluxconf: a general configuration tool
fluxkeys: keyboard shortcut configuration tool
fluxmenu: menu configuration tool using drag & drop

%prep
%setup -q
%patch0

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

cd $RPM_BUILD_ROOT/%{_bindir}
rm -f fluxkeys fluxmenu fluxbare
ln %{name} fluxkeys
ln %{name} fluxmenu
ln %{name} fluxbare

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0755,root,root,0755)
%{_bindir}/*
%defattr(0644,root,root,0755)
%doc ChangeLog README COPYING AUTHORS NEWS docs/*.png docs/*.html
%defattr(0444,root,root)
%lang(fi) %{_prefix}/share/locale/fi/LC_MESSAGES/%{name}.mo
%lang(fr) %{_prefix}/share/locale/fr/LC_MESSAGES/%{name}.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.9-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-1m)
- import from Fedora 11

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.9-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 11 2008 Andreas Bierfert <andreas.bierfert[AT]lowlatency.de> - 0.9.9-5
- Rebuilt for gcc43

* Wed Aug 22 2007 Andreas Bierfert <andreas.bierfert[AT]lowlatency.de>
- 0.9.9-4
- new license tag
- rebuild for buildid

* Wed Sep 13 2006 Andreas Bierfert <andreas.bierfert[AT]lowlatency.de>
0.9.9-3
- FE6 rebuild

* Tue Feb 14 2006 Andreas Bierfert <andreas.bierfert[AT]lowlatency.de>
0.9.9-2
- Rebuild for Fedora Extras 5

* Mon Jan 23 2006 Andreas Bierfert <andreas.bierfert[AT]lowlatency.de>
0.9.9-1
- version upgrade

* Sun Oct 16 2005 Andreas Bierfert <andreas.bierfert[AT]lowlatency.de>
0.9.8-2
- use %lang
- fix #170933
- fix build problem

* Wed Jun 01 2005 Andreas Bierfert <andreas.bierfert[AT]lowlatency.de>
- upgrade to 0.9.8

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Tue Jan 13 2004 Arnaud Abelard <arny@arny.org>
- Initial release
