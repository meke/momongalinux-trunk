%global momorel 9

# install these packages into /opt if we have a prefix defined for the
# java packages
%{?java_pkg_prefix: %define _prefix /opt/frysk }
%{?java_pkg_prefix: %define _sysconfdir %{_prefix}/etc }
%{?java_pkg_prefix: %define _localstatedir %{_prefix}/var }
%{?java_pkg_prefix: %define _infodir %{_prefix}/share/info }
%{?java_pkg_prefix: %define _mandir %{_prefix}/share/man }
%{?java_pkg_prefix: %define _defaultdocdir %{_prefix}/share/doc }

%{!?c_pkg_prefix: %define c_pkg_prefix %{nil}}
%{!?java_pkg_prefix: %define java_pkg_prefix %{nil}}

# FIXME: temporary workaround until bug #202210 is fixed:
%define __perl_provides %{nil}

%define	name_base 	libgtk-java

Summary:	Java bindings for GTK+
Name:		%{java_pkg_prefix}%{name_base}
Version:	2.8.7
Release: 	%{momorel}m%{?dist}
License:	LGPLv2
Group:		Development/Libraries
URL:		http://java-gnome.sourceforge.net
Source:		http://ftp.gnome.org/pub/GNOME/sources/%{name_base}/2.8/%{name_base}-%{version}.tar.bz2
NoSource:	0
Patch0:		libgtk-java-Drawable-region.patch
Patch1:		%{name}-gjavah.patch
Patch2:		libgtk-java-2.8.7-gslice.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:	%{c_pkg_prefix}gtk2 >= 2.8.0
Requires:	%{java_pkg_prefix}cairo-java >= 1.0.5
Requires:	%{java_pkg_prefix}glib-java >= 0.2.6
BuildRequires:  %{java_pkg_prefix}cairo-java-devel >= 1.0.5
BuildRequires:  %{java_pkg_prefix}glib-java-devel >= 0.2.6
BuildRequires:  %{c_pkg_prefix}gtk2-devel >= 2.8.0
BuildRequires:  java-devel >= 1.4.2, gcc-java >= 4.1.1, docbook-utils

%description
libgtk-java is a language binding that allows developers to write GTK
applications in Java.  It is part of Java-GNOME.

%package        devel
Summary:	Compressed Java source files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	gtk2-devel
Requires:	cairo-java-devel
Requires:	glib-java-devel

%description    devel
Development part of %{name}.

%prep

%setup -q -n %{name_base}-%{version}
%patch0
%patch1
%patch2 -p1
touch aclocal.m4
touch configure Makefile.in

%build
# we need POSIX.2 grep
export POSIXLY_CORRECT=1
 
# if either the C or Java packages has a prefix declared, then we will
# add /opt/frysk/lib/pkgconfig to the pkgconfig path
if  [  'x%{java_pkg_prefix}' != 'x' ] || [ 'x%{c_pkg_prefix}' != 'x' ]; then
	export PKG_CONFIG_PATH=/opt/frysk/lib/pkgconfig
fi

# Two workarounds:
# 1) libtool.m4 calls gcj with $CFLAGS and gcj seems to choke on -Wall.
# 2) libtool does not use pic_flag when compiling, so we have to force it.
RPM_OPT_FLAGS=${RPM_OPT_FLAGS/-Wall /}
%configure CFLAGS="$RPM_OPT_FLAGS" GCJFLAGS="-O2 -fPIC" CPPFLAGS=-DGLIB_COMPILATION

make %{?_smp_mflags}

# pack up the java source
find src/java -name \*.java -newer ChangeLog | xargs touch -r ChangeLog
(cd src/java && find . -name \*.java | sort | xargs zip -X -9 src.zip)
touch -r ChangeLog src/java/src.zip

# fix up the modification date of generated documentation
find doc -type f -newer ChangeLog | xargs touch -r ChangeLog

%install
rm -rf %{buildroot}

# The script runExample.sh is both in example_SRCIPTS and example_DATA,
# which is unhealthy.
make %{?_smp_mflags} DESTDIR=$RPM_BUILD_ROOT example_DATA= install

# rename doc dir to reflect package rename, if the names differ
if [ 'x%{name_base}' != 'x%{name}' ] ; then
	mv $RPM_BUILD_ROOT%{_docdir}/%{name_base}-%{version} $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
fi

# Remove unpackaged files:
rm $RPM_BUILD_ROOT/%{_libdir}/*.la

# install the src zip and make a sym link
jarversion=$(echo -n %{version} | cut -d . -f -2)
jarname=$(echo -n %{name_base} | sed 's/-.*//;s/^lib//')
zipfile=$jarname$jarversion-src-%{version}.zip
install -m 644 src/java/src.zip $RPM_BUILD_ROOT%{_datadir}/java/$zipfile
(cd $RPM_BUILD_ROOT%{_datadir}/java &&
  ln -sf $zipfile $jarname$jarversion-src.zip)


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING README NEWS THANKS doc/FAQ.html
%dir %{_includedir}/%{name_base}
%{_includedir}/%{name_base}/*
%{_libdir}/libgtkjava-*.so
%{_libdir}/libgtkjni-*.so
%{_datadir}/java/*.jar
%dir %{_datadir}/%{name_base}
%{_datadir}/%{name_base}/*

%files devel
%defattr(-,root,root)
%doc doc/api doc/examples
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgtkjava.so
%{_libdir}/libgtkjni.so
%{_datadir}/java/*.zip


%changelog
* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.7-9m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.7-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.7-7m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.7-6m)
- fix build failure by adding "export POSIXLY_CORRECT=1"

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.7-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.7-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.7-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.7-2m)
- update Patch2 for fuzz=0
- License: LGPLv2

* Tue May 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.7-1m)
- sync Fedora
- version down

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.2-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.2-2m)
- %%NoSource -> NoSource

* Fri Feb  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2

* Mon Jan  1 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.10.1-2m)
- add "-fPIC" to GCJFLAGS in spec file

* Mon Jan  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- initial build
