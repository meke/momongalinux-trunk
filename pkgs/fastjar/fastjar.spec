%global momorel 8

Summary: The fastjar Java package archiver
Name: fastjar
License: GPLv2+
Group: Development/Tools
Version: 0.97
Release: %{momorel}m%{?dist}
URL: http://savannah.nongnu.org/projects/fastjar/
Source0: http://download.savannah.nongnu.org/releases/fastjar/fastjar-%{version}.tar.gz
NoSource: 0
Patch0: fastjar-0.97-segfault.patch
Patch1: fastjar-CVE-2010-0831.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info

%description
Fastjar is an implementation of Sun's jar utility that comes with the
JDK, written entirely in C, and runs in a fraction of the time while
being 100% feature compatible.
#'

%prep
%setup -q
%patch0 -p1 -b .segfault
%patch1 -p1 -b .CVE-2010-0831

%build
%configure
%make

%install
%makeinstall

rm -f %{buildroot}%{_infodir}/dir

%post
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/%{name}.info

%preun
/sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/%{name}.info

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS README NEWS ChangeLog
%{_bindir}/fastjar
%{_bindir}/grepjar
%{_infodir}/fastjar.info*
%{_mandir}/man1/fastjar.1*
%{_mandir}/man1/grepjar.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-6m)
- full rebuild for mo7 release

* Fri Jun 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-5m)
- [SECURITY] CVE-2010-0831 CVE-2010-2322
- import a security patch from Fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.97-3m)
- add Requires(post): info

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-2m)
- do not specify info file compression format

* Sun Mar 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-1m)
- update to 0.97
-- drop Patch0, merged upstream
-- import Patch1 from Rawhide (gcc-4.4.0-0.28)
- remove Provides:  libgcj:/usr/bin/fastjar

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.92-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.92-3m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr  3 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.92-2m)
- copy from T4R/gcc-test/fastjar r19016


