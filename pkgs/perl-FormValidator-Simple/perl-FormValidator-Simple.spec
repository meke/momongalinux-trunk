%global         momorel 9

Name:           perl-FormValidator-Simple
Version:        0.29
Release:        %{momorel}m%{?dist}
Summary:        Validation with simple chains of constraints
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/FormValidator-Simple/
Source0:        http://www.cpan.org/authors/id/L/LY/LYOKATO/FormValidator-Simple-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Class-Accessor >= 0.22
BuildRequires:  perl-Class-Data-Accessor >= 0.03
BuildRequires:  perl-Class-Data-Inheritable >= 0.04
BuildRequires:  perl-Class-Inspector >= 1.13
BuildRequires:  perl-Date-Calc >= 5.4
BuildRequires:  perl-DateTime-Format-Strptime >= 1.07
BuildRequires:  perl-Email-Valid >= 0.15
BuildRequires:  perl-Email-Valid-Loose >= 0.04
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-MoreUtils >= 0.16
BuildRequires:  perl-List-Util
BuildRequires:  perl-MailTools
BuildRequires:  perl-Tie-IxHash >= 1.21
BuildRequires:  perl-UNIVERSAL-require >= 0.1
BuildRequires:  perl-YAML >= 0.39
Requires:       perl-Class-Accessor >= 0.22
Requires:       perl-Class-Data-Accessor >= 0.03
Requires:       perl-Class-Data-Inheritable >= 0.04
Requires:       perl-Class-Inspector >= 1.13
Requires:       perl-Date-Calc >= 5.4
Requires:       perl-DateTime-Format-Strptime >= 1.07
Requires:       perl-Email-Valid >= 0.15
Requires:       perl-Email-Valid-Loose >= 0.04
Requires:       perl-List-MoreUtils >= 0.16
Requires:       perl-List-Util
Requires:       perl-MailTools
Requires:       perl-Tie-IxHash >= 1.21
Requires:       perl-UNIVERSAL-require >= 0.1
Requires:       perl-YAML >= 0.39
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides you a sweet way of form data validation with simple
constraints chains. You can write constraints on single line for each
input data.

%prep
%setup -q -n FormValidator-Simple-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/FormValidator/Simple.pm
%{perl_vendorlib}/FormValidator/Simple
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-2m)
- rebuild against perl-5.16.0

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- updae to 0.29

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.28-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.28-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.28-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.28-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-2m)
- rebuild against perl-5.10.1

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-2m)
- rebuild against rpm-4.6

* Fri Apr 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22-2m)
- %%NoSource -> NoSource

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.21-3m)
- use vendor

* Mon Mar 26 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21-2m)
- add Date-Calc-version.patch for perl-Date-Calc version compare fail.
  5.51 is eval to 5.005001...

* Sat Feb  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Fri Jan 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.18-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
