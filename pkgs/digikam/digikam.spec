%global momorel 1
%global qtver 4.8.6
%global kdever 4.13.0
%global kdelibsrel 1m
%global kdepimlibsrel 1m
%global kdeedurel 1m
%global kdegraphicsrel 1m
%global kdesdkrel 1m
%global qtdir %{_libdir}/qt4
%global qt3dir %{_libdir}/qt3
%global kdedir /usr
%global docname %{name}-doc
%global docver 0.9.5
%global unstable 0
%if 0%{unstable}
%global ftpdir pub/kde/unstable/%{name}
#global beta -rc
#global prever 5
%else
%global ftpdir pub/kde/stable/%{name}
%endif

Summary: GPhoto2 KDE4 frontend
Name: digikam
Version: 4.0.0
Release: %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License: GPLv2 and LGPLv2 and GFDL
Group: Applications/Multimedia
URL: http://www.digikam.org/
Source0: ftp://ftp.kde.org/%{ftpdir}/%{name}-%{version}%{?beta:%{beta}}.tar.bz2
NoSource: 0
Patch10: kipi-plugins-0.2.0-rc1-gcc44.patch
Patch11: %{name}-3.2.0-beta2-kipi-plugins-acquireimages-desktop.patch
Patch12: %{name}-3.2.0-beta2-kipi-plugins-expoblending-desktop.patch
Patch13: %{name}-3.0.0-beta3-kipi-plugins-panorama-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: marble >= %{kdever}-%{kdeedurel}
Requires: kdegraphics-libs >= %{kdever}-%{kdegraphicsrel}
Requires: ImageMagick >= 6.8.0.10
Requires: gphoto2
Requires: kipi-plugins
Requires: libkdcraw >= %{kdever}
Requires: libkexiv2 >= %{kdever}
Requires: mjpegtools >= 1.6.0
Requires: oxygen-icon-theme
Requires: oxygen-icon-theme-scalable
Requires: sqlite
Requires: qt-sqlite
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdepimlibs-devel >= %{kdever}-%{kdepimlibsrel}
BuildRequires: marble-devel >= %{kdever}-%{kdeedurel}
BuildRequires: kdegraphics-devel >= %{kdever}-%{kdegraphicsrel}
BuildRequires: kdesdk-devel >= %{kdever}-%{kdesdkrel}
BuildRequires: ImageMagick >= 6.8.8.10
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: boost-devel >= 1.48.0
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: gphoto2 >= 2.2.0
BuildRequires: exiv2-devel >= 0.18
BuildRequires: expat-devel
BuildRequires: gdbm-devel >= 1.8.3-7m
BuildRequires: gdk-pixbuf2-devel
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: gphoto2 >= 2.1.5-3m
BuildRequires: imlib2-devel >= 1.2.2-3m
BuildRequires: jasper-devel
BuildRequires: lcms-devel >= 1.15-4m
BuildRequires: lensfun-devel
BuildRequires: libart_lgpl-devel
BuildRequires: libexif-devel >= 0.6.16-2m
BuildRequires: libgphoto2-devel >= 2.5.0
BuildRequires: libgpod-devel >= 0.7.0
BuildRequires: libimobiledevice-devel >= 1.1.5
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libkdcraw-devel >= %{kdever}
BuildRequires: libkexiv2-devel >= %{kdever}
BuildRequires: libkipi-devel >= %{kdever}
BuildRequires: libksane-devel >= %{kdever}
BuildRequires: liblqr-1-devel
BuildRequires: libpgf-devel
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libtool
BuildRequires: libxml2
BuildRequires: libxslt-devel
BuildRequires: mjpegtools-devel >= 1.6.0
BuildRequires: mysql-server
BuildRequires: opencv-devel >= 2.4.5
BuildRequires: pkgconfig
BuildRequires: sed
BuildRequires: usbmuxd-devel >= 1.0.8
BuildRequires: zlib-devel
Obsoletes: %{name}imageplugins

%description
Digikam is a fine KDE interface for gphoto2.
Designed to be a standalone application to preview and download images
from a digital camera on a linux machine.

%package libs
Summary: Shared runtime libraries of digikam
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of digikam.

%package devel
Summary: Header files and development libraries from digikam
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on digikam.

%package doc
Summary: Documentation for digikam
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for digikam.

%package -n kipi-plugins
Summary: KDE image Interface Plugins
Group: System Environment/Libraries
Requires: kipi-plugins-libs = %{version}-%{release}
Requires: ImageMagick
Requires: enblend
Requires: hugin
Requires: k3b
Requires: mjpegtools

%description -n kipi-plugins
The library of the KDE Image Plugin Interface.  
Libkipi allows image applications to use a plugin architecture 
for additional functionality  such as: RawConverter, SlideShow, 
MpegEncoder, ImagesGallery, PrintWizard, CdArchiving...

%package -n kipi-plugins-libs
Summary: Shared runtime libraries of kipi-plugins
Group: System Environment/Libraries

%description -n kipi-plugins-libs
This package contains shared runtime libraries of kipi-plugins.

%package -n kipi-plugins-devel
Summary: Header files and development libraries from kipi-lugins
Group: Development/Libraries
Requires: kipi-plugins = %{version}-%{release}

%description -n kipi-plugins-devel
Libraries and includes files for developing programs based on kipi-plugins.

%prep
%setup -q -n %{name}-%{version}%{?beta:%{beta}}

pushd extra/kipi-plugins
%patch10 -p1 -b .gcc44~
popd
%patch11 -p1 -b .acquireimages-desktop-ja
%patch12 -p1 -b .expoblending-desktop-ja
%patch13 -p1 -b .panorama

# don't use bundled/old FindKipi.cmake in favor of kdelibs' version
# see http:/bugs.kde.org/307213
mv cmake/modules/FindKipi.cmake cmake/modules/FindKipi.cmake.ORIG

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
    -DENABLE_LCMS2=ON \
    -DDIGIKAMSC_USE_PRIVATE_KDEGRAPHICS=OFF \
    -DDIGIKAMSC_COMPILE_PO:BOOL=ON \
    -DDIGIKAMSC_COMPILE_DOC:BOOL=ON \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icons
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/oxygen/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png
ln -s ../icons/oxygen/48x48/apps/showfoto.png %{buildroot}%{_datadir}/pixmaps/showfoto.png

rm -fv %{buildroot}%{_kde4_libdir}/libHUpnp.a
rm -fv %{buildroot}%{_kde4_libdir}/libHUpnpAv.a
rm -fv %{buildroot}%{_kde4_libdir}/libQtSoap.a

%find_lang digikam --with-kde --without-mo
mv digikam.lang digikam-doc.lang
%find_lang showfoto --with-kde --without-mo
mv showfoto.lang showfoto-doc.lang
cat showfoto-doc.lang >> digikam-doc.lang
%find_lang kipi-plugins --with-kde --without-mo
mv kipi-plugins.lang kipi-plugins-doc.lang
cat kipi-plugins-doc.lang >> digikam-doc.lang

%find_lang digikam
%find_lang libkgeomap
cat libkgeomap.lang >> digikam.lang

%find_lang kipiplugins
%find_lang kipiplugin_acquireimages
%find_lang kipiplugin_advancedslideshow
%find_lang kipiplugin_batchprocessimages
%find_lang kipiplugin_calendar
%find_lang kipiplugin_dngconverter
%find_lang kipiplugin_expoblending
%find_lang kipiplugin_facebook
%find_lang kipiplugin_flashexport
%find_lang kipiplugin_flickrexport
%find_lang kipiplugin_galleryexport
%find_lang kipiplugin_gpssync
%find_lang kipiplugin_htmlexport
%find_lang kipiplugin_imageviewer
%find_lang kipiplugin_ipodexport
%find_lang kipiplugin_jpeglossless
%find_lang kipiplugin_kioexportimport
%find_lang kipiplugin_metadataedit
%find_lang kipiplugin_picasawebexport
%find_lang kipiplugin_piwigoexport
%find_lang kipiplugin_printimages
%find_lang kipiplugin_rawconverter
%find_lang kipiplugin_removeredeyes
%find_lang kipiplugin_sendimages
%find_lang kipiplugin_shwup
%find_lang kipiplugin_smug
%find_lang kipiplugin_timeadjust
cat kipiplugin_acquireimages.lang kipiplugin_advancedslideshow.lang \
kipiplugin_batchprocessimages.lang kipiplugin_calendar.lang \
kipiplugin_dngconverter.lang kipiplugin_expoblending.lang \
kipiplugin_facebook.lang kipiplugin_flashexport.lang \
kipiplugin_flickrexport.lang kipiplugin_galleryexport.lang \
kipiplugin_gpssync.lang kipiplugin_htmlexport.lang \
kipiplugin_imageviewer.lang kipiplugin_ipodexport.lang \
kipiplugin_jpeglossless.lang kipiplugin_kioexportimport.lang \
kipiplugin_metadataedit.lang kipiplugin_picasawebexport.lang \
kipiplugin_piwigoexport.lang kipiplugin_printimages.lang \
kipiplugin_rawconverter.lang kipiplugin_removeredeyes.lang \
kipiplugin_sendimages.lang kipiplugin_shwup.lang \
kipiplugin_smug.lang kipiplugin_timeadjust.lang \
kipiplugins.lang >> kipi-plugins.lang

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%post -n kipi-plugins-libs
/sbin/ldconfig

%postun -n kipi-plugins-libs
/sbin/ldconfig

%files -f digikam.lang
%defattr(-, root, root)
%doc core/AUTHORS core/COPYING core/COPYING.LIB core/COPYING-CMAKE-SCRIPTS core/ChangeLog core/INSTALL core/NEWS core/README* core/TODO*
%{_kde4_bindir}/cleanup_%{name}db
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/digitaglinktree
%{_kde4_bindir}/photolayoutseditor
%{_kde4_bindir}/showfoto
%{_kde4_libdir}/kde4/*%{name}*.so
%{_kde4_libexecdir}/%{name}databaseserver
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/applications/kde4/photolayoutseditor.desktop
%{_kde4_datadir}/applications/kde4/showfoto.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/gpssync
%{_kde4_appsdir}/kconf_update/adjustlevelstool.upd
%{_kde4_appsdir}/libkface
%{_kde4_appsdir}/libkgeomap
%{_kde4_appsdir}/photolayoutseditor
%{_kde4_appsdir}/showfoto
%{_kde4_appsdir}/solid/actions/%{name}-opencamera.desktop
%{_kde4_iconsdir}/hicolor/*/apps/*.png
%{_kde4_iconsdir}/hicolor/*/apps/*.svgz
%{_kde4_datadir}/kde4/services/%{name}*.desktop
%{_kde4_datadir}/kde4/services/%{name}*.protocol
%{_kde4_datadir}/kde4/servicetypes/%{name}imageplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/photolayoutseditorborderplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/photolayoutseditoreffectplugin.desktop
%{_kde4_datadir}/config.kcfg/photolayoutseditor.kcfg
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/locale/*/LC_MESSAGES/kipiplugins.mo
%{_datadir}/locale/*/LC_MESSAGES/libkgeomap.mo
%{_mandir}/man1/cleanup_%{name}db.1*
%{_mandir}/man1/digitaglinktree.1*
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/pixmaps/showfoto.png

%files libs
%defattr(-, root, root)
%{_kde4_libdir}/lib%{name}*.so.*
%{_kde4_libdir}/libkface.so.*
%{_kde4_libdir}/libkgeomap.so.*
%{_kde4_libdir}/libkvkontakte.so.*
%{_kde4_libdir}/libmediawiki.so.*

%files devel
%defattr(-, root, root)
%doc core/DESIGN core/HACKING
%{_kde4_includedir}/libkface
%{_kde4_includedir}/libkgeomap
%{_kde4_includedir}/libkvkontakte
%{_kde4_includedir}/libmediawiki
%{_kde4_libdir}/cmake/LibKVkontakte
%{_kde4_libdir}/pkgconfig/libkface.pc
%{_kde4_libdir}/pkgconfig/libkgeomap.pc
%{_kde4_libdir}/pkgconfig/libmediawiki.pc
%{_kde4_libdir}/lib%{name}*.so
%{_kde4_libdir}/libkface.so
%{_kde4_libdir}/libkgeomap.so
%{_kde4_libdir}/libkvkontakte.so
%{_kde4_libdir}/libmediawiki.so
%{_kde4_appsdir}/cmake/modules/FindKGeoMap.cmake
%{_kde4_appsdir}/cmake/modules/FindKface.cmake
%{_kde4_appsdir}/cmake/modules/FindMediawiki.cmake

%files doc -f digikam-doc.lang

%files -n kipi-plugins -f kipi-plugins.lang
%defattr(-, root, root)
%doc extra/kipi-plugins/AUTHORS extra/kipi-plugins/COPYING* extra/kipi-plugins/ChangeLog
%doc extra/kipi-plugins/INSTALL extra/kipi-plugins/NEWS extra/kipi-plugins/README
%doc extra/kipi-plugins/TODO
%{_kde4_bindir}/dngconverter
%{_kde4_bindir}/dnginfo
%{_kde4_bindir}/expoblending
%{_kde4_bindir}/panoramagui
%{_kde4_bindir}/scangui
%{_kde4_libdir}/kde4/kipiplugin_*.so
%{_kde4_datadir}/applications/kde4/dngconverter.desktop
%{_kde4_datadir}/applications/kde4/expoblending.desktop
%{_kde4_datadir}/applications/kde4/kipiplugins.desktop
%{_kde4_datadir}/applications/kde4/panoramagui.desktop
%{_kde4_datadir}/applications/kde4/scangui.desktop
%{_kde4_datadir}/templates/kipiplugins_photolayoutseditor
%{_kde4_appsdir}/kipi/tips
%{_kde4_appsdir}/kipi/kipiplugin_*.rc
%{_kde4_appsdir}/kipiplugin_dlnaexport
%{_kde4_appsdir}/kipiplugin_expoblending
%{_kde4_appsdir}/kipiplugin_flashexport
%{_kde4_appsdir}/kipiplugin_galleryexport
%{_kde4_appsdir}/kipiplugin_htmlexport
%{_kde4_appsdir}/kipiplugin_imageviewer
%{_kde4_appsdir}/kipiplugin_panorama
%{_kde4_appsdir}/kipiplugin_piwigoexport
%{_kde4_appsdir}/kipiplugin_printimages
%{_kde4_appsdir}/kipiplugin_removeredeyes
%{_kde4_iconsdir}/hicolor/*/actions/*.png
%{_kde4_iconsdir}/oxygen/*/apps/*.png
%{_kde4_iconsdir}/oxygen/*/apps/*.svgz
%{_kde4_datadir}/kde4/services/kipiplugin_*.desktop
%{_datadir}/locale/*/LC_MESSAGES/kipiplugin_*.mo
%{_datadir}/locale/*/LC_MESSAGES/libkipi.mo

%files -n kipi-plugins-libs
%defattr(-, root, root)
%{_kde4_libdir}/libkipiplugins.so.*

%files -n kipi-plugins-devel
%defattr(-,root,root,-)
%doc extra/kipi-plugins/HACKING
%{_kde4_libdir}/libkipiplugins.so

%changelog
* Tue May 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to 4.0.0

* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-0.5.1m)
- update to 4.0.0-rc

* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-0.4.2m)
- rebuild against ImageMagick-6.8.8.10

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-0.4.1m)
- update to 4.0.0-beta4
- rebuild against KDE 4.13 RC
- remove old docs

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-0.3.1m)
- update to 4.0.0-beta3

* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-0.2.1m)
- update to 4.0.0-beta2

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-0.1.2m)
- rebuild against KDE 4.12.0

* Thu Dec 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-0.1.1m)
- update to 4.0.0-beta1

* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-2m)
- rebuild against libimobiledevice-1.1.5

* Fri Oct 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-1m)
- update to 3.5.0

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-1m)
- update to 3.4.0

* Tue Aug  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-1m)
- update to 3.3.0

* Wed Jul 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-0.3.1m)
- update to 3.3.0-beta3

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-0.2.2m)
- rebuild against KDE 4.11 beta2

* Thu Jun 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-0.2.1m)
- update to 3.3.0-beta2

* Sat Jun  8 2013 NARITA Koichi <pulsar[momonga-linux.org>
- (3.3.0-0.1.1m)
- update to 3.3.0-beta1

* Tue May 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.2.1m)
- update to 3.2.0-beta2

* Mon Apr  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.1.1m)
- update to 3.2.0-beta1
 
* Thu Mar 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.4.1m)
- update to 3.0.0rc

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.3.2m)
- rebuild against ImageMagick-6.8.0.10

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-0.3.1m)
- update to 3.0.0beta3
- rebuild with KDE 4.10 beta2

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.0-1m)
- update to 2.9.0

* Wed Aug 29 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.0-4m)
- rebuild against libimobiledevice-1.1.4 and usbmuxd-1.0.8

* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-3m)
- rebuild against opencv-2.4.2

* Thu Aug 23 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.0-2m)
- rebuild against libgphoto2-2.5.0
- import libgphoto2_25.patch from Fedora to enable build

* Sun Aug 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- version 2.8.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-4m)
- rebuild with KDE 4.9 RC2

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.0-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (2.7.0-2m)
- rebuild for boost 1.50.0

* Tue Jul 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-1m)
- version 2.7.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-2m)
- rebuild for glib 2.33.2

* Sat Jun  9 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.0-1m)
- version 2.6.0
- update kipi-plugins-panorama-desktop.patch

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-0.3.1m)
- update to 2.6beta3

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-0.2.2m)
- rebuild against libtiff-4.0.1

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-0.2.1m)
- version 2.6.0beta2

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- version 2.5.0
- enable to build with boost-1.48.0

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-1m)
- version 2.4.1

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-3m)
- rebuild against KDE 4.7.90

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-2m)
- rebuild against kdeedu metapackage and marble

* Thu Sep 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Mon Sep 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-2m)
- move new kipi-plugins files from digikam to kipi-plugins
- kipi-plugins are used by many packages, handle with care
- add kipi-plugins-panorama-desktop.patch

* Sat Sep 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- version 2.1.0

* Mon Aug 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-2m)
- rebuild against opencv-2.3.1

* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1m)
- version 2.0.0
- merge kipi-plugins
- update expoblending-desktop.patch

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-4m)
- rebuild against KDE 4.7.0

* Sat Jul  2 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.0-3m)
- rebuild without Marble widget support for KDE 4.6.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-2m)
- rebuild for new GCC 4.6

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- version 1.9.0

* Thu Jan 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.0-1m)
- version 1.8.0

* Wed Dec 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-1m)
- version 1.7.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-4m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-3m)
- rebuild against KDE 4.5.80

* Sat Nov 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-2m)
- import Japanese translation from svn.kde.org, we need it!

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- version 1.6.0

* Tue Oct 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-1m)
- version 1.5.0

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-2m)
- full rebuild for mo7 release

* Mon Aug 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-1m)
- version 1.4.0

* Sun Jul 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.0-4m)
- Requires: qt-sqlite

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-3m)
- rebuild against kdeedu-4.4.95 and kdegraphics-4.4.95

* Sun Jul 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.0-2m)
- split package libs

* Sun Jul 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.0-1m)
- version 1.3.0

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-5m)
- rebuild against qt-4.6.3-1m

* Sun Jun 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-4m)
- modify spec file for smart build

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-3m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against libjpeg-8a

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-1m)
- version 1.2.0

* Mon Feb  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-1m)
- version 1.1.0

* Tue Dec 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-2m)
- set BuildArch: noarch on package doc

* Tue Dec 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-1m)
- version 1.0.0 final

* Thu Dec 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.4.2m)
- rebuild against kdegraphics-4.3.80

* Tue Dec  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.4.1m)
- update to 1.0.0-rc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.3.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.3.1m)
- update to 1.0.0-beta6

* Wed Oct  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.2.1m)
- update to 1.0.0-beta5

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-0.1.2m)
- rebuild against libjpeg-7

* Sun Sep 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.1.1m)
- update to version 1.0.0-beta4

* Fri Jun 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-3m)
- rebuild against KDE 4.2.90

* Tue May 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-2m)
- fix build with new automake

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-1m)
- version 0.10.0

* Sat Mar  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-0.10.2m)
- remove showfoto.png to avoid conflicting with oxygen-icon-theme-4.2.1
- remove showfoto.svgz to avoid conflicting with oxygen-icon-theme-scalable-4.2.1

* Mon Feb 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-0.10.1m)
- update to 0.10.0-rc2

* Tue Jan 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-0.9.1m)
- update to 0.10.0-rc1
- update digikam-doc to 0.9.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.0-0.8.3m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-0.8.2m)
- rebuild against KDE 4.2 RC

* Fri Jan  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-0.8.1m)
- update to 0.10.0-beta8

* Wed Dec 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-0.7.3m)
- version 0.10.0-beta7 dive into the trunk
- build with new kdegraphics
- change Requires: from kipi-plugins4 to kipi-plugins
- remove override-pkgconfig.patch

- * Wed Dec 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (0.10.0-0.7.2m)
- - build with lensfun

- * Sat Dec 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (0.10.0-0.7.1m)
- - update to 0.10.0-beta7

* Mon Dec 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.5-0.2.1m)
- update to 0.9.5-beta2

- * Sat Nov 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (0.10.0-0.6.1m)
- - update to 0.10.0-beta6
- - update override-pkgconfig.patch and %%build section

* Thu Nov 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.5-0.1.1m)
- update to version 0.9.5-beta1
- remove merged libkdcraw016.patch

- * Sun Nov  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (0.10.0-0.5.1m)
- - update to 0.10.0-beta5

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-2m)
- rebuild against libkdcraw-0.1.6

- * Wed Oct 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (0.10.0-0.4.1m)
- - update to 0.10.0-beta4
- - override pkgconfig to find libkdcraw

- * Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (0.10.0-0.3.1m)
- - update to 0.10.0-beta3
- - remove merged mandir.patch

- * Sat Aug  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (0.10.0-0.2.1m)
- - update to 0.10.0-beta2

* Thu Jul 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-1m)
- version 0.9.4

- * Tue Jul  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (0.10.0-0.1.1m)
- - update to 0.10.0-beta1
- - add a package devel

* Tue Jul  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.6.1m)
- update to 0.9.4-rc2

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.5.1m)
- update to 0.9.4-rc1
- remove optflags modify section, -O2 is working with gcc-4.3.1-2m now

* Mon May 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.4.1m)
- update to 0.9.4-beta5
- use -O1 instead of -O2 to avoid hanging up of cc1plus

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.3.4m)
- revise %%{_docdir}/HTML/*/digikam/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.3.3m)
- rebuild against qt3

* Wed May  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.3.2m)
- use -O0 on x86_64 too 

* Wed May  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.3.1m)
- update to 0.9.4-beta4
- use -O0 instead of -O2 on i686 to avoid hanging up of cc1plus

* Fri Apr 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.2.1m)
- update to 0.9.4-beta3
- update digikam-doc to 0.9.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-0.1.2m)
- rebuild against gcc43

* Mon Mar 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.1.1m)
- update to version 0.9.4-beta2
- remove merged 142055.digikam.kde3.patch

* Mon Mar 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-6m)
- rebuild against libkdcraw-devel-0.1.4
- import 142055.digikam.kde3.patch from cooker
 +* Sat Mar 01 2008 Angelo Naselli <anaselli@mandriva.org> 0.9.3-4mdv2008.1
 +- Added a patch to build against new libkdcraw 0.1.4 provided by Gilles Caulier

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-5m)
- move headers to %%{_includedir}/kde

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3-3m)
- %%NoSource -> NoSource

* Thu Jan 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-2m)
- update digikam-doc to 0.9.3 official release

* Sun Dec 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-1m)
- version 0.9.3

* Wed Dec 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-0.4.1m)
- update to 0.9.3-rc1 (tar-ball was changed at Wed Dec 19)

* Sun Dec  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-0.3.1m)
- update digikam and digikam-doc to 0.9.3-beta3

* Sat Nov  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-0.2.1m)
- update to 0.9.3-beta2

* Fri Oct 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-0.1.1m)
- update to version 0.9.3-beta1

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-6m)
- rebuild against libkexiv2-1.1.6 and libkdcraw-1.1.2
- License: GPLv2 and GFDL

* Mon Jul  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-5m)
- remove duplicate BPR

* Thu Jul  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-4m)
- add BuildPreReq: libkdcraw-devel >= 0.1.1

* Fri Jun 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-3m)
- replace tar-ball (from 0.9.2-final to 0.9.2)

* Thu Jun 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-2m)
- split digikam-doc

* Thu Jun 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-1m)
- version 0.9.2
- update digikam-doc to version 0.9.2-beta2
- DigikamImagePlugins have been merged into digiKam
- remove digi_libkexiv2.patch

* Sun May 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-5m)
- rebuild against libkexiv2-0.1.5

* Fri Apr 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-4m)
- rebuild against exiv2-0.14 and libkexiv2-0.1.2-2m
- import libkexiv2 patch from cooker

* Thu Mar 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-3m)
- update digikam-doc to 0.9.1

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-2m)
- clean up spec file

* Mon Mar  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-1m)
- version 0.9.1

* Thu Mar  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-0.3.1m)
- update to 0.9.1-rc2

* Tue Feb 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-0.2.1m)
- update to 0.9.1-rc1

* Wed Feb 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-0.1.1m)
- update to version 0.9.1-beta1

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-2m)
- rebuild against libkipi etc.

* Tue Dec 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-1m)
- version 0.9.0

* Fri Dec  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.2.1m)
- update to 0.9.0-rc2

* Thu Nov 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.1.2m)
- rebuild against exiv2-0.12

* Wed Nov 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.1.1m)
- update to version 0.9.0-rc1

* Fri Aug 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-4m)
- update digikam-doc to 0.8.2-r1

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-3m)
- rebuild against kdelibs-3.5.4-2m
- rebuild against libkexif-0.2.4-2m
- rebuild against libkipi-0.1.4-2m

* Thu Jul 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-2m)
- update digikam-doc to 0.8.2

* Wed Jul 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-1m)
- version 0.8.2

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.2-0.1.3m)
- depend sqlite3 -> sqlite

* Sat Jun 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-0.1.2m)
- fix symlinks

* Fri May 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-0.1.1m)
- update to version 0.8.2 rc1

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-3m)
- revise %%files for KDE 3.5.2

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-2m)
- add --enable-new-ldflags to configure

* Sat Jan 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-1m)
- version 0.8.1
- remove merged gcc41.patch

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-3m)
- add gcc-4.1 patch
-- Patch0: digikam-0.8.0-gcc41.patch

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-2m)
- rebuild against libexif-0.6.12-1m

* Sat Nov 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-1m)
- version 0.8.0
- BuildPreReq: sqlite3-devel
- Requires: sqlite3

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.4-3m)
- rebuild against KDE 3.5 RC1

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.4-2m)
- add --disable-rpath to configure

* Fri Aug 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.4-1m)
- version 0.7.4

* Mon Jul 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-1m)
- initial package for Momonga Linux
- import Summary and %%description from cooker
