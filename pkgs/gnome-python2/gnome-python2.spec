%global momorel 8
%define realname gnome-python
%define py_ver %(python -c 'import sys;print(sys.version[0:3])')
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: PyGNOME Python extension module
Name: gnome-python2
Version: 2.28.1
Release: %{momorel}m%{?dist}
URL: http://download.gnome.org/sources/gnome-python/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/2.28/%{realname}-%{version}.tar.bz2
NoSource: 0

License: LGPL
Group: Development/Languages
Requires: gnome-python2-bonobo
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: pygtk2-devel >= 2.24
BuildRequires: python-devel >= %{py_ver}
BuildRequires: libgnome-devel >= 2.25.1
BuildRequires: pyorbit-devel >= 2.24.0
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: libgnomecanvas-devel >= 2.25.90
BuildRequires: gnome-vfs2-devel >= 2.24.0
BuildRequires: GConf2-devel >= 2.25.2
BuildRequires: libbonobo-devel >= 2.24.1
BuildRequires: libbonoboui-devel >= 2.24.1

BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: nautilus-devel >= 2.25.93
BuildRequires: gnome-panel >= 2.25.92
BuildRequires: libgtkhtml-devel >= 2.11.1
BuildRequires: libgnomeprint22-devel >= 2.18.6
BuildRequires: libgnomeprintui22-devel >= 2.18.4
Requires: GConf2

Obsoletes: %{name}-nautilus
Obsoletes: %{name}-gtkhtml

Provides: %{realname}
Obsoletes: %{realname}

%description
The gnome-python package contains the source packages for the Python
bindings for GNOME called PyGNOME.
 
PyGNOME is an extension module for Python that provides access to the
base GNOME libraries, so you have access to more widgets, a simple
configuration interface, and metadata support.

%package gnome
Summary: Python bindings for libgnome
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: %{name}-gnomevfs = %{version}-%{release}
Requires: libgnome >= %{libgnome_version}
Requires: libgnomeui >= %{libgnomeui_version}

%description gnome
This module contains a wrapper that makes libgnome functionality available
from Python.

%package capplet
Summary: Python bindings for GNOME Panel applets
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

Provides: %{realname}-capplet
Obsoletes: %{realname}-capplet
 
%description capplet
This module contains a wrapper that allows GNOME Control Center
capplets to be in Python.
 
%package canvas
Summary: Python bindings for the GNOME Canvas
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: gtk2
Requires: libgnomecanvas >= 2.12.1
Requires: pygtk2

Provides: %{realname}-canvas
Obsoletes: %{realname}-canvas
 
%description canvas
This module contains a wrapper that allows use of the GNOME Canvas
in Python.
 
%package bonobo
Summary: Python bindings for interacting with Bonobo
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: bonobo-activation
Requires: libbonobo
Requires: libbonoboui
Requires: pyorbit

Provides: %{realname}-bonobo
Obsoletes: %{realname}-bonobo
 
%description bonobo
This module contains a wrapper that allows the creation of bonobo
components and the embedding of bonobo components in Python.
 
%package gconf
Summary: Python bindings for interacting with GConf
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: GConf2

Provides: %{realname}-gconf
Obsoletes: %{realname}-gconf
 
%description gconf
This module contains a wrapper that allows the use of GConf via Python.
 
%package gnomevfs
Summary: Python bindings for interacting with gnome-vfs
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: gnome-vfs2
Requires: libbonobo >= 2.24.1

Provides: %{realname}-gnomevfs
Obsoletes: %{realname}-gnomevfs
 
%description gnomevfs
This module contains a wrapper that allows the use of gnome-vfs via python.

%package devel 
Summary: Development files for building add-on libraries
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: gnome-vfs2-devel
Requires: pkgconfig
Requires: python-devel

%description devel
This package contains files required to build wrappers for GNOME add-on
libraries so that they interoperate with gnome-python2.
 
%prep
%setup -q -n %{realname}-%{version}
 
%build
%configure
%make
 
%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name '*.la' -exec rm {} \;

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS
%dir %{python_sitearch}/gtk-2.0/gnome/

%files gnome
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gnome/__init__.*
%{python_sitearch}/gtk-2.0/gnome/_gnome.so
%{python_sitearch}/gtk-2.0/gnome/ui.so
%{_datadir}/pygtk/2.0/defs/ui.defs
%{_datadir}/pygtk/2.0/defs/gnome.defs
%{_datadir}/pygtk/2.0/defs/gnome-types.defs

%files canvas
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gnome/canvas.*
%{python_sitearch}/gtk-2.0/gnomecanvas.so
%{_datadir}/pygtk/2.0/defs/canvas.defs
%defattr(644,root,root,755)
%doc examples/canvas/*

%files bonobo
%defattr(-,root,root,-)
%dir %{python_sitearch}/gtk-2.0/bonobo/
%{python_sitearch}/gtk-2.0/bonobo/__init__.*
%{python_sitearch}/gtk-2.0/bonobo/*.so
%{_datadir}/pygtk/2.0/defs/bonobo*.defs
%{_datadir}/pygtk/2.0/argtypes/bonobo*
%defattr(644,root,root,755)
%doc examples/bonobo/*

%files gconf
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gconf.so
%{_datadir}/pygtk/2.0/defs/gconf.defs
%{_datadir}/pygtk/2.0/argtypes/gconf*
%defattr(644,root,root,755)
%doc examples/gconf/*

%files gnomevfs
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gnomevfs
%{python_sitearch}/gtk-2.0/gnome/vfs*
%{_libdir}/gnome-vfs-2.0/modules/libpythonmethod.so
%doc %{_datadir}/gtk-doc/html/pygnomevfs
%defattr(644,root,root,755)
%doc examples/vfs/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/gnome-python-2.0
%{_libdir}/pkgconfig/gnome-python-2.0.pc

# old versions did not have .pyc and .pyo files in their file list
# remove them now, or bad things will now happen because of the new
# paths.  This trigger must remain until upgrading from RHL 8.0 is no 
# longer supported.
%triggerun bonobo -- gnome-python2-bonobo < 1.99.14-5
rm -f %{python_sitearch}/bonobo/__init__.{pyc,pyo}

%changelog
* Thu Sep  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-8m)
- remove conflicts dirs

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.28.1-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-5m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-4m)
- delete conflict dirs

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.28.1-3m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.28.1-2m)
- separate gnome

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Mon Mar  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-3m)
- remove BuildPrereq: eel2-devel >= 2.25.91

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun Jun  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-2m)
- comment out Obsolete gnome-python2-gnomeprint
-- (provides: gnome-python2-desktop)

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Sun Mar 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.3-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.22.3-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Fri Aug  8 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.1-2m)
- delete Obsoletes: %%{name}-applet

* Mon Jun 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sun Nov 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Sun Sep 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.2-1m)
- 2.18.2

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.0-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- 2.18.0

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.2-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.2-1m)
- update to 2.17.2 (unstable)

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-5m)
- delete pyc pyo

* Wed Feb  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.3-4m)
- fix gnome-python -> gnome-python2

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-3m)
- rename gnome-python -> gnome-python2

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.2-2m)
- rebuild against python-2.5

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- updateo to 2.16.0

* Fri Jun  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-5m)
- delete duplicated dir

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-4m)
- delete duplicated files

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-3m)
- delete dupulicated file
- add canvas (Requires: %%{name} == %%{version})

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-2m)
- delete duplicated dir

* Wed Apr 12 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.4-1m)
- version 2.12.4

* Fri Nov 26 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.12.1-1m)
- update to 2.12.1
- gnome-python-applet was OBSOLETED
- gnome-python-gtkhtml was OBSOLETED
- gnome-python-gnomeprint was OBSOLETED
- gnome-python-nautilus was OBSOLETED

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.0-2m)
- build against python-2.4.2

* Tue Nov 09 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.0-1m)
- Gnome-Python 2.6.0
- zvt bindings are removed in upstream.

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.0-4m)
- rebuild against python2.3

* Tue Apr  6 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.0.0-3m)
- BuildPrereq: pygtk -> pygtk-devel

* Mon Mar 22 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.0-2m)
- import from Fedora Core 1

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.18-1m)
- version 1.99.18

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.16-1m)
- version 1.99.16

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.15-3m)
- rebuild against for XFree86-4.3.0

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.99.15-2m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.15-1m)
- version 1.99.15

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.14-1m)
- version 1.99.14

* Sun Jan 5 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.99.13-2m)
-  pyorbit obsoletes orbit-python. Use pyorbit.

* Mon Aug 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.13-1m)
- version 1.99.13

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.11-7m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.11-6m)
- rebuild against for gnome-panel-2.0.3

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.11-5m)
- rebuild against for gnome-desktop-2.0.4
- rebuild against for gnome-session-2.0.3

* Fri Jul 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.11-4m)
- rebuild against for eel-2.0.2
- rebuild against for nautilus-2.0.2
- rebuild against for gnome-utils-2.0.1

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.11-3m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.11-2m)
- add BuildPrereq: libzvt-devel
- add BuildPrereq: libgtkhtml-devel
- add BuildPrereq: nautilus-devel

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.11-1m)
- version 1.99.11

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-40k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-38k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-36k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-34k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-32k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-30k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-28k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-26k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-24k)
- rebuild against for gnome-desktop-2.0.0
- rebuild against for gnome-session-2.0.0
- rebuild against for gnome-panel-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-22k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-20k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-18k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-16k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-14k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-12k)
- rebuild against for libgtkhtml-1.99.9
- rebuild against for libzvt-1.117.0
- rebuild against for gdm-2.3.90.5
- rebuild against for yelp-0.10
- rebuild against for eel-1.1.17
- rebuild against for nautilus-1.1.19
- rebuild against for gnome-desktop-1.5.22
- rebuild against for gnome-session-1.5.21
- rebuild against for gnome-panel-1.5.24

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-10k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-8k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-6k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Mon May 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-4k)
- rebuild against for glade-1.1.0
- rebuild against for gnome-desktop-1.5.20
- rebuild against for gnome-panel-1.5.22
- rebuild against for gnome-system-monitor-1.1.7
- rebuild against for libwnck-0.12

* Tue Apr 02 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-10k)
- rebuild against for gnome-desktop-1.5.15
- rebuild against for gnome-panel-1.5.16
- rebuild against for gnome-session-1.5.15

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-8k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-6k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-2k)
- version 1.99.8

* Tue Mar 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-58k)
- rebuild against for gnome-panel-1.5.13

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-56k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-54k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-52k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-50k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-48k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-46k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-44k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-42k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-40k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-38k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Fri Feb 22 2002 Shingo AKagaki <dora@kondara.org>
- (1.99.7-36k)
- move install path to gnome2 because conflicts pygnome1

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-32k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-30k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-28k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-26k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-24k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-22k)
- rebuild against for libzvt-1.111.0
- rebuild against for metatheme-0.9.3

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-20k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-18k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-16k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-14k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-12k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-10k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-8k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-6k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-4k)
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-2k)
- rebuild against for libglade-1.99.6
- rebuild against for gnome-vfs-1.9.5
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13
- rebuild against for libzvt-1.110.0
- rebuild against for libgnomecanvas-1.110.0
- rebuild against for libgnome-1.110.0
- rebuild against for libbonobo-1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-14k)
- rebuild against for linc-0.1.15

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.99.6-2k)
- change package name
- fork pygtk2
- version 1.99.6

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (1.4.1-10k)
- rebuild against libpng 1.2.0.

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- gtkglarea update

* Tue Aug 21 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- revuild against for XFree86-4.1.0

* Sat Jul 21 2001 Shingo Akagaki <dora@kondara.org>
- rebuild against for libcapplet-1.5.0

* Tue Apr 24 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.4.1

* Thu Mar 29 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.0
- K2K

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Mon Apr 10 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.53
- release tag rule changed

* Thu Mar 16 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.52

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file
- version 1.0.51

* Thu Sep 09 1999 Cristian Gafton <gafton@redhat.com>
- added the changelog :-)
- update descriptions
