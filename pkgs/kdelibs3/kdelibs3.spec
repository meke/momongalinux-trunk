%global momorel 20
%global relname FallingStar
%global kde_release %{version}-%{release}.%{relname}
%global srcname kdelibs
%global artsver 1.5.10
%global artsrel 2m
%global qtver 3.3.7
%global qtrel 17m
%global kdever 3.5.10
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global ftpdirver %{kdever}
%global sourcedir stable/%{ftpdirver}/src
%global debug 0
%global with_docs 0
%global enable_final 1
%global enable_gcc_check_and_hidden_visibility 0
%global distname Momonga Linux
%global include_crystalsvg 1

Summary: K Desktop Environment - Libraries
Name: kdelibs3

%{?include_specopt}
%{?!with_cups: %global with_cups 1}

Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2+ or LGPLv2+ or Modified BSD or GFDL
Group: System Environment/Libraries
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{srcname}-%{version}.tar.bz2
NoSource: 0
Source1: devices.protocol
# rc file
Source2: kderc

## patches from fedora
Patch0: %{srcname}-3.2.2-pie.patch
Patch1: %{srcname}-3.0.0-ndebug.patch
Patch2: %{srcname}-3.5.6-resize-icons.patch
Patch3: %{srcname}-3.0.4-ksyscoca.patch
Patch4: %{srcname}-%{version}-openssl.patch
Patch5: %{srcname}-3.4.91-buildroot.patch
Patch6: %{srcname}-3.1-ssl-krb5.patch
Patch7: %{srcname}-3.1-libtool.patch
Patch8: %{srcname}-3.2.3-cups.patch
Patch9: %{srcname}-3.3.2-ppc.patch
Patch10: %{srcname}-3.4.92-inttype.patch
Patch11: %{srcname}-3.5.8-utempter.patch
Patch12: %{srcname}-3.5.7-fixarcjpencode.patch
Patch13: %{srcname}-3.5.7-autostart.patch
Patch14: %{srcname}-3.5.9-cupsserverbin.patch
Patch15: %{srcname}-%{version}-latex-syntax-kile-2.0.3.patch
Patch16: %{srcname}-%{version}-gcc44.patch
Patch17: %{srcname}-%{version}-ossl-1.x.patch

## patch from gentoo
Patch20: %{srcname}-3.5.0-kicker-crash.patch

## patches from cooker
Patch30: %{srcname}-3.5.6-add-dnssd-avahi-support.patch
Patch31: %{srcname}-3.5.8-smooth-scrolling.patch

## patch from arch
Patch40: %{name}-cups16.patch

## old patches up to 3.4.1
Patch50: %{srcname}-3.4.0-rpath.patch
Patch51: %{srcname}-3.1.1-lang.patch
Patch52: %{srcname}-3.0.4-dtfix.patch
Patch53: %{srcname}-3.0-gui_effect-20020415.patch

# use /etc/kde in addition to /usr/share/config, borrowed from debian
Patch70: %{srcname}-3.5.5-kstandarddirs.patch
# automake-1.10, $(all_libraries) is missing from a few LDFLAGS (kde #137675)
Patch71: %{srcname}-%{version}-137675.patch

# no upstream patch

# fix menu
Patch200: %{srcname}-3.5.2-menu-fixcomment.patch

# security fixes
Patch300: kde-khtml-overflow-CVE-2006-4811.patch
Patch310: %{srcname}-%{version}-CVE-2009-2537-select-length.patch
Patch311: %{srcname}-%{version}-CVE-2009-1725.patch
Patch312: %{srcname}-3.5.4-CVE-2009-1687.patch
Patch313: %{srcname}-3.5.4-CVE-2009-1690.patch
Patch314: %{srcname}-%{version}-CVE-2009-1698.patch
Patch315: %{srcname}-%{version}-CVE-2009-2702.patch
Patch316: libltdl-CVE-2009-3736.patch
Patch320: xmlhttprequest_3.x.diff

# misc
Patch400: %{srcname}-%{version}-gcc43.patch
Patch401: %{srcname}-3.5.9-meinproc.patch
Patch402: %{srcname}-3.5.9-inotify-fix.patch
Patch403: %{srcname}-%{version}-automake-version.patch
Patch404: %{srcname}-%{version}-autoconf265.patch
Patch405: %{srcname}-%{version}-gcc45.patch
Patch406: %{srcname}-%{version}-cups150.patch
Patch407: %{srcname}-%{version}-add-missing.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: qt3 >= %{qtver}-%{qtrel}
Requires: arts >= %{artsver}
Requires: kdelibs
Requires: hicolor-icon-theme
Requires: hspell >= 0.9
Requires: jasper
BuildRequires: qt3-devel >= %{qtver}-%{qtrel}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: aspell-devel >= 0.60.4-2m
BuildRequires: audiofile-devel >= 0.2.1
BuildRequires: avahi-qt3-devel >= 0.6.13-3m
BuildRequires: cups-devel >= 1.1.9
BuildRequires: docbook-dtds
BuildRequires: docbook-style-dsssl
BuildRequires: doxygen
BuildRequires: esound-devel >= 0.2.36-4m
BuildRequires: expat-devel >= 2.0.0-2m
BuildRequires: flex >= 2.5.4a-13
BuildRequires: gdbm-devel >= 1.8.3-7m
BuildRequires: hspell-devel >= 0.9
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: jadetex
BuildRequires: jasper-devel >= 1.900.0-2m
BuildRequires: libacl-devel >= 2.2.39-3m
BuildRequires: libart_lgpl-devel >= 2.3.19-2m
BuildRequires: libattr-devel >= 2.4.32-2m
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libutempter-devel
BuildRequires: libxml2-devel >= 2.4.9
BuildRequires: libxslt-devel >= 1.0.9
BuildRequires: openjade
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: pcre-devel >= 8.31
BuildRequires: perl
BuildRequires: perl-SGMLSpm
BuildRequires: pkgconfig
BuildRequires: sgml-common
BuildRequires: zlib-devel
%if %{include_crystalsvg}
Obsoletes: crystalsvg-icon-theme
Obsoletes: kdeartwork-icons-crystalsvg
%endif
%if "%{name}" != "kdelibs" && "%{with_docs}" != "1"
Obsoletes: kdelibs3-apidocs
%endif

%description
The K Desktop Environment (KDE) is a GUI desktop for the X Window
System. The kdelibs package contains a variety of libraries used by
KDE, including: kdecore, the KDE core library; kdeui, a user interface
library; kfm, the file manager; khtmlw, HTML widgets; kfile, for file
access; kspell, the spelling checker; jscript, for javascript; kab,
the addressbook; kimgio, for image manipulation; and arts, for sound,
mixing, and animation.

%package devel
Group: Development/Libraries
Summary: Header files and documentation for compiling KDE applications
Requires: qt3-devel >= %{qtver}, %{name} = %{version}-%{release}, pcre-devel
Obsoletes: %{srcname}-sound-devel
Obsoletes: kdesupport-devel

%description devel
This package includes the header files you will need to compile
applications for KDE. The KDE API documentation is also included in
HTML format.

%if %{with_docs}
%package apidocs
Summary: KDE API documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description apidocs
This package includes the KDE API documentation in HTML
format for easy browsing
%endif

%prep
%setup -q -n %{srcname}-%{version}

# %%patch1 -p1 -b .pie
%if %{debug} == 0
%patch1 -p1 -b .debug
%endif
%patch2 -p1 -b .resize
%patch3 -p1 -b .ksyscoca
%patch4 -p1 -b .openssl
%patch5 -p1 -b .buildroot
%patch6 -p1 -b .ssl-krb5
%patch7 -p1 -b .libtool
%patch8 -p1 -b .cups
%patch9 -p1 -b .ppc
%patch10 -p1 -b .inttype
%patch11 -p1 -b .utempter
%patch12 -p1 -b .fixarcjpencode
%patch13 -p1 -b .xdg-autostart
%patch14 -p1 -b .cupsserverbin
%patch15 -p1 -b .latex-syntax
%patch16 -p1 -b .gcc44
%patch17 -p1 -b .ossl-1.x

%patch20 -p0 -b .fix-kicker-crash

%patch30 -p1 -b .avahi_support
%patch31 -p1 -b .smooth

%patch40 -p1 -b .cups16

%patch50 -p1 -b .rpath
%patch51 -p1 -b .lang
%patch52 -p1 -b .dtfix
%patch53 -p1 -b .gui

%patch70 -p1 -b .kstandarddirs
%patch71 -p1 -b .kde#137675

# no upstream patch

%patch200 -p1 -b .menu

# security fixes
%patch300 -p0 -b .CVE-2006-4811
%patch310 -p1 -b .CVE-2009-2537
%patch311 -p0 -b .CVE-2009-1725
%patch312 -p1 -b .CVE-2009-1687
%patch313 -p1 -b .CVE-2009-1690
%patch314 -p1 -b .CVE-2009-1698
%patch315 -p1 -b .CVE-2009-2702
%patch316 -p1 -b .CVE-2009-3736
pushd khtml/ecma
%patch320 -p0 -b .20091027-1
popd

# misc
%patch400 -p1 -b .gcc43~
%patch401 -p1 -b .meinproc
%patch402 -p1 -b .inotify~
%patch403 -p1 -b .automake
%patch404 -p1 -b .autoconf265
%patch405 -p1 -b .gcc45~
%patch406 -p1 -b .cups150~
%patch407 -p1 -b .add-missing

# Make the version number say what it should say
perl -pi -e "s,^#define KDE_VERSION_STRING .*,#define KDE_VERSION_STRING \"%{kde_release} %{distname}\"," kdecore/kdeversion.h

make -f admin/Makefile.common cvs

%build
%ifarch ppc64
%global optflags %(echo %{optflags} -mminimal-toc)
%endif

export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

VERSION=`cat /etc/momonga-release |awk '{ print $4; }'`

%if %{debug}
export FLAGS="-O0 -g -DDEBUG=1"
%else
%ifnarch ppc
export FLAGS="-DNDEBUG -UDEBUG -DNODEBUG -UDEBUG_ASYNC_TRANSFER -UVPORT_DEBUG -UDEBUG_IO -UDEBUG_MESSAGES -UDEBUG_AUTH -UDEBUG_CONNECTION_DATA -DYYDEBUG=0 -UNETWMDEBUG -UCSS_DEBUG -UCSS_STYLESHEET_DEBUG -USPEED_DEBUG -UKJS_DEBUGGER -UFORMS_DEBUG -UDEBUG_LAYOUT -UDEBUG_DRAW_BORDER -UPARSER_DEBUG -UDECODE_DEBUG -UCACHE_DEBUG -UBUFFER_DEBUG -UJPEG_DEBUG -UDEBUG_LINEBREAKS -UBOX_DEBUG -UTABLE_DEBUG -UYYERROR_VERBOSE -UKJS_DEBUG_MEM -UMIDIOUTDEBUG -UGENERAL_DEBUG_MESSAGES"
%else
export FLAGS="-O3 -DNDEBUG -UDEBUG -DNODEBUG -UDEBUG_ASYNC_TRANSFER -UVPORT_DEBUG -UDEBUG_IO -UDEBUG_MESSAGES -UDEBUG_AUTH -UDEBUG_CONNECTION_DATA -DYYDEBUG=0 -UNETWMDEBUG -UCSS_DEBUG -UCSS_STYLESHEET_DEBUG -USPEED_DEBUG -UKJS_DEBUGGER -UFORMS_DEBUG -UDEBUG_LAYOUT -UDEBUG_DRAW_BORDER -UPARSER_DEBUG -UDECODE_DEBUG -UCACHE_DEBUG -UBUFFER_DEBUG -UJPEG_DEBUG -UDEBUG_LINEBREAKS -UBOX_DEBUG -UTABLE_DEBUG -UYYERROR_VERBOSE -UKJS_DEBUG_MEM -UMIDIOUTDEBUG -UGENERAL_DEBUG_MESSAGES"
%endif
%endif

export CFLAGS_="%{optflags} -fno-exceptions $FLAGS -D_GNU_SOURCE"
export CXXFLAGS_="-fno-check-new $CFLAGS_ -fno-use-cxa-atexit"

suffix=""
if [ "%{_lib}" == "lib64" ] ; then
   suffix="64"
fi

CFLAGS="$CFLAGS_" \
CXXFLAGS="$CXXFLAGS_" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--includedir=%{_includedir}/kde \
	--sysconfdir=%{_sysconfdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/avahi-compat-libdns_sd/ \
	--with-rgbfile=%{_datadir}/X11/rgb.txt \
	--enable-libsuffix="$suffix" \
%if %{with_cups}
	--enable-cups \
%endif
	--enable-mitshm \
%if "%{debug}" == "0"
	--disable-debug \
	--without-debug \
	--disable-warnings \
%endif
	--enable-mt \
	--with-distribution="Momonga Linux $VERSION" \
	--disable-libfam \
	--without-libfam \
	--with-alsa \
	--with-xinerama \
	--with-utempter \
%if %{enable_final}
	--enable-final \
%endif
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-rpath

# ld breaks with -fPIC on ppc, workaround
# it will be removed when this bug is fixed
%ifarch ppc
perl -pi -e 's,^pic_flag=" -fPIC -DPIC",pic_flag=" -fpic",g' libtool
%endif

%if %{with_docs}
doxygen -s -u admin/Doxyfile.global
make %{?_smp_mflags} apidox
%endif

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

make install DESTDIR=%{buildroot}

%if %{with_docs}
pushd %{buildroot}%{_docdir}
ln -sf HTML/en/%{srcname}-apidocs %{name}-devel-%{version}
popd
%else
rm -rf %{buildroot}%{_docdir}/HTML/en/%{srcname}-apidocs
%endif

chmod a+x %{buildroot}%{_libdir}/*
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/services/

# install rc file
install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/

# Make symlinks relative
pushd %{buildroot}%{_docdir}/HTML/en
for i in *; do
	if [ -d $i -a -L $i/common ]; then
		rm -f $i/common
		ln -sf ../common $i
	fi
done
popd

%if %{with_docs}
install -m 644 KDE*PORTING.html %{buildroot}%{_docdir}/%{name}-devel-%{version}
%endif

# remove to avoid conflicting with hicolor-icon-theme
# hicolor/index.theme is provided by hicolor-icon-theme
rm -rf %{buildroot}%{_datadir}/icons/hicolor

# remove to avoid conflicting with kdelibs and kdelibs-devel
rm -rf %{buildroot}%{_sysconfdir}/xdg/menus
rm -f %{buildroot}%{_bindir}/checkXML
rm -f %{buildroot}%{_bindir}/ksvgtopng
rm -f %{buildroot}%{_bindir}/kmailservice
rm -f %{buildroot}%{_bindir}/ktelnetservice
rm -f %{buildroot}%{_bindir}/kunittestmodrunner
rm -f %{buildroot}%{_bindir}/preparetips
rm -rf %{buildroot}%{_datadir}/apps/katepart
rm -rf %{buildroot}%{_datadir}/apps/kcertpart
rm -rf %{buildroot}%{_datadir}/apps/kcm_componentchooser
rm -rf %{buildroot}%{_datadir}/apps/kconf_update
rm -rf %{buildroot}%{_datadir}/apps/kdeui
rm -rf %{buildroot}%{_datadir}/apps/kdewidgets
rm -rf %{buildroot}%{_datadir}/apps/khtml/css
rm -f %{buildroot}%{_datadir}/apps/khtml/domain_info
rm -f %{buildroot}%{_datadir}/apps/khtml/khtml.rc
rm -f %{buildroot}%{_datadir}/apps/khtml/khtml_browser.rc
rm -f %{buildroot}%{_datadir}/apps/kjava/kjava.jar
rm -f %{buildroot}%{_datadir}/apps/kjava/kjava.policy
rm -f %{buildroot}%{_datadir}/apps/kjava/pluginsinfo
rm -rf %{buildroot}%{_datadir}/apps/kssl
rm -rf %{buildroot}%{_datadir}/apps/ktexteditor_docwordcompletion
rm -rf %{buildroot}%{_datadir}/apps/ktexteditor_insertfile
rm -rf %{buildroot}%{_datadir}/apps/ktexteditor_kdatatool
rm -rf %{buildroot}%{_datadir}/config/colors
rm -f %{buildroot}%{_datadir}/config/katesyntaxhighlightingrc
rm -f %{buildroot}%{_datadir}/config/kdebug.areas
rm -f %{buildroot}%{_datadir}/config/kdebugrc
rm -f %{buildroot}%{_datadir}/config/ksslcalist
rm -rf %{buildroot}%{_datadir}/config/ui
rm -f %{buildroot}%{_docdir}/HTML/en/common/1.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/10.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/2.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/3.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/4.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/5.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/6.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/7.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/8.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/9.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/artistic-license.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/bottom-left.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/bottom-middle.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/bottom-right.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/bsd-license.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/doxygen.css
rm -f %{buildroot}%{_docdir}/HTML/en/common/favicon.ico
rm -f %{buildroot}%{_docdir}/HTML/en/common/fdl-license
rm -f %{buildroot}%{_docdir}/HTML/en/common/fdl-license.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/fdl-notice.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/footer.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/gpl-license
rm -f %{buildroot}%{_docdir}/HTML/en/common/gpl-license.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/header.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/kde-default.css
rm -f %{buildroot}%{_docdir}/HTML/en/common/kde_logo_bg.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/lgpl-license
rm -f %{buildroot}%{_docdir}/HTML/en/common/lgpl-license.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/mainfooter.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/mainheader.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/qpl-license.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/top-left.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/top-middle.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/top-right-konqueror.png
rm -f %{buildroot}%{_docdir}/HTML/en/common/top-right.jpg
rm -f %{buildroot}%{_docdir}/HTML/en/common/x11-license.html
rm -f %{buildroot}%{_docdir}/HTML/en/common/xml.dcl
rm -rf %{buildroot}%{_datadir}/locale/all_languages

# move to avoid conflicting with kdelibs and kdelibs-devel
mv -f %{buildroot}%{_datadir}/apps/LICENSES/ARTISTIC %{buildroot}%{_datadir}/apps/LICENSES/ARTISTIC.kde3
mv -f %{buildroot}%{_datadir}/apps/LICENSES/BSD %{buildroot}%{_datadir}/apps/LICENSES/BSD.kde3
mv -f %{buildroot}%{_datadir}/apps/LICENSES/GPL_V2 %{buildroot}%{_datadir}/apps/LICENSES/GPL_V2.kde3
mv -f %{buildroot}%{_datadir}/apps/LICENSES/LGPL_V2 %{buildroot}%{_datadir}/apps/LICENSES/LGPL_V2.kde3
mv -f %{buildroot}%{_datadir}/apps/LICENSES/QPL_V1.0 %{buildroot}%{_datadir}/apps/LICENSES/QPL_V1.0.kde3

# remove to avoid conflicting with kdepimlibs
rm -rf %{buildroot}%{_datadir}/apps/kabc

%if ! %{include_crystalsvg}
# remove to avoid conflicting with kdeartwork-icons-crystalsvg
rm -rf %{buildroot}%{_datadir}/icons/crystalsvg
%endif

# remove to avoid conflicting with kdebase-runtime
rm -f %{buildroot}%{_datadir}/apps/kstyle/themes/qtcde.themerc
rm -f %{buildroot}%{_datadir}/apps/kstyle/themes/qtmotif.themerc
rm -f %{buildroot}%{_datadir}/apps/kstyle/themes/qtwindows.themerc

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COMPILING* COPYING* DEBUG INSTALL NAMING README TODO
%config %{_sysconfdir}/kderc
%{_bindir}/artsmessage
%{_bindir}/cupsdconf
%{_bindir}/cupsdoprint
%{_bindir}/dcop
%{_bindir}/dcopclient
%{_bindir}/dcopfind
%{_bindir}/dcopidl
%{_bindir}/dcopidl2cpp
%{_bindir}/dcopidlng
%{_bindir}/dcopobject
%{_bindir}/dcopquit
%{_bindir}/dcopref
%{_bindir}/dcopserver
%{_bindir}/dcopserver_shutdown
%{_bindir}/dcopstart
%{_bindir}/filesharelist
%{_bindir}/fileshareset
%{_bindir}/imagetops
%{_bindir}/kab2kabc
%{_bindir}/kaddprinterwizard
%{_bindir}/kbuildsycoca
%{_bindir}/kcmshell
%{_bindir}/kconf_update
%{_bindir}/kconfig_compiler
%{_bindir}/kcookiejar
%{_bindir}/kde-config
%{_bindir}/kde-menu
%{_bindir}/kded
%{_bindir}/kdeinit
%{_bindir}/kdeinit_shutdown
%{_bindir}/kdeinit_wrapper
%{_bindir}/kdesu_stub
%{_bindir}/kdontchangethehostname
%{_bindir}/kdostartupconfig
%{_bindir}/kfile
%{_bindir}/kfmexec
%attr(4755,root,root) %{_bindir}/kgrantpty
%{_bindir}/khotnewstuff
%{_bindir}/kinstalltheme
%{_bindir}/kio_http_cache_cleaner
%{_bindir}/kio_uiserver
%{_bindir}/kioexec
%{_bindir}/kioslave
%{_bindir}/klauncher
%attr(4755,root,root) %{_bindir}/kpac_dhcp_helper
%{_bindir}/ksendbugmail
%{_bindir}/kshell
%{_bindir}/kstartupconfig
%{_bindir}/ktradertest
%{_bindir}/kwrapper
%{_bindir}/lnusertemp
%{_bindir}/make_driver_db_cups
%{_bindir}/make_driver_db_lpr
%{_bindir}/makekdewidgets
%{_bindir}/meinproc
%{_bindir}/start_kdeinit
%{_bindir}/start_kdeinit_wrapper
%{_libdir}/kde3/
%{_libdir}/lib*
%{_datadir}/applications/kde
%{_datadir}/apps/LICENSES/*kde3
%{_datadir}/apps/dcopidlng
%{_datadir}/apps/kdeprint
%{_datadir}/apps/khtml/icons/crystalsvg/*/*/*.png
%{_datadir}/apps/khtml/khtml_popupmenu.rc
%{_datadir}/apps/kio_uiserver
%{_datadir}/apps/kjava/icons
%{_datadir}/apps/knewstuff/types
%{_datadir}/apps/knotify
%{_datadir}/apps/ksgmltools2/kde3
%{_datadir}/apps/kstyle/pixmaps
%{_datadir}/apps/kstyle/themes/*.themerc
%{_datadir}/apps/ktexteditor_isearch
%{_datadir}/apps/proxyscout/eventsrc
%{_datadir}/autostart
%config %{_datadir}/config/ipv6blacklist
%config %{_datadir}/config/katefiletyperc
%config %{_datadir}/config/kdeprintrc
%config %{_datadir}/config/khotnewstuffrc
%config %{_datadir}/config/kthemestylerc
%config %{_datadir}/config/language.codes
%{_datadir}/doc/HTML/en/common/*
%{_datadir}/doc/HTML/en/kspell
%{_datadir}/emoticons
%if %{include_crystalsvg}
%{_datadir}/icons/crystalsvg
%endif
%{_datadir}/icons/default.kde
%{_datadir}/mimelnk
%{_datadir}/services
%{_datadir}/servicetypes

%files devel
%defattr(-,root,root)
%{_includedir}/kde/arts/*.h
%{_includedir}/kde/dnssd
%{_includedir}/kde/dom
%{_includedir}/kde/kabc
%{_includedir}/kde/kate
%{_includedir}/kde/kdeprint
%{_includedir}/kde/kdesu
%{_includedir}/kde/khexedit
%{_includedir}/kde/kio
%{_includedir}/kde/kjs
%{_includedir}/kde/kmdi
%{_includedir}/kde/kmediaplayer
%{_includedir}/kde/knewstuff
%{_includedir}/kde/kparts
%{_includedir}/kde/kresources
%{_includedir}/kde/ksettings
%{_includedir}/kde/kspell2
%{_includedir}/kde/ktexteditor
%{_includedir}/kde/kunittest
%{_includedir}/kde/libkmid
%{_includedir}/kde/kde.pot
%{_includedir}/kde/kgenericfactory.tcc
%{_includedir}/kde/*.h

%if %{with_docs}
%files apidocs
%defattr(-,root,root)
%doc %{_docdir}/%{name}-devel-%{version}
%doc %{_docdir}/HTML/en/%{srcname}*
%endif

%changelog
* Mon Apr 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-20m)
- enable to build with new automake
- disable apidocs

* Fri Nov 29 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-19m)
- split package apidocs
- set relname FallingStar

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-18m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Mon Dec 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-17m)
- remove conflicting file(s) with kdelibs-4.9.95

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-16m)
- remove conflicting file(s) with kdelibs-4.9.90

* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-15m)
- import cups16.patch from Arch Linux to enable build with new cups

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-14m)
- rebuild againsr pcre-8.31

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-13m)
- remove conflicting file(s) with kdelibs-4.9.0-1m

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-12m)
- rebuild against libtiff-4.0.1

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-11m)
- fix build failure; add patch for cups-1.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-8m)
- add patch for gcc45, generated by gen45patch(v1)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.10-7m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-6m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Wed Apr 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-5m)
- update openssl.patch

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.10-4m)
- rebuild against libjpeg-8a

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.10-3m)
- apply openssl100 patch

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-2m)
- update openssl.patch

* Sun Mar 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-1m)
- update to KDE 3.5.10
- set relname PassingTime
- [SECURITY] CVE-2009-3736
- import libltdl-CVE-2009-3736.patch from Fedora
- remove merged post-kde-3.5.5-kinit.diff

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-24m)
- remove conflicting files with kdebase-runtime (>=4.3.80)

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-23m)
- [SECURITY] http://www.kde.org/info/security/advisory-20091027-1.txt
- oCert: #2009-015 http://www.ocert.org/advisories/ocert-2009-015.html

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-21m)
- [SECURITY] CVE-2009-2702
- import a security patch (Patch315) from Rawhide (3.5.10-15)

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.9-20m)
- rebuild against libjpeg-7

* Wed Jul 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-19m)
- [SECURITY] CVE-2009-2537 CVE-2009-1725 CVE-2009-1687 CVE-2009-1690 CVE-2009-1698
- import security patches from Fedora 11 (3.5.10-13)

* Mon May 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-18m)
- import cupsserverbin.patch from Fedora
 +* Tue Feb 26 2008 Lukas Tinkl <ltinkl@redhat.com> - 3.5.9-3
 +- #230979: Writes ServerBin into cupsd.conf
 +- #416101: unable to print after configuring printing in KDE
- import latex-syntax-kile-2.0.3.patch from Fedora
 +* Mon Dec 15 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.5.10-3
 +- update the KatePart latex.xml syntax definition to the version from Kile 2.0.3
- import gcc44.patch from Fedora
 +* Wed Feb 25 2009 Than Ngo <than@redhat.com> - 3.5.10-7
 +- fix build issue with gcc-4.4
- build apidocs again
- set relname TransistorRadio

* Tue May 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-17m)
- fix build with new automake
- update openssl.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-16m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-15m)
- stop making apidocs for the moment

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-14m)
- update Patch2,7,8,9,11,52,71 for fuzz=0
- drop Patch50, bombing by mistake
- License: GPLv2+ or LGPLv2+ or Modified BSD or GFDL

* Tue Dec  9 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.9-13m)
- add patch to fix compile error

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-12m)
- add kderc for font settings

* Fri Jun 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-11m)
- revive crystalsvg icons for applications of KDE3

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-10m)
- remove Requires: kdelibs-devel from kdelibs3-devel

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-9m)
- update openssl.patch for openssl-0.9.8h

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-8m)
- move ksgmltools2 to ksgmltools2/kde3

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-7m)
- remove Requires: redhat-artwork

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-6m)
- revive %%{_docdir}/HTML/en/common for kde3-i18n

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-5m)
- rebuild against qt3

* Sun Apr 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-4m)
- [SECURITY] CVE-2008-1671
- start_kdeinit multiple vulnerabilities
- http://www.kde.org/info/security/advisory-20080426-2.txt

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.9-3m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-2m)
- rebuild against OpenEXR-1.6.1

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-1m)
- update to KDE 3.5.9
- update gcc43.patch
- update openssl.patch
- remove merged upstream patches
- import autostart.patch from Fedora
 +* Tue Sep 25 2007 Than Ngo <than@redhat.com> - 6:3.5.7-23
 +- fix rh#243611, autostart from XDG_CONFIG_DIRS
- import kstandarddirs.patch and 137675.patch from Fedora
- set relname bluecloud

* Wed Feb 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-7m)
- kdelibs3-devel Requires: kdelibs-devel
- building of KDE3 applications need %%{_datadir}/apps/ksgmltools2

* Tue Jan 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-6m)
- rename from kdelibs to kdelibs3
- modify headers directory
- remove conflicting files and directories with KDE4
- %%{_docdir}/HTML and %%{_docdir}/HTML/en should be owned by other packages, may be kdelibs?

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-5m)
- apply latest svn fix
- http://bugs.kde.org/show_bug.cgi?id=155001

* Thu Dec 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-4m)
- apply latest svn fixes to avoid crashing nspluginviewer with new flash

* Thu Nov 29 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-3m)
- add a patch for gcc-4.3

* Fri Oct 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-2m)
- add fixarcjpencode patch for non-ASCII file name in zip archives

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8
- delete unused patches and update some patches

* Fri Oct  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-12m)
- update applications.menu.momonga for new gnome-games

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.7-11m)
- revised spec for debuginfo

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-10m)
- [SECURITY] CVE-2007-3820, CVE-2007-4224, CVE-2007-4225
- update security patch (post-3.5.7-kdelibs-kdecore-2.diff)

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-9m)
- [SECURITY] CVE-2007-3820, CVE-2007-4224, CVE-2007-4225
- import upstream security patch (post-3.5.7-kdelibs-kdecore.diff)

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-8m)
- enable to build with cups-1.3
- import patch from FC-devel

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-7m)
- fix mo-server-settings.menu

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-6m)
- update menus for system-config-{audit,cluster,lvm}

* Wed Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-5m)
- remove ICEauthority.patch

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-4m)
- update menus for revisor

* Tue Jun  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-3m)
- exclude screensavers from applications.menu.momonga

* Sun Jun  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- update menus for system-config-{httpd,nfs,samba,sevices}
- import kdelibs-3.5.7-ICEauthority.patch from Fedora
- import kdelibs-3.5.7-kde#146105.patch from Fedora
 +* Thu May 24 2007 Than Ngo <than@redhat.com> 3.5.6-10.fc7
 +- don't change permission .ICEauthority by sudo KDE programs
 +- apply upstream patch to fix kde#146105

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- delete unused upstream patches
- kde-khtml-overflow-CVE-2006-4811.patch is still needed

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-16m)
- set relname rainbowshoes, LittleWing is a registered trademark of LittleWing CO.LTD.

* Sun May  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-15m)
- set relname littlewing
- sort BuildPreReq

* Sun May  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-14m)
- set relname LittleWing for STABLE release

* Thu Mar 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-13m)
- [SECURITY] CVE-2007-1564
- replace CVE-2007-1308.patch to CVE-2007-1564-kdelibs-3.5.6.diff
  http://www.kde.org/info/security/advisory-20070326-1.txt

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-12m)
- import upstream patch to fix following problem
  #138976, Some links are difficult to click in the documentation (docs.kde.org)

* Wed Mar 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-11m)
- update applications.menu.momonga for gnome-sudoku, gnomeattacks, monkey-bubble and tuxtype

* Sun Mar 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-10m)
- [SECURITY] CVE-2007-1308
  import http://bindshell.net/advisories/konq355/konq355-patch.diff as CVE-2007-1308.patch
  http://bindshell.net/advisories/konq355
- modify applications.menu.momonga
  include all desktop files of new gdm
- import kdelibs-3.5.6-utempter.patch from Fedora
- BuildPreReq: libutempter-devel
- add --with-utempter option to configure

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-9m)
- BuildPreReq: attr-devel -> libattr-devel

* Sun Mar  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-8m)
- update openssl.patch
- import kdelibs-3.5.1-smooth-scrolling.patch from cooker
 +* Fri Jul 14 2006 Helio Chissini de Castro <helio@mandriva.com> 30000000:3.5.3-8mdv2007.0
 ++ Revision: 41174
 +- Added smooth scrolling patch

* Sat Feb 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-7m)
- import upstream patch to fix following problem
  #141670, Paste image from clipboard to another app (crash)

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-6m)
- rebuild against arts pcre gdbm libjpeg aspell jasper

* Tue Feb  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-5m)
- revise avahi-support patch (patch30)

* Sun Feb  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-4m)
- import upstream patch to fix following problem
  #140768, loading new page resets scroll position of old page to top (Reverting r617941)

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-3m)
- revive avahi-support patch (patch30)

* Sat Feb  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-2m)
- update applications.menu.momonga
 - include epiphany.desktop, bmp-*.desktop and totem.desktop
  - Epiphany, BMPx and Totem are working on KDE now
 - modify for SELinux tools
- modify mo-system-settings.menu for SELinux tools

* Sat Jan 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete unused upstream patches
- some of previously imported patches were modified

* Thu Jan 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-19m)
- import upstream patch to fix XSS vulnerability
- see http://www.securityfocus.com/archive/1/457763/30/30/threaded

* Tue Jan 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-18m)
- import upstream patch to fix following problem
  #136630, 'Save As' behaviour change (Revert 589847)

* Fri Jan 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-17m)
- import upstream patches to fix following problems
  #107455, [patch] File upload input does not fire "onchange" event (javascript)
  #105351, get new wallpapers doesn't save extension

* Wed Jan 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-16m)
- rebuild against jasper-1.900.0-1m
- add Requires: jasper and BuildPreReq: jasper-devel

* Mon Jan  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-15m)
- import upstream patch to fix following problem
  #121867, [site-issue] Largest Hungarian press media webpage looks ugly - www.nol.hu

* Thu Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-14m)
- import upstream patches to fix following problems
  #139488, crash when finding backwards
  #24820, visited links don't change colour until after they are hovered over with the mouse.
  #109038, folder icon not changed back when write/read-permission is re-added to a folder

* Sun Dec 24 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-13m)
- import upstream patch to fix following problem
  #138818, "Select a year" in KDatePicker does not properly update KDateTable child widget

* Sun Dec 24 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-12m)
- import upstream patch to fix following problem
  r615933, the dialog is not created every time, but is cached, so values are not reset

* Fri Dec 22 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-11m)
- import upstream patch to fix following problem
  #139077, kdeinit prints "created link" to stdout which spoils kdialog output

* Tue Dec 12 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-10m)
- import upstream patch to fix following problem
  #103775, kontact --module XYZ crashes after minimizing to tray icon and maximizing back
- revise kdelibs-3.5.5-kde#125559.patch

* Sat Dec  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-9m)
- import upstream patch to fix following problem
  #125559, Keycode 111 starts ksnapshot (hardcoded?)

* Tue Nov 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-8m)
- rebuild against OpenEXR-1.4.0

* Sun Nov 12 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-7m)
- rebuild against hspell-1.0-1m
- add Requires: hspell >= 0.9
- add BuildPreReq: hspell-devel >= 0.9
- import upstream patches to fix following problems
  #15876, status bar has 2 lines
  #136952, [regression] crash on www.cosmote.gr
  #122047, &notin; is displayed as &not;in;

* Wed Nov  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-6m)
- modify applications.menu.momonga and mo-*-settings.menu for system-config-printer

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-5m)
- import upstream patch to fix following problem
- #136649, 64bit specific bug in KDEPrintd

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- [SECURITY] import patch from mdk (kde-khtml-overflow-CVE-2006-4811.patch)
-- * Wed Oct 18 2006 Vincent Danen <vdanen@mandriva.com>
--   security fix for CVE-2006-4811
- import upstream patches from FC6
- fix CUPS manager problem (kdelibs-3.5.5-kmcupsmanager.patch)
- fix khtml problem (kdelibs-3.5.5-kde#135988.patch)

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.5-3m)
- rebuild against expat-2.0.0-2m

* Sat Oct 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-2m)
- modify applications.menu.momonga and mo-*-settings.menu for system-config-netboot
 - SystemSetup and X-Red-Hat-ServerConfig are obsoleted by desktop-file-utils
- exclude bmp-*2.0.desktop from applications.menu.momonga
 - bmpx doesn't work on KDE

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5
- remove merged upstream patches

* Wed Sep 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-5m)
- import 10 upstream patches from Fedora Core devel
 +* Tue Sep 05 2006 Than Ngo <than@redhat.com> 6:3.5.4-5
 +- apply upstream patches
 +   fix #123413, kded crash when KDED modules make DCOP calls in their destructors
 +   fix #133529, konqueror's performance issue
 +   fix kdebug crash
 +   more icon contexts (Tango icontheme)
 +   fix #133677, file sharing doesn't work with 2-character long home directories
 +* Mon Sep 04 2006 Than Ngo <than@redhat.com> 6:3.5.4-4
 +- apply upstream patches
 +   fix kde#121528, konqueror crash
 +* Wed Aug 23 2006 Than Ngo <than@redhat.com> 6:3.5.4-3
 +- apply upstream patches
 +   fix kde#131366, Padding-bottom and padding-top not applied to inline elements
 +   fix kde#131933, crash when pressing enter inside a doxygen comment block
 +   fix kde#106812, text-align of tables should only be reset in quirk mode
 +   fix kde#90462, konqueror crash while rendering in khtml

* Sun Sep 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-4m)
- rebuild against arts-1.5.4-2m and avahi-0.6.13-3m

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-3m)
- rebuild against esound-devel-0.2.36-4m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-2m)
- rebuild against libart_lgpl-2.3.17-4m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4
- remove merged upstream patches

* Mon Jul 24 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-9m)
- fix kde#116092
- remove transkicker-highcolor.patch

* Sat Jul 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.5.3-8m)
- [SECURITY] CVE-2006-3672
- http://browserfun.blogspot.com/2006/07/mobb-14-konqueror-replacechild.html
- add Patch301: kdelibs-3.5.3-CVE-2006-3672.patch

* Wed Jul 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-7m)
- import 2 upstream patches from Fedora Core devel
 +* Tue Jul 11 2006 Than Ngo <than@redhat.com> 6:3.5.3-8
 +- upstream patches,
 +    kde#130605 - konqueror crash
 +    kde#129187 - konqueror crash when modifying address bar address

* Tue Jul 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-6m)
- import 2 upstream patches from Fedora Core devel
 +* Mon Jul 10 2006 Than Ngo <than@redhat.com> 6:3.5.3-7
 +- apply upstream patches,
 +    kde#123307 - Find previous does nothing sometimes
 +    kde#106795 - konqueror crash

* Wed Jul  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-5m)
- fix setuid

* Wed Jul  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-4m)
- import 2 upstream patches from Fedora Core devel
 +* Tue Jul 04 2006 Than Ngo <than@redhat.com> 6:3.5.3-6
 +- apply upstream patches, fix #128940/#81806/#128760

* Sat Jul  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- import 9 upstream patches from Fedora Core devel
 +* Sat Jun 24 2006 Than Ngo <than@redhat.com> 6:3.5.3-5
 +- apply upstream patches
- set distname

* Fri Jun  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- rebuild with avahi (enable Zeroconf support)
- import kdelibs-3.5.0-add-dnssd-avahi-support.patch from cooker
 +* Wed Jan 25 2006 Laurent Montel <lmontel@mandriva.com>
 ++ 2006-01-25 18:45:30 (1469)
 +- Fix linkage of dnssd with libavahi-client (thanks gb)
 +* Wed Jan 25 2006 Laurent Montel <lmontel@mandriva.com>
 ++ 2006-01-25 08:55:44 (1460)
 +- Add support for zeroconf+avahi lib
 +* Tue Jan 24 2006 Laurent Montel <lmontel@mandriva.com>
 ++ 2006-01-24 17:05:13 (1457)
 +- Fix search avahi daemon
 +* Mon Jan 23 2006 Laurent Montel <lmontel@mandriva.com>
 ++ 2006-01-23 17:58:36 (1448)
 +- Add support for avahi

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3
- remove modular-x.patch

* Fri May 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-10m)
- revise %%files for rpm-4.4.2

* Wed May 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-9m)
- update applications.menu.momonga
 - exclude nact.desktop
  - nautilus-actions is an extension of nautilus for GNOME
 - include gnomebaker.desktop
  - gnomebaker works fine on KDE

* Tue May  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-8m)
- update openssl.patch

* Sat Apr 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-7m)
- update mo-system-settings.menu and applications.menu.momonga for pirut

* Wed Apr 26 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.5.2-6m)
- add kdelibs-3.5.2-menu-fixcomment.patch and revised applications.menu.momonga
- - wrong position of a comment in applications.menu seemed to prevent "science" submenu to appear in a menu.

* Fri Apr 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-5m)
- exclude gnome-screenshot.desktop from applications.menu.momonga
 - ksnapshot has same function as gnome-screenshot
- exclude sabayon.desktop from mo-system-settings.menu
 - sabayon.desktop doesn't work on KDE

* Tue Apr 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-4m)
- exclude epiphany.desktop and mergeant.desktop from applications.menu.momonga
 - epiphany doesn't work on KDE
 - mergeant leaves an unnecessary process at the next session of KDE

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-3m)
- update openssl.patch

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.2-2m)
- rebuild against openssl-0.9.8a

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Sun Mar 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-3m)
- import kdelibs-3.5.0-modular-x.patch from Fedora Core devel
 +* Thu Dec 01 2005 Than Ngo <than@redhat.com> 6:3.5.0-1
 +- add fix for modular X, thanks to Ville Skytt #174131

* Mon Feb 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-2m)
- modify applications.menu.momonga

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1
- remove post-3.4.3-kdelibs-kjs.diff

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-4m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Fri Jan 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-3m)
- [SECURITY] CVE-2006-0019
  post-3.4.3-kdelibs-kjs.diff for "kjs encodeuri/decodeuri heap overflow vulnerability"
  http://www.kde.org/info/security/advisory-20060119-1.txt

* Fri Dec 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- import kdelibs-3.5.0-kicker-crash.patch from gentoo-x86-portage
 +- 14 Dec 2005; Diego Petteno <flameeyes@gentoo.org>
 +- +files/kdelibs-3.5.0-kicker-crash.patch, +kdelibs-3.5.0-r1.ebuild:
 +- Add patch to fix kicker's crashes, hopefully fixing bug #114141. Patch from
 +- upstream committed by aseigo.

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.3m)
- remove lnusertemp.patch
  (move %%{_bindir}/lnusertemp from arts to kdelibs)
- disable hidden visibility

* Sun Nov 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- fix applications.menu.momonga

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- update applications.menu.momonga
- update buildroot.patch from Fedora Core devel
- import 2 patches from Fedora Core devel
 - kdelibs-3.4.92-inttype.patch
 - kdelibs-3.4.92-lnusertemp.patch
  +* Fri Nov 04 2005 Than Ngo <than@redhat.com> 6:3.4.92-2
  +- move lnusertemp in arts, workaround for #169631
- import Source4: devices.protocol from Fedora Core devel
- BuildPreReq: attr-devel >= 2.4.10-2m, libacl-devel >= 2.2.28-2m
- BuildPreReq: aspell-devel

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- add %%doc

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1
- update openssl.patch
- update visibility.patch
- remove post-3.4.0-kdelibs-kimgio-fixed.diff
- remove kdelibs-3.4.0-FC.diff

* Mon May  9 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.4.0-7m)
- ppc build fix.

* Fri May  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-6m)
- [SECURITY] update Patch30: post-3.4.0-kdelibs-kimgio(-fixed).diff
  http://kde.org/info/security/advisory-20050504-1.txt

* Sat Apr 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-5m)
- [SECURITY] post-3.4.0-kdelibs-kimgio.diff for "kimgio input validation errors"
  http://kde.org/info/security/advisory-20050421-1.txt

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-4m)
- modify %%files section
- remove -fno-rtti from CXXFLAGS

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-3m)
- sync with FC devel due to enable x86_64.

* Tue Mar 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-2m)
- modify applications.menu.momonga for HelixPlayer

* Sun Mar 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- update applications.menu.momonga
- update rpath.patch
- update openssl.patch from Fedora Core
- update buildroot.patch from Fedora Core
- remove old patches
- BuildPrereq: doxygen, libart_lgpl-devel, pkgconfig, perl
- BuildPrereq: libjpeg-devel, libpng-devel, libtiff-devel
- BuildPrereq: cups-devel >= 1.1.9

* Thu Mar 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-8m)
- [SECURITY] post-3.3.2-kdelibs-dcopidlng.patch for "Insecure temporary file creation by dcopidlng"
  http://kde.org/info/security/advisory-20050316-3.txt
- [SECURITY] post-3.3.2-kdelibs-idn-2.patch for "Konqueror International Domain Name Spoofing"
  http://kde.org/info/security/advisory-20050316-2.txt
- [SECURITY] post-3.3.2-kdelibs-dcop.patch for "Local DCOP denial of service vulnerability"
  http://kde.org/info/security/advisory-20050316-1.txt

* Thu Mar  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-7m)
- modify mo-server-settings.menu and mo-system-settings.menu

* Sun Feb 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-6m)
- update Source1: applications.menu.momonga
- change Source2 to mo-server-settings.menu
- add Source3: mo-system-settings.menu
  Source2 and Source3 are modified FC3 files

* Sun Feb 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.2-5m)
- add BuildPrereq: esound-devel for use /usr/lib/libesd.la

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-4m)
- edit applications.menu
  add Source1: applications.menu.momonga
  add Source2: rh-system-settings.menu.momonga
- Requires: redhat-artwork

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-3m)
- [SECURITY] post-3.3.2-kdelibs-htmlframes2.patch for "Konqueror Window Injection Vulnerability"
  http://kde.org/info/security/advisory-20041213-1.txt
- [SECURITY] post-3.3.2-kdelibs-kioslave.patch for "ftp kioslave command injection"
  http://kde.org/info/security/advisory-20050101-1.txt
- import patches from Fedora Core
 - kdelibs-3.3.2-kate-immodule.patch
  +* Sat Feb 12 2005 Than Ngo <than@redhat.com> 6:3.3.2-0.7
  +- backport CVS patch, cleanup InputMethod
 - kdelibs-3.3.2-arts.patch
  +* Thu Feb 10 2005 Than Ngo <than@redhat.com> 6:3.3.2-0.5
  +- fix knotify crash after applying sound system change
 - kdelibs-3.3.2-cleanup.patch
  +* Thu Feb 10 2005 Than Ngo <than@redhat.com> 6:3.3.2-0.5
  +- add steve cleanup patch
 - kdelibs-3.3.2-ppc.patch
  +* Wed Dec 15 2004 Than Ngo <than@redhat.com> 6:3.3.2-0.4
  +- get rid of broken AltiVec instructions on ppc

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.
- remove kdelibs-3.1.93-lib64.patch.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2
- [SECURITY] post-3.3.2-kdelibs-kio.diff for "plain text password exposure" (http://www.kde.org/info/security/advisory-20041209-1.txt)

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-3m)
- rebuild against qt-3.3.3-2m, arts-1.3.0-2m (libstdc++-3.4.1)

* Sun Sep 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-2m)
- revise to include x-ms-{asf,wmv}.desktop again...(I made a mistake...sorry!)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0
- cleanup patches
- import Patch1,5,31,32,33 from Fedora Core

* Tue Aug 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-4m)
- apply following patch
- http://www.kde.org/info/security/advisory-20040823-1.txt

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-3m)
- apply following patches
- http://www.kde.org/info/security/advisory-20040811-1.txt
- http://www.kde.org/info/security/advisory-20040811-2.txt
- http://www.kde.org/info/security/advisory-20040811-3.txt

* Wed Aug 18 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-2m)
- revise %%install section with update of kaffeine

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Tue May 25 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.2-4m)
- fixed vulnerability in the mailto handler, CAN-2004-0411
- fixed KDE Telnet URI Handler File Vulnerability , CAN-2004-0411

* Fri May 22 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.2-3m)
- kde-i18n-Azerbaijani and kde-i18n-Icelandic is exist.

* Sat May 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.2-2m)
- remove /usr/share/icons/hicolor/index.theme which is provided by hicolor-icon-theme
- add Requires: hicolor-icon-theme

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.2-1m)
- KDE 3.2.2

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.2.1-3m)
- rebuild against for libxml2-2.6.8
- rebuild against for libxslt-1.1.5

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.1-2m)
- revised spec to avoid conflicting with kaffeine.

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Tue Feb 17 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.0-2m)
- use %%patch macro for Patch101

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- add --sysconfdir argument to ./configure (for %%{_sysconfdir}/xdg/menus)
- following patches can't be applied (so are omitted now)
- - Patch5: kdelibs-2.1.1-path.patch
- - Patch7: kdelibs-3.0.2-dock.patch
- - Patch12: kioslavetest.patch
- - Patch13: metatest.patch
- - Patch15: kdelibs-3.1-buildroot.patch
- - Patch18: kdelibs-3.1-icon_scale.patch
- - Patch21: http://www6.plala.or.jp/d-frog/kdelibs-3.1.3-khtml-spacefix-r1.patch.gz
- - Patch30: kdelibs-3.1.3a-xandr.patch
- replace following patches by patches from Fedora
- - Patch11: kdelibs-3.1-lib64.patch -> kdelibs-3.1.93-lib64.patch
- change the way doc build (make apidox)
- modified %files (many new files)

* Thu Jan 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.5-2m)
- include again %%{_datadir}/mimelnk/application/x-kword.desktop, etc.
- Without these files, applications in koffice couldn't have saved files
  with their native formats for a long time... now fixed!

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Tue Jan  6 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-6m)
- put kdelibs-3.1.3-khtml-spacefix-r1.patch.gz into repository

* Tue Jan  6 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-5m)
- import IE compatible space handling patch for khtml

* Thu Dec 25 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-4m)
- rebuild against qt-3.2.3 (merge from QT322 branch)

* Fri Dec 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-3m)
- remove -fno-rtti, -fno-check-new from CFLAGS for configure to detect availability of -fPIC option
- add patch fot alsa-1.0.0

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.4-2m)
- rebuild against gdbm-1.8.0

* Wed Sep 24 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4
  - changes  are
    * Allows compilation against Qt 3.2.x
    * kio: only cache successful passwords, otherwise its impossible to re-enter a password when it failed authorisation the first time.
    * other changes are listed in http://www.kde.org/announcements/changelogs/changelog3_1_3to3_1_4.php

* Wed Jul 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3
  - changes are
    * keramik: Major fixes to toolbar gradient alignment, as well as some miscellaneous fixes.
    * SECURITY: kio/khtml: Improve referer handling, always strip out username and password.

* Mon Jul 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-4m)
- add security patches from
     [Kuser:04021] KDE SECURITY:  Konqueror Referer Leaking Website Authentication Credentials
- remove --disable-warnings from configure

* Tue Jul  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-3m)
- add -fno-stack-protector if gcc is 3.3

* Sun Jun 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-2m)
- remove --disable-warnings from configure

* Thu May 22 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- changes are:
     https authentication through proxy fixed.
     KZip failed for some .zip archives.
     Fixed a bug in socket code that made KDEPrint crash.
     kspell: Support for Hebrew spell checking using hspell (requires hspell 0.5).
- delete export _POSIX2_VERSION=199209 and add BuildPreReq: coreutils >= 5.0-1m

# make -f Makefile.cvs

* Sun May 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1a-4m)
- export _POSIX2_VERSION=199209

* Thu May  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1a-3m)
- Kondara MNU/Linux -> Momonga Linux

* Sun Apr 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1a-2m)
  fix keramik style

* Fri Apr 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1a-1m)
  update to 3.1.1a

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- specopt
- enable cups by default
- update to 3.1.1
    kdialog: Fix screen numbering problem for centerOnScreen() static method
    kprogress: Fix math problem in displaying correct percentage for large numbers
    kio_http: Fix data processing for webdav(s) listing of directories and files
    Fixed code completion drop-down box position
    kate: Many small bugfixes, including:
      Fixed code completion drop-down box position
      Fixed "wrap cursor disabled" bugs
      Fixed vertical scrollbar middle mouse behaviour
      Fixed remove whitespace feature
      Now clears the redo history when it is irrelevant
      Fixed crash after starting up with a non-existant directory in the file selector history
    kparts: Fix extension of transparently downloaded files, this fixes ark (used to display temp file instead of archive content)
    klauncher: Fixed support for "Path=" entry in .desktop files. This entry can be used to specify a working directory.
    kio: Don't let ChModJob's -X emulation interfere with mandatory file locking.
    kdeui: Fix for alternate background coloring in Konqueror list views.
    kdeui: Fix to prevent an event loop in conjunction with Qt 3.1.2.
    kio/bookmarks: Properly handle multiple shown bookmarks with the same URL; fixes crash on exit in Konqueror when bookmarkbar is on and some bookmarks points to the same place
    kstyles: Handle focus indicators on label-less checkboxes better
    kdeprint: Don't freeze when there is no route to the selected CUPS server
    SSL: add support for OpenSSL 0.9.7
    SSL: ADH ciphers are now explicitly disabled in all cases
    SSL: new CA root certificate added
    Several Xinerama related fixes
    QXEmbed fixes for various situations that don't handle XEMBED well
    Java on IRIX with SGI 1.2 VM is fixed
    khtml: Several major bugfixes, partially incorporated fixes from Safari as well.

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-6m)
- add URL

* Thu Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1-5m)
  rebuild against openssl 0.9.7a

* Thu Feb 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-4m)
- add Obsoletes: kde-i18n-Azerbaijani
- add Obsoletes: kde-i18n-Korean
- add Obsoletes: kde-i18n-Icelandic
- add Obsoletes: kde-i18n-Latvian

* Sat Feb 08 2003 TAKAHASHI Tamotsu <tamo>
- (3.1-3m)
- do not apply lib64 patch if _lib != lib64
 (if you want to build kdelibs on lib64 machine,
  you need to do more fixes)

* Wed Feb 05 2003 TAKAHASHI Tamotsu <tamo>
- (3.1-2m)
- Patch11: kdelibs-3.1-lib64.patch (from SUSE kdelibs3-3.1-0)

* Sat Feb  1 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.1-1m)
- ver up.

* Mon Jan 20 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-2m)
- rebuild against qt-3.0.5-8m.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Thu Jan 2 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.4-3m)
- add BuildPrereq: kernel >= 2.4.18-119m
  for deviceman.cc require sound/asound.h and sound/asequencer.h

* Tue Dec 10 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Thu Sep 12 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-3m)
- update to kdelibs-3.0.3a by security fixes.

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.3-2m)
- rebuild against arts-1.0.3-2m

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.
- remove patch1.

* Thu Jun 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.1-6k)
- momonga-release

* Tue Jun 18 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-4k)
- add transrate patch for highcolor & keramik style.
- ( See http://kde-look.org/content/show.php?content=2177 )
- change build options. ( use %{optflags} ).

* Sun Jun 09 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up.

* Mon May 20 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-10k)
- no more docbook-utils.

* Sat Apr 20 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-8k)
- applied kdelibs-3.0-gui_effect-20020415.patch

* Tue Apr  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-6k)
- add keramik kstyle from kdelibs-3.0rc3
- (keramik kstyle is a alpha-blending style.)
- applied kdelibs-3.0-kate-charset-20020407.diff

* Wed Apr  5 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-4k)
- remove kde-i18n-Norwegian kde-i18n-Norwegian-Nynorsk from Obsolutes

* Thu Apr  4 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release.

* Sun Mar 24 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003004k)
- rebuild.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.
- arts has been separated.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-12k)
- nigirisugi.
- revised spec file.

* Tue Nov  6 2001 Shingo Akagaki <dora@kondara.org>
- (2.2.1-10k)
- update libxslt patch for new libxslt

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- revised spec file.

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- rebuild against libpng 1.2.0.

* Sat Oct 13 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- added debug flag.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Tue Sep 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-6k)
- add ppc support.

* Mon Aug 20 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Wed Aug  9 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.
- Obsolete kdesupport, kdesupport-devel

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002004k)
- based on 2.2alpha2.

* Wed May 30 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001004k)
- Ummmm. KDE 2.2alpha1 is disappeared :-P

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Sat Apr 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.2alpha1-2k)

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.1-6k)
- rebuild against openssl 0.9.6.

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.1-5k)
- rebuild against audiofile-0.2.1.

* Fri Mar 30 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- [2.1.1-3k]

* Tue Mar 20 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-6k]
- obsolete sound, sound-devel.

* Fri Mar 16 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-2k]

* Mon Jan 21 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-14k]
- backport 2.0.1-15k(Jirai).

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-12k]
- backport 2.0.1-13k(Jirai).

* Mon Jan 01 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-10k]
- backport 2.0.1-11k.

* Mon Jan 01 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-11k]
- rebuild against.

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-9k]
- rebuild against audiofile-0.2.0.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-7k]
- rebuild against qt-2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Thu Dec 07 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Fri Nov 24 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-7k]
- klauncher.la change package kdelibs-devel to kdelibs.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-6k]
- rebuild against egcs++.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-5k]
- rebuild against new environment XFree86-4.0.1-39k.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-4k]
- rebuild against new environment gcc-2.95.2, glibc-2.2.
- obsolete kdelibs-2.0-khtml-m17n-20001024.diff

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-3k]
- rebuild against qt 2.2.2.

* Wed Nov 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-2k]
- update kdelibs-2.0-gcc296.patch.

* Wed Nov 01 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-1k]
- update kdelibs-2.0-gcc296.patch
- update kdelibs-2.0-khtml-m17n-20001024.diff.
- add kdelibs-2.0-artswrapper.patch.
- dcopserver.la change package kdelibs-devel to kdelibs.
- bugfix %files sections.
- add GIF Support switch.
- release.

* Tue Oct 24 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- [2.0-0.1k]
- update to 2.0

* Thu Oct 19 2000 Toru Hoshina <toru@df-usa.com>
- [2.0rc2-0.5k]
- add kdelibs-2.0rc2-khtml-egcs112.diff for egcs-1.1.2 :-P

* Thu Oct 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.4k]
- modified Requires,BuildPrereq.

* Wed Oct 18 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.3k]
- add kdelibs-2.0rc2-khtml-m17n-20001013.diff.
- modified Requires,BuildPrereq

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.2k]
- enable debug mode.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- little bit fixed specfile.

* Wed Oct 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- update 2.0rc2.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.

* Thu Sep 23 2000 Kenichi Matsubara <m@kondara.org>
- bugfix specfile.[%files sections.]

* Wed Sep 20 2000 Kenichi Matsubara <m@kondara.org>
- update 1.94.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Fri Jun 23 2000 Kenichi Matsubara <m@kondara.org>
- add kdelibs-1.91-without-openssl-kondara.patch.

* Mon Jun 19 2000 Kenichi Matsubara <m@kondara.org>
- update 1.91.

* Tue May 16 2000 Kenichi Matsubara <m@kondara.org>
- initial release Kondara MNU/Linux.
