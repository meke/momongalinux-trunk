%global momorel 6

%global upname xfswitch-plugin
%global xfce4ver 4.10.0
%global major 0.0

Name:           xfce4-xfswitch-plugin
Version:        0.0.1
Release:        %{momorel}m%{?dist}
Summary:        User switching plugin for the Xfce4 Panel

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/panel-plugins/%{upname}
Source0:        http://archive.xfce.org/src/panel-plugins/%{upname}/%{major}/%{upname}-0.0.1.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       gdm
BuildRequires:  gettext intltool
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
Requires:       xfce4-panel >= %{xfce4ver}
Provides:       %{upname} = %{name}-%{version}

%description
Xfswitch-plugin is a user switching plugin for the Xfce4 Panel. It allows you 
to leave the current session opened and open a new session with another user. 
At the moment it relies on GDM, but other display managers will be supported 
in the future. 


%prep
%setup -qn %{upname}-%{version}


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
%find_lang %{upname}


%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{upname}.lang
%defattr(-,root,root,-)
%doc AUTHORS  ChangeLog COPYING NEWS README
%{_libexecdir}/xfce4/panel-plugins/%{upname}
%{_datadir}/xfce4/panel-plugins/*.desktop


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.1-6m)
- rebuild against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-5m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.1-1m)
- import from Fedora to Momonga

* Thu Feb 26 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.1-1
- Initial RPM package
