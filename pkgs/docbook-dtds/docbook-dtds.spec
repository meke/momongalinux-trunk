%global momorel 10

%global openjadever 1.3.2

Name: docbook-dtds
Version: 4.5
Release: %{momorel}m%{?dist}
Group: Applications/Text

Summary: SGML and XML document type definitions for DocBook.

License: see "docbook*.dtd"
URL: http://www.oasis-open.org/docbook/

Obsoletes: docbook-dtd30-sgml docbook-dtd31-sgml
Obsoletes: docbook-dtd40-sgml docbook-dtd41-sgml
Obsoletes: docbook-dtd412-xml

Provides: docbook-dtd-xml docbook-dtd-sgml
Provides: docbook-dtd30-sgml docbook-dtd31-sgml
Provides: docbook-dtd40-sgml docbook-dtd41-sgml
Provides: docbook-dtd412-xml
Provides: docbook-dtd42-sgml docbook-dtd42-xml
Provides: docbook-dtd43-sgml docbook-dtd43-xml
Provides: docbook-dtd44-sgml docbook-dtd44-xml
Provides: docbook-dtd45-sgml docbook-dtd45-xml

Requires(post): libxml2 >= 2.4.8
Requires(post): perl coreutils

Requires(postun): libxml2 >= 2.4.8
Requires(postun): coreutils perl

Requires: xml-common fileutils grep
# If upgrading, the old package's postun scriptlet may use install-catalog
# to remove its entries.  xmlcatalog (which this package uses) adds quotes
# to the catalog files, and install-catalog only handles this in 0.6.3-19m or
# later.
Requires: sgml-common >= 0.6.3-19m
# We provide the directory layout expected by 0.6.3-19m or later of
# xml-common.  Earlier versions won't understand.
Requires: xml-common >= 0.6.3-19m

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch
Source0: http://www.oasis-open.org/docbook/sgml/3.0/docbk30.zip
Source1: http://www.oasis-open.org/docbook/sgml/3.1/docbk31.zip
Source2: http://www.oasis-open.org/docbook/sgml/4.0/docbk40.zip
Source3: http://www.oasis-open.org/docbook/sgml/4.1/docbk41.zip
Source4: http://www.oasis-open.org/docbook/xml/4.1.2/docbkx412.zip
Source5: http://www.oasis-open.org/docbook/sgml/4.2/docbook-4.2.zip
Source6: http://www.oasis-open.org/docbook/xml/4.2/docbook-xml-4.2.zip
Source7: http://www.docbook.org/sgml/4.3/docbook-4.3.zip
Source8: http://www.docbook.org/xml/4.3/docbook-xml-4.3.zip
Source9: http://www.docbook.org/sgml/4.4/docbook-4.4.zip
Source10: http://www.docbook.org/xml/4.4/docbook-xml-4.4.zip
Source11: http://www.docbook.org/sgml/4.5/docbook-4.5.zip
Source12: http://www.docbook.org/xml/4.5/docbook-xml-4.5.zip
Patch0: docbook-dtd30-sgml-1.0.catalog.patch
Patch1: docbook-dtd31-sgml-1.0.catalog.patch
Patch2: docbook-dtd40-sgml-1.0.catalog.patch
Patch3: docbook-dtd41-sgml-1.0.catalog.patch
Patch4: docbook-dtd42-sgml-1.0.catalog.patch
Patch5: docbook-4.2-euro.patch
Patch6: docbook-dtds-ents.patch
Patch7: docbook-sgml-systemrewrite.patch
Patch8: docbook-dtd412-entities.patch
BuildRequires: unzip
Requires: openjade = %{openjadever}

%description
The DocBook Document Type Definition (DTD) describes the syntax of
technical documentation texts (articles, books and manual pages).
This syntax is XML-compliant and is developed by the OASIS consortium.
This package contains SGML and XML versions of the DocBook DTD.


%prep
%setup -c -T
# DocBook V3.0
mkdir 3.0-sgml
cd 3.0-sgml
unzip %{SOURCE0}
patch -b docbook.cat %{PATCH0}
cd ..

# DocBook V3.1
mkdir 3.1-sgml
cd 3.1-sgml
unzip %{SOURCE1}
patch -b docbook.cat %{PATCH1}
cd ..

# DocBook V4.0
mkdir 4.0-sgml
cd 4.0-sgml
unzip %{SOURCE2}
patch -b docbook.cat %{PATCH2}
cd ..

# DocBook V4.1
mkdir 4.1-sgml
cd 4.1-sgml
unzip %{SOURCE3}
patch -b docbook.cat %{PATCH3}
cd ..

# DocBook XML V4.1.2
mkdir 4.1.2-xml
cd 4.1.2-xml
unzip %{SOURCE4}
cd ..

# DocBook V4.2
mkdir 4.2-sgml
cd 4.2-sgml
unzip %{SOURCE5}
patch -b docbook.cat %{PATCH4}
cd ..

# DocBook XML V4.2
mkdir 4.2-xml
cd 4.2-xml
unzip %{SOURCE6}
cd ..

# DocBook V4.3
mkdir 4.3-sgml
cd 4.3-sgml
unzip %{SOURCE7}
cd ..

# DocBook XML V4.3
mkdir 4.3-xml
cd 4.3-xml
unzip %{SOURCE8}
cd ..

# DocBook V4.4
mkdir 4.4-sgml
cd 4.4-sgml
unzip %{SOURCE9}
cd ..

# DocBook XML V4.4
mkdir 4.4-xml
cd 4.4-xml
unzip %{SOURCE10}
cd ..

# DocBook v4.5
mkdir 4.5-sgml
cd 4.5-sgml
unzip %{SOURCE11}
cd ..

# DocBook XML v4.5
mkdir 4.5-xml
cd 4.5-xml
unzip %{SOURCE12}
cd ..

# Fix &euro; in SGML.
%patch5 -p1

# Fix ISO entities in 4.3/4.4 SGML
%patch6 -p1

# Rewrite SYSTEM to use local catalog instead web ones (#478680)
%patch7 -p1

# Add XML to the end of public identificators of 4.1.2 XML entities
%patch8 -p1

# Increase NAMELEN (bug #36058, bug #159382).
sed -e's,\(NAMELEN\s\+\)44\(\s\*\)\?,\1256,' -i.namelen */docbook.dcl

# fix of \r\n issue from rpmlint
sed -i 's/\r//' */*.txt


if [ `id -u` -eq 0 ]; then
  chown -R root:root .
  chmod -R a+rX,g-w,o-w .
fi


%build


%install
rm -rf $RPM_BUILD_ROOT

# DocBook V3.0
cd 3.0-sgml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/sgml-dtd-3.0-%{version}-%{release}
mkdir -p $DESTDIR
install *.dcl $DESTDIR
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
cd ..

# DocBook V3.1
cd 3.1-sgml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/sgml-dtd-3.1-%{version}-%{release}
mkdir -p $DESTDIR
install *.dcl $DESTDIR
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
cd ..

# DocBook V4.0
cd 4.0-sgml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/sgml-dtd-4.0-%{version}-%{release}
mkdir -p $DESTDIR
install *.dcl $DESTDIR
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
cd ..

# DocBook V4.1
cd 4.1-sgml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/sgml-dtd-4.1-%{version}-%{release}
mkdir -p $DESTDIR
install *.dcl $DESTDIR
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
cd ..

# DocBook XML V4.1.2
cd 4.1.2-xml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/xml-dtd-4.1.2-%{version}-%{release}
mkdir -p $DESTDIR/ent
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
install ent/* $DESTDIR/ent
cd ..

# DocBook V4.2
cd 4.2-sgml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/sgml-dtd-4.2-%{version}-%{release}
mkdir -p $DESTDIR
install *.dcl $DESTDIR
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
cd ..

# DocBook XML V4.2
cd 4.2-xml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/xml-dtd-4.2-%{version}-%{release}
mkdir -p $DESTDIR/ent
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
install ent/* $DESTDIR/ent
cd ..

# DocBook V4.3
cd 4.3-sgml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/sgml-dtd-4.3-%{version}-%{release}
mkdir -p $DESTDIR
install *.dcl $DESTDIR
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
cd ..

# DocBook XML V4.3
cd 4.3-xml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/xml-dtd-4.3-%{version}-%{release}
mkdir -p $DESTDIR/ent
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
install ent/* $DESTDIR/ent
cd ..

# DocBook V4.4
cd 4.4-sgml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/sgml-dtd-4.4-%{version}-%{release}
mkdir -p $DESTDIR
install *.dcl $DESTDIR
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
cd ..

# DocBook XML V4.4
cd 4.4-xml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/xml-dtd-4.4-%{version}-%{release}
mkdir -p $DESTDIR/ent
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
install ent/* $DESTDIR/ent
cd ..

# DocBook V4.5
cd 4.5-sgml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/sgml-dtd-4.5-%{version}-%{release}
mkdir -p $DESTDIR
install *.dcl $DESTDIR
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
cd ..

# DocBook XML V4.5
cd 4.5-xml
DESTDIR=$RPM_BUILD_ROOT/usr/share/sgml/docbook/xml-dtd-4.5-%{version}-%{release}
mkdir -p $DESTDIR/ent
install docbook.cat $DESTDIR/catalog
install *.dtd $DESTDIR
install *.mod $DESTDIR
install ent/* $DESTDIR/ent
cd ..


# Symlinks
mkdir -p $RPM_BUILD_ROOT/etc/sgml
ln -s sgml-docbook-4.5-%{version}-%{release}.cat \
	$RPM_BUILD_ROOT/etc/sgml/sgml-docbook.cat
ln -s xml-docbook-4.5-%{version}-%{release}.cat \
	$RPM_BUILD_ROOT/etc/sgml/xml-docbook.cat

# Files for %ghost
touch $RPM_BUILD_ROOT/etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat
touch $RPM_BUILD_ROOT/etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat

%clean
rm -rf $RPM_BUILD_ROOT


%post
## Clean up pre-docbook-dtds mess caused by broken trigger.
for v in 3.0 3.1 4.0 4.1 4.2
do
	if [ -f /etc/sgml/sgml-docbook-$v.cat ]
	then
		/usr/bin/xmlcatalog --sgml --noout --del \
			/etc/sgml/sgml-docbook-$v.cat \
			/usr/share/sgml/openjade-1.3.1/catalog 2>/dev/null
	fi
done

##
## SGML catalog
##

# Update the centralized catalog corresponding to this version of the DTD
# DocBook V3.0
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/sgml-dtd-3.0-%{version}-%{release}/catalog

# DocBook V3.1
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/sgml-dtd-3.1-%{version}-%{release}/catalog

# DocBook V4.0
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/sgml-dtd-4.0-%{version}-%{release}/catalog

# DocBook V4.1
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/sgml-dtd-4.1-%{version}-%{release}/catalog

# DocBook XML V4.1.2
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/xml-dtd-4.1.2-%{version}-%{release}/catalog

# DocBook V4.2
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/sgml-dtd-4.2-%{version}-%{release}/catalog

# DocBook XML V4.2
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/xml-dtd-4.2-%{version}-%{release}/catalog

# DocBook V4.3
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/sgml-dtd-4.3-%{version}-%{release}/catalog

# DocBook XML V4.3
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/xml-dtd-4.3-%{version}-%{release}/catalog

# DocBook V4.4
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/sgml-dtd-4.4-%{version}-%{release}/catalog

# DocBook XML V4.4
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/xml-dtd-4.4-%{version}-%{release}/catalog

# DocBook V4.5
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/sgml-dtd-4.5-%{version}-%{release}/catalog

# DocBook XML V4.5
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat \
	/usr/share/sgml/sgml-iso-entities-8879.1986/catalog
/usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/xml-dtd-4.5-%{version}-%{release}/catalog


# The following lines are for the case in which the style sheets
# were installed after another DTD but before this DTD
STYLESHEETS=$(echo /usr/share/sgml/docbook/dsssl-stylesheets-*)
STYLESHEETS=${STYLESHEETS##*/dsssl-stylesheets-}
if [ "$STYLESHEETS" != "*" ]; then
    # DocBook V3.0
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook V3.1
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook V4.0
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook V4.1
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook XML V4.1.2
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook V4.2
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook XML V4.2
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook V4.3
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook XML V4.3
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook V4.4
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook XML V4.4
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

   # DocBook V4.5
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

    # DocBook XML V4.5
    /usr/bin/xmlcatalog --sgml --noout --add \
	/etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat \
	/usr/share/sgml/docbook/dsssl-stylesheets-$STYLESHEETS/catalog

fi

# Fix up SGML super catalog so that there isn't an XML DTD before an
# SGML one.  We need to do this (*sigh*) because xmlcatalog messes up
# the order of the lines, and SGML tools don't like to see XML things
# they aren't expecting.
CAT_DIR=/usr/share/sgml/docbook/
CATALOG=/etc/sgml/catalog
SGML=$(cat -n ${CATALOG} | grep sgml-docbook | head -1 | (read n line;echo $n))
XML=$(cat -n ${CATALOG} | grep xml-docbook | head -1 | (read n line; echo $n))
# Do they need switching around?
if [ -n "${XML}" ] && [ -n "${SGML}" ] && [ "${XML}" -lt "${SGML}" ]
then
  # Switch those two lines around.
  XML=$((XML - 1))
  SGML=$((SGML - 1))
  perl -e "@_=<>;@_[$XML, $SGML]=@_[$SGML, $XML];print @_" \
    ${CATALOG} > ${CATALOG}.rpmtmp
  mv -f ${CATALOG}.rpmtmp ${CATALOG}
fi

##
## XML catalog
##

CATALOG=/usr/share/sgml/docbook/xmlcatalog

if [ -w $CATALOG ]
then
	# DocBook XML V4.1.2
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Publishing//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-pub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Letters//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-grk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Information Pool V4.1.2//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Box and Line Drawing//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-box.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML V4.1.2//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-grk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Negated Relations//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Numeric and Special Graphic//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-num.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Character Entities V4.1.2//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Alternative Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-grk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Notations V4.1.2//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Diacritical Marks//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-dia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Monotoniko Greek//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-grk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Additional General Entities V4.1.2//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Document Hierarchy V4.1.2//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Arrow Relations//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Ordinary//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-cyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES General Technical//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-tech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Delimiters//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD XML Exchange Table Model 19990315//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML CALS Table Model V4.1.2//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 1//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-lat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Binary Operators//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 2//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-lat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Relations//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Non-Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-cyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
		"http://www.oasis-open.org/docbook/xml/4.1.2" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteURI" \
		"http://www.oasis-open.org/docbook/xml/4.1.2" \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}" $CATALOG

	# DocBook XML V4.2
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Publishing//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-pub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Letters//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-grk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Information Pool V4.2//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Box and Line Drawing//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-box.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML V4.2//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-grk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Negated Relations//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Numeric and Special Graphic//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-num.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Character Entities V4.2//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Alternative Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-grk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Notations V4.2//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Diacritical Marks//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-dia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Monotoniko Greek//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-grk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Additional General Entities V4.2//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Document Hierarchy V4.2//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Arrow Relations//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Ordinary//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-cyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES General Technical//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-tech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Delimiters//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD XML Exchange Table Model 19990315//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML CALS Table Model V4.2//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 1//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-lat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Binary Operators//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 2//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-lat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Relations//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Non-Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-cyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
		"http://www.oasis-open.org/docbook/xml/4.2" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteURI" \
		"http://www.oasis-open.org/docbook/xml/4.2" \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}" $CATALOG

	# DocBook XML V4.3
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Publishing//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-pub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Letters//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-grk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Information Pool V4.3//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Box and Line Drawing//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-box.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML V4.3//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-grk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Negated Relations//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Numeric and Special Graphic//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-num.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Character Entities V4.3//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Alternative Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-grk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Notations V4.3//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Diacritical Marks//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-dia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Monotoniko Greek//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-grk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Additional General Entities V4.3//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Document Hierarchy V4.3//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Arrow Relations//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Ordinary//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-cyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES General Technical//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-tech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Delimiters//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD XML Exchange Table Model 19990315//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML CALS Table Model V4.3//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 1//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-lat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Binary Operators//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 2//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-lat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Relations//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Non-Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-cyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
		"http://www.oasis-open.org/docbook/xml/4.3" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteURI" \
		"http://www.oasis-open.org/docbook/xml/4.3" \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}" $CATALOG

	# DocBook XML V4.4
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Publishing//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isopub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Letters//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isogrk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Information Pool V4.4//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Box and Line Drawing//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isobox.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML V4.4//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isogrk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Negated Relations//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Numeric and Special Graphic//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isonum.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Character Entities V4.4//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Alternative Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isogrk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Notations V4.4//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Diacritical Marks//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isodia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Monotoniko Greek//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isogrk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Additional General Entities V4.4//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Document Hierarchy V4.4//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Arrow Relations//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Ordinary//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isocyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES General Technical//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isotech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Delimiters//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD XML Exchange Table Model 19990315//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML CALS Table Model V4.4//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 1//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isolat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Binary Operators//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 2//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isolat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Relations//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Non-Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isocyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
		"http://www.oasis-open.org/docbook/xml/4.4" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteURI" \
		"http://www.oasis-open.org/docbook/xml/4.4" \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}" $CATALOG

   # DocBook XML V4.5
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Publishing//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isopub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Letters//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isogrk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Information Pool V4.5//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Box and Line Drawing//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isobox.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML V4.5//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isogrk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Negated Relations//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Numeric and Special Graphic//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isonum.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Character Entities V4.5//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Alternative Greek Symbols//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isogrk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Notations V4.5//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Diacritical Marks//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isodia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Monotoniko Greek//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isogrk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ENTITIES DocBook XML Additional General Entities V4.5//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//ELEMENTS DocBook XML Document Hierarchy V4.5//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Arrow Relations//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Ordinary//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isocyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES General Technical//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isotech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Delimiters//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD XML Exchange Table Model 19990315//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"-//OASIS//DTD DocBook XML CALS Table Model V4.5//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 1//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isolat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Binary Operators//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Latin 2//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isolat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Added Math Symbols: Relations//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "public" \
		"ISO 8879:1986//ENTITIES Non-Russian Cyrillic//EN" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isocyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
		"http://www.oasis-open.org/docbook/xml/4.5" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}" $CATALOG
	/usr/bin/xmlcatalog --noout --add "rewriteURI" \
		"http://www.oasis-open.org/docbook/xml/4.5" \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}" $CATALOG

fi

# Finally, make sure everything in /etc/sgml is readable!
/bin/chmod a+r /etc/sgml/*

%triggerin -- openjade >= %{?openjadever}
#openjade catalog registration
  # DocBook V3.0
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V3.1
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.0
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog
 
  # DocBook V4.1
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.1.2
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.2
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.2
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.3
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.3
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.4
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.4
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.5
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.5
  /usr/bin/xmlcatalog --sgml --noout --add \
  /etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog
#openjade registration trigger end

%triggerun -- openjade >= %{?openjadever}
  [ $2 = 0 ] || exit 0
  #openjade catalog unregistration
  # DocBook V3.0
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V3.1
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.0
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog
 
  # DocBook V4.1
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.1.2
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.2
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.2
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.3
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.3
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook V4.4
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.4
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog
 
  # DocBook V4.5
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog

  # DocBook XML V4.5
  /usr/bin/xmlcatalog --sgml --noout --del \
  /etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat \
  /usr/share/sgml/openjade-%{openjadever}/catalog
#openjade unregistration trigger end


%postun
##
## SGML catalog
##

# Update the centralized catalog corresponding to this version of the DTD
# DocBook V3.0
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat
rm -f /etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat

# DocBook V3.1
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat
rm -f /etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat

# DocBook V4.0
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat
rm -f /etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat

# DocBook V4.1
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat
rm -f /etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat

# DocBook XML V4.1.2
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat
rm -f /etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat

# DocBook V4.2
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat
rm -f /etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat

# DocBook XML V4.2
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat
rm -f /etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat

# DocBook V4.3
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat
rm -f /etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat

# DocBook XML V4.3
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat
rm -f /etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat

# DocBook V4.4
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat
rm -f /etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat

# DocBook XML V4.4
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat
rm -f /etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat

# DocBook V4.5
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat
rm -f /etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat

# DocBook XML V4.5
/usr/bin/xmlcatalog --sgml --noout --del /etc/sgml/catalog \
	/etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat
rm -f /etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat


# Fix up SGML super catalog so that there isn't an XML DTD before an
# SGML one.  We need to do this (*sigh*) because xmlcatalog messes up
# the order of the lines, and SGML tools don't like to see XML things
# they aren't expecting.
CATALOG=/etc/sgml/catalog
SGML=$(cat -n ${CATALOG} | grep sgml-docbook | head -1 | (read n line;echo $n))
XML=$(cat -n ${CATALOG} | grep xml-docbook | head -1 | (read n line; echo $n))
# Do they need switching around?
if [ -n "${XML}" ] && [ -n "${SGML}" ] && [ "${XML}" -lt "${SGML}" ]
then
  # Switch those two lines around.
  XML=$((XML - 1))
  SGML=$((SGML - 1))
  perl -e "@_=<>;@_[$XML, $SGML]=@_[$SGML, $XML];print @_" \
    ${CATALOG} > ${CATALOG}.rpmtmp
  mv -f ${CATALOG}.rpmtmp ${CATALOG}
fi

##
## XML catalog
##

CAT_DIR=/usr/share/sgml/docbook/
CATALOG=/usr/share/sgml/docbook/xmlcatalog

if [ -w $CATALOG ]
then
	# DocBook XML V4.1.2
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-pub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-grk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-box.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-grk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-num.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-grk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-dia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-grk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-cyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-tech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-lat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-lat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-amsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}/ent/iso-cyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.1.2-%{version}-%{release}" $CATALOG

	# DocBook XML V4.2
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-pub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-grk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-box.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-grk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-num.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-grk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-dia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-grk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-cyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-tech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-lat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-lat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-amsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}/ent/iso-cyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.2-%{version}-%{release}" $CATALOG

	# DocBook XML V4.3
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-pub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-grk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-box.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-grk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-num.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-grk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-dia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-grk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-cyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-tech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-lat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-lat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-amsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}/ent/iso-cyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.3-%{version}-%{release}" $CATALOG

	# DocBook XML V4.4
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isopub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isogrk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isobox.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isogrk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isonum.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isogrk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isodia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isogrk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isocyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isotech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isolat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isolat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isoamsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}/ent/isocyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.4-%{version}-%{release}" $CATALOG

  # DocBook XML V4.5
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isopub.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isogrk1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbpoolx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isobox.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/docbookx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isogrk3.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsn.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isonum.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbcentx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isogrk4.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbnotnx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isodia.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isogrk2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbgenent.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/dbhierx.mod" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsa.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamso.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isocyr1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isotech.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsc.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/soextblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/calstblx.dtd" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isolat1.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsb.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isolat2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isoamsr.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}/ent/isocyr2.ent" $CATALOG
	/usr/bin/xmlcatalog --noout --del \
		"${CAT_DIR}xml-dtd-4.5-%{version}-%{release}" $CATALOG
fi

%files
%defattr (0644,root,root,0755)
#in upstream tarballs there is a lot of files with 0755 permissions
#but they don't need to be, 0644 is enough for every file in tarball
%doc --parents 3.1-sgml/ChangeLog
%doc --parents 4.1-sgml/ChangeLog
%doc --parents */*.txt
/etc/sgml/sgml-docbook.cat
/etc/sgml/xml-docbook.cat
/usr/share/sgml/docbook/sgml-dtd-3.0-%{version}-%{release}
/usr/share/sgml/docbook/sgml-dtd-3.1-%{version}-%{release}
/usr/share/sgml/docbook/sgml-dtd-4.0-%{version}-%{release}
/usr/share/sgml/docbook/sgml-dtd-4.1-%{version}-%{release}
/usr/share/sgml/docbook/sgml-dtd-4.2-%{version}-%{release}
/usr/share/sgml/docbook/sgml-dtd-4.3-%{version}-%{release}
/usr/share/sgml/docbook/sgml-dtd-4.4-%{version}-%{release}
/usr/share/sgml/docbook/sgml-dtd-4.5-%{version}-%{release}
/usr/share/sgml/docbook/xml-dtd-4.1.2-%{version}-%{release}
/usr/share/sgml/docbook/xml-dtd-4.2-%{version}-%{release}
/usr/share/sgml/docbook/xml-dtd-4.3-%{version}-%{release}
/usr/share/sgml/docbook/xml-dtd-4.4-%{version}-%{release}
/usr/share/sgml/docbook/xml-dtd-4.5-%{version}-%{release}
%ghost /etc/sgml/sgml-docbook-3.0-%{version}-%{release}.cat
%ghost /etc/sgml/sgml-docbook-3.1-%{version}-%{release}.cat
%ghost /etc/sgml/sgml-docbook-4.0-%{version}-%{release}.cat
%ghost /etc/sgml/sgml-docbook-4.1-%{version}-%{release}.cat
%ghost /etc/sgml/sgml-docbook-4.2-%{version}-%{release}.cat
%ghost /etc/sgml/sgml-docbook-4.3-%{version}-%{release}.cat
%ghost /etc/sgml/sgml-docbook-4.4-%{version}-%{release}.cat
%ghost /etc/sgml/sgml-docbook-4.5-%{version}-%{release}.cat
%ghost /etc/sgml/xml-docbook-4.1.2-%{version}-%{release}.cat
%ghost /etc/sgml/xml-docbook-4.2-%{version}-%{release}.cat
%ghost /etc/sgml/xml-docbook-4.3-%{version}-%{release}.cat
%ghost /etc/sgml/xml-docbook-4.4-%{version}-%{release}.cat
%ghost /etc/sgml/xml-docbook-4.5-%{version}-%{release}.cat

%changelog
* Sat Dec 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.5-10m)
- update patches

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5-6m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Mar 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.5-5m)
- sync Fedora. add Patch7,8

* Wed Feb 11 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.5-4m)
- sync Fedora, fix typo.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5-2m)
- rebuild against gcc43

* Sun Mar  2 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (4.5-1m)
- sync Fedora docbook-dtds-1.0-32

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.4-2m)
- rebuild against perl-5.10.0-1m

* Mon May 21 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4.4-1m)
- sync Fedora docbook-dtds-1.0-30.1

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.3-8m)
- delete duplicated file

* Wed Nov 17 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.3-7m)
- add version 4.3
- remove xml-4.1
- sync with Fedora (1.0-25)

* Thu Nov 04 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.3-6m)
- docbkx412.zip was replaced, change md5sum

* Thu Oct 28 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.3-5m)
- remake patch.

* Tue Apr 20 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.3-4m)
- fix script to delete openjade-1.3.1 entry

* Sun Apr 18 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.3-3m)
- add 4.2-sgml
- add script to delete openjade-1.3.1 entry from /etc/sgml/*.cat in %%post section (from Fedora Core 1)

* Tue Apr 13 2004 Hirouiki Koga <kuma@momonga-linux.org>
- (4.3-2m)
- rebuild against for openjade-1.3.2

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.3-1m)
- version 4.3

* Sun Dec 28 2003 Kenta MURATA <muraken2@nifty.com>
- (4.2-3m)
- collect symlink of xml catalog.
- %%post.

* Sun Dec 28 2003 Kenta MURATA <muraken2@nifty.com>
- (4.2-2m)
- %%post, %%postun, %%files.

* Sun Dec 28 2003 Kenta MURATA <muraken2@nifty.com>
- (4.2-1m)
- version 4.2.

* Fri Nov 21 2003 zunda <zunda at freeshell.org>
- (1.0-12m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Jul 11 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-10m)
- change BuildPreReq: xml-common => PreReq: xml-common

* Thu Jul 11 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-9m)
- change PreReq: xml-common => BuildPreReq: xml-common

* Wed May 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-8k)
- %%post, %%preun script bug fix. (mousukoshi)
- add sgml dtds

* Sun May 19 2002 Kenta MURATA <muraken@kondara.org>
- (1.0-6k)
- add mathml, htmlforms, EBNF modules.
- fix typo.

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-4k)
- fix source path

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.0-2k)
- new spec

* Mon Jan 28 2002 Tim Waugh <twaugh@redhat.com> 1.0-2
- Prepare for openjade 1.3.1.

* Thu Jan 17 2002 Tim Waugh <twaugh@redhat.com> 1.0-1
- Merged all the DTD packages into one (bug #58448).
- Use /usr/share/sgml exclusively.
- Prevent catalog files from disappearing on upgrade (bug #58463).

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Nov  5 2001 Tim Waugh <twaugh@redhat.com> 1.0-8
- Hmm, still need to depend on sgml-common for /etc/sgml.

* Mon Nov  5 2001 Tim Waugh <twaugh@redhat.com> 1.0-7
- Use xmlcatalog (libxml2) instead of install-catalog (sgml-common) in
  scriptlets.
- Conflict with install-catalog if it can't handle quotes in catalogs.
- Use release number in centralized catalog name, so that the scriptlets
  work properly.

* Wed Oct 10 2001 Tim Waugh <twaugh@redhat.com> 1.0-6
- Change some Requires: to PreReq:s (bug #54507).

* Mon Oct  8 2001 Tim Waugh <twaugh@redhat.com> 1.0-5
- Use release number in the installed directory name, so that the
  package scripts work.

* Sat Oct  6 2001 Tim Waugh <twaugh@redhat.com> 1.0-4
- Restore the /etc/sgml/catalog manipulation again.
- Oops, fix DTD path.

* Sat Oct  6 2001 Tim Waugh <twaugh@redhat.com> 1.0-2
- Require xml-common.  Use xmlcatalog.
- Move files to /usr/share/xml.

* Tue Jun 12 2001 Tim Waugh <twaugh@redhat.com> 1.0-1
- Build for Red Hat Linux.

* Sat Jun 09 2001 Chris Runge <crunge@pobox.com>
- Provides: docbook-dtd-xml (not docbook-dtd-sgml)
- undo catalog patch and dbcentx patch (this resulted in an effectively
  broken DTD when the document was processed with XSL stylesheets); added a
  symbolic link to retain docbook.cat -> catalog; added ent
- added ChangeLog to doc

* Fri Jun 08 2001 Chris Runge <crunge@pobox.com>
- created a 4.1.2 version
- update required a change to OTHERCAT in postun
- update required a change to the Makefile patch (no dbgenent.ent any more,
  apparently)

* Wed Jan 24 2001 Tim Waugh <twaugh@redhat.com>
- Scripts require fileutils.
- Make scripts quieter.

* Mon Jan 15 2001 Tim Waugh <twaugh@redhat.com>
- Don't play so many macro games.
- Don't use 'rpm' in post scripts.
- Be sure to own xml-dtd-4.1 directory.

* Sun Jan 14 2001 Tim Waugh <twaugh@redhat.com>
- Change requirement on /usr/bin/install-catalog to sgml-common.

* Tue Jan 09 2001 Tim Waugh <twaugh@redhat.com>
- Change group.
- Use %%{_tmppath}/%%{name}-%%{version}-root
- Correct typo.
- rm before install
- openjade not jade.
- Build requires unzip.
- Require install-catalog for post and postun.
- Change Copyright: to License:.
- Remove Packager: line.

* Tue Jan 09 2001 Tim Waugh <twaugh@redhat.com>
- Based on Eric Bischoff's new-trials packages.
