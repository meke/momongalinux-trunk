%global momorel 7

Name: hyphen-sl
Summary: Slovenian hyphenation rules
%define upstreamid 20070127 
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://ftp.services.openoffice.org/pub/OpenOffice.org/contrib/dictionaries/hyph_sl_SI.zip
Group: Applications/Text
URL: http://wiki.services.openoffice.org/wiki/Dictionaries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2+
BuildArch: noarch
Requires: hyphen

%description
Slovenian hyphenation rules.

%prep
%setup -q -c -n hyphen-sl

%build
chmod -x *

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/hyphen
cp -p hyph_sl_SI.dic $RPM_BUILD_ROOT/%{_datadir}/hyphen

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_hyph_sl_SI.txt
%{_datadir}/hyphen/*

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20070127-7m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20070127-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20070127-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20070127-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20070127-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20070127-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20070127-1m)
- import from Fedora

* Fri Nov 23 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070127-1
- initial version
