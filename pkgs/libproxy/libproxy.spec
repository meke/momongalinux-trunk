%global momorel 1
%global geckover 17.0.1

%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Name:           libproxy
Version:        0.4.11
Release:        %{momorel}m%{?dist}
Summary:        A library handling all the details of proxy configuration

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://code.google.com/p/libproxy/
Source0:        http://libproxy.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:		libproxy-0.4.10-mozjs.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  libmodman-devel >= 2.0.1
BuildRequires:  cmake >= 2.6.0
#Virtual Provides - We need either mozjs or WebKit
# Requires: %{name}-pac >= %{version}
#
Requires: libproxy-python = %{version}-%{release}
Requires: libproxy-bin = %{version}-%{release}

# gnome
BuildRequires:  GConf2-devel
# mozjs
BuildRequires:  gecko-devel >= %{geckover}
# NetworkManager
BuildRequires:  NetworkManager-devel
BuildRequires:  dbus-devel
# webkitgtk3 (gtk)
BuildRequires:  webkitgtk3-devel
# kde
BuildRequires:  kdelibs-devel
BuildRequires:  libXmu-devel
BuildRequires:  libX11-devel

# 0.4.11
Obsoletes: %{name}-webkit

%description
libproxy offers the following features:

    * extremely small core footprint (< 35K)
    * no external dependencies within libproxy core
      (libproxy plugins may have dependencies)
    * only 3 functions in the stable external API
    * dynamic adjustment to changing network topology
    * a standard way of dealing with proxy settings across all scenarios
    * a sublime sense of joy and accomplishment 

Non-default rpmbuild options:
--with webkit:           Enable WebKit-gtk support


%package        bin
Summary:        Binary to test %{name}
Group:          Applications/System
Requires:       %{name} = %{version}-%{release}

%description    bin
The %{name}-bin package contains the proxy binary for %{name}

%package        python
Summary:        Binding for %{name} and python
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description    python
The %{name}-python package contains the python binding for %{name}

%package        gnome
Summary:        Plugin for %{name} and gnome
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description    gnome
The %{name}-gnome package contains the %{name} plugin for gnome.

%package        kde
Summary:        Plugin for %{name} and kde
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description    kde
The %{name}-kde package contains the %{name} plugin for kde.

%package        mozjs
Summary:        Plugin for %{name} and mozjs
Group:          System Environment/Libraries
Requires:       %{name} = %{version}
#Tweak this according to the current gecko-libs version
Requires:       gecko-libs >= %{gecko_version}
Provides:       %{name}-pac = %{version}-%{release}

%description    mozjs
The %{name}-mozjs package contains the %{name} plugin for mozjs.

%package        networkmanager
Summary:        Plugin for %{name} and networkmanager
Group:          System Environment/Libraries
Requires:       %{name} = %{version}

%description    networkmanager
The %{name}-networkmanager package contains the %{name} plugin for networkmanager.

%package        webkitgtk3
Summary:        Plugin for %{name} and webkitgtk3
Group:          System Environment/Libraries
Requires:       %{name} = %{version}
Provides:       %{name}-pac = %{version}-%{release}

%description    webkitgtk3
The %{name}-webkit package contains the %{name} plugin for webkitgtk3.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q
%patch0 -p1 -b .orig

%build
PATH="%{_qt4_bindir}:$PATH" %cmake \
    -DLIBEXEC_INSTALL_DIR=%{_libexecdir} \
    -DMODULE_INSTALL_DIR=%{_libdir}/%{name}/%{version}/modules \
    -DPYTHON_SITEPKG_DIR=%{python_sitearch} \
    -DWITH_PERL=OFF \
    -DWITH_GNOME3=ON \
    -DWITH_VALA=ON \
    -DWITH_WEBKIT3=ON \
    -DWITH_MOZJS=ON

make VERBOSE=1 %{?_smp_mflags}

%if 0
%check
make test
%endif

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/*.so.*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/%{version}
%dir %{_libdir}/%{name}/%{version}/modules

%files bin
%defattr(-,root,root,-)
%{_bindir}/proxy

%files python
%defattr(-,root,root,-)
%{python_sitearch}/*

%files gnome
%defattr(-,root,root,-)
%{_libdir}/%{name}/%{version}/modules/config_gnome3.so
%{_libexecdir}/pxgsettings

%files kde
%defattr(-,root,root,-)
%{_libdir}/%{name}/%{version}/modules/config_kde4.so

%files mozjs
%defattr(-,root,root,-)
%{_libdir}/%{name}/%{version}/modules/pacrunner_mozjs.so

%files networkmanager
%defattr(-,root,root,-)
%{_libdir}/%{name}/%{version}/modules/network_networkmanager.so

%files webkitgtk3
%defattr(-,root,root,-)
%{_libdir}/%{name}/%{version}/modules/pacrunner_webkit.so

%files devel
%defattr(-,root,root,-)
%{_includedir}/proxy.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/libproxy-1.0.pc
%{_datadir}/cmake/Modules/Findlibproxy.cmake
%{_datadir}/vala/vapi/libproxy-1.0.vapi

%changelog
* Tue Dec 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.11-1m)
- [SECURITY] CVE-2012-5580
- update to 0.4.11
- disable test
- revive mozjs package
- add webkitgtk3 package

* Wed Mar 28 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.7-3m)
- build without wegkit-1.4.0

* Fri Dec  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.7-2m)
- enable to build on x86_64

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.7-1m)
- update to 0.4.7
- delete webkit and mozjs package (pac)

* Sat Aug 20 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.4.6-8m)
- fix BR

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.6-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-6m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-5m)
- fix build

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-4m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.6-3m)
- rebuild against webkitgtk-1.3.4

* Sun Sep 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-2m)
- fix build on x86_64

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.6-1m)
- update to 0.4.6
- delete patch1 (merged)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-liux.org>
- (0.4.4-1m)
- update to 0.4.4

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-liux.org>
- (0.3.1-1m)
- update to 0.3.1
- sync with Fedora 13

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-5m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-3m)
- rebuild against xulrunner-1.9.1

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-2m)
- rebuild against WebKit-1.1.1

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.3-1m)
- import from Fedora

* Thu Jan 22 2009 kwizart < kwizart at gmail.com > - 0.2.3-8
- Merge NetworkManager module into the main libproxy package
- Main Requires the -python and -bin subpackage 
 (splitted for multilibs compliance).

* Fri Oct 24 2008 kwizart < kwizart at gmail.com > - 0.2.3-7
- Disable Gnome/KDE default support via builtin modules.
 (it needs to be integrated via Gconf2/neon instead).

* Tue Oct 21 2008 kwizart < kwizart at gmail.com > - 0.2.3-6
- Disable Obsoletes.
- Requires ev instead of evr for optionnals sub-packages.

* Tue Oct 21 2008 kwizart < kwizart at gmail.com > - 0.2.3-5
- Use conditionals build.

* Mon Sep 15 2008 kwizart < kwizart at gmail.com > - 0.2.3-4
- Remove plugin- in the name of the packages

* Mon Aug  4 2008 kwizart < kwizart at gmail.com > - 0.2.3-3
- Move proxy.h to libproxy/proxy.h
  This will prevent it to be included in the default include path
- Split main to libs and util and use libproxy to install all

* Mon Aug  4 2008 kwizart < kwizart at gmail.com > - 0.2.3-2
- Rename binding-python to python
- Add Requires: gecko-libs >= %%{gecko_version}
- Fix some descriptions
- Add plugin-webkit package
 
* Fri Jul 11 2008 kwizart < kwizart at gmail.com > - 0.2.3-1
- Convert to Fedora spec

* Fri Jun 6 2008 - dominique-rpm@leuenberger.net
- Updated to version 0.2.3
* Wed Jun 4 2008 - dominique-rpm@leuenberger.net
- Extended spec file to build all available plugins
* Tue Jun 3 2008 - dominique-rpm@leuenberger.net
- Initial spec file for Version 0.2.2

