%global momorel 1
Summary: Symbolic GNOME icons
Name: gnome-icon-theme-symbolic
Version: 3.6.0
Release: %{momorel}m%{?dist}
#VCS: git:git://git.gnome.org/gnome-icon-theme-symbolic
Source0: http://download.gnome.org/sources/gnome-icon-theme-symbolic/3.6/%{name}-%{version}.tar.xz
NoSource: 0
License: CC-BY-SA
BuildArch: noarch
Group: User Interface/Desktops
BuildRequires: icon-naming-utils >= 0.8.7
Requires: gnome-icon-theme >= 2.30.2.1-2

%description
This package contains symbolic icons for use by the GNOME desktop.

%prep
%setup -q

%build
# Avoid a BuildRequires on gtk2-devel
export ac_cv_path_GTK_UPDATE_ICON_CACHE=/bin/true

%configure

%install
make install DESTDIR=%{buildroot}

%post
touch --no-create %{_datadir}/icons/gnome &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/gnome &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :

%files
%defattr(-,root,root)
%doc COPYING AUTHORS
%{_datadir}/icons/gnome/*

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-2m)
- reimport from fedora

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.0-1m)
- update to 2.31.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- initial build

