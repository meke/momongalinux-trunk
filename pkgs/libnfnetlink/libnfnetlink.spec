%global momorel 4

Name:           libnfnetlink
Version:        1.0.0
Release:        %{momorel}m%{?dist}
Summary:        Netfilter netlink userspace library
Group:          System Environment/Libraries
License:        GPLv2
URL:            http://netfilter.org/projects/libnfnetlink/index.html
Source0:        ftp://ftp.netfilter.org/pub/libnfnetlink/libnfnetlink-%{version}.tar.bz2
NoSource:       0
Source1:        http://www.gnu.org/licenses/gpl.txt
Patch0:         libnfnetlink-sysheader.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kernel-headers
BuildRequires:  automake autoconf libtool pkgconfig

%description
libnfnetlink is a userspace library that provides some low-level
nfnetlink handling functions.  It is used as a foundation for other, netfilter
subsystem specific libraries such as libnfnetlink_conntrack, libnfnetlink_log
and libnfnetlink_queue.

%package        devel
Summary:        Netfilter netlink userspace library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       kernel-headers

%description    devel
libnfnetlink is a userspace library that provides some low-level
nfnetlink handling functions.  It is used as a foundation for other, netfilter
subsystem specific libraries such as libnfnetlink_conntrack, libnfnetlink_log
and libnfnetlink_queue.

%prep
%setup -q
cp %{SOURCE1} LICENSE
%patch0 -p1

autoreconf -i --force

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name "*.la" -exec rm -f {} ';'

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README LICENSE
%{_libdir}/*.so.*


%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/libnfnetlink
%{_includedir}/libnfnetlink/*.h

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.41-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.41-1m)
- update to 0.0.41

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.33-0.1.svn7211.3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.33-0.1.svn7211.2m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.33-0.1.svn7211.1m)
- import from Fedora to Momonga for libnetfilter_conntrack

* Fri Feb 22 2008 Paul P. Komkoff Jr <i@stingr.net> - 0.0.33-0.1.svn7211
- grab latest upstream changes and fixes, along with new version number
- do not mess with bundled nfnetlink.h, use <linux/netfilter/nfnetlink.h>

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.0.30-2
- Autorebuild for GCC 4.3

* Thu Aug 30 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.30-1
- new upstream version

* Sun Mar 25 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.25-2
- grab ownership of some directories

* Fri Feb  9 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.25-1
- upstream version 0.0.25

* Sun Sep 10 2006 Paul P. Komkoff Jr <i@stingr.net>
- rebuilt

* Wed Jul 12 2006 Felipe Kellermann <stdfk@terra.com.br> - 0.0.16-1
- Adds pkgconfig to devel files.
- Version 0.0.16.

* Mon May  8 2006 Paul P Komkoff Jr <i@stingr.net> - 0.0.14-3
- Include borrowed gpl.txt as LICENSE in %doc

* Sun Mar 26 2006 Paul P Komkoff Jr <i@stingr.net> - 0.0.14-1
- Preparing for submission to fedora extras

