%global         momorel 1

Name:           perl-Role-REST-Client
Version:        0.18
Release:        %{momorel}m%{?dist}
Summary:        REST Client Role
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Role-REST-Client/
Source0:        http://www.cpan.org/authors/id/K/KA/KAARE/Role-REST-Client-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.010
BuildRequires:  perl-Data-Serializer
BuildRequires:  perl-File-Temp
BuildRequires:  perl-HTTP-Thin
BuildRequires:  perl-HTTP-Tiny
BuildRequires:  perl-JSON >= 2.00
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Moose
BuildRequires:  perl-MooseX-Traits
BuildRequires:  perl-MooX-HandlesVia
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Try-Tiny
BuildRequires:  perl-Type-Tiny
BuildRequires:  perl-URI
BuildRequires:  perl-URI-Escape-XS
BuildRequires:  perl-XML-Simple
BuildRequires:  perl-YAML
Requires:       perl-Data-Serializer
Requires:       perl-HTTP-Thin
Requires:       perl-HTTP-Tiny
Requires:       perl-Moose
Requires:       perl-MooseX-Traits
Requires:       perl-MooX-HandlesVia
Requires:       perl-Try-Tiny
Requires:       perl-Type-Tiny
Requires:       perl-URI
Requires:       perl-URI-Escape-XS
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This REST Client role makes REST connectivety easy.

%prep
%setup -q -n Role-REST-Client-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/Role/REST/Client
%{perl_vendorlib}/Role/REST/Client.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- rebuild against perl-5.20.0
- update to 0.18

* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.18.2

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-2m)
- rebuild against perl-5.18.0

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-2m)
- rebuild against perl-5.16.3

* Mon Nov 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12
- rebuild against perl-5.16.0

* Fri Mar 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-1m)
- update to 0.04

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- update to 0.03

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
