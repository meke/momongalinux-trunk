%global momorel 7

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-turboflot
Version:        0.1.2
Release:        %{momorel}m%{?dist}
Summary:        A TurboGears widget for Flot, a jQuery plotting library

Group:          Development/Languages
License:        MIT
URL:            http://pypi.python.org/pypi/TurboFlot
Source0:        http://pypi.python.org/packages/source/T/TurboFlot/TurboFlot-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7 TurboGears python-markdown
Requires:       TurboGears python-simplejson

%description
A TurboGears widget for Flot, a jQuery plotting library.

%prep
%setup -q -n TurboFlot-%{version}


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT \
        --install-data=%{python_sitelib}/turboflot/static/

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README
# For noarch packages: sitelib
%{python_sitelib}/*


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.2-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-4m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-3m)
- add BuildRequires: python-markdown

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-1m)
- import from Fedora 11
- update to 0.1.2

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.1.1-2
- Rebuild for Python 2.6

* Sun May 11 2008 Luke Macken <lmacken@redhat.com> - 0.1.1-1
- Call flot only when the document is ready
- Properly instantiate the TurboFlot widget

* Sat Mar 08 2008 Luke Macken <lmacken@redhat.com> - 0.1.0-1
- Update to jQuery 1.2.3 and flot 0.4

* Tue Feb 27 2008 Luke Macken <lmacken@redhat.com> - 0.0.9-1
- 0.0.9, which contains the following changes, thanks to Arthur Clune:
  - Support an optional graph 'label'
  - Add a css file for themeing the label
  - Fix the user-specified IDs

* Mon Feb 25 2008 Luke Macken <lmacken@redhat.com> - 0.0.8-1
- Allow for user-specified IDs

* Mon Feb 25 2008 Luke Macken <lmacken@redhat.com> - 0.0.7-1
- Trivial bugfix to fix widget IDs 

* Mon Feb 18 2008 Luke Macken <lmacken@redhat.com> - 0.0.6-1
- Update to 0.0.6

* Sun Feb 17 2008 Luke Macken <lmacken@redhat.com> - 0.0.5-3
- Fix our URL and Source0

* Sat Feb 16 2008 Luke Macken <lmacken@redhat.com> - 0.0.5-2
- Add TurboGears to BuildRequires and Requires
- Require python-simplejson
- Fix description
- Fix Source0

* Wed Feb 13 2008 Luke Macken <lmacken@redhat.com> - 0.0.5-1
- Ok, lets try this random id generation one more time.

* Wed Feb 13 2008 Luke Macken <lmacken@redhat.com> - 0.0.4-1
- Give each widget a "unique" id

* Tue Jan  8 2008 Luke Macken <lmacken@redhat.com> - 0.0.3-1
- Rename turboflot.widget to turboflot.widgets to fix some naming issues

* Fri Jan  4 2008 Luke Macken <lmacken@redhat.com> - 0.0.2-1
- Install the javascript files to our static directory

* Thu Dec 20 2007 David Malcolm <dmalcolm@redhat.com> - 0.0.1-1
- initial packaging
