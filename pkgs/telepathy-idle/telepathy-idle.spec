%global         momorel 1

Name:           telepathy-idle
Version:        0.1.16
Release:        %{momorel}m%{?dist}
Summary:        IRC connection manager for Telepathy
Group:          Applications/Communications
License:        LGPLv2+
URL:            http://telepathy.freedesktop.org/wiki/
Source0:        http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  openssl-devel >= 0.9.7
BuildRequires:  telepathy-glib-devel >= 0.14.3
BuildRequires:  libxslt
BuildRequires:  python
Requires:       telepathy-filesystem

%description
A full-featured IRC connection manager for the Telepathy project.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%check
# make check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS
%{_libexecdir}/%{name}
%{_datadir}/dbus-1/services/*.service
%{_datadir}/telepathy/managers/*.manager
%{_mandir}/man8/%{name}.8*

%changelog
* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.16-1m)
- update to 0.1.16

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.14-1m)
- update to 0.1.14

* Fri Oct  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.12-2m)
- import patch from Fedora to fix build error

* Sun Aug 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.12-1m)
- update to 0.1.12

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.11-1m)
- update to 0.1.11

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.10-1m)
- update to 0.1.10

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.9-1m)
- update to 0.1.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-2m)
- rebuild for new GCC 4.6

* Sat Feb 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.8-1m)
- update to 0.1.8

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.6-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.6-1m)
- import from Fedora devel

* Fri Feb 19 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.1.6-1
- Update to 0.1.6.

* Mon Sep 14 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.1.5-1
- Update to 0.1.5.
- Drop glibc patch.  Fixed upstream.

* Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 0.1.4-3
- rebuilt with new openssl

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Jun 27 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.1.4-1
- Update to 0.1.4.
- Add patch to fix glibc compilation bug.

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 18 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.1.3-1
- Update to 0.1.3.
- Bump minimum version of tp-glib-devel needed.

* Sun Jan 18 2009 Tomas Mraz <tmraz@redhat.com> - 0.1.2-4
- rebuild with new openssl

* Fri Feb  8 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.1.2-3
- Rebuild for gcc-4.3.

* Wed Dec  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.1.2-2
- rebuild for new libssl.so.6/libcrypto.so.6

* Sat Nov 24 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.1.2-1
- Update to 0.1.2.
- Add BR for telepathy-glib-devel, libxslt, & python.

* Tue Aug 21 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.1.1-3
- Rebuild.

* Sun Aug  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.1.1-2
- Update license tag.

* Tue Jun 19 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.1.1-1
- Update to 0.1.1.
- Add check section for tests.
- Add BR on telepathy-glib-unstable-static.

* Mon Apr 16 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.0.5-1
- Initial spec file.

