%global momorel 4

Name:           libdap
Summary:        The C++ DAP2 library from OPeNDAP
Version:        3.11.0
Release:        %{momorel}m%{?dist}
License:        LGPLv2+
Group:          Development/Libraries
URL:            http://www.opendap.org/
Source0:        http://www.opendap.org/pub/source/%{name}-%{version}.tar.gz
NoSource:       0
#Don't run HTTP tests - builders don't have network connections
Patch2:         libdap-3.10.2-offline.patch
Patch3:		libdap-3.11.0-curl.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cppunit-devel
BuildRequires:  curl-devel
BuildRequires:  doxygen
BuildRequires:  graphviz
BuildRequires:  libuuid-devel
BuildRequires:  libxml2-devel
BuildRequires:  pkgconfig

%description
The libdap++ library contains an implementation of DAP2. This package
contains the library, dap-config, getdap and deflate. The script dap-config
simplifies using the library in other projects. The getdap utility is a
simple command-line tool to read from DAP2 servers. It is built using the
library and demonstrates simple uses of it. The deflate utility is used by
the library when it returns compressed responses.

%package devel
Summary:        Static libraries and header files from libdap
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       curl-devel >= 7.10.6
Requires:       libxml2-devel >= 2.5.7
# for the /usr/share/aclocal directory ownership
Requires:       automake

%description devel
This package contains all the files needed to develop applications that
will use libdap.

%package doc
Summary:        Documentation of the libdap library
Group:          Documentation

%description doc
Documentation of the libdap library.

%prep
%setup -q
%patch2 -p1 -b .offline
%patch3 -p1 -b .curl~

iconv -f latin1 -t utf8 < COPYRIGHT_W3C > COPYRIGHT_W3C.utf8
touch -r COPYRIGHT_W3C COPYRIGHT_W3C.utf8
mv COPYRIGHT_W3C.utf8 COPYRIGHT_W3C

%build
%configure --disable-static --disable-dependency-tracking
make %{?_smp_mflags}
make docs

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"
find %{buildroot} -name "*.la" -exec rm -f {} \;
mv %{buildroot}%{_bindir}/dap-config-pkgconfig %{buildroot}%{_bindir}/dap-config

rm -rf __dist_docs
cp -pr docs __dist_docs
# those .map and .md5 are of dubious use, remove them
rm -f __dist_docs/html/*.map __dist_docs/html/*.md5
# use the ChangeLog timestamp to have the same timestamps for the doc files 
# for all arches
touch -r ChangeLog __dist_docs/html/*

%check
# continue if tests fail...
make check ||:

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README NEWS COPYING COPYRIGHT_URI README.dodsrc
%doc COPYRIGHT_W3C
%{_bindir}/getdap
%{_libdir}/libdap.so.*
%{_libdir}/libdapclient.so.*
%{_libdir}/libdapserver.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libdap.so
%{_libdir}/libdapclient.so
%{_libdir}/libdapserver.so
%{_bindir}/dap-config
%{_includedir}/libdap/
%{_datadir}/aclocal/*
%{_libdir}/pkgconfig/*.pc

%files doc
%defattr(-,root,root,-)
%doc COPYING COPYRIGHT_URI COPYRIGHT_W3C
%doc __dist_docs/html/

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.11.0-4m)
- rebuild against graphviz-2.36.0-1m

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.0-3m)
- fix build failure; add patch for curl

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.0-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11.0-1m)
- update to 3.11.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.2-2m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.2-1m)
- update to 3.10.2
- sepaate doc package
- sync with Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.2-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.2-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.2-1m)
- update to 3.8.2

* Sun Jun 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.1-1m)
- update to 3.8.1

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.10-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.10-4m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.7.10-3m)
- rebuild against openldap-2.4.8

* Mon Feb 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.7.10-2m)
- do not use %%{?_smp_mflags}

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.10-1m)
- update 3.7.10
- update patch for gcc43
- add pkgconfig support

* Mon Jan 21 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.2-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.2-1m)
- import from fc7 to Momonga
- delete .la files

* Tue Oct 31 2006 Patrice Dumas <pertusus@free.fr> 3.7.2-3
- rebuild for new libcurl soname

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 3.7.2-2
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Tue Sep 19 2006 Patrice Dumas <pertusus@free.fr> 3.7.2-1
- update to 3.7.2

* Wed Sep  6 2006 Patrice Dumas <pertusus@free.fr> 3.7.1-1
- update to 3.7.1
- set licence to LGPL instead of W3C/LGPL, since only deflate is W3C, so
  the whole is under the LGPL

* Fri Jul 21 2006 Patrice Dumas <pertusus@free.fr> 3.7.0-1
- update to 3.7.0

* Mon Feb 27 2006 James Gallagher <jgallagher@opendap.org> - 3.6.0-1
- update to 3.6.0

* Mon Nov 21 2005 Patrice Dumas <pertusus@free.fr> - 3.5.3-2
- fix Source0

* Tue Aug 30 2005 Patrice Dumas <pertusus@free.fr> - 3.5.2-3
- Add missing Requires

* Sat Jul  2 2005 Patrice Dumas <pertusus@free.fr> - 3.5.1-2
- Support for shared libraries
- Add COPYING
- Update with fedora template

* Thu May 12 2005 James Gallagher <jimg@comet.opendap.org> - 3.5.0-1
- Changed: Requires xml2 to libxml2

* Wed May 11 2005 James Gallagher <jimg@zoey.opendap.org> 3.5.0-1
- Removed version numbers from .a and includes directory.

* Tue May 10 2005 James Gallagher <jimg@zoey.opendap.org> 
- Mostly works. Problems: Not sure if the %%post script stuff works.
- Must also address the RHEL3 package deps issue (curl 7.12.0 isn't available;
  not sure about xml2 2.5.7). At least the deps fail when they are not present!

* Fri May  6 2005 James Gallagher <jimg@zoey.opendap.org> 
- Initial build.
