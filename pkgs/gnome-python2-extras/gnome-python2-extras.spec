%global momorel 29
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define glib_version 2.6.0
%define gtk_version 2.4.0
%define gnome_panel_version 2.2.0
%define gnome_python_version 2.10.0
%define gtkspell_version 2.0.7
%define libgda_version 3.99.9
%define libgdl_version 2.24.0

%define enable_gda 1
%define enable_gdl 0

### Abstract ###

Name: gnome-python2-extras
Version: 2.25.3
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: Development/Languages
Summary: Additional PyGNOME Python extension modules
URL: http://www.pygtk.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/gnome-python-extras/2.25/gnome-python-extras-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Obsoletes: gnome-python2-gda <= 2.14.3-1
Obsoletes: gnome-python2-gda-devel <= 2.14.3-1

Obsoletes: gnome-python2-gdl < 2.25.3-36
Obsoletes: gnome-python2-gtkhtml2 < 2.25.3-36
Obsoletes: gnome-python2-gtkmozembed < 2.25.3-36

### Patches ###

# GNOME bug #584126
Patch1: gnome-python-extras-2.25.3-update-for-2.27.2.patch

### Dependencies ###

Requires: gnome-python2 >= %{gnome_python_version}

### Build Dependencies ###

BuildRequires: glib2 >= %{glib_version}
BuildRequires: gnome-panel-devel >= %{gnome_panel_version}
BuildRequires: gnome-python2-bonobo >= %{gnome_python_version}
BuildRequires: gnome-python2-devel >= %{gnome_python_version}
BuildRequires: gnome-python2-gnome >= %{gnome_python_version}
BuildRequires: gtk2 >= %{gtk_version}
BuildRequires: gtkspell-devel >= %{gtkspell_version}
BuildRequires: libbonoboui-devel
BuildRequires: pygtk2-devel
BuildRequires: python-devel

%if %{enable_gda}
BuildRequires: libgda4-devel >= %{libgda_version}
%endif

%if %{enable_gdl}
BuildRequires: libgdl-devel >= %{libgdl_version}
%endif

%description
The gnome-python-extra package contains the source packages for additional 
Python bindings for GNOME. It should be used together with gnome-python.

%if %{enable_gda}
%package -n gnome-python2-gda
Summary: Python bindings for interacting with libgda
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libgda4 >= %{libgda_version}

%description -n gnome-python2-gda
This module contains a wrapper that allows the use of libgda via Python.

%package -n gnome-python2-gda-devel
Summary: Headers for developing programs that will use gnome-python2-gda
Group: Development/Libraries
Requires: gnome-python2-gda = %{version}-%{release}
Requires: pkgconfig
Requires: pygobject2-devel
Requires: libgda4-devel >= %{libgda_version}

%description -n gnome-python2-gda-devel
This module contains files needed for developing applications using
gnome-python2-gda.
%endif

%if %{enable_gdl}
%package -n gnome-python2-gdl
Summary: Python bindings for the GNOME Development Library
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: libgdl >= %{libgdl_version}

%description -n gnome-python2-gdl
This module contains a wrapper that allows the use of the GNOME Development
Library (gdl) via Python.
%endif

%package -n gnome-python2-gtkspell
Summary: Python bindings for interacting with gtkspell
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: gtkspell >= %{gtkspell_version}

%description -n gnome-python2-gtkspell
This module contains a wrapper that allows the use of gtkspell via Python.

%package -n gnome-python2-libegg
Summary: Python bindings for recent files and tray icons
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: gnome-python2-bonobo >= %{gnome_python_version}
Requires: gnome-python2-gnome >= %{gnome_python_version}

%description -n gnome-python2-libegg
This module contains a wrapper that allows the use of recent files and tray
icons via Python.

%prep
%setup -q -n gnome-python-extras-%{version}
%patch1 -p1 -b .update-for-2.27.2

%build

%if %{enable_gda}
%define gda_flags --enable-gda
%else
%define gda_flags --disable-gda
%endif

%if %{enable_gdl}
%define gdl_flags --enable-gdl
%else
%define gdl_flags --disable-gdl
%endif

%configure --enable-docs %gda_flags %gdl_flags
%make 

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name '*.la' -exec rm {} \;

rm -rf %{buildroot}/%{python_sitearch}/gtk-2.0/gksu
rm -rf %{buildroot}/%{python_sitearch}/gtk-2.0/gksu2
rm -rf %{buildroot}/%{python_sitearch}/gtk-2.0/gtkhtml2.so


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS
%{_libdir}/pkgconfig/gnome-python-extras-2.0.pc
%{_datadir}/pygtk

%if %{enable_gda}
%files -n gnome-python2-gda
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gda.so

%files -n gnome-python2-gda-devel
%defattr(-,root,root,-)
%{_includedir}/pygda-4.0/
%{_libdir}/pkgconfig/pygda-4.0.pc
%endif

%if %{enable_gdl}
%files -n gnome-python2-gdl
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gdl.so
%endif

%files -n gnome-python2-gtkspell
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/gtkspell.so
%{_datadir}/gtk-doc/html/pygtkspell

%files -n gnome-python2-libegg
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/egg
%defattr(644,root,root,755)
%doc examples/egg/*

%changelog
* Sat Jul 07 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.25.3-29m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.25.3-28m)
- rebuild for glib 2.33.2

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-27m)
- rebuild against xulrunner-6.0
- we no longer support gtkmozembed

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-26m)
- rebuild against xulrunner-5.0

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.25.3-25m)
- rebuild for python-2.7

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-24m)
- rebuild against xulrunner-2.0.1

* Fri Apr 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-23m)
- rebuild against xulrunner-1.9.2.17

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.25.3-22m)
- rebuild for new GCC 4.6

* Thu Mar 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-21m)
- rebuild against xulrunner-1.9.2.16

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-20m)
- rebuild against xulrunner-1.9.2.14

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-19m)
- rebuild against xulrunner-1.9.2.13

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.25.3-18m)
- rebuild for new GCC 4.5

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-17m)
- rebuild against xulrunner-1.9.2.12

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-16m)
- rebuild against xulrunner-1.9.2.11

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.3-15m)
- delete conflict dirs

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.25.3-14m)
- rebuild against xulrunner-1.9.2.10

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.3-13m)
- rebuild against xulrunner-1.9.2.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.25.3-12m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.25.3-11m)
- change Requires libgdl
- change BuildRequires libgdl-devel

* Sun Jul 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.25.3-10m)
- change Requires from libgdl to gdl
- change BuildRequires from libgdl-devel to gdl-devel

* Sun Jul 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.25.3-9m)
- add Provides and Obsoletes

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.25.3-8m)
- separate package

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.3-7m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Masanobu Sato <satoshiga@momonga-linux.prg>
- (2.25.3-5m)
- use %%python_sitearch argument for python library directory name

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.3-4m)
- disable gdl

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.3-3m)
- add gda-devel package

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.3-2m)
- rebuild against xulrunner-1.9.1

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.3-1m)
- update to 2.25.3

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-2m)
- fix BPR

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-1m)
- update to 2.25.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19.1-16m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19.1-15m)
- update Patch0 for fuzz=0
- License: LGPLv2+

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.19.1-14m)
- rebuild against python-2.6.1-1m

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.1-13m)
- rebuild against gdl-2.24.0

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19.1-12m)
- remove Requires: firefox

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.19.1-11m)
- rebuild against firefox-3

* Thu Apr 17 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.19.1-10m)
- rebuild against firefox-2.0.0.14

* Tue Apr  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19.1-9m)
- fix BuildRequires, Requires

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.19.1-8m)
- rebuild against gcc43

* Thu Mar 27 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.19.1-7m)
- rebuild against firefox-2.0.0.13

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19.1-6m)
- rebuild against firefox-2.0.0.12

* Sat Dec  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.19.1-5m)
- rebuild against firefox-2.0.0.11

* Wed Nov 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.19.1-4m)
- rebuild against firefox-2.0.0.10

* Fri Nov  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.19.1-3m)
- rebuild against firefox-2.0.0.9

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.19.1-2m)
- rebuild against firefox-2.0.0.8

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.1-1m)
- update to 2.19.1

* Wed Sep 19 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.3-6m)
- rebuild against firefox-2.0.0.7-1m

* Tue Aug  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.14.3-5m)
- add Requires for firefox

* Sat Jul  7 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-4m)
- Req Change gtkhtml2 -> libgtkhtml

* Sat Jul  7 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-3m)
- Req Change gtkhtml3 -> gtkhtml2
- add Provides gnome-python2-gtkhtml2

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.3-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Mon Feb 12 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.14.2-5m)
- nedd higher release of libgda

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-4m)
- rename gnome-python-extras -> gnome-python2-extras

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14.2-3m)
- rebuild against python-2.5

* Mon Nov 13 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.2-3m)
- copy from momonga's trunk (r11279)
- use firefox-devel instead of mozilla-devel

* Mon Sep 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.2-2m)
- rebuild against gdl-0.6.1-4m

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.0-4m)
- rebuild against expat-2.0.0-1m

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-3m)
- delete duplicated files

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- revise %%files (conflict)

* Sun Apr 16 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-1m)
- first import from FC5
- add gda package

* Mon Mar 13 2006 Ray Strode <rstrode@redhat.de> 2.14.0-1
- Update to 2.14.0

* Tue Feb 28 2006 Karsten Hopp <karsten@redhat.de> 2.13.3-4
- Buildrequires: python-devel

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.13.3-3.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.13.3-3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Feb  6 2006 John (J5) Palmieri <johnp@redhat.com> - 2.13.3-3
- Upload correct tar ball and try again

* Mon Feb  6 2006 John (J5) Palmieri <johnp@redhat.com> - 2.13.3-2
- Bump and rebuild (force-tag fails for this module)

* Mon Feb  6 2006 John (J5) Palmieri <johnp@redhat.com> - 2.13.3-1
- Update to 2.13.3
- Move the gnome-python2-applet gnome-python2-gnomeprint 
  gnome-python2-gtksourceview gnome-python2-libwnck 
  gnome-python2-libgtop2 gnome-python2-nautilus-cd-burner 
  gnome-python2-metacity and gnome-python2-totem subpackages 
  to gnome-python2-desktop because gnome-python-extras was split upstream

* Mon Jan 23 2006 Ray Strode <rstrode@redhat.com> - 2.12.1-10
- rebuild

* Thu Jan 05 2006 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-9
- Last rebuild didn't get the new libgtop

* Tue Dec 20 2005 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-8
- rebuild for new libgtop soname change

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com> - 2.12.1-7.1
- rebuilt

* Fri Dec 02 2005 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-7
- rebuild with new libnautilus-cd-burner

* Wed Nov 09 2005 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-6
- Don't delete the mozembed docs

* Wed Nov 09 2005 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-5
- Try this again

* Wed Nov 09 2005 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-4
- Remove ifarch directives around mozembed since it is now built on
  ppc64. Bump release again and retag

* Wed Nov 09 2005 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-3
- Use pyver directly and bump release because force-tag doesn't work

* Wed Nov 09 2005 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-2
- Module won't tag - bump release and try again

* Wed Nov 09 2005 John (J5) Palmieri <johnp@redhat.com> - 2.12.1-1
- Update to 2.12.1

* Tue Sep 27 2005 David Malcolm <dmalcolm@redhat.com> - 2.12.0-4
- remove conditionality of requirement on gnome-media-devel

* Tue Sep 27 2005 David Malcolm <dmalcolm@redhat.com> - 2.12.0-3
- consolidate s390 conditional part of build requirement on gnome-media-devel

* Tue Sep 27 2005 David Malcolm <dmalcolm@redhat.com> - 2.12.0-2
- fix sources

* Tue Sep 27 2005 David Malcolm <dmalcolm@redhat.com> - 2.12.0-1
- bump from 2.11.4 to 2.12.0
- rename wnck_window_demands_attention to wnck_window_needs_attention, to track
  change made in libwnck C API in 2.11.4, #169383
- added build requirement on gnome-media-devel, since this is needed to build
  mediaprofiles.so

* Fri Aug 19 2005 Jonathan Blandford <jrb@redhat.com> - 2.11.4-9
- add requires for gtksourceview, #162403

* Fri Aug 19 2005 Jeremy Katz <katzj@redhat.com> - 2.11.4-8
- totem subpackage shouldn't require mozilla
- build again on s390{,x}, but don't do the -nautilus-cdburner subpackage

* Wed Aug 17 2005 David Zeuthen <davidz@redhat.com> - 2.11.4-6
- Rebuilt

* Thu Aug 11 2005 Jeremy Katz <katzj@redhat.com> - 2.11.4-5
- add -totem subpackage
- nuke mozembed docs on ppc64

* Tue Aug  9 2005 Jeremy Katz <katzj@redhat.com> - 2.11.4-2
- and fix the build

* Tue Aug  9 2005 Jeremy Katz <katzj@redhat.com> - 2.11.4-1
- bump version and rebuild against current stack

* Mon Jul 11 2005  <jrb@redhat.com> - 2.11.2-1
- bump version and fix nautilus-cd-burner for s390

* Mon Mar 28 2005 John (J5) Palmieri <johnp@redhat.com> - 2.10.0-2.1
- Retag and rebuild

* Mon Mar 28 2005 John (J5) Palmieri <johnp@redhat.com> - 2.10.0-2
- Add patch to fix build error with gtkspell module

* Mon Mar 28 2005 John (J5) Palmieri <johnp@redhat.com> - 2.10.0-1
- Update to upstream 2.10.0

* Mon Feb  7 2005 Matthias Clasen <mclasen@redhat.com> - 2.9.3-1
- Initial build.

