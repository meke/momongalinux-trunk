%global momorel 5

Summary: Converts filenames from one encoding to another
Name: convmv
Version: 1.14
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://j3e.de/linux/convmv/
Source0: http://j3e.de/linux/convmv/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
Converts filenames from one encoding to another.

%prep
%setup -q

%build
make

%check
make test

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} PREFIX=%{_prefix} MANDIR=%{_mandir} install
gzip -d %{buildroot}%{_datadir}/man/man1/convmv.1.gz

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CREDITS Changes GPL2 TODO VERSION
%doc testsuite.tar
%{_bindir}/convmv
%{_mandir}/man1/convmv.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Feb 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14-1m)
- update to 1.14
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-2m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10-3m)
- %%NoSource -> NoSource
 
* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.10-2m)
- rebuild against perl-5.10.0-1m

* Wed Oct  4 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-1m)
- initial package
