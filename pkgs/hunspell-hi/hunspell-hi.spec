%global momorel 6

Name: hunspell-hi
Summary: Hindi hunspell dictionaries
Version: 20050726
Release: %{momorel}m%{?dist}
Source: http://hunspell.sourceforge.net/hi-demo.tar.gz
Group: Applications/Text
URL: http://hunspell.sourceforge.net
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Hindi hunspell dictionaries.
iconv -f ISO-8859-1 -t UTF-8 hi/Copyright > hi/Copyright.utf8
mv hi/Copyright.utf8 hi/Copyright

%prep
%setup -q -c -n hi-demo

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
mv hi/hi.dic hi/hi_IN.dic
mv hi/hi.aff hi/hi_IN.aff
cp -p hi/*.dic hi/*.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc hi/README hi/COPYING hi/Copyright
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20050726-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20050726-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20050726-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20050726-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20050726-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20050726-1m)
- import from Fedora to Momonga

* Sun Jan 06 2008 Parag <pnemade@redhat.com> - 20050726-2
- Added Copyright

* Thu Jan 03 2008 Parag <pnemade@redhat.com> - 20050726-1
- Initial Fedora release
