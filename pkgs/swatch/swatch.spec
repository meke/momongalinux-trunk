%global momorel 17

Summary: the Simple WATCHdog
Name: swatch
Version: 3.2.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://swatch.sourceforge.jp/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: swatch-3.2.3-manpage-fix.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: perl >= 5.8.2
BuildRequires: perl-Date-Calc
BuildRequires: perl-File-Tail
BuildRequires: perl-Time-Date
BuildRequires: momonga-rpmmacros >= 20050120-1m
Requires(post): chkconfig
Requires(preun): chkconfig

%description
Swatch was originally written to actively monitor messages as they are
written to a log file via the UNIX syslog utility.

%prep
%setup -n swatch-%{version} -q
%patch0 -p1 -b .fix
chmod -v 644 tools/*

%{?filter_from_requires: %filter_from_requires /^perl(Mail:Sendmail)$/d }
%{?filter_from_requires: %filter_from_requires /^perl(Sys:Hostname)$/d }
%{?perl_default_filter}

%build
perl Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make pure_install PERL_INSTALL_ROOT=%{buildroot}
find %{buildroot} -type f -name .packlist  -exec rm -f {} ';'
find %{buildroot} -type d -depth -exec rmdir {} 2>/dev/null ';'
chmod -R u+w $RPM_BUILD_ROOT/*

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES COPYRIGHT COPYING KNOWN_BUGS README examples/ tools/
%{_bindir}/swatch
%{_mandir}/man1/*.1*
%{_mandir}/man3/*.3pm*
%{perl_vendorlib}/Swatch/
%{perl_vendorlib}/auto/Swatch/

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-17m)
- rebuild against perl-5.20.0

* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.3-16m)
- remove daemonize

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-15m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-14m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-13m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.3-2m)
- full rebuild for mo7 release

* Thu Jul  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-10m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-9m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.2.1-7m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-5m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.1-3m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.2.1-2m)
- rebuild against perl-5.10.0-1m

* Sun May 20 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.2.1-1m)
- up to 3.2.1

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.1-5m)
- use vendor

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.1-4m)
- rebuild against perl-5.8.8

* Mon Jun 13 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.1-3m)
- rebuilt against perl-5.8.7

* Sun Mar 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-2m)
- add BuildPrereq: momonga-rpmmacros >= 20050120-1m
  for avoid require perl(Mail::Sendmail)

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.1-1m)
- revise URL

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.8-5m)
- build against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.0.8-4m)
- remove Epoch from BuildPrereq

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.8-3m)
- stop daemon

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (3.0.8-2m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.8-1m)
- rebuild against perl-5.8.2
- update to 3.0.8
- change Source0 URI

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.4-11m)
- rebuild against perl-5.8.0

* Wed Mar  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.0.4-10k)
- treat /etc/swatch.conf as %config

* Mon Dec 10 2001 Tsutomu Yasuda <tom@kondara.org>
- (3.0.4-8k)
  fix preun script

* Mon Dec 10 2001 Motonobu Ichimura <famao@kondara.org>
- (3.0.4-6k)
- added swatchd-daemon.patch

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (3.0.4-4k)
- comment out %chkconfig

* Thu Nov  8 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0.4-2k)
- change URI of Source0

* Sat Oct 27 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0.3-2k)
- BuildArch: noarch
- remove .packlist
- rename initscript to swatch

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Wed Sep 12 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0.1-10k)
- fix a problem in init file (Thanks to seki <m.seki@fcs.fujitsu.com>)

* Sat May  5 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (3.0.1-8k)
- add perl-Time-Date to Requires:

* Sun Apr 29 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.1-6k)
- add %preun section

* Mon Apr 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.1-4k)
- modified boot script...

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.1-2k)
- errased swatch.manfix.patch and modified spec file for compatibility

* Wed Nov 29 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- use perl_archlib & perl_sitearch macro

* Tue Nov 28 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d to %{_initscriptdir}.

* Fri Mar 11 2000 Motonobu Ichimura <famao@kondara.org>
- fix .packlist problem

* Fri Feb 4 2000 Kyoichi Ozaki <k@afromania.org>
- 1st release
