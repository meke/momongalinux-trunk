%global momorel 1
%global unstable 0
%global kdever 4.11.4
%global kdelibsrel 1m
%global kdebaserel 1m
%global kdesdkrel 1m
%global kdevplatformver 1.6.0
%global kdevplatformrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.6.0
%if 0%{?unstable}
%global sourcedir unstable/kdevelop/%{ftpdirver}/src
%else
%global sourcedir stable/kdevelop/%{ftpdirver}/src
%endif

%global kdevelopver 4.6.0

Summary: Integrated Development Environment for C++/C
Name: kdevelop
Version: %{kdevelopver}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
URL: http://www.kdevelop.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires: qt >= %{qtver}
Requires: kde-baseapps >= %{kdever}-%{kdebaserel}
Requires: autoconf >= 2.58
Requires: automake >= 1.5
Requires: flex >= 2.5.4
Requires: make
Requires: perl >= 5.8.1
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdebase >= %{kdever}-%{kdebaserel}
BuildRequires: kdesdk-devel >= %{kdever}-%{kdesdkrel}
BuildRequires: kdevplatform-devel >= %{kdevplatformver}-%{kdevplatformrel}
BuildRequires: apr-devel >= 1.2.6
BuildRequires: apr-util-devel >= 1.2.6
BuildRequires: autoconf >= 2.58
BuildRequires: automake
BuildRequires: db4-devel >= 4.5.20-3m
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libpng-devel >= 1.2.0
BuildRequires: libtool
BuildRequires: shared-mime-info
BuildRequires: subversion-devel >= 1.3.1
BuildRequires: valgrind-devel

%description
The KDevelop Integrated Development Environment provides many features
that developers need as well as providing a unified interface to programs
like gdb, the C/C++ compiler, and make. KDevelop manages or provides:

All development tools needed for C++ programming like Compiler,
Linker, automake and autoconf; KAppWizard, which generates complete,
ready-to-go sample applications; Classgenerator, for creating new
classes and integrating them into the current project; File management
for sources, headers, documentation etc. to be included in the
project; The creation of User-Handbooks written with SGML and the
automatic generation of HTML-output with the KDE look and feel;
Automatic HTML-based API-documentation for your project's classes with
cross-references to the used libraries; Internationalization support
for your application, allowing translators to easily add their target
language to a project;

KDevelop also includes WYSIWYG (What you see is what you get)-creation
of user interfaces with a built-in dialog editor; Debugging your
application by integrating KDbg; Editing of project-specific pixmaps
with KIconEdit; The inclusion of any other program you need for
development by adding it to the "Tools"-menu according to your
individual needs.

%package libs
Summary: %{name} runtime libraries
Group:   System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kdelibs >= %{kdever}

%description libs
%{summary}.

%package devel
Group:    Development/Libraries
Summary:  Header files for kdevelop
Requires: %{name} = %{version}-%{release}
Requires: kdelibs-devel

%description devel
Header files for developing applications using %{name}.

%prep
%setup -q -n %{name}-%{version}

%build
case "`gcc -dumpversion`" in
4.6.*)
	# kdevelop.spec-4.2.0 requires this
	CFLAGS="$RPM_OPT_FLAGS  -fpermissive"
	CXXFLAGS="$RPM_OPT_FLAGS  -fpermissive"
	;;
esac
export CFLAGS CXXFLAGS
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

%find_lang %{name} --all-name --with-kde

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor &> /dev/null
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :
fi

%posttrans
update-desktop-database -q &> /dev/null
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING* CHANGELOG* HACKING* INSTALL README TODO
%{_kde4_bindir}/*
%{_kde4_appsdir}/kdevappwizard/templates/*
%{_kde4_appsdir}/kdevcodegen/templates/*
%{_kde4_appsdir}/kdevcppsupport
%{_kde4_appsdir}/kdevcustommakemanager
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/kdevfiletemplates/templates
%{_kde4_appsdir}/kdevgdb
%{_kde4_appsdir}/kdevokteta
%{_kde4_appsdir}/plasma/plasmoids/kdevelopsessions
%{_kde4_appsdir}/plasma/services/org.kde.plasma.dataengine.kdevelopsessions.operations
%{_kde4_datadir}/config/*
%{_kde4_datadir}/applications/kde4/*
%{_kde4_datadir}/kde4/services/*
%{_kde4_datadir}/mime/packages/%{name}.xml
%{_kde4_iconsdir}/hicolor/*/*/*

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/lib*.so
%{_kde4_libdir}/kde4/*.so

%files devel
%defattr(-,root,root)
%{_kde4_includedir}/%{name}
%{_kde4_appsdir}/cmake/modules/*

%changelog
* Tue Dec 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to 4.6.0

* Sun Dec  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-2m)
- remove Requires: qt-devel and kdbg
- add Requires: qt

* Thu Oct 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to 4.5.2

* Thu May 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to 4.5.1

* Sat Apr 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to 4.5.0

* Fri Apr 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.90-1m)
- update to 4.4.90

* Sat Nov  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to 4.4.1

* Wed Oct 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to 4.4.0

* Sun Sep  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to 4.3.90

* Mon Aug  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to 4.3.80

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-2m)
- rebuild with KDE 4.9 RC2

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to 4.3.1

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to 4.3.0

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to 4.2.90

* Sat Jan 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.81-1m)
- update to 4.2.81

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-2m)
- rebuild against KDE 4.7.90


* Tue Jun 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to 4.2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.2-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2

* Tue Apr  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Sat Feb 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.0-2m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Mon Jan 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to 4.2.0

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90.
- KDevelop-4.1 does not work with KDE 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.1-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.1-1m)
- update to 4.1.1

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to 4.1.0

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.1-3m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-2m)
- change Requires from qt-designer to qt-devel

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-2m)
- rebuild against qt-4.6.3-1m

* Thu Apr 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to 4.0.0
- add kdevelop-libs subpackage

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.2-1m)
- update to 3.10.2

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.0-1m)
- update to 3.10.0

* Tue Mar  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.99-1m)
- update to 3.9.99

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.98-1m)
- update to 3.9.98

* Mon Dec 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.97-1m)
- update to 3.9.97

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.96-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.96-1m)
- update to 3.9.96

* Tue Sep  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.95-1m)
- update to 3.9.95

* Wed May 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.91-1m)
- update to 3.9.91 for KDE4

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-5m)
- fix build with new automake

- * Wed May 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (3.9.91-1m)
- - update to 3.9.91 for KDE4

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.4-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.4-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.4-2m)
- drop Patch1 for fuzz=0, already merged upstream
- License: GPLv2+

* Mon Dec 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.5.3-2m)
- rebuild against db4-4.7.25-1m

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Tue May 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.2-1m)
- update to 3.5.2

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-8m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.0-7m)
- rebuild against gcc43

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-6m)
- move headers to %%{_includedir}/kde

* Fri Feb 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-5m)
- specify KDE3 headers and libs
- modify Requires and buildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.0-4m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.5.0-3m)
- rebuild against perl-5.10.0-1m

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.5.0-2m)
- rebuild against db4-4.6.21

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-1m)
- update to KDE 3.5.8 (kdevelop-3.5.0)

* Mon May 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-3m)
- revise and clean up %%files

* Sun May 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-2m)
- import kdevelop-3.4.1-alt-svn-lib.patch from ALT Linux (re-enable subversion support)

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.1-1m)
- update to KDE 3.5.7 (kdevelop-3.4.1)
- import patch11 (kdevelop-3.4.1-stop-hang.patch) from Mandriva
- disable subversion support at this time
- - delete "--with-svn-include=%%{_includedir}/subversion-1"
- - delete "--with-svn-lib=%%{_libdir}"

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.6-2m)
- rebuild against kdelibs etc.

* Wed Jan 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.6-1m)
- update to KDE 3.5.6

* Sun Nov 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-3m)
- rebuild against db4-4.5.20-1m

* Sun Oct 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-2m)
- update admin of c_cpp_reference for new autotools
- remove c_cpp_reference-2.0.2-config.patch
- remove kdevelop-2.1.5_for_KDE_3.1-doc.patch

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.5-1m)
- update to KDE 3.5.5

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.4-2m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m kdesdk-3.5.4-4m

* Fri Aug 18 2006 Nishio Futoshi <futoshi@momonga-linix.org>
- (3.3.4-2m)
- rebuild against kdesdk-3.5.4-3m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-1m)
- update to KDE 3.5.4

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.3-1m)
- update to KDE 3.5.3

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-1m)
- update to KDE 3.5.2

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.1-1m)
- update to KDE 3.5.1

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.0-3m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.0-2m)
- rebuild against db4.3

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.0-1m)
- update to KDE 3.5

* Wed Nov 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (3.3.0-0.1.2m)
- delete "Workaround for legacy auto* tools"

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.0-0.1.1m)
- update to KDE 3.5 RC1
- remove fpic.patch
- modify spec file

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-1m)
- update to KDE 3.4.3
- remove kdevelop-3.2.2-uic.patch

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-3m)
- import kdevelop-3.2.2-uic.patch from Fedora Core devel
 +* Thu Sep 22 2005 Than Ngo <than@redhat.com> 9:3.2.2-2
 +- fix uic build problem

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-2m)
- clean up spec file

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-1m)
- update to KDE 3.4.2
- import gcc4.patch from Fedora Core
- add %%doc

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.1-1m)
- update to KDE 3.4.1

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-1m)
- update to KDE 3.4.0
- update fpic.patch from Fedora Core
- Requires: qt-designer

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.1.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.2-1m)
- update to KDE 3.3.2

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.1.0-2m)
- rebuild against kdelibs-3.3.1-3m (libstdc++-3.4.1)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.0-1m)
- KDE 3.3.1
- import Patch4 from Fedora Core

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.4-2m)
- rebuild against db4.2

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0.4-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0.3-1m)
- KDE 3.2.3

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0.2-1m)
- KDE 3.2.1 Release

* Thu Feb 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Wed Feb 18 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.0-1m)
- delete Obsoletes: kdevelop
- delete Provides: kdevelop

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0.0-1m)
- KDevelop 3.0.0 (aka Gideon) with KDE 3.2.0
- change URL and name of Source1
- import following patches from Fedora
- - Patch1: c_cpp_reference-2.0.2-config.patch
- - Patch3: kdevelop-2.1.5_for_KDE_3.1-doc.patch
- remove Patch100: kdevelop-2.0.2-font-i18n-20011127.diff (not appliable)
- make -f Makefile.cvs in %%build

* Sat Jan 10 2004 kourin <kourin@fh.freeserve.ne.jp>
- (2.1.5-5m)
- rebuild against for qt-3.2.3

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (2.1.5-4m)
- rebuild against for qt-3.2.3

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.5-3m)
- for kde-3.1.2

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-2m)
- remove requires: qt-Xt

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.5-1m)
- update to 2.1.5
- update c_cpp_reference to 2.0.2

* Sat Mar 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-2m)
- rebuild against for XFree86-4.3.0

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (2.1.4-1m)
- ver up.

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (2.1.3-2m)
- rebuild against kde-3.0.4.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (2.1.3-1m)
- ver up.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (2.1.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (2.1.2-1m)
- ver up.

* Sun Jun  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (2.1.1-2k)
- ver up.

* Fri Apr  5 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.1-2k)
  update to 2.1 release

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (2.1-0.0003002k)
- based on 2.1rc3.

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.2-8k)
- perl 5.6.1

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (2.0.2-6k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.2-4k)
- automake autoconf 1.5

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.0.2-2k)
- version up.

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.0.1-6k)
- revised spec file.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (2.0.1-4k)
- rebuild against libpng 1.2.0.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.0.1-2k)
- version up.

* Thu Aug 23 2001 Toru Hoshina <toru@df-usa.com>
- (2.0-2k)
- stable release.
- fix version no ;-p

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1, but...

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002004k)
- based on 2.2alpha2.

* Sat May 19 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001004k)
- avoid conflicting /usr/bin/extractrc.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (2.1-0.0001010k)
- rebuild against openssl 0.9.6.

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.1-0.0001006k)
- rebuild against QT-2.3.0.

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.1-0.0001004k]
- backport 2.1-0.0001005k(Jirai).

* Wed Jan 10 2001 Kenichi Matsubara <m@kondara.org>
- [2.1-0.0001002k]
- update 2.1beta1.

* Sat Jan 06 2001 Kenichi Matsubara <m@kondara.org>
- [2.1-0.0001003k]
- update 2.1beta1.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-0.020001214003k]
- update new snapshot version.
- use build qt-2.2.3.

* Sat Dec 09 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-0.020001209003k]
- update to new snapshot version.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-0.4k]
- rebuild against new environment glibc-2.2 egcs++

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-0.3k]
- rebuild against qt 2.2.2.

* Fri Oct 27 2000 Toru Hoshina <toru@df-usa.com>
- snapshot release.

* Sun Aug 13 2000 Than Ngo <than@redhat.com>
- fix kdelibsdoc-dir to show kdelibs-1.1.2 html docu correct

* Tue Aug 01 2000 Than Ngo <than@redhat.de>
- add missing ldconfig in %post and %postun section (Bug #14924)
- add missing C references stuff to kdevelop

* Sun Jul 30 2000 Than Ngo <than@redhat.de>
- rebuilt against compat-egcs-c++, put KDE1 under /usr
- cleanup specfile

* Tue Jul 25 2000 Than Ngo <than@redhat.de>
- fix dependency problem

* Wed Jul 19 2000 Than Ngo <than@redhat.de>
- rebuilt against compat-libstdc++

* Mon Jul 17 2000 Than Ngo <than@redhat.de>
- install under /usr/share instead /usr/lib/kde1-compat/share,
  fix dependency problem

* Sat Jul 15 2000 Than Ngo <than@redhat.de>
- rebuilt with egcs-c++-1.1.2

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul 03 2000 Than Ngo <than@redhat.de>
- fix docdir
- add requires kdebase

* Sun Jul 02 2000 Than Ngo <than@redhat.de>
- rebuilt with  kde1-compat

* Sun Jun 18 2000 Than Ngo <than@redhat.de>
- rebuilt in the new build environment, fix docdir
- FHS packaging

* Thu Jun 08 2000 Than Ngo <than@redhat.de>
- update to 1.2
- move from powertools to main CD
- use %%configure

* Mon Apr 3 2000 Ngo Than <than@redhat.de>
- fix up reference (Bug #10368)

* Tue Feb 15 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up documentation path (Bug #7291)
- Update to current stable branch - this should fix up the debugger problem
- clean up spec file

* Fri Jan  6 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Update to current stable branch
- Adapt to 6.2 Qt libraries
- Use BuildPrereq

* Wed Jan 05 2000 Ngo Than <than@redhat.de>
- added patch for alpha

* Tue Dec 21 1999 Ngo Than <than@redhat.de>
- updated kdevelop-1.0 release

* Tue Nov 16 1999 Preston Brown <pbrown@redhat.com>
- kdevelop 1.0beta4.1, docdir added, using DESTDIR env. variable.

* Thu Sep 09 1999 Preston Brown <pbrown@redhat.com>
- initial packaging for 6.1.
