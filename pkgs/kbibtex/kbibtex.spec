%global momorel 2
%global qtver 4.8.5
%global kdever 4.12.0
%global kdelibsrel 1m

Summary: A BibTeX editor for KDE
Name: kbibtex
Version: 0.5
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Editors
URL: http://home.gna.org/kbibtex/
Source0: http://download.gna.org/%{name}/0.5/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-fix-build.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: libxslt-devel
BuildRequires: libutempter-devel
BuildRequires: libxml2
BuildRequires: poppler-qt4-devel >= 0.20.1

%description
KBibTeX is a BibTeX editor for KDE.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{summary}.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .fix-build

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc LICENSE README
%{_kde4_bindir}/%{name}
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/%{name}part
%{_kde4_libdir}/*.so.*
%{_kde4_libdir}/kde4/*.so
%{_kde4_datadir}/applications/kde4/*.desktop
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/config/kbibtexrc
%{_kde4_iconsdir}/hicolor/*/apps/*.png
%{_kde4_datadir}/pixmaps/*.png
%{_kde4_datadir}/mime/packages/*.xml
%{_mandir}/man1/*

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/*.so

%changelog
* Sat Jan 18 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-2m)
- fix build

* Sat Jan 18 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Mon Mar 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Mon Oct  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-0.2.2m)
- source tarball is replaced, patch1 is no longer needed

* Sun Sep 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-0.2.1m)
- update to 0.4.1-rc2

* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-0.1.2m)
- rebuild against poppler-0.20.1

* Sun May  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-0.1.1m)
- update to 0.4.1-rc1

* Tue Feb  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-4m)
- fix bug#19202 (https://gna.org/bugs/?19202)

* Sat Dec 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-3m)
- drop unneeded patch

* Sun Dec 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-2m)
- fix build error

* Fri Nov 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-1m)
- update to 0.4

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- update to 0.3

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-0.1.1m)
- update to 0.3beta2
- support KDE4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.3-2m)
- full rebuild for mo7 release

* Wed May 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.3-1m)
- version 0.2.3

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-4m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-2m)
- switch to new tar-ball

* Sun Jun 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-1m)
- version 0.2.2

* Tue Feb 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-0.2.1m)
- update to 0.2.1.91 (0.2.2 beta2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-4m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.1-2m)
- rebuild against gcc43

* Mon Mar 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-1m)
- version 0.2.1
- update dekstop.patch

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-2m)
- %%NoSource -> NoSource

* Wed Nov 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-1m)
- version 0.2
- clean up merged upstream patches
- update desktop.patch
- License: GPLv2

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.5-1m)
- initial package for Momonga Linux
