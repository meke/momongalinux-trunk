%global momorel 2

Summary: Interfaces for accessibility support
Name: libgail-gnome
Version: 1.20.4
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.20/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: atk-devel >= 1.24.0
BuildRequires: gtk2-devel >= 2.14.3
BuildRequires: libbonobo-devel >= 2.24.0
BuildRequires: libgnomeui-devel >= 2.24.0
BuildRequires: libbonoboui-devel >= 2.24.0
BuildRequires: gnome-panel-devel >= 2.24.0
BuildRequires: at-spi-devel >= 1.24.0
BuildRequires: pkgconfig

%description
The gail library provides a set of interfaces for adding accessibility
support to applications and graphical user interface toolkits.
By supporting the gail interfaces, an application or toolkit can
be used such as tools such as screen readers and magnifiers,
and alternative input devices.

%package devel
Summary:  System for layout and rendering of internationalized text
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: at-spi-devel
Requires: gtk2-devel
Requires: libbonoboui-devel
Requires: atk-devel
Requires: libgnomeui-devel
Requires: libbonobo-devel
Requires: pkgconfig

%description devel
The libgail-gnome-devel package includes the static libraries
for the libgail-gnome package.

Install gailgail-gnome-devel if you want to develop programs which will use
gail.

%prep
%setup -q

%build
%configure --enable-doc --disable-schemas-install
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}


%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/libgail-gnome.schemas \
  > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/libgail-gnome.schemas \
    > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/libgail-gnome.schemas \
    > /dev/null || :
fi

%files
%defattr(-, root, root)
%doc README AUTHORS COPYING ChangeLog NEWS
%{_sysconfdir}/gconf/schemas/libgail-gnome.schemas
%{_libdir}/gtk-2.0/modules/libgail-gnome.so
%{_libdir}/gtk-2.0/modules/libgail-gnome.la

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/libgail-gnome.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20.4-2m)
- rebuild for new GCC 4.6

* Thu Feb 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.4-1m)
- update to 1.20.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.20.3-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.3-1m)
- update to 1.20.3

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.2-1m)
- update to 1.20.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20.1-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (1.20.1-3m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20.1-2m)
- rebuild against rpm-4.6

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.1-1m)
- update to 1.20.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20.0-2m)
- rebuild against gcc43

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.0-1m)
- update to 1.20.0

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.0-1m)
- update to 1.18.0

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-2m)
- delete libtool library

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2
- GNOME 2.12.2 Desktop

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.0-1m)
- version 1.1.0
- GNOME 2.8 Desktop

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.2-3m)
- rebuild against for XFree86-4.3.0

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.2-2m)
  rebuild against openssl 0.9.7a

* Thu Nov 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.2-1m)
- version 1.0.2

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- version 1.0.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.0-18m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.0-17m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.0-16m)
- rebuild against for gnome-games-2.0.1.1
- rebuild against for gail-0.17
- rebuild against for pygtk-1.99.12
- rebuild against for librsvg-2.0.1
- rebuild against for eel-2.0.1
- rebuild against for nautilus-2.0.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.0-15m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-14k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-12k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-10k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-8k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-6k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-4k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-2k)
- version 1.0.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-40k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-38k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-36k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-34k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-32k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-30k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-28k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-26k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-24k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-22k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-20k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-18k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-16k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-14k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Thu Apr 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-12k)
- rebuild against for at-spi-0.12.1
- rebuild against for eog-0.116.1

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-10k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-8k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-2k)
- version 0.5.0

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-38k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-36k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-34k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-32k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-30k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-28k)
- rebuild against for gail-0.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-26k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-24k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-22k)
- change depends line

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-20k)
- rebuild against for yelp-0.3
- rebuild against for libgnomeui-1.112.1
- rebuild against for nautilus-1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-18k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-16k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-14k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-12k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-10k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-8k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-6k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-4k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-2k)
- created
