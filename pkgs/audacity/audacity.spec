%global          momorel 1

Summary:        Audacity is a free audio editor
Name:           audacity
Version:        2.0.5
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/Multimedia
URL:            http://audacity.sourceforge.net/
Source0:        http://audacity.googlecode.com/files/%{name}-minsrc-%{version}.tar.xz
NoSource:       0
Patch0:         %{name}-1.3.8-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       flac
Requires:       jack
Requires:       libmad
Requires:       libsndfile
Requires:       libvorbis
Requires:       wxGTK-unicode >= 2.8.6
BuildRequires:  ImageMagick
BuildRequires:  flac-devel >= 1.1.4
BuildRequires:  ffmpeg-devel >= 2.2.2-1m 
BuildRequires:  gcc-c++
BuildRequires:  jack-devel
BuildRequires:  ladspa-devel
BuildRequires:  libid3tag-devel
BuildRequires:  libmad
BuildRequires:  libsamplerate-devel
BuildRequires:  libsndfile-devel
BuildRequires:  libtool
BuildRequires:  libvorbis-devel
BuildRequires:  wxGTK-unicode-devel >= 2.8.6
BuildRequires:  zip

%description
Audacity is a free audio editor. You can record sounds, play sounds, import
and export WAV, AIFF, and MP3 files, and more. Use it to edit your sounds
using Cut, Copy and Paste (with unlimited Undo), mix tracks together, or apply
effects to your recordings. It also has a built-in amplitude envelope editor,
a customizable spectrogram mode and a frequency analysis window for audio
analysis applications. Built-in effects include Bass Boost, Wahwah, and Noise
Removal, and it also supports VST plug-in effects.

%prep
%setup -q -n %{name}-src-%{version}

%patch0 -p1 -b .desktop~

# patch3 requires this
#pushd lib-src/portmixer
#autoreconf
#popd

%build
# for enable unicode
export WX_CONFIG=%{_bindir}/wx-config-unicode
export CPPFLAGS=-DGLIB_COMPILATION
%configure \
        --enable-unicode \
	--with-expat=system \
	--with-ffmpeg=local \
        --with-help \
        --with-libflac \
        --with-libid3tag \
        --with-libmad \
        --with-libsamplerate \
        --with-libsndfile=system \
        --with-vorbis \
        CFLAGS=-DGLIB_COMPILATION
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

# link icon
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# remove docdir/audacity
rm -rf  %{buildroot}%{_docdir}/%{name}

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,755)
%doc LICENSE.txt README.txt
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/icons/*/*/*/%{name}.svg
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/pixmaps/%{name}*.xpm
%{_datadir}/pixmaps/gnome-mime-application-x-%{name}-project.xpm
%{_datadir}/mime/packages/*.xml
%{_mandir}/man1/%{name}.1*

%changelog
* Fri May 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5

* Mon Sep 09 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-1m)
- version 2.0.4

* Sun Jan  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- version 2.0.2

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.0-1m)
- update 2.0.0

* Mon May 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.13-2m)
- fix build error

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.13-1m)
- rebuild against ffmpeg-0.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-6m)
- rebuild for new GCC 4.5

* Wed Oct 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-5m)
- import gcc45 patch from debian

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.12-4m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.12-3m)
- build fixes for newer ffmpeg
-- http://forum.audacityteam.org/viewtopic.php?f=19&t=33088

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.12-2m)
- use BuildRequires

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.12-1m)
- version 1.3.12

* Wed Jan 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.11-1m)
- version 1.3.11

* Sun Dec 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.10-1m)
- version 1.3.10
- remove glib220.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-2m)
- rebuild against glib-2.22.0

* Fri Sep  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.9-1m)
- version 1.3.9

* Wed Aug  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.8-1m)
- version 1.3.8
- update desktop.patch

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.7-2m)
- support newer ffmpeg

* Tue Feb  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.7-1m)
- version 1.3.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-3m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.6-2m)
- drop Patch1 for fuzz=0, already merged upstream
- License: GPLv2+

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.6-1m)
- version 1.3.6

* Sun Aug 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-3m)
- switch internal portaudio from v18 to v19

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-2m)
- change BR from libid3tag to libid3tag-devel

* Sun May 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-1m)
- version 1.3.5
- update desktop.patch
- remove merged not_require_lame-libs-devel.patch
- remove merged opt.patch
- remove merged CVE-2007-6061.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-4m)
- rebuild against gcc43

* Tue Jan 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-3m)
- [SECURITY] CVE-2007-6061
- import CVE-2007-6061.patch from gentoo
 +- 26 Jan 2008; Alexis Ballier <aballier@gentoo.org>
 +- +files/CVE-2007-6061.patch, +audacity-1.3.4-r1.ebuild:
 +- Add a patch for temporary file vulnerablilty (CVE-2007-6061), bug #199751.
 +- It will set the default temporary file location to the user home directory
 +- add discard preferences if it is in /tmp.

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-1m)
- version 1.3.4
- update desktop.patch
- import 2 patches from cooker
 - audacity-not_require_lame-libs-devel.patch
  ++ Oden Eriksson <oeriksson@mandriva.com>
  +- added P3,P4 from debian to make it find the right lame lib
 - audacity-opt.patch
  +- added P6 from debian to not override CFLAGS
- remove merged flac.patch
- remove locale_installpath.patch
- set --enable-unicode and use wxGTK-unicode
- set --with-portmixer=no to enable build on trunk Revision: 19662

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-2m)
- rebuild against libvorbis-1.2.0-1m

* Sat May 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3-1m)
- version 1.3.3
- fix and modify audacity.desktop
- update flac.patch (imported from cooker)
- import smart icon from cooker

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.2-3m)
- rebuild against flac-1.1.4
- import audacity-src-1.3.2-beta-flac.patch from cooker
 +* Tue Dec 12 2006 Gz Waschk <waschk@mandriva.org> 1.3.2-4mdv2007.0
 ++ Revision: 95230
 +- patch for new libflac

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.3.2-2m)
- enable ppc64

* Fri Nov 10 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Wed May 03 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.0b-2m)
- fix configure to use system libsamplerate
- change release expression since 1.3.0b does not mean 1.3.0-beta although this is development version

* Sat Mar 25 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.0b-0.1.1m)
- update to 1.3.0b-beta
- build with wxGTK-2.6
- remove old patches
- - audacity-1.2.4b-adhoc_prefstrings.patch: fixed with wxGTK-2.6
- - audacity-1.2.3-gcc41.patch: fixed in upstream
- add new patches
- - audacity-1.3.0b-soundtouch-gcc41.patch(Patch0): build fix for gcc41
- - audacity-1.3.0b-locale_installpath.patch(Patch1): fix stupid INSTALL_PATH in Makefile in locale dir

* Sat Dec 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4b-1m)
- update to 1.2.4b
- update adhoc_prefstrings.patch
- include all message catalog files (preference dialog crash problem was fixed)

* Sat Dec 17 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-3m)
- support gcc-4.1
-- add audacity-1.2.3-gcc41.patch

* Sun Nov  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-2m)
- add new audacity.desktop

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-1m)
- update to 1.2.3
- BuildPreReq: ladspa-devel

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.2-3m)
- revised docdir permission

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-2m)
- rebuild against flac 1.1.1
- add category AudioVideo to desktop file for KDE 

* Fri Nov 05 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2
- remove some message catalog files which cause a crash when one open a preference dialog (http://audacity.fuchsi.de/forum.php?req=thread&id=51)
- add Patch0 since some strings of a preferance dialog are not shown in the case launguage setting is "Nihongo". This may be bug of wxNotebook::AddPage, but I simply remove some translations from ja.po of audacity.

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.0-0.2.2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sun Dec 14 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.0-0.2.1m)
- update to version 1.2.0-pre2 (1.2.0-pre3 doesn't work correctly now...)
- rebuild against wxGTK-2.4.2

* Thu Feb 13 2003 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.0.0-2m)
- BuildPrereq: wxGTK-devel.

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.0-1m)
- imported
