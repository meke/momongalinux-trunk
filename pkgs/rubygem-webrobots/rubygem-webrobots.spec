# Generated from webrobots-0.0.13.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname webrobots

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: A Ruby library to help write robots.txt compliant web robots
Name: rubygem-%{gemname}
Version: 0.0.13
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: BSD
URL: https://github.com/knu/webrobots
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
This library helps write robots.txt compliant web robots in Ruby.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE.txt
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.13-1m)
- update 0.0.13

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.12-1m)
- Initial commit Momonga Linux

