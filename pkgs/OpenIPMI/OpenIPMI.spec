%global momorel 3

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: IPMI (Intelligent Platform Management Interface) library and tools
Name: OpenIPMI
Version: 2.0.19
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+ and Modified BSD and BSD
Group: System Environment/Base
URL: http://sourceforge.net/projects/openipmi/
Source0: http://downloads.sourceforge.net/openipmi/%{name}-%{version}.tar.gz
Source1: openipmi.sysconf
Source2: openipmi-helper
Source3: ipmi.service
Source4: openipmi.modalias
NoSource: 0
Patch1: OpenIPMI-2.0.18-pthread-pkgconfig.patch
Patch2: OpenIPMI-2.0.19-man.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gdbm-devel swig glib-devel ncurses-devel
BuildRequires: net-snmp-devel >= 5.7.1
BuildRequires: openssl-devel >= 1.0.0 python-devel >= 2.7 perl-devel >= 5.14.0 tcl-devel
BuildRequires: tkinter >= 2.5.1-9m
BuildRequires: desktop-file-utils
BuildRequires: systemd-units
Requires(post): chkconfig
Requires(preun): chkconfig

Obsoletes: OpenIPMI-gui

%description
The Open IPMI project aims to develop an open code base to allow access to
platform information using Intelligent Platform Management Interface (IPMI).
This package contains the tools of the OpenIPMI project.

%package modalias
Group: System Environment/Kernel
Summary: Module aliases for IPMI subsystem

%description modalias
The OpenIPMI-modalias provides configuration file with module aliases
of ACPI and PNP wildcards.

%package libs
Group: Development/Libraries
Summary: The OpenIPMI runtime libraries

%description libs
The OpenIPMI-libs package contains the runtime libraries for shared binaries
and applications.

%package perl
Group: Development/Libraries
Summary: IPMI Perl language bindings
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description perl
The OpenIPMI-perl package contains the Perl language bindings for OpenIPMI.

%package python
Group: Development/Libraries
Summary: IPMI Python language bindings

%description python
The OpenIPMI-python package contains the Python language bindings for OpenIPMI.

%package devel
Group: Development/Libraries
Summary: The development environment for the OpenIPMI project
Requires: pkgconfig
Requires: %{name} = %{version}-%{release}

%description devel
The OpenIPMI-devel package contains the development libraries and header files
of the OpenIPMI project.

%prep
%setup -q
%patch1 -p1 -b .pthread
%patch2 -p1 -b .manscan

%build

export CFLAGS="-fPIC $RPM_OPT_FLAGS -fno-strict-aliasing"

%configure \
    --with-pythoninstall=%{python_sitearch} \
    --disable-dependency-tracking \
    --with-tcl=no \
    --disable-static \
    --with-tkinter=no

# https://fedoraproject.org/wiki/Packaging:Guidelines?rd=Packaging/Guidelines#Beware_of_Rpath
# get rid of rpath still present in OpenIPMI-perl package
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make   # not %{?_smp_mflags} safe

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -rf %{buildroot}/%{_libdir}/*.la

# Remove python cruft in 32bit libdir on 64bit archs...
%ifarch ppc64 s390x x86_64
rm -rf %{buildroot}/usr/lib
%endif

install -d %{buildroot}%{_sysconfdir}/sysconfig
install -m 644 %SOURCE1 %{buildroot}%{_sysconfdir}/sysconfig/ipmi
install -d %{buildroot}%{_libexecdir}
install -m 755 %SOURCE2 %{buildroot}%{_libexecdir}/openipmi-helper
install -d %{buildroot}%{_unitdir}
install -m 644 %SOURCE3 %{buildroot}%{_unitdir}/ipmi.service
install -d %{buildroot}%{_sysconfdir}/modprobe.d
install -m 644 %SOURCE4 %{buildroot}%{_sysconfdir}/modprobe.d/OpenIPMI.conf

rm %{buildroot}/%{_mandir}/man1/openipmigui.1

%post
%systemd_post ipmi.service

%preun
%systemd_preun ipmi.service

%postun
%systemd_postun_with_restart ipmi.service

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

### A sysv => systemd migration contains all of the same scriptlets as a
### systemd package.  These are additional scriptlets

%triggerun -- OpenIPMI < 2.0.19
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply httpd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save ipmi >/dev/null 2>&1 ||:
/bin/systemctl --no-reload enable ipmi.service >/dev/null 2>&1 ||:
# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del ipmi >/dev/null 2>&1 || :
/bin/systemctl try-restart ipmi.service >/dev/null 2>&1 || :

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/sysconfig/ipmi
%{_libexecdir}/openipmi-helper
%{_unitdir}/ipmi.service
%{_bindir}/ipmicmd
%{_bindir}/ipmilan
%{_bindir}/ipmish
%{_bindir}/ipmi_ui
%{_bindir}/openipmicmd
%{_bindir}/openipmish
%{_bindir}/rmcp_ping
%{_bindir}/solterm
%{_mandir}/man1/ipmi_ui*
%{_mandir}/man1/openipmicmd*
%{_mandir}/man1/openipmish*
%{_mandir}/man1/rmcp_ping*
%{_mandir}/man1/solterm*
%{_mandir}/man7/ipmi_cmdlang*
%{_mandir}/man7/openipmi_conparms*
%{_mandir}/man8/ipmilan*

%files perl
%defattr(-,root,root)
%attr(644,root,root) %{perl_vendorarch}/OpenIPMI.pm
%{perl_vendorarch}/auto/OpenIPMI/

%files python
%defattr(-,root,root)
%{python_sitearch}/*OpenIPMI*

%files libs
%defattr(-,root,root)
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/OpenIPMI
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files modalias
%config(noreplace) %{_sysconfdir}/modprobe.d/OpenIPMI.conf

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.19-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.19-2m)
- rebuild against perl-5.18.2

* Wed Oct 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.19-1m)
- update 2.0.19
- support systemd

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-18m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-17m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-16m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-15m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-14m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-13m)
- rebuild against perl-5.16.0

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.18-12m)
- rebuild against net-snmp-5.7.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-11m)
- rebuild against perl-5.14.2

* Sun Jul 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.18-10m)
- rebuild against net-snmp-5.7

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-9m)
- rebuild against perl-5.14.1

* Tue May 10 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.18-8m)
- rebuild against perl-5.14.0

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.18-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.18-6m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-5m)
- rebuild against net-snmp-5.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.18-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.18-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-1m)
- update to 2.0.18
- import patch0 from Fedora devel

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.16-8m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.16-7m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-6m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-5m)
- rebuild against net-snmp-5.5

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.16-2m)
- rebuild against perl-5.10.1

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-1m)
- sync with Fedora 11 (2.0.16-1)
- %%define __libtoolize :

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.14-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.14-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.14-3m)
- rebuild against python-2.6.1-2m

* Sun Jun 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.14-2m)
- remove man files from devel package (to avoid conflicting with main package)

* Sun Jun 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.14-1m)
- sync Fedora
- version up 2.0.14

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.11-9m)
- rebuild against openssl-0.9.8h-1m

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.11-8m)
- rebuild against tkinter-2.5.1-9m

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.11-7m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.11-6m)
- rebuild against gcc43

* Sun Feb  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.11-5m)
- rebuild against perl-5.10.0

* Thu Jan  3 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.11-4m)
- added BuildPrereq: tkinter

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.11-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.11-2m)
- rebuild against net-snmp

* Thu Mar 22 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.11-1m)
- up to 2.0.11
- prdownloads.sorceforge.net -> dl.sourceforge.net

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.10-2m)
- revise %%files to avoid conflicting

* Mon Jan 22 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.10-1m)
- first build in Momonga Linux
- spec based on 2.0.6-7 from FC7 
