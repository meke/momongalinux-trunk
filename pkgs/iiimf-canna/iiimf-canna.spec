%global momorel 21

Summary:     Canna Language Engine for IIIMF
Name:	     iiimf-canna
Version:     0.3.0
Release:     %{momorel}m%{?dist}
License:     MIT/X
Group:	     User Interface/X
Source:	     http://dl.sourceforge.jp/iiimf-canna/5749/iiimf-canna-%{version}.tar.gz
Nosource:    0
Patch0:      iiimf-canna-0.3.0-linking.patch
Requires:    glib2, gtk2
Requires:    iiimf
Requires:    iiimf_conv
Requires:    iiimf-xiiimp
BuildRequires: glib2-devel,gtk2-devel
BuildRequires: Canna-devel
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
IIIMF-Canna is a Language Engine Module for IIIMF(Internet/Intranet Input Method Framework).
This module provides Canna input method. 

%prep
%setup -q
%patch0 -p1 -b .linking

%build
%configure --enable-gtk2 LIBS="-lX11"
make

%install
rm -rf %{buildroot}
%makeinstall xauxdir=%{buildroot}%{_libdir}/im/locale/ja/canna/xaux \
             leifdir=%{buildroot}%{_libdir}/im/leif \
             plugindir=%{buildroot}%{_libdir}/%{name}/plugins

# Momonga Linux Specific
mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/xim.d
cat <<EOF >%{buildroot}%{_sysconfdir}/X11/xinit/xim.d/canna
NAME="Canna/IIIMP"
IM_EXEC="env LC_ALL=ja_JP.eucJP /usr/lib/im/httx -if canna -lc_basic_locale ja_JP.eucJP -xim htt_xbe"
XMODIFIERS=@im=htt
HTT_DISABLE_STATUS_WINDOW=t
HTT_GENERATES_KANAKEY=t
HTT_USES_LINUX_XKEYSYM=t
GTK_IM_MODULE=xim
QT_IM_MODULE=xim
export XMODIFIERS HTT_DISABLE_STATUS_WINDOW HTT_GENERATES_KANAKEY \
HTT_USES_LINUX_XKEYSYM GTK_IM_MODULE QT_IM_MODULE

export IM_EXEC
EOF

%find_lang %{name}

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-21m)
- fix requires:

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-20m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-19m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-18m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-17m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-16m)
- fix build

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-15m)
- explicitly link libdl

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-14m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.3.0-12m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-10m)
- rebuild against gcc43

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-9m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.3.0-8m)
- rebuild against expat-2.0.0-1m

* Thu Feb 24 2005 Toru Hoshina t@momonga-linux.org>
- (0.3.0-7m)
- auto* fix.

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-6m)
- change xim.d/canna (add QT_IM_MODULE)

* Fri Nov  5 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.3.0-5m)
- change xim.d/canna (add GTK_IM_MODULE)

* Thu Dec 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-4m)
- add Patch0: iiimf-canna-0.3.0-po-Makefile.patch for compile

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.3.0-3m)
- change License

* Tue Sep 02 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-2m)
- add Requires iiimf, iiimf_conv, and iiimf-xiiimp same as iiimf-skk

* Sat Aug 30 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.3.0-1m)
- version 0.3.0

* Sat Apr 13 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.20.9-3m)
- add BuildRequires: Canna-devel for use -lcanna

* Thu Sep 26 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.20.9-2m)
- not CannaLE.

* Wed Sep 25 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.20.9-1m)
- version 0.1.20.9

* Wed Sep 25 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.20.8-1m)
- version 0.1.20.8

* Mon Sep 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.20.7-1m)
- first release for public

%files -f %{name}.lang
%defattr(-,root,root)
%{_sysconfdir}/X11/xinit/xim.d/canna
%{_libdir}/im/leif/canna*
%{_libdir}/im/locale/ja/canna
