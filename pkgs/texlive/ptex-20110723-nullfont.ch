@x
function get_jfm_pos(@!kcode:KANJI_code;@!f:internal_font_number):eight_bits;
var @!jc:KANJI_code; {temporary register for KANJI}
@!sp,@!mp,@!ep:pointer;
begin@/
@y
function get_jfm_pos(@!kcode:KANJI_code;@!f:internal_font_number):eight_bits;
var @!jc:KANJI_code; {temporary register for KANJI}
@!sp,@!mp,@!ep:pointer;
begin@/
if f=null_font then begin
  get_jfm_pos:=kchar_type(null_font)(0); return; end;
@z

@x
  fast_get_avail(main_p); font(main_p):=main_f; character(main_p):=cur_l;
  link(tail):=main_p; tail:=main_p; last_jchr:=tail;
  fast_get_avail(main_p); info(main_p):=KANJI(cur_chr);
  link(tail):=main_p; tail:=main_p;
  cx:=cur_chr; @<Insert kinsoku penalty@>;
@y
  if main_f<>null_font then begin
  fast_get_avail(main_p); font(main_p):=main_f; character(main_p):=cur_l;
  link(tail):=main_p; tail:=main_p; last_jchr:=tail;
  fast_get_avail(main_p); info(main_p):=KANJI(cur_chr);
  link(tail):=main_p; tail:=main_p;
  cx:=cur_chr; @<Insert kinsoku penalty@>;
  end;
@z

@x
  @<Look ahead for glue or kerning@>;
@y
  if main_f<>null_font then 
  begin @<Look ahead for glue or kerning@>; end
  else inhibit_glue_flag:=false;
@z

