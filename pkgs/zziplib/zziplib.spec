%global         momorel 1

Summary:	Lightweight library to easily extract data from zip files
Name:		zziplib
Version:	0.13.62
Release:	%{momorel}m%{?dist}
License:	LGPLv2+ or MPLv1.1
Group:		Applications/Archiving
URL:		http://zziplib.sourceforge.net/
Source:		http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	zlib-devel
BuildRequires:	zip
BuildRequires:	python
BuildRequires:	SDL-devel
BuildRequires:	xmlto
BuildRequires:	pkgconfig
BuildRequires:	autoconf
BuildRequires:	automake

%description
The zziplib library is intentionally lightweight, it offers the ability to
easily extract data from files archived in a single zip file. Applications
can bundle files into a single zip archive and access them. The implementation
is based only on the (free) subset of compression with the zlib algorithm
which is actually used by the zip/unzip tools.


%package utils
Summary: Utilities for the zziplib library
Group: Applications/Archiving
Requires: %{name} = %{version}-%{release}

%description utils
The zziplib library is intentionally lightweight, it offers the ability to
easily extract data from files archived in a single zip file. Applications
can bundle files into a single zip archive and access them. The implementation
is based only on the (free) subset of compression with the zlib algorithm
which is actually used by the zip/unzip tools.

This packages contains all the utilities that come with the zziplib library.


%package devel
Summary: Development files for the zziplib library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}, pkgconfig, zlib-devel, SDL-devel

%description devel
The zziplib library is intentionally lightweight, it offers the ability to
easily extract data from files archived in a single zip file. Applications
can bundle files into a single zip archive and access them. The implementation
is based only on the (free) subset of compression with the zlib algorithm
which is actually used by the zip/unzip tools.

This package contains files required to build applications that will use the
zziplib library.


%prep
%setup -q

%build
%configure \
    --disable-static \
    --enable-sdl \
    --enable-frame-pointer
# Remove rpath on 64bit archs
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' */libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' */libtool
%{__make}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot} transform='s,x,x,'

# remove libtool libraries
%{__rm} -f %{buildroot}%{_libdir}/*.la

%clean
%{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc docs/COPYING* ChangeLog README TODO
%{_libdir}/*.so.*

%files utils
%defattr(-,root,root,-)
%{_bindir}/*

%files devel
%defattr(-,root,root,-)
%doc docs/README.SDL docs/*.htm
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/*.m4
%{_mandir}/man3/*

%changelog
* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.62-1m)
- update to 0.13.62

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.60-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.60-1m)
- update to 0.13.60

* Sat Mar 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.59-4m)
- fix build failure with gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.59-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.59-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.59-1m)
- update to 0.13.59

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.58-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.58-1m)
- update to 0.13.58

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.56-1m)
- update to 0.13.56

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.13.54-2m)
- add transform

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.54-1m)
- update to 0.13.54

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.52-1m)
- update to 0.13.52

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.50-2m)
- rebuild against rpm-4.6

* Mon Dec 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.50-1m)
- update to 0.13.50
- import patch1 from Fedora devel and modify for this version

* Tue Aug 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.49-1m) 
- Initial build for Momonga Linux 5
