%global momorel 2

Summary: A program for synchronizing files over a network
Name: rsync
Version: 3.1.0
Release: %{momorel}m%{?dist}
Group: Applications/Internet
URL: http://rsync.samba.org/
Source0: http://rsync.samba.org/ftp/rsync/%{name}-%{version}.tar.gz
NoSource: 0
Source1: http://rsync.samba.org/ftp/rsync/rsync-patches-%{version}.tar.gz
NoSource: 1
Source2: rsync.xinetd
Patch0: rsync.git-0dedfbce2c1b851684ba658861fe9d620636c56a.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: momonga-rpmmacros >= 20080403-4m
BuildRequires: docbook-utils
BuildRequires: libacl-devel, libattr-devel, autoconf, make, gcc, popt-devel
License: GPLv3+

%description
Rsync uses a reliable algorithm to bring remote and host files into
sync very quickly. Rsync is fast because it just sends the differences
in the files over the network instead of sending the complete
files. Rsync is often used as a very powerful mirroring process or
just as a more capable replacement for the rcp command. A technical
report which describes the rsync algorithm is included in this
package.

%prep
%setup -q
%setup -q -b 1
%patch0 -p1 -b .CVE-2014-2855

chmod -x support/*

# Needed for compatibility with previous patched rsync versions
patch -p1 -i patches/acls.diff
patch -p1 -i patches/xattrs.diff

%build
rm -fr autom4te.cache
autoconf
autoheader

%configure --with-acl-support --with-xattr-support \
%if %{_ipv6}
  --enable-ipv6
%endif
make proto
make %{?_smp_mflags} CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}

%makeinstall
mkdir -p %{buildroot}/etc/xinetd.d
install -m 644 %{SOURCE2} %{buildroot}/etc/xinetd.d/rsync

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING NEWS OLDNEWS README support/ tech_report.tex
%config(noreplace) /etc/xinetd.d/rsync
%{_prefix}/bin/rsync
%{_mandir}/man1/rsync.1*
%{_mandir}/man5/rsyncd.conf.5*

%changelog
* Sat Apr 26 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-2m)
- [SECURITY] CVE-2014-2855

* Mon Sep 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-1m)
- update 3.1.0

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.9-1m)
- update 3.0.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.8-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.8-1m)
- [SECURITY] CVE-2011-1097
- update to 3.0.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.7-2m)
- full rebuild for mo7 release

* Mon Jan  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.7-1m)
- update to 3.0.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.6-1m)
- update to 3.0.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.5-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5

* Sun Sep 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Fri Jul 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Sun May 11 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.0.2-2m)
- copy origin of the xinetd file was wrong

* Fri Apr 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.2-1m)
- [SECURITY] Xattr security fix (http://rsync.samba.org/security.html#s3_0_2)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-2m)
- rebuild against gcc43

* Tue Mar  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-1m)
- update

* Sat Dec  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.9-3m)
- [SECURITY] CVE-2007-6199 CVE-2007-6200
- add http://rsync.samba.org/ftp/rsync/munge-symlinks-2.6.9.diff

* Wed Aug 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.9-2m)
- [SECURITY] CVE-2007-4091
- import security patch from suse

* Mon Feb  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-1m)
- up to 2.6.9

* Wed May  3 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.6.8-1m)
- up to 2.6.8
- [SECURITY] CVE-2006-2083

* Wed Dec 21 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.6-2m)
- add Patch0: rsync-2.6.6-xattr.patch from fc-devel rysnc-2.6.6-2.1

* Wed Oct 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.6-1m)
- version up

* Fri Oct  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.3-1m)
- this version contains a few new features and quite a few bug fixes.
- add more documents

* Mon Aug 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.2-4m)
- fix a security hole in non-chroot rsync daemon

* Sat Jul 31 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.2-3m)
- add /etc/xinetd.d/rsync

* Mon Jul 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.2-2m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Tue May  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.2-1m)
- bugfix release that mainly fixes a bug with the --relative option (-R)
  in 2.6.1 that could cause files to be transferred incorrectly

* Wed Apr 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.1-1m)
- primarily a performance release
- security fix that affects only people running a read/write daemon
  WITHOUT using chroot

* Sat Jan  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.0-1m)
- lots of bugfixing and enhancements

* Fri Dec  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.7-1m)
- major security fixes

* Thu Jan 30 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (2.5.6-2m)
- use ssh instead of rsh
- use %%makeinstall
- add more docs

* Thu Jan 30 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.6-1m)

* Tue Apr  2 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.5.5-2k)

* Thu Mar 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.5.4-2k)

* Tue Mar 12 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (2.5.3-2k)
- up to 2.5.3

* Sun Jan 27 2002 WATABE Toyokazu <toy2@kondara.org>
- (2.5.2-2k)
- update to 2.5.2, including security fix.

* Fri Jan  4 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.5.1-2k)

* Sat Dec  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.5.0-2k)
- remove kame patch because it was merged

* Thu Nov 29 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.4.7-0.0004002k)
- 2.4.7pre4

* Sun Nov 18 2001 Tsutomu Yasuda <tom@kondara.org>
- (2.4.7-0.0001004k)
  no optimize

* Wed Aug 15 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (2.4.7-0.0001002k)
- update to 2.4.7pre1
- add URL: tag

* Tue Mar  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.4.6-4k)
- errased IPv6 function with %{_ipv6} macro

* Thu Oct 19 2000 Motonobu Ichimura <famao@kondara.org>
- updated to 2.4.6 and IPv6 patch

* Fri Aug 11 2000 AYUHANA Tomonori <l@kondara.org>
- (2.4.4-1k)
- update to 2.4.4

* Thu Jun 26 2000 Masaaki Noro <noro@flab.fujitsu.co.jp>
- updated to 2.4.3
- merged IPv6 kame patch

* Thu Feb 24 2000 Tenkou N. Hattori <tnh@kondara.org>
- version down to 2.3.2 :-p

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Jun 12 1999 Jeff Johnson <jbj@redhat.com>
- add "max. delete" patch to limit damage when server is hosed.

* Wed Apr 07 1999 Bill Nottingham <notting@redhat.com>
- update to 2.3.1.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Tue Mar 16 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.3.0.

* Sat Mar 13 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.3.0 beta.

* Fri Dec 18 1998 Bill Nottingham <notting@redhat.com>
- update to 2.2.1

* Thu Sep 10 1998 Jeff Johnson <jbj@redhat.com>
- updated to 2.1.1

* Mon Aug 17 1998 Erik Troan <ewt@redhat.com>
- updated to 2.1.0

* Thu Aug 06 1998 Erik Troan <ewt@redhat.com>
- buildrooted and attr-rophied
- removed tech-report.ps; the .tex should be good enough

* Mon Aug 25 1997 John A. Martin <jam@jamux.com>
- Built 1.6.3-2 after finding no rsync-1.6.3-1.src.rpm although there
  was an ftp://ftp.redhat.com/pub/contrib/alpha/rsync-1.6.3-1.alpha.rpm
  showing no packager nor signature but giving 
  "Source RPM: rsync-1.6.3-1.src.rpm".
- Changes from 1.6.2-1 packaging: added '%{optflags}' to make, strip
  to '%build', removed '%prefix'.

* Thu Apr 10 1997 Michael De La Rue <miked@ed.ac.uk>
- rsync-1.6.2-1 packaged.  (This entry by jam to credit Michael for the
  previous package(s).)
