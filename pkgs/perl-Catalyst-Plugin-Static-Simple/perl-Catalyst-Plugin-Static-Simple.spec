%global         momorel 1

Name:           perl-Catalyst-Plugin-Static-Simple
Version:        0.32
Release:        %{momorel}m%{?dist}
Summary:        Make serving static pages painless
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Plugin-Static-Simple/
Source0:        http://www.cpan.org/authors/id/J/JJ/JJNAPIORK/Catalyst-Plugin-Static-Simple-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Catalyst-Runtime >= 5.80008
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MIME-Types >= 2.03
BuildRequires:  perl-Moose
BuildRequires:  perl-MooseX-Types
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-Test-Simple
Requires:       perl-Catalyst-Runtime >= 5.80008
Requires:       perl-MIME-Types >= 2.03
Requires:       perl-Moose
Requires:       perl-MooseX-Types
Requires:       perl-namespace-autoclean
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Static::Simple plugin is designed to make serving static content in
your application during development quick and easy, without requiring a
single line of code from you.

%prep
%setup -q -n Catalyst-Plugin-Static-Simple-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Catalyst/Plugin/Static/Simple.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- rebuild against perl-5.20.0
- update to 0.32

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-2m)
- rebuild against perl-5.18.2

* Fri Sep 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.29-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-2m)
- rebuild against perl-5.12.0

* Wed Feb  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Mon Jan  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- rebuild against perl-5.10.1

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20-2m)
- rebuild against gcc43

* Tue Sep 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-2m)
- add BuildRequires: perl-Catalyst-Runtime >= 5.30
- add BuildRequires: perl-MIME-Types >= 1.15
- add Requires: perl-Catalyst-Runtime >= 5.30
- add Requires: perl-MIME-Types >= 1.15

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.14-3m)
- use vendor

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-2m)
- delete dupclicate directory

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.14-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
