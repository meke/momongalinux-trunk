%global         momorel 1

Summary:	POE-Component-IKC module for perl 
Name:		perl-POE-Component-IKC
Version:	0.2401
Release:	%{momorel}m%{?dist}
License:	GPL
Group:		Development/Languages
Source0:	http://www.cpan.org/authors/id/G/GW/GWYN/POE-Component-IKC-%{version}.tar.gz
NoSource:	0
URL:		http://www.cpan.org/modules/by-module/POE/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-POE >= 1
BuildRequires:  perl-POE-API-Peek >= 1
BuildRequires:  perl-Scalar-Util >= 1
BuildRequires:  perl-Test-Simple >= 0.6
Requires:       perl-POE >= 1
Requires:       perl-POE-API-Peek >= 1
Requires:       perl-Scalar-Util >= 1
Requires:       perl-Test-Simple >= 0.6
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is Inter-Kernel Communication for POE. It is used to get events from
one POE kernel to another

%prep
%setup -q -n POE-Component-IKC-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes FUTUR ikc-architecture.txt README test-client test-lite test-thunk TODO
%{perl_vendorlib}/POE/Component/IKC.pm
%{perl_vendorlib}/POE/Component/IKC.pod
%{perl_vendorlib}/POE/Component/IKC
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2401-1m)
- rebuild against perl-5.20.0
- update to 0.2401

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2305-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2305-2m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2305-1m)
- update to 0.2305

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2304-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2304-2m)
- rebuild against perl-5.16.3

* Tue Feb  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2304-1m)
- update to 0.2304

* Fri Jan 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2303-1m)
- update to 0.2303

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2302-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2302-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2302-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2302-2m)
- rebuild against perl-5.14.2

* Sun Aug 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2302-1m)
- update to 0.2302

* Sat Aug 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2301-1m)
- update to 0.2301

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-11m)
- rebuild against perl-5.14.0-0.2.1m
- ignore some test failures...

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2200-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2200-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2200-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-5m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2200-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-2m)
- rebuild against perl-5.10.1

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2200-1m)
- update to 0.2200

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2101-1m)
- update to 0.2101

* Sat Apr  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2003-1m)
- update to 0.2003

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2002-2m)
- rebuild against rpm-4.6

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (02002-1m)
- update to 0.2002

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2001-1m)
- update to 0.2001

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2000-2m)
- rebuild against gcc43

* Fri Nov 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2000-1m)
- update to 0.2000

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1904-2m)
- use vendor

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1904-1m)
- update to 0.1904

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1902-1m)
- update to 0.1902

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1802-1m)
- spec file was autogenerated
