%global momorel 5

Summary: A library to handle EB, EBG, EBXA, EBXA-C, S-EBXA and EPWING 
Name: eb
Version: 4.4.3
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
Source0: ftp://ftp.sra.co.jp/pub/misc/eb/eb-%{version}.tar.bz2
NoSource: 0
Source1: http://www.sra.co.jp/people/m-kasahr/dict-compress/epwing-compress.html
Source2: http://www.sra.co.jp/people/m-kasahr/dict-compress/sebxa-compress.html
URL: http://www.sra.co.jp/people/m-kasahr/eb/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: ndtpd-devel
BuildRequires: zlib-devel >= 1.1.4-5m
Requires(post): info

%description
EB is a library to handle EB, EBG, EBXA, EBXA-C, S-EBXA and EPWING.

%package devel
Summary: A library and header files for eb
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: ndtpd-devel

%description devel
EB is a library to handle EB, EBG, EBXA, EBXA-C, S-EBXA and EPWING.

%package static
Summary: static library for eb
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description static
EB is a library to handle EB, EBG, EBXA, EBXA-C, S-EBXA and EPWING.

%prep
%setup -q
cp -p %{SOURCE1} .
cp -p %{SOURCE2} .

%build
%configure \
    --without-included-zlib \
    --enable-pthread \
    --enable-largefile
%make

%install
rm -rf %{buildroot}
%makeinstall
rm -rf %{buildroot}%{_datadir}/eb/doc

find %{buildroot} -name "*.la" -delete

%post
/sbin/ldconfig
# remove old info files
for i in eb-ja ebappendix-ja ebfont-ja ebfont ebinfo-ja ebinfo ebrefile-ja ebrefile ebstopcode-ja ebstopcode ebzip-ja ebzip; do \
    /sbin/install-info --delete %{_infodir}/$i.info %{_infodir}/dir 2> /dev/null || :
done

%postun
if [ "$1" = 0 ]; then
    /sbin/ldconfig
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog* NEWS README
%doc doc/*.html
%doc samples
%config /etc/eb.conf
%{_bindir}/ebappendix
%{_bindir}/ebfont
%{_bindir}/ebinfo
%{_bindir}/ebrefile
%{_bindir}/ebstopcode
%{_bindir}/ebunzip
%{_bindir}/ebzip
%{_bindir}/ebzipinfo
%{_libdir}/libeb.so.*
%{_datadir}/aclocal/eb4.m4
%dir %{_datadir}/eb
%dir %{_datadir}/eb/appendix
%lang(ja) %{_datadir}/locale/ja/LC_MESSAGES/eb.mo
%lang(ja) %{_datadir}/locale/ja/LC_MESSAGES/ebutils.mo

%files devel
%defattr(-,root,root,-)
%{_libdir}/libeb.so
%dir %{_includedir}/eb
%{_includedir}/eb/appendix.h
%{_includedir}/eb/binary.h
%{_includedir}/eb/booklist.h
%{_includedir}/eb/defs.h
%{_includedir}/eb/eb.h
%{_includedir}/eb/error.h
%{_includedir}/eb/font.h
%{_includedir}/eb/sysdefs.h
%{_includedir}/eb/text.h
%{_includedir}/eb/zio.h

%files static
%defattr(-,root,root,-)
%{_libdir}/libeb.a

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.3-3m)
- full rebuild for mo7 release

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (4.4.3-2m)
- add %%dir

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.3-1m)
- update to 4.4.3
- split out static package

* Sun Jan 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-1m)
- update to 4.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.1-3m)
- enable largefile

* Sun Mar 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.1-2m)
- temporarily add --disable-largefile configure option
-- memory corruption occurs at starting ebview

* Sat Mar 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.1-1m)
- update to 4.4.1

* Sun Jan 25 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.4-3m)
- fix %%post

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.4-2m)
- rebuild against rpm-4.6

* Sun Dec 28 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.4-1m)
- update to 4.3.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.2-2m)
- rebuild against gcc43

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.2-1m)
- update to 4.3.2

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.3.1-2m)
- rebuild against perl-5.10.0-1m

* Sun Jan 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-1m)
- update to 4.3.1

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.1-3m)
- change Source URL

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.1-2m)
- delete libtool library

* Thu Sep 15 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.1-1m)
- version up

* Wed Jun  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2-1m)
- version up

* Thu Jan 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.1.3-1m)
- up to 4.1.3

* Sat Dec  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.2-1m)
- bugfix version

* Wed Oct 27 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.1-1m)
- bugfix version

* Mon Jul  5 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1-1m)
- release

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1-0.1.1m)
- 4.1rc1

* Wed Jun 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1-0.0.1m)
- 4.1rc0

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (4.0-3m)
- revised spec for enabling rpm 4.2.

* Fri Jan 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0-2m)
- revise prototype definitions
- http://www.sra.co.jp/people/m-kasahr/ndtpd/ml-archives/msg01893.html

* Sun Dec 28 2003 Kazuhiko <kazuhiko@fdiary.net>
- (4.0-1m)

* Sat Dec 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (4.0-0.3.4m)
- really enable pthread

* Mon Nov 24 2003 Kazuhiko <kazuhiko@fdiary.net>
- (4.0-0.3.3m)
- apply 'eb.diff'

* Fri Nov 14 2003 Kazuhiko <kazuhiko@fdiary.net>
- (4.0-0.3.2m)
- apply 'eb-4.0beta3+.diff'

* Mon Oct 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (4.0-0.3.1m)
- 4.0beta3

* Sun Jul 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.4-1m)

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.2-2m)
- rebuild against zlib 1.1.4-5m

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.2-1m)

* Sun Mar  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.1-1m)

* Tue Feb 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.3-2m)
- apply a patch to eb/multi.c [ndtpd 01795]

* Fri Feb  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.3-1m)

* Sat Feb  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.3-0.3.2m)
- apply a patch to eb/binary.c [ndtpd 01787]

* Sun Jan 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.3-0.3.1m)
- 3.3beta3

* Mon Nov  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.3-0.2.1m)
- 3.3beta2

* Mon Sep 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.3-0.1.1m)
- 3.3beta1

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.3-0.0.1m)
- 3.3beta0

* Sat Aug 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-1m)

* Tue Jun 11 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.2.2-2k)

* Thu Mar 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.2.1-2k)

* Wed Feb 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.2-2k)

* Wed Jan 23 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.2-0.0003002k)
- 3.2beta3

* Thu Jan 17 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.2-0.0001004k)
- add a patch for zio.c

* Tue Jan  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.2-0.0001002k)
- 3.2beta1
- enable pthreads

* Sat Dec 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.2-0.0000002k)
- 3.2beta0

* Sat Dec  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1-14k)
- revise text.c patch to support inline bmp

* Mon Nov 26 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1-12k)
- add text.c patch

* Sun Nov 18 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1-10k)
- add eb-3.1+4.diff

* Mon Nov 12 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1-8k)
- add eb-3.1+3.diff

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (3.1-6k)
- rebuild against gettext 0.10.40.

* Thu Oct  4 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1-4k)
- add eb-3.1+.diff and eb-3.1+2.diff

* Fri Aug 31 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.1-2k)
- use %configure macro

* Sun Jul  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0.1-2k)

* Wed Jun  6 2001 Hidetomo Machi <mcHT@kondara.org>
- (3.0-2k)
- use macro
- modif files section

* Tue Jun  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0-3k)

* Tue Apr 10 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0-0.0100003k) 
- 0.0100 means beta0, 0.0101 means beta1...

* Thu Feb  8 2001 Kazuhiko <kazuhiko@kondara.org>
- (3.0-0.0004003k) 

* Sat Oct 28 2000 Toru Hoshina <toru@df-usa.com>
- bugfix by Kazuhiko has been merged, release 3k.

* Wed Oct 25 2000 Kenichi Matsubara <m@kondara.org>
- info.gz to info.*.

* Thu Oct 19 2000 Kazuhiko <kazuhiko@kondara.org>
- (3.0alpha1-5k)
- add ebzip.c patch

* Fri Oct 13 2000 Kazuhiko <kazuhiko@kondara.org>
- (3.0alpha1-3k)
- modify eb/text.c (patch by Rei)

* Mon Oct  9 2000 Kazuhiko <kazuhiko@kondara.org>
- (eb-3.0alpha1)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Jun 27 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (2.3.8-1k)
- version 2.3.8

* Mon Apr 17 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- version 2.3.7

* Fri Jan 21 2000 Toru Hoshina <t@kondara.org>
- fixed info conflict.

* Fri Jan 14 2000 Yuichi ARATA <you@magiccity.ne.jp>
- /usr/local -> /usr

* Thu Dec 23 1999 Masanori Hanawa <hanawa@kde.gr.jp>
- The 1st RPM release (Kondara style)
