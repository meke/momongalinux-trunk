%global momorel 1
#%%global snapdate 2010-06-27


Summary: Complex Graphical Design/Layout Engine
Name: edje
Version: 1.7.7
Epoch: 1
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://www.enlightenment.org/
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2 
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libjpeg-devel
BuildRequires: zlib-devel
BuildRequires: imlib2-devel
BuildRequires: vim-common
BuildRequires: lua-devel
BuildRequires: ecore-devel  >= 1.7.7
BuildRequires: embryo-devel >= 1.7.7
BuildRequires: evas-devel   >= 1.7.7
BuildRequires: eet-devel >= 1.7.7
BuildRequires: chrpath pkgconfig doxygen
Requires: python

%description
A graphical layout and animation library for animated resizable, compressed and
scalable themes.

%package devel
Summary: Edje headers, static libraries, documentation and test programs
Group: System Environment/Libraries
Requires: %{name} = %{epoch}:%{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

%build
%configure --disable-static
%make
sed -i -e 's/$projectname Documentation Generated: $datetime/$projectname Documentation/' doc/foot.html


%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
chrpath --delete %{buildroot}%{_bindir}/%{name}_cc
chrpath --delete %{buildroot}%{_bindir}/%{name}_decc
find %{buildroot} -name '*.la' -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/%{name}_*
%{_bindir}/inkscape2edc
%{_libdir}/*.so.*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/utils
%{_libdir}/%{name}/utils/epp
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/include
%{_datadir}/%{name}/include/%{name}.inc
%{_datadir}/mime/packages/edje.xml

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_datadir}/%{name}/examples
%{_datadir}/%{name}/examples/*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.7.7-1m)
- update to 1.7.7

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1:1.0.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.0.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1:1.0.0-1m)
- update to 1.0 release of core EFL

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.9.99.49898-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.9.99.49898-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.9.99.49898-2m)
- add epoch to %%changelog

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1:0.9.99.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1:0.9.9.49539-2m)
- add epoch

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.49539-1m)
- update to new svn snap
- version renumbering

* Wed Jun 02 2010 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.9.93.063-2m)
- build requires lua-devel

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.93.063-1m)
- update to new svn snap

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.92.062-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.062-1m)
- update to new svn snap

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.061-1m)
- update to new svn snap

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.9.060-1m)
- update

* Thu May 21 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.9.9.050-4m)
- add autoreconf (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.050-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9.050-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.050-1m)
- update

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.9.043-1m)
- update to 0.9.9.043
- add BuildPreReq: evas-devel >= 0.9.9.043-1m

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0.042-1m)
- merge from T4R
- 
- changelog is below
- * Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- - (0.5.0.042-1m)
- - update to 0.5.0.042
- - delete %%{_bindir}/edje-config

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0.037-0.20061231.3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0.037-0.20061231.2m)
- %%NoSource -> NoSource

* Mon Jan  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0.037-0.20061231.1m)
- version 0.5.0.037-20061231

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0.022-0.20051209.1m)
- version 0.5.0.022-20051209

* Tue Jun 28 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0.010-0.20050627.1m)
- version 0.5.0.010-20050627

* Thu Feb 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0-0.20050215.1m)
- version 0.5.0-0.20050215

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.0-0.20041218.3m)
- rebuid against ecore-1.0.0-20041218.2m

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.5.0-0.20041218.2m)
- enable x86_64.

* Tue Dec 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0-0.20041218.1m)
- first import to Momonga
