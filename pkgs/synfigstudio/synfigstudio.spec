%global         momorel 1

Summary:        vector animation renderer
Name:           synfigstudio
Version:        0.63.05
Release:        %{momorel}m%{?dist}
License:        GPLv2
Group:          Applications/Multimedia
URL:            http://synfig.org/
Source0:        http://dl.sourceforge.net/sourceforge/synfig/%{name}-%{version}.tar.gz
NoSource:       0
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  OpenEXR-devel >= 1.7.1
BuildRequires:  etl-devel >= 0.04.15
BuildRequires:  glibmm-devel
BuildRequires:  gtkmm-devel
BuildRequires:  ilmbase-devel >= 1.0.3
BuildRequires:  libsigc++-devel
BuildRequires:  libxml++-devel
BuildRequires:  synfig-devel >= 0.62.01

%description
synfig is a vector based 2D animation package. It is designed to be capable of producing feature-film quality animation. It eliminates the need for tweening, preventing the need to hand-draw each frame. synfig features spatial and temporal resolution independence (sharp and smooth at any resolution or framerate), high dynamic range images, and a flexible plugin system.

synfigstudio is the animation studio for synfig and provides the GUI
interface to create synfig animations which are saved in synfig .sif
format.

%package devel
Summary:        Header files and libraries for a development with %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Header files and libraries for a development with %{name}.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%find_lang %{name}

rm -rf %{buildroot}%{_libdir}/libsynfigapp.la

# Remove all mime sub-directories but package
rm -rf $RPM_BUILD_ROOT%{_datadir}/mime/{application,generic-icons,globs,globs2,icons}
rm -rf $RPM_BUILD_ROOT%{_datadir}/mime/{treemagic,types,version,XMLnamespaces}
rm -rf $RPM_BUILD_ROOT%{_datadir}/mime/{aliases,subclasses,mime.cache,magic}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog* INSTALL NEWS README TODO
%{_bindir}/%{name}
%{_libdir}/libsynfigapp.so.*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/synfig_icon.*
%{_datadir}/mime-info/%{name}.*
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/pixmaps/%{name}
%{_datadir}/pixmaps/*_icon.png

%files devel
%defattr(-,root,root)
%{_includedir}/synfigapp-0.0
%{_libdir}/libsynfigapp.so

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63.05-1m)
- update to 0.63.05
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62.01-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62.01-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.62.01-3m)
- add patch for gcc45, generated by gen45patch(v1)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.62.01-2m)
- full rebuild for mo7 release

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62.01-1m)
- update to 0.62.01

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.61.09-3m)
- fix up desktop file
- License: GPLv2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.61.09-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.61.09-1m)
- update 0.61.09

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.61.08-2m)
- rebuild against rpm-4.6

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.61.08-1m)
- import to Momonga
