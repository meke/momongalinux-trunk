%global momorel 1

Summary: A library of functions for manipulating PNG image format files
Name: libpng
Version: 1.2.51
Release: %{momorel}m%{?dist}
License: see "LICENSE"
Group: System Environment/Libraries
URL: http://www.libpng.org/pub/png/libpng.html
Source0: http://dl.sourceforge.net/project/%{name}/%{name}12/%{version}/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: libpng-1.2.18-libdir.patch
Patch1: libpng-1.2.39-pngconf.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel >= 1.1.4-5m
Requires: zlib >= 1.1.4-5m
Provides: libpng-compat121
Obsoletes: libpng-compat121

%description
The libpng package contains a library of functions for creating and
manipulating PNG (Portable Network Graphics) image format files.  PNG is
a bit-mapped graphics format similar to the GIF format.  PNG was created to
replace the GIF format, since GIF uses a patented data compression
algorithm.

Libpng should be installed if you need to manipulate PNG format image
files.

%package devel
Summary: Development tools for programs to manipulate PNG image format files
Group: Development/Libraries
Requires: libpng = %{version}

%description devel
The libpng-devel package contains the header files and static libraries
necessary for developing programs using the PNG (Portable Network
Graphics) library.

If you want to develop programs which will manipulate PNG image format
files, you should install libpng-devel.  You'll also need to install the
libpng package.

%prep
%setup -q
%patch0 -p1 -b .libdir~
%patch1 -p1 -b .pngconf

%build
%configure
make

%install
%{__rm} -rf --preserve-root %{buildroot}
%makeinstall
%{__rm} -rf %{buildroot}%{_libdir}/libpng.la
%{__rm} -rf %{buildroot}%{_libdir}/libpng12.la

%clean
%{__rm} -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc ANNOUNCE CHANGES KNOWNBUG LICENSE README TODO Y2KINFO *.txt
%{_libdir}/libpng*.so.*
%{_mandir}/man5/*

%files devel
%defattr(-,root,root)
%doc example.c contrib
%{_bindir}/*
%{_includedir}/*
%{_libdir}/libpng*.a
%{_libdir}/libpng*.so
%{_libdir}/pkgconfig/*
%{_mandir}/man3/*

%changelog
* Fri Mar 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.51-1m)
- update 1.2.51

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.50-1m)
- [SECURITY] CVE-2012-3386
- update to 1.2.50

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.49-1m)
- [SECURITY] CVE-2011-3048 CVE-2012-3425
- update to 1.2.49

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.48-1m)
- update to 1.2.48

* Fri Feb 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.46-2m)
- [SECURITY] CVE-2011-3026

* Fri Jul 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.46-1m)
- [SECURITY] CVE-2011-2501 CVE-2011-2690 CVE-2011-2691 CVE-2011-2692
- update to 1.2.46

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.44-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.44-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.44-2m)
- full rebuild for mo7 release

* Tue Jun 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.44-1m)
- [SECURITY] CVE-2010-1205 CVE-2010-2249
- update to 1.2.44

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.43-1m)
- [SECURITY] CVE-2010-0205
- update to 1.2.43

* Mon Dec  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.41-1m)
- update to 1.2.41

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.40-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.40-1m)
- update to 1.2.40

* Fri Aug 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.39-1m)
- update to 1.2.39

* Wed Jun 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.37-1m)
- [SECURITY] CVE-2009-2042
- update to 1.2.37

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.36-1m)
- update to 1.2.36

* Thu Feb 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.35-1m)
- [SECURITY] CVE-2009-0040
- update to 1.2.35

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.34-2m)
- rebuild against rpm-4.6

* Sun Dec 28 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.34-1m)
- [SECURITY] CVE-2008-5907
- update to 1.2.34
-- update Patch0 for fuzz=0

* Sat Nov  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.33-1m)
- [SECURITY] CVE-2008-6218
- update to 1.2.33

* Wed Oct 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.32-1m)
- update to 1.2.32 (include CVE-3964 fix)

* Sun Sep 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.31-1m)
- [SECURITY] CVE-2008-1382 CVE-2008-3964
- update to 1.2.31
- import `Patch2: libpng-ztxt-bug.patch' from Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.25-2m)
- rebuild against gcc43

* Fri Feb 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.25-1m)
- update 1.2.25

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.22-2m)
- %%NoSource -> NoSource

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.22-1m)
- update to 1.2.22
- fix crash bug when reading the ICC-profile chunk, iCCP

* Wed Oct 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.21-1m)
- [SECURITY] CVE-2007-5266 CVE-2007-5267 CVE-2007-5268 CVE-2007-5269
- update to 1.2.21

* Thu May 24 2007 Masayuki SANO <nosanosa@momonga-linix.org>
- (1.2.18-1m)
- update to 1.2.18
- - security vulnerability fixed [CVE-2007-2445]

* Sat Dec 02 2006 Masayuki SANO <nosanosa@momonga-linix.org>
- (1.2.14-1m)
- update to 1.2.14

* Fri Nov 17 2006 Masayuki SANO <nosanosa@momonga-linix.org>
- (1.2.13-1m)
- update 1.2.13 (Minor security fixes)

* Wed Aug 16 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (1.2.12-2m)
- import libpng-1.2.10-pngconf.patch from Fedora Core
  Disable asm on arches other than i386
  see https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=196580

* Tue Jul  4 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.12-1m)
- [SECURITY] CVE-2006-3334
- Fix potential buffer overrun in chunk error processing.

* Sun Feb  6 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.8-3m)
- fix libpng.pc

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.8-2m)
- enable x86_64.

* Wed Dec  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.8-1m)
- minor bugfixes

* Sun Oct  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.7-3m)
- fixed for 64 bit arch

* Sun Oct  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.7-2m)
- fixed for 64 bit arch

* Thu Sep 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.7-1m)
- major bugfixes

* Sun Sep  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.6-1m)
- versin up

* Thu Aug  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.5-10m)
- [SECURITY] apply 'libpng-1.2.5-all-patches.txt'

* Sat Jul 10 2004 TAKAHASHI Tamotsu <tamo>
- (1.2.5-9m)
- [SECURITY] CAN-2002-1363
  http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2002-1363
  http://www.mandrakesoft.com/security/advisories?name=MDKSA-2004:063

* Wed May 12 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.5-8m)
- [SECURITY] fixed CAN-2004-0421
  http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0421
  http://www.freebsd.org/cgi/cvsweb.cgi/ports/graphics/png/files/patch-ac?rev=1.1&content-type=text/x-cvsweb-markup

* Thu Oct 23 2003 zunda <zunda at freeshell.org>
- (1.2.5-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Aug 13 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.2.5-6m)
- use %%NoSource macros
- change the URL of %%{SOURCE0}

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.5-5m)
- rebuild against zlib 1.1.4-5m

* Mon Nov 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.5-4m)
- add '-lz -lm' in making shared libraries

* Thu Nov 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.5-3m)
- merge libpng-compat121

* Fri Oct 25 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.5-1m)
- modified patch

* Sat Oct  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.5-1m)
- minor bugfixes

* Mon Jul  8 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.4-1m)

* Sun May 26 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.2.3-1k)
- up to 1.2.3

* Sun Apr 21 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2.2-8k)
- add subpackage libpng-compat121

* Sun Apr 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.2-6k)
- ugly hack for possible missing file...

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.2-4k)
- Provides: libpng.so.3 for compatibility
- support pkgconfig

* Fri Apr 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.2-2k)

* Thu Dec 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2.1-2k)

* Wed Dec 19 2001 Toru Hoshina <t@kondara.org>
- (1.2.0-4k)
- man libpng.so.3... I cannot read...

* Fri Sep 21 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.0-3k)
- up to 1.2.0

* Thu Aug 23 2001 Motonobu Ichimura <famao@kondara.org>
- 1.0.12
- up to 1.0.12.

* Thu Aug 10 2000 Kazuhiko <kazuhiko@kondara.org>
- 1.0.8
- adapt patch0 for 1.0.8

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Jul 03 2000 Takaaki Tabuchi <tab@kondara.org>
- 1.0.7-1k
- update to 1.0.7
- add URL
- add -q at %setup
- comment out patches
- adapt patch0 for 1.0.7 and bziped

* Thu Apr 27 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- 1.0.6

* Sun Nov 21 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.0.5
- some tweaks to spec file to make updating easier
- handle RPM_OPT_FLAGS

* Mon Sep 20 1999 Matt Wilson <msw@redhat.com>
- changed requires in libpng-devel to include serial
- corrected typo

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 2)

* Sun Feb 07 1999 Michael Johnson <johnsonm@redhat.com>
- rev to 1.0.3

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build for 6.0

* Wed Sep 23 1998 Cristian Gafton <gafton@redhat.com>
- we are Serial: 1 now because we are reverting the 1.0.2 version from 5.2
  beta to this prior one
- install man pages; set defattr defaults

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- devel subpackage moved to Development/Libraries

* Wed Apr 08 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.0.1
- added buildroot

* Tue Oct 14 1997 Donnie Barnes <djb@redhat.com>
- updated to new version
- spec file cleanups

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
