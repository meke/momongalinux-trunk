%global momorel 2

Summary: Apple Built-In iSight Firmware Tools
Name: isight-firmware-tools
Version: 1.6
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://bersace03.free.fr/ift/
Source: http://bersace03.free.fr/ift/%{name}-%{version}.tar.gz
Patch0: isight-firmware-tools-ift-load-path.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:   udev
Requires(post): info
Requires(preun): info
BuildRequires: glib2-devel
BuildRequires: intltool
BuildRequires: libgcrypt-devel
BuildRequires: libusb-devel
BuildRequires: sed

%description
Apple Built-In iSight Firmware Tools.

%prep
%setup -q

%patch0 -p1

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

# clean up
rm -rf %{buildroot}%{_docdir}/%{name}
rm -f %{buildroot}%{_infodir}/dir

%find_lang %{name}

%clean
%__rm -rf "%{buildroot}"

%post
/sbin/install-info %{_infodir}/ift-export.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/ift-extract.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/ift-export.info %{_infodir}/dir || :
  /sbin/install-info --delete %{_infodir}/ift-extract.info %{_infodir}/dir || :
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HOWTO INSTALL NEWS README
%{_sysconfdir}/udev/rules.d/isight.rules
%{_bindir}/ift-export
%{_bindir}/ift-extract
/lib/udev/ift-load
%{_infodir}/ift-export.info.*
%{_infodir}/ift-extract.info.*
%{_mandir}/man1/ift-export.1*
%{_mandir}/man1/ift-extract.1*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-2m)
- rebuild for glib 2.33.2

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6-1m)
- update 1.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-9m)
- full rebuild for mo7 release

* Mon Feb 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-8m)
- change hal-callouts-dir from %%{_libdir}/hal/scripts to %%{_libexecdir}/scripts
- use %%make
- add %%doc
- modify %%files
- License: GPLv2

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-6m)
- rebuild against rpm-4.6

* Mon Nov 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-5m)
- fix force bz2 info

* Fri Nov 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-4m)
- run install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-3m)
- rebuild against gcc43

* Thu Mar 27 2008 Masanobu Sato <satoshiga@momonga-linux-org>
- (1.2-2m)
- fix libhal dir for ift-callout

* Wed Mar 26 2008 Masanobu Sato <satoshiga@momonga-linux-org>
- (1.2-1m)
- version up

* Tue Mar 25 2008 Masanobu Sato <satoshiga@momonga-linux-org>
- (1.0.2-2m)
- add source

* Tue Mar 25 2008 Masanobu Sato <satoshiga@momonga-linux-org>
- (1.0.2-1m)
- initial build


