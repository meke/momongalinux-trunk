%global momorel 1

# NOTE: This package contains only C source and header files and pkg-config
# *.pc files, and does not contain any ELF binaries or DSOs, so we disable
# debuginfo generation.
%define debug_package %{nil}

Summary: X.Org X11 developmental X transport library
Name: xorg-x11-xtrans-devel
Version: 1.3.4
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/xtrans-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros

Requires(pre): xorg-x11-filesystem >= 0.99.2-3

%description
X.Org X11 developmental X transport library

%prep
%setup -n xtrans-%{version}

%build
%configure --enable-docs
# Running 'make' not needed.

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%dir %{_includedir}/X11/Xtrans
%{_includedir}/X11/Xtrans/Xtrans.c
%{_includedir}/X11/Xtrans/Xtrans.h
%{_includedir}/X11/Xtrans/Xtransint.h
%{_includedir}/X11/Xtrans/Xtranslcl.c
%{_includedir}/X11/Xtrans/Xtranssock.c
%{_includedir}/X11/Xtrans/Xtransutil.c
%{_includedir}/X11/Xtrans/transport.c
%{_datadir}/pkgconfig/xtrans.pc
%{_datadir}/aclocal/xtrans.m4
%{_datadir}/doc/xtrans

%changelog
* Sun Apr 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4

* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.7-1m)
- update to 1.2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.6-2m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-1m)
- update 1.2.5

* Tue Sep 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-1m)
- update 1.2.4

* Sat Feb 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-1m)
- update 1.2.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against rpm-4.6

* Thu Jul  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat May 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-1m)
- update 1.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- %%NoSource -> NoSource

* Wed Aug 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Thu Jan 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Tue Jun 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-5m)
- Security advisory http://xorg.freedesktop.org/releases/X11R7.1/patches/

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-4m)
- Change Source URL
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-3.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-3.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan 23 2006 Mike A. Harris <mharris@redhat.com> 1.0.0-3
- Bump and rebuild.

* Fri Dec 23 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-2
- Bump and rebuild.

* Thu Dec 15 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Update to xtrans-1.0.0 from X11R7 RC4 release.

* Tue Nov 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Add "Requires(pre): xorg-x11-filesystem >= 0.99.2-3" to avoid bug (#173384).

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Update to xtrans-0.99.2 from X11R7 RC2 release.

* Thu Oct 20 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Update to xtrans-0.99.1 from X11R7 RC1 release.
- This package contains only C source and header files and pkg-config
  *.pc files, and does not contain any ELF binaries or DSOs, so we disable
  debuginfo generation.

* Sun Oct  2 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Use Fedora-Extras style BuildRoot tag
- Add tarball URL

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
