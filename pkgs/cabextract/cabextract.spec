%global momorel 4

Summary: A program to extract Microsoft Cabinet files
Name: cabextract
Version: 1.3
Release: %{momorel}m%{?dist}
License: GPLv3
Group: Applications/Archiving
URL: http://www.cabextract.org.uk/
Source0: http://www.cabextract.org.uk/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils

%description
Cabinet (.CAB) files are a form of archive, which Microsoft use to
distribute their software, and things like Windows Font Packs. The
cabextract program simply unpacks such files.

%prep
%setup -q

%build
%configure
%make

%install
[ %{buildroot} != "/" ] && rm -rf %{buildroot}
%makeinstall

# install Japanese manual page
mkdir -p %{buildroot}%{_mandir}/ja/man1
iconv -f euc-jp -t utf-8 doc/ja/%{name}.1 > %{buildroot}%{_mandir}/ja/man1/%{name}.1
chmod 644 %{buildroot}%{_mandir}/ja/man1/%{name}.1

%clean
[ %{buildroot} != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%doc doc/magic doc/wince_cab_format.html
%{_bindir}/%{name}
%{_mandir}/ja/man1/%{name}.1*
%{_mandir}/man1/%{name}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-1m)
- [SECURITY] CVE-2010-2800 CVE-2010-2801
- version 1.3
- License: GPLv3

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-8m)
- touch up spec file
- License: GPLv2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-5m)
- rebuild against gcc43

* Wed Mar 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-4m)
- update URL and Source0 URL

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-3m)
- %%NoSource -> NoSource

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-2m)
- convert ja.man from EUC-JP to UTF-8

* Fri Oct  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-1m)
- version 1.2

* Mon Oct 18 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1m)
- security fix
- see NEWS or http://www.kyz.uklinux.net/cabextract.php#changes

* Sat Aug 14 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- cp -> install -m 644

* Fri Jun 18 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- version 1.0

* Fri Jun 18 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-1m)
- import
