%global momorel 24
#%global fontver 1.1

Summary: Video4Linux Stream Capture Viewer
Name: xawtv
Version: 3.101
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
License: GPL
URL: http://linuxtv.org/
Source0: http://linuxtv.org/downloads/xawtv/xawtv-3.101.tar.bz2
#Source1: http://dl.bytesex.org/releases/tv-fonts/tv-fonts-%{fontver}.tar.bz2
NoSource: 0
#NoSource: 1
Source2: xawtv.desktop
Source3: v4l-conf.pam
Patch1: xawtv-3.101-app-defaults.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ImageMagick
BuildRequires: alsa-lib-devel
BuildRequires: autoconf
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: libFS-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXaw-devel
BuildRequires: libXext-devel
BuildRequires: libXft-devel
BuildRequires: libXinerama-devel
BuildRequires: libXmu-devel
BuildRequires: libXp-devel
BuildRequires: libXpm-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libXt-devel
BuildRequires: libXv-devel
BuildRequires: libXxf86dga-devel
BuildRequires: libXxf86vm-devel
BuildRequires: libdv-devel >= 0.103-2m
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: lirc-devel
BuildRequires: mesa-libGL-devel
BuildRequires: ncurses-devel
BuildRequires: openmotif-devel >= 2.2.4-4m
BuildRequires: unzip
BuildRequires: libquicktime-devel

Requires(post,postun): xorg-x11-font-utils
Requires: usermode
Obsoletes: xawtv-radio, xawtv-misc, xawtv-webcam, alevtd
Provides: xawtv-radio, xawtv-misc, xawtv-webcam, alevtd

%description
Xawtv is a simple xaw-based TV program which uses the bttv driver or
video4linux. Xawtv contains various command-line utilities for
grabbing images and .avi movies, for tuning in to TV stations, etc.
Xawtv also includes a grabber driver for vic.

%prep
%setup -q
%patch1 -p1 -b .app-defaults~
./autogen.sh

%build
unset DISPLAY
%configure \
  --enable-xfree-ext \
  --enable-xvideo \
  --enable-lirc \
  --enable-quicktime \
  --enable-motif \
  --enable-aa \
  --enable-alsa
%make 
#LDFLAGS="`pkg-config fontconfig --libs` -ldl"


%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} SUID_ROOT="" install

%{__mkdir_p} %{buildroot}%{_sysconfdir}/pam.d \
             %{buildroot}%{_sysconfdir}/security/console.apps
 
%{__install} -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/pam.d/v4l-conf
echo "SESSION=true" > %{buildroot}%{_sysconfdir}/security/console.apps/v4l-conf
echo "USER=root"   >> %{buildroot}%{_sysconfdir}/security/console.apps/v4l-conf
echo "PROGRAM=%{_sbindir}/v4l-conf" >> %{buildroot}%{_sysconfdir}/security/console.apps/v4l-conf

%{__mkdir_p} %{buildroot}%{_datadir}/applications
%{__install} -m 644 %{SOURCE2} %{buildroot}%{_datadir}/applications
#(
#  cd tv-fonts-%{fontver}
#  %{__mkdir_p} %{buildroot}%{_libdir}/X11/fonts/misc
#  %{__install} -m 0644 *.pcf.gz %{buildroot}%{_libdir}/X11/fonts/misc
#)

# install icons
%{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48}/apps
convert -scale 16x16 contrib/%{name}16x16.xpm %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 contrib/%{name}32x32.xpm %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert -scale 48x48  contrib/%{name}48x48.xpm %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{__mkdir_p} %{buildroot}%{_datadir}/pixmaps
%{__ln_s} ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
rm -rf --preserve-root %{buildroot}

%post
#(
#  cd %{_libdir}/X11/fonts/misc
#  %{_bindir}/mkfontdir > /dev/null 2>&1
#)
 
%postun
#if [ $1 = 0 ]; then
#  (
#    cd %{_libdir}/X11/fonts/misc
#    %{_bindir}/mkfontdir > /dev/null 2>&1
#  )
#fi

%files
%defattr(-,root,root,-)
%doc COPYING Changes MAKEDEV.v4l README* TODO contrib
%config(noreplace) %{_sysconfdir}/pam.d/v4l-conf
%config(noreplace) %{_sysconfdir}/security/console.apps/v4l-conf
%{_bindir}/*
%config %{_datadir}/X11/app-defaults/*
%config %{_datadir}/X11/*/app-defaults/*
#%{_libdir}/X11/fonts/misc
%{_libdir}/xawtv
%{_mandir}/man?/*
%{_mandir}/*/man?/*
%{_datadir}/xawtv
%{_datadir}/applications/xawtv.desktop
%{_datadir}/icons/*/*/*/xawtv.png
%{_datadir}/pixmaps/xawtv.png

%changelog
* Tue Jul 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.101-1m)
- update 3.101

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.95-24m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.95-23m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.95-22m)
- fix build failure

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.95-21m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.95-20m)
- rebuild against libjpeg-8a

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.95-19m)
- fix wrong permission

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.95-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.95-17m)
- rebuild against libjpeg-7

* Sun Sep 13 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.95-16m)
- fix build failure; unset DISPLAY in %%build

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.95-15m)
- rebuild against rpm-4.6

* Mon Apr 21 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.95-14m)
- add patch for kernel-2.6.25
- remove patches and source that had been merged already.

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.95-13m)
- rebuild against gcc43

* Thu Feb  7 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.95-12m)
- fix %%build section 

* Thu Feb  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.95-11m)
- modify patching section for x86_64
- fix %%post and %%postun

* Tue Feb  5 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.95-10m)
- rebuild with libquicktime
-- add BuildRequires: libquicktime-devel
-- add xawtv-3.95-libquicktime.patch
- disable unofficial deinterlace patch, because it is no longer maintained
-- if you want to re-enable the patch, set with_deinterlace to 1

* Tue May 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.95-9m)
- try to fix build on x86_64

* Mon Apr 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.95-8m)
- install icon
- update desktop file

* Mon Apr 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.95-7m)
- move desktop file

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.95-6m)
- BuildRequires: freetype2-devel -> freetype-devel

* Mon Feb 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.95-5m)
- -mtune=generic was deleted from OPT_FLAGS

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.95-4m)
- enable ppc, ppc64

* Mon Nov 13 2006 Masayuki SANO <nosanosai@momonga-linux.org>
- (3.95-3m)
- rebuild against openmotif-2.3.0-beta2

* Sun Apr  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.95-2m)
- add patch9 (app-defaults directory patch)

* Tue Mar 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.95-1m)
- update to 3.95

* Tue Mar 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.92-7m)
- add patch7 and patch8 for xorg-7.0

* Wed Dec  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.92-6m)
- add gcc4 patch
- disable MMX for non ix86 arch

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.92-5m)
- rebuild against SDL-1.2.7-11m

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.92-4m)
- revised docdir permission

* Sun Jan  9 2005 Toru Hoshina <t@momonga-linux.org>
- (3.92-3m)
- force using gcc_3_2 and g++_3_2.
- asm directive for gcc 3.4 is malfunction yet.

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.92-2m)
- rebuild against libdv-0.102

* Tue Apr 27 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.92-1m)
- version 3.92

* Sun Mar 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.91-2m)
- modified Require: usermode

* Sun Feb  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.91-1m)

* Wed Nov 05 2003 Kenta MURATA <muraken2@nifty.com>
- (3.90-1m)
- pretty spec file.

* Tue Oct 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.90-1m)

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.88-2m)
- rebuild against slang(utf8 version)
- use %%{?_smp_mflags}

* Sat Apr 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.88-1m)

* Fri Apr 11 2003 Kenta MURATA <muraken2@nifty.com>
- (3.86-2m)
- BuildPreReq: unzip

* Wed Mar 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.86-1m)

* Sat Feb 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.85-1m)

* Sat Feb  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.83-1m)
- add smooth flt-smooth (http://leute.server.de/peichl/smooth.htm)

* Wed Jan 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.82-4m)
- libpng

* Fri Jan 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.82-3m)
- add BuildPrereq: libdv-devel >= 0.98-1m for libdv.so.2

* Sat Jan 4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.82-2m)
- add Patch3: xawtv-3.82-configure-ac.patch and exec autoconf
  for not use /etc/X11/{,*/}app-defaults/

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.82-1m)

* Tue Dec  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.80-1m)

* Sun Nov 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.79-1m)

* Thu Nov  7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.78-1m)
- enable dv again

* Thu Oct 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.77-1m)
- temporary disable dv...

* Sat Sep  7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.76-2m)
- add deinterlace plugin

* Wed Sep  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.76-1m)

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (3.74-6k)
- mkfontdir tukattendaro korua.

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (3.74-4k)
- rebuild against openmotif-2.2.2-2k.

* Mon Apr 22 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (3.74-2k)
- up to 3.74
- add patch0: xawtv_debug-skip.diff by yosshy@kondara.org

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.54-6k)
- rebuild against libpng 1.2.2.

* Mon Dec 10 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (3.54-4k)
  fix v4l-conf link

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- (3.48-8k)
- nigirisugi

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (3.48-6k)
- rebuild against libpng 1.2.0.

* Sat Jun 23 2001 Toru Hoshina <toru@df-usa.com>
- (3.48-4k)
- rebuild without /etc/X11/app-defaults

* Wed May 23 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.48-2k)
- update to 3.48

* Mon Apr 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.30-8k)
- errased xset exectuin at %post section

* Wed Mar 21 2001 Motonobu Ichimura <famao@kondara.org>
- (3.30-7k)
- change group name to Applications/Multimedia

* Mon Jan 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.30-5k)
- added gnome desktop file

* Mon Jan 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.30-3k)
- Kondarization
