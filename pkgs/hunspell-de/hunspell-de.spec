%global momorel 4

Name: hunspell-de
Summary: German hunspell dictionaries
%define upstreamid 20091006
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://www.j3e.de/ispell/igerman98/dict/igerman98-%{upstreamid}.tar.bz2
Group: Applications/Text
URL: http://www.j3e.de/ispell/igerman98
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2 or GPLv3
BuildArch: noarch
BuildRequires: aspell, hunspell

Requires: hunspell

%description
German (Germany, Switzerland, etc.) hunspell dictionaries.

%prep
%setup -q -n igerman98-%{upstreamid}
sed -i -e "s/AFFIX_EXPANDER = ispell/AFFIX_EXPANDER = aspell/g" Makefile

%build
make hunspell/de_AT.dic hunspell/de_AT.aff \
     hunspell/de_CH.dic hunspell/de_CH.aff \
     hunspell/de_DE.dic hunspell/de_DE.aff
cd hunspell
for i in README_*.txt; do
  if ! iconv -f utf-8 -t utf-8 -o /dev/null $i > /dev/null 2>&1; then
    iconv -f ISO-8859-1 -t UTF-8 $i > $i.new
    touch -r $i $i.new
    mv -f $i.new $i
  fi
  tr -d '\r' < $i > $i.new
  touch -r $i $i.new
  mv -f $i.new $i
done

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cd hunspell
cp -p de_??.dic de_??.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
de_DE_aliases="de_BE de_LU"
for lang in $de_DE_aliases; do
	ln -s de_DE.aff $lang.aff
	ln -s de_DE.dic $lang.dic
done
de_CH_aliases="de_LI"
for lang in $de_CH_aliases; do
	ln -s de_CH.aff $lang.aff
	ln -s de_CH.dic $lang.dic
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc hunspell/README_de_??.txt hunspell/COPYING_OASIS hunspell/COPYING_GPLv2 hunspell/COPYING_GPLv3 hunspell/Copyright
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20091006-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20091006-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20091006-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20091006-1m)
- sync with Fedora 13 (0.20091006-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090107-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090107-1m)
- update to 20090107

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20071211-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20071211-1m)
- import from Fedora to Momonga

* Tue Dec 11 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071211-1
- latest version

* Thu Aug 30 2007 Caolan McNamara <caolanm@redhat.com> - 0.20070829-1
- latest version
- build from canonical source

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 0.20051213-2
- clarify license version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20051213-1
- initial version
