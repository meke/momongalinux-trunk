%global momorel 1

#
# Important for %{ix86}:
# This rpm has to be build on a CPU with sse2 support like Pentium 4 !
#

Summary: A GNU arbitrary precision library
Name: gmp
Version: 5.1.3
Release: %{momorel}m%{?dist}
URL: http://gmplib.org/
Source0: ftp://ftp.gnu.org/pub/gnu/gmp/gmp-%{version}.tar.bz2
NoSource: 0
Source2: gmp.h
Source3: gmp-mparam.h

License: LGPLv3+
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automake16, autoconf, libtool
BuildRequires: perl

%description
The gmp package contains GNU MP, a library for arbitrary precision
arithmetic, signed integers operations, rational numbers and floating
point numbers. GNU MP is designed for speed, for both small and very
large operands. GNU MP is fast because it uses fullwords as the basic
arithmetic type, it uses fast algorithms, it carefully optimizes
assembly code for many CPUs' most common inner loops, and it generally
emphasizes speed over simplicity/elegance in its operations.

Install the gmp package if you need a fast arbitrary precision
library.

%package devel
Summary: Development tools for the GNU MP arbitrary precision library
Group: Development/Libraries
Requires: %{name} = %{version}
Requires(post): info
Requires(preun): info

%description devel
The static libraries, header files and documentation for using the GNU
MP arbitrary precision library in applications.

If you want to develop applications which will use the GNU MP library,
you'll need to install the gmp-devel package.  You'll also need to
install the gmp package.

%package static
Summary: Development tools for the GNU MP arbitrary precision library
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
The static libraries for using the GNU MP arbitrary precision library 
in applications.


%prep
%setup -q

%build
if as --help | grep -q execstack; then
  # the object files do not require an executable stack
  export CCAS="gcc -c -Wa,--noexecstack"
fi
mkdir base
cd base
ln -s ../configure .
./configure --build=%{_build} --host=%{_host} \
         --program-prefix=%{?_program_prefix} \
         --prefix=%{_prefix} \
         --exec-prefix=%{_exec_prefix} \
         --bindir=%{_bindir} \
         --sbindir=%{_sbindir} \
         --sysconfdir=%{_sysconfdir} \
         --datadir=%{_datadir} \
         --includedir=%{_includedir} \
         --libdir=%{_libdir} \
         --libexecdir=%{_libexecdir} \
         --localstatedir=%{_localstatedir} \
         --sharedstatedir=%{_sharedstatedir} \
         --mandir=%{_mandir} \
         --infodir=%{_infodir} \
	 --enable-cxx
perl -pi -e 's|hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=\"-L\\\$libdir\"|g;' libtool
export LD_LIBRARY_PATH=`pwd`/.libs
%{__make} %{?_smp_mflags}
cd ..
%ifarch %{ix86}
mkdir build-sse2
cd build-sse2
ln -s ../configure .
CFLAGS="%{optflags} -march=pentium4"
./configure --build=%{_build} --host=%{_host} \
         --program-prefix=%{?_program_prefix} \
         --prefix=%{_prefix} \
         --exec-prefix=%{_exec_prefix} \
         --bindir=%{_bindir} \
         --sbindir=%{_sbindir} \
         --sysconfdir=%{_sysconfdir} \
         --datadir=%{_datadir} \
         --includedir=%{_includedir} \
         --libdir=%{_libdir} \
         --libexecdir=%{_libexecdir} \
         --localstatedir=%{_localstatedir} \
         --sharedstatedir=%{_sharedstatedir} \
         --mandir=%{_mandir} \
         --infodir=%{_infodir} \
	 --enable-cxx
perl -pi -e 's|hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=\"-L\\\$libdir\"|g;' libtool
export LD_LIBRARY_PATH=`pwd`/.libs
%{__make} %{?_smp_mflags}
unset CFLAGS
cd ..
%endif

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
cd base
export LD_LIBRARY_PATH=`pwd`/.libs
%{makeinstall}
install -m 644 gmp-mparam.h ${RPM_BUILD_ROOT}%{_includedir}
rm -f $RPM_BUILD_ROOT%{_libdir}/lib{gmp,mp,gmpxx}.la
rm -f $RPM_BUILD_ROOT%{_infodir}/dir
/sbin/ldconfig -n $RPM_BUILD_ROOT%{_libdir}
ln -sf libgmpxx.so.4 $RPM_BUILD_ROOT%{_libdir}/libgmpxx.so
cd ..
%ifarch %{ix86}
cd build-sse2
export LD_LIBRARY_PATH=`pwd`/.libs
mkdir $RPM_BUILD_ROOT%{_libdir}/sse2
install -m 755 .libs/libgmp.so.*.* $RPM_BUILD_ROOT%{_libdir}/sse2
cp -a .libs/libgmp.so.[^.]* $RPM_BUILD_ROOT%{_libdir}/sse2
chmod 755 $RPM_BUILD_ROOT%{_libdir}/sse2/libgmp.so.[^.]*
install -m 755 .libs/libgmpxx.so.*.* $RPM_BUILD_ROOT%{_libdir}/sse2
cp -a .libs/libgmpxx.so.? $RPM_BUILD_ROOT%{_libdir}/sse2
chmod 755 $RPM_BUILD_ROOT%{_libdir}/sse2/libgmpxx.so.?
cd ..
%endif

# Rename gmp.h to gmp-<arch>.h and gmp-mparam.h to gmp-mparam-<arch>.h to 
# avoid file conflicts on multilib systems and install wrapper include files
# gmp.h and gmp-mparam-<arch>.h
basearch=%{_arch}
# always use i386 for iX86
%ifarch %{ix86}
basearch=i386
%endif
# Rename files and install wrappers

mv %{buildroot}/%{_includedir}/gmp.h %{buildroot}/%{_includedir}/gmp-${basearch}.h
install -m644 %{SOURCE2} %{buildroot}/%{_includedir}/gmp.h
mv %{buildroot}/%{_includedir}/gmp-mparam.h %{buildroot}/%{_includedir}/gmp-mparam-${basearch}.h
install -m644 %{SOURCE3} %{buildroot}/%{_includedir}/gmp-mparam.h


%check
%ifnarch ppc
cd base
export LD_LIBRARY_PATH=`pwd`/.libs
%{__make} %{?_smp_mflags} check
cd ..
%endif
%ifarch %{ix86}
cd build-sse2
export LD_LIBRARY_PATH=`pwd`/.libs
%{__make} %{?_smp_mflags} check
cd ..
%endif

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/gmp.info %{_infodir}/dir

%preun devel
if [ "$1" = 0 ]; then
	/sbin/install-info --delete %{_infodir}/gmp.info %{_infodir}/dir
fi

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING COPYING.LIB NEWS README
%{_libdir}/libgmp.so.*
%{_libdir}/libgmpxx.so.*
%ifarch %{ix86}
%{_libdir}/sse2/*
%endif

%files devel
%defattr(-,root,root,-)
%doc README
%{_libdir}/libgmp.so
%{_libdir}/libgmpxx.so
%{_includedir}/*.h
%{_infodir}/gmp.info*

%files static
%defattr(-,root,root,-)
%doc README
%{_libdir}/libgmp.a
%{_libdir}/libgmpxx.a

%changelog
* Sat Oct 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1.3-1m)
- update 5.1.3

* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.2-1m)
- update 5.1.2

* Thu Mar 14 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.1.1-1m)
- update 5.1.1

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.5-1m)
- update 5.0.5

* Sat Mar 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.3-1m)
- update 5.0.3

* Mon May  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.2-1m)
- update 5.0.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.1-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.1-2m)
- force rebuild with gmp-5.0.1-1m to resolve dependency

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.0.1-1m)
- update 5.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.2-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.3.2-1m)
- update 4.3.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.1-6m)
- use Requires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.1-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-3m)
- --target option of configure is not used when sse2 build

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-2m)
- --target option of configure is not used

* Mon May 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.3.1-1m)
- update 4.3.1

* Mon May 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-5m)
- fix install-info

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.4-4m)
- define __libtoolize
- fix *.so file's permissions

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-3m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-2m)
- update Patch0 for fuzz=0
- License: LGPLv3+

* Thu Nov 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.4-1m)
- update to 4.2.4

* Mon Apr 07 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.2.2-5m)
- sync with Fedora
- - import Patch0,2,3
- - mpfr is moved to separate package
- - split devel package to static and devel package

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-4m)
- rebuild against gcc43

* Fri Feb  1 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.2-3m)
- rebase to mpfr 2.3.1
- update gmp-mparam.h and gmp.h

* Tue Nov 20 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.2-2m)
- added cumulative patch (2007-10-23)

* Wed Oct  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.2-1m)
- update gmp-4.2.2

* Thu Aug 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.1-2m)
- update mpfr-2.3.0

* Mon Aug 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.1-1m)
- update gmp-4.2.1

* Fri Jan 19 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.4-4m)
- sync with fc-devel (gmp-4_1_4-11)
--* Wed Jan 17 2007 Jakub Jelinek <jakub@redhat.com> 4.1.4-11
--- make sure libmpfr.a doesn't contain SSE2 instructions on i?86 (#222371)
--- rebase to mpfr 2.2.1 from 2.2.0 + cumulative fixes

* Sun Nov 12 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.4-3m)
- copied from trunk(r12649)
- sync with fc-devel (gmp-4_1_4-10)
-- upgrade mpfr to 2.2.0

* Tue Apr 12 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.4-2m)
- enable MPFR

* Sun Nov 28 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.1.4-1m)
- update to 4.1.4

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (4.1.2-1m)
- revised spec for enabling rpm 4.2.

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.1.2-1m)
  update to 4.1.2

* Sun May 26 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (4.0.1-3k)
- add BuildRequires

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (4.0.1-2k)
- ver up.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.1.1-4k)
- /sbin/install-info -> info in PreReq.

* Mon Feb 05 2001 Philipp Knirsch <pknirsch@redhat.de>
- Fixed bugzilla bug #25515 where GMP wouldn't work on IA64 as IA64 is not
correctly identified as a 64 bit platform.

* Mon Dec 18 2000 Preston Brown <pbrown@redhat.com>
- include bsd mp library

* Tue Oct 17 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 3.1.1

* Sun Sep  3 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to 3.1

* Sat Aug 19 2000 Preston Brown <pbrown@redhat.com>
- devel subpackage depends on main package so that .so symlink is OK.

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jun  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- switch to the configure and makeinstall macros
- FHS-compliance fixing
- move docs to non-devel package

* Fri Apr 28 2000 Bill Nottingham <notting@redhat.com>
- libtoolize for ia64

* Fri Apr 28 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to 3.0.1

* Thu Apr 27 2000 Jakub Jelinek <jakub@redhat.com>
- sparc64 fixes for 3.0

* Wed Apr 26 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to 3.0

* Mon Feb 14 2000 Matt Wilson <msw@redhat.com>
- #include <string.h> in files that use string functions

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description and summary

* Mon Dec 06 1999 Michael K. Johnson <johnsonm@redhat.com>
- s/GPL/LGPL/
- build as non-root (#7604)

* Mon Sep 06 1999 Jakub Jelinek <jj@ultra.linux.cz>
- merge in some debian gmp fixes
- Ulrich Drepper's __gmp_scale2 fix
- my mpf_set_q fix
- sparc64 fixes

* Wed Apr 28 1999 Cristian Gafton <gafton@redhat.com>
- add sparc patch for PIC handling

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Thu Feb 11 1999 Michael Johnson <johnsonm@redhat.com>
- include the private header file gmp-mparam.h because several
  apps seem to assume that they are building against the gmp
  source tree and require it.  Sigh.

* Tue Jan 12 1999 Michael K. Johnson <johnsonm@redhat.com>
- libtoolize to work on arm

* Thu Sep 10 1998 Cristian Gafton <gafton@redhat.com>
- yet another touch of the spec file

* Wed Sep  2 1998 Michael Fulbright <msf@redhat.com>
- looked over before inclusion in RH 5.2

* Sat May 24 1998 Dick Porter <dick@cymru.net>
- Patch Makefile.in, not Makefile
- Don't specify i586, let configure decide the arch

* Sat Jan 24 1998 Marc Ewing <marc@redhat.com>
- started with package from Toshio Kuratomi <toshiok@cats.ucsc.edu>
- cleaned up file list
- fixed up install-info support

