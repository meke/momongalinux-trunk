%global src_name gnome_shell___orta_by_half_left-d3f9qp5
%global theme_name orta
%global theme Orta
%global momorel 2


Name:           gnome-shell-theme-%{theme_name}
Version:        1.0
Release:        %{momorel}m%{?dist}
Summary:        The %{theme} gnome-shell theme  
Group:          User Interface/Desktops     

License:        GPLv3
URL:            http://half-left.deviantart.com/art/GNOME-Shell-Orta-207047273
## http://www.deviantart.com/download/207047273/%{src_name}.zip
Source0:        %{src_name}.zip

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       gnome-shell-extension-user-theme
Requires:       gnome-shell >= 3.0.1
BuildArch:      noarch

%description
The %{theme} gnome-shell theme created by half_left

%prep
%setup -q -c 
unzip gs-%{theme_name}.zip
unzip gs-%{theme_name}-dark.zip

%build
# nothing to build

%install
mkdir -p -m755 %{buildroot}/%{_datadir}/themes/%{theme}/gnome-shell
mkdir -p -m755 %{buildroot}/%{_datadir}/themes/%{theme}-dark/gnome-shell

# put the theme files into some data dir
cp -r gs-%{theme_name}/* %{buildroot}/%{_datadir}/themes/%{theme}/.
cp -r gs-%{theme_name}-dark/* %{buildroot}/%{_datadir}/themes/%{theme}-dark/.

# delete backup files (*~)
find %{buildroot} -name *~ -type f -print | xargs /bin/rm -f

# remove LICENSE from BUILDROOT
find %{buildroot} -name LICENSE -type f -print | xargs /bin/rm -f


%files
%doc gs-orta/gnome-shell/LICENSE
%dir %{_datadir}/themes/%{theme}
%dir %{_datadir}/themes/%{theme}-dark
%{_datadir}/themes/%{theme}/*
%{_datadir}/themes/%{theme}-dark/*

%changelog
* Tue Jul 31 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- change Requires from gnome-shell-extensions-user-theme to gnome-shell-extension-user-theme

* Mon Aug 29 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (1.0-1m)
- initial build

* Thu Jun 09 2011 Tim Lauridsen <timlau@fedoraproject.org> 1.0
- initial rpm build
