%global momorel 5
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-dbus
Version:        0.29
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for using D-Bus

Group:          Development/Libraries
License:        LGPLv2
URL:            http://tab.snarc.org/projects/ocaml_dbus/
Source0:        http://tab.snarc.org/download/ocaml/ocaml_dbus-%{version}.tar.bz2
#NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}, ocaml-findlib
BuildRequires:  dbus-devel
BuildRequires:  chrpath


%description
D-Bus is a project that permits programs to communicate with each
other, using a simple IPC protocol.  This is an OCaml binding for
D-Bus.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n ocaml_dbus-%{version}


%build
make

if ! test -f "README"; then
cat > README <<_EOF
OCaml D-BUS bindings version %{version}.

Please see the main website for documentation:
http://tab.snarc.org/projects/ocaml_dbus/
_EOF
fi


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make OCAMLDESTDIR=$OCAMLFIND_DESTDIR install

strip -s $OCAMLFIND_DESTDIR/stublibs/dlldbus_stubs.so
chrpath --delete $OCAMLFIND_DESTDIR/stublibs/dlldbus_stubs.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README
%{_libdir}/ocaml/dbus
%if %opt
%exclude %{_libdir}/ocaml/dbus/*.a
%exclude %{_libdir}/ocaml/dbus/*.cmxa
%exclude %{_libdir}/ocaml/dbus/*.cmx
%endif
%exclude %{_libdir}/ocaml/dbus/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc README THANKS example_avahi.ml
%if %opt
%{_libdir}/ocaml/dbus/*.a
%{_libdir}/ocaml/dbus/*.cmxa
%{_libdir}/ocaml/dbus/*.cmx
%endif
%{_libdir}/ocaml/dbus/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-5m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.29-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.29-1m)
- update to 0.29
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.07-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.07-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.07-1m)
- update to 0.07
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.06-2
- Rebuild for OCaml 3.10.2

* Tue Mar  4 2008 Richard W.M. Jones <rjones@redhat.com> - 0.06-1
- New upstream release 0.06.
- All patches are now upstream.

* Mon Mar  3 2008 Richard W.M. Jones <rjones@redhat.com> - 0.05-1
- New upstream release 0.05.
- Include 'THANKS' file in doc.

* Sat Mar  1 2008 Richard W.M. Jones <rones@redhat.com> - 0.04-2
- Rebuild for ppc64.

* Sat Feb 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.04-1
- New upstream release 0.04.
- Added patches which have gone upstream for Avahi support.
- Added demo Avahi program.

* Tue Jan  8 2008 Richard W.M. Jones <rjones@redhat.com> - 0.03-2
- BR dbus-devel.
- Fix a typo in the description.
- Initial RPM release.
