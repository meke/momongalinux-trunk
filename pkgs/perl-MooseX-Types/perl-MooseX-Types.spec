%global         momorel 2

Name:           perl-MooseX-Types
Version:        0.44
Release:        %{momorel}m%{?dist}
Summary:        Organise your Moose types in libraries
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Types/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/MooseX-Types-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008
BuildRequires:  perl-base
BuildRequires:  perl-Carp
BuildRequires:  perl-Carp-Clan >= 6.00
BuildRequires:  perl-CPAN-Meta-Check >= 0.007
BuildRequires:  perl-IO
BuildRequires:  perl-IPC-Open3
BuildRequires:  perl-lib
BuildRequires:  perl-List-Util >= 1.19
BuildRequires:  perl-Module-Build-Tiny >= 0.030
BuildRequires:  perl-Module-Runtime
BuildRequires:  perl-Moose >= 1.06
BuildRequires:  perl-namespace-autoclean >= 0.08
BuildRequires:  perl-Sub-Exporter
BuildRequires:  perl-Sub-Name
BuildRequires:  perl-Test-CheckDeps >= 0.006
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-More >= 0.94
BuildRequires:  perl-Test-Requires
Requires:       perl-base
Requires:       perl-Carp
Requires:       perl-Carp-Clan >= 6.00
Requires:       perl-List-Util >= 1.19
Requires:       perl-Module-Runtime
Requires:       perl-Moose >= 1.06
Requires:       perl-namespace-autoclean >= 0.08
Requires:       perl-Sub-Exporter
Requires:       perl-Sub-Name
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The type system provided by Moose effectively makes all of its builtin type
global, as are any types you declare with Moose. This means that every
module that declares a type named PositiveInt is sharing the same type
object. This can be a problem when different parts of the code base want to
use the same name for different things.

%prep
%setup -q -n MooseX-Types-%{version}

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install --destdir=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/MooseX/Types*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Sun Dec 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Wed Sep 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Fri Sep 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-2m)
- rebuild against perl-5.18.1

* Tue Jun 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35
- rebuild against perl-5.16.0

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.14.2

* Sun Sep 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-2m)
- rebuild against perl-5.14.1

* Mon Jun  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-2m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.24-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- rebuild against perl-5.12.0

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.10.1

* Tue Jun 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-2m)
- BR: perl-Moose >= 0.83 since dependency cheking fails

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Mon Jun 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-2m)
- modify BuildRequires

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
