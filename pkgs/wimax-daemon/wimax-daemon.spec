%global momorel 1

%global gitdate 20110419
%global gitrev 6718941c

%define pkgname daemon
%define rpmname daemon
%define tarname wimax
%define udevrulesdir /lib/udev

Name:       %{tarname}-%{pkgname}
Summary:    WiMAX Network Service for the Intel 2400m
Version:    1.5.2
Release:    %{?gitdate:%{gitdate}git%{gitrev}}.%{momorel}m%{dist}
Group:      System Environment/Base
License:    BSD
URL:        http://linuxwimax.org/
# To recreate the tarball, do "./make-git-snapshot.sh %{gitrev}"
%if 0%{?gitdate}
Source0:    %{tarname}-%{gitdate}.tar.bz2
Source1:    make-git-snapshot.sh
Source2:    commitid
%else       ## dead link
Source0: http://linuxwimax.org/Download?action=AttachFile&do=get&target=%{tarname}-1.5.1.tar.gz
%endif
# Clean up manpage syntax
Patch1: wimax-1.5.2-noise.patch
# Don't handle dhclient ourselves
Patch2: wimax-1.5.2-iprenew.patch
BuildRequires: wimax-tools-devel
BuildRequires: zlib-devel
BuildRequires: libeap-devel
BuildRequires: chrpath
ExcludeArch: s390 s390x

%description
User space daemon for the Intel 2400m Wireless WiMAX Link.
This daemon takes care of handling network scan, discovery and
management.

%package libs
Summary:    Libraries for WiMAX network service
Group:      System Environment/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description libs
Runtime libraries for the WiMAX network service.

%package devel
Summary:    Development files for WiMAX Low Level Tools
Group:      Development/Libraries
Requires:   %{name}-libs = %{version}-%{release}
Requires:   pkgconfig

%description devel
Header files and libraries needed to link to the WiMAX network service.

%prep
%setup -q -n %{tarname}-%{gitdate}
%patch1 -p1 -b .man-noise
%patch2 -p1 -b .renew

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

find %{buildroot} -name "*.a" -exec rm -f {} \;
find %{buildroot} -name "*.la" -exec rm -f {} \;

chrpath --delete %{buildroot}%{_bindir}/%{tarname}d

mv %{buildroot}/etc/logrotate.d/%{tarname}.conf %{buildroot}/etc/logrotate.d/%{tarname}

mkdir -p %{buildroot}/%{udevrulesdir}
mv %{buildroot}/etc/udev/rules.d %{buildroot}/%{udevrulesdir}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README.txt CHANGELOG LICENSE INSTALL
%config %{_sysconfdir}/logrotate.d/%{tarname}
%config %{_sysconfdir}/modprobe.d/i2400m.conf
%config %{_sysconfdir}/%{tarname}/config.xml
/%{udevrulesdir}/rules.d/iwmxsdk.rules
%{_bindir}/%{tarname}cu
%{_bindir}/%{tarname}d
%{_bindir}/%{tarname}_monitor
%{_mandir}/man1/*
%{_datadir}/%{tarname}
%dir %{_localstatedir}/lib/%{tarname}
%{_localstatedir}/lib/%{tarname}/*

%files libs
%defattr(-,root,root,-)
%{_libdir}/lib*.so.0*

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{tarname}
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Wed Sep 05 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.5.2-20110419git6718941c-m1)
- Initial momonga release, imported from f17

* Tue Apr 19 2011 Bill Nottingham <notting@redhat.com> - 1.5.2-2.20110419git6718941c
- update to upstream tagged 1.5.2
- fix assorted issues from package review
- don't handle dhclient ourselves

* Tue Mar 22 2011 Bill Nottingham <notting@redhat.com> - 1.5.1-1.20110322gitbefcae11
- initial packaging
