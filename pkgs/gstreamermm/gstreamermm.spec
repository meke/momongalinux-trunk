%global momorel 2
%global majorminor 0.10

Name: gstreamermm
Version: 0.10.10
Release: %{momorel}m%{?dist}
Summary: GStreamer C++ binding

Group: Applications/Multimedia
License: LGPL
URL: http://gstreamer.freedesktop.org/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.10/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gstreamer
BuildRequires: glibmm-devel >= 2.28.2
BuildRequires: libxml++-devel
BuildRequires: gtkmm-devel
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-good

%description
gstreamermm provides C++ bindings for the GStreamer streaming
multimedia library (http://gstreamer.freedesktop.org).  With
gstreamermm it is possible to develop applications that work with
multimedia in C++.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --disable-static \
    MMDOCTOOLDIR=%{_datadir}/mm-common/doctool \
	CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/%{name}-%{majorminor}
%{_libdir}/lib%{name}-%{majorminor}.so.*
%{_libdir}/lib%{name}_get_plugin_defs-%{majorminor}.so.*
%exclude %{_libdir}/lib%{name}-%{majorminor}.la
%exclude %{_libdir}/lib%{name}_get_plugin_defs-%{majorminor}.la

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}-%{majorminor}
%{_libdir}/pkgconfig/%{name}-%{majorminor}.pc
%{_libdir}/lib%{name}-%{majorminor}.so
%{_libdir}/lib%{name}_get_plugin_defs-%{majorminor}.so

%{_datadir}/doc/%{name}-%{majorminor}
%{_datadir}/devhelp/books/%{name}-%{majorminor}

%changelog
* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-2m)
- build fix

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-1m)
- update to 0.10.10

* Mon Jul  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.8-4m)
- rebuild with glibmm-2.28.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.8-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.8-2m)
- rebuild for new GCC 4.5

* Sat Oct 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8
- delete patch1 (merged)

* Sun Oct  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.7.3-3m)
- add patch for gcc45

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.7.3-2m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7.3-1m)
- update to 0.10.7.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.6-2m)
- use BuildRequires

* Tue Dec 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- initial build
