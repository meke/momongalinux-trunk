%global         momorel 5

Name:           perl-HTML-TreeBuilder-LibXML
Version:        0.23
Release:        %{momorel}m%{?dist}
Summary:        HTML::TreeBuilder and XPath compatible interface with libxml
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/HTML-TreeBuilder-LibXML/
Source0:        http://www.cpan.org/authors/id/C/CA/CAFEGRATZ/HTML-TreeBuilder-LibXML-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008001
BuildRequires:  perl-HTML-TreeBuilder-XPath >= 0.14
BuildRequires:  perl-libwww-perl >= 6
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.98
BuildRequires:  perl-URI
BuildRequires:  perl-Web-Scraper
BuildRequires:  perl-XML-LibXML >= 1.7
Requires:       perl-HTML-TreeBuilder-XPath >= 0.14
Requires:       perl-libwww-perl >= 6
Requires:       perl-URI
Requires:       perl-Web-Scraper
Requires:       perl-XML-LibXML >= 1.7
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
HTML::TreeBuilder::XPath is libxml based compatible interface to
HTML::TreeBuilder, which could be slow for a large document.

%prep
%setup -q -n HTML-TreeBuilder-LibXML-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes cpanfile LICENSE META.json README.md
%{perl_vendorlib}/HTML/TreeBuilder/LibXML*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-2m)
- rebuild against perl-5.18.0

* Fri May 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Mon May 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17
- rebuild against perl-5.16.0

* Wed Nov 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sun Nov 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- updatee to 0.14

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-2m)
- rebuild against perl-5.14.2

* Wed Sep 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-2m)
- rebuild against perl-5.10.1

* Tue Aug 18 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.09-1m)
- up to 0.09

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
