%global momorel 2


Summary: startup-notification
Name: startup-notification
Version: 0.12
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.gnome.org/

Source0: http://www.freedesktop.org/software/%{name}/releases/%{name}-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libICE-devel, libSM-devel, libX11-devel xcb-util-devel >= 0.3.9

%description
startup-notification

%package devel
Summary: startup-notification-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libICE-devel, libSM-devel, libX11-devel

%description devel
startup-notification-devel

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%{_libdir}/*a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/startup-notification-1.0

%changelog
* Mon Aug 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-2m)
- rebuild for xcb-util

* Tue Jun 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-5m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-4m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9-5m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-2m)
- %%NoSource -> NoSource

* Fri Mar 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-3m)
- delete libtool library

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-2m)
- delete autoreconf and make check

* Tue Nov 15 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-1m)
- version up
- GNOME 2.12.1 Desktop

* Sat Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.7-1m)
- version 0.7
- GNOME 2.8 Desktop

* Tue Oct 28 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5-1m)
- pretty spec file.

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5-1m)
- version 0.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4-1m)
- version 0.4

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3-1m)
- version 0.3
