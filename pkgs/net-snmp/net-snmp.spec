%global momorel 2

# use netsnmp_tcp_wrappers 0 to disable tcp_wrappers support
%{!?netsnmp_tcp_wrappers:%global netsnmp_tcp_wrappers 1}
# use nestnmp_check 0 to speed up packaging by disabling 'make test'
%{!?netsnmp_check: %global netsnmp_check 1}

# allow compilation on Fedora 11 and older
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
# Arches on which we need to prevent arch conflicts on net-snmp-config.h
%define multilib_arches %{ix86} ia64 ppc ppc64 s390 s390x x86_64 sparc sparcv9 sparc64

Summary: A collection of SNMP protocol tools and libraries
Name: net-snmp
Version: 5.7.2.1
Release: %{momorel}m%{?dist}
Epoch: 0

License: Modified BSD
Group: System Environment/Daemons
URL: http://net-snmp.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/net-snmp/net-snmp-%{version}.tar.gz
NoSource: 0
Source1: net-snmp.redhat.conf
Source2: net-snmpd.init
Source3: net-snmptrapd.init
Source4: net-snmp-config.h
Source5: net-snmp-config
Source6: net-snmp-trapd.redhat.conf
Source7: net-snmpd.sysconfig
Source8: net-snmptrapd.sysconfig
Source9: net-snmp-tmpfs.conf
Source10: snmpd.service
Source11: snmptrapd.service

Patch1: net-snmp-5.7.2-pie.patch
Patch2: net-snmp-5.5-dir-fix.patch
Patch3: net-snmp-5.6-multilib.patch
Patch4: net-snmp-5.5-apsl-copying.patch
Patch5: net-snmp-5.6-test-debug.patch
Patch6: net-snmp-5.7.2-systemd.patch
Patch7: net-snmp-5.7.2-python-ipaddress-size.patch
Patch8: net-snmp-5.7.2-create-user-multilib.patch
Patch9: net-snmp-5.7.2-autoreconf.patch
Patch10: net-snmp-5.7.2-btrfs.patch
Patch11: net-snmp-5.7-agentx-crash.patch
Patch12: net-snmp-5.5-agentx-disconnect-crash.patch

Requires(post): chkconfig
Requires(preun): chkconfig
# for /sbin/service
Requires(preun): initscripts
# for /bin/rm
Requires(preun): coreutils
Requires: %{name}-libs = %{epoch}:%{version}-%{release}
Requires: mysql-libs
# This is actually needed for the %triggerun script but Requires(triggerun)
# is not valid.  We can use %post because this particular %triggerun script
# should fire just after this package is installed.
Requires(post): systemd-sysv

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel >= 1.0.0, bzip2-devel, elfutils-devel
BuildRequires: libselinux-devel, elfutils-libelf-devel, rpm-devel
BuildRequires: perl-devel, perl-ExtUtils-Embed, gawk, procps
BuildRequires: python-devel >= 2.7 , python-setuptools
BuildRequires: chrpath
BuildRequires: mysql-devel >= 5.5.10
# for netstat, needed by 'make test'
BuildRequires: net-tools
# for make test
BuildRequires: perl(TAP::Harness)
BuildRequires: systemd-units
%ifnarch s390 s390x
BuildRequires: lm_sensors-devel >= 3
%endif
%if %{netsnmp_tcp_wrappers}
BuildRequires: tcp_wrappers-devel
%endif

%description
SNMP (Simple Network Management Protocol) is a protocol used for
network management. The NET-SNMP project includes various SNMP tools:
an extensible agent, an SNMP library, tools for requesting or setting
information from SNMP agents, tools for generating and handling SNMP
traps, a version of the netstat command which uses SNMP, and a Tk/Perl
mib browser. This package contains the snmpd and snmptrapd daemons,
documentation, etc.

You will probably also want to install the net-snmp-utils package,
which contains NET-SNMP utilities.

%package utils
Group: Applications/System
Summary: Network management utilities using SNMP, from the NET-SNMP project
Requires: %{name}-libs = %{epoch}:%{version}-%{release}

%description utils
The net-snmp-utils package contains various utilities for use with the
NET-SNMP network management project.

Install this package if you need utilities for managing your network
using the SNMP protocol. You will also need to install the net-snmp
package.

%package devel
Group: Development/Libraries
Summary: The development environment for the NET-SNMP project
Requires: %{name}-libs = %{epoch}:%{version}-%{release}
Requires: elfutils-devel, rpm-devel, elfutils-libelf-devel, openssl-devel
%if %{netsnmp_tcp_wrappers}
Requires: tcp_wrappers-devel
%endif
%ifnarch s390 s390x
Requires: lm_sensors-devel
%endif

%description devel
The net-snmp-devel package contains the development libraries and
header files for use with the NET-SNMP project's network management
tools.

Install the net-snmp-devel package if you would like to develop
applications for use with the NET-SNMP project's network management
tools. You'll also need to have the net-snmp and net-snmp-utils
packages installed.

%package perl
Group: Development/Libraries
Summary: The perl NET-SNMP module and the mib2c tool
Requires: %{name}-libs = %{epoch}:%{version}-%{release}, perl
BuildRequires: perl

%description perl
The net-snmp-perl package contains the perl files to use SNMP from within
Perl.

Install the net-snmp-perl package, if you want to use mib2c or SNMP 
with perl.

%package gui
Group: Applications/System
Summary: An interactive graphical MIB browser for SNMP
Requires: perl-Tk, net-snmp-perl = %{epoch}:%{version}-%{release}

%description gui
The net-snmp-gui package contains tkmib utility, which is a graphical user 
interface for browsing the Message Information Bases (MIBs). It is also 
capable of sending or retrieving the SNMP management information to/from 
the remote agents interactively.

Install the net-snmp-gui package, if you want to use this interactive utility.

%package libs
Group: Development/Libraries
Summary: The NET-SNMP runtime client libraries
# the libs link against libperl.so:
Requires:  perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description libs
The net-snmp-libs package contains the runtime client libraries for shared
binaries and applications.

%package agent-libs
Group: Development/Libraries
Summary: The NET-SNMP runtime agent libraries
# the libs link against libperl.so:
Requires:  perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description agent-libs
The net-snmp-agent-libs package contains the runtime agent libraries for shared
binaries and applications.

%package python
Group: Development/Libraries
Summary: The Python 'netsnmp' module for the Net-SNMP
Requires: %{name}-libs = %{epoch}:%{version}-%{release}

%description python
The 'netsnmp' module provides a full featured, tri-lingual SNMP (SNMPv3, 
SNMPv2c, SNMPv1) client API. The 'netsnmp' module internals rely on the
Net-SNMP toolkit library.

%package sysvinit
Group: System Environment/Daemons
Summary: Legacy SysV init scripts for Net-SNMP daemons
Requires: %{name} = %{epoch}:%{version}-%{release}

%description sysvinit
The net-snmp-sysvinit package provides SysV init scripts for Net-SNMP daemons.

%prep
%setup -q

%ifnarch ia64
%patch1 -p1 -b .pie
%endif

%patch2 -p1 -b .dir-fix
%patch3 -p1 -b .multilib
%patch4 -p1 -b .apsl
%patch5 -p1
%patch6 -p1 -b .systemd
%patch7 -p1 -b .ipaddress-size
%patch8 -p1 -b .multilib
%patch9 -p1 -b .autoreconf
%patch10 -p1 -b .btrfs
%patch11 -p1 -b .agentx-crash
%patch12 -p1 -b .agentx-disconnect-crash

%ifarch sparc64 s390 s390x
# disable failing test - see https://bugzilla.redhat.com/show_bug.cgi?id=680697
rm testing/fulltests/default/T200*
%endif

%build
MIBS="host agentx smux \
     ucd-snmp/diskio tcp-mib udp-mib mibII/mta_sendmail \
     ip-mib/ipv4InterfaceTable ip-mib/ipv6InterfaceTable \
     ip-mib/ipAddressPrefixTable/ipAddressPrefixTable \
     ip-mib/ipDefaultRouterTable/ipDefaultRouterTable \
     ip-mib/ipv6ScopeZoneIndexTable ip-mib/ipIfStatsTable \
     sctp-mib rmon-mib etherlike-mib"

%ifnarch s390 s390x
# there are no lm_sensors on s390
MIBS="$MIBS ucd-snmp/lmsensorsMib"
%endif

autoreconf -vif
%configure \
    --disable-static --enable-shared \
    --with-cflags="$RPM_OPT_FLAGS -D_RPM_4_4_COMPAT" \
    --with-sys-location="Unknown" \
    --with-logfile="/var/log/snmpd.log" \
    --with-persistent-directory="/var/lib/net-snmp" \
    --with-mib-modules="$MIBS" \
%if %{netsnmp_tcp_wrappers}
    --with-libwrap=yes \
%endif
    --sysconfdir=%{_sysconfdir} \
    --enable-ipv6 \
    --enable-ucd-snmp-compatibility \
    --with-openssl \
    --with-pic \
    --enable-embedded-perl \
    --enable-as-needed \
    --with-perl-modules="INSTALLDIRS=vendor" \
    --enable-mfd-rewrites \
    --enable-local-smux \
    --with-temp-file-pattern=/var/run/net-snmp/snmp-tmp-XXXXXX \
    --with-transports="DTLSUDP TLSTCP" \
    --with-security-modules=tsm  \
    --with-mysql \
    --with-systemd \
    --with-sys-contact="root@localhost" <<EOF
EOF

# store original libtool file, we will need it later
cp libtool libtool.orig
# remove rpath from libtool
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# the package is not %_smp_mflags safe
make

# remove rpath from compiled perl libs
find perl/blib -type f -name "*.so" -print -exec chrpath --delete {} \;

# compile python module
pushd python
%{__python} setup.py --basedir="../" build
popd


%install
rm -rf ${RPM_BUILD_ROOT}
make install DESTDIR=${RPM_BUILD_ROOT}

# Determine which arch net-snmp-config.h is going to try to #include.
basearch=%{_arch}
%ifarch %{ix86}
basearch=i386
%endif

%ifarch %{multilib_arches}
# Do an net-snmp-config.h switcheroo to avoid file conflicts on systems where you
# can have both a 32- and 64-bit version of the library, as they each need
# their own correct-but-different versions of net-snmp-config.h to be usable.
mv ${RPM_BUILD_ROOT}/%{_bindir}/net-snmp-config ${RPM_BUILD_ROOT}/%{_bindir}/net-snmp-config-${basearch}
install -m 755 %SOURCE5 ${RPM_BUILD_ROOT}/%{_bindir}/net-snmp-config
mv ${RPM_BUILD_ROOT}/%{_includedir}/net-snmp/net-snmp-config.h ${RPM_BUILD_ROOT}/%{_includedir}/net-snmp/net-snmp-config-${basearch}.h
install -m644 %SOURCE4 ${RPM_BUILD_ROOT}/%{_includedir}/net-snmp/net-snmp-config.h
%endif

install -d ${RPM_BUILD_ROOT}%{_sysconfdir}/snmp
install -m 644 %SOURCE1 ${RPM_BUILD_ROOT}%{_sysconfdir}/snmp/snmpd.conf
install -m 644 %SOURCE6 ${RPM_BUILD_ROOT}%{_sysconfdir}/snmp/snmptrapd.conf

install -d ${RPM_BUILD_ROOT}%{_initscriptdir}
install -m 755 %SOURCE2 ${RPM_BUILD_ROOT}%{_initscriptdir}/snmpd
install -m 755 %SOURCE3 ${RPM_BUILD_ROOT}%{_initscriptdir}/snmptrapd

install -d ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig
install -m 644 %SOURCE7 ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/snmpd
install -m 644 %SOURCE8 ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/snmptrapd

# prepare /var/lib/net-snmp
install -d ${RPM_BUILD_ROOT}%{_localstatedir}/lib/net-snmp
install -d ${RPM_BUILD_ROOT}%{_localstatedir}/run/net-snmp

# remove things we don't want to distribute
rm -f ${RPM_BUILD_ROOT}%{_bindir}/snmpinform
ln -s snmptrap ${RPM_BUILD_ROOT}/usr/bin/snmpinform
rm -f ${RPM_BUILD_ROOT}%{_bindir}/snmpcheck
rm -f ${RPM_BUILD_ROOT}/%{_bindir}/fixproc
rm -f ${RPM_BUILD_ROOT}/%{_mandir}/man1/fixproc*
rm -f ${RPM_BUILD_ROOT}/%{_bindir}/ipf-mod.pl
rm -f ${RPM_BUILD_ROOT}/%{_libdir}/*.la
rm -f ${RPM_BUILD_ROOT}/%{_libdir}/libsnmp*

# remove special perl files
find $RPM_BUILD_ROOT -name perllocal.pod \
    -o -name .packlist \
    -o -name "*.bs" \
    -o -name Makefile.subs.pl \
    | xargs -ri rm -f {}
# remove docs that do not apply to Linux
rm -f README.aix README.hpux11 README.osX README.Panasonic_AM3X.txt README.solaris README.win32

# copy missing mib2c.conf files
install -m 644 local/mib2c.*.conf ${RPM_BUILD_ROOT}%{_datadir}/snmp

# install python module
pushd python
%{__python} setup.py --basedir=.. install -O1 --skip-build --root $RPM_BUILD_ROOT 
popd

find $RPM_BUILD_ROOT -name '*.so' | xargs chmod 0755

# trim down massive ChangeLog
dd bs=1024 count=250 if=ChangeLog of=ChangeLog.trimmed

# convert files to UTF-8
for file in README COPYING; do
    iconv -f 8859_1 -t UTF-8 <$file >$file.utf8
    mv $file.utf8 $file
done

# remove executable bit from documentation samples
chmod 644 local/passtest local/ipf-mod.pl

# dirty hack for #603243, until it's fixed properly upstream
install -m 755 -d $RPM_BUILD_ROOT/usr/include/net-snmp/agent/util_funcs
install -m 644  agent/mibgroup/util_funcs/*.h $RPM_BUILD_ROOT/usr/include/net-snmp/agent/util_funcs

# systemd stuff
install -m 755 -d $RPM_BUILD_ROOT/%{_sysconfdir}/tmpfiles.d/

install -m 644 %SOURCE9 $RPM_BUILD_ROOT/%{_sysconfdir}/tmpfiles.d/net-snmp.conf
install -m 755 -d $RPM_BUILD_ROOT/%{_unitdir}
install -m 644 %SOURCE10 %SOURCE11 $RPM_BUILD_ROOT/%{_unitdir}/

%check
%if %{netsnmp_check}
# restore libtool, for unknown reason it does not work with the one without rpath
cp -f libtool.orig libtool
# temporary workaround to make test "extending agent functionality with pass" working
chmod 755 local/passtest

LD_LIBRARY_PATH=${RPM_BUILD_ROOT}/%{_libdir} make test
%endif


%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 = 0 ]; then
    /bin/systemctl --no-reload disable snmpd.service > /dev/null 2>&1 || :
    /bin/systemctl --no-reload disable snmptrapd.service > /dev/null 2>&1 || :
    /bin/systemctl stop snmpd.service > /dev/null 2>&1 || :
    /bin/systemctl stop snmptrapd.service > /dev/null 2>&1 || :
fi


%postun
if [ "$1" -ge "1" ]; then
    /bin/systemctl try-restart snmpd.service >/dev/null 2>&1 || :
    /bin/systemctl try-restart snmptrapd.service >/dev/null 2>&1 || :
fi


%triggerun -- net-snmp < 5.7-2
# Convert SysV -> systemd.
# Save the current service runlevel info,
# User must manually run systemd-sysv-convert --apply snmpd
# to migrate them to systemd targets
echo "hello world" >> /tmp/snmp
echo date >>/tmp/snmp
/usr/bin/systemd-sysv-convert --save snmpd >/dev/null 2>&1 ||:
/usr/bin/systemd-sysv-convert --save snmptrapd >/dev/null 2>&1 ||:
/sbin/chkconfig --del snmpd >/dev/null 2>&1 || :
/sbin/chkconfig --del snmptrapd >/dev/null 2>&1 || :
/bin/systemctl try-restart snmpd.service >/dev/null 2>&1 || :
/bin/systemctl try-restart snmptrapd.service >/dev/null 2>&1 || :

%triggerpostun -n net-snmp-sysvinit -- net-snmp < 5.7-2
/sbin/chkconfig --add snmpd >/dev/null 2>&1 || :
/sbin/chkconfig --add snmptrapd >/dev/null 2>&1 || :

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post agent-libs -p /sbin/ldconfig

%postun agent-libs -p /sbin/ldconfig

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog.trimmed EXAMPLE.conf FAQ NEWS TODO
%doc README README.agent-mibs README.agentx README.krb5 README.snmpv3
%doc local/passtest local/ipf-mod.pl
%doc README.thread AGENT.txt PORTING local/README.mib2c README.systemd
%dir %{_sysconfdir}/snmp
%config(noreplace,missingok) %{_sysconfdir}/snmp/snmpd.conf
%config(noreplace,missingok) %{_sysconfdir}/snmp/snmptrapd.conf
%{_bindir}/snmpconf
%{_bindir}/agentxtrap
%{_bindir}/net-snmp-create-v3-user
%{_sbindir}/*
%attr(0644,root,root) %{_mandir}/man[58]/snmp*d*
%attr(0644,root,root) %{_mandir}/man5/snmp_config.5.*
#%%attr(0644,root,root) %{_mandir}/man5/variables.5.*
%attr(0644,root,root) %{_mandir}/man1/net-snmp-create-v3-user*
#%%attr(0644,root,root) %{_mandir}/man1/snmpconf.1.*
#%%dir %{_datadir}/snmp
%{_datadir}/snmp/snmpconf-data
%dir %{_localstatedir}/lib/net-snmp
%dir %{_localstatedir}/run/net-snmp
%config(noreplace) %{_sysconfdir}/tmpfiles.d/net-snmp.conf
%{_unitdir}/snmp*

%files utils
%defattr(-,root,root,-)
%{_bindir}/encode_keychange
%{_bindir}/snmp[^c-]*
%attr(0644,root,root) %{_mandir}/man1/snmp[^-]*.1*
%attr(0644,root,root) %{_mandir}/man1/encode_keychange*.1*
%attr(0644,root,root) %{_mandir}/man1/agentxtrap.1*
%attr(0644,root,root) %{_mandir}/man5/snmp.conf.5.*
%attr(0644,root,root) %{_mandir}/man5/variables.5.*


%files devel
%defattr(0644,root,root,0755)
%{_libdir}/lib*.so
/usr/include/*
%attr(0644,root,root) %{_mandir}/man3/*.3.*
%attr(0755,root,root) %{_bindir}/net-snmp-config*
%attr(0644,root,root) %{_mandir}/man1/net-snmp-config*.1.*

%files perl
%defattr(-,root,root)
%{_bindir}/mib2c-update
%{_bindir}/mib2c
%{_bindir}/snmp-bridge-mib
%{_bindir}/net-snmp-cert
#%%dir %{_datadir}/snmp
%{_datadir}/snmp/mib2c*
%{_datadir}/snmp/*.pl
%{_bindir}/traptoemail
%attr(0644,root,root) %{_mandir}/man[15]/mib2c*
%attr(0644,root,root) %{_mandir}/man3/*.3pm.*
%attr(0644,root,root) %{_mandir}/man1/traptoemail*.1.*
%attr(0644,root,root) %{_mandir}/man1/snmp-bridge-mib.1*
%{perl_vendorarch}/*SNMP*
%{perl_vendorarch}/auto/*SNMP*
%{perl_vendorarch}/auto/Bundle/*SNMP*

%files python
%defattr(-,root,root,-)
%doc README
%{python_sitearch}/*

%files gui
%defattr(-,root,root)
%{_bindir}/tkmib
%attr(0644,root,root) %{_mandir}/man1/tkmib.1.*

%files libs
%defattr(-,root,root)
%doc COPYING README ChangeLog.trimmed FAQ NEWS TODO
%{_libdir}/libnetsnmp.so.*
%dir %{_datadir}/snmp
%dir %{_datadir}/snmp/mibs
%{_datadir}/snmp/mibs/*

%files agent-libs
%defattr(-,root,root)
%{_libdir}/libnetsnmpagent*.so.*
%{_libdir}/libnetsnmphelpers*.so.*
%{_libdir}/libnetsnmpmibs*.so.*
%{_libdir}/libnetsnmptrapd*.so.*

%files sysvinit
%defattr(-,root,root)
%{_initscriptdir}/snmpd
%{_initscriptdir}/snmptrapd
%config(noreplace,missingok) %{_sysconfdir}/sysconfig/snmpd
%config(noreplace,missingok) %{_sysconfdir}/sysconfig/snmptrapd

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.2.1-2m)
- rebuild against perl-5.20.0

* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.2.1-1m)
- [SECURITY] CVE-2014-2284 CVE-2014-2285
- update to 5.7.2.1

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.1-2m)
- rebuild against perl-5.16.0

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.7.1-1m)
- update 5.7.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7-4m)
- rebuild against perl-5.14.2

* Sat Sep 10 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.7-3m)
- fix %%files to avoid conflicting

* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-2m)
- support systemd
- add tmpfile conf.

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.7-1m)
- update 5.7

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6.1-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6.1-4m)
- rebuild against perl-5.14.0-0.2.1m

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.6.1-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6.1-2m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6.1-1m)
- update to 5.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.5-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.5-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5-4m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5-3m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.5-2m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.5-1m)
- sync with Fedora 13 (5.5-12)

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.2.1-8m)
- rebuild against rpm-4.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.2.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.2.1-6m)
- rebuild against perl-5.10.1

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.2.1-5m)
- remove duplicate directories

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.4.2.1-4m)
- modify %%files to avoid conflicting

* Tue May  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.4.2.1-3m)
- use %%global instead of %%define to set correct %%{_includedir}

* Mon May  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.4.2.1-2m)
- use %%{python_sitearch} to fix build on x86_64

* Mon May  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.2.1-1m)
- [SECURITY] CVE-2008-6123
- sync with Fedora 11 (5.4.2.1-10)

* Sat Apr 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.1.1-6m)
- rebuild against rpm-4.7

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.1.1-5m)
- rebuild against openssl-0.9.8k

* Sat Dec 27 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.1.1-4m)
- rebuild against rpm-4.6
-- apply Patch23
-- update Patch1,3,6,9,16,21 for fuzz=0
- License: Modified BSD

* Thu Nov  6 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.4.1.1-3m)
- [SECURITY] CVE-2008-4309 CVE-2008-2292
-- import Patch19,22 from Fedora 9 updates
- merge following changes
-- * Tue Jul 22 2008 Jan Safranek <jsafranek@redhat.com> 5.4.1-19
-- - fix perl SNMP::Session::set (#452131)
-- - support interface names longer than 8 characters (#468045)

* Sat Oct 18 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (5.4.1.1-2m)
- added BuildPreReq perl-ExtUtils-Embed

* Wed Jun 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.1.1-1m)
- [SECURITY] CVE-2008-0960
- update to 5.4.1.1
 
* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.1-5m)
- rebuild against openssl-0.9.8h-1m

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.4.1-4m)
- rebuild against lm_sensors-3.0.2
- modify %%configure section
- import sensors3.patch and xen-crash.patch from Fedora
- add alpha support to net-snmp-config.h

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.4.1-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.4.1-2m)
- rebuild against perl-5.10.0-1m

* Fri Nov  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.1-1m)
- [SECURITY] CVE-2007-5846
- update to 5.4.1, sync with Fedora devel

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.4-2m)
- back to Momonga net-snmp.momonga.conf

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.4-1m)
- update to 5.4 (sync Fedora)

* Mon Nov  6 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.3.1.0-4m)
- rebuild against lm_sensor

* Thu Aug 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.3.1.0-3m)
- revise perl.lst

* Tue Aug 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.3.1.0-2m)
- modify install dir

* Wed Aug  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.3.1.0-1m)
- import from fc-devel

* Wed Jun  7 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2.1.2-6m)
- fix files list (%%{_includedir})

* Tue May 23 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.2.1.2-5m)
- rebuild against rpm-4.4.2

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.2.1.2-4m)
- rebuild against openssl-0.9.8a

* Tue Nov 1 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.2.1.2-3m)
- do not remove /etc/snmp/snmpd.conf when package is uninstalled.
- install required /var/net-snmp dir

* Mon Sep 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (5.2.1.2-2m)
- revise a logrotate script

* Mon Jul 18 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.2.1.2-1m)
- up to 5.2.1.2
- sync with FC cvs
- [SECURITY] CAN-2005-1740 CAN-2005-2177

* Sat Mar 12 2005 Toru Hoshina <t@momonga-linux.org>
- (5.1.2-1m)
- ver up. sync with FC3(5.1.2-11).

* Sat Aug  7 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.0.9-6m)
- rebuild against rpm-4.3

* Thu Jun  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.0.9-5m)
- rebuild against rpm-4.2

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (5.0.9-4m)
- revised spec for enabling rpm 4.2.

* Tue Oct 7 2003 Ysuo Ohgaki <yohgaki@momonga-linux.org>
- (5.0.9-3m)
- fix include file installation path

* Sat Oct 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.9-2m)
- add BuildPrereq: bzip2-devel for link -lbz2 snmpd

* Fri Sep 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.0.9-1m)
- security fix
- change licence (BSDish -> BSD)

* Fri May  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.0.8-2m)
- fix permission of '/usr/bin/net-snmp-config'

* Fri May  9 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.0.8-1m)
- major bugfixes
- move '/usr/bin/net-snmp-config' to 'net-snmp-devel' package

* Mon Mar 10 2003 Shingo Akagaki <droa@kitty.dnsalias.org>
- (5.0.7-4m)
- yarisugi...

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (5.0.7-3m)
  rebuild against openssl 0.9.7a

* Sun Mar 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.7-2m)
- add URL tag

* Wed Jan 22 2003 Tadataka Yoshikawa <yosikawa@momonga-linux.org>
- (5.0.7-1m)
- version 5.0.7.
- replace patch12 for version 5.0.7.
- remove patch14 because it already applied.

* Wed Dec 25 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (5.0.6-1m)
- upgrade and import patch from rawhide

* Wed Aug 21 2002 Kikutani Makoto <poo@momonga-linux.org>
- (5.0.4-5m)
- revive net-snmp-5.0.1-initializer.patch

* Wed Aug 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.0.4-4m)
- apply net-snmp-5.0.3-compatible.patch for compatibility

* Fri Aug 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.0.3-3m)
- 'Provides: ucd-snmp-*' in sub packages
- never define '__libtoolize' as '/bin/true'

* Fri Aug 16 2002 Junichiro Kita <kita@momonga-linux.org>
- (5.0.3-2m)
- /etc/rc.d/init.d -> /etc/init.d (_initscriptdir macro)

* Thu Aug 15 2002 Kikutani Makoto <poo@momonga-linux.org>
- (5.0.3-1m)
- 1st Momonga version
- Provides: ucd-snmp

* Thu Jun 27 2002 Phil Knirsch <pknirsch@redhat.com> 5.0.1-5
- Added --enable-ucd-snmp-compatibility for compatibility with older version
  and fixed installation thereof.
- Got rid of the perl(Tk) dependancy by removing snmpcheck.
- Include /usr/include/ucd-snmp in the filelist.
- Fixed a problem with the ucd-snmp/version.h file.

* Wed Jun 26 2002 Phil Knirsch <pknirsch@redhat.com> 5.0.1-1
- Updated to 5.0.1
- Dropped --enable-reentrant as it's currently broken

* Tue Apr 23 2002 Phil Knirsch <pknirsch@redhat.com> 5.0-1
- Switch to latest stable version, 5.0
- Renamed the packate to net-snmp and obsoleted ucd-snmp.

* Wed Apr 17 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.4-3
- Fixed problem with reload in initscript (#63526).

* Mon Apr 15 2002 Tim Powers <timp@redhat.com> 4.2.4-2
- rebuilt in new environment

* Mon Apr 15 2002 Tim Powers <timp@redhat.com> 4.2.4-1
- update to 4.2.4 final

* Sat Apr 13 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.4.pre3-5
- Added some missing files to the %files section.

* Tue Apr 09 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.4.pre3-4
- Hardcoded the ETC_MNTTAB to point to "/etc/mtab".

* Mon Apr 08 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.4.pre3-3
- Removed the check for dbFOO as we don't want to add another requirement.

* Fri Apr 05 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.4.pre3-2
- Added missing BuildPrereq to openssl-devel (#61525)

* Thu Apr 04 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.4.pre3-1
- Added ucd5820stat to the files section.
- Updated to latest version (4.2.4.pre3)

* Mon Mar 18 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.4.pre2-1
- Updated to latest version (4.2.4.pre2)

* Tue Jan 29 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.3-4
- Added the snmptrapd init script as per request (#49205)
- Fixed the again broken rpm query stuff (#57444)
- Removed all old and none-used db related stuff (libs and header checks/files)

* Mon Jan 07 2002 Phil Knirsch <pknirsch@redhat.com> 4.2.3-2
- Included the Axioma Security Research fix for snmpnetstat from bugtraq.

* Mon Dec 03 2001 Phil Knirsch <phil@redhat.de> 4.2.3-1
- Update to 4.2.3 final.
- Fixed libtool/rpath buildroot pollution problem.
- Fixed library naming problem.

* Fri Oct  5 2001 Philipp Knirsch <pknirsch@redhat.de>
- Fixed a server segfault for snmpset operation (#53640). Thanks to Josh Giles
  and Wes Hardaker for the patch.

* Mon Sep 10 2001 Philipp Knirsch <pknirsch@redhat.de>
- Fixed problem with RUNTESTS script.

* Tue Sep  4 2001 Preston Brown <pbrown@redhat.com>
- fixed patch related to bug #35016 (Dell)

* Fri Aug 24 2001 Philipp Knirsch <pknirsch@redhat.de> 4.2.1-6
- Fixed snmpd description (#52366)

* Wed Aug 22 2001 Philipp Knirsch <pknirsch@redhat.de>
- Final bcm5820 fix. Last one was broken.
- Fixed bugzilla bug (#51960) where the binaries contained rpath references.

* Wed Aug 15 2001 Philipp Knirsch <pknirsch@redhat.de>
- Fixed a couple of security issues:
  o /tmp race and setgroups() privilege problem
  o Various buffer overflow and format string issues.
  o One signedness problem in ASN handling.
- Fixed an important RFE to support bcm5820 cards. (#51125)

* Fri Jul 20 2001 Philipp Knirsch <pknirsch@redhat.de>
- Removed tkmib from the package once again as we don't ship the Tk.pm CPAN
  perl module required to run it (#49363)
- Added missing Provides for the .so.0 libraries as rpm doesn't seem to find
  those during the build anymore (it used to) (#46388)

* Thu Jul 19 2001 Philipp Knirsch <pknirsch@redhat.de>
- Enabled IPv6 support (RFE #47764)
- Hopefully final fix of snmpwalk problem (#42153). Thanks to Douglas Warzecha
  for the patch and Matt Domsch for reporting the problem.

* Tue Jun 26 2001 Philipp Knirsch <pknirsch@redhat.de>
- Fixed smux compilation problems (#41452)
- Fixed wrong paths displayed in manpages (#43053)

* Mon Jun 25 2001 Philipp Knirsch <pknirsch@redhat.de>
- Updated to 4.2.1. Removed 2 obsolete patches (fromcvs and #18153)
- Include /usr/share/snmp/snmpconf in %files

* Wed Jun 13 2001 Than Ngo <than@redhat.com>
- fix to use libwrap in distro
- add buildprereq: tcp_wrappers

* Fri Jun  1 2001 Bill Nottingham <notting@redhat.com>
- add a *new* patch for IP address return sizes

* Fri Apr 20 2001 Bill Nottingham <notting@redhat.com>
- add patch so that only four bytes are returned for IP addresses on ia64 (#32244)

* Wed Apr 11 2001 Bill Nottingham <notting@redhat.com>
- rebuild (missing alpha packages)

* Fri Apr  6 2001 Matt Wilson <msw@redhat.com>
- added ucd-snmp-4.2-null.patch to correcly handle a NULL value (#35016)

* Tue Apr  3 2001 Preston Brown <pbrown@redhat.com>
- clean up deinstallation (#34168)

* Tue Mar 27 2001 Matt Wilson <msw@redhat.com>
- return a usable RETVAL when running "service snmpd status" (#33571)

* Tue Mar 13 2001 Matt Wilson <msw@redhat.com>
- configure with --enable-reentrant and added "smux" and "agentx" to
  --with-mib-modules= argument (#29626)

* Fri Mar  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Mon Feb 26 2001 Tim Powers <timp@redhat.com>
- fixed initscript, for reload and restart it was start then stop,
  fixed. (#28477)

* Fri Feb  2 2001 Trond Eivind Glomsrod <teg@redhat.com>
- i18nize initscript

* Sat Jan  6 2001 Jeff Johnson <jbj@redhat.com>
- don't depend on /etc/init.d so that package will work with 6.2.
- perl path fiddles no longer needed.
- rely on brp-compress frpm rpm to compress man pages.
- patch from ucd-snmp CVS (Wes Hardaker).
- configure.in needs to check for rpm libraries correctly (#23033).
- add simple logrotate script (#21399).
- add options to create pidfile and log with syslog with addresses (#23476).

* Sat Dec 30 2000 Jeff Johnson <jbj@redhat.com>
- package for Red Hat 7.1.

* Thu Dec 07 2000 Wes Hardaker <hardaker@users.sourceforge.net>
- update for 4.2

* Thu Oct 12 2000 Jeff Johnson <jbj@redhat.com>
- add explicit format for syslog call (#18153).

* Thu Jul 20 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Thu Jul 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild per Trond's request.

* Tue Jul 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix syntax error that crept in with condrestart

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul 10 2000 Preston Brown <pbrown@redhat.com>
- move initscript and add condrestart magic

* Sat Jun 17 2000 Bill Nottingham <notting@redhat.com>
- fix %%attr on man pages

* Mon Jun 12 2000 Jeff Johnson <jbj@redhat.com>
- tkmib doco had #!/usr/bin/perl55
- include snmpcheck and tkmib again (still needs some CPAN module, however).

* Tue Jun  6 2000 Jeff Johnson <jbj@redhat.com>
- update to 4.1.2.
- FHS packaging.
- patch for rpm 4.0.

* Thu May 18 2000 Trond Eivind Glomsrod <teg@redhat.com>
- add version to buildroot
- rebuilt with new libraries

* Sun Feb 27 2000 Jeff Johnson <jbj@redhat.com>
- default config was broken (from Wes Hardaker) (#9752)

* Sun Feb 13 2000 Jeff Johnson <jbj@redhat.com>
- compressed man pages.

* Fri Feb 11 2000 Wes Hardaker <wjhardaker@ucdavis.edu>
- update to 4.1.1

* Sat Feb  5 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- change %postun to %preun

* Thu Feb 3 2000 Elliot Lee <sopwith@redhat.com>
- Don't ship tkmib, since we don't ship the perl modules needed to run it.
(Bug #4881)

* Tue Aug 31 1999 Jeff Johnson <jbj@redhat.com>
- default config permits RO access to system group only (Wed Hardaker).

* Sun Aug 29 1999 Jeff Johnson <jbj@redhat.com>
- implement suggestions from Wes Hardaker.

* Fri Aug 27 1999 Jeff Johnson <jbj@redhat.com>
- stateless access to rpm database.

* Wed Aug 25 1999 Jeff Johnson <jbj@redhat.com>
- update to 4.0.1.

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Sat Jun 12 1999 Jeff Johnson <jbj@redhat.com>
- update to 3.6.2 (#3219,#3259).
- add missing man pages (#3057).

* Thu Apr  8 1999 Wes Hardaker <wjhardaker@ucdavis.edu>
- fix Source0 location.
- fix the snmpd.conf file to use real community names.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Fri Mar 19 1999 Preston Brown <pbrown@redhat.com>
- upgrade to 3.6.1, fix configuration file stuff.

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Tue Feb  2 1999 Jeff Johnson <jbj@redhat.com>
- restore host resources mib
- simplified config file
- rebuild for 6.0.

* Tue Dec 22 1998 Bill Nottingham <notting@redhat.com>
- remove backup file to fix perl dependencies

* Tue Dec  8 1998 Jeff Johnson <jbj@redhat.com>
- add all relevant rpm scalars to host resources mib.

* Sun Dec  6 1998 Jeff Johnson <jbj@redhat.com>
- enable libwrap (#253)
- enable host module (rpm queries over SNMP!).

* Mon Oct 12 1998 Cristian Gafton <gafton@redhat.com>
- strip binaries

* Fri Oct  2 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.5.3.
- don't include snmpcheck until perl-SNMP is packaged.

* Thu Aug 13 1998 Jeff Johnson <jbj@redhat.com>
- ucd-snmpd.init: start daemon w/o -f.

* Tue Aug  4 1998 Jeff Johnson <jbj@redhat.com>
- don't start snmpd unless requested
- start snmpd after pcmcia.

* Sun Jun 21 1998 Jeff Johnson <jbj@redhat.com>
- all but config (especially SNMPv2p) ready for prime time

* Sat Jun 20 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.5.

* Tue Dec 30 1997 Otto Hammersmith <otto@redhat.com>
- created the package... possibly replace cmu-snmp with this.
