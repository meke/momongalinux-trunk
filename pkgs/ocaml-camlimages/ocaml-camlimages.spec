%global momorel 6
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}
%global _default_patch_fuzz 2

Name:           ocaml-camlimages
Version:        3.0.2
Release:        %{momorel}m%{?dist}
Summary:        OCaml image processing library

Group:          Development/Libraries
License:        "LGPLv2 with exceptions"
URL:            http://cristal.inria.fr/camlimages/eng.html
Source0:        http://cristal.inria.fr/camlimages/camlimages-%{version}.tar.gz
NoSource:       0
Source1:        camlimages-2.2.0-htmlref.tar.gz
Patch0:         camlimages-3.0.2-display-module.patch
Patch2:         camlimages-oversized-tiff-check-CVE-2009-3296.patch
Patch3:         camlimages-3.0.2-ocaml-autoconf.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-lablgtk-devel >= 2.14.2
BuildRequires:  ocaml-x11
BuildRequires:  lablgtk, libpng-devel, libjpeg-devel >= 8a
BuildRequires:  libXpm-devel, ghostscript-devel, freetype-devel
BuildRequires:  giflib-devel
BuildRequires:  libtiff-devel
BuildRequires:  gtk2-devel
BuildRequires:  libtool, automake, autoconf
BuildRequires:  ocaml-autoconf

%global __ocaml_requires_opts -i Image_intf

%description
CamlImages is an image processing library for Objective CAML, which provides:
basic functions for image processing and loading/saving, various image file 
formats (hence providing a translation facility from format to format), 
and an interface with the Caml graphics library allows to display images 
in the Graphics module screen and to mix them with Caml drawings

In addition, the library can handle huge images that cannot be (or can hardly
be) stored into the main memory (the library then automatically creates swap
files and escapes them to reduce the memory usage).


%package        devel
Summary:        Development files for camlimages
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release} 


%description    devel
The camlimages-devel package provides libraries and headers for 
developing applications using camlimages

Includes documentation provided by ocamldoc


%prep
%setup -q -n camlimages-%{version} -a 1

# Gdk.Display submodule clashes with the Display module in
# the examples/liv directory, so rename it:
%patch0 -p1
%patch2 -p1
%patch3 -p1
aclocal -I .
automake
autoconf
mv examples/liv/display.ml examples/liv/livdisplay.ml


%build
%configure

# Hack to fix RHBZ#564798.  It's completely unclear why this fails
# in Koji when it works perfectly well for me locally.
echo image_intf.cmi: image_intf.mli >> src/.depend
echo mylazy.cmi: mylazy.mli >> examples/liv/.depend

make

%install
rm -rf $RPM_BUILD_ROOT
make install ocamlsitelibdir=%{_libdir}/ocaml/camlimages DESTDIR=$RPM_BUILD_ROOT

strip $RPM_BUILD_ROOT%{_libdir}/ocaml/stublibs/dllcamlimages.so \
  $RPM_BUILD_ROOT%{_libdir}/ocaml/stublibs/dllcamlimages_core.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc INSTALL README
%{_libdir}/ocaml/camlimages
%{_libdir}/ocaml/stublibs/*.so
%if %opt
%exclude %{_libdir}/ocaml/camlimages/*.a
%exclude %{_libdir}/ocaml/camlimages/*.cmxa
%endif
%exclude %{_libdir}/ocaml/camlimages/*.mli


%files devel
%defattr(-,root,root,-)
%doc doc/*.{html,jpg}
%if %opt
%{_libdir}/ocaml/camlimages/*.a
%{_libdir}/ocaml/camlimages/*.cmxa
%endif
%{_libdir}/ocaml/camlimages/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-6m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.2-3m)
- full rebuild for mo7 release

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.2-2m)
- rebuild against ocaml-lablgtk-2.14.1

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2
- rebuild against ocaml-3.11.2
- rebuild against ocaml-lablgtk-2.14.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-9m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-7m)
- [SECURITY] CVE-2009-3296
-- https://bugzilla.redhat.com/show_bug.cgi?id=509531
-- https://bugzilla.redhat.com/show_bug.cgi?id=528732
-- https://bugs.gentoo.org/show_bug.cgi?id=276235

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-6m)
- rebuild against libjpeg-7

* Wed Aug  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-5m)
- [SECURITY] CVE-2009-2660
- import a security patch (Patch1) from Gentoo

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-4m)
- [SECURITY] CVE-2009-2295
- import a security patch from Fedora 11 (3.0.1-7.fc11.2)

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-3m)
- rebuild against ocaml-lablgtk-2.12.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-1m)
- sync with Fedora devel (3.0.1-3)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 2.2.0-11
- Rebuild for OCaml 3.10.2

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> 2.2.0-10
- Rebuild for ppc64.

* Wed Feb 13 2008 Richard W.M. Jones <rjones@redhat.com> 2.2.0-9
- Rebuild for OCaml 3.10.1
- Fix paths to conform to packaging policy.

* Wed May 09 2007 Nigel Jones <dev@nigelj.com> 2.2.0-8
- Exclude ppc64 builds due to missing ocaml

* Fri May 04 2007 Nigel Jones <dev@nigelj.com> 2.2.0-7
- Change to Makefile patch to move .so files to stublibs
- Rename to ocaml-camlimages
- Other changes per review

* Thu May 03 2007 Nigel Jones <dev@nigelj.com> 2.2.0-6
- Include .*a files just to make sure

* Thu May 03 2007 Nigel Jones <dev@nigelj.com> 2.2.0-5
- Revert -4 changes
- Remove excludedirs patch, replace with a sed
- Provide html documentation generated from running ocaml-ocamldoc

* Thu Apr 26 2007 Nigel Jones <dev@nigelj.com> 2.2.0-4
- Add Provides: camlimages-static, and LICENSE to -devel docs

* Thu Apr 12 2007 Nigel Jones <dev@nigelj.com> 2.2.0-3
- Remove .a & .o files

* Wed Apr 11 2007 Nigel Jones <dev@nigelj.com> 2.2.0-2
- Add missing dependencies

* Tue Apr 10 2007 Nigel Jones <dev@nigelj.com> 2.2.0-1
- Initial spec file
