%global momorel 1
%global pyver 2.7
%global python3 0

Summary: D-BUS message bus python
Name: dbus-python
Version: 1.1.1
Release: %{momorel}m%{?dist}
URL: http://www.freedesktop.org/Software/dbus
Source0: http://dbus.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0

Patch0: dbus-python-aarch64.patch
# http://cgit.freedesktop.org/dbus/dbus-python/commit/?id=423ee853dfbb4ee9ed89a21e1cf2b6a928e2fc4d
Patch1: dbus-python-pygobject38.patch
Patch2: 0001-Move-python-modules-to-architecture-specific-directo.patch

License: "AFL/GPL"
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: python >= %{pyver}
BuildRequires: dbus-devel >= 1.0.2
BuildRequires: dbus-glib-devel >= 0.96
%if 0%{?python3}
BuildRequires: python3-devel
%endif

%description
A core concept of the D-BUS implementation is that "libdbus" is
intended to be a low-level API, similar to Xlib. Most programmers are
intended to use the bindings to GLib, Qt, Python, Mono, Java, or
whatever. These bindings have varying levels of completeness.

%package devel
Summary: dbus-python devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
dbus-python-devel

%package -n python3-dbus
Summary: D-Bus bindings for python3
%description -n python3-dbus
%{summary}.

%prep
%setup -q 
%patch0 -p1
%patch1 -p1
%patch2 -p1

autoreconf -vfi

%build
%global _configure ../configure

mkdir python2-build; pushd python2-build
%configure PYTHON=%{__python}
make %{?_smp_mflags}
popd

%if 0%{?python3}
mkdir python3-build; pushd python3-build
%configure PYTHON=%{__python3}
make %{?_smp_mflags}
popd
%endif


%install
%if 0%{?python3}
make install DESTDIR=$RPM_BUILD_ROOT -C python3-build
%endif

make install DESTDIR=$RPM_BUILD_ROOT -C python2-build

# unpackaged files
rm -fv $RPM_BUILD_ROOT%{python_sitearch}/*.la
rm -fv $RPM_BUILD_ROOT%{python3_sitearch}/*.la
rm -rfv $RPM_BUILD_ROOT%{_datadir}/doc/dbus-python/


%check
# FIXME: seeing failures on f19+, http://bugzilla.redhat.com/913936
#make check -k -C python2-build
%if 0%{?python3}
#make check -k -C python3-build
%endif

%files
%doc COPYING ChangeLog README NEWS
%{python_sitearch}/*.so
%{python_sitearch}/dbus/

%files devel
%doc doc/API_CHANGES.txt doc/HACKING.txt doc/tutorial.txt
%{_includedir}/dbus-1.0/dbus/dbus-python.h
%{_libdir}/pkgconfig/dbus-python.pc

%if 0%{?python3}
%files -n python3-dbus
%{python3_sitearch}/*.so
%{python3_sitearch}/dbus/
%endif

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.84.0-1m)
- update to 0.84.0

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.83.2-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.83.2-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.83.2-1m)
- update to 0.83.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.83.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.83.1-2m)
- full rebuild for mo7 release

* Thu Jul  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.83.1-1m)
- update to 0.83.1

* Wed May 12 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.83.0-6m)
- add patch0 Bug 21172 https://bugs.freedesktop.org/show_bug.cgi?id=21172

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.83.0-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.83.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.83.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.83.0-2m)
- rebuild against python-2.6.1-1m

* Fri Sep  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.83.0-1m)
- update 0.83.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.82.4-2m)
- rebuild against gcc43

* Sun Feb 24 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.82.4-1m)
- update 0.82.4

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.82.3-2m)
- %%NoSource -> NoSource

* Tue Oct 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.82.3-1m)
- update to 0.82.3

* Fri Jun 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.81.1-1m)
- update to 0.81.1

* Fri Mar  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.80.2-1m)
- update to 0.80.2

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.80.1-2m)
- delete pyc pyo

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.80.1-1m)
- update to 0.80.1

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.71-2m)
- rebuild against python-2.5

* Thu Aug 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.71-1m)
- initila build
