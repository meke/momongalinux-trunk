%global momorel 1
%define libauditver 1.0.6
%define pango_version 1.2.0
%define gtk3_version 2.99.2
%define libglade2_version 2.0.0
%define libgnomeui_version 2.2.0
%define scrollkeeper_version 0.3.4
%define pam_version 0.99.8.1-11
%define desktop_file_utils_version 0.2.90
%define gail_version 1.2.0
%define nss_version 3.11.1
%define fontconfig_version 2.6.0
%define _default_patch_fuzz 999

Summary: The GNOME Display Manager
Name: gdm
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: User Interface/X
URL: http://download.gnome.org/sources/gdm
#VCS: git:git://git.gnome.org/gdm
Source: http://download.gnome.org/sources/gdm/3.6/gdm-%{version}.tar.xz
NoSource: 0
Source1: org.gnome.login-screen.gschema.override
#Patch1: gdm-upstream.patch

Requires(pre): /usr/sbin/useradd

Requires: pam >= 0:%{pam_version}
Requires: /sbin/nologin
Requires: system-logos
Requires: xorg-x11-server-utils
Requires: xorg-x11-setxkbmap
Requires: xorg-x11-xinit
Requires: systemd >= 188-3m
Requires: accountsservice >= 0.6.24
Requires: gnome-settings-daemon >= 2.21.92
Requires: iso-codes
Requires: gnome-session
Requires: polkit-gnome >= 0.105-2m
Requires: ConsoleKit-x11
# since we use it, and pam spams the log if the module is missing
Requires: gnome-keyring-pam
Requires: pulseaudio-gdm-hooks
# We need 1.0.4-5 since it lets us use "localhost" in auth cookies
Requires: libXau >= 1.0.4-4
# RH #746693: gdm's fallback session specifies metacity as the WM
# and refuses to run if metacity is not present
Requires: metacity
BuildRequires: pkgconfig(libcanberra-gtk)
BuildRequires: scrollkeeper >= 0:%{scrollkeeper_version}
BuildRequires: pango-devel >= 0:%{pango_version}
BuildRequires: gtk3-devel >= 0:%{gtk3_version}
BuildRequires: pam-devel >= 0:%{pam_version}
BuildRequires: fontconfig >= 0:%{fontconfig_version}
BuildRequires: desktop-file-utils >= %{desktop_file_utils_version}
BuildRequires: libtool automake autoconf
BuildRequires: libattr-devel
BuildRequires: gettext
BuildRequires: gnome-doc-utils
BuildRequires: libdmx-devel
BuildRequires: audit-libs-devel >= %{libauditver}
BuildRequires: gobject-introspection-devel
BuildRequires: autoconf automake libtool
BuildRequires: intltool
%ifnarch s390 s390x ppc64
BuildRequires: xorg-x11-server-Xorg
%endif
BuildRequires: nss-devel >= %{nss_version}
BuildRequires: libselinux-devel
BuildRequires: check-devel
BuildRequires: iso-codes-devel
BuildRequires: libxklavier-devel >= 4.0
BuildRequires: upower-devel >= 0.9.7
BuildRequires: libXdmcp-devel
BuildRequires: dbus-glib-devel
BuildRequires: GConf2-devel
#BuildRequires: pkgconfig(accountsservice) >= 0.6.3
BuildRequires: accountsservice-devel >= 0.6.22
#BuildRequires: pkgconfig(libsystemd-login)
#BuildRequires: pkgconfig(libsystemd-daemon)
BuildRequires: pkgconfig(ply-boot-client)
BuildRequires: systemd-devel >= 188-2m

Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

# these are all just for rewriting gdm.d/00-upstream-settings
Requires(posttrans): dconf

Provides: service(graphical-login) = %{name}

Requires: audit-libs >= %{libauditver}

# Swallow up old fingerprint/smartcard plugins
Obsoletes: gdm-plugin-smartcard < 3.2.1
Provides: gdm-plugin-smartcard = %{version}-%{release}

Obsoletes: gdm-plugin-fingerprint < 3.2.1
Provides: gdm-plugin-fingerprint = %{version}-%{release}

%package libs
Summary: Client-side library to talk to gdm
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description libs
The gdm-libs package contains libraries that can
be used for writing custom greeters.

%package devel
Summary: Development files for gdm-libs
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}

%description devel
The gdm-devel package contains headers and other
files needed to build custom greeters.

%description
GDM provides the graphical login screen, shown shortly after boot up,
log out, and when user-switching.

%description devel
Development files and headers for writing GDM greeters.

%prep
%setup -q

# import upstream fixes and changes
#patch1 -p1 -b .upstream~
#autopoint --force
#AUTOPOINT='intltoolize --automake --copy' autoreconf --force --install --verbose

%build

%configure --with-pam-prefix=%{_sysconfdir} \
           --enable-split-authentication \
           --enable-profiling      \
           --enable-console-helper \
           --with-plymouth \
           --with-selinux \
           --with-systemd

# drop unneeded direct library deps with --as-needed
# libtool doesn't make this easy, so we do it the hard way
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' -e 's/    if test "$export_dynamic" = yes && test -n "$export_dynamic_flag_spec"; then/      func_append compile_command " -Wl,-O1,--as-needed"\n      func_append finalize_command " -Wl,-O1,--as-needed"\n\0/' libtool

%make 


%install
mkdir -p %{buildroot}%{_sysconfdir}/gdm/Init
mkdir -p %{buildroot}%{_sysconfdir}/gdm/PreSession
mkdir -p %{buildroot}%{_sysconfdir}/gdm/PostSession

make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_sysconfdir}/pam.d/gdm

# add logo to shell greeter
cp %{SOURCE1} %{buildroot}%{_datadir}/glib-2.0/schemas

# gets rebuilt in posttrans
rm -f %{buildroot}%{_sysconfdir}/dconf/db/gdm

# docs go elsewhere
rm -rf %{buildroot}/%{_prefix}/doc

# create log dir
mkdir -p %{buildroot}/var/log/gdm

# remove the gdm Xsession as we're using the xdm one
rm -f %{buildroot}%{_sysconfdir}/gdm/Xsession
(cd %{buildroot}%{_sysconfdir}/gdm; ln -sf ../X11/xinit/Xsession .)

rm -f %{buildroot}%{_libdir}/gtk-2.0/modules/*.a
rm -f %{buildroot}%{_libdir}/gtk-2.0/modules/*.la

mkdir -p %{buildroot}%{_datadir}/gdm/autostart/LoginWindow

mkdir -p %{buildroot}%{_localstatedir}/gdm/greeter

rm -rf %{buildroot}%{_localstatedir}/scrollkeeper

find %{buildroot} -name '*.a' -delete
find %{buildroot} -name '*.la' -delete

%find_lang gdm --with-gnome

%pre
/usr/sbin/useradd -M -u 42 -d /var/lib/gdm -s /sbin/nologin -r gdm > /dev/null 2>&1
/usr/sbin/usermod -d /var/lib/gdm -s /sbin/nologin gdm >/dev/null 2>&1
# ignore errors, as we can't disambiguate between gdm already existed
# and couldn't create account with the current adduser.
exit 0

%post
/sbin/ldconfig
touch --no-create /usr/share/icons/hicolor >&/dev/null || :

# if the user already has a config file, then migrate it to the new
# location; rpm will ensure that old file will be renamed

custom=/etc/gdm/custom.conf

if [ $1 -ge 2 ] ; then
    if [ -f /usr/share/gdm/config/gdm.conf-custom ]; then
        oldconffile=/usr/share/gdm/config/gdm.conf-custom
    elif [ -f /etc/X11/gdm/gdm.conf ]; then
        oldconffile=/etc/X11/gdm/gdm.conf
    fi

    # Comment out some entries from the custom config file that may
    # have changed locations in the update.  Also move various
    # elements to their new locations.

    [ -n "$oldconffile" ] && sed \
    -e 's@^command=/usr/X11R6/bin/X@#command=/usr/bin/Xorg@' \
    -e 's@^Xnest=/usr/X11R6/bin/Xnest@#Xnest=/usr/X11R6/bin/Xnest@' \
    -e 's@^BaseXsession=/etc/X11/xdm/Xsession@#BaseXsession=/etc/X11/xinit/Xsession@' \
    -e 's@^BaseXsession=/etc/X11/gdm/Xsession@#&@' \
    -e 's@^BaseXsession=/etc/gdm/Xsession@#&@' \
    -e 's@^Greeter=/usr/bin/gdmgreeter@#Greeter=/usr/libexec/gdmgreeter@' \
    -e 's@^RemoteGreeter=/usr/bin/gdmlogin@#RemoteGreeter=/usr/libexec/gdmlogin@' \
    -e 's@^GraphicalTheme=Bluecurve@#&@' \
    -e 's@^BackgroundColor=#20305a@#&@' \
    -e 's@^DefaultPath=/usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin@#&@' \
    -e 's@^RootPath=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/X11R6/bin@#&@' \
    -e 's@^HostImageDir=/usr/share/hosts/@#HostImageDir=/usr/share/pixmaps/faces/@' \
    -e 's@^LogDir=/var/log/gdm@#&@' \
    -e 's@^PostLoginScriptDir=/etc/X11/gdm/PostLogin@#&@' \
    -e 's@^PreLoginScriptDir=/etc/X11/gdm/PreLogin@#&@' \
    -e 's@^PreSessionScriptDir=/etc/X11/gdm/PreSession@#&@' \
    -e 's@^PostSessionScriptDir=/etc/X11/gdm/PostSession@#&@' \
    -e 's@^DisplayInitDir=/var/run/gdm.pid@#&@' \
    -e 's@^RebootCommand=/sbin/reboot;/sbin/shutdown -r now;/usr/sbin/shutdown -r now;/usr/bin/reboot@#&@' \
    -e 's@^HaltCommand=/sbin/poweroff;/sbin/shutdown -h now;/usr/sbin/shutdown -h now;/usr/bin/poweroff@#&@' \
    -e 's@^ServAuthDir=/var/gdm@#&@' \
    -e 's@^Greeter=/usr/bin/gdmlogin@Greeter=/usr/libexec/gdmlogin@' \
    -e 's@^RemoteGreeter=/usr/bin/gdmgreeter@RemoteGreeter=/usr/libexec/gdmgreeter@' \
    $oldconffile > $custom
fi

if [ $1 -ge 2 -a -f $custom ] && grep -q /etc/X11/gdm $custom ; then
   sed -i -e 's@/etc/X11/gdm@/etc/gdm@g' $custom
fi

#systemd_post gdm.service

[ -L /etc/systemd/system/display-manager.service ] || rm -f /etc/systemd/system/display-manager.service
%{_sbindir}/update-alternatives --install /etc/systemd/system/display-manager.service \
    display-manager.service %{_unitdir}/gdm.service 90

%preun
%gconf_schema_remove gdm-simple-greeter
#systemd_preun gdm.service

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

if [ $1 -eq 0 ] ; then
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

#systemd_postun

[ -e %{_unitdir}/gdm.service ] || %{_sbindir}/update-alternatives --remove display-manager.service \
    %{_unitdir}/gdm.service

%posttrans
dconf update
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files -f gdm.lang
%doc AUTHORS COPYING NEWS README TODO

%dir %{_sysconfdir}/gdm
%config(noreplace) %{_sysconfdir}/gdm/custom.conf
%config %{_sysconfdir}/gdm/Init/*
%config %{_sysconfdir}/gdm/PostLogin/*
%config %{_sysconfdir}/gdm/PreSession/*
%config %{_sysconfdir}/gdm/PostSession/*
%config %{_sysconfdir}/pam.d/gdm-autologin
%config %{_sysconfdir}/pam.d/gdm-password
# not config files5B
%{_sysconfdir}/gdm/Xsession
%{_datadir}/gdm/gdm.schemas
%{_sysconfdir}/dbus-1/system.d/gdm.conf
%dir %{_sysconfdir}/gdm/Init
%dir %{_sysconfdir}/gdm/PreSession
%dir %{_sysconfdir}/gdm/PostSession
%dir %{_sysconfdir}/gdm/PostLogin
%{_datadir}/pixmaps/*.png
%{_datadir}/glib-2.0/schemas/org.gnome.login-screen.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.login-screen.gschema.override
%{_datadir}/gdm/simple-greeter/extensions/unified/page.ui
%{_libexecdir}/gdm-host-chooser
%{_libexecdir}/gdm-session-worker
%{_libexecdir}/gdm-simple-chooser
%{_libexecdir}/gdm-simple-greeter
%{_libexecdir}/gdm-simple-slave
%{_libexecdir}/gdm-xdmcp-chooser-slave
%{_sbindir}/gdm
%{_sbindir}/gdm-binary
%{_bindir}/gdm-screenshot
%{_bindir}/gdmflexiserver
%{_datadir}/gdm/greeter/applications/*
%{_datadir}/gdm/greeter/autostart/*
%{_datadir}/gdm/*.ui
%{_datadir}/gdm/locale.alias
%{_datadir}/gnome-session/sessions/*
%{_datadir}/gdm/gdb-cmd
%{_libexecdir}/gdm-crash-logger
%{_libdir}/libgdm*.so*
%dir %{_libdir}/gdm
%dir %{_libdir}/gdm/simple-greeter
%dir %{_libdir}/gdm/simple-greeter/extensions
%{_libdir}/gdm/simple-greeter/extensions/libpassword.so
%dir %{_datadir}/gdm/simple-greeter
%dir %{_datadir}/gdm/simple-greeter/extensions
%dir %{_datadir}/gdm/simple-greeter/extensions/password
%{_datadir}/gdm/simple-greeter/extensions/password/page.ui
%dir %{_datadir}/gdm
%dir %{_datadir}/gdm/greeter
%dir %{_datadir}/gdm/greeter/applications
%dir %{_localstatedir}/log/gdm
%attr(1770, gdm, gdm) %dir %{_localstatedir}/lib/gdm
%attr(1755, gdm, gdm) %dir %{_localstatedir}/run/gdm/greeter
%attr(1770, root, gdm) %dir %{_localstatedir}/gdm
%attr(1777, root, gdm) %dir %{_localstatedir}/run/gdm
%attr(1755, root, gdm) %dir %{_localstatedir}/cache/gdm
%dir %{_sysconfdir}/dconf/db/gdm.d/locks
%dir %{_sysconfdir}/dconf/db/gdm.d
%{_sysconfdir}/dconf/db/gdm.d/00-upstream-settings
%{_sysconfdir}/dconf/db/gdm.d/locks/00-upstream-settings-locks
%{_sysconfdir}/dconf/profile/gdm
%{_datadir}/icons/hicolor/*/*/*.png
%config %{_sysconfdir}/pam.d/gdm-smartcard
%dir %{_datadir}/gdm/simple-greeter/extensions/smartcard
%{_datadir}/gdm/simple-greeter/extensions/smartcard/page.ui
%{_libdir}/gdm/simple-greeter/extensions/libsmartcard.so
%{_libexecdir}/gdm-smartcard-worker
%config %{_sysconfdir}/pam.d/gdm-fingerprint
%dir %{_datadir}/gdm/simple-greeter/extensions/fingerprint
%{_datadir}/gdm/simple-greeter/extensions/fingerprint/page.ui
%{_libdir}/gdm/simple-greeter/extensions/libfingerprint.so
%{_sysconfdir}/pam.d/gdm-launch-environment
%{_unitdir}/gdm.service
%doc %{_datadir}/help/*/gdm

%files devel
%dir %{_includedir}/gdm
%{_includedir}/gdm/*.h
%{_datadir}/gir-1.0/Gdm-1.0.gir
%{_libdir}/pkgconfig/gdm.pc
%{_includedir}/gdm/simple-greeter/gdm-login-extension.h
%{_libdir}/pkgconfig/gdmsimplegreeter.pc

%files libs
%{_libdir}/girepository-1.0/Gdm-1.0.typelib

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92.1-1m)
- update to 3.5.92.1

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-2m)
- import many bug fixes from upstream

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90
- remove pam.d/gdm-welcome, which is not used anymore

* Tue Aug 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-4m)
- backport two bug fix patches from upstream

* Sun Aug 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-3m)
- fix BTS #469; display-manager.service is now managed by alternatives

* Sun Aug 12 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-2m)
- add pam.d/gdm-welcome again

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sun Jul 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4.2-1m)
- update to 3.5.4.2
- fix BTS #455
-- add workaroudn patch

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-3m)
- rebuild with GConf2 3.2.5

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-2m)
- fix file conflict

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.11-20m)
- rebuild for glib 2.33.2

* Thu Mar  8 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.11-19m)
- delete Source4 for hal

* Tue Mar  6 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.11-18m)
- add Requires: gksu libgksu PackageKit-gtk-module (for gdm-display)

* Tue Mar  6 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.11-17m)
- default background image to wide with link momonga8.jpg
- add gdm-display command

* Mon Mar  5 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (2.20.11-16m)
- add Momonga Linux 8 Theme
-- background image update

* Sun Feb 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.11-15m)
- fix gdm-2.20.11-no-gdm-restart.patch

* Fri Feb 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.11-14m)
- remove EUC_JP support
- do not restart the gdm when select a language

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.11-13m)
- remove BR hal

* Tue Sep  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.11-12m)
- fix i18n for greeter momongagreeter-default-7.0.1.tar.bz2

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.11-11m)
- remove conflict files in %%{_datadir}/pixmaps/faces

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.11-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.11-9m)
- rebuild for new GCC 4.5

* Sun Sep  5 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.20.11-8m)
- change default desplay resolution for background image

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.11-7m)
- full rebuild for mo7 release

* Thu Aug 26 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.20.11-6m)
- background image fine tuning

* Mon Aug 23 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.20.11-5m)
- add Momonga Linux 7 Theme
-- background image update
-- add wide(16:10) size background image

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.20.11-4m)
- change background image (pre 7)

* Wed Aug  4 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.20.11-3m)
- change backguond color (#060630)

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.20.11-2m)
- fix build

* Fri Jun  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.11-1m)
- update to 2.20.11

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.10-8m)
- build fix gcc-4.4.4

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- use BuildRequires and Requires

* Wed Dec 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.10-6m)
- rebuild against audit-2.0.4 and pam-1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.10-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.10-4m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Tue Jun 23 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.20.10-3m)
- modify default theme patch ( change backguond color and default theme name )
- modify Momonga Linux 6 Themes (Source1)

* Mon Jun  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.10-2m)
- Provides: service(graphical-login) for anaconda

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.10-1m)
- update to 2.20.10

* Sun Mar  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.9-4m)
- back to 2.20.9

* Sun Mar  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-4m)
- add require gnome-session
- add some BPR

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-3m)
- fix %%{_localstatedir} owner

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-2m)
- merge form TSUPPA4RI

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-1m)
- update to 2.25.2
- delete Source11, 12 (merged)
- comment out Patch2, 3, 10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.9-3m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.9-2m)
- update Patch3 for fuzz=0
- License: LGPLv2+ and GPLv2+

* Thu Dec 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.9-1m)
- update to 2.20.9

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.20.8-1m)
- [CRASH FIX] version 2.20.8
- http://bugzilla.gnome.org/show_bug.cgi?id=517526
- https://bugs.launchpad.net/ubuntu/+source/gdm/+bug/249039
- use /sbin/shutdown instead of reboot and poweroff, upstart needs it
- own %%{_localstatedir}/log/gdm

* Wed Sep 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.20.7-5m)
- use /sbin/reboot instead of /usr/bin/reboot
- use /sbin/poweroff instead of /usr/bin/poweroff

* Wed Sep  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.7-4m)
- add Requires: gnome-keyring-pam

* Thu Aug 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.7-3m)
- modify momongagreater-default

* Wed Aug 27 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.20.7-2m)
- modify default theme patch ( change backguond color and default theme name )
- modify Momonga Linux 5 Themes (Source1)

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.7-1m)
- update to 2.20.7

* Sat Jun 14 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.6-6m)
- add BuildPrereq: zenity
-- "--enable-secureremote" requires zenity

* Tue May 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.20.6-5m)
- Requires: xorg-x11-server-utils and xorg-x11-xkb-utils
- Requires: gnome-settings-daemon and iso-codes

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.6-4m)
- fix pam setting (gdmsetup)

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.6-3m)
- del ShowOnlyIn GNOME

* Wed May 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.6-2m)
- fix gdmphotosetup.desktop

* Tue May 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.6-1m)
- update to 2.20.6

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20.5-4m)
- rebuild against librsvg2-2.22.2-3m

* Sat Apr 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.20.5-3m)
- re-construct options of configure

* Sat Apr 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.20.5-2m)
- build with ConsoleKit and PolicyKit
- import 90-grant-audio-devices-to-gdm.fdi from Fedora

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.5-1m)
- update to 2.20.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20.4-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.4-1m)
- update to 2.20.4

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2
- del CFLAGS="%%{optflags} -DG_DISABLE_ASSERT" (not so good)

* Sun Nov 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-2m)
- add CFLAGS="%%{optflags} -DG_DISABLE_ASSERT"

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri Aug  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.4-1m)
- [SECURITY] CVE-2007-3381
- update to 2.18.4

* Tue Jul 31 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.3-5m)
- modify default theme patch ( change backguond color and default theme name )
- modify Momonga Linux 4 Themes (Source1)

* Mon Jul 30 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.18.3-5m)
- modify ExcludeUser
- modify default theme patch

* Mon Jul 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-4m)
- use consolehelper

* Tue Jul 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-3m)
- modify Source2

* Sun Jul  8 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.3-2m)
- update patch2
- add path100(momonga default graphic theme)
- add Momonga Linux 4 Themes (Source2)

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Sun Jul  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.2-4m)
- do not install default.desktop

* Tue Jun 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.2-3m)
- install default.desktop for Beta1

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.2-2m)
- modify /etc/pam.d/* for new pam

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Sat May 19 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.1-2m)
- delete gnome.desktop file

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Thu Apr  5 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildPrereq: attr-devel

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.8-1m)
- update to 2.17.8

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.7-1m)
- update to 2.17.7 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-1m)
- update to 2.16.5

* Thu Dec 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.4-1m)
- SECUITY FIX
- CVE-2006-6105
- update to 2.16.4

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update 2.16.2

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sun Sep 10 2006 Nishio Futosh <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.10-2m)
- rebuild against expat-2.0.0-1m

* Fri Aug 18 2006 Nishio Futoshi <futoshi@momonga-linix.org>
- (2.14.10-1m)
- update to 2.14.10

* Sun Jul 23 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- adjustment momonga3-pink image

* Fri Jul 14 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.8-7m)
- revise Source1 (Straight Flush +6 colors)
- change background color

* Sun Jul  9 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.8-6m)
- revise Source1 (Straight Flush)

* Fri Jul 07 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.14.8-5m)
- please dont remove custom.conf if it is modified.

* Fri Jun 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.14.8-4m)
- added Japanese(EUC-JP) entry to Language selection dialog (/etc/X11/gdm/locale.alias)

* Thu Jun 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.8-3m)
- revise patch2 (background color)
- revise Source1 (card place)

* Wed Jun 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.8-2m)
- change default setting for momonga
-- revise patch2
-- if you want change them, exec /usr/sbin/gdmsetup
- update momongagreeter-default (SOURCE1)

* Thu Jun  8 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.8-1m)
- update to 2.14.8
- AOOH-GAH
- Security Fix

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.7-1m)
- update to 2.14.7
- delete patch0 (fixed)
- add /usr/local/bin to PATH

* Sun May 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.6-2m)
- add patch2: correct gdmsetup path

* Sun May 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.6-1m)
- update to 2.14.6

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.1.4-16m)
- rebuild against libgsf-1.14.0

* Fri Apr 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1.4-15m)
- revise gdm-2.4.1.4-xpath.patch
-- delete /usr/bin/X11 from PATH

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.1.4-14m)
- rebuild against openssl-0.9.8a

* Tue Mar 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1.4-13m)
- Change gdm.conf for X PATH /usr/bin

* Fri Dec 02 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.1.4-12m)
- rebuild against gnome-2.12.x
- revice Patch3 to gdm-2.4.1.4-gtk_gnome.patch (more ommit "-DGNOME_DEPRECATED" in compiler option to avoid build error with gnome-2.12.x)

* Sun Feb 27 2005 minakami <minakami@momonga-linux.org>
- (2.4.1.4-11m)
- add Source2: gdm-momonga-logo.png
- add Patch5: gdm-momonga-logo.patch
- add %%config(noreplace) %%{_sysconfdir}/X11/gdm/gdm.conf

* Wed Feb 16 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.4.1.4-10m)
- modify pam.d files for x86_64

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.1.4-8m)
- rebuild againt for gtk+-2.6.1

* Wed Jul 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.1.4-8m)
- add momongagreeter

* Fri Apr 23 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.1.4-7m)
- rebuild against libcroco-0.5.0

* Tue Apr 20 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.1.4-6m)
- adjustment BuildPreReq

* Tue Apr 20 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.1.4-5m)
- rebuild against for gtk+-2.4.0
- add patch2, patch3

* Wed Mar 10 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.4.1.4-4m)
- change Greeter from gdmlogin to gdmgreeter

* Sun Oct 12 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.1.4-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Jul 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1.4-2m)
- rebuild against for libcroco-0.3.0

* Fri May  9 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (2.4.1.4-1m)
- version 2.4.1.4
- enable XIM selector.

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1.3-3m)
- rebuild against for openssl-0.9.7

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1.3-2m)
- rebuild against for XFree86-4.3.0

* Tue Feb 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1.3-1m)
- version 2.4.1.3

* Sun Jan 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1.2-1m)
- version 2.4.1.2

* Wed Jan 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1.1-1m)
- version 2.4.1.1

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1.0-1m)
- version 2.4.1.0

* Thu Nov 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.12-1m)
- version 2.4.0.12

* Sat Aug 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.11-1m)
- version 2.4.0.11

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.10-1m)
- version 2.4.0.10

* Mon Aug 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.9-1m)
- version 2.4.0.9

* Thu Aug 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.8-1m)
- version 2.4.0.8

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.4-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Fri Jul 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.4-1m)
- version 2.4.0.4

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.3-1m)
- version 2.4.0.3

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0.2-1m)
- version 2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-18k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-16k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-14k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-12k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-10k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-8k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-6k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-4k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0.0-2k)
- version 2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.6-8k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.6-6k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.6-4k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.6-2k)
- version 2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.5-2k)
- version 2.3.90.5

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.4-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Thu May 30 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.4-2k)
- version 2.3.90.4

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.3-6k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.3-4k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Wed May 15 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.3-2k)
- version 2.3.90.3

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.2-10k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.3.90.2-8k)
- useradd and usermod revised.

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.2-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.2-4k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.2-2k)
- version 2.3.90.2

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.3.90.2-4k)
- /usr/sbin/useradd -> shadow-utils in PreReq.

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.2-2k)
- version 2.3.90.2

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-72k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-70k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-68k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-66k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-64k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-62k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-60k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-58k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-56k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-54k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-52k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-50k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-48k)
- change depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-46k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-44k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-42k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-40k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-38k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-36k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-34k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-32k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-30k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-28k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-26k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-24k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-22k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-20k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-18k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-16k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-14k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-12k)
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-10k)
- rebuild against for libglade-1.99.6

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-8k)
- rebuild against for gnome-vfs-1.9.5

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-6k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13
- rebuild against for libgnomecanvas-1.110.0
- rebuild against for libgnome-1.110.0
- rebuild against for libbonobo-1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-4k)
- rebuild against for linc-0.1.15

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.90.1-2k)
- version 2.3.90.1
- gnome2 env

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.0beta4-18k)
- nigittenu

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (2.0beta4-14k)
- rebuild against gettext 0.10.40.

* Sun Aug 05 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0beta4-12k)
- fix pam.d

* Sun Jul  8 2001 Toru Hoshina <toru@df-usa.com>
- (2.0beta4-10k)
- add alphaev5 support.

* Wed May  9 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- fix system default [session,xim,lang] path

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0beta4-5k)
- fixed for FHS

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Feb 13 2000 Shingo Akagaki <dora@kondara.org>
- Kondara-1.1 description

* Tue Jan  4 2000 Shingo Akagaki <dora@kondara.org>
- Bug fix.
- fix Tyuuta #271.
- many change for save{sess | lang | xim}

* Wed Dec  1 1999 Shingo Akagaki <dora@kondara.org>
- use new kondara xinitrc

* Fri Nov 19 1999 Shingo Akagaki <dora@kondara.org>
- reject all patch
- add Kondara patch

* Thu Nov 16 1999 Shingo Akagaki <dora@kondara.org>
- added requires for kondara-logos
  
* Mon Nov 1 1999 Shingo Akagaki <akagaki@ece.numazu-ct.ac.jp>
- reject redhat gdmlang patch
- add gdmlang patch
- patch /usr/share/locale/locale.alias file

* Sat Oct 20 1999 Shingo Akagaki <akagaki@ece.numazu-ct.ac.jp>
- build gdm-2.0beta4
- added XIM select menu
- patch X server start command

* Fri Sep 17 1999 Michael Fulbright <drmike@redhat.com>
- added requires for pam >= 0.68

* Fri Sep 10 1999 Elliot Lee <sopwith@redhat.com>
- I just update this package every five minutes, so any recent changes are my fault.

* Thu Sep 02 1999 Michael K. Johnson <johnsonm@redhat.com>
- built gdm-2.0beta2

* Mon Aug 30 1999 Michael K. Johnson <johnsonm@redhat.com>
- built gdm-2.0beta1

* Tue Aug 17 1999 Michael Fulbright <drmike@redhat.com>
- included rmeier@liberate.com patch for tcp socket X connections

* Mon Apr 19 1999 Michael Fulbright <drmike@redhat.com>
- fix to handling ancient gdm config files with non-standard language specs
- dont close display connection for xdmcp connections, else we die if remote
  end dies. 

* Fri Apr 16 1999 Michael Fulbright <drmike@redhat.com>
- fix language handling to set GDM_LANG variable so gnome-session 
  can pick it up

* Wed Apr 14 1999 Michael Fulbright <drmike@redhat.com>
- fix so certain dialog boxes dont overwrite background images

* Wed Apr 14 1999 Michael K. Johnson <johnsonm@redhat.com>
- do not specify -r 42 to useradd -- it doesn't know how to fall back
  if id 42 is already taken

* Fri Apr 9 1999 Michael Fulbright <drmike@redhat.com>
- removed suspend feature

* Mon Apr 5 1999 Jonathan Blandford <jrb@redhat.com>
- added patch from otaylor to not call gtk funcs from a signal.
- added patch to tab when username not added.
- added patch to center About box (and bring up only one) and ignore "~"
  and ".rpm" files.

* Fri Mar 26 1999 Michael Fulbright <drmike@redhat.com>
- fixed handling of default session, merged all gdmgreeter patches into one

* Tue Mar 23 1999 Michael Fulbright <drmike@redhat.com>
- remove GNOME/KDE/AnotherLevel session scripts, these have been moved to
  the appropriate packages instead.
- added patch to make option menus always active (security problem otherwise)
- added jrb's patch to disable stars in passwd entry field

* Fri Mar 19 1999 Michael Fulbright <drmike@redhat.com>
- made sure /usr/bin isnt in default path twice
- strip binaries

* Wed Mar 17 1999 Michael Fulbright <drmike@redhat.com>
- fixed to use proper system path when root logs in

* Tue Mar 16 1999 Michael Fulbright <drmike@redhat.com>
- linked Init/Default to Red Hat default init script for xdm
- removed logo from login dialog box

* Mon Mar 15 1999 Michael Johnson <johnsonm@redhat.com>
- pam_console integration

* Tue Mar 09 1999 Michael Fulbright <drmike@redhat.com>
- added session files for GNOME/KDE/AnotherLevel/Default/Failsafe
- patched gdmgreeter to not complete usernames
- patched gdmgreeter to not safe selected session permanently
- patched gdmgreeter to center dialog boxes

* Mon Mar 08 1999 Michael Fulbright <drmike@redhat.com>
- removed comments from gdm.conf file, these are not parsed correctly

* Sun Mar 07 1999 Michael Fulbright <drmike@redhat.com>
- updated source line for accuracy

* Fri Feb 26 1999 Owen Taylor <otaylor@redhat.com>
- Updated patches for 1.0.0
- Fixed some problems in 1.0.0 with installation directories
- moved /usr/var/gdm /var/gdm

* Thu Feb 25 1999 Michael Fulbright <drmike@redhat.com>
- moved files from /usr/etc to /etc

* Tue Feb 16 1999 Michael Johnson <johnsonm@redhat.com>
- removed commented-out #1 definition -- put back after testing gnome-libs
  comment patch

* Sat Feb 06 1999 Michael Johnson <johnsonm@redhat.com>
- initial packaging
