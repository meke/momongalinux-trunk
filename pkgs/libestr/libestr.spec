%global momorel 1

Name:           libestr
Version:        0.1.9
Release:        %{momorel}m%{?dist}
Summary:        String handling essentials library

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://libestr.adiscon.com/
Source0:        http://libestr.adiscon.com/files/download/libestr-%{version}.tar.gz
NoSource:	0
Patch0:        libestr-0.1.3-broken-configure-script.patch

%description
This package compiles the string handling essentials library
used by the Rsyslog daemon.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The package contains libraries and header files for
developing applications that use libestr.

%prep
%setup -q
%patch0 -p1 -b broken-configure-script.patch

%build
%configure --disable-static --with-pic
V=1 make %{?_smp_mflags}

%install
make install INSTALL="install -p" DESTDIR=%{buildroot}
rm -f %{buildroot}/%{_libdir}/*.{a,la}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc README COPYING AUTHORS ChangeLog
%{_libdir}/lib*.so.*

%files devel
%{_includedir}/libestr.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/libestr.pc

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.9-1m)
- update to 0.1.9

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.5-1m)
- update to 0.1.5

* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.4-1m)
- initial commit Momonga Linux
