%global         momorel 5

Name:           perl-DBD-Multi
Version:        0.18
Release:        %{momorel}m%{?dist}
Summary:        Manage Multiple Data Sources with Failover and Load Balancing
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBD-Multi/
Source0:        http://www.cpan.org/authors/id/D/DW/DWRIGHT/DBD-Multi-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.0
BuildRequires:  perl-Class-Accessor >= 0.19
BuildRequires:  perl-DBD-SQLite >= 1.09
BuildRequires:  perl-DBI
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-Util >= 1.18
BuildRequires:  perl-Pod-Simple
BuildRequires:  perl-Sys-SigAction >= 0.1
BuildRequires:  perl-Test-Exception >= 0.21
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-Pod >= 1.14
BuildRequires:  perl-Test-Pod-Coverage >= 1.04
Requires:       perl-Class-Accessor >= 0.19
Requires:       perl-DBI
Requires:       perl-List-Util >= 1.18
Requires:       perl-Sys-SigAction >= 0.1
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This software manages multiple database connections for failovers and also
simple load balancing. It acts as a proxy between your code and your
database connections, transparently choosing a connection for each query,
based on your preferences and present availability of the DB server.

%prep
%setup -q -n DBD-Multi-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README TODO
%{perl_vendorlib}/DBD/Multi.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-2m)
- rebuild against perl-5.18.0

* Wed Apr 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-11m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-10m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-9m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-8m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-7m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-2m)
- rebuild against gcc43

* Wed Mar  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Thu Feb 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Wed Aug 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sat Jul 07 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- Specfile autogenerated by cpanspec 1.70 for Momonga Linux.
