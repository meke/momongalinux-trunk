%global momorel 10

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define dotname werken.xpath

%define section free

Name:           werken-xpath
Version:        0.9.4
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        XPath implementation using JDOM
# Worth noting that this ASL 1.1 has slightly different wording.
# It may be GPL compatible as a result.
License:        "ASL 1.1"
Source0:        %{dotname}-%{version}-beta-src.tar.gz
Source1:        http://repo1.maven.org/maven2/%{name}/%{name}/%{version}/%{name}-%{version}.pom
Patch0:         %{name}-ElementNamespaceContext.patch
Patch1:         %{name}-Partition.patch
Patch2:         %{name}-ParentStep.patch
Patch3:         %{name}-NodeTypeStep.patch
Patch4:         %{name}-UnAbbrStep.patch
Patch5:         %{name}-StringFunction.patch
Patch6:         %{name}-Test.patch
Patch7:         %{name}-Driver.patch
Patch8:         %{name}-runtests_sh.patch
URL:            http://sourceforge.net/projects/werken-xpath/
Requires:       jdom
BuildRequires:  ant >= 0:1.6
BuildRequires:  antlr 
BuildRequires:  jdom
BuildRequires:  xerces-j2
BuildRequires:  xml-commons-apis
BuildRequires:  jpackage-utils >= 0:1.6
Group:          Development/Libraries
%if ! %{gcj_support}
BuildArch:      noarch
%endif
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:    werken.xpath = %{epoch}:%{version}-%{release}
Obsoletes:   werken.xpath < 0.9.4

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
werken.xpath is an implementation of the W3C XPath Recommendation, on
top of the JDOM library.  It takes as input a XPath expression, and a
JDOM tree, and returns a NodeSet (java.util.List) of selected
elements.  Is is being used in the development of the
as-yet-unreleased werken.xslt (eXtensible Stylesheet Language) and the
werken.canonical (XML canonicalization) packages.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
BuildRequires:  java-javadoc
# for /bin/rm and /bin/ln
Requires(post): coreutils
Requires(postun): coreutils
Provides:    werken.xpath-javadoc = %{epoch}:%{version}-%{release}
Obsoletes:   werken.xpath-javadoc < 0.9.4

%description    javadoc
Javadoc for %{name}.

# -----------------------------------------------------------------------------

%prep
%setup -q -n %{dotname}
%patch0 -b .sav
%patch1 -b .sav
%patch2 -b .sav
%patch3 -b .sav
%patch4 -b .sav
%patch5 -b .sav
%patch6 -b .sav
%patch7 -b .sav
%patch8 -b .sav

# remove all binary libs
for j in $(find . -name "*.jar"); do
	mv $j $j.no
done

cp %{SOURCE1} .

#pushd lib
#ln -sf $(build-classpath antlr) antlr-runtime.jar
#ln -sf $(build-classpath jdom) jdom.jar
#ln -sf $(build-classpath xerces-j2) xerces.jar
#popd

# -----------------------------------------------------------------------------

%build
export CLASSPATH=$(build-classpath jdom antlr xerces-j2 xml-commons-apis)
ant -Dbuild.compiler=modern package javadoc compile-test
# Note that you'll have to java in PATH for this to work, it is by default
# when using a JPackage JVM.
CLASSPATH=$CLASSPATH:build/werken.xpath.jar:build/test/classes
sh runtests.sh

# -----------------------------------------------------------------------------

%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p build/%{dotname}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done
ln -sf %{name}.jar %{dotname}.jar)

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr build/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

# maven
install -d -m 755 $RPM_BUILD_ROOT%{_mavenpomdir}
install -pm 644 %{name}-%{version}.pom \
        $RPM_BUILD_ROOT%{_mavenpomdir}/JPP-%{name}.pom
%add_to_maven_depmap %{name} %{name} %{version} JPP %{name}


# -----------------------------------------------------------------------------

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

# -----------------------------------------------------------------------------

%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%postun javadoc
if [ "$1" = "0" ]; then
    rm -f %{_javadocdir}/%{name}
fi

# -----------------------------------------------------------------------------

%post
%update_maven_depmap
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%update_maven_depmap
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc INSTALL LICENSE LIMITATIONS README TODO
%{_javadir}/*
%{_mavenpomdir}/*
%{_mavendepmapfragdir}/*

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}/werken-xpath-0.9.4.jar.*
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}

# -----------------------------------------------------------------------------

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-8m)
- full rebuild for mo7 release

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-7m)
- rename JPP.werken-xpath.pom to JPP-werken-xpath.pom otherwise
  building maven2-2.0.8 fails

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-6m)
- sync with Fedora 13 (0:0.9.4-5.beta.12.3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-0.beta.12jpp.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-0.beta.12jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-0.beta.12jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.beta.12jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.4-0.beta.12jpp.1m)
- import from Fedora

* Tue Mar 20 2007 Florian La Roche <laroche@redhat.com> 0:0.9.4-0.beta.12jpp.2
- change Provides:/Obsoletes: to standard way of epoch:version-release

* Fri Aug 18 2006 Fernando Nasser <fnasser@redhat.com> 0:0.9.4-0.beta.12jpp.1
- Merge with upstream
- Re-add define gcj_support

* Fri Aug 18 2006 Fernando Nasser <fnasser@redhat.com> 0:0.9.4-0.beta.12jpp
- Add requires for post and postun javadoc sections

* Thu Aug 03 2006 Fernando Nasser <fnasser@redhat.com> 0:0.9.4-0.beta.11jpp_2fc
- Remove define gcj_support and rebuild

* Tue Jul 25 2006 Fernando Nasser <fnasser@redhat.com> 0:0.9.4-0.beta.11jpp_1fc
- Merge with upstream

* Tue Jul 25 2006 Fernando Nasser <fnasser@redhat.com> 0:0.9.4-0.beta.11jpp
- Add missing header

* Fri May 05 2006 Ralph Apel <r.apel at r-apel.de> 0:0.9.4-0.beta.10jpp
- Rebuild for JPP-1.7

* Mon Aug 30 2004 Ralph Apel <r.apel at r-apel.de> 0:0.9.4-0.beta.9jpp
- Build with ant-1.6.2

* Thu Jan 22 2004 David Walluck <david@anti-microsoft.org> 0:0.9.4-0.beta.8jpp
- use oldjdom

* Sun May 25 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.9.4-0.beta.7jpp
- Add Epochs to dependencies.
- Add non-versioned javadoc symlinks.
- Add Distribution tag.

* Fri May 23 2003 Richard Bullington-McGuire <rbulling@pkrinternet.com> - 0.9.4-0.beta.6jpp
- Reworked spec file for JPackage 1.5 release

* Sun Mar  2 2003 Ville Skytta <ville.skytta at iki.fi> - 0.9.4-0.beta.5jpp
- Fix Group, License and Distribution tags.
- Patched to work with recent JDOM versions.
- Run unit tests during build.
- Use sed instead of bash 2 extension when symlinking jars during build.
- Some spec file cleanup.

* Mon Jan 21 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 0.9.4-0.beta.4jpp
- versioned dir for javadoc
- no dependencies for javadoc package
- macro section
- prevented Jikes use

* Fri Dec 7 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 0.9.4-0.beta.3jpp
- javadoc into javadoc package

* Sat Oct 13 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 0.9.4-0.beta.2jpp
- first unified release
- used original archive
- s/jPackage/JPackage

* Tue Aug 28 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 0.9.4-0.beta.1mdk
- first Mandrake release
