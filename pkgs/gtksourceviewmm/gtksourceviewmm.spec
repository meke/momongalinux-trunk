%global momorel 1

Summary: GtkSourceView is a text widget that extends the GtkTextView.
Name: gtksourceviewmm
Version: 3.2.0
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.2/%{name}-%{version}.tar.xz
NoSource: 0
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig

%description
GtkSourceView is a text widget that extends the standard gtk+ 2.x
text widget GtkTextView.
                                                                               
It improves GtkTextView by implementing syntax highlighting and other
features typical of a source editor.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc COPYING README ChangeLog AUTHORS NEWS
%{_libdir}/libgtksourceviewmm-3.0.so.*
%exclude %{_libdir}/*.la


%files devel
%defattr(-, root, root)
%{_includedir}/gtksourceviewmm-3.0
%{_libdir}/gtksourceviewmm-3.0/include/gtksourceviewmmconfig.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/gtksourceviewmm-3.0.pc
%{_datadir}/devhelp/books/gtksourceviewmm-3.0
%doc %{_datadir}/doc/gtksourceviewmm-3.0

%changelog
* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- initial build

