%global momorel 21
%global rbname newt

Summary: The extension library to use newt from Ruby.
Name: ruby-%{rbname}
Version: 0.3.4
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPL
URL: http://www.momonga-linux.org/

Source0: %{name}-%{version}.tar.gz
Patch0: ruby-newt-0.3.4-ruby19.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby
BuildRequires: newt-devel >= 0.52.2, gnewt-devel
BuildRequires: ruby-devel >= 1.9.2

%description
The extension library to use newt from Ruby.

%package -n ruby-gnewt
Summary: The extension library to use gnewt from Ruby.
Group: Development/Languages

%description -n ruby-gnewt
The extension library to use gnewt from Ruby.

%prep
%setup -q
%patch0 -p1 -b .ruby19

%build
ruby extconf.rb
make

cp newt.c gnewt.c
ruby -p -i -e "sub(/Init_newt/, 'Init_gnewt')" gnewt.c
cp Makefile Makefile.g
ruby -p -i -e "sub(/newt/, 'gnewt')" Makefile.g
make -f Makefile.g

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
make -f Makefile.g install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING sample/*.rb
%{ruby_sitearchdir}/newt.so

%files -n ruby-gnewt
%defattr(-, root, root)
%{ruby_sitearchdir}/gnewt.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.4-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-19m)
- full rebuild for mo7 release

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.4-18m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-17m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.4-15m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.4-14m)
- rebuild against gcc43

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.4-13m)
- rebuild against ruby-1.8.6-4m

* Thu May 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-12m)
- rebuild against newt-0.52.2

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.3.4-11m)
- /usr/lib/ruby

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.4-10m)
- rebuild against ruby-1.8.2

* Wed Apr  7 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.4-9m)
- add defattr to ruby-gnewt.

* Fri Mar  5 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.4-8m)
- rebuild against newt-0.51.6-1m

* Fri Dec 19 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.4-7m)
- rebuild against newt

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3.4-6m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3.4-5m)
- rebuild against ruby-1.8.0.

* Wed May 01 2002 Toru Hoshina <t@kondara.org>
- (0.3.4-2k)
- separate ruby-gnewt.

* Wed Apr 24 2002 Kenta MURATA <muraken@kondara.org>
- (0.3.4-2k)
  some method was fixed and changed few APIs.
  see sample/*.rb

* Mon Apr 22 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.3.3-2k)
  module function setHelpCallback, setSuspendCallback and sigtstp supported.
  see sample/test.rb

- (0.3.2-2k)
  module function winMenu and winEntries supported.
  see sample/testgrid.rb

* Sun Apr 21 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.3.1-2k)
  module function setColors and getDefaultColors supported.

* Sat Apr 20 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.3-2k)
  module function componentAddCallback supported.

* Fri Apr 19 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.2-2k)
  bug fix
  added sample

* Thu Apr 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.1-4k)
  Support gnewt.
- (0.1-2k)
  first release.
