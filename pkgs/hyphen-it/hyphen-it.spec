%global momorel 7

Name: hyphen-it
Summary: Italian hyphenation rules
%define upstreamid 20071127
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://ftp.services.openoffice.org/pub/OpenOffice.org/contrib/dictionaries/hyph_it_IT.zip
Group: Applications/Text
URL: http://wiki.services.openoffice.org/wiki/Dictionaries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2+
BuildArch: noarch
Requires: hyphen

%description
Italian hyphenation rules.

%prep
%setup -q -c -n hyphen-it
chmod -x *

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/hyphen
cp -p hyph_it_IT.dic $RPM_BUILD_ROOT/%{_datadir}/hyphen
pushd $RPM_BUILD_ROOT/%{_datadir}/hyphen/
it_IT_aliases="it_CH"
for lang in $it_IT_aliases; do
        ln -s hyph_it_IT.dic "hyph_"$lang".dic"
done

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_hyph_it_IT.txt
%{_datadir}/hyphen/*

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20071127-7m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20071127-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20071127-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20071127-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20071127-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20071127-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20071127-1m)
- import from Fedora

* Thu Nov 29 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071127-2
- add switz italian alias

* Tue Nov 27 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071127-1
- latest version

* Fri Nov 23 2007 Caolan McNamara <caolanm@redhat.com> - 0.20030809-1
- initial version
