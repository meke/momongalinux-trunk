%global momorel 1
%global drvver 5.0

Summary: A networked sound server, similar in theory to the Enlightened Sound Daemon
Name: pulseaudio
Version: 5.0
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPL
Group: System Environment/Daemons
URL: http://pulseaudio.org/
Source0: http://freedesktop.org/software/%{name}/releases/%{name}-%{version}.tar.xz
NoSource: 0
Source1: default.pa-for-gdm
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): shadow-utils
Requires: %{name}-libs = %{version}-%{release}
Requires: kernel >= 2.6.30
Requires: rtkit
Requires: systemd >= 185
BuildRequires: GConf2-devel
BuildRequires: alsa-lib-devel >= 1.0.19
BuildRequires: avahi-devel
BuildRequires: bluez-libs-devel >= 4.0
BuildRequires: doxygen
BuildRequires: gdbm-devel
BuildRequires: glib2-devel
BuildRequires: jack-devel
BuildRequires: libXt-devel
BuildRequires: libasyncns-devel >= 0.7
BuildRequires: libatomic_ops-devel
## reserve for new libatomic_ops
# BuildRequires: libatomic_ops-static
BuildRequires: libcap-devel
BuildRequires: liboil-devel
BuildRequires: libsamplerate-devel
BuildRequires: libsndfile-devel
BuildRequires: libtool
BuildRequires: libtool-ltdl-devel
BuildRequires: libudev-devel >= 185
BuildRequires: lirc-devel
BuildRequires: m4
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: pkgconfig
BuildRequires: sbc-devel
BuildRequires: speex-devel
BuildRequires: tcp_wrappers-devel
BuildRequires: xorg-x11-proto-devel
BuildRequires: xcb-util-devel >= 0.3.8
BuildRequires: json-c-devel >= 0.9
Obsoletes: pulseaudio-devel
Obsoletes: pulseaudio-core-libs

%description
PulseAudio is a sound server for Linux and other Unix like operating
systems. It is intended to be an improved drop-in replacement for the
Enlightened Sound Daemon (ESOUND).

%package esound-compat
Summary: PulseAudio EsounD daemon compatibility script
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}
Provides: esound
Obsoletes: esound

%description esound-compat
A compatibility script that allows applications to call /usr/bin/esd
and start PulseAudio with EsounD protocol modules.

%package module-lirc
Summary: LIRC support for the PulseAudio sound server
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}

%description module-lirc
LIRC volume control module for the PulseAudio sound server.

%package module-x11
Summary: X11 support for the PulseAudio sound server
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}
Requires: %{name}-utils = %{version}-%{release}

%description module-x11
X11 bell and security modules for the PulseAudio sound server.

%package module-zeroconf
Summary: Zeroconf support for the PulseAudio sound server
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}
Requires: pulseaudio-utils

%description module-zeroconf
Zeroconf publishing module for the PulseAudio sound server.

%package module-bluetooth
Summary: Bluetooth proximity support for the PulseAudio sound server
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}

%description module-bluetooth
Contains a module that can be used to automatically turn down the volume if
a bluetooth mobile phone leaves the proximity or turn it up again if it enters the
proximity again

%package module-jack
Summary: JACK support for the PulseAudio sound server
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}

%description module-jack
JACK sink and source modules for the PulseAudio sound server.

%package module-gconf
Summary: GConf support for the PulseAudio sound server
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}

%description module-gconf
GConf configuration backend for the PulseAudio sound server.

%package libs
Summary: Libraries for PulseAudio clients
License: LGPL
Group: System Environment/Libraries

%description libs
This package contains the runtime libraries for any application that wishes
to interface with a PulseAudio sound server.

%package core-libs
Summary: Core libraries for the PulseAudio sound server.
License: LGPL
Group: System Environment/Libraries

%description core-libs
This package contains runtime libraries that are used internally in the
PulseAudio sound server.

%package libs-glib2
Summary: GLIB 2.x bindings for PulseAudio clients
License: LGPL
Group: System Environment/Libraries

%description libs-glib2
This package contains bindings to integrate the PulseAudio client library with
a GLIB 2.x based application.

%package libs-zeroconf
Summary: Zeroconf support for PulseAudio clients
License: LGPL
Group: System Environment/Libraries

%description libs-zeroconf
This package contains the runtime libraries and tools that allow PulseAudio
clients to automatically detect PulseAudio servers using Zeroconf.

%package libs-devel
Summary: Headers and libraries for PulseAudio client development
License: LGPL
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: %{name}-libs-glib2 = %{version}-%{release}
Requires: %{name}-libs-zeroconf = %{version}-%{release}
Requires: glib2-devel
Requires: pkgconfig
Requires: vala
Provides: %{name}-devel
Obsoletes: %{name}-devel

%description libs-devel
Headers and libraries for developing applications that can communicate with
a PulseAudio sound server.

%package utils
Summary: PulseAudio sound server utilities
License: LGPL
Group: Applications/Multimedia
Requires: %{name}-libs = %{version}-%{release}

%description utils
This package contains command line utilities for the PulseAudio sound server.

%package gdm-hooks
Summary: PulseAudio GDM integration
License: LGPLv2+
Group: Applications/Multimedia
Requires: gdm
# for the gdm user
Requires(pre): gdm

%description gdm-hooks
This package contains GDM integration hooks for the PulseAudio sound server.

%prep
%setup -q -T -b0

# disable valgrind support to suppress
# "undefined symbol: pa_in_valgrind" warning with firefox and so on
sed --in-place=.valgrind~ -e 's,AC_CHECK_HEADERS_ONCE(\[valgrind/memcheck\.h\]),,g' configure.ac
autoreconf


%build
%configure \
	--disable-static \
	--disable-rpath \
	--with-system-user=pulse \
	--with-system-group=pulse \
	--with-access-group=pulse-access \
	--disable-hal \
	--without-fftw \
	--enable-systemd

%make LIBTOOL=%{_bindir}/libtool
%make doxygen

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name "*.la" -delete
find %{buildroot}%{_libdir}/* -name "*.a" -delete

chmod 755 %{buildroot}%{_bindir}/pulseaudio

# for applications using esd
ln -s esdcompat %{buildroot}%{_bindir}/esd

# clean up
rm -f %{buildroot}/%{_libdir}/libpulsecore.so

mkdir -p %{buildroot}%{_localstatedir}/lib/pulse

# preserve time stamps, for multilib's sake
touch -r src/daemon/daemon.conf.in %{buildroot}%{_sysconfdir}/pulse/daemon.conf
touch -r src/daemon/default.pa.in %{buildroot}%{_sysconfdir}/pulse/default.pa
touch -r man/pulseaudio.1.xml.in %{buildroot}%{_mandir}/man1/pulseaudio.1
touch -r man/default.pa.5.xml.in %{buildroot}%{_mandir}/man5/default.pa.5
touch -r man/pulse-client.conf.5.xml.in %{buildroot}%{_mandir}/man5/pulse-client.conf.5
touch -r man/pulse-daemon.conf.5.xml.in %{buildroot}%{_mandir}/man5/pulse-daemon.conf.5

mkdir -p %{buildroot}%{_localstatedir}/lib/pulse
mkdir -p %{buildroot}%{_localstatedir}/lib/gdm/.pulse
install -m 644 %{SOURCE1} %{buildroot}%{_localstatedir}/lib/gdm/.pulse/default.pa

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%pre
%{_sbindir}/groupadd -f -r pulse || :
%{_bindir}/id pulse >/dev/null 2>&1 || \
            %{_sbindir}/useradd -r -c 'PulseAudio System Daemon' -s /sbin/nologin -d /var/run/pulse -g pulse pulse || :
%{_sbindir}/groupadd -f -r pulse-access || :
exit 0

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%post libs-glib2 -p /sbin/ldconfig
%postun libs-glib2 -p /sbin/ldconfig

%post libs-zeroconf -p /sbin/ldconfig
%postun libs-zeroconf -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc GPL LGPL LICENSE README
%dir %{_sysconfdir}/pulse
%config(noreplace) %{_sysconfdir}/pulse/daemon.conf
%config(noreplace) %{_sysconfdir}/pulse/default.pa
%config(noreplace) %{_sysconfdir}/pulse/system.pa
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/pulseaudio-system.conf
%{_sysconfdir}/bash_completion.d/pulseaudio-bash-completion.sh
%attr(4755,root,root) %{_bindir}/pulseaudio
%dir %{_libdir}/pulse-%{drvver}
%dir %{_libdir}/pulse-%{drvver}/modules
%{_libdir}/pulse-%{drvver}/modules/libalsa-util.so
%{_libdir}/pulse-%{drvver}/modules/libcli.so
%{_libdir}/pulse-%{drvver}/modules/liboss-util.so
%{_libdir}/pulse-%{drvver}/modules/libprotocol-cli.so
%{_libdir}/pulse-%{drvver}/modules/libprotocol-esound.so
%{_libdir}/pulse-%{drvver}/modules/libprotocol-http.so
%{_libdir}/pulse-%{drvver}/modules/libprotocol-native.so
%{_libdir}/pulse-%{drvver}/modules/libprotocol-simple.so
%{_libdir}/pulse-%{drvver}/modules/librtp.so
%{_libdir}/pulse-%{drvver}/modules/module-alsa-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-alsa-source.so
%{_libdir}/pulse-%{drvver}/modules/module-alsa-card.so
%{_libdir}/pulse-%{drvver}/modules/module-cli-protocol-tcp.so
%{_libdir}/pulse-%{drvver}/modules/module-cli-protocol-unix.so
%{_libdir}/pulse-%{drvver}/modules/module-cli.so
%{_libdir}/pulse-%{drvver}/modules/module-combine.so
%{_libdir}/pulse-%{drvver}/modules/module-combine-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-dbus-protocol.so
%{_libdir}/pulse-%{drvver}/modules/module-device-manager.so
%{_libdir}/pulse-%{drvver}/modules/module-loopback.so
%{_libdir}/pulse-%{drvver}/modules/module-detect.so
#%%{_libdir}/pulse-%{drvver}/modules/module-equalizer-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-esound-compat-spawnfd.so
%{_libdir}/pulse-%{drvver}/modules/module-esound-compat-spawnpid.so
%{_libdir}/pulse-%{drvver}/modules/module-esound-protocol-tcp.so
%{_libdir}/pulse-%{drvver}/modules/module-esound-protocol-unix.so
%{_libdir}/pulse-%{drvver}/modules/module-esound-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-filter-apply.so
%{_libdir}/pulse-%{drvver}/modules/module-filter-heuristics.so
%{_libdir}/pulse-%{drvver}/modules/module-udev-detect.so
%{_libdir}/pulse-%{drvver}/modules/module-hal-detect.so
%{_libdir}/pulse-%{drvver}/modules/module-http-protocol-tcp.so
%{_libdir}/pulse-%{drvver}/modules/module-http-protocol-unix.so
%{_libdir}/pulse-%{drvver}/modules/module-match.so
%{_libdir}/pulse-%{drvver}/modules/module-mmkbd-evdev.so
%{_libdir}/pulse-%{drvver}/modules/module-native-protocol-fd.so
%{_libdir}/pulse-%{drvver}/modules/module-native-protocol-tcp.so
%{_libdir}/pulse-%{drvver}/modules/module-native-protocol-unix.so
%{_libdir}/pulse-%{drvver}/modules/module-null-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-null-source.so
%{_libdir}/pulse-%{drvver}/modules/module-oss.so
%{_libdir}/pulse-%{drvver}/modules/module-pipe-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-pipe-source.so
%{_libdir}/pulse-%{drvver}/modules/module-remap-source.so
%{_libdir}/pulse-%{drvver}/modules/module-rescue-streams.so
%{_libdir}/pulse-%{drvver}/modules/module-role-cork.so
%{_libdir}/pulse-%{drvver}/modules/module-role-ducking.so
%{_libdir}/pulse-%{drvver}/modules/module-rtp-recv.so
%{_libdir}/pulse-%{drvver}/modules/module-rtp-send.so
%{_libdir}/pulse-%{drvver}/modules/module-simple-protocol-tcp.so
%{_libdir}/pulse-%{drvver}/modules/module-simple-protocol-unix.so
%{_libdir}/pulse-%{drvver}/modules/module-sine.so
%{_libdir}/pulse-%{drvver}/modules/module-switch-on-connect.so
%{_libdir}/pulse-%{drvver}/modules/module-switch-on-port-available.so
%{_libdir}/pulse-%{drvver}/modules/module-systemd-login.so
%{_libdir}/pulse-%{drvver}/modules/module-tunnel-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-tunnel-sink-new.so
%{_libdir}/pulse-%{drvver}/modules/module-tunnel-source.so
%{_libdir}/pulse-%{drvver}/modules/module-tunnel-source-new.so
%{_libdir}/pulse-%{drvver}/modules/module-virtual-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-virtual-source.so
%{_libdir}/pulse-%{drvver}/modules/module-virtual-surround-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-volume-restore.so
%{_libdir}/pulse-%{drvver}/modules/module-xenpv-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-suspend-on-idle.so
%{_libdir}/pulse-%{drvver}/modules/module-default-device-restore.so
%{_libdir}/pulse-%{drvver}/modules/module-device-restore.so
%{_libdir}/pulse-%{drvver}/modules/module-stream-restore.so
%{_libdir}/pulse-%{drvver}/modules/module-card-restore.so
%{_libdir}/pulse-%{drvver}/modules/module-ladspa-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-remap-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-always-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-console-kit.so
%{_libdir}/pulse-%{drvver}/modules/module-position-event-sounds.so
%{_libdir}/pulse-%{drvver}/modules/module-augment-properties.so
#%%{_libdir}/pulse-%{drvver}/modules/module-cork-music-on-phone.so
%{_libdir}/pulse-%{drvver}/modules/module-sine-source.so
%{_libdir}/pulse-%{drvver}/modules/module-intended-roles.so
%{_libdir}/pulse-%{drvver}/modules/module-rygel-media-server.so
%{_libdir}/pulse-%{drvver}/modules/module-echo-cancel.so
%{_libdir}/pulse-%{drvver}/modules/module-switch-on-port-available.so


%{_datadir}/pulseaudio/alsa-mixer/paths/*.conf*
%{_datadir}/pulseaudio/alsa-mixer/profile-sets/*.conf
%{_mandir}/man1/pulseaudio.1*
%{_mandir}/man5/default.pa.5*
%{_mandir}/man5/pulse-client.conf.5*
%{_mandir}/man5/pulse-daemon.conf.5*
%{_mandir}/man5/pulse-cli-syntax.5*
/lib/udev/rules.d/90-pulseaudio.rules
%dir %{_libexecdir}/pulse
%attr(0700, pulse, pulse) %dir %{_localstatedir}/lib/pulse

%files esound-compat
%defattr(-,root,root)
%{_bindir}/esd
%{_bindir}/esdcompat
%{_mandir}/man1/esdcompat.1*

%files module-lirc
%defattr(-,root,root)
%{_libdir}/pulse-%{drvver}/modules/module-lirc.so

%files module-x11
%defattr(-,root,root)
%config %{_sysconfdir}/xdg/autostart/pulseaudio.desktop
%config %{_sysconfdir}/xdg/autostart/pulseaudio-kde.desktop
%{_bindir}/start-pulseaudio-kde
%{_bindir}/start-pulseaudio-x11
%{_libdir}/pulse-%{drvver}/modules/module-x11-bell.so
%{_libdir}/pulse-%{drvver}/modules/module-x11-publish.so
%{_libdir}/pulse-%{drvver}/modules/module-x11-xsmp.so
%{_libdir}/pulse-%{drvver}/modules/module-x11-cork-request.so
%{_mandir}/man1/start-pulseaudio-kde.1*
%{_mandir}/man1/start-pulseaudio-x11.1*

%files module-zeroconf
%defattr(-,root,root)
%{_libdir}/pulse-%{drvver}/modules/libavahi-wrap.so
%{_libdir}/pulse-%{drvver}/modules/module-zeroconf-publish.so
%{_libdir}/pulse-%{drvver}/modules/module-zeroconf-discover.so
%{_libdir}/pulse-%{drvver}/modules/libraop.so
%{_libdir}/pulse-%{drvver}/modules/module-raop-discover.so
%{_libdir}/pulse-%{drvver}/modules/module-raop-sink.so

%files module-jack
%defattr(-,root,root)
%{_libdir}/pulse-%{drvver}/modules/module-jack-sink.so
%{_libdir}/pulse-%{drvver}/modules/module-jack-source.so
%{_libdir}/pulse-%{drvver}/modules/module-jackdbus-detect.so

%files module-bluetooth
%defattr(-,root,root)
%{_libdir}/pulse-%{drvver}/modules/libbluez4-util.so
%{_libdir}/pulse-%{drvver}/modules/libbluez5-util.so
%{_libdir}/pulse-%{drvver}/modules/module-bluetooth-discover.so
%{_libdir}/pulse-%{drvver}/modules/module-bluetooth-policy.so
%{_libdir}/pulse-%{drvver}/modules/module-bluez*

%files module-gconf
%defattr(-,root,root)
%{_libdir}/pulse-%{drvver}/modules/module-gconf.so
%{_libexecdir}/pulse/gconf-helper

%files libs -f %{name}.lang
%defattr(-,root,root)
%doc README LICENSE GPL LGPL
%config(noreplace) %{_sysconfdir}/pulse/client.conf
%{_libdir}/libpulse.so.*
%{_libdir}/libpulse-simple.so.*
%{_libdir}/libpulsecore-%{drvver}.so
%{_libdir}/pulseaudio/libpulsecommon-%{drvver}.*

%files libs-glib2
%defattr(-,root,root)
%{_libdir}/libpulse-mainloop-glib.so.*

%files libs-zeroconf
%defattr(-,root,root)
#%{_bindir}/pabrowse
#%{_libdir}/libpulse-browse.so.*
#%{_mandir}/man1/pabrowse.1*

%files libs-devel
%defattr(-,root,root)
%doc doxygen/html
%{_libdir}/pkgconfig/libpulse*.pc
%{_includedir}/pulse/
%{_libdir}/libpulse.so
#%{_libdir}/libpulse-browse.so
%{_libdir}/libpulse-mainloop-glib.so
%{_libdir}/libpulse-simple.so
%{_libdir}/cmake/PulseAudio/PulseAudioConfig.cmake
%{_libdir}/cmake/PulseAudio/PulseAudioConfigVersion.cmake
%{_datadir}/vala/vapi/libpulse.vapi
%{_datadir}/vala/vapi/libpulse.deps
%{_datadir}/vala/vapi/libpulse-mainloop-glib.vapi
%{_datadir}/vala/vapi/libpulse-mainloop-glib.deps

%files utils
%defattr(-,root,root)
%{_bindir}/pacat
%{_bindir}/pacmd
%{_bindir}/pactl
%{_bindir}/paplay
%{_bindir}/parec
%{_bindir}/pamon
%{_bindir}/parecord
%{_bindir}/pax11publish
%{_bindir}/padsp
%{_bindir}/pasuspender
#%{_bindir}/qpaeq
%{_libdir}/pulseaudio/libpulsedsp.so
%{_mandir}/man1/pacat.1*
%{_mandir}/man1/pacmd.1*
%{_mandir}/man1/pactl.1*
%{_mandir}/man1/paplay.1*
%{_mandir}/man1/pasuspender.1*
%{_mandir}/man1/padsp.1*
%{_mandir}/man1/pax11publish.1*

%files gdm-hooks
%defattr(-,root,root)
%attr(0700, gdm, gdm) %dir %{_localstatedir}/lib/gdm/.pulse
%attr(0600, gdm, gdm) %{_localstatedir}/lib/gdm/.pulse/default.pa

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0-1m)
- update to 5.0

* Sat Jun  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0-1m)
- update to 4.0

* Wed Dec 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-1m)
- update to 3.0

* Wed Aug 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-2m)
- fix %%files to avoid conflicting

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1-1m)
- update to 2.1

* Fri Jul 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-1m)
- update to 1.1

* Thu Sep 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-1m)
- update to 1.0

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.99.4-1m)
- update to 0.99.4

* Mon Aug 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.99.2-1m)
- update to 0.99.2

* Tue Jun 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.23-1m)
- update to 0.9.23

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.22-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.22-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.22-1m)
- update to 0.9.22

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.21-6m)
- full rebuild for mo7 release

* Sun May 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.21-5m)
- disable valgrind support explicitly

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.21-4m)
- rebuild against openssl-1.0.0

* Sat Mar 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.21-3m)
- apply 62 upstream patches
- remove Requires: %%{name} = %%{version}-%%{release} from gdm-hooks

* Fri Jan  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.21-2m)
- revive module-device-manager.so, pulseaudio-kde.desktop and start-pulseaudio-kde
- https://bugzilla.redhat.com/show_bug.cgi?id=541419

* Tue Dec  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.21-1m)
- update 0.9.21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.19-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.19-1m)
- update 0.9.19

* Tue Sep 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.18-1m)
- version 0.9.18
- add --disable-hal to configure
- Requires: rtkit and udev >= 145
- add a package gdm-hooks
- import default.pa-for-gdm from Fedora
- remove all patches
- good-bye PolicyKit

* Sat Jul 25 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.15-5m)
- import a lot of patches from fedora 
- delete unused patch

* Fri Jul 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.15-4m)
- [SECURITY] CVE-2009-1894
- import upstream patch (Patch1)

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.15-3m)
- add pulseaudio-0.9.15-no-daemon-if-remote-desktop.patch

* Thu May 21 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.15-2m)
- fix libtoolize issue

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.15-1m)
- update 0.9.15

* Sat Jan 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.14-1m)
- update 0.9.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.13-5m)
- rebuild against rpm-4.6

* Fri Dec 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.13-4m)
- rebuild against bluez-4.22

* Fri Dec 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.13-3m)
- merge Fedora's changes
 +- move x11 scripts to p-m-x11 package
 +- Make the pulseaudio(-module-x11) package depend on pulseaudio-utils

* Tue Dec  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.13-2m)
- import 2 upstream patches from Fedora
- BuildRequires: libasyncns-devel

* Sun Oct 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.13-1m)
- version 0.9.13
- import upstream patches from Fedora

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.11-1m)
- update to 0.9.11
- comment out patch0 

* Mon Sep 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.10-3m)
- import lock-file.patch from F9-updates(testing)
 +* Thu Sep 9 2008 Lennart Poettering <lpoetter@redhat.com> 0.9.10-2
 +- Fix lock file handling: properly check if the PID stored in our lock file
 +  actually belongs to us. Fixes #438284

* Sat Apr 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.10-2m)
- build with PolicyKit

* Wed Apr 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.10-1m)
- version 0.9.10
- clean up patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.8-7m)
- rebuild against gcc43

* Mon Mar 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.8-5m)
- modify Provides and Obsoletes

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.8-4m)
- re-split packages like Fedora
- import 4 patches from Fedora

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-3m)
- %%NoSource -> NoSource

* Sun Jan  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-1m)
- update 0.9.8
- delete patch4

* Tue Mar 20 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.9.5-1m)
- welcome to momonga

* Sat Mar  2 2007 Pierre Ossman <drzeus@drzeus.cx> 0.9.5-5
- Fix merge problems with patch.

* Fri Mar  2 2007 Pierre Ossman <drzeus@drzeus.cx> 0.9.5-4
- Add patch to handle ALSA changing the frame size (bug 230211).
- Add patch for suspended ALSA devices (bug 228205).

* Mon Feb  5 2007 Pierre Ossman <drzeus@drzeus.cx> 0.9.5-3
- Add esound-compat subpackage that allows PulseAudio to be a drop-in
  replacement for esd (based on patch by Matthias Clasen).
- Backport patch allows startup to continue even when the users'
  config cannot be read.

* Wed Oct 23 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.5-2
- Create user and groups for daemon.

* Mon Aug 28 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.5-1
- Upgrade to 0.9.5.

* Wed Aug 23 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.4-3
- Make sure JACK modules are built and packaged.

* Tue Aug 22 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.4-2
- Merge the ALSA modules into the main package as ALSA is the
  standard API.

* Sun Aug 20 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.4-1
- Update to 0.9.4.
- Remove fix for rpath as it is merged upstream.

* Fri Jul 21 2006 Toshio Kuratomi <toshio@tiki-lounge.com> 0.9.3-2
- Remove static libraries.
- Fix for rpath issues.

* Fri Jul 21 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.3-1
- Update to 0.9.3
- GLib 1.2 bindings dropped.
- Howl compat dropped as Avahi is supported natively.
- Added fix for pc files on x86_64.

* Sat Jul  8 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.2-1
- Update to 0.9.2.
- Added Avahi HOWL compat dependencies.

* Thu Jun  8 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.1-1
- Update to 0.9.1.

* Mon May 29 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.0-2
- Build and package doxygen docs
- Call ldconfig for relevant subpackages.

* Mon May 29 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.0-1
- Update to 0.9.0

* Tue May  9 2006 Pierre Ossman <drzeus@drzeus.cx> 0.8.1-1
- Update to 0.8.1
- Split into more packages
- Remove the modules' static libs as those shouldn't be used (they shouldn't
  even be installed)

* Fri Feb 24 2006 Tom "spot" Callaway <tcallawa@redhat.com> 0.7-2
- dance around with perms so we don't strip the binary
- add missing BR

* Mon Nov 28 2005 Tom "spot" Callaway <tcallawa@redhat.com> 0.7-1
- Initial package for Fedora Extras
