%global momorel 21


Summary: GNOME http client library
Name: libghttp
Version: 1.0.9
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.gnome.org/
Source: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.0/%{name}-%{version}.tar.gz
Nosource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: libtool

%description
Library for making HTTP 1.1 requests.

%package devel
Summary: GNOME http client development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files you can use for libghttp development

%prep
%setup -q

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

%build
%configure
make %{?_smp_mflags} LIBTOOL=%{_bindir}/libtool

%install
rm -rf %{buildroot}
%makeinstall LIBTOOL=%{_bindir}/libtool

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README doc/ghttp.html
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_includedir}/*
%{_libdir}/ghttpConf.sh

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.9-19m)
- full rebuild for mo7 release

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-18m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.9-17m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-15m)
- always use %%{_bindir}/libtool

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.9-14m)
- fix build on x86_64

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-13m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-12m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.9-11m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.9-10m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.9-9m)
- enable ppc64

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.9-8m)
- enable x86_64. use %{_bindir}/libtool for x86_64.

* Sat Mar  8 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.9-7m)
- arrangement spec file

* Sun Aug 26 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.9-6k)
- arrangement spec file

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.0.9-4k)
- no more ifarch alpha.

* Thu Jan 25 2001 Yoshito Komatsu <yoshito.komatsu@nifty.com>
- (1.0.9-3k)
- update to 1.0.9.

* Mon Jan 15 2001 Toru Hoshina <toru@df-usa.com>
- (1.0.8-3k)

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.7-5k)
- modified Docdir with macros

* Sun Aug 20 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.6

* Thu Mar 16 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.5

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Fri Feb 18 2000 Toru Hoshina <t@kondara.org>
- add alpha arch support.

* Fri Aug 13 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.4

* Thu Mar 18 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.2

* Sun Mar 14 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.0

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.2

* Wed Jan 06 1999 Michael Fulbright <drmike@redhat.com>
- built with gnome-libs 0.99.2 
