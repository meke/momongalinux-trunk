%global momorel 8
Summary      : Documents for Ngraph
Name         : Ngraph-doc
Version      : 6.3.30
Release      : %{momorel}m%{?dist}
License      : GPL
Group        : Documentation
Source       : http://www2e.biglobe.ne.jp/~isizaka/bin/Ngraph-%{version}-doc.tar.gz
NoSource     : 0
URL          : http://www2e.biglobe.ne.jp/~isizaka/
BuildRoot    : %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch

%description
Documents for X-based Graph Plot Utility 'Ngraph'

%prep
rm -rf --preserve-root %{buildroot}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_prefix}/lib/Ngraph
cd %{buildroot}%{_prefix}/lib/Ngraph
tar xvzf %{SOURCE0}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%{_prefix}/lib/Ngraph/doc/Ngraph.html
%{_prefix}/lib/Ngraph/doc/dialogs
%{_prefix}/lib/Ngraph/doc/img
%{_prefix}/lib/Ngraph/doc/*.htm
%{_prefix}/lib/Ngraph/doc/tutorial

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3.30-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3.30-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3.30-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.30-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.30-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.30-3m)
- rebuild against gcc43

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.3.30-2m)
- delete duplicated dir

* Sat Aug 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (6.3.30-1m)
  update to 6.3.30
  
* Sat Mar 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.18-1m)
- update to 6.3.18

* Wed Jul 17 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (6.3.16-1m)
- update to 6.3.16

* Wed Mar 21 2001 NORIKANE Seiichiro <no_ri@kf6.so-net.ne.jp>
- (6.03-5k)
- relocate docmentation to %{_prefix}/lib/Ngraph

* Thu Jan 25 2001 NORIKANE Seiichiro <no_ri@kf6.so-net.ne.jp>
- (6.03-3k)
- ported from Official Release (6.03-1)
