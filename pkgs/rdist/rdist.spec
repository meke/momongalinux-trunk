%global momorel 25

Summary: Maintains identical copies of files on multiple machines.
Name: rdist
Version: 6.1.5
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/System
Source0: http://www.MagniComp.com/download/rdist/rdist-%{version}.tar.gz
NoSource: 0
Source1: http://www.magnicomp.com/rdist/rdist-eu-license.txt
Source2: http://people.redhat.com/pknirsch/src/rdist-v1.1.tar.bz2
Patch0: rdist-6.1.5-linux.patch
Patch1: rdist-6.1.5-links.patch
Patch2: rdist-6.1.5-oldpath.patch
Patch3: rdist-6.1.5-hardlink.patch
Patch4: rdist-6.1.5-bison.patch
Patch5: rdist-6.1.5-varargs.patch
Patch6: rdist-6.1.5-maxargs.patch
Patch7: rdist-6.1.5-lfs.patch
Patch8: rdist-6.1.5-cleanup.patch
Patch9: rdist-6.1.5-svr4.patch
Patch10: rdist-6.1.5-ssh.patch
Patch11: rdist-6.1.5-mkstemp.patch
Patch12: rdist-6.1.5-stat64.patch
Patch13: rdist-6.1.5-re_args.patch
Patch14: rdist-6.1.5-fix-msgsndnotify-loop.patch
URL: http://www.MagniComp.com/rdist/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: byacc bison

%description
The RDist program maintains identical copies of files on multiple
hosts.  If possible, RDist will preserve the owner, group, mode and
mtime of files and it can update programs that are executing.

Install rdist if you need to maintain identical copies of files on
multiple hosts.

%prep
%setup -q
%setup -q -T -D -a 2

cp %{SOURCE1} .

%patch0 -p1 -b .linux
%patch1 -p1 -b .links
%patch2 -p1 -b .oldpath
%patch3 -p1 -b .hardlink
%patch4 -p1 -b .bison
%patch5 -p1 -b .varargs
%patch6 -p1 -b .maxargs
%patch7 -p1 -b .lfs
%patch8 -p1 -b .cleanup
%patch9 -p1 -b .svr4
%patch10 -p1 -b .ssh
%patch11 -p1 -b .mkstemp
%patch12 -p1 -b .stat64
%patch13 -p1 -b .re_args
%patch14 -p1 -b .fix-msgsndnotify-loop

%build
make
make -C rdist

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_mandir}/man{1,8}

install -m755 src/rdist %{buildroot}%{_bindir}
install -m755 rdist/rdist %{buildroot}%{_bindir}/oldrdist
install -m755 src/rdistd %{buildroot}%{_sbindir}
ln -sf ../sbin/rdistd %{buildroot}%{_bindir}/rdistd

install -m644 doc/rdist.man %{buildroot}%{_mandir}/man1/rdist.1
install -m644 doc/rdistd.man %{buildroot}%{_mandir}/man8/rdist.8

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README rdist-eu-license.txt
%{_bindir}/rdist
%{_bindir}/oldrdist
%{_bindir}/rdistd
%{_sbindir}/rdistd
%{_mandir}/man1/rdist.1*
%{_mandir}/man8/rdist.8*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.5-25m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.5-24m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.1.5-23m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.1.5-22m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.5-21m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.5-20m)
- sync with Fedora 11 (1:6.1.5-46)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.5-19m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.1.5-18m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.1.5-17m)
- %%NoSource -> NoSource

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (6.1.5-16m)
- remove Epoch

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Jul 20 1999 Jeff Johnson <jbj@redhat.com>
- re-release latest rdist package.

* Thu Jun  3 1999 Jeff Johnson <jbj@redhat.com>
- permit rdist to distribute hard links (#3228)

* Tue Apr 13 1999 Jeff Johnson <jbj@redhat.com>
- add /usr/bin/rdistd symlink (#2154)
- update docs to reflect /usr/bin/oldrdist change.

* Mon Apr 12 1999 Jeff Johnson <jbj@redhat.com>
- use /usr/bin/oldrdist for old rdist compatibility path (#2044).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Wed Feb 17 1999 Jeff Johnson <jbj@redhat.com>
- dynamic allocation for link info (#1046)

* Thu Nov 12 1998 Jeff Johnson <jbj@redhat.com>
- update to 6.1.5

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Oct 20 1997 Otto Hammersmith <otto@redhat.com>
- fixed the url to the source
- fixed the copyright field

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc
