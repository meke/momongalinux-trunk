%global momorel 1
%global realname xkbevd

Summary: X.Org X11 xkb utilities (%{realname})
Name: xorg-x11-%{realname}
Version: 1.1.3
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%define xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{realname}-%{version}.tar.bz2 
NoSource: 0

BuildRequires: pkgconfig
BuildRequires: libxkbfile-devel
BuildRequires: libX11-devel

%description
%{realname}

%prep
%setup -q -n %{realname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/%{realname}
%{_mandir}/man1/%{realname}.1*

%changelog
* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-2m)
- rebuild for new GCC 4.6

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- %%NoSource -> NoSource

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- separated from xorg-x11-xkb-utils
