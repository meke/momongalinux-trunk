%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global debug_package %{nil}

Name:           caribou          
Version:        0.4.4
Release:        %{momorel}m%{?dist}
Summary:        A simplified in-place on-screen keyboard

Group:          User Interface/Desktops
License:        LGPLv2+
URL:            http://live.gnome.org/Caribou
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.4/%{name}-%{version}.tar.xz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  gtk3-devel
BuildRequires:  clutter-devel
BuildRequires:  pygobject-devel >= 2.90
BuildRequires:  vala-devel >= 0.13.4
Requires:       pygtk2
Requires:       at-spi-python
Requires:       pyclutter
Requires:       python-virtkey

%description
Caribou is a text entry application that currently manifests itself as
a simplified in-place on-screen keyboard.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static \
	--enable-silent-rules \
	--disable-scrollkeeper
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

# BTS 394
echo "OnlyShowIn=GNOME;" >> %{buildroot}%{_sysconfdir}/xdg/autostart/caribou-autostart.desktop

%clean
rm -rf --preserve-root %{buildroot}

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/xdg/autostart/caribou-autostart.desktop
%{_bindir}/%{name}
%{_bindir}/%{name}-preferences
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.caribou.gschema.xml
%{python_sitelib}/%{name}

%{_datadir}/dbus-1/services/org.gnome.Caribou.Antler.service

%{_libdir}/libcaribou.so.*
%exclude %{_libdir}/*.la

%{_libdir}/gnome-settings-daemon-3.0/gtk-modules/caribou-gtk-module.desktop
%{_libdir}/girepository-1.0/Caribou-1.0.typelib
%{_libdir}/gtk-2.0/modules/libcaribou-gtk-module.*
%{_libdir}/gtk-3.0/modules/libcaribou-gtk-module.*

%{_libexecdir}/antler-keyboard
%{_datadir}/antler
%{_datadir}/glib-2.0/schemas/org.gnome.antler.gschema.xml

%files devel
%defattr(-,root,root)
%{_includedir}/libcaribou
%{_libdir}/*.so
%{_datadir}/gir-1.0/Caribou-1.0.gir

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-1m)
- update to 0.4.4

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.92-1m)
- update to 0.3.92

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.9-4m)
- fix BTS:394

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.9-3m)
- add BuildRequires: vala-devel >= 0.13.4

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.3.91-2m)
- fix BuildRequires

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.91-1m)
- update to 0.3.91

* Mon Aug  1 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.00-2m)
- add BuildRequires

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.00-1m)
- version down to 0.2.00
- delete devel package

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2
- add devel package

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.2-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0.2-1m)
- import from Fedora

* Thu Jan 21 2009 Ben Konrath <ben@bagu.org> - 0.0.2-1
- Initial release.
