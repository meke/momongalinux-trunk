%global momorel 22

Name:           JSDoc
Version:        1.10.2
Release:        %{momorel}m%{?dist}
Summary:        Produces javadoc-style documentation from JavaScript sourcefiles
Group:          Development/Tools
License:        GPL
URL:            http://jsdoc.sourceforge.net
Source0:        http://dl.sourceforge.net/sourceforge/jsdoc/%{name}-%{version}.tgz
Source1:        %{name}-COPYING.txt.gz
Patch0:         %{name}-mc-build.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
Requires:       perl(HTML::Template)

%description
A script that produces javadoc-style documentation from
well-formed JavaScript sourcefiles. At the moment, this means it
supports sourcefiles where all functions are mapped to a class
using prototype-based inheritance.  Anonymous function
definitions (e.g. Circle.prototype.getRadius = function(){ ...} )
are supported.

%prep
%setup -q -n %{name}-%{version}
gzip -dc %{SOURCE1} >COPYING
%patch0 -p1

%build

%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_bindir}
install -p -m 755 jsdoc.pl $RPM_BUILD_ROOT%{_bindir}/jsdoc
install -d -p $RPM_BUILD_ROOT%{perl_vendorlib}/JSDoc
install -p -m 644 JSDoc.pm $RPM_BUILD_ROOT%{perl_vendorlib}
install -p -m 644 JSDoc/* $RPM_BUILD_ROOT%{perl_vendorlib}/JSDoc/
install -d -p $RPM_BUILD_ROOT%{perl_vendorlib}/JavaScript/Syntax/
install -p -m 644 JavaScript/Syntax/* \
    $RPM_BUILD_ROOT%{perl_vendorlib}/JavaScript/Syntax/
install -d -p -m 755 $RPM_BUILD_ROOT%{_datadir}/jsdoc/
install -p -m 444 *.tmpl *.css $RPM_BUILD_ROOT%{_datadir}/jsdoc/

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README TODO CHANGES COPYING
%{_bindir}/jsdoc
%{perl_vendorlib}/JSDoc
%{perl_vendorlib}/JSDoc.pm
%{perl_vendorlib}/JavaScript
%{_datadir}/jsdoc

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-17m)
- rebuild against perl-5.16.2

* Tue Aug 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-16m)
- no NoSource

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.2-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.2-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.2-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-6m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.2-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.10.2-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.2-2m)
- rebuild against rpm-4.6

* Wed May 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10.2-1m)
- import from Fedora

* Sun Jul  1 2007 Matej Cepl <mcepl@redhat.com> - 1.10.2-4
- Administrative increase of the release in order to overcome screwed up tagging in Fedora CVS.

* Sat Jun 23 2007 Matej Cepl <mcepl@redhat.com> - 1.10.2-3
- Added GPL text as new source
- documentation files (*.tmpl, *.css) shouldn't be executable.
- limited ownership of perl_vendorlib direcotries.

* Sat Apr  7 2007 Matej Cepl <mcepl@redhat.com> - 1.10.2-2
- License is GPL, according to the website.

* Fri Apr  6 2007 Matej Cepl <mcepl@redhat.com> - 1.10.2-1
- Initial build
