%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define realname progressbar

Name:           python-%{realname}
Version:        2.2
Release:        %{momorel}m%{?dist}
Summary:        Text progressbar library for python

Group:          Development/Libraries
License:        LGPLv2+ 
URL:            http://pypi.python.org/pypi/%{realname}/
Source0:        http://pypi.python.org/packages/source/p/%{realname}/%{realname}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildArch:      noarch

%description

This library provides a text mode progress bar. This is typically used to 
display the progress of a long running operation, providing a visual clue that 
processing is under way.

The progressbar module is very easy to use, yet very powerful. And 
automatically supports features like auto-resizing when available.

%prep
%setup -q -n %{realname}-%{version}


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
chmod 0755 $RPM_BUILD_ROOT/%{python_sitelib}/progressbar.py
 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%doc README
%{python_sitelib}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-2m)
- full rebuild for mo7 release

* Fri Mar 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-1m)
- import from Fedora 13

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jan 8 2009 Christof Damian <christof@damian.net> 2.2-5
- don't include sitelib in files 

* Thu Jan 8 2009 Christof Damian <christof@damian.net> 2.2-4
- change the attributes of progressbar.py in install

* Thu Jan 8 2009 Christof Damian <christof@damian.net> 2.2-3
- changed attributes for progressbar.py, it contains excuteable examples

* Tue Jan 6 2009 Christof Damian <christof@damian.net> 2.2-2
- remove version from upstream url

* Sat Jan 3 2009 Christof Damian <christof@damian.net> 2.2-1
- initial spec file
