%global momorel 1

Name: brasero
Summary: brasero
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
URL: http://projects.gnome.org/brasero/

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig

BuildRequires: gstreamer1-devel
BuildRequires: gstreamer1-plugins-base-devel
BuildRequires: glib2-devel
BuildRequires: libxml2-devel
BuildRequires: GConf2-devel
BuildRequires: gtk3-devel
BuildRequires: dbus-glib-devel
BuildRequires: libSM-devel
BuildRequires: nautilus-devel >= 3.0.0
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: totem-pl-parser-devel
BuildRequires: libburn-devel
BuildRequires: libisofs-devel
BuildRequires: tracker-devel >= 0.12
Requires(pre): hicolor-icon-theme gtk2
Requires: %{name}-libs
Obsoletes: nautilus-cd-burner

%description
Brasero is a application to burn CD/DVD for the Gnome Desktop. It is
designed to be as simple as possible and has some unique features to
enable users to create their discs easily and quickly.

%package libs
Summary: %{name}-libs
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description libs
%{name}-libs

%package nautilus
Summary: %{name} nautilus extention
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description nautilus
%{name} nautilus extention

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libglade2-devel
Requires: glib2-devel
Requires: gnome-vfs2-devel
Requires: gtk2-devel
Requires: libgnome-devel
Requires: nautilus-devel

Obsoletes: nautilus-cd-burner-devel

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --enable-gtk-doc \
    --disable-scrollkeeper \
    --disable-schemas-install \
    --enable-introspection=yes
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%postun
rarian-sk-update

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
%{_libdir}/%{name}3
%{_datadir}/%{name}
%{_datadir}/applications/*
%{_datadir}//help/*/*
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/mime/packages/brasero.xml
%{_mandir}/man1/brasero.1.*
%{_libdir}/girepository-1.0/BraseroBurn-*.typelib
%{_libdir}/girepository-1.0/BraseroMedia-*.typelib
%{_datadir}/GConf/gsettings/brasero.convert
%{_datadir}/glib-2.0/schemas/org.gnome.brasero.gschema.xml

%files libs
%defattr(-,root,root)
%{_libdir}/libbrasero-burn3.so.*
%{_libdir}/libbrasero-media3.so.*
%{_libdir}/libbrasero-utils3.so.*
%exclude %{_libdir}/*.la

%files nautilus
%defattr(-,root,root)
%{_libdir}/nautilus/extensions-3.0/*

%files devel
%defattr (-, root, root)
%{_includedir}/%{name}3
%{_libdir}/*.so
%{_libdir}/pkgconfig/libbrasero-burn3.pc
%{_libdir}/pkgconfig/libbrasero-media3.pc
%{_datadir}/gtk-doc/html/libbrasero-burn
%{_datadir}/gtk-doc/html/libbrasero-media
%{_datadir}/gir-1.0/*.gir
%{_datadir}/gir-1.0/BraseroBurn-*.gir
%{_datadir}/gir-1.0/BraseroMedia-*.gir

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-4m)
- rebuild for tracker 0.14.2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-3m)
- rebuild for glib 2.33.2

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0-2m)
- remove BR hal

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-2m)
- rebuild against tracker

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- add glib-compile-schemas script

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-4m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.30.2-3m)
- remove %%{_libdir}/nautilus/extensions-2.0 from main package to avoid conflicting

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- split libs and nautilus

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.3-2m)
- delete __libtoolize hack

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-3m)
- Obsolete nautilus-cd-burner

* Thu Oct 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- good-bye beagle

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Tue Oct  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91.2-1m)
- update to 2.25.91.2

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- initial build
