%global momorel 10

%global rhel 7
%global fedora 18

# Define variables to use in conditionals
%global force_sound_aplay       0
%global dbus_integration        0
%global gstreamer_integration   0
%global nm_integration          0
%global modular_x               0
%global dbus_glib_splt          0
%global bonjour_support         0
%global meanwhile_integration   0
%global use_gnome_open          0
%global perl_devel_separated    1
%global perl_embed_separated    0
%global api_docs                0
%global krb4_removed            1
%global nss_md2_disabled        0
%global vv_support              0
%global libidn_support          0
%global disable_silc            0
%global disable_evolution       0
%global split_evolution         0
%global use_system_certs        0
%global use_system_libgadu      0
%global build_only_libs         0

# RHEL4: Use ALSA aplay to output sounds because it lacks gstreamer
%if 0%{?fedora} < 5
%global force_sound_aplay       1
%endif
# RHEL4+ and FC5+: dbus, gstreamer, NetworkManager, modular X
%if 0%{?fedora} >= 5
%global dbus_integration        1
%global gstreamer_integration   1
%global nm_integration          1
%global modular_x               1
%endif
# RHEL4+ and FC6+: dbus-glib split, bonjour, meanwhile
%if 0%{?fedora} >= 6
%global dbus_glib_splt          1
%global bonjour_support         1
%global meanwhile_integration   1
%endif
# RHEL4 and RHEL5: Use gnome-open instead of xdg-open (RHEL4 and RHEL5)
%if 0%{?fedora} <= 6
%global use_gnome_open          1
%endif
# F7+: Perl devel separated out
%if 0%{?fedora} >= 7
%global perl_devel_separated    1
%endif
# F8+: Perl embed separated out, generate pidgin API documentation
%if 0%{?fedora} >= 8
%global perl_embed_separated    1
%global api_docs                1
%endif
# F10+: New NSS (3.12.3) disables weaker MD2 algorithm
%if 0%{?fedora} >= 10
%global nss_md2_disabled        1
%endif
# F11+: libidn for punycode domain support, voice and video support,
# use system SSL certificates
%if 0%{?fedora} >= 11
%global vv_support              1
%global libidn_support          1
%global use_system_certs        1
%endif
# F12+: krb4 removed
%if 0%{?fedora} >= 12
%global krb4_removed            1
%endif
# EL6: Disable SILC protocol
# (get rid of extra crypto lib for perpetually broken protocol that nobody uses)
# (the above comment is not necessarily the view held by all maintaners of this package)
%if 0%{?rhel} >= 6
%global disable_silc            1
%endif
# F13+ Split Evolution plugin to separate package (#581144)
%if 0%{?fedora} >= 13
%global split_evolution         1
%endif
# F16+ Use system libgadu (#713888)
%if 0%{?fedora} >= 16
%global use_system_libgadu      1
%endif
%if 0%{?rhel} >= 7
%global build_only_libs         0
%global api_docs                0
%endif
# F18+ Disable evolution integration (temporarily?)
# due to evolution-data-server 3.6 API changes
%if 0%{?fedora} >= 18
%global disable_evolution       1
%global split_evolution         0
%endif

Name:           pidgin
Version:        2.10.6
Release: %{momorel}m%{?dist}
License:        GPLv2+ and GPLv2 and MIT
# GPLv2+ - libpurple, gnt, finch, pidgin, most prpls
# GPLv2 - silc & novell prpls
# MIT - Zephyr prpl
Group:          Applications/Internet
URL:            http://pidgin.im/
Source0:        http://downloads.sourceforge.net/pidgin/pidgin-%{version}.tar.bz2
NoSource: 0
Obsoletes:      gaim < 999:1
Provides:       gaim = 999:1

%if %{split_evolution}
Obsoletes:      pidgin <= 2.7.1-1%{?dist}
%endif

## Fedora pidgin defaults
# Only needs regenerating if Pidgin breaks backwards compatibility with prefs.xml
# 1) uninstall any non-default pidgin or libpurple plugins
# 2) run pidgin as new user 3) edit preferences 4) close 5) copy .purple/prefs.xml
# OR 1) edit manually
# - enable ExtPlacement plugin
# - enable History plugin
# - enable Message Notification plugin
#   Insert count of new messages into window title
#   Set window manager "URGENT" hint
# - disable buddy icon in buddy list
# - enable Logging (in HTML)
# - Browser "GNOME Default"
# - Smiley Theme "Default"
Source1:        purple-fedora-prefs.xml

## Patches 0-99: Fedora specific or upstream wont accept
Patch0:         pidgin-NOT-UPSTREAM-2.5.2-rhel4-sound-migration.patch

## Patches 100+: To be Included in Future Upstream
Patch100:       pidgin-2.10.1-fix-msn-ft-crashes.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-root
Summary:        A Gtk+ based multiprotocol instant messaging client

# Require Binary Compatible glib
# returns bogus value if glib2-devel is not installed in order for parsing to succeed
# bogus value wont make it into a real package
%global glib_ver %([ -a %{_libdir}/pkgconfig/glib-2.0.pc ] && pkg-config --modversion glib-2.0 | cut -d. -f 1,2 || echo -n "999")
BuildRequires:  glib2-devel
Requires:       glib2 >= %{glib_ver}
# Require exact libpurple
Requires:       libpurple%{?_isa} = %{version}-%{release}

Requires(pre):  GConf2
Requires(post): GConf2
Requires(preun): GConf2

# Basic Library Requirements
BuildRequires:  autoconf
BuildRequires:  libtool
BuildRequires:  cyrus-sasl-devel
%if %{nss_md2_disabled}
BuildRequires:  nss-devel >= 3.12.3
%else
BuildRequires:  nss-devel
%endif

%if ! %{build_only_libs}
BuildRequires:  startup-notification-devel
BuildRequires:  gtk2-devel
BuildRequires:  desktop-file-utils
BuildRequires:  ncurses-devel
# gtkspell integration (FC1+)
BuildRequires:  gtkspell-devel
# Evolution integration (FC3+, < F18)
%if ! %{disable_evolution}
BuildRequires:  evolution-data-server-devel
%endif
%endif

BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  tcl-devel
BuildRequires:  tk-devel
BuildRequires:  libxml2-devel

%if ! %{krb4_removed}
# krb5 needed for Zephyr (FC1+)
BuildRequires:  krb5-devel
%endif
# SILC integration (FC3+)
%if ! %{disable_silc}
BuildRequires:  libsilc-devel
%endif
# DBus integration (FC5+)
%if %{dbus_integration}
BuildRequires:  dbus-devel >= 0.60
BuildRequires:  python     >= 2.4
%endif
# GStreamer integration (FC5+)
%if %{gstreamer_integration}
BuildRequires:  gstreamer-devel >= 0.10
%endif
# NetworkManager integration (FC5+)
%if %{nm_integration}
BuildRequires:  NetworkManager-glib-devel
%endif
# Modular X (FC5+)
%if %{modular_x}
BuildRequires:  libSM-devel
BuildRequires:  libXScrnSaver-devel
%endif
# Preferred Applications (xdg for FC6+)
%if %{use_gnome_open}
Requires:       libgnome
%else
Requires:       xdg-utils
%endif
# DBus GLIB Split (FC6+)
%if %{dbus_glib_splt}
BuildRequires:  dbus-glib-devel >= 0.70
%endif
%if %{bonjour_support}
BuildRequires:  avahi-glib-devel
%endif
# Meanwhile integration (F6+)
%if %{meanwhile_integration}
BuildRequires:  meanwhile-devel
%endif
# Perl devel separated out (F7+)
%if %{perl_devel_separated}
BuildRequires:  perl-devel >= 5.16.1
%endif
# Perl embed separated out (F9+)
%if %{perl_embed_separated}
BuildRequires:  perl(ExtUtils::Embed)
%endif
# Voice and video support (F11+)
%if %{vv_support}
%if 0%{?fedora} >= 17
BuildRequires:  farstream-devel
%else
BuildRequires:  farsight2-devel
%endif
Requires:       gstreamer-plugins-good
%if 0%{?fedora} >= 12
Requires:       gstreamer-plugins-bad
%endif
%endif
# libidn punycode domain support (F11+)
%if %{libidn_support}
BuildRequires:  libidn-devel
%endif
# use system libgadu (F13+)
%if %{use_system_libgadu}
BuildRequires:  libgadu-devel
%endif

%if %{api_docs}
BuildRequires:  doxygen
%endif

%description
Pidgin allows you to talk to anyone using a variety of messaging
protocols including AIM, MSN, Yahoo!, Jabber, Bonjour, Gadu-Gadu,
ICQ, IRC, Novell Groupwise, QQ, Lotus Sametime, SILC, Simple and
Zephyr.  These protocols are implemented using a modular, easy to
use design.  To use a protocol, just add an account using the
account editor.

Pidgin supports many common features of other clients, as well as many
unique features, such as perl scripting, TCL scripting and C plugins.

Pidgin is not affiliated with or endorsed by America Online, Inc.,
Microsoft Corporation, Yahoo! Inc., or ICQ Inc.

%if %{split_evolution}
%package evolution
Summary:    Pidgin Evolution integration plugin
Group:      Applications/Internet
Requires:   %{name} = %{version}-%{release}
Obsoletes:  pidgin <= 2.7.1-1%{?dist}

%description evolution
This package contains the Evolution integration plugin for Pidgin.

%endif


%package devel
Summary:    Development headers and libraries for pidgin
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}
Requires:   libpurple-devel = %{version}-%{release}
Requires:   pkgconfig
Requires:   gtk2-devel
Obsoletes:  gaim-devel
Provides:   gaim-devel = %{version}-%{release}


%description devel
The pidgin-devel package contains the header files, developer
documentation, and libraries required for development of Pidgin scripts
and plugins.

%package perl
Summary:    Perl scripting support for Pidgin
Group:      Applications/Internet
Requires:   libpurple = %{version}-%{release}
Requires:   libpurple-perl = %{version}-%{release}

%description perl
Perl plugin loader for Pidgin. This package will allow you to write or
use Pidgin plugins written in the Perl programming language.


%package -n libpurple
Summary:    libpurple library for IM clients like Pidgin and Finch
Group:      Applications/Internet
# Ensure elimination of gaim.i386 on x86_64
Obsoletes:  gaim < 999:1
%if %{meanwhile_integration}
Obsoletes:  gaim-meanwhile
%endif
Requires:   glib2 >= %{glib_ver}
# Bug #212817 Jabber needs cyrus-sasl plugins for authentication
Requires:   cyrus-sasl-plain, cyrus-sasl-md5
# Use system SSL certificates (F11+)
%if %{use_system_certs}
Requires:   ca-certificates
%endif
# Workaround for accidental shipping of pidgin-docs
%if 0%{?rhel} == 5
Obsoletes:  pidgin-docs = 2.5.2
%endif

Obsoletes:  libpurple-bonjour
%if %{build_only_libs}
Obsoletes:  finch
Obsoletes:  %{name}
Obsoletes:  %{name}-perl
%endif

%description -n libpurple
libpurple contains the core IM support for IM clients such as Pidgin
and Finch.

libpurple supports a variety of messaging protocols including AIM, MSN,
Yahoo!, Jabber, Bonjour, Gadu-Gadu, ICQ, IRC, Novell Groupwise, QQ,
Lotus Sametime, SILC, Simple and Zephyr.


%package -n libpurple-devel
Summary:    Development headers, documentation, and libraries for libpurple
Group:      Applications/Internet
Requires:   libpurple = %{version}-%{release}
Requires:   pkgconfig
%if %{dbus_integration}
Requires:   dbus-devel >= 0.60
%endif
%if %{dbus_glib_splt}
Requires:   dbus-glib-devel >= 0.70
%endif

%if %{build_only_libs}
Obsoletes:  finch-devel
Obsoletes:  %{name}-devel
%endif

%description -n libpurple-devel
The libpurple-devel package contains the header files, developer
documentation, and libraries required for development of libpurple based
instant messaging clients or plugins for any libpurple based client.

%package -n libpurple-perl
Summary:    Perl scripting support for libpurple
Group:      Applications/Internet
Requires:   libpurple = %{version}-%{release}
Requires:   perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description -n libpurple-perl
Perl plugin loader for libpurple. This package will allow you to write or
use libpurple plugins written in the Perl programming language.


%package -n libpurple-tcl
Summary:    Tcl scripting support for libpurple
Group:      Applications/Internet
Requires:   libpurple = %{version}-%{release}

%description -n libpurple-tcl
Tcl plugin loader for libpurple. This package will allow you to write or
use libpurple plugins written in the Tcl programming language.


%package -n finch
Summary:    A text-based user interface for Pidgin
Group:      Applications/Internet
Requires:   glib2 >= %{glib_ver}
Requires:   libpurple = %{version}-%{release}

%description -n finch
A text-based user interface for using libpurple.  This can be run from a
standard text console or from a terminal within X Windows.  It
uses ncurses and our homegrown gnt library for drawing windows
and text.


%package -n finch-devel
Summary:    Headers etc. for finch stuffs
Group:      Applications/Internet
Requires:   finch = %{version}-%{release}
Requires:   libpurple-devel = %{version}-%{release}
Requires:   pkgconfig
Requires:   ncurses-devel

%description -n finch-devel
The finch-devel package contains the header files, developer
documentation, and libraries required for development of Finch scripts
and plugins.

%if %{api_docs}
%package -n pidgin-docs
Summary:    API docs for pidgin and libpurple
Group:      Applications/Internet
Requires:   pidgin = %{version}-%{release}
Provides:   libpurple-docs = %{version}-%{release}

%description -n pidgin-docs
Doxygen generated API documentation.

%endif

%prep
echo "FEDORA=%{fedora} RHEL=%{rhel}"
%setup -q
## Patches 0-99: Fedora specific or upstream wont accept
%if %{force_sound_aplay}
%patch0 -p1 -b .aplay
%endif

## Patches 100+: To be Included in Future Upstream

# http://pidgin.im/pipermail/devel/2011-November/010477.html
%patch100 -p0 -R -b .ftcrash

# Our preferences
cp %{SOURCE1} prefs.xml

# RHEL5 and earlier did not have xdg-open, so use gnome-open instead
if [ "%{use_gnome_open}" == "1" ]; then
    sed -i "s/value='xdg-open'/value='gnome-open'/" prefs.xml
fi

# Bug #528796: Get rid of #!/usr/bin/env python
# Upstream refuses to use ./configure --python-path= in these scripts.
for file in finch/plugins/pietray.py libpurple/purple-remote libpurple/plugins/dbus-buddyicons-example.py \
            libpurple/plugins/startup.py libpurple/purple-url-handler libpurple/purple-notifications-example; do
    sed -i 's/env python/python/' $file
done

%build
SWITCHES="--with-extraversion=%{release}"
%if ! %{krb4_removed}
    SWITCHES="$SWITCHES --with-krb4"
%endif
    SWITCHES="$SWITCHES --enable-perl"
%if ! %{disable_evolution}
    SWITCHES="$SWITCHES --enable-gevolution"
%else
	SWITCHES="$SWITCHES --disable-gevolution"
%endif
%if %{dbus_integration}
    SWITCHES="$SWITCHES --enable-dbus"
%else
    SWITCHES="$SWITCHES --disable-dbus"
%endif
%if %{nm_integration}
    SWITCHES="$SWITCHES --enable-nm"
%endif
%if %{gstreamer_integration}
    SWITCHES="$SWITCHES --enable-gstreamer"
%else
    SWITCHES="$SWITCHES --disable-gstreamer"
%endif
%if ! %{bonjour_support}
    SWITCHES="$SWITCHES --disable-avahi"
%endif
%if ! %{meanwhile_integration}
    SWITCHES="$SWITCHES --disable-meanwhile"
%endif
%if ! %{libidn_support}
    SWITCHES="$SWITCHES --disable-idn"
%endif
%if ! %{vv_support}
    SWITCHES="$SWITCHES --disable-vv"
%endif
%if %{use_system_certs}
    SWITCHES="$SWITCHES --with-system-ssl-certs=/etc/pki/tls/certs"
%endif
%if %{build_only_libs}
    SWITCHES="$SWITCHES --disable-consoleui --disable-gtkui"
%endif

# FC5+ automatic -fstack-protector-all switch
export RPM_OPT_FLAGS=${RPM_OPT_FLAGS//-fstack-protector/-fstack-protector-all}
export CFLAGS="$RPM_OPT_FLAGS"

# gnutls is buggy so use mozilla-nss on all distributions
%configure --enable-gnutls=no --enable-nss=yes --enable-cyrus-sasl \
           --enable-tcl --enable-tk \
           --disable-schemas-install $SWITCHES

%make  LIBTOOL=/usr/bin/libtool

# one_time_password plugin, included upstream but not built by default
cd libpurple/plugins/
make one_time_password.so LIBTOOL=/usr/bin/libtool
cd -

%if %{api_docs}
make docs
find doc/html -empty -delete
%endif

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install LIBTOOL=/usr/bin/libtool

install -m 0755 libpurple/plugins/one_time_password.so %{buildroot}%{_libdir}/purple-2/

%if ! %{build_only_libs}
desktop-file-install --vendor pidgin --delete-original              \
                     --add-category X-Red-Hat-Base                  \
                     --dir %{buildroot}%{_datadir}/applications  \
                     %{buildroot}%{_datadir}/applications/pidgin.desktop
%endif

# remove libtool libraries and static libraries
find %{buildroot} \( -name "*.la" -o -name "*.a" \) -exec rm -f {} ';'
# remove the perllocal.pod file and other unrequired perl bits
find %{buildroot} -type f -name perllocal.pod -exec rm -f {} ';'
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -empty -exec rm -f {} ';'
# remove relnot.so plugin since it is unusable for our package
rm -f %{buildroot}%{_libdir}/pidgin/relnot.so
# remove dummy nullclient
rm -f %{buildroot}%{_bindir}/nullclient
# install Fedora pidgin default prefs.xml
mkdir -p %{buildroot}%{_sysconfdir}/purple/
install -m 644 prefs.xml %{buildroot}%{_sysconfdir}/purple/prefs.xml

# remove non-plugin unrequired library symlinks
rm -f %{buildroot}%{_libdir}/purple-2/liboscar.so
rm -f %{buildroot}%{_libdir}/purple-2/libjabber.so
rm -f %{buildroot}%{_libdir}/purple-2/libymsg.so

# make sure that we can write to all the files we've installed
# so that they are properly stripped
chmod -R u+w %{buildroot}/*

%find_lang pidgin

%if ! %{build_only_libs}
# symlink /usr/bin/gaim to new pidgin name
ln -sf pidgin %{buildroot}%{_bindir}/gaim
%endif

%if %{api_docs}
rm -rf html
rm -f doc/html/installdox
mv doc/html/ html/
mkdir -p %{buildroot}%{_datadir}/gtk-doc/html/
ln -sf ../../doc/pidgin-docs-%{version}/html/ \
    %{buildroot}%{_datadir}/gtk-doc/html/pidgin
%endif

%if %{build_only_libs}
rm -f %{buildroot}%{_sysconfdir}/gconf/schemas/purple.schemas
%endif

%if ! %{build_only_libs}
%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
        %{_sysconfdir}/gconf/schemas/purple.schemas >/dev/null || :
    killall -HUP gconfd-2 &> /dev/null || :
fi

%post
touch --no-create %{_datadir}/icons/hicolor || :
[ -x %{_bindir}/gtk-update-icon-cache ] && \
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/purple.schemas > /dev/null || :
killall -HUP gconfd-2 &> /dev/null || :

%post -n finch -p /sbin/ldconfig
%endif

%post -n libpurple -p /sbin/ldconfig

%if ! %{build_only_libs}
%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
        %{_sysconfdir}/gconf/schemas/purple.schemas > /dev/null || :
    killall -HUP gconfd-2 &> /dev/null || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor || :
[ -x %{_bindir}/gtk-update-icon-cache ] && \
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun -n finch -p /sbin/ldconfig
%endif

%postun -n libpurple -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%if ! %{build_only_libs}
%files
%defattr(-,root,root,-)
%doc NEWS COPYING AUTHORS README ChangeLog doc/PERL-HOWTO.dox
%{_bindir}/pidgin
%{_bindir}/gaim
%{_libdir}/pidgin/
%exclude %{_libdir}/pidgin/perl
%if %{split_evolution}
%exclude %{_libdir}/pidgin/gevolution.so
%endif
%{_mandir}/man1/pidgin.*
%{_datadir}/applications/pidgin.desktop
%{_datadir}/pixmaps/pidgin/
%{_datadir}/icons/hicolor/*/apps/pidgin.*
%{_sysconfdir}/gconf/schemas/purple.schemas

%if %{split_evolution}
%files evolution
%defattr(-,root,root,-)
%{_libdir}/pidgin/gevolution.so
%endif

%files perl
%defattr(-,root,root,-)
%{_mandir}/man3/Pidgin*
%{_libdir}/pidgin/perl/

%files devel
%defattr(-,root,root,-)
%{_includedir}/pidgin/
%{_libdir}/pkgconfig/pidgin.pc
%endif

%files -f pidgin.lang -n libpurple
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/purple-2/
%exclude %{_libdir}/purple-2/perl
%{_libdir}/libpurple.so.*
%{_datadir}/sounds/purple/
%{_datadir}/purple
%dir %{_sysconfdir}/purple
%config(noreplace) %{_sysconfdir}/purple/prefs.xml
%if %{dbus_integration}
%{_bindir}/purple-client-example
%{_bindir}/purple-remote
%{_bindir}/purple-send
%{_bindir}/purple-send-async
%{_bindir}/purple-url-handler
%{_libdir}/libpurple-client.so.*
#%%{_datadir}/dbus-1/services/pidgin.service
%doc libpurple/purple-notifications-example
%endif
%exclude %{_libdir}/purple-2/tcl.so
%exclude %{_libdir}/purple-2/perl.so
%exclude %{_libdir}/purple-2/perl/

%files -n libpurple-devel
%defattr(-,root,root,-)
%{_datadir}/aclocal/purple.m4
%{_libdir}/libpurple.so
%{_includedir}/libpurple/
%{_libdir}/pkgconfig/purple.pc
%if %{dbus_integration}
%{_libdir}/libpurple-client.so
%endif

%files -n libpurple-perl
%defattr(-,root,root,-)
%{_mandir}/man3/Purple*
%{_libdir}/purple-2/perl.so
%{_libdir}/purple-2/perl/

%files -n libpurple-tcl
%defattr(-,root,root,-)
%{_libdir}/purple-2/tcl.so

%if ! %{build_only_libs}
%files -n finch
%defattr(-,root,root,-)
%{_bindir}/finch
%{_libdir}/finch/
%{_libdir}/gnt/
%{_libdir}/libgnt.so.*
%{_mandir}/man1/finch.*

%files -n finch-devel
%defattr(-,root,root,-)
%{_includedir}/finch/
%{_includedir}/gnt/
%{_libdir}/libgnt.so
%{_libdir}/pkgconfig/gnt.pc
%{_libdir}/pkgconfig/finch.pc
%endif

%if %{api_docs}
%files -n pidgin-docs
%defattr(-,root,root,-)
%doc html
%{_datadir}/gtk-doc/html/*
%endif

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.6-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.6-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.6-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.6-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.6-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.6-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.6-4m)
- rebuild against perl-5.16.1

* Wed Aug  1 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.6-3m)
- fix up Obsoletes

* Tue Jul 31 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.6-2m)
- enable pidgin client
- disable evolution plugin

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.6-1m)
- update to 2.10.6
- enable evolution plugin

* Tue Jul 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.5-2m)
- set some Obsoletes

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.5-1m)
- reimport from fedora
- temporary disable evolution plugin for a while

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.0-3m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Fri Mar 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.0-2m)
- [SECURITY] CVE-2011-4602 CVE-2011-4603 CVE-2011-4601
- [SECURITY] CVE-2011-4939 CVE-2012-1178
- all issues are fixed in 10.0.1 and 10.0.2

* Sat Dec 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.0-1m)
- update to 2.10.0

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.0-1m)
- update to 2.9.0

* Wed Jun  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.0-1m)
- update to 2.8.0

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.11-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.11-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.11-1m)
- update to 2.7.11
- [SECURITY] CVE-2011-1091

* Tue Feb 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.10-1m)
- update to 2.7.10
- fix security issue

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.8-1m)
- update to 2.7.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.7-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.7-1m)
- update 2.7.7

* Sat Nov 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.5-3m)
- BR tk-devel

* Sat Nov 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.5-2m)
- BR tcl-devel perl-devel

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.5-1m)
- update 2.7.5
- [SECURITY] CVE-2010-3711 

* Wed Sep 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.3-3m)
- drop dependencies on enchant
-- enchant support is available on ms windows only

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.3-2m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.3-1m)
- [SECURITY] CVE-2010-2528
- update to 2.7.3

* Fri Jul 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.2-1m)
- [SECURITY] CVE-2010-2528
- update 2.7.2

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.1-3m)
- fix build

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-2m)
- rebuild against libsilc-1.1.10

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-1m)
- update 2.7.1

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-1m)
- [SECURITY] CVE-2010-1624
- update to 2.7.0

* Tue May 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.6-3m)
- add BuildRequires

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.6-2m)
- modify __os_install_post for new momonga-rpmmacros

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.6-1m)
- [SECURITY] CVE-2010-0277 CVE-2010-0420 CVE-2010-0423
- update to 2.6.6

* Thu Jan 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.5-1m)
- [SECURITY] CVE-2010-0013
- update 2.6.5

* Wed Dec 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.4-1m)
- update 2.6.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.3-1m)
- [SECURITY] CVE-2009-3615
- update to 2.6.3

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.2-1m)
- [SECURITY] CVE-2009-2703 CVE-2009-3083 CVE-2009-3084 CVE-2009-3085
- update to 2.6.2

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.1-1m)
- [SECURITY] CVE-2009-3025 CVE-2009-3026
- update to 2.6.1

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.9-1m)
- [SECURITY] CVE-2009-2694
- update to 2.5.9 for mo6+

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.8-1m)
- [SECURITY] CVE-2009-1889
- update 2.5.8

* Sun Jun 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.7-1m)
- update 2.5.7

* Sun May 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.6-2m)
- add 2.5.6-jp patch

* Thu May 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.6-1m)
- [SECURITY] CVE-2009-1373 CVE-2009-1374 CVE-2009-1375 CVE-2009-1376
- update 2.5.6

* Sun Mar  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.5-2m)
- fix BuildPreReq

* Sat Mar  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.5-1m)
- update 2.5.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.4-2m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.4-1m)
- update 2.5.4

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.3-2m)
- rebuild against python-2.6.1-2m

* Wed Dec 24 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.3-1m)
- update 2.5.3

* Tue Oct 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.2-1m)
- update 2.5.2

* Wed Sep 10 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.1-1m)
- update 2.5.1

* Thu Aug 28 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.0-1m)
- [SECURITY] CVE 2008-2957 CVE-2008-3532
- update 2.5.0

* Wed Jul 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-3m)
- rebuild against libsilc-1.1.7

* Tue Jul 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-2m)
- rebuild against gnutls-2.4.1

* Sat Jul  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.3-1m)
- [SECURITY] CVE-2008-2927 CVE-2008-2955
- update 2.4.3

* Sun Jun  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-3m)
- add Prereq: hicolor-icon-theme

* Sat May 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-2m)
- add BPR gtkspell-devel avahi-glib-devel NetworkManager-devel

* Tue May 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-1m)
- update 2.4.2

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.1-3m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.1-2m)
- rebuild against gcc43

* Wed Apr  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.1-1m)
- update 2.4.1

* Thu Mar 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.0-2m)
- BPR avahi-devel

* Mon Mar  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.0-1m)
- update 2.4.0
- add jp patch

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-2m)
- %%NoSource -> NoSource

* Tue Nov 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-1m)
- update 2.3.0

* Tue Oct 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.2-1m)
- update 2.2.2
-- fix CVE-2007-4999

* Tue Oct  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-1m)
- update 2.2.1
-- fix CVE-2007-4996

* Sun Sep 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-1m)
- update 2.2.0

* Tue Aug 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-1m)
- update 2.1.1

* Tue Aug 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-1m)
- update 2.1.0
- but no japanese patch

* Fri Jul 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-3m)
- fix %%pre, %%post and %%preun scripts

* Wed Jun 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-2m)
- update japanese-patch

* Sat Jun 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-1m)
- update 2.0.2

* Fri Jun  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-2m)
- add patch
-- http://www.honeyplanet.jp/patch.html

* Thu Jun  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1

* Mon May  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-2m)
- fix duplicate files
- Obsoletes: gaim-galago
-- gaim-galago unsupport pidgin

* Mon May  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- rename pidgin
- update 2.0.0

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-0.4.4m)
- rebuild against evolution-data-server-1.10.0
- revise version and release

* Sat Dec 23 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0beta4-3m)
- use nspr-devel and nss-devel instead of mozilla-devel
-- mozilla-psm is no longer required

* Wed Dec 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0beta4-2m)
- fix %%file section

* Tue Dec 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0beta4-1m)
- update 2.0.0beta4

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.5.0-8m)
- rebuild against gnutls-1.4.4-1m

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.0-7m)
- rebuild against evolution-data-server-1.8.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.5.0-6m)
- rebuild against expat-2.0.0-1m

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.0-5m)
- delete duplicated dir

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.0-4m)
- rebuild against evolution-data-server 1.6.0

* Sat Feb 25 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.0-3m)
- build with new nspr/nss

* Wed Jan  4 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.0-2m)
- rebuild against evolution-data-server

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.5.0-1m)
- up to 1.5.0
- CAN-2005-2102, CAN-2005-2103

* Mon Aug  1 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- up to 1.4.0
- [SECURITY] CAN-2005-2370

* Thu May 19 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0
- many bug fixes and many security fixes
-  long URL crash fix CAN-2005-1261
-  MSN bad messages crash fix CAN-2005-1262
-  drag-n-drop URL crash fix
-  CAN-2005-0965 CAN-2005-0966 CAN-2005-0967

* Thu Mar 24 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.0-1m)
- updated to 1.2.0

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.2-4m)
- enable x86_64.

* Tue Dec 28 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.0.2-3m)
- add Requires mozilla-psm

* Fri Dec 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.2-2m)
- rebuild against mozilla-1.7.5

* Fri Oct 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.2-1m)
- version 1.0.2

* Sun Sep 19 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.0.0-2m)
- change mozilla_ver

* Sun Sep 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.0-1m)
- update to 1.0.0

* Tue Aug 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.82.1-1m)
- major security fixes and major bugfixes

* Mon Aug 23 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (0.81-1m)
- ver up
- remove --enable-screensaver option. Because configure autodetects it.

* Sat Aug  7 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.77-4m)
- rebuild against mozilla-1.7.2

* Mon Jul 18 2004 Yuya Yamaguchi <bebe@hepo.jp>
- (0.77-3m)
- rebuild against mozilla 1.7

* Mon Jun 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.77-2m)
- rebuild against tcl,tk

* Sat May 22 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.77-1m)
- update to 0.77

* Sat Mar 20 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.74-4m)
- change docdir %%defattr

* Thu Jan 22 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.74-3m)
- rebuild against mozilla-1.6

* Wed Dec 17 2003 Kenta MURATA <muraken2@nifty.com>
- (0.74-2m)
- sepalate devel package.
- modify dependencies.

* Mon Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.74-1m)
- update to 0.74

* Fri Oct 31 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.71-3m)
- add -Wl,-rpath,%{_libdir}/mozilla-%{mozilla_ver}  for libnss3.so

* Thu Oct 30 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.71-2m)
- add jp patch
- enable nss

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.71-1m)
- version 0.71

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.68-1m)
- version 0.68

* Sat Jun 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.64-1m)
- version 0.64

* Mon Apr  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.60-1m)
- update to 0.60

* Sat Mar 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.59.8-2m)
- rebuild against for XFree86-4.3.0

* Thu Feb 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.59.8-1m)
- update to 0.59.8

* Sun Sep 15 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.59.3-1m)
- version up

* Mon Sep 02 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.59.1-2m)
- unapply euc2 patch to fix build error

* Wed Aug 28 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.59.1-1m)
- version udpate
- unapply ja.patch and Makefile.patch to fix compilation errors

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.56-4k)
- cancel gcc-3.1 autoconf-2.53

* Tue May 28 2002 Toru Hoshina <t@kondara.org>
- (0.56-2k)
- force autoconf-old.

* Wed Apr 24 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.56-1k)
- kondarize

* Tue Apr 23 2002 Ryosuke Kutsuna <ryosuke@cool.email.ne.jp>
- Add Japanese Message catalog.

* Thu Apr 11 2002 Rob Flynn <rob@marko.net (0.56 release)
- German translation update (Thanks Karsten Weiss)
- Shell-like send history binded to Ctrl-Up and Ctrl-Down
- Russian Translation Updated (thanks Grigory Bakunov)
- libjabber upgraded to most recent stable version
- Buddylist looks a little better
- Fixed MSN privacy settings
- Group deletion fix (Thanks Mark Doliner)
- Alias/Group syncronization for Jabber (Thanks JSeymour)
- Fixed broken signal handling in gdm-started GNOME sessions (Thanks Jim Seymour, Vann, Robert McQueen)

* Fri Mar 29 2002 Rob Flynn <rob@marko.net> (0.55 release)
- Jabber improvements (Thanks Jim Seymour)
- Various sound cleanups (Thanks Robert McQueen)
- Login process shown in single window (Thanks Michael Golden)
- Can reorder your accounts in the account editor (Thanks Luke Schierer)
- Updated Dutch translation
- Shows "mobile" icon for Oscar buddies using mobile devices (Thanks Mark Doliner)
- Fixed bug in MSN smilies that crashed PPC (and other?) platforms
- Updated Polish Translation (Thanks Przemyslaw Sulek)
- Updated Spanish Translation (Thanks Amaya)
- Updated French translation
- Updated Finnish translation (Thanks Tero Kuusela)
- HTTP Proxy settings now HTTP compliant (Thanks Robert McQueen)
- Speling corections (Thanks Tero Kuusela)
- Oscar list icon fixes (Thanks Mark Doliner)
- Oscar idle times work again (Thanks Mark Doliner)
- Protocol icons on Edit Buddies tab (Thanks Christian Hammond)

* Thu Mar 14 2002 Rob Flynn <rob@marko.net> (0.54 release)
- Compiles without GdkPixbuf again
- GtkIMHtml will refresh when you set a new GTK+ theme
- Improved Yahoo! typing notification (thanks Brian Macke)
- Prompt to authorize MSN buddies who added you while you were offline (Thanks Jason Willis)
- Option to globally disable Buddy Icon animation (Thanks Luke Schierer)
- Numerous bugfixes
- Yahoo! will tell you when your buddies are playing Yahoo! games and give you the ability to join them
- Yahoo! can receive offline messages
- IRC can do DCC chat.
- IRC will convert HTML formatting to mIRC formatting.
- Buddylist tab placement option (Thanks Jason Willis)
- Protocol specific smiley faces
- Italian translation added
- IM Image sending

* Fri Feb 28 2002 Rob Flynn <rob@marko.net> (0.53 release)
- Updated Polish Translation (thanks Przemyslaw Sulek)
- Slovak translation added (Thanks Daniel Rezny)
- Minor bug fixes re: queued away messages
- Better buddy icon transparency (for real this time ;-))
- Ability to change formatting of Oscar screen name
- Better selection in HTML widget (Thanks BMiller)
- New icons for ICQ (Thanks Kevin Miller)
- Editable buddy pounces (Thanks Jason Willis)
- Server side buddy lists in Oscar (Thanks KingAnt :-))
- Fix for the chatlist plugin
- Typing Notification (AIM Direct Connect, Yahoo, MSN)
- IM Images (Receive Only)
- Prettier GtkImHtml selection

* Sun Feb 17 2002 Rob Flynn <rob@marko.net> (0.52 release)
- Better buddy icon transparency (thanks SeanEgan)
- Updated Polish Translation (thanks Przemyslaw Sulek)
- Fixed a little bug with connecting via proxy (thanks for reminding me of this, Manish Singh)
- Updated Simplified Chinese Translation (Thanks Rocky S. Lee)
- Updated German Translation (Thanks Karsten Weiss)
- Yahoo! Messenger works again
- MSN Works again
- Can register a new user with a Jabber Server (JSeymour)
- Can now set Jabber vCards (JSeymour)
- Jabber vCards are now shown in their entirety (JSeymour)
- Various jabber bug fixes/enhancements (JSeymour)

* Thu Jan 24 2002 Rob Flynn <rob@marko.net> (0.51 release)
- Arrow buttons in log viewer and some other dialogs work (thanks Ben Miller)
- Option to only send auto-response while idle (thanks Sean Egan)
- Control time between sending auto-responses (thanks Mark Doliner)
- Should be able to sign on to Oscar using Mac OS X (thanks Fingolfin, Vincas Ciziunas, et al.)
- Finnish translation added (Thanks Tero Kuusela)
- Updated French Translation (Thanks sebfrance)

* Thu Dec 13 2001 Rob Flynn <rob@marko.net> (0.50 release)
- Updated polish translation (Thanks Przemyslaw Sulek)
- Able to import GnomeICU contact lists
- Galeon as browser option (Thanks Rob McQueen)
- IRC /list, /invite (Thanks Sean Egan)
- Added swedish translation (Thanks Christian Rose)
- Option to have IMs and Chats tabbed in same window
- Finally put the lagmeter plugin out of its misery and removed it. (/me dances on its grave.)

* Thu Nov 29 2001 Rob Flynn <rob@marko.net> (0.49 release)
- Can compile against GTK+ 2.0 (version 1.3.10/1.3.11)
- Confirm before removing buddies
- Updated Russian translation (thanks Grigory Bakunov)
- Updated Korean translation (thanks Ho-seok Lee, also for resized ICQ icons)
- Updated Dutch translation (thanks Floris Eshuis)
- Yahoo updates (thanks Brian Macke)
- Jabber updates
- Zephyr updates (thanks Arun A Tharuvai)
- Gadu-Gadu updates (thanks Arkadiusz Miskiewicz)
- Option to show aliases in conversation tabs
- Option to hide windows after sending messages
- licq2gaim.pl conversion script (thanks Arturo Cisneros, Jr.)

* Thu Nov 18 2001 Rob Flynn <rob@marko.net> (0.48 release)
- Right-click on links to open/copy URL
- Yahoo changes
- Oscar can send/receive offline messages in ICQ. Since the "real" ICQ protocol isn't working too well it's recommended that you use Oscar for ICQ.

* Thu Nov 01 2001 Rob Flynn <rob@marko.net> (0.47 release)
- Better font loading (pays attention to charset now) (thanks Arkadiusz Miskiewicz)
- Better recoding in Gadu-Gadu (thanks Arkadiusz Miskiewicz)
- Open Mail button for when you get new mail (Yahoo and MSN)
- New buddy pounce option: Popup Notification
- When adding a buddy, the groups list now updates when you switch accounts.
- When creating a new buddy pounce, gaim now automagically selects "on away" or "on idle", if the user is away or idle.
- Add Opera to the available browsers (thanks Brian Enigma)
- Improved log viewer (thanks to Ben Miller)
- When you are queueing away messages, double clicking on a buddy's name will cause the messages for that name to be dequeued.
- You can choose which sound player you use at run-time (thanks Ben Miller)
- When someone adds you to their buddy list, it asks if you want to add them as well (Yahoo, ICQ, and MSN) (thanks Nathan Walp)
- Option to grey idle buddies (thanks Nathan Walp)
- MSN Privacy Options
- In MSN you can set a person's alias to their "friendly name" by right-click on their name while they're online.
- IRC can do /WHOIS
- The usual bug fixes and memory leak plugs
- Added Dutch translation
- Updated Korean translation

* Thu Oct 18 2001 Rob Flynn <rob@marko.net> (0.46 release)
- New applet icons (courtesy David Raeman)
- ICQ works on big-endian platforms, e.g. sparc and ppc (thanks to Nathan Walp and Ben Miller)
- Better applet icon drawing (thanks to Ari Pollak)
- An extraordinary number of bug fixes
- Updated Korean translation
- Ability to stop animation on buddy icons, restart animation, hide certain buddy icons, and save people's buddy icons, all through a right-click menu
- Event handlers in perl passed arguments as elements of an array rather than all concatenated as a string, making perl much easier to use (thanks Dennis Lambe Jr.)
- Can pass an argument to timeout_handlers in perl (thanks Artem Litvinovich)
- Redesigned Modify Account window (thanks Sean Egan)
- Add buddy dialog now lets you select which protocol to add the buddy to
- Pressing 'signon' on the first screen for accounts that do not require passwords no longer incorrectly displays an error message.

* Thu Oct 04 2001 Rob Flynn <rob@marko.net> (0.45 release)
- New plugin event: event_chat_send_invite
- Major updates to the perl system (reread PERL-HOWTO and SIGNALS)
- Major updates to event_chat_* events for plugins (reread SIGNALS)
- Some GtkIMHtml improvements
- Various bugfixes
- Nick Highlighting in chat
- Tab-completion for nicks in chat (thanks to Sean Egan)
- Large internal reworkings
- New Protocol: Gadu-Gadu, written by Arkadiusz Miskiewicz
- Can choose buddy icon to send (for Oscar)
- New Translation: Polish translation by Przemysaw Suek

* Thu Sep 20 2001 Rob Flynn <rob@marko.net> (0.44 release)
- More sane scaling of buddy icons (intelligently scale to either 48x48 or 50x50 depending on icon)
- Have you ever had it happen where you cancel a login and Gaim starts using all the available processing power? I think I fixed that.
- Temporarily removed Jabber user registration, which wasn't working anyway.
- Added a spiffy Help button
- Wrote a plugin for all those people who miss having the chat rooms in their buddy lists (chatlist.so)
- Updated libfaim
- Added drop down selection to chat invitation
- Improved the look of the chat invitation dialog
- Improved the look of the proxy preferences
- event_im_recv and event_im_display_rcvd passed whether the message received was auto-response (see SIGNALS)
- IRC fixes (largly copied from X-Chat)
- Internal change to how preferences are stored
- Other bug fixes
- Option to hide buddy icons

* Thu Sep 06 2001 Rob Flynn <rob@marko.net> (0.43 release)
- Updated German Translation (thanks Daniel Seifert)
- Can change friendly name in MSN again
- Bug fixes
- Auto-reconnect plugin has exponential timeout (i.e. it tries after 8 seconds, then 16, then 32, etc. up to 17 minutes)
- Removed file transfer things from Napster. It didn't work well anyway. It'll be back eventually. (Does anyone even use napster anymore?)
