%global momorel 24
# Note:  The tests for this perl dist. are disabled by default, as they
# require display access and would thus fail in the buildsys' mock
# environments.  To build locally while enabling tests, either:
#
#   rpmbuild ... --define '_with_display_tests 1' ...
#   rpmbuild ... --with display_tests ...
#   define _with_display_tests 1 in your ~/.rpmmacros
#
# Note that right now, the only way to run tests locally from a cvs sandbox
# "make noarch" type scenario is the third one.


Name:           perl-Gtk2-TrayIcon
Version:        0.06
Release:	%{momorel}m%{?dist}
Summary:        Perl interface to the EggTrayIcon library 

Group:          Development/Libraries
License:        LGPL 
URL:            http://search.cpan.org/dist/Gtk2-TrayIcon            
Source0: http://search.cpan.org/CPAN/authors/id/B/BO/BORUP/Gtk2-TrayIcon-%{version}.tar.gz 
NoSource: 0
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl
BuildRequires:  perl-Gtk2 >= 0.96, gtk2-devel
BuildRequires:  perl-ExtUtils-PkgConfig, perl-ExtUtils-Depends
Requires:  perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires:  perl-Gtk2 >= 0.96

%description
This module allows a Perl developer to embed an arbitrary widget in a System
Tray like the Gnome notification area.

%prep
%setup -q -n Gtk2-TrayIcon-%{version}


%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make pure_install PERL_INSTALL_ROOT=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
find %{buildroot} -type d -depth -exec rmdir {} 2>/dev/null ';'
chmod -R u+w %{buildroot}/*


%check
# see top of spec
%{?_with_display_tests: make test}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc TODO examples/*
%{perl_vendorarch}/auto/Gtk2/*
%{perl_vendorarch}/Gtk2/*
%{_mandir}/man3/*.3*


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-24m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-23m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-22m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-21m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-20m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-19m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-18m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-17m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.06-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-6m)
- rebuild against perl-5.10.1

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (0.06-5m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.06-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.06-2m)
- %%NoSource -> NoSource

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.03-4m)
- fix files dup

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.03-3m)
- use vendor

* Sat Mar 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.03-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.03-1m)
- import to Momonga from Fedora Development


* Thu Aug 31 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.03-3
- bump for mass rebuild

* Thu Aug 10 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.03-2
- bump for release

* Fri Jul 28 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.03-1
- Initial spec file for F-E
