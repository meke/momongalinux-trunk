%global momorel 4

# Use rpmbuild --without gcj to disable native bits
%define with_gcj %{!?_without_gcj:1}%{?_without_gcj:0}
%define origname libxml

Name: pentaho-libxml
Version: 1.1.3
Release: %{momorel}m%{?dist}
Summary: Namespace aware SAX-Parser utility library
License: LGPLv2+
Group: System Environment/Libraries
Source: http://dl.sourceforge.net/sourceforge/jfreereport/%{origname}-%{version}.zip
NoSource: 0
URL: http://reporting.pentaho.org/
BuildRequires: ant, ant-contrib >= 1.0-0.6.1m, ant-nodeps, java-devel, jpackage-utils, libbase >= 1.1.3, libloader >= 1.1.3
Buildroot: %{_tmppath}/%{origname}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: java, jpackage-utils, libbase >= 1.1.3, libloader >= 1.1.3
%if %{with_gcj}
BuildRequires: java-gcj-compat-devel >= 1.0.31
Requires(post): java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
%else
BuildArch: noarch
%endif
Patch0: libxml-1.1.2-build.patch

%description
Pentaho LibXML is a namespace aware SAX-Parser utility library. It eases the
pain of implementing non-trivial SAX input handlers.

%package javadoc
Summary: Javadoc for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
Requires: jpackage-utils
%if %{with_gcj}
BuildArch: noarch
%endif

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -c
%patch0 -p1 -b .build
find . -name "*.jar" -exec rm -f {} \;
mkdir -p lib
build-jar-repository -s -p lib commons-logging-api libbase libloader
cd lib
ln -s %{_javadir}/ant ant-contrib

%build
ant jar javadoc
for file in README.txt licence-LGPL.txt ChangeLog.txt; do
    tr -d '\r' < $file > $file.new
    mv $file.new $file
done

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p ./dist/%{origname}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}
pushd $RPM_BUILD_ROOT%{_javadir}
ln -s %{origname}-%{version}.jar %{origname}.jar
popd

mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{origname}
cp -rp bin/javadoc/docs/api $RPM_BUILD_ROOT%{_javadocdir}/%{origname}
%if %{with_gcj}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc licence-LGPL.txt README.txt ChangeLog.txt
%{_javadir}/%{origname}-%{version}.jar
%{_javadir}/%{origname}.jar
%if %{with_gcj}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{origname}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-1m)
- sync with Fedora 13 (1.1.3-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- sync with Fedora 11 (1.0.0-1.OOo31)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.11-2m)
- rebuild against rpm-4.6

* Thu Oct 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.11-1m)
- import from Fedora to Momonga for pentaho-reporting-flow-engine -> OOo-3

* Wed May 07 2008 Caolan McNamara <caolanm@redhat.com> 0.9.11-1
- initial fedora import
