%global momorel 1

%{!?__pear: %{expand: %%global __pear %{_bindir}/pear}}
%global pear_name Auth_RADIUS

Name:           php-pear-Auth-RADIUS
Version:        1.0.7
Release:        %{momorel}m%{?dist}
Summary:        Wrapper Classes for the RADIUS PECL
Group:          Development/Libraries
License:        BSD
URL:            http://pear.php.net/package/Auth_RADIUS
Source0:        http://pear.php.net/get/%{pear_name}-%{version}.tgz
NoSource:       0
Source2:        xml2changelog
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  php-pear >= 1:1.4.9-1.2
Requires:       php-pear(PEAR)
Requires:       php-pecl(radius) >= 1.2.5
Requires(post): %{__pear}
Requires(postun): %{__pear}
Provides:       php-pear(%{pear_name}) = %{version}

%description
This package provides wrapper-classes for the RADIUS PECL.
There are different Classes for the different authentication methods.
If you are using CHAP-MD5 or MS-CHAP you need also the Crypt_CHAP package.
If you are using MS-CHAP you need also the mcrypt extension.

%prep
%setup -qc
%{_bindir}/php -n %{SOURCE2} package.xml | tee CHANGELOG | head -n 10

# Create a "localized" php.ini to avoid build warning
cp /etc/php.ini .
echo "date.timezone=UTC" >>php.ini

cd %{pear_name}-%{version}
# package.xml is V2
mv ../package.xml %{name}.xml

%build
cd %{pear_name}-%{version}
# Empty build section, most likely nothing required.

%install
rm -rf $RPM_BUILD_ROOT
cd %{pear_name}-%{version}
PHPRC=../php.ini %{__pear} install --nodeps --packagingroot $RPM_BUILD_ROOT %{name}.xml

# Clean up unnecessary files
rm -rf $RPM_BUILD_ROOT%{pear_phpdir}/.??*

# Install XML package description
install -d $RPM_BUILD_ROOT%{pear_xmldir}
install -pm 644 %{name}.xml $RPM_BUILD_ROOT%{pear_xmldir}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%{__pear} install --nodeps --soft --force --register-only \
    %{pear_xmldir}/%{name}.xml >/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    %{__pear} uninstall --nodeps --ignore-errors --register-only \
        %{pear_name} >/dev/null || :
fi

%files
%defattr(-,root,root,-)
%doc CHANGELOG
%doc %{pear_docdir}/%{pear_name}
%{pear_xmldir}/%{name}.xml
%dir %{pear_phpdir}/Auth
%{pear_phpdir}/Auth/*

%changelog
* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-1m)
- import from Fedora for moodle-2.4.1

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.7-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.7-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Apr 06 2011 Remi Collet <Fedora@FamilleCollet.com> - 1.0.7-3
- doc in /usr/share/doc/pear

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Oct 26 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.0.7-1
- Version 1.0.7 (stable) - API 1.0.6 (stable) - QA release
- add generated Changelog
- define timezone during build

* Wed Aug 11 2009 Remi Collet <Fedora@FamilleCollet.com> - 1.0.6-5
- clean define
- fix description

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.6-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 13 2009 Remi Collet <Fedora@FamilleCollet.com> - 1.0.6-3
- remove mhash dependency (optional, and not provided by php 5.3.0)
- rename Auth_RADIUS.xml to php-pear-Auth-RADIUS.xml

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jun 19 2008 Christopher Stone <chris.stone@gmail.com> 1.0.6-1
- Initial Fedora release
