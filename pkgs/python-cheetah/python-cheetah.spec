%global momorel 1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           python-cheetah
Version:        2.4.4
Release:        %{momorel}m%{?dist}
Summary:        Template engine and code-generator

Group:          Development/Libraries
License:        MIT
URL:            http://cheetahtemplate.org/
Source0:        http://pypi.python.org/packages/source/C/Cheetah/Cheetah-%{version}.tar.gz
NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel >= 0.6c9

%description
Cheetah is an open source template engine and code generation tool,
written in Python. It can be used standalone or combined with other
tools and frameworks. Web development is its principle use, but
Cheetah is very flexible and is also being used to generate C++ game
code, Java, sql, form emails and even Python code.

%prep
%setup -q -n Cheetah-%{version}
# remove unnecessary shebang lines to silence rpmlint
%{__sed} -i -e '/^#!/,1d' cheetah/Tests/* \
	 cheetah/DirectiveAnalyzer.py cheetah/Utils/Misc.py

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%check
export PATH="%{buildroot}/%{_bindir}:$PATH"
export PYTHONPATH="%{buildroot}/%{python_sitearch}"
%{__python} %{buildroot}/%{python_sitearch}/Cheetah/Tests/Test.py

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES LICENSE README.markdown TODO

%{_bindir}/cheetah
%{_bindir}/cheetah-analyze
%{_bindir}/cheetah-compile

%dir %{python_sitearch}/Cheetah
%{python_sitearch}/Cheetah/*.py*
%{python_sitearch}/Cheetah/_namemapper.so

%dir %{python_sitearch}/Cheetah/Macros
%{python_sitearch}/Cheetah/Macros/*.py*

%dir %{python_sitearch}/Cheetah/Templates
%{python_sitearch}/Cheetah/Templates/*.py*
%{python_sitearch}/Cheetah/Templates/*.tmpl

%dir %{python_sitearch}/Cheetah/Tests
%{python_sitearch}/Cheetah/Tests/*.py*

%dir %{python_sitearch}/Cheetah/Tools
%{python_sitearch}/Cheetah/Tools/*.py*
%{python_sitearch}/Cheetah/Tools/*.txt

%dir %{python_sitearch}/Cheetah/Utils
%{python_sitearch}/Cheetah/Utils/*.py*

%dir %{python_sitearch}/Cheetah-%{version}-*.egg-info
%{python_sitearch}/Cheetah-%{version}-*.egg-info/PKG-INFO
%{python_sitearch}/Cheetah-%{version}-*.egg-info/*.txt

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.4-1m)
- update 2.4.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2.1-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2.1-1m)
- update to 2.4.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-3m)
- rebuild against python-2.6.1-2m
- apply Patch0 from Rawhide (2.0.1-4)

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-2m)
- rebuild against python-setuptools-0.6c9

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.1-1m)
- version up 2.0.1
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-0.5.rc8.2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0-0.5.rc8.1m)
- import from Fedora

* Mon Apr 23 2007 Mike Bonnet <mikeb@redhat.com> - 2.0-0.5.rc8
- update to 2.0rc8

* Mon Jan  8 2007 Mike Bonnet <mikeb@redhat.com> - 2.0-0.4.rc7
- use setuptools and install setuptools metadata

* Sun Dec 10 2006 Mike Bonnet <mikeb@redhat.com> - 2.0-0.3.rc7
- rebuild against python 2.5
- remove obsolete python-abi Requires:

* Mon Sep 11 2006 Mike Bonnet <mikeb@redhat.com> - 2.0-0.2.rc7
- un-%%ghost .pyo files

* Thu Jul 13 2006 Mike Bonnet <mikeb@redhat.com> - 2.0-0.1.rc7
- update to 2.0rc7
- change %%release format to conform to Extras packaging guidelines

* Sun May 21 2006 Mike Bonnet <mikeb@redhat.com> - 2.0-0.rc6.0
- update to 2.0rc6
- run the included test suite after install

* Thu Feb 16 2006 Mike Bonnet <mikeb@redhat.com> - 1.0-2
- Rebuild for Fedora Extras 5

* Wed Dec  7 2005 Mike Bonnet <mikeb@redhat.com> - 1.0-1
- Initial version
