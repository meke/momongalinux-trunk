%global momorel 4
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: omnipresent versatile search interface.
Name: deskbar-applet

Version: 2.32.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://raphael.slinckx.net/deskbar/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.32/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: pygtk2-devel >= 2.24
BuildRequires: pygobject-devel >= 2.16.1
BuildRequires: gnome-python2-devel >= 2.26.0
BuildRequires: gnome-desktop-devel >= 2.29.0
BuildRequires: dbus-python-devel >= 0.83.0
BuildRequires: gnome-python2-desktop-applet
BuildRequires: evolution-data-server-devel >= 2.32.0
Requires: gnome-python2-desktop-applet
Requires: gnome-python-gconf
Requires: GConf2
Requires(pre): hicolor-icon-theme gtk2

%description
The goal of DeskbarApplet is to provide an omnipresent versatile
search interface. By typing search terms into the deskbar entry in
your panel you are presented with the search results as you type.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
    --disable-schemas-install \
    --disable-scrollkeeper
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/deskbar-applet.schemas \
    > /dev/null || :

rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/deskbar-applet.schemas \
      > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/deskbar-applet.schemas \
      > /dev/null || :
fi

%postun
/sbin/ldconfig
rarian-sk-update

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README TODO
%{_sysconfdir}/gconf/schemas/%{name}.schemas
%{_libdir}/bonobo/servers/Deskbar_Applet.server
%{python_sitelib}/deskbar
%{_libexecdir}/deskbar-applet
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/gnome/help/deskbar
%{_datadir}/omf/deskbar

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.0-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-5m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-4m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-3m)
- rebuild against evolution-2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-2m)
- rebuild against gnome-desktop

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.28.0-2m)
- replace %%{python_sitearch} to %%{python_sitelib}

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-2m)
- add devel package

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Apr 17 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.26.1-2m)
- revised python directory of %%files section for lib64

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-2m)
- rebuild against gnome-desktop-2.25.90

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild against python-2.6.1-1m

* Sun Nov 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.3-3m)
- change %%preun script

* Mon Jul 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.22.3-2m)
- modify Requires: gnome-python2-desktop-applet

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2.1-1m)
- update to 2.22.2.1

* Tue Apr 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0.1-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0.1-1m)
- update to 2.22.0.1

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Wed Mar 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-4m)
- delete libtool library

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.16.2-3m)
- rebuild against python-2.5-9m
- delete deskbar-applet-2.16.0-configure.patch

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.2-2m)
- rebuild against python-2.5

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Wed Sep 13 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.16.0-2m)
- revise configure patch 

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sun Jul 23 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.14.2-4m)
- add BuildPreReq: gnome-python-desktop-applet

* Sat Jul 22 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2-3m)
- add Requires 

* Tue May 30 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.14.2-2m)
- revise configure patch

* Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Sat May 27 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1.1-2m)
- add icon image file (SOURCE1)
- add Patch1 (change icon file path)
- add Patch2 (change ui-name)
 
* Wed Apr 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1.1-1m)
- update to 2.14.1.1

* Sun Apr 16 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.14.1-2m)
- add configure patch for finding site-packages dir on lib64 env.

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- Initial Build
