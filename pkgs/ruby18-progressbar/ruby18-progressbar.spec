%global momorel 11
%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: a Text Progress Bar Library for Ruby
Name: ruby18-progressbar
Version: 0.8
Release: %{momorel}m%{?dist}
License: Ruby
Group: Development/Libraries
Source0: http://namazu.org/~satoru/ruby-progressbar/ruby-progressbar-%{version}.tar.gz
URL: http://namazu.org/~satoru/ruby-progressbar/
NoSource: 0
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18
Requires: ruby18

%description
Ruby/ProgressBar is a text progress bar library for Ruby. It can indicate
progress with percentage, a progress bar, and estimated remaining time.

%prep
%setup -q -n ruby-progressbar-%{version}

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{ruby18_sitelibdir}
%{__install} -c -m 644 progressbar.rb %{buildroot}%{ruby18_sitelibdir}

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog progressbar.en.rd progressbar.ja.rd test.rb
%{ruby18_sitelibdir}/progressbar.rb

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-9m)
- full rebuild for mo7 release

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-8m)
- copy from ruby-progressbar and build with ruby18

* Fri Aug  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-7m)
- fix build on x86_64

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8-6m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8-3m)
- rebuild against gcc43

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.8-2m)
- /usr/lib/ruby

* Tue Jun 22 2004 kourin <kourin@momonga-linux.org>
- (0.8-1m)
- import

* Fri Feb  6 2004 Masaki Yatsu <yatsu@digital-genes.com>
- (0.8-1)
