%global momorel 1
%global reldirver 2.18
%global vimdirver 74
%global catalogue %{_sysconfdir}/X11/fontpath.d

Summary: A program for printing sheet music
Name: lilypond
Version: 2.18.2
Release: %{momorel}m%{?dist}
License: GPLv3
Group: Applications/Publishing
URL: http://www.lilypond.org/
Source0: http://download.linuxaudio.org/%{name}/sources/v%{reldirver}/%{name}-%{version}.tar.gz
NoSource: 0
Patch1: %{name}-2.21.2-gcc44-relocate.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils
Requires(post): findutils
Requires(post): info
Requires(post): kpathsea
Requires(post): xorg-x11-font-utils
Requires(preun): coreutils
Requires(preun): info
Requires(postun): kpathsea
Requires: ghostscript
Requires: vim-common
BuildRequires: ImageMagick
BuildRequires: bison
BuildRequires: flex
BuildRequires: fontforge
BuildRequires: freetype-devel >= 2.5.3
BuildRequires: gettext-devel
BuildRequires: ghostscript
BuildRequires: gtk2-devel
BuildRequires: compat-guile18-devel
BuildRequires: info >= 4.11
BuildRequires: mftrace
BuildRequires: python-devel
BuildRequires: tetex
BuildRequires: texinfo >= 5.1
BuildRequires: texinfo-tex >= 5.1
BuildRequires: urw-fonts >= 1:2.4-1m
BuildRequires: zip

%description
LilyPond is a music typesetter.  It produces beautiful sheet music using a
high level description file as input.  LilyPond is part of the GNU project.
 
LilyPond is split into two packages.  The package "lilypond" provides the
core package, containing the utilities for converting the music source
(.ly) files into printable output. 

%prep
%setup -q
%patch1 -p0

%build
export GUILE=/usr/bin/guile1.8
export GUILE_CONFIG=/usr/bin/guile1.8-config
export GUILE_TOOLS=/usr/bin/guile1.8-tools

%configure --enable-gui --with-ncsb-dir=%{_datadir}/fonts/default/Type1
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall vimdir=%{buildroot}%{_datadir}/vim/vim%{vimdirver} package_infodir=%{buildroot}%{_infodir}

# web doc
# Doesn't work out of the box for this version.
# %%make out=www web-install DESTDIR=%%{buildroot}

chmod +x %{buildroot}%{_libdir}/%{name}/%{version}/python/midi.so

# Symlink lilypond-init.el in emacs' site-start.d directory
pushd %{buildroot}%{_datadir}/emacs/site-lisp
mkdir site-start.d
ln -s ../%{name}-init.el site-start.d
popd

# Create symlinks to lilypond folder under TeX directory, so that TeX can
# use lilypond files natively, courtesy of Michael Brown's great hacks
# Necessary for tex backend to work, since startup profile is gone -- Abel
mkdir -p %{buildroot}%{_datadir}/texmf/dvips \
         %{buildroot}%{_datadir}/texmf/tex \
         %{buildroot}%{_datadir}/texmf/fonts/source \
         %{buildroot}%{_datadir}/texmf/fonts/tfm
pushd %{buildroot}%{_datadir}/texmf
ln -s ../../%{name}/%{version}/ps dvips/%{name}
ln -s ../../%{name}/%{version}/tex tex/%{name}
ln -s ../../../%{name}/%{version}/fonts/source fonts/source/%{name}
ln -s ../../../%{name}/%{version}/fonts/tfm fonts/tfm/%{name}
popd

mkdir -p %{buildroot}%{catalogue}
ln -sf %{_datadir}/%{name}/%{version}/fonts/type1 %{buildroot}%{catalogue}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/%{name}.info > /dev/null 2>&1 || :
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/%{name}-internals.info > /dev/null 2>&1 || :
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/music-glossary.info > /dev/null 2>&1 || :

if [ -d /var/lib/texmf ]; then
find /var/lib/texmf \( -name 'feta*.pk' -or -name 'feta*.tfm' -or -name 'parmesan*.pk' -or -name 'parmesan*.tfm' \) -print0 | xargs -r -0 rm -f || :
fi

mktexlsr > /dev/null

%preun
if [ "$1" = "0" ]; then
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/%{name}.info > /dev/null 2>&1 || :
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/%{name}-internals.info > /dev/null 2>&1 || :
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/music-glossary.info > /dev/null 2>&1 || :
fi

# compatibility for upgrading from STABLE_7
rm -f %{_datadir}/%{name}/%{version}/fonts/type1/fonts.dir

%postun
mktexlsr > /dev/null

%files
%defattr(-,root,root)
%doc AUTHORS.txt COPYING DEDICATION HACKING INSTALL.txt
%doc LICENSE NEWS.txt README.txt ROADMAP
%{_bindir}/abc2ly
%{_bindir}/convert-ly
%{_bindir}/etf2ly
%{_bindir}/lilymidi
%{_bindir}/%{name}
%{_bindir}/%{name}-book
%{_bindir}/%{name}-invoke-editor
%{_bindir}/lilysong
%{_bindir}/midi2ly
%{_bindir}/musicxml2ly
%{_libdir}/%{name}
%{_datadir}/emacs/site-lisp/site-start.d/%{name}-init.el
%{_datadir}/emacs/site-lisp/%{name}-*.el
%{_infodir}/*.info*
%{_datadir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/*.1*
%{_datadir}/texmf/dvips/%{name}
%{_datadir}/texmf/fonts/source/%{name}
%{_datadir}/texmf/fonts/tfm/%{name}
%{_datadir}/texmf/tex/%{name}
%{_datadir}/vim/vim%{vimdirver}/*/%{name}*
%{catalogue}/%{name}

%changelog
* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18.2-1m)
- version 2.18.2

* Thu Aug 22 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.16.2-2m)
- rebuild against vim-7.4
- import and use debian's fixes against texinfo-5.1 for the moment
- http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=707190

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2
- rebuild against compat-guile18-1.8.8

* Fri Sep 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.2-1m)
- version 2.14.2
- remove merged gcc45.patch
- change License: to GPLv3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.3-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.3-8m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.3-7m)
- rebuild against vim-7.3

* Sun Sep 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.3-6m)
- add patch for gcc45

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.3-5m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.3-4m)
- more fix

* Sun Aug 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.3-3m)
- fix %%post

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.3-2m)
- source was replaced, please build with --rmsrc option

* Sat Jan  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.3-1m)
- version 2.12.3
- remove merged glibc210.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.2-3m)
- correct font catalogue
- BuildRequires: urw-fonts >= 1:2.4-1m
- remove PreReq: chkfontpath-momonga

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.2-2m)
- apply glibc210 patch

* Tue Jan 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.2-1m)
- version 2.12.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.1-2m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.1-1m)
- version 2.12.1
- remove merged guile186.patch

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.0-2m)
- drop Patch2 for fuzz=0, already merged upstream

* Mon Dec 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.0-1m)
- version 2.12.0
- fix build with guile-1.8.6
- remove tex-use-utf8.patch

* Sun Dec  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.65-1m)
- version 2.11.65

* Thu Nov 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.64-1m)
- version 2.11.64

* Fri Oct 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.63-1m)
- version 2.11.63

* Sat Oct 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.62-1m)
- version 2.11.62

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.61-1m)
- version 2.11.61
- update tex-use-utf8.patch

* Fri Sep 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.60-1m)
- version 2.11.60

* Mon Sep 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.59-1m)
- version 2.11.59

* Sat Sep 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.58-1m)
- version 2.11.58

* Wed Aug 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.57-1m)
- version 2.11.57

* Tue Aug 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.56-2m)
- rebuild against vim-7.2.6

* Sun Aug 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.56-1m)
- version 2.11.56

* Wed Aug  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.55-1m)
- version 2.11.55

* Sat Aug  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.54-1m)
- version 2.11.54

* Wed Jul 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.53-1m)
- version 2.11.53

* Wed Jul 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.52-1m)
- version 2.11.52
- fix vim files install directories
- Requires: vim-common

* Fri Jul 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.51-1m)
- version 2.11.51

* Thu Jul  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.50-1m)
- version 2.11.50

* Sat Jun 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.49-1m)
- version 2.11.49

* Mon Jun  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.48-1m)
- version 2.11.48

* Sun Jun  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.47-1m)
- version 2.11.47

* Mon May 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.46-1m)
- version 2.11.46

* Sun Apr 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.45-1m)
- version 2.11.45

* Tue Apr 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.43-1m)
- version 2.11.43

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11.42-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.42-1m)
- version 2.11.42

* Fri Feb 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.41-1m)
- version 2.11.41

* Tue Feb 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.40-1m)
- version 2.11.40

* Wed Feb 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.39-3m)
- revise gcc43 patch

* Mon Feb 11 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.39-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Sun Feb 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.39-1m)
- version 2.11.39

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.37-1m)
- version 2.11.37

* Tue Jan  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.36-1m)
- version 2.11.36

* Sun Dec  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.35-1m)
- version 2.11.35

* Wed Oct  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.34-1m)
- version 2.11.34

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.33-1m)
- version 2.11.33

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.32-1m)
- version 2.11.32

* Thu Aug 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.30-1m)
- version 2.11.30

* Wed Aug 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.29-1m)
- version 2.11.29

* Thu Jul 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.28-1m)
- version 2.11.28

* Sun Jul 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.27-3m)
- revise %%post and %%preun again

* Sun Jul 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.27-2m)
- revise PreReq, %%post and %%preun

* Wed Jul  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.27-1m)
- version 2.11.27

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.26-1m)
- version 2.11.26

* Tue May 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.25-1m)
- version 2.11.25

* Sat May 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.24-1m)
- version 2.11.24

* Fri May  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.23-1m)
- version 2.11.23

* Tue Apr 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.22-1m)
- version 2.11.22

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.20-1m)
- initial package for rosegarden-1.5.0
- import 3 patches from cooker
 - lilypond-2.8.6-tex-use-utf8.patch
 - lilypond-2.6.3-locale-indep-date.patch
 - lilypond-2.6.4-use-imagemagick.patch
- Summary and %%description are imported from cooker
