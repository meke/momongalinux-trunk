%global momorel 14

%define pkgname apps

Summary: X.Org X11 applications
Name: xorg-x11-%{pkgname}
Version: 1.0.3
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: xorg-x11-oclock
Requires: xorg-x11-xclock
Requires: xorg-x11-xwd
Requires: xorg-x11-xwud
Requires: xorg-x11-xpr
Requires: xorg-x11-luit
Requires: xorg-x11-x11perf
Requires: xorg-x11-xbiff
Requires: xorg-x11-xclipboard
Requires: xorg-x11-xconsole
Requires: xorg-x11-xcursorgen
Requires: xorg-x11-xeyes
Requires: xorg-x11-xkill
Requires: xorg-x11-xload
Requires: xorg-x11-xlogo
Requires: xorg-x11-xmag
Requires: xorg-x11-xmessage
Requires: xorg-x11-xcalc
Requires: xorg-x11-xinput

%description
A collection of common X Window System applications.

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-12m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-10m)
- add xorg-x11-xinput

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-8m)
- rebuild against gcc43

* Thu Nov  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-7m)
- separate package

* Tue Jun 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-6m)
- Security advisory http://xorg.freedesktop.org/releases/X11R7.1/patches/

* Tue Jun 20 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.3-5m)
- add xcalc

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-4m)
- delete duplicated files

* Thu May 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-3m)
- update xconsole-1.0.2

* Sun May 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- update xcursorgen-1.0.1

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update xclock-1.0.2

* Wed Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2(Xorg7.1RC1)
-- x11perf-1.4.1

* Sun Mar 26 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.1-3m)
- add "BuildRequires: libxkbfile-devel"

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- To trunk

* Wed Mar  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1.3m)
- commentout obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Upgraded all apps to version 1.0.1 from X11R7.0

* Sat Dec 17 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Upgraded all apps to version 1.0.0 from X11R7 RC4.
- Changed manpage dir from man1x to man1 to match upstream default now.
- Dropped all of the datadir-cleanups patches added in the previous build.
- Added x11perf-1.0.0-x11perf-datadir-cleanups.patch as it is still needed
  to put the helper scripts in datadir.
- Added --disable-xprint to configure, as a great symbolic jesture.

* Wed Nov 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-4
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3" to workaround
  (#173384)
- Added the following patches, and invoke aclocal/automake/autoconf on them
  to force app-defaults and other datafiles into _datadir instead of _libdir:
  - oclock-0.99.1-oclock-datadir-cleanups.patch
  - x11perf-0.99.1-x11perf-datadir-cleanups.patch
  - xclipboard-0.99.1-xclipboard-datadir-cleanups.patch
  - xclock-0.99.1-xclock-datadir-cleanups.patch
  - xconsole-0.99.2-xconsole-datadir-cleanups.patch
  - xload-0.99.1-xload-datadir-cleanups.patch
  - xlogo-0.99.1-xlogo-datadir-cleanups.patch
  - xmag-0.99.1-xmag-datadir-cleanups.patch
  - xmessage-0.99.1-xmessage-datadir-cleanups.patch
- Added luit-0.99.1-luit-locale-dir-fix.patch to fix bug (#173702)

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.2-3
- add Requires(pre) on newer filesystem package (#172610)

* Sun Nov 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Add "Obsoletes: XFree86, XFree86-tools, xorg-x11, xorg-x11-tools", as 
  various utils have moved here from there in monolithic X packaging.
- Add "BuildRequires: xbitmaps-devel" for xbiff.
- Rebuild against new libXaw 0.99.2-2, which has fixed DT_SONAME. (#173027)

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Initial build, with all apps taken from X11R7 RC2.
- Use "make install DESTDIR=$RPM_BUILD_ROOT" as the makeinstall macro fails on
  some packages.
- Temporary hack to move xcursorgen manpage to 'man1' dir.
