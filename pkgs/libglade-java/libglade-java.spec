%global momorel 8

# install these packages into /opt if we have a prefix defined for the
# java packages
%{?java_pkg_prefix: %define _prefix /opt/frysk }
%{?java_pkg_prefix: %define _sysconfdir %{_prefix}/etc }
%{?java_pkg_prefix: %define _localstatedir %{_prefix}/var }
%{?java_pkg_prefix: %define _infodir %{_prefix}/share/info }
%{?java_pkg_prefix: %define _mandir %{_prefix}/share/man }
%{?java_pkg_prefix: %define _defaultdocdir %{_prefix}/share/doc }

%{!?c_pkg_prefix: %define c_pkg_prefix %{nil}}
%{!?java_pkg_prefix: %define java_pkg_prefix %{nil}}

%define	name_base	libglade-java

Summary:	Java bindings for libglade
Name:		%{java_pkg_prefix}%{name_base}
Version:	2.12.5
Release: 	%{momorel}m%{?dist}
License:	LGPLv2
Group:		Development/Libraries
URL:		http://java-gnome.sourceforge.net
Source:		http://ftp.gnome.org/pub/GNOME/sources/%{name_base}/2.12/%{name_base}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:		%{name}-gjavah.patch

Requires:	libglade2 >= 2.5.1
Requires:	%{java_pkg_prefix}libgtk-java >= 2.8.6
Requires:	%{java_pkg_prefix}libgnome-java >= 2.12.4
BuildRequires:	%{java_pkg_prefix}libgtk-java-devel >= 2.8.6
BuildRequires:	%{java_pkg_prefix}libgnome-java-devel >= 2.12.4
BuildRequires:  libglade2-devel >= 2.5.1, gcc-java >= 4.1.1
BuildRequires:	java-devel >= 1.4.2
BuildRequires:  docbook-utils

%description
libglade-java is a language binding that allows developers to write
Java applications that use libglade.  It is part of Java-GNOME.

%package        devel
Summary:	Compressed Java source files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	libglade2-devel
Requires:	libgnome-java-devel

%description    devel
Development part of %{name}.

%prep
%setup -q -n %{name_base}-%{version}
%patch0
touch aclocal.m4
touch configure Makefile.in

%build
# we need POSIX.2 grep
export POSIXLY_CORRECT=1

# if either the C or Java packages has a prefix declared, then we will
# add /opt/frysk/lib/pkgconfig to the pkgconfig path
if  [  'x%{java_pkg_prefix}' != 'x' ] || [ 'x%{c_pkg_prefix}' != 'x' ]; then
	export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/opt/frysk/lib/pkgconfig
fi

# Two workarounds:
# 1) libtool.m4 calls gcj with $CFLAGS and gcj seems to choke on -Wall.
# 2) libtool does not use pic_flag when compiling, so we have to force it.
RPM_OPT_FLAGS=${RPM_OPT_FLAGS/-Wall /}
%configure CFLAGS="$RPM_OPT_FLAGS" GCJFLAGS="-O2 -fPIC"

mkdir -p doc/api/
make %{?_smp_mflags}

# pack up the java source
find src/java -name \*.java -newer ChangeLog | xargs touch -r ChangeLog
(cd src/java && find . -name \*.java | sort | xargs zip -X -9 src.zip)
touch -r ChangeLog src/java/src.zip

%install
rm -rf %{buildroot}

make %{?_smp_mflags} DESTDIR=$RPM_BUILD_ROOT install

# rename doc dir to reflect package rename, if the names differ
if [ 'x%{name_base}' != 'x%{name}' ] ; then
	mv $RPM_BUILD_ROOT%{_docdir}/%{name_base}-%{version} $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
fi

# Remove unpackaged files:
rm $RPM_BUILD_ROOT/%{_libdir}/*.la

# install the src zip and make a sym link
jarversion=$(echo -n %{version} | cut -d . -f -2)
jarname=$(echo -n %{name_base} | sed 's/-.*//;s/^lib//')
zipfile=$jarname$jarversion-src-%{version}.zip
install -m 644 src/java/src.zip $RPM_BUILD_ROOT%{_datadir}/java/$zipfile
(cd $RPM_BUILD_ROOT%{_datadir}/java &&
  ln -sf $zipfile $jarname$jarversion-src.zip)


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README
%{_libdir}/libgladejava-*.so
%{_libdir}/libgladejni-*.so
%{_datadir}/java/*.jar

%files devel
%defattr(-,root,root)
%doc doc/api
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgladejava.so
%{_libdir}/libgladejni.so
%{_datadir}/java/*.zip

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.5-7m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.5-6m)
- fix build failure by adding "export POSIXLY_CORRECT=1"

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.5-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.5-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.5-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: LGPLv2

* Tue May 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.12.5-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.8-2m)
- rebuild against gcc43

* Fri Feb  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.8-1m)
- update to 2.12.8

* Mon Jan  1 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.12.7.1-2m)
- add "-fPIC" to GCJFLAGS in spec file

* Mon Jan  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.7.1-1m)
- initial build
