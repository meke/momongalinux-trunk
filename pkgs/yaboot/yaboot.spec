%global momorel 8

Summary: Linux bootloader for Power Macintosh "New World" computers.
Name: yaboot
Version: 1.3.13
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Base
Source: http://penguinppc.org/projects/yaboot/yaboot-%{version}.tar.gz
Patch1: yaboot-1.3.3-man.patch
Patch2: yaboot-1.3.6-ofboot.patch
Patch3: yaboot-1.3.6-rh.patch
Patch4: yaboot-1.3.13-yabootconfig.patch
Patch5: yaboot-1.3.8-ppc64-initrd.patch
Patch6: yaboot-1.3.10-proddiscover.patch
Patch7: yaboot-1.3.10-ext3.patch
Patch8: yaboot-1.3.10-sbindir.patch
Patch9: yaboot-1.3.10-configfile.patch
Patch10: yaboot-1.3.10-parted.patch
Patch11: yaboot-1.3.13-manpage.patch
Patch12: yaboot-1.3.13-gcc34.patch
Patch13: yaboot-1.3.13-amigaparts.patch
Patch14: yaboot-1.3.13-swraid1.patch
Patch15: yaboot-1.3.13-nobootx.patch
Patch16: yaboot-1.3.13-swraid2.patch
Patch17: yaboot-1.3.13-pegasos-claim.patch
Patch18: yaboot-1.3.13-pegasos-ext2.patch
Patch19: yaboot-1.3.13-confarg.patch
Patch20: yaboot-1.3.13-ofpath-pegasos.patch
Patch21: yaboot-1.3.13-pegasos-serial.patch
Patch22: yaboot-1.3.13-allow-deep-mntpoint.patch
Patch23: yaboot-1.3.13-malloc.patch
Patch24: yaboot-1.3.13-telnet.patch
Patch25: yaboot-1.3.13-netboot.patch
Patch26: yaboot-1.3.13-printversion.patch
Patch27: yaboot-ppc64.patch
Patch28: yaboot-1.3.13-multisata.patch
Patch29: yaboot-1.3.13-dontwritehome.patch
Patch30: yaboot-1.3.12-addnote.patch
Patch31: yaboot-1.3.13-ofpath-firewire-usb.patch
Patch32: yaboot-1.3.13-bplan.patch

URL: http://penguinppc.org/projects/yaboot/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: ybin
ExclusiveArch: ppc ppc64

%description
yaboot is a bootloader for PowerPC machines which works on New World ROM
machines (Rev. A iMac and newer) and runs directly from Open Firmware,
eliminating the need for Mac OS.
yaboot can also bootload IBM pSeries machines.

%prep
%setup -q
%patch1 -p0
%patch2 -p1
%patch3 -p1
%patch4 -p1 -b .yabootconfig
# %patch5 -p1 -b .ppc64initrd
%patch6 -p1 -b .proddisc
%patch7 -p1 -b .ext3
%patch8 -p1 -b .sbin
%patch9 -p1 -b .config
%patch10 -p1 -b .parted
%patch11 -p1 -b .manpage
%patch12 -p1 -b .gcc34
%patch13 -p1 -b .amigaparts
%patch14 -p1 -b .swraid1
%patch15 -p1 -b .nobootx
%patch16 -p1 -b .swraid2
%patch17 -p1 -b .pegasos
%patch18 -p1 -b .ext2
%patch19 -p1 -b .confarg
%patch20 -p1 -b .ofpath
%patch21 -p1 -b .pegasos-serial
%patch22 -p1 -b .deepmnt
%patch23 -p1 -b .malloc
#patch24 -p1 -b .telnet
%patch25 -p1 -b .netboot
%patch26 -p1 -b .printversion
%patch27 -p1 -b .ppc64
%patch28 -p1 -b .multisata
%patch29 -p1 -b .bootwrite
%patch30 -p1 -b .addnote
%patch31 -p1 -b .firewire
%patch32 -p1 -b .bplan


%build
%make

%install
%makeinstall ROOT=$RPM_BUILD_ROOT PREFIX=%{_prefix} MANDIR=share/man SBINDIR=/sbin
rm -f $RPM_BUILD_ROOT/etc/yaboot.conf
touch $RPM_BUILD_ROOT/etc/yaboot.conf
mkdir -p $RPM_BUILD_ROOT/boot
install -m0644 %{SOURCE1} $RPM_BUILD_ROOT/boot/efika.forth

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc COPYING README* doc/*
/sbin/ofpath
/sbin/ybin
/sbin/yabootconfig
/sbin/mkofboot
%{_libdir}/yaboot/addnote
%{_libdir}/yaboot/ofboot
%{_libdir}/yaboot/yaboot
%{_mandir}/man8/bootstrap.8.bz2
%{_mandir}/man8/mkofboot.8.bz2
%{_mandir}/man8/ofpath.8.bz2
%{_mandir}/man8/yaboot.8.bz2
%{_mandir}/man8/yabootconfig.8.bz2
%{_mandir}/man8/ybin.8.bz2
%{_mandir}/man5/yaboot.conf.5.bz2

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.13-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.13-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.13-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.13-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.13-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.13-3m)
- rebuild against gcc43

* Fri Nov 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.13-2m)
- import Fedora patch
- build ppc64 arch

* Mon May 09 2005 mutecat <mutecat@momonga-linux.org>
- (1.3.13-1m)
- ver up.

* Mon Jan 24 2005 mutecat <mutecat@momonga-linux.org>
- (1.3.12-2m)
- rebuild against

* Fri Jul 02 2004 mutecat <mutecat@momonga-linux.org>
- (1.3.12-1m)
- momonganized
- inport from FC.

* Fri Jun 18 2004 Jeremy Katz <katzj@redhat.com> - 1.3.12-3
- s/Copyright/License/
- fix build with gcc 3.4

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon May 24 2004 Paul Nasrat <pnasrat@redhat.com> 1.3.12-1
- update to 1.3.12

* Tue Apr 20 2004 David Woodhouse <dwmw2@redhat.com> 1.3.10-10
- make yabootconfig use parted if available

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Aug 22 2003 Elliot Lee <sopwith@redhat.com> 1.3.10-8
- Build for rawhide

* Fri Jun 20 2003 Jeremy Katz <katzj@redhat.com> 1.3.10-7
- allow passing configfile name to yabootconfig with -C

* Wed Jun 18 2003 Jeremy Katz <katzj@redhat.com> 1.3.10-6
- don't ship (invalid) default yaboot.conf
- update to newer version of ppc64 initrd patch

* Mon Apr 28 2003 Jeremy Katz <katzj@redhat.com> 1.3.10-5
- clean up how yabootconfig adds stuff, do some rediff'ing
- install ybin, etc in /sbin instead of /usr/sbin (#83229)

* Wed Apr 23 2003 Jeremy Katz <katzj@redhat.com> 1.3.10-4
- make sure there's a newline at the end of yaboot.conf generated by 
  yabootconfig

* Wed Apr  9 2003 Jeremy Katz <katzj@redhat.com> 1.3.10-3
- try to read product name from /etc/redhat-release instead of hard coding
- add patch (from silo) to allow mounting dirty fs's instead of mounting 
  read-write

* Thu Apr  3 2003 Jeremy Katz <katzj@redhat.com> 1.3.10-2
- add patch from Peter Bergner <bergner@vnet.ibm.com> to add support for 
  ppc64 initrds (warning: breaks ppc32 initrds)

* Thu Mar 20 2003 Jeremy Katz <katzj@redhat.com> 1.3.10-1
- update to 1.3.10
- fix ofboot patch to use Red Hat Linux instead of Yellow Dog Linux
- include patch from Dan Burcaw for yabootconfig so that it can be used 
  from the installer

* Tue Jan 21 2003 Elliot Lee <sopwith@redhat.com> 1.3.8-1
- Update to new version

* Wed Dec 11 2002 Tim Powers <timp@redhat.com> 1.3.6-1b.2rh
- rebuild
- added ExclusiveArch

* Wed Mar 06 2002 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- modify for YDL 2.2

* Fri Dec 07 2001 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- update to 1.3.6

* Sat Oct 06 2001 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- update to 1.3.4

* Sun Sep 30 2001 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- updated to yaboot 1.3.3

* Tue Sep 25 2001 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- add makefile patch

* Sun Sep 23 2001 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- update to 1.3.1
- obsoletes ybin

* Thu Aug 08 2001 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- updated to 1.2.3 per Ben H's urgent announcement
- removed obsoleted patches

* Tue Jan 16 2001 Hollis Blanchard <hollis@terrasoftsolutions.com>
- hacked out bug preventing manual boot from yaboot prompt (may break CHRP)
- removed ybin's man pages
- moved to /usr/lib/yaboot for ybin, FHS compliance
- changed permissions to 644 (can't run yaboot under Linux)

* Sat Nov 18 2000 Hollis Blanchard <hollis@terrasoftsolutions.com>
- updated to yaboot 0.9
- added man pages from ybin
- added sample yaboot.conf and bootscript (and README's)

* Sun Feb 27 2000 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- modified for YDL

* Wed Jan 18 2000 Tom Rini <trini@kernel.crashing.org>
- created
