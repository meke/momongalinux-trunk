%global momorel 1

Name:           netcf
Version:        0.2.4
Release:        %{momorel}m%{?dist}
Summary:        Cross-platform network configuration library

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            https://fedorahosted.org/netcf/
Source0:        https://fedorahosted.org/released/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  readline-devel augeas-devel >= 0.5.2
BuildRequires:  libxml2-devel libxslt-devel
BuildRequires:  libnl-devel
Requires:       %{name}-libs = %{version}-%{release}

%description
A library for modifying the network configuration of a system. Network
configurations are expresed in a platform-independent XML format, which
netcf translates into changes to the system's 'native' network
configuration files.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        libs
Summary:        Libraries for %{name}
Group:          System Environment/Libraries

%description    libs
The libraries for %{name}.

%prep
%setup -q

%build
%configure --disable-static \
           --with-libnl1 \
           --with-sysinit=systemd
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="%{__install} -p"
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

%clean
rm -rf $RPM_BUILD_ROOT

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/ncftool
%{_libexecdir}/netcf-transaction.sh
%{_mandir}/man1/ncftool.1*

%files libs
%defattr(-,root,root,-)
%{_datadir}/netcf
%{_libdir}/*.so.*
%{_unitdir}/netcf-transaction.service
%doc AUTHORS COPYING NEWS

%files devel
%defattr(-,root,root,-)
%doc
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/netcf.pc

%changelog
* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.4-1m)
- update 0.2.4

* Fri Mar 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.3-1m)
- update 0.2.3

* Wed Sep 26 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.2.2-1m)
- update 0.2.2

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.9-1m)
- update 0.1.9

* Mon Jul 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.8-1m)
- update 0.1.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.6-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.6-1m)
- update to 0.1.6

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.5-3m)
- rebuild against readline6

* Fri Feb 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.5-2m)
- apply glibc212 patch

* Sun Jan 31 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.5-1m)
- import from Fedora devel

* Mon Nov 30 2009 David Lutterkort <lutter@redhat.com> - 0.1.5-1
- New version

* Thu Nov  5 2009 David Lutterkort <lutter@redhat.com> - 0.1.4-1
- New version

* Tue Oct 27 2009 David Lutterkort <lutter@redhat.com> - 0.1.3-1
- New version

* Fri Sep 25 2009 David Lutterkort <lutter@redhat.com> - 0.1.2-1
- New Version

* Wed Sep 16 2009 David Lutterkort <lutter@redhat.com> - 0.1.1-1
- Remove patch netcf-0.1.0-fix-initialization-of-libxslt.patch,
  included upstream

* Tue Sep 15 2009 Mark McLoughlin <markmc@redhat.com> - 0.1.0-3
- Fix libvirtd segfault caused by libxslt init issue (#523382)

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 13 2009 David Lutterkort <lutter@redhat.com> - 0.1.0-1
- BR on augeas-0.5.2
- Drop explicit requires for augeas-libs

* Wed Apr 15 2009 David Lutterkort <lutter@redhat.com> - 0.0.2-1
- Updates acording to Fedora review

* Fri Feb 27 2009 David Lutterkort <lutter@redhat.com> - 0.0.1-1
- Initial specfile
