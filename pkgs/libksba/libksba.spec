%global momorel 1

Summary: X.509 library
Name: libksba
Version: 1.3.0
Release: %{momorel}m%{?dist}
URL: http://www.gnupg.org/
Source0: ftp://ftp.gnupg.org/gcrypt/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
License: GPLv3
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgpg-error-devel >= 1.10
Requires(post): info
Requires(preun): info

%description
KSBA is a library designed to build software based
on the X.509 and CMS protocols.

%prep
%setup -q

%build
./configure \
	--prefix=%{_prefix} \
	--enable-maintainer-mode CC=gcc
%make

%install
rm -rf %{buildroot}
%makeinstall
%__rm -f %{buildroot}%{_infodir}/dir

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/ksba.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/ksba.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root)
%doc README TODO INSTALL ChangeLog NEWS AUTHORS COPYING THANKS
%doc tests doc
%{_bindir}/ksba-config
%{_includedir}/*
%{_libdir}/lib*
%{_infodir}/ksba*
%{_datadir}/aclocal/ksba.m4

%changelog
* Sat Nov 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- rebuild for new GCC 4.5

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.7-3m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-2m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-3m)
- remove --entry option of install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-4m)
- update 1.0.3

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-3m)
- %%NoSource -> NoSource

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-2m)
- revised spec for debuginfo

* Sun Jul  8 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2
  switch license to GPLv3
  no %%configure macro

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.1-2m)
- add BuildPrereq: libgpg-error-devel >= 1.2

* Fri Dec  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Thu Oct  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sat May 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.9.14-1m)
- update to 0.9.14

* Fri Aug 12 2005 TAKAHASHI Tamotsu <tamo>
- (0.9.12-1m)

* Sat Apr 23 2005 TAKAHASHI Tamotsu <tamo>
- (0.9.11-1m)

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (0.9.10-1m)
- Alter -> Main

* Tue Nov 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.9.9-1m)
- updated to 0.9.9

* Mon Jun 14 2004 TAKAHASHI Tamotsu <tamo>
- (0.9.7-1m)

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4.7-2m)
- revised spec for enabling rpm 4.2.

* Tue Mar 18 2003 TAKAHASHI Tamotsu <tamo>
- (0.4.7-1m)
- "fixes a problem mainly relevant to certificate request creation
 (if you must use the ugly way of putting the email address into
 the subject DN). This is a drop-in replacement for 0.4.6." -- Werner

* Thu Dec 05 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.4.6-1m)

* Sat Oct 05 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.4.5-1m)
- verup
- post process: added if-fi

* Mon Aug 19 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.4.4-1m)
- (from NEWS) Multiple signatures can now be created and parsed.

* Thu Jul 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.4.3-1m)
- update
- URL: http://www.gnupg.org/
- install-info

* Wed May 01 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (0.4.0-0.020020501002k)
- from anoncvs
- url unknown
- don't know if ldconfig needed or not
- not committed yet
