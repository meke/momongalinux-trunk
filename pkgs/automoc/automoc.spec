%global momorel 7
%global qtver 4.7.0
%global qtrel 0.2.1m
%global cmakever 2.4.8
%global cmakerel 5m
%global ftpdirver automoc4/0.9.88
%global sourcedir stable/%{ftpdirver}

%global beta 0.9.88
%global beta_ver 6

Name:           automoc
Version:        1.0
Release:        0.%{beta_ver}.%{momorel}m%{?dist}
Summary:        KDE4 automoc
Group:          Development/Tools
License:        BSD
URL:            http://www.kde.org
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}4-%{beta}.tar.bz2
NoSource:       0
Provides:       automoc4 = %{beta}
Requires:       cmake
BuildRequires:  cmake 
BuildRequires:  qt-devel >= %{qtver}

%description
KDE4 automoc

%prep
%setup -q -n %{name}4-%{beta}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} VERBOSE=1 -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_kde4_bindir}/automoc4
%{_kde4_libdir}/automoc4/Automoc4Config.cmake
%{_kde4_libdir}/automoc4/Automoc4Version.cmake
%{_kde4_libdir}/automoc4/automoc4.files.in

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.6.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.6.6m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.6.5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.6.4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.6.3m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.6.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Feb 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.6.1m)
- update to 0.9.88

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.5.2m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.5.1m)
- update to 0.9.87 (svn 20081012 snapshot)

* Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.4.1m)
- update to 0.9.86 (svn snapshot, rev.850002)

* Fri Jul 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.3.1m)
- update to 0.9.84

* Wed Jun 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.2.1m)
- import from Fedora devel

* Fri Jun 20 2008 RexDieter <rdieter@fedoraproject.org> 1.0-0.6.beta2
- automoc4-0.9.93 (aka 1.0beta2)

* Thu Jun 10 2008 Lorenzo Villani <lvillani@binaryhelix.net> - 1.0-0.5.20080527svn811390
- Leave automoc4.files.in in _libdir
- Same applies to Automoc4Config.cmake

* Thu May 29 2008 Lorenzo Villani <lvillani@binaryhelix.net> - 1.0-0.3.20080527svn811390
- Added 'cmake' to Requires

* Wed May 28 2008 Lorenzo Villani <lvillani@binaryhelix.net> - 1.0-0.2.20080527svn811390
- Patched to make it build on other systems than i386 (thanks to Rex Dieter)

* Tue May 27 2008 Lorenzo Villani <lvillani@binaryhelix.net> - 1.0-0.1.20080527svn811390
- Initial release
