%global momorel 7

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1
%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:
  %{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:
  %{_gcj_support}}%{!?_gcj_support:0}}}

%define parent plexus
%define subname runtime-builder

Name:           %{parent}-%{subname}
Version:        1.0
Release:        0.9.%{momorel}m%{?dist}
Epoch:          0
Summary:        Plexus Component Descriptor Creator
License:        MIT
Group:          Development/Libraries
URL:            http://plexus.codehaus.org/
Source0:        %{name}-src.tar.gz
# svn export svn://svn.plexus.codehaus.org/plexus/tags/plexus-runtime-builder-1.0-alpha-9 plexus-runtime-builder/
# tar czf plexus-runtime-builder-1.0-alpha-9.tar.gz plexus-runtime-builder/
Patch0:         plexus-runtime-builder-maven208.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if ! %{gcj_support}
BuildArch:      noarch
%endif

BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  jakarta-commons-codec
BuildRequires:  jakarta-commons-httpclient
BuildRequires:  maven2 >= 2.0.4
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-surefire
BuildRequires:  maven2-common-poms >= 1.0
BuildRequires:  maven-wagon
BuildRequires:  plexus-appserver
BuildRequires:  plexus-archiver
BuildRequires:  plexus-container-default
BuildRequires:  plexus-utils
BuildRequires:  plexus-velocity
BuildRequires:  plexus-xmlrpc
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  velocity
BuildRequires:  xmlrpc

Requires:       jakarta-commons-codec
Requires:       jakarta-commons-httpclient
Requires:       maven2-common-poms >= 1.0
Requires:       maven-wagon
Requires:       plexus-appserver
Requires:       plexus-archiver
Requires:       plexus-container-default
Requires:       plexus-utils
Requires:       plexus-velocity
Requires:       plexus-xmlrpc
Requires:       velocity
Requires:       xmlrpc

Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2

%if %{gcj_support}
BuildRequires:       java-gcj-compat-devel
Requires(post):      java-gcj-compat
Requires(postun):    java-gcj-compat
%endif

%description
The Plexus project seeks to create end-to-end developer tools for
writing applications. At the core is the container, which can be
embedded or for a full scale application server. There are many
reusable components for hibernate, form processing, jndi, i18n,
velocity, etc. Plexus also includes an application server which
is like a J2EE application server, without all the baggage.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{name}

%patch0 -p1 -b .maven208

%build

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

# FIXME: Ignoring text failures for now
mvn-jpp \
        -e \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        -Dmaven2.jpp.mode=true \
        -Dmaven.test.failure.ignore=true \
        install javadoc:javadoc

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/plexus
install -pm 644 target/*.jar \
      $RPM_BUILD_ROOT%{_javadir}/%{parent}/%{subname}-%{version}.jar
%add_to_maven_depmap org.codehaus.plexus %{name} 1.0-alpha-9 JPP/%{parent} %{subname}

(cd $RPM_BUILD_ROOT%{_javadir}/%{parent} && for jar in *-%{version}*; \
  do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# pom
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -pm 644 pom.xml \
  $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP.%{parent}-%{subname}.pom

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

cp -pr target/site/apidocs/* \
        $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/

ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%update_maven_depmap

%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(-,root,root,-)
%{_javadir}/plexus/*
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/%{name}
%config(noreplace) /etc/maven/fragments/plexus-runtime-builder

%if %{gcj_support}
%dir %attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/runtime-builder-1.0.jar.*
%endif

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.9.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.9.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.9.5m)
- full rebuild for mo7 release

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.9.4m)
- revise Patch0 for maven-2.0.8
- drop BuildRequires: maven2-plugin-release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.9.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.9.2m)
- remove duplicate directories

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.9.1m)
- import from Fedora 11

* Mon Mar 23 2009 Deepak Bhole <dbhole@redhat.com> 1.0-0.3.a9.1.9
- Build on ppc64

* Fri Feb 27 2009 Deepak Bhole <dbhole@redhat.com> - 0:1.0-0.3.a9.1.8
- Add tomcat5 and servlet-api BRs

* Fri Feb 27 2009 Deepak Bhole <dbhole@redhat.com> - 0:1.0-0.3.a9.1.7
- Rebuild.

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0:1.0-0.3.a9.1.6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep 24 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.2.a9.1.6
- Add proper patch number

* Wed Jul  9 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-0.2.a9.1.5
- drop repotag

* Thu May 29 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-0.2.a9.1jpp.4
- fix license tag

* Thu Feb 28 2008 Deepak Bhole <dbhole@redhat.com> 1.0-0.2.a9.1jpp.3
- Rebuild

* Fri Sep 21 2007 Deepak Bhole <dbhole@redhat.com> 0:1.0-0.1.a9.2jpp.2
- ExcludeArch ppc64

* Thu Feb 22 2007 Tania Bento <tbento@redhat.com> 0:1.0-0.1.a9.2jpp.1
- Fixed %%Release.
- Fixed %%BuildRoot.
- Removed %%Vendor.
- Removed %%Distribution.
- Fixed instructions how to generate the source drop.
- Added gcj support.
- Removed %%post and %%postun sections for javadoc.

* Tue Oct 17 2006 Deepak Bhole <dbhole@redhat.com> 1.0-0.a9.2jpp
- Update for maven2 9jpp.

* Mon Jun 12 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.0-0.a9.1jpp
- Initial build
