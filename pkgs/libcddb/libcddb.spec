%global momorel 7

Summary: C library to access data on a CDDB server (freedb.org)
Name: libcddb
Version: 1.3.2
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0:	libcddb-1.3.0-multilib.patch
Patch1: libcddb-1.3.2-rhbz770611.patch
URL: http://libcddb.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libcdio-devel >= 0.90

%description
Libcddb is a C library to access data on a CDDB server
(freedb.org). It allows you to:

 1. search the database for possible CD matches;
 2. retrieve detailed information about a specific CD;
 3. submit new CD entries to the database.

Libcddb supports both the custom CDDB protocol and tunnelling the
query and read operations over plain HTTP. It is also possible to use
an HTTP proxy server. If you want to speed things up, you can make use
of the built-in caching facility provided by the library.

%package devel
Summary: %{name} header files and development documentation
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Header files and development documentation for %{name}


%prep
#'
%setup -q
%patch0 -p1
%patch1 -p1

iconv -f ISO_8859-1 -t UTF-8 THANKS > THANKS.tmp
touch -r THANKS THANKS.tmp
mv THANKS.tmp THANKS
iconv -f ISO_8859-1 -t UTF-8 ChangeLog > ChangeLog.tmp
touch -r ChangeLog ChangeLog.tmp
mv ChangeLog.tmp ChangeLog

%build
%configure --disable-static
# Don't use rpath!
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

# remove libtool la
rm -rf %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc 
%{_bindir}/*
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/*

%changelog
* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-7m)
- rebuild against libcdio-0.90

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-4m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-3m)
- rebuild against libcdio-0.82

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2
- rebuild against libcdio-0.81

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-2m)
- %%NoSource -> NoSource

* Wed Dec 06 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.0-1m)
- initial packaging for Momonga
