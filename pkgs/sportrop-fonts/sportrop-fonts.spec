%global momorel 6

%define fontname sportrop
%define fontconf 63-%{fontname}.conf

#define archivename %{name}-%{version}

Name:           %{fontname}-fonts
Version:        0.9
Release:        %{momorel}m%{?dist}
Summary:        A multiline decorative font

Group:          User Interface/X
License:        OFL
URL:            http://openfontlibrary.org/media/files/gluk/287
Source0:        http://openfontlibrary.org/people/gluk/gluk_-_Sportrop.zip
Source1:        %{name}-fontconfig.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
A neat multiline font


%prep
%setup -c
chmod 0644 *.txt
for txt in *.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%doc OFL_FAQ.txt OFL_License.txt tabl_c.jpg

%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9-1m)
- import from Fedora

* Mon Mar 9 2009 Jon Stanley <jonstanley@gmail.com> - 0.9-6
- Update to new packaging guidelines

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jul 27 2008 Jon Stanley <jonstanley@gmail.com> - 0.9-4
- Revert package split, package only TTF
- Use %%setup macro

* Wed Jul 23 2008 Jon Stanley <jonstanley@gmail.com> - 0.9-3
- Split OTF and TTF into subpackages

* Wed Jul 23 2008 Jon Stanley <jonstanley@gmail.com> - 0.9-2
- Fixed rpmlint warning in license and license FAQ

* Tue Jul 22 2008 Jon Stanley <jonstanley@gmail.com> - 0.9-1
- Initial package

