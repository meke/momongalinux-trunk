# Generated from rcov-0.9.11.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname rcov

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Code coverage analysis tool for Ruby
Name: rubygem-%{gemname}
Version: 0.9.11
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/relevance/rcov
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}

%description
rcov is a code coverage tool for Ruby. It is commonly used for viewing overall
test unit coverage of target code.  It features fast execution (20-300 times
faster than previous tools), multiple analysis modes, XHTML and several kinds
of text reports, easy automation with Rake via a RcovTask, fairly accurate
coverage information through code linkage inference using simple heuristics,
colorblind-friendliness...


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            -V \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x
# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%files
%dir %{geminstdir}
%{_bindir}/rcov
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%doc %{gemdir}/doc/%{gemname}-%{version}


%changelog
* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.11-1m)
- update 0.9.11

* Tue Sep 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.10-1m)
- update 0.9.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.9-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-1m)
- update 0.9.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-2m)
- full rebuild for mo7 release

* Mon Aug 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-1m)
- Initial package for Momonga Linux
