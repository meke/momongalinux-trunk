%global momorel 1
#%%global snapdate 2010-06-27

Summary: Enlightenment Input Output Library
Name: eio
Version: 1.7.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://enlightenment.org/
BuildRequires: ecore-devel >= 1.7.7
BuildRequires: chrpath pkgconfig
BuildRequires: openldap-devel
BuildRequires: libssh2-devel
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This library is intended to provide non blocking IO by using thread for all
operations that may block. It depends only on eina, eet and ecore right now.
It should integrate all the features/functions of Ecore_File that could block.

It is part of what we call the EFL and can be a dependence of E17.

%package devel
Summary: Eio header files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static
%{__make}

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
chrpath --delete %{buildroot}%{_libdir}/lib%{name}.so.*

find %{buildroot} -name '*.la' -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(755,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/lib*.so.*

%files devel
%defattr(755,root,root,-)
%dir %{_includedir}/%{name}-1
%{_includedir}/%{name}-1/*.*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- initial build for Momonga Linux
