%global         momorel 1

Name:           perl-Log-Log4perl
Version:        1.44
Release:        %{momorel}m%{?dist}
Summary:        Log4j implementation for Perl
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Log-Log4perl/
Source0:        http://www.cpan.org/authors/id/M/MS/MSCHILLI/Log-Log4perl-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-PathTools >= 0.82
BuildRequires:  perl-Test-Simple >= 0.45
BuildRequires:  rrdtool-devel >= 1.4.5
Requires:       perl-PathTools >= 0.82
Requires:       perl-Test-Simple >= 0.45
Requires:       rrdtool-perl >= 1.4.5
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-Catalyst-Log-Log4perl
Provides:       perl-Catalyst-Log-Log4perl

## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 0}

%description
Log::Log4perl lets you remote-control and fine-tune the logging behaviour
of your system from the outside. It implements the widely popular (Java-
based) Log4j logging package in pure Perl.

%prep
%setup -q -n Log-Log4perl-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{_bindir}/l4p-tmpl
%{perl_vendorlib}/Log/Log4perl
%{perl_vendorlib}/Log/Log4perl.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- rebuild against perl-5.20.0
- update to 1.44

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.43-1m)
- update to 1.43

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-3m)
- rebuild against perl-5.18.2

* Fri Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-2m)
- rebuild against perl-5.18.1

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-2m)
- rebuild against perl-5.18.0

* Mon Apr 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- update to 1.41

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-2m)
- rebuild against perl-5.16.3

* Sat Dec  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-2m)
- rebuild against perl-5.16.2

* Sun Oct 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-1m)
- update to 1.39

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Wed Jan  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-5m)
- rebuild against perl-5.14.2

* Wed Sep 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-4m)
- Obsoletes and Provides perl-Catalyst-Log-Log4perl

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-3m)
- rebuild against perl-5.14.1

* Sat Jun  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-2m)
- all tests are passed with rrdtools-1.4.5

* Wed Jun  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.32-2m)
- rebuild for new GCC 4.6

* Mon Feb 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.31-2m)
- rebuild for new GCC 4.5

* Wed Oct 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.29-2m)
- full rebuild for mo7 release

* Thu Jun 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-2m)
- rebuild against perl-5.12.0

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Mon Feb  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.20-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Thu Oct 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Wed Aug 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Sun Jul 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Wed May 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.15-4m)
- rebuild against gcc43

* Sat Feb 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.15-3m)
- delete Requires: perl-XML-DOM

* Thu Feb 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.15-2m)
- add Requires: perl-XML-DOM

* Thu Feb 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Fri Nov 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-2m)
- use vendor

* Wed Apr  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Sun Feb 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.06-1m)
- spec file was autogenerated
