%global momorel 5

Name:		kcc
Version:	2.3
Release:	%{momorel}m%{?dist}
License:	GPLv2+
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

## missed upstream.
Source:		ftp://ftp.sra.co.jp/pub/os/linux/JE/sources/base/%{name}.tar.gz
NoSource:	0
Source1:	kcc.1
Patch0:		kcc-2.3-dontstrip.patch
Patch1:		kcc-2.3-varargs.patch
Patch2:		kcc-2.3-fix-segv.patch
Patch3:		kcc-2.3-timestamp.patch

Summary:	Kanji Code Converter
Group:		Applications/Text
%description
kcc is a kanji code converter with an auto detection.

%prep
%setup -q -n %{name}
%patch0 -p1 -b .dontstrip
%patch1 -p1 -b .varargs
%patch2 -p1 -b .segv
%patch3 -p1 -b .timestamp

%build
make "CFLAGS=$RPM_OPT_FLAGS"

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_mandir}/ja/man1
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
make BINPATH="$RPM_BUILD_ROOT%{_bindir}" install 
make MANPATH="$RPM_BUILD_ROOT%{_mandir}" JMANDIR="ja" install.man
for i in `find $RPM_BUILD_ROOT%{_mandir}/ja -type f`; do
	iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done
install -m0644 -p %{SOURCE1} $RPM_BUILD_ROOT%{_mandir}/man1/
gzip -9 $RPM_BUILD_ROOT%{_mandir}/man1/kcc.1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING README 
%{_bindir}/kcc
%lang(ja) %{_mandir}/ja/man1/kcc.1*
%{_mandir}/man1/kcc.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-3m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3-29
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 12 2008 Akira TAGOH <tagoh@redhat.com> - 2.3-28
- Rebuild for gcc-4.3.

* Fri Sep 21 2007 Akira TAGOH <tagoh@redhat.com> - 2.3-27
- clean up the spec file.

* Thu Aug 23 2007 Akira TAGOH <tagoh@redhat.com> - 2.3-26
- Rebuild

* Wed Aug  8 2007 Akira TAGOH <tagoh@redhat.com> - 2.3-25
- Update License tag.
- clean up the spec file.

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 2.3-24.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.3-24.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.3-24.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Mar 17 2005 Akira TAGOH <tagoh@redhat.com> - 2.3-24
- rebuilt

* Thu Feb 10 2005 Akira TAGOH <tagoh@redhat.com> - 2.3-23
- rebuilt

* Tue Jun 22 2004 Akira TAGOH <tagoh@redhat.com> 2.3-22
- kcc-2.3-fix-segv.patch: applied to fix segfaults with invalid options. (#126338)
- add kcc.1 from Debian package.

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Sep 30 2003 Akira TAGOH <tagoh@redhat.com> 2.3-19
- converted Japanese manpage to UTF-8.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu May 22 2003 Jeremy Katz <katzj@redhat.com> 2.3-17
- gcc 3.3 doesn't implement varargs.h, include stdarg.h instead

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Nov 13 2002 Akira TAGOH <tagoh@redhat.com> 2.3-15
- rebuild.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Jun 18 2002 Akira TAGOH <tagoh@redhat.com>
- clean up a spec.
- s/Copyright/License/
- kcc-2.3-dontstrip.patch: fix the stripped binary.

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Tue Dec 19 2000 Akira Tagoh <tagoh@redhat.com>
- rebuild for 7.1

* Mon Sep 11 2000 Matt Wilson <msw@redhat.com>
- added %%defattr(-,root,root)

* Fri Aug  4 2000 ISHIKAWA Mutsumi <ishikawa@redhat.com>
- built for RedHat 7.0
- adopt fhs

* Sun Mar 26 2000 Chris Ding <cding@redhat.com>
- ja -> ja_JP.eucJP

* Tue Mar 21 2000 Chris Ding <cding@redhat.com>
- ja_JP.eucJP -> ja

* Wed Mar 15 2000 Matt Wilson <msw@redhat.com>
- rebuild for 6.2j
- gzip man pages

* Tue Feb 29 2000 Chris Ding <cding@redhat.com>
- ja_JP.ujis -> ja_JP.eucJP

* Thu Oct  7 1999 Matt Wilson <msw@redhat.com>
- rebuilt against 6.1

* Sun May 30 1999 FURUSAWA,Kazuhisa <kazu@linux.or.jp>
- 1st Release for i386 (glibc2.1).
- Original Packager: Atsushi Yamagata <yamagata@plathome.co.jp>

* Wed Jan 20 1999 Atsushi Yamagata <yamagata@plathome.co.jp>
- version up to 2.3
- /usr/local -> /usr
- Extensions/Japanese -> Utilities/Text

* Fri May  1 1998 FURUSAWA,Kazuhisa <kazu_f@big.or.jp>
- 9th release
