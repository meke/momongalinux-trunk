%global         momorel 8

Name:           perl-Catalyst-ActionRole-ACL
Version:        0.07
Release:        %{momorel}m%{?dist}
Summary:        User role-based authorization action class
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-ActionRole-ACL/
Source0:        http://www.cpan.org/authors/id/B/BO/BOBTFISH/Catalyst-ActionRole-ACL-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Catalyst-Controller-ActionRole
BuildRequires:  perl-Catalyst-Runtime
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Moose
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-Test-Simple
Requires:       perl-Catalyst-Controller-ActionRole
Requires:       perl-Catalyst-Runtime
Requires:       perl-Moose
Requires:       perl-namespace-autoclean
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Provides a Catalyst reusable action role for user role-based authorization.
ACLs are applied via the assignment of attributes to application action
subroutines.

%prep
%setup -q -n Catalyst-ActionRole-ACL-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/Action/Role/ACL.pm
%{perl_vendorlib}/Catalyst/ActionRole/ACL.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-2m)
- rebuild against perl-5.16.1

* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-2m)
- rebuild against perl-5.14.2

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.05-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.05-2m)
- rebuild for new GCC 4.5

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
