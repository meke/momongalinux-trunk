%global momorel 1

Summary: a collection of c functions implementing the http://oauth.net API
Name: liboauth
Version: 0.9.5
Release: %{momorel}m%{?dist}
License: GPL MIT
Group: System Environment/Libraries
URL: http://liboauth.sourceforge.net/
Source0: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: libcurl-devel
BuildRequires: nss-devel

%description
liboauth provides functions to escape and encode stings according to
OAuth specifications and offers high-level functionality built on top to sign
requests or verify signatures using either NSS or OpenSSL for calculating
the hash/signatures.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.GPL COPYING.MIT ChangeLog NEWS README
%{_libdir}/liboauth.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-, root, root)
%{_includedir}/oauth.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/oauth.pc
%{_mandir}/man3/oauth.3.*

%changelog
* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.4-1m)
- initial build
