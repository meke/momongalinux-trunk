%global momorel 1
%global pkg ibus
%global pkgname ibus.el

Name:		emacs-ibus
Version:	0.3.2
Release: %{momorel}m%{?dist}
Summary:	IBus client for GNU Emacs

Group:		System Environment/Libraries
License:	GPLv3+
URL:		http://www11.atwiki.jp/s-irie/pages/21.html
Source0:	http://www11.atwiki.jp/s-irie/pub/emacs/ibus/ibus-el-%{version}.tar.gz
NoSource:	0

BuildArch:	noarch
BuildRequires:	emacs
Requires:	emacs(bin) >= %{_emacs_version}, ibus, python-xlib

Obsoletes: elisp-ibus
Provides: elisp-ibus

%description
ibus.el is a IBus client for GNU Emacs. This program allows users
on-the-spot style input with IBus. The input statuses are individually
kept for each buffer, and prefix-keys such as C-x and C-c can be used
even if IBus is active. So you can input various languages fast and
comfortably by using it.


%package -n emacs-%{pkg}-el
Summary:	Elisp source files for %{pkgname} under GNU Emacs
Group:		System Environment/Libraries
Requires:	emacs-%{pkg} = %{version}-%{release}

%description -n emacs-%{pkg}-el
This package contains the elisp source files for %{pkgname} under GNU
Emacs. You do not need to install this package to run
%{pkgname}. Install the emacs-%{pkg} package to use %{pkgname} with
GNU Emacs.


%prep
%setup -q -n ibus-el-%{version}


%build
%{_emacs_bytecompile} ibus.el
cat > %{name}-init.el <<"EOF"
(require 'ibus)
(add-hook 'after-init-hook 'ibus-mode-on)
EOF

%install
%__mkdir_p $RPM_BUILD_ROOT%{_libexecdir}
install -p -m 755 ibus-el-agent $RPM_BUILD_ROOT%{_libexecdir}
%__mkdir_p $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{pkg}
install -p -m 644 ibus.elc $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{pkg}
install -p -m 644 ibus.el $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{pkg}
%__mkdir_p $RPM_BUILD_ROOT%{_emacs_sitestartdir}
install -p -m 644 %{name}-init.el \
	$RPM_BUILD_ROOT%{_emacs_sitestartdir}/%{pkg}-init.el


%files
%defattr(-,root,root,-)
%doc README doc/ChangeLog doc/COPYING
%{_emacs_sitelispdir}/%{pkg}/ibus.elc
%{_libexecdir}/ibus-el-agent
%{_emacs_sitestartdir}/ibus-init.el
%dir %{_emacs_sitelispdir}/%{pkg}


%files -n emacs-ibus-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/ibus.el


%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-3m)
- rebuild for emacs-24.1

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-2m)
- set Obsoletes: elisp-ibus

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1
- rename the package name
- re-import fedora's emacs-ibus

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.1-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-1m)
- initial packaging
