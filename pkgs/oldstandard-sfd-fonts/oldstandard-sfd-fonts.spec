%global momorel 6

%define	fontname	oldstandard
%define fontconf 	60-%{fontname}.conf

Name:		%{fontname}-sfd-fonts
Version:	2.0.2
Release:	%{momorel}m%{?dist}
Summary:	Old Standard True-Type Fonts

Group:		User Interface/X
License:	OFL
URL:		http://www.thessalonica.org.ru/en/fonts.html
Source0:	http://www.thessalonica.org.ru/downloads/oldstandard-2.0.2.src.zip
Source1:	%{name}-fontconfig.conf
Source2:	http://www.thessalonica.org.ru/downloads/oldstand-manual.pdf

# guidelines say this can be used
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:	noarch
BuildRequires:	fontforge,fontpackages-devel
Requires:	fontpackages-filesystem

%description
The Old Standard font family is an attempt to revive
a specific type of Modern (classicist) style of serif
typefaces, very commonly used in various editions
printed in the late 19th and early 20th century,
but almost completely  abandoned later.


%prep
%setup -q -c -n oldstandard-%{version}

for i in $(ls OldStandard*.sfd);do
	sed -i -e 's/OldStandardTT/OldStandardSFD/'  -e 's/Old \Standard \TT/Old \Standard \SFD/' $i;
done
for txt in OFL* ; do
	sed 's/\r//' $txt > $txt.new
	touch -r $txt $txt.new
	mv $txt.new $txt
done

install -m 644 -p %{SOURCE2} .

%build
fontforge -lang=ff -script "-" OldStandard*.sfd <<EOF
i = 1 
while ( i < \$argc )
  Open (\$argv[i], 1)
  Generate (\$fontname + ".ttf")
  PrintSetup (5) 
  PrintFont (0, 0, "", \$fontname + "-sample.pdf")
  Close()
  i++ 
endloop
EOF

%install
rm -fr %{buildroot}

install -m 755 -d %{buildroot}%{_fontdir}
install -m 644 -p *.ttf %{buildroot}%{_fontdir}

install -m 755 -d %{buildroot}%{_fontconfig_templatedir} \
		%{buildroot}%{_fontconfig_confdir}

install -m 644 -p %{SOURCE1} \
	%{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} \
	%{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf %{buildroot}

%_font_pkg -f %{fontconf} *.ttf

%doc *.txt *.pdf

%dir %{_fontdir}/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.2-1m)
- import from Fedora


* Mon Mar 16 2009 Ankur Sinha <ankursinha at fedoraproject.org> - 2.0.2-7
- changed in accordance with #490369
* Sun Mar 15 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net> - 2.0.2-5
- Make sure F11 font packages have been built with F11 fontforge

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 20 2009 Ankur Sinha <ankursinha@fedoraproject.org>
- 2.0.2-3
- corrected the fontconfig
* Sun Jan 4 2009 Ankur Sinha <ankursinha@fedoraproject.org>
- 2.0.2-2
- Rebuild according to new packaging guidelines
* Mon Dec 15 2008 Ankur Sinha <ankursinha@fedoraproject.org> 
- 2.0.2-1
- Rebuilt for f10 with minor changes to spec file. (#457947 at bugzilla)
- changed version number as per FONTLOG.txt in source zip file.
