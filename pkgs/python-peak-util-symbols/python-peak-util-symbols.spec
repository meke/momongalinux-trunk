%global momorel 8

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

%define packagename SymbolType

Name:           python-peak-util-symbols
Version:        1.0
Release:        %{momorel}m%{?dist}
Summary:        Simple "symbol" type, useful for enumerations or sentinels

Group:          Development/Languages
License:        "PSF" or "ZPL"
URL:            http://peak.telecommunity.com/DevCenter/%{packagename}
Source0:        http://pypi.python.org/packages/source/S/%{packagename}/%{packagename}-%{version}.zip
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel
BuildRequires:  python-nose

%description
SymbolType gives you access to the peak.util.symbols module, previously
available only by installing the full PEAK toolkit. peak.util.symbols provides
a Symbol type and two built-in symbols that are used by PEAK: NOT_FOUND and
NOT_GIVEN.

%prep
%setup -q -n %{packagename}-%{version}

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%check
nosetests

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.txt
%{python_sitelib}/peak/util/*
%{python_sitelib}/%{packagename}-%{version}-py%{pyver}.egg-info
%{python_sitelib}/%{packagename}-%{version}-py%{pyver}-nspkg.pth

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-8m)
- rebuild for python-2.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- delete duplicate directory

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- import from Rawhide

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.0-2
- Rebuild for Python 2.6

* Sun Aug  3 2008 Luke Macken <lmacken@redhat.com> - 1.0-1
- Initial package for Fedora
