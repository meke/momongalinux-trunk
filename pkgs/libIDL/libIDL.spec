%global momorel 5

Summary: A library for creating trees of CORBA IDL files.
Name: libIDL

Version: 0.8.14
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL and GPL
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.8/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib-devel >= 2.18.1
BuildRequires: pkgconfig
Requires(pre): info

%description
libIDL is a library licensed under the GNU LGPL for creating trees of
CORBA Interface Definition Language (IDL) files, which is a
specification for defining portable interfaces.  libIDL was initially
written for ORBit (the ORB from the GNOME project, and the primary
means of libIDL distribution).  However, the functionality was
designed to be as reusable and portable as possible.

%package devel
Summary: Development libraries, header files and utilities for libIDL.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: filesystem
Requires: glib-devel

%description devel
libIDL is a library licensed under the GNU LGPL for creating trees of
CORBA Interface Definition Language (IDL) files, which is a
specification for defining portable interfaces.  libIDL was initially
written for ORBit (the ORB from the GNOME project, and the primary
means of libIDL distribution).  However, the functionality was
designed to be as reusable and portable as possible.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
%__mkdir_p %{buildroot}%{_prefix}/share/idl

# Do not set PATH sbin directory !!!
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/libIDL2.info.* %{_infodir}/dir

%preun devel
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/libIDL2.info.* %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING ChangeLog HACKING MAINTAINERS NEWS README*
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root)
%doc COPYING
%{_bindir}/libIDL-config-2
%{_libdir}/pkgconfig/libIDL-2.0.pc
%{_libdir}/*.a
%{_libdir}/*.so
%{_infodir}/libIDL2.info*
%{_includedir}/libIDL-2.0

%changelog
* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.14-5m)
- release a directory provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.14-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.14-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.14-2m)
- full rebuild for mo7 release

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.14-1m)
- update to 0.8.14

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.13-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.13-1m)
- update to 0.8.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.12-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.12-1m)
- update to 0.8.12
- delete patch0 (merged)

* Mon Nov 17 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.11-2m)
- libIDL-bison24.patch. from NetBSD

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.11-1m)
- update to 0.8.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.10-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.10-1m)
- update to 0.8.10

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.9-1m)
- update to 0.8.9

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.6-2m)
- comment out unnessesaly autoreconf and make check

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.6-1m)
- version up.
- GNOME 2.12.1 Desktop

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.4-2m)
- rebuild against automake

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.4-1m)
- version 0.8.4
- GNOME 2.8 Desktop

* Tue Dec 21 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.3-5m)
- add auto commands before %%build for fix missing lib*.so problem

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.3-4m)
- adjustment BuildPreReq

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.3-3m)
- rebuild against for glib-2.4.0
- GNOME 2.6 Desktop

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.8.3-2m)
- revised spec for enabling rpm 4.2.

* Thu Jan 22 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.8.3-1m)
- version 0.8.3

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (0.8.2-4m)
- devel package includes COPYING.
- changes Summary and Description.
- pretty spec file.

* Sun Oct 12 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.2-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Jul  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.2-2m)
- rebuild against rpm-4.0.4-52m

* Tue May 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.2-1m)
- version 0.8.2

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.1-1m)
- version 0.8.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.0-5m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.0-2k)
- version 0.8.0

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.4-18k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.7.4-16k)

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.4-14k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.4-12k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.4-10k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.4-8k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.4-6k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.4-4k)
- rebuild against for glib-1.3.13

* Tue Feb  5 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.4-2k)
- version 0.7.4

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.7.3-4k)
- rebuild against for glib-1.3.13

* Wed Dec 26 2001 Shingo Akagaki <dora@kondara.org>
- (0.7.1-2k)
- port form Jirai

* Mon Oct 10 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.7.1-200110013k)
- 2.0.0
