%global momorel 5

Summary:        Music Player Daemon Library
Name:           libmpd
Version:        0.20.0
Release:        %{momorel}m%{?dist}
License:        GPLv2+
URL:            http://mpd.wikia.com/wiki/Music_Player_Daemon_Wiki
Group:          System Environment/Libraries
Source0:        http://dl.sourceforge.net/sourceforge/musicpd/libmpd-%{version}.tar.gz
NoSource:       0
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  glib2-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
libmpd is an abstraction around libmpdclient. It provides an easy and
reliable callback based interface to mpd.

%package devel
Summary: Header files for developing programs with libmpd
Requires: %{name} = %{version}
Group: Development/Libraries
Requires: pkgconfig
Obsoletes: libmpd-static

%description devel
libmpd-devel is a sub-package which contains header files and static libraries
for developing program with libmpd.

%prep
%setup -q

%build
%configure --disable-static
%{__make} %{?_smp_mflags}

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}
%{__rm} -f %{buildroot}%{_libdir}/%{name}.la

%clean
rm -rf %{buildroot}

%post   -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr (-,root,root)
%doc ChangeLog COPYING README
%{_libdir}/libmpd.so.*

%files devel
%defattr (-,root,root)
%{_libdir}/libmpd.so
%{_libdir}/pkgconfig/libmpd.pc
%{_includedir}/libmpd-1.0

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.0-5m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20.0-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20.0-1m)
- update to 0.20.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18.0-1m)
- update to 0.18.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.0-3m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.0-2m)
- Obsoletes: libmpd-static

* Tue Jul 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15.0-1m)
- update to 0.15.0
- add --disable-static at configure

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.0-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.0-1m)
- initial Momonga package based on PLD
