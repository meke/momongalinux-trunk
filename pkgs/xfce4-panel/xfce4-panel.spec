%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0

Name:     xfce4-panel
Version:  4.11.0
Release:  %{momorel}m%{?dist}
Summary:  Next generation panel for xfce

Group:    User Interface/Desktops
License:  GPL
URL:      http://www.xfce.org/
Source0:  http://archive.xfce.org/src/xfce/%{name}/%{xfcever}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1:  xsession.xfce4
Source2:  xfce4rc.momonga
# clock icon taken from system-config-date, license is GPLv2+
Source3:        xfce4-clock.png
Source4:        xfce4-clock.svg
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libxfce4ui-devel
BuildRequires:  xfconf-devel
#BuildRequires:  xfce4-settings >= %{xfce4ver}
BuildRequires:  libxml2-devel >= 2.7.2
BuildRequires:  gtk2-devel
BuildRequires:  glib2-devel
BuildREquires:  exo-devel
BuildRequires:  pkgconfig
BuildRequires:  gettext
BuildRequires:  garcon-devel >= 0.1.7-1m

#Requires: xfconf >= %{xfce4ver}
#Requires: libxml2 >= 2.7.2
#Requires: Terminal
#Requires: mousepad
#Requires: xorg-x11-utils, xorg-x11-apps
#Requires: hicolor-icon-theme
#Requires: xfce4-session >= %{xfce4ver}


# xfce4-artwork isn't in Xfce 4.4
Provides: xfce4-artwork = %{version}-%{release}
Obsoletes: xfce4-artwork <= 4.2.2
# xfce4-iconbox isn't in Xfce 4.4
Provides: xfce4-iconbox = %{version}-%{release}
Obsoletes: xfce4-iconbox <= 4.2.3
# xfce4-systray isn't in Xfce 4.4
Provides: xfce4-systray = %{version}-%{release}
Obsoletes: xfce4-systray <= 4.2.3
# xfce4-toys isn't in Xfce 4.4
Provides: xfce4-toys = %{version}-%{release}
Obsoletes: xfce4-toys <= 4.2.3
# xfce4-trigger-launcher isn't in Xfce 4.4
Provides: xfce4-trigger-launcher = %{version}-%{release}
Obsoletes: xfce4-trigger-launcher <= 4.2.3
# xfce4-modemlights-plugin isn't in Xfce 4.4
Provides: xfce4-modemlights-plugin = %{version}-%{release}
Obsoletes: xfce4-modemlights-plugin <= 4.2.3
# xfce4-showdesktop-plugin isn't in Xfce 4.4
Provides: xfce4-showdesktop-plugin = %{version}-%{release}
Obsoletes: xfce4-showdesktop-plugin <= 4.2.3
# xfce4-taskbar-plugin isn't in Xfce 4.4
Provides: xfce4-taskbar-plugin = %{version}-%{release}
Obsoletes: xfce4-taskbar-plugin <= 4.2.3
# xfce4-windowlist-plugin isn't in Xfce 4.4
Provides: xfce4-windowlist-plugin = %{version}-%{release}
Obsoletes: xfce4-windowlist-plugin <= 4.2.3

%description
This package includes the panel for the Xfce desktop environment.

%package devel
Summary: Development headers for xfce4-panel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: libxfce4util-devel >= %{xfce4ver}
Requires: libxfce4ui-devel >= %{xfce4ver}

%description devel
This package includes the header files you will need to build
plugins for xfce4-panel.

%prep
%setup -q

%build
%configure LIBS="-lm"
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}

mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/session.d/
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/X11/xinit/session.d/xfce4
#mv %{buildroot}%{_sysconfdir}/xfce4/xfce4rc %{buildroot}%{_sysconfdir}/xfce4/xfce4rc.orig
#install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/xfce4/xfce4rc

find %{buildroot} -name "*.la" -delete

# remove useless dummy files
rm -f %{buildroot}/%{_bindir}/xfce4-iconbox
rm -f %{buildroot}/%{_bindir}/xftaskbar4
## we need to own these dirs
#mkdir -p %{buildroot}/%{_libexecdir}/xfce4/panel-plugins
#mkdir -p %{buildroot}/%{_datadir}/xfce4/panel-plugins

#tmp
rm -f %{buildroot}/%{_libdir}/libxfce4panel-1.0.a

%find_lang %{name}

# install additional icons
install -pm 0644 %{SOURCE3} %{buildroot}/%{_datadir}/icons/hicolor/48x48/apps/
install -pm 0644 %{SOURCE4} %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
/sbin/ldconfig

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
/sbin/ldconfig

#%%files -f %{name}.lang
%files
%defattr(-,root,root)
%doc README ChangeLog NEWS COPYING AUTHORS
%dir %{_sysconfdir}/xdg/xfce4/panel/
%config(noreplace) %{_sysconfdir}/xdg/xfce4/panel/default.xml
%{_bindir}/*
%{_libdir}/xfce4/panel/plugins/*
%{_libdir}/*.so.*
%dir %{_libdir}/xfce4/panel
%{_libdir}/xfce4/panel/*
%{_sysconfdir}/X11/xinit/session.d/xfce4
#%%{_libexecdir}/xfce4/panel-plugins/
%{_datadir}/icons/hicolor/*/*/*.png
%{_datadir}/icons/hicolor/*/*/*.svg
#%%doc %{_datadir}/xfce4/doc/*/images/*
#%%doc %{_datadir}/xfce4/doc/*/*.html
#%%dir %{_datadir}/doc/xfce4-panel-%{version}
#%%doc %{_datadir}/doc/xfce4-panel-%{version}/*
#%%{_datadir}/xfce4/panel-plugins
#%%{_datadir}/xfce4/panel-plugins/*
%dir %{_datadir}/xfce4/panel/plugins
%{_datadir}/xfce4/panel/plugins/*
%{_datadir}/applications/*.desktop
%{_datadir}/locale/*/*/*

%files devel
%defattr(-, root,root,-)
%{_libdir}/pkgconfig/*
%{_libdir}/libxfce4panel-1.0.so
%doc %{_datadir}/gtk-doc/html/libxfce4panel-1.0
%{_includedir}/xfce4/libxfce4panel-1.0

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.0-1m)
- update to 4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to version 4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-5m)
- change Source0 URI

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-4m)
- rebuild for glib 2.33.2

* Tue Aug 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-3m)
- add BuildRequires

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-2m)
- fix build failure; add BuildRequires

* Tue May  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update
- update Patch0:   xfce4-panel-4.8.0-defaults.patch

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.6-1m)
- update
- %%{_datadir}/xfce4/panel-plugins has xfce4-session package

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.4-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.4-1m)
- update to 4.6.4

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.3-2m)
- fix build

* Tue Jan 26 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.6.3-1m)
- update to 4.6.3
- modify Source0 URL
- patches from fc devel

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%define __libtoolize :

* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Fri Oct 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-2m)
- fix %%files

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Wed Jan 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.0-2m)
- add Provides and Obsoletes xfce4-artwork

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- Xfce 4.4.0
- add Requires: Terminal
- add Requires: mousepad

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3-4m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.2.3-3m)
- rebuild against expat-2.0.0-1m

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3-2m)
- delete duplicated dir

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3-1m)
- Xfce 4.2.3.2

* Wed May 25 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-2m)
- add Requires hicolor-icon-theme

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Sat Mar 19 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1.1-1m)
- update to 4.2.1.1

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1-1m)                                                   
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.0-1m)                                                   
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Sat Oct 30 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.1.90-2m)
- modify xfce4rc.momonga (soffice -> ooffice)

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90
- tmp comment out patch0
- tmp comment out patch1

* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.6-1m)
- minor bugfixes

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.0.5-1m)
- version update to 4.0.5

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-3m)
- rebuild against for gtk+-2.4.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-2m)
- rebuild against for libxml2-2.6.8
- rebuild against for glib-2.4.0

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.4-1m)

* Sat Mar  6 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.0.3-2m)
- revise ja.po

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-1m)
- version up to 4.0.3

* Mon Dec 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-1m)
- version up to 4.0.2

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.1-1m)
- version up to 4.0.1

* Tue Nov 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-4m)
- add xfce4-panel-ja.po.patch
- add gettext at BuildRequires

* Wed Oct 08 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (4.0.0-3m)
- remove Provides: xfce4 (we have xfce4 meta-package now)

* Sat Oct 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-2m)
- add Requires: XFree86-tools for use xmessage in xinitrc

* Fri Sep 26 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (4.0.0-1m)
- version up

* Sat Sep 13 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (3.99.4-1m)
- XFce4 RC4 release
