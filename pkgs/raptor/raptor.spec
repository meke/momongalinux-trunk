%global         momorel 6

Summary:        Raptor RDF Parser Toolkit for Redland
Name:           raptor
Version:        1.4.21
Release:        %{momorel}m%{?dist}
License:        LGPLv2+ and Apache
URL:            http://librdf.org/raptor/
Group:          System Environment/Libraries
Source0:        http://download.librdf.org/source/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         raptor-1.4.21-CVE-2012-0037.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libxml2-devel >= 2.6.26
BuildRequires:  curl-devel >= 7.16.0
BuildRequires:  w3c-libwww-devel
BuildRequires:  openldap-devel >= 2.4.8
BuildRequires:  openssl-devel >= 0.9.8a

%description
Raptor is the RDF Parser Toolkit for Redland that provides
a set of standalone RDF parsers, generating triples from RDF/XML
or N-Triples.

%package devel
Summary: eader files and static libraries from raptor
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This package contains the headers that programmers will need to develop
Libraries and includes files for developing programs based on raptor.

%prep
%setup -q
%patch0 -p1 -b .CVE-2012-0037

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog* INSTALL* LICENSE* NEWS* NOTICE README* RELEASE*
%{_bindir}/rapper
%{_libdir}/libraptor.so.*
%{_datadir}/gtk-doc/html/raptor
%{_mandir}/man1/rapper.1*
%{_mandir}/man1/raptor-config.1*
%{_mandir}/man3/libraptor.3*

%files devel
%defattr(-,root,root)
%{_bindir}/raptor-config
%{_includedir}/raptor.h
%{_libdir}/pkgconfig/raptor.pc
%{_libdir}/libraptor.a
%{_libdir}/libraptor.la
%{_libdir}/libraptor.so

%changelog
* Sat Mar 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.21-6m)
- [SECURITY] CVE-2012-0037
- patch for curl was included in raptor-1.4.21-CVE-2012-0037.patch

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.21-5m)
- fix build failure; add patch for curl

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.21-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.21-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.21-2m)
- full rebuild for mo7 release

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.21-1m)
- update to 1.4.21

* Sun Dec 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.20-1m)
- update to 1.4.20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.19-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.19-1m)
- update to 1.4.19

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.18-1m)
- update to 1.4.18

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.16-6m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.16-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.16-4m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.16-3m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.16-2m)
- %%NoSource -> NoSource

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.16-1m)
- update to 1.4.16

* Fri May 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.15-1m)
- version 1.4.15

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.13-2m)
- retrived libtool library

*  Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.13-1m)
- update to 1.4.13
- rebuild against curl-7.16.0

* Sat Aug 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.9-2m)
- delete libtool library

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.9-1m)
- update to 1.4.9

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.8-1m)
- version 1.4.8

* Wed Jul 06 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.7-1m)
- update to 1.4.7

* Wed Feb 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-1m)
- import from cooker

* Sun Feb 6 2005 Austin Acton <austin@mandrake.org> 1.4.5-1mdk
- 1.4.5

* Tue Jan 4 2005 Austin Acton <austin@mandrake.org> 1.4.3-1mdk
- 1.4.3

* Wed Dec 29 2004 Austin Acton <austin@mandrake.org> 1.4.2-2mdk
- rebuild

* Mon Nov 29 2004 Austin Acton <austin@mandrake.org> 1.4.2-1mdk
- 1.4.2
- source URL

* Fri Oct 29 2004 Lenny Cartier <lenny@mandrakesoft.com> 1.4.1-1mdk
- 1.4.1

* Mon Oct 26 2004 Austin Acton <austin@mandrake.org> 1.4.0-1mdk
- 1.4.0
- configure 2.5

* Fri Jul 02 2004 Rafael Garcia-Suarez <rgarciasuarez@mandrakesoft.com> 1.3.1-2mdk
- Rebuild for new curl

* Tue Jun 29 2004 Per Oyvind Karlsen <peroyvind@linux-mandrake.com> 1.3.1-1mdk
- 1.3.1
- do parallel build
- reenable libtoolize

* Wed May 19 2004 Austin Acton <austin@mandrake.org> 1.3.0-1mdk
- 1.3.0

* Mon Feb 2 2004 Austin Acton <austin@mandrake.org> 1.2.0-1mdk
- 1.2.0

* Wed Dec 31 2003 Austin Acton <austin@linux.ca> 1.1.0-1mdk
- 1.1.0
- libtoolize
- remove lib dependency

* Mon Sep 8 2003 Austin Acton <aacton@yorku.ca> 1.0.0-1mdk
- 1.0.0
- major 1

* Mon Aug 25 2003 Austin Acton <aacton@yorku.ca> 0.9.12-1mdk
- 0.9.12

* Tue Aug 5 2003 Austin Acton <aacton@yorku.ca> 0.9.11-1mdk
- 0.9.11

* Tue Jul 15 2003 Austin Acton <aacton@yorku.ca> 0.9.10-3mdk
- rebuild for rpm

* Wed May 14 2003 Austin Acton <aacton@yorku.ca> 0.9.10-2mdk
- fix library naming

* Wed May 7 2003 Austin Acton <aacton@yorku.ca> 0.9.10-1mdk
- 0.9.10

* Wed Apr 2 2003 Austin Acton <aacton@yorku.ca> 0.9.9-1mdk
- 0.9.9

* Tue Feb 18 2003 Austin Acton <aacton@yorku.ca> 0.9.8-1mdk
- initial package
