%global momorel 21

Summary:	MP3::Info module for perl
Name:		perl-MP3-Info
Version:	1.24
Release:	%{momorel}m%{?dist}
License:	Artistic
Group:		Development/Libraries
URL:		http://www.cpan.org/modules/by-module/MP3/
Source0:	http://www.cpan.org/modules/by-module/MP3/MP3-Info-%{version}.tar.gz 
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	perl >= 5.8.5
BuildArch:	noarch

%description
MP3::Info module for perl

# Provide perl-specific find-{provides,requires}.
%global __find_provides /usr/lib/rpm/find-provides.perl
%global __find_requires /usr/lib/rpm/find-requires.perl

%prep
%setup -q -n MP3-Info-%{version}

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make


%clean 
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
make PERL_INSTALL_ROOT=%{buildroot} install

find %{buildroot}/usr -type f -print | \
	sed "s@^%{buildroot}@@g" | \
	grep -v perllocal.pod | \
	sed -e 's,\(.*/man/.*\),\1*,' | \
	grep -v "\.packlist" > MP3-Info-%{version}-filelist
if [ "$(cat MP3-Info-%{version}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

rm -f %{buildroot}%{perl_vendorarch}/auto/MP3/Info/.packlist

%files -f MP3-Info-%{version}-filelist
%defattr(-,root,root)

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.24-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-2m)
- rebuild against rpm-4.6

* Sun Nov  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.23-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.23-2m)
- %%NoSource -> NoSource

* Sat Jul 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.22-2m)
- use vendor

* Sat Mar 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Fri Jan  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.20-1m)
- update to 1.20

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.13-3m)
- rebuild against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.13-2m)
- rebuilt against perl-5.8.7

* Wed Apr 13 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13-1m)
- update 1.13

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (1.02-4m)
- noarch... orz

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.02-3m)
- rebuild against perl-5.8.5

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.02-2m)
- revised spec for rpm 4.2.

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.02-1m)

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.01-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.01-3m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.01-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Fri Jan 24 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.01-1m)
- import to momonga

