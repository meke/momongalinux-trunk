%global momorel 7

Summary:	C library for parsing command line parameters
Name:		popt
Version:	1.16
Release:	%{momorel}m%{?dist}
License:	MIT
Group:		System Environment/Libraries
URL:		http://www.rpm5.org/
Source0: http://www.rpm5.org/files/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRequires:	gettext, doxygen, graphviz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Popt is a C library for parsing command line parameters. Popt was
heavily influenced by the getopt() and getopt_long() functions, but
it improves on them by allowing more powerful argument expansion.
Popt can parse arbitrary argv[] style arrays and automatically set
variables based on command line arguments. Popt allows command line
arguments to be aliased via configuration files and includes utility
functions for parsing arbitrary strings into argv[] arrays using
shell-like rules.

%package devel
Summary:	Development files for the popt library
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
The popt-devel package includes header files and libraries necessary
for developing programs which use the popt C library. It contains the
API documentation of the popt library, too.

%package static
Summary:	Static library for parsing command line parameters
Group:		Development/Libraries
Requires:	%{name}-devel = %{version}-%{release}

%description static
The popt-static package includes static libraries of the popt library.
Install it if you need to link statically with libpopt.

%prep
%setup -q

%build
%configure 
%make
doxygen

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,' pkgconfigdir=%{_libdir}/pkgconfig

rm -f %{buildroot}/%{_libdir}/libpopt.la

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc CHANGES COPYING
%{_libdir}/libpopt.so.*

%files devel
%defattr(-,root,root)
%doc README doxygen/html
%{_libdir}/libpopt.so
%{_libdir}/pkgconfig/popt.pc
%{_includedir}/popt.h
%{_mandir}/man3/popt.3*

%files static
%defattr(-,root,root)
%{_libdir}/libpopt.a

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.16-7m)
- rebuild against graphviz-2.36.0-1m

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.16-6m)
- support UserMove env

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.16-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.16-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.16-3m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.16-2m)
- fix build on x86_64

* Sat Jul 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.16-1m)
- update 1.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Apr 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.15-1m)
- update 1.15

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14-2m)
- rebuild against rpm-4.6

* Mon Apr  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14-1m)
- update 1.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13-2m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13-2m)
- %%NoSource -> NoSource

* Mon Jan  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13-1m)
- update 1.13

* Tue Nov  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-1m)
- import from Fedora

* Thu Aug 23 2007 Robert Scheck <robert@fedoraproject.org> 1.12-3
- Added buildrequirement to graphviz (#249352)
- Backported bugfixes from CVS (#102254, #135428 and #178413)

* Sun Aug 12 2007 Robert Scheck <robert@fedoraproject.org> 1.12-2
- Move libpopt to /lib[64] (#249814)
- Generate API documentation, added buildrequirement to doxygen

* Mon Jul 23 2007 Robert Scheck <robert@fedoraproject.org> 1.12-1
- Changes to match with Fedora Packaging Guidelines (#249352)

* Tue Jul 10 2007 Jeff Johnson <jbj@rpm5.org>
- release popt-1.12 through rpm5.org.

* Sat Jun  9 2007 Jeff Johnson <jbj@rpm5.org>
- release popt-1.11 through rpm5.org.

* Thu Dec 10 1998 Michael Johnson <johnsonm@redhat.com>
- released 1.2.2; see CHANGES

* Tue Nov 17 1998 Michael K. Johnson <johnsonm@redhat.com>
- added man page to default install

* Thu Oct 22 1998 Erik Troan <ewt@redhat.com>
- see CHANGES file for 1.2

* Thu Apr 09 1998 Erik Troan <ewt@redhat.com>
- added ./configure step to spec file
