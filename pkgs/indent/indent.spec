%global momorel 4

Summary: A GNU program for formatting C code.
Name: indent
Version: 2.2.11
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
URL: http://indent.isidore-it.eu/beautify.html
Source0: http://indent.isidore-it.eu/%{name}-%{version}.tar.gz
Nosource: 0
Requires(post): info
Requires(preun): info
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Indent is a GNU program for beautifying C code, so that it is easier to
read.  Indent can also convert from one C writing style to a different
one.  Indent understands correct C syntax and tries to handle incorrect
C syntax.

Install the indent package if you are developing applications in C and
you want a program to format your code.

%prep
%setup -q

%build

%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

%makeinstall
rm -f %{buildroot}/%{_infodir}/dir
rm -f %{buildroot}/usr/doc/indent/indent.html
%find_lang %{name}

%check
echo ===============TESTING===============
make -C regression
echo ===============TESTING END===========

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/indent.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/indent.info %{_infodir}/dir || :
fi

%files -f %{name}.lang
%defattr(-,root,root)
%{_bindir}/indent
%{_bindir}/texinfo2man
%{_mandir}/man1/*
%{_infodir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.11-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.11-1m)
- update to 2.2.11

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.10-2m)
- use Requires

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.10-1m)
- update to 2.2.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-7m)
- rebuild against rpm-4.6

* Fri Nov 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-6m)
- remove --entry option of install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.9-5m)
- rebuild against gcc43

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.9-4m)
- add gcc4 patch.
- Patch0: indent-2.2.9-gcc4.patch

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.9-3m)
- revised spec for enabling rpm 4.2.

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.9-2m)
- fix URL tag

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.9-1m)
  update to 2.2.9

* Thu Nov 21 2002 WATABE Toyokazu <toy2@nidone.org>
- (2.2.8-1m)
- update to 2.2.8.

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.2.7-2k)
- ver up.

* Tue Apr 30 2002 Kenta Murata <muraken@kondara.org>
- /sbin/install-info -> info in PreReq.

* Wed Nov 22 2000 Kenichi Matsubara <m@kondara.org>
- bugfix Source section.

* Sun Nov 19 2000 Takaaki Tabuchi <tab@kondara.org.
- update version 2.2.6.

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Fri Jul 21 2000 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jun 08 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%configure, %%makeinstall, %%{_infodir}, %%{_mandir} 
  and %%{_tmppath}
- don't use %%{_prefix}

* Wed May 10 2000 Trond Eivind Glomsrod <teg@redhat.com>
- added URL
- remove manual stripping


* Thu Feb 03 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed

* Thu Jan 20 2000 Bill Nottingham <notting@redhat.com>
- 2.2.5

* Mon Jul 26 1999 Bill Nottingham <notting@redhat.com>
- 2.2.0

* Fri Jul 16 1999 Bill Nottingham <notting@redhat.com>
- update to 2.1.1

* Sun May 30 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.10.0.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 11)

* Fri Dec 18 1998 Bill Nottingham <notting@redhat.com>
- build for 6.0 tree

* Thu Aug 13 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Oct 21 1997 Otto Hammersmith <otto@redhat.com>
- use install-info

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
