%global momorel 8

Name:           rpcbind
Version:        0.2.0
Release:	%{momorel}m%{?dist}
Summary:        Universal Addresses to RPC Program Number Napper
Group:          System Environment/Daemons
License:        GPL
URL:            http://sourceforge.net/projects/rpcbind/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:  	http://downloads.sourceforge.net/rpcbind/%{name}-%{version}.tar.bz2
NoSource: 	0
Source1:	rpcbind.service
Source2:	rpcbind.socket
Source3:        rpcbind.sysconfig

Patch1: 	rpcbind-0_2_1-rc4.patch

Requires: glibc-common >= 2.6
#Requires: setup >= 2.6.4-1
#Conflicts: man-pages < 2.43-12
BuildRequires: automake, autoconf, libtool systemd-units
BuildRequires: libtirpc-devel quota-devel perl tcp_wrappers-devel
Requires(pre): shadow-utils >= 4.1.4
Requires(post): chkconfig systemd-units systemd-sysv
Requires(preun): systemd-units
Requires(postun): systemd-units coreutils


Provides: portmap = 4.0
Obsoletes: portmap

%description
The rpcbind utility is a server that converts RPC program numbers into
universal addresses.  It must be running on the host to be able to make
RPC calls on a server on that machine.

%prep
%setup -q
%patch1 -p1

%build
%ifarch s390 s390x
PIE="-fPIE"
%else
PIE="-fpie"
%endif
export PIE

RPCBUSR=rpc
RPCBDIR=/var/lib/rpcbind
CFLAGS="`echo $RPM_OPT_FLAGS $ARCH_OPT_FLAGS $PIE`"

autoreconf -fisv
%configure CFLAGS="$CFLAGS" LDFLAGS="-pie" \
    --enable-warmstarts \
    --with-statedir="$RPCBDIR" \
    --with-rpcuser="$RPCBUSR" \
    --enable-libwrap \
    --enable-debug

%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

mkdir -p %{buildroot}{/sbin,/usr/sbin}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_mandir}/man8 
mkdir -p %{buildroot}/var/lib/rpcbind
mkdir -p %{buildroot}/etc/sysconfig/
make DESTDIR=%{buildroot} install

mv -f ${RPM_BUILD_ROOT}%{_bindir}/rpcbind %{buildroot}/sbin
mv -f ${RPM_BUILD_ROOT}%{_bindir}/rpcinfo %{buildroot}/%{_sbindir}
install -m644 %{SOURCE1} %{buildroot}%{_unitdir}
install -m644 %{SOURCE2} %{buildroot}%{_unitdir}
install -m644 %{SOURCE3} %{buildroot}/etc/sysconfig/rpcbind

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%pre
# Check the validity of the rpc uid and gid.
# If they don't exist, create them
# If they exist but are the wrong value, remove them 
#   and recreate them with the correct value
# If they exist and are the correct value do nothing
rpcid=`getent passwd rpc | cut -d: -f 3`
if [ -n "$rpcid" -a "$rpcid" != "32" ]; then
       /usr/sbin/userdel  rpc 2> /dev/null || :
       /usr/sbin/groupdel rpc 2> /dev/null || : 
fi
if [ -z "$rpcid" -o "$rpcid" != "32" ]; then
/usr/sbin/groupadd -o -g 32 rpc > /dev/null 2>&1
/usr/sbin/useradd -o -l -c "Rpcbind Daemon" -d /var/lib/rpcbind -g 32 \
    -M -s /sbin/nologin -u 32 rpc > /dev/null 2>&1
fi
%post 
if [ $1 -eq 1 ]; then
  # Initial installation
  /bin/systemctl enable rpcbind.service >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ]; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable rpcbind.service >/dev/null 2>&1 || :
  /bin/systemctl stop rpcbind.service >/dev/null 2>&1 || :
  /usr/sbin/userdel  rpc 2>/dev/null || :
  /usr/sbin/groupdel rpc 2>/dev/null || :
  rm -rf /var/lib/rpcbind
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ]; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart rpcbind.service >/dev/null 2>&1 || :
fi  

%triggerun -- rpcbind < 0.2.0-6m
%{_bindir}/systemd-sysv-convert --save rpcbind >/dev/null 2>&1 ||:
/bin/systemctl --no-reload enable rpcbind.service >/dev/null 2>&1:
/sbin/chkconfig --del rpcbind >/dev/null 2>&1 || :
/bin/systemctl try-restart rpcbind.service >/dev/null 2>&1 || :

%files
%doc AUTHORS ChangeLog README
%config(noreplace) /etc/sysconfig/rpcbind
/sbin/rpcbind
%{_sbindir}/rpcinfo
%{_mandir}/man8/*
%{_unitdir}/rpcbind.service
%{_unitdir}/rpcbind.socket

%dir %attr(700,rpc,rpc) /var/lib/rpcbind

%changelog
* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-8m)
- update service files

* Tue Sep 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.0-7m)
- update %%pre section

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.0-6m)
- update post section

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.0-5m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Tue Jan 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-5m)
- fix %%pre failure by adding "-f" option to userdel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-3m)
- fix BuildRequires

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-2m)
- rebuild against rpm-4.6

* Wed Dec 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7
- add %%{_sysconfigdir}/sysconfig/rpcbind

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.4-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-5m)
- %%NoSource -> NoSource

* Tue Dec 18 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-4m)
- sync with fc-devel (rpcbind-0_1_4-12_fc9)
--* Mon Dec 17 2007 Steve Dickson <steved@redhat.com> 0.1.4-12
--- Changed is_loopback() and check_access() see if the calling
--  address is an address on a local interface, just not a loopback
--  address (bz 358621).
--* Wed Oct 17 2007 Steve Dickson <steved@redhat.com> 0.1.4-11
--- Reworked logic in initscript so the correct exit is 
--  used when networking does not exist or is set up
--  incorrectly.
--* Tue Oct 16 2007 Steve Dickson <steved@redhat.com> 0.1.4-10
--- Corrected a typo in the initscript from previous 
--  commit.
--* Mon Oct 15 2007 Steve Dickson <steved@redhat.com> 0.1.4-9
--- Fixed typo in Summary (bz 331811)
--- Corrected init script (bz 247046)
--* Sat Sep 15 2007 Steve Dickson <steved@redhat.com> 0.1.4-8
--- Fixed typo in init script (bz 248285)
--- Added autoconf rules to turn on secure host checking
--  via libwrap. Also turned on host check by default (bz 248284)
--- Changed init script to start service in runlevel 2 (bz 251568)
--- Added a couple missing Requires(pre) (bz 247134)

* Wed Jun 20 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.1.4-3m)
- stop auto start

* Mon May 28 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-2m)
- sync with fc-devel (rpcbind-0_1_4-7_fc8)
--* Fri May 25 2007 Steve Dickson <steved@redhat.com> 0.1.4-7
--- Fixed condrestarts (bz 241332)
--* Tue May 22 2007 Steve Dickson <steved@redhat.com> 0.1.4-6
--- Fixed an ipv6 related segfault on startup (bz 240873)

* Sat May 19 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org> 
- (0.1.4-1m)
- import from fc-devel 0.1.4-5
