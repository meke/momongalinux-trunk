%global momorel 9

Name:           deja-dup
Version:        14.2
Release:        %{momorel}m%{?dist}
Summary:        Simple backup tool and frontend for duplicity

Group:          Applications/Archiving
License:        GPLv3+
URL:            https://launchpad.net/deja-dup
Source0:        http://launchpad.net/%{name}/14/%{version}/+download/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:		deja-dup-14.2-libnotify-0.7.2.patch
BuildRequires:  gettext desktop-file-utils intltool scrollkeeper
BuildRequires:  gnome-doc-utils pango-devel cairo-devel vala-devel
BuildRequires:  libtool GConf2 GConf2-devel glib2-devel libnotify-devel >= 0.7.2
BuildRequires:  po4a unique-devel nautilus-devel gcr-devel
Requires:       duplicity
Requires(pre):  GConf2
Requires(post): GConf2
Requires(preun):GConf2

%description
Deja is a simple backup tool. It hides the complexity of doing backups the 
'right way' (encrypted, off-site, and regular) and uses duplicity as the 
backend.

Features:
- Support for local or remote backup locations, including Amazon S3
- Securely encrypts and compresses your data
- Incrementally backs up, letting you restore from any particular backup
- Schedules regular backups
- Integrates well into your GNOME desktop

%prep
%setup -q
%patch0 -p1 -b .libnotify

%build
%configure --disable-static --disable-schemas-install
make %{?_smp_mflags} CFLAGS+="`pkg-config --cflags dbus-glib-1`" LIBS+="`pkg-config --libs dbus-glib-1`"

%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot}
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL
rm -f %{buildroot}/%{_libdir}/nautilus/extensions-2.0/*.la
rm -f %{buildroot}/%{_datadir}/icons/Humanity/

# revise deja-dup.desktop
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Utility \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%find_lang %{name} --with-man --with-gnome 

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/deja-dup.schemas \
    > /dev/null || :
fi

%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/deja-dup.schemas \
  > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/deja-dup.schemas \
    > /dev/null || :
fi

%postun
if [ "$1" -eq 0 ]; then
    gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
fi

%posttrans
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%files -f %{name}.lang 
%defattr(-,root,root,-)
%doc COPYING NEWS 
%{_bindir}/deja-*
%{_mandir}/man1/deja-dup.1*
%{_mandir}/man1/deja-dup-monitor.1*
%{_mandir}/man1/deja-dup-preferences.1*
%{_mandir}/*/man1/deja-dup-monitor.1*
%{_mandir}/*/man1/deja-dup-preferences.1*
%{_sysconfdir}/gconf/schemas/deja-dup.schemas
%{_sysconfdir}/xdg/autostart/deja-dup-monitor.desktop
%{_libdir}/nautilus/extensions-2.0/libnautilus-deja-dup.so
%{_datadir}/applications/deja-dup.desktop
%{_datadir}/icons/hicolor/scalable/actions/deja-dup-backup.svg
%{_datadir}/icons/hicolor/scalable/actions/deja-dup-restore.svg
%{_datadir}/icons/hicolor/scalable/apps/deja-dup.svg
%{_datadir}/icons/hicolor/scalable/apps/deja-dup-symbolic.svg

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (14.2-9m)
- rebuild for glib 2.33.2

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (14.2-8m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (14.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (14.2-6m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (14.2-5m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (14.2-4m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (14.2-3m)
- remove Utility from Categories of deja-dup.desktop

* Sat Jul 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (14.2-2m)
- fix scripts

* Sat Jul 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (14.2-1m)
- import from Fedora 13

* Sun Jun 06 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 14.2-2
- Drop the dependency on yelp. Fixes rhbz #592751

* Sun Jun 06 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 14.2-1
- Several bug fixes including a potential data loss fix

* Sat May 01 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 14.1-1
- https://launchpad.net/deja-dup/+announcement/5730
- Fix critical bugs preventing backup to external disks and restore single dir

* Sun Apr 18 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 14.0.3-1
- https://launchpad.net/deja-dup/+announcement/5630
- fix restoring to a non-empty directory

* Mon Apr 12 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 14.0.2-1
- https://launchpad.net/deja-dup/+announcement/5544
- drop the clean section

* Thu Apr 01 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 14.0-1
- new upstream release
- http://bazaar.launchpad.net/~deja-dup-team/deja-dup/14/annotate/head:/NEWS
- Gconf schema installation. Fixes rhbz #577004

* Mon Mar 20 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 13.92-1
- new upstream release
- https://launchpad.net/deja-dup/+announcement/5313

* Mon Mar 01 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 13.91-1
- new upstream release
- Fix review issues

* Tue Dec 22 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 13.4-1
- new upstream release
- http://bazaar.launchpad.net/~deja-dup-team/deja-dup/trunk/revision/557#NEWS

* Tue Dec 08 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 13.3-1
- new upstream release

* Tue Nov 23 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 11.1-1
- Initial spec
