%global momorel 1
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: A featureful Unicode character map. 
Name: gucharmap
Version: 3.6.0
Release: %{momorel}m%{?dist}
Group: Applications/System
License: GPL
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
URL: http://gucharmap.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: gtk3-devel
BuildRequires: libgnome-devel >= 2.26.0
BuildRequires: pango-devel >= 1.24.0
BuildRequires: glib2-devel >= 2.20.1
BuildRequires: openssl-devel >= 0.9.8g
BuildRequires: gnome-doc-utils-devel >= 0.16.0
Requires(pre): hicolor-icon-theme gtk2
Requires: rarian
Requires: libgnomeui
Requires: libgnome

%description
gucharmap is a featureful Unicode character map and font viewer.

%package devel
Summary: Header files, libraries and development documentation for %{name}.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libgnomeui-devel
Requires: gtk2-devel
Requires: libgnome-devel

%description devel
This package contains the header files, static libraries and development
documentation for %{name}.

%prep
%setup -q

%build
%configure --enable-silent-rules \
    --disable-scrollkeeper \
    --disable-schemas-install \
    --enable-python-bindings \
    --enable-introspection=yes \
    --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/gucharmap.schemas \
    > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/gucharmap.schemas \
      > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gucharmap.schemas \
	> /dev/null || :
fi

%postun
rarian-sk-update

%files -f %{name}.lang
%defattr (-, root, root)
%doc COPYING* ChangeLog NEWS README TODO
#%{_sysconfdir}/gconf/schemas/gucharmap.schemas
%{_bindir}/charmap
%{_bindir}/gnome-character-map
%{_bindir}/gucharmap
%{_libdir}/libgucharmap_2_90.so.*
%exclude %{_libdir}/lib*.la
%{_datadir}/applications/gucharmap.desktop
%{_datadir}/help/*/gucharmap
#%{_datadir}/omf/gucharmap
%{_datadir}/glib-2.0/schemas/*.xml
%{_libdir}/girepository-1.0/Gucharmap-2.90.typelib

%files devel
%defattr (-, root, root)
%{_libdir}/pkgconfig/gucharmap-2.90.pc
%{_libdir}/lib*.so
%{_includedir}/gucharmap-2.90
%doc %{_datadir}/gtk-doc/html/gucharmap-2.90
%{_datadir}/gir-1.0/Gucharmap-2.90.gir

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update to 3.6.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for glib 2.33.2

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Sun Nov 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.29.1-2m)
- add patch for gtk-2.20 by gengtk220patch

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.1-1m)
- update to 2.29.1

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.3.1-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3.1-1m)
- update to 2.26.3.1

* Mon Jun 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Thu Oct 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1.1-1m)
- update to 2.24.1.1

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Mon Sep 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.4-1m)
- update to 1.10.4

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-1m)
- update to 1.10.1

* Wed Mar 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.0-2m)
- add BuildPreReq: gnome-doc-utils >= 0.9.0

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0 (unstable?)

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update 1.8.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.6.0-5m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-4m)
- delete libtool library

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-3m)
- delete duplicated dir

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Tue Nov 22 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.4-1m)
- version up
- GNOME 2.12.1 Desktop

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.2-1m)
- version 1.4.2
- GNOME 2.8 Desktop

* Fri Jun 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.1-3m)
- cancel %%requires_eq gtk+

* Mon May 23 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.1-2m)
- prereq scrollkeeper.

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.1-1m)
- version up

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.0.0-1m)
- version 1.0.0

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.0-1m)
- version 0.9.0

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.0-1m)
- version 0.8.0

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.1-3m)
- rebuild against for gtk+-2.2.2

* Fri May 30 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.6.1-2m)
  LD_LIBRARY_PATH

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.1-1m)
- version 0.6.1

* Mon May 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.0-1m)
- version 0.6.0
