%global momorel 52
%global srcname miyagawa-plagger-377a54b
%global mime_lite_srcname MIME-Lite-3.01_05
%global __perl_requires %{_tmppath}/%{name}-%{version}.requires
%global __perl_provides %{_tmppath}/%{name}-%{version}.provides

Summary: Pluggable RSS/Atom Aggregator
Name: perl-Plagger
Version: 0.7.17
Release: %{momorel}m%{?dist}
License: GPL+ or Artistic
Group: Development/Languages
Source0: %{srcname}.tar.gz
Source1: %{mime_lite_srcname}.tar.gz
Patch0: plagger-use-old-mime-lite.patch
Patch1: MIME-Lite-3.01_05-silent.patch
URL: http://plagger.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: perl-Audio-Beep >= 0.11
BuildRequires: perl-Cache-Cache
BuildRequires: perl-Cache-FastMmap >= 1.09
BuildRequires: perl-Class-Accessor >= 0.27
BuildRequires: perl-Config-IniFiles
BuildRequires: perl-DBIx-Class >= 0.07002
BuildRequires: perl-Data-ICal >= 0.08
BuildRequires: perl-Data-Serializer >= 0.36
BuildRequires: perl-DateTime >= 0.34
BuildRequires: perl-DateTime-Format-Epoch >= 0.10
BuildRequires: perl-DateTime-Format-ICal >= 0.08
BuildRequires: perl-DateTime-Format-Japanese >= 0.03
BuildRequires: perl-DateTime-Format-Mail >= 0.30
BuildRequires: perl-DateTime-Format-Strptime >= 1.0700
BuildRequires: perl-DateTime-Format-W3CDTF >= 0.04
BuildRequires: perl-DateTime-Locale >= 0.32
BuildRequires: perl-DateTime-TimeZone >= 0.54
BuildRequires: perl-Digest-HMAC >= 1.01
BuildRequires: perl-Email-Address >= 1.870
BuildRequires: perl-Encode-Detect >= 1.00
BuildRequires: perl-File-Copy-Recursive >= 0.28
BuildRequires: perl-File-Find-Rule >= 0.30
BuildRequires: perl-File-Grep >= 0.02
BuildRequires: perl-File-HomeDir >= 0.58
BuildRequires: perl-File-Mork
BuildRequires: perl-Flickr-API >= 0.08
BuildRequires: perl-Gungho
BuildRequires: perl-HTML-Format >= 2.04
BuildRequires: perl-HTML-Parser >= 3.54
BuildRequires: perl-HTML-RSSAutodiscovery >= 1.21
BuildRequires: perl-HTML-ResolveLink >= 0.03
BuildRequires: perl-HTML-Scrubber >= 0.08
BuildRequires: perl-HTML-Tidy >= 1.07-0.1.1m
BuildRequires: perl-HTML-Tree
BuildRequires: perl-HTML-TreeBuilder-XPath >= 0.06
BuildRequires: perl-HTTP-Async >= 0.07
BuildRequires: perl-HTTP-Cookies-Mozilla >= 1.09
BuildRequires: perl-Hatena-Keyword >= 0.04
BuildRequires: perl-IP-Country >= 2.21
BuildRequires: perl-IPC-Run >= 0.80
BuildRequires: perl-Image-Info >= 1.22
BuildRequires: perl-KinoSearch >= 0.162
BuildRequires: perl-Lingua-EN-Summarize >= 0.2
BuildRequires: perl-Lingua-JA-Summarize-Extract >= 0.01
BuildRequires: perl-Lingua-ZH-HanDetect >= 0.04
BuildRequires: perl-MIME-Lite >= 3.01
BuildRequires: perl-MIME-Types >= 1.17
BuildRequires: perl-Mail-IMAPClient >= 2.2.9
BuildRequires: perl-MailTools
BuildRequires: perl-Module-Pluggable-Fast >= 0.18
BuildRequires: perl-Net-DNS >= 0.59
BuildRequires: perl-Net-Delicious >= 1.01
BuildRequires: perl-Net-Google-Calendar
BuildRequires: perl-Net-MovableType >= 1.74
BuildRequires: perl-Net-NetSend >= 0.12
BuildRequires: perl-Net-SMTP-TLS >= 0.12-7m
BuildRequires: perl-Net-Twitter
BuildRequires: perl-PDF-FromHTML >= 0.20
BuildRequires: perl-POE >= 0.38
BuildRequires: perl-POE-Component-Client-DNS >= 0.99
BuildRequires: perl-POE-Component-Client-HTTP >= 0.78
BuildRequires: perl-POE-Component-IKC >= 0.1802
BuildRequires: perl-POE-Component-IRC >= 5.04
BuildRequires: perl-Palm-PalmDoc >= 0.13
BuildRequires: perl-Regexp-Common-profanity_us >= 2.2
BuildRequires: perl-SOAP-Lite >= 1.000
BuildRequires: perl-SWF-Builder >= 0.14
BuildRequires: perl-Search-Estraier >= 0.07
BuildRequires: perl-Spreadsheet-WriteExcel >= 2.17
BuildRequires: perl-Template-Plugin-JavaScript >= 0.01
BuildRequires: perl-Template-Provider-Encoding >= 0.05
BuildRequires: perl-Template-Toolkit >= 2.15
BuildRequires: perl-Term-Encoding >= 0.02
BuildRequires: perl-Test-Base >= 0.52
BuildRequires: perl-Test-Pod >= 1.26
BuildRequires: perl-Test-Pod-Coverage >= 1.08
BuildRequires: perl-Test-Simple
BuildRequires: perl-Text-CSV
BuildRequires: perl-Text-CSV_PP >= 1.01
BuildRequires: perl-Text-Emoticon
BuildRequires: perl-Text-Hatena >= 0.15
BuildRequires: perl-Text-Kakasi >= 2.04
BuildRequires: perl-Text-Language-Guess >= 0.02
BuildRequires: perl-Text-Markdown >= 1.0.3
BuildRequires: perl-Text-Original >= 1.4
BuildRequires: perl-Text-Tags >= 0.04
BuildRequires: perl-Text-WrapI18N >= 0.06
BuildRequires: perl-Time-Date
BuildRequires: perl-Time-Duration-Parse >= 0.02
BuildRequires: perl-UNIVERSAL-require >= 0.10
BuildRequires: perl-URI-Fetch >= 0.08
BuildRequires: perl-URI-Find >= 0.16
BuildRequires: perl-Unicode-Normalize
BuildRequires: perl-WWW-Babelfish >= 0.15
BuildRequires: perl-WWW-HatenaDiary
BuildRequires: perl-WWW-Mechanize >= 1.20
BuildRequires: perl-WWW-Mixi >= 0.48
BuildRequires: perl-WWW-Mixi-Scraper >= 0.14
BuildRequires: perl-WebService-Bloglines >= 0.11
BuildRequires: perl-WebService-BuzzurlAPI
BuildRequires: perl-WebService-YouTube >= 0.04
BuildRequires: perl-XMLRPC-Lite
BuildRequires: perl-XML-Atom >= 0.23
BuildRequires: perl-XML-FOAF >= 0.03
BuildRequires: perl-XML-Feed >= 0.12
BuildRequires: perl-XML-LibXML >= 1.62
BuildRequires: perl-XML-Liberal >= 0.16
BuildRequires: perl-XML-RSS-LibXML >= 0.3002
BuildRequires: perl-XML-RSS-Liberal >= 0.01
BuildRequires: perl-XML-XPath >= 1.13
BuildRequires: perl-YAML >= 0.58
BuildRequires: perl-YAML-Syck >= 0.67
BuildRequires: perl-libwww-perl
BuildRequires: spamassassin
BuildRequires: subversion-perl >= 1.3.2
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires: perl-Audio-Beep >= 0.11
Requires: perl-Cache-Cache
Requires: perl-Cache-FastMmap >= 1.09
Requires: perl-Class-Accessor >= 0.27
Requires: perl-Config-IniFiles
Requires: perl-DBIx-Class >= 0.07002
Requires: perl-Data-ICal >= 0.08
Requires: perl-Data-Serializer >= 0.36
Requires: perl-DateTime >= 0.34
Requires: perl-DateTime-Format-Epoch >= 0.10
Requires: perl-DateTime-Format-ICal >= 0.08
Requires: perl-DateTime-Format-Japanese >= 0.03
Requires: perl-DateTime-Format-Mail >= 0.30
Requires: perl-DateTime-Format-Strptime >= 1.0700
Requires: perl-DateTime-Format-W3CDTF >= 0.04
Requires: perl-DateTime-Locale >= 0.32
Requires: perl-DateTime-TimeZone >= 0.54
Requires: perl-Digest-HMAC >= 1.01
Requires: perl-Email-Address >= 1.870
Requires: perl-Encode-Detect >= 1.00
Requires: perl-File-Copy-Recursive >= 0.28
Requires: perl-File-Find-Rule >= 0.30
Requires: perl-File-Grep >= 0.02
Requires: perl-File-HomeDir >= 0.58
Requires: perl-File-Mork
Requires: perl-Flickr-API >= 0.08
Requires: perl-Gungho
Requires: perl-HTML-Format >= 2.04
Requires: perl-HTML-Parser >= 3.54
Requires: perl-HTML-RSSAutodiscovery >= 1.21
Requires: perl-HTML-ResolveLink >= 0.03
Requires: perl-HTML-Scrubber >= 0.08
Requires: perl-HTML-Tidy >= 1.07-0.1.1m
Requires: perl-HTML-Tree
Requires: perl-HTML-TreeBuilder-XPath >= 0.06
Requires: perl-HTTP-Async >= 0.07
Requires: perl-HTTP-Cookies-Mozilla >= 1.09
Requires: perl-Hatena-Keyword >= 0.04
Requires: perl-IP-Country >= 2.21
Requires: perl-IPC-Run >= 0.80
Requires: perl-Image-Info >= 1.22
Requires: perl-KinoSearch >= 0.162
Requires: perl-Lingua-EN-Summarize >= 0.2
Requires: perl-Lingua-JA-Summarize-Extract >= 0.01
Requires: perl-Lingua-ZH-HanDetect >= 0.04
Requires: perl-MIME-Lite >= 3.01
Requires: perl-MIME-Types >= 1.17
Requires: perl-Mail-IMAPClient >= 2.2.9
Requires: perl-MailTools
Requires: perl-Module-Pluggable-Fast >= 0.18
Requires: perl-Net-DNS >= 0.59
Requires: perl-Net-Delicious >= 1.01
Requires: perl-Net-Google-Calendar
Requires: perl-Net-MovableType >= 1.74
Requires: perl-Net-NetSend >= 0.12
Requires: perl-Net-SMTP-TLS >= 0.12
Requires: perl-Net-Twitter
Requires: perl-PDF-FromHTML >= 0.20
Requires: perl-POE >= 0.38
Requires: perl-POE-Component-Client-DNS >= 0.99
Requires: perl-POE-Component-Client-HTTP >= 0.78
Requires: perl-POE-Component-IKC >= 0.1802
Requires: perl-POE-Component-IRC >= 5.04
Requires: perl-Palm-PalmDoc >= 0.13
Requires: perl-Regexp-Common-profanity_us >= 2.2
Requires: perl-SOAP-Lite >= 1.000
Requires: perl-SWF-Builder >= 0.14
Requires: perl-Search-Estraier >= 0.07
Requires: perl-Spreadsheet-WriteExcel >= 2.17
Requires: perl-Template-Plugin-JavaScript >= 0.01
Requires: perl-Template-Provider-Encoding >= 0.05
Requires: perl-Template-Toolkit >= 2.15
Requires: perl-Term-Encoding >= 0.02
Requires: perl-Test-Base >= 0.52
Requires: perl-Test-Pod >= 1.26
Requires: perl-Test-Pod-Coverage >= 1.08
Requires: perl-Test-Simple
Requires: perl-Text-CSV
Requires: perl-Text-CSV_PP >= 1.01
Requires: perl-Text-Emoticon
Requires: perl-Text-Hatena >= 0.15
Requires: perl-Text-Kakasi >= 2.04
Requires: perl-Text-Language-Guess >= 0.02
Requires: perl-Text-Markdown >= 1.0.3
Requires: perl-Text-Original >= 1.4
Requires: perl-Text-Tags >= 0.04
Requires: perl-Text-WrapI18N >= 0.06
Requires: perl-Time-Date
Requires: perl-Time-Duration-Parse >= 0.02
Requires: perl-UNIVERSAL-require >= 0.10
Requires: perl-URI-Fetch >= 0.08
Requires: perl-URI-Find >= 0.16
Requires: perl-Unicode-Normalize
Requires: perl-WWW-Babelfish >= 0.15
Requires: perl-WWW-HatenaDiary
Requires: perl-WWW-Mechanize >= 1.20
Requires: perl-WWW-Mixi >= 0.48
Requires: perl-WWW-Mixi-Scraper >= 0.14
Requires: perl-WebService-Bloglines >= 0.11
Requires: perl-WebService-BuzzurlAPI
Requires: perl-WebService-YouTube >= 0.04
Requires: perl-XMLRPC-Lite
Requires: perl-XML-Atom >= 0.23
Requires: perl-XML-FOAF >= 0.03
Requires: perl-XML-Feed >= 0.12
Requires: perl-XML-LibXML >= 1.62
Requires: perl-XML-Liberal >= 0.16
Requires: perl-XML-RSS-LibXML >= 0.3002
Requires: perl-XML-RSS-Liberal >= 0.01
Requires: perl-XML-XPath >= 1.13
Requires: perl-YAML >= 0.58
Requires: perl-YAML-Syck >= 0.67
Requires: perl-libwww-perl
Requires: spamassassin
Requires: subversion-perl >= 1.3.2

%description
Plagger is a pluggable RSS/Atom feed aggregator and remixer platform.

%prep
%setup -q -n %{srcname} -a 1
%patch0 -p0
pushd %{mime_lite_srcname}
%patch1 -p1
popd

rm -f deps/Filter-Lou.yaml
rm -f lib/Plagger/Plugin/Filter/Lou.pm

rm -f deps/Notify-Lingr.yaml
rm -f lib/Plagger/Plugin/Notify/Lingr.pm
rm -f assets/plugins/CustomFeed-Script/lingr.pl
rm -rf assets/plugins/Notify-Lingr
rm -rf t/plugins/Notify-Lingr

rm -f lib/Plagger/Plugin/Filter/FetchEnclosure/Xango.pm

%build

perl Makefile.PL INSTALLDIRS=vendor
make

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}

# assets
%{__mkdir_p} %{buildroot}%{_datadir}/plagger
%{__cp} -a assets %{buildroot}%{_datadir}/plagger/

# use old MIME::Lite
pushd %{mime_lite_srcname}
perl Makefile.PL INSTALLDIRS=site INSTALLSITELIB=%{_datadir}/plagger/perl5
make install PERL_INSTALL_ROOT=%{buildroot}
rm -fr %{buildroot}%{perl_vendorlib}/MIME
rm -f %{buildroot}%{_mandir}/man3/MIME::Lite.3pm*
rm -f %{buildroot}%{_mandir}/man3/MIME::changes.3pm*
popd

find %{buildroot} -type f -name .packlist -exec rm -f {} \;

find %{buildroot}/usr -type f -print | \
	sed "s@^%{buildroot}@@g" | \
	grep -v perllocal.pod | \
	sed -e 's,\(.*/man/.*\),\1*,' | \
	grep -v "\.packlist" > Plagger-%{version}-filelist
if [ "$(cat Plagger-%{version}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

# custom requires script
%{__cat} <<EOF > %{__perl_requires}
#!/bin/bash
%{__cat} | %{__grep} -v %{buildroot}%{_docdir} | /usr/lib/rpm/perl.req $* \
        | %{__grep} -v 'perl(Win32::' | %{__grep} -v 'perl(Mac::'
EOF
%{__chmod} 700 %{__perl_requires}

# custom provides script
%{__cat} <<EOF > %{__perl_provides}
#!/bin/bash
%{__cat} | %{__grep} -v %{buildroot}%{_docdir} | /usr/lib/rpm/perl.prov $* \
        | %{__grep} -v 'perl(MIME::Lite'
EOF
%{__chmod} 700 %{__perl_provides}

%clean 
%{__rm} -rf %{buildroot}
%{__rm} -f %{__perl_requires}
%{__rm} -f %{__perl_provides}

%files -f Plagger-%{version}-filelist
%defattr(-,root,root)
%doc Changes AUTHORS examples

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-52m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-51m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-50m)
- rebuild against perl-5.18.1

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-49m)
- rebuild with perl-SOAP-Lite-1.000

* Tue May 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-48m)
- remove .packlist entirely

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-47m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-46m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-45m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-44m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-43m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-42m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-41m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-40m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.17-39m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.17-38m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-37m)
- rebuild against perl-5.12.2

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-36m)
- update to git snapshot (miyagawa/master)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.17-35m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-34m)
- update to git snapshot (miyagawa/master)

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-33m)
- rebuild against perl-5.12.1

* Wed May  5 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-32m)
- update to git snapshot (miyagawa/master)

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-31m)
- rebuild against perl-5.12.0

* Wed Nov 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-30m)
- update to git snapshot (miyagawa/master)
- include MIME-Lite-3.01_05 since newer MIME::Lite breaks HTML layout

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-29m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-28m)
- fix __perl_requires
- remove Lingr stuff since Lingr has been closed
- remove Xango since perl-Xango was obsolete

* Sun Nov  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.17-27m)
- comment out BuildRequires: perl-Cache-Cache >=1.05
-- build fix only

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.17-26m)
- rebuild against perl-5.10.1

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-25m)
- update to git snapshot

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-24m)
- update to git snapshot

* Thu Mar 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-23m)
- remove BuildRequires: perl-Xango >= 1.08

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-22m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-21m)
- update to revision 2059

* Sun Dec 28 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-20m)
- update to revision 2057

* Thu Nov 27 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-19m)
- update to revision 2053 (more support XML-Feed-0.3 or later)

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-18m)
- update to revision 2052 (support XML-Feed-0.3)

* Wed Oct 29 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-17m)
- update to revision 2046

* Mon Sep 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-16m)
- update to revision 2044

* Sat Jul 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-15m)
- update to revision 2040

* Tue Jun 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-14m)
- update to revision 2038

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.17-13m)
- rebuild against gcc43

* Fri Mar 28 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-12m)
- update to revision 2027

* Thu Jan 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-11m)
- update to revision 1998
- add BuildRequires: perl-WWW-HatenaDiary

* Sun Dec  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-10m)
- update to revision 1994
-- Plugin::Publish::Gmail: support higher version of MIME::Lite
-- ref. <http://www.karashi.org/~poppen/d/20070921.html>

* Mon Nov 26 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-9m)
- update to revision 1993

* Fri Nov  2 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.7.17-8m)
- require perl-XML-RSS-LibXML >= 0.3002

* Sun Oct 28 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-7m)
- update to revision 1986

* Fri Oct  5 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-6m)
- update to revision 1965, but disable Filter::Lou
- add BuildRequires: perl-WWW-Mixi-Scraper >= 0.07
      BuildRequires: perl-HTTP-Async >= 0.07
      BuildRequires: perl-Gungho
      BuildRequires: perl-WebService-BuzzurlAPI
      BuildRequires: perl-Net-Google-Calendar
      BuildRequires: perl-Net-Twitter
      BuildRequires: perl-File-Mork

* Tue Aug 14 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-5m)
- set BuildArch: noarch
- remove a backup file of patch0

* Sun Aug 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-4m)
- add Patch0: plagger-0.7.17-mixi-login.patch

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.17-3m)
- use vendor

* Thu Jan  4 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.7.17-2m)
- add BuildRequires: perl-XML-Feed >= 0.12

* Wed Dec  6 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-1m)
- update to 0.7.17
  add BuildRequires: perl-HTML-Tidy >= 1.07-0.1.1m

* Tue Nov 28 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.16-1m)
- update to 0.7.16
  add BuildRequires: perl-DateTime-Format-Japanese >= 0.03
      BuildRequires: perl-WebService-YouTube >= 0.04
      BuildRequires: perl-Lingua-JA-Summarize-Extract >= 0.01
  update BuildRequires: perl-DateTime-Locale >= 0.32
         BuildRequires: perl-WWW-Mixi >= 0.48
         BuildRequires: perl-XML-LibXML >= 1.62

* Thu Nov  2 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.15-1m)
- update to 0.7.15
  add BuildRequires: perl-Encode-Detect >= 1.00
  update BuildRequires: perl-DateTime-TimeZone >= 0.54
  delete Patch1: plagger-0.7.14-mixi-login.patch

* Tue Oct 24 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.14-1m)
- update to 0.7.14
  add Patch1: plagger-0.7.14-mixi-login.patch
  add BuildRequires: perl-HTML-Format >= 2.04
      BuildRequires: perl-DateTime-TimeZone >= 0.52
      BuildRequires: perl-IP-Country >= 2.21
      BuildRequires: perl-DateTime-Format-ICal >= 0.08
      BuildRequires: perl-Lingua-EN-Summarize >= 0.2
      BuildRequires: perl-Text-Original >= 1.4

* Wed Oct 11 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.13-1m)
- update to 0.7.13
  add BuildRequires: perl-Data-ICal >= 0.08

* Sat Sep 30 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.12-2m)
- add assets

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.12-1m)
- spec file was autogenerated
