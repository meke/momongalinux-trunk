%global momorel 2

Summary: Network monitoring tools including ping
Name: iputils
Version: 20121221
Release: %{momorel}m%{?dist}
License: BSD
URL: http://www.skbuff.net/iputils
Group: System Environment/Daemons

Source0: http://www.skbuff.net/iputils/%{name}-s%{version}.tar.bz2
NoSource: 0
Source1: ifenslave.tar.gz
Source3: rdisc.initd
Source4: rdisc.service
Source5: rdisc.sysconfig
Source6: ninfod.service

Patch0: iputils-20020927-rh.patch
Patch1: iputils-ifenslave.patch
Patch2: iputils-20121221-floodlocale.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: docbook-utils perl-SGMLSpm
BuildRequires: glibc-kernheaders >= 2.4-8.19
BuildRequires: libsysfs-devel
BuildRequires: libidn-devel
Requires: chkconfig
Requires: initscripts
Provides: /bin/ping
Provides: /bin/ping6
Provides: /sbin/arping
Provides: /sbin/rdisc

%description
The iputils package contains basic utilities for monitoring a network,
including ping. The ping command sends a series of ICMP protocol
ECHO_REQUEST packets to a specified network host to discover whether
the target machine is alive and receiving network traffic.

%package sysvinit
Group: System Environment/Daemons
Summary: SysV initscript for rdisc daemon
Requires: %{name} = %{version}-%{release}
Requires(preun): chkconfig
Requires(postun): chkconfig

%description sysvinit
The iputils-sysvinit contains SysV initscritps support.

%package ninfod
Group: System Environment/Daemons
Summary: Node Information Query Daemon
Requires: %{name} = %{version}-%{release}
Provides: %{_sbindir}/ninfod

%description ninfod
Node Information Query (RFC4620) daemon. Responds to IPv6 Node Information
Queries.

%prep
%setup -q -a 1 -n %{name}-s%{version}

%patch0 -p1 -b .rh
%patch1 -p1 -b .addr
%patch2 -p1 -b .floc

%build
%ifarch s390 s390x
  export CFLAGS="-fPIE"
%else
  export CFLAGS="-fpie"
%endif
export LDFLAGS="-pie -Wl,-z,relro,-z,now"

make %{?_smp_mflags} arping clockdiff ping ping6 rdisc tracepath tracepath6 \
                     ninfod
gcc -Wall $RPM_OPT_FLAGS ifenslave.c -o ifenslave
make -C doc man


%install
rm -rf --preserve-root %{buildroot}

mkdir -p ${RPM_BUILD_ROOT}/usr/bin ${RPM_BUILD_ROOT}/usr/sbin
install -c clockdiff            ${RPM_BUILD_ROOT}%{_sbindir}/
install -cp arping              ${RPM_BUILD_ROOT}%{_sbindir}/
install -cp ping                ${RPM_BUILD_ROOT}%{_bindir}/
install -cp ifenslave           ${RPM_BUILD_ROOT}%{_sbindir}/
install -cp rdisc               ${RPM_BUILD_ROOT}%{_sbindir}/
install -cp ping6               ${RPM_BUILD_ROOT}%{_bindir}/
install -cp tracepath           ${RPM_BUILD_ROOT}%{_bindir}/
install -cp tracepath6          ${RPM_BUILD_ROOT}%{_bindir}/
install -cp ninfod/ninfod	${RPM_BUILD_ROOT}%{_sbindir}/

ln -sf /bin/ping6 ${RPM_BUILD_ROOT}/%{_sbindir}/
ln -sf /bin/tracepath ${RPM_BUILD_ROOT}/%{_sbindir}/
ln -sf /bin/tracepath6 ${RPM_BUILD_ROOT}/%{_sbindir}/

mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man8
install -cp doc/clockdiff.8	${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/arping.8	${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/ping.8		${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/rdisc.8		${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/tracepath.8	${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp ifenslave.8		${RPM_BUILD_ROOT}%{_mandir}/man8/
ln -s ping.8.gz ${RPM_BUILD_ROOT}%{_mandir}/man8/ping6.8.gz
ln -s tracepath.8.gz ${RPM_BUILD_ROOT}%{_mandir}/man8/tracepath6.8.gz

install -dp ${RPM_BUILD_ROOT}%{_sysconfdir}/init.d
install -dp ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/
install -dp ${RPM_BUILD_ROOT}/usr/lib/systemd/system/
install -m 755 -p %SOURCE3 ${RPM_BUILD_ROOT}%{_sysconfdir}/init.d/rdisc
install -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/rdisc
install -m 644 %SOURCE4 ${RPM_BUILD_ROOT}/usr/lib/systemd/system

iconv -f ISO88591 -t UTF8 RELNOTES -o RELNOTES.tmp
touch -r RELNOTES RELNOTES.tmp
mv -f RELNOTES.tmp RELNOTES

mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man8
install -cp doc/clockdiff.8     ${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/arping.8        ${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/ping.8          ${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/rdisc.8         ${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/tracepath.8     ${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp doc/ninfod.8        ${RPM_BUILD_ROOT}%{_mandir}/man8/
install -cp ifenslave.8         ${RPM_BUILD_ROOT}%{_mandir}/man8/
ln -s ping.8.bz2 ${RPM_BUILD_ROOT}%{_mandir}/man8/ping6.8.bz2
ln -s tracepath.8.bz2 ${RPM_BUILD_ROOT}%{_mandir}/man8/tracepath6.8.bz2

install -dp ${RPM_BUILD_ROOT}%{_sysconfdir}/init.d
install -m 755 -p %SOURCE3 ${RPM_BUILD_ROOT}%{_sysconfdir}/init.d/rdisc
install -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/rdisc
install -m 644 %SOURCE4 ${RPM_BUILD_ROOT}/%{_unitdir}
install -m 644 %SOURCE6 ${RPM_BUILD_ROOT}/%{_unitdir}

iconv -f ISO88591 -t UTF8 RELNOTES -o RELNOTES.tmp
touch -r RELNOTES RELNOTES.tmp
mv -f RELNOTES.tmp RELNOTES

%post
%systemd_post rdisc.service

%preun
%systemd_preun rdisc.service

%postun
%systemd_postun_with_restart rdisc.service

%post ninfod
%systemd_post ninfod.service

%preun ninfod
%systemd_preun ninfod.service

%postun ninfod
%systemd_postun_with_restart ninfod.service

%triggerun --  %{name} < 20101006-2
        /sbin/chkconfig --del rdisc >/dev/null 2>&1 || :
        /bin/systemctl try-restart rdisc.service >/dev/null 2>&1 || :

%triggerpostun -n %{name}-sysvinit -- %{name} < 20101006-2
        /sbin/chkconfig --add rdisc >/dev/null 2>&1 || :

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc RELNOTES README.bonding
%{_unitdir}/rdisc.service
%attr(0755,root,root) %caps(cap_net_raw=ep) %{_sbindir}/clockdiff
%attr(0755,root,root) %caps(cap_net_raw=ep) %{_sbindir}/arping
%attr(0755,root,root) %caps(cap_net_raw=ep cap_net_admin=ep) %{_bindir}/ping
%{_sbindir}/ifenslave
%{_sbindir}/rdisc
%attr(0755,root,root) %caps(cap_net_raw=ep cap_net_admin=ep) %{_bindir}/ping6
%{_bindir}/tracepath
%{_bindir}/tracepath6 
%{_sbindir}/arping
%{_sbindir}/ping6
%{_sbindir}/tracepath
%{_sbindir}/tracepath6
%attr(644,root,root) %{_mandir}/man8/clockdiff.8.*
%attr(644,root,root) %{_mandir}/man8/arping.8.*
%attr(644,root,root) %{_mandir}/man8/ping.8.*
%attr(644,root,root) %{_mandir}/man8/ping6.8.*
%attr(644,root,root) %{_mandir}/man8/rdisc.8.*
%attr(644,root,root) %{_mandir}/man8/tracepath.8.*
%attr(644,root,root) %{_mandir}/man8/tracepath6.8.*
%attr(644,root,root) %{_mandir}/man8/ifenslave.8.*
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/sysconfig/rdisc

%files sysvinit
%{_sysconfdir}/init.d/rdisc

%files ninfod
%attr(0755,root,root) %caps(cap_net_raw=ep) /usr/sbin/ninfod
%{_unitdir}/ninfod.service
%attr(644,root,root) %{_mandir}/man8/ninfod.8.*

%changelog
* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (20121221-2m)
- support UserMove env

* Sun Jan 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (20121221-1m)
- update 20121221

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20101006-2m)
- support systemd

* Thu Jun 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20101006-1m)
- update to 20101006

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20071127-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20071127-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20071127-5m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071127-4m)
- [SECURITY] CVE-2010-2529
- sync with Fedora 13 (20071127-12)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071127-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20071127-2m)
- rebuild against rpm-4.6

* Fri Jul 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20071127-1m)
- update to 20071127

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20070202-4m)
- rebuild against gcc43

* Sun Oct 21 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (20070202-3m)
- add open-max patch from fc-devel

* Mon Jul 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (20070202-2m)
- add Requires: chkconfig

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (20070202-1m)
- update 20070202

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (20020927-4m)
- remove traceroute6

* Sat Mar 18 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (20020927-3m)
- add Patch22: iputils-ipv6-hoplimit.patch from FC (BTS #149)

* Mon Feb 28 2005 TABUCHI Takaaki <tab@momonga-linux,org>
- (20020927-2m)
- update patches

* Mon Jan 10 2005 TABUCHI Takaaki <tab@momonga-linux,org>
- (20020927-1m)
- update to 020927
- comment out Patch0: iputils-20001007-rh7.patch

* Mon Jan 10 2005 Toru Hoshina <t@momonga-linux,org>
- (20020124-6m)
- glibc-fix is no longer needed.
- adjusted to new glibc headers.

* Sun Aug 22 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (20020124-5m)
  revised source url

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (20020124-4m)
- revised spec for enabling rpm 4.2.

* Fri Oct  3 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (20020124-3m)
- fix compilation error

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (20020124-2k)
- ver up.
- man...

* Thu Dec 27 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (20001110-8k)
- fix for kernel-2.4.17

* Tue Dec 11 2001 Toru Hoshina <t@kondara.org>
- (20001110-6k)
- arping! arping!

* Sun Apr 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (20001110-4k)
- add iputils.ipv6.nomake.patch

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (20001110-2k)
- update to 20001110
- errased IPv6 commands with %{_ipv6} macro

* Thu Nov  2 2000 Toru Hoshina <toru@df-usa.com>
- I'm wondering why socksddr_in6 structure is different in
  /usr/src/linux/include/linux/in6.h and /usr/include/netinet/in.h ???
- Anyway, I've patched ping6.c to avoid stupid undeclare error.
  Please fix it ASAP, because I don't think ping6 works :-P

* Thu Oct 26 2000 Toyokazu Watabe <toy2@kondara.org> 20001011-1k
- update for 001011 including security fix.
- remove patch file iputils-991024.manfix.

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org>
- Kodaraized.

* Tue Jul 25 2000 Jakub Jelinek <jakub@redhat.com>
- fix include-glibc/ to work with new glibc 2.2 resolver headers

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.
- update to ss000418.
- perform reverse DNS lookup only once for same input.

* Sun Mar  5 2000 Jeff Johnson <jbj@redhat.com>
- include README.ifenslave doco.
- "ping -i N" was broke for N >= 3 (#9929).
- update to ss000121:
-- clockdiff: preserve raw socket errno.
-- ping: change error exit code to 1 (used to be 92,93, ...)
-- ping,ping6: if -w specified, transmit until -c limit is reached.
-- ping,ping6: exit code non-zero if some packets not received within deadline.

* Tue Feb 22 2000 Jeff Johnson <jbj@redhat.com>
- man page corrections (#9690).

* Wed Feb  9 2000 Jeff Johnson <jbj@jbj.org>
- add ifenslave.

* Thu Feb  3 2000 Elliot Lee <sopwith@redhat.com>
- List /usr/sbin/rdisc in %files list.

* Thu Jan 27 2000 Jeff Johnson <jbj@redhat.com>
- add remaining binaries.
- casts to remove compilation warnings.
- terminate if -w deadline is reached exactly (#8724).

* Fri Dec 24 1999 Jeff Johnson <jbj@redhat.com>
- create (only ping for now, traceroute et al soon).
