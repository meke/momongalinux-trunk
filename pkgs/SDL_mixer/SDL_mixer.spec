%global momorel 1

Summary: Simple DirectMedia Layer - Sample Mixer Library
Name: SDL_mixer
Version: 1.2.12
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://www.libsdl.org/projects/SDL_mixer/
Group: System Environment/Libraries
Source0: http://www.libsdl.org/projects/%{name}/release/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: audiofile >= 0.2.1
BuildRequires: audiofile-devel >= 0.2.1
BuildRequires: SDL-devel >= 1.2.15
BuildRequires: libvorbis >= 1.0-2m
BuildRequires: libmikmod >= 3.2.0

%description
Due to popular demand, here is a simple multi-channel audio mixer.
It supports 4 channels of 16 bit stereo audio, plus a single channel
of music, mixed by the popular MikMod MOD, Timidity MIDI and SMPEG MP3
libraries.

%package devel
Summary: Libraries, includes and more to develop SDL applications.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Due to popular demand, here is a simple multi-channel audio mixer.
It supports 4 channels of 16 bit stereo audio, plus a single channel
of music, mixed by the popular MikMod MOD, Timidity MIDI and SMPEG MP3
libraries.

%prep
%setup -q

%build
%configure \
           --disable-dependency-tracking	\
	   --disable-static 			\
	   --enable-music-libmikmod
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall install-bin

find %{buildroot} -name '*.la' -exec rm -f {} ';'

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc CHANGES COPYING README
%{_bindir}/playmus
%{_bindir}/playwave
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/SDL/*

%changelog
* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.12-1m)
- update to 1.2.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.11-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.11-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.11-2m)
- use BuildRequires

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.11-1m)
- update to 1.2.11
- remove %%global __libtoolize :

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.8-3m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.8-2m)
- rebuild against libmikmod.so.3

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.7-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-5m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.7-4m)
- rebuild against libvorbis-1.2.0-1m

* Fri Mar 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.7-3m)
- add freepats-cfg.patch
- import SDL_mixer-1.2.7-timidity-crash.patch from cooker
 +* Sat Feb 24 2007 Giuseppe Ghib <ghibo@mandriva.com> 1.2.7-4mdv2007.0
 ++ Revision: 125375
 +  fix timidity crash (P3, from SuSE)
- add %%post and %%postun

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.7-2m)
- delete libtool library

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.6-1m)
- version up

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.5-4m)
- build against SDL-1.2.7-4m

* Sat May 22 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.5-3m)
- remove smpeg support

* Tue Sep  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.5-2m)
- rebuild against SDL-1.2.6
- don't use %%{version} in Version:
- use %%NoSOurce macro

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.5-1m)
  update to 1.2.5(rebuild against DirectFB 0.9.18)

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.4-1m)

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.1-4m)
- reubuild aginst SDL-1.2.4-3m

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.2.1-3m)
- added BuildPreReq: libvorbis-devel >= 1.0-2m

* Thu May  9 2002 Toru Hoshina <t@kondara.org>
- (1.2.1-2k)
- version up.

* Thu Jan 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.0-8k)
- automake autoconf 1.5

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.0-6k)
- nigirisugi

* Thu Aug 23 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-4k)
- rebuild against SDL 1.2.2.

* Sat Apr 14 2001 Shingo Akagaki <dora@kondara.org>
- version 1.2.0

* Tue Mar 27 2001 Toru Hoshina <toru@df-usa.com>
- (1.1.0-3k)

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (1.0.6-7k)
- rebuild against audiofile-0.2.0.

* Fri Dec  1 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.6-5k)
- added unresolved.resolve.patch

* Mon Oct 30 2000 Toru Hoshina <toru@df-usa.com>
- be kondarized. (1.0.10-1k)

* Wed Jan 19 2000 Sam Lantinga 
- converted to get package information from configure
* Sun Jan 16 2000 Hakan Tandogan <hakan@iconsult.com>
- initial spec file

