%global momorel 6

Name: pdsh
Version: 2.18
Release: %{momorel}m%{?dist}
Summary: Parallel remote shell program
License: GPLv2+
Url: http://sourceforge.net/projects/pdsh/
Group: System Environment/Base
Source0: http://dl.sourceforge.net/sourceforge/pdsh/pdsh-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: pdsh-rcmd

# Enabling and disabling pdsh options
#  defaults:
#  enabled:  readline, rsh, ssh, dshgroup, netgroup, debug
#  disabled: rms, mrsh, qshell, mqshell, xcpu, genders, nodeattr, machines,
#            nodeupdown, slurm
#
#  To build the various module subpackages, pass --with <pkg> on
#   the rpmbuild command line (if your rpm is a recent enough version)
#  
#  Similarly, to disable various pdsh options pass --without <pkg> on
#   the rpmbuild command line.
#
#  This specfile used to support passing the --with and --without through
#   the environment variables PDSH_WITH_OPTIONS and PDSH_WITHOUT_OPTIONS.
#   e.g. PDSH_WITH_OPTIONS="qshell genders" rpmbuild ....
#   Unfortunately, new rpm doesn't tolerate such nonsense, so it doesn't work anymore.

# Read: If neither macro exists, then add the default definition.
# These are default ENABLED.
%{!?_with_readline: %{!?_without_readline: %define _with_readline --with-readline}}
%{!?_with_rsh: %{!?_without_rsh: %define _with_rsh --with-rsh}}
%{!?_with_ssh: %{!?_without_ssh: %define _with_ssh --with-ssh}}
%{!?_with_dshgroups: %{!?_without_dshgroups: %define _with_dshgroups --with-dshgroups}}
%{!?_with_netgroup: %{!?_without_netgroup: %define _with_netgroup --with-netgroup}}
%{!?_with_debug: %{!?_without_debug: %define _with_debug --with-debug}}
# These are default DISABLED.
%{!?_with_rms: %{!?_without_rms: %define _without_rms --without-rms}}
%{!?_with_mrsh: %{!?_without_mrsh: %define _without_mrsh --without-mrsh}}
%{!?_with_qshell: %{!?_without_qshell: %define _without_qshell --without-qshell}}
%{!?_with_mqshell: %{!?_without_mqshell: %define _without_mqshell --without-mqshell}}
%{!?_with_xcpu: %{!?_without_xcpu: %define _without_xcpu --without-xcpu}}
%{!?_with_genders: %{!?_without_genders: %define _without_genders --without-genders}}
%{!?_with_nodeattr: %{!?_without_nodeattr: %define _without_nodeattr --without-nodeattr}}
%{!?_with_machines: %{!?_without_machines: %define _without_machines --without-machines}}
%{!?_with_nodeupdown: %{!?_without_nodeupdown: %define _without_rms --without-nodeupdown}}
%{!?_with_slurm: %{!?_without_slurm: %define _without_slurm --without-slurm}}

#
# If "--with debug" is set compile with --enable-debug
#   and try not to strip binaries.
#
# (See /usr/share/doc/rpm-*/conditionalbuilds)
#
%if %{?_with_debug:1}%{!?_with_debug:0}
  %define _enable_debug --enable-debug
%endif

# Macro controlled BuildRequires
%{?_with_qshell:BuildRequires: qsnetlibs}
%{?_with_mqshell:BuildRequires: qsnetlibs}
BuildRequires: readline-devel
%{?_with_nodeupdown:BuildRequires: whatsup}
%{?_with_genders:BuildRequires: genders > 1.0}

%description
Pdsh is a multithreaded remote shell client which executes commands
on multiple remote hosts in parallel.  Pdsh can use several different
remote shell services, including standard "rsh", Kerberos IV, and ssh.

%package qshd
Summary: Remote shell daemon for pdsh/qshell/Elan3
Group:   System Environment/Base
Requires(post):  xinetd

%description qshd
Remote shell service for running Quadrics Elan3 jobs under pdsh.
Sets up Elan capabilities and environment variables needed by Quadrics
MPICH executables.

%package mqshd
Summary: Remote shell daemon for pdsh/mqshell/Elan3
Group:   System Environment/Base
Requires(post):  xinetd

%description mqshd
Remote shell service for running Quadrics Elan3 jobs under pdsh with
mrsh authentication.  Sets up Elan capabilities and environment variables 
needed by Quadrics MPICH executables.

%package   rcmd-rsh
Summary:   Provides bsd rcmd capability to pdsh
Group:     System Environment/Base
Provides:  pdsh-rcmd
Requires:  %{name} = %{version}-%{release}

%description rcmd-rsh
Pdsh module for bsd rcmd functionality. Note: This module
requires that the pdsh binary be installed setuid root.

%package   rcmd-ssh
Summary:   Provides ssh rcmd capability to pdsh
Group:     System Environment/Base
Provides:  pdsh-rcmd
Requires:  openssh-clients
Requires:  %{name} = %{version}-%{release}

%description rcmd-ssh
Pdsh module for ssh rcmd functionality.

%package   rcmd-qshell
Summary:   Provides qshell rcmd capability to pdsh
Group:     System Environment/Base
Provides:  pdsh-rcmd
Conflicts: pdsh-rcmd-mqshell
Requires:  %{name} = %{version}-%{release}

%description rcmd-qshell
Pdsh module for running QsNet MPI jobs. Note: This module
requires that the pdsh binary be installed setuid root.

%package   rcmd-mrsh
Summary:   Provides mrsh rcmd capability to pdsh
Group:     System Environment/Base
Provides:  pdsh-rcmd
Requires:  %{name} = %{version}-%{release}

%description rcmd-mrsh
Pdsh module for mrsh rcmd functionality.

%package   rcmd-mqshell
Summary:   Provides mqshell rcmd capability to pdsh
Group:     System Environment/Base
Provides:  pdsh-rcmd
Conflicts: pdsh-rcmd-qshell
Requires:  %{name} = %{version}-%{release}

%description rcmd-mqshell
Pdsh module for mqshell rcmd functionality.

%package   rcmd-xcpu
Summary:   Provides xcpu rcmd capability to pdsh
Group:     System Environment/Base
Provides:  pdsh-xcpu
Requires:  %{name} = %{version}-%{release}

%description rcmd-xcpu
Pdsh module for xcpu rcmd functionality.

%package   mod-genders
Summary:   Provides libgenders support for pdsh
Group:     System Environment/Base
Requires:  genders >= 1.1
Conflicts: pdsh-mod-nodeattr
Conflicts: pdsh-mod-machines
Requires:  %{name} = %{version}-%{release}

%description mod-genders
Pdsh module for libgenders functionality.

%package   mod-nodeattr
Summary:   Provides genders support for pdsh using the nodeattr program
Group:     System Environment/Base
Requires:  genders 
Conflicts: pdsh-mod-genders
Conflicts: pdsh-mod-machines
Requires:  %{name} = %{version}-%{release}

%description mod-nodeattr
Pdsh module for genders functionality using the nodeattr program.

%package   mod-nodeupdown
Summary:   Provides libnodeupdown support for pdsh
Group:     System Environment/Base
Requires:  whatsup
Requires:  %{name} = %{version}-%{release}

%description mod-nodeupdown
Pdsh module providing -v functionality using libnodeupdown.

%package   mod-rms
Summary:   Provides RMS support for pdsh
Group:     System Environment/Base
Requires:  qsrmslibs
Requires:  %{name} = %{version}-%{release}

%description mod-rms
Pdsh module providing support for gathering the list of target nodes
from an allocated RMS resource.

%package   mod-machines
Summary:   Pdsh module for gathering list of target nodes from a machines file
Group:     System Environment/Base
Conflicts: pdsh-mod-genders
Conflicts: pdsh-mod-nodeattr
Requires:  %{name} = %{version}-%{release}

%description mod-machines
Pdsh module for gathering list of all target nodes from a machines file.

%package   mod-dshgroup
Summary:   Provides dsh-style group file support for pdsh
Group:     System Environment/Base
Requires:  %{name} = %{version}-%{release}

%description mod-dshgroup
Pdsh module providing dsh (Dancer's shell) style "group" file support.
Provides -g groupname and -X groupname options to pdsh.

%package   mod-netgroup
Summary:   Provides netgroup support for pdsh
Group:     System Environment/Base
Requires:  %{name} = %{version}-%{release}

%description mod-netgroup
Pdsh module providing support for targeting hosts based on netgroup.
Provides -g groupname and -X groupname options to pdsh.

%package   mod-slurm
Summary:   Provides support for running pdsh under SLURM allocations
Group:     System Environment/Base
Requires:  slurm
Requires:  %{name} = %{version}-%{release}

%description mod-slurm
Pdsh module providing support for gathering the list of target nodes
from an allocated SLURM job.

%prep
%setup -q

%build
%configure \
    %{?_enable_debug}       \
    %{?_with_rsh}           \
    %{?_without_rsh}        \
    %{?_with_ssh}           \
    %{?_without_ssh}        \
    %{?_with_qshell}        \
    %{?_without_qshell}     \
    %{?_with_readline}      \
    %{?_without_readline}   \
    %{?_with_machines}      \
    %{?_without_machines}   \
    %{?_with_genders}       \
    %{?_without_genders}    \
    %{?_with_rms}           \
    %{?_without_rms}        \
    %{?_with_nodeupdown}    \
    %{?_without_nodeupdown} \
    %{?_with_nodeattr}      \
    %{?_without_nodeattr}   \
    %{?_with_mrsh}          \
    %{?_without_mrsh}       \
    %{?_with_mqshell}       \
    %{?_without_mqshell}    \
    %{?_with_xcpu}	    \
    %{?_without_xcpu}       \
    %{?_with_slurm}         \
    %{?_without_slurm}      \
    %{?_with_dshgroups}     \
    %{?_without_dshgroups}  \
    %{?_with_netgroup}      \
    %{?_without_netgroup}   \
    --program-transform-name=""

# FIXME: build fails when trying to build with _smp_mflags if qsnet is enabled
# make %{_smp_mflags} CFLAGS="$RPM_OPT_FLAGS"
make CFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
DESTDIR="$RPM_BUILD_ROOT" make install transform='s,x,x,'
if [ -x $RPM_BUILD_ROOT/%{_sbindir}/in.qshd ]; then
   install -D -m644 etc/qshell.xinetd $RPM_BUILD_ROOT/%{_sysconfdir}/xinetd.d/qshell
fi
if [ -x $RPM_BUILD_ROOT/%{_sbindir}/in.mqshd ]; then
   install -D -m644 etc/mqshell.xinetd $RPM_BUILD_ROOT/%{_sysconfdir}/xinetd.d/mqshell
fi
# according to developer: .so's are modules not really libraries .a's and
# .la's don't need to be packaged.
rm $RPM_BUILD_ROOT/%{_libdir}/pdsh/*a

%clean
rm -rf "$RPM_BUILD_ROOT"

%files
%defattr(-,root,root,-)
%doc COPYING README ChangeLog NEWS DISCLAIMER README.KRB4 README.modules README.QsNet
%{_bindir}/pdsh
%{_bindir}/pdcp
%{_bindir}/dshbak
%{_bindir}/rpdcp
%dir %{_libdir}/pdsh
%{_libdir}/pdsh/execcmd.so
%{_mandir}/man1/*

%if %{?_with_rsh:1}%{!?_with_rsh:0}
%files rcmd-rsh
%defattr(-,root,root,-)
%{_libdir}/pdsh/xrcmd.*
%endif

%if %{?_with_ssh:1}%{!?_with_ssh:0}
%files rcmd-ssh
%defattr(-,root,root,-)
%{_libdir}/pdsh/sshcmd.*
%endif

%if %{?_with_qshell:1}%{!?_with_qshell:0}
%files rcmd-qshell
%defattr(-,root,root,-)
%{_libdir}/pdsh/qcmd.*
%endif

%if %{?_with_mrsh:1}%{!?_with_mrsh:0}
%files rcmd-mrsh
%defattr(-,root,root,-)
%{_libdir}/pdsh/mcmd.*
%endif

%if %{?_with_mqshell:1}%{!?_with_mqshell:0}
%files rcmd-mqshell
%defattr(-,root,root,-)
%{_libdir}/pdsh/mqcmd.*
%endif

%if %{?_with_xcpu:1}%{!?_with_xcpu:0}
%files rcmd-xcpu
%defattr(-,root,root,-)
%{_libdir}/pdsh/xcpucmd.*
%endif

%if %{?_with_genders:1}%{!?_with_genders:0}
%files mod-genders
%defattr(-,root,root,-)
%{_libdir}/pdsh/genders.*
%endif

%if %{?_with_nodeattr:1}%{!?_with_nodeattr:0}
%files mod-nodeattr
%defattr(-,root,root,-)
%{_libdir}/pdsh/nodeattr.*
%endif

%if %{?_with_nodeupdown:1}%{!?_with_nodeupdown:0}
%files mod-nodeupdown
%defattr(-,root,root)
%{_libdir}/pdsh/nodeupdown.*
%endif

%if %{?_with_rms:1}%{!?_with_rms:0}
%files mod-rms
%defattr(-,root,root,-)
%{_libdir}/pdsh/rms.*
%endif

%if %{?_with_machines:1}%{!?_with_machines:0}
%files mod-machines
%defattr(-,root,root,-)
%{_libdir}/pdsh/machines.*
%endif

%if %{?_with_dshgroups:1}%{!?_with_dshgroups:0}
%files mod-dshgroup
%defattr(-,root,root,-)
%{_libdir}/pdsh/dshgroup.*
%endif

%if %{?_with_netgroup:1}%{!?_with_netgroup:0}
%files mod-netgroup
%defattr(-,root,root,-)
%{_libdir}/pdsh/netgroup.*
%endif

%if %{?_with_slurm:1}%{!?_with_slurm:0}
%files mod-slurm
%defattr(-,root,root,-)
%{_libdir}/pdsh/slurm.*
%endif

%if %{?_with_qshell:1}%{!?_with_qshell:0}
%files qshd
%defattr(-,root,root,-)
%{_sbindir}/in.qshd
%{_sysconfdir}/xinetd.d/qshell

%post qshd
if ! grep "^qshell" /etc/services >/dev/null; then
  echo "qshell            523/tcp                  # pdsh/qshell/elan3" >>/etc/services
fi
%{_initscriptdir}/xinetd reload

%endif

%if %{?_with_mqshell:1}%{!?_with_mqshell:0}
%files mqshd
%defattr(-,root,root)
%{_sbindir}/in.mqshd
%{_sysconfdir}/xinetd.d/mqshell

%post mqshd
if ! grep "^mqshell" /etc/services >/dev/null; then
  echo "mqshell         21234/tcp                  # pdsh/mqshell/elan3" >>/etc/services
fi
%{_initscriptdir}/xinetd reload

%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18-4m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.18-3m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18-1m)
- update to 2.18

* Sun Feb  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.17-1m)
- import from Fedora to Momonga
- add %%configure --program-transform-name=""
- add %%makeinstall transform='s,x,x,'

* Wed Jan 14 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 2.17-1
- update to 2.17
- fix netgroup

* Thu Aug 28 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 2.16-1
- attempt to make this package suck... less.
- fix license tag
- update to 2.16
- fix compile against glibc 2.8 (ARG_MAX not defined)

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.11-6
- Autorebuild for GCC 4.3

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 2.11-5
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Sep 18 2006 Ben Woodard <woodard@redhat.com> 2.11-4
- bump version for fc6

* Thu Aug 24 2006 Ben Woodard <woodard@redhat.com> 2.11-3
- Remove whatsup requirement for dshgrp and netgroup
- Enable dshgroup and netgroup by default

* Mon Jul 31 2006 Ben Woodard <woodard@redhat.com> 2.11-2
- Hardcode readline-devel dependency.

* Mon Jul 31 2006 Ben Woodard <woodard@redhat.com> 2.11-1
- Track upstream version
- Added dependency on openssh-clients for rcmd-ssh

* Thu Mar 30 2006 Ben Woodard <woodard@redhat.com> 2.10-4
- upstream pointed out that they had added two new subpackages
  that I'd failed to include.

* Thu Mar 30 2006 Ben Woodard <woodard@redhat.com> 2.10-3
- added dist tag so that I can build on multiple versions

* Thu Mar 30 2006 Ben Woodard <woodard@redhat.com> 2.10-2
- new version 2.10-1 used by upstream
- remove release from source line to track upstream
- deleted patch which is no longer needed
- removed -n from setup line. No longer needed.
- hack to fix perms so no longer needed

* Mon Mar 13 2006 Ben Woodard <woodard@redhat.com> 2.8.1-7
- An optimization in pdsh depended on the .la files being there. Removed
  optimization.

* Mon Mar 6 2006 Ben Woodard <woodard@redhat.com> 2.8.1-6
- Add COPYING file to file list
- removed .la packages from sub packages.

* Fri Feb 23 2006 Ben Woodard <woodard@redhat.com> 2.8.1-5
- changed source location to point to main site not mirror.
- inserted version macro in source line

* Thu Feb 22 2006 Ben Woodard <woodard@redhat.com> 2.8.1-4
- changed perms of pdsh and pcp after install so that find-debuginfo.sh finds
  the files and they get stripped.
- removed change of attributes of pdsh and pcp in files section
- removed .a files from packages.

* Wed Feb 22 2006 Ben Woodard <woodard@redhat.com>
- add parameters to make
- replace etc with _sysconfdir in most places
- remove post section with unexplained removing of cached man pages.
- removed dots at end of all summaries.

* Wed Feb 16 2006 Ben Woodard <woodard@redhat.com> 2.8.1-3
- removed dot at end of summary
- removed unused/broken smp build
- changed to using initrddir macro
- changed depricated Prereq to Requires

* Thu Feb 9 2006 Ben Woodard <woodard@redhat.com> 2.8.1-2
- add in rpmlint fixes
- change buildroot

* Wed Feb 8 2006 Mark Grondona <mgrondona@llnl.gov> 2.8.1-1
- pdsh 2.8.1 critical bugfix release

* Wed Feb 1 2006 Ben Woodard <woodard@redhat.com> 2.8-2
- Modified spec file to fix some problems uncovered by rpmlint
