%global momorel 5

%define _bindir /bin

Summary: A GNU archiving program
Name: cpio
Version: 2.11
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Archiving
URL: http://www.gnu.org/software/cpio/
Source0: ftp://ftp.gnu.org/gnu/cpio/cpio-%{version}.tar.bz2
NoSource: 0
Source1: cpio.1
Patch1: cpio-2.9-rh.patch
Patch2: cpio-2.9-exitCode.patch
Patch3: cpio-2.9-dir_perm.patch
Patch4: cpio-2.9-dev_number.patch 
Patch5: cpio-2.9-sys_umask.patch
Patch6: cpio-2.9.90-defaultremoteshell.patch
Patch7: cpio-2.10-patternnamesigsegv.patch
Patch8: cpio-2.11-stdio.in.patch

Requires(post): info
Requires(preun): info
BuildRequires: texinfo, gettext, rmt
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
GNU cpio copies files into or out of a cpio or tar archive.  Archives
are files which contain a collection of other files plus information
about them, such as their file name, owner, timestamps, and access
permissions.  The archive can be another file on the disk, a magnetic
tape, or a pipe.  GNU cpio supports the following archive formats:  binary,
old ASCII, new ASCII, crc, HPUX binary, HPUX old ASCII, old tar and POSIX.1
tar.  By default, cpio creates binary format archives, so that they are
compatible with older cpio programs.  When it is extracting files from
archives, cpio automatically recognizes which kind of archive it is reading
and can read archives created on machines with a different byte-order.

Install cpio if you need a program to manage file archives.

%prep
%setup -q

%patch1 -p1 -b .rh
%patch2 -p1 -b .exitCode
%patch3 -p1 -b .dir_perm
%patch4 -p1 -b .dev_number
%patch5 -p1 -b .sys_umask
%patch6 -p1 -b .defaultremote
%patch7 -p1 -b .patternsegv
%patch8 -p1 -b .stdio.in

autoheader

%build
CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -pedantic -Wall" %configure --with-rmt="%{_sysconfdir}/rmt"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} INSTALL="install -p" install

rm -f %{buildroot}%{_libexecdir}/rmt
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_mandir}/man1/*.1*
install -c -p -m 0644 %{SOURCE1} %{buildroot}%{_mandir}/man1

%clean
rm -rf %{buildroot}

%check
rm -f %{buildroot}/test/testsuite
make check

%post
/sbin/install-info %{_infodir}/cpio.info %{_infodir}/dir

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/cpio.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS README THANKS TODO COPYING
%{_bindir}/cpio
%{_mandir}/man1/cpio.1*
%{_infodir}/cpio.info*
%{_datadir}/locale/*/*/cpio.mo

%changelog
* Sun Sep 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-5m)
- add patch from Fedora to fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11-2m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11-1m)
- update 2.11
- add check section

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10-3m)
- [SECURITY] CVE-2010-0624
- import a security patch from Fedora 13 (2.10-6)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10-1m)
- sync with Rawhide (2.10-1)
- remove new autoconf hacks

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-7m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9-6m)
- buildfix (new autoconf)
- add SOURCE1 extentions.m4
- http://www.nabble.com/-PATCH--avoid-bootstrap-failure-when-using-newer-autoconf-td21029804.html

* Thu May 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9-5m)
- import Fedora Patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9-3m)
- %%NoSource -> NoSource

* Tue Jan  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-2m)
- add -fgnu89-inline for gcc43

* Sat Nov 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9-1m)
- update to 2.9 (sync with Fedora)

* Sun Nov  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-3m)
- [SECURITY] CVE-2007-4476
- add Patch9: cpio-2.6-safer_name_suffix.patch

* Thu Jul 21 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.6-2m)
- [SECURITY] CAN-2005-1229, fix race condition 
- sync with FC cvs


* Mon Feb 21 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6-1m)
  update to 2.6(based fedora core dev.)

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.5-3m)
- revised docdir permission

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.5-2m)
- revised spec for enabling rpm 4.2.

* Mon Aug 04 2003 Hiroyuki KOGA <kuma@momonga-linux.org>
- (2.5-1m)
- update to 2.5

* Tue Apr 30 2002 Motonobu Ichimura <famao@kondara.org>
- (2.4.2-30k)
- add i18n patch from li18nux

* Mon Apr 29 2002 Kenta MURATA <muraken@kondara.org>
- (2.4.2-28k)
- "Requires: hoge" toka ha yame youyo

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.4.2-26k)
- Source0 URL was fixed

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Sat Dec 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.4.2-25k)
- modified about infodir with %{_infodir} macros

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Tue Aug 22 2000 Kenichi Matsubara <m@kondara.org>
- Change PATH (FHS)

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.4.2-16).

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Wed Feb  9 2000 Jeff Johnson <jbj@redhat.com>
- missing defattr.

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- add manpages-ja-20000115

* Fri Dec 17 1999 Jeff Johnson <jbj@redhat.com>
- revert the stdout patch (#3358), restoring original GNU cpio behavior
  (#6376, #7538), the patch was dumb.

* Wed Nov 24 1999 Tenkou N. Hattori <tnh@kondara.org>
- add manpages-ja

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Nov 3 1999 Norihito Ohmori <nono@kondara.org>
- /usr/share/man -> /uar/man

* Sat Oct 16 1999 Norihito Ohmori <nono@kondara.org>
- add jman pages

* Tue Aug 31 1999 Jeff Johnson <jbj@redhat.com>
- fix infinite loop unpacking empty files with hard links (#4208).
- stdout should contain progress information (#3358).

* Sun Mar 21 1999 Crstian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 12)

* Sat Dec  5 1998 Jeff Johnson <jbj@redhat.com>
- longlong dev wrong with "-o -H odc" headers (formerly "-oc").

* Thu Dec 03 1998 Cristian Gafton <gafton@redhat.com>
- patch to compile on glibc 2.1, where strdup is a macro

* Tue Jul 14 1998 Jeff Johnson <jbj@redhat.com>
- Fiddle bindir/libexecdir to get RH install correct.
- Don't include /sbin/rmt -- use the rmt from dump package.
- Don't include /bin/mt -- use the mt from mt-st package.
- Add prereq's

* Tue Jun 30 1998 Jeff Johnson <jbj@redhat.com>
- fix '-c' to duplicate svr4 behavior (problem #438)
- install support programs & info pages

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Oct 17 1997 Donnie Barnes <djb@redhat.com>
- added BuildRoot
- removed "(used by RPM)" comment in Summary

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc
- no longer statically linked as RPM doesn't use cpio for unpacking packages
