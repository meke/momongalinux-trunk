%global         momorel 1

Summary:        ZFS ported to Linux FUSE
Name:           zfs-fuse
Version:        0.7.0
Release:        %{momorel}m%{?dist}
Group:          System Environment/Base
License:        "CDDL"
URL:            http://zfs-fuse.net/
Source0:        http://zfs-fuse.net/releases/%{version}/%{name}-%{version}.tar.bz2
#NoSource:       0
Source1:        zfs-fuse.init
Source2:        zfs-fuse.scrub
Source3:        zfs-fuse.sysconfig

Patch20:        zfs-fuse-0.6.9-glibc214.patch

BuildRequires:  fuse-devel libaio-devel scons zlib-devel
BuildRequires:  glibc-devel >= 2.14
Requires:       fuse >= 2.7.4-1
Requires(post): chkconfig
Requires(preun):chkconfig initscripts
Requires(postun): initscripts
# (2008-10-30)(uwe@kubosch.no) zfs-fuse doesn't have PPC and PPC64
#                              implementations for atomic instructions
# karsten@redhat.com:          neither does it have an implementation for s390(x)
ExcludeArch:    ppc64
ExcludeArch:    ppc
ExcludeArch:    s390
ExcludeArch:    s390x
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
ZFS is an advanced modern general-purpose filesystem from Sun
Microsystems, originally designed for Solaris/OpenSolaris.

This project is a port of ZFS to the FUSE framework for the Linux
operating system.

Project home page is at http://zfs-fuse.net/

%prep
%setup -q
%patch20 -p1 -b .glibc214

f=LICENSE
%{__mv} $f $f.iso88591
iconv -o $f -f iso88591 -t utf8 $f.iso88591
%{__rm} -f $f.iso88591

%build
pushd src
%{__sed} -i -e 's/-pipe -Wall -Werror/%{optflags}/' SConstruct
scons

%install
%{__rm} -rf %{buildroot}
pushd src

mkdir -p %{buildroot}%{_sysconfdir}/%{name}
mkdir -p %{buildroot}%{_mandir}/man8

scons install install_dir=%{buildroot}%{_bindir} man_dir=%{buildroot}%{_mandir}/man8 \
cfg_dir=%{buildroot}%{_sysconfdir}/%{name}

%{__install} -Dp -m 0755 %{SOURCE1} %{buildroot}%{_initscriptdir}/%{name}
%{__install} -Dp -m 0755 %{SOURCE2} %{buildroot}%{_sysconfdir}/cron.weekly/98-%{name}-scrub
%{__install} -Dp -m 0755 %{SOURCE3} %{buildroot}%{_sysconfdir}/sysconfig/%{name}

# conflicts with xfs-0.4.9-1m
pushd %{buildroot}%{_mandir}/man8
mv zdb.8 zdb-%{name}.8
mv zfs.8 zfs-%{name}.8
mv zpool.8 zpool-%{name}.8
popd

# zstreamdump is provided by zfs
find %{buildroot} -name 'zstreamdump*' -exec rm -f {} ';' 

%clean
%{__rm} -rf %{buildroot}

%post
if [ $1 = 1 ] ; then
    /sbin/chkconfig --add %{name}
fi

%preun
if [ $1 = 0 ] ; then
    /sbin/service %{name} stop >/dev/null 2>&1 || :
    /sbin/chkconfig --del %{name}
fi

%postun
if [ $1 -ge 1 ] ; then
    /sbin/service %{name} condrestart >/dev/null 2>&1 || :
    rm -rf /var/run/zfs
    rm -rf /var/lock/zfs
fi

%files
%defattr(-, root, root, -)
%doc BUGS CHANGES contrib HACKING LICENSE README 
%doc README.NFS STATUS TESTING TODO
%{_bindir}/zdb
%{_bindir}/zfs
%{_bindir}/zfs-fuse
%{_bindir}/zpool
%{_bindir}/ztest
%{_initscriptdir}/%{name}
%{_sysconfdir}/cron.weekly/98-%{name}-scrub
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%dir %{_sysconfdir}/%{name}
%{_sysconfdir}/%{name}/*
%{_mandir}/man8/*

%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0
- remove zstreamdump binary and man8 as zfs package provides them

* Sun Jun 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.9-7m)
- enable to build with glibc-2.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.9-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.9-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.9-4m)
- full rebuild for mo7 release

* Sat Jul 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.9-3m)
- move man files conflicting with zfs-0.4.9 from *-fuse.8 to *-zfs-fuse.8

* Sat Jul 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.9-2m)
- move man files conflicting with zfs-0.4.9 from *.8 to *-fuse.8 for the moment

* Sun Jun  6 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.9-1m)
- update to 0.6.9
- drop included Patch11:        zfs-fuse-0.5.0-glibc212.patch
- add Patch12 for adapt rpm man
- define man_dir and cfg_dir
- add %%{_bindir}/zstreamdump into %%files

* Sun Apr 25 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.0-2m)
- fix syntax error of zfs-fuse.scrub

* Fri Apr 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-6m)
- apply glibc212 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Mar 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-4m)
- do not start by default

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-3m)
- add Source2: zfs-fuse.sysconfig

* Tue Dec 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-2m)
- add Patch0: zfs-fuse-trunk-008c531499cd.patch

* Tue Dec 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-1m)
- import from Fedora to Momonga
- add Patch10: zfs-fuse-strops.h.patch

* Tue Nov 11 2008 Uwe Kubosh <uwe@kubosch.no> - 0.5.0-3.20081009
- Rebuild after import into Fedora build system.

* Thu Oct 09 2008 Uwe Kubosh <uwe@kubosch.no> - 0.5.0-2.20081009
- Updated to upstream trunk of 2008-10-09
- Adds changes to make zfs-fuse build out-of-the-box on Fedora 9, and removes the need for patches.

* Sat Oct  4 2008 Terje Rosten <terje.rosten@ntnu.no> - 0.5.0-1 
- initial build
