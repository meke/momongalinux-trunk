%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define debug_package %{nil}

Name: bzr-rewrite
Summary: Revision rewrite support for Bazaar
Version: 0.6.3
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Development/Tools
URL: https://launchpad.net/bzr-rewrite
Source0: http://launchpad.net/bzr-rewrite/trunk/%{version}/+download/bzr-rewrite-%{version}.tar.gz
NoSource: 0
BuildRequires: bzr python-devel >= 2.7
Requires: bzr
Obsoletes: bzr-rebase < 0.6.0
Provides: bzr-rebase

%description
The Bazaar rebase plugin provides support for rebasing branches much
like git-rebase does in git. It adds the command 'rebase' to Bazaar.

%prep
%setup -q

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --root %{buildroot} --skip-build

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
%{python_sitelib}/bzrlib/plugins/rewrite
%{python_sitelib}/bzr_rewrite-*.egg-info

%changelog
* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.3-1m)
- update 0.6.3

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-1m)
- update 0.6.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-2m)
- rebuild for new GCC 4.5

* Mon Nov  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-1m)
- update 0.6.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-1m)
- renamed
- update to 0.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.3-1m)
- update to 0.5.3

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Sat May 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.0-1m)
- update 0.5.0

* Sun Mar  8 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.2-2m)
- build fix in x86_64

* Thu Feb 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-1m)
- initial packaging
