%global momorel 1
%global ertsver 5.8.4


Summary: programming language Erlang
Name: erlang
Version: R14B04
Release: %{momorel}m%{?dist}
License: "ERLANG PUBLIC LICENSE"
Group: Development/Languages
URL: http://www.erlang.org/
Source0: http://www.erlang.org/download/otp_src_%{version}.tar.gz 
NoSource: 0

# patches from Fedora
Patch10: otp-links.patch
Patch11: otp-install.patch
Patch12: otp_src_R12B-5-rpath.patch
Patch13: otp_src_R12B-5-sslrpath.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: flex
BuildRequires: java-1.6.0-openjdk-devel
BuildRequires: m4
BuildRequires: ncurses-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: tcl-devel
BuildRequires: tk-devel
BuildRequires: unixODBC-devel >= 2.2.14

%description
Erlang is a programming language designed at the Ericsson Computer
Science Laboratory. Open-source Erlang is being released to help
encourage the spread of Erlang outside Ericsson.

%prep
%setup -q -n otp_src_%{version}

#%%patch10 -p1 -b .links
#%%patch11 -p1 -b .install
#%%patch12 -p1 -b .rpath
#%%patch13 -p1 -b .sslrpath


%build
#export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
#export CXXFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
#export JAVAC=%{javac}
./configure \
    --prefix=%{_prefix} \
    --libdir=%{_libdir} \
    --without-perfctr \
    --enable-smp-support \
    --enable-threads \
    --enable-kernel-poll \
    --enable-hipe \
    --enable-megaco-flex-scanner-lineno \
    --enable-megaco-reentrant-flex-scanner \
    --enable-dynamic-ssl-lib \
    --enable-shared-zlib
%make

%install
rm -rf --preserve-root %{buildroot}
make install INSTALL_PREFIX=%{buildroot}

# change path "%%{buildroot}" to ""
sed "s,%{buildroot},," -i %{buildroot}/%{_libdir}/erlang/bin/erl
sed "s,%{buildroot},," -i %{buildroot}/%{_libdir}/erlang/bin/start
sed "s,%{buildroot},," -i %{buildroot}/%{_libdir}/erlang/erts-*/bin/erl
sed "s,%{buildroot},," -i %{buildroot}/%{_libdir}/erlang/erts-*/bin/start
sed "s,%{buildroot},," -i %{buildroot}/%{_libdir}/erlang/releases/RELEASES

# change absolute symlink to related
pushd %{buildroot}/%{_bindir}
for file in cerl ct_run dialyzer erl erlc escript typer
do
    ln -sf ../%{_lib}/erlang/bin/$file $file
done
popd

pushd %{buildroot}/%{_libdir}/erlang/bin
ln -sf ../erts-%{ertsver}/bin/epmd epmd
popd

# made objects non-executable to avoid "No build ID note" errors
find %{buildroot} -name "*.o" -perm +0111 -exec chmod a-x {} \;

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS EPLICENCE README.md
%{_bindir}/*
%{_libdir}/erlang

%changelog
* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (R14B04-1m)
- update to R14B04

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (R14B03-1m)
- update to R14B03

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (R13B02-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (R13B02-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (R13B02-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R13B02-3m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (R13B02-2m)
- delete __libtoolize hack

* Sun Nov 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (R13B01-1m)
- update to R13B02-1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R13B-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R13B-4m)
- set ertsver

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R13B-3m)
- rebuild against unixODBC-2.2.14

* Sun May 10 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (R13B-2m)
- add JAVAC=%%{javac}

* Sat May  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R13B-1m)
- update to R13B
- BuildPreReq: java-1.6.0-openjdk-devel

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R12B-8m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R12B-7m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R12B-6m)
- update to R12B-5
-- drop Patch0, merged upstream
-- update Patch12,13 for fuzz=0
-- drop Patch14, not needed

* Sun Jan  4 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (R12B1-5m)
- update patch0 for glibc-2.9

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (R12B1-4m)
- rebuild against openssl-0.9.8h-1m

* Sun Apr 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (R12B1-3m)
- add patch0 (for glibc-2.8)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (R12B-2m)
- rebuild against gcc43

* Wed Mar  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (R12B-1m)
- update R12B-1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (R11B-10m)
- %%NoSource -> NoSource

* Mon Oct 22 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (R11B-9m)
- add glibc-2.7 patch

* Mon Sep 24 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (R11B-8m)
- revised spec for debuginfo again

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (R11B-7m)
- revised spec for debuginfo

* Sun Jul  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (R11B-6m)
- update to R11B-5

* Sat Jun 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (R11B-5m)
- change BPR from java-sun-j2se1.5-sdk to java-1.5.0-gcj-devel
- import 5 patches from Fedora

* Mon Jan  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (R11B-4m)
- second maintainence release R11B-2

* Mon Dec 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (R11B-3m)
- support glibc-2.5

* Mon Jul 10 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (R11b-2m)
- revise spec for x86_64
- BuildPreReq: java-sun-j2se1.5-sdk

* Sat Jul  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (R11b-1m)
- initial build
