%global momorel 16

Name:           perl-File-RsyncP
Version:        0.70
Release:        %{momorel}m%{?dist}
Summary:        Perl Rsync client
License:        "Distributable, see LICENSE"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/File-RsyncP/
Source0:        http://www.cpan.org/authors/id/C/CB/CBARRATT/File-RsyncP-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
File::RsyncP is a perl implementation of an Rsync client. It is compatible
with Rsync 2.5.5 - 2.6.3 (protocol versions 26-28). It can send or receive
files, either by running rsync on the remote machine, or connecting to an
rsyncd deamon on the remote machine.

%prep
%setup -q -n File-RsyncP-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
# parallel build does not work...
make #{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
make test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorarch}/File/RsyncP.pm
%{perl_vendorarch}/File/RsyncP/
%{perl_vendorarch}/auto/File/RsyncP/
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-16m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-15m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-14m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-13m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.70-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.70-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.70-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.68-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.68-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.68-2m)
- rebuild against gcc43

* Mon Jul 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68
- delete patch0

* Fri Jul  6 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.62-1m)
- import to Momonga

* Sun Sep 10 2006 Mike McGrath <imlinux@gmail.com> - 0.62-2
- Rebuild

* Fri Jul 21 2006 Mike McGrath <imlinux@gmail.com> - 0.62-2
- Fixed whitespace issue and removed SMP flags on make

* Thu Jul 20 2006 Mike McGrath <imlinux@gmail.com> - 0.62-1
- Updated to 0.62 and applied two known patches

* Thu Jul 20 2006 Mike McGrath <imlinux@gmail.com> - 0.52-1
- Initial Fedora Packaging
