%global momorel 10

Summary: Synaptics touchpad driver library
Name: libsynaptics
Version: 0.14.6c
Release: %{momorel}m%{?dist}
License: GPL
URL: http://qsynaptics.sourceforge.net/
Group: System Environment/Libraries
Source0: http://qsynaptics.sourceforge.net/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1: libsynaptics-0.14.6c-gcc43.patch
Patch2: libsynaptics-0.14.6c-gcc44.patch
BuildRequires: xorg-x11-proto-devel

%description
A small C++ library usable by the synaptics driver.
The version numbering will follow any driver releases, the appended letter
marks releases due to bug fixing.

%package devel
Summary: Header files and static libraries from libsynaptics
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on libsynaptics.

%prep
%setup -q

%patch1 -p1 -b .gcc43~
%patch2 -p1 -b .gcc44~

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING ChangeLog INSTALL README TODO
%{_libdir}/libsynaptics.a
%{_libdir}/libsynaptics.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/synaptics
%{_libdir}/libsynaptics.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.6c-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.6c-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.6c-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.6c-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.6c-6m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.6c-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.6c-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.6c-3m)
- %%NoSource -> NoSource

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.6c-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.6c-1m)
- initial package for Momonga Linux
