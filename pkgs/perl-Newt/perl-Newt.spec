%global momorel 24

Summary:	Perl bindings for the Newt library
Name:		perl-Newt
Version:	1.08
Release:	%{momorel}m%{?dist}
Group:		System Environment/Libraries
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL:		http://search.cpan.org/~amedina/Newt-1.08/
Source0:	http://www.cpan.org/authors/id/A/AM/AMEDINA/Newt-%{version}.tar.gz
NoSource:	0
Patch0:		newt-perl-1.08-debian.patch
Patch1:		newt-perl-1.08-typemap.patch
Patch2:		newt-perl-1.08-fix.patch
Patch3:		newt-perl-1.08-xs.patch
Patch4:		newt-perl-1.08-lang.patch
Patch5:		perl-Newt-bz385751.patch
BuildRequires:	newt-devel
BuildRequires:	perl >= 5.10.0
BuildRequires:	perl-devel
Requires:	%(eval `perl -V:version`; echo "perl(:MODULE_COMPAT_$version)")
License:	GPL or Artistic

Obsoletes:	newt-perl
Provides:	newt-perl

%description
This package provides Perl bindings for the Newt widget
library, which provides a color text mode user interface.

%prep
%setup -q -n Newt-%{version}
%patch0 -p1 -b .debian
%patch1 -p1 -b .valist
%patch2 -p1 -b .fix
%patch3 -p1 -b .exes
%patch4 -p1 -b .lang
%patch5 -p1 -b .bz385751
rm -rf newtlib

%build
perl Makefile.PL PREFIX=%{_prefix} INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS"
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} \( -name perllocal.pod -o -name .packlist \) -exec rm -v {} \;
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null ';'
chmod -R u+w %{buildroot}/*

%clean
rm -rf %{buildroot}

%files
%defattr (-,root,root)
%doc ChangeLog README
%{perl_vendorarch}/Newt.pm
%{perl_vendorarch}/auto/Newt
%{_mandir}/man3/Newt*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-24m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-23m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-22m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-21m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-20m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-19m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-18m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-17m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.08-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.08-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.08-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-5m)
- rebuild against rpm-4.6

* Mon May  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-4m)
- package name was renamed from newt-perl to perl-Newt

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.08-3m)
- rebuild against gcc43

* Wed Jun 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.08-2m)
- rebuild against perl-5.10.0

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.08-1m)
- import from fc to Momonga
- use %%{buildroot}

* Thu Mar  1 2007 Joe Orton <jorton@redhat.com> 1.08-14
- various cleanups (Jason Tibbs, #226196)
- require perl-devel

* Tue Feb 27 2007 Joe Orton <jorton@redhat.com> 1.08-13
- clean up URL, Source, BuildRoot, BuildRequires

* Thu Dec 14 2006 Joe Orton <jorton@redhat.com> 1.08-12
- fix test.pl (Charlie Brady, #181674)

* Thu Dec 14 2006 Joe Orton <jorton@redhat.com> 1.08-11
- fix directory ownership (#216610)

* Wed Nov 15 2006 Joe Orton <jorton@redhat.com> 1.08-10
- fix compiler warnings (#155977)

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.08-9.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.08-9.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.08-9.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Sep 27 2005 Petr Rockai <prockai@redhat.com> - 1.08-9
- rebuild against newt 0.52.0

* Fri Mar  4 2005 Joe Orton <jorton@redhat.com> 1.08-8
- rebuild

* Tue Aug 17 2004 Joe Orton <jorton@redhat.com> 1.08-7
- add perl MODULE_COMPAT requirement

* Mon Aug 16 2004 Joe Orton <jorton@redhat.com> 1.08-6
- rebuild

* Mon Sep  8 2003 Joe Orton <jorton@redhat.com> 1.08-5
- fix issue with non-English LANG setting (#67735)

* Tue Aug  5 2003 Joe Orton <jorton@redhat.com> 1.08-4
- rebuild

* Thu May  9 2002 Joe Orton <jorton@redhat.com> 1.08-3
- add newt requirement

* Wed Apr 03 2002 Gary Benson <gbenson@redhat.com> 1.08-2
- tweak perl dependency as suggested by cturner@redhat.com

* Wed Mar 20 2002 Gary Benson <gbenson@redhat.com>
- make like all the other perl modules we ship (bind to perl version,
  use perl dependency finding scripts, build filelist automatically).
- include documentation
- build against perl 5.6.1

* Thu Jan 10 2002 Joe Orton <jorton@redhat.com>
- Adapted for RHL

* Tue Sep 11 2001 Mark Cox <mjc@redhat.com>
- Change paths to new layout

* Mon Jun 11 2001 Joe Orton <jorton@redhat.com>
- Initial revision.
