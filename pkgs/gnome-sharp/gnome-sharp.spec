%global momorel 11

Summary: .NET language binding for the GTK+ toolkit
Name: gnome-sharp
Version: 2.24.2
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://www.mono-project.com/
Source0: ftp://ftp.gnome.org/pub/gnome/sources/%{name}/2.24/%{name}-%{version}.tar.bz2
NoSource: 0

# bootstrap gacutils
Patch0: gnome-sharp-2.24.1-gacutil.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.14.3
BuildRequires: mono-devel >= 2.8
BuildRequires: gtk-sharp2-devel >= 2.12.4
BuildRequires: libart_lgpl-devel >= 2.3.20
BuildRequires: gnome-vfs2-devel >= 2.24.0
BuildRequires: libgnomecanvas-devel >= 2.20.1.1
BuildRequires: libgnomeui-devel >= 2.24.0
BuildRequires: glib2-devel >= 2.18.1
BuildRequires: gtkhtml3-devel
BuildRequires: vte3-devel
Requires: mono-core
Requires: gtk2

%description
Gtk# is a .NET language binding for the GTK+ toolkit and assorted GNOME
libraries.  Gtk# is free software, licensed under the GNU LGPL.  The target
is the 2.6 platform.

%package libart_lpgl
Summary: %{name}-libart_lpgl
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description libart_lpgl
%{name}-libart_lpgl

%package GConf2
Summary: %{name}-GConf2
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description GConf2
%{name}-GConf2

%package gnome-vfs2
Summary: %{name}-gnome-vfs2
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description gnome-vfs2
%{name}-gnome-vfs2

%package devel
Summary: .NET language binding for the GTK+ toolkit (devel)
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: %{name}-libart_lpgl
Requires: %{name}-GConf2
Requires: %{name}-gnome-vfs2
Requires: gtk2-devel
Requires: mono-devel
Requires: gtk-sharp2-devel
Requires: libart_lgpl-devel
Requires: gnome-vfs-devel
Requires: libgnomecanvas-devel
Requires: libgnomeui-devel
Requires: libgnomeprintui-devel
Requires: glib-devel
Requires: gnome-panel-devel
Requires: librsvg2-devel
Requires: gtkhtml3-devel
Requires: vte3-devel

%description devel
gnome-sharp devel

%prep
%setup -q
%patch0 -p1 -b .gacutils

%build
autoreconf -vfi
%configure \
    --disable-static \
    --program-prefix=""
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING HACKING NEWS README
%{_bindir}/gconfsharp2-schemagen
%{_libdir}/libgnomesharpglue-2.so
%exclude %{_libdir}/*.la
%{_prefix}/lib/mono/gac/gnome-sharp
%{_prefix}/lib/mono/gtk-sharp-2.0/gnome-sharp.dll

%files libart_lpgl
%defattr(-,root,root)
%{_prefix}/lib/mono/gac/art-sharp
%{_prefix}/lib/mono/gac/policy.2.*.art-sharp
%{_prefix}/lib/mono/gtk-sharp-2.0/art-sharp.dll
%{_prefix}/lib/mono/gtk-sharp-2.0/policy.2.*.art-sharp.dll

%files GConf2
%defattr(-,root,root)
%{_prefix}/lib/mono/gac/gconf-sharp-peditors
%{_prefix}/lib/mono/gac/policy.2.*.gconf-sharp-peditors
%{_prefix}/lib/mono/gtk-sharp-2.0/gconf-sharp-peditors.dll
%{_prefix}/lib/mono/gtk-sharp-2.0/policy.2.*.gconf-sharp-peditors.dll
%{_prefix}/lib/mono/gac/gconf-sharp
%{_prefix}/lib/mono/gac/policy.2.*.gconf-sharp
%{_prefix}/lib/mono/gtk-sharp-2.0/gconf-sharp.dll
%{_prefix}/lib/mono/gtk-sharp-2.0/policy.2.*.gconf-sharp.dll
%{_prefix}/lib/gtk-sharp-2.0/gconfsharp-schemagen.exe

%files gnome-vfs2
%defattr(-,root,root)
%{_prefix}/lib/mono/gac/gnome-vfs-sharp
%{_prefix}/lib/mono/gac/policy.2.*.gnome-vfs-sharp
%{_prefix}/lib/mono/gtk-sharp-2.0/gnome-vfs-sharp.dll
%{_prefix}/lib/mono/gtk-sharp-2.0/policy.2.*.gnome-vfs-sharp.dll

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/art-sharp-2.0.pc
%{_libdir}/pkgconfig/gconf-sharp-2.0.pc
%{_libdir}/pkgconfig/gconf-sharp-peditors-2.0.pc
%{_libdir}/pkgconfig/gnome-sharp-2.0.pc
%{_libdir}/pkgconfig/gnome-vfs-sharp-2.0.pc

%{_datadir}/gapi-2.0/art-api.xml
%{_datadir}/gapi-2.0/gnome-api.xml
%{_datadir}/gapi-2.0/gnome-vfs-api.xml

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-11m)
- rebuild for mono-2.10.9

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-10m)
- revise Requires, again.

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-9m)
- revise Requires, again.

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-8m)
- fix BTS #436 issue

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-7m)
- revise BuildRequires/Requires for vte3

* Fri Jun 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-6m)
- fix BuildRequires/Requires

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-5m)
- rebuild for glib 2.33.2

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-4m)
- add Requires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.1-8m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.1-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.24.1-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-4m)
- split package

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-3m)
- modify patch0

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-2m)
- add patch0 for gacutils

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Apr 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20.0-2m)
- rebuild against gcc43

* Wed Mar  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri Feb 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update 2.16.1

* Wed Mar 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-4m)
- add patch0 for gtkhtml-3.14.0

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-3m)
- delete all patches (no needs)

* Wed Aug 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-2m)
- delete duplicate dir %%{_prefix}/lib/mono/gtk-sharp-2.0

* Tue Aug 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- initial build
