%global momorel 2
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define gst_ver 0.10.33
%define gst_plugins_base_ver 0.10.33

Name:           farstream
Version:        0.1.2
Release: %{momorel}m%{?dist}
Summary:        Libraries for videoconferencing
Group: System Environment/Libraries
License:        LGPLv2+
URL:            http://www.freedesktop.org/wiki/Software/Farstream
Source0:        http://freedesktop.org/software/%{name}/releases/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0:         farstream-prefer-vp8.patch

BuildRequires:  libnice-devel >= 0.1.0
BuildRequires:  gstreamer-devel >= %{gst_ver}
BuildRequires:  gstreamer-plugins-base-devel >= %{gst_plugins_base_ver}
BuildRequires:  gupnp-igd-devel >= 0.2.0
BuildRequires:  gobject-introspection-devel
BuildRequires:  python-devel
BuildRequires:  gstreamer-python-devel
BuildRequires:  pygobject2-devel

Requires:       gstreamer-plugins-good >= 0.10.29
Requires:       gstreamer-plugins-bad >= 0.10.23

## Obsolete farsight2 with Fedora 17.
Provides:       farsight2 = %{version}
Obsoletes:      farsight2 < 0.0.32


%description
%{name} is a collection of GStreamer modules and libraries for
videoconferencing.


%package        python
Summary:        Python binding for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

## Obsolete farsight2 with Fedora 17.
Provides:       farsight2-python = %{version}
Obsoletes:      farsight2-python < 0.0.32


%description    python
Python bindings for %{name}.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-python = %{version}-%{release}
Requires:       gstreamer-devel  >= %{gst_ver}
Requires:       gstreamer-plugins-base-devel >= %{gst_plugins_base_ver}
Requires:       pkgconfig

## Obsolete farsight2 with Fedora 17.
Provides:       farsight2-devel = %{version}
Obsoletes:      farsight2-devel < 0.0.32

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q
#%patch0 -p1 -b .vp8


%build
%configure                                                              \
  --with-package-name='Fedora Farstream package'                        \
  --with-package-origin='http://download.fedoraproject.org'             \
  --disable-static \
  --enable-introspection=no
%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"
find %{buildroot} -name '*.la' -exec rm -f {} ';'


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%doc COPYING NEWS AUTHORS
%{_libdir}/*.so.*
%dir %{_libdir}/%{name}-0.1
%{_libdir}/%{name}-0.1/libmulticast-transmitter.so
%{_libdir}/%{name}-0.1/libnice-transmitter.so
%{_libdir}/%{name}-0.1/librawudp-transmitter.so
%{_libdir}/%{name}-0.1/libshm-transmitter.so
%{_libdir}/gstreamer-0.10/libfsfunnel.so
%{_libdir}/gstreamer-0.10/libfsmsnconference.so
%{_libdir}/gstreamer-0.10/libfsrawconference.so
%{_libdir}/gstreamer-0.10/libfsrtcpfilter.so
%{_libdir}/gstreamer-0.10/libfsrtpconference.so
%{_libdir}/gstreamer-0.10/libfsvideoanyrate.so
#%{_libdir}/girepository-1.0/Farstream-0.1.typelib
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/0.1
%dir %{_datadir}/%{name}/0.1/fsrtpconference
%dir %{_datadir}/%{name}/0.1/fsrawconference
%{_datadir}/%{name}/0.1/fsrtpconference/default-codec-preferences
%{_datadir}/%{name}/0.1/fsrtpconference/default-element-properties
%{_datadir}/%{name}/0.1/fsrawconference/default-element-properties


%files python
%{python_sitearch}/farstream.so


%files devel
%{_libdir}/libfarstream-0.1.so
%{_libdir}/pkgconfig/%{name}-0.1.pc
%{_includedir}/%{name}-0.1/%{name}/
#%{_datadir}/gir-1.0/Farstream-0.1.gir
%{_datadir}/gtk-doc/html/%{name}-libs-0.10/
%{_datadir}/gtk-doc/html/%{name}-plugins-0.1/


%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.2-2m)
- rebuild against gupnp-igd-0.2.0

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-1m)
- import from fedora
-- temporary disable introspection

