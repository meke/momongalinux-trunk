#!/bin/sh

VERSION=$1
SOURCE0=$2
NAME=$3

tar -xzvf $SOURCE0
rm -rf $NAME/stardict-3.0.1-1.fc8.i386.rpm
tar -czvf $NAME-$VERSION-nobinary.tar.gz $NAME

