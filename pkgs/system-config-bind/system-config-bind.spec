%global momorel 5

Summary:        BIND DNS Configuration Tool
Name:           system-config-bind
Version:        4.0.15
Release:        %{momorel}m%{?dist}
URL:            http://fedorahosted.org/system-config-bind
License:        GPLv2
Group:          Applications/System
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python, gettext, make, intltool, desktop-file-utils
Requires:       pygtk2, gnome-python2, pygtk2-libglade, gnome-python2-canvas
Requires:       bind, bind-utils, usermode, xdg-utils
Requires(post): hicolor-icon-theme

%description
The system-config-bind package provides a graphical user interface (GUI) to
configure the Berkeley Internet Name Domain (BIND) Domain Name System (DNS)
server, "named", with a set of python modules.
Users new to BIND configuration can use this tool to quickly set up a working
DNS server.

%prep
%setup -q

%build
make 

%install
rm -rf %{buildroot}
for dir in ar as bg bn_IN bn bs ca cs cy da de el en_GB es et fa fi fr gu he hi \
	   hr hu hy id is it ja ka kn ko ku lo mk ml mr ms my nb nl nn or pa pl \
	   pt_BR pt ro ru si sk sl sq sr sr@latin sv ta te tr uk ur vi zh_CN zh_TW
do
	mkdir -p %{buildroot}%{_datadir}/locale/LC_MESSAGES/$dir
done

make install      ROOT=%{buildroot}
%find_lang %{name} || touch %{name}.lang;
%find_lang bindconf || touch bindconf.lang;
cat %{name}.lang bindconf.lang > scb.lang

desktop-file-install --vendor "" --delete-original  \
  --dir %{buildroot}%{_datadir}/applications  \
  --add-category Application  \
  --add-category System  \
  --add-category X-Red-Hat-Base  \
  --add-category X-Red-Hat-ServerConfig  \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

echo "%{version}" > %{buildroot}/usr/share/system-config-bind/%{name}.version

%clean
rm -rf %{buildroot}

%files -f scb.lang
%defattr(-,root,root,-)
%{_sbindir}/system-config-bind
%{_sbindir}/system-config-bind-gui
%{_bindir}/system-config-bind
%{_bindir}/bindconf
%{_datadir}/system-config-bind
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/48x48/apps/preferences-network-dns.png
%config(noreplace) %{_sysconfdir}/security/console.apps/system-config-bind
%config(noreplace) %{_sysconfdir}/security/console.apps/bindconf
%config(noreplace) %{_sysconfdir}/pam.d/system-config-bind
%config(noreplace) %{_sysconfdir}/pam.d/bindconf
%doc %{_datadir}/omf/system-config-bind
%doc %{_datadir}/gnome/help/system-config-bind
%doc COPYING

%post
if [ "$1" = "1" ]; then
   /bin/touch --no-create %{_datadir}/icons/hicolor
   if [ -x /usr/bin/gtk-update-icon-cache ]; then
      /usr/bin/gtk-update-icon-cache -q %{_datadir}/icons/hicolor
   fi
fi

%postun
if [ "$1" -eq 0 ]; then
   /bin/touch --no-create %{_datadir}/icons/hicolor
   if [ -x /usr/bin/gtk-update-icon-cache ]; then
      /usr/bin/gtk-update-icon-cache -q %{_datadir}/icons/hicolor
   fi;
fi;

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.15-5m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.15-4m)
- build fix make-3.82

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.15-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.15-2m)
- full rebuild for mo7 release

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.15-1m)
- update 4.0.15

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.8-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.8-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.8-1m)
- version up 4.0.8

* Mon Jun 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.6-1m)
- sync Fedora
- version up 4.0.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sun Jul 29 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.2-1m)
- import from Fedora

* Tue Apr 3 2007 Ondrej Dvoracek <odvorace@redhat.com> - 4.0.2-6
- Corrected rndc key labels
- Resolves #219795
- Corrected DNSSEC key record column
- Resolves #219798

* Tue Apr 3 2007 Ondrej Dvoracek <odvorace@redhat.com> - 4.0.2-5
- Added SSHFP Resource record
- Resolves #205689
- Corrected some typos in strings
- Resolves #232054

* Tue Jan 23 2007 Martin Stransky <stransky@redhat.com> - 4.0.2-4
- reworked version check
- added check for child processes (#216584)

* Tue Nov 21 2006 Martin Stransky <stransky@redhat.com> - 4.0.2-3
- added a second patch for issue #216584

* Tue Nov 21 2006 Martin Stransky <stransky@redhat.com> - 4.0.2-2
- added version patch (for #216584)

* Tue Oct 17 2006 Martin Stransky <stransky@redhat.com> - 4.0.2-1
- updated translations

* Wed Oct 4 2006 Martin Stransky <stransky@redhat.com> - 4.0.1-3
- added patch for #195001 - string with translators credits

* Tue Sep 12 2006 Martin Stransky <stransky@redhat.com> - 4.0.1-2
- added patch for #195001 - system-config-bind is not fully localized

* Tue Sep 5 2006 Martin Stransky <stransky@redhat.com> - 4.0.1-1
- package version bump

* Fri Jul 21 2006 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-44
- ship updated translations
- fix build and release tag (update release to 44).

* Wed Jul 19 2006 Jesse Keating <jkeating@redhat.com> - 4.0.0-43
- fix release tag

* Wed Jun 14 2006 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-42
- Handle named.conf 'include' statements within clauses 
  e.g. 'view x { include "y"; }'
- Tooltip text not gettext-ized (part of fix for bug 195001)

* Wed Jun 07 2006 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-41
- fix bug 194058: freeze / unfreeze dynamic zones when saving

* Wed Mar 08 2006 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-40
- fix bug 184065: prompts to place slave / DDNS updateable zone
                  files in slaves/ 
- ship updated translations
 
* Tue Mar 01 2006 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-38
- fix bug 182857: add Requires(post): hicolor-icon-them
- ship updated translations

* Tue Jan 10 2006 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-36
- fix bug 176142 (final!) : ship the Serbian translations
- fix str widget save (TXT records)

* Wed Dec 21 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-34
- fix bug 170617: don't use pam_stack in pam configuration file

* Mon Dec 19 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-33
- fix bug 175420: traceback when named.conf included a nonexistent file
- fix bug 176142: ship updated Serbian translation

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov 28 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-32
- fix bug 174284: Lookup.py failed when hostname matches IP
		  address regexps and has no DNS record

* Fri Sep 23 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-31
- fix deletion of record with following records for same name
- fix zone serial increment on save
- ship updated translations

* Mon Aug 15 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-30
- fix IPv6 address labelling
- fix reverse IP zone origin selection menus for A & AAAA records
  when there are forwarder reverse IP zones
- remove CVS directories mistakenly included in previous versions

* Tue Aug 09 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-28
- fix bug 165445: relax restriction on no address records for origin

* Mon Aug 08 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-22
- fix bug 165292: missing 'key' keyword in ACL named keys
- workaround ISC bug 15195: named cannot handle multiple keys in rndc.key

* Fri Jul 29 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-21
- fix bug 164245: generate desktop file from translations in .po files
- fix bug 164613, 164611: translated string typos
- further fix for bug 158438: sentence splitting

* Mon Jul 25 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-20
- fix bug 164129: DNS.py 'declartation' -> 'declaration'
- fix bug 163937: NamedConfOptions 'Name of ke.' -> 'Name of key.'
- fix bug 158438: avoid sentence splitting in translatable messages

* Fri Jul 15 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-18
- fix bug 163304: handle empty contents in Zone.out
- fix bug 161988: create links to .mo files for bindconf
- fix bug 161987: don't use substring of translated string in DNSsec TrustedKeys
- fix bug 159534: add descriptions to deprecated record types
- fix bug 158441: shorten NamedConfOptions description strings

* Mon May 08 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-16
- fix bug 157207:  allow build to succeed if bind package is not installed

* Thu May 05 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-16
- fix out-of-zone data reporting
- out-of-zone string comparison should be case-insensitive
- fix bug 156913: wrong file permissions for config files

* Wed May 04 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-12
- fix bug 156884: handle named.conf with NO options clause .

* Wed Apr 27 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-10
- Add User Guide & Manual in /usr/share/doc/system-config-bind-%{version}

* Wed Apr 27 2005 Miloslav Trmac <mitr@redhat.com> - 4.0.0-9
- Remove dependency on 4Suite (#155113)

* Wed Apr 27 2005 Jeremy Katz <katzj@redhat.com> - 4.0.0-8
- silence %%post

* Mon Apr 04 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-7
- fix bug 153035: gtk.FALSE/TRUE deprecation warnings

* Mon Mar 28 2005 Christopher Aillon <caillon@redhat.com>
- added .spec file icon cache update

* Tue Mar 08 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-4
- fix bug 150011
- Add DNSsec capabilities.

* Sun Feb 01 2005 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-3
- fix bugs 146475, 146606, 143780
 
* Fri Dec 17 2004 Jason Vas Dias <jvdias@redhat.com> - 4.0.0-1
- Initial build.

