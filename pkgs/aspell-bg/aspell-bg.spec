%global momorel 6

%define lang bg
%define langrelease 0
Summary: Bulgarian dictionaries for Aspell
Name: aspell-%{lang}
Version: 4.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Text
URL: http://aspell.net/
Source:   http://dl.sourceforge.net/sourceforge/bgoffice/aspell6-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Buildrequires: aspell >= 0.60
Requires: aspell >= 0.60
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for the following: Bulgarian

%prep

%setup -q -n aspell6-%{lang}-%{version}-%{langrelease}

%build
./configure 
make %{?_smp_mflags}
iconv -f windows-1251 -t utf-8 <bg_phonet.dat >bg_phonet.dat.tmp
mv bg_phonet.dat.tmp bg_phonet.dat


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING Copyright
%{_libdir}/aspell-0.60/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1-1m)
- import from Fedora

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 50:4.1-2
- Autorebuild for GCC 4.3

* Tue Dec 11 2007 Ivana Varekova <varekova@redhat.com> - 50:4.1-1
- update to 4.1

* Wed Jun 27 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50-16
- Resolves: #245257
  aspell-bg 50:0.50-15 kills bulgarian.kbd

* Wed May  2 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50-15
- Resolves: 238426
  convert bulgarian.kbd file

* Wed Mar 28 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50-14
- variable DESTDIR is set in make command

* Tue Mar 27 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50-13
- use configure script to generate makefile

* Thu Feb 22 2007 Ivana Varekova <varekova redhat com> - 50:0.50-12
- spec file cleanup

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-11.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-11.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-11.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Jul 15 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-11
- fix aspell dependence

* Fri Jul 15 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-10
- build with aspell-0.60.3
- convert bulgarian.kdb to utf-8

* Thu Feb 24 2005 Ivana Varekova <varekova@redhat.com> 50:0.50-8
- build new version (tarball 0.50-2), bug #142238

* Tue Oct 26 2004 Adrian Havill <havill@redhat.com> 50:0.50-4
- aspell already owns cp1251.dat (#137022)

* Tue Oct 19 2004 Adrian Havill <havill@redhat.com> 50:0.50-3
- fix version in changelog; incorrect
- fix corrupted word list; incorrect, wrong codeset (#128137)

* Wed Oct 14 2004 Adrian Havill <havill@redhat.com> 50:0.50-2
- Remove debuginfo

* Wed Aug 11 2004 Adrian Havill <havill@redhat.com> 50:0.50-1
- initial release
