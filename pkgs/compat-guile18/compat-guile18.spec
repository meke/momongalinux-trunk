%global momorel 1
%global mver 1.8

Summary: A GNU implementation of Scheme for application extensibility
Name: compat-guile18
Version: 1.8.8
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
URL: http://www.gnu.org/software/guile/
Group: Development/Languages
Source0: ftp://ftp.gnu.org/pub/gnu/guile/guile-%{version}.tar.gz
NoSource: 0
Patch1: guile-1.8.7-multilib.patch
Patch2: guile-1.8.7-testsuite.patch
Patch3: guile-1.8.8-deplibs.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: readline-devel >= 5.0
BuildRequires: libtool-ltdl-devel >= 2.2.0
BuildRequires: gmp-devel >= 5.0

Requires: coreutils
Requires(post): info
Requires(preun): info

Provides: /usr/bin/guile
Obsoletes: guile-oops guild-oops-devel
Provides: guile-oops

%description
GUILE (GNU's Ubiquitous Intelligent Language for Extension) is a library
implementation of the Scheme programming language, written in C.  GUILE
provides a machine-independent execution platform that can be linked in
as a library during the building of extensible programs.

Install the guile package if you'd like to add extensibility to programs
that you are developing.

%package devel
Summary: Libraries and header files for the GUILE extensibility library.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The guile-devel package includes the libraries, header files, etc.,
that you'll need to develop applications that are linked with the
GUILE extensibility library.

You need to install the guile-devel package if you want to develop
applications that will be linked to GUILE.  You'll also need to
install the guile package.

%prep
%setup -q -n guile-%{version}
%patch1 -p1 -b .multilib
%patch2 -p1 -b .testsuite
%patch3 -p1 -b .deplibs

%build
%configure --disable-static \
    --disable-error-on-warning \
    --with-pic \
    --with-threads \
    --without-lispdir

# Remove RPATH
sed -i 's|" $sys_lib_dlsearch_path "|" $sys_lib_dlsearch_path %{_libdir} "|' \
    {,guile-readline/}libtool

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}%{_datadir}/guile/site

for i in %{buildroot}%{_infodir}/goops.info; do
    iconv -f iso8859-1 -t utf-8 < $i > $i.utf8 && mv -f ${i}{.utf8,}
done

rm -f %{buildroot}%{_infodir}/dir
find %{buildroot} -name "*.la" -delete

# Necessary renaming and removing
rm -rf %{buildroot}%{_infodir}
mv %{buildroot}%{_bindir}/guile{,%{mver}}
mv %{buildroot}%{_bindir}/guile{,%{mver}}-tools
mv %{buildroot}%{_mandir}/man1/guile{,%{mver}}.1
mv %{buildroot}%{_bindir}/guile{,%{mver}}-config
mv %{buildroot}%{_bindir}/guile{,%{mver}}-snarf
mv %{buildroot}%{_datadir}/aclocal/guile{,%{mver}}.m4
sed -i -e 's|/usr/bin/guile|/usr/bin/guile%{mver}|' \
    %{buildroot}%{_bindir}/guile%{mver}-config
sed -i -e 's|guile-tools|guile%{mver}-tools|g' \
    %{buildroot}%{_bindir}/guile%{mver}-tools
sed -i -e 's|guile-snarf|guile%{mver}-snarf|g' \
    %{buildroot}%{_bindir}/guile%{mver}-snarf

ac=%{buildroot}%{_datadir}/aclocal/guile%{mver}.m4
sed -i -e 's|,guile|,guile%{mver}|g' $ac
sed -i -e 's|guile-tools|guile%{mver}-tools|g' $ac
sed -i -e 's|guile-config|guile%{mver}-config|g' $ac
sed -i -e 's|GUILE_PROGS|GUILE1_8_PROGS|g' $ac
sed -i -e 's|GUILE_FLAGS|GUILE1_8_FLAGS|g' $ac
sed -i -e 's|GUILE_SITE_DIR|GUILE1_8_SITE_DIR|g' $ac
sed -i -e 's|GUILE_CHECK|GUILE1_8_CHECK|g' $ac
sed -i -e 's|GUILE_MODULE_CHECK|GUILE1_8_MODULE_CHECK|g' $ac
sed -i -e 's|GUILE_MODULE_AVAILABLE|GUILE1_8_MODULE_AVAILABLE|g' $ac
sed -i -e 's|GUILE_MODULE_REQUIRED|GUILE1_8_MODULE_REQUIRED|g' $ac
sed -i -e 's|GUILE_MODULE_EXPORTS|GUILE1_8_MODULE_EXPORTS|g' $ac
sed -i -e 's|GUILE_MODULE_REQUIRED_EXPORT|GUILE1_8_MODULE_REQUIRED_EXPORT|g' $ac

# Compress large documentation
bzip2 NEWS

touch %{buildroot}%{_datadir}/guile/%{mver}/slibcat
ln -s ../../slib %{buildroot}%{_datadir}/guile/%{mver}/slib

%check
export LD_LIBRARY_PATH=`pwd`/test-suite/standalone
make %{?_smp_mflags} check

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%triggerin -- slib
# Remove files created in guile < 1.8.3-2
rm -f %{_datadir}/guile/site/slib{,cat}

ln -sfT ../../slib %{_datadir}/guile/%{mver}/slib
rm -f %{_datadir}/guile/%{mver}/slibcat
export SCHEME_LIBRARY_PATH=%{_datadir}/slib/

# Build SLIB catalog
for pre in \
    "(use-modules (ice-9 slib))" \
    "(load \"%{_datadir}/slib/guile.init\")"
do
    %{_bindir}/guile%{mver} -c "$pre
        (set! implementation-vicinity (lambda ()
        \"%{_datadir}/guile/%{mver}/\"))
        (require 'new-catalog)" &> /dev/null && break
    rm -f %{_datadir}/guile/%{mver}/slibcat
done
:

%triggerun -- slib
if [ "$2" = 0 ]; then
    rm -f %{_datadir}/guile/%{mver}/slib{,cat}
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING* ChangeLog HACKING INSTALL
%doc LICENSE NEWS.bz2 README THANKS
%{_bindir}/guile%{mver}
%{_bindir}/guile%{mver}-tools
%{_libdir}/libguile-srfi-srfi-*.so
%{_libdir}/libguile*.so.*
%{_libdir}/libguilereadline-*.so
%{_datadir}/guile/%{mver}
%{_mandir}/man1/guile%{mver}.1*

%files devel
%defattr(-,root,root,-)
%{_bindir}/guile%{mver}-config
%{_bindir}/guile%{mver}-snarf
%{_includedir}/guile/*
%{_includedir}/libguile
%{_includedir}/libguile.h
%{_libdir}/libguile.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/guile%{mver}.m4

%changelog
* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.8-1m)
- import from Fedora
