%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define debug_package %{nil}

Name: bzr-svn
Summary: Subversion branch support for Bazaar
Version: 1.2.2
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Development/Tools
URL: http://bazaar-vcs.org/BzrForeignBranches/Subversion
Source0: http://samba.org/~jelmer/bzr/bzr-svn-%{version}.tar.gz
NoSource: 0

BuildRequires: bzr >= 2.3 python-devel >= 2.7 subversion python-subvertpy >= 0.6.4
Requires: bzr >= 2.3 bzr-rebase subversion python-subvertpy >= 0.6.4

%description
bzr-svn is a plugin that allows Bazaar direct access to Subversion
repositories. It allows most bzr commands to work directly against
Subversion repositories, as if you were using bzr with a native bzr
repository.

%prep
%setup -q

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --root %{buildroot} --skip-build

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING FAQ HACKING INSTALL NEWS README TODO 
%{python_sitelib}/bzrlib/plugins/svn/
%{python_sitelib}/bzr_svn-*.egg-info

%changelog
* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2 release

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2 release

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0 release

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.5m)
- update to 1.1.0-20110919

* Sun Jun  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.4m)
- update to 1.1.0-20110605

* Wed May 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.3m)
- update to 1.1.0-20110511

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.2m)
- rebuild against python-2.7

* Sun Apr 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-0.1m)
- update to 1.1.0-snapshot

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-2m)
- rebuild for new GCC 4.6

* Tue Feb  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-1m)
- update to 1.0.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-1m)
- update to 1.0.3

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-1m)
- update 1.0.1

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.4-1m)
- update 0.6.4

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Sat May 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-1m)
- update to 0.6.1

* Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Sun Mar  8 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-2m)
- build fix in x86_64

* Sun Mar  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Thu Feb 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-1m)
- initial packaging
