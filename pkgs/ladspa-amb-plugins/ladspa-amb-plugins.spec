%global momorel 1

Summary: A set of audio plugins for LADSPA by Fons Adriaensen.
Name: ladspa-amb-plugins
Version: 0.8.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://kokkinizita.linuxaudio.org/linuxaudio/
Group: Applications/Multimedia
Source0:  http://kokkinizita.linuxaudio.org/linuxaudio/downloads/AMB-plugins-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
BuildRequires: ladspa-devel

%description
A set of first order Ambisonics plugins. Included are: mono and stereo input panner, horizontal rotation, and square and hexagon horizontal decoders. See the README for more.

%prep
%setup -q -n AMB-plugins-%{version}
rm ladspa.h

%build
%make

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_libdir}/ladspa
install -m 755 *.so %{buildroot}%{_libdir}/ladspa

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README
%{_libdir}/ladspa/*.so

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Sun May 29 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1
- change Source URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.4.0-1m)
- initial build for Momonga Linux
