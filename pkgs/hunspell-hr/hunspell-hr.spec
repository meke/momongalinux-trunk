%global momorel 6

Name: hunspell-hr
Summary: Croatian hunspell dictionaries
%define upstreamid 20040608
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
#Epoch: 1
Source: http://cvs.linux.hr/spell/myspell/hr_HR.zip
Group: Applications/Text
URL: http://cvs.linux.hr/spell/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: "LGPLv2+ or SISSL"
BuildArch: noarch

Requires: hunspell

%description
Croatian hunspell dictionaries.

%package -n hyphen-hr
Requires: hyphen
Summary: Croatian hyphenation rules
Group: Applications/Text

%description -n hyphen-hr
Croatian hyphenation rules.

%prep
%setup -q -c -n hunspell-hr

%build
chmod -x *

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p hr_HR.dic hr_HR.aff $RPM_BUILD_ROOT/%{_datadir}/myspell
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/hyphen
cp -p hyph_hr.dic $RPM_BUILD_ROOT/%{_datadir}/hyphen/hyph_hr_HR.dic

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_hr_HR.txt
%{_datadir}/myspell/*

%files -n hyphen-hr
%defattr(-,root,root,-)
%{_datadir}/hyphen/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20040608-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20040608-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20040608-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20040608-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20040608-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20040608-1m)
- import from Fedora to Momonga
- delete Epoch

* Thu Nov 22 2007 Caolan McNamara <caolanm@redhat.com> - 1:0.20040608-2
- package hyphenation rules

* Tue Aug 21 2007 Caolan McNamara <caolanm@redhat.com> - 1:0.20040608-1
- clarify license
- canonical upstream version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20060607-1
- initial version
