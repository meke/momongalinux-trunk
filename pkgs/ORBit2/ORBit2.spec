%global momorel 4

Summary: A high-performance CORBA Object Request Broker.
Name: ORBit2
Version: 2.14.19
Release: %{momorel}m%{?dist}
Group: System Environment/Daemons
License: LGPL and GPL
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.14/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: linc-devel >= 1.1.1
BuildRequires: libIDL-devel >= 0.8.12
BuildRequires: glib2-devel >= 2.19.10

Requires(pre): info
Requires: indent

%description
ORBit2 is a high-performance CORBA (Common Object Request Broker 
Architecture) ORB (object request broker). It allows programs to 
send requests and receive replies from other programs, regardless 
of the locations of the two programs. CORBA is an architecture that 
enables communication between program objects, regardless of the 
programming language they're written in or the operating system they
run on.
#'
You will need to install this package and ORBIT-devel if you want to 
write programs that use CORBA technology.

%package devel
Summary: Development libraries, header files and utilities for ORBit2.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: linc-devel
Requires: libIDL-devel

%description devel
ORBit2 is a high-performance CORBA (Common Object Request Broker
Architecture) ORB (object request broker) with support for the 
C language.

This package contains the header files, libraries and utilities 
necessary to write programs that use CORBA technology. If you want to
write such programs, you'll also need to install the ORBIT package.
#'

%prep
%setup -q

%build
%configure \
    --enable-gtk-doc \
    --enable-http \
    --enable-purify \
    --disable-static
make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%__install -m 755 -d %{buildroot}%{_includedir}/orbit-2.0/orbit/GIOP
%__install -m 644 include/orbit/GIOP/*.h %{buildroot}%{_includedir}/orbit-2.0/orbit/GIOP

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog HACKING INSTALL MAINTAINERS NEWS README TODO
%{_bindir}/typelib-dump
%{_bindir}/ior-decode-2
%{_bindir}/linc-cleanup-sockets
%dir %{_libdir}/orbit-2.0
%{_libdir}/orbit-2.0/Everything_module.so
%{_libdir}/orbit-2.0/Everything_module.la
%{_libdir}/libORBit-2.so.*
%{_libdir}/libORBit-imodule-2.so.*
%{_libdir}/libORBitCosNaming-2.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root)
%doc COPYING* test
%{_bindir}/orbit-idl-2
%{_bindir}/orbit2-config
%{_libdir}/pkgconfig/ORBit-2.0.pc
%{_libdir}/pkgconfig/ORBit-CosNaming-2.0.pc
%{_libdir}/pkgconfig/ORBit-idl-2.0.pc
%{_libdir}/pkgconfig/ORBit-imodule-2.0.pc
%{_libdir}/*.so
%{_libdir}/libname-server-2.a
%{_datadir}/idl/orbit-2.0
%{_datadir}/aclocal/ORBit2.m4
%{_includedir}/orbit-2.0
%doc %{_datadir}/gtk-doc/html/ORBit2

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.19-4m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.19-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.19-2m)
- rebuild for new GCC 4.5

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.14.19-1m)
- update to 2.14.19

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14.18-2m)
- full rebuild for mo7 release

* Tue Apr 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.18-1m)
- update to 2.14.18

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.17-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.17-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.17-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.13-1m)
- update to 2.14.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.16-2m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.13-1m)
- update to 2.14.13

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.14.12-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.12-1m)
- update to 2.14.12

* Sun Jan 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.11-1m)
- update to 2.14.11

* Mon Oct 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.10-1m)
- update to 2.14.10

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.9-1m)
- update to 2.14.9

* Fri Jul 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.8-1m)
- update to 2.14.8

* Tue Jul  3 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.14.7-2m)
- remove % from make to build package without getting into loop
  when building with make-3.81-1m.mo4

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.7-1m)
- update to 2.14.7

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.6-1m)
- update to 2.14.6

* Thu Feb  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.5-1m)
- update to 2.14.5

* Mon Dec 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-1m)
- update to 2.14.4

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Apr  6 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-1m)
- version up.
- GNOME 2.14.0 Desktop

* Thu Feb  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.5-1m)
- update to 2.12.5

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-3m)
- comment out unnessesaly autoreconf and make check

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-2m)
- enable gtk-doc

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.4-1m)
- version up.
- GNOME 2.12.1 Desktop

* Thu Jul 14 2005 Toru Hoshina <t@momonga-linux.org>
- (2.12.0-2m)
- no more aclocal-1.7 & autoconf.

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.0-1m)
- version 2.12.0
- GNOME 2.8 Desktop
- disable gtk-doc

* Sun Dec 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.2-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Sun Aug 14 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.10.2-1m)
- version 2.10.2

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.10.0-2m)
- adjustment BuildPreReq

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.10.0-1m)
- version 2.10.0
- GNOME 2.6 Desktop

* Fri Apr  2 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.8.3-4m)
- remove 'Obsoletes: pyorbit gnome-python'

* Wed Mar 31 2004 Toru Hoshina <t@momonga-linux.org>
- (2.8.3-3m)
- revised spec. some executable files are moved to devel packages.

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.8.3-2m)
- revised spec for enabling rpm 4.2.

* Wed Jan 21 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.8.3-1m)
- version 2.8.3

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.8.2-2m)
- devel package includes COPYING, COPYING.LIB and some tests.
- pretty spec file.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.8.2-1m)
- version 2.8.2

* Sun Oct 12 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.1-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.8.1-1m)
- version 2.8.1

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.7.6-1m)
- version 2.7.6

* Sat Aug 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.7.5-1m)
- version 2.7.5

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.7.3-1m)
- version 2.7.3

* Mon Jul  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.7.2-3m)
- rebuild against rpm-4.0.4-52m

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.7.2-2m)
- install GIOP headers.

* Tue Jun 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.7.2-1m)
- version 2.7.2

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.7.1-1m)
- version 2.7.1

* Wed Mar 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.7.0-1m)
- version 2.7.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.1-1m)
- version 2.6.1

* Wed Jan 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.0-1m)
- version 2.6.0

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.1-1m)
- version 2.5.1

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.0-1m)
- version 2.5.0

* Sat Aug 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.3-1m)
- version 2.4.3

* Thu Aug 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.2-1m)
- version 2.4.2

* Tue Aug 06 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0-8m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0-7m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0-6k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.0-2k)
- version 2.4.0

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.110-2k)
- version 2.3.110

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.109-2k)
- version 2.3.109

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.3.108-4k)
- /sbin/install-info -> info in PreReq.

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.108-2k)
- version 2.3.108

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.107-2k)
- version 2.3.107

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.106-10k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.106-8k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.106-6k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.106-4k)
- modify file list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.106-2k)
- version 2.3.106

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.105-6k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.105-4k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.105-2k)
- version 2.3.105
* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.104-4k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.104-2k)
- version 2.3.104
- rebuild against for libIDL-0.7.4

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.103-4k)
- rebuild against for glib-1.3.13

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.103-2k)
- version 2.3.103
- rebuild against for libIDL-0.7.3
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (2.3.102-4k)
- rebuild against for linc-0.1.15

* Wed Dec 26 2001 Shingo Akagaki <dora@kondara.org>
- (2.3.100-2k)
- port from Jirai
- version 2.3.100

* Fri Sep 21 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (2.3.95-0.200109273k)
- 2.0.0
