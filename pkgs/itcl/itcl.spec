%global momorel 6

%{!?tcl_version: %define tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitearch: %define tcl_sitearch %{_libdir}/tcl%{tcl_version}}

Name:           itcl
Version:        3.4
Release:        %{momorel}m%{?dist}
Summary:        Object oriented extensions to Tcl and Tk

Group:          Development/Libraries
License:        "TCL"
URL:            http://incrtcl.sourceforge.net/itcl/
# cvs -d:pserver:anonymous@incrtcl.cvs.sourceforge.net:/cvsroot/incrtcl export -D 2007-12-31 -d incrtcl-20071231cvs itcl
# tar czf incrtcl-20071231cvs.tgz ./incrtcl-20071231cvs
Source0:        incrtcl-20071231cvs.tgz
Patch0:         itcl-3.4-tclbindir.patch
Patch1:         itcl-3.4-libdir.patch
Patch2:         itcl-3.4-soname.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:  tcl(abi) = 8.5
BuildRequires:       tcl-devel

%description
[incr Tcl] is Tcl extension that provides object-oriented features that are
missing from the Tcl language.

%package devel
Summary:  Development headers and libraries for linking against itcl
Group: Development/Libraries
Requires:       %{name} = %{version}-%{release}
%description devel
Development headers and libraries for linking against itcl.

%prep
%setup -q -n incrtcl-20071231cvs
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# Patch the updated location of the stub library
sed -i -e "s#%{_libdir}/%{name}%{version}#%{tcl_sitearch}/%{name}%{version}#" \
        $RPM_BUILD_ROOT%{_libdir}/itclConfig.sh

%check
make test

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%dir %{tcl_sitearch}/%{name}%{version}
%{tcl_sitearch}/%{name}%{version}/*.tcl
%{_libdir}/*.so
%{_mandir}/mann/*.*
%doc license.terms

%files devel
%defattr(-,root,root,-)
%{_includedir}/*.h
%{tcl_sitearch}/%{name}%{version}/*.a
%{_libdir}/itclConfig.sh

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4-4m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4-2m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4-1m)
- import from Fedora for plplot->gdl

* Sat Feb 9 2008 Wart <wart@kobold.org> - 3.4-2
- Rebuild for gcc 4.3
- Add patch for adding soname

* Thu Jan 10 2008 Wart <wart@kobold.org> - 3.4-1
- Update to latest CVS head for tcl 8.5 compatibility

* Wed Dec 19 2007 Wart <wart@kobold.org> - 3.3-0.11.RC1
- Move libitcl shared library to %%{_libdir} so that applications
  linked against itcl can find it. (BZ #372791)

* Sun Aug 19 2007 Wart <wart@kobold.org> - 3.3-0.10.RC1
- License tag clarification
- Better download URL
- Minor rpmlint cleanup

* Thu Mar 22 2007 Orion Poplawski <orion@cora.nwra.com> - 3.3-0.9.RC1
- Rebuild for tcl8.4 downgrade

* Thu Feb 8 2007 Wart <wart at kobold.org> - 3.3-0.8.RC1
- Rebuild for tcl8.5a5

* Mon Aug 28 2006 Wart <wart at kobold.org> - 3.3-0.7.RC1
- Rebuild for Fedora Extras

* Fri Jun 2 2006 Wart <wart at kobold.org> - 3.3-0.6.RC1
- Added upstream's patch to close a minor memory leak

* Thu Jun 1 2006 Wart <wart at kobold.org> - 3.3-0.5.RC1
- Fix BR: for -devel subpackage

* Thu Feb 16 2006 Wart <wart at kobold.org> - 3.3-0.4.RC1
- Rebuild for FC-5

* Fri Jan 27 2006 Wart <wart at kobold.org> - 3.3-0.3.RC1
- Remove duplicate in file list.

* Wed Jan 11 2006 Wart <wart at kobold.org> - 3.3-0.2.RC1
- Fix quoting bug that is exposed by bash >= 3.1

* Mon Jan 9 2006 Wart <wart at kobold.org> - 3.3-0.1.RC1
- Update to 3.3 upstream sources.  itk now uses a different source
  archive than itcl and has been moved to a separate spec file.

* Wed Dec 28 2005 Wart <wart at kobold.org> - 3.2.1-4
- Create itk as a subpackage.
- Rename patch to include version number.
- New source url.

* Fri Nov 25 2005 Wart <wart at kobold.org> - 3.2.1-3
- Minor fixes to remove rpmlint warnings.
- Move DSOs to itcl library directory instead of polluting /usr/lib.

* Sat Oct 22 2005 Wart <wart at kobold.org> - 3.2.1-2
- Look for itk.tcl in the lib64 directory for x86_64 platforms.

* Fri Oct 21 2005 Wart <wart at kobold.org> - 3.2.1-1
- Intial spec file for Fedora Extras
