%global momorel 1

Name:           b43-fwcutter
Version:        018
Release:        %{momorel}m%{?dist}
Summary:        Firmware extraction tool for Broadcom wireless driver

Group:          System Environment/Base
License:        BSD
URL:            http://bu3sch.de/b43/fwcutter/
Source0:        http://bu3sch.de/b43/fwcutter/%{name}-%{version}.tar.bz2
Source1:        README.too
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package contains the 'b43-fwcutter' tool which is used to
extract firmware for the Broadcom network devices.

See the README.Fedora file shipped in the package's documentation for
instructions on using this tool.

%prep
%setup -q

cp %{SOURCE1} .

%build
CFLAGS="$RPM_OPT_FLAGS" make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -m0755 b43-fwcutter $RPM_BUILD_ROOT%{_bindir}/b43-fwcutter
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
install -m0644 b43-fwcutter.1 $RPM_BUILD_ROOT%{_mandir}/man1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/b43-fwcutter
%{_mandir}/man1/*
%doc COPYING README README.too

%changelog
* Mon Oct 28 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (018-1m)
- version 018

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (015-1m)
- update to 015
- add source

* Sun Sep 11 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (014-1m)
- version up 014

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (013-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (013-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (013-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (013-1m)
- update to 013

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (011-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (011-3m)
- rebuild against rpm-4.6

* Tue May  6 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (011-2m)
- change Source0 URL

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (011-1m)
- import from Fedora

* Mon Feb 15 2008 John W. Linville <linville@redhat.com> 011-3
- Update for b43-fwcutter-011

* Mon Jan 21 2008 John W. Linville <linville@redhat.com> 010-2
- Update for b43-fwcutter-010

* Thu Aug 23 2007 John W. Linville <linville@redhat.com> 008-1
- Import skeleton from bcm43xx-fwcutter-006-3
- Initial build
