%global momorel 3
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m
%global kdepimlibsrel 1m
%global qimageblitzver 0.0.6

Summary: Notes taker for KDE
Name: basket
Version: 1.81
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://basket.kde.org/
Group: Applications/Productivity
Source0: http://basket.kde.org/downloads/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-20091213-desktop.patch
Patch1: %{name}-%{version}-make-doc.patch
Patch2: %{name}-1.80-fix-docbook.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdepimlibs-devel >= %{kdever}-%{kdepimlibsrel}
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: gamin-devel
BuildRequires: gettext
BuildRequires: gpgme-devel
BuildRequires: libxml2
BuildRequires: qimageblitz-devel >= %{qimageblitzver}

%description
This application is mainly an all-purpose notes taker. It provides baskets
where any item can be dragged and dropped: text, formatted text, links,
images, sounds, files, colors, application launcher...
BasKet lets you keep all objects in one place, and keep data at hand.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .make-doc
%patch2 -p1 -b .docbook

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README TODO
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/%{name}thumbcreator.so
%{_kde4_libdir}/kde4/kcm_%{name}.so
%{_kde4_libdir}/lib%{name}common.so*
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_configdir}/magic/%{name}.magic
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/actions/likeback_*.png
%{_kde4_iconsdir}/hicolor/*/actions/tag_*.png
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_datadir}/kde4/services/%{name}_config_apps.desktop
%{_kde4_datadir}/kde4/services/%{name}_config_%{name}s.desktop
%{_kde4_datadir}/kde4/services/%{name}_config_general.desktop
%{_kde4_datadir}/kde4/services/%{name}_config_new_notes.desktop
%{_kde4_datadir}/kde4/services/%{name}_config_notes_appearance.desktop
%{_kde4_datadir}/kde4/services/%{name}thumbcreator.desktop
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/mimelnk/application/x-%{name}-archive.desktop
%{_kde4_datadir}/mimelnk/application/x-%{name}-template.desktop
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.81-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.81-2m)
- rebuild for new GCC 4.5

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.81-1m)
- update to version 2.0-beta2 (1.81)

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.80-5m)
- rebuild against qt-4.7.0 and qimageblitz-0.0.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.80-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.80-3m)
- fix build with KDE 4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.80-2m)
- rebuild against qt-4.6.3-1m

* Wed Mar 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.80-1m)
- update to version 2.0-beta (1.80)
- set NoSource: 0 again

* Wed Jan  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3.1-4.20100106.1m)
- update to 20100106 git snapshot
- move basket.desktop from Utility to Office
- remove merged fix-crash.patch and gcc44.patch
- add make-doc.patch

* Sun Dec 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3.1-4.20091213.1m)
- update to 20091213 git snapshot
- update desktop.patch
- update gcc43.patch
- good-bye KDE3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3.1-2m)
- rebuild against rpm-4.6

* Sat Jul  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3.1-1m)
- version 1.0.3.1
- remove merged fix-desktop.patch
- add desktop.patch for Japanese

* Fri Jun 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-1m)
- version 1.0.3
- fix up basket.desktop

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-8m)
- rebuild without kdepim

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-7m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-6m)
- rebuild against gcc43

* Sun Feb 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-5m)
- specify KDE3 headers and libs
- modify BuildRequires

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-4m)
- %%NoSource -> NoSource

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- add patch for gcc43, generated by gen43patch(v1)

* Fri Jul 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-2m)
- import basket-1.0-fix-crash.patch from cooker

* Sat Apr 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-1m)
- version 1.0.2

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- initial package for Momonga Linux
