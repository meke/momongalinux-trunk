%global momorel 1
%define debug_package %{nil}

Name:           libquvi-scripts
Version:        0.4.4 
Release: %{momorel}m%{?dist}
Summary:        Embedded lua scripts that libquvi uses for parsing the media details

Group:          Applications/Internet
License:        LGPLv2+
URL:            http://quvi.sourceforge.net/
Source0:        http://downloads.sourceforge.net/quvi/%{name}-%{version}.tar.gz
NoSource: 0

BuildArch:      noarch

%description
libquvi-scripts contains the embedded lua scripts that libquvi
uses for parsing the media details. Some additional utility
scripts are also included.


%prep
%setup -q

%build
%configure --with-nsfw --with-nlfy

%install
make install DESTDIR=%{buildroot} pkgconfigdir=%{_datadir}/pkgconfig/

%files
%doc ChangeLog COPYING README AUTHORS NEWS 
%{_datadir}/%{name}/
%{_mandir}/man7/%{name}.7.*
%{_datadir}/pkgconfig/%{name}.pc

%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-1m)
- import from fedora

