%global momorel 7
%global abi_ver 1.9.1

# Generated from fastthread-1.0.1.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname fastthread
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Optimized replacement for thread.rb primitives
Name: rubygem-%{gemname}
Version: 1.0.7
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubyforge.org/projects/mongrel/
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
BuildRequires: rubygems
Provides: rubygem(%{gemname}) = %{version}

%description
Optimized replacement for thread.rb primitives


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.7-7m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7-4m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.7-3m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.7-1m)
- update 1.0.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-1m)
- Initial package for Momonga Linux
