#!/bin/bash
#

export LANG=C

get_current_revision()
{
    svn info $1 | awk '"Revision:"==$1 {print $2}'
}

error()
{
    echo "$@" > /dev/stderr
    exit 2
}

gen_diff()
{
    [ -n "$1" ] || error "missing URI operand"
    [ -n "$2" ] || error "missing repo-name operand"

    URI=$1
    NAME=$2

    PREV=`for x in ${NAME}_*_*.patch.xz; do x=${x/.patch.xz/}; echo ${x/*_/}; done| sort -g |tail -n 1`
    HEAD=`get_current_revision $1`

    if [ $HEAD -eq $PREV ]; then
	echo "NO UPDATE AVAILABLE in $1"
	return 
   fi

    dest=${NAME}_${PREV}_${HEAD}.patch
    echo "generating $dest.xz ..."
    svn diff $URI -r$PREV:$HEAD > $dest
    xz $dest

}

gen_diff svn://svn.valgrind.org/valgrind/trunk valgrind
gen_diff svn://svn.valgrind.org/vex/trunk      valgrind-vex

exit 0
