#!/bin/bash

gen_patch_list()
{
    for x in $1; do
	[ -f $x ] || continue
	echo $x
    done
}

make_patch_block()
{
    pattern="$1"
    count=$2
    shift
    shift
    while [ -n "$1" ]; do
	printf "$pattern" $count "$1"
	count=$(($count + 1))
	shift
    done
}

replace_block()
{
    awk -vTAG1="$1" -vTAG2="$2" -vBODY="$3" 'BEGIN {
	while (getline > 0) {
	    print $0
	    if (TAG1==$0) {
		break
	    }
	}
	print BODY
	while (getline > 0) {
	    if (TAG2==$0) {
		print $0
		break
	    }
	}
	while (getline > 0) {
	    print $0
	}
    }'
}

LIST1=`gen_patch_list 'valgrind_*_*.patch.xz'`
LIST2=`gen_patch_list 'valgrind-vex_*_*.patch.xz'`

cp -f valgrind.spec valgrind.spec.bak
cat valgrind.spec.bak \
    | replace_block "#--BP1A--" "#--EP1A--" "`make_patch_block "Patch%d: %s\n" 1000 $LIST1`" \
    | replace_block "#--BP2A--" "#--EP2A--" "`make_patch_block "Patch%d: %s\n" 2000 $LIST2`" \
    | replace_block "#--BP1B--" "#--EP1B--" "`make_patch_block "%%patch%d -p0 -E -b .%s~\n" 1000 $LIST1`" \
    | replace_block "#--BP2B--" "#--EP2B--" "`make_patch_block "%%patch%d -p0 -E -b .%s~\n" 2000 $LIST2`" \
    > valgrind.spec

exit 0
