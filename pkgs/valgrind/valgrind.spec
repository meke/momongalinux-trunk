%global momorel 1
%global fedora 18

Summary: Tool for finding memory management bugs in programs
Name: valgrind
Version: 3.9.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.valgrind.org/
Group: Development/Debuggers

# Only necessary for RHEL, will be ignored on Fedora
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: http://www.valgrind.org/downloads/valgrind-%{version}.tar.bz2
NoSource: 0

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpmbuild/specopt/valgrind.specopt and edit it.
%{?!do_test: %global do_test 1}

# Needs investigation and pushing upstream
Patch1: valgrind-3.9.0-cachegrind-improvements.patch

# KDE#211352 - helgrind races in helgrind's own mythread_wrapper
Patch2: valgrind-3.9.0-helgrind-race-supp.patch

# undef st_atime, st_mtime and st_ctime. Unknown why this is (still?) needed.
Patch3: valgrind-3.9.0-stat_h.patch

# Make ld.so supressions slightly less specific.
Patch4: valgrind-3.9.0-ldso-supp.patch

# On some ppc64 installs these test just hangs
Patch5: valgrind-3.9.0-gdbserver_tests-mcinvoke-ppc64.patch

# KDE#326983 - insn_basic test might crash because of setting DF flag 
Patch6: valgrind-3.9.0-amd64_gen_insn_test.patch

# KDE#327837 - dwz compressed alternate .debug_info/str not read correctly.
Patch7: valgrind-3.9.0-dwz-alt-buildid.patch

# KDE#327284 - s390x VEX miscompilation of -march=z10 binary
Patch8: valgrind-3.9.0-s390-risbg.patch

%ifarch x86_64 ppc64
# Ensure glibc{,-devel} is installed for both multilib arches
BuildRequires: /lib/libc.so.6 /usr/lib/libc.so /lib64/libc.so.6 /usr/lib64/libc.so
%endif
%if 0%{?fedora} >= 15
BuildRequires: glibc-devel >= 2.14
%else
%if 0%{?rhel} >= 6
BuildRequires: glibc-devel >= 2.12
%else
BuildRequires: glibc-devel >= 2.5
%endif
%endif
%ifarch %{ix86} x86_64 ppc ppc64 %{arm}
BuildRequires: openmpi-devel >= 1.3.3
%endif

# For %%build and %%check.
# In case of a software collection, pick the matching gdb and binutils.
BuildRequires: gdb
BuildRequires: binutils

# gdbserver_tests/filter_make_empty uses ps in test
BuildRequires: procps

ExclusiveArch: %{ix86} x86_64 ppc ppc64 s390x %{arm}
%ifarch %{ix86}
%define valarch x86
%define valsecarch %{nil}
%endif
%ifarch x86_64
%define valarch amd64
%define valsecarch x86
%endif
%ifarch ppc
%define valarch ppc32
%define valsecarch ppc64
%endif
%ifarch ppc64
%define valarch ppc64
%define valsecarch ppc32
%endif
%ifarch s390x
%define valarch s390x
%define valsecarch %{nil}
%endif
%ifarch armv7hl
%define valarch armv7hl
%define valsecarch %{nil}
%endif
%ifarch armv5tel
%define valarch armv5tel
%define valsecarch %{nil}
%endif

%description
Valgrind is a tool to help you find memory-management problems in your
programs. When a program is run under Valgrind's supervision, all
reads and writes of memory are checked, and calls to
malloc/new/free/delete are intercepted. As a result, Valgrind can
detect a lot of problems that are otherwise very hard to
find/diagnose.

%package devel
Summary: Development files for valgrind
Group: Development/Debuggers
Requires: valgrind = %{version}-%{release}
Provides: %{name}-static = %{version}-%{release}

%description devel
Header files and libraries for development of valgrind aware programs
or valgrind plugins.

%package openmpi
Summary: OpenMPI support for valgrind
Group: Development/Debuggers
Requires: valgrind = %{version}-%{release}

%description openmpi
A wrapper library for debugging OpenMPI parallel programs with valgrind.
See the section on Debugging MPI Parallel Programs with Valgrind in the
Valgrind User Manual for details.

%prep
%setup -q

%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1

%build
# We need to use the software collection compiler and binutils if available.
# The configure checks might otherwise miss support for various newer
# assembler instructions.
%{?scl:PATH=%{_bindir}${PATH:+:${PATH}}}

CC=gcc
%ifarch x86_64 ppc64
# Ugly hack - libgcc 32-bit package might not be installed
mkdir -p libgcc/32
ar r libgcc/32/libgcc_s.a
ar r libgcc/libgcc_s_32.a
CC="gcc -B `pwd`/libgcc/"
%endif

# Old openmpi-devel has version depended paths for mpicc.
%if 0%{?fedora} >= 13 || 0%{?rhel} >= 6
%define mpiccpath %{!?scl:%{_libdir}}%{?scl:%{_root_libdir}}/openmpi/bin/mpicc
%else
%define mpiccpath %{!?scl:%{_libdir}}%{?scl:%{_root_libdir}}/openmpi/*/bin/mpicc
%endif

# Filter out some flags that cause lots of valgrind test failures.
# Also filter away -O2, valgrind adds it wherever suitable, but
# not for tests which should be -O0, as they aren't meant to be
# compiled with -O2 unless explicitely requested. Same for any -mcpu flag.
# Ideally we will change this to only be done for the non-primary build
# and the test suite.
OPTFLAGS="`echo " %{optflags} " | sed 's/ -m\(64\|3[21]\) / /g;s/ -fexceptions / /g;s/ -fstack-protector / / g;s/ -Wp,-D_FORTIFY_SOURCE=2 / /g;s/ -O2 / /g;s/ -mcpu=\([a-z0-9]\+\) / /g;s/^ //;s/ $//'`"
%configure CC="$CC" CFLAGS="$OPTFLAGS" CXXFLAGS="$OPTFLAGS" \
%ifarch %{ix86} x86_64 ppc ppc64 %{arm}
  --with-mpicc=%{mpiccpath} \
%endif
  GDB=%{_bindir}/gdb

make %{?_smp_mflags}

# Ensure there are no unexpected file descriptors open,
# the testsuite otherwise fails.
cat > close_fds.c <<EOF
#include <stdlib.h>
#include <unistd.h>
int main (int argc, char *const argv[])
{
  int i, j = sysconf (_SC_OPEN_MAX);
  if (j < 0)
    exit (1);
  for (i = 3; i < j; ++i)
    close (i);
  execvp (argv[1], argv + 1);
  exit (1);
}
EOF
gcc $RPM_OPT_FLAGS -o close_fds close_fds.c

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
mkdir docs/installed
mv %{buildroot}%{_datadir}/doc/valgrind/* docs/installed/
rm -f docs/installed/*.ps

%if "%{valsecarch}" != ""
pushd $RPM_BUILD_ROOT%{_libdir}/valgrind/
rm -f *-%{valsecarch}-* || :
for i in *-%{valarch}-*; do
  j=`echo $i | sed 's/-%{valarch}-/-%{valsecarch}-/'`
%ifarch ppc
  ln -sf ../../lib64/valgrind/$j $j
%else
  ln -sf ../../lib/valgrind/$j $j
%endif
done
popd
%endif

rm -f %{buildroot}%{_libdir}/valgrind/*.supp.in

%ifarch %{ix86} x86_64
# To avoid multilib clashes in between i?86 and x86_64,
# tweak installed <valgrind/config.h> a little bit.
for i in HAVE_PTHREAD_CREATE_GLIBC_2_0 HAVE_PTRACE_GETREGS \
%if 0%{?rhel} == 5
         HAVE_BUILTIN_ATOMIC HAVE_BUILTIN_ATOMIC_CXX \
%endif
         ; do
  sed -i -e 's,^\(#define '$i' 1\|/\* #undef '$i' \*/\)$,#ifdef __x86_64__\n# define '$i' 1\n#endif,' \
    $RPM_BUILD_ROOT%{_includedir}/valgrind/config.h
done
%endif

%if 0%{?do_test}
%check
# Build the test files with the software collection compiler if available.
%{?scl:PATH=%{_bindir}${PATH:+:${PATH}}}
# Make sure no extra CFLAGS leak through, the testsuite sets all flags
# necessary. See also configure above.
make %{?_smp_mflags} CFLAGS="" check || :

# Remove and cleanup fake 32-bit libgcc package created in  %%build.
%ifarch x86_64 ppc64
rm -rf libgcc
%endif

echo ===============TESTING===================
./close_fds make regtest || :
# Make sure test failures show up in build.log
# Gather up the diffs (at most the first 20 lines for each one)
MAX_LINES=20
diff_files=`find . -name '*.diff' | sort`
if [ z"$diff_files" = z ] ; then
   echo "Congratulations, all tests passed!" >> diffs
else
   for i in $diff_files ; do
      echo "=================================================" >> diffs
      echo $i                                                  >> diffs
      echo "=================================================" >> diffs
      if [ `wc -l < $i` -le $MAX_LINES ] ; then
         cat $i                                                >> diffs
      else
         head -n $MAX_LINES $i                                 >> diffs
         echo "<truncated beyond $MAX_LINES lines>"            >> diffs
      fi
   done
fi
cat diffs
echo ===============END TESTING===============
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING NEWS README_*
%doc docs/installed/html docs/installed/*.pdf
%{_bindir}/*
%dir %{_libdir}/valgrind
%{_libdir}/valgrind/*[^ao]
%{_libdir}/valgrind/[^l]*o
%{_mandir}/man1/*

%files devel
%defattr(-,root,root)
%{_includedir}/valgrind
%dir %{_libdir}/valgrind
%{_libdir}/valgrind/*.a
%{_libdir}/pkgconfig/*

%ifarch %{ix86} x86_64 ppc ppc64 %{arm}
%files openmpi
%defattr(-,root,root)
%dir %{_libdir}/valgrind
%{_libdir}/valgrind/libmpiwrap*.so
%endif

%changelog
* Tue Nov 19 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.0-1m)
- update to 3.9.0
- add support for glibc 2.18+
-- import some patches from fedora
- remove old patches

* Tue Oct  1 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.8.1-6m)
- move part of %%install to correct section and revive %%clean

* Tue Oct  1 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.8.1-5m)
- remove epoch to resolve dependency again

* Mon Sep 30 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.8.1-4m)
- add patches from fc

* Mon Sep 23 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.8.1-3m)
- add build fix patches:
	Patch17: valgrind-3.8.1-takeGlibc217.patch
	Patch18: valgrind-3.8.1-ptrace.patch

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.1-2m)
- rebuild against mpich2-1.5

* Sat Oct 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.1-1m)
- update to 3.8.1

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.0-2m)
- rebuild against mpich2-1.5rc1

* Mon Aug 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.0-1m)
- update to 3.8.0

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.0-9m)
- update to the latest snapshot

* Mon Jun 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.0-8m)
- merge from T4R/valgrind
-- update to the latest snapshot

* Sun Mar 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.0-4m)
- update to the latest snapshot

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.0-3m)
- fix some minor bugs

* Thu Jan  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.0-2m)
- update to the latest snapshot
-- some minor bugs have been fixed

* Thu Nov 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.0-1m)
- update to the latest snapshot
- bump version to 3.7.0

* Sun Nov  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-13m)
- update to the latest snapshot

* Sun Oct  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-12m)
- update to the latest snapshot
-- x86_64 support seems to be improved

* Tue Sep  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-11m)
- update to the latest snapshot
-- some 64bit instructions added
- fix release

* Fri Aug 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-10m)
- update to the latest snapshot
-- improve suppression rule
- revise specopt; enable document and manpage generation by default

* Sun Aug 21 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-9m)
- update to the latest snapshot

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-8m)
- merge from TSUPPA4RI/valgrind r54131
-- update to the latest snapshot
-- revise %%check but disabled by default at this time
- delete glibc patch, which is no longer needed

* Tue Jul  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.1-7m)
- rebuild against mpich2-1.4

* Tue Jun 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.1-6m)
- add a patch to enable build with new glibc

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-5m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-4m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-3m)
- rebuild against boost-1.46.0

* Sat Feb 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-2m)
- add Requires: glibc-debuginfo

* Fri Feb 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1,  bug fix release

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-3m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-2m)
- rebuild against boost-1.44.0

* Wed Oct 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0 release version

* Fri Oct 15 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11444.1m)
- update to the latest snapshot

* Fri Sep 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11376.1m)
- update to the latest snapshot

* Thu Sep 16 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11358.1m)
- update to the latest snapshot

* Fri Aug 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11293.1m)
- update to the latest snapshot

* Sun Jul 25 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11227.1m)
- update to the latest snapshot

* Sat Jul 10 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11212.1m)
- update to the latest snapshot

* Sat Jun 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11188.2m)
- update vex r1985

* Fri Jun 25 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11188.1m)
- update to rev 11188

* Sun Jun 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-0.11181.1m)
- update to rev 11181
- change visioning style

* Sun May 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-4m)
- update to rev 11140

* Fri Apr 16 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-3m)
- update to rev 11104
-- see https://bugs.kde.org/show_bug.cgi?id=233565

* Sat Apr  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-2m)
- add manuals again

* Mon Mar 22 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to development snapshot, which includes better glibc-2.11.90 supports
- temporally exclude some manuals because of its compilation failures

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.0-5m)
- modify for sys/stat.h

* Mon Nov 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.0-4m)
- fix compilation failure with glibc-2.11
-- please note that this fix is just a workaround.
  %%{_libdir}/valgrind/default.supp is not updated yet.

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- release %%dir %%{_libdir}/valgrind from package devel
- it's already provided by main package: valgrind and valgrind-devel Requires: valgrind
- License: GPLv2

* Thu Sep  3 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.0-1m)
- update to 3.5.0
- import three patches from fedora

* Mon Mar  2 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1 (bug fix release)
- delete Patch2, which no longer seems to be required.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-2m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-1m)
- [SECURITY] CVE-2008-4865
- update to 3.4.0
- revise spec

* Sat Dec  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-2m)
- add support for glibc 2.9

* Thu Jun  5 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-1m)
- update 3.3.1
-- fixes many bugs
-- adds support for the SSSE3 (Core 2) instruction set
- delete patches

* Thu May  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.0-6m)
- rebuild against openmpi-1.2.6

* Thu Apr 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.0-5m)
- remove epoch to resolve dependency

* Thu Apr 17 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0-4m)
- merge changes from fc-devel (valgrind-3_3_0-3)
- * Wed Apr 16 2008 Jakub Jelinek <jakub@redhat.com> 3.3.0-3
- - add suppressions for glibc 2.8
- - add a bunch of syscall wrappers (#441709)
- * Mon Mar  3 2008 Jakub Jelinek <jakub@redhat.com> 3.3.0-2
- - add _dl_start suppression for ppc/ppc64
- * Mon Mar  3 2008 Jakub Jelinek <jakub@redhat.com> 3.3.0-1
- - update to 3.3.0
- - split off devel bits into valgrind-devel subpackage

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.0-3m)
- %%NoSource -> NoSource

* Sat Jan 12 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0-2m)
- use mpich2-devel instead of mpich-devel

* Sat Dec 15 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0-1m)
- updated to 3.3.0
-- deleted all patches and autoconf hack

* Mon Oct 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-7m)
- updated default.supp for glibc-2.7

* Thu Oct 18 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-6m)
- sync with fc-devel (valgrind-3_2_3-7)
--* Thu Oct 18 2007 Jakub Jelinek <jakub@redhat.com> 3.2.3-7
--- add suppressions for glibc >= 2.7

* Mon Sep  3 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-5m)
- sync with fc-devel (valgrind-3_2_3-6)
--* Fri Aug 31 2007 Jakub Jelinek <jakub@redhat.com> 3.2.3-6
--- handle new x86_64 nops (#256801, KDE#148447)
--- add support for private futexes (KDE#146781)
--* Fri Aug  3 2007 Jakub Jelinek <jakub@redhat.com> 3.2.3-5
--- add ppc64-linux symlink in valgrind ppc.rpm, so that when
---  rpm prefers 32-bit binaries over 64-bit ones 32-bit
--- /usr/bin/valgrind can find 64-bit valgrind helper binaries (#249773)
--- power5-- and power6 support (#240762)

* Wed Jul  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-4m)
- move %%check place for configure option %%do_test

* Sat Jun 30 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-3m)
- sync with fc-devel (valgrind-3_2_3-4)
-- add valgrind-3.2.3-io_destroy.patch

* Wed May 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-2m)
- add workaround support for glibc-2.6

* Wed Feb 14 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-1m)
- sync with fc-devel (valgrind-3_2_3-2)	
--* Tue Feb 13 2007 Jakub Jelinek <jakub@redhat.com> 3.2.3-2
--- fix valgrind.pc again
--* Tue Feb 13 2007 Jakub Jelinek <jakub@redhat.com> 3.2.3-1
--- update to 3.2.3

* Fri Nov 10 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-4m)
- sync with fc-devel (valgrind-3.2.1-7)
-* Wed Nov  8 2006 Jakub Jelinek <jakub@redhat.com> 3.2.1-7
-- some cachegrind improvements (Ulrich Drepper)

* Wed Nov  8 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-3m)
- sync with fc-devel (valgrind-3.2.1-6)
-* Mon Nov  6 2006 Jakub Jelinek <jakub@redhat.com> 3.2.1-6
-- fix valgrind.pc (#213149)
-- handle Intel Core2 cache sizes in cachegrind (Ulrich Drepper)

* Thu Oct 26 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- sync with fc-devel (valgrind-3.2.1-5)
-* Wed Oct 25 2006 Jakub Jelinek <jakub@redhat.com> 3.2.1-5
-- fix valgrind on ppc/ppc64 where PAGESIZE is 64K (#211598)

* Sat Oct 14 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Sun Jul 16 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.0-2m)
- enable x86_64

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (3.1.0-1m)
- update to 3.1.0

* Sat Nov  6 2004 TAKAHASHI Tamotsu <tamo>
- (2.2.0-1m)
- new stable version

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.0-2m)
- revised spec for rpm 4.2.

* Fri Dec 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.0-1m)
- initial import to Momonga
