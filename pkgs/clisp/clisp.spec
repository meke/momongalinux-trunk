%global momorel 5
Name:		clisp
Summary:	ANSI Common Lisp implementation
Version:	2.49
Release: %{momorel}m%{?dist}

Group:		Development/Languages
License:	GPLv2
URL:		http://www.clisp.org/
Source0:	http://downloads.sourceforge.net/clisp/clisp-%{version}.tar.bz2
NoSource:	0
# Adapt to libsvm 3.1.  Applied upstream.
Patch0:		clisp-libsvm.patch
# Fix an illegal C construct that allows GCC 4.7 to produce bad code.  Applied
# upstream.
Patch1:		clisp-hostname.patch
# Fix old ARM assembly that is invalid for newer platforms.  Sent upstream.
Patch2:		clisp-arm.patch
# Linux-specific fixes.  Sent upstream 25 Jul 2012.
Patch3:		clisp-linux.patch
BuildRequires:	compat-readline5-devel
BuildRequires:	dbus-devel
BuildRequires:	libfcgi-devel
BuildRequires:	ffcall-devel
BuildRequires:	libffi-devel
BuildRequires:	gdbm-devel
BuildRequires:	gettext-devel
BuildRequires:	ghostscript
BuildRequires:	groff
BuildRequires:	gtk2-devel
BuildRequires:	gzip
BuildRequires:	libICE-devel
BuildRequires:	libSM-devel
BuildRequires:	libX11-devel
BuildRequires:	libXaw-devel
BuildRequires:	libXext-devel
BuildRequires:	libXft-devel
BuildRequires:	libXmu-devel
BuildRequires:	libXrender-devel
BuildRequires:	libXt-devel
BuildRequires:	db4-devel
BuildRequires:	libglade2-devel
BuildRequires:	libsigsegv-devel
BuildRequires:	libsvm-devel
#BuildRequires:	pari-devel
BuildRequires:	pcre-devel
BuildRequires:	postgresql-devel
BuildRequires:	zlib-devel

# See Red Hat bug #238954
ExcludeArch:	ppc64

# clisp contains a copy of gnulib, which has been granted a bundling exception:
# https://fedoraproject.org/wiki/Packaging:No_Bundled_Libraries#Packages_granted_exceptions
Provides:	bundled(gnulib)

%description
ANSI Common Lisp is a high-level, general-purpose programming
language.  GNU CLISP is a Common Lisp implementation by Bruno Haible
of Karlsruhe University and Michael Stoll of Munich University, both
in Germany.  It mostly supports the Lisp described in the ANSI Common
Lisp standard.  It runs on most Unix workstations (GNU/Linux, FreeBSD,
NetBSD, OpenBSD, Solaris, Tru64, HP-UX, BeOS, NeXTstep, IRIX, AIX and
others) and on other systems (Windows NT/2000/XP, Windows 95/98/ME)
and needs only 4 MiB of RAM.

It is Free Software and may be distributed under the terms of GNU GPL,
while it is possible to distribute commercial proprietary applications
compiled with GNU CLISP.

The user interface comes in English, German, French, Spanish, Dutch,
Russian and Danish, and can be changed at run time.  GNU CLISP
includes an interpreter, a compiler, a debugger, CLOS, MOP, a foreign
language interface, sockets, i18n, fast bignums and more.  An X11
interface is available through CLX, Garnet, CLUE/CLIO.  GNU CLISP runs
Maxima, ACL2 and many other Common Lisp packages.


%package devel
Summary:	Development files for CLISP
Group:		Development/Languages
Provides:	%{name}-static = %{version}-%{release} 
Requires:	%{name}%{?_isa} = %{version}-%{release}, automake

%description devel
Files necessary for linking CLISP programs.


%prep
%setup -q
%patch0
%patch1
%patch2
%patch3

# Convince CLisp to build against compat-readline5 instead of readline.
# This is to avoid pulling the GPLv3 readline 6 into a GPLv2 CLisp binary.
# See Red Hat bug #511303.
mkdir -p readline/include
ln -s %{_includedir}/readline5/readline readline/include/readline
ln -s %{_libdir}/readline5 readline/%{_lib}

# Change URLs not affected by the --hyperspec argument to configure
sed -i.orig 's|lisp.org/HyperSpec/Body/chap-7.html|lispworks.com/documentation/HyperSpec/Body/07_.htm|' \
    src/clos-package.lisp
touch -r src/clos-package.lisp.orig src/clos-package.lisp
rm -f src/clos-package.lisp.orig
for f in src/_README.*; do
  sed -i.orig 's|lisp.org/HyperSpec/FrontMatter|lispworks.com/documentation/HyperSpec/Front|' $f
  touch -r ${f}.orig $f
  rm -f ${f}.orig
done

# We only link against libraries in system directories, so we need -L dir in
# place of -Wl,-rpath -Wl,dir
cp -p src/build-aux/config.rpath config.rpath.orig
sed -i -e 's/${wl}-rpath ${wl}/-L/g' src/build-aux/config.rpath

# Enable firefox to be the default browser for displaying documentation
sed -i 's/;; \((setq \*browser\* .*)\)/\1/' src/cfgunix.lisp

%build
ulimit -s unlimited

# Do not need to specify base modules: i18n, readline, regexp, syscalls
# The dirkey module currently can only be built on Windows/Cygwin/MinGW
./configure --prefix=%{_prefix} \
	    --libdir=%{_libdir} \
	    --mandir=%{_mandir} \
	    --docdir=%{_docdir}/clisp-%{version} \
	    --fsstnd=redhat \
	    --hyperspec=http://www.lispworks.com/documentation/HyperSpec/ \
	    --with-module=berkeley-db \
	    --with-module=bindings/glibc \
	    --with-module=clx/new-clx \
	    --with-module=dbus \
	    --with-module=fastcgi \
	    --with-module=gdbm \
	    --with-module=gtk2 \
	    --with-module=libsvm \
	    --with-module=pcre \
	    --with-module=postgresql \
	    --with-module=rawsock \
	    --with-module=wildcard \
	    --with-module=zlib \
	    --with-libreadline-prefix=`pwd`/readline \
	    --cbc \
	    build \
%ifarch ppc ppc64
	    CFLAGS="${RPM_OPT_FLAGS} -DNO_GENERATIONAL_GC -DNO_MULTIMAP_FILE -DNO_SINGLEMAP -I/usr/include/readline5 -I/usr/include/libsvm -I/usr/include/db4 -Wa,--noexecstack -L%{_libdir}/readline5" \
%else
	    CFLAGS="${RPM_OPT_FLAGS} -I/usr/include/readline5 -I/usr/include/libsvm -I/usr/include/db4 -Wa,--noexecstack -L%{_libdir}/readline5" \
%endif
	    LDFLAGS="-L%{_libdir}/readline5 -Wl,-z,noexecstack"

%install
make -C build DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_docdir}/clisp-%{version}/doc/clisp.{dvi,1,ps}
cp -p doc/mop-spec.pdf %{buildroot}%{_docdir}/clisp-%{version}/doc
cp -p doc/*.png %{buildroot}%{_docdir}/clisp-%{version}/doc
cp -p doc/Why-CLISP* %{buildroot}%{_docdir}/clisp-%{version}/doc
cp -p doc/regexp.html %{buildroot}%{_docdir}/clisp-%{version}/doc
find %{buildroot}%{_libdir} -name '*.dvi' | xargs rm -f
%find_lang %{name}
%find_lang %{name}low
cat %{name}low.lang >> %{name}.lang

# Put back the original config.rpath, and fix executable bits
cp -p config.rpath.orig %{buildroot}/%{_libdir}/clisp-%{version}/build-aux/config.rpath
chmod a+x \
  %{buildroot}/%{_libdir}/clisp-%{version}/build-aux/config.guess \
  %{buildroot}/%{_libdir}/clisp-%{version}/build-aux/config.sub \
  %{buildroot}/%{_libdir}/clisp-%{version}/build-aux/depcomp

%files -f %{name}.lang
%{_bindir}/clisp
%{_mandir}/man1/clisp.1*
%{_docdir}/clisp-%{version}
%dir %{_libdir}/clisp-%{version}/
%dir %{_libdir}/clisp-%{version}/base/
%{_libdir}/clisp-%{version}/base/lispinit.mem
%{_libdir}/clisp-%{version}/base/lisp.run
%dir %{_libdir}/clisp-%{version}/berkeley-db/
%{_libdir}/clisp-%{version}/berkeley-db/*.fas
%dir %{_libdir}/clisp-%{version}/bindings/
%dir %{_libdir}/clisp-%{version}/bindings/glibc/
%{_libdir}/clisp-%{version}/bindings/glibc/*.fas
%dir %{_libdir}/clisp-%{version}/clx/
%dir %{_libdir}/clisp-%{version}/clx/new-clx/
%{_libdir}/clisp-%{version}/clx/new-clx/*.fas
%{_libdir}/clisp-%{version}/data/
%dir %{_libdir}/clisp-%{version}/dbus/
%{_libdir}/clisp-%{version}/dbus/*.fas
%{_libdir}/clisp-%{version}/dynmod/
%dir %{_libdir}/clisp-%{version}/fastcgi/
%{_libdir}/clisp-%{version}/fastcgi/*.fas
%dir %{_libdir}/clisp-%{version}/gdbm/
%{_libdir}/clisp-%{version}/gdbm/*.fas
%dir %{_libdir}/clisp-%{version}/gtk2/
%{_libdir}/clisp-%{version}/gtk2/*.fas
%dir %{_libdir}/clisp-%{version}/libsvm/
%{_libdir}/clisp-%{version}/libsvm/*.fas
#%%dir %%{_libdir}/clisp-%%{version}/pari/
#%%{_libdir}/clisp-%%{version}/pari/*.fas
%dir %{_libdir}/clisp-%{version}/pcre/
%{_libdir}/clisp-%{version}/pcre/*.fas
%dir %{_libdir}/clisp-%{version}/postgresql/
%{_libdir}/clisp-%{version}/postgresql/*.fas
%dir %{_libdir}/clisp-%{version}/rawsock/
%{_libdir}/clisp-%{version}/rawsock/*.fas
%dir %{_libdir}/clisp-%{version}/wildcard/
%{_libdir}/clisp-%{version}/wildcard/*.fas
%dir %{_libdir}/clisp-%{version}/zlib/
%{_libdir}/clisp-%{version}/zlib/*.fas
%{_datadir}/emacs/site-lisp/*
%{_datadir}/vim/vimfiles/after/syntax/*

%files devel
%{_bindir}/clisp-link
%{_mandir}/man1/clisp-link.1*
%{_libdir}/clisp-%{version}/base/*.a
%{_libdir}/clisp-%{version}/base/*.o
%{_libdir}/clisp-%{version}/base/*.h
%{_libdir}/clisp-%{version}/base/makevars
%{_libdir}/clisp-%{version}/berkeley-db/Makefile
%{_libdir}/clisp-%{version}/berkeley-db/*.lisp
%{_libdir}/clisp-%{version}/berkeley-db/*.o
%{_libdir}/clisp-%{version}/berkeley-db/*.sh
%{_libdir}/clisp-%{version}/bindings/glibc/Makefile
%{_libdir}/clisp-%{version}/bindings/glibc/*.lisp
%{_libdir}/clisp-%{version}/bindings/glibc/*.o
%{_libdir}/clisp-%{version}/bindings/glibc/*.sh
%{_libdir}/clisp-%{version}/build-aux/
%{_libdir}/clisp-%{version}/clx/new-clx/demos/
%{_libdir}/clisp-%{version}/clx/new-clx/README
%{_libdir}/clisp-%{version}/clx/new-clx/Makefile
%{_libdir}/clisp-%{version}/clx/new-clx/*.lisp
%{_libdir}/clisp-%{version}/clx/new-clx/*.o
%{_libdir}/clisp-%{version}/clx/new-clx/*.sh
%{_libdir}/clisp-%{version}/dbus/Makefile
%{_libdir}/clisp-%{version}/dbus/*.lisp
%{_libdir}/clisp-%{version}/dbus/*.o
%{_libdir}/clisp-%{version}/dbus/*.sh
%{_libdir}/clisp-%{version}/fastcgi/README
%{_libdir}/clisp-%{version}/fastcgi/Makefile
%{_libdir}/clisp-%{version}/fastcgi/*.lisp
%{_libdir}/clisp-%{version}/fastcgi/*.o
%{_libdir}/clisp-%{version}/fastcgi/*.sh
%{_libdir}/clisp-%{version}/gdbm/Makefile
%{_libdir}/clisp-%{version}/gdbm/*.lisp
%{_libdir}/clisp-%{version}/gdbm/*.o
%{_libdir}/clisp-%{version}/gdbm/*.sh
%{_libdir}/clisp-%{version}/gtk2/Makefile
%{_libdir}/clisp-%{version}/gtk2/*.cfg
%{_libdir}/clisp-%{version}/gtk2/*.glade
%{_libdir}/clisp-%{version}/gtk2/*.lisp
%{_libdir}/clisp-%{version}/gtk2/*.o
%{_libdir}/clisp-%{version}/gtk2/*.sh
%{_libdir}/clisp-%{version}/libsvm/Makefile
%{_libdir}/clisp-%{version}/libsvm/*.lisp
%{_libdir}/clisp-%{version}/libsvm/*.o
%{_libdir}/clisp-%{version}/libsvm/*.sh
%{_libdir}/clisp-%{version}/linkkit/
#%%{_libdir}/clisp-%%{version}/pari/README
#%%{_libdir}/clisp-%%{version}/pari/Makefile
#%%{_libdir}/clisp-%%{version}/pari/*.lisp
#%%{_libdir}/clisp-%%{version}/pari/*.o
#%%{_libdir}/clisp-%%{version}/pari/*.sh
%{_libdir}/clisp-%{version}/pcre/Makefile
%{_libdir}/clisp-%{version}/pcre/*.lisp
%{_libdir}/clisp-%{version}/pcre/*.o
%{_libdir}/clisp-%{version}/pcre/*.sh
%{_libdir}/clisp-%{version}/postgresql/README
%{_libdir}/clisp-%{version}/postgresql/Makefile
%{_libdir}/clisp-%{version}/postgresql/*.lisp
%{_libdir}/clisp-%{version}/postgresql/*.o
%{_libdir}/clisp-%{version}/postgresql/*.sh
%{_libdir}/clisp-%{version}/rawsock/demos/
%{_libdir}/clisp-%{version}/rawsock/Makefile
%{_libdir}/clisp-%{version}/rawsock/*.lisp
%{_libdir}/clisp-%{version}/rawsock/*.o
%{_libdir}/clisp-%{version}/rawsock/*.sh
%{_libdir}/clisp-%{version}/wildcard/README
%{_libdir}/clisp-%{version}/wildcard/Makefile
%{_libdir}/clisp-%{version}/wildcard/*.a
%{_libdir}/clisp-%{version}/wildcard/*.lisp
%{_libdir}/clisp-%{version}/wildcard/*.o
%{_libdir}/clisp-%{version}/wildcard/*.sh
%{_libdir}/clisp-%{version}/zlib/Makefile
%{_libdir}/clisp-%{version}/zlib/*.lisp
%{_libdir}/clisp-%{version}/zlib/*.o
%{_libdir}/clisp-%{version}/zlib/*.sh
%{_datadir}/aclocal/clisp.m4


%changelog
* Mon Sep  3 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.49-5m)
- import glibc patch from fedora

* Sun Aug  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.49-4m)
- add BuildRequires

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.49-3m)
- reimport from fedora

* Wed Jan 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.49-2m)
- add BuildRequires

* Sun Aug 21 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.49-1m)
- update to 2.49
- merge from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.48-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.48-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.48-8m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.48-7m)
- revert to 2.48
-- stumpwm does not work

* Thu Jul 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.49-1m)
- update 2.49

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.48-6m)
- rebuild against readline6

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.48-5m)
- rebuild against libsigsegv-2.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.48-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.48-3m)
- disable make check because one test failure occurs during ffi test
  on i686

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.48-2m)
- enable some modules
- remove doc
- remove %%check section

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.48-1m)
- update to 2.48

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.47-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.47-2m)
- rebuild against rpm-4.6

* Wed Oct 29 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.47-1m)
- update to 2.47
- add some BuildRequires
- set specopt

* Sat Jul  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.46-2m)
- disable make check

* Sat Jul  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.46-1m)
- update 2.46

* Thu May 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.45-1m)
- update 2.45

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.44.1-1m)
- update 2.44.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.43-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.43-3m)
- %%NoSource -> NoSource

* Fri Feb  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.43-2m)
- add BuildRequires: ghostscript for /usr/bin/dvipdf
- add workaround for gcc43

* Thu Nov 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.43-1m)
- update to 2.43
- License: GPLv2

* Fri Nov  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.42-1m)
- update to 2.42

* Tue Feb 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.41a-1m)
- update to 2.41a

* Sat Aug 19 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.39-1m)
- update to 2.39
- revise %%description, %%doc, configure options

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.38-1m)
- update to 2.38
- add BuildRequires: libsigsegv-devel >= 2.2

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.35-2m)
- rebuild against readline-5.0

* Fri Sep 23 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.35-1m)
- update 2.35

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.33.2-2m)
- enable x86_64.

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.33.2-1m)
- update to 2.33.2
- use %%NoSource

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.33-1m)
- version 2.33

* Tue Apr  6 2004 Toru Hoshina <t@momonga-linux.org>
- (2.31-3m)
- force LANG=C.

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.31-2m)
- revised spec for enabling rpm 4.2.

* Sun Oct 26 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.31-1m)
- update to 2.31

* Sat Nov  9 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (2.30-1m)
- initial import.
