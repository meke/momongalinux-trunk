%global momorel 1
%global m17n_db_ver 1.6.4
%global libotf_ver 0.9.13
%global anthy_ver 9100

Summary: The m17n library is a multilingual text processing library
Name: m17n-lib
Version: 1.6.4
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.nongnu.org/m17n/
Source0: http://ftp.twaren.net/Unix/NonGNU//m17n/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-1.5.3-fix.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libotf >= %{libotf_ver}
Requires: m17n-db >= %{m17n_db_ver}
BuildRequires: anthy-devel >= %{anthy_ver}
BuildRequires: freetype-devel
BuildRequires: fribidi-devel
BuildRequires: gd-devel
BuildRequires: gettext-devel
BuildRequires: libX11-devel
BuildRequires: libjpeg-devel
BuildRequires: libotf-devel >= %{libotf_ver}
BuildRequires: libxml2-devel
BuildRequires: m17n-db-devel >= %{m17n_db_ver}
BuildRequires: pkgconfig

%description
The m17n library is a multilingual text processing library for the C
language.

%package devel
Summary: Headers of m17n for development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Headers of m17n-lib for development.

%prep
%setup -q

%patch0 -p1 -b .fix~ 

%build
%configure
# make -j8 fail (m17n-lib-1.5.3-1m)
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/m17n-conv
%{_bindir}/m17n-date
%{_bindir}/m17n-dump
%{_bindir}/m17n-edit
%{_bindir}/m17n-view
%dir %{_libdir}/m17n
%dir %{_libdir}/m17n/1.0
%{_libdir}/m17n/1.0/libm17n-*.la
%{_libdir}/m17n/1.0/libm17n-*.so
%{_libdir}/m17n/1.0/libmimx-*.la
%{_libdir}/m17n/1.0/libmimx-*.so
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/m17n-config
%{_includedir}/m17n*.h
%{_libdir}/m17n/1.0/libm17n-*.a
%{_libdir}/m17n/1.0/libmimx-*.a
%{_libdir}/pkgconfig/m17n-*.pc
%{_libdir}/lib*.a
%{_libdir}/lib*.so

%changelog
* Tue Jul 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.4-1m)
- version 1.6.4

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3-1m)
- update 1.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-2m)
- full rebuild for mo7 release

* Sun May  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-1m)
- version 1.6.1

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-1m)
- version 1.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.5-1m)
- version 1.5.5

* Wed Jun 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.4-3m)
- No NoSource

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.4-2m)
- %%global libotf_ver 0.9.9

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.4-1m)
- version 1.5.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-1m)
- version 1.5.3
- disable parallel build, make -j8 fail
- update compilation fix.patch

* Sat Jul 12 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-2m)
- add patch to fix a compilation error

* Wed Jun 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.2-1m)
- version 1.5.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1-2m)
- rebuild against gcc43

* Fri Feb  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-1m)
- version 1.5.1
- remove added internal-flt.h

* Sat Dec 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-1m)
- version 1.5.0
- import missing internal-flt.h
- remove deps.patch

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-1m)
- version 1.4.0

* Mon Apr  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-1m)
- import from cooker, we don't need Fedora style

* Wed Jan 10 2007 Thierry Vignaud <tvignaud@mandriva.com> 1.3.4-1mdv2007.0
+ Revision: 106863
- add buildrequires on gettext-devel for `AM_GNU_GETTEXT
- new release
- latest snapshot (UTUMI Hirosi <utuhiro78@yahoo.co.jp>)
- Import m17n-lib

* Tue Aug 15 2006 Christiaan Welvaart <cjw@daneel.dyndns.org> 1.3.3-3.20060803.2
- rebuild for fixed libxaw soname

* Thu Aug 03 2006 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.3-3.20060803.1mdv2007.0
- latest snapshot
- (better Thai support)
- (http://sourceforge.net/mailarchive/forum.php?thread_id=26116958&forum_id=43684)
- update patch0 for latest m17n-lib

* Sun Jun 25 2006 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.3-2.20060625.1mdv2007.0
- latest snapshot

* Sat Feb 25 2006 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.3-1mdk
- new release
- move lib*.so to devel

* Thu Jan 19 2006 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.1-1mdk
- new release

* Fri Dec 23 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.3.0-1mdk
- new release

* Wed Nov 16 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.2.0-7.20051116.1mdk
- latest snapshot
- update patch0

* Tue Aug 09 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.2.0-6.20050809.1mdk
- latest snapshot
- remove patch1 (merged upstream)

* Mon May 09 2005 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.2.0-5.20050425.1mdk
- latest snapshot (UTUMI Hirosi <utuhiro78@yahoo.co.jp>)
- patch 1: fix compilation with gcc-4.0.0 (Kenichi Handa)

* Fri Mar 18 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.2.0-4mdk
- add "-a" to automake-1.8
- add symlink to ltmain.sh
- add autoheader-2.5x
- spec cleanup
- remove find_lang (it doesn't have po files)
- add some symlinks to libdir since SCIM/UIM open them (#14709)

* Wed Feb 16 2005 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.2.0-3mdk
- multiarch and move m17n-config to -devel package
- hmmm, forgot a change from older tree, aka nuke explicit libfribidi0 dep

* Tue Feb 15 2005 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.2.0-2mdk
- nuke lib64 rpaths (again)
- fix parallel build (again)

* Tue Dec 28 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.2.0-1mdk
- new release

* Fri Nov 26 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.1.0-4.cvs20041126.2mdk
- latest snapshot
- add BuildRequires: libotf-devel (for OpenType font support)
- add BuildRequires: libfribidi-devel (for Arabic/Hebrew support)

* Thu Oct 21 2004 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.1.0-3mdk
- fix parallel build
- requires: fonts-ttf-freefont for medit, mdump

* Wed Aug 18 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.1.0-2mdk
- add BuildRequires:	libanthy-devel 

* Tue Aug 17 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.1.0-1mdk
- new release
- only run bootstrap if needed

* Wed Jul 28 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.0.2-7.cvs20040714.3mdk
- remove requires on devel packages

* Fri Jul 23 2004 Christiaan Welvaart <cjw@daneel.dyndns.org> 1.0.2-7.cvs20040714.2mdk
- add BuildRequires: libjpeg-static-devel

* Tue Jul 20 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.0.2-7.cvs20040714.1mdk
- fix buildrequires
- cvs 20040714 (UTUMI Hirosi <utuhiro78@yahoo.co.jp>)

* Thu Jul 15 2004 Christiaan Welvaart <cjw@daneel.dyndns.org> 1.0.2-7.cvs20040619.2mdk
- add BuildRequires: freetype2-static-devel

* Sat Jun 19 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.0.2-7.cvs20040619.1mdk
- cvs 20040619
- remove patch0 (merged)

* Tue Jun 15 2004 Per Oyvind Karlsen <peroyvind@linux-mandrake.com> 1.0.2-6mdk
- catch another buildrequires

* Mon Jun 14 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 1.0.2-5mdk
- add patch0 (made by James Su <suzhe@tsinghua.org.cn>) for scim-m17n 

* Tue Jun 08 2004 Per Oyvind Karlsen <peroyvind@linux-mandrake.com> 1.0.2-4mdk
- really fix buildrequires (I suck, I suck, I suck, I suck, I suck, I suck, I suck)

* Tue Jun 08 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.0.2-3mdk
- move plugins' *.so into main lib package since uim opens them through
  dlopen()

* Tue Jun 08 2004 Per Oyvind Karlsen <peroyvind@linux-mandrake.com> 1.0.2-2mdk
- fix buildrequires
- proper use of %%mklibname

* Thu May 27 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 1.0.2-1mdk
- initial release (based on UTUMI Hirosi <utuhiro78@yahoo.co.jp> 's work)
