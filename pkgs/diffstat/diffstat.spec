%global         momorel 1

Summary:        A utility which provides statistics based on the output of diff.
Name:           diffstat
Version:        1.58
Release:        %{momorel}m%{?dist}
Group:          Development/Tools
License:        see "diffstat.1*"
URL:            http://dickey.his.com/diffstat/diffstat.html
Source0:        ftp://invisible-island.net/pub/%{name}/%{name}-%{version}.tgz 
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The diff command compares files line by line.  Diffstat reads the
output of the diff command and displays a histogram of the insertions,
deletions and modifications in each file.  Diffstat is commonly used
to provide a summary of the changes in large, complex patch files.

Install diffstat if you need a program which provides a summary of the
diff command's output.  You'll need to also install diffutils.

%prep
%setup -q

%build
%configure
make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README CHANGES
%{_bindir}/diffstat
%{_mandir}/*/*

%changelog
* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.58-1m)
- update to 1.58

* Sat May  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.57-1m)
- update to 1.57

* Tue Feb 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.56-1m)
- update to 1.56

* Fri Oct 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.55-1m)
- update to 1.55

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.54-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.54-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-1m)
- update to 1.54

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.53-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.53-1m)
- update to 1.53

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.49-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.49-1m)
- update to 1.49

* Thu May  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.47-1m)
- update to 1.47

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.45-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.45-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-2m)
- %%NoSource -> NoSource

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-1m)
- update to 1.45

* Sun Jul 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.43-1m)
- update to 1.43

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.42-1m)
- update to 1.42

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.41-1m)
- update to 1.41

* Fri Nov 21 2003 zunda <zunda at freeshell.org>
- (1.33-2m)
- corrected path to the man page
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Mar  4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Thu Feb  7 2002 Kazuhiko <kazuhiko@kondara.org>
- revise URL

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (1.28-2k)
- version up.

* Sun Oct 29 2000 Toru Hoshina <toru@df-usa.com>
- to be Kondarized.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 06 2000 Than Ngo <than@redhat.de>
- use rpm macros

* Wed May 31 2000 Ngo Than <than@redhat.de>
- rebuild for 7.0
- put man page in /usr/share/man/*
- use %configure
- fix makefile.in
- cleanup specfile

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- gzip man page.

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- upgrade to 1.27, add URL tag.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 7)

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
