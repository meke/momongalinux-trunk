%global momorel 1
%global anjutaver 3.10.2
%global anjutarel 1m

Summary: anjuta-plugins
Name: anjuta-extras
Version: 3.10.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Tools
URL: http://blogs.gnome.org/anjuta/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.10/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: anjuta-devel >= %{anjutaver}-%{anjutarel}
BuildRequires: libgnomecanvas-devel
BuildRequires: glib2-devel
BuildRequires: graphviz-devel
BuildRequires: libxml2-devel
BuildRequires: binutils-devel

Requires: anjuta

%description
anjuta-plugins

%prep
%setup -q

%build
%configure \
    --disable-schemas-install \
    --enable-static=no
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/anjuta/libanjuta-editor.*
%{_libdir}/anjuta/libanjuta-sample.*
%{_libdir}/anjuta/libanjuta-scratchbox.*

%{_libdir}/anjuta/anjuta-editor.plugin
%{_libdir}/anjuta/anjuta-sample.plugin
%{_libdir}/anjuta/anjuta-scratchbox.plugin

%{_datadir}/anjuta/glade/*
%{_datadir}/anjuta/properties/*
%{_datadir}/anjuta/ui/*
%{_datadir}/pixmaps/anjuta/*

%{_datadir}/glib-2.0/schemas/org.gnome.anjuta.plugins.scintilla.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.anjuta.plugins.scratchbox.gschema.xml
%{_datadir}/help/*
%{_datadir}/locale/*/*/*

%changelog
* Sun Apr 13 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.10.0-1m)
- updte to 3.10.0

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.6.0-3m)
- rebuild against graphviz-2.36.0-1m

* Sun Nov 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.1-2m)
- rebuild against libgdl-3.6.0

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-1m)
- update to 3.4.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-2m)
- rebuild for glib 2.33.2

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0.0-4m)
- delete "BuildRequires: binutils-static"

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0.0-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0.0-1m)
- update to 2.32.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1.0-4m)
- full rebuild for mo7 release

* Mon May  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1.0-3m)
- add BuildRequires

* Sat May  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1.0-2m)
- update BuildRequires

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1.0-1m)
- update to 2.30.1.0

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.29.2.0-1m)
- update to 2.29.2.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- initial build
