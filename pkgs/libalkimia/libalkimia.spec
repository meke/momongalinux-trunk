%global         momorel 1
%global         kdever 4.10.0
%global         content_id 137323

Name:           libalkimia
Summary:        Financial library
Group:          System Environment/Libraries
Version:        4.3.2
Release:        %{momorel}m%{?dist}
License:        LGPLv2+
URL:            http://kde-apps.org/content/show.php/libalkimia?content=137323
Source0:        http://kde-apps.org/CONTENT/content-files/%{content_id}-%{name}-%{version}.tar.bz2
NoSource:       0
BuildRequires:  gmp-devel
BuildRequires:  kdelibs-devel >= %{kdever}

%description
%{summary}

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       kdelibs-devel >= %{kdever}

%description devel
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%check
export PKG_CONFIG_PATH=%{buildroot}%{_datadir}/pkgconfig:%{buildroot}%{_libdir}/pkgconfig
test "$(pkg-config --modversion libalkimia)" = "%{version}"

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%{_kde4_libdir}/libalkimia.so.4*

%files devel
%{_kde4_includedir}/alkimia/
%{_kde4_libdir}/libalkimia.so
%{_kde4_libdir}/pkgconfig/libalkimia.pc
%{_kde4_appsdir}/cmake/modules/FindLibAlkimia.cmake

%changelog
* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to 4.3.2

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- import from Fedora devel

* Wed Nov 02 2011 Rex Dieter <rdieter@fedoraproject.org> 4.3.1-4
- rebuild (gmp)

* Mon Aug 22 2011 Rex Dieter <rdieter@fedoraproject.org> 4.3.1-3
- .spec cosmetics

* Sat Aug 20 2011 Rex Dieter <rdieter@fedoraproject.org> 4.3.1-2
- BR: gmp-devel
- %%check : don't ignore errors

* Sat Aug 06 2011 Rex Dieter <rdieter@fedoraproject.org> 4.3.1-1
- 4.3.1

* Tue Jun 21 2011 Rex Dieter <rdieter@fedoraproject.org> 4.3.0-1
- first try


