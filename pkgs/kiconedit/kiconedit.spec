%global momorel 7
%global qtver 4.8.2
%global kdever 4.9.0
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global sourcedir stable/extragear

Name:           kiconedit
Version:        4.4.0
Release:        %{momorel}m%{?dist}
Summary:        An icon editor
Group:          Applications/Publishing
License:        GPLv2+
URL:            http://www.kde.org
Source0:	ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:         %{name}-%{version}-docbook.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kdelibs-devel >= %{version}
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
Requires:       kdelibs >= %{version}
Requires:       oxygen-icon-theme
Requires(post): xdg-utils
Requires(postun): xdg-utils

Conflicts: kdegraphics <= 4

%description
KIconEdit is designed to help create icons for 
KDE using the standard icon palette.

%prep
%setup -q

%patch0 -p1 -b .v4.2

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
mkdir %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# validate desktop file
desktop-file-install --vendor "" \
    --dir %{buildroot}%{_datadir}/applications/kde4 --remove-key=Path \
    %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
xdg-icon-resource forceupdate --theme hicolor 2> /dev/null || :

%postun
xdg-icon-resource forceupdate --theme hicolor 2> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
# FIXME/TODO: Ping upstream to include COPYING
%doc AUTHORS NEWS
%{_kde4_bindir}/kiconedit
%{_kde4_appsdir}/kiconedit/
%{_kde4_datadir}/applications/kde4/kiconedit.desktop
%{_kde4_docdir}/HTML/*/kiconedit/
%{_kde4_iconsdir}/hicolor/*/*/kiconedit.png

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-7m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.0-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.0-3m)
- fix build with new kdelibs

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-2m)
- rebuild against qt-4.6.3-1m

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to 4.4.0

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- update to 4.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-3m)
- NoSource again

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-2m)
- no NoSource for STABLE_6

* Fri Sep  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to 4.3.0

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to 4.2.4

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to 4.2.3

* Wed Apr 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2

* Sun Feb  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to 4.1.96

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-1m)
- update to 4.1.3

* Sat Aug  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to 4.1.0

* Fri Jul 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to 4.0.98

* Sun Jun  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to 4.0.80

* Sun May 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- import from Fedora devel

* Thu Apr 03 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.3-2
- rebuild (again) for the fixed %%{_kde4_buildtype}

* Mon Mar 31 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.3-1
- update to 4.0.3
- rebuild for NDEBUG and _kde4_libexecdir

* Tue Mar 04 2008 Sebastian Vahl <fedora@deadbabylon.de> 4.0.2-1
- new upstream version: 4.0.2

* Thu Feb 14 2008 Sebastian Vahl <fedora@deadbabylon.de> 4.0.1-2
- remove reference to KDE 4 in summary

* Fri Feb 08 2008 Sebastian Vahl <fedora@deadbabylon.de> 4.0.1-1
- new upstream version: 4.0.1

* Fri Jan 25 2008 Sebastian Vahl <fedora@deadbabylon.de> 4.0.0-1
- Initial version of kde-4.0.0 version
