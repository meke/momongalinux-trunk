%global momorel 1

%define ruby_sitearch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")
%define ruby_inc %(pkg-config --cflags ruby-1.9)
%define libsepolver 2.2
%define rubyver 1.9.2
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: SELinux library and simple utilities
Name: libselinux
Version: 2.3
Release: %{momorel}m%{?dist}
License: Public Domain
Group: System Environment/Libraries
#Source0: http://www.nsa.gov/research/selinux/%{name}-%{version}.tgz
Source0: http://userspace.selinuxproject.org/releases/20140506/%{name}-%{version}.tar.gz
NoSource: 0
#Source0: %{name}-%{version}.tgz
Source1: selinuxconlist.8
Source2: selinuxdefcon.8
Url: http://oss.tresys.com/git/selinux.git
Patch0: libselinux-rhat.patch
Patch1: libselinux-2.1.13-ruby19.patch

BuildRequires: ruby-devel ruby libsepol-static >= %{libsepolver} swig
BuildRequires: python-devel >= 2.7 python3-devel >= 3.4
BuildRequires: ruby-devel  >= %{rubyver} ruby >= %{rubyver}
Requires: libsepol >= %{libsepolver}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Security-enhanced Linux is a feature of the Linux(R) kernel and a number
of utilities with enhanced security functionality designed to add
mandatory access controls to Linux.  The Security-enhanced Linux
kernel contains new architectural components originally developed to
improve the security of the Flask operating system. These
architectural components provide general support for the enforcement
of many kinds of mandatory access control policies, including those
based on the concepts of Type Enforcement(R), Role-based Access
Control, and Multi-level Security.

libselinux provides an API for SELinux applications to get and set
process and file security contexts and to obtain security policy
decisions.  Required for any applications that use the SELinux API.

%package utils
Summary: SELinux libselinux utilies
Group: Development/Libraries
Requires: libselinux = %{version}-%{release} 

%description utils
The libselinux-utils package contains the utilities

%package python
Summary: SELinux python bindings for libselinux
Group: Development/Libraries
Requires: libselinux = %{version}-%{release} 

%description python
The libselinux-python package contains the python bindings for developing 
SELinux applications. 

%package python3
Summary: SELinux python 3 bindings for libselinux
Group: Development/Libraries
Requires: libselinux = %{version}-%{release}

%description python3
The libselinux-python3 package contains python 3 bindings for developing
SELinux applications.

%package ruby
Summary: SELinux ruby bindings for libselinux
Group: Development/Libraries
Requires: libselinux = %{version}-%{release} 
Provides: ruby(selinux)

%description ruby
The libselinux-ruby package contains the ruby bindings for developing 
SELinux applications. 

%package devel
Summary: Header files and libraries used to build SELinux
Group: Development/Libraries
Requires: libselinux = %{version}-%{release} 
Requires: libsepol-devel >= %{libsepolver}

%description devel
The libselinux-devel package contains the libraries and header files
needed for developing SELinux applications. 

%package static
Summary: Static libraries used to build SELinux
Group: Development/Libraries
Requires: libselinux-devel = %{version}-%{release}

%description static
The libselinux-static package contains the static libraries
needed for developing SELinux applications. 

%prep
%setup -q
%patch0 -p2 -b .rhat
#%%patch1 -p1 -b .ruby19

%build
make clean
make LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" %{?_smp_mflags} swigify
make LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" %{?_smp_mflags} all

BuildPythonWrapper() {
  BinaryName=$1

  # Perform the build from the upstream Makefile:
  make \
    PYTHON=$BinaryName \
    LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" %{?_smp_mflags} \
    pywrap
}

BuildPythonWrapper %{__python}
BuildPythonWrapper %{__python3}

make RUBYINC="%{ruby_inc}" SHLIBDIR="%{_libdir}" LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" %{?_smp_mflags} rubywrap

%install
rm -rf %{buildroot}
InstallPythonWrapper() {
  BinaryName=$1

  make \
    PYTHON=$BinaryName \
    LIBDIR="%{_libdir}" CFLAGS="-g %{optflags}" %{?_smp_mflags} \
    pywrap

  make \
    PYTHON=$BinaryName \
    DESTDIR="%{buildroot}" LIBDIR="%{buildroot}%{_libdir}" \
    SHLIBDIR="%{buildroot}/%{_lib}" BINDIR="%{buildroot}%{_bindir}" \
    SBINDIR="%{buildroot}%{_sbindir}" \
    install-pywrap
}

rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_prefix}/lib/tmpfiles.d
mkdir -p %{buildroot}/%{_libdir} 
mkdir -p %{buildroot}%{_includedir} 
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}/var/run/setrans
echo "d /var/run/setrans 0755 root root" > %{buildroot}/%{_prefix}/lib/tmpfiles.d/libselinux.conf

InstallPythonWrapper %{__python}
InstallPythonWrapper %{__python3}

make DESTDIR="%{buildroot}" LIBDIR="%{buildroot}%{_libdir}" SHLIBDIR="%{buildroot}%{_libdir}" BINDIR="%{buildroot}%{_bindir}" SBINDIR="%{buildroot}%{_sbindir}" RUBYINSTALL=%{buildroot}%{ruby_sitearch} install install-rubywrap

# Nuke the files we don't want to distribute
rm -f %{buildroot}%{_sbindir}/compute_*
rm -f %{buildroot}%{_sbindir}/deftype
rm -f %{buildroot}%{_sbindir}/execcon
rm -f %{buildroot}%{_sbindir}/getenforcemode
rm -f %{buildroot}%{_sbindir}/getfilecon
rm -f %{buildroot}%{_sbindir}/getpidcon
rm -f %{buildroot}%{_sbindir}/mkdircon
rm -f %{buildroot}%{_sbindir}/policyvers
rm -f %{buildroot}%{_sbindir}/setfilecon
rm -f %{buildroot}%{_sbindir}/selinuxconfig
rm -f %{buildroot}%{_sbindir}/selinuxdisable
rm -f %{buildroot}%{_sbindir}/getseuser
rm -f %{buildroot}%{_sbindir}/togglesebool
rm -f %{buildroot}%{_sbindir}/selinux_check_securetty_context
mv %{buildroot}%{_sbindir}/getdefaultcon %{buildroot}%{_sbindir}/selinuxdefcon
mv %{buildroot}%{_sbindir}/getconlist %{buildroot}%{_sbindir}/selinuxconlist
install -d %{buildroot}%{_mandir}/man8/
install -m 644 %{SOURCE1} %{buildroot}%{_mandir}/man8/
install -m 644 %{SOURCE2} %{buildroot}%{_mandir}/man8/
rm -f %{buildroot}%{_mandir}/man8/togglesebool*

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libselinux.so.*
%ghost /var/run/setrans
%{_sbindir}/sefcontext_compile
%{_prefix}/lib/tmpfiles.d/libselinux.conf

%files utils
%defattr(-,root,root,-)
%{_sbindir}/avcstat
%{_sbindir}/getenforce
%{_sbindir}/getsebool
%{_sbindir}/matchpathcon
%{_sbindir}/selinuxconlist
%{_sbindir}/selinuxdefcon
%{_sbindir}/selinuxexeccon
%{_sbindir}/selinuxenabled
%{_sbindir}/setenforce
%{_mandir}/man5/*
%{_mandir}/man8/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libselinux.so
%{_libdir}/pkgconfig/libselinux.pc
%dir %{_libdir}/golang/src/pkg/github.com/selinux
%{_libdir}/golang/src/pkg/github.com/selinux/selinux.go
%dir %{_includedir}/selinux
%{_includedir}/selinux/*
%{_mandir}/man3/*

%files static
%defattr(-,root,root,-)
%{_libdir}/libselinux.a

%files python
%defattr(-,root,root,-)
%dir %{python_sitearch}/selinux
%{python_sitearch}/selinux/*

%files python3
%defattr(-,root,root,-)
%dir %{python3_sitearch}/selinux
%dir %{python3_sitearch}/selinux/__pycache__
%{python3_sitearch}/selinux/*.py*
%{python3_sitearch}/selinux/*.so
%{python3_sitearch}/selinux/__pycache__/*

%files ruby
%defattr(-,root,root,-)
%{ruby_sitearch}/selinux.so

%changelog
* Sat Jun 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-1m)
- udpate 2.3

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.13-3m)
- support UserMove env

* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.13-2m)
- rebuild against python-3.4

* Sat Mar  9 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.13-1m)
- update to 2.1.13
- sync FC19

* Fri Dec  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.6-2m)
- update patches

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.6-1m)
- update 2.1.6

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.4-2m)
- rebuild against python-3.2

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.4-1m)
- update 2.1.4

* Wed Jun  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.99-1m)
- update 2.0.99
-- but python3 disabled...

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.94-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.94-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.94-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.94-3m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.94-2m)
- rebuild against ruby-1.9.2

* Wed Jul 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.94-1m)
- update to 2.0.94

* Mon Mar  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.91-1m)
- update to 2.0.91
-- fixed double free

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.80-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.80-1m)
- update 2.0.80

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.76-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.76-3m)
- rebuild against python-2.6.1-1m

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.76-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Sat Dec 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.76-1m)
- sync with Rawhide (2.0.76-6)

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.64-1m)
- version up 2.0.64

* Wed Apr 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.59-1m)
- version up 2.0.53
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.23-2m)
- rebuild against gcc43

* Fri Jun 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.23-1m)
- update 2.0.23

* Fri May 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.13-1m)
- update 2.0.13

* Sat Mar 10 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.5-2m)
- resurrect Patch: libselinux-rhat.patch
- Patch1: libselinux-2.0.5-avch.patch

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.5-1m)
- update 2.0.5

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- update 2.0.0

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.34.0-2m)
- delete pyc pyo

* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.34.0-1m)
- update 1.34.0

* Sun May  7 2006 Mashairo Takahata <takahata@momonga-linux.org>
- (1.30-1m)
- version up

* Wed Jan 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.29.2-2m)
- add BuildRequires: libsepol >= %{libsepolver}

* Wed Dec 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.29.2-1m)
- version 1.29.2
- sync with fc-devel

* Sun Oct 30 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.26-1m)
- version 1.26
- remove Patch

* Mon Nov 29 2004 TAKAHASHI Tamotsu <tamo>
- (1.19.4-1m)
- sync with Fedora 1.19.1-6 (but comment out
 1.17.15-1 change: "Nuke the files we don't want to distribute")

 +* Thu Nov 18 2004 Dan Walsh <dwalsh@redhat.com> 1.19.1-6
 +- Add avcstat program
 +
 +* Mon Nov 15 2004 Dan Walsh <dwalsh@redhat.com> 1.19.1-4
 +- Add lots of missing man pages
 +
 +* Fri Nov 12 2004 Dan Walsh <dwalsh@redhat.com> 1.19.1-2
 +- Fix output of getsebool.
 +
 +* Tue Nov 9 2004 Dan Walsh <dwalsh@redhat.com> 1.19.1-1
 +- Update from upstream, fix setsebool -P segfault
 +
 +* Fri Nov 5 2004 Steve Grubb <sgrubb@redhat.com> 1.18.1-5
 +- Add a patch from upstream. Fixes signed/unsigned issues, and
 +  incomplete structure copy.
 +
 +* Thu Nov 4 2004 Dan Walsh <dwalsh@redhat.com> 1.18.1-4
 +- More fixes from sgrubb, better syslog
 +
 +* Thu Nov 4 2004 Dan Walsh <dwalsh@redhat.com> 1.18.1-3
 +- Have setsebool and togglesebool log changes to syslog
 +
 +* Wed Nov 3 2004 Steve Grubb <sgrubb@redhat.com> 1.18.1-2
 +- Add patch to make setsebool update bool on disk
 +- Make togglesebool have a rollback capability in case it blows up inflight
 +
 +* Tue Nov 2 2004 Dan Walsh <dwalsh@redhat.com> 1.18.1-1
 +- Upgrade to latest from NSA
 +
 +* Thu Oct 28 2004 Steve Grubb <sgrubb@redhat.com> 1.17.15-2
 +- Changed the location of the utilities to /usr/sbin since
 +   normal users can't use them anyways.
 +
 +* Wed Oct 27 2004 Steve Grubb <sgrubb@redhat.com> 1.17.15-2
 +- Updated various utilities, removed utilities that are for testing,
 +  added man pages.
 +
 +* Fri Oct 15 2004 Dan Walsh <dwalsh@redhat.com> 1.17.15-1
 +- Add -g flag to make
 +- Upgrade to latest  from NSA
 +       * Added rpm_execcon.

* Sat Nov 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17.14-1m)
- re-import from fedora developemnt
- No NoSource: 0
- changelog is below
- 
-* Fri Oct 1 2004 Dan Walsh <dwalsh@redhat.com> 1.17.14-1
-- Upgrade to latest  from NSA
-	* Merged setenforce and removable context patch from Dan Walsh.
-	* Merged build fix for alpha from Ulrich Drepper.
-	* Removed copyright/license from selinux_netlink.h - definitions only.
-
-* Fri Oct 1 2004 Dan Walsh <dwalsh@redhat.com> 1.17.13-3
-- Change setenforce to accept Enforcing and Permissive
-
-* Wed Sep 22 2004 Dan Walsh <dwalsh@redhat.com> 1.17.13-2
-- Add alpha patch
-
-* Mon Sep 20 2004 Dan Walsh <dwalsh@redhat.com> 1.17.13-1
-- Upgrade to latest  from NSA
-
-* Thu Sep 16 2004 Dan Walsh <dwalsh@redhat.com> 1.17.12-2
-- Add selinux_removable_context_path
-
-* Tue Sep 14 2004 Dan Walsh <dwalsh@redhat.com> 1.17.12-1
-- Update from NSA
-	* Add matchmediacon
-
-* Tue Sep 14 2004 Dan Walsh <dwalsh@redhat.com> 1.17.11-1
-- Update from NSA
-	* Merged in matchmediacon changes.
-
-* Fri Sep 10 2004 Dan Walsh <dwalsh@redhat.com> 1.17.10-1
-- Update from NSA
-	* Regenerated headers for new nscd permissions.
-
-* Wed Sep 8 2004 Dan Walsh <dwalsh@redhat.com> 1.17.9-2
-- Add matchmediacon
-
-* Wed Sep 8 2004 Dan Walsh <dwalsh@redhat.com> 1.17.9-1
-- Update from NSA
-	* Added get_default_context_with_role.
-
-* Thu Sep 2 2004 Dan Walsh <dwalsh@redhat.com> 1.17.8-2
-- Clean up spec file
-	* Patch from Matthias Saou
-
-* Thu Sep 2 2004 Dan Walsh <dwalsh@redhat.com> 1.17.8-1
-- Update from NSA
-	* Added set_matchpathcon_printf.	
-
-* Wed Sep 1 2004 Dan Walsh <dwalsh@redhat.com> 1.17.7-1
-- Update from NSA
-	* Reworked av_inherit.h to allow easier re-use by kernel. 
-
-* Tue Aug 31 2004 Dan Walsh <dwalsh@redhat.com> 1.17.6-1
-- Add strcasecmp in selinux_config
-- Update from NSA
-	* Changed avc_has_perm_noaudit to not fail on netlink errors.
-	* Changed avc netlink code to check pid based on patch by Steve Grubb.
-	* Merged second optimization patch from Ulrich Drepper.
-	* Changed matchpathcon to skip invalid file_contexts entries.
-	* Made string tables private to libselinux.
-	* Merged strcat->stpcpy patch from Ulrich Drepper.
-	* Merged matchpathcon man page from Dan Walsh.
-	* Merged patch to eliminate PLTs for local syms from Ulrich Drepper.
-	* Autobind netlink socket.
-	* Dropped compatibility code from security_compute_user.
-	* Merged fix for context_range_set from Chad Hanson.
-	* Merged allocation failure checking patch from Chad Hanson.
-	* Merged avc netlink error message patch from Colin Walters.
-
-
-* Mon Aug 30 2004 Dan Walsh <dwalsh@redhat.com> 1.17.5-1
-- Update from NSA
-	* Merged second optimization patch from Ulrich Drepper.
-	* Changed matchpathcon to skip invalid file_contexts entries.
-	* Made string tables private to libselinux.
-	* Merged strcat->stpcpy patch from Ulrich Drepper.
-	* Merged matchpathcon man page from Dan Walsh.
-	* Merged patch to eliminate PLTs for local syms from Ulrich Drepper.
-	* Autobind netlink socket.
-	* Dropped compatibility code from security_compute_user.
-	* Merged fix for context_range_set from Chad Hanson.
-	* Merged allocation failure checking patch from Chad Hanson.
-	* Merged avc netlink error message patch from Colin Walters.
-
-* Mon Aug 30 2004 Dan Walsh <dwalsh@redhat.com> 1.17.4-1
-- Update from NSA
-- Add optflags
-
-* Fri Aug 26 2004 Dan Walsh <dwalsh@redhat.com> 1.17.3-1
-- Update from NSA
-
-* Thu Aug 26 2004 Dan Walsh <dwalsh@redhat.com> 1.17.2-1
-- Add matchpathcon man page
-- Latest from NSA
-	* Merged patch to eliminate PLTs for local syms from Ulrich Drepper.
-	* Autobind netlink socket.
-	* Dropped compatibility code from security_compute_user.
-	* Merged fix for context_range_set from Chad Hanson.
-	* Merged allocation failure checking patch from Chad Hanson.
-	* Merged avc netlink error message patch from Colin Walters.
-
-* Tue Aug 24 2004 Dan Walsh <dwalsh@redhat.com> 1.17.1-1
-- Latest from NSA
-	* Autobind netlink socket.
-	* Dropped compatibility code from security_compute_user.
-	* Merged fix for context_range_set from Chad Hanson.
-	* Merged allocation failure checking patch from Chad Hanson.
-	* Merged avc netlink error message patch from Colin Walters.
-
-* Sun Aug 22 2004 Dan Walsh <dwalsh@redhat.com> 1.16.1-1
-- Latest from NSA
-
-* Thu Aug 19 2004 Colin Walters <walters@redhat.com> 1.16-1
-- New upstream version
-
-* Tue Aug 17 2004 Dan Walsh <dwalsh@redhat.com> 1.15.7-1
-- Latest from Upstream
-
-* Mon Aug 16 2004 Dan Walsh <dwalsh@redhat.com> 1.15.6-1
-- Fix man pages
-
-* Mon Aug 16 2004 Dan Walsh <dwalsh@redhat.com> 1.15.5-1
-- Latest from Upstream
-
-* Fri Aug 13 2004 Dan Walsh <dwalsh@redhat.com> 1.15.4-1
-- Latest from Upstream
-
-* Thu Aug 12 2004 Dan Walsh <dwalsh@redhat.com> 1.15.3-2
-- Add man page for boolean functions and SELinux
-
-* Sat Aug 8 2004 Dan Walsh <dwalsh@redhat.com> 1.15.3-1
-- Latest from NSA
-
-* Mon Jul 19 2004 Dan Walsh <dwalsh@redhat.com> 1.15.2-1
-- Latest from NSA
-
-* Mon Jul 19 2004 Dan Walsh <dwalsh@redhat.com> 1.15.1-3
-- uppercase getenforce returns, to make them match system-config-securitylevel
-
-* Thu Jul 15 2004 Dan Walsh <dwalsh@redhat.com> 1.15.1-2
-- Remove old path patch
-
-* Thu Jul 8 2004 Dan Walsh <dwalsh@redhat.com> 1.15.1-1
-- Update to latest from NSA
-- Add fix to only get old path if file_context file exists in old location
-
-* Wed Jun 30 2004 Dan Walsh <dwalsh@redhat.com> 1.14.1-1
-- Update to latest from NSA
-
-* Wed Jun 16 2004 Dan Walsh <dwalsh@redhat.com> 1.13.4-1
-- add nlclass patch
-- Update to latest from NSA
-
-* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
-- rebuilt
-
-* Sat Jun 13 2004 Dan Walsh <dwalsh@redhat.com> 1.13.3-2
-- Fix selinux_config to break once it finds SELINUXTYPE.
-
-* Fri May 28 2004 Dan Walsh <dwalsh@redhat.com> 1.13.2-1
--Update with latest from NSA
-
-* Thu May 27 2004 Dan Walsh <dwalsh@redhat.com> 1.13.1-1
-- Change to use new policy mechanism
-
-* Mon May 17 2004 Dan Walsh <dwalsh@redhat.com> 1.12-2
-- add man patch
-
-* Thu May 14 2004 Dan Walsh <dwalsh@redhat.com> 1.12-1
-- Update with latest from NSA

* Wed Sep 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.16-1m)
  imported from FC2 and update to 1.16
  
* Wed May 5 2004 Dan Walsh <dwalsh@redhat.com> 1.11.4-1
- Update with latest from NSA

* Thu Apr 22 2004 Dan Walsh <dwalsh@redhat.com> 1.11.3-1
- Add changes for relaxed policy 
- Update to match NSA 

* Thu Apr 15 2004 Dan Walsh <dwalsh@redhat.com> 1.11.2-1
- Add relaxed policy changes 

* Thu Apr 15 2004 Dan Walsh <dwalsh@redhat.com> 1.11-4
- Sync with NSA

* Thu Apr 15 2004 Dan Walsh <dwalsh@redhat.com> 1.11-3
- Remove requires glibc>2.3.4

* Wed Apr 14 2004 Dan Walsh <dwalsh@redhat.com> 1.11-2
- Fix selinuxenabled man page.

* Wed Apr 7 2004 Dan Walsh <dwalsh@redhat.com> 1.11-1
- Upgrade to 1.11

* Wed Apr 7 2004 Dan Walsh <dwalsh@redhat.com> 1.10-2
- Add memleaks patch

* Wed Apr 7 2004 Dan Walsh <dwalsh@redhat.com> 1.10-1
- Upgrade to latest from NSA and add more man pages

* Thu Apr 1 2004 Dan Walsh <dwalsh@redhat.com> 1.9-1
- Update to match NSA
- Cleanup some man pages

* Tue Mar 30 2004 Dan Walsh <dwalsh@redhat.com> 1.8-1
- Upgrade to latest from NSA

* Thu Mar 25 2004 Dan Walsh <dwalsh@redhat.com> 1.6-6
- Add Russell's Man pages

* Thu Mar 25 2004 Dan Walsh <dwalsh@redhat.com> 1.6-5
- Change getenforce to also check is_selinux_enabled

* Thu Mar 25 2004 Dan Walsh <dwalsh@redhat.com> 1.6-4
- Add ownership to /usr/include/selinux

* Wed Mar 10 2004 Dan Walsh <dwalsh@redhat.com> 1.6-3
- fix location of file_contexts file.

* Wed Mar 10 2004 Dan Walsh <dwalsh@redhat.com> 1.6-2
- Fix matchpathcon to use BUFSIZ

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Feb 23 2004 Dan Walsh <dwalsh@redhat.com> 1.4-11
- add matchpathcon

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Jan 23 2004 Dan Walsh <dwalsh@redhat.com> 1.4-9
- Add rootok patch

* Wed Jan 14 2004 Dan Walsh <dwalsh@redhat.com> 1.4-8
- Updated getpeernam patch

* Tue Jan 13 2004 Dan Walsh <dwalsh@redhat.com> 1.4-7
- Add getpeernam patch

* Thu Dec 18 2003 Dan Walsh <dwalsh@redhat.com> 1.4-6
- Add getpeercon patch

* Thu Dec 18 2003 Dan Walsh <dwalsh@redhat.com> 1.4-5
- Put mntpoint patch, because found fix for SysVinit

* Wed Dec 17 2003 Dan Walsh <dwalsh@redhat.com> 1.4-4
- Add remove mntpoint patch, because it breaks SysVinit

* Wed Dec 17 2003 Dan Walsh <dwalsh@redhat.com> 1.4-3
- Add mntpoint patch for SysVinit

* Fri Dec 12 2003 Dan Walsh <dwalsh@redhat.com> 1.4-2
- Add -r -u -t to getcon 

* Sat Dec 6 2003 Dan Walsh <dwalsh@redhat.com> 1.4-1
- Upgrade to latest from NSA

* Mon Oct 27 2003 Dan Walsh <dwalsh@redhat.com> 1.3-2
- Fix x86_64 build

* Wed Oct 21 2003 Dan Walsh <dwalsh@redhat.com> 1.3-1
- Latest tarball from NSA.

* Tue Oct 21 2003 Dan Walsh <dwalsh@redhat.com> 1.2-9
- Update with latest changes from NSA

* Mon Oct 20 2003 Dan Walsh <dwalsh@redhat.com> 1.2-8
- Change location of .so file

* Wed Oct 8 2003 Dan Walsh <dwalsh@redhat.com> 1.2-7
- Break out into development library

* Wed Oct  8 2003 Dan Walsh <dwalsh@redhat.com> 1.2-6
- Move location of libselinux.so to /lib

* Fri Oct  3 2003 Dan Walsh <dwalsh@redhat.com> 1.2-5
- Add selinuxenabled patch

* Wed Oct  1 2003 Dan Walsh <dwalsh@redhat.com> 1.2-4
- Update with final NSA 1.2 sources.

* Fri Sep  12 2003 Dan Walsh <dwalsh@redhat.com> 1.2-3
- Update with latest from NSA.

* Fri Aug  28 2003 Dan Walsh <dwalsh@redhat.com> 1.2-2
- Fix to build on x86_64

* Thu Aug  21 2003 Dan Walsh <dwalsh@redhat.com> 1.2-1
- update for version 1.2

* Wed May 27 2003 Dan Walsh <dwalsh@redhat.com> 1.0-1
- Initial version

