#!/bin/bash
. /usr/share/java-utils/java-functions

MAIN_CLASS=org.tmatesoft.sqljet.browser.DBBrowser

set_classpath "sqljet sqljet-browser"

export CLASSPATH=$CLASSPATH:/usr/share/netbeans/platform11/modules/org-netbeans-swing-outline.jar

run "$@"
