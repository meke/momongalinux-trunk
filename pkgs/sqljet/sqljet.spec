%global momorel 4

Name:           sqljet
Version:        1.0.2
Release:        %{momorel}m%{?dist}
Summary:        Pure Java SQLite

Group:          Development/Libraries
License:        GPLv2
URL:            http://sqljet.com/
# Obtained by sh fetch-sqljet.sh
Source0:        %{name}-%{version}.tar.bz2
Source1:        fetch-sqljet.sh
Source2:        %{name}-browser.sh
Source3:        %{name}-browser.desktop
Patch0:         %{name}-javadoc.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ant
BuildRequires:  antlr
BuildRequires:  antlr3-tool
BuildRequires:  antlr3-java
BuildRequires:  easymock2
BuildRequires:  netbeans-platform
BuildRequires:  java-devel >= 1.6
BuildRequires:  junit4
BuildRequires:  desktop-file-utils
Requires:       antlr3-java

BuildArch: noarch

%description
SQLJet is an independent pure Java implementation of a popular SQLite database
management system. SQLJet is a software library that provides API that enables
Java application to read and modify SQLite databases.

%package        browser
Group:          Development/Tools
Summary:        SQLJet database browser
Requires:       %{name} = %{version}-%{release}
Requires:       netbeans-platform

%description    browser
Utility for browsing SQLJet/SQLite databases.

%package        javadoc
Group:          Documentation
Summary:        Javadoc for %{name}
Requires:       jpackage-utils

%description    javadoc
API documentation for %{name}.

%prep
%setup -q
%patch0

find \( -name '*.class' -o -name '*.jar' \) -delete 

pushd lib
ln -s %{_javadir}/antlr3-runtime.jar antlr-runtime-3.1.3.jar
popd
pushd sqljet-examples/browser/lib
ln -s %{_datadir}/netbeans/platform11/modules/org-netbeans-swing-outline.jar org-netbeans-swing-outline.jar
popd


%build
export CLASSPATH=$(build-classpath antlr3 antlr3-runtime antlr stringtemplate easymock2 junit4)

ant jars osgi javadoc

jar umf sqljet/osgi/MANIFEST.MF build/sqljet.jar

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_javadir}
cp -p  build/sqljet.jar  \
%{buildroot}%{_javadir}/%{name}-%{version}.jar
cp -p  build/sqljet-browser.jar  \
%{buildroot}%{_javadir}/%{name}-browser-%{version}.jar

(cd %{buildroot}%{_javadir} && for jar in *-%{version}*; \
    do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

mkdir -p %{buildroot}%{_javadocdir}/%{name}
cp -rp build/javadoc \
%{buildroot}%{_javadocdir}/%{name}

install -d %{buildroot}%{_bindir}
install -m 755 %{SOURCE2} %{buildroot}%{_bindir}/%{name}-browser

desktop-file-install                                    \
--dir=${RPM_BUILD_ROOT}%{_datadir}/applications         \
%{SOURCE3}

desktop-file-validate %{buildroot}/%{_datadir}/applications/sqljet-browser.desktop

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING INSTALL README.txt
%{_javadir}/%{name}.jar
%{_javadir}/%{name}-%{version}.jar

%files browser
%defattr(-,root,root,-)
%{_javadir}/%{name}-browser.jar
%{_javadir}/%{name}-browser-%{version}.jar
%{_bindir}/%{name}-browser
%{_datadir}/applications/%{name}-browser.desktop

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- import from Fedora 13

* Wed Apr 21 2010 Alexander Kurtakov <akurtako@redhat.com> 1.0.2-3
- Fix build with new antlr3.
- Fix startup script for the browser.
- Add missing semicolon in the desktop file.

* Mon Feb 15 2010 Alexander Kurtakov <akurtako@redhat.com> 1.0.2-2
- Rebuild for netbeans-platform update. (rhbz#564657)

* Thu Jan 14 2010 Alexander Kurtakov <akurtako@redhat.com> 1.0.2-1
- Update to 1.0.2.

* Thu Dec 3 2009 Alexander Kurtakov <akurtako@redhat.com> 1.0.1-3
- Require antlr3.

* Mon Nov 30 2009 Alexander Kurtakov <akurtako@redhat.com> 1.0.1-2
- Fix build and review comments.

* Thu Nov 26 2009 Alexander Kurtakov <akurtako@redhat.com> 1.0.1-1
- Initial package.
