#! /bin/sh

version=1.0.2
mkdir fetch
cd fetch
svn export http://svn.sqljet.com/repos/sqljet/tags/$version sqljet-$version

tar cfj sqljet-$version.tar.bz2 sqljet-$version

cd ..
