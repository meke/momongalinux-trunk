From cf0b35e221797f20caf0f9904237dea9905aecc8 Mon Sep 17 00:00:00 2001
From: Stephen Kelly <stephen.kelly@kdab.com>
Date: Tue, 23 Jul 2013 09:22:17 +0200
Subject: [PATCH] CMake: Use path manipulation features from qtbase.

The qtbase commit 4ce32f66 (Use new path manipulation functions
from qmake., 2013-01-18) used new qmake functions for path manipulation,
and commits 7ac58d1f (Make cmake packages installed to /usr
non-relocatable., 2013-02-11) and 5aa34962 (Handle usr-move without
forcing absolute paths, 2013-04-17) added handling of symlinks which
would otherwise break relative path calculation.

Port those patches to the qttools cmake files.

Task-number: QTBUG-32570
Change-Id: I7d22609c3a03be4b9197dd030d7e9bd6c2e04731
Reviewed-by: Stephen Kelly <stephen.kelly@kdab.com>
---
 src/linguist/Qt5LinguistToolsConfig.cmake.in | 26 ++++++++++++++++++++++++++
 src/linguist/linguist.pro                    | 24 +++++++++++++-----------
 2 files changed, 39 insertions(+), 11 deletions(-)

diff --git a/src/linguist/Qt5LinguistToolsConfig.cmake.in b/src/linguist/Qt5LinguistToolsConfig.cmake.in
index 6584b56..fac7cc6 100644
--- a/src/linguist/Qt5LinguistToolsConfig.cmake.in
+++ b/src/linguist/Qt5LinguistToolsConfig.cmake.in
@@ -1,5 +1,31 @@
 
+if (CMAKE_VERSION VERSION_LESS 2.8.3)
+    message(FATAL_ERROR \"Qt 5 requires at least CMake version 2.8.3\")
+endif()
+
+!!IF !isEmpty(CMAKE_USR_MOVE_WORKAROUND)
+!!IF !isEmpty(CMAKE_LIB_DIR_IS_ABSOLUTE)
+set(_qt5_linguisttools_install_prefix \"$$[QT_INSTALL_PREFIX]\")
+!!ELSE
+get_filename_component(_IMPORT_PREFIX \"${CMAKE_CURRENT_LIST_FILE}\" PATH)
+# Use original install prefix when loaded through a
+# cross-prefix symbolic link such as /lib -> /usr/lib.
+get_filename_component(_realCurr \"${_IMPORT_PREFIX}\" REALPATH)
+get_filename_component(_realOrig \"$$CMAKE_INSTALL_LIBS_DIR/cmake/Qt5LinguistTools\" REALPATH)
+if(_realCurr STREQUAL _realOrig)
+    get_filename_component(_qt5_linguisttools_install_prefix \"$$CMAKE_INSTALL_LIBS_DIR\" PATH)
+else()
+    get_filename_component(_qt5_linguisttools_install_prefix \"${CMAKE_CURRENT_LIST_DIR}/$${CMAKE_RELATIVE_INSTALL_DIR}\" ABSOLUTE)
+endif()
+unset(_realOrig)
+unset(_realCurr)
+unset(_IMPORT_PREFIX)
+!!ENDIF
+!!ELIF isEmpty(CMAKE_LIB_DIR_IS_ABSOLUTE)
 get_filename_component(_qt5_linguisttools_install_prefix \"${CMAKE_CURRENT_LIST_DIR}/$${CMAKE_RELATIVE_INSTALL_DIR}\" ABSOLUTE)
+!!ELSE
+set(_qt5_linguisttools_install_prefix \"$$[QT_INSTALL_PREFIX]\")
+!!ENDIF
 
 if (NOT TARGET Qt5::lrelease)
     add_executable(Qt5::lrelease IMPORTED)
diff --git a/src/linguist/linguist.pro b/src/linguist/linguist.pro
index df3c0c7..2f7850e 100644
--- a/src/linguist/linguist.pro
+++ b/src/linguist/linguist.pro
@@ -11,22 +11,24 @@ qtNomakeTools( \
 
 win32:CMAKE_BIN_SUFFIX = ".exe"
 
-CMAKE_QT_INSTALL_PREFIX_ESCAPED = "^$$re_escape($$[QT_INSTALL_PREFIX])/"
-win32:CMAKE_QT_INSTALL_PREFIX_ESCAPED ~= s,\\\\,/
+load(cmake_functions)
 
-CMAKE_LIB_DIR = $$replace($$list($$[QT_INSTALL_LIBS]), \\\\, /)/
-contains(CMAKE_LIB_DIR, "$${CMAKE_QT_INSTALL_PREFIX_ESCAPED}.*") {
-    CMAKE_LIB_DIR = $$replace(CMAKE_LIB_DIR, "$$CMAKE_QT_INSTALL_PREFIX_ESCAPED", )
-    CMAKE_RELATIVE_INSTALL_DIR = $$replace(CMAKE_LIB_DIR, "[^/]+", ..)
+CMAKE_INSTALL_LIBS_DIR = $$cmakeTargetPath($$[QT_INSTALL_LIBS])
+contains(CMAKE_INSTALL_LIBS_DIR, ^(/usr)?/lib(64)?.*): CMAKE_USR_MOVE_WORKAROUND = $$CMAKE_INSTALL_LIBS_DIR
+
+CMAKE_LIB_DIR = $$cmakeRelativePath($$[QT_INSTALL_LIBS], $$[QT_INSTALL_PREFIX])
+!contains(CMAKE_LIB_DIR,"^\\.\\./.*") {
+    CMAKE_RELATIVE_INSTALL_DIR = $$cmakeRelativePath($$[QT_INSTALL_PREFIX], $$[QT_INSTALL_LIBS])
     # We need to go up another two levels because the CMake files are
     # installed in $${CMAKE_LIB_DIR}/cmake/Qt5$${CMAKE_MODULE_NAME}
-    CMAKE_RELATIVE_INSTALL_DIR = "$${CMAKE_RELATIVE_INSTALL_DIR}/../../"
+    CMAKE_RELATIVE_INSTALL_DIR = "$${CMAKE_RELATIVE_INSTALL_DIR}../../"
+} else {
+    CMAKE_LIB_DIR_IS_ABSOLUTE = True
 }
 
-CMAKE_BIN_DIR = $$replace($$list($$[QT_INSTALL_BINS]), \\\\, /)/
-contains(CMAKE_BIN_DIR, "$${CMAKE_QT_INSTALL_PREFIX_ESCAPED}.*") {
-    CMAKE_BIN_DIR = $$replace(CMAKE_BIN_DIR, "$$CMAKE_QT_INSTALL_PREFIX_ESCAPED", )
-} else {
+CMAKE_BIN_DIR = $$cmakeRelativePath($$[QT_HOST_BINS], $$[QT_INSTALL_PREFIX])
+contains(CMAKE_BIN_DIR, "^\\.\\./.*") {
+    CMAKE_BIN_DIR = $$[QT_HOST_BINS]/
     CMAKE_BIN_DIR_IS_ABSOLUTE = True
 }
 
-- 
1.8.3.1

