%global momorel 3

%{expand: %%global py_ver %(python -c 'import sys;print(sys.version[0:3])')}
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")
%define tarname vte

Name: vte028
Version: 0.28.2
Release: %{momorel}m%{?dist}
Summary: An experimental terminal emulator.
License: LGPL
Group: User Interface/X
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{tarname}/0.28/%{tarname}-%{version}.tar.xz
NoSource: 0
BuildRequires: python-devel >= %{py_ver}
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: pygtk2-devel >= 2.13.1
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: pango-devel >= 1.23.0
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXcursor-devel
BuildRequires: libXext-devel, libXfixes-devel, libXft-devel, libXi-devel
BuildRequires: libXinerama-devel, libXrandr-devel, libXrender-devel

%description
VTE is an experimental terminal emulator widget for use with GTK+ 2.0.

%package devel
Summary: Files needed for developing applications which use vte.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: pygtk2-devel >= 2.24
Requires: gtk2-devel
Requires: pango-devel
Requires: python-devel >= %{py_ver}
Requires: libXft-devel
Requires: fontconfig-devel
Obsoletes: vte29-devel
Provides: vte29-devel

%description devel
VTE is an experimental terminal emulator widget for use with GTK+ 2.0.  This
package contains the files needed for building applications using VTE.

%prep
%setup -q -n %{tarname}-%{version}

%build
PYTHON=%{_bindir}/python%{py_ver}; export PYTHON
%configure \
    --enable-silent-rules \
    --enable-gtk-doc \
    --enable-python \
    --disable-static \
    --enable-gnome-pty-helper \
    --enable-introspection=yes \
    --with-gtk=2.0
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{tarname}-0.0

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{tarname}-0.0.lang
%defattr(-,root,root)
%doc ChangeLog COPYING HACKING NEWS README
%{_bindir}/vte
%{_libdir}/libvte.so.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/Vte-0.0.typelib

%{_datadir}/vte/termcap-0.0
%exclude %{_libexecdir}/gnome-pty-helper

%{python_sitearch}/gtk-2.0/vtemodule.*
%{_datadir}/pygtk/2.0/defs/vte.defs

%files devel
%defattr(-,root,root)
%{_includedir}/vte-0.0
%{_libdir}/*.so
%{_libdir}/pkgconfig/vte.pc
%{_libdir}/pkgconfig/pyvte.pc
%doc %{_datadir}/gtk-doc/html/vte-0.0
%{_datadir}/gir-1.0/Vte-0.0.gir

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.28.2-3m)
- rebuild for glib 2.33.2

* Sat Oct  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.28.2-2m)
- enabel-python

* Thu Sep 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.28.2-1m)
- copy from vte
- disable-python

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.29.1-1m)
- update to 0.29.1

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.28.1-1m)
- update to 0.28.1

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.28.0-4m)
- build with gtk2 and gtk3
- obsoletes vte29

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.28.0-3m)
- build with gtk3

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.28.0-2m)
- rebuild for python-2.7

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.28.0-1m)
- update to 0.28.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.26.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.26.2-2m)
- rebuild for new GCC 4.5

* Sun Nov 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.26.2-1m)
- update to 0.26.2

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.26.0-1m)
- update to 0.26.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.24.3-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.24.3-1m)
- [SECURITY] CVE-2010-2713
- update to 0.24.3

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.24.2-1m)
- update to 0.24.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.24.1-1m)
- update to 0.24.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.24.0-1m)
- update to 0.24.0

* Mon Feb  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23.5-1m)
- update and rebuild against new gtk-2.19.4

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22.5-3m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.5-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Thu Nov 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.5-1m)
- update to 0.22.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.4-1m)
- update to 0.22.4

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.3-1m)
- update to 0.22.3

* Tue Sep 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.2-1m)
- update to 0.22.2

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.1-1m)
- update to 0.22.1

* Sat Jul  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20.5-2m)
- remove BuildPrereq: libzvt-devel

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.5-1m)
- update to 0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.4-1m)
- update to 0.20.4

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.2-1m)
- update to 0.20.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20.1-1m)
- update to 0.20.1

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19.4-1m)
- update to 0.19.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17.4-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.17.4-2m)
- rebuild against python-2.6.1-1m

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17.4-1m)
- update to 0.17.4

* Thu Jun  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.14-1m)
- update to 0.16.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.13-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.13-1m)
- update to 0.16.13

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.12-1m)
- update to 0.16.12

* Tue Dec 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.11-1m)
- update to 0.16.11

* Wed Dec  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.10-1m)
- update to 0.16.10

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.9-1m)
- update to 0.16.9

* Wed Jun 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.6-1m)
- update to 0.16.6

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.4-1m)
- update to 0.16.4

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.3-1m)
- update to 0.16.3

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16.1-1m)
- update to 0.16.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.0-1m)
- update to 0.16.0

* Fri Mar  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.6-1m)
- update to 0.15.6

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.5-1m)
- update to 0.15.5

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.4-1m)
- update to 0.15.4

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.3-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.3-1m)
- update to 0.15.3 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.2-1m)
- update to 0.14.2

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.1-2m)
- rebuild against python-2.5

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.1-1m)
- update 0.14.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.12.2-3m)
- rebuild against expat-2.0.0-1m

* Sun Aug 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.2-2m)
- delete libtool library

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.2-1m)
- update to 0.12.2

* Thu May 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-2m)
- delete duplicate dir

* Thu Apr 27 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Sat Feb 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.18-1m)
- update to 0.11.18

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.17-1m)
- update to 0.11.17

* Tue Nov 22 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.15-1m)
- version up
- GNOME 2.12.1 Desktop

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.11-4m)
- rebuild against python-2.4.2

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.11.11-3m)
- adjustment BuildPreReq

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.11.11-2m)
- rebuild against python2.3

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.11.11-1m)
- version 0.11.11

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (0.11.10-5m)
- revised spec for rpm 4.2.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.11.10-4m)
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Tue Apr  6 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.11.10-3m)
- BuildPrereq: pygtk -> pygtk-devel
- use %%momorel
- use %%NoSource and %%make

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.11.10-2m)
- revised spec for rpm 4.2.

* Tue Jun 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11.10-1m)
- version 0.11.10

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11.9-1m)
- version 0.11.9

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11.6-1m)
- version 0.11.6

* Thu May 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11.5-1m)
- version 0.11.5

* Fri Apr 18 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11.3-1m)
- version 0.11.3

* Thu Mar 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11.0-1m)
- version 0.11.0

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.26-2m)
- rebuild against for XFree86-4.3.0

* Wed Mar 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.26-1m)
- version 0.10.26

* Fri Feb 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.25-1m)
- version 0.10.25

* Mon Feb 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.23-1m)
- version 0.10.23

* Mon Feb 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.20-1m)
- version 0.10.20

* Fri Feb 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.19-1m)
- version 0.10.19

* Tue Feb 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.17-1m)
- version 0.10.17

* Mon Feb 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.16-1m)
- version 0.10.16

* Tue Jan 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.15-1m)
- version 0.10.15

* Sat Jan 25 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.14-1m)
- version 0.10.14

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.12-1m)
- version 0.10.12

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.11-1m)
- version 0.10.11

* Wed Jan 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.10-1m)
- version 0.10.10

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.8-1m)
- version 0.10.8

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.7-1m)
- version 0.10.7

* Wed Dec 04 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.5-1m)
- version 0.10.5

* Wed Nov 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.4-1m)
- version 0.10.4

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.3-1m)
- version 0.10.3

* Tue Oct 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.2-1m)
- version 0.9.2

* Sat Oct  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.0-1m)
- import
