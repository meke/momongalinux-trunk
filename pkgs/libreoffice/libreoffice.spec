%global momorel 1

%global ooover 3.3.4.1
%global ooobuildname libreoffice-build
%global ooobuildver 3.3.4.1
%global ooosrc libreoffice
%global oootag 3.3.4
%global srctag SRC680
%global OFFICEUPD 320
%global fontname opensymbol
%global basis_name basis3.3
%global ooodirname openoffice.org3.0
%global instdir %{_libdir}/%{ooodirname}

%global gecko_ver 1.9.2

%global mirror_site http://download.documentfoundation.org/libreoffice/src/%{oootag}/

%global langpacks 1
#%%global langpack_langs ALL
%global langpack_langs af ar as bg bn ca cs cy da de dz el en-US es et eu fi fr ga gl gu-IN he hi-IN hr hu it ja kn ko lt mai ml mr ms nb ne nl nn nr ns or pa-IN pl pt pt-BR ro ru sk sl sr ss st sv ta te th tn tr ts uk ur ve xh zh-CN zh-TW zu

%global withlang --with-lang="%{langpack_langs}"

# we make gallery files for openclipart in openclipart package (openoffice.org-openclipart)
%global with_openclipart 0
%global openclipartdir %{_datadir}/openclipart

%global use_ccache 1

%ifarch x86_64
%global distro Momonga64
%else
%global distro Momonga
%endif

# From FC
%global _use_internal_dependency_generator 0
%global __jar_repack %{nil}

Summary: LibreOffice comprehensive office suite
Name: libreoffice
Version: %{ooover}
Release: %{momorel}m%{?dist}
License: LGPLv3+ and LGPLv2+
Group: Applications/Productivity
URL: http://www.openoffice.org/
# ooo-build
# URL: http://go-oo.org/

# local configuration
%{?include_specopt}
%{?!_numjobs:        %global _numjobs 1}
# build kde integration with X loading NVIDIA driver may fail?
# so I made it configuable to build with/without kde (default should be 1)
%{?!enable_kde:      %global enable_kde 0}
# Java stuff (need openjdk-1.6.0-devel, but we do not need to switch a system java to openjdk-1.6.0 by adding --with-jdk-home)
%{?!with_java:       %global with_java 1}

%if %{with_java}
%ifarch x86_64
%global java_arg --with-java --with-jdk-home=%{_prefix}/lib/jvm/java-1.6.0-openjdk.x86_64
%else
%global java_arg --with-java --with-jdk-home=%{_libdir}/jvm/java-1.6.0-openjdk
%endif
%else
%global java_arg --without-java
%endif

# meta package
Requires: %{name}-core = %{version}-%{release}
%if %{enable_kde}
Requires: %{name}-kde = %{version}-%{release}
%endif
Requires: %{name}-base = %{version}-%{release}
Requires: %{name}-calc = %{version}-%{release}
Requires: %{name}-draw = %{version}-%{release}
Requires: %{name}-writer = %{version}-%{release}
Requires: %{name}-impress = %{version}-%{release}
Requires: %{name}-math = %{version}-%{release}
Requires: %{name}-graphicfilter = %{version}-%{release}
Requires: %{name}-xsltfilter = %{version}-%{release}
Requires: %{name}-pyuno = %{version}-%{release}
Requires: %{name}-langpack-ja_JP = %{version}-%{release}
%if %{with_java}
Requires: %{name}-javafilter = %{version}-%{release}
%endif

# Sources
Source0: %{mirror_site}/%{ooobuildname}-%{ooobuildver}.tar.gz
NoSource: 0
Source10: %{mirror_site}/%{ooosrc}-artwork-%{version}.tar.bz2
NoSource: 10
Source11: %{mirror_site}/%{ooosrc}-base-%{version}.tar.bz2
NoSource: 11
Source12: %{mirror_site}/%{ooosrc}-bootstrap-%{version}.tar.bz2
NoSource: 12
Source13: %{mirror_site}/%{ooosrc}-calc-%{version}.tar.bz2
NoSource: 13
Source14: %{mirror_site}/%{ooosrc}-components-%{version}.tar.bz2
NoSource: 14
Source15: %{mirror_site}/%{ooosrc}-extensions-%{version}.tar.bz2
NoSource: 15
Source16: %{mirror_site}/%{ooosrc}-extras-%{version}.tar.bz2
NoSource: 16
Source17: %{mirror_site}/%{ooosrc}-filters-%{version}.tar.bz2
NoSource: 17
Source18: %{mirror_site}/%{ooosrc}-help-%{version}.tar.bz2
NoSource: 18
Source19: %{mirror_site}/%{ooosrc}-impress-%{version}.tar.bz2
NoSource: 19
Source20: %{mirror_site}/%{ooosrc}-libs-core-%{version}.tar.bz2
NoSource: 20
Source21: %{mirror_site}/%{ooosrc}-libs-extern-sys-%{version}.tar.bz2
NoSource: 21
Source22: %{mirror_site}/%{ooosrc}-libs-extern-%{version}.tar.bz2
NoSource: 22
Source23: %{mirror_site}/%{ooosrc}-libs-gui-%{version}.tar.bz2
NoSource: 23
Source24: %{mirror_site}/%{ooosrc}-postprocess-%{version}.tar.bz2
NoSource: 24
Source25: %{mirror_site}/%{ooosrc}-sdk-%{version}.tar.bz2
NoSource: 25
Source26: %{mirror_site}/%{ooosrc}-testing-%{version}.tar.bz2
NoSource: 26
Source27: %{mirror_site}/%{ooosrc}-ure-%{version}.tar.bz2
NoSource: 27
Source28: %{mirror_site}/%{ooosrc}-writer-%{version}.tar.bz2
NoSource: 28
Source29: %{mirror_site}/%{ooosrc}-l10n-%{version}.tar.bz2
NoSource: 29
Source32: http://download.go-oo.org/%{srctag}/extras-3.1.tar.bz2
NoSource: 32
Source33: http://download.go-oo.org/xt/xt-20051206-src-only.zip
NoSource: 33
Source35: http://download.go-oo.org/%{srctag}/biblio.tar.bz2
NoSource: 35
Source40: http://download.go-oo.org/%{srctag}/oox.2008-08-28.tar.bz2
NoSource: 40
Source41: http://download.go-oo.org/%{srctag}/writerfilter.2008-02-29.tar.bz2
NoSource: 41
Source42: http://download.go-oo.org/DEV300/scsolver.2008-10-30.tar.bz2
NoSource: 42
Source43: http://download.go-oo.org/DEV300/ooo_oxygen_images-2009-06-17.tar.gz
NoSource: 43
Source50: http://multidimalgorithm.googlecode.com/files/mdds_0.3.0.tar.bz2
NoSource: 50
# This can be made by mingw32-* but it is not available in Momonga.
# This dll should exist for SDK (see http://www.mail-archive.com/dev@openoffice.org/msg04589.html etc.)
Source60: http://tools.openoffice.org/unowinreg_prebuild/680/unowinreg.dll
NoSource: 60
Source100: Momonga.conf.in
Source101: Momonga64.conf.in
Source103: ja.po.updated
# extra localisation files (*.sdf) should be renamed to GSI_<localecode>.sdf and put into %{_sourcedir}
# Source201-206 are extracted from sdf files in 'OOo_2.4.0_patches_curvirgo.zip' at http://waooo.sourceforge.jp/
# It is needed to apply corresponding patches (patches-waooo-<num>- in patches)
Source201: sdf-waooo-4-Add_Postacrd_Japan_and_BJIS_priority_up.sdf
Source202: sdf-waooo-8-ContextMenu_FontList_Disable.sdf
Source203: sdf-waooo-9-i35579_sc_enhanced_autofilter.sdf
Source204: sdf-waooo-11-Add_SortList_Japanese.sdf
Source205: sdf-waooo-find_attributes_fix_2.0.3rc5.sdf
Source206: sdf-waooo-sw_split_cell_dialog.sdf

# need LibreOffice 
Source502: http://hg.services.openoffice.org/binaries/0b49ede71c21c0599b0cc19b353a6cb3-README_apache-commons.txt
Source503: http://hg.services.openoffice.org/binaries/68dd2e8253d9a7930e9fd50e2d7220d0-hunspell-1.2.9.tar.gz
Source504: http://hg.services.openoffice.org/binaries/128cfc86ed5953e57fe0f5ae98b62c2e-libtextcat-2.2.tar.gz
Source505: http://hg.services.openoffice.org/binaries/17410483b5b5f267aa18b7e00b65e6e0-hsqldb_1_8_0.zip
Source506: http://hg.services.openoffice.org/binaries/1756c4fa6c616ae15973c104cd8cb256-Adobe-Core35_AFMs-314.tar.gz
Source507: http://hg.services.openoffice.org/binaries/18f577b374d60b3c760a3a3350407632-STLport-4.5.tar.gz
Source508: http://hg.services.openoffice.org/binaries/1f24ab1d39f4a51faf22244c94a6203f-xmlsec1-1.2.14.tar.gz
Source509: http://hg.services.openoffice.org/binaries/24be19595acad0a2cae931af77a0148a-LICENSE_source-9.0.0.7-bj.html
Source510: http://hg.services.openoffice.org/binaries/26b3e95ddf3d9c077c480ea45874b3b8-lp_solve_5.5.tar.gz
Source512: http://hg.services.openoffice.org/binaries/2a177023f9ea8ec8bd00837605c5df1b-jakarta-tomcat-5.0.30-src.tar.gz
Source513: http://hg.services.openoffice.org/binaries/2ae988b339daec234019a7066f96733e-commons-lang-2.3-src.tar.gz
Source514: http://hg.services.openoffice.org/binaries/2c9b0f83ed5890af02c0df1c1776f39b-commons-httpclient-3.1-src.tar.gz
Source516: http://hg.services.openoffice.org/binaries/ca4870d899fd7e943ffc310a5421ad4d-liberation-fonts-ttf-1.06.0.20100721.tar.gz
Source517: http://hg.services.openoffice.org/binaries/35c94d2df8893241173de1d16b6034c0-swingExSrc.zip
Source518: http://hg.services.openoffice.org/binaries/35efabc239af896dfb79be7ebdd6e6b9-gentiumbasic-fonts-1.10.zip
Source519: http://hg.services.openoffice.org/binaries/377a60170e5185eb63d3ed2fae98e621-README_silgraphite-2.3.1.txt
Source520: http://hg.services.openoffice.org/binaries/39bb3fcea1514f1369fcfc87542390fd-sacjava-1.3.zip
Source521: http://hg.services.openoffice.org/binaries/3ade8cfe7e59ca8e65052644fed9fca4-epm-3.7.tar.gz
Source522: http://hg.services.openoffice.org/binaries/3c219630e4302863a9a83d0efde889db-commons-logging-1.1.1-src.tar.gz
Source523: http://hg.services.openoffice.org/binaries/48470d662650c3c074e1c3fabbc67bbd-README_source-9.0.0.7-bj.txt
Source524: http://hg.services.openoffice.org/binaries/48d8169acc35f97e05d8dcdfd45be7f2-lucene-2.3.2.tar.gz
Source527: http://hg.services.openoffice.org/binaries/599dc4cc65a07ee868cf92a667a913d2-xpdf-3.02.tar.gz
Source528: http://hg.services.openoffice.org/binaries/5aba06ede2daa9f2c11892fbd7bc3057-libserializer.zip
Source529: http://hg.services.openoffice.org/binaries/67b42915c8432abf0a922438f00860a2-libxml.zip
Source530: http://hg.services.openoffice.org/binaries/7740a8ec23878a2f50120e1faa2730f2-libxml2-2.7.6.tar.gz
Source531: http://hg.services.openoffice.org/binaries/7376930b0d3f3d77a685d94c4a3acda8-STLport-4.5-0119.tar.gz
Source532: http://hg.services.openoffice.org/binaries/79600e696a98ff95c2eba976f7a8dfbb-liblayout.zip
Source533: http://hg.services.openoffice.org/binaries/798b2ffdc8bcfe7bca2cf92b62caf685-rhino1_5R5.zip
Source534: http://hg.services.openoffice.org/binaries/ecb2e37e45c9933e2a963cabe03670ab-curl-7.19.7.tar.gz
Source535: http://hg.services.openoffice.org/binaries/8294d6c42e3553229af9934c5c0ed997-stax-api-1.0-2-sources.jar
Source536: http://hg.services.openoffice.org/binaries/8ea307d71d11140574bfb9fcc2487e33-libbase.zip
Source537: http://hg.services.openoffice.org/binaries/bd30e9cf5523cdfc019b94f5e1d7fd19-cppunit-1.12.1.tar.gz
Source538: http://hg.services.openoffice.org/binaries/a06a496d7a43cbdc35e69dbe678efadb-libloader.zip
Source539: http://hg.services.openoffice.org/binaries/a169ab152209200a7bad29a275cb0333-seamonkey-1.1.14.source.tar.gz
Source540: http://hg.services.openoffice.org/binaries/a4d9b30810a434a3ed39fc0003bbd637-LICENSE_stax-api-1.0-2-sources.html
Source541: http://hg.services.openoffice.org/binaries/a7983f859eafb2677d7ff386a023bc40-xsltml_2.1.2.zip
Source542: http://hg.services.openoffice.org/binaries/ada24d37d8d638b3d8a9985e80bc2978-source-9.0.0.7-bj.zip
Source543: http://hg.services.openoffice.org/binaries/af3c3acf618de6108d65fcdc92b492e1-commons-codec-1.3-src.tar.gz
Source544: http://hg.services.openoffice.org/binaries/ba1015b59c112d44d7797b62fe7bee51-neon-0.29.3.tar.gz
Source545: http://hg.services.openoffice.org/binaries/bc702168a2af16869201dbe91e46ae48-LICENSE_Python-2.6.1
Source546: http://hg.services.openoffice.org/binaries/c441926f3a552ed3e5b274b62e86af16-STLport-4.0.tar.gz
Source548: http://hg.services.openoffice.org/binaries/d0b5af6e408b8d2958f3d83b5244f5e8-hyphen-2.4.tar.gz
Source549: http://hg.services.openoffice.org/binaries/d1a3205871c3c52e8a50c9f18510ae12-libformula.zip
Source550: http://hg.services.openoffice.org/binaries/d35724900f6a4105550293686688bbb3-silgraphite-2.3.1.tar.gz
Source551: http://hg.services.openoffice.org/binaries/d4c4d91ab3a8e52a2e69d48d34ef4df4-core.zip
Source552: http://hg.services.openoffice.org/binaries/d70951c80dabecc2892c919ff5d07172-db-4.7.25.NC-custom.tar.gz
Source553: http://hg.services.openoffice.org/binaries/dbb3757275dc5cc80820c0b4dd24ed95-librepository.zip
Source554: http://hg.services.openoffice.org/binaries/dbd5f3b47ed13132f04c685d608a7547-jpeg-6b.tar.gz
Source555: http://hg.services.openoffice.org/binaries/e0707ff896045731ff99e99799606441-README_db-4.7.25.NC-custom.txt
Source556: http://hg.services.openoffice.org/binaries/e81c2f0953aa60f8062c05a4673f2be0-Python-2.6.1.tar.bz2
Source557: http://hg.services.openoffice.org/binaries/e61d0364a30146aaa3001296f853b2b9-libxslt-1.1.26.tar.gz
Source558: http://hg.services.openoffice.org/binaries/ea570af93c284aa9e5621cd563f54f4d-bsh-2.0b1-src.tar.gz
Source559: http://hg.services.openoffice.org/binaries/ea91f2fb4212a21d708aced277e6e85a-vigra1.4.0.tar.gz
Source560: http://hg.services.openoffice.org/binaries/ee8b492592568805593f81f8cdf2a04c-expat-2.0.1.tar.gz
Source561: http://hg.services.openoffice.org/binaries/f3e2febd267c8e4b13df00dac211dd6d-flute.zip
Source562: http://hg.services.openoffice.org/binaries/f7925ba8491fe570e5164d2c72791358-libfonts.zip
Source563: http://hg.services.openoffice.org/binaries/fb7ba5c2182be4e73748859967455455-README_stax-api-1.0-2-sources.txt
Source564: http://hg.services.openoffice.org/binaries/fca8706f2c4619e2fa3f8f42f8fc1e9d-rasqal-0.9.16.tar.gz
Source565: http://hg.services.openoffice.org/binaries/fcc6df1160753d0b8c835d17fdeeb0a7-boost_1_39_0.tar.gz
Source566: http://hg.services.openoffice.org/binaries/fdb27bfe2dbe2e7b57ae194d9bf36bab-SampleICC-1.3.2.tar.gz
Source567: http://hg.services.openoffice.org/binaries/37282537d0ed1a087b1c8f050dc812d9-dejavu-fonts-ttf-2.32.zip
Source568: http://hg.services.openoffice.org/binaries/831126a1ee5af269923cfab6050769fe-mysql-connector-cpp.zip
Source569: http://hg.services.openoffice.org/binaries/067201ea8b126597670b5eff72e1f66c-mythes-1.2.0.tar.gz
Source570: http://hg.services.openoffice.org/binaries/cf8a6967f7de535ae257fa411c98eb88-mdds_0.3.0.tar.bz2
Source571: http://download.go-oo.org/src/47e1edaa44269bc537ae8cabebb0f638-JLanguageTool-1.0.0.tar.bz2
Source572: http://download.go-oo.org/src/90401bca927835b6fbae4a707ed187c8-nlpsolver-0.9.tar.bz2
Source580: http://download.go-oo.org/extern/185d60944ea767075d27247c3162b3bc-unowinreg.dll
Source581: http://www.numbertext.org/linux/881af2b7dca9b8259abbca00bbbc004d-LinLibertineG-20110101.zip
NoSource: 502
NoSource: 503
NoSource: 504
NoSource: 505
NoSource: 506
NoSource: 507
NoSource: 508
NoSource: 509
NoSource: 510
NoSource: 512
NoSource: 513
NoSource: 514
NoSource: 516
NoSource: 517
NoSource: 518
NoSource: 519
NoSource: 520
NoSource: 521
NoSource: 522
NoSource: 523
NoSource: 524
NoSource: 527
NoSource: 528
NoSource: 529
NoSource: 530
NoSource: 531
NoSource: 532
NoSource: 533
NoSource: 534
NoSource: 535
NoSource: 536
NoSource: 537
NoSource: 538
NoSource: 539
NoSource: 540
NoSource: 541
NoSource: 542
NoSource: 543
NoSource: 544
NoSource: 545
NoSource: 546
NoSource: 548
NoSource: 549
NoSource: 550
NoSource: 551
NoSource: 552
NoSource: 553
NoSource: 554
NoSource: 555
NoSource: 556
NoSource: 557
NoSource: 558
NoSource: 559
NoSource: 560
NoSource: 561
NoSource: 562
NoSource: 563
NoSource: 564
NoSource: 565
NoSource: 566
NoSource: 567
NoSource: 568
NoSource: 569
NoSource: 570
NoSource: 571
NoSource: 572
NoSource: 580
NoSource: 581


# Patches
# patches for ooo-build
Patch0: libreoffice-build-3.3.0.3-distroconf_Momonga.patch
Patch1: libreoffice-build-3.3.2.2-patches_apply_Momonga.patch
Patch2: libreoffice-build-3.3.2.2-fix-desktop-files.patch
# no need libreoffice-3.3?
#Patch4: ooo-build-3.2.0.99.3-install_odk.patch
Patch5: ooo-build-2.0.4.1-dont_set_nonsense_CCACHEDIR.patch

Patch10: libreoffice-build-3.3.0.4-temp-fix.patch

# additional patches for OOo source
# These patches must have suffix 'diff' and can be applied in the top of a source dir (ooo-build-<VERSION>/build/OO*/) by 'patch -p0'.
# We must COPY these patches INTO ooo-build-<VERSION>/patches/src<TAG> dir.
# We also must modify patches/OOO_2_0/apply in accordance with these patches (ooo-build-2.0.x-patches_apply_Momonga.patch)
# !!! MAKE SURE TO MODIFY Patch1 (ooo-build-*-patches_apply_Momonga.patch) WHEN WE ADD/DELETE/RENAME PATCHES !!!
Patch100: default-disable-embedded-bitmap.diff
Patch101: default-color-genwidget.diff
#Patch102: momonga-splash.diff
## Following patches are created based on the ones at http://waooo.sourceforge.jp/wiki/index.php?Patches
# Patch205,206 comes from OOo_3.1.1_patches_curvirgo.zip at http://sourceforge.jp/projects/waooo/
Patch201: patch-waooo-3-helpcontent2_fix.diff
#Patch202: patch-waooo-4-Labels.xcu_merge_data_SRC680_m219.diff
Patch203: patch-waooo-5-helpcontent2_color_fix.diff
Patch204: patch-waooo-6-punctuation_and_adjust_2.3.0.diff
Patch205: patch-waooo-8-contextmenu_fontlist_disable_OOO310_m11.diff
Patch206: patch-waooo-11-add_sorlist_curvirgo_SRC680_m221.diff
## other patches
Patch300: workspace.srb1.diff

## security patches
Patch400: cws-impress178-xpm-and-gif-fix.diff
Patch401: cws-hb32showstoppers3.diff
Patch402: libxmlsec-CVE-2009-0217.diff
Patch403: openoffice.org-3.2.1-pyuno.diff

# build fixes
Patch500: %{name}-3.3.2.2-correct-fexceptions.diff
Patch501: %{name}-gcc46.diff
Patch502: %{name}-vbahelper-visibility.diff

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# build dependencies
BuildRequires: tcsh, perl, sed, zip, bzip2, unzip, tar, findutils, patch >= 2.5.9
BuildRequires: autoconf, make >= 3.79.1, flex, bison, perl-Compress-Zlib
BuildRequires: gcc >= 3.4.3, gcc-c++ >= 3.4.3, binutils, perl-Archive-Zip
BuildRequires: gstreamer-devel >= 0.10.10-1m
BuildRequires: gstreamer-plugins-base-devel >= 0.10.10-1m
BuildRequires: dbus-devel >= 0.71-2m
BuildRequires: dbus-glib >= 0.71-2m
%if %{with_java}
BuildRequires: java-1.6.0-openjdk-devel
%endif
BuildRequires: zlib-devel, freetype-devel
BuildRequires: gtk+-devel >= 2.4.8, gnome-vfs-devel, cups-devel, curl-devel >= 7.16.0
%if %{enable_kde}
BuildRequires: qt-devel >= 4.7.0, kdelibs-devel
%endif
BuildRequires: python-devel >= 2.7
BuildRequires: xulrunner-devel >= %{gecko_ver}
BuildRequires: expat-devel, libxml2-devel, openldap-devel >= 2.4.0 , neon-devel >= 0.24
BuildRequires: libidn-devel, sane-backends-devel
BuildRequires: startup-notification-devel, intltool
BuildRequires: desktop-file-utils >= 0.9, libjpeg-devel >= 8a, pam-devel, prelink
BuildRequires: libsndfile-devel, portaudio
BuildRequires: libxslt-devel, unixODBC-devel
%if %{with_openclipart}
BuildRequires: openclipart-png
%endif
BuildRequires: cairo-devel, pango-devel, bonobo-activation-devel
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXaw-devel
BuildRequires: libXcursor-devel, libXext-devel, libXfixes-devel, libXft-devel
BuildRequires: libXi-devel, libXinerama-devel, libXrandr-devel
BuildRequires: libXrender-devel, libXt-devel
# bonobo-activation-devel is provides libbonobo
BuildRequires: libicu-devel >= 4.6, raptor-devel, rasqal-devel, redland-devel
BuildRequires: libwpd-devel >= 0.9, libwpg-devel >= 0.2 , libwps-devel >= 0.2
BuildRequires: perl-DProf
BuildRequires: pentaho-reporting-flow-engine

BuildRequires: hunspell-devel hyphen-devel

BuildRequires: fontpackages-devel
BuildRequires: poppler-devel >= 0.16.0

# use 20GB for build
BuildConflicts: rhbuildsys(DiskFree) < 20480Mb

Obsoletes: openoffice.org < 3.2.1-16m
Provides: openoffice.org = %{version}-%{release}

%description
LibreOffice is an Open Source, community-developed, multi-platform
office productivity suite.  It includes the key desktop applications,
such as a word processor, spreadsheet, presentation manager, formula
editor and drawing program, with a user interface and feature set
similar to other office suites.  Sophisticated and flexible,
LibreOffice also works transparently with a variety of file
formats, including Microsoft Office.

Usage: Simply type "ooffice" to run LibreOffice or select the
requested component (Writer, Calc, Draw, Impress, etc.) from your
desktop menu. On first start a few files will be installed in the
user's home, if necessary.

The LibreOffice team hopes you enjoy working with LibreOffice!

%package core
Summary: core modules for %{name}
Group: Applications/Productivity
Requires: %{name}-ure = %{version}-%{release}
Requires: %{name}-%{fontname}-fonts = %{version}-%{release}
Requires: neon >= 0.24
Requires: startup-notification, portaudio, libsndfile
Requires: gtk+ >= 2.4.8, atk, libbonobo, pango, openldap
Requires: libstdc++, zlib, popt
Requires: freetype, libpng, libjpeg, python, openssl, libxml2
Requires: cyrus-sasl, openldap, libpaper
Requires: dbus >= 0.92
Requires: bash-completion
Requires: hunspell-en, hyphen-en, hyphen >= 2.4, autocorr-en
Requires(post): desktop-file-utils >= 0.9
Requires(postun): desktop-file-utils >= 0.9
%if ! %{enable_kde}
Obsoletes: openoffice.org-kde
%endif
Obsoletes: openoffice.org-libs
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-extradict
Obsoletes: openoffice.org-langpack-eo
Obsoletes: openoffice.org-langpack-tn
Obsoletes: openoffice.org-langpack-ts
%if ! %{with_java}
Obsoletes: openoffice.org-javafilter
%endif

Obsoletes: openoffice.org-core < 3.2.1-16m
Provides: openoffice.org-core = %{version}-%{release}

%description core
core libraries and support files for %{name}.

%if %{enable_kde}
%package kde
Summary: KDE integration files for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: qt, kdelibs
Obsoletes: openoffice.org-kde < 3.2.1-16m
Provides: openoffice.org-kde = %{version}-%{release}

%description kde
KDE integration files for %{name}.
%endif

%package pyuno
Summary: python bindings for %{name}
Group: Development/Libraries
Requires: %{name}-core = %{version}-%{release}
Requires: python
Obsoletes: openoffice.org-pyuno < 3.2.1-16m
Provides: openoffice.org-pyuno = %{version}-%{release}

%description pyuno
Cool python bindings for the %{name} UNO component model. Allows scripts both
external to %{name} and within the internal %{name} scripting module to be
written in python.

%package base
Summary: database frontend for %{name}
Group: Development/Libraries
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-base < 3.2.1-16m
Provides: openoffice.org-base = %{version}-%{release}

%description base
Gui database frontend for %{name}. Allows creation and management of databases
through a GUI.

%package writer
Summary: writer module for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: libwpd >= 0.8.0
Obsoletes: openoffice.org-writer < 3.2.1-16m
Provides: openoffice.org-writer = %{version}-%{release}

%description writer
wordprocessor application of %{name}.

%if 1
%package emailmerge
Summary: email mail merge component for %{name}
Group: Applications/Productivity
Requires: %{name}-writer = %{version}-%{release}
Requires: %{name}-pyuno = %{version}-%{release}
Obsoletes: openoffice.org-emailmerge < 3.2.1-16m
Provides: openoffice.org-emailmerge = %{version}-%{release}

%description emailmerge
enables %{name} writer module to enable mail merge to email.
%endif

%package calc
Summary: calc module for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-calc < 3.2.1-16m
Provides: openoffice.org-calc = %{version}-%{release}

%description calc
spreadsheet application of %{name}.

%package draw
Summary: draw module for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-draw < 3.2.1-16m
Provides: openoffice.org-draw = %{version}-%{release}

%description draw
drawing application of %{name}.

%package impress
Summary: impress module for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-impress < 3.2.1-16m
Provides: openoffice.org-impress = %{version}-%{release}

%description impress
presentation application of %{name}.

%package math
Summary: math module for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-math < 3.2.1-16m
Provides: openoffice.org-math = %{version}-%{release}

%description math
math editor of %{name}.

%package graphicfilter
Summary: extra graphicfilter module for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-graphicfilter < 3.2.1-16m
Provides: openoffice.org-graphicfilter = %{version}-%{release}

%description graphicfilter
graphicfilter module for %{name}, provides additional svg and flash
export filters.

%package xsltfilter
Summary: extra xsltfilter module for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-xsltfilter < 3.2.1-16m
Provides: openoffice.org-xsltfilter = %{version}-%{release}

%description xsltfilter
xsltfilter module for %{name}, provides additional docbook and xhtml
export transforms. Install this to enable docbook export.

%if %{with_java}
%package javafilter
Summary: extra javafilter module for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-javafilter < 3.2.1-16m
Provides: openoffice.org-javafilter = %{version}-%{release}

%description javafilter
javafilter module for %{name}, provides additional aportisdoc, pocket
excel and pocket word import filters.
%endif

%package testtools
Summary: testtools for %{name}
Group: Development/Libraries
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-testtools < 3.2.1-16m
Provides: openoffice.org-testtools = %{version}-%{release}

%description testtools
QA tools for %{name}, enables automated testing.

%package langpack-af_ZA
Summary: Afrikaans language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-af, hyphen-af, autocorr-af
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-af < 3.2.1-16m
Obsoletes: openoffice.org-langpack-af_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-af_ZA = %{version}-%{release}

%description langpack-af_ZA
Provides additional afrikaans translations for %{name}.

%package langpack-ar
Summary: Arabic language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Arabic
Obsoletes: openoffice.org-langpack-ar < 3.2.1-16m
Provides: openoffice.org-langpack-ar = %{version}-%{release}

%description langpack-ar
Provides additional arabic translations for %{name}.

%package langpack-as_IN
Summary: Assamese language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-as, hyphen-as
Requires: lohit-bengali-fonts
Obsoletes: openoffice.org-langpack-as_IN < 3.2.1-16m
Provides: openoffice.org-langpack-as_IN = %{version}-%{release}

%description langpack-as_IN
Provides additional Assamese translations and resources for %{name}.

%package langpack-bg_BG
Summary: Bulgarian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-bg, hyphen-bg, mythes-bg, autocorr-bg
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-bg < 3.2.1-16m
Obsoletes: openoffice.org-langpack-bg_BG < 3.2.1-16m
Provides: openoffice.org-langpack-bg_BG = %{version}-%{release}

%description langpack-bg_BG
Provides additional bulgarian translations for %{name}.

%package langpack-bn_IN
Summary: Bengali language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-bn, hyphen-bn
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-bn_IN < 3.2.1-16m
Provides: openoffice.org-langpack-bn_IN = %{version}-%{release}

%description langpack-bn_IN
Provides additional bengali translations for %{name}.

%package langpack-ca_ES
Summary: Catalan language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ca, hyphen-ca, mythes-ca
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-ca < 3.2.1-16m
Obsoletes: openoffice.org-i18n-Catalan
Obsoletes: openoffice.org-langpack-ca_ES < 3.2.1-16m
Provides: openoffice.org-langpack-ca_ES = %{version}-%{release}

%description langpack-ca_ES
Provides additional catalan translations for %{name}.

%package langpack-cs_CZ
Summary: Czech language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-cs, hyphen-cs, mythes-cs, autocorr-cs
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-cs < 3.2.1-16m
Obsoletes: openoffice.org-i18n-Czech
Obsoletes: openoffice.org-langpack-cs_CZ < 3.2.1-16m
Provides: openoffice.org-langpack-cs_CZ = %{version}-%{release}

%description langpack-cs_CZ
Provides additional czech translations for %{name}.

%package langpack-cy_GB
Summary: Welsh language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-cy, hyphen-cy
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-cy < 3.2.1-16m
Obsoletes: openoffice.org-langpack-cy_GB < 3.2.1-16m
Provides: openoffice.org-langpack-cy = %{version}-%{release}
Provides: openoffice.org-langpack-cy_GB = %{version}-%{release}

%description langpack-cy_GB
Provides additional welsh translations for %{name}.

%package langpack-da_DK
Summary: Danish language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-da, hyphen-da, mythes-da, autocorr-da
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-da < 3.2.1-16m
Obsoletes: openoffice.org-i18n-Danish
Obsoletes: openoffice.org-langpack-da_DK < 3.2.1-16m
Provides: openoffice.org-langpack-da_DK = %{version}-%{release}

%description langpack-da_DK
Provides additional danish translations for %{name}.

%package langpack-de
Summary: German language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-de, hyphen-de, mythes-de, autocorr-de
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-German
Obsoletes: openoffice.org-langpack-de < 3.2.1-16m
Provides: openoffice.org-langpack-de = %{version}-%{release}

%description langpack-de
Provides additional german translations for %{name}.

%package langpack-dz
Summary: Dzongkha language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: jomolhari-fonts
Obsoletes: openoffice.org-langpack-dz < 3.2.1-16m
Provides: openoffice.org-langpack-dz = %{version}-%{release}

%description langpack-dz
Provides additional Dzongkha translations and resources for %{name}.

%package langpack-el_GR
Summary: Greek language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-el, hyphen-el, mythes-el
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-el < 3.2.1-16m
Obsoletes: openoffice.org-i18n-Greek
Obsoletes: openoffice.org-langpack-el_GR < 3.2.1-16m
Provides: openoffice.org-langpack-el_GR = %{version}-%{release}

%description langpack-el_GR
Provides additional greek translations for %{name}.

%package langpack-es
Summary: Spanish language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-es, hyphen-es, mythes-es, autocorr-es
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Spanish
Obsoletes: openoffice.org-langpack-es < 3.2.1-16m
Provides: openoffice.org-langpack-es = %{version}-%{release}

%description langpack-es
Provides additional spanish translations for %{name}.

%package langpack-et_EE
Summary: Estonian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-et, hyphen-et
Obsoletes: openoffice.org-langpack-et < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Estonian
Obsoletes: openoffice.org-langpack-et_EE < 3.2.1-16m
Provides: openoffice.org-langpack-et_EE = %{version}-%{release}

%description langpack-et_EE
Provides additional estonian translations for %{name}.

%package langpack-eu_ES
Summary: Basque language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-eu, hyphen-eu, autocorr-eu
Obsoletes: openoffice.org-langpack-eu
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-eu_ES < 3.2.1-16m
Provides: openoffice.org-langpack-eu_ES = %{version}-%{release}

%description langpack-eu_ES
Provides additional basque translations for %{name}.

%package langpack-fi_FI
Summary: Finish language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: autocorr-fi
Obsoletes: openoffice.org-langpack-fi
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Finnish
Obsoletes: openoffice.org-langpack-fi_FI < 3.2.1-16m
Provides: openoffice.org-langpack-fi_FI = %{version}-%{release}

%description langpack-fi_FI
Provides additional finish translations for %{name}.

%package langpack-fr
Summary: French language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-fr, hyphen-fr, mythes-fr, autocorr-fr
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-French
Obsoletes: openoffice.org-langpack-fr < 3.2.1-16m
Provides: openoffice.org-langpack-fr = %{version}-%{release}

%description langpack-fr
Provides additional french translations for %{name}.

%package langpack-ga_IE
Summary: Irish language pack for %{name}
Group: Applications/Productivity
Requires: hunspell-ga, hyphen-ga, mythes-ga
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-langpack-ga_IE < 3.2.1-16m
Provides: openoffice.org-langpack-ga_IE = %{version}-%{release}

%description langpack-ga_IE
Provides additional Irish translations and resources for %{name}.

%package langpack-gl_ES
Summary: Galician language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-gl, hyphen-gl
Obsoletes: openoffice.org-langpack-gl < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-gl_ES < 3.2.1-16m
Provides: openoffice.org-langpack-gl_ES = %{version}-%{release}

%description langpack-gl_ES
Provides additional galician translations for %{name}.

%package langpack-gu_IN
Summary: Gujarati language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-gu, hyphen-gu
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-gu_IN < 3.2.1-16m
Provides: openoffice.org-langpack-gu_IN = %{version}-%{release}

%description langpack-gu_IN
Provides additional gujarati translations for %{name}.

%package langpack-he_IL
Summary: Hebrew language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-he
Requires: culmus-nachlieli-clm-fonts
Obsoletes: openoffice.org-langpack-he < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-he_IL < 3.2.1-16m
Provides: openoffice.org-langpack-he_IL = %{version}-%{release}

%description langpack-he_IL
Provides additional hebrew translations for %{name}.

%package langpack-hi_IN
Summary: Hindi language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-hi, hyphen-hi
Requires: lohit-hindi-fonts
Obsoletes: openoffice.org-langpack-hi-IN < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-hi_IN < 3.2.1-16m
Provides: openoffice.org-langpack-hi_IN = %{version}-%{release}

%description langpack-hi_IN
Provides additional hindi translations for %{name}.

%package langpack-hu_HU
Summary: Hungarian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-hu, hyphen-hu, mythes-hu, autocorr-hu
Obsoletes: openoffice.org-langpack-hu < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Hungarian
Obsoletes: openoffice.org-langpack-hu_HU < 3.2.1-16m
Provides: openoffice.org-langpack-hu_HU = %{version}-%{release}

%description langpack-hu_HU
Provides additional hungarian translations for %{name}.

%package langpack-hr_HR
Summary: Croatian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-hr, hyphen-hr, autocorr-hr
Obsoletes: openoffice.org-langpack-hr < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-hr_HR < 3.2.1-16m
Provides: openoffice.org-langpack-hr_HR = %{version}-%{release}

%description langpack-hr_HR
Provides additional croatian translations for %{name}.

%package langpack-it
Summary: Italian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-it, hyphen-it, mythes-it, autocorr-it
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Italian
Obsoletes: openoffice.org-langpack-it < 3.2.1-16m
Provides: openoffice.org-langpack-it = %{version}-%{release}

%description langpack-it
Provides additional italian translations for %{name}.

%package langpack-ja_JP
Summary: Japanese language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: autocorr-ja
Obsoletes: openoffice.org-langpack-ja < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Japanese
Obsoletes: openoffice.org-langpack-ja_JP < 3.2.1-16m
Provides: openoffice.org-langpack-ja_JP = %{version}-%{release}

%description langpack-ja_JP
Provides additional japanese translations for %{name}.

%package langpack-kn_IN
Summary: Kannada language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-kn, hyphen-kn
Requires: lohit-kannada-fonts
Obsoletes: openoffice.org-langpack-kn_IN < 3.2.1-16m
Provides: openoffice.org-langpack-kn_IN = %{version}-%{release}

%description langpack-kn_IN
Provides additional Kannada translations and resources for %{name}.

%package langpack-ko_KR
Summary: Korean language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ko, autocorr-ko
Requires: baekmuk-ttf-gulim-fonts
Obsoletes: openoffice.org-langpack-ko < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Korean
Obsoletes: openoffice.org-langpack-ko_KR < 3.2.1-16m
Provides: openoffice.org-langpack-ko_KR = %{version}-%{release}

%description langpack-ko_KR
Provides additional korean translations for %{name}.

%package langpack-lt_LT
Summary: Lithuanian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-lt, hyphen-lt
Obsoletes: openoffice.org-langpack-lt < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-lt_LT < 3.2.1-16m
Provides: openoffice.org-langpack-lt_LT = %{version}-%{release}

%description langpack-lt_LT
Provides additional lithuanian translations for %{name}.

%package langpack-mai_IN
Summary: Maithili language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: lohit-maithili-fonts
Requires: hunspell-mai
Obsoletes: openoffice.org-langpack-mai_IN < 3.2.1-16m
Provides: openoffice.org-langpack-mai_IN = %{version}-%{release}

%description langpack-mai_IN
Provides additional Maithili translations and resources for %{name}.

%package langpack-ml_IN
Summary: Malayalam language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ml, hyphen-ml
Requires: smc-meera-fonts
Obsoletes: openoffice.org-langpack-ml_IN < 3.2.1-16m
Provides: openoffice.org-langpack-ml_IN = %{version}-%{release}

%description langpack-ml_IN
Provides additional Malayalam translations and resources for %{name}.

%package langpack-mr_IN
Summary: Marathi language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-mr, hyphen-mr
Requires: lohit-marathi-fonts
Obsoletes: openoffice.org-langpack-mr_IN < 3.2.1-16m
Provides: openoffice.org-langpack-mr_IN = %{version}-%{release}

%description langpack-mr_IN
Provides additional Marathi translations and resources for %{name}.

%package langpack-ms_MY
Summary: Malay language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ms
Obsoletes: openoffice.org-langpack-ms < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-ms_MY < 3.2.1-16m
Provides: openoffice.org-langpack-ms_MY = %{version}-%{release}

%description langpack-ms_MY
Provides additional malay translations for %{name}.

%package langpack-nb_NO
Summary: Bokmal language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-nb, hyphen-nb, mythes-nb
Obsoletes: openoffice.org-langpack-nb < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-nb_NO < 3.2.1-16m
Provides: openoffice.org-langpack-nb_NO = %{version}-%{release}

%description langpack-nb_NO
Provides additional bokmal translations for %{name}.

%package langpack-ne_NP
Summary: Nepali language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-langpack-nb < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-ne_NP < 3.2.1-16m
Provides: openoffice.org-langpack-ne_NP = %{version}-%{release}

%description langpack-ne_NP
Provides additional Nepali translations for %{name}.

%package langpack-nl
Summary: Dutch language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-nl, hyphen-nl, mythes-nl, autocorr-nl
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Dutch
Obsoletes: openoffice.org-langpack-nl < 3.2.1-16m
Provides: openoffice.org-langpack-nl = %{version}-%{release}

%description langpack-nl
Provides additional dutch translations for %{name}.

%package langpack-nn_NO
Summary: Nynorsk language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-nn, hyphen-nn, mythes-nn
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-nn < 3.2.1-16m
Obsoletes: openoffice.org-langpack-nn_NO < 3.2.1-16m
Provides: openoffice.org-langpack-nn_NO = %{version}-%{release}

%description langpack-nn_NO
Provides additional Nynorsk translations and resources for %{name}.

%package langpack-nr_ZA
Summary: Southern Ndebele language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-nr
Obsoletes: openoffice.org-langpack-nr_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-nr_ZA = %{version}-%{release}

%description langpack-nr_ZA
Provides additional Southern Ndebele translations and resources for 
%{name}.

%package langpack-nso_ZA
Summary: Northern Sotho language pack for LibreOffice
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-nso
Obsoletes: openoffice.org-langpack-nso_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-nso_ZA = %{version}-%{release}

%description langpack-nso_ZA
Provides additional Northern Sotho translations and resources for 
%{name}.

%package langpack-or_IN
Summary: Oriya language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-or, hyphen-or
Requires: lohit-oriya-fonts
Obsoletes: openoffice.org-langpack-or_IN < 3.2.1-16m
Provides: openoffice.org-langpack-or_IN = %{version}-%{release}

%description langpack-or_IN
Provides additional Oriya translations and resources for %{name}.

%package langpack-pa_IN
Summary: Punjabi language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-pa, hyphen-pa
Requires: lohit-punjabi-fonts
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-pa_IN < 3.2.1-16m
Provides: openoffice.org-langpack-pa_IN = %{version}-%{release}

%description langpack-pa_IN
Provides additional punjabi translations for %{name}.

%package langpack-pl_PL
Summary: Polish language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-pl, hyphen-pl, mythes-pl, autocorr-pl
Obsoletes: openoffice.org-langpack-pl < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Polish
Obsoletes: openoffice.org-langpack-pl_PL < 3.2.1-16m
Provides: openoffice.org-langpack-pl_PL = %{version}-%{release}

%description langpack-pl_PL
Provides additional polish translations for %{name}.

%package langpack-pt_PT
Summary: Portuguese language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-pt, hyphen-pt, mythes-pt, autocorr-pt
Obsoletes: openoffice.org-langpack-pt < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Portuguese
Obsoletes: openoffice.org-langpack-pt_PT < 3.2.1-16m
Provides: openoffice.org-langpack-pt_PT = %{version}-%{release}

%description langpack-pt_PT
Provides additional portuguese translations for %{name}.

%package langpack-pt_BR
Summary: Brazilian Portuguese language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-pt, hyphen-pt, mythes-pt, autocorr-pt
Obsoletes: openoffice.org-langpack-pt-BR < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-pt_BR < 3.2.1-16m
Provides: openoffice.org-langpack-pt_BR = %{version}-%{release}

%description langpack-pt_BR
Provides additional brazilian portuguese translations for %{name}.

%package langpack-ro
Summary: Romanian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ro, hyphen-ro, mythes-ro
Obsoletes: openoffice.org-langpack-ro < 3.2.1-16m
Provides: openoffice.org-langpack-ro = %{version}-%{release}

%description langpack-ro
Provides additional Romanian translations and resources for %{name}.

%package langpack-ru
Summary: Russian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ru, hyphen-ru, mythes-ru, autocorr-ru
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Russian
Obsoletes: openoffice.org-langpack-ru < 3.2.1-16m
Provides: openoffice.org-langpack-ru = %{version}-%{release}

%description langpack-ru
Provides additional russian translations for %{name}.

%package langpack-sk_SK
Summary: Slovak language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-sk, hyphen-sk, mythes-sk, autocorr-sk
Obsoletes: openoffice.org-langpack-sk < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Slovak
Obsoletes: openoffice.org-langpack-sk_SK < 3.2.1-16m
Provides: openoffice.org-langpack-sk_SK = %{version}-%{release}

%description langpack-sk_SK
Provides additional slovak translations for %{name}.

%package langpack-sl_SI
Summary: Slovenian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-sl, hyphen-sl, mythes-sl, autocorr-sl
Obsoletes: openoffice.org-langpack-sl < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Slovenian
Obsoletes: openoffice.org-langpack-sl_SI < 3.2.1-16m
Provides: openoffice.org-langpack-sl_SI = %{version}-%{release}

%description langpack-sl_SI
Provides additional slovenian translations for %{name}.

%package langpack-sr
Summary: Serbian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-sr, hyphen-sr, autocorr-sr
Obsoletes: openoffice.org-langpack-sr_CS < 3.2.1-16m
Obsoletes: openoffice.org-langpack-sr < 3.2.1-16m
Provides: openoffice.org-langpack-sr = %{version}-%{release}

%description langpack-sr
Provides additional Serbian translations and resources for %{name}.

%package langpack-ss_ZA
Summary: Swati language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ss
Obsoletes: openoffice.org-langpack-ss_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-ss_ZA = %{version}-%{release}

%description langpack-ss_ZA
Provides additional Swati translations and resources for %{name}.

%package langpack-st_ZA
Summary: Southern Sotho language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-st
Obsoletes: openoffice.org-langpack-st_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-st_ZA = %{version}-%{release}

%description langpack-st_ZA
Provides additional Southern Sotho translations and resources for 
%{name}.

%package langpack-sv
Summary: Swedish language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-sv, hyphen-sv, mythes-sv, autocorr-sv
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Swedish
Obsoletes: openoffice.org-langpack-sv < 3.2.1-16m
Provides: openoffice.org-langpack-sv = %{version}-%{release}

%description langpack-sv
Provides additional swedish translations for %{name}.

%package langpack-ta_IN
Summary: Tamil language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ta, hyphen-ta
Requires: lohit-tamil-fonts
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-ta_IN < 3.2.1-16m
Provides: openoffice.org-langpack-ta_IN = %{version}-%{release}

%description langpack-ta_IN
Provides additional Tamil translations and resources for %{name}.

%package langpack-te_IN
Summary: Telugu language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-te, hyphen-te
Requires: lohit-telugu-fonts
Obsoletes: openoffice.org-langpack-te_IN < 3.2.1-16m
Provides: openoffice.org-langpack-te_IN = %{version}-%{release}

%description langpack-te_IN
Provides additional Telugu translations and resources for %{name}.

%package langpack-th_TH
Summary: Thai language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-th
Requires: thai-scalable-fonts-compat
Obsoletes: openoffice.org-langpack-th < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-th_TH < 3.2.1-16m
Provides: openoffice.org-langpack-th_TH = %{version}-%{release}

%description langpack-th_TH
Provides additional thai translations for %{name}.

%package langpack-tn_ZA
Summary: Tswana language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-tn
Obsoletes: openoffice.org-langpack-tn < 1:2.0.3
Obsoletes: openoffice.org-langpack-tn_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-tn_ZA = %{version}-%{release}

%description langpack-tn_ZA
Provides additional Tswana translations and resources for %{name}.

%package langpack-tr_TR
Summary: Turkish language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: autocorr-tr
Obsoletes: openoffice.org-langpack-tr < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Turkish
Obsoletes: openoffice.org-langpack-tr_TR < 3.2.1-16m
Provides: openoffice.org-langpack-tr_TR = %{version}-%{release}

%description langpack-tr_TR
Provides additional turkish translations for %{name}.

%package langpack-ts_ZA
Summary: Tsonga language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ts
Obsoletes: openoffice.org-langpack-ts < 1:2.0.3
Obsoletes: openoffice.org-langpack-ts_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-ts_ZA = %{version}-%{release}

%description langpack-ts_ZA
Provides additional Tsonga translations and resources for %{name}.

%package langpack-uk
Summary: Ukrainian language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-uk, hyphen-uk, mythes-uk
Obsoletes: openoffice.org-langpack-uk < 3.2.1-16m
Provides: openoffice.org-langpack-uk = %{version}-%{release}

%description langpack-uk
Provides additional Ukrainian translations and resources for %{name}.

%package langpack-ur
Summary: Urdu language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: paktype-naqsh-fonts, paktype-tehreer-fonts
Requires: hunspell-ur
Obsoletes: openoffice.org-langpack-ur < 3.2.1-16m
Provides: openoffice.org-langpack-ur = %{version}-%{release}

%description langpack-ur
Provides additional Urdu translations and resources for %{name}.

%package langpack-ve_ZA
Summary: Venda language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-ve
Obsoletes: openoffice.org-langpack-ve_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-ve_ZA = %{version}-%{release}

%description langpack-ve_ZA
Provides additional Venda translations and resources for %{name}.

%package langpack-xh_ZA
Summary: Xhosa language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-xh
Obsoletes: openoffice.org-langpack-xh_ZA
Provides: openoffice.org-langpack-xh_ZA = %{version}-%{release}

%description langpack-xh_ZA
Provides additional Xhosa translations and resources for %{name}.

%package langpack-zh_CN
Summary: Simplified Chinese language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: autocorr-zh
Obsoletes: openoffice.org-langpack-zh-CN < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Chinese
Obsoletes: openoffice.org-langpack-zh_CN < 3.2.1-16m
Provides: openoffice.org-langpack-zh_CN = %{version}-%{release}

%description langpack-zh_CN
Provides additional simplified chinese translations for %{name}.

%package langpack-zh_TW
Summary: Traditional Chinese language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: autocorr-zh
Obsoletes: openoffice.org-langpack-zh-TW < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-i18n-Chinese-Big5
Obsoletes: openoffice.org-langpack-zh_TW < 3.2.1-16m
Provides: openoffice.org-langpack-zh_TW = %{version}-%{release}

%description langpack-zh_TW
Provides additional traditional chinese translations for %{name}.

%package langpack-zu_ZA
Summary: Zulu language pack for %{name}
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: hunspell-zu, hyphen-zu
Obsoletes: openoffice.org-langpack-zu < 3.2.1-16m
Obsoletes: openoffice.org-i18n
Obsoletes: openoffice.org-langpack-zu_ZA < 3.2.1-16m
Provides: openoffice.org-langpack-zu_ZA = %{version}-%{release}

%description langpack-zu_ZA
Provides additional zulu translations for %{name}.

%package devel
Summary: LibreOffice SDK
Group: Development/Libraries
Requires: %{name}-core = %{version}-%{release}
Obsoletes: openoffice.org-devel < 3.2.1-16m
Provides: openoffice.org-devel = %{version}-%{release}

%description devel
LibreOffice SDK.

%package devel-doc
Summary: LibreOffice SDK documentation
Group: Documentation
Requires: %{name}-devel = %{version}-%{release}
Obsoletes: openoffice.org-devel-doc < 3.2.1-16m
Provides: openoffice.org-devel-doc = %{version}-%{release}

%description devel-doc
LibreOffice SDK documentation.

%package ure
Summary: UNO Runtime Environment
Group: Development/Libraries
Requires: unzip, jre >= 1.5.0
Obsoletes: openoffice.org-ure < 3.2.1-16m
Provides: openoffice.org-ure = %{version}-%{release}

%description ure
UNO is the component model of LibreOffice. UNO offers interoperability
between programming languages, other components models and hardware
architectures, either in process or over process boundaries, in the Intranet
as well as in the Internet. UNO components may be implemented in and accessed
from any programming language for which a UNO implementation (AKA language
binding) and an appropriate bridge or adapter exists

%package report-builder
Summary: Create database reports from LibreOffice
Group: Applications/Productivity
Requires: pentaho-reporting-flow-engine
Requires: %{name}-base = %{version}-%{release}
Requires(pre):    openoffice.org-core
Requires(post):   openoffice.org-core
Requires(preun):  openoffice.org-core
Requires(postun): openoffice.org-core
Obsoletes: openoffice.org-report-builder < 3.2.1-16m
Provides: openoffice.org-report-builder = %{version}-%{release}

%description report-builder
Creates database reports from LibreOffice databases. The report builder can
define group and page headers as well as group, page footers and calculation
fields to accomplish complex database reports.

%package wiki-publisher
Summary: Create Wiki articles on MediaWiki servers with LibreOffice
Group: Applications/Productivity
Requires: %{name}-writer = %{version}-%{release}
Requires: jakarta-commons-codec, jakarta-commons-httpclient
Requires: jakarta-commons-lang, jakarta-commons-logging
Requires(pre):    openoffice.org-core
Requires(post):   openoffice.org-core
Requires(preun):  openoffice.org-core
Requires(postun): openoffice.org-core
Obsoletes: openoffice.org-wiki-publisher < 3.2.1-16m
Provides: openoffice.org-wiki-publisher = %{version}-%{release}

%description wiki-publisher
The Wiki Publisher enables you to create Wiki articles on MediaWiki servers
without having to know the syntax of the MediaWiki markup language. Publish
your new and existing documents transparently with writer to a wiki page.

%package presentation-minimizer
Summary: Shrink LibreOffice presentations
Group: Applications/Productivity
Requires: %{name}-impress = %{version}-%{release}
Requires(pre):    openoffice.org-core
Requires(post):   openoffice.org-core
Requires(preun):  openoffice.org-core
Requires(postun): openoffice.org-core
Obsoletes: openoffice.org-presentation-minimizer < 3.2.1-16m
Provides: openoffice.org-presentation-minimizer = %{version}-%{release}

%description presentation-minimizer
The Presentation Minimizer is used to reduce the file size of the current
presentation. Images will be compressed, and data that is no longer needed will
be removed.

%package presenter-screen
Summary: Presenter Screen for LibreOffice Presentations
Group: Applications/Productivity
Requires: %{name}-impress = %{version}-%{release}
Requires(pre):    openoffice.org-core
Requires(post):   openoffice.org-core
Requires(preun):  openoffice.org-core
Requires(postun): openoffice.org-core
Obsoletes: openoffice.org-presenter-screen < 3.2.1-16m
Provides: openoffice.org-presenter-screen = %{version}-%{release}

%description presenter-screen
The Presenter Screen is used to provides information on a second screen, that
typically is not visible to the audience when delivering a presentation. e.g.
slide notes.

%package pdfimport
Summary: PDF Importer for LibreOffice Draw
Group: Applications/Productivity
Requires: %{name}-draw = %{version}-%{release}
Requires(pre):    openoffice.org-core
Requires(post):   openoffice.org-core
Requires(preun):  openoffice.org-core
Requires(postun): openoffice.org-core
Obsoletes: openoffice.org-pdfimport < 3.2.1-16m
Provides: openoffice.org-pdfimport = %{version}-%{release}

%description pdfimport
The PDF Importer imports PDF into drawing documents to preserve layout
and enable basic editing of PDF documents.

%package %{fontname}-fonts
Summary: LibreOffice dingbats font
Group: User Interface/X
Requires: fontpackages-filesystem
BuildArch: noarch
Obsoletes: openoffice.org-%{fontname}-fonts < 3.2.1-16m
Provides: openoffice.org-%{fontname}-fonts = %{version}-%{release}

%description %{fontname}-fonts
A dingbats font, OpenSymbol, suitable for use by LibreOffice for bullets and
mathematical symbols. 

%package -n autocorr-en
Summary: English autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-en < 3.2.1-16m
Provides: openoffice.org-autocorr-en = %{version}-%{release}

%description -n autocorr-en
Rules for autocorrecting common English typing errors.

%package -n autocorr-af
Summary: Afrikaans autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-af < 3.2.1-16m
Provides: openoffice.org-autocorr-af = %{version}-%{release}

%description -n autocorr-af
Rules for autocorrecting common Afrikaans typing errors.

%package -n autocorr-bg
Summary: Bulgarian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-bg < 3.2.1-16m
Provides: openoffice.org-autocorr-bg = %{version}-%{release}

%description -n autocorr-bg
Rules for autocorrecting common Bulgarian typing errors.

%package -n autocorr-cs
Summary: Czech autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-cs < 3.2.1-16m
Provides: openoffice.org-autocorr-cs = %{version}-%{release}

%description -n autocorr-cs
Rules for autocorrecting common Czech typing errors.

%package -n autocorr-da
Summary: Danish autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-da < 3.2.1-16m
Provides: openoffice.org-autocorr-da = %{version}-%{release}

%description -n autocorr-da
Rules for autocorrecting common Danish typing error.

%package -n autocorr-de
Summary: German autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-de < 3.2.1-16m
Provides: openoffice.org-autocorr-de = %{version}-%{release}

%description -n autocorr-de
Rules for autocorrecting common German typing errors.

%package -n autocorr-es
Summary: Spanish autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-es < 3.2.1-16m
Provides: openoffice.org-autocorr-es = %{version}-%{release}

%description -n autocorr-es
Rules for autocorrecting common Spanish typing errors.

%package -n autocorr-eu
Summary: Basque autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-eu < 3.2.1-16m
Provides: openoffice.org-autocorr-eu = %{version}-%{release}

%description -n autocorr-eu
Rules for autocorrecting common Basque typing errors.

%package -n autocorr-fa
Summary: Farsi autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-fa < 3.2.1-16m
Provides: openoffice.org-autocorr-fa = %{version}-%{release}

%description -n autocorr-fa
Rules for autocorrecting common Farsi typing errors.

%package -n autocorr-fi
Summary: Finnish autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-fi < 3.2.1-16m
Provides: openoffice.org-autocorr-fi = %{version}-%{release}

%description -n autocorr-fi
Rules for autocorrecting common Finnish typing errors.

%package -n autocorr-fr
Summary: French autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-fr < 3.2.1-16m
Provides: openoffice.org-autocorr-fr = %{version}-%{release}

%description -n autocorr-fr
Rules for autocorrecting common French typing errors.

%package -n autocorr-ga
Summary: Irish autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-ga < 3.2.1-16m
Provides: openoffice.org-autocorr-ga = %{version}-%{release}

%description -n autocorr-ga
Rules for autocorrecting common Irish typing errors.

%package -n autocorr-hu
Summary: Hungarian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-hu < 3.2.1-16m
Provides: openoffice.org-autocorr-hu = %{version}-%{release}

%description -n autocorr-hu
Rules for autocorrecting common Hungarian typing errors.

%package -n autocorr-hr
Summary: Croatian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-hr < 3.2.1-16m
Provides: openoffice.org-autocorr-hr = %{version}-%{release}

%description -n autocorr-hr
Rules for autocorrecting common croatian typing errors.

%package -n autocorr-it
Summary: Italian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-it < 3.2.1-16m
Provides: openoffice.org-autocorr-it = %{version}-%{release}

%description -n autocorr-it
Rules for autocorrecting common Italian typing errors.

%package -n autocorr-ja
Summary: Japanese autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-ja < 3.2.1-16m
Provides: openoffice.org-autocorr-ja = %{version}-%{release}

%description -n autocorr-ja
Rules for autocorrecting common Japanese typing errors.

%package -n autocorr-ko
Summary: Korean autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-ko < 3.2.1-16m
Provides: openoffice.org-autocorr-ko = %{version}-%{release}

%description -n autocorr-ko
Rules for autocorrecting common Korean typing errors.

%package -n autocorr-lb
Summary: Luxembourgish autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-lb < 3.2.1-16m
Provides: openoffice.org-autocorr-lb = %{version}-%{release}

%description -n autocorr-lb
Rules for autocorrecting common Luxembourgish typing errors.

%package -n autocorr-lt
Summary: Lithuanian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-lt < 3.2.1-16m
Provides: openoffice.org-autocorr-lt = %{version}-%{release}

%description -n autocorr-lt
Rules for autocorrecting common Lithuanian typing errors.

%package -n autocorr-mn
Summary: Mongolian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-mn < 3.2.1-16m
Provides: openoffice.org-autocorr-mn = %{version}-%{release}

%description -n autocorr-mn
Rules for autocorrecting common Mongolian typing errors.

%package -n autocorr-nl
Summary: Dutch autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-nl < 3.2.1-16m
Provides: openoffice.org-autocorr-nl = %{version}-%{release}

%description -n autocorr-nl
Rules for autocorrecting common Dutch typing errors.

%package -n autocorr-pl
Summary: Polish autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-pl < 3.2.1-16m
Provides: openoffice.org-autocorr-pl = %{version}-%{release}

%description -n autocorr-pl
Rules for autocorrecting common Polish typing errors.

%package -n autocorr-pt
Summary: Portuguese autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-pt < 3.2.1-16m
Provides: openoffice.org-autocorr-pt = %{version}-%{release}

%description -n autocorr-pt
Rules for autocorrecting common Portuguese typing errors.

%package -n autocorr-ru
Summary: Russian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-ru < 3.2.1-16m
Provides: openoffice.org-autocorr-ru = %{version}-%{release}

%description -n autocorr-ru
Rules for autocorrecting common Russian typing errors.

%package -n autocorr-sk
Summary: Slovak autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-sk < 3.2.1-16m
Provides: openoffice.org-autocorr-sk = %{version}-%{release}

%description -n autocorr-sk
Rules for autocorrecting common Slovak typing errors.

%package -n autocorr-sl
Summary: Slovenian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-sl < 3.2.1-16m
Provides: openoffice.org-autocorr-sl = %{version}-%{release}

%description -n autocorr-sl
Rules for autocorrecting common Slovenian typing errors.

%package -n autocorr-sr
Summary: Serbian autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-sr < 3.2.1-16m
Provides: openoffice.org-autocorr-sr = %{version}-%{release}

%description -n autocorr-sr
Rules for autocorrecting common Serbian typing errors.

%package -n autocorr-sv
Summary: Swedish autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-sv < 3.2.1-16m
Provides: openoffice.org-autocorr-sv = %{version}-%{release}

%description -n autocorr-sv
Rules for autocorrecting common Swedish typing errors.

%package -n autocorr-tr
Summary: Turkish autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-tr < 3.2.1-16m
Provides: openoffice.org-autocorr-tr = %{version}-%{release}

%description -n autocorr-tr
Rules for autocorrecting common Turkish typing errors.

%package -n autocorr-vi
Summary: Vietnamese autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-vi < 3.2.1-16m
Provides: openoffice.org-autocorr-vi = %{version}-%{release}

%description -n autocorr-vi
Rules for autocorrecting common Vietnamese typing errors.

%package -n autocorr-zh
Summary: Chinese autocorrection rules
Group: Applications/Text
BuildArch: noarch
Obsoletes: openoffice.org-autocorr-zh < 3.2.1-16m
Provides: openoffice.org-autocorr-zh = %{version}-%{release}

%description -n autocorr-zh
Rules for autocorrecting common Chinese typing errors.

%prep
%setup -q -n %{ooobuildname}-%{ooobuildver}
cp -f %{SOURCE100} distro-configs/
cp -f %{SOURCE101} distro-configs/
cp -f %{SOURCE103} po/ja.po

# prepare extra localization file (Patch205/Source202 is not needed and not applied now) by a command "cat *.sdf %{_sourcedir}/>GSI_ja.sdf"
cat %{SOURCE201} %{SOURCE203} %{SOURCE204} %{SOURCE205} %{SOURCE206} > %{_sourcedir}/GSI_ja.sdf

%patch0 -p1 -b .distroconf
%patch1 -p1 -b .patches_apply
%patch2 -p1 -b .fix-menu-entries
# %patch4 -p1 -b .install_odk
# %patch5 -p1 -b .ccache

%patch10 -p1 -b .ccache

cp -f %{PATCH100} %{PATCH101} %{PATCH201} %{PATCH203} %{PATCH204} %{PATCH205} %{PATCH206} patches/dev300
cp -f %{PATCH300} %{PATCH400} %{PATCH401} %{PATCH402} %{PATCH403} %{PATCH500} %{PATCH501} patches/dev300
cp -f %{PATCH502} patches/dev300

%build
# csh cannot recognize LS_COLORS' variable: "su"
CURRENT_LS_COLORS=$LS_COLORS
unset LS_COLORS

%if %{enable_kde}
export QT4INC="%{_includedir}" QT4DIR="%{_libdir}/qt4" QT4LIB="%{_libdir}" KDE4DIR="%{_libdir}/kde4"
%endif
./configure \
    --prefix=%{_prefix} \
    --libdir=%{_libdir} \
    --datadir=%{_datadir} \
    --sysconfdir=%{_sysconfdir} \
    --mandir=%{_mandir} \
    --with-distro="%{distro}" \
    --with-vendor="Momonga Project" \
    --with-srcdir=%{_sourcedir} \
    --with-installed-ooo-dirname=%{ooodirname} \
    --without-binsuffix \
    --enable-odk \
    --enable-openxml \
    --disable-post-install-scripts \
    --enable-lockdown \
    --disable-mono \
    --disable-access \
    --enable-xsltproc \
    --without-nas \
    --with-system-hunspell \
    --with-system-icu \
    --with-system-libwpd \
    --with-system-libwpg \
    --with-system-libwps \
    --with-system-icu \
    --without-system-db \
    --with-system-mozilla \
    --with-firefox \
    --with-xulrunner \
    --with-system-raptor \
    --with-system-rasqal \
    --with-system-redland \
    --with-system-portaudio \
    --enable-split-app-modules \
    --enable-split-opt-features \
    --disable-evolution2 \
    --with-num-cpus="%{_numjobs}" \
%if %{with_openclipart}
    --with-openclipart=%{openclipartdir} \
%endif
    --disable-kde \
%if ! %{enable_kde}
    --disable-kde4 \
%endif
    --enable-cairo \
    --with-system-cairo \
    %{java_arg} %{withlang} \
    --with-system-mesa-headers \
    --enable-opengl \
    --enable-ogltrans \
    --enable-gstreamer \
    --enable-symbols \
    --enable-dbus \
    --enable-minimizer \
    --enable-presenter-console \
    --enable-pdfimport \
    --enable-wiki-publisher \
    --enable-vba \
    --enable-report-builder \
    --with-system-jfreereport \
%if %{use_ccache}
    --with-gcc-speedup=ccache \
%endif
    --with-num-cpus=`echo  %{?_smp_mflags} | sed 's/^\-j//g' | awk '{print $1}'`

export RPM_OPT_FLAGS="$RPM_OPT_FLAGS -fpermissive"

# Compile problems with these ... (from Gentoo ebuild) ... Really?
for i in $RPM_OPT_FLAGS; do
        case "$i" in
                -O?|-funroll-loops|-fomit-frame-pointer|-fprefetch-loop-arrays|-fno-default-inline|-fstack-protector|-ftracer|-ffast-math) continue;;
        esac
        ARCH_FLAGS="$ARCH_FLAGS $i"
done
export ARCH_FLAGS

#make dev-tool
make build.tools

# ./download

make
LS_COLORS=$CURRENT_LS_COLORS

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

make DESTDIR=%{buildroot} install

# From FC
# We don't need to carry around all the letter templates for all the languages
# in each langpack! In addition, all the bitmaps are the same!
#pushd %{buildroot}/%{instdir}/%{basis_name}/share/template/
#mkdir -p wizard
#for I in %{langpack_langs}; do
#	cp -af $I/wizard/bitmap wizard/
#	rm -rf $I/wizard/bitmap
#	ln -sf ../../wizard/bitmap $I/wizard/bitmap
#
#	if [ -d $I/wizard/letter/$I ]; then
#		mv -f $I/wizard/letter/$I ${I}_wizard_letter_${I}
#		rm -rf $I/wizard/letter/*
#		mv -f ${I}_wizard_letter_${I} $I/wizard/letter/$I
#	else
#		rm -rf $I/wizard/letter/*
#	fi
#done
#popd

# From FC
# use system file dialog (not common)
# perl -p -i -e "s/<value>false<\/value>/<value>true<\/value>/g" %{buildroot}/%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-UseOOoFileDialogs.xcu

# readmes and licenses to doc
rm -rf readmes.OOo licenses.OOo
mv %{buildroot}/%{instdir}/readmes readmes.OOo
#mv %{buildroot}/%{instdir}/licenses licenses.OOo

# From FC
#remove the email mailmerge impl if packageing without it
%if 0
rm -rf %{buildroot}/%{instdir}/%{basis_name}/program/mailmerge.py*
#rm -rf %{buildroot}/%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Writer/Writer-javamail.xcu
%endif

# "man ooffice", "man openoffice.org" and "man ooo" should work, I think
#pushd %{buildroot}%{_mandir}/man1
#cp -a openoffice.1 ooffice.1
#cp -a openoffice.1 openoffice.org.1
#cp -a openoffice.1 ooo.1
#popd

# chmod (permissions of almost all files are 444)
pushd %{buildroot}/%{instdir}
chmod -R u+w *
cd program
chmod +x *.so*
popd

# fix symlink for SDK
(cd %{buildroot}/%{instdir}/%{basis_name}/sdk
rm -f classes
ln -s ../../../../..%{_datadir}/%{ooodirname}/sdk/classes classes
rm -f docs
ln -s ../../../../..%{_datadir}/doc/packages/LibreOffice/sdk/docs docs
rm -f idl
ln -s ../../../../..%{_datadir}/idl/%{ooodirname} idl
rm -f include
ln -s ../../../../..%{_includedir}/%{ooodirname} include
rm -f index.html
ln -s ../../../../..%{_datadir}/doc/packages/LibreOffice/sdk/index.html index.html
)

# fix csh path
(cd %{buildroot}/%{instdir}/%{basis_name}/sdk
sed -i -e "s:/usr/bin/csh:/bin/csh:g" setsdkenv_unix.csh
sed -i -e "s:/usr/bin/csh:/bin/csh:g" setsdkenv_unix.csh.in
)

# it could be better to make samples dir for each lang to avoid an error when selecting a sample icon in an open new dialog
pushd %{buildroot}/%{instdir}/%{basis_name}/share/samples
for I in %{langpack_langs}; do
    if [ ! -d $I ];then
	mkdir $I
    fi
done
popd

mkdir -p %{buildroot}%{instdir}/extensions
mkdir -p %{buildroot}%{_datadir}/openoffice.org/extensions

# unpack report-builder extension
install -d -m 755 %{buildroot}%{_datadir}/openoffice.org/extensions/report-builder.oxt
# FIXME
#unzip build/%{ooosrc}/solver/%{OFFICEUPD}/unxlng*/bin/report-builder.oxt -d %{buildroot}%{_datadir}/openoffice.org/extensions/report-builder.oxt

# unpack wiki-publisher extension
install -d -m 755 %{buildroot}%{_datadir}/openoffice.org/extensions/wiki-publisher.oxt
# FIXME
#unzip build/%{ooosrc}/solver/%{OFFICEUPD}/unxlng*/bin/swext/wiki-publisher.oxt -d %{buildroot}%{_datadir}/openoffice.org/extensions/wiki-publisher.oxt

# unpack presentation-minimizer extension
install -d -m 755 %{buildroot}%{instdir}/extensions/presentation-minimizer.oxt
# FIXME
#unzip build/%{ooosrc}/solver/%{OFFICEUPD}/unxlng*/bin/minimizer/presentation-minimizer.oxt -d %{buildroot}%{instdir}/extensions/presentation-minimizer.oxt
#chmod -x %{buildroot}%{instdir}/extensions/presentation-minimizer.oxt/help/component.txt

# unpack presenter screen extension
install -d -m 755 %{buildroot}%{instdir}/extensions/presenter-screen.oxt
# FIXME
#unzip build/%{ooosrc}/solver/%{OFFICEUPD}/unxlng*/bin/presenter/presenter-screen.oxt -d %{buildroot}%{instdir}/extensions/presenter-screen.oxt
#chmod -x %{buildroot}%{instdir}/extensions/presenter-screen.oxt/help/component.txt

# unpack pdfimport extension
install -d -m 755 %{buildroot}%{instdir}/extensions/pdfimport.oxt
# FIXME
#unzip build/%{ooosrc}/solver/%{OFFICEUPD}/unxlng*/bin/pdfimport/pdfimport.oxt -d %{buildroot}%{instdir}/extensions/pdfimport.oxt
#chmod -x %{buildroot}%{instdir}/extensions/pdfimport.oxt/help/component.txt

# rhbz#477435 package opensymbol separately
pushd %{buildroot}%{instdir}/%{basis_name}/share/fonts/truetype
install -d -m 0755 %{buildroot}%{_fontdir}
install -p -m 0644 *.ttf %{buildroot}%{_fontdir}
popd
rm -rf %{buildroot}%{instdir}/%{basis_name}/share/fonts

rm -rf  %{buildroot}%{instdir}/%{basis_name}/share/autocorr/acor_sh*
%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%_font_pkg -n %{fontname} opens___.ttf

%files
# metapackage

%if %{langpacks}

%files langpack-af_ZA
%defattr(-,root,root,-)
# no help when --without-java
%{instdir}/%{basis_name}/help/af
#%%{instdir}/%{basis_name}/program/resource/*680af.res
#%%{instdir}/share/dict/ooo/README_af_ZA.txt
#%%{instdir}/share/dict/ooo/af_*.*
#%%{instdir}/share/dict/ooo/hyph_af_ZA.dic
#%{instdir}/share/readme/LICENSE_af*
#%{instdir}/share/readme/README_af*
%{instdir}/program/resource/*af.res
%{instdir}/%{basis_name}/program/resource/*af.res
%{instdir}/%{basis_name}/share/autotext/af
#%{instdir}/%{basis_name}/share/layout/af
#%{instdir}/%{basis_name}/share/registry/res/af
%{instdir}/%{basis_name}/share/template/af
%{instdir}/%{basis_name}/share/samples/af
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-af.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-af.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_af.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_af.xcd

%files langpack-ar
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/ar
#%%{instdir}/%{basis_name}/program/resource/*680ar.res
#%%{instdir}/share/dict/ooo/ar_*.*
#%{instdir}/share/readme/LICENSE_ar*
#%{instdir}/share/readme/README_ar*
%{instdir}/program/resource/*ar.res
%{instdir}/%{basis_name}/program/resource/*ar.res
%{instdir}/%{basis_name}/share/autotext/ar
#%{instdir}/%{basis_name}/share/layout/ar
#%{instdir}/%{basis_name}/share/registry/res/ar
%{instdir}/%{basis_name}/share/template/ar
%{instdir}/%{basis_name}/share/samples/ar
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ar.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_ar.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ar.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ar.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ar.xcd

%files langpack-as_IN
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_as*
#%{instdir}/share/readme/README_as*
%{instdir}/program/resource/oooas.res
%{instdir}/%{basis_name}/help/as
%{instdir}/%{basis_name}/program/resource/*as.res
%{instdir}/%{basis_name}/share/autotext/as
#%{instdir}/%{basis_name}/share/layout/as
#%{instdir}/%{basis_name}/share/registry/res/as
%{instdir}/%{basis_name}/share/template/as
%{instdir}/%{basis_name}/share/samples/as
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-as.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-as.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_as.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_as.xcd

%files langpack-bg_BG
%defattr(-,root,root,-)
# no help when --without-java
%{instdir}/%{basis_name}/help/bg
#%%{instdir}/%{basis_name}/program/resource/*680bg.res
#%%{instdir}/share/dict/ooo/bg_BG.*
#%{instdir}/share/readme/LICENSE_bg*
#%{instdir}/share/readme/README_bg*
%{instdir}/program/resource/*bg.res
%{instdir}/%{basis_name}/program/resource/*bg.res
%{instdir}/%{basis_name}/share/autotext/bg
#%{instdir}/%{basis_name}/share/layout/bg
#%{instdir}/%{basis_name}/share/registry/res/bg
%{instdir}/%{basis_name}/share/template/bg
%{instdir}/%{basis_name}/share/samples/bg
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-bg.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-bg.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_bg.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_bg.xcd

%files langpack-bn_IN
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/bn
#%%{instdir}/%{basis_name}/program/resource/*680bn.res
#%%{instdir}/share/dict/ooo/bn_IN.*
#%{instdir}/share/readme/LICENSE_bn*
#%{instdir}/share/readme/README_bn*
%{instdir}/program/resource/*bn.res
%{instdir}/%{basis_name}/program/resource/*bn.res
%{instdir}/%{basis_name}/share/autotext/bn
#%{instdir}/%{basis_name}/share/layout/bn
#%{instdir}/%{basis_name}/share/registry/res/bn
%{instdir}/%{basis_name}/share/template/bn
%{instdir}/%{basis_name}/share/samples/bn
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-bn.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-bn.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_bn.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_bn.xcd

%files langpack-ca_ES
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/ca
#%%{instdir}/%{basis_name}/program/resource/*680ca.res
#%%{instdir}/share/dict/ooo/ca_ES.*
#%{instdir}/share/readme/LICENSE_ca*
#%{instdir}/share/readme/README_ca*
%{instdir}/program/resource/*ca.res
%{instdir}/%{basis_name}/program/resource/*ca.res
%{instdir}/%{basis_name}/share/autotext/ca
#%{instdir}/%{basis_name}/share/layout/ca
#%{instdir}/%{basis_name}/share/registry/res/ca
%{instdir}/%{basis_name}/share/template/ca
%{instdir}/%{basis_name}/share/samples/ca
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ca.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ca.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ca.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ca.xcd

%files langpack-cs_CZ
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/cs
#%%{instdir}/%{basis_name}/program/resource/*680cs.res
#%%{instdir}/share/dict/ooo/cs_CZ.*
#%%{instdir}/share/dict/ooo/th_cs_CZ_*
#%{instdir}/share/readme/LICENSE_cs*
#%{instdir}/share/readme/README_cs*
%{instdir}/program/resource/*cs.res
%{instdir}/%{basis_name}/program/resource/*cs.res
%{instdir}/%{basis_name}/share/autotext/cs
#%{instdir}/%{basis_name}/share/layout/cs
#%{instdir}/%{basis_name}/share/registry/res/cs
%{instdir}/%{basis_name}/share/template/cs
%{instdir}/%{basis_name}/share/samples/cs
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-cs.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-cs.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_cs.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_cs.xcd

%files langpack-cy_GB
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/cy
%{instdir}/program/resource/ooocy.res
#%%{instdir}/share/dict/ooo/cy_GB.*
#%{instdir}/share/readme/LICENSE_cy*
#%{instdir}/share/readme/README_cy*
%{instdir}/%{basis_name}/program/resource/*cy.res
%{instdir}/%{basis_name}/share/autotext/cy
#%{instdir}/%{basis_name}/share/layout/cy
#%{instdir}/%{basis_name}/share/registry/res/cy
%{instdir}/%{basis_name}/share/template/cy
%{instdir}/%{basis_name}/share/samples/cy
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-cy.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-cy.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_cy.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_cy.xcd

%files langpack-da_DK
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/da
#%%{instdir}/%{basis_name}/program/resource/*680da.res
#%%{instdir}/share/dict/ooo/da_DK.*
#%%{instdir}/share/dict/ooo/hyph_da_DK.dic
#%{instdir}/share/readme/LICENSE_da*
#%{instdir}/share/readme/README_da*
%{instdir}/program/resource/*da.res
%{instdir}/%{basis_name}/program/resource/*da.res
%{instdir}/%{basis_name}/share/autotext/da
#%{instdir}/%{basis_name}/share/layout/da
#%{instdir}/%{basis_name}/share/registry/res/da
%{instdir}/%{basis_name}/share/template/da
%{instdir}/%{basis_name}/share/samples/da
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-da.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-da.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_da.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_da.xcd

%files langpack-de
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/de
#%%{instdir}/%{basis_name}/program/resource/*680de.res
#%%{instdir}/share/dict/ooo/README*_de_*.txt
#%%{instdir}/share/dict/ooo/de_*.*
#%%{instdir}/share/dict/ooo/hyph_de_DE.dic
#%%{instdir}/share/dict/ooo/th_de_*_v2.*
#%{instdir}/share/readme/LICENSE_de*
#%{instdir}/share/readme/README_de*
%{instdir}/program/resource/*de.res
%{instdir}/%{basis_name}/program/resource/*de.res
%{instdir}/%{basis_name}/share/autotext/de
#%{instdir}/%{basis_name}/share/layout/de
#%{instdir}/%{basis_name}/share/registry/res/de
%{instdir}/%{basis_name}/share/template/de
%{instdir}/%{basis_name}/share/samples/de
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-de.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-de.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_de.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_de.xcd

%files langpack-dz
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_dz*
#%{instdir}/share/readme/README_dz*
%{instdir}/program/resource/ooodz.res
%{instdir}/%{basis_name}/help/dz
%{instdir}/%{basis_name}/program/resource/*dz.res
%{instdir}/%{basis_name}/share/autotext/dz
#%{instdir}/%{basis_name}/share/layout/dz
#%{instdir}/%{basis_name}/share/registry/res/dz
%{instdir}/%{basis_name}/share/template/dz
%{instdir}/%{basis_name}/share/samples/dz
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-dz.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_dz.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-dz.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_dz.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_dz.xcd

%files langpack-el_GR
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/el
#%%{instdir}/%{basis_name}/program/resource/*680el.res
#%%{instdir}/share/dict/ooo/el_GR.*
#%{instdir}/share/readme/LICENSE_el*
#%{instdir}/share/readme/README_el*
%{instdir}/%{basis_name}/program/resource/*el.res
%{instdir}/program/resource/*el.res
%{instdir}/%{basis_name}/share/autotext/el
#%{instdir}/%{basis_name}/share/layout/el
#%{instdir}/%{basis_name}/share/registry/res/el
%{instdir}/%{basis_name}/share/template/el
%{instdir}/%{basis_name}/share/samples/el
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-el.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-el.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_el.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_el.xcd

%files langpack-es
%defattr(-,root,root,-)
# no help when --without-java
%{instdir}/%{basis_name}/help/es
#%%{instdir}/%{basis_name}/program/resource/*680es.res
#%%{instdir}/share/dict/ooo/README_es_ES.txt
#%%{instdir}/share/dict/ooo/es_*.*
#%{instdir}/share/readme/LICENSE_es*
#%{instdir}/share/readme/README_es*
%{instdir}/program/resource/*es.res
%{instdir}/%{basis_name}/program/resource/*es.res
%{instdir}/%{basis_name}/share/autotext/es
#%{instdir}/%{basis_name}/share/layout/es
#%{instdir}/%{basis_name}/share/registry/res/es
%{instdir}/%{basis_name}/share/template/es
%{instdir}/%{basis_name}/share/samples/es
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-es.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-es.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_es.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_es.xcd

%files langpack-et_EE
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/et
#%%{instdir}/%{basis_name}/program/resource/*680et.res
#%%{instdir}/share/dict/ooo/README*_et_EE.txt
#%%{instdir}/share/dict/ooo/et_EE.*
#%%{instdir}/share/dict/ooo/hyph_et_EE.dic
#%{instdir}/share/readme/LICENSE_et*
#%{instdir}/share/readme/README_et*
%{instdir}/program/resource/*et.res
%{instdir}/%{basis_name}/program/resource/*et.res
%{instdir}/%{basis_name}/share/autotext/et
#%{instdir}/%{basis_name}/share/layout/et
#%{instdir}/%{basis_name}/share/registry/res/et
%{instdir}/%{basis_name}/share/template/et
%{instdir}/%{basis_name}/share/samples/et
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-et.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-et.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_et.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_et.xcd

%files langpack-eu_ES
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/eu
#%%{instdir}/%{basis_name}/program/resource/*680eu.res
#%{instdir}/share/readme/LICENSE_eu*
#%{instdir}/share/readme/README_eu*
%{instdir}/program/resource/*eu.res
%{instdir}/%{basis_name}/program/resource/*eu.res
%{instdir}/%{basis_name}/share/autotext/eu
#%{instdir}/%{basis_name}/share/layout/eu
#%{instdir}/%{basis_name}/share/registry/res/eu
%{instdir}/%{basis_name}/share/template/eu
%{instdir}/%{basis_name}/share/samples/eu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-eu.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-eu.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_eu.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_eu.xcd

%files langpack-fi_FI
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/fi
#%%{instdir}/%{basis_name}/program/resource/*680fi.res
#%{instdir}/share/dict/ooo/*fi_*
#%{instdir}/share/readme/LICENSE_fi*
#%{instdir}/share/readme/README_fi*
%{instdir}/program/resource/*fi.res
%{instdir}/%{basis_name}/program/resource/*fi.res
%{instdir}/%{basis_name}/share/autotext/fi
#%{instdir}/%{basis_name}/share/layout/fi
#%{instdir}/%{basis_name}/share/registry/res/fi
%{instdir}/%{basis_name}/share/template/fi
%{instdir}/%{basis_name}/share/samples/fi
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-fi.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-fi.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_fi.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_fi.xcd

%files langpack-fr
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/fr
#%%{instdir}/%{basis_name}/program/resource/*680fr.res
#%%{instdir}/share/dict/ooo/README_fr_FR.txt
#%%{instdir}/share/dict/ooo/fr_*.*
#%{instdir}/share/readme/LICENSE_fr*
#%{instdir}/share/readme/README_fr*
%{instdir}/program/resource/*fr.res
%{instdir}/%{basis_name}/program/resource/*fr.res
%{instdir}/%{basis_name}/share/autotext/fr
#%{instdir}/%{basis_name}/share/layout/fr
#%{instdir}/%{basis_name}/share/registry/res/fr
%{instdir}/%{basis_name}/share/template/fr
%{instdir}/%{basis_name}/share/samples/fr
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-fr.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-fr.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_fr.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_fr.xcd

%files langpack-ga_IE
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ga*
#%{instdir}/share/readme/README_ga*
%{instdir}/program/resource/oooga.res
%{instdir}/%{basis_name}/help/ga
%{instdir}/%{basis_name}/program/resource/*ga.res
%{instdir}/%{basis_name}/share/autotext/ga
#%{instdir}/%{basis_name}/share/layout/ga
#%{instdir}/%{basis_name}/share/registry/res/ga
%{instdir}/%{basis_name}/share/template/ga
%{instdir}/%{basis_name}/share/samples/ga
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ga.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ga.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ga.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ga.xcd

%files langpack-gl_ES
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/gl
#%%{instdir}/%{basis_name}/program/resource/*680gl.res
#%%{instdir}/share/dict/ooo/gl_ES.*
#%{instdir}/share/readme/LICENSE_gl*
#%{instdir}/share/readme/README_gl*
%{instdir}/program/resource/*gl.res
%{instdir}/%{basis_name}/program/resource/*gl.res
%{instdir}/%{basis_name}/share/autotext/gl
#%{instdir}/%{basis_name}/share/layout/gl
#%{instdir}/%{basis_name}/share/registry/res/gl
%{instdir}/%{basis_name}/share/template/gl
%{instdir}/%{basis_name}/share/samples/gl
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-gl.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-gl.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_gl.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_gl.xcd

%files langpack-gu_IN
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/gu-IN
#%%{instdir}/%{basis_name}/program/resource/*680gu-IN.res
#%{instdir}/share/readme/LICENSE_gu-IN*
#%{instdir}/share/readme/README_gu-IN*
%{instdir}/program/resource/*gu-IN.res
%{instdir}/%{basis_name}/program/resource/*gu-IN.res
%{instdir}/%{basis_name}/share/autotext/gu-IN
#%{instdir}/%{basis_name}/share/layout/gu-IN
#%{instdir}/%{basis_name}/share/registry/res/gu-IN
%{instdir}/%{basis_name}/share/template/gu-IN
%{instdir}/%{basis_name}/share/samples/gu-IN
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-gu-IN.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-gu-IN.xcd
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_gu-IN.xcu
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_gu-IN.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_gu-IN.xcd

%files langpack-he_IL
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/he
#%%{instdir}/%{basis_name}/program/resource/*680he.res
#%{instdir}/share/dict/ooo/*he_IL*
#%{instdir}/share/readme/LICENSE_he*
#%{instdir}/share/readme/README_he*
%{instdir}/program/resource/*he.res
%{instdir}/%{basis_name}/program/resource/*he.res
%{instdir}/%{basis_name}/share/autotext/he
#%{instdir}/%{basis_name}/share/layout/he
#%{instdir}/%{basis_name}/share/registry/res/he
%{instdir}/%{basis_name}/share/template/he
%{instdir}/%{basis_name}/share/samples/he
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-he.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-he.xcd
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_he.xcu
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_he.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_he.xcd

%files langpack-hi_IN
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/hi-IN
#%%{instdir}/%{basis_name}/program/resource/*680hi-IN.res
#%%{instdir}/share/dict/ooo/hi_IN.*
#%{instdir}/share/readme/LICENSE_hi-IN*
#%{instdir}/share/readme/README_hi-IN*
%{instdir}/program/resource/*hi-IN.res
%{instdir}/%{basis_name}/program/resource/*hi-IN.res
%{instdir}/%{basis_name}/share/autotext/hi-IN
#%{instdir}/%{basis_name}/share/layout/hi-IN
#%{instdir}/%{basis_name}/share/registry/res/hi-IN
%{instdir}/%{basis_name}/share/template/hi-IN
%{instdir}/%{basis_name}/share/samples/hi-IN
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-hi-IN.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-hi-IN.xcd
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_hi-IN.xcu
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_hi-IN.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_hi-IN.xcd

%files langpack-hu_HU
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/hu
#%%{instdir}/%{basis_name}/program/resource/*680hu.res
#%%{instdir}/share/dict/ooo/README*_hu_HU.txt
#%%{instdir}/share/dict/ooo/hu_HU.*
#%%{instdir}/share/dict/ooo/hyph_hu_HU.dic
#%{instdir}/share/readme/LICENSE_hu*
#%{instdir}/share/readme/README_hu*
%{instdir}/program/resource/*hu.res
%{instdir}/%{basis_name}/program/resource/*hu.res
%{instdir}/%{basis_name}/share/autotext/hu
#%{instdir}/%{basis_name}/share/layout/hu
#%{instdir}/%{basis_name}/share/registry/res/hu
%{instdir}/%{basis_name}/share/template/hu
%{instdir}/%{basis_name}/share/samples/hu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-hu.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-hu.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_hu.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_hu.xcd

%files langpack-hr_HR
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/hr
#%%{instdir}/%{basis_name}/program/resource/*680hr.res
#%%{instdir}/share/dict/ooo/hr_HR.*
#%{instdir}/share/readme/LICENSE_hr*
#%{instdir}/share/readme/README_hr*
%{instdir}/program/resource/*hr.res
%{instdir}/%{basis_name}/program/resource/*hr.res
%{instdir}/%{basis_name}/share/autotext/hr
#%{instdir}/%{basis_name}/share/layout/hr
#%{instdir}/%{basis_name}/share/registry/res/hr
%{instdir}/%{basis_name}/share/template/hr
%{instdir}/%{basis_name}/share/samples/hr
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-hr.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-hr.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_hr.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_hr.xcd

%files langpack-it
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/it
#%%{instdir}/%{basis_name}/program/resource/*680it.res
#%%{instdir}/share/dict/ooo/README*_it_IT*.txt
#%%{instdir}/share/dict/ooo/it_*.*
#%%{instdir}/share/dict/ooo/hyph_it_IT.dic
#%%{instdir}/share/dict/ooo/th_it_IT_v2.*
#%{instdir}/share/readme/LICENSE_it*
#%{instdir}/share/readme/README_it*
%{instdir}/program/resource/*it.res
%{instdir}/%{basis_name}/program/resource/*it.res
%{instdir}/%{basis_name}/share/autotext/it
#%{instdir}/%{basis_name}/share/layout/it
#%{instdir}/%{basis_name}/share/registry/res/it
%{instdir}/%{basis_name}/share/template/it
%{instdir}/%{basis_name}/share/samples/it
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-it.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-it.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_it.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_it.xcd

%files langpack-ja_JP
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/ja
#%%{instdir}/%{basis_name}/program/resource/*680ja.res
#%{instdir}/share/readme/LICENSE_ja*
#%{instdir}/share/readme/README_ja*
%{instdir}/program/resource/*ja.res
%{instdir}/share/registry/cjk_ja.xcd
%{instdir}/%{basis_name}/program/resource/*ja.res
%{instdir}/%{basis_name}/share/autotext/ja
#%{instdir}/%{basis_name}/share/layout/ja
#%{instdir}/%{basis_name}/share/registry/res/ja
%{instdir}/%{basis_name}/share/template/ja
%{instdir}/%{basis_name}/share/samples/ja
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ja.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ja.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ja.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ja.xcd
#%{instdir}/share/registry/modules/org/openoffice/Office/Common/Common-cjk_ja.xcu
#%{instdir}/share/registry/modules/org/openoffice/Office/Writer/Writer-cjk_ja.xcu

%files langpack-kn_IN
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_kn*
#%{instdir}/share/readme/README_kn*
%{instdir}/program/resource/oookn.res
%{instdir}/%{basis_name}/help/kn
%{instdir}/%{basis_name}/program/resource/*kn.res
%{instdir}/%{basis_name}/share/autotext/kn
#%{instdir}/%{basis_name}/share/layout/kn
#%{instdir}/%{basis_name}/share/registry/res/kn
%{instdir}/%{basis_name}/share/template/kn
%{instdir}/%{basis_name}/share/samples/kn
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-kn.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-kn.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_kn.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_kn.xcd

%files langpack-ko_KR
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/ko
#%%{instdir}/%{basis_name}/program/resource/*680ko.res
#%{instdir}/share/readme/LICENSE_ko*
#%{instdir}/share/readme/README_ko*
%{instdir}/share/registry/cjk_ko.xcd
%{instdir}/share/registry/korea.xcd
%{instdir}/program/resource/*ko.res
%{instdir}/%{basis_name}/program/resource/*ko.res
%{instdir}/%{basis_name}/share/autotext/ko
#%{instdir}/%{basis_name}/share/layout/ko
#%{instdir}/%{basis_name}/share/registry/res/ko
%{instdir}/%{basis_name}/share/template/ko
%{instdir}/%{basis_name}/share/samples/ko
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ko.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ko.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ko.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ko.xcd
#%{instdir}/share/registry/modules/org/openoffice/Office/Common/Common-cjk_ko.xcu
#%{instdir}/share/registry/modules/org/openoffice/Office/Common/Common-korea.xcu
#%{instdir}/share/registry/modules/org/openoffice/Office/Writer/Writer-cjk_ko.xcu

%files langpack-lt_LT
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/lt
#%%{instdir}/%{basis_name}/program/resource/*680lt.res
#%%{instdir}/share/dict/ooo/README*_lt_LT.txt
#%%{instdir}/share/dict/ooo/lt_LT.*
#%%{instdir}/share/dict/ooo/hyph_lt_LT.dic
#%{instdir}/share/readme/LICENSE_lt*
#%{instdir}/share/readme/README_lt*
%{instdir}/program/resource/*lt.res
%{instdir}/%{basis_name}/program/resource/*lt.res
%{instdir}/%{basis_name}/share/autotext/lt
#%{instdir}/%{basis_name}/share/layout/lt
#%{instdir}/%{basis_name}/share/registry/res/lt
%{instdir}/%{basis_name}/share/template/lt
%{instdir}/%{basis_name}/share/samples/lt
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-lt.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-lt.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_lt.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_lt.xcd

%files langpack-mai_IN
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_mai*
#%{instdir}/share/readme/README_mai*
%{instdir}/program/resource/ooomai.res
%{instdir}/%{basis_name}/help/mai
%{instdir}/%{basis_name}/program/resource/*mai.res
%{instdir}/%{basis_name}/share/autotext/mai
#%{instdir}/%{basis_name}/share/layout/mai
#%{instdir}/%{basis_name}/share/registry/res/mai
%{instdir}/%{basis_name}/share/template/mai
%{instdir}/%{basis_name}/share/samples/mai
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-mai.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-mai.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_mai.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_mai.xcd

%files langpack-ml_IN
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ml*
#%{instdir}/share/readme/README_ml*
%{instdir}/program/resource/oooml.res
%{instdir}/%{basis_name}/help/ml
%{instdir}/%{basis_name}/program/resource/*ml.res
%{instdir}/%{basis_name}/share/autotext/ml
#%{instdir}/%{basis_name}/share/layout/ml
#%{instdir}/%{basis_name}/share/registry/res/ml
%{instdir}/%{basis_name}/share/template/ml
%{instdir}/%{basis_name}/share/samples/ml
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ml.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ml.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ml.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ml.xcd

%files langpack-mr_IN
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_mr*
#%{instdir}/share/readme/README_mr*
%{instdir}/program/resource/ooomr.res
%{instdir}/%{basis_name}/help/mr
%{instdir}/%{basis_name}/program/resource/*mr.res
%{instdir}/%{basis_name}/share/autotext/mr
#%{instdir}/%{basis_name}/share/layout/mr
#%{instdir}/%{basis_name}/share/registry/res/mr
%{instdir}/%{basis_name}/share/template/mr
%{instdir}/%{basis_name}/share/samples/mr
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-mr.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-mr.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_mr.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_mr.xcd

%files langpack-ms_MY
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/ms
#%%{instdir}/%{basis_name}/program/resource/*680ms.res
#%%{instdir}/share/dict/ooo/ms_*.*
#%{instdir}/share/readme/LICENSE_ms*
#%{instdir}/share/readme/README_ms*
%{instdir}/program/resource/*ms.res
%{instdir}/%{basis_name}/program/resource/*ms.res
%{instdir}/%{basis_name}/share/autotext/ms
#%{instdir}/%{basis_name}/share/layout/ms
#%{instdir}/%{basis_name}/share/registry/res/ms
%{instdir}/%{basis_name}/share/template/ms
%{instdir}/%{basis_name}/share/samples/ms
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ms.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ms.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ms.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ms.xcd

%files langpack-nb_NO
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/nb
#%%{instdir}/%{basis_name}/program/resource/*680nb.res
#%%{instdir}/share/dict/ooo/nb_NO.*
#%{instdir}/share/readme/LICENSE_nb*
#%{instdir}/share/readme/README_nb*
%{instdir}/program/resource/*nb.res
%{instdir}/%{basis_name}/program/resource/*nb.res
%{instdir}/%{basis_name}/share/autotext/nb
#%{instdir}/%{basis_name}/share/layout/nb
#%{instdir}/%{basis_name}/share/registry/res/nb
%{instdir}/%{basis_name}/share/template/nb
%{instdir}/%{basis_name}/share/samples/nb
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-nb.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-nb.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_nb.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_nb.xcd

%files langpack-ne_NP
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/ne
#%%{instdir}/%{basis_name}/program/resource/*680ne.res
#%%{instdir}/share/dict/ooo/README*_ne_NP*.txt
#%%{instdir}/share/dict/ooo/ne_NP.*
#%%{instdir}/share/dict/ooo/th_ne_NP_v2.*
#%{instdir}/share/readme/LICENSE_ne*
#%{instdir}/share/readme/README_ne*
%{instdir}/program/resource/*ne.res
%{instdir}/%{basis_name}/program/resource/*ne.res
%{instdir}/%{basis_name}/share/autotext/ne
#%{instdir}/%{basis_name}/share/layout/ne
#%{instdir}/%{basis_name}/share/registry/res/ne
%{instdir}/%{basis_name}/share/template/ne
%{instdir}/%{basis_name}/share/samples/ne
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ne.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ne.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ne.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ne.xcd
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_ne.xcu

%files langpack-nl
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/nl
#%%{instdir}/%{basis_name}/program/resource/*680nl.res
#%%{instdir}/share/dict/ooo/README_nl_NL.txt
#%%{instdir}/share/dict/ooo/nl_*.*
#%%{instdir}/share/dict/ooo/hyph_nl_NL.dic
#%{instdir}/share/readme/LICENSE_nl*
#%{instdir}/share/readme/README_nl*
%{instdir}/program/resource/*nl.res
%{instdir}/%{basis_name}/program/resource/*nl.res
%{instdir}/%{basis_name}/share/autotext/nl
#%{instdir}/%{basis_name}/share/layout/nl
#%{instdir}/%{basis_name}/share/registry/res/nl
%{instdir}/%{basis_name}/share/template/nl
%{instdir}/%{basis_name}/share/samples/nl
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-nl.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-nl.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_nl.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_nl.xcd

%files langpack-nn_NO
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_nn*
#%{instdir}/share/readme/README_nn*
%{instdir}/program/resource/ooonn.res
%{instdir}/%{basis_name}/help/nn
%{instdir}/%{basis_name}/program/resource/*nn.res
%{instdir}/%{basis_name}/share/autotext/nn
#%{instdir}/%{basis_name}/share/layout/nn
#%{instdir}/%{basis_name}/share/registry/res/nn
%{instdir}/%{basis_name}/share/template/nn
%{instdir}/%{basis_name}/share/samples/nn
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-nn.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-nn.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_nn.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_nn.xcd

%files langpack-nr_ZA
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_nr*
#%{instdir}/share/readme/README_nr*
%{instdir}/program/resource/ooonr.res
%{instdir}/%{basis_name}/help/nr
%{instdir}/%{basis_name}/program/resource/*nr.res
%{instdir}/%{basis_name}/share/autotext/nr
#%{instdir}/%{basis_name}/share/layout/nr
#%{instdir}/%{basis_name}/share/registry/res/nr
%{instdir}/%{basis_name}/share/template/nr
%{instdir}/%{basis_name}/share/samples/nr
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-nr.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-nr.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_nr.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_nr.xcd

%files langpack-nso_ZA
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ns*
#%{instdir}/share/readme/README_ns*
%{instdir}/program/resource/ooons.res
%{instdir}/%{basis_name}/help/ns
%{instdir}/%{basis_name}/program/resource/*ns.res
%{instdir}/%{basis_name}/share/autotext/ns
#%{instdir}/%{basis_name}/share/layout/ns
#%{instdir}/%{basis_name}/share/registry/res/ns
%{instdir}/%{basis_name}/share/template/ns
%{instdir}/%{basis_name}/share/samples/ns
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ns.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ns.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ns.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ns.xcd

%files langpack-or_IN
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_or*
#%{instdir}/share/readme/README_or*
%{instdir}/program/resource/oooor.res
%{instdir}/%{basis_name}/help/or
%{instdir}/%{basis_name}/program/resource/*or.res
%{instdir}/%{basis_name}/share/autotext/or
#%{instdir}/%{basis_name}/share/layout/or
#%{instdir}/%{basis_name}/share/registry/res/or
%{instdir}/%{basis_name}/share/template/or
%{instdir}/%{basis_name}/share/samples/or
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-or.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-or.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_or.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_or.xcd
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_or.xcu

%files langpack-pa_IN
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/pa-IN
#%%{instdir}/%{basis_name}/program/resource/*680pa-IN.res
#%%{instdir}/share/dict/ooo/pa_IN.*
#%{instdir}/share/readme/LICENSE_pa-IN*
#%{instdir}/share/readme/README_pa-IN*
%{instdir}/program/resource/*pa-IN.res
%{instdir}/%{basis_name}/program/resource/*pa-IN.res
%{instdir}/%{basis_name}/share/autotext/pa-IN
#%{instdir}/%{basis_name}/share/layout/pa-IN
#%{instdir}/%{basis_name}/share/registry/res/pa-IN
%{instdir}/%{basis_name}/share/template/pa-IN
%{instdir}/%{basis_name}/share/samples/pa-IN
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-pa-IN.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-pa-IN.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_pa-IN.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_pa-IN.xcd
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_pa-IN.xcu

%files langpack-pl_PL
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/pl
#%%{instdir}/%{basis_name}/program/resource/*680pl.res
#%%{instdir}/share/dict/ooo/README*_pl_PL*.txt
#%%{instdir}/share/dict/ooo/pl_PL.*
#%%{instdir}/share/dict/ooo/hyph_pl_PL.dic
#%%{instdir}/share/dict/ooo/th_pl_PL_v2.*
#%{instdir}/share/readme/LICENSE_pl*
#%{instdir}/share/readme/README_pl*
%{instdir}/program/resource/*pl.res
%{instdir}/%{basis_name}/program/resource/*pl.res
%{instdir}/%{basis_name}/share/autotext/pl
#%{instdir}/%{basis_name}/share/layout/pl
#%{instdir}/%{basis_name}/share/registry/res/pl
%{instdir}/%{basis_name}/share/template/pl
%{instdir}/%{basis_name}/share/samples/pl
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-pl.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-pl.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_pl.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_pl.xcd

%files langpack-pt_PT
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/pt
#%%{instdir}/%{basis_name}/program/resource/*680pt.res
#%%{instdir}/share/dict/ooo/pt_PT.*
#%{instdir}/share/readme/LICENSE_pt
#%{instdir}/share/readme/LICENSE_pt.html
#%{instdir}/share/readme/README_pt
#%{instdir}/share/readme/README_pt.html
%{instdir}/program/resource/*pt.res
%{instdir}/%{basis_name}/program/resource/*pt.res
%{instdir}/%{basis_name}/share/autotext/pt
#%{instdir}/%{basis_name}/share/layout/pt
#%{instdir}/%{basis_name}/share/registry/res/pt
%{instdir}/%{basis_name}/share/template/pt
%{instdir}/%{basis_name}/share/samples/pt
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-pt.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-pt.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_pt.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_pt.xcd

%files langpack-pt_BR
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/pt-BR
#%%{instdir}/%{basis_name}/program/resource/*680pt-BR.res
#%%{instdir}/share/dict/ooo/README_pt_BR.txt
#%%{instdir}/share/dict/ooo/pt_BR.*
#%{instdir}/share/readme/LICENSE_pt-BR*
#%{instdir}/share/readme/README_pt-BR*
%{instdir}/program/resource/*pt-BR.res
%{instdir}/%{basis_name}/program/resource/*pt-BR.res
%{instdir}/%{basis_name}/share/autotext/pt-BR
#%{instdir}/%{basis_name}/share/layout/pt-BR
#%{instdir}/%{basis_name}/share/registry/res/pt-BR
%{instdir}/%{basis_name}/share/template/pt-BR
%{instdir}/%{basis_name}/share/samples/pt-BR
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-pt-BR.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-pt-BR.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_pt-BR.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_pt-BR.xcd

%files langpack-ro
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ro*
#%{instdir}/share/readme/README_ro*
%{instdir}/program/resource/oooro.res
%{instdir}/%{basis_name}/help/ro
%{instdir}/%{basis_name}/program/resource/*ro.res
%{instdir}/%{basis_name}/share/autotext/ro
#%{instdir}/%{basis_name}/share/layout/ro
#%{instdir}/%{basis_name}/share/registry/res/ro
%{instdir}/%{basis_name}/share/template/ro
%{instdir}/%{basis_name}/share/samples/ro
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ro.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ro.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ro.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ro.xcd

%files langpack-ru
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/ru
#%%{instdir}/%{basis_name}/program/resource/*680ru.res
#%%{instdir}/share/dict/ooo/README*_ru_RU.txt
#%%{instdir}/share/dict/ooo/ru_RU.*
#%%{instdir}/share/dict/ooo/hyph_ru_RU.dic
#%%{instdir}/share/dict/ooo/th_ru_RU_v2.*
#%{instdir}/share/readme/LICENSE_ru*
#%{instdir}/share/readme/README_ru*
%{instdir}/program/resource/*ru.res
%{instdir}/%{basis_name}/program/resource/*ru.res
%{instdir}/%{basis_name}/share/autotext/ru
#%{instdir}/%{basis_name}/share/layout/ru
#%{instdir}/%{basis_name}/share/registry/res/ru
%{instdir}/%{basis_name}/share/template/ru
%{instdir}/%{basis_name}/share/samples/ru
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ru.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ru.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ru.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ru.xcd

%files langpack-sk_SK
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/sk
#%%{instdir}/%{basis_name}/program/resource/*680sk.res
#%%{instdir}/share/dict/ooo/README_sk_SK.txt
#%%{instdir}/share/dict/ooo/sk_SK.*
#%%{instdir}/share/dict/ooo/th_sk_SK_*
#%{instdir}/share/readme/LICENSE_sk*
#%{instdir}/share/readme/README_sk*
%{instdir}/program/resource/*sk.res
%{instdir}/%{basis_name}/program/resource/*sk.res
%{instdir}/%{basis_name}/share/autotext/sk
#%{instdir}/%{basis_name}/share/layout/sk
#%{instdir}/%{basis_name}/share/registry/res/sk
%{instdir}/%{basis_name}/share/template/sk
%{instdir}/%{basis_name}/share/samples/sk
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-sk.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-sk.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_sk.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_sk.xcd

%files langpack-sl_SI
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/sl
#%%{instdir}/%{basis_name}/program/resource/*680sl.res
#%%{instdir}/share/dict/ooo/README*_sl_SI.txt
#%%{instdir}/share/dict/ooo/sl_SI.*
#%%{instdir}/share/dict/ooo/hyph_sl_SI.dic
#%{instdir}/share/readme/LICENSE_sl*
#%{instdir}/share/readme/README_sl*
%{instdir}/program/resource/*sl.res
%{instdir}/%{basis_name}/program/resource/*sl.res
%{instdir}/%{basis_name}/share/autotext/sl
#%{instdir}/%{basis_name}/share/layout/sl
#%{instdir}/%{basis_name}/share/registry/res/sl
%{instdir}/%{basis_name}/share/template/sl
%{instdir}/%{basis_name}/share/samples/sl
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-sl.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-sl.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_sl.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_sl.xcd

%files langpack-sr
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_sr*
#%{instdir}/share/readme/README_sr*
%{instdir}/program/resource/ooosr.res
%{instdir}/%{basis_name}/help/sr
%{instdir}/%{basis_name}/program/resource/*sr.res
%{instdir}/%{basis_name}/share/autotext/sr
#%{instdir}/%{basis_name}/share/layout/sr
#%{instdir}/%{basis_name}/share/registry/res/sr
%{instdir}/%{basis_name}/share/template/sr
%{instdir}/%{basis_name}/share/samples/sr
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-sr.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-sr.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_sr.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_sr.xcd

%files langpack-ss_ZA
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ss*
#%{instdir}/share/readme/README_ss*
%{instdir}/program/resource/oooss.res
%{instdir}/%{basis_name}/help/ss
%{instdir}/%{basis_name}/program/resource/*ss.res
%{instdir}/%{basis_name}/share/autotext/ss
#%{instdir}/%{basis_name}/share/layout/ss
#%{instdir}/%{basis_name}/share/registry/res/ss
%{instdir}/%{basis_name}/share/template/ss
%{instdir}/%{basis_name}/share/samples/ss
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ss.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ss.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ss.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ss.xcd

%files langpack-st_ZA
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_st*
#%{instdir}/share/readme/README_st*
%{instdir}/program/resource/ooost.res
%{instdir}/%{basis_name}/help/st
%{instdir}/%{basis_name}/program/resource/*st.res
%{instdir}/%{basis_name}/share/autotext/st
#%{instdir}/%{basis_name}/share/layout/st
#%{instdir}/%{basis_name}/share/registry/res/st
%{instdir}/%{basis_name}/share/template/st
%{instdir}/%{basis_name}/share/samples/st
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-st.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-st.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_st.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_st.xcd

%files langpack-sv
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/sv
#%%{instdir}/%{basis_name}/program/resource/*680sv.res
#%%{instdir}/share/dict/ooo/README_sv_SE.txt
#%%{instdir}/share/dict/ooo/sv_*.*
#%{instdir}/share/readme/LICENSE_sv*
#%{instdir}/share/readme/README_sv*
%{instdir}/program/resource/*sv.res
%{instdir}/%{basis_name}/program/resource/*sv.res
%{instdir}/%{basis_name}/share/autotext/sv
#%{instdir}/%{basis_name}/share/layout/sv
#%{instdir}/%{basis_name}/share/registry/res/sv
%{instdir}/%{basis_name}/share/template/sv
%{instdir}/%{basis_name}/share/samples/sv
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-sv.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-sv.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_sv.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_sv.xcd

%files langpack-ta_IN
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ta*
#%{instdir}/share/readme/README_ta*
%{instdir}/program/resource/ooota.res
%{instdir}/%{basis_name}/help/ta
%{instdir}/%{basis_name}/program/resource/*ta.res
%{instdir}/%{basis_name}/share/autotext/ta
#%{instdir}/%{basis_name}/share/layout/ta
#%{instdir}/%{basis_name}/share/registry/res/ta
%{instdir}/%{basis_name}/share/template/ta
%{instdir}/%{basis_name}/share/samples/ta
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ta.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ta.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ta.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ta.xcd

%files langpack-te_IN
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_te*
#%{instdir}/share/readme/README_te*
%{instdir}/program/resource/ooote.res
%{instdir}/%{basis_name}/help/te
%{instdir}/%{basis_name}/program/resource/*te.res
%{instdir}/%{basis_name}/share/autotext/te
#%{instdir}/%{basis_name}/share/layout/te
#%{instdir}/%{basis_name}/share/registry/res/te
%{instdir}/%{basis_name}/share/template/te
%{instdir}/%{basis_name}/share/samples/te
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-te.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-te.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_te.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_te.xcd

%files langpack-th_TH
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/th
#%%{instdir}/share/dict/ooo/README_th_TH.txt
#%%{instdir}/share/dict/ooo/th_TH.*
#%%{instdir}/%{basis_name}/program/resource/*680th.res
#%{instdir}/share/readme/LICENSE_th*
#%{instdir}/share/readme/README_th*
%{instdir}/program/resource/*th.res
%{instdir}/%{basis_name}/program/resource/*th.res
%{instdir}/%{basis_name}/share/autotext/th
#%{instdir}/%{basis_name}/share/layout/th
#%{instdir}/%{basis_name}/share/registry/res/th
%{instdir}/%{basis_name}/share/template/th
%{instdir}/%{basis_name}/share/samples/th
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-th.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-th.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_th.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_th.xcd
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-ctl_th.xcu

%files langpack-tn_ZA
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_tn*
#%{instdir}/share/readme/README_tn*
%{instdir}/program/resource/oootn.res
%{instdir}/%{basis_name}/help/tn
%{instdir}/%{basis_name}/program/resource/*tn.res
%{instdir}/%{basis_name}/share/autotext/tn
#%{instdir}/%{basis_name}/share/layout/tn
#%{instdir}/%{basis_name}/share/registry/res/tn
%{instdir}/%{basis_name}/share/template/tn
%{instdir}/%{basis_name}/share/samples/tn
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-tn.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-tn.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_tn.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_tn.xcd

%files langpack-tr_TR
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/tr
#%%{instdir}/%{basis_name}/program/resource/*680tr.res
#%{instdir}/share/readme/LICENSE_tr*
#%{instdir}/share/readme/README_tr*
%{instdir}/program/resource/*tr.res
%{instdir}/%{basis_name}/program/resource/*tr.res
%{instdir}/%{basis_name}/share/autotext/tr
#%{instdir}/%{basis_name}/share/layout/tr
#%{instdir}/%{basis_name}/share/registry/res/tr
%{instdir}/%{basis_name}/share/template/tr
%{instdir}/%{basis_name}/share/samples/tr
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-tr.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-tr.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_tr.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_tr.xcd

%files langpack-ts_ZA
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ts*
#%{instdir}/share/readme/README_ts*
%{instdir}/program/resource/ooots.res
%{instdir}/%{basis_name}/help/ts
%{instdir}/%{basis_name}/program/resource/*ts.res
%{instdir}/%{basis_name}/share/autotext/ts
#%{instdir}/%{basis_name}/share/layout/ts
#%{instdir}/%{basis_name}/share/registry/res/ts
%{instdir}/%{basis_name}/share/template/ts
%{instdir}/%{basis_name}/share/samples/ts
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ts.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ts.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ts.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ts.xcd

%files langpack-uk
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_uk*
#%{instdir}/share/readme/README_uk*
%{instdir}/program/resource/ooouk.res
%{instdir}/%{basis_name}/help/uk
%{instdir}/%{basis_name}/program/resource/*uk.res
%{instdir}/%{basis_name}/share/autotext/uk
#%{instdir}/%{basis_name}/share/layout/uk
#%{instdir}/%{basis_name}/share/registry/res/uk
%{instdir}/%{basis_name}/share/template/uk
%{instdir}/%{basis_name}/share/samples/uk
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-uk.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-uk.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_uk.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_uk.xcd

%files langpack-ur
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ur*
#%{instdir}/share/readme/README_ur*
%{instdir}/program/resource/ooour.res
%{instdir}/%{basis_name}/help/ur
%{instdir}/%{basis_name}/program/resource/*ur.res
%{instdir}/%{basis_name}/share/autotext/ur
#%{instdir}/%{basis_name}/share/layout/ur
#%{instdir}/%{basis_name}/share/registry/res/ur
%{instdir}/%{basis_name}/share/template/ur
%{instdir}/%{basis_name}/share/samples/ur
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ur.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ur.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ur.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ur.xcd

%files langpack-ve_ZA
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_ve*
#%{instdir}/share/readme/README_ve*
%{instdir}/program/resource/ooove.res
%{instdir}/%{basis_name}/help/ve
%{instdir}/%{basis_name}/program/resource/*ve.res
%{instdir}/%{basis_name}/share/autotext/ve
#%{instdir}/%{basis_name}/share/layout/ve
#%{instdir}/%{basis_name}/share/registry/res/ve
%{instdir}/%{basis_name}/share/template/ve
%{instdir}/%{basis_name}/share/samples/ve
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-ve.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-ve.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_ve.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_ve.xcd

%files langpack-xh_ZA
%defattr(-,root,root,-)
#%{instdir}/share/readme/LICENSE_xh*
#%{instdir}/share/readme/README_xh*
%{instdir}/program/resource/oooxh.res
%{instdir}/%{basis_name}/help/xh
%{instdir}/%{basis_name}/program/resource/*xh.res
%{instdir}/%{basis_name}/share/autotext/xh
#%{instdir}/%{basis_name}/share/layout/xh
#%{instdir}/%{basis_name}/share/registry/res/xh
%{instdir}/%{basis_name}/share/template/xh
%{instdir}/%{basis_name}/share/samples/xh
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-xh.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-xh.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_xh.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_xh.xcd

%files langpack-zh_CN
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/zh-CN
#%%{instdir}/%{basis_name}/program/resource/*680zh-CN.res
#%{instdir}/share/readme/LICENSE_zh-CN*
#%{instdir}/share/readme/README_zh-CN*
%{instdir}/share/registry/cjk_zh-CN.xcd
%{instdir}/program/resource/*zh-CN.res
%{instdir}/%{basis_name}/program/resource/*zh-CN.res
%{instdir}/%{basis_name}/share/autotext/zh-CN
#%{instdir}/%{basis_name}/share/layout/zh-CN
#%{instdir}/%{basis_name}/share/registry/res/zh-CN
%{instdir}/%{basis_name}/share/template/zh-CN
%{instdir}/%{basis_name}/share/samples/zh-CN
#%{instdir}/share/registry/modules/org/openoffice/Office/Common/Common-cjk_zh-CN.xcu
#%{instdir}/share/registry/modules/org/openoffice/Office/Writer/Writer-cjk_zh-CN.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-zh-CN.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-zh-CN.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_zh-CN.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_zh-CN.xcd

%files langpack-zh_TW
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/zh-TW
#%%{instdir}/%{basis_name}/program/resource/*680zh-TW.res
#%{instdir}/share/readme/LICENSE_zh-TW*
#%{instdir}/share/readme/README_zh-TW*
%{instdir}/share/registry/cjk_zh-TW.xcd
%{instdir}/program/resource/*zh-TW.res
%{instdir}/%{basis_name}/program/resource/*zh-TW.res
%{instdir}/%{basis_name}/share/autotext/zh-TW
#%{instdir}/%{basis_name}/share/layout/zh-TW
#%{instdir}/%{basis_name}/share/registry/res/zh-TW
%{instdir}/%{basis_name}/share/template/zh-TW
%{instdir}/%{basis_name}/share/samples/zh-TW
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-zh-TW.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-zh-TW.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_zh-TW.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_zh-TW.xcd
#%{instdir}/share/registry/modules/org/openoffice/Office/Common/Common-cjk_zh-TW.xcu
#%{instdir}/share/registry/modules/org/openoffice/Office/Writer/Writer-cjk_zh-TW.xcu

%files langpack-zu_ZA
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/zu
#%%{instdir}/%{basis_name}/program/resource/*680zu.res
#%%{instdir}/share/dict/ooo/zu_ZA.*
#%%{instdir}/share/dict/ooo/hyph_zu_ZA.dic
#%{instdir}/share/readme/LICENSE_zu*
#%{instdir}/share/readme/README_zu*
%{instdir}/program/resource/*zu.res
%{instdir}/%{basis_name}/program/resource/*zu.res
%{instdir}/%{basis_name}/share/autotext/zu
#%{instdir}/%{basis_name}/share/layout/zu
#%{instdir}/%{basis_name}/share/registry/res/zu
%{instdir}/%{basis_name}/share/template/zu
%{instdir}/%{basis_name}/share/samples/zu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-zu.xcu
%{instdir}/%{basis_name}/share/registry/Langpack-zu.xcd
%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_zu.xcd
%{instdir}/%{basis_name}/share/registry/res/registry_zu.xcd
%endif

%files core
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog* INSTALL MAINTAINERS NEWS README TODO doc readmes.OOo
# %doc licenses.OOo
%dir %{instdir}
%dir %{_datadir}/%{ooodirname}
%{instdir}/LICENSE
%{instdir}/LICENSE.odt
#%{instdir}/README
#%{instdir}/README.odt
%{instdir}/THIRDPARTYLICENSEREADME.html
%{instdir}/CREDITS.odt
#%{instdir}/install-dict
#%%{_sysconfdir}/bash_completion.d/ooo-wrapper.sh
%{_sysconfdir}/bash_completion.d/libreoffice.sh
%{instdir}/basis-link
#%{instdir}/share/extensions
%dir %{instdir}/%{basis_name}/help
%{instdir}/%{basis_name}/help/idxcaption.xsl
%{instdir}/%{basis_name}/help/idxcontent.xsl
%dir %{instdir}/%{basis_name}/help/en
%{instdir}/%{basis_name}/help/en/default.css
%{instdir}/%{basis_name}/help/en/err.html
%{instdir}/%{basis_name}/help/en/highcontrast1.css
%{instdir}/%{basis_name}/help/en/highcontrast2.css
%{instdir}/%{basis_name}/help/en/highcontrastblack.css
%{instdir}/%{basis_name}/help/en/highcontrastwhite.css
%{instdir}/%{basis_name}/help/en/sbasic.*
%{instdir}/%{basis_name}/help/en/schart.*
%{instdir}/%{basis_name}/help/en/shared.*
%{instdir}/%{basis_name}/help/main_transform.xsl
%{instdir}/%{basis_name}/presets
%dir %{instdir}/%{basis_name}/program
# java
%if %{with_java}
#%%{instdir}/%{basis_name}/program/JREProperties.class
%endif
#%%{instdir}/%{basis_name}/program/about.bmp
#%%{instdir}/%{basis_name}/program/acceptor.uno.so
%{instdir}/%{basis_name}/program/addin
#%%{instdir}/%{basis_name}/program/basprov680*.uno.so
#%{instdir}/%{basis_name}/program/behelper.uno.so
%{instdir}/program/bootstraprc
#%%{instdir}/%{basis_name}/program/bootstrap.uno.so
#%%{instdir}/%{basis_name}/program/bridgefac.uno.so
%{instdir}/%{basis_name}/program/basprovl?.uno.so
%{instdir}/%{basis_name}/program/cairocanvas.uno.so
%{instdir}/%{basis_name}/program/canvasfactory.uno.so
%{instdir}/%{basis_name}/program/cde-open-url
# java
%if %{with_java}
%dir %{instdir}/%{basis_name}/program/classes
%{instdir}/%{basis_name}/program/classes/LuceneHelpWrapper.jar
%{instdir}/%{basis_name}/program/classes/agenda.jar
#%{instdir}/%{basis_name}/program/classes/avmedia.jar
#%{instdir}/%{basis_name}/program/classes/classes.jar
%{instdir}/%{basis_name}/program/classes/commonwizards.jar
%{instdir}/%{basis_name}/program/classes/fax.jar
%{instdir}/%{basis_name}/program/classes/form.jar
#%%{instdir}/%{basis_name}/program/classes/java_uno.jar
#%{instdir}/%{basis_name}/program/classes/java_uno_accessbridge.jar
%{instdir}/%{basis_name}/program/classes/js.jar
#%%{instdir}/%{basis_name}/program/classes/juh.jar
#%%{instdir}/%{basis_name}/program/classes/jurt.jar
#%%{instdir}/%{basis_name}/program/classes/jut.jar
%{instdir}/%{basis_name}/program/classes/letter.jar
%{instdir}/%{basis_name}/program/classes/lucene-analyzers-2.3.jar
%{instdir}/%{basis_name}/program/classes/lucene-core-2.3.jar
%{instdir}/%{basis_name}/program/classes/query.jar
%{instdir}/%{basis_name}/program/classes/officebean.jar
%{instdir}/%{basis_name}/program/classes/report.jar
#%%{instdir}/%{basis_name}/program/classes/ridl.jar
%{instdir}/%{basis_name}/program/classes/saxon9.jar
#%{instdir}/%{basis_name}/program/classes/sandbox.jar
#%%{instdir}/%{basis_name}/program/classes/serializer.jar
%{instdir}/%{basis_name}/program/classes/sdbc_hsqldb.jar
%{instdir}/%{basis_name}/program/classes/ScriptFramework.jar
%{instdir}/%{basis_name}/program/classes/ScriptProviderForBeanShell.jar
%{instdir}/%{basis_name}/program/classes/ScriptProviderForJava.jar
%{instdir}/%{basis_name}/program/classes/ScriptProviderForJavaScript.jar
%{instdir}/%{basis_name}/program/classes/table.jar
%{instdir}/%{basis_name}/program/classes/unoil.jar
#%%{instdir}/%{basis_name}/program/classes/unoloader.jar
%{instdir}/%{basis_name}/program/classes/web.jar
#%%{instdir}/%{basis_name}/program/classes/writer2latex.jar
%{instdir}/%{basis_name}/program/classes/XMergeBridge.jar
%{instdir}/%{basis_name}/program/classes/xmerge.jar
%{instdir}/%{basis_name}/program/classes/XSLTFilter.jar
%{instdir}/%{basis_name}/program/classes/XSLTValidate.jar
%endif
%{instdir}/%{basis_name}/program/cmdmail.uno.so
%{instdir}/%{basis_name}/program/configmgr.uno.so
#%%{instdir}/%{basis_name}/program/configimport
#%%{instdir}/%{basis_name}/program/configimport.bin
#%{instdir}/%{basis_name}/program/configmgr2.uno.so
#%{instdir}/%{basis_name}/program/configmgrrc
#%%{instdir}/%{basis_name}/program/connector.uno.so
#%{instdir}/%{basis_name}/program/crash_report
%{instdir}/%{basis_name}/program/deploymentguil?.uno.so
%{instdir}/%{basis_name}/program/deploymentl?.uno.so
%{instdir}/%{basis_name}/program/dlgprovl?.uno.so
#%%{instdir}/%{basis_name}/program/deploymentgui680*.uno.so
%{instdir}/%{basis_name}/program/desktopbe1.uno.so
#%%{instdir}/%{basis_name}/program/dlgprov680*.uno.so
%{instdir}/%{basis_name}/program/fastsax.uno.so
%{instdir}/%{basis_name}/program/fpicker.uno.so
%{instdir}/%{basis_name}/program/fps_gnome.uno.so
%{instdir}/%{basis_name}/program/fps_office.uno.so
%{instdir}/%{basis_name}/program/fsstorage.uno.so
%{instdir}/%{basis_name}/program/fundamentalbasisrc
%{instdir}/%{basis_name}/program/gengal*
%{instdir}/%{basis_name}/program/gnome-open-url
%{instdir}/%{basis_name}/program/gnome-open-url.bin
%{instdir}/%{basis_name}/program/hatchwindowfactory.uno.so
%{instdir}/%{basis_name}/program/i18npool.uno.so
%{instdir}/%{basis_name}/program/i18nsearch.uno.so
#%%{instdir}/%{basis_name}/program/implreg.uno.so
#%%{instdir}/%{basis_name}/program/openintro_momonga.bmp
#%%{instdir}/%{basis_name}/program/introspection.uno.so
#%%{instdir}/%{basis_name}/program/invocadapt.uno.so
#%%{instdir}/%{basis_name}/program/invocation.uno.so
%if %{with_java}
#%%{instdir}/ure/bin/javaldx
#%%{instdir}/ure/lib/javaloader.uno.so
#%%{instdir}/ure/lib/javavm.uno.so
%endif
%{instdir}/%{basis_name}/program/java-set-classpath
#%%{instdir}/%{basis_name}/program/jvmfwk3rc
%{instdir}/%{basis_name}/program/kde-open-url
%{instdir}/%{basis_name}/program/ldapbe2.uno.so
# we dont build binfilter
#%{instdir}/%{basis_name}/program/legacy_binfilters.rdb
#%%{instdir}/%{basis_name}/program/libabp680*.so
#%%{instdir}/%{basis_name}/program/libacc680*.so
#%%{instdir}/%{basis_name}/program/libadabas2.so
#%%{instdir}/%{basis_name}/program/libaffine_uno_uno.so
# %{instdir}/%{basis_name}/program/libagg680*.so
#%%{instdir}/%{basis_name}/program/libavmedia680*.so
%{instdir}/%{basis_name}/program/libavmediagst.so
#%%{instdir}/%{basis_name}/program/libbasctl680*.so
#%%{instdir}/%{basis_name}/program/libbasebmp680*.so
#%%{instdir}/%{basis_name}/program/libbasegfx680*.so
# we dont build binfilter
#%{instdir}/%{basis_name}/program/libbf_frm680*.so
#%{instdir}/%{basis_name}/program/libbf_lng680*.so
#%{instdir}/%{basis_name}/program/libbf_migratefilter680*.so
#%{instdir}/%{basis_name}/program/libbf_ofa680*.so
#%{instdir}/%{basis_name}/program/libbf_sch680*.so
#%{instdir}/%{basis_name}/program/libbf_sd680*.so
#%{instdir}/%{basis_name}/program/libbf_sm680*.so
#%{instdir}/%{basis_name}/program/libbf_svx680*.so
#%{instdir}/%{basis_name}/program/libbf_sw680*.so
#%{instdir}/%{basis_name}/program/libbf_wrapper680*.so
#%{instdir}/%{basis_name}/program/libbf_xo680*.so
#%%{instdir}/%{basis_name}/program/libbib680*.so
# ?
#%{instdir}/%{basis_name}/program/libbindet680*.so
%{instdir}/%{basis_name}/program/libcached1.so
#%%{instdir}/%{basis_name}/program/libcanvastools680*.so
#%%{instdir}/%{basis_name}/program/libchartcontroller680*.so
#%%{instdir}/%{basis_name}/program/libchartmodel680*.so
#%%{instdir}/%{basis_name}/program/libcharttools680*.so
#%%{instdir}/%{basis_name}/program/libchartview680*.so
%{instdir}/%{basis_name}/program/libcomphelp4gcc3.so
%{instdir}/%{basis_name}/program/libcollator_data.so
#%%{instdir}/%{basis_name}/program/libcppcanvas680*.so
#%%{instdir}/%{basis_name}/program/libcppu.so
#%%{instdir}/%{basis_name}/program/libcppu.so.3
#%%{instdir}/%{basis_name}/program/libcppuhelper3gcc3.so
#%%{instdir}/%{basis_name}/program/libcppuhelpergcc3.so
#%%{instdir}/%{basis_name}/program/libcppuhelpergcc3.so.3
#%%{instdir}/%{basis_name}/program/libctl680*.so
#%%{instdir}/%{basis_name}/program/libcui680*.so
#%%{instdir}/%{basis_name}/program/libdba680*.so
#%%{instdir}/%{basis_name}/program/libdbacfg680*.so
#%%{instdir}/%{basis_name}/program/libdbase680*.so
#%%{instdir}/%{basis_name}/program/libdbaxml680*.so
#%%{instdir}/%{basis_name}/program/libdbp680*.so
%{instdir}/%{basis_name}/program/libdbpool2.so
#%%{instdir}/%{basis_name}/program/libdbtools680*.so
#%%{instdir}/%{basis_name}/program/libdbu680*.so
#%%{instdir}/%{basis_name}/program/libdeploymentmisc680*.so
%{instdir}/%{basis_name}/program/libdesktop_detector*.so
%{instdir}/%{basis_name}/program/libdict_ja.so
%{instdir}/%{basis_name}/program/libdict_zh.so
#%%{instdir}/%{basis_name}/program/libdtransX11680*.so
%{instdir}/%{basis_name}/program/libeditengl?.so
%{instdir}/%{basis_name}/program/libembobj.so
%{instdir}/%{basis_name}/program/libemboleobj.so
#%%{instdir}/%{basis_name}/program/libevoab1.so
%{instdir}/%{basis_name}/program/libevtatt.so
#%%{instdir}/%{basis_name}/program/libegi680*.so
#%%{instdir}/%{basis_name}/program/libeggtray680*.so
#%%{instdir}/%{basis_name}/program/libeme680*.so
#%%{instdir}/%{basis_name}/program/libemp680*.so
#%%{instdir}/%{basis_name}/program/libepb680*.so
#%%{instdir}/%{basis_name}/program/libepg680*.so
#%%{instdir}/%{basis_name}/program/libepp680*.so
#%%{instdir}/%{basis_name}/program/libeps680*.so
#%%{instdir}/%{basis_name}/program/libept680*.so
#%%{instdir}/%{basis_name}/program/libera680*.so
#%%{instdir}/%{basis_name}/program/libeti680*.so
# disable evolution2 ?
#%{instdir}/%{basis_name}/program/libevoab2.so
#%%{instdir}/%{basis_name}/program/libexlink680*.so
#%%{instdir}/%{basis_name}/program/libexp680*.so
#%%{instdir}/%{basis_name}/program/libfwm680*.so
#%%{instdir}/%{basis_name}/program/libicd680*.so
#%%{instdir}/%{basis_name}/program/libicg680*.so
#%%{instdir}/%{basis_name}/program/libidx680*.so
#%%{instdir}/%{basis_name}/program/libime680*.so
%{instdir}/%{basis_name}/program/libindex_data.so
#%%{instdir}/%{basis_name}/program/libipb680*.so
#%%{instdir}/%{basis_name}/program/libipd680*.so
#%%{instdir}/%{basis_name}/program/libips680*.so
#%%{instdir}/%{basis_name}/program/libipt680*.so
#%%{instdir}/%{basis_name}/program/libipx680*.so
#%%{instdir}/%{basis_name}/program/libira680*.so
#%%{instdir}/%{basis_name}/program/libitg680*.so
#%%{instdir}/%{basis_name}/program/libiti680*.so
# java
%if %{with_java}
%{instdir}/%{basis_name}/program/libofficebean.so
%endif
#%%{instdir}/%{basis_name}/program/libfile680*.so
%{instdir}/%{basis_name}/program/libfileacc.so
%{instdir}/%{basis_name}/program/libfilterconfig1.so
#%%{instdir}/%{basis_name}/program/libflat680*.so
#%%{instdir}/%{basis_name}/program/libfrm680*.so
#%%{instdir}/%{basis_name}/program/libfwe680*.so
#%%{instdir}/%{basis_name}/program/libfwi680*.so
#%%{instdir}/%{basis_name}/program/libfwk680*.so
#%%{instdir}/%{basis_name}/program/libfwl680*.so
#%%{instdir}/%{basis_name}/program/libgcc3_uno.so
#%%{instdir}/%{basis_name}/program/libgo680*.so
#%%{instdir}/%{basis_name}/program/libguesslang680*.so
# java database
#%{instdir}/%{basis_name}/program/libhsqldb2.so
#%%{instdir}/%{basis_name}/program/libhelplinker680*.so
#%{instdir}/%{basis_name}/program/libhunspell.so
#%%{instdir}/%{basis_name}/program/libhyphen680*.so
%{instdir}/%{basis_name}/program/libi18nisolang1gcc3.so
%{instdir}/%{basis_name}/program/libi18npaper*.so
#%{instdir}/%{basis_name}/program/libi18npool.uno.so
#%{instdir}/%{basis_name}/program/libi18npool_asian.uno.so
%{instdir}/%{basis_name}/program/libi18nregexpgcc3.so
%{instdir}/%{basis_name}/program/libi18nutilgcc3.so
#%%{instdir}/%{basis_name}/program/libj680*_g.so
# java
%if %{with_java}
#%%{instdir}/%{basis_name}/program/libjava_uno.so
#%%{instdir}/%{basis_name}/program/libjdbc2.so
#%%{instdir}/%{basis_name}/program/libjpipe.so
#%%{instdir}/%{basis_name}/program/libjuh.so
#%%{instdir}/%{basis_name}/program/libjuhx.so
#%%{instdir}/%{basis_name}/program/libjvmaccessgcc3.so
%endif
#%%{instdir}/%{basis_name}/program/libjvmaccessgcc3.so.3
%if %{with_java}
#%%{instdir}/%{basis_name}/program/libjvmfwk.so
%endif
#%%{instdir}/%{basis_name}/program/libjvmfwk.so.3
# we dont build binfilter
#%{instdir}/%{basis_name}/program/liblegacy_binfilters680*.so
#%%{instdir}/%{basis_name}/program/liblng680*.so
%{instdir}/%{basis_name}/program/libmsfilterl?.so
%{instdir}/%{basis_name}/program/liblocaledata_en.so
%{instdir}/%{basis_name}/program/liblocaledata_es.so
%{instdir}/%{basis_name}/program/liblocaledata_euro.so
%{instdir}/%{basis_name}/program/liblocaledata_others.so
#%%{instdir}/%{basis_name}/program/liblog680*.so
%{instdir}/%{basis_name}/program/liblpsolve*.so
#%%{instdir}/%{basis_name}/program/liblwpft680*.so
%{instdir}/%{basis_name}/program/libmcnttype.so
#%%{instdir}/%{basis_name}/program/libmsworks680*.so
#%%{instdir}/%{basis_name}/program/libmtfrenderer*.uno.so
# system mozilla ?
#%{instdir}/%{basis_name}/program/libmozab*
#%%{instdir}/%{basis_name}/program/libmyspell.so
#%%{instdir}/%{basis_name}/program/libmysql2.so
#%%{instdir}/%{basis_name}/program/libnpsoplugin.so
#%%{instdir}/%{basis_name}/program/libodbc2.so
#%%{instdir}/%{basis_name}/program/libodbcbase2.so
#%%{instdir}/%{basis_name}/program/liboffacc680*.so
#%%{instdir}/%{basis_name}/program/liboox680*.so
%{instdir}/%{basis_name}/program/libpackage2.so
#%%{instdir}/%{basis_name}/program/libpcr680*.so
#%%{instdir}/%{basis_name}/program/libpdffilter680*.so
#%%{instdir}/%{basis_name}/program/libpl680*.so
#%%{instdir}/%{basis_name}/program/libpreload680*.so
#%%{instdir}/%{basis_name}/program/libprotocolhandler680*.so
#%%{instdir}/%{basis_name}/program/libpsp680*.so
#%%{instdir}/%{basis_name}/program/libqstart_gtk680*.so
%{instdir}/%{basis_name}/program/librecentfile.so
#%%{instdir}/%{basis_name}/program/libreg.so
#%%{instdir}/%{basis_name}/program/libreg.so.3
#%%{instdir}/%{basis_name}/program/libres680*.so
#%%{instdir}/%{basis_name}/program/librmcxt.so
#%%{instdir}/%{basis_name}/program/librmcxt.so.3
%if %{with_java}
#%%{instdir}/%{basis_name}/program/librpt680*.so
#%%{instdir}/%{basis_name}/program/librptui680*.so
#%%{instdir}/%{basis_name}/program/librptxml680*.so
%endif
#%%{instdir}/%{basis_name}/program/libsal.so
#%%{instdir}/%{basis_name}/program/libsal.so.3
#%%{instdir}/%{basis_name}/program/libsalhelper3gcc3.so
#%%{instdir}/%{basis_name}/program/libsalhelpergcc3.so
#%%{instdir}/%{basis_name}/program/libsalhelpergcc3.so.3
#%%{instdir}/%{basis_name}/program/libsax680*.so
#%%{instdir}/%{basis_name}/program/libsb680*.so
#%%{instdir}/%{basis_name}/program/libsc680*.so
#%%{instdir}/%{basis_name}/program/libscn680*.so
%{instdir}/%{basis_name}/program/libscriptframe.so
#%%{instdir}/%{basis_name}/program/libscui680*.so
#%%{instdir}/%{basis_name}/program/libsd680*.so
%{instdir}/%{basis_name}/program/libsdbc2.so
#%%{instdir}/%{basis_name}/program/libsdbt680*.so
#%%{instdir}/%{basis_name}/program/libsdd680*.so
#%%{instdir}/%{basis_name}/program/libsdui680*.so
#%%{instdir}/%{basis_name}/program/libsfx680*.so
#%%{instdir}/%{basis_name}/program/libso680*.so
#%%{instdir}/%{basis_name}/program/libsot680*.so
#%%{instdir}/%{basis_name}/program/libspa680*.so
#%%{instdir}/%{basis_name}/program/libspell680*.so
#%%{instdir}/%{basis_name}/program/libspl680*.so
#%%{instdir}/%{basis_name}/program/libspl_unx680*.so
%{instdir}/%{basis_name}/program/libsrtrs1.so
#%%{instdir}/%{basis_name}/program/libstore.so
#%%{instdir}/%{basis_name}/program/libstore.so.3
#%%{instdir}/%{basis_name}/program/libsts680*.so
#%%{instdir}/%{basis_name}/program/libsvl680*.so
#%%{instdir}/%{basis_name}/program/libsvt680*.so
#%%{instdir}/%{basis_name}/program/libsvx680*.so
#%%{instdir}/%{basis_name}/program/libsw680*.so
%{instdir}/%{basis_name}/program/libtextcat.so
%{instdir}/%{basis_name}/program/libtextconv_dict.so
#%%{instdir}/%{basis_name}/program/libtextconversiondlgs680*.so
#%%{instdir}/%{basis_name}/program/libt602filter680*.so
#%%{instdir}/%{basis_name}/program/libtfu680*.so
#%%{instdir}/%{basis_name}/program/libtk680*.so
#%%{instdir}/%{basis_name}/program/libtl680*.so
%{instdir}/%{basis_name}/program/libtvhlp1.so
%{instdir}/%{basis_name}/program/libucb1.so
%{instdir}/%{basis_name}/program/libucbhelper4gcc3.so
%{instdir}/%{basis_name}/program/libucpchelp1.so
%{instdir}/%{basis_name}/program/libucpdav1.so
%{instdir}/%{basis_name}/program/libucpfile1.so
%{instdir}/%{basis_name}/program/libucpftp1.so
%{instdir}/%{basis_name}/program/libucphier1.so
%{instdir}/%{basis_name}/program/libucppkg1.so
#%%{instdir}/%{basis_name}/program/libuno_cppu.so
#%%{instdir}/%{basis_name}/program/libuno_cppu.so.3
#%%{instdir}/%{basis_name}/program/libuno_cppuhelpergcc3.so
#%%{instdir}/%{basis_name}/program/libuno_cppuhelpergcc3.so.3
#%%{instdir}/%{basis_name}/program/libuno_purpenvhelpergcc3.so.3
#%%{instdir}/%{basis_name}/program/libuno_sal.so
#%%{instdir}/%{basis_name}/program/libuno_sal.so.3
#%%{instdir}/%{basis_name}/program/libuno_salhelpergcc3.so
#%%{instdir}/%{basis_name}/program/libuno_salhelpergcc3.so.3
#%%{instdir}/%{basis_name}/program/libunsafe_uno_uno.so
#%%{instdir}/%{basis_name}/program/libunoxml680*.so
#%%{instdir}/%{basis_name}/program/libupdchk680*.so
#%%{instdir}/%{basis_name}/program/liburp_uno.so
#%%{instdir}/%{basis_name}/program/libutl680*.so
#%%{instdir}/%{basis_name}/program/libuui680*.so
#%%{instdir}/%{basis_name}/program/libvbaobj680*.so
#%%{instdir}/%{basis_name}/program/libvcl680*.so
#%%{instdir}/%{basis_name}/program/libvclplug_svp680*.so
#%{instdir}/%{basis_name}/program/libvclplug_dummy680*.so
#%{_libdir}/libvclplug_dummy680*.so
#%%{instdir}/%{basis_name}/program/libvclplug_gen680*.so
#%%{_libdir}/libvclplug_gen680*.so
#%%{instdir}/%{basis_name}/program/libvclplug_gtk680*.so
#%%{_libdir}/libvclplug_gtk680*.so
#%%{_libdir}/libvclplug_svp680*.so
#%{instdir}/%{basis_name}/program/libvos3gcc3.so
#%%{instdir}/%{basis_name}/program/libwriterfilter680*.so
#%%{instdir}/%{basis_name}/program/libxcr680*.so
#%%{instdir}/%{basis_name}/program/libxmlfa680*.so
#%%{instdir}/%{basis_name}/program/libxmlfd680*.so
#%%{instdir}/%{basis_name}/program/libxmx680*.so
#%%{instdir}/%{basis_name}/program/libxo680*.so
#%%{instdir}/%{basis_name}/program/libxof680*.so

# Sumaso Toriaezu
%{instdir}/%{basis_name}/program/libabpl?.so
%{instdir}/%{basis_name}/program/libaccl?.so
%{instdir}/%{basis_name}/program/libadabasl?.so
%{instdir}/%{basis_name}/program/libadabasuil?.so
%{instdir}/%{basis_name}/program/libanalysisl?.so
%{instdir}/%{basis_name}/program/libavmedial?.so
%{instdir}/%{basis_name}/program/libbasctll?.so
%{instdir}/%{basis_name}/program/libbasebmpl?.so
%{instdir}/%{basis_name}/program/libbasegfxl?.so
%{instdir}/%{basis_name}/program/libbibl?.so
%{instdir}/%{basis_name}/program/libcalcl?.so
%{instdir}/%{basis_name}/program/libcanvastoolsl?.so
%{instdir}/%{basis_name}/program/libchartcontrollerl?.so
%{instdir}/%{basis_name}/program/libchartmodell?.so
%{instdir}/%{basis_name}/program/libcharttoolsl?.so
%{instdir}/%{basis_name}/program/libchartviewl?.so
%{instdir}/%{basis_name}/program/libcommunil?.so
%{instdir}/%{basis_name}/program/libcppcanvasl?.so
%{instdir}/%{basis_name}/program/libctll?.so
%{instdir}/%{basis_name}/program/libcuil?.so
%{instdir}/%{basis_name}/program/libdatel?.so
%{instdir}/%{basis_name}/program/libdb-4.7.so
%{instdir}/%{basis_name}/program/libdbacfgl?.so
%{instdir}/%{basis_name}/program/libdbal?.so
%{instdir}/%{basis_name}/program/libdbasel?.so
%{instdir}/%{basis_name}/program/libdbaxmll?.so
%{instdir}/%{basis_name}/program/libdbmml?.so
%{instdir}/%{basis_name}/program/libdbpl?.so
%{instdir}/%{basis_name}/program/libdbtoolsl?.so
%{instdir}/%{basis_name}/program/libdbul?.so
%{instdir}/%{basis_name}/program/libdeploymentmiscl?.so
%{instdir}/%{basis_name}/program/libdrawinglayerl?.so
#%%{instdir}/%{basis_name}/program/libdtransX11l?.so
%{instdir}/%{basis_name}/program/libeggtrayl?.so
%{instdir}/%{basis_name}/program/libegil?.so
%{instdir}/%{basis_name}/program/libemel?.so
#%%{instdir}/%{basis_name}/program/libempl?.so
%{instdir}/%{basis_name}/program/libepbl?.so
%{instdir}/%{basis_name}/program/libepgl?.so
%{instdir}/%{basis_name}/program/libeppl?.so
%{instdir}/%{basis_name}/program/libepsl?.so
%{instdir}/%{basis_name}/program/libeptl?.so
%{instdir}/%{basis_name}/program/liberal?.so
%{instdir}/%{basis_name}/program/libetil?.so
#%%{instdir}/%{basis_name}/program/libevoabl?.so
#%%{instdir}/%{basis_name}/program/libexlinkl?.so
%{instdir}/%{basis_name}/program/libexpl?.so
%{instdir}/%{basis_name}/program/libfilel?.so
%{instdir}/%{basis_name}/program/libflashl?.so
%{instdir}/%{basis_name}/program/libflatl?.so
%{instdir}/%{basis_name}/program/libforl?.so
%{instdir}/%{basis_name}/program/libforuil?.so
%{instdir}/%{basis_name}/program/libfrml?.so
%{instdir}/%{basis_name}/program/libfwel?.so
%{instdir}/%{basis_name}/program/libfwil?.so
%{instdir}/%{basis_name}/program/libfwkl?.so
%{instdir}/%{basis_name}/program/libfwll?.so
%{instdir}/%{basis_name}/program/libfwml?.so
#%{instdir}/%{basis_name}/program/libgol?.so
%{instdir}/%{basis_name}/program/libguesslangl?.so
%{instdir}/%{basis_name}/program/libhelplinkerl?.so
%{instdir}/%{basis_name}/program/libhsqldb.so
%{instdir}/%{basis_name}/program/libhyphenl?.so
%{instdir}/%{basis_name}/program/libicdl?.so
%{instdir}/%{basis_name}/program/libicgl?.so
%{instdir}/%{basis_name}/program/libidxl?.so
%{instdir}/%{basis_name}/program/libimel?.so
%{instdir}/%{basis_name}/program/libipbl?.so
%{instdir}/%{basis_name}/program/libipdl?.so
%{instdir}/%{basis_name}/program/libipsl?.so
%{instdir}/%{basis_name}/program/libiptl?.so
%{instdir}/%{basis_name}/program/libipxl?.so
%{instdir}/%{basis_name}/program/libiral?.so
%{instdir}/%{basis_name}/program/libitgl?.so
%{instdir}/%{basis_name}/program/libitil?.so
%{instdir}/%{basis_name}/program/libjdbcl?.so
#%%{instdir}/%{basis_name}/program/libjl?_g.so
%{instdir}/%{basis_name}/program/liblngl?.so
%{instdir}/%{basis_name}/program/liblnthl?.so
%{instdir}/%{basis_name}/program/liblogl?.so
%{instdir}/%{basis_name}/program/liblwpftl?.so
%{instdir}/%{basis_name}/program/libmozbootstrap.so
%{instdir}/%{basis_name}/program/libmsformsl?.uno.so
%{instdir}/%{basis_name}/program/libmswordl?.so
%{instdir}/%{basis_name}/program/libmsworksl?.so
%{instdir}/%{basis_name}/program/libmtfrenderer.uno.so
%{instdir}/%{basis_name}/program/libmysqll?.so
%{instdir}/%{basis_name}/program/libodbcbasel?.so
%{instdir}/%{basis_name}/program/libodbcl?.so
%{instdir}/%{basis_name}/program/liboffaccl?.so
%{instdir}/%{basis_name}/program/liboooimprovecorel?.so
%{instdir}/%{basis_name}/program/libooxl?.so
%{instdir}/%{basis_name}/program/libpcrl?.so
%{instdir}/%{basis_name}/program/libpdffilterl?.so
%{instdir}/%{basis_name}/program/libpll?.so
%{instdir}/%{basis_name}/program/libpreloadl?.so
%{instdir}/%{basis_name}/program/libprotocolhandlerl?.so
#%%{instdir}/%{basis_name}/program/libpspl?.so
%{instdir}/%{basis_name}/program/libqstart_gtkl?.so
#%%{instdir}/%{basis_name}/program/libraptor.so
#%%{instdir}/%{basis_name}/program/libraptor.so.1
#%%{instdir}/%{basis_name}/program/libraptor.so.1.1.0
#%%{instdir}/%{basis_name}/program/librasqal.so
#%%{instdir}/%{basis_name}/program/librasqal.so.0
#%%{instdir}/%{basis_name}/program/librasqal.so.0.0.0
#%%{instdir}/%{basis_name}/program/librdf.so
#%%{instdir}/%{basis_name}/program/librdf.so.0
#%%{instdir}/%{basis_name}/program/librdf.so.0.0.0
%{instdir}/%{basis_name}/program/libresl?.so
%{instdir}/%{basis_name}/program/librptl?.so
%{instdir}/%{basis_name}/program/librptuil?.so
%{instdir}/%{basis_name}/program/librptxmll?.so
%{instdir}/%{basis_name}/program/libsaxl?.so
%{instdir}/%{basis_name}/program/libsbl?.so
%{instdir}/%{basis_name}/program/libscdl?.so
%{instdir}/%{basis_name}/program/libscfiltl?.so
%{instdir}/%{basis_name}/program/libscl?.so
%{instdir}/%{basis_name}/program/libscnl?.so
%{instdir}/%{basis_name}/program/libscuil?.so
%{instdir}/%{basis_name}/program/libsdbtl?.so
%{instdir}/%{basis_name}/program/libsddl?.so
%{instdir}/%{basis_name}/program/libsdl?.so
%{instdir}/%{basis_name}/program/libsduil?.so
%{instdir}/%{basis_name}/program/libsfxl?.so
%{instdir}/%{basis_name}/program/libsimplecml?.so
%{instdir}/%{basis_name}/program/libsmdl?.so
%{instdir}/%{basis_name}/program/libsml?.so
%{instdir}/%{basis_name}/program/libsofficeapp.so
%{instdir}/%{basis_name}/program/libsolverl?.so
%{instdir}/%{basis_name}/program/libsotl?.so
%{instdir}/%{basis_name}/program/libspal?.so
%{instdir}/%{basis_name}/program/libspelll?.so
%{instdir}/%{basis_name}/program/libspl_unxl?.so
%{instdir}/%{basis_name}/program/libspll?.so
%{instdir}/%{basis_name}/program/libstsl?.so
%{instdir}/%{basis_name}/program/libsvgfilterl?.so
%{instdir}/%{basis_name}/program/libsvll?.so
%{instdir}/%{basis_name}/program/libsvtl?.so
%{instdir}/%{basis_name}/program/libsvxl?.so
%{instdir}/%{basis_name}/program/libsvxcorel?.so
#%{instdir}/%{basis_name}/program/libsvxmsfilterl?.so
%{instdir}/%{basis_name}/program/libswdl?.so
%{instdir}/%{basis_name}/program/libswl?.so
%{instdir}/%{basis_name}/program/libswuil?.so
%{instdir}/%{basis_name}/program/libt602filterl?.so
%{instdir}/%{basis_name}/program/libtextconversiondlgsl?.so
#%{instdir}/%{basis_name}/program/libtful?.so
%{instdir}/%{basis_name}/program/libtkl?.so
%{instdir}/%{basis_name}/program/libtll?.so
%{instdir}/%{basis_name}/program/libunopkgapp.so
%{instdir}/%{basis_name}/program/libunordfl?.so
%{instdir}/%{basis_name}/program/libunoxmll?.so
%{instdir}/%{basis_name}/program/libupdchkl?.so
%{instdir}/%{basis_name}/program/libutll?.so
%{instdir}/%{basis_name}/program/libuuil?.so
%{instdir}/%{basis_name}/program/libvbahelperl?.so
%{instdir}/%{basis_name}/program/libvbaobjl?.uno.so
%{instdir}/%{basis_name}/program/libvbaswobjl?.uno.so
%{instdir}/%{basis_name}/program/libvcll?.so
%{instdir}/%{basis_name}/program/libvclplug_genl?.so
%{instdir}/%{basis_name}/program/libvclplug_gtkl?.so
%{instdir}/%{basis_name}/program/libvclplug_svpl?.so
%{instdir}/%{basis_name}/program/libwpftl?.so
%{instdir}/%{basis_name}/program/libwpgimportl?.so
%{instdir}/%{basis_name}/program/libwriterfilterl?.so
%{instdir}/%{basis_name}/program/libxcrl?.so
%{instdir}/%{basis_name}/program/libxmlfal?.so
%{instdir}/%{basis_name}/program/libxmlfdl?.so
%{instdir}/%{basis_name}/program/libxmxl?.so
%{instdir}/%{basis_name}/program/libxofl?.so
%{instdir}/%{basis_name}/program/libxol?.so
%{instdir}/%{basis_name}/program/libxsltdlgl?.so
%{instdir}/%{basis_name}/program/libxsltfilterl?.so

# Koko Made

%{instdir}/%{basis_name}/program/libxsec_fw.so
%{instdir}/%{basis_name}/program/libxsec_xmlsec.so
#%%{instdir}/%{basis_name}/program/libxsltdlg680*.so
#%%{instdir}/%{basis_name}/program/libxsltfilter680*.so
%{instdir}/%{basis_name}/program/libxstor.so
%{instdir}/%{basis_name}/program/localebe1.uno.so
%{instdir}/%{basis_name}/program/migrationoo2.uno.so
%{instdir}/%{basis_name}/program/migrationoo3.uno.so
#%%{instdir}/%{basis_name}/program/namingservice.uno.so
#%%{instdir}/%{basis_name}/program/nestedreg.uno.so
%{instdir}/%{basis_name}/program/nsplugin
#%%{instdir}/%{basis_name}/program/oo_product.bmp
%{instdir}/program/oosplash.bin
%{instdir}/%{basis_name}/program/oovbaapi.rdb
%{instdir}/%{basis_name}/program/open-url
%{instdir}/%{basis_name}/program/pagein
%{instdir}/%{basis_name}/program/pagein-calc
%{instdir}/%{basis_name}/program/pagein-common
%{instdir}/%{basis_name}/program/pagein-draw
%{instdir}/%{basis_name}/program/pagein-impress
%{instdir}/%{basis_name}/program/pagein-writer
%{instdir}/%{basis_name}/program/passwordcontainer.uno.so
#%%{instdir}/%{basis_name}/program/pkgchk
#%%{instdir}/%{basis_name}/program/pkgchk.bin
%{instdir}/%{basis_name}/program/plugin
%{instdir}/%{basis_name}/program/pluginapp.bin
%{instdir}/%{basis_name}/program/productregistration.uno.so
#%%{instdir}/%{basis_name}/program/proxyfac.uno.so
#%%{instdir}/%{basis_name}/program/reflection.uno.so
#%%{instdir}/%{basis_name}/program/regtypeprov.uno.so
#%%{instdir}/%{basis_name}/program/remotebridge.uno.so
#%%{instdir}/%{basis_name}/program/vbaevents680*.uno.so
%dir %{instdir}/program/resource
%{instdir}/program/resource/oooen-US.res
%dir %{instdir}/%{basis_name}/program/resource
%{instdir}/%{basis_name}/program/resource/*en-US.res
#%%{instdir}/%{basis_name}/program/resource/acc680en-US.res
#%%{instdir}/%{basis_name}/program/resource/avmedia680en-US.res
#%%{instdir}/%{basis_name}/program/resource/abp680en-US.res
#%%{instdir}/%{basis_name}/program/resource/basctl680en-US.res
# we dont build binfilter
#%{instdir}/%{basis_name}/program/resource/bf_frm680en-US.res
#%{instdir}/%{basis_name}/program/resource/bf_ofa680en-US.res
#%{instdir}/%{basis_name}/program/resource/bf_sch680en-US.res
#%{instdir}/%{basis_name}/program/resource/bf_sd680en-US.res
#%{instdir}/%{basis_name}/program/resource/bf_sfx680en-US.res
#%{instdir}/%{basis_name}/program/resource/bf_svx680en-US.res
#%{instdir}/%{basis_name}/program/resource/bf_sw680en-US.res
#%%{instdir}/%{basis_name}/program/resource/bib680en-US.res
#%%{instdir}/%{basis_name}/program/resource/cal680en-US.res
#%%{instdir}/%{basis_name}/program/resource/chartcontroller680en-US.res
#%%{instdir}/%{basis_name}/program/resource/dba680en-US.res
#%%{instdir}/%{basis_name}/program/resource/dbp680en-US.res
#%%{instdir}/%{basis_name}/program/resource/dbu680en-US.res
#%%{instdir}/%{basis_name}/program/resource/dbw680en-US.res
#%%{instdir}/%{basis_name}/program/resource/deployment680en-US.res
#%%{instdir}/%{basis_name}/program/resource/deploymentgui680en-US.res
#%%{instdir}/%{basis_name}/program/resource/dkt680en-US.res
#%%{instdir}/%{basis_name}/program/resource/egi680en-US.res
#%%{instdir}/%{basis_name}/program/resource/eme680en-US.res
#%%{instdir}/%{basis_name}/program/resource/epb680en-US.res
#%%{instdir}/%{basis_name}/program/resource/epg680en-US.res
#%%{instdir}/%{basis_name}/program/resource/epp680en-US.res
#%%{instdir}/%{basis_name}/program/resource/eps680en-US.res
#%%{instdir}/%{basis_name}/program/resource/ept680en-US.res
#%%{instdir}/%{basis_name}/program/resource/eur680en-US.res
#%%{instdir}/%{basis_name}/program/resource/fps_office680en-US.res
#%%{instdir}/%{basis_name}/program/resource/frm680en-US.res
#%%{instdir}/%{basis_name}/program/resource/fwe680en-US.res
#%%{instdir}/%{basis_name}/program/resource/gal680en-US.res
#%%{instdir}/%{basis_name}/program/resource/imp680en-US.res
# %{instdir}/%{basis_name}/program/resource/lgd680en-US.res
#%%{instdir}/%{basis_name}/program/resource/ofa680en-US.res
#%%{instdir}/%{basis_name}/program/resource/ooo680en-US.res
#%%{instdir}/%{basis_name}/program/resource/pcr680en-US.res
#%%{instdir}/%{basis_name}/program/resource/pdffilter680en-US.res
#%%{instdir}/%{basis_name}/program/resource/preload680en-US.res
#%%{instdir}/%{basis_name}/program/resource/productregistration680en-US.res
%if %{with_java}
#%%{instdir}/%{basis_name}/program/resource/rpt680en-US.res
#%%{instdir}/%{basis_name}/program/resource/rptui680en-US.res
%endif
#%%{instdir}/%{basis_name}/program/resource/san680en-US.res
#%%{instdir}/%{basis_name}/program/resource/sb680en-US.res
#%%{instdir}/%{basis_name}/program/resource/sd680en-US.res
#%%{instdir}/%{basis_name}/program/resource/sdbcl680en-US.res
#%%{instdir}/%{basis_name}/program/resource/sdberr680en-US.res
#%%{instdir}/%{basis_name}/program/resource/sdbt680en-US.res
#%%{instdir}/%{basis_name}/program/resource/scsolver680en-US.res
#%%{instdir}/%{basis_name}/program/resource/sfx680en-US.res
#%%{instdir}/%{basis_name}/program/resource/spa680en-US.res
#%%{instdir}/%{basis_name}/program/resource/svs680en-US.res
#%%{instdir}/%{basis_name}/program/resource/svt680en-US.res
#%%{instdir}/%{basis_name}/program/resource/svx680en-US.res
#%%{instdir}/%{basis_name}/program/resource/sw680en-US.res
#%%{instdir}/%{basis_name}/program/resource/textconversiondlgs680en-US.res
#%%{instdir}/%{basis_name}/program/resource/t602filter680en-US.res
#%%{instdir}/%{basis_name}/program/resource/tfu680en-US.res
#%%{instdir}/%{basis_name}/program/resource/tk680en-US.res
#%%{instdir}/%{basis_name}/program/resource/tpl680en-US.res
#%%{instdir}/%{basis_name}/program/resource/updchk680en-US.res
#%%{instdir}/%{basis_name}/program/resource/uui680en-US.res
#%%{instdir}/%{basis_name}/program/resource/vcl680en-US.res
#%%{instdir}/%{basis_name}/program/resource/wzi680en-US.res
#%%{instdir}/%{basis_name}/program/resource/xmlsec680en-US.res
#%%{instdir}/%{basis_name}/program/resource/xsltdlg680en-US.res
%{instdir}/%{basis_name}/program/root*.dat
%{instdir}/%{basis_name}/program/sax.uno.so
#%%{instdir}/%{basis_name}/program/scsolver.uno.so
#%%{instdir}/%{basis_name}/program/security.uno.so
%{instdir}/%{basis_name}/program/senddoc
#%%{instdir}/%{basis_name}/program/servicemgr.uno.so
%{instdir}/%{basis_name}/program/services.rdb
%{instdir}/program/setuprc
#%%{instdir}/%{basis_name}/program/shlibloader.uno.so
%{instdir}/%{basis_name}/program/simplecanvas.uno.so
#%%{instdir}/%{basis_name}/program/simplereg.uno.so
%{instdir}/%{basis_name}/program/slideshow.uno.so
#%%{instdir}/%{basis_name}/program/setofficelang*
#%%{instdir}/%{basis_name}/program/soffice
#%%{instdir}/%{basis_name}/program/soffice.bin
# ?
#%{instdir}/%{basis_name}/program/libsoffice.so
#%%{instdir}/%{basis_name}/program/stocservices.uno.so
#%%{instdir}/%{basis_name}/program/streams.uno.so
#%%{instdir}/%{basis_name}/program/stringresource680*.so
# java
%if %{with_java}
#%%{instdir}/%{basis_name}/program/sunjavaplugin.so
%endif
#%{instdir}/%{basis_name}/program/svtmisc.uno.so
#%{instdir}/%{basis_name}/program/sysmgr1.uno.so
%{instdir}/%{basis_name}/program/syssh.uno.so
#%%{instdir}/%{basis_name}/program/textinstream.uno.so
#%%{instdir}/%{basis_name}/program/textoutstream.uno.so
#%%{instdir}/%{basis_name}/program/typeconverter.uno.so
#%%{instdir}/%{basis_name}/program/typemgr.uno.so
#%%{instdir}/%{basis_name}/program/types.rdb
%{instdir}/%{basis_name}/program/ucpexpand1.uno.so
%{instdir}/%{basis_name}/program/ucpext.uno.so
%{instdir}/%{basis_name}/program/ucptdoc1.uno.so
%{instdir}/%{basis_name}/program/unorc
%{instdir}/%{basis_name}/program/updatefeed.uno.so
%{instdir}/%{basis_name}/program/uri-encode
#%%{instdir}/%{basis_name}/program/uriproc.uno.so
#%%{instdir}/%{basis_name}/program/uuresolver.uno.so
%{instdir}/%{basis_name}/program/vclcanvas.uno.so
# ?
#%%{instdir}/%{basis_name}/program/viewdoc
%{instdir}/program/sofficerc
%{instdir}/program/spadmin
#%%{instdir}/program/spadmin.bin
%{instdir}/program/unoinfo
%{instdir}/program/unopkg
%{instdir}/program/unopkg.bin

# ToDo
%{instdir}/%{basis_name}/program/offapi.rdb
%{instdir}/%{basis_name}/program/spadmin.bin
%{instdir}/%{basis_name}/program/stringresourcel?.uno.so
%{instdir}/%{basis_name}/program/vbaeventsl?.uno.so
%{instdir}/%{basis_name}/program/versionrc

%{instdir}/program/shell/backing_left.png
%{instdir}/program/shell/backing_right.png
%{instdir}/program/shell/backing_rtl_left.png
%{instdir}/program/shell/backing_rtl_right.png
%{instdir}/program/shell/backing_space.png

%{instdir}/program/fundamentalrc
%{instdir}/program/libnpsoplugin.so
%{instdir}/program/about.png
%{instdir}/program/intro.png
#%{instdir}/program/openabout_go-oo.bmp
#%{instdir}/program/openintro_go-oo.bmp
%{instdir}/program/redirectrc

%{instdir}/program/versionrc
%dir %{instdir}/share
%dir %{instdir}/%{basis_name}/share/Scripts
%{instdir}/%{basis_name}/share/Scripts/beanshell
%{instdir}/%{basis_name}/share/Scripts/javascript
# java
%if %{with_java}
%{instdir}/%{basis_name}/share/Scripts/java
%endif
%{instdir}/%{basis_name}/share/Scripts/python
%dir %{instdir}/%{basis_name}/share/autocorr
%dir %{instdir}/%{basis_name}/share/autotext
%{instdir}/%{basis_name}/share/autotext/en-US
%{instdir}/%{basis_name}/share/basic

%dir %{instdir}/%{basis_name}/share/config
%{instdir}/%{basis_name}/share/config/images.zip
%{instdir}/share/config/images_brand.zip
%{instdir}/%{basis_name}/share/registry/Langpack-en-US.xcd
#%{instdir}/%{basis_name}/share/config/images_industrial.zip
%{instdir}/%{basis_name}/share/config/images_classic.zip
%{instdir}/%{basis_name}/share/config/images_crystal.zip
%{instdir}/%{basis_name}/share/config/images_hicontrast.zip
%{instdir}/%{basis_name}/share/config/images_tango.zip
%{instdir}/%{basis_name}/share/config/javasettingsunopkginstall.xml
%{instdir}/%{basis_name}/share/config/images_oxygen.zip
# java
%if %{with_java}
#%%{instdir}/%{basis_name}/share/config/javavendors.xml
%endif
%{instdir}/%{basis_name}/share/config/psetup.xpm
%{instdir}/%{basis_name}/share/config/psetupl.xpm
%dir %{instdir}/%{basis_name}/share/config/soffice.cfg
#%{instdir}/%{basis_name}/share/config/soffice.cfg/global
%{instdir}/%{basis_name}/share/config/soffice.cfg/modules
%{instdir}/%{basis_name}/share/config/symbol
%{instdir}/%{basis_name}/share/config/webcast
%{instdir}/%{basis_name}/share/config/wizard
#%%dir %{instdir}/share/dict
#%%dir %{instdir}/share/dict/ooo
%{instdir}/share/extensions/dict-*
%{instdir}/share/extensions/package.txt
%dir %{instdir}/%{basis_name}/share/dtd
%{instdir}/%{basis_name}/share/dtd/officedocument
%{instdir}/%{basis_name}/share/dtd/math
%{instdir}/%{basis_name}/share/fingerprint
#%%{instdir}/%{basis_name}/share/fonts
%{instdir}/%{basis_name}/share/gallery
%{instdir}/%{basis_name}/share/template/layout/*
#%{instdir}/%{basis_name}/share/layout/en-US
#%{instdir}/%{basis_name}/share/layout/*.xml
%{instdir}/%{basis_name}/share/psprint
#%dir %{instdir}/share/readme
#%{instdir}/share/readme/LICENSE_en-US
#%{instdir}/share/readme/LICENSE_en-US.html
#%{instdir}/share/readme/README_en-US
#%{instdir}/share/readme/README_en-US.html
%{instdir}/share/registry/brand.xcd
%dir %{instdir}/%{basis_name}/share/registry
#%dir %{instdir}/%{basis_name}/share/registry/data
#%dir %{instdir}/%{basis_name}/share/registry/data/org
#%dir %{instdir}/%{basis_name}/share/registry/data/org/openoffice
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/FirstStartWizard.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Inet.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/LDAP.xcu.sample
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/UserProfile.xcu
#%dir %{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Accelerators.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Calc.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Canvas.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Common.xcu
#%{instdir}/share/registry/data/org/openoffice/Office/Compatibility.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/DataAccess.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Embedding.xcu
##%%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/ExtendedColorScheme.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/ExtensionManager.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/FormWizard.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Histories.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Impress.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Jobs.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Labels.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Logging.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Math.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Paths.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/ProtocolHandler.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/SFX.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Scripting.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Security.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/TableWizard.xcu
#%dir %{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/BaseWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/BasicIDECommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/BasicIDEWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/BibliographyCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/ChartCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/ChartWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/Controller.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DbBrowserWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DbQueryWindowState.xcu
##%%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DbReportWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DbRelationWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DbTableDataWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DbTableWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DbuCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DrawImpressCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/Factories.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/GenericCategories.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/GenericCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/StartModuleCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/StartModuleWindowState.xcu
##%%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/ReportCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/WriterFormWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/WriterReportWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/XFormsWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Views.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/WebWizard.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/Writer.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Setup.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/TypeDetection
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/VCL.xcu
#%dir %{instdir}/%{basis_name}/share/registry/data/org/openoffice/ucb
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/ucb/Configuration.xcu
#%dir %{instdir}/%{basis_name}/share/registry/ldap
#%{instdir}/%{basis_name}/share/registry/ldap/oo-ad-ldap-attr.map
#%{instdir}/%{basis_name}/share/registry/ldap/oo-ldap-attr.map
#%dir %{instdir}/%{basis_name}/share/registry/modules
#%dir %{instdir}/%{basis_name}/share/registry/modules/org
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Accelerators/Accelerators-unxwnt.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Embedding
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-unx.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-UseOOoFileDialogs.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/DataAccess
##%%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-dicooo.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Linguistic
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Paths
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Paths/Paths-unxwnt.xcu
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Langpack-en-US.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Setup-start.xcu
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_base_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_chart_filters.xcu
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/GraphicFilter
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/GraphicFilter/fcfg_internalgraphics_filters.xcu
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Misc
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Misc/fcfg_base_others.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Misc/fcfg_chart_others.xcu
#%dir %{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_base_types.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_chart_types.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_internalgraphics_types.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/UISort

# ToDo
#%{instdir}/share/registry/modules/org/openoffice/Office/Common/Common-brand.xcu
#%dir %{instdir}/share/registry/modules/org/openoffice/Office/UI
#%{instdir}/share/registry/modules/org/openoffice/Office/UI/UI-brand.xcu
#%{instdir}/share/registry/modules/org/openoffice/Setup/Setup-brand.xcu

%dir %{instdir}/%{basis_name}/share/registry/res
%{instdir}/%{basis_name}/share/registry/base.xcd
%{instdir}/%{basis_name}/share/registry/ctl_ar.xcd
%{instdir}/%{basis_name}/share/registry/ctl_dz.xcd
%{instdir}/%{basis_name}/share/registry/ctl_gu-IN.xcd
%{instdir}/%{basis_name}/share/registry/ctl_he.xcd
%{instdir}/%{basis_name}/share/registry/ctl_hi-IN.xcd
%{instdir}/%{basis_name}/share/registry/ctl_ne.xcd
%{instdir}/%{basis_name}/share/registry/ctl_or.xcd
%{instdir}/%{basis_name}/share/registry/ctl_pa-IN.xcd
%{instdir}/%{basis_name}/share/registry/ctl_th.xcd
%{instdir}/%{basis_name}/share/registry/gnome.xcd
%{instdir}/%{basis_name}/share/registry/lingucomponent.xcd
%{instdir}/%{basis_name}/share/registry/main.xcd
%{instdir}/%{basis_name}/share/registry/ogltrans.xcd
%{instdir}/%{basis_name}/share/registry/oo-ad-ldap.xcd.sample
%{instdir}/%{basis_name}/share/registry/oo-ldap.xcd.sample
%{instdir}/%{basis_name}/share/registry/palm.xcd
%{instdir}/%{basis_name}/share/registry/pocketexcel.xcd
%{instdir}/%{basis_name}/share/registry/pocketword.xcd

%{instdir}/%{basis_name}/share/registry/res/fcfg_langpack_en-US.xcd
%{instdir}/%{basis_name}/share/registry/xsltfilter.xcd

# %{instdir}/%{basis_name}/share/registry/res/en-US
#%dir %{instdir}/%{basis_name}/share/registry/schema
#%dir %{instdir}/%{basis_name}/share/registry/schema/org
#%dir %{instdir}/%{basis_name}/share/registry/schema/org/openoffice
##%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/FirstStartWizard.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Inet.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/LDAP.xcs
#%dir %{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Accelerators.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Addons.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Calc.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/CalcAddIns.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Canvas.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Chart.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Commands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Common.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Compatibility.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/DataAccess
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/DataAccess.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Draw.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Embedding.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/ExtendedColorScheme.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/ExtensionManager.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Events.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/FormWizard.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Histories.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Impress.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Java.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Jobs.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Labels.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Linguistic.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Logging.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Math.xcs
# ? FC specific?
##%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/OptionsDialog.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Paths.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/ProtocolHandler.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Recovery.xcs
##%%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/ReportDesign.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/SFX.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Scripting.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Security.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Substitution.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/TableWizard.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/TabBrowse.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/TypeDetection.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/OOoImprovement
#%dir %{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/BaseWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/BasicIDECommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/BasicIDEWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/BibliographyCommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/BibliographyWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/Category.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/ChartCommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/ChartWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/Commands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/Controller.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DbBrowserWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DbQueryWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DbRelationWindowState.xcs
##%%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DbReportWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DbTableDataWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DbTableWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DbuCommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DrawImpressCommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/Factories.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/GenericCategories.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/GenericCommands.xcs
# ? FC specific?
##%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/GlobalSettings.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/MathWindowState.xcs
##%%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/ReportCommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/StartModuleCommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/StartModuleWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/WindowContentFactories.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/WindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/WriterFormWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/WriterReportWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/XFormsWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Views.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/WebWizard.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/Writer.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/WriterWeb.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Setup.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/System.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/TypeDetection
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/UserProfile.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/VCL.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/ucb
%dir %{instdir}/%{basis_name}/share/samples
%{instdir}/%{basis_name}/share/samples/en-US
%dir %{instdir}/%{basis_name}/share/template
%{instdir}/%{basis_name}/share/template/en-US
%{instdir}/%{basis_name}/share/template/wizard
%dir %{instdir}/share/uno_packages
%dir %{instdir}/share/uno_packages/cache
%{instdir}/%{basis_name}/share/wordbook
%dir %{instdir}/%{basis_name}/share/xslt
%{instdir}/%{basis_name}/share/xslt/common
%dir %{instdir}/%{basis_name}/share/xslt/export
%{instdir}/%{basis_name}/share/xslt/export/common
%{instdir}/%{basis_name}/share/xslt/export/spreadsheetml
%{instdir}/%{basis_name}/share/xslt/export/wordml
%{instdir}/%{basis_name}/share/xslt/export/uof
%dir %{instdir}/%{basis_name}/share/xslt/import
%{instdir}/%{basis_name}/share/xslt/import/common
%{instdir}/%{basis_name}/share/xslt/import/spreadsheetml
%{instdir}/%{basis_name}/share/xslt/import/uof
%{instdir}/%{basis_name}/share/xslt/import/wordml
%{instdir}/%{basis_name}/share/xslt/odfflatxml
#%%{instdir}/%{basis_name}/share/xslt/wiki
#spellchecking lib
#%%{instdir}/%{basis_name}/program/liblnth680*.so
#launchers
%{_bindir}/libreoffice
##%{_bindir}/ooviewdoc
#%%{_sbindir}/oopadmin
%{_bindir}/ooconfig
%{_bindir}/lofromtemplate
#%%{_bindir}/ooo-wrapper
%{_bindir}/ootool
%{_bindir}/soffice
%{_bindir}/unopkg
%{instdir}/program/soffice
%{instdir}/program/soffice.bin
#gnome integration
%{instdir}/%{basis_name}/program/gconfbe1.uno.so
#%%{instdir}/%{basis_name}/program/gnome-set-default-application
%{instdir}/%{basis_name}/program/ucpgvfs1.uno.so
%{instdir}/share/xdg
#icons and mime
#%%{_datadir}/application-registry
%{_datadir}/icons/hicolor/*/apps/ooo-*.png
%{_datadir}/icons/hicolor/*/apps/ooo-*.svg
%{_datadir}/pixmaps/*
%{_mandir}/man1/*
%{_datadir}/applications/template.desktop
%{_datadir}/applications/ooo-extension-manager.desktop
%{_datadir}/applications/startcenter.desktop
%{_datadir}/mime/packages/openoffice.xml
#%dir %{_datadir}/mime-info
#%{_datadir}/mime-info/*
#%if ! %{galangpack}
#%{instdir}/share/dict/ooo/*ga_IE*
#%endif
#%if %{internalxmlsec}
%{instdir}/%{basis_name}/program/libxmlsec*
#%else
#%{instdir}/%{basis_name}/program/libxmlsecurity.so
#%endif
#%if %{stlvisibilityfcked}
#%%ifnarch x86_64
#%%{instdir}/%{basis_name}/program/libstlport*.so
#%%endif
#%endif
#%if %{internalicu}
#%%{instdir}/%{basis_name}/program/libicu*
#%endif
# java
%if %{with_java}
%{instdir}/%{basis_name}/program/classes/bsh.jar
%{instdir}/%{basis_name}/program/classes/hsqldb.jar
#%%{instdir}/%{basis_name}/program/classes/xalan.jar
#%%{instdir}/%{basis_name}/program/classes/xercesImpl.jar
#%%{instdir}/%{basis_name}/program/classes/xml-apis.jar
%endif

# Kokomade

%post core
update-desktop-database -q %{_datadir}/applications
for theme in hicolor; do
  if [ -x /usr/bin/gtk-update-icon-cache ]; then
    if test -f "%{_datadir}/icons/$theme"; then
      if test -f "%{_datadir}/icons/$theme/index.theme"; then
        touch --no-create %{_datadir}/icons/$theme
        gtk-update-icon-cache -q %{_datadir}/icons/$theme
      fi
    fi
  fi
done
/sbin/ldconfig

%postun core
update-desktop-database -q %{_datadir}/applications
for theme in hicolor; do
  if [ -x /usr/bin/gtk-update-icon-cache ]; then
    if test -f "%{_datadir}/icons/$theme"; then
      if test -f "%{_datadir}/icons/$theme/index.theme"; then
        touch --no-create %{_datadir}/icons/$theme
        gtk-update-icon-cache -q %{_datadir}/icons/$theme
      fi
    fi
  fi
done
/sbin/ldconfig

%if %{enable_kde}
%files kde
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/program/kdefilepicker
%{instdir}/%{basis_name}/program/fps_kde.uno.so
%{instdir}/%{basis_name}/program/kdebe1.uno.so
%{instdir}/%{basis_name}/program/libkab1.so
%{instdir}/%{basis_name}/program/libkabdrv1.so
%{instdir}/%{basis_name}/program/libvclplug_kde680*.so
%{_libdir}/libvclplug_kde680*.so

%post kde
/sbin/ldconfig

%postun kde
/sbin/ldconfig
%endif

%files base
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/en/sdatabase.*
#%%{instdir}/%{basis_name}/program/resource/cnr680en-US.res
%{instdir}/program/sbase
# java
%if %{with_java}
#%%{instdir}/%{basis_name}/program/libhsqldb2.so
%endif
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-base.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Setup-base.xcu
##%%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Setup-report.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_database_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Misc/fcfg_database_others.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_database_types.xcu
%{_bindir}/lobase
%{_datadir}/applications/base.desktop
##%{_datadir}/mimelnk/application/openoffice.org-*-oasis-database.desktop

%post base
update-desktop-database -q %{_datadir}/applications

%postun base
update-desktop-database -q %{_datadir}/applications

%files calc
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/help/en/scalc.*
#%%{instdir}/%{basis_name}/program/libanalysis680*.so
# we dont build binfilter
#%{instdir}/%{basis_name}/program/libbf_sc680*.so
#%%{instdir}/%{basis_name}/program/libcalc680*.so
#%%{instdir}/%{basis_name}/program/libdate680*.so
#%%{instdir}/%{basis_name}/program/libscd680*.so
#%%{instdir}/%{basis_name}/program/resource/analysis680en-US.res
# we dont build binfilter
#%{instdir}/%{basis_name}/program/resource/bf_sc680en-US.res
#%%{instdir}/%{basis_name}/program/resource/date680en-US.res
#%%{instdir}/%{basis_name}/program/resource/sc680en-US.res
%{instdir}/program/scalc
%{instdir}/%{basis_name}/share/registry/calc.xcd
#%{instdir}/%{basis_name}/program/libxlsxl?.so
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/CalcCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/CalcWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-calc.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Setup-calc.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_calc_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_calc_types.xcu
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/OptionsDialog.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/CalcCommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/CalcWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/GlobalSettings.xcs
%{_datadir}/applications/calc.desktop
##%{_datadir}/mimelnk/application/*spreadsheet*
%{_bindir}/localc

%post calc
update-desktop-database -q %{_datadir}/applications

%postun calc
update-desktop-database -q %{_datadir}/applications

%files draw
%defattr(-,root,root,-)
# no help when --without-java
%{instdir}/%{basis_name}/help/en/sdraw.*
%{instdir}/program/sdraw
%{instdir}/%{basis_name}/program/libsdfiltl?.so
%{instdir}/%{basis_name}/share/registry/draw.xcd
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/DrawWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-draw.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Setup-draw.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_draw_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_draw_types.xcu
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/DrawWindowState.xcs
##%if ! %{cripplemenus}
%{_datadir}/applications/draw.desktop
##%endif
##%{_datadir}/mimelnk/application/*drawing*
%{_bindir}/lodraw

%post draw
update-desktop-database -q %{_datadir}/applications

%postun draw
update-desktop-database -q %{_datadir}/applications

%if 1
%files emailmerge
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/program/mailmerge.py*
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Writer/Writer-javamail.xcu
%endif

%files writer
%defattr(-,root,root,-)
# no help when --without-java
%{instdir}/%{basis_name}/help/en/swriter.*
%{instdir}/%{basis_name}/program/libhwp.so
%{instdir}/%{basis_name}/share/registry/writer.xcd
#%%{instdir}/%{basis_name}/program/libdocxl?.so
#%%{instdir}/%{basis_name}/program/libswd680*.so
#%%{instdir}/%{basis_name}/program/libswui680*.so
#%%{instdir}/%{basis_name}/program/libwpft680*.so
#%%{instdir}/%{basis_name}/program/libwpgimport680*.so
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/WriterCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/WriterGlobalWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/WriterWebWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/WriterWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-writer.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Setup-writer.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_global_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_web_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_writer_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_global_types.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_web_types.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_writer_types.xcu
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/WriterCommands.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/WriterGlobalWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/WriterWebWindowState.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/WriterWindowState.xcs
%{instdir}/program/swriter
%{_datadir}/applications/writer.desktop
%{_datadir}/applications/web.desktop
##%{_datadir}/mimelnk/application/*master*
##%{_datadir}/mimelnk/application/*text*
##%{_datadir}/mimelnk/application/*web*
%{_bindir}/lowriter
%{_bindir}/loweb

%post writer
update-desktop-database -q %{_datadir}/applications

%postun writer
update-desktop-database -q %{_datadir}/applications

%files impress
%defattr(-,root,root,-)
# no help when --without-java
%{instdir}/%{basis_name}/help/en/simpress.*
%{instdir}/%{basis_name}/program/libanimcore.so
%{instdir}/%{basis_name}/program/libplaceware*.so
#%{instdir}/%{basis_name}/program/libpptxl?.so
%{instdir}/%{basis_name}/program/OGLTrans.uno.so
%{instdir}/%{basis_name}/share/registry/impress.xcd
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/Effects.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/ImpressWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-impress.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Impress/Impress-ogltrans.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Setup-impress.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_impress_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_impress_types.xcu
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/Effects.xcs
#%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/ImpressWindowState.xcs
%{instdir}/%{basis_name}/share/config/soffice.cfg/simpress
%{instdir}/program/simpress
%{_datadir}/applications/impress.desktop
##%{_datadir}/mimelnk/application/*presentation*
%{_bindir}/loimpress

%post impress
update-desktop-database -q %{_datadir}/applications

%postun impress
update-desktop-database -q %{_datadir}/applications

%files math
%defattr(-,root,root,-)
# no help when --without-java
%{instdir}/%{basis_name}/help/en/smath.*
%{instdir}/%{basis_name}/share/registry/math.xcd
#%%{instdir}/%{basis_name}/program/libsm680*.so
#%%{instdir}/%{basis_name}/program/libsmd680*.so
# we dont build binfilter
#%{instdir}/%{basis_name}/program/resource/bf_sm680en-US.res
#%{instdir}/%{basis_name}/program/resource/sm680en-US.res
%{instdir}/program/smath
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/MathCommands.xcu
#%{instdir}/%{basis_name}/share/registry/data/org/openoffice/Office/UI/MathWindowState.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Common/Common-math.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Setup/Setup-math.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_math_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_math_types.xcu
##%{instdir}/%{basis_name}/share/registry/schema/org/openoffice/Office/UI/MathCommands.xcs
##%if ! %{cripplemenus}
%{_datadir}/applications/math.desktop
##%endif
##%{_datadir}/mimelnk/application/*formula*
%{_bindir}/lomath

%post math
update-desktop-database -q %{_datadir}/applications

%postun math
update-desktop-database -q %{_datadir}/applications

%files graphicfilter
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/share/registry/graphicfilter.xcd
#%%{instdir}/%{basis_name}/program/libflash680*.so
#%%{instdir}/%{basis_name}/program/libsvgfilter680*.so
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_drawgraphics_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_impressgraphics_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_drawgraphics_types.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_impressgraphics_types.xcu

%files xsltfilter
%defattr(-,root,root,-)
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_xslt_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_xslt_types.xcu
%{instdir}/%{basis_name}/share/xslt/docbook
%{instdir}/%{basis_name}/share/xslt/export/xhtml

%if %{with_java}
%files javafilter
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/program/classes/aportisdoc.jar
%{instdir}/%{basis_name}/program/classes/pexcel.jar
%{instdir}/%{basis_name}/program/classes/pocketword.jar
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_palm_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_pocketexcel_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Filter/fcfg_pocketword_filters.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_palm_types.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_pocketexcel_types.xcu
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/TypeDetection/Types/fcfg_pocketword_types.xcu
%endif

%files testtools
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/program/hid.lst
%{instdir}/%{basis_name}/program/testtoolrc
#%%{instdir}/%{basis_name}/program/libcommuni680*.so
#%%{instdir}/%{basis_name}/program/libsimplecm680*.so
%{instdir}/%{basis_name}/program/testtool.bin
#%%{instdir}/%{basis_name}/program/resource/stt680en-US.res

%files pyuno
%defattr(-,root,root,-)
%{instdir}/%{basis_name}/program/libpyuno.so
%{instdir}/%{basis_name}/program/officehelper.py*
%{instdir}/%{basis_name}/program/pythonloader.py*
%{instdir}/%{basis_name}/program/pythonloader.uno.so
%{instdir}/%{basis_name}/program/pythonloader.unorc
%{instdir}/%{basis_name}/program/pythonscript.py*
%{instdir}/%{basis_name}/program/pyuno.so
%{instdir}/%{basis_name}/program/uno.py*
%{instdir}/%{basis_name}/program/unohelper.py*
%ifnarch x86_64 ppc
%{instdir}/%{basis_name}/program/pyunorc-update64
%endif
%{instdir}/%{basis_name}/share/registry/pyuno.xcd
#%%{instdir}/share/Scripts/python
#%{instdir}/%{basis_name}/share/registry/modules/org/openoffice/Office/Scripting/Scripting-python.xcu

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{ooodirname}
%dir %{instdir}/%{basis_name}/sdk
%{instdir}/%{basis_name}/sdk/bin
%{instdir}/%{basis_name}/sdk/classes
%{instdir}/%{basis_name}/sdk/idl
%{instdir}/%{basis_name}/sdk/include
%{instdir}/%{basis_name}/sdk/lib
%{instdir}/%{basis_name}/sdk/setsdkenv_unix*
%{instdir}/%{basis_name}/sdk/config*
%{instdir}/%{basis_name}/sdk/examples
#%%{instdir}/%{basis_name}/sdk/linux
%{instdir}/%{basis_name}/sdk/settings
%{_datadir}/%{ooodirname}/sdk
%{_datadir}/idl/%{ooodirname}
#%%{_datadir}/xml/%{ooodirname}

%files devel-doc
%defattr(-,root,root,-)
%docdir %{_datadir}/doc/packages/LibreOffice
%docdir %{_datadir}/doc/packages/LibreOffice/sdk
%doc %{_datadir}/doc/packages/LibreOffice/sdk/*
%{instdir}/%{basis_name}/sdk/docs
%{instdir}/%{basis_name}/sdk/index.html

%files ure
%defattr(-,root,root,-)
%dir %{instdir}/ure
%{instdir}/ure/*
%{instdir}/%{basis_name}/ure-link

%files -n autocorr-af
%{instdir}/%{basis_name}/share/autocorr/acor_af-*

%files -n autocorr-bg
%{instdir}/%{basis_name}/share/autocorr/acor_bg-*

%files -n autocorr-cs
%{instdir}/%{basis_name}/share/autocorr/acor_cs-*

%files -n autocorr-da
%{instdir}/%{basis_name}/share/autocorr/acor_da-*

%files -n autocorr-de
%{instdir}/%{basis_name}/share/autocorr/acor_de-*

%files -n autocorr-en
%{instdir}/%{basis_name}/share/autocorr/acor_en-*

%files -n autocorr-es
%{instdir}/%{basis_name}/share/autocorr/acor_es-*

%files -n autocorr-eu
%{instdir}/%{basis_name}/share/autocorr/acor_eu.dat

%files -n autocorr-fa
%{instdir}/%{basis_name}/share/autocorr/acor_fa-*

%files -n autocorr-fi
%{instdir}/%{basis_name}/share/autocorr/acor_fi-*

%files -n autocorr-fr
%{instdir}/%{basis_name}/share/autocorr/acor_fr-*

%files -n autocorr-ga
%{instdir}/%{basis_name}/share/autocorr/acor_ga-*

%files -n autocorr-hr
%{instdir}/%{basis_name}/share/autocorr/acor_hr-*

%files -n autocorr-hu
%{instdir}/%{basis_name}/share/autocorr/acor_hu-*

%files -n autocorr-it
%{instdir}/%{basis_name}/share/autocorr/acor_it-*

%files -n autocorr-ja
%{instdir}/%{basis_name}/share/autocorr/acor_ja-*

%files -n autocorr-ko
%{instdir}/%{basis_name}/share/autocorr/acor_ko-*

%files -n autocorr-lb
%{instdir}/%{basis_name}/share/autocorr/acor_lb-*

%files -n autocorr-lt
%{instdir}/%{basis_name}/share/autocorr/acor_lt-*

%files -n autocorr-mn
%{instdir}/%{basis_name}/share/autocorr/acor_mn-*

%files -n autocorr-nl
%{instdir}/%{basis_name}/share/autocorr/acor_nl-*

%files -n autocorr-pl
%{instdir}/%{basis_name}/share/autocorr/acor_pl-*

%files -n autocorr-pt
%{instdir}/%{basis_name}/share/autocorr/acor_pt-*

%files -n autocorr-ru
%{instdir}/%{basis_name}/share/autocorr/acor_ru-*

%files -n autocorr-sk
%{instdir}/%{basis_name}/share/autocorr/acor_sk-*

%files -n autocorr-sl
%{instdir}/%{basis_name}/share/autocorr/acor_sl-*

%files -n autocorr-sr
%{instdir}/%{basis_name}/share/autocorr/acor_sr-*

%files -n autocorr-sv
%{instdir}/%{basis_name}/share/autocorr/acor_sv-*

%files -n autocorr-tr
%{instdir}/%{basis_name}/share/autocorr/acor_tr-*

%files -n autocorr-vi
%{instdir}/%{basis_name}/share/autocorr/acor_vi-*

%files -n autocorr-zh
%{instdir}/%{basis_name}/share/autocorr/acor_zh-*


%files report-builder
%defattr(-,root,root,-)
%docdir %{_datadir}/openoffice.org/extensions/report-builder.oxt/help
%{_datadir}/openoffice.org/extensions/report-builder.oxt

%pre report-builder
if [ $1 -gt 1 ]; then
    # Upgrade => deregister old extension
    unopkg remove --shared com.sun.reportdesigner > /dev/null 2>&1 || :
fi

%post report-builder
    # register extension
    unopkg add --shared --force --link %{_datadir}/openoffice.org/extensions/report-builder.oxt > /dev/null 2>&1 || :

%preun report-builder
if [ $1 -eq 0 ]; then
    # not upgrading => deregister
    unopkg remove --shared com.sun.reportdesigner > /dev/null 2>&1 || :
fi

%postun report-builder
    # clear disk cache
    unopkg list --shared > /dev/null 2>&1 || :


%files wiki-publisher
%defattr(-,root,root,-)
%docdir %{_datadir}/openoffice.org/extensions/wiki-publisher.oxt/license
%{_datadir}/openoffice.org/extensions/wiki-publisher.oxt

%pre wiki-publisher
if [ $1 -gt 1 ]; then
    # Upgrade => deregister old extension
    unopkg remove --shared com.sun.wiki-publisher > /dev/null 2>&1 || :
fi

%post wiki-publisher
    # register extension
    unopkg add --shared --force --link %{_datadir}/openoffice.org/extensions/wiki-publisher.oxt > /dev/null 2>&1 || :

%preun wiki-publisher
if [ $1 -eq 0 ]; then
    # not upgrading => deregister
    unopkg remove --shared com.sun.wiki-publisher > /dev/null 2>&1 || :
fi

%postun wiki-publisher
    # clear disk cache
    unopkg list --shared > /dev/null 2>&1 || :


%files presentation-minimizer
%defattr(-,root,root,-)
%docdir %{instdir}/extensions/presentation-minimizer.oxt/help
%{instdir}/extensions/presentation-minimizer.oxt

%pre presentation-minimizer
if [ $1 -gt 1 ]; then
    # Upgrade => deregister old extension
    unopkg remove --shared `grep -s identifier %{instdir}/extensions/presentation-minimizer.oxt/description.xml | cut -d '"' -f 2` > /dev/null 2>&1 || :
fi
#"

%post presentation-minimizer
    # register extension
    unopkg add --shared --force --link %{instdir}/extensions/presentation-minimizer.oxt > /dev/null 2>&1 || :

%preun presentation-minimizer
if [ $1 -eq 0 ]; then
    # not upgrading => deregister
    unopkg remove --shared `grep -s identifier %{instdir}/extensions/presentation-minimizer.oxt/description.xml | cut -d '"' -f 2` > /dev/null 2>&1 || :
fi
#"

%postun presentation-minimizer
    # clear disk cache
    unopkg list --shared > /dev/null 2>&1 || :


%files presenter-screen
%defattr(-,root,root,-)
%docdir %{instdir}/extensions/presenter-screen.oxt/help
%{instdir}/extensions/presenter-screen.oxt

%pre presenter-screen
if [ $1 -gt 1 ]; then
    # Upgrade => deregister old extension
    unopkg remove --shared `grep -s identifier %{instdir}/extensions/presenter-screen.oxt/description.xml | cut -d '"' -f 2` > /dev/null 2>&1 || :
fi
#"

%post presenter-screen
    # register extension
    unopkg add --shared --force --link %{instdir}/extensions/presenter-screen.oxt > /dev/null 2>&1 || :

%preun presenter-screen
if [ $1 -eq 0 ]; then
    # not upgrading => deregister
    unopkg remove --shared `grep -s identifier %{instdir}/extensions/presenter-screen.oxt/description.xml | cut -d '"' -f 2` > /dev/null 2>&1 || :
fi
#"

%postun presenter-screen
    # clear disk cache
    unopkg list --shared > /dev/null 2>&1 || :


%files pdfimport
%defattr(-,root,root,-)
%docdir %{instdir}/extensions/pdfimport.oxt/help
%{instdir}/extensions/pdfimport.oxt

%pre pdfimport
if [ $1 -gt 1 ]; then
    # Upgrade => deregister old extension
    unopkg remove --shared `grep -s identifier %{instdir}/extensions/pdfimport.oxt/description.xml | cut -d '"' -f 2` > /dev/null 2>&1 || :
fi
#"

%post pdfimport
    # register extension
    unopkg add --shared --force --link %{instdir}/extensions/pdfimport.oxt > /dev/null 2>&1 || :

%preun pdfimport
if [ $1 -eq 0 ]; then
    # not upgrading => deregister
    unopkg remove --shared `grep -s identifier %{instdir}/extensions/pdfimport.oxt/description.xml | cut -d '"' -f 2` > /dev/null 2>&1 || :
fi
#"

%postun pdfimport
    # clear disk cache
    unopkg list --shared > /dev/null 2>&1 || :

%changelog
* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.4.1-1m)
- update 3.3.4.1

* Sun Aug 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.3.1-3m)
- use internal berkeley db

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.3.1-2m)
- rebuild against icu-4.6

* Sun Jun 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.3.1-1m)
- update 3.3.3.1

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.2.2-6m)
- rebuild against python-2.7

* Mon Apr 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2.2-5m)
- fix up unexecutable and hidden desktop files

* Mon Apr 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2.2-4m)
- add a patch to enable build on i686

* Mon Apr 18 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2.2-3m)
- replace --fexceptions to -fexceptions
- import gcc46.diff from Fedora
- I can not build this package yet, fix me

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.2.2-2m)
- rebuild for new GCC 4.6

* Fri Mar 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.2.2-1m)
- update 3.3.2.2
- cleanup spec

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0.4-4m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0.4-3m)
- rebuild against boost-1.46.0

* Wed Jan 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.0.4-2m)
- fix build on x86_64
- modify %%files to avoid conflicting
- fix up Provides and Obsoletes

* Mon Jan 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.0.4-1m)
- rename openoffice.org to libreoffice
- libreoffice-3.3 RC4

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.1-15m)
- rebuild against poppler-0.16.0

* Wed Dec  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.1-14m)
- import libX11-fix.diff from gentoo to enable build with new libX11
 +* 12 Nov 2010; Andreas Proschofsky <suka@gentoo.org> +files/libX11-fix.diff,
 +- openoffice-3.2.1-r1.ebuild:
 +- Fix build with >=libX11-1.3.99 bug #344993
- I can not build this package yet, we need more fixes

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-13m)
- rebuild for new GCC 4.5

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-12m)
- rebuild against poppler-0.14.3

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-11m)
- rebuild against qt-4.7.0-0.2.1m

* Mon Sep  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-10m)
- drop Requires: takao-gothic-fonts

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.1-9m)
- full rebuild for mo7 release

* Sat Aug 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-8m)
- update ooo-build-3.2.1.5
- [SECURITY] CVE-2010-2935, CVE-2010-2936

* Fri Aug 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.1-7m)
- remove Obsoletes: openoffice.org-langpack-kn_IN from package core

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-6m)
- rebuild against pentaho-reporting-flow-engine-0.9.4-1m

* Wed Jul 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-5m)
- split out autocorr-*, opensymbol-fonts and ure
- add langpacks
- install report-builder, wiki-publisher, presentation-minimizer,
  presenter-screen and pdfimport

* Sun Jul 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.1-4m)
- drop Patch102: momonga-splash.diff since the design of splash image
  has been changed

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-3m)
- build fix with desktop-file-utils-0.16

* Thu Jul  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-2m)
- omit ooo-build-3.1.1.3-glibc210.patch

* Tue Jun 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-1m)
- update ooo-build-3.2.1.4

* Mon Jun  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-12m)
- [SECURITY] CVE-2010-0395
- import a security patch from Fedora 11 (1:3.1.1-19.13)

* Fri May 14 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.1-11m)
- add BuildRequires

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.1.1-10m)
- rebuild against icu-4.2.1

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-9m)
- rebuild against libjpeg-8a

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-8m)
- [SECURITY] CVE-2009-2949 CVE-2009-2950
- [SECURITY] CVE-2009-3301 CVE-2009-3302 CVE-2009-0217
- import security patches from Debian unstable (1:3.1.1-16)

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-7m)
- rebuild against db-4.8.26

* Mon Feb  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-6m)
- rebuild against xulrunner-1.9.2

* Sat Dec  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-5m)
- update to ooo-build-3.1.1.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-3m)
- update to ooo-build-3.1.1.4

* Wed Oct 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-2m)
- add --with-about-bitmaps (Source100,101)
- add --with-vendor="Momonga Project"
- disable GoOoSplash (Patch1) and set the color of progressbar in
  intro splash on #FFED32 (Patch102)

* Thu Oct  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-1m)
- [SECURITY] CVE-2009-0200 CVE-2009-0201
- update to ooo-build-3.1.1.3 and ooo310-m19
- update Patch200,205,206 from OOo_3.1.1_patches_curvirgo.zip
- update %%files
 +- core own libsvxcorel?.so, libsvxmsfilterl?.so and images_oxygen.zip
 +- writer own libdocxl?.so
 +- calc own libxlsxl?.so
 +- impress own libpptxl?.so
 +- draw own libsdfiltl?.so
 +- ignore %%{instdir}/basis3.1/program/libempl?.so
- still disable kde4 since we can not build
- do not apply default-color-genwidget.diff (Patch101). because i do
  not merely want to apply this patch. but nosanosa said that default
  color for gen widget is too darty.
- when glibc211 on x86_64, apply dmake-use-memmove.diff which replace
  strcpy to memove. strcpy will behave strangely in rare cases.
- remove obsolete or unused momonga specific sources and patches
 +- already integrated sources
  +- ooo_crystal_images-1.tar.gz (Source30)
  +- ooo_crystal_images-6.tar.bz2 (Source31)
 +- do no rely on the sources
  +- lp_solve_5.5.0.10_source.tar.gz
 +- build with --with-system-{libwpd,libwpg,libwps}
  +- libwpd-0.8.14.tar.gz (Source37)
  +- libwpg-0.1.3.tar.gz (Source36)
  +- libwps-0.1.2.tar.gz (Source38)
 +- do not need
  +- libsvg-0.1.4.tar.gz (Source39)
 +- vlc patches are not needed since 2.4.0-4m because fontconfig seems
    to work well (nosanosa said)
  +- patch-waooo-1_2-VCL.xcu_font_fix_OOH680_m3.diff
  +- ja-waooo-vcl-setdefaultfontsize.diff
  +- ja-waooo-vcl-setinterfacefontsize.diff
  +- ja-waooo-vcl-setscreendpi.diff
 + remove jsdp2007 patches, which are for OOo241. if jsdp2007 will
   support OOo3x, then we will re-apply them.
  +- jsdp2007_2.4.0.1-Writer_additional_files.tar.bz2
  +- README.jsdp2007.txt
  +- jsdp2007_2.4.0.1-Writer_patch.diff
  +- cws-cjksp1-sw.disabled_for_jsdp2007.diff
  +- jsdp2007_2.4.0.1-Calc_patch.diff
 +- ja-waooo-word6_filter_fix_20.diff
 +- patch-waooo-12-issue82456_chart2_080303.diff
 +- patch-waooo-13-issue86579_chart2_080305.diff
 +- momonga-splash.diff
 +- config_office-kde3.diff
 +- kde4-configure.diff
 +- kde4-kab.diff
 +- kde4-kdebe.diff
 +- kde4-kdedata.cxx.diff
 +- kde4-kde_headers.diff
 +- kde4-salnativewidgets-kde.diff
 +- kde4-plugin-detection.diff
 +- kde4-fpicker.diff, i#90618, brosenk
 +- ooo-build-3.1.0.0-kde4-configure.diff.patch
 +- ooo-build-3.1.0.0-kde4-configure2.diff
 +- ooo-build-3.1.0.0-firefox-3.5.diff
 +- workspace.cmcfixes62.diff
 +- openoffice.org-3.1.0.oooXXXXX.gcc44.buildfixes.diff
 +- etc.

* Tue Sep 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-7m)
- apply gcc44 patch (Patch306)
- 64bit fixes (Patch305)
-- http://www.openoffice.org/issues/show_bug.cgi?id=104088
-- http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=539642
-- https://bugzilla.redhat.com/show_bug.cgi?id=518623

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-6m)
- rebuild against libjpeg-7

* Fri Jul 31 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.0-5m)
- add "BuildConflicts: rhbuildsys(DiskFree) < 20480Mb" for avoid disk full
  but this method is not correct always, ex. tmpfs on use WORKDIR in .OmoiKondara

* Fri Jul 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-4m)
- build with system icu, raptor, rasqal and redland

* Mon Jul 20 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.1.0-3m)
- merge from TSUPPA4RI

* Sun Jul 19 2009 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (3.1.0-2m)
- fix %%files to avoid conflicting
- fix up symlink for SDK

* Sun Jul 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-1m)
- replace intro-bitmaps (Source100,101)
- add configure options
-- --enable-gstreamer --enable-symbols --enable-dbus --enable-minimizer
-- --enable-presenter-console --enable-pdfimport --enable-wiki-publisher
-- --enable-vba --enable-report-builder --with-system-jfreereport

* Sun Jul 19 2009 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (3.1.0-0.5m)
- fix build on x86_64
- Obsoletes: openoffice.org-kde

* Sun Jul 19 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-0.4m)
- build successed!

* Fri Jul 10 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-0.3m)
- support FireFox-3.5
-- can't packaging this version

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-0.2m)
- update ooo-build-3.1.0.6
-- can't build this version

* Fri May  8 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-0.1m)
- [SECURITY] CVE-2009-2139 CVE-2009-2140
- update ooo-build-3.1.0
-- can't build this version

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-10m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-9m)
- update Patch4 for fuzz=0

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.1-8m)
- rebuild against python-2.6.1-1m

* Wed Nov 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-7m)
- [SECURITY] CVE-2008-4937
- revise specfile based on Debian unstable changes (1:2.4.1-8)

* Sat Nov  8 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-6m)
- [SECURITY] CVE-2008-2237 CVE-2008-2238 CVE-2008-3282
- apply cws-sjfixes07.diff and openoffice.org-2.4.1.ooo92217.sal.alloc.diff

* Mon Oct 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-5m)
- rebuild against db4-4.7.25

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.1-4m)
- change BuildRequires: gst-plugins-base-devel to gstreamer-plugins-base-devel

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.1-3m)
- update ooo-build-2.4.1.8

* Thu Jul 24 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.1-2m)
- use mozilla-plugin.pc

* Mon Jul  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.1-1m)
- update OOo-2.4.1
- update ooo-build-2.4.1.7

* Mon May 19 2008 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (2.4.0-11m)
- modify %%files langpack-cs_CZ
- BuildRequires: hunspell-cs

* Mon May 19 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.0-10m)
- update ooo-build-2.4.0.11
- BPR hunspell-*

* Tue May 13 2008 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (2.4.0-9m)
- update jsdp2007_2.4.0.1-Writer_patch.diff
- [Momonga-devel.ja:03621] [Momonga-devel.ja:03623]
- adjust %%files

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.0-8m)
- rebuild against qt3

* Sun May  4 2008 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (2.4.0-7m)
- fix jdk-home to enable build on x86_64

* Sun May 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.0-6m)
- integrated Calc part of the work by JSDP2007
- - added jsdp2007_2.4.0.1-Calc_patch.diff

* Sun May 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.0-5m)
- build with xulrunner instead of firefox

* Sat May 03 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.0-4m)
- enabled java using java-1.6.0-openjdk (configurable by specopt)
- updated and applied japanization patches from waooo (http://waooo.sf.jp/)
- - added Source201-206 (sdf-waooo-*.sdf)
- - added Patch200-Patch211 (patch-waooo-*.diff)
- integrated the work by Japanese Seagull Development Project 2007 (JSDP2007, http://jsdp2007.net/)
- - the work for Calc has not been inegrated yet (Writer only)
- - added Source250: jsdp2007_2.4.0.1-Writer_additional_files.tar.bz2
- - added Patch250: jsdp2007_2.4.0.1-Writer_patch.diff
- - added Patch251: cws-cjksp1-sw.disabled_for_jsdp2007.diff
- modified splash (2.4)

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-3m)
- rebuild against firefox-3

* Tue Apr 22 2008 Ichiro Nakai <ichiro@n.eamil.ne.jp>
- (2.4.0-2m)
- remove %%{instdir}/program/libstlport*.so from x86_64 for the moment
  (to fix build on x86_64)
- modify %%files langpack-es to avoid conflicting with langpack-ru

* Wed Apr 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.0-1m)
- update 2.4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.1-10m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-9m)
- rebuild against openldap-2.4.8

* Sat Feb 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-8m)
- build with kdelibs3

* Fri Feb 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-7m)
- support gcc-4.3

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-6m)
- %%NoSource -> NoSource

* Sun Jan 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-5m)
- fix build error

* Sun Jan 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-4m)
- update splash bmp

* Sun Jan  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-3m)
- update ooo-build-2.3.1.2

* Mon Dec 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-2m)
- revise %%files to correct included files of langpacks and core

* Fri Nov  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-1m)
- update ooo-build-2.3.1, oog680-m9

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.3.0-0.2m)
- rebuild against db4-4.6.21

* Mon Sep 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-0.1m)
- update ooo-build-2.3.0.1.2, OOG-680-m5(OpenOffice.org-2.3.0-RC3)

* Thu Aug 30 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-5m)
- add Requires: bash-completion for %%{_sysconfdir}/bash_completion.d

* Tue Aug  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-4m)
- add version on Requires for firefox

* Sun Jun 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1-3m)
- move "Obsoletes: openoffice.org-javafilter" from OOo to OOo-core

* Thu Jun 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-2m)
- Obsoletes: openoffice.org-javafilter

* Thu Jun 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-1m)
- update OpenOffice.org-2.2.1(oof680-m18)
- update ooo-build-2.2.1
- disable gcj-java

* Mon May 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-1m)
- update ooo-build-2.2.0

* Tue Apr 10 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.0-0.2m)
- modified splash (version "2.2")

* Sat Apr  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-0.1m)
- update oof680-m14(2.2.0-RC)

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-5m)
- openoffice.org-core Requires: freetype2 -> freetype

* Sat Feb 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-4m)
- update ooo-build-2.1.4

* Wed Jan 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-3m)
- revise %%files to avoid conflicting

* Wed Jan 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-2m)
- do not apply fontconfig-substitute-localizedname.diff for fontconfig-2.2.3
- fontconfig should be updated ASAP

* Tue Jan 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-1m)
- update 2.1.0

* Thu Dec 28 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-13m)
- merge from TSUPPA4RI-branch
- good-bye mozilla

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-12m)
- update ooo-build to version 2.0.4.11

* Wed Nov 22 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-12m)
- copy from momonga's trunk (r12841)
- use firefox-devel instead of mozilla-devel
- added --with-system-mozilla and --with-firefox

* Mon Nov 20 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.4-11m)
- update ooo-build to version 2.0.4.6
- - update patches_apply_Momonga.patch to modify patches/src680/apply to skip fontconfig-substitute-localizedname.diff. This causes an error in a build.

* Sun Nov 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-10m)
- rebuild against db4-4.5.20-1m

* Sat Nov 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.4-9m)
- changed %%{with_openclipart} to 0
- - Now, OOo gallery files for OpenClipArt are made by openclipart package (openoffice.org-clipart) and not in main OOo package.
-- This is for aboiding a potential mismatch between contents in openclipart and OOo gallery index files.

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.4-8m)
- rebuild against curl-7.16.0
- fix %%files

* Sun Nov  5 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-7m)
- update ooobuild-2.0.4.2

* Tue Oct 31 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.4-6m)
- enabled OpenClipArt Galleries (BuildRequires: openclipart-png)
- - enable/disable openclipart is configuable by specopt
- install %%{instadir}/share/samples/%{lang} for each langpack to avoid error in selecting menu "New -> Templates and Documents"
- update GSI_ja.sdf by a latest waooo patch
- - source is OOo_2.0.4_patches_curvirgo.zip at http://waooo.sourceforge.jp/
- - see also http://sourceforge.jp/projects/waooo/document/OpenOffice.org_Japanese_Localized_Version_for_Linux_OOo_2.0.4_LinuxIntel_install_ja_curvirgo_with_glibc-2.3.3_-_Notes/

* Tue Oct 31 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.4-5m)
- Build and include SDK
- - added "--enable-odk" to configure and applied ooo-build-2.0.4.1-install_odk.patch
- - Now, openoffice.org-devel and openoffice.org-devel-doc are made.
- Has CCACHE been really working recentry?
- - Plese dont set "CCACHE_DIR=${BUILDDIR}/.ccache" in bin/setup! "${BUILDDIR}/.ccache" is always deleted after a build. Without any settings, we should use a system ccache.
- - applied ooo-build-2.0.4.1-dont_set_nonsense_CCACHEDIR.patch
- Disabled Java for x86_64 because of broken Java SDK 1.5.0.x.
- - ( see http://bugs.gentoo.org/show_bug.cgi?id=151225 )
- use unowinreg.dll at OOo site. (NoSource:20)

* Wed Oct 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.4-4m)
- revise %%files to avoid conflicting

* Wed Oct 25 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.4-3m)
- enabled Java features using Java from Sun (only in %%{ix86} and x86_64)
- - BuildRequires: java-sun-j2se-sdk, db4-java
- - temporary workaround : imported unowinreg.dll from opensuse package OpenOffice_org-2.0.4-14.src.rpm
- - modified ooo-build-2.0.4.1-patches_apply_Momonga.patch (apply fix for db4-java)
- Fonts are antialiased by default (default-disable-embedded-bitmap.diff)
- - for other options, see manual openoffice(1)

* Tue Oct 24 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.4-2m)
- export QTDIR KDEDIR QTLIB

* Mon Oct 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-1m)
- update 2.0.4

* Wed Sep 27 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-0.1m)
- update 2.0.4-RC3

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3-3m)
- rebuils against dbus 0.92
- add patch301 for dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.3-2m)
- rebuild against expat-2.0.0-1m

* Sat Jul 15 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-1m)
- update OpenOffice.org-2.0.3 , ooo-build-2.0.3.0

* Tue Jul 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-0.2m)
- use "_numjobs"
- add Patch104: openoffice.org-ooc-m7-builddep.diff.
-- fix parallel build .

* Sun Jul  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-0.1m)
- update OpenOffice.org-2.0.3-RC7, ooo-build-ooc680-m7

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-5m)
- revised %%files for duplicated files and direcotries again

* Tue May 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.2-4m)
- revised %%files for duplicated files and direcotries

* Fri May 26 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.2-3m)
- applied vcl-fontconfig.patch in patches/src680/apply so that OOo use fontconfig settings for fontrendering.
- - this patch is already merged in OOC680 branch. So we do not need it when updating to 2.0.3)
- removed unnecessary procedure for desktop files (issue of "empty desktop file").
- revised a Japanese translation in desktop files
- add desctiption of some environment variables to manual (SAL_USE_VCLPLUGIN, OOO_INTERFACE_FONT_SIZE, OOO_DEFAULT_FONT_SIZE, OOO_SCREEN_DPI, SAL_EMBEDDED_BITMAP_PRIORITY, SAL_ANTIALIASED_TEXT_PRIORITY, SAL_AUTOHINTING_PRIORITY)

* Sun May 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-2m)
- update ooo-build-2.0.2.11

* Sat May 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-1m)
- update OpenOffice.org-2.0.2
- update ooo-build-2.0.2.9

* Thu Feb  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-6m)
- update ooo-build-2.0.1.3
- enable buildfix-gcc41-friend-decl-i18npool.diff

* Tue Jan 31 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-5m)
- add system-neon-0.25.diff

* Sat Jan 28 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-4m)
- update ooo-build-2.0.1.2

* Thu Jan 26 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-3m)
- add BuildRequires: 	pango-devel, bonobo-activation-devel
- use %%make

* Sun Jan  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-2m)
- BuildRequire: patch >= 2.5.9

* Sat Dec 31 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.1-1m)
- version 2.0.1 (a.k.a OOA680-m1)
- add more "Requires:" to openoffice.org-core explicitly, since OOo cannot start when some libraries are missing.
- add Momonga64.conf for x86_64 (but not tested :-P)
- merge Postacrd data (Add_Postacrd_Japan_and_BJIS_priority_up.sdf in OOo_2.0.1.rc4_patches_curvirgo.zip at http://waooo.sourceforge.jp/) in the build procedure
- omit several ooo-build patches
- - cws-i18nshrink.diff: OOo crashes when it try to display japanese fonts if this patch is applied.
- - cws-fakebold.diff: Menu font becomes bold...
- - buildfix-gcc41-friend-decl-i18npool.diff: cannot be applied when cws-i18nshrink.diff is not applied.

* Wed Dec 21 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.0-1m)
- version 2.0.0 (a.k.a OOO680-m3)
- - Packaging changed
- - - This follows that of Fedora Core, but kde subpackage are added and main package is made as a meta package.
- build using ooo-build 2.0.0.2
- - to know about ooo-build, see http://www.go-ooo.org/
- -  revised Momonga specific build configure file (Source15:Momonga.conf.in and Patch0:ooo-build-2.0.0.2-distroconf_Momonga.patch)
- - add more options (--without-antialias, --embedded-bitmap,--menufont-size, --dialogfont-size, --screendpi) to ooo-wrapper (and manual) (Patch2:ooo-build-2.0.0.2-add_more_option_to_ooo-wrapper_and_manual.patch). see manualpage of openoffice(1)
- - fix incorrect MIME key (missing "aplications/") in desktop/writer.desktop.in (Patch3:ooo-build-2.0.0.2-fixkey-mime.patch)
- add Momonga specific patch categories in patches/src680/apply (Patch1: ooo-build-2.0.0.2-patches_apply_Momonga.patch)
- - Momonga, MomongaBase, MomongaOnly, JA_waooo categories are added.
- - - [MomongaOnly]
- - - - prefer antialiased japanese font (Patch100:default-disable-embedded-bitmap.diff)
- - - - change default color of gen widget (Patch101: default-color-genwidget.diff)
- - - - use coustomize splash (Source16:openintro_momonga.bmp and Patch102: momonga-splash.diff)
- - - - fix default fontlist of japanese (Patch103:ja-VCL.xcu_font_fix_for_ja_ooobuild_2.0.0.2.diff)
- - - [Ja_waooo] import and modified following patches from OpenOffice.org Waooo
- - - to know about Waooo and these patches, see http://waooo.sourceforge.jp/wiki/index.php?Patches
- - - - add japanese postcard data (unworking now, neet more work) (Patch200:ja-waooo-Add_Postacrd_Japan_SRC680_m104s1.diff)
- - - - add label data (Patch201:ja-waooo-Labels.xcu_merge_data.diff)
- - - - fix color in helpfile (Patch202:ja-waooo-helpcontent2_color_fix_SRC680_m112.diff)
- - - - fix font in help (Patch203:ja-waooo-helpcontent2_fix_SRC680_m94.diff)
- - - - fix math help (Patch204:ja-waooo-helpcontent2_smath_fix.diff)
- - - - fix IZ#54320 (Patch205:ja-waooo-punctuation_and_adjust.diff)
- - - - fix mojibake in rtf import (Patch206:ja-waooo-rtf_cjk_fix_20.diff)
- - - - fix encoding in rtf export (Patch207:ja-waooo-rtf_filter_fix_20.diff)
- - - - fix IZ#18285, but this is not applied now since this patch seems to make menufonts ugly (Patch208:ja-waooo-vcl-virtualstyles-20050919.diff)
- - - - fix IZ#17498 (Patch209:ja-waooo-word6_filter_fix_20.diff)
- - - - cofigurable menufont size (Patch210:ja-waooo-vcl-setdefaultfontsize.diff)
- - - - configurable menufont size (Patch211:ja-waooo-vcl-setinterfacefontsize.diff)
- - - - configurable screen dpi (Patch212:ja-waooo-vcl-setscreendpi.diff)
- TODO
- - merge Postacrd data in the build procedure.
- - enable JAVA (SUN Java and GIJ4?)
- - template and galleries (OpenClipArt(http://www.openclipart.org) and SozaiOOo(http://openoffice.s16.xrea.com:8080/pukiwiki/pukiwiki.php?%5B%5BSozai_ooo%5D%5D))
- - useful macros (OOoLatex (http://ooolatex.sourceforge.net/) etc.)
- - enable cairo, evolution2?

* Mon Sep 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Wed Apr 20 2005 mutecat <mutecat@momonga-linux.org>
- (1.1.4-2m)
- fix ppc i18n

* Sat Apr 16 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.4-1m)
- update to OpenOffice.org version 1.1.4
- [SECURITY] fixed DOC document Heap Overflow, CAN-2005-0941

* Mon Apr  5 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.2-6m)
- update SOURCE9(ooo-desktops.tgz)
- added mimetype declaration to desktop files.

* Sat Feb 26 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.2-5m)
- added write permission in order to strip executables.

* Mon Feb 14 2005 mutecat <mutecat@momonga-linux.org>
- (1.1.2-4m)
- arrange ppc i18n

* Sun Nov 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-3m)
- change BuildRequires: xorg-x11-Xvfb

* Wed Sep  8 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.1.2-2m)
- use gcc3.2 since gcc-3.4 fails to build

* Wed Jun 30 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.2-1m)
- update to OpenOffice.org version 1.1.2
- ooo-build-1.1.60 and OOO_1_1_2 source from ooo.ximian.com
- add %%patch11 to ensure to link fontconfig building psprint
- add %%patch12 for correct version number 1.1.2 in configures and wrappers

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.1-3m)
- rebuild against db4.2

* Mon Jun 07 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-2m)
- fix %%install so that language packs contain true corresponding contents (BugID 42: Thanks to Mr. Ichiro Nakai)

* Mon Jun 07 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-1m)
- update to OpenOffice.Org version 1.1.1
- switch to ooo-build (version 1.1.59) from Ximian (see http://ooo.ximian.com/)
- - rewrite specfile completely based on FC2 package (openoffice.org-1.1.1-4.src.rpm)
- - add Vendor "Momonga" and corrensponding procedures (%%Patch1)
- - use Ximian icon set
- - user installation dir is changed to ~/.xopenoffice1.1
- - ommit cups support patch (psprint-cups.diff seems to make kinput2 unusable)
- - not adopt gnome-integration patches (it intdroduce many annoying dependencies)
- - revise to use ~/.xopenoffice1.1/ooo.kdeglobals when WM is KDE
- - - to enable to fetch some settings independent of ~/.kde/share/kdeglobals
- - - it can be symlnk to ~/.kde/share/kdeglobals and that is default
- import some patches (%%Source10000) which is used by Japanese N-L team(http://ja.openoffice.org/)
- - modified for ooo-build
- build japanese helpcontent (%%Patch2), (help is now english and japanese only)
- import "ER for Linux" (http://openoffice-docj.sf.jp/) and revise ooo-wrapper (%%Patch3)
- set _numjobs to 1 since there is multi-process build problems
- Notice: java enabled build (with_java 1) is not tested
- TODO (HELP!!)
- - fix building SDK (failed in make checkflags); It needs JAVA??
- - build Scop (http://www.3bit.co.jp/scop/); this needs SDK
- - Font Antialiasing for Kochi (Why embeded bitmaps are used???)
- - cups support and kinput2 (What is wrong with psprint-cups.diff??)

* Sat May 22 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-0m)
- test builds for the import ximian.ooo to Momonga Linux
- Old changelogs of FC2 openoffice.org-1.1.1-4.src.rpm are as follows
- -
- * Wed Apr 21 2004 Dan Williams <dcbw@redhat.com> 1.1.1-4
- - First cut of Bluecurve Icons
- - Use gnome-open for helper application (launching URLs and mailto:)
-      (#121291)
- - Use CVS snapshot of ooo-build (2004-04-21, 1.1.53pre)
- 
- * Fri Apr 16 2004 Dan Williams <dcbw@redhat.com> 1.1.1-3
- - Fix silly specfile issue preventing PPC builds (Dave Woodhouse, #121182)
- - Add a slew of dictionaries, hyphenators, and thesauruses (#108720)
- 
- * Tue Apr 06 2004 Dan Williams <dcbw@redhat.com> 1.1.1-2
- - Fix annoying dialogs on execution of Autopilots
- - Bring Tools->Word Count item back (patch was being misapplied)
- 
- * Wed Mar 31 2004 Dan Williams <dcbw@redhat.com> 1.1.1-1
- - Bump version to 1.1.1
- - Fedora doesn't depend on Mozilla either
- 
- * Thu Mar 18 2004 Dan Williams <dcbw@redhat.com> 1.1.0-32
- - Removed hard coded dependancy on "XFree86", and removed the unnecessary
-   BuildRequires: XFree86-libs as the XFree86-devel dep is all that is needed
-   because it will ensure the libs are installed itself.  These changes are
-   required in order to make the distribution X11 implementation agnostic so
-   we can switch to xorg-x11.  (#118441)  <mharris>
- - s/Requires: fontconfig/Requires(post,postun): fontconfig/  <mharris>
- - Really really make splash & about screens work
- 
- * Mon Mar 15 2004 Dan Williams <dcbw@redhat.com> 1.1.0-31
- - Really make spalshscreens work this time
- 
- * Fri Mar 12 2004 Dan Williams <dcbw@redhat.com> 1.1.0-30
- - Add Red Hat splash & about screens
- - Add gnome-vfs2-devel to BuildRequires, remove duplicate
-     libgnomecups-devel BuildRequires
- - Fix RH #117440, sanity check TTF 'name' table strings before
-     using them
- 
- * Tue Mar  2 2004 Dan Williams <dcbw@redhat.com> 1.1.0-29
- - Update to ooo-build 1.1.51 and ooo-icons-OO_1_1-8
- - Activate multi-process builds on > 2-way machines
- 
- * Thu Feb 19 2004 Dan Williams <dcbw@redhat.com> 1.1.0-28
- - Flip symbol generation to do full symbols, enabling
-     much more useful debuginfo RPMs
- 
- * Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- - rebuilt
- 
- * Mon Feb  9 2004 Dan Williams <dcbw@redhat.com> 1.1.0-27
- - Don't install spadmin .desktop file since spadmin isn't shipped
- - Remove some python cruft
- - Don't use blocking lockf() when locking ~/.recently-used
- 
- * Wed Feb  4 2004 Dan Williams <dcbw@redhat.com> 1.1.0-26
- - Add libgnomecups to BuildRequires
- 
- * Mon Feb  2 2004 Dan Williams <dcbw@redhat.com> 1.1.0-25
- - Fix up font locations and use correct opensymbol font
- 
- * Wed Jan 28 2004 Dan Williams <dcbw@redhat.com> 1.1.0-24
- - Fix UI font for non Western locales (RH #114357)
- 
- * Thu Jan 22 2004 Dan Williams <dcbw@redhat.com> 1.1.0-23
- - Fix menu highlight/selected colors again (RH #113991)
- - Enable all languages
- 
- * Wed Jan 21 2004 Dan Williams <dcbw@redhat.com> 1.1.0-22
- - Fix menu highlight/selected colors (RH #113991)
- 
- * Tue Jan 20 2004 Dan Williams <dcbw@redhat.com> 1.1.0-21
- - Correct installed location in wrapper script
- 
- * Mon Jan 19 2004 Dan Williams <dcbw@redhat.com> 1.1.0-20
- - Switch to gnome.org common infrastructure OpenOffice.org builds
- 
- * Thu Dec  11 2003 Dan Williams <dcbw@redhat.com> 1.1.0-10
- - Use configimport.bin to replace nasty 'sed' stuff in wrapper script
- - Switch back to soffice1.bin
- - Fix "perpetual re-install" problem with wrapper script (due to empty
-     ~/.sversionrc file)
- 
- * Sat Dec 06 2003 Dan Williams <dcbw@redhat.com> 1.1.0-9
- - Fix building on single processor systems.  Ooops.
- 
- * Wed Nov  26 2003 Dan Williams <dcbw@redhat.com> 1.1.0-8
- - Disable building of Mozilla AB integration on Shrike since the system
-     mozilla was built with gcc 2.96 and we build with gcc 3.2
- - Add Java-enable switches to allow building a Java-enabled version
- - Add libart_lgpl-devel to BuildRequires, allow versions lower than 2.3.13
- - Make splash screen not annoying
- - Switch to more prelink-optimized soffice2.bin
- 
- * Wed Nov  26 2003 Dan Williams <dcbw@redhat.com> 1.1.0-7
- - 1.1.0-7 was internal-only test build getting RHEL3 and RH9 support
-     working
- 
- * Mon Nov  3 2003 Dan Williams <dcbw@redhat.com> 1.1.0-6
- - Tweak default fonts some more (#108812)
- 
- * Fri Oct 31 2003 Dan Williams <dcbw@redhat.com> 1.1.0-5
- - Update default fonts, change Central/Eastern Europe to Nimbus from Luxi
- - Remove fonts not shipped with the OS from the Font menu
- 
- * Tue Oct 28 2003 Dan Williams <dcbw@redhat.com> 1.1.0-4
- - Make OpenOffice.org more prelink-friendly, ie. when prelinked
-   it ought to start up faster (Jakub Jelinek)
- - Bypass soffice script, handle the necessary things already
-   in ooffice script (Jakub Jelinek)
- - Make getstyle-gnome and msgbox-gnome binaries prelinkable (Jakub Jelinek)
- - Speed up the build by not packing all binaries and libraries
-   18 times (and also require way less diskspace for the build) (Jakub Jelinek)
- - Fix .desktop file stuff
- - Fix slightly broken 1.0.2 upgrade process
- - Enable parallel building for > 1 processor machines
- 
- * Fri Oct  17 2003 Dan Williams <dcbw@redhat.com> 1.1.0-3
- - Adjust UI font for different languages
- - Remove oosetup
- - Allow for RH 9 building (not quite working yet)
- - Fix .desktop files location in menus
- - Add libart_lgpl to Requirements, remove Startup Notification for RH9
- - Massively clean up ooffice launcher script, upgrades cleaner
- - Remove Mozilla zipfiles from OOo sources to reduce bloat
- - Use system BerekelyDB rather than builtin one (requires a
-     berkeleydb built _without_ -nostdlib, ie >= 4.1.25-11
-     or most v 4.0)
- - Remove "registration" dialog on startup
- - Patch in font antialias size rather than set on launch
- - Add regcomp tool to final package
- - Don't build ICU libs as DT_TEXTREL
- - Require GCC 3.2.2 now, rather than 3.3
- - Switch on symbols and make debuginfo RPMs work
- 
- * Tue Oct  7 2003 Jeremy Katz <katzj@redhat.com> 1.1.0-2
- - add obsoletes to subpackages
- 
- * Mon Oct 03 2003 Dan Williams <dcbw@redhat.com> 1.1.0-1
- - Initial Release of 1.1.0 rpms

* Thu May 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.0-7m)
- apply neon-fix-CVE-CAN-2004-0389.patch

* Wed Apr 21 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.0-6m)
- export DISPLAY=:$XDISPLAY

* Wed Apr  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.0-5m)
- use Xvfb for build

* Wed Mar 24 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.0-4m)
- revised spec for rpm 4.2.

* Wed Oct 29 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.0-3m)
- not launch a virtual framebuffer X server (use DISPLAY.PLEASE)
- revise %%files (not make %%files sections dinamically)
- use %%patch11(openoffice-1.1-disable-python.patch) not to install
  python-2.2.2 stuff and modified %%patch212 to avoid rejection
- --enable-libart when --without-gpc

* Mon Oct 27 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.1.0-2m)
- append symlink
  (oofax, oolabel, ooletter, oomaster, oomemo,
   ooprinteradmin, oovcard, ooweb)
- modify openoffice-wrapper.pl
  (for new symlinks)
- append some desktop files in %%files.
  (for ooprinteradmin ooweb)

* Fri Oct 24 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.0-1m)
- first import to Momonga
- adopted a prototype package OpenOffice.org-1.1.0-4m [devel.ja:02206] and
  renamed it to openoffice.org like RedHat package (revise %%{SOURCE13})
- the prototype package was based on OpenOffice.org-1.1.0-0.rc4.2mdk
  of Mandrake Cooker
- a changelog of the prototype package were as follows
  -
  - * Thu Oct 23 2003 Masayuki SANO <nosanosa@momonga-linux.org>
  - - (1.1.0-4m)
  - - disabled gpc by default (%%define enable_gpc 0)
  -   - gpc is released under non-free license.
  -     They said "This software is free for non-commercial use."(see gpc231/gpc.c)
  -   - To build enabling gpc, change the value of enable_gpc to 1 in this spec
  -     file or /etc/rpm/specopt/OpenOffice.org.specopt
  -   - remove dependencies on perl-MDK-Common
  -   - %%{SOURCE5},%%{SOURCE6},%%{SOURCE7},%%{SOURCE14},%%{SOURCE16} and
  -     this spec file are revised
  -   - sub routines used by scripts are copied from files in perl-MDK-Common
  -     and inserted in each files
  -  -  add BuildRequires: mozilla, mozilla-devel (really needed?)
  -  -  not make Russian, Finnish, Czeck, Slovak localized help files now, because
  -     the sources for these are not available on ftp sites and including these
  -     files makes nosrc.rpm very big...
  -
  - * Wed Oct 15 2003 Masayuki SANO <nosanosa@momonga-linux.org>
  - - (1.1.0-3m)
  - - disabled java by default (%%define enable_java 0)
  -   - To build with java, change the value of enable_java to 1 in this spec
  -     file or /etc/rpm/specopt/OpenOffice.org.specopt and Install
  -     java-sun-j2se-sdk
  -   - Import debian patches (%%patch201-219) from rawhide
  -     (openoffice.org-1.1.0-2.src.rpm)
  -   - modified %%patch209 to avoid conflicting with %%patch26
  -   - not need %%patch201 because we use systemdb by %%patch400
  -   - add prefix to debian patches
  - - rename %%patch601 to openoffice-1.1-fixes-odk.patch
  - - bunzip all patches from Mandrake
  -
  - * Fri Oct 10 2003 Masayuki SANO <nosanosa@momonga-linux.org>
  - - (1.1.0-2m)
  - - add Requires: perl-MDK-Common
  -
  - * Thu Oct  9 2003 Masayuki SANO <nosanosa@momonga-linux.org>
  - - (1.1.0-1m)
  - - second test build for Momonga
  - - sync with OpenOffice.org-1.1-0.rc4.2mdk at Mandrake Cooker
  - - revise %%patch37 for 1.1.0 source
  - - not patching %%patch11
  - - build fails with fakejdk of original spec file, so use java-sun-j2se-sdk now..- notice that we need make symbolic link before build, otherwise build fails
  -   - # ln -s /usr/share/java/db.jar /usr/share/java/db-4.0.jar
  -   - maybe we shold revise db4-java...
  - - not install mandrake specific menus.
  -
  - * Sun Oct  5 2003 Masayuki SANO <nosanosa@momonga-linux.org>
  - - (1.1-0.rc3.1m)
  - - first test build for Momonga
  - - modified OpenOffice.org-1.1-0.rc3.2mdk.src.rpm from Mandrake Cooker
  - - add %%patch601

* Mon Sep 15 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.1-0.rc4.2mdk
- Really enable fixed Patch102 (bmp32)
- Patch40: Make font substitution algorithm match documentation (IZ #19591)
- Updates to wrapper script:
  - Really use localized instdb.ins files for installation
  - Reactivate font substitutions for now otherwise Japanese & Korean
    help content is broken. This concerns (Andale Sans UI, Albany, Arial)

* Fri Sep 12 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.1-0.rc4.1mdk
- 1.1rc4
- Nuke mutation of instdb.ins since it confuses updates and is now useless
- Refine selection of MySpell packages to install along with -l10n- packages
- Add Slovak help files, update Czech files to 1.1rc4 from OOo-cs project
- Make extra psprint filters work again
- Make sure we only handle 32-bit bmp images and don't default to that
  format. Otherwise, we need to always copy/transform things around
  and this slows UI down terribly
- Make doc defaults configurable through DOC_DEFAULTS variable in
  /etc/openoffice/openoffice.conf or ~/.oofficerc. i.e. set it to "MS"
  (default) for .doc formats or any other (e.g. "NATIVE") for OOo
- Revert to use Nimbus Roman No9L as default font for text since Vera
  Serif doesn't have any oblique face yet
- Add more "Word count" translations (thanks Pablo, Giuseppe)
- Patch11: Don't install python-2.2 stuff for now
- Patch31: Update to make $HOME/Documents default only if it exists
- Merge in more Ximian patches:
  - Patch101: Conditionalize use of MS defaults (default)
  - Patch106: Startup notification fixes (IZ #18970)
- Updates to wrapper script:
  - Try hard to update user install base even through different language
  - Always default to one installed language set, preferably matching
    the current locale

* Mon Sep  1 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.1-0.rc3.2mdk
- Merge in some Ximian patches:
  - Patch100: Add word count feature under Tools/Hyphenation item
  - Patch101: Default to .doc for text documents
  - Patch102: Add support for 32-bit bitmaps and icons
  - Patch103: Correct typo in Options->HTML compatibility for sv (IZ #16437)
  - Patch104: Export 'no color' background to doc (IZ #18671)
  - Patch105: Don't continually beep annoyingly (IZ #18440)

* Fri Aug 22 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.1-0.rc3.1mdk
- 1.1rc3
- Default compiler is now any gcc >= 3.2 (thusly MDK >= 9.0)
- Use built-in Mozilla address book libraries
- Add localized UI strings for: Portugul do Brasil
- Temporarily remove localized help files for: Spanish, Finnish, Czech
- Try to fix encodings of localized menu File/New entries
- Don't install (unlocalized) readme & license files to user dir
- Patch11: Don't build crashdump reporter
- Patch30: Fix broken inclusions of STL headers
- Patch31: Make $HOME/Documents the work directory
- Patch32: Look for additional templates in <ooo>share/templates/extra/
- Patch33: Don't export crazy symbols from svunzip (IZ #17841)
- Patch34: Remove unneeded sched_yield calls (IZ #9277)
- Patch35: MySpell dictionaries are shared in /usr/share/dict/ooo
- Patch36: Enable automatic spellchecking by default
- Patch37: Use better default fonts available in the distribution
- Patch38: Fix crash in ExtendedFontStruct::HasUnicodeChar()
- Patch39: Disable KDE screensaver during presentations
- Patch405: Use system Vera fonts
- Deprecate UI_FONT & FONT_SCALING in openoffice.conf and don't handle
  them any longer (should be handled automatically with new font
  substitution facilities)
- Updates to wrapper script:
  - Handle CTL layouts (Arabic)
  - SAL_FONTPATH_USER is gone, SAL_FONTPATH should still work though
  - Only merge in new configuration data if we are switching locales
  - Attempt to update an existing installation and let OOo merge user
    *.xml files to new *xcu
  - Drop XML::Twig parser as configuration files format is now
    overkill to handle. Better user configimport tool to merge in new
    config bits

* Mon May 19 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.1-0.beta2.1mdk
- 1.1 beta 2

* Tue Apr 29 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.3-2mdk
- Patch0: Fix vcl printer code that may have caused a crash (1.0.3.1)
- Patch29: Update to add a generic PostScript distillable output (Giuseppe)
- Patch50: Reintroduce soffice.sh fix, it got mis-integrated upstream
- Patch53: Install OOo Impress templates
- Introduce UI_LANG variable to /etc/openoffice/openoffice.conf or
  ~/.oofficerc in order to force a specific language for the user or
  system wide. This helps if you have a mismatch between the system
  locale (which the ooffice script uses) and what you actually have
  installed as OOo-l10n-* package

* Wed Apr  9 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.3-1mdk
- BuildRequires: xpm-devel for nas library build
- Rework Patch400 (system-db) to fit libdb4.0 and lib64 on MDK >= 9.2
- Use zh_{CN,TW} instead of zh-{CN,TW} in package names so that
  rpmsrate can auto-expands the filelist
- Patch599: Colateral, induced by previous 64-bit patches on 32-bit arches
- Patch49: Enable user to set priorities among embedded bitmaps,
  auto-hinting and anti-aliasing. Backport from OOo 1.1. Variables are
  SAL_EMBEDDED_BITMAP_PRIORITY, SAL_ANTIALIASED_TEXT_PRIORITY,
  SAL_AUTOHINTING_PRIORITY to be set with numerical values. Defaults
  are 2/1/1, respectively. i.e. preferring embedded bitmaps first.
  <http://gsl.openoffice.org/servlets/ReadMsg?msgId=490464&listName=features>

* Mon Mar 10 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.2-7mdk
- Patch1: Ignore Xerrors from XSetInputFocus() for now (IZ #10779)
- Patch25: Update to handle Kerkis as default font for Greek. Also add
  better/available candidates for Asian locales
- Patch44: Update to get correct condition for PPDKey::eraseValue()
- Patch45: Update to immediately cache the PPD file to user's psprint
- Patch46: Handle SAL_DEBUGGER in soffice script
- Patch47: Make soffice script handle chdir to dirs denoted relatively
- Patch48: Always enable antialiasing if the font is antialias capable
- Patch109: Update to really enable Slovak localization
- Patch405: Update to match current CVS sources
- Fix localized help/err.html mutation, only special case for Finnish
- Updates to wrapper script:
  - Handle CJK locales configuration better
  - Handle new --debug(ger) and --help options
  - Handle language -> font mappings better
  - Use KerkisSans as UI font for Greek
  - Also substitute Albany and Arial UI fonts, if not available
  - Make LC_CTYPE an alternative to set UI localization, not LC_MESSAGES
  - On user installation, make sure that ~/.mime.types doesn't contain
    any duplicates. i.e. don't let it grow constantly

* Tue Mar  4 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.2-6mdk
- Remove ~/.openoffice/user/work symlink if target is user's HOME dir
- Requires: OpenOffice.org %{version} for -help and -l10n packages
- Patch44: Keep ordering of original PPD file values in Printer Properties
- Patch45: Use CUPS to grab the corresponding PPD file

* Mon Mar  3 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.2-5mdk
- Patch43: Use light hinting for freetype2 >= 2.1.3-3mdk (RH)
- Patch0: Take care of selected directory in Save dialogs (IZ #8928)
- Main package now owns help/en/err.html file to suggest a help
  package to install if it is not localized yet
- Default font scaling to 100 if FONT_SCALING is not set correctly

* Fri Feb 28 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.2-4mdk
- Force OOo Draw styles in English for main package
- Patch42: Use MDK colors
- Patch41: Fix crash with a PPD file that does not specify the DPI
  resolution in the string version of the "Resolution" option. (IZ #11616)
- Updates to Source5 (wrapper-script):
  - Handle Arabic UI fonts and language
  - For language selection, prefer LC_ALL which has higher priority
  - If FONT_SCALING variable is not set, don't change user's config either

* Fri Feb 21 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.2-3mdk
- Add localized UI strings for: Arab, Slovak
- Add localized help files for: Russian, Finnish, Czech, Japanese,
  Korean, Chinese (simplified and traditional)
- Patch39: Hack up scpzip so that it doesn't duplicate everything 17
  or more times than necessary (RH). Also make sure to handle
  localized help files.

* Wed Jan 29 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.2-2mdk
- Remove (obsolete) specfile nonsense
- Handle/skip -g in fake javac wrapper
- Patch37: Don't use obsolete CLK_TCK macros in debug=true mode
- Patch38: Fix rsc code in debug=true mode
- Updates to Source5 (wrapper-script):
  - Don't nuke away other OpenOffice.org installations info from sversionrc

* Tue Jan 28 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.2-1mdk
- Update to 1.0.2 final
- Patch16 (no-stlport): Remove bits merged upstream
- Add yet another fix to errno (Patch32)
- Patch406: Use system zlib + master truanderie
- Patch407: Directly link against system Xrender library
- Patch408: Fix conflict with system zlib brought in by recent
  freetype2 packages

* Wed Jan 15 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.2-0.1mdk
- Update to OOO_STABLE_1 2002/01/13
- Mostly get rid of tcsh use during main build
- Stop symlink'ing <ooo_home>/user/work, since user Common.xml is now
  correctly generated with user Work dir being his home

* Thu Jan  9 2003 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-10mdk
- Let extra templates be found by OOo in $(insturl)/share/template/extra
- Enforce dependency on perl-XML-Twig with a version known to work
- Add Requires: libsane1, Xaw3d
- Really make it ExclusiveArch: %{ix86} ppc
- Patch34: Enable dmake clean (Debian patch)
- Patch35: Enable RTTI & other optimizations on PPC (CVS, 1.0.2)
- Patch36: Fix bridges on PPC with gcc3 (CVS, 1.0.2)
- Patch37: Add Finnish localizatin, use "35" as prefix code
- Patch38: Fix references to errno
- Patch51: Fix build with recent Mozilla LDAP API changes
- Patch404: Correctly load system libsane
- Patch405: Fix build with newer FreeType 2.1.X libraries (Red Hat)
- Source21: Fix OpenSymbol font encoding table (RH #69996)

* Wed Sep 11 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-9mdk
- Patch33: Stop forwarding to stderr things we don't actually care of
  while testing for spooler availability. No longer Requires: lpddaemon
- Patch32: Add PDF converters both suitable for either view or press
  purposes as printer option to global psprint.conf (Giuseppe)
- Update wrapper script to reflect that change (auto page size selection)
- Make it run on "msec level 4" systems:
  - Patch30: Get command line arguments the regular way and not from
    /proc/<pid>/cmdline
  - Patch31: Stop trying to read directories up to the root to
    determine the full path of a file. Instead, use a derivative from
    realpath()

* Fri Sep  6 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-8mdk
- Fix calling of create-instdb script for future releases
- Requires the right set of font packages in -l10n- packages
- Make oo* wrapper scripts actually a link to real ooffice wrapper
- Updates to Source5 (wrapper-script):
  - Handle switch to zh-{CN,TW}
  - Handle more and correct language => UI font mappings
  - No longer expand fontpath from chkfontpath since OOo does exactly
    the same since, well, probably OOo 1.0.0.
  - Workaround MDK::Common::update_gnomekderc bug where a new
    PPD_PageSize entry was not added on a new line, if there was no
    previous user psprint.conf available and the last section didn't
    terminate with a newline character
  - Handle oo<component> --lang <lang> to actually start <component>
    in selected <lang>

* Tue Sep  3 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-7mdk
- Don't provide nor require any libraries from OOo at all.
- Patch29: Make setup look for a "localized" instdb.ins script
  specified by a new -LANG: option. Hackaround until I find a way to
  do that in scp or autoresponse files.
- Source20: Add script to generate localized instdb.ins files from
  setup.ins. Aka let the right files to be installed for a user
  installation in his language.
- Updates to Source5 (wrapper-script):
  - Add more mappings to Language -> Country table
  - Add --lang option to override default from locale
  - Use "localized" instdb.ins file as mentioned above, for a user
    installation

* Mon Sep  2 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-6mdk
- Add support for Mozilla addressbook and LDAP sources
- Fix permissions on thesaurus files
- OpenOffice.org-help-* packages now depends on locale-* as an DrakX hack
- Don't forcibly Requires: OpenOffice.org-l10n-en as it works with the
  right language package you want alone.
- Don't forcibly Requires: OpenOffice.org-l10n-* in -help- packages as
  Help can work without. e.g. Having English help with only Russian UI
  localization installed.
- Make transmute-help-errfile.pl hint about the help package to
  install in <b> instead of ugly <tt> with some fonts.
- Merge with Red Hat releases:
  - Start Xvfb in %%{init_xdisplay}
  - Set default document fonts to Nimbus Roman No9 L, Luxi Sans and
    Luxi Mono instead of the non existant Thorndale
  - Make sure New/Wizard menus and OfficeObjects are localized

* Wed Aug 28 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-5mdk
- Patch19: Make WM_CLASS property for ICCCM compliant, aka don't have
  "VCLSalFrame" showing up in taskbar of KDE 3.0 (IZ #4830, CVS HEAD)
- Add Catalan localization:
  - Source17: Catalan strings (Jesus Corrius, Jordi Mas)
  - Patch25: Add Catalan localization as POSTAPP2TARGET
  - Patch26: Various fixes/additions for Catalan support
  - Patch27: Catalan is only implemented as ca-ES (Jesus Corrius)
- Source18: Add global ooffice configation file
- Updates to Source5 (wrapper-script):
  - Define user work directory to $HOME
  - Use XML::Twig to parse and generate XML code. That's cleaner, more
    flexible but surely not the fastest way
  - Handle global ooffice config in /etc/openoffice/openoffice.conf
    and users' in ~/.oofficerc
  - Automatically define font to use for user interface based on the
    current locale. Overridden with UI_FONT config variable
  - Possibly set UI font scaling based on current display resolution
    (Jakub Jelinek, Red Hat). However, as this is not enabled by
    default, you need to properly set FONT_SCALING first
  - Define paper size to use based on the current locale, and provided
    you don't have any printer configured yet. Otherwise, printerdrake
    will still generate the right default values.

* Tue Aug 27 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-4mdk
- Fix BuildRequires: for xmlparse (Ian C. Sison)
- Patch17: Add POSTAPP<N>TARGET to be executed after APP targets are built
- Patch18: Add Czech localization as POSTAPP1TARGET
- Updates to Source5 (wrapper-script):
  - Correctly set default language for documents. Aka, transmute user
    Linguistic.xml, or create it if it doesn't exist yet
- Merge with Red Hat releases (5 new patches):
  - Patch{20,21,24,26}: Add Czech localization
  - Patch22: Fix language list for some languages
  - Patch{40,41,42}: Add some patches from <http://oo-cs.sourceforge.net/>

* Mon Aug 26 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-3mdk
- Move localized help files to new OpenOffice.org-help-* subpackages
- Add localized help files for Spanish, Italian, Swedish, German
- Make help/<lang> a real directory, possibly empty
- Patch16: Check that we have the necessary help files. The rationale
  is if help/<lang>/err.html exists then, we have localized help data,
  possibly not installed. Otherwise, we won't have localized help and
  we try to fallback to English

* Fri Aug 23 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-2mdk
- Add C/J/K UI localization. Likewise for Danish, Greek, Turkish
- bzip2 helpcontent even if bigger than tgz. We don't want binaries in CVS
- Don't handle upgrade, from 1.0-Xmdk with links from help/$ISOCODE to
  help/en, to 1.0.1-Ymdk where we sometimes have a real directory for
  help/$ISOCODE. Language packages PreReq: OpenOffice.org = %%{version}
  UPDATE: That still doesn't work. As a workaround, you can update
  with l10n-en first. Then, install proper l10n-*. That way works.
- Updates to Source13 (gen-langpack):
  - Fix %%description, don't redefine variables correctly set
  - Map zh-{CN|TW} to zh. Aka. correctly Requires: the right locales-* package
- Updates to Source5 (wrapper-script):
  - Fix silly typo introduced post-tests in versions file
    regeneration. If you installed 1.0.1-1mdk, you'd have better luck
    to rm -rf ~/.openoffice prior to rerunning "ooffice".
  - Add current version tag if it does not exist already. This occurs
    when you upgrade from 1.0 to 1.0.1 for example.
  - If the user installation exists, then the versions file is likely
    to exist too. Otherwise, if we (or the user) nuked it away, then
    simply regenerate it.

* Thu Aug 22 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0.1-1mdk
- OpenOffice.org 1.0.1
- Requires: libunixODBC2
- Requires: virtual package myspell-dictionary
- Add fully versioned Requires: libgcc
- BuildRequires: gcc3.2 >= 3.2-0.3mdk
- Build with STLport 4.5.3 everywhere
- New "libs" subpackage with OpenOffice.org libraries
- Really make dictionaries stuff go to %{_datadir}/dict/ooo/
- Add localized helpcontent for French
- Update Source7 (dpack-lang) to extract help files in some cases
- Merge Source6 (xlate-lang) with Debian 1.0.1-5
- Automatically regenerate Source6 (xlate-lang) data, add resource names
- Remove support for inexistant Finnish localisation
- Rewrite Source5 (wrapper-script) in full perl. Hopefully, it's now
  more flexible and readable. Also add or fixes the following:
  - Fix paths returned by chkconfig
  - Remove older dictionary symlinks that are now global
  - Don't remove user dictionaries however
  - Add ability to change UI localisation dynamically (merge ideas
    from both Debian and Red Hat people). You have to make sure the
    correct OpenOffice.org-l10-* package is installed however
- Source1[01]: Add additional templates and gallery files from upstream
- Source13: Automatically generate language subpackages
- Patch12: Add parallel build.pl from CVS HEAD (tune later)
- Patch14: Fix set_new_handler() exception specifications, if necessary
- Patch100: Add support for gcc3.2 to UNO
- Patch101: Any gcc >= 3.1.1 has versioned libstdc++ headers location
- Patch303: Add support for any gcc >= 3.1.1 includes dir in STLport
- Patch304: Add EXTRA_CXXFLAGS for RPM_OPT_FLAGS, some fixlets for STLport
- Patch400: Use system db3 library, but only on MDK 9.0 builds
- Patch403: Load libodbc library, not linker name library (from -devel package)
- Make sure we have a correct tcsh when building on MDK 9.0
- Nuke dependency on Sun JDK, use gcj instead (Jakub Jelinek)
- Use perl regexp for postprocess in xmlparse replacement (sed loops otherwise)
- Merge with Red Hat releases (12 new patches):
  - Don't uglify HTML for iso 8859-2
  - Don't catch SIGSEGV and SIGILL
  - Don't die if we cannot write registry cache
  - Don't build internal STLport when using gcc3
  - Make sure we don't do -I/usr/include, it's evil
  - Add support for compiler variables to configure script
  - Add support for new compiler variables to makefiles (COMPCC, COMPCXX)
  - Add debug for setup so that the /tmp directory is not removed
  - Fix broken inline assembly
  - Fix broken makefiles
  - Nuke STLport badlink
  - Use system getopt()

* Wed May 29 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0-5mdk
- Add Russian localization
- Build with gcc3.1 for all supported arches in MDK 9.0
- Remove Patch3, Patch4, Patch13, Patch19, Patch22
- Merge Patch22 (gcc31-misc) and Patch13 (gcc3-linker) in Patch5 (mdkconfig)
- Patch3: Add support for gcc3 on PPC
- Patch4: Workaround gcc3.1 bugs on PPC
- Patch19: Don't copy nor install at setup time standard libraries
- Patch22: Improve font server discovery (OOo 1.0-branch)
- Patch23: Updates to lingucomponent from OOo 1.0-branch:
  - Register own hypenation dictionaries (IZ #45555, IZ #4687)
  - Fix segfault under Thesaurus (IZ #4435)
  - Enable shared spellchecking capabilities
- Update Source4 (local autoresponse) to disable Java detection
- Update Source5 (wrapper script):
  - Better detection of setup failure for user installation
  - Remove any reference to en_US dictionaries for now since OOo
    provides shared ones that work
  - Better handling of broken links in the ~/.openoffice/user/worbook/
    directory. Also possibly unregister the previous dictionary

* Wed May 22 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0-4mdk
- Patch22: Misc patches for gcc3.1 (experimental)
- Rebuild with gcc3.1-1mdk in Cooker

* Wed May 15 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0-3mdk
- Default to gcc3.0 in Cooker/x86 for now
- Only build STLport 4.5.3 with gcc-3.0+ builds
- BuildRequires: blackdown-j2sdk = 1.3.1 on PPC (Stew)
- Patch20: cpputools regcomp workaround on PPC (Stew, IZ #3980)
- Patch21: Move OOo Math into the correct GNOME integration module
- Use official OpenOffice.org 1.0 menu icons
- Update and clean %%description to notify language packs available
- Update KDE and GNOME integration
- Update OOo Math document association
- Update Source4 (local autoresponse file) to detect Java Runtime
- Disable desktop (KDE, GNOME, CDE) integration at user's level

* Tue May  7 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0-2mdk
- Build Mozilla Address book on X86. Later for PPC
- BuildRequires: freetype2-devel
- Patch17: Minor fontcache fix (copy weight attribute) from CVS
- Patch18: Build with system FreeType 2 library
- Patch19: Don't install standard libraries (libgcc_s, libstdc++)
- Update Patch8 (no-mozab) to not install mozab and runtime from setup
- Update specfile to remove hackage now done by the above two patches
- Update Patch9 (user-fontpath) to apply to the new oounix/ directory
- Update Source5 (wrapper script):
  - If ~/.openoffice user directory is not detected, remove any
    reference to that directory in ~/.sversionrc, if that file exists
  - Check that user installation succeeded. Exit 1 otherwise

* Fri May  3 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0-1mdk
- OpenOffice.org 1.0
- Epoch: 1 since openoffice is obsoleted
- Remove Patch10 (psprint-euro) since merged upstream
- Patch10: Fix configure script with NULL statement for X check on Darwin
- Update KDE applnk files, icons and MIME associations unpacking
- Update Source5 (wrapper script) to patch new version tag if a
  previous installation of 641-series is found

* Tue Apr 16 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 1.0-0.1mdk
- Update to OpenOffice.org 641d
- Package is now called OpenOffice.org. Full transition for the file
  hierarchy and revamp of "ooffice" wrapper script will happen later
  though. As this ChangeLog is becoming bigger and bigger ;-)
- ExclusiveArch: %%{ix86} ppc
- Update %%description to clarify myspell purposes
- Add northern languages: Swedish, Finnish, Polish
- Revamp compiler selection:
  - Use gcc-2.95.x on Mandrake Linux 8.2/PowerPC
  - Use gcc-3.0.4 on Mandrake Linux 8.2/Intel x86
  - Use gcc-3.1 on Mandrake Linux 9.0/Intel x86 [LATER]
- Split Patch2 into Patch1 (CLK_TCK), Patch[34] (gcc3[01]-libs)
- Remove Patch6 (bison-1.30+) since merged upstream
- Regenerate Patch3 (MDK config) into Patch5
- Regenerate Patch4 (exceptions specifications) into Patch6
- Update Patch6 with more excp-fixes for bridges test programs
- Update Patch8 (no-mozab) to remove dependency on moz module
- Patch11: Add support for gcc-3.1
- Patch12: Add support for gcc-3.1 to STLport-4.5.3
- Patch13: gcc3+ linker should be g++ instead of gcc
- Patch14: Fix main() return values in bridges tests
- Patch15: Workaround _STL to std:: for gcc < 3.0
- Patch16: JDK 1.3 does exist on PPC too
- Fix /net installation under Xvfb in %%install
- Fix instdb.ins to get rid of $RPM_BUILD_ROOT
- Remove workaround for English wordbook in %%install
- Update "ooffice" wrapper script to reflect this change
- Also make sure to remove (possibly update) broken links to dictionaries

* Wed Mar 13 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 6.0.41-6mdk
- BuildRequires: jdk = 1.3.1 exactly
- Disable strict aliasing for all gcc builds. aka. Fix copy-pasting in
  Calc component.

* Wed Mar  6 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 6.0.41-5mdk
- Add mimetypes item to menu entries
- Fix component wrapper scripts
- Fix %%post to get language from LC_MESSAGES environment variable
  instead of /etc/sysconfig/i18n
- Update the ooffice wrapper script to get rid of possible
  /usr/share/fonts/ttf/japanese/ font path that could cause
  OpenOffice.org to crash

* Mon Mar  4 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 6.0.41-4mdk
- Update %%description to note the existence of myspell-* packages
- Fix %%post and enhance instdb.ins mutation
- Source5: Updates to the ooffice wrapper script to support
  registration of dictionary files in the user's installation

* Mon Mar  4 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 6.0.41-3mdk
- Patch9: Fix printing of Euro symbol (Till, Giuseppe)
- Source5: Updates to the ooffice wrapper script:
  - Enhance even more to take the result of chkfontpath into account,
    if installed. Otherwise, default to the old method
  - Remove any entry in ~/.sversionrc if OpenOffice.org was not
    provided from this package
  - Workaround location of English workdbook

* Thu Feb 28 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 6.0.41-2mdk
- Clean rebuild with more languages: Spanish, Dutch, Italian, Portugese
- Add KDE icons and MIME associations
- Patch8: Support extra font paths with the SAL_FONTPATH_USER variable
- Source5: Enhance with extra font paths (TrueType and Type1 fonts)

* Tue Feb 26 2002 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 6.0.41-1mdk
- Release 641c
- Support (experimental) auto-selection of language pack
- 5 new sources:
  - Source3: Global autoresponse file for rpm build
  - Source4: Local autoresponse file for user setup
  - Source5: OpenOffice.org wrapper script (setup if necessary)
  - Source6: Misc hashes related to language sets
  - Source7: Unpack language sets
- 8 new patches:
  - Patch0: Add support/fixes for gcc-"2.96"
  - Patch1: Add support/fixes for gcc-3.0.4
  - Patch2: Exception patch for STLport 4.5.3 and gcc3
  - Patch3: MDK config (CFLAGS et al.)
  - Patch4: Fix exception specifications here and there
  - Patch5: Hackery around zipdep
  - Patch6: Hackery for new bison (1.30+)
  - Patch7: Don't build Mozilla Address Book

* Mon Oct 23 2000 Frederic Crozat <fcrozat@mandrakesoft.com> 6.0.5-1mdk
- Initial Release
