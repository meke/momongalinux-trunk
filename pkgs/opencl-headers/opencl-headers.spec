%global momorel 1

Summary: Header files for OpenCL API
Name: opencl-headers
Version: 1.2
Release: %{momorel}m%{?dist}
License: "see each header file"

Group: Development/Libraries
URL: http://www.khronos.org/registry/cl/
Source0: http://www.khronos.org/registry/cl/api/1.2/opencl.h
Source1: http://www.khronos.org/registry/cl/api/1.2/cl_platform.h
Source2: http://www.khronos.org/registry/cl/api/1.2/cl.h
Source3: http://www.khronos.org/registry/cl/api/1.2/cl_ext.h
Source4: http://www.khronos.org/registry/cl/api/1.2/cl_gl.h
Source5: http://www.khronos.org/registry/cl/api/1.2/cl_gl_ext.h
Source6: http://www.khronos.org/registry/cl/api/1.2/cl.hpp
NoSource: 0
NoSource: 1
NoSource: 2
NoSource: 3
NoSource: 4
NoSource: 5
NoSource: 6
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Header files for OpenCL API %{version}

%prep
%build
%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_includedir}/CL

install -m 0600 %{SOURCE0} %{buildroot}/%{_includedir}/CL
install -m 0600 %{SOURCE1} %{buildroot}/%{_includedir}/CL
install -m 0600 %{SOURCE2} %{buildroot}/%{_includedir}/CL
install -m 0600 %{SOURCE3} %{buildroot}/%{_includedir}/CL
install -m 0600 %{SOURCE4} %{buildroot}/%{_includedir}/CL
install -m 0600 %{SOURCE5} %{buildroot}/%{_includedir}/CL
install -m 0600 %{SOURCE6} %{buildroot}/%{_includedir}/CL

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_includedir}/CL
%{_includedir}/CL/*

%changelog
* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-1m)
- Initial package for OpenCL API 1.2
