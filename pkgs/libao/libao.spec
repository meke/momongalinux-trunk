%global momorel 1

Summary:	Cross Platform Audio Output Library
Name: libao
Version: 1.1.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.xiph.org/ao/
Source0:	http://downloads.xiph.org/releases/ao/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: arts-devel >= 1.0.3-2m
BuildRequires: esound-devel >= 0.2.8
BuildRequires: nas-devel
BuildRequires: perl
BuildRequires: pulseaudio-libs-devel

%description
Libao is a cross platform audio output library.  It currently supports
ESD, aRts, ALSA, OSS, *BSD and Solaris.

%package devel
Summary: Cross Platform Audio Output Library Development
Group: Development/Libraries
Requires: %{name} = %{version}
Requires: pkgconfig

%description devel
The libao-devel package contains the header files and documentation
needed to develop applications with libao.

%prep
%setup -q

perl -p -i -e "s/-O20/%{optflags}/" configure
perl -p -i -e "s/-ffast-math//" configure

%build
%configure --enable-static
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la file
find %{buildroot}%{_libdir} -maxdepth 1 -name "*.la" -delete

%clean 
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS CHANGES COPYING README TODO
%{_libdir}/libao.so.*
%dir %{_libdir}/ao
%dir %{_libdir}/ao/plugins-4
%{_libdir}/ao/*/libalsa.la
%{_libdir}/ao/*/libalsa.so
%{_libdir}/ao/*/libarts.la
%{_libdir}/ao/*/libarts.so
%{_libdir}/ao/*/libesd.la
%{_libdir}/ao/*/libesd.so
%{_libdir}/ao/*/libnas.la
%{_libdir}/ao/*/libnas.so
%{_libdir}/ao/*/liboss.la
%{_libdir}/ao/*/liboss.so
%{_libdir}/ao/*/libpulse.la
%{_libdir}/ao/*/libpulse.so
%{_mandir}/man5/libao.conf.5*

%files devel
%defattr(-,root,root)
%doc doc/ao_example.c doc/*.css doc/*.html
%{_includedir}/ao
%{_libdir}/ao/*/lib*.a
%{_libdir}/pkgconfig/ao.pc
%{_libdir}/libao.a
%{_libdir}/libao.so
%{_datadir}/aclocal/ao.m4

%changelog
* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-2m)
- full rebuild for mo7 release

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-1m)
- update 1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-2m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-1m)
- version 0.8.8
- enable nas support and pulseaudio support
- License: GPLv2+
- clean up spec file

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6-4m)
- %%NoSource -> NoSource

* Mon Jul 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.6-3m)
- add configure option --disable-nas
- add -maxdepth 1 to del .la

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.6-2m)
- delete libtool library

* Mon May 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.8.6-1m)
- update to 0.8.6

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.5-2m)
- suppress AC_DEFUN warning.

* Thu Apr 22 2004 Tetsuo Sakai <tetsuos@zephyr.dti.ne.jp>
- (0.8.5-1m)
- update to 0.8.5
- Support now for the ALSA 1.0 API
- use %%configure, --enable-alsa09
- add pkgconfig/ao.pc
- change URL

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.3-5m)
- rebuild against for esound-0.2.34

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3-4m)
- kill %%define name
- kill %%define version
- fix change configure by perl

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.3-3m)
- rebuild against arts-1.0.3-2m

* Wed Jul 31 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.8.3-2m)
- BuildRequires: alsa-lib, arts-devel

* Fri Jul 19 2002 Kentarou SHINOHARA <putnium@momonga-linux.org>
- (0.8.3.1m)
- upgrade to 0.8.3
- removed requires alsa >= 0.9.0 from vorbis.com specfile

* Wed Apr 17 2002 Hidetomo Machi <mcht@kondara.org>
- (0.8.2-2k)
- update to 0.8.2 release
- include man file (%files)

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.8.0-4k)
- nigittenu

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (0.8.0-2k)
- merge from Jirai.

* Fri Apr 13 2001 Daiki Matsuda <dyky@df-usa.com>
- (0.5.0-12k)
- change devel package to require library package

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (0.5.0-11k)
- rebuild against audiofile-0.2.1.

* Wed Mar 21 2001 Motonobu Ichimura <famao@kondara.org>
- (0.5.0-9k)
- change group name

* Tue Jan 16 2001 Kenichi Matsubara <m@kondara.org>
- (0.5.0-7k)
- %defattr(-,root,root).

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (0.5.0-5k)
- rebuild against audiofile-0.2.0.

* Mon Dec 11 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- to NoSource(source from http://www.vorbis.com/download.new.html)
- TAG changed (Copyright -> License)

* Mon Dec 11 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- 1st release

* Sun Sep 03 2000 Jack Moffitt <jack@icecast.org>
- initial spec file created
