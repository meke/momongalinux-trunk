%global momorel 1

%global sendmail_path %{_sbindir}/sendmail

Summary: Job spooling tools.
Name: at
Version: 3.1.13
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
Source: http://ftp.debian.org/debian/pool/main/a/at/at_%{version}.orig.tar.gz
NoSource: 0
Source1: pam_atd
Source2: atd.init
Source3: atd.sysconf
Source5: atd.systemd

Patch1:  at-3.1.13-makefile.patch
Patch2:  at-3.1.12-opt_V.patch
Patch3:  at-3.1.12-shell.patch
Patch4:  at-3.1.13-nitpicks.patch
Patch5:  at-3.1.13-pam.patch
Patch6:  at-3.1.13-selinux.patch
Patch7:  at-3.1.12-nowrap.patch
Patch8:  at-3.1.12-fix_no_export.patch
Patch9:  at-3.1.13-mailwithhostname.patch
Patch10: at-3.1.13-usePOSIXtimers.patch
Patch11: at-3.1.13-help.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils chkconfig
Requires(preun): chkconfig
Requires: smtpdaemon
BuildRequires: coreutils
BuildRequires: bison
BuildRequires: flex
BuildRequires: flex-static
BuildRequires: libtool
Conflicts: crontabs <= 1.5

Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
At and batch read commands from standard input or from a specified file.
At allows you to specify that a command will be run at a particular time
(now or a specified time in the future).  Batch will execute commands
when the system load levels drop to a particular level.  Both commands
use /bin/sh to run the commands.

You should install the at package if you need a utility that will do
time-oriented job control.  Note: you should use crontab instead, if it is
a recurring job that will need to be repeated at the same time every
day/week/etc.

%package sysvinit
Summary:        SysV init script for at
Group:          System Environment/Base
Requires:       %{name} = %{version}-%{release}
Requires(post): /sbin/chkconfig

%description sysvinit
SysV style init script for at. It needs to be installed only if systemd
is not used as the system init process.

%prep
%setup -q
cp %{SOURCE1} .
%patch1 -p1 -b .make
%patch2 -p1 -b .opt_V
%patch3 -p1 -b .shell
%patch4 -p1 -b .nit
%patch5 -p1 -b .pam
%patch6 -p1 -b .selinux
%patch7 -p1 -b .nowrap
%patch8 -p1 -b .export
%patch9 -p1 -b .mail
%patch10 -p1 -b .posix
%patch11 -p1 -b .help

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

autoconf

%build
# patch9 touches configure.in
autoconf
# uselles files
rm -f lex.yy.* y.tab.*
%configure --with-atspool=%{_localstatedir}/spool/at/spool \
        --with-jobdir=%{_localstatedir}/spool/at \
        --with-daemon_username=root  \
        --with-daemon_groupname=root \
        --with-selinux \
        --with-pam

#make %{?_smp_mflags} V=1
make V=1

%install
make install \
        DAEMON_USERNAME=`id -nu`\
        DAEMON_GROUPNAME=`id -ng` \
        DESTDIR=%{buildroot}\
        sbindir=%{buildroot}%{_prefix}/sbin\
        bindir=%{buildroot}%{_bindir}\
        prefix=%{buildroot}%{_prefix}\
        exec_prefix=%{buildroot}%{_prefix}\
        docdir=%{buildroot}/usr/doc\
        mandir=%{buildroot}%{_mandir}\
        etcdir=%{buildroot}%{_sysconfdir} \
        ATJOB_DIR=%{buildroot}%{_localstatedir}/spool/at \
        ATSPOOL_DIR=%{buildroot}%{_localstatedir}/spool/at/spool \
        INSTALL_ROOT_USER=`id -nu` \
        INSTALL_ROOT_GROUP=`id -nu`;

echo > %{buildroot}%{_sysconfdir}/at.deny
mkdir docs
cp  %{buildroot}/%{_prefix}/doc/at/* docs/

mkdir -p %{buildroot}%{_sysconfdir}/pam.d
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/pam.d/atd

mkdir -p %{buildroot}%{_sysconfdir}/init.d
install -m 755 %{SOURCE2} %{buildroot}%{_sysconfdir}/init.d/atd

mkdir -p %{buildroot}/etc/sysconfig
install -m 644 %{SOURCE3} %{buildroot}/etc/sysconfig/atd

# install systemd initscript
mkdir -p %{buildroot}/%{_unitdir}/
install -m 644 %{SOURCE5} %{buildroot}/%{_unitdir}/atd.service

# remove unpackaged files from the buildroot
rm -r  %{buildroot}%{_prefix}/doc

%check
make test

%clean
rm -rf %{buildroot}

%post
touch %{_localstatedir}/spool/at/.SEQ
chmod 600 %{_localstatedir}/spool/at/.SEQ
chown daemon:daemon %{_localstatedir}/spool/at/.SEQ
/bin/systemctl enable atd.service >/dev/null 2>&1 || :

%preun
%systemd_preun atd.service

%postun
%systemd_postun_with_restart atd.service

%triggerun -- at < 3.1.12
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply atd
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save atd

# The package is allowed to autostart:
/bin/systemctl enable atd.service >/dev/null 2>&1

/sbin/chkconfig --del atd >/dev/null 2>&1 || :
/bin/systemctl try-restart atd.service >/dev/null 2>&1 || :
/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%triggerpostun -n at-sysvinit -- at < 3.1.12
/sbin/chkconfig --add atd >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%doc docs/*
%attr(0644,root,root)           %config(noreplace) %{_sysconfdir}/at.deny
%attr(0644,root,root)           %config(noreplace) %{_sysconfdir}/sysconfig/atd
%attr(0700,daemon,daemon)       %dir %{_localstatedir}/spool/at
%attr(0600,daemon,daemon)       %verify(not md5 size mtime) %ghost %{_localstatedir}/spool/at/.SEQ
%attr(0700,daemon,daemon)       %dir %{_localstatedir}/spool/at/spool
%attr(0644,root,root)           %config(noreplace) %{_sysconfdir}/pam.d/atd
%{_sbindir}/atrun
%attr(0755,root,root)           %{_sbindir}/atd
%{_mandir}/man*/*
%{_bindir}/batch
%{_bindir}/atrm
%{_bindir}/atq
%attr(4755,root,root)           %{_bindir}/at
%attr(0644,root,root)           /%{_unitdir}/atd.service

%files sysvinit
%attr(0755,root,root)           %{_initscriptdir}/atd

%changelog
* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.12-1m)
- update 3.1.12
- remove sysvinit

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.8-29m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.8-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.8-27m)
- full rebuild for mo7 release

* Tue Jul 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.8-26m)
- add BuildRequires

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.8-25m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.8-24m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.8-23m)
- fix build on x86_64

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.8-22m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.8-21m)
- update Patch5 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.8-20m)
- rebuild against gcc43

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (3.1.8-19m)
- revised spec for enabling rpm 4.2.

* Wed Aug 07 2002 Kenta MURATA <muraken2@nifty.com>
- (3.1.8-18m)
- add at-3.1.8-with-sendmail.patch to configure.in.
- add BuildPreReq: bison.

* Tue Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.8-17m)
- Remove BuildRequire

* Mon Mar 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1.8-16k)
- security bug fixed. heap corruption environment.
  merge from RedHat

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Thu Nov  2 2000 Toru Hoshina <toru@df-usa.com>
- include tar ball because at-3.1.8.tar.bz2 is not in primary site.

* Sat Jul 15 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jul  6 2000 Bill Nottingham <notting@redhat.com>
- prereq /etc/init.d

* Sat Jul  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix syntax error in init script

* Tue Jun 27 2000 Preston Brown <pbrown@redhat.com>
- don't prereq, only require initscripts

* Mon Jun 26 2000 Preston Brown <pbrown@redhat.com>
- move init script
- add condrestart directive
- fix post/preun/postun scripts
- prereq initscripts >= 5.20

* Sat Jun 17 2000 Bill Nottingham <notting@redhat.com>
- fix verify of /var/spool/at/.SEQ (#12262)

* Mon Jun 12 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix status checking and syntax error in init script

* Fri Jun  9 2000 Bill Nottingham <notting@redhat.com>
- fix for long usernames (#11321)
- add some bugfixes from debian

* Mon May  8 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 3.1.8

* Wed Mar  1 2000 Bill Nottingham <notting@redhat.com>
- fix a couple of more typos, null-terminate some strings

* Thu Feb 10 2000 Bill Nottingham <notting@redhat.com>
- fix many-years-old typo in atd.c

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging, build as non-root user

* Sun Jun 13 1999 Jeff Johnson <jbj@redhat.com>
- correct perms for /var/spool/at after defattr.

* Mon May 24 1999 Jeff Johnson <jbj@redhat.com>
- reset SIGCHLD before exec (#3016).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Thu Mar 18 1999 Cristian Gafton <gafton@redhat.com>
- fix handling the 12:00 time

* Wed Jan 13 1999 Bill Nottingham <notting@redhat.com>
- configure fix for arm

* Wed Jan 06 1999 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 22 1998 Michael K. Johnson <johnsonm@redhat.com>
- enhanced initscript

* Sun Nov 09 1997 Michael K. Johnson <johnsonm@redhat.com>
- learned to spell

* Wed Oct 22 1997 Michael K. Johnson <johnsonm@redhat.com>
- updated to at version 3.1.7
- updated lock and sequence file handling with %ghost
- Use chkconfig and atd, now conflicts with old crontabs packages

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc

