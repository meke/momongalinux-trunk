%global momorel 14
Name: xmms-kjofol-skins
Summary: XMMS - Vis plugin to get kjofol skins + some kjofol skins
Version: 0.95
Release: %{momorel}m%{?dist}
Source0: http://www.dgs.monash.edu.au/~timf/kint_xmms-0.95.tar.gz
Source1: kjofol-skins.tar.bz2
Patch0: kint_xmms-0.94.patch
Patch1: xmms-kj-cflags.patch
License: GPL
Group: Applications/Multimedia
URL: http://www.dgs.monash.edu.au/~timf/xmms/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Prefix: %{_prefix}
Requires: xmms >= 1.2.10-10m, unzip
BuildRequires: gtk+1-devel libpng-devel >= 1.2.2
BuildRequires: xmms-devel >= 1.2.10-10m 

%description
In this package you can find the Visualization plugin that enable K-Jofol skins
support, and several cute K-Jofol skins.

%prep
%setup -q -a 1 -n xmms-kj
%patch0 -p1
%patch1 -p1 -b .cflags~

%build
%make vislib OPTFLAGS="%{optflags}"

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_libdir}/xmms/Visualization
mkdir -p %{buildroot}%{_datadir}/xmms/kjofol
make LIBDIR=%{buildroot}%{_libdir}/xmms/Visualization install-vislib
install -m644 default.zip kjofol/* %{buildroot}%{_datadir}/xmms/kjofol

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING readme.txt
%{_libdir}/xmms/Visualization/libkjofol*
%{_datadir}/xmms/kjofol/

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-14m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.95-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.95-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.95-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.95-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.95-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95-8m)
- rebuild against gcc43

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.95-7m)
- change installdir (/usr/X11R6 -> /usr)

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.95-6m)
- enable x86_64.

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.95-5m)
- rebuild against xmms-1.2.8

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.95-4k)
- rebuild against libpng-1.2.2

* Wed Oct 24 2001 Toru Hoshina <t@kondara.org>
- (0.95-2k)
- 1st release.
