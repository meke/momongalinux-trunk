%global           momorel 1

%global           nspr_version 4.10.6

Summary:          Network Security Services Utilities Library
Name:             nss-util
Version:          3.16.1
Release:          %{momorel}m%{?dist}
License:          MPLv1.1 or GPLv2+ or LGPLv2+
URL:              http://www.mozilla.org/projects/security/pki/nss/
Group:            System Environment/Libraries
Requires:         nspr >= %{nspr_version}
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:    nspr-devel >= %{nspr_version}
BuildRequires:    zlib-devel
BuildRequires:    pkgconfig
BuildRequires:    gawk
BuildRequires:    psmisc
BuildRequires:    perl

Source0:          %{name}-%{version}.tar.bz2
# The nss-util tar ball is a subset of nss-{version}-stripped.tar.bz2, 
# Therefore we use the nss-split-util.sh script to keeping only what we need.
# Download the nss tarball via CVS from the nss propect and follow these
# steps to make the r tarball for nss-util out of the for nss:
# fedpkg clone nss
# fedpkg clone nss-util
# cd nss-util/master
# cp ../../nss/devel/${version}-stripped.tar.bz2 .
# sh ./nss-split-util.sh ${version}
# A file named {name}-{version}.tar.bz2 should appear
Source1:          nss-split-util.sh
Source2:          nss-util.pc.in
Source3:          nss-util-config.in

Patch0:           build-nss-util-only.patch
Patch2:           hasht-dont-include-prtypes.patch

%description
Utilities for Network Security Services and the Softoken module

# We shouln't need to have a devel subpackage as util will be used in the
# context of nss or nss-softoken. keeping to please rpmlint.
# 
%package devel
Summary:          Development libraries for Network Security Services Utilities
Group:            Development/Libraries
Requires:         nss-util = %{version}-%{release}
Requires:         nspr-devel >= %{nspr_version}
Requires:         pkgconfig

%description devel
Header and library files for doing development with Network Security Services.


%prep
%setup -q
%patch0 -p0 -b .utilonly
%patch2 -p0 -b .prtypes

%build

# Enable compiler optimizations and disable debugging code
BUILD_OPT=1
export BUILD_OPT

# Uncomment to disable optimizations
#RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS | sed -e 's/-O2/-O0/g'`
#export RPM_OPT_FLAGS

# Generate symbolic info for debuggers
XCFLAGS=$RPM_OPT_FLAGS
export XCFLAGS

PKG_CONFIG_ALLOW_SYSTEM_LIBS=1
PKG_CONFIG_ALLOW_SYSTEM_CFLAGS=1

export PKG_CONFIG_ALLOW_SYSTEM_LIBS
export PKG_CONFIG_ALLOW_SYSTEM_CFLAGS

NSPR_INCLUDE_DIR=`/usr/bin/pkg-config --cflags-only-I nspr | sed 's/-I//'`
NSPR_LIB_DIR=`/usr/bin/pkg-config --libs-only-L nspr | sed 's/-L//'`

export NSPR_INCLUDE_DIR
export NSPR_LIB_DIR

NSS_USE_SYSTEM_SQLITE=1
export NSS_USE_SYSTEM_SQLITE

%ifarch x86_64 ppc64 ia64 s390x sparc64
USE_64=1
export USE_64
%endif

# make util
%{__make} -C ./nss/coreconf
%{__make} -C ./nss

# Set up our package file
%{__mkdir_p} ./dist/pkgconfig
%{__cat} %{SOURCE2} | sed -e "s,%%libdir%%,%{_libdir},g" \
                          -e "s,%%prefix%%,%{_prefix},g" \
                          -e "s,%%exec_prefix%%,%{_prefix},g" \
                          -e "s,%%includedir%%,%{_includedir}/nss3,g" \
                          -e "s,%%NSPR_VERSION%%,%{nspr_version},g" \
                          -e "s,%%NSSUTIL_VERSION%%,%{version},g" > \
                          ./dist/pkgconfig/nss-util.pc

NSSUTIL_VMAJOR=`cat nss/lib/util/nssutil.h | grep "#define.*NSSUTIL_VMAJOR" | awk '{print $3}'`
NSSUTIL_VMINOR=`cat nss/lib/util/nssutil.h | grep "#define.*NSSUTIL_VMINOR" | awk '{print $3}'`
NSSUTIL_VPATCH=`cat nss/lib/util/nssutil.h | grep "#define.*NSSUTIL_VPATCH" | awk '{print $3}'`

export NSSUTIL_VMAJOR 
export NSSUTIL_VMINOR 
export NSSUTIL_VPATCH

%{__cat} %{SOURCE3} | sed -e "s,@libdir@,%{_libdir},g" \
                          -e "s,@prefix@,%{_prefix},g" \
                          -e "s,@exec_prefix@,%{_prefix},g" \
                          -e "s,@includedir@,%{_includedir}/nss3,g" \
                          -e "s,@MOD_MAJOR_VERSION@,$NSSUTIL_VMAJOR,g" \
                          -e "s,@MOD_MINOR_VERSION@,$NSSUTIL_VMINOR,g" \
                          -e "s,@MOD_PATCH_VERSION@,$NSSUTIL_VPATCH,g" \
                          > ./dist/pkgconfig/nss-util-config

chmod 755 ./dist/pkgconfig/nss-util-config


%install

%{__rm} -rf $RPM_BUILD_ROOT

# There is no make install target so we'll do it ourselves.

%{__mkdir_p} $RPM_BUILD_ROOT/%{_includedir}/nss3
%{__mkdir_p} $RPM_BUILD_ROOT/%{_includedir}/nss3/templates
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libdir}
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libdir}/nss3
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libdir}/pkgconfig
%{__mkdir_p} $RPM_BUILD_ROOT/%{_bindir}

for file in libnssutil3.so
do
  %{__install} -p -m 755 dist/*.OBJ/lib/$file $RPM_BUILD_ROOT/%{_libdir}
done

# Copy the include files we want
# The util headers, the rest come from softokn and nss
for file in dist/public/nss/*.h
do
  %{__install} -p -m 644 $file $RPM_BUILD_ROOT/%{_includedir}/nss3
done

# Copy the template files we want
for file in dist/private/nss/templates.c
do
  %{__install} -p -m 644 $file $RPM_BUILD_ROOT/%{_includedir}/nss3/templates
done

# Copy the package configuration files
%{__install} -p -m 644 ./dist/pkgconfig/nss-util.pc $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/nss-util.pc
%{__install} -p -m 755 ./dist/pkgconfig/nss-util-config $RPM_BUILD_ROOT/%{_bindir}/nss-util-config

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig >/dev/null 2>/dev/null

%postun
/sbin/ldconfig >/dev/null 2>/dev/null

%files
%defattr(-,root,root)
%{_libdir}/libnssutil3.so

%files devel
%defattr(-,root,root)
# package configuration files
%{_libdir}/pkgconfig/nss-util.pc
%{_bindir}/nss-util-config

# co-owned with nss
%dir %{_includedir}/nss3
# these are marked as public export in
# mozilla/security/nss/lib/util/manifest.mk
%{_includedir}/nss3/base64.h
%{_includedir}/nss3/ciferfam.h
%{_includedir}/nss3/hasht.h
%{_includedir}/nss3/nssb64.h
%{_includedir}/nss3/nssb64t.h
%{_includedir}/nss3/nsslocks.h
%{_includedir}/nss3/nssilock.h
%{_includedir}/nss3/nssilckt.h
%{_includedir}/nss3/nssrwlk.h
%{_includedir}/nss3/nssrwlkt.h
%{_includedir}/nss3/nssutil.h
%{_includedir}/nss3/pkcs11.h
%{_includedir}/nss3/pkcs11f.h
%{_includedir}/nss3/pkcs11n.h
%{_includedir}/nss3/pkcs11p.h
%{_includedir}/nss3/pkcs11t.h
%{_includedir}/nss3/pkcs11u.h
%{_includedir}/nss3/portreg.h
%{_includedir}/nss3/secasn1.h
%{_includedir}/nss3/secasn1t.h
%{_includedir}/nss3/seccomon.h
%{_includedir}/nss3/secder.h
%{_includedir}/nss3/secdert.h
%{_includedir}/nss3/secdig.h
%{_includedir}/nss3/secdigt.h
%{_includedir}/nss3/secerr.h
%{_includedir}/nss3/secitem.h
%{_includedir}/nss3/secoid.h
%{_includedir}/nss3/secoidt.h
%{_includedir}/nss3/secport.h
%{_includedir}/nss3/utilmodt.h
%{_includedir}/nss3/utilpars.h
%{_includedir}/nss3/utilparst.h
%{_includedir}/nss3/utilrename.h
%{_includedir}/nss3/templates/templates.c

%changelog
* Sat Jun 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16.1-1m)
- update to 3.16.1

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16.0-1m)
- update to 3.16.0

* Thu Feb  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.4-1m)
- update to 3.15.4

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.3-1m)
- [SECURITY] CVE-2013-1741 CVE-2013-5605 CVE-2013-5606
- update to 3.15.3

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.2-1m)
- update to 3.15.2

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.1-1m)
- update to 3.15.1

* Fri Mar  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.3-1m)
- update to 3.14.3

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.2-1m)
- update to 3.14.2

* Mon Dec 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.1-1m)
- update to 3.14.1

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.6-1m)
- update to 3.13.6

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.3-1m)
- update to 3.13.3

* Sat Dec 24 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.13.1-2m)
- import patch from rawhide
   - Fix a gnuc def typo

* Wed Dec 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.1-1m)
- update to 3.13.1

* Thu Sep  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.11-1m)
- update to 3.12.11

* Sun Aug 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.10-2m)
- update workaround for linux-3.x

* Tue Aug  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.10-1m)
- update
- merge with fedora's nss-util-3.12.10-1
- add workaround for linux-3.x

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.9-2m)
- rebuild for new GCC 4.6

* Fri Feb 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.9-1m)
- update

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.8-2m)
- rebuild for new GCC 4.5

* Thu Nov  4 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.8-1m)
- update to 3.12.8

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.7-1m)
- update to 3.12.7
- NSS_USE_SYSTEM_SQLITE=1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.12.6-2m)
- full rebuild for mo7 release

* Wed Mar 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.6-1m)
- update to 3.12.6

* Fri Dec 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.5-1m)
- [SECURITY] CVE-2009-3555
- update to 3.12.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-2m)
- requires nspr-4.8.2 or later

* Tue Oct 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-1m)
- import from Fedora 12

* Thu Sep 10 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-8
- Retagging for a chained build with nss-softokn and nss

* Thu Sep 10 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-5
- Restoring -rpath-link to nss-util-config

* Tue Sep 08 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-4
- Installing shared libraries to %%{_libdir}

* Sat Sep 05 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-3
- Remove symbolic links to shared libraries from devel - 521155
- Apply nss-nolocalsql patch subset for nss-util
- No rpath-link in nss-util-config

* Fri Sep 04 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-2
- Retagging for a chained build

* Thu Sep 03 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-1
- Update to 3.12.4
- Don't require sqlite

* Thu Aug 27 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-15
- Bump the release number for a chained build of nss-util, nss-softokn and nss

* Thu Aug 27 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-14
- Cleanup nss-util-config.in

* Thu Aug 27 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-13
- nss-util-devel doesn't require nss-devel

* Wed Aug 26 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-12
- bump to unique nvr

* Wed Aug 26 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-11
- Remove spurious executable permissions from nss-util-config
- Shorten some descriptions to keep rpmlint happy

* Mon Aug 24 2009 Dennis Gilmore <dennis@ausil.us> 3.12.3.99.3-10
- dont include the headers in nss-util only in the -devel package
- nss-util-devel Requires nss-devel since its only providing a subset of the headers.

* Thu Aug 20 2009 Dennis Gilmore <dennis@ausil.us> 3.12.3.99.3-9
- Provide nss-devel since we obsolete it

* Thu Aug 19 2009 Elio Maldonado <emaldona@redhat.com> 3.12.3.99.3-8.1
- nss-util-devel obsoletes nss-devel < 3.12.3.99.3-8

* Thu Aug 19 2009 Elio Maldonado <emaldona@redhat.com> 3.12.3.99.3-8
- Initial build
