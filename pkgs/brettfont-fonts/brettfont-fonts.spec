%global momorel 6

%define fontname brettfont

Name:           %{fontname}-fonts
Version:        20080506
Release:        %{momorel}m%{?dist}
Summary:        A handwriting font

Group:          User Interface/X
License:        OFL
URL:            http://openfontlibrary.org/media/files/brettalton/205
Source0:        BrettFont1.1.ttf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
A handwriting font made by Brett Alton.


%prep

%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{SOURCE0} %{buildroot}%{_fontdir}/brettfont.ttf


%clean
rm -fr %{buildroot}


%_font_pkg *.ttf

%doc

%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080506-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080506-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20080506-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080506-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080506-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20080506-1m)
- import from Fedora

* Sun Mar 8 2009 Jon Stanley <jonstanley@gmail.com> - 20080506-5
- Update to new packaging guidelines

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20080506-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon May 19 2008 Jon Stanley <jonstanley@gmail.com> - 20080506-3
- Remove extraneous metadata (#447350)

* Wed May 07 2008 Jon Stanley <jonstanley@gmail.com> - 20080506-2
- Change installed filename to something more palatabe (#445531)

* Tue May 06 2008 Jon Stanley <jonstanley@gmail.com> - 20080506-1
- Bump version, remove %%prep cp, change %%install

* Mon May 05 2008 Jon Stanley <jonstanley@gmail.com> - 1.0-2
- Changed %%description

* Mon May 05 2008 Jon Stanley <jonstanley@gmail.com> - 1.0-1
- Initial package

