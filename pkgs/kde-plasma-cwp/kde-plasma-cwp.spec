%global         momorel 1
%global         kdebaseworkspacerel 1m
%global         kdever 4.12.1
%global         qtver 4.8.5
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         content_no 98925
%global         src_name cwp

Name:           kde-plasma-cwp
Version:        1.10.0
Release:        %{momorel}m%{?dist}
Summary:        Another Weather Plasmoid
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://kde-apps.org/content/show.php/Customizable+Weather+Plasmoid+%28CWP%29?content=%{content_no}
Source0:        http://kde-apps.org/CONTENT/content-files/%{content_no}-%{src_name}-%{version}.tar.bz2
NoSource:	0
Patch0:         %{name}-1.5.13-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gettext
BuildRequires:  kde-workspace-devel >= %{kdever}-%{kdebaseworkspacerel}
Requires:       kde-workspace >= %{kdever}-%{kdebaseworkspacerel}

%description
This is another weather plasmoid.
It aims to be highly customizable, but a little harder to setup than other weather plasmoids.
Nearly any weather forecast provider can be used, as long as the data is provided as html files (no flash).
The information how to extract the information from these html files is stored inside xml files.
Commands like grep, head, tail, sed, awk, ... will do this job.

%prep
%setup -q -n %{src_name}-%{version}
%patch0 -p1

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :
  gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README
%{_kde4_libdir}/kde4/plasma_applet_cwp.so
%{_kde4_appsdir}/desktoptheme/default/widgets/*
%{_kde4_appsdir}/plasma-cwp
%{_kde4_iconsdir}/oxygen/*/status/weather-windy.*
%{_kde4_datadir}/kde4/services/plasma-applet-cwp.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/plasma_applet_cwp.mo

%changelog
* Wed Jan 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0

* Thu Dec 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.1-1m)
- update to 1.9.1

* Wed Nov  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3

* Sat Sep 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2

* Sat Jul 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Sun Jun 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Fri Jun 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.3-1m)
- update to 1.7.3

* Mon Apr 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.2-1m)
- update to 1.7.2

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-1m)
- update to 1.7.1

* Wed Feb 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0-1m)
- update to 1.7.0

* Mon Feb 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.13-1m)
- update to 1.6.13

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.12-1m)
- update to 1.6.12

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.11-1m)
- update to 1.6.11

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.10-1m)
- update to 1.6.10

* Fri Nov 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.9-1m)
- update to 1.6.9

* Thu Nov 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.8-1m)
- update to 1.6.8

* Sun Nov  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.7-1m)
- update to 1.6.7

* Mon Oct 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.6-1m)
- update to 1.6.6

* Mon Oct  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.5-1m)
- update to 1.6.5

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4

* Wed Aug  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3

* Sun Jun 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Sun May  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.14-1m)
- update to 1.5.14

* Sat Mar 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.13-1m)
- initial build for Momonga Linux
