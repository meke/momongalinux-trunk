%global momorel 22

Summary: WWW wo Miru Tool
Name: w3m
Version: 0.5.2
Release: %{momorel}m%{?dist}
License: MIT/X
URL: http://w3m.sourceforge.net/
Group: Applications/Internet
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-libdir.patch
Patch1: w3m-0.5.2-nulcn.patch
Patch2: w3m-0.5.2-ssl_verify_server_on.patch
Patch3: w3m-0.5.2-file_handle.patch
Patch4: w3m-0.5.2-gc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby, perl
BuildRequires: openssl-devel >= 1.0.0, ncurses-devel
BuildRequires: gdk-pixbuf-devel >= 0.16.0
BuildRequires: libpng-devel >= 1.2.2, gc-devel
BuildRequires: gpm-devel >= 1.20.5
Requires: jed
Requires(post): chkconfig
Requires(postun): chkconfig
Provides: webclient, pager
Obsoletes: w3m-m17n

%description
W3m is a pager adapted to World Wide Web. W3m is a text-based WWW
browser as well as a pager.

%package img
Summary: w3m - WWW wo Miru Tool - with image and romaji serach support
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}
Requires(post): chkconfig
Requires(postun): chkconfig
Obsoletes: w3m-m17n-img

%description img
W3m is a pager adapted to World Wide Web. W3m is a text-based WWW
browser as well as a pager.

This w3m-img package supports romaji search, incremental search and
displaying inline images.

%package cgi
Summary: dirlist cgi supplement for w3m
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}, perl
Obsoletes: w3m-m17n-cgi

%description cgi
Internal cgi supplement for w3m.

%prep
%__rm -rf %{buildroot}
%setup -q -n w3m-%{version}
%if %{_lib} == "lib64"
%patch0 -p1
%endif
%patch1 -p1 -b .CVE-2010-2074
%patch2 -p1 -b .ssl_verify_server
%patch3 -p1 -b .file_handle
%patch4 -p1 -b .gc~
perl -p -i -e 's,/usr/local/bin,/usr/bin,g' Bonus/*

%build
%define confargs --with-editor='/usr/bin/jed' --with-mailer='/usr/bin/mutt' --with-browser="/usr/bin/mozilla -remote 'openURL('%s',new-tab)' --enable-gopher --disable-xface" --with-termlib=ncurses

%configure %confargs --enable-image=no --with-migemo=no
%make
%__mv w3m w3m-normal
make clean

%configure %confargs --with-migemo='migemo -t egrep /usr/share/migemo/migemo-dict' --enable-japanese=E LIBS="-lX11"
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
mkdir -p %{buildroot}%{_sysconfdir}/w3m
%__install -m 755 w3m %{buildroot}%{_bindir}/w3m-img
%__install -m 755 w3m-normal %{buildroot}%{_bindir}/w3m

find . -size 0 | xargs %__rm -f
find Bonus w3m-doc -type f | xargs %__chmod -x

# convert Japanese manual page from EUC-JP to UTF-8
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
        iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
%{_sbindir}/alternatives --install %{_bindir}/pager pager %{_bindir}/w3m 60

%postun
[ -e %{_bindir}/w3m ] || %{_sbindir}/alternatives --remove pager %{_bindir}/w3m

%post -n w3m-img
%{_sbindir}/alternatives --install %{_bindir}/pager pager %{_bindir}/w3m-img 55

%postun -n w3m-img
[ -e %{_bindir}/w3m-img ] || %{_sbindir}/alternatives --remove pager %{_bindir}/w3m-img

%files
%defattr(-, root, root)
%doc Bonus ChangeLog NEWS README TODO doc doc-jp
%{_sysconfdir}/w3m
%{_bindir}/w3m
%dir %{_libexecdir}/w3m
%{_libexecdir}/w3m/inflate
%dir %{_libexecdir}/w3m/cgi-bin
%{_libexecdir}/w3m/cgi-bin/w3mbookmark
%{_libexecdir}/w3m/cgi-bin/w3mhelperpanel
%{_mandir}/man1/w3m.1*
%{_mandir}/ja/man1/w3m.1*
%{_datadir}/locale/*/*/*
%{_datadir}/w3m/w3mhelp-funcdesc.en.pl
%{_datadir}/w3m/w3mhelp-funcdesc.ja.pl
%{_datadir}/w3m/w3mhelp-funcname.pl
%{_datadir}/w3m/w3mhelp.html

%files img
%defattr(-, root, root)
%{_bindir}/w3m-img
%attr(4755,root,root) %{_libexecdir}/w3m/w3mimgdisplay
%{_libexecdir}/w3m/xface2xpm

%files cgi
%defattr(-, root, root)
%{_bindir}/w3mman
%{_libexecdir}/w3m/cgi-bin/dirlist.cgi
%{_libexecdir}/w3m/cgi-bin/multipart.cgi
%{_libexecdir}/w3m/cgi-bin/w3mhelp.cgi
%{_libexecdir}/w3m/cgi-bin/w3mmail.cgi
%{_libexecdir}/w3m/cgi-bin/w3mman2html.cgi
%{_mandir}/man1/w3mman.1*

%changelog
* Sun Apr 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-22m)
- fix build failure

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-21m)
- enable to build with glibc-2.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.2-18m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-17m)
- fix build

* Wed Jun 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-16m)
- [SECURITY] CVE-2010-2074
- import security patches from Fedora 13 (0.5.2-18)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-15m)
- use BuildRequires

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-14m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-12m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-11m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-10m)
- rebuild against gpm-1.20.5

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-9m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-8m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-7m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.2-6m)
- rebuild against perl-5.10.0-1m

* Sat Dec  1 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-5m)
- fixed alternatives

* Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-4m)
- revised alternatives

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.2-3m)
- convert ja.man from EUC-JP to UTF-8

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-2m)
- replace patch

* Mon Jun  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-1m)
- [SECURITY] CVE-2007-3125
- update to 0.5.2

* Tue Jan 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-7m)
- [SECURITY] CVE-2006-6772
- crashes on -dump or -backend with "%n%n" in SSL certificate

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.1-6m)
- rebuild against openssl-0.9.8a

* Mon Mar 28 2005 TAKAHASHI Tamotsu <tamo>
- (0.5.1-5m)
- add --with-termlib=ncurses
  (The default value is "terminfo mytinfo termcap ncurses curses"
  but ncurses is better than libtermcap IMHO)

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.5.1-4m)
- rebuild against libtermcap and ncurses

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.5.1-3m)
- fix libdir check for multilib arch.

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (0.5.1-2m)
- enable x86_64.

* Sat Jun 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.1-1m)
- now w3m supports m17n
- cancel alternatives of w3m

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4.2-5m)
- rebuild against ncurses 5.3.

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4.2-4m)
- revised spec for rpm 4.2.

* Sat Oct 25 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.2-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Oct 25 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.2-2m)
- add '--disable-xface'

* Tue Sep 23 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.2-1m)

* Tue Apr  8 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (0.4.1-3m)
- depends on gdk-pixbuf instead of imlib
  http://mi.med.tohoku.ac.jp/~satodai/w3m-dev/200209.month/3327.html
  http://mi.med.tohoku.ac.jp/~satodai/w3m-dev/200301.month/3665.html

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.4.1-2m)
  rebuild against openssl 0.9.7a

* Fri Mar  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.1-1m)

* Fri Feb 28 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.3.2.2-5m)
- enable stack protector

* Sat Feb 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2.2-4m)
- add requires jed

* Fri Feb 21 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3.2.2-3m)
- use internal boehm-gc.

* Fri Dec 20 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.3.2.2-2m)
- s/w3m-%%{version}/w3m.sh/

* Fri Dec  6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2.2-1m)
- security fix

* Fri Nov 29 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2.1-2m)
- add version at requires chkconfig for alternatives

* Wed Nov 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.2.1-1m)
- security fix

* Wed Nov 20 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.3.1-4m)
- adopt the alternative system(requires chkconfig, provides pager, w3m).

* Sat Oct  5 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.3.1-3m)
- fix build problem on gcc-3.X environment (Patch100: w3m-gc.patch)
  gc requires -ldl

* Sun Aug 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-2m)
- revise external browser setting

* Tue Jul 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-1m)
- add some missing files

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.3-8k)
- BuildPreReq: imlib-devel >= 1.9.14-4k

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.3-6k)
- rebuild against libpng 1.2.2.

* Sat Mar 23 2002 Toru Hoshina <t@kondara.org>
- (0.3-4k)
- use outside gc.

* Wed Mar  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.3-2k)

* Wed Feb  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.5.1-2k)
- 0.2.5.1 + cvs snapshot patch
- migemo support is merged in 0.2.5.1
- image support is merged in snapshot

* Thu Jan 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.4-6k)
- add w3mmail.cgi

* Tue Jan 22 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.4-4k)
- follow cvs
- never requires migemo-cs

* Sun Jan 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.2.4-2k)

* Fri Dec 28 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.3.2-4k)
- fix typo in config.param files
- include /etc/w3m

* Sun Dec 23 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.3.2-2k)
- remove w3m-doc etc.

* Tue Dec  3 2001 Toru Hoshina <t@kondara.org>
- (0.2.2-8k)
- dirlist.cgi...

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (0.2.2-6k)
- provide webclient as a virtual package.
- No docs could be excutable :-p

* Mon Nov 26 2001 Tsutomu Yasuda <tom@kondara.org>
- (0.2.2-4k)
  rebuild faild on Alpha architecture.
  added "make clean -C gc"

* Fri Nov 23 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.2-2k)
- update to w3m-0.2.2
- update to img-1.14
- divide into w3m and w3m-img packages
- remove w3m-img-default.patch
- remove quite ugly hack for perl path problem...

* Fri Nov 17 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-22k)
- update to inu-1.6
- update to img-1.13
- revise romaji+isearch patch
- disable img functions by default

* Sat Nov 17 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.2.1-20k)
- add man page from doc/ and doc-jp/
 
* Tue Nov  6 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-18k)
- quite ugly hack for perl path problem...

* Mon Nov  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-16k)
- update to img-1.12
- add w3m-doc directory to %doc

* Sat Nov  3 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-14k)
- update to inu-1.5
- revise img-1.11 patch for inu-1.5

* Wed Oct 31 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-12k)
- use ruby's regex library (Thanks to HIDAI Ken-ichi <hidai@nmn.jp>)

* Thu Oct 18 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-10k)
- based on w3m-0.2.1-inu-1.4
- revise romaji+isearch-0.6 patch for w3m-0.2.1-inu-1.4

* Thu Oct 18 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.2.1-8k)
- merge w3m-romaji+isearch-0.6.patch from Jirai (0.2.1-9k)

* Thu Jun 21 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.2.1-6k)
- apply security fix patch [w3m-dev 02066]

* Thu May 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.1-4k)
- import from Jirai
- add w3m.wrapper for changable language

* Fri Apr 06 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (0.2.1-3k)
- rebuild for Jirai

* Fri Apr 06 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (0.2.1-0k8KOM)
- define "use_japanese" (default 1)
