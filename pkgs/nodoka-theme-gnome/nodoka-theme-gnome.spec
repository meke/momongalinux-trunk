%global momorel 7

%define nodoka_dir %{_datadir}/themes/Nodoka

Name:           nodoka-theme-gnome
Version:        0.3.90
Release:        %{momorel}m%{?dist}
Summary:        The Nodoka Theme Pack for Gnome

Group:          System Environment/Libraries
License:        GPLv2
URL:            http://hosted.fedoraproject.org/projects/nodoka

# can get on a wiki, see URL
Source0:        %{name}-%{version}.tar.gz 

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       gtk-nodoka-engine >= 0.3.1.1
Requires:       nodoka-metacity-theme
Requires:       fedora-icon-theme
Requires:       notification-daemon-engine-nodoka

%description
The Nodoka Theme Pack for Gnome make use of Nodoka Metacity theme, Nodoka gtk2
theme and Echo Icon set.


# subpackage has inverse relationship to the main package
# the reason is that metacity theme is a part of the whole theme and as its
# in one source with the metatheme the nodoka-theme-gnome seems more rational
# to be the name of the main package

%package -n     nodoka-metacity-theme
Summary:        The Nodoka theme for Metacity 
Group:          System Environment/Libraries

# needed for dir ownership
Requires:       nodoka-filesystem

%description -n nodoka-metacity-theme
The Nodoka theme for metacity. A clean theme featuring soft gradients and 
Echoey look and feel.

%package -n     nodoka-filesystem
Summary:        The directory infrastructure for Nodoka
Group:          System Environment/Libraries

# Require the %{_datadir}/themes directory
Requires:       filesystem
Conflicts:      gtk-nodoka-engine < 0.7.0-2

%description -n nodoka-filesystem
The directory infrastructure needed by various Nodoka packages.

%prep
%setup -q
echo 'NotificationTheme=nodoka' >> Nodoka/index.theme

%build

%install
rm -rf $RPM_BUILD_ROOT
%{__install} -Dp -m 0644 Nodoka/index.theme                             $RPM_BUILD_ROOT/%{nodoka_dir}/index.theme
%{__install} -Dp -m 0644 Nodoka/metacity-1/button_close.png             $RPM_BUILD_ROOT/%{nodoka_dir}/metacity-1/button_close.png
%{__install} -Dp -m 0644 Nodoka/metacity-1/button_maximize.png          $RPM_BUILD_ROOT/%{nodoka_dir}/metacity-1/button_maximize.png
%{__install} -Dp -m 0644 Nodoka/metacity-1/button_minimize.png          $RPM_BUILD_ROOT/%{nodoka_dir}/metacity-1/button_minimize.png
%{__install} -Dp -m 0644 Nodoka/metacity-1/menu_button_close.png        $RPM_BUILD_ROOT/%{nodoka_dir}/metacity-1/menu_button_close.png
%{__install} -Dp -m 0644 Nodoka/metacity-1/menu_button_maximize.png     $RPM_BUILD_ROOT/%{nodoka_dir}/metacity-1/menu_button_maximize.png
%{__install} -Dp -m 0644 Nodoka/metacity-1/menu_button_minimize.png     $RPM_BUILD_ROOT/%{nodoka_dir}/metacity-1/menu_button_minimize.png
%{__install} -Dp -m 0644 Nodoka/metacity-1/metacity-theme-1.xml         $RPM_BUILD_ROOT/%{nodoka_dir}/metacity-1/metacity-theme-1.xml

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%{nodoka_dir}/index.theme

%files -n nodoka-metacity-theme
%defattr(-,root,root,-)
%{nodoka_dir}/metacity-1

%files -n nodoka-filesystem
%defattr(-,root,root,-)
%dir %{nodoka_dir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.90-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.90-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.90-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.90-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.90-3m)
- create nodoka-filesystem subpackage

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.90-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.90-1m)
- import from Fedora to Momonga

* Sun Feb 10 2008 Martin Sourada <martin.sourada@gmail.com> - 0.3.90-1
- New release 0.4 beta

* Thu Sep 27 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.2-2
- Require fedora-icon-theme instead of redhat-artwork (rhbz #309631)

* Thu Sep 13 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.2-1.fc8.1
- fix dir name

* Thu Sep 13 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.2-1
- new version, reworked gradients in metacity theme

* Sat Aug 11 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.1.2-1
- new version, change used icon set to fedora (in redhat-artwork pkg)

* Thu Aug 09 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.1.1-4
- update License: field to GPLv2

* Sat Aug 04 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.1.1-3
- fix dir ownership
- add a comment about the inverse relationship of the main package to the 
  subpackage
- add a comment about upstream sources location

* Fri Jul 27 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.1.1-2
- remove empty %%dir for nodoka-metacity-theme
- fix the %%description to be more sane

* Fri Jul 13 2007 Martin Sourada <martin.sourada@seznam.cz> - 0.3.1.1-1
- split metacity and metatheme into separate package in upstream
