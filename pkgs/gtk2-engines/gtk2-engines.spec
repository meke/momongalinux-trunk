%global momorel 3
%global realname gtk-engines

Summary: Theme engines for GTK+.
Name: gtk2-engines
Version: 2.20.2
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/2.20/%{realname}-%{version}.tar.bz2 
NoSource: 0
Patch0: gtk-engines-2.16.0-clealooks.patch
Patch1: gtk-engines-2.18.1-cast.patch
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.16.0
Requires(pre): gtk2
Obsoletes: crux

Provides: %{realname}
Obsoletes: %{realname}

%description
The gtk-engines package contains shared objects and configuration
files that implement a number of GTK+ theme engines.  Theme engines
provide different looks for GTK+, so that it can resemble other
toolkits or operating systems.  The gtk-engines package contains
graphical engines for various GTK+ toolkit themes, including
Redmond95, and Metal (swing-like).

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{realname}-%{version}
%patch0 -p1 -b .clearlooks
%ifarch x86_64
%patch1 -p1 -b .cast
%endif

%build
%configure
# \
#    --enable-animation \
#    --enable-paranoia
#    --enable-lua
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(644, root, root, 755)
%doc COPYING README ChangeLog AUTHORS NEWS
%{_libdir}/gtk-2.0/2.10.0/engines/*
%{_datadir}/themes/Clearlooks/gtk-2.0/gtkrc
%{_datadir}/themes/Crux/gtk-2.0/gtkrc
%{_datadir}/themes/Industrial/gtk-2.0/gtkrc
%{_datadir}/themes/Mist/gtk-2.0/gtkrc
%{_datadir}/themes/Redmond/gtk-2.0/gtkrc
%{_datadir}/themes/ThinIce/gtk-2.0/gtkrc
%{_datadir}/gtk-engines
%{_datadir}/locale/*/*/*

%files devel
%defattr(644, root, root, 755)
%{_libdir}/pkgconfig/gtk-engines-2.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20.2-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.1-2m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Mon Jan  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.5-1m)
- update to 2.18.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.4-1m)
- update to 2.18.4

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-2m)
- add devel package

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Tue Apr 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.1-2m)
- update cast.patch to enable build on x86_64

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.4-1m)
- update to 2.17.4

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.3-1m)
- update to 2.17.3

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.2-1m)
- update to 2.17.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16.1-3m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16.1-2m)
- fix build on x86_64 (add patch1)

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Thu Aug 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-2m)
- fix %files (avoid conflict)

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.14.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.12.2-2m)
- %%NoSource -> NoSource

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2

* Sat Sep 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.0-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.4-1m)
- update to 2.9.4

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.9.3-1m)
- update to 2.9.3 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.2-2m)
- rename gtk-engines -> gtk2-engines

* Fri Nov 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.2-1m)
- update 2.8.2

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.1-1m)
- update 2.8.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.0-1m)
- update 2.8.0

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.10-1m)
- update to 2.6.10

* Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.9-1m)
- update to 2.6.9

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonta-linux.org>
- (2.6.8-1m)
- update to 2.6.8

* Mon Feb 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.7-2m)
- fix conflict directories

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.7-1m)
- update to 2.6.7

* Wed Dec  7 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.6-2m)
- http://bugzilla.gnome.org/show_bug.cgi?id=322886
- Not Win32, but gnome-theme-manager was crash.

* Sun Dec 4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.6-1m)
- update to 2.6.6
- GNOME 2.12.2 Desktop

* Mon Nov 21 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.5-1m)
- version 2.6.5

* Sun Jan 30 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-2m)
- remove duplicated files

* Sat Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.8 Desktop

* Tue Jun 22 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.2.0-9m)
- remove %%requires_eq gtk+

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.2.0-8m)
- adjustment BuildPreReq

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.2.0-7m)
- rebuild ageinst for gtk+-2.4.0

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.2.0-6m)
- rebuild ageinst for gtk+-2.2.4

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-5m)
- rebuild against for gtk+-2.2.2

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-4m)
- rebuild against for XFree86-4.3.0

* Wed Feb  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-3m)
- just rebuild for gtk+

* Mon Feb  3 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-2m)
- include *.pc

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Sun Jan  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.0-14m)
- just rebuild for gtk+

* Sun Dec 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.0-13m)
- just rebuild for gtk+

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.0-12m)
- add requires_eq gtk+

* Sun Nov 24 2002 HOSONO Hidetomo <h@h12o.org>
- (1.9.0-11m)
- change Source: because of change the URL of source
- define the macro "series"

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.0-10m)
- rebuild against for gtk+-2.1.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.9.0-9m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.0-8k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.0-6k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.0-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.0-2k)
- version 1.9.0
- gtk2 env

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.12-8k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- automake autoconf 1.5

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- nigittenu

* Sun Mar 25 2001 Shingo Akagaki <dora@kondara.org>
- version 0.12
- K2K

* Sat Feb 10 2001 Owen Taylor <otaylor@redhat.com>
- New, slightly improved version of Raleigh

* Sun Feb 04 2001 Owen Taylor <otaylor@redhat.com>
- Require as well as BuildPrereq a sufficiently new GTK+ package.

* Wed Jan 17 2001 Owen Taylor <otaylor@redhat.com>
- remove references to /home/raster from a couple of themes

* Tue Nov 21 2000 Owen Taylor <otaylor@redhat.com>
- Add 'Raleigh' theme

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.10-5k)
- fixed for FHS

* Fri Aug 11 2000 Jonathan Blandford <jrb@redhat.com>
- Up Epoch and release

* Thu Jul 13 2000 Owen Taylor <otaylor@redhat.com>
- Go back to real gtk-engines-0.10.tar.gz instead of hosed
  cvs snapshot that someone had inserted.

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Jun 19 2000 Dave Mason <dcm@redhat.com>
- Removed many themes to speed up control center and get rid of ugly themes

* Sat Jun 17 2000 Matt Wilson <msw@redhat.com>
- defattr files 644 and dirs 755, list attr of 755 for libraries explicitly
- use %%makeinstall
- remove spec file stupidism (defining docdir, using own prefix macro, rel, ver, etc)

* Tue May 16 2000 Dave Mason <dcm@redhat.com>
- fixed Tiger, RatsTheme, OldWood, and LCD themes as they had no gtk subdirectory

* Tue Feb 22 2000 Bill Nottingham <notting@redhat.com>
- sanitize various things (permissions, .xv thumbnails)

* Thu Feb 10 2000 Preston Brown <pbrown@redhat.com>
- remove backup files from package

* Tue Jan 25 2000 Owen Taylor <otaylor@redhat.com>
- Update to 0.10 (fixing problem with text in eventboxes
  becoming garbled)

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Sep 15 1999 Elliot Lee <sopwith@redhat.com>
- Misc fixes from DrMike suggestions

* Thu Sep 09 1999 Elliot Lee <sopwith@redhat.com>
- Update to 0.6, etc.

* Wed Apr 14 1999 Michael Fulbright <drmike@redhat.com>
- removed Odo (has issues)

* Fri Apr 9 1999 The Rasterman <raster@redhat.com>
- patched metal theme - fixed handlebox redraw.

* Wed Mar 31 1999 Michael Fulbright <drmike@redhat.com>
- removed some themes that were misbehaving

* Tue Mar 16 1999 Michael Fulbright <drmike@redhat.com>
- removed enlightened themes, seems to be defective

* Thu Mar 11 1999 Michael Fulbright <drmike@redhat.com>
- removed Default theme data, this comes with gtk+1 package

* Wed Mar 10 1999 Michael Fulbright <drmike@redhat.com>
- added extra gtk themes

* Thu Mar 04 1999 Michael Fulbright <drmike@redhat.com>
- version 0.5

* Fri Feb 12 1999 Michael Fulbright <drmike@redhat.com>
- version 0.4

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- version 0.3

* Mon Dec 18 1998 Michael Fulbright <drmike@redhat.com>
- version 0.2

* Wed Dec 16 1998 Michael Fulbright <drmike@redhat.com>
- rebuild because gtk+1 version changed

* Wed Dec 16 1998 Michael Fulbright <drmike@redhat.com>
- in preparation for GNOME freeze

* Fri Nov 20 1998 Michael Fulbright <drmike@redhat.com>
- First try at a spec file
