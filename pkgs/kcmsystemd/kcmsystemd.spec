%global        momorel 2
%global        qtver 4.8.5
%global        kdever 4.11.4
%global        kdelibsrel 1m
%global boost_version 1.55.0
Summary:       Systemd control module for KDE.
Name:          kcmsystemd
Version:       0.5.0
Release:       %{momorel}m%{?dist}
License:       GPLv2
Group:         Applications/System
URL:           https://github.com/rthomsen/kcmsystemd
Source0:       http://kde-apps.org/CONTENT/content-files/161871-%{name}-%{version}.tar.gz
NoSource:      0
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:      kdelibs >= %{kdever}-%{kdelibsrel}
Requires:      kdebase-workspace >= %{kdever}
Requires:      systemd
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdebase-workspace-devel >= %{kdever}
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: ImageMagick-devel >= 6.8.0.10
BuildRequires: PackageKit-qt-devel >= 0.8.1
BuildRequires: boost-devel >= %{boost_version}
%description
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc LICENSE README.md
%{_kde4_libdir}/kde4/kcm_systemd.so
%{_kde4_libexecdir}/kcmsystemdhelper
%{_kde4_datadir}/kde4/services/kcm_systemd.desktop
%{_sysconfdir}/dbus-1/system.d/org.kde.kcontrol.kcmsystemd.conf
%{_datadir}/dbus-1/system-services/org.kde.kcontrol.kcmsystemd.service
%{_datadir}/polkit-1/actions/org.kde.kcontrol.kcmsystemd.policy

%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-2m)
- rebuild against boost-1.55.0

* Sun Dec 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Mon Dec  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Wed Dec  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Fri Nov 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Thu Nov 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-m)
- update to 0.2.0

* Tue Nov 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-1m)
- initial build for Momonga Linux
