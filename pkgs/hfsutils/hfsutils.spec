%global momorel 14

Summary: Tools for reading and writing Macintosh HFS volumes.
Name: hfsutils
Version: 3.2.6  
Release: %{momorel}m%{?dist}
Group: Applications/File
License: GPLv2+
Source: ftp://ftp.mars.org/pub/hfs/%{name}-%{version}.tar.gz 
Patch0: hfsutils-3.2.6-errno.patch
Patch1: hfsutils-3.2.6-largefile.patch
URL: http://www.mars.org/home/rob/proj/hfs/ 
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libXft-devel tcl-devel tk-devel
BuildRequires: libxcb-devel >= 1.2
Requires: tcl

%description
HFS (Hierarchical File System) is the native volume format found on
modern Macintosh computers.  Hfsutils provides utilities for accessing
HFS volumes from Linux and UNIX systems.  Hfsutils contains several
command-line programs which are comparable to mtools.

%package x11
Summary: A Tk-based front end for browsing and copying files
Group: Applications/File 
Requires: %{name} = %{version}-%{release}

%description x11
The hfsutils-x11 package includes a Tk-based front end for browsing
and copying files, and a Tcl package and interface for scriptable access
to volumes.  A C library for low-level access to volumes is included in the
hfsutils-devel package.

%package devel
Summary: A C library for reading and writing Macintosh HFS volumes
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The hfsutils-devel package provides a C library for low-level access
to Macintosh volumes. HFS (Hierarchical File System) is the native
volume format found on modern Macintosh computers.  The C library can
be linked with other programs to allow them to manipulate Macintosh
files in their native format.  Other HFS accessing utilities, which
are comparable to mtools, are included in the hfsutils package.

%prep
%setup -q 

%patch0 -p1
%patch1 -p1

%build
%configure --with-tcl=%{_libdir}  --with-tk=%{_libdir}
%make
%make hfsck/hfsck

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_includedir}
mkdir -p %{buildroot}%{_libdir}
make	BINDEST=%{buildroot}%{_bindir} \
	LIBDEST=%{buildroot}%{_libdir} \
	INCDEST=%{buildroot}%{_includedir} \
	MANDEST=%{buildroot}%{_mandir} \
	install install_lib

# mv %{buildroot}/%{_libdir}/libhfs.a %{buildroot}/%{_libdir}/libhfsutil.a
install -m0755 hfsck/hfsck %{buildroot}/%{_bindir}
ln -sf hfsck %{buildroot}/%{_bindir}/fsck.hfs

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES COPYING COPYRIGHT CREDITS INSTALL README TODO
%config
%{_bindir}/hattrib
%{_bindir}/hcd
%{_bindir}/hcopy
%{_bindir}/hdel
%{_bindir}/hdir
%{_bindir}/hformat
%{_bindir}/hfs
%{_bindir}/hfssh
%{_bindir}/hls
%{_bindir}/hmkdir
%{_bindir}/hmount
%{_bindir}/hpwd
%{_bindir}/hrename
%{_bindir}/hrmdir
%{_bindir}/humount
%{_bindir}/hvol
%{_bindir}/hfsck
%{_bindir}/fsck.hfs
%{_mandir}/man1/hattrib.1*
%{_mandir}/man1/hcd.1*
%{_mandir}/man1/hcopy.1*
%{_mandir}/man1/hdel.1*
%{_mandir}/man1/hdir.1*
%{_mandir}/man1/hformat.1*
%{_mandir}/man1/hfs.1*
%{_mandir}/man1/hfssh.1*
%{_mandir}/man1/hfsutils.1*
%{_mandir}/man1/hls.1*
%{_mandir}/man1/hmkdir.1*
%{_mandir}/man1/hmount.1*
%{_mandir}/man1/hpwd.1*
%{_mandir}/man1/hrename.1*
%{_mandir}/man1/hrmdir.1*
%{_mandir}/man1/humount.1*
%{_mandir}/man1/hvol.1*

# We don't want this.
%exclude %{_bindir}/hfssh

%files x11
%defattr(-,root,root)
%{_bindir}/xhfs
%{_mandir}/man1/xhfs.1*

%files devel
%defattr(-,root,root)
%{_libdir}/libhfs.a
%{_libdir}/librsrc.a
%{_includedir}/hfs.h
%{_includedir}/rsrc.h

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.6-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.6-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.6-12m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.6-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Feb 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.6-10m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.6-9m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.6-8m)
- sync fedora 3.2.6-15

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.6-7m)
- update Patch1 for fuzz=0
- License: GPLv2+

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.6-6m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.6-5m)
- rebuild against gcc43

* Fri May 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.6-4m)
- modify spec file

* Sat Mar 12 2005 mutecat <mutecat@momonga-linux.org>
- (3.2.6-3m)
- add hfsutils-3.2.6-largefile.patch from fc.

* Mon Jan 24 2005 mutecat <mutecat@momonga-linux.org>
- (3.2.6-2m)
- rebuild against
- rename libhfs.a -> libhfsutul.a

* Fri Jul 02 2004 mutecat <mutecat@momonga-linux.org>
- (3.2.6-1m)
- momonganized
- import from fc.

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Apr 19 2004 David Woodhouse  <dwmw2@redhat.com> 3.2.6-3
- BuildRequires tk-devel

* Sun Apr 11 2004 David Woodhouse  <dwmw2@redhat.com> 3.2.6-2.1
- Adjust configure invocation to find tcl in %%{_libdir}

* Sun Apr 11 2004 David Woodhouse  <dwmw2@redhat.com> 3.2.6-2
- Require tcl

* Fri Apr 09 2004 David Woodhouse  <dwmw2@redhat.com> 3.2.6-1
- Fix BuildRequires, include errno.h in tclhfs.c, use %%{configure}

* Wed Oct 02 2002 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- Anubis rebuild

* Fri Mar 30 2001 Dan Burcaw <dburcaw@terrasoftsolutions.com>
- split xhfs into its own package

* Fri Feb 11 2000 Tim Powers <timp@redhat.com>
- gzip manpages, strip binaries

* Thu Jul 15 1999 Tim Powers <timp@redhat.com>
- added %defattr
- rebuilt for 6.1

* Thu Apr 15 1999 Michael Maher <mike@redhat.com>
- built package for 6.0
- updated source

* Thu Aug 20 1998 Michael Maher <mike@redhat.com>
- built package 

