%global momorel 4

Name: cadaver
Version: 0.23.3
Release: %{momorel}m%{?dist}
Summary: Command-line WebDAV client
License: GPL
Group: Applications/Internet
Source0: http://www.webdav.org/cadaver/%{name}-%{version}.tar.gz 
NoSource: 0
URL: http://www.webdav.org/cadaver/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: neon-devel >= 0.29.3
BuildRequires: readline-devel >= 5.0, zlib-devel, openssl-devel, libxml2-devel

%description
cadaver is a command-line WebDAV client, with support for file upload, 
download, on-screen display, in-place editing, namespace operations
(move/copy), collection creation and deletion, property manipulation, 
and resource locking.

%prep
%setup -q
%__chmod go+r [A-Z]*

%build
%configure --with-neon=%{_prefix}
%make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%doc NEWS FAQ THANKS TODO COPYING README ChangeLog INTEROP
%{_mandir}/*/*
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23.3-2m)
- full rebuild for mo7 release

* Mon May 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23.3-1m)
- update to 0.23.3 and rebuild against neon-0.29.3

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23.2-6m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23.2-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23.2-2m)
- rebuild against gcc43

* Sun Mar  9 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23.2-1m)
- update 0.23.2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22.5-2m)
- %%NoSource -> NoSource

* Sat Jun  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.22.5-1m)
- [SECURITY] CVE-2007-0157
- update 0.22.5

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.22.3-3m)
- rebuild against readline-5.0

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.22.3-2m)
- rebuild against neon-0.25.5-3m

* Wed Jan  4 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (0.22.3-1m)
- updated to 0.22.3 (to support neon-0.25.4)
- rebuild against neon-0.25.4-1m

* Fri Dec  3 2004 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (0.22.2-1m)
- initial import for momonga

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed May 12 2004 Joe Orton <jorton@redhat.com> 0.22.1-2
- build as PIE

* Tue Apr 20 2004 Joe Orton <jorton@redhat.com> 0.22.1-1
- update to 0.22.1

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Oct  3 2003 Joe Orton <jorton@redhat.com> 0.22.0-1
- update to 0.22.0; use system neon

* Tue Jul 22 2003 Nalin Dahyabhai <nalin@redhat.com> 0.21.0-2
- rebuild

* Mon Jul 21 2003 Joe Orton <jorton@redhat.com> 0.21.0-1
- update to 0.21.0

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Tue Jan  7 2003 Nalin Dahyabhai <nalin@redhat.com> 0.20.5-5
- rebuild

* Fri Nov 22 2002 Joe Orton <jorton@redhat.com> 0.20.5-4
- force use of bundled neon (#78260)

* Mon Nov  4 2002 Joe Orton <jorton@redhat.com> 0.20.5-3
- rebuild in new environment

* Fri Aug 30 2002 Joe Orton <jorton@redhat.com> 0.20.5-2
- update to 0.20.5; many bug fixes, minor security-related
 fixes, much improved SSL support, a few new features.

* Thu Aug 22 2002 Joe Orton <jorton@redhat.com> 0.20.4-1
- add --with-force-ssl

* Wed May  1 2002 Joe Orton <joe@manyfish.co.uk>
- add man page

* Sat Jan 19 2002 Joe Orton <joe@manyfish.co.uk>
- updated description

* Mon Nov 19 2001 Joe Orton <joe@manyfish.co.uk>
- Merge changes from Nalin Dahyabhai <nalin@redhat.com>.

* Fri Feb 11 2000 Joe Orton <joe@orton.demon.co.uk>
- Text descriptions modified

* Thu Feb 10 2000 Lee Mallabone <lee0@callnetuk.com>
- Initial creation.
