%global momorel 19
%global qtver 4.8.6

Name:       ibus-qt
Version:    1.3.1
Release:    %{momorel}m%{?dist}
Summary:    Qt IBus library and Qt input method plugin
License:    GPLv2+
Group:      System Environment/Libraries
URL:        http://code.google.com/p/ibus/
Source0:    http://ibus.googlecode.com/files/%{name}-%{version}-Source.tar.gz
NoSource:   0

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:   ibus >= 1.2
Requires:   qt-x11 >= %{qtver}
BuildRequires:  cmake
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  dbus-devel >= 1.2
BuildRequires:  ibus-devel >= 1.3.99
BuildRequires:  libicu-devel >= 52

%description
Qt IBus library and Qt input method plugin.

%package devel
Summary:    Development tools for ibus qt
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description devel
The ibus-qt-devel package contains the header files for ibus qt library.

%prep
%setup -q -n %{name}-%{version}-Source

%build
PATH="%{_qt4_bindir}:$PATH" %cmake \
    -DCMAKE_INSTALL_PREFIX=%{_usr} \
    -DLIBDIR=%{_libdir}
make \
    VERBOSE=1 \
    C_DEFINES="$RPM_OPT_FLAGS" \
    CXX_DEFINES="$RPM_OPT_FLAGS" \
    %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# %find_lang %{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
# -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS README INSTALL
%{_libdir}/libibus-qt.so.*
%{_libdir}/qt4/plugins/inputmethods/libqtim-ibus.so

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libibus-qt.so
#{_docdir}/%{name}-docs-%{version}
#{_mandir}/man3/*

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-19m)
- rebuild against qt-4.8.6
- ibus-qt links qt libraries dynamically, it does not depend on the qt version closely

* Thu Mar 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-18m)
- rebuild against icu52

* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-17m)
- rebuild against qt-4.8.5

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-16m)
- rebuild against qt-4.8.4

* Mon Oct  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-15m)
- rebuild against qt-4.8.3

* Thu Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-14m)
- rebuild against qt-4.8.2

* Wed Apr  4 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-13m)
- rebuild against qt-4.8.1

* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-12m)
- rebuild against qt-4.8.0

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-11m)
- rebuild against qt-4.7.4

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-10m)
- rebuild against icu-4.6

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-9m)
- rebuild against ibus-1.3.99

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-8m)
- rebuild against qt-4.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-7m)
- rebuild for new GCC 4.6

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-6m)
- rebuild against qt-4.7.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-5m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-4m)
- specify PATH for Qt4

* Fri Nov 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-3m)
- set QT_QMAKE_EXECUTABLE to enable build

* Fri Nov 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-2m)
- rebuild against qt-4.7.1

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-5m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-4m)
- rebuild against qt-4.6.3-1m

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.3.0-3m)
- rebuild against icu-4.2.1

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-2m)
- rebuild against qt-4.7.0

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0.20091217-1m)
- update to 1.2.0.20091217
- rebuild against qt-4.6.2

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0.20091215-2m)
- rebuild against qt-4.6.1

* Thu Dec 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0.20091215-1m)
- update to 1.2.0.20091215

* Mon Dec  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0.20091206-1m)
- update to 1.2.0.20091206

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0.20091014-3m)
- rebuild against qt-4.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20091014-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0.20091014-1m)
- update to 1.2.0.20091014

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090822-1m)
- update to 1.2.0.20090822

* Sun Aug 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090728-1m)
- import from Rawhide

* Mon Jul 27 2009 Peng Huang <shawn.p.huang@gmail.com> - 1.2.0.20090728-1
- The first version.
