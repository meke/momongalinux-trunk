%global momorel 1 
%global srcname remmina-xfce

Name:           xfce4-remmina-plugin
Version:        0.8.1
Release:        %{momorel}m%{?dist}
Summary:        Xfce panel plugin for Remmina remote desktop client

Group:          Applications/Internet
License:        GPLv2+
URL:            http://remmina.sourceforge.net/
Source0:        http://downloads.sourceforge.net/remmina/%{srcname}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  avahi-devel
BuildRequires:  xfce4-panel-devel
BuildRequires:  gettext intltool
Requires:       xfce4-panel, remmina >= %{version}
# for easy installation
Provides:       %{srcname} = %{version}

%description
Remmina is a remote desktop client written in GTK+, aiming to be useful for 
system administrators and travelers, who need to work with lots of remote 
computers in front of either large monitors or tiny netbooks. Remmina supports 
multiple network protocols in an integrated and consistent user interface. 
Currently RDP, VNC, XDMCP and SSH are supported.

This package includes the Xfce panel plugin for remmina.


%prep
%setup -q -n %{srcname}-%{version}


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_libdir}/xfce4/panel-plugins/*.la
%find_lang %{srcname}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{srcname}.lang
%defattr(-,root,root,-)
# FIXME add NEWS if not empty
%doc AUTHORS ChangeLog COPYING README
%{_libdir}/xfce4/panel-plugins/libremmina-xfce-plugin.so
%{_datadir}/xfce4/panel-plugins/*.desktop


%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1
- build against xfce4-4.11.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.3-6m)
- rebuild against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.3-5m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.3-1m)
- import from Fedora to Momonga
 
* Wed May 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.7.3-1
- Update to 0.7.3

* Wed Feb 24 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.7.2-1
- Update to 0.7.2

* Thu Feb 04 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.7.1-1
- Initial package

