# Generated from ffi-1.0.11.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname ffi

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Ruby-FFI is a ruby extension for programmatically loading dynamic libraries, binding functions within them, and calling those functions from Ruby code
Name: rubygem-%{gemname}
Version: 1.0.11
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://wiki.github.com/ffi/ffi
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Ruby-FFI is a ruby extension for programmatically loading dynamic
libraries, binding functions within them, and calling those functions
from Ruby code. Moreover, a Ruby-FFI extension works without changes
on Ruby and JRuby. Discover why should you write your next extension
using Ruby-FFI here[http://wiki.github.com/ffi/ffi/why-use-ffi].


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            -V \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/lib/ffi/platform/i386-darwin/types.conf
%doc %{geminstdir}/lib/ffi/platform/i386-freebsd/types.conf
%doc %{geminstdir}/lib/ffi/platform/i386-linux/types.conf
%doc %{geminstdir}/lib/ffi/platform/i386-netbsd/types.conf
%doc %{geminstdir}/lib/ffi/platform/i386-openbsd/types.conf
%doc %{geminstdir}/lib/ffi/platform/i386-solaris/types.conf
%doc %{geminstdir}/lib/ffi/platform/i386-windows/types.conf
%doc %{geminstdir}/lib/ffi/platform/powerpc-aix/types.conf
%doc %{geminstdir}/lib/ffi/platform/powerpc-darwin/types.conf
%doc %{geminstdir}/lib/ffi/platform/sparc-solaris/types.conf
%doc %{geminstdir}/lib/ffi/platform/sparcv9-solaris/types.conf
%doc %{geminstdir}/lib/ffi/platform/x86_64-darwin/types.conf
%doc %{geminstdir}/lib/ffi/platform/x86_64-freebsd/types.conf
%doc %{geminstdir}/lib/ffi/platform/x86_64-linux/types.conf
%doc %{geminstdir}/lib/ffi/platform/x86_64-netbsd/types.conf
%doc %{geminstdir}/lib/ffi/platform/x86_64-openbsd/types.conf
%doc %{geminstdir}/lib/ffi/platform/x86_64-solaris/types.conf
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.11-1m)
- update 1.0.11

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.10-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.10-1m)
- update 1.0.10

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.9-1m)
- udpate 1.0.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.3-2m)
- rebuild against ruby-1.9.2

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-1m)
- update 0.6.3

* Thu Dec 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.4-1m)
- update 0.5.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-1m)
- update 0.5.1

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.0-1m)
- update 0.5.0

* Mon May 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.5-1m)
- update 0.3.5

* Mon Mar 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.2-1m)
- update 0.3.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-2m)
- rebuild against rpm-4.6

* Sun Dec 21 2008  <meke@momonga-linux.org>
- (0.2.0-1m)
- Initial package for Momonga Linux

