%global momorel 4

%define dicname stardict-english-czech
Name: stardict-dic-cs_CZ
Summary: Czech dictionaries for StarDict
Version: 20100801
Release: %{momorel}m%{?dist}
Group: Applications/Text
License: GFDL

URL: http://cihar.com/software/slovnik/
Source0: ftp://dl.cihar.com/slovnik/stable/%{dicname}-%{version}.tar.gz

BuildRoot: %{_tmppath}/%{dicname}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

Requires: stardict >= 2

%description
Czech-English and English-Czech translation dictionaries for StarDict, a
GUI-based dictionary software.

%prep
%setup -q -c -n %{dicname}-%{version}

%build

%install
rm -rf ${RPM_BUILD_ROOT}
install -m 0755 -p -d ${RPM_BUILD_ROOT}%{_datadir}/stardict/dic
install -m 0644 -p  %{dicname}-%{version}/cz* ${RPM_BUILD_ROOT}%{_datadir}/stardict/dic/
install -m 0644 -p  %{dicname}-%{version}/en* ${RPM_BUILD_ROOT}%{_datadir}/stardict/dic/

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root,-)
%doc %{dicname}-%{version}/README
%{_datadir}/stardict/dic/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100801-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100801-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100801-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100801-1m)
- update to 20100801

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081201-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081201-1m)
- import from Fedora 11

* Mon Feb 9 2009 Petr Sklenar <psklenar@redhat.com> - 20081201-2
- editing specfile - name and description

* Mon Jan 26 2009 Petr Sklenar <psklenar@redhat.com> - 20081201-1
- Initial build for Fedora

