%global momorel 8
%global qtver 4.7.0
%global kdever 4.5.1
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global content_id 50971

Summary: A KDE version of ssh-askpass with KWallet support
Name: ksshaskpass
Version: 0.5.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Internet
URL: http://www.kde-apps.org/content/show.php?content=50971
Source0: http://www.kde-apps.org/CONTENT/content-files/%{content_id}-%{name}-%{version}.tar.gz
NoSource: 0
Source1: hi16-app-%{name}.png
Source2: hi32-app-%{name}.png
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: openssh-clients
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: lcms-devel
BuildRequires: libart_lgpl-devel
BuildRequires: libidn-devel
BuildRequires: libutempter-devel

%description
A KDE version of ssh-askpass with KWallet support.

%prep
%setup -q

%patch0 -p1 -b .desktop

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# move and install ksshaskpass
mv %{buildroot}%{_bindir}/%{name} %{buildroot}%{_bindir}/%{name}.bin

cat > %{buildroot}%{_bindir}/%{name} << EOF
#!/bin/sh
SSH_ASKPASS=%{_bindir}/%{name}.bin
export SSH_ASKPASS
exec ssh-add
EOF

chmod 755 %{buildroot}%{_bindir}/%{name}

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32}/apps
install -m 644 %{SOURCE1} %{buildroot}%{_kde4_iconsdir}/hicolor/16x16/apps/%{name}.png
install -m 644 %{SOURCE2} %{buildroot}%{_kde4_iconsdir}/hicolor/32x32/apps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_kde4_datadir}/applications/kde4/ksshaskpass.desktop 

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING ChangeLog INSTALL
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/%{name}.bin
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_mandir}/man1/%{name}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-7m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-5m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-4m)
- fix up package to work

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-3m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-2m)
- touch up spec file

* Sat Jan  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against rpm-4.6

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Mon Nov 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-2m)
- remove ksshaskpass.desktop
- use macros.kde4
- License: GPLv2

* Mon Nov 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Thu Jun 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-2m)
- use Qt threaded (default) libraries
- fix permission of %%{_sysconfdir}/kde/env/ksshaskpass.sh
- re-install icons and desktop file

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1
- change source URI

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-2m)
- %%NoSource -> NoSource

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-1m)
- initial package for Momonga Linux
