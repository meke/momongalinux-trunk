%global         momorel 1
%global         repoid 58488
%global         xyzsh_ver 1.4.2

Name:           mfiler4
Version:        1.1.8
Release:        %{momorel}m%{?dist}
Summary:        2 pain file manager with a embedded shell
Group:          Applications/Editors
License:        MIT
URL:            http://sourceforge.jp/projects/mfiler4/
Source0:        http://dl.sourceforge.jp/%{name}/%{repoid}/%{name}-%{version}.tgz
NoSource:       0
BuildRequires:  cmigemo-devel
BuildRequires:  ncurses-devel
BuildRequires:  oniguruma-devel
BuildRequires:  readline-devel
BuildRequires:  xyzsh-devel >= %{xyzsh_ver}
Requires:       xyzsh >= %{xyzsh_ver}

%description
mfiler4 is a 2pain file manager with a embedded shell.

%prep
%setup -q

# Kill -O3
sed -i.optflags \
	-e 's|-O3|-O2|' \
	configure

# Kill -Werror
sed -i.werror \
	-e 's|-Werror||' \
	configure Makefile.in

# Change docdir
sed -i.docdir \
	-e '/^CFLAGS=.*DATAROOTDIR=/s|doc/mfiler4/|doc/xyzsh-%{version}/|' \
	configure

# Don't strip binary
# Keep timestamp
sed -i.bak \
	-e 's|install -m |install -p -m |' \
	-e 's|install -s |install |' \
	Makefile.in

# Umm...
sed -i.inst \
	-e 's|USAGE.ja |USAGE.ja.txt |' \
	-e 's|USAGE |USAGE.txt |' \
	Makefile.in

%build
%configure \
    --bindir=%{_libexecdir}/%{name}/ \
    --with-migemo \
    --with-system-migemodir=%{_datadir}/cmigemo/

make %{?_smp_mflags} -k \
    CC="gcc %optflags" \
    docdir=%{_datadir}/doc/%{name}-%{version}

%install
make install \
    DESTDIR=%{buildroot} \
    docdir=%{_datadir}/doc/%{name}-%{version}

mkdir %{buildroot}%{_bindir}
cat > %{buildroot}%{_bindir}/%{name} <<EOF
#!/bin/bash
export PATH=%{_libexecdir}/%{name}:\${PATH}
exec %{_libexecdir}/%{name}/%{name} "\$@"
EOF
chmod 0755 %{buildroot}%{_bindir}/%{name}

%clean
rm -rf %{buildroot}

%files
%doc AUTHORS CHANGELOG LICENSE README* USAGE*
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/*.xyzsh
%{_bindir}/%{name}
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/%{name}
%{_libexecdir}/%{name}/mattr
%{_mandir}/man1/%{name}.1*

%changelog
* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.8-1m)
- update to 1.1.8

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Sun Feb 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Sat Jan 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.8-1m)
- initial build for Momonga Linux
