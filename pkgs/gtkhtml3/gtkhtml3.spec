%global	momorel 1
%define gtkhtml_major 4.0
%define editor_major 4.0

### Abstract ###

Name: gtkhtml3
Version: 4.6.1
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
Summary: GtkHTML library
License: LGPLv2+ and GPLv2
URL: http://projects.gnome.org/evolution/
Source: http://download.gnome.org/sources/gtkhtml/4.6/gtkhtml-%{version}.tar.xz
NoSource: 0
### Build Dependencies ###

BuildRequires: enchant-devel
BuildRequires: gail-devel
BuildRequires: gettext
BuildRequires: gnome-common
BuildRequires: gnome-icon-theme
BuildRequires: gsettings-desktop-schemas-devel
BuildRequires: gtk3-devel
BuildRequires: intltool
BuildRequires: iso-codes
BuildRequires: libtool
Obsoletes: gtkhtml4

%description
GtkHTML is a lightweight HTML rendering/printing/editing engine.
It was originally based on KHTMLW, but is now being developed
independently of it.

%package devel
Group: Development/Libraries
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}
Obsoletes: gtkhtml4-devel

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n gtkhtml-%{version}
#%patch01 -p1 -b .no-g-thread-init

%build
autoreconf -fi
%configure --disable-static --disable-maintainer-mode
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS"

%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

%find_lang gtkhtml-%{gtkhtml_major}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f gtkhtml-%{gtkhtml_major}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS README COPYING TODO
%{_bindir}/gtkhtml-editor-test
%{_libdir}/libgtkhtml-%{gtkhtml_major}.so.*
%{_libdir}/libgtkhtml-editor-%{editor_major}.so.*
%{_datadir}/gtkhtml-%{gtkhtml_major}

%files devel
%defattr(-,root,root,-)
%{_includedir}/libgtkhtml-%{gtkhtml_major}
%{_libdir}/libgtkhtml-%{gtkhtml_major}.so
%{_libdir}/libgtkhtml-editor-%{editor_major}.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.6.1-1m)
- update to 4.6.1

* Sun Jul  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.3-2m)
- add Obsoletes: gtkhtml4

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.3-1m)
- update to 4.5.3
- reimport from fedora's gtkhtml3

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.32.2-3m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.32.2-2m)
- rebuild for new GCC 4.6

* Wed Feb  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.32.2-1m)
- update to 3.32.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.32.1-1m)
- update to 3.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.32.0-1m)
- update to 3.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.30.2-2m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.30.2-1m)
- update to 3.30.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.30.1-1m)
- update to 3.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.30.0-1m)
- update to 3.30.0

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.29.91-2m)
- add patch for gtk-2.20 by gengtk220patch

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.29.91-1m)
- update to 3.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.29.90-1m)
- update to 3.29.90
- delete patch1

* Sat Feb 13 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.28.2-3m)
- add patch for gtk-2.20

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.28.2-2m)
- delete __libtoolize hack

* Mon Dec 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.28.2-1m)
- update to 3.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.28.1-1m)
- update to 3.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.26.2-1m)
- update to 3.26.2

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.26.1.1-2m)
- define __libtoolize

* Tue Apr 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.26.1.1-1m)
- update to 3.26.1.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.26.0-1m)
- update to 3.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.25.92-1m)
- update to 3.25.92

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.25.91-1m)
- update to 3.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.25.90-1m)
- update to 3.25.90

* Sat Jan 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.24.4-1m)
- update to 3.24.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.24.3-1m)
- update to 3.24.3

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.24.2-1m)
- update to 3.24.2

* Fri Nov  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.24.1.1-1m)
- update to 3.24.1.1

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.24.1-1m)
- update to 3.24.1

* Wed Oct  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.24.0-1m)
- update to 3.24.0

* Sun Sep 14 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.18.3-1m)
- add gtk-2.14 patch

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.18.3-1m)
- update to 3.18.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.18.2-1m)
- update to 3.18.2

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.18.1-1m)
- update to 3.18.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.18.0-2m)
- rebuild against gcc43

* Mon Jan  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.18.0-1m)
- update to 3.18.0

* Mon Nov 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.16.2-1m)
- update to 3.16.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.16.1-1m)
- update to 3.16.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.16.0-1m)
- update to 3.16.0

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.14.3-1m)
- update to 3.14.3

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.14.2-1m)
- update to 3.14.2

* Wed Apr 18 2007 Yohsuke Ooi <futoshi@momonga-linux.org>
- (3.14.1-1m)
- update 3.14.1

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.14.0-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.14.0-1m)
- update to 3.14.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.13.92-1m)
- update to 3.13.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.13.91-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.13.91-1m)
- update to 3.13.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.12.2-1m)
- update to 3.12.2
- rename gtkhtml -> gtkhtml3

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.12.1-1m)
- update to 3.12.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.12.0-1m)
- update to 3.12.0

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.10.3-1m)
- update to 3.10.3

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.10.2-1m)
- update to 3.10.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.10.1-1m)
- update to 3.10.1

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.10.0-1m)
- update to 3.10.0

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.8.2-1m)
- update to 3.8.2

* Tue Nov 22 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.8.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Fri Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.2.4-1m)
- import from Fedora
- version 3.2.4

* Tue Sep 21 2004 Owen Taylor <otaylor@redhat.com> - 3.3.2-2
- Add a patch to fix input method commit issues (#Bug 130751)

* Thu Sep 16 2004 Owen Taylor <otaylor@redhat.com> - 3.3.2-1
- Upgrade to 3.3.2 (Fixes tab display, #132208, ordering
  issues with IM preedit #130751, Leon Ho)

* Fri Sep  3 2004 Owen Taylor <otaylor@redhat.com> - 3.3.1-1
- Upgrade to 3.3.1, includes GtkFileChoose support (#130039)

* Fri Aug 13 2004 Tim Waugh <twaugh@redhat.com> - 3.3.0-3
- Prevent a crash (bug #129844).

* Mon Aug  9 2004 Owen Taylor <otaylor@redhat.com> - 3.3.0-2
- Fix a problem where preformatted text wrapped at column 0

* Wed Aug  4 2004 Owen Taylor <otaylor@redhat.com> - 3.3.0-1
- Upgrade to 3.3.0 (gnome-2-8-devel branch)

* Mon Jul 26 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Thu Jul 22 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Thu Jul 22 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Thu Jul 22 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Tue Jul 20 2004 David Malcolm <dmalcolm@redhat.com> - 3.1.18-1
- 3.1.18

* Thu Jul  8 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Wed Jul  7 2004 David Malcolm <dmalcolm@redhat.com>
- rebuilt

* Tue Jul  6 2004 David Malcolm <dmalcolm@redhat.com> - 3.1.17-1
- 3.1.17

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jun  7 2004 David Malcolm <dmalcolm@redhat.com> - 3.1.16-2
- rebuilt

* Fri Jun  4 2004 David Malcolm <dmalcolm@redhat.com> - 3.1.16-1
- 3.1.16

* Fri May 21 2004 David Malcolm <dmalcolm@redhat.com> - 3.1.14-2
- rebuilt

* Thu May 20 2004 David Malcolm <dmalcolm@redhat.com> - 3.1.14-1
- 3.1.14

* Tue Apr 20 2004 David Malcolm <dmalcolm@redhat.com> - 3.1.12-1
- 3.1.12

* Wed Mar 10 2004 Jeremy Katz <katzj@redhat.com> - 3.1.9-1
- 3.1.9

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Feb 17 2004 Jeremy Katz <katzj@redhat.com> - 3.1.8-1
- 3.1.8

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jan 27 2004 Jeremy Katz <katzj@redhat.com> - 3.1.7-1
- 3.1.7

* Wed Jan 14 2004 Jeremy Katz <katzj@redhat.com> 3.1.6-0
- update to 3.1.6

* Sat Jan  3 2004 Jeremy Katz <katzj@redhat.com> 3.1.5-0
- update to 3.1.5

* Thu Sep 25 2003 Jeremy Katz <katzj@redhat.com> 3.0.9-5
- rebuild

* Thu Sep 25 2003 Jeremy Katz <katzj@redhat.com> 3.0.9-4
- add patch for XIM (#91481)

* Tue Sep 23 2003 Jeremy Katz <katzj@redhat.com> 3.0.9-3
- rebuild

* Fri Sep 19 2003 Jeremy Katz <katzj@redhat.com> 3.0.9-2
- add patch to fix crash on ia64

* Fri Sep 19 2003 Jeremy Katz <katzj@redhat.com> 3.0.9-1
- 3.0.9

* Mon Sep  8 2003 Jeremy Katz <katzj@redhat.com> 
- add some buildrequires (#103901)

* Thu Sep  4 2003 Jeremy Katz <katzj@redhat.com> 3.0.8-3
- patch from upstream copy for new libbonobo oddities (#103730)

* Mon Aug  4 2003 Jeremy Katz <katzj@redhat.com> 3.0.8-1
- 3.0.8

* Thu Jul 10 2003 Jeremy Katz <katzj@redhat.com> 3.0.7-1
- 3.0.7

* Wed Jun 11 2003 Jeremy Katz <katzj@redhat.com> 
- add some buildrequires (#97181)

* Tue Jun 10 2003 Jeremy Katz <katzj@redhat.com> 3.0.5-2
- rebuild

* Mon Jun  9 2003 Jeremy Katz <katzj@redhat.com> 3.0.5-1
- 3.0.5

* Wed Jun 5 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jun  5 2003 Jeremy Katz <katzj@redhat.com> 3.0.4-3
- rebuild

* Mon May 26 2003 Jeremy Katz <katzj@redhat.com> 3.0.4-2
- rebuild to fix deps

* Sun May 25 2003 Jeremy Katz <katzj@redhat.com> 3.0.4-1
- 3.0.4

* Tue May  6 2003 Jeremy Katz <katzj@redhat.com> 3.0.3-1
- 3.0.3

* Wed Apr 16 2003 Jeremy Katz <katzj@redhat.com> 3.0.2-2
- libtool's revenge

* Wed Apr 16 2003 Jeremy Katz <katzj@redhat.com> 3.0.2-1
- update to 3.0.2

* Sun Apr  6 2003 Jeremy Katz <katzj@redhat.com> 1.1.9-1
- update to 1.1.9

* Mon Mar 24 2003 Jeremy Katz <katzj@redhat.com> 1.1.8-6
- rebuild for new gal

* Mon Feb 24 2003 Elliot Lee <sopwith@redhat.com> 1.1.8-5
- debuginfo rebuild

* Thu Feb 20 2003 Jeremy Katz <katzj@redhat.com> 1.1.8-4
- gtkhtml capplet doesn't need to be in the menus; it's configurable 
  from within evolution

* Mon Feb 10 2003 Akira TAGOH <tagoh@redhat.com> 1.1.8-3
- don't use fontset as default. (#83899)
- improve the default font for CJK.

* Sat Feb  8 2003 Akira TAGOH <tagoh@redhat.com> 1.1.8-2
- hack to modify po dynamically to add currect XLFD for CJK.
- re-enable patches.

* Fri Feb  7 2003 Jeremy Katz <katzj@redhat.com> 1.1.8-1
- 1.1.8
- disable tagoh's patch for now.  it's not applied upstream and ends up 
  backing out some translation changes

* Fri Feb  7 2003 Akira TAGOH <tagoh@redhat.com> 1.1.7-4
- gtkhtml-1.1.7-fixfont.patch: applied to allow fontset by default.
- gtkhtml-po.tar.bz2: to changes default display/print fonts for CJK.
  perhaps it should be removed when the upstream will releases the next version.
- gtkhtml-1.1.7-domain.patch: define GNOME_EXPLICIT_TRANSLATION_DOMAIN as gtkhtml-1.1.

* Wed Feb  5 2003 Bill Nottingham <notting@redhat.com> 1.1.7-2
- fix some spewage to stdout/stderr

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Fri Dec 13 2002 Jeremy Katz <katzj@redhat.com> 1.1.7-1
- update to 1.1.7

* Tue Nov 12 2002 Jeremy Katz <katzj@redhat.com> 1.1.6-1
- update to 1.1.6

* Thu Nov  7 2002 Jeremy Katz <katzj@redhat.com> 1.1.5-3
- rebuild to really fix Xlib paths now that gnome-libs is fixed

* Tue Nov  5 2002 Jeremy Katz <katzj@redhat.com> 1.1.5-2
- rebuild to fix Xlib paths in .pc files

* Fri Nov  1 2002 Jeremy Katz <katzj@redhat.com> 1.1.5-1
- update to 1.1.5

* Thu Oct 24 2002 Jeremy Katz <katzj@redhat.com> 1.1.4-1
- remove unwanted files from buildroot
- update to 1.1.4

* Thu Sep 26 2002 Jeremy Katz <katzj@redhat.com>
- make sure we get all of the stuff from %%{_datadir}/gtkhtml-1.1

* Wed Sep 25 2002 Jeremy Katz <katzj@redhat.com>
- update to 1.1.2

* Tue Jul 23 2002 Owen Taylor <otaylor@redhat.com>
- Fix problem with finding the closest size

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Jun 19 2002 Jeremy Katz <katzj@redhat.com>
- update to 1.0.4
- remove .la files

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue May 14 2002 Jeremy Katz <katzj@redhat.com>
- tweak buildrequires for libcapplet0-devel

* Tue Mar 19 2002 Jeremy Katz <katzj@redhat.com>
- update to gtkhtml 1.0.2

* Thu Mar  7 2002 Jeremy Katz <katzj@redhat.com>
- remove superflous capplet entry (#59698)

* Mon Jan 14 2002 Jeremy Katz <katzj@redhat.com>
- rebuild to get rid of ridiculous libgal18 linkage

* Sat Jan 12 2002 Jeremy Katz <katzj@redhat.com>
- update to 1.0.1

* Sun Dec  2 2001 Jeremy Katz <katzj@redhat.com>
- update to 1.0.0

* Sat Nov 17 2001 Jeremy Katz <katzj@redhat.com>
- update to 0.16.1

* Mon Nov  5 2001 Jeremy Katz <katzj@redhat.com>
- updated to 0.16

* Tue Oct 23 2001 Havoc Pennington <hp@redhat.com>
- 0.15

* Thu Oct  4 2001 Havoc Pennington <hp@redhat.com>
- 0.14
- remove --without-bonobo
- langify

* Mon Aug 20 2001 Alexander Larsson <alexl@redhat.com> 0.9.2-9
- Moved gnome-conf file to the devel package
- Fixes SHOULD-FIX bug #49796

* Mon Jul 30 2001 Alexander Larsson <alexl@redhat.com> 
- Added dependencies on -devel packages from the gtkhtml-devel package

* Fri Jul 20 2001 Alexander Larsson <alexl@redhat.com>
- Add more build dependencies

* Thu Jul 17 2001 Bill Nottingham <notting@redhat.com>
- fix devel package requirements

* Sat Jul  7 2001 Tim Powers <timp@redhat.com>
- changed bad groups
- laguified package

* Tue Jul 03 2001 Havoc Pennington <hp@redhat.com>
- fix X11/libraries -> X11/Libraries, #47137 

* Wed Jun 13 2001 Bill Nottingham <notting@redhat.com>
- fix brokenness due to gal damage

* Wed Jun  6 2001 Bill Nottingham <notting@redhat.com>
- adapt included specfile
