%global momorel 24
%global pkg clmemo
%global pkgname clmemo

Summary: clmemo.el provides some commands and minor modes for ChangeLog MEMO
Name: emacs-clmemo
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Editors
URL: http://pop-club.hp.infoseek.co.jp/emacs/changelog.html
# see also <http://at-aka.blogspot.com/2005/05/clmemo-100-emacschangelog.html>
Source0: http://pop-club.hp.infoseek.co.jp/emacs/clmemo-%{version}.tar.gz
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
Requires: emacs(bin) >= %{_emacs_version}
BuildRequires: tetex-ptex
BuildRequires: texinfo, tetex-latex, tetex-dvipsk, dvipdfmx
Requires(post): info
Requires(preun): info
Obsoletes: clmemo-emacs
Obsoletes: clmemo-xemacs
Obsoletes: elisp-clmemo
Provides: elisp-clmemo

%description
clmemo.el provides some commands and minor modes for ChangeLog MEMO.

`ChangeLog MEMO' is a kind of concept that writing memo into _ONE_
file in ChangeLog format.  You can take a memo about address book,
bookmark, diary, idea, memo, news, schedule, time table, todo list,
citation from book, one-liner that you wrote but will forget, etc....

(1) Why one file?

* Easy for you to copy and move file, to edit memo, and to find out
  something important.

* Obvious that text which you want is in this file.  In other words,
  if the text you need is not in this file, you don't have a memo
  about it.  You will be free from searching all over the files for
  what you have taken or might have not.

(2) Why ChangeLog format?

* One of well known format.

* A plain text file.  Binary is usually difficult to edit and search.
  File size gets bigger.  And most of binary file needs special soft.
  You should check that your soft is distributed permanently.

* Easier to read than HTML, XML, and TeX.

* Entries are automatically sorted by chronological order.

%package el
Summary:	Elisp source files for %{pkgname} under GNU Emacs
Group:		Applications/Text
Requires:	%{name} = %{version}-%{release}

%description el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.

%prep
%setup -q -n clmemo-%{version}

%build
%{__make} EMACS=emacs prefix=%{_prefix} datadir=%{_datadir} lispdir=%{_emacs_sitelispdir}
%{__make} info html pdf

%install
rm -rf %{buildroot}

%__mkdir_p %{buildroot}%{_emacs_sitelispdir}/%{pkg}
%{__install} -m 644 clmemo.el %{buildroot}%{_emacs_sitelispdir}/%{pkg}
%{__install} -m 644 clmemo.elc %{buildroot}%{_emacs_sitelispdir}/%{pkg}

%{__mkdir_p} %{buildroot}%{_infodir}
%{__install} -m 644 doc/changelog.info %{buildroot}%{_infodir}

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/changelog.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/changelog.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc ChangeLog INSTALL.ja INSTALL README
%doc doc/changelog.html doc/clmemo.pdf
%{_infodir}/changelog.info*
%{_emacs_sitelispdir}/%{pkg}/*.elc

%files el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el


%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-24m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-23m)
- rename the package name
- add -el subpackage

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0-22m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-21m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-20m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-18m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-17m)
- rebuild against emacs-23.2

* Fri May 21 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-16m)
- add BuildRequires

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-15m)
- merge clmemo-emacs to elisp-clmemo
- kill clmemo-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-13m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-12m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-11m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-10m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-8m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-7m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43

* Wed Aug 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- initial packaging
