%global momorel 5

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:		libftdi
Version:	0.18
Release:	%{momorel}m%{?dist}
Summary:	Library to program and control the FTDI USB controller

Group:		System Environment/Libraries
License:	LGPLv2
URL:		http://www.intra2net.com/de/produkte/opensource/ftdi/
Source0:	http://www.intra2net.com/de/produkte/opensource/ftdi/TGZ/%{name}-%{version}.tar.gz
NoSource:	0
Source1:	no_date_footer.html
Patch1:		libftdi-0.17-multilib.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	libusb-devel, doxygen, boost-devel, python-devel >= 2.7 , swig
Requires:	pkgconfig, udev
Requires(pre):	shadow-utils


%package devel
Summary:	Header files and static libraries for libftdi
Group:		Development/Libraries
Requires:	libftdi = %{version}-%{release}
Requires:	libusb-devel

%package python
Summary:	Libftdi library Python binding
Group:		Development/Libraries
Requires:	libftdi = %{version}-%{release}

%package c++
Summary:	Libftdi library C++ binding
Group:		Development/Libraries
Requires:	libftdi = %{version}-%{release}

%package c++-devel
Summary:	Libftdi library C++ binding development headers and libraries
Group:		Development/Libraries
Requires:	libftdi-devel = %{version}-%{release}, libftdi-c++ = %{version}-%{release}


%description
A library (using libusb) to talk to FTDI's FT2232C,
FT232BM and FT245BM type chips including the popular bitbang mode.

%description devel
Header files and static libraries for libftdi

%description python
Libftdi Python Language bindings.

%description c++
Libftdi library C++ language binding.

%description c++-devel
Libftdi library C++ binding development headers and libraries
for building C++ applications with libftdi.


%prep
%setup -q
sed -i -e 's/HTML_FOOTER            =/HTML_FOOTER            = no_date_footer.html/g' doc/Doxyfile.in
#kernel does not provide usb_device anymore
sed -i -e 's/usb_device/usb/g' packages/99-libftdi.rules
%patch1 -p1 -b .multilib


%build
%configure --enable-python-binding --enable-libftdipp --disable-static
cp %{SOURCE1} %{_builddir}/%{name}-%{version}/doc
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
find %{buildroot} -name \*\.la -print | xargs rm -f
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man3
#no man install
install -p -m 644 doc/man/man3/*.3 $RPM_BUILD_ROOT%{_mandir}/man3
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d
install -p -m 644 packages/99-libftdi.rules $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d


# Cleanup examples
rm -f $RPM_BUILD_ROOT/%{_bindir}/simple
rm -f $RPM_BUILD_ROOT/%{_bindir}/bitbang
rm -f $RPM_BUILD_ROOT/%{_bindir}/bitbang2
rm -f $RPM_BUILD_ROOT/%{_bindir}/bitbang_ft2232
rm -f $RPM_BUILD_ROOT/%{_bindir}/bitbang_cbus
rm -f $RPM_BUILD_ROOT/%{_bindir}/find_all
rm -f $RPM_BUILD_ROOT/%{_bindir}/find_all_pp
rm -f $RPM_BUILD_ROOT/%{_bindir}/baud_test
rm -f $RPM_BUILD_ROOT/%{_bindir}/serial_read

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING.LIB README
%{_libdir}/libftdi.so.*
%config(noreplace) %{_sysconfdir}/udev/rules.d/99-libftdi.rules

%files devel
%defattr(-,root,root,-)
%doc doc/html
%{_bindir}/libftdi-config
%{_libdir}/libftdi.so
%{_includedir}/*.h
%{_libdir}/pkgconfig/libftdi.pc
%{_mandir}/man3/*
%exclude %{_mandir}/man3/deprecated.3*

%files python
%defattr(-, root, root, -)
%doc AUTHORS ChangeLog COPYING.LIB README
%{python_sitearch}/*

%files c++
%defattr(-, root, root, -)
%doc AUTHORS ChangeLog COPYING.LIB README
%{_libdir}/libftdipp.so.*

%files c++-devel
%defattr(-, root, root, -)
%doc doc/html
%{_libdir}/libftdipp.so
%{_includedir}/*.hpp
%{_libdir}/pkgconfig/libftdipp.pc

%pre
getent group plugdev >/dev/null || groupadd -r plugdev
exit 0

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%post c++ -p /sbin/ldconfig
%postun c++ -p /sbin/ldconfig

%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-5m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.18-4m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-3m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (0.18-2m)
- rebuild for boost

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.18-1m)
- update 0.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-7m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-6m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-5m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-4m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-3m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-1m)
- sync with Fedora 13 (0.17-5)

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-1m)
- import from Fedora 11

* Sat May 09 2009 Lucian Langa <cooly@gnome.eu.org> - 0.16-1
- new upstream release

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.15-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Lucian Langa <cooly@gnome.eu.org> - 0.15-3
- fix tag

* Sun Feb 15 2009 Lucian Langa <cooly@gnome.eu.org> - 0.15-2
- add new BR boost-devel

* Sun Feb 15 2009 Lucian Langa <cooly@gnome.eu.org> - 0.15-1
- fix for bug #485600: pick libusb-devel for -devel subpackage
- new upstream release

* Fri Sep 26 2008 Lucian Langa <cooly@gnome.eu.org> - 0.14-2
- require pkgconfig for devel

* Tue Sep 23 2008 Lucian Langa <cooly@gnome.eu.org> - 0.14-1
- new upstream

* Wed Sep 03 2008 Lucian Langa <cooly@gnome.eu.org> - 0.13-1
- initial specfile
