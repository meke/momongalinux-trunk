%global         momorel 1
%global         repoid 54458

Name:           saphire
Version:        3.6.5
Release:        %{momorel}m%{?dist}
Summary:        Yet another shell
Group:          System Environment/Shells
License:        GPL+
URL:            http://ab25cq.web.fc2.com/
Source0:        http://dl.sourceforge.jp/sash/%{repoid}/saphire-%{version}.tgz
NoSource:       0
BuildRequires:  cmigemo-devel
BuildRequires:  ncurses-devel
BuildRequires:  oniguruma-devel
BuildRequires:  readline-devel

%description
Yet another shell

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

# Don't strip binary
sed -i.strip -e 's|install -s|install |' Makefile.in
# cp -> ln for library
sed -i.ln -e '/libsaphire.so/s|cp |ln -s -f |' Makefile.in
# Add current directory to library search path
sed -i.libpath -e '/^LIBS[2]*=/s|^\(.*\)$|\1 -L.|' Makefile.in
# Don't do lib-install for all
sed -i.all -e '/^all:/s|lib-install||' Makefile.in
# Keep timestamp
sed -i.stamp \
        -e 's| -m 0755| -p -m 0755|g' \
        -e 's| -m 0644| -p -m 0644|g' \
        Makefile.in
# Umm...
sed -i.soname \
        -e '/[ \t]/s|\( -o libsaphire.so.2.0.0 \)| -Wl,-soname,libsaphire.so.2 \1|' \
        Makefile.in

# FIX CRLF
for file in \
        CHANGELOG.txt \
        README.*.txt \
        USAGE.*.txt
do
        sed -i.dos -e 's|\r||' $file
        touch -r $file{.dos,}
        rm $file.dos
done

# set less as a pager
sed -i.pager \
        -e 's| lv| less|' \
        -e 's|lv |less |' \
        saphire.sa

%build
# Move maybe-arch-dependent file out of %%sysconfdir
# --docdir is needed
%configure \
	--with-migemo \
	--with-system-migemodir=%{_datadir}/cmigemo \
        --sysconfdir=%{_libdir}/%{name} \
        --docdir=%{_defaultdocdir}/%{name}-%{version} \
        --includedir=%{_includedir}/%{name}

# configure overrides $CFLAGS
# Kill parallel make
# Umm... override docdir also here
make -j1 \
        CC="gcc %{optflags}" \
        docdir=%{_defaultdocdir}/%{name}-%{version} \
        -k

%install
# Unfortunately, this Makefile.in does not honor DESTDIR, so
# let's use %%makeinstall :(
# docdir is to be cleaned up with %%doc, however specify it
# anyway for consistency
%makeinstall \
        lib-install \
        sysconfdir=%{buildroot}%{_libdir}/%{name} \
        docdir=%{buildroot}%{_defaultdocdir}/%{name}-%{version} \
        includedir=%{buildroot}%{_includedir}/%{name}

%post	-p /sbin/ldconfig
%postun	-p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS CHANGELOG.txt README.*.txt TODO.*.txt USAGE.*.txt
%lang(ja) %doc CHANGELOG.txt
%lang(ja) %doc README.ja.txt
%lang(ja) %doc USAGE.ja.txt
%{_bindir}/%{name}
%{_bindir}/saphiresh
%{_libdir}/lib%{name}.so.2*
%{_libdir}/%{name}/
%{_mandir}/man1/%{name}*.1*

%files	devel
%defattr(-,root,root,-)
%{_includedir}/%{name}/
%{_libdir}/lib%{name}.so

%changelog
* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.5-1m)
- update to 3.6.5

* Sat Dec 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.4-1m)
- update to 3.6.4

* Sat Dec  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.3-1m)
- update to 3.6.3

* Sat Nov 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2

* Sun Nov 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-1m)
- update to 3.5.9

* Sun Oct 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to 3.5.6

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Sun Sep 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.1-1m)
- update to 3.5.1

* Fri Sep  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-1m)
- update to 3.5.0

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.9-1m)
- update to 3.4.9

* Sat Aug 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.5-1m)
- update to 3.4.5

* Sat Aug  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.2-1m)
- update to 3.4.2

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.5-1m)
- update to 3.3.5

* Fri Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.3-1m)
- update to 3.3.3

* Fri Jul 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-1m)
- update to 3.3.1

* Mon Jul  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Fri Jul  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.5-1m)
- update to 3.1.5

* Mon Jun  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.3-1m)
- update to 3.1.3

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2

* Sun May 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Mon May 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0

* Fri May 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.9-1m)
- update to 3.0.9

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.8-1m)
- update to 3.0.8

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.7-1m)
- update to 3.0.7

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5

* Mon May  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Fri Apr 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Sun Mar 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Wed Mar  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Mon Mar  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.8-1m)
- update to 1.2.8

* Mon Feb  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Tue Jan 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sun Jan 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Fri Jan 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7

* Sun Jan  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Sat Jan  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Wed Dec 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- import from Fedora devel for new mfiler3

* Wed Dec 29 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.1.0-1
- 1.1.0

* Wed Dec 21 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.8-1
- 1.0.8

* Tue Dec 21 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.7-1
- 1.0.7

* Fri Dec 17 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.6-1
- 1.0.6

* Thu Dec 16 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.5-1
- 1.0.5

* Thu Dec  9 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.2-1
- 1.0.2

* Tue Dec  7 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.1-1
- 1.0.1

* Sat Dec  4 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.0-1
- 1.0.0

* Fri Nov 10 2010 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.9.5-1
- Initial packaging
