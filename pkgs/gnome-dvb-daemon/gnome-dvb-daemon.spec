%global momorel 1

%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Name: gnome-dvb-daemon
Summary: The GNOME Control Center
Version: 0.2.10
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Multimedia
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.2/%{name}-%{version}.tar.xz
NoSource: 0

URL: http://live.gnome.org/DVBDaemon
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gstreamer
Requires: python >= 2.7

BuildRequires: intltool
BuildRequires: pkgconfig

BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: gstreamer-plugins-bad-devel
BuildRequires: glib2-devel
BuildRequires: dbus-glib-devel
BuildRequires: libgee-devel
BuildRequires: sqlite-devel
BuildRequires: gstreamer-rtsp-devel
BuildRequires: libgudev1-devel
BuildRequires: dbus-devel
BuildRequires: vala-devel
BuildRequires: totem-devel

Requires(pre): hicolor-icon-theme gtk2
Obsoletes: %{name}-devel < %{version}-%{release}

%description
GNOME DVB Daemon is a daemon written in Vala based on GStreamer to setup your DVB devices, record and watch TV shows and browse EPG. It can be controlled by any application via its D-Bus interface.

%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --disable-static \
    --enable-totem-plugin
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/gnome-dvb-control
%{_bindir}/gnome-dvb-daemon
%{_bindir}/gnome-dvb-setup
%{_datadir}/applications/gnome-dvb-control.desktop
%{_datadir}/applications/gnome-dvb-setup.desktop
%{_datadir}/dbus-1/services/org.gnome.DVB.service
%{_datadir}/dbus-1/services/org.gnome.UPnP.MediaServer2.DVBDaemon.service
%{_datadir}/icons/hicolor/*/*/*

# python
%{python_sitelib}/gnomedvb

# totem
%{_libdir}/totem/plugins/dvb-daemon

%changelog
* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.10-1m)
- update to 0.2.10

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-2m)
- rebuild for glib 2.33.2

* Tue Oct 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.5-1m)
- update to 0.2.5

* Sat Oct  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.4-1m)
- update to 0.2.4

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.3-1m)
- update to 0.2.3

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Tue May 24 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-2m)
- add BuildRequires

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.20-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.20-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.20-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.20-3m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.20-2m)
- add BuildRequires

* Thu Jul 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.20-1m)
- initial build
