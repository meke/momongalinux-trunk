%global momorel 4

# Copyright (c) 2000-2008, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# If you don't want to build with maven, and use straight ant instead,
# give rpmbuild option '--without maven'

%define with_maven %{!?_without_maven:1}%{?_without_maven:0}
%define without_maven %{?_without_maven:1}%{!?_without_maven:0}

%define bname embedder

%define maven_settings_file %{_builddir}/%{name}/settings.xml

Name:           maven-embedder
Version:        2.0.4
Release:        %{momorel}m%{?dist}
Summary:        Maven Embedder
License:        "ASL 2.0"
Group:          Development/Libraries
URL:            http://maven.apache.org/
# svn export http://svn.apache.org/repos/asf/maven/components/tags/maven-embedder-2.0.4/
# Note: Exported revision 776856.
# tar czf maven-embedder-2.0.4-src.tgz maven-embedder-2.0.4
Source0:        %{name}-%{version}-src.tgz

Source1:        %{name}-build.xml
Source2:        %{name}-jpp-depmap.xml

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot

BuildArch:      noarch
BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  java-devel >= 0:1.5.0
BuildRequires:  ant >= 0:1.6.5
%if %{with_maven}
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-ant
BuildRequires:  maven2-plugin-assembly
BuildRequires:  maven2-plugin-checkstyle
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-plugin
BuildRequires:  maven2-plugin-pmd
BuildRequires:  maven2-plugin-project-info-reports
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-site
BuildRequires:  maven-surefire-maven-plugin = 2.3
BuildRequires:  maven-surefire-provider-junit = 2.3
BuildRequires:  maven-doxia
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  checkstyle
%endif

Requires:       maven2
Requires(post):   jpackage-utils >= 0:1.7.2
Requires(postun): jpackage-utils >= 0:1.7.2
Conflicts:      maven2 < 0:2.0.6

%description
The Maven Embedder is used by the Maven CLI, by IDE integration projects like
Mevenide and potentially any tool that needs to embed Maven's capabilities.
You could embed Maven in a Continuous Integration application to run Maven
build, an application lifecycle management (ALF) tool, or Ant tasks that
utilize Maven's functionality.
These are just a few examples of what the Maven Embedder can be used for.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
%{summary}.

%prep
%setup -q
cp %{SOURCE1} build.xml

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

%if %{with_maven}

# disable tests because they download stuff from the net

    mvn-jpp \
        -e \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        -Dmaven.test.skip=true \
        install javadoc:javadoc

# ignoring failures only needed for mock environments
%else
export CLASSPATH=$(build-classpath \
commons-codec \
commons-httpclient \
jdom \
maven2/artifact \
maven2/artifact-manager \
maven2/core \
maven2/model \
maven2/monitor \
maven2/plugin-api \
maven2/plugin-descriptor \
maven2/plugin-registry \
maven2/profile \
maven2/project \
maven2/repository-metadata \
maven2/settings \
maven-wagon/provider-api \
maven-wagon/file \
maven-wagon/http \
plexus/classworlds \
plexus/container-default \
plexus/interactivity-api \
plexus/utils \
)
CLASSPATH=$CLASSPATH:target/classes:target/test-classes
ant -Dbuild.sysclasspath=only jar javadoc
%endif

%install
rm -rf $RPM_BUILD_ROOT
# jars/poms
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}/maven2
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms

install -m 644 target/%{name}-%{version}.jar \
               $RPM_BUILD_ROOT%{_javadir}/maven-embedder-%{version}.jar
%add_to_maven_depmap org.apache.maven %{name} %{version} JPP embedder
install -pm 644 pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.maven-embedder.pom

(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# create a compatibility link
ln -s ../maven-embedder-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/maven2/embedder.jar

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}

cp -pr target/site/apidocs/* \
                    $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_javadir}/maven2/embedder*.jar
%{_javadir}/%{name}*jar
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%if %{with_maven}
%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-1m)
- import from Fedora 13

* Tue Sep 1 2009 Alexander Kurtakov <akurtako@redhat.com> 2.0.4-6
- Remove Epoch:0.
- Require maven-surefire-maven-plugin instead of maven2-plugin-surefire.

* Mon Aug 31 2009 Alexander Kurtakov <akurtako@redhat.com> 0:2.0.4-5
- Adapt for Fedora.

* Wed May 20 2009 Fernando Nasser <fnasser@redhat.com> 2.0.4-4
- Fix license
- Fix instructions to generate source tar ball
- Refresh source tar ball
- Better description

* Mon Apr 27 2009 Yong Yang <yyang@redhat.com> 2.0.4-3
- Add BRs for maven-doxia*, checkstyle
- Rebuild with maven2-2.0.8 built in non-bootstrap mode

* Mon Mar 09 2009 Yong Yang <yyang@redhat.com> 2.0.4-2
- Import from dbhole's maven2 2.0.8 packages

* Thu Apr 03 2008 Deepak Bhole <dbhole@redhat.com> 2.0.4-1jpp.1
- Import from JPackage

* Tue Feb 05 2008 Ralph Apel <r.apel at r-apel.de> 0:2.0.4-1jpp
- First release
