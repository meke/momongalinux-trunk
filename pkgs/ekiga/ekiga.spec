%global momorel 1
%global boost_version 1.55.0
Summary: A Gnome based SIP/H323 teleconferencing application
Name: ekiga
Version: 4.0.1
Release: %{momorel}m%{?dist}
URL: http://www.ekiga.org/
License: GPL
Source0: http://ftp.gnome.org/pub/gnome/sources/ekiga/4.0/%{name}-%{version}.tar.xz
NoSource: 0

Group: Applications/Communications
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: libsigc++-devel >= 2.2.3
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: libgnome-devel >= 2.26.0
BuildRequires: libgnomeui-devel >= 2.24.1
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: libXv-devel
BuildRequires: dbus-devel >= 1.2.4
BuildRequires: dbus-glib-devel >= 0.80
BuildRequires: avahi-devel >= 0.6.24
BuildRequires: avahi-glib-devel >= 0.6.24
BuildRequires: ptlib-devel >= 2.10.10
BuildRequires: opal-devel >= 3.10.10
BuildRequires: SDL-devel
BuildRequires: opal-devel >= 3.10.7
BuildRequires: openssl-devel >= 0.9.8g
BuildRequires: evolution-data-server-devel >= 3.5.90
BuildRequires: unixODBC-devel >= 2.2.14
BuildRequires: boost-devel >= %{boost_version}
Requires(pre): hicolor-icon-theme gtk2
Requires: dbus
Obsoletes: gnomemeeting

%description
GnomeMeeting is a tool to communicate with video and audio over the internet.
It uses the H323 protocol and is compatible with Microsoft Netmeeting.

%prep
%setup -q

%build
%configure \
    --disable-scrollkeeper \
    --disable-schemas-install \
    --enable-gnome \
    --enable-gconf \
    --enable-eds \
    --program-prefix="" \
%ifnarch %{ix86}
    --with-boost-libdir=%{_libdir} \
%endif
    --enable-notify \
    --enable-ldap \
    --enable-mmx \
    --enable-shm \
    --enable-dbus \
    --enable-dbus-service \
    --enable-avahi \
    --enable-gstreamer \
    --enable-gdu \
    --enable-h323  \
    LIBS="-lX11"
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/ekiga.schemas \
    > /dev/null || :
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
rarian-sk-update

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/ekiga.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/ekiga.schemas \
	> /dev/null || :
fi

%postun
rarian-sk-update

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog FAQ INSTALL NEWS README TODO
%{_bindir}/ekiga
%{_bindir}/ekiga-config-tool
%{_bindir}/ekiga-helper
%{_libdir}/ekiga/%{version}/libekiga.so
%{_libdir}/ekiga/%{version}/libekiga.la
%{_libdir}/ekiga/%{version}/plugins/*.so
%{_libdir}/ekiga/%{version}/plugins/*.la
%{_sysconfdir}/gconf/schemas/ekiga.schemas
%{_datadir}/gnome/help/ekiga/*/*.xml
%{_datadir}/gnome/help/ekiga/*/*/*.png
%{_datadir}/locale/*/*/*
%{_mandir}/man1/*
%{_datadir}/pixmaps/ekiga/*.png
%{_datadir}/sounds/ekiga/*
%{_datadir}/applications/ekiga.desktop
%{_datadir}/omf/ekiga
%{_datadir}/dbus-1/services/*.service
%{_datadir}/icons/hicolor/*/*/*
%exclude %{_datadir}/icons/hicolor/icon-theme.cache

%changelog
* Wed Jan 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.1-1m)
- update 4.0.1

* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.90-0.3m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.90-2m)
- rebuild against boost-1.52.0

* Wed Oct 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.90-1m)
- update to 3.9.90
- rebuild against ptlib-2.10.7
- rebuild against opal-3.10.7

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.2-0.8m)
- rebuild against evolution-data-server-3.5.90

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-0.7m)
- rebuild against evolution-data-server-3.5.4

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.2-0.6m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (3.3.2-0.5m)
- rebuild for boost 1.50.0

* Wed Jul 11 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.3.2-0.4m)
- add patch0
--Fixes bug 677383
--Adapt to Evolution-Data-Server API changes in 3.5.3

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.2-0.3m)
- rebuild for glib 2.33.2

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.2-0.2m)
- rebuild for boost-1.48.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.2-1m)
- update to 3.3.2

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (3.3.1-0.18m)
- rebuild for boost

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.1-0.17m)
- rebuild against evolution-data-server-3.0.0

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.1-0.16m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-0.15m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-0.14m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-0.13m)
- rebuild against boost-1.46.0

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-0.12m)
- rebuild against ptlib and opal

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-0.11m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-0.10m)
- rebuild against boost-1.44.0

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.1-0.9m)
- rebuild against evolution-2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.1-0.8m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.1-0.7m)
- fix build

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-0.6m)
- rebuila against ptlib & opal

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.1-0.5m)
- rebuild against boost-1.43.0

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.1-0.4m)
- rebuild against evolution-2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.1-0.3m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-0.2m)
- enable to build on x86_64

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.1-0.1m)
- update 3.3.1-20100508 sources. 

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.6-4m)
- rebuild against ptlib-2.8.1 opal-3.8.1

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.6-3m)
- rebuild against ptlib-2.8.0 opal-3.6.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.6-1m)
- update to 3.2.6

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.5-1m)
- update to 3.2.5

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.4-1m)
- update to 3.2.4

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-3m)
- rebuild against unixODBC-2.2.14

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-2m)
- rebuild against openssl-0.9.8k

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.2-2m)
- fix BPR

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2
- delete patch0 (fixed)

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0
- add patch0 (fix install)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.2-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-2m)
- welcome evolution-data-server

* Mon Sep 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.12-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.12-1m)
- update to 2.0.12

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.11-2m)
- %%NoSource -> NoSource

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.11-1m)
- update to 2.0.11
- build with opal-2.2.11([SECURITY] CVE-2007-4924)

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.9-1m)
- update 2.0.9

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.7-2m)
- rebuild against evolution-data-server-1.10.0

* Mon Mar 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5

* Mon Jan 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Fri Oct 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3-1m)
- real update

#* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
#- (2.0.3-3m)
#- rebuild against evolution-data-server-1.8.0
#
#* Mon Sep  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
#- (2.0.3-1m)
#- update to 2.0.3

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-2m)
- rebuild against dbus 0.92

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Fri Jun  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-3m)
- enable avahi, dbus support

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Fri Mar  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.99.1-1m)
- version up

* Wed Jan 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.99.0-1m)
- Start
