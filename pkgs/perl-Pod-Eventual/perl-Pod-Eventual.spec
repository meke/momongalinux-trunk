%global         momorel 3

Name:           perl-Pod-Eventual
Version:        0.094001
Release:        %{momorel}m%{?dist}
Summary:        Read a POD document as a series of trivial events
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Pod-Eventual/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/Pod-Eventual-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Mixin-Linewise >= 0.001
BuildRequires:  perl-Test-Deep
Requires:       perl-Mixin-Linewise >= 0.001
Requires:       perl-Test-Deep
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
POD is a pretty simple format to write, but it can be a big pain to deal
with reading it and doing anything useful with it. Most existing POD
parsers care about semantics, like whether a =item occurred after an
=over but before a back, figuring out how to link a L<>, and other things
like that.

%prep
%setup -q -n Pod-Eventual-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes eg LICENSE META.json README
%{perl_vendorlib}/Pod/Eventual.pm
%{perl_vendorlib}/Pod/Eventual
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.094001-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.094001-2m)
- rebuild against perl-5.18.2

* Sun Nov 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.094001-1m)
- update to 0.094001

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.093330-2m)
- rebuild against perl-5.18.1

* Thu May 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.093330-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
