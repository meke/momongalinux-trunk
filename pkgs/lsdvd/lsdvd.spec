%global momorel 10

Summary: Small application for displaying the contents of a DVD
Name: lsdvd
Version: 0.16
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://untrepid.com/lsdvd/
Source0: http://dl.sourceforge.net/sourceforge/lsdvd/lsdvd-%{version}.tar.gz 
NoSource: 0
Patch0: lsdvd-0.16-build.patch
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf, automake
BuildRequires: libdvdread-devel >= 4.1.2

%description
Lsdvd is a c application for reading the contents of a DVD and printing the
contents to your terminal. Lsdvd uses libdvdread, the most popular dvd
reading library for *nix

%prep
%setup -q
%patch0 -p1 -b .build
%{__aclocal}
%{__automake} --add-missing --copy --force --gnu --include-deps Makefile
%{__autoconf}

%build
%configure
%{__make} %{?_smp_mflags} CFLAGS="%{optflags}"

%install
%{__rm} -rf %{buildroot}
%makeinstall

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc AUTHORS ChangeLog COPYING README
%{_bindir}/lsdvd
%{_mandir}/man1/lsdvd.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-5m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-4m)
- rebuild against libdvdread-4.1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-2m)
- %%NoSource -> NoSource

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.16-1m)
- import to Momonga from freshrpms.net

* Mon May  8 2006 Matthias Saou <http://freshrpms.net/> 0.16-2
- Rebuild with latest tarball from sf.net, as apparently the original 0.16
  source was replaced after 3 days by a "fixed" source with the same file name.

* Wed Apr 19 2006 Matthias Saou <http://freshrpms.net/> 0.16-1
- Update to 0.16.
- Update URL.
- Update build patch, keep fixed libdvdread include detection.
- Include newly added man page.

* Fri Mar 17 2006 Matthias Saou <http://freshrpms.net/> 0.15-2
- Release bump to drop the disttag number in FC5 build.

* Mon Jan  9 2006 Matthias Saou <http://freshrpms.net/> 0.15-1
- Initial RPM release.

