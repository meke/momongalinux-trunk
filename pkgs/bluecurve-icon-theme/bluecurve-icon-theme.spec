%global momorel 12

Summary: Bluecurve icon theme
Name: bluecurve-icon-theme
Version: 8.0.2
Release: %{momorel}m%{?dist}
BuildArch: noarch
License: GPL+
Group: User Interface/Desktops
# There is no official upstream yet
Source0: %{name}-%{version}.tar.bz2
URL: http://www.redhat.com
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 

Requires: system-logos
Requires: bluecurve-cursor-theme
Requires(post): coreutils

# we require XML::Parser for our in-tree intltool
BuildRequires: perl(XML::Parser)

%description
This package contains Bluecurve style icons.

%package -n bluecurve-cursor-theme
Summary: Bluecurve cursor theme
Group: User Interface/Desktops

%description -n bluecurve-cursor-theme
This package contains Bluecurve style cursors.

%prep
%setup -q 

%build
%configure 
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# These are empty
rm -f ChangeLog NEWS README

touch %{buildroot}%{_datadir}/icons/Bluecurve/icon-theme.cache

# backward compatibility for KDE4
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/Bluecurve/48x48/apps/redhat-accessories.png %{buildroot}%{_datadir}/pixmaps/redhat-accessories.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-email.png %{buildroot}%{_datadir}/pixmaps/redhat-email.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-games.png %{buildroot}%{_datadir}/pixmaps/redhat-games.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-graphics.png %{buildroot}%{_datadir}/pixmaps/redhat-graphics.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-home.png %{buildroot}%{_datadir}/pixmaps/redhat-home.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-internet.png %{buildroot}%{_datadir}/pixmaps/redhat-internet.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-office.png %{buildroot}%{_datadir}/pixmaps/redhat-office.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-preferences.png %{buildroot}%{_datadir}/pixmaps/redhat-preferences.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-presentations.png %{buildroot}%{_datadir}/pixmaps/redhat-presentations.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-programming.png %{buildroot}%{_datadir}/pixmaps/redhat-programming.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-server_settings.png %{buildroot}%{_datadir}/pixmaps/redhat-server_settings.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-sound_video.png %{buildroot}%{_datadir}/pixmaps/redhat-sound_video.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-spreadsheet.png %{buildroot}%{_datadir}/pixmaps/redhat-spreadsheet.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-starthere.png %{buildroot}%{_datadir}/pixmaps/redhat-starthere.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-system_settings.png %{buildroot}%{_datadir}/pixmaps/redhat-system_settings.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-system_tools.png %{buildroot}%{_datadir}/pixmaps/redhat-system_tools.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-tools.png %{buildroot}%{_datadir}/pixmaps/redhat-tools.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-web-browser.png %{buildroot}%{_datadir}/pixmaps/redhat-web-browser.png
ln -s ../icons/Bluecurve/48x48/apps/redhat-word-processor.png %{buildroot}%{_datadir}/pixmaps/redhat-word-processor.png
ln -s ../icons/Bluecurve/48x48/apps/switchdesk.png %{buildroot}%{_datadir}/pixmaps/switchdesk.png
ln -s ../icons/Bluecurve/48x48/apps/temp-home.png %{buildroot}%{_datadir}/pixmaps/temp-home.png

# The upstream packages may gain po files at some point in the near future
%find_lang %{name} || touch %{name}.lang

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/Bluecurve
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
    %{_bindir}/gtk-update-icon-cache -f --quiet %{_datadir}/icons/Bluecurve || :
fi

%postun
touch --no-create %{_datadir}/icons/Bluecurve
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
    %{_bindir}/gtk-update-icon-cache -f --quiet %{_datadir}/icons/Bluecurve || :
fi


%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING
%{_datadir}/icons/Bluecurve/index.theme
%{_datadir}/icons/Bluecurve/16x16
%{_datadir}/icons/Bluecurve/20x20
%{_datadir}/icons/Bluecurve/24x24
%{_datadir}/icons/Bluecurve/32x32
%{_datadir}/icons/Bluecurve/36x36
%{_datadir}/icons/Bluecurve/48x48
%{_datadir}/icons/Bluecurve/64x64
%{_datadir}/icons/Bluecurve/96x96
%ghost %{_datadir}/icons/Bluecurve/icon-theme.cache
%{_datadir}/pixmaps/*.png

%files -n bluecurve-cursor-theme
%dir %{_datadir}/icons/Bluecurve
%{_datadir}/icons/Bluecurve/Bluecurve.cursortheme
%{_datadir}/icons/Bluecurve/cursors
%{_datadir}/icons/Bluecurve-inverse
%{_datadir}/icons/LBluecurve
%{_datadir}/icons/LBluecurve-inverse

%changelog
* Sun Nov 24 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0.2-12m)
- add %%{_datadir}/pixmaps/*.png again
- Momonga's customized KDE4 menu is using these icons

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.2-11m)
- reimport from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.2-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.2-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.0.2-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.0.2-7m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.0.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.0.2-5m)
- rebuild against rpm-4.6

* Fri May 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0.2-4m)
- remove %%{_datadir}/pixmaps/*.png
- these icons are provided by fedora-icon-theme
- remove Provides and Obsoletes: redhat-artwork
- fedora-gnome-theme Provides and Obsoletes: redhat-artwork

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0.2-3m)
- add %%{_datadir}/pixmaps/*.png
- Momonga's customized KDE4 menu is using these icons

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0.2-2m)
- Provides and Obsoletes: redhat-artwork

* Sun May 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.0.2-1m)
- import from Fedora to Momonga

* Mon Apr  7 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> - 8.0.2-1
- Add some symlinks to make Bluecurve work well with KDE 4 (#408151)

* Fri Feb  1 2008 Matthias Clasen <mclasen@redhat.com> - 8.0.1-1
- Fix some lrt <-> ltr typos
- Flip some redo icons

* Fri Oct 12 2007 Ray Strode <rstrode@redhat.com> - 8.0.0-1
- Add a lot of missing icons back (bug 328391)
- redo Bluecurve Makefile to scale better to all the new icons
- bump version to 8.0.0

* Tue Sep 25 2007 Ray Strode <rstrode@redhat.com> - 1.0.0-1
- Initial import, version 1.0.0
