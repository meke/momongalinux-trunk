%global momorel 23

Summary: The standard UNIX FTP (File Transfer Protocol) client.
Name: ftp
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
Source0: ftp://ftp.uk.linux.org/pub/linux/Networking/netkit-devel/netkit-ftp-%{version}.tar.bz2
Patch1: netkit-ftp-0.17-pre20000412.pasv-security.patch
Patch2: netkit-ftp-0.17-acct.patch
Patch3: netkit-ftp.usagi-ipv6.patch
Patch4: netkit-ftp-0.17-segv.patch
Patch5: netkit-ftp-0.17-volatile.patch
Patch6: netkit-ftp-0.17-runique_mget.patch
Patch7: netkit-ftp-locale.patch
Patch8: netkit-ftp-0.17-printf.patch
Patch9: netkit-ftp-0.17-longint.patch
Patch10: netkit-ftp-0.17-vsftp165083.patch
Patch11: netkit-ftp-0.17-C-Frame121.patch
Patch12: netkit-ftp-0.17-data.patch
Patch13: netkit-ftp-0.17-multihome.patch
Patch14: netkit-ftp-0.17-longnames.patch
Patch15: netkit-ftp-0.17-multiipv6.patch
Patch16: netkit-ftp-0.17-nodebug.patch
Patch17: netkit-ftp-0.17-stamp.patch
Patch18: netkit-ftp-0.17-sigseg.patch
Patch19: netkit-ftp-0.17-size.patch
Patch20: netkit-ftp-0.17-fdleak.patch
Patch21: netkit-ftp-0.17-fprintf.patch
Patch22: netkit-ftp-0.17-bitrate.patch
Patch23: netkit-ftp-0.17-arg_max.patch
Patch24: netkit-ftp-0.17-case.patch
Patch25: netkit-ftp-0.17-chkmalloc.patch
Patch26: netkit-ftp-0.17-man.patch
Patch27: netkit-ftp-0.17-acct_ovl.patch
Patch28: netkit-ftp-0.17-remove-nested-include.patch
Patch29: netkit-ftp-0.17-linelen.patch
Patch30: netkit-ftp-0.17-active-mode-option.patch
Patch31: netkit-ftp-0.17-commands-leaks.patch
Patch32: netkit-ftp-0.17-lsn-timeout.patch
Patch33: netkit-ftp-0.17-getlogin.patch
Patch34: netkit-ftp-0.17-token.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gcc, glibc-devel, ncurses-devel, perl
BuildRequires: readline-devel >= 5.0

%description
The ftp package provides the standard UNIX command-line FTP (File
Transfer Protocol) client.  FTP is a widely used protocol for
transferring files over the Internet and for archiving files.

If your system is on a network, you should install ftp in order to do 
file transfers.

%prep
%setup -q -n netkit-ftp-%{version}
%patch1 -p1
%patch2 -p1 -b .acct
%patch3 -p1 -b .ipv6
%patch4 -p1 -b .segv
%patch5 -p1 -b .volatile
%patch6 -p1 -b .runique_mget
%patch7 -p1 -b .locale
%patch8 -p1 -b .printf
%patch9 -p1 -b .longint
%patch10 -p1 -b .vsftp165083
%patch11 -p1 -b .C-Frame121
%patch12 -p1 -b .data
%patch13 -p1 -b .multihome
%patch14 -p1 -b .patch
%patch15 -p1 -b .multiipv6
%patch16 -p1 -b .nodebug
%patch17 -p1 -b .stamp
%patch18 -p1 -b .sigseg
%patch19 -p1 -b .size
%patch20 -p1 -b .fdleak
%patch21 -p1 -b .fprintf
%patch22 -p1 -b .bitrate
%patch23 -p1 -b .arg_max
%patch24 -p1 -b .case
%patch25 -p1 -b .chkmalloc
%patch26 -p1 -b .man
%patch27 -p1 -b .acct_ovl
%patch28 -p1
%patch29 -p1 -b .linelen
%patch30 -p1 -b .activemode
%patch31 -p1 -b .cmds-leaks
%patch32 -p1 -b .lsn-timeout
%patch33 -p1 -b .getlogin
%patch34 -p1 -b .token

%build
sh configure --with-c-compiler=gcc --enable-ipv6
perl -pi -e '
    s,^CC=.*$,CC=cc,;
    s,-O2,\$(RPM_OPT_FLAGS) -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64,;
    s,^BINDIR=.*$,BINDIR=%{_bindir},;
    s,^MANDIR=.*$,MANDIR=%{_mandir},;
    s,^SBINDIR=.*$,SBINDIR=%{_sbindir},;
    ' MCONFIG

make %{?_smp_mflags}

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}%{_bindir}
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man1
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man5

make INSTALLROOT=${RPM_BUILD_ROOT} install

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
%{_bindir}/ftp
%{_bindir}/pftp
%{_mandir}/man1/ftp.*
%{_mandir}/man1/pftp.*
%{_mandir}/man5/netrc.*

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-23m)
- import fedora patches

* Mon Sep 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-22m)
- fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-19m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-18m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-16m)
- rebuild against rpm-4.6

* Sun Apr  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-15m)
- add patch for glibc-2.7.90

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-14m)
- rebuild against gcc43

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.17-13m)
- rebuild against readline-5.0

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-12m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Thu Jul 18 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.17-11m)
- cleanup unused patch
- import patches from PLD cvs
  Patch0: netkit-ftp-macro-quit.patch
  Patch1: netkit-ftp-acct.patch
  Patch2: netkit-ftp-usagi-ipv6.patch
  Patch3: netkit-ftp-input_line.patch
   Original log:
    fix behavior when the line entered is too long and
    we are using readline. Fix another bug when the line is
    too long, we are using readline, and the 199th character
    is '\n'.

* Wed Jan  2 2002 Shingo Akagaki <dora@kondara.org>
- (0.17-10k)
- release version

* Fri Jul 14 2000 Jeff Johnson <jbj@redhat.com>
- add netrc man page (#7443).
- fix possible buffer overflows in ftp client.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.
- update to 0.17-pre20000412.

* Wed Apr  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild with current libreadline

* Fri Mar 24 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.17

* Fri Feb  4 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Tue Jan  4 2000 Bill Nottingham <notting@redhat.com>
- the ftp client does not require inetd

* Wed Dec 22 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Mon Aug 30 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.15.
- enable readline support (#3796).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr
