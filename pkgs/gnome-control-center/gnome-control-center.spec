%global momorel 1

%global with_ibus 1

%define gettext_package gnome-control-center-2.0

%define glib2_version 2.13.0
%define gtk3_version 3.5.13
%define gnome_desktop3_version 3.5.90
%define desktop_file_utils_version 0.9
%define xft_version 2.1.7
%define fontconfig_version 1.0.0
%define redhat_menus_version 1.8
%define metacity_version 2.23.1
%define libxklavier_version 4.0
%define gnome_menus_version 2.11.1
%define usermode_version 1.83
%define libgnomekbd_version 2.31.1
%define libXrandr_version 1.2.99

Summary: Utilities to configure the GNOME desktop
Name: gnome-control-center
Version: 3.6.3
Release: %{momorel}m%{?dist}
License: GPLv2+ and GFDL
Group: User Interface/Desktops
#VCS: git:git://git.gnome.org/gnome-control-center
Source: http://download.gnome.org/sources/gnome-control-center/3.6/gnome-control-center-%{version}.tar.xz
NoSource: 0
URL: http://www.gnome.org

Requires: gnome-settings-daemon >= 3.6.1
# !!FIX ME!!
#Requires: redhat-menus >= %{redhat_menus_version}
Requires: gnome-icon-theme
Requires: alsa-lib
Requires: gnome-menus >= %{gnome_menus_version}
Requires: gnome-desktop3 >= %{gnome_desktop3_version}
Requires: dbus-x11
Requires: gnome-control-center-filesystem = %{version}-%{release}
# we need XRRGetScreenResourcesCurrent
Requires: libXrandr >= %{libXrandr_version}
# for user accounts
Requires: accountsservice apg
# For the user languages
Requires: iso-codes
# For the sound panel and gnome-sound-applet
Requires: gnome-icon-theme-symbolic
# For the printers panel
Requires: cups-pk-helper

Provides: control-center

BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gtk3-devel >= %{gtk3_version}
BuildRequires: gdk-pixbuf2-devel >= 2.23.0
BuildRequires: librsvg2-devel
BuildRequires: GConf2-devel
BuildRequires: gnome-desktop3-devel >= %{gnome_desktop3_version}
BuildRequires: desktop-file-utils >= %{desktop_file_utils_version}
BuildRequires: libxklavier-devel >= %{libxklavier_version}
BuildRequires: libXcursor-devel
BuildRequires: libXrandr-devel >= %{libXrandr_version}
BuildRequires: gettext
BuildRequires: gnome-menus-devel >= %{gnome_menus_version}
BuildRequires: libgnomekbd-devel >= %{libgnomekbd_version}
BuildRequires: gnome-settings-daemon-devel >= 3.6.1
BuildRequires: intltool >= 0.37.1
BuildRequires: libXxf86misc-devel
BuildRequires: libxkbfile-devel
BuildRequires: libXScrnSaver-devel
BuildRequires: gnome-doc-utils
BuildRequires: libglade2-devel
BuildRequires: libxml2-devel
BuildRequires: dbus-devel >= 0.90
BuildRequires: dbus-glib-devel >= 0.70
BuildRequires: scrollkeeper
BuildRequires: libcanberra-devel
BuildRequires: chrpath
BuildRequires: gsettings-desktop-schemas-devel
BuildRequires: pulseaudio-libs-devel >= 2.0 libcanberra-devel
BuildRequires: upower-devel
BuildRequires: NetworkManager-glib-devel >= 0.9
BuildRequires: NetworkManager-gtk-devel >= 0.9
#BuildRequires: libnm-gtk-devel >= 0.9
BuildRequires: polkit-devel
BuildRequires: gnome-common >= 3.5.91
BuildRequires: cups-devel
BuildRequires: libgtop2-devel
BuildRequires: iso-codes-devel
BuildRequires: cheese-libs-devel >= 3.0.1 clutter-gst-devel clutter-gtk-devel
BuildRequires: gnome-online-accounts-devel >= 3.5.91
BuildRequires: colord-devel
BuildRequires: libnotify-devel
BuildRequires: gnome-doc-utils
BuildRequires: libwacom-devel
BuildRequires: systemd-devel
BuildRequires: libpwquality-devel
BuildRequires: nautilus-sendto-devel
%if %{with_ibus}
BuildRequires: ibus-devel >= 1.4.99
%endif
%ifnarch s390 s390x
BuildRequires: gnome-bluetooth-libs-devel >= 3.5.5
%endif

Requires(post): desktop-file-utils >= %{desktop_file_utils_version}
Requires(post): shared-mime-info
Requires(postun): desktop-file-utils >= %{desktop_file_utils_version}
Requires(postun): shared-mime-info

Provides: control-center-extra = %{version}-%{release}
Obsoletes: control-center-extra < 2.30.3-3
Obsoletes: accountsdialog <= 0.6
Provides: accountsdialog = %{version}-%{release}
Obsoletes: desktop-effects <= 0.8.7-3
Provides: desktop-effects = %{version}-%{release}
Provides: control-center-devel = %{version}-%{release}
Obsoletes: control-center-devel < 3.1.4-2

Provides: gnome-control-center-devel = %{version}-%{release}
Obsoletes: gnome-control-center-devel 

%description
This package contains configuration utilities for the GNOME desktop, which
allow to configure accessibility options, desktop fonts, keyboard and mouse
properties, sound setup, desktop theme and background, user interface
properties, screen resolution, and other settings.

%package filesystem
Summary: GNOME Control Center directories
Group: Development/Libraries
# NOTE: this is an "inverse dep" subpackage. It gets pulled in
# NOTE: by the main package an MUST not depend on the main package

Provides: control-center-filesystem

%description filesystem
The GNOME control-center provides a number of extension points
for applications. This package contains directories where applications
can install configuration files that are picked up by the control-center
utilities.


%prep
%setup -q -n gnome-control-center-%{version}

%build
%configure \
        --disable-static \
        --disable-scrollkeeper \
        --disable-update-mimedb \
        --with-libsocialweb=no \
        --enable-systemd \
%if !%{with_ibus}
        --disable-ibus \
%endif
        CFLAGS="$RPM_OPT_FLAGS -Wno-error"

# drop unneeded direct library deps with --as-needed
# libtool doesn't make this easy, so we do it the hard way
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' -e 's/    if test "$export_dynamic" = yes && test -n "$export_dynamic_flag_spec"; then/      func_append compile_command " -Wl,-O1,--as-needed"\n      func_append finalize_command " -Wl,-O1,--as-needed"\n\0/' libtool

%make

%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=$RPM_BUILD_ROOT
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

desktop-file-install --delete-original			\
  --dir $RPM_BUILD_ROOT%{_datadir}/applications				\
  --add-only-show-in GNOME						\
  $RPM_BUILD_ROOT%{_datadir}/applications/*.desktop

# we do want this
mkdir -p $RPM_BUILD_ROOT%{_datadir}/gnome/wm-properties

# we don't want these
rm -rf $RPM_BUILD_ROOT%{_datadir}/gnome/autostart
rm -rf $RPM_BUILD_ROOT%{_datadir}/gnome/cursor-fonts

# remove useless libtool archive files
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} \;

# remove rpath
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/control-center-1/panels/*.so
chrpath --delete $RPM_BUILD_ROOT%{_bindir}/gnome-control-center

%find_lang %{gettext_package} --all-name --with-gnome

%post
/sbin/ldconfig
update-desktop-database --quiet %{_datadir}/applications
update-mime-database %{_datadir}/mime > /dev/null
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
/sbin/ldconfig
update-desktop-database --quiet %{_datadir}/applications
update-mime-database %{_datadir}/mime > /dev/null
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :

%files -f %{gettext_package}.lang
%doc AUTHORS COPYING NEWS README
%{_datadir}/gnome-control-center/keybindings/*.xml
%{_datadir}/gnome-control-center/ui
%{_datadir}/gnome-control-center/pixmaps
%{_datadir}/gnome-control-center/datetime/
%{_datadir}/gnome-control-center/sounds/gnome-sounds-default.xml
%{_datadir}/gnome-control-center/bluetooth.ui
%{_datadir}/applications/*.desktop
%{_datadir}/desktop-directories/*
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/gnome-control-center/icons/
%{_datadir}/polkit-1/actions/org.gnome.controlcenter.datetime.policy
%{_datadir}/polkit-1/actions/org.gnome.controlcenter.user-accounts.policy
%{_datadir}/polkit-1/rules.d/gnome-control-center.rules
%{_datadir}/pkgconfig/gnome-keybindings.pc
%{_datadir}/sounds/gnome/default/*/*.ogg
# list all binaries explicitly, so we notice if one goes missing
%{_bindir}/gnome-control-center
%{_bindir}/gnome-sound-applet
%{_sysconfdir}/xdg/autostart/gnome-sound-applet.desktop
%{_sysconfdir}/xdg/menus/gnomecc.menu
%dir %{_libdir}/control-center-1
%{_libdir}/control-center-1/panels/libbackground.so
%{_libdir}/control-center-1/panels/libbluetooth.so
%{_libdir}/control-center-1/panels/libcolor.so
%{_libdir}/control-center-1/panels/libdate_time.so
%{_libdir}/control-center-1/panels/libdisplay.so
%{_libdir}/control-center-1/panels/libinfo.so
%{_libdir}/control-center-1/panels/libkeyboard.so
%{_libdir}/control-center-1/panels/libmouse-properties.so
%{_libdir}/control-center-1/panels/libnetwork.so
%{_libdir}/control-center-1/panels/libonline-accounts.so
%{_libdir}/control-center-1/panels/libpower.so
%{_libdir}/control-center-1/panels/libprinters.so
%{_libdir}/control-center-1/panels/libregion.so
%{_libdir}/control-center-1/panels/libscreen.so
%{_libdir}/control-center-1/panels/libsound.so
%{_libdir}/control-center-1/panels/libuniversal-access.so
%{_libdir}/control-center-1/panels/libuser-accounts.so
%{_libdir}/control-center-1/panels/libwacom-properties.so
%{_datadir}/pixmaps/faces
%{_datadir}/man/man1/*

%files filesystem
%dir %{_datadir}/gnome/wm-properties
%dir %{_datadir}/gnome-control-center
%dir %{_datadir}/gnome-control-center/keybindings

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.3-1m)
- update to 3.6.3

* Tue Oct 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2

* Wed Oct 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Sun Sep 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Mon Sep 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-3m)
- fix an IBus issue; see https://bugzilla.gnome.org/show_bug.cgi?id=682864

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.90-2m)
- BuildRequires: gnome-settings-daemon-devel >= 3.5.90

* Wed Aug 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.6-1m)
- update to 3.5.6

* Fri Jul 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-4m)
- rebuild for librsvg2 2.36.1

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-3m)
- rebuild for cheese-libs-devel

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-2m)
- revise provides and requires

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- reimport from fedora's control-center

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-4m)
- rebuild for glib 2.33.2

* Sat Apr  7 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.2-3m)
- add require apg

* Wed Feb  8 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-2m)
- add patch1 for Authentication dialog (adhoc)

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Tue Nov  1 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1-3m)
- add desktop file  install

* Fri Oct 28 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1-2m)
- require adjustment

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.92-2m)
- remove BR hal-devel

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.91-3m)
- specify gnome-menus version

* Tue Sep 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.91-2m)
- update BuildRequires

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Wed Aug 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-4m)
- build fix with cups-1.5.0 (add patch0)

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-3m)
- add BuildRequires

* Sun Jun 26 2011 Ryu SASAOKA <ryu@momonga-linux.org>  
- (3.0.2-1m)
- revised br

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1.1-1m)
- update to 3.0.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-6m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-5m)
- move menu to XDG_CONFIG_DIR

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-4m)
- rebuild against evolution-2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-3m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add %%dir

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Tue Feb 16 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.29.90-2m)
- remove eel2-devel from BuildPrereq

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-4m)
- rebuild against libxklavier-5.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
- delete patch0

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-3m)
- rebuild against libxklavier-4.0
-- add patch0

* Wed May 27 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.26.0-2m)
- libtool build fix

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-3m)
- fix autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-4m)
- add --enable-gstreamer=0.10

* Sat Feb 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-3m)
- delete mime.cache again

* Thu Feb 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-2m)
- delete mime.cache

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0.1-2m)
- rebuild against rpm-4.6

* Mon Sep 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0.1-1m)
- update to 2.24.0.1
- translation file was broken (control-center.schemas.in)
- do not use >& /dev/null in gconftool-2 (not so good ;-) 

* Mon Aug 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2.1-4m)
- delete mimeinfo.cache

* Sat Aug  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2.1-3m)
- mv gnomecc.menu to gnome menu directory

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.2.1-2m)
- change BuildPrereq: gst-plugins-base-devel to gstreamer-plugins-base-devel

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2.1-1m)
- update to 2.22.2.1

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1-2m)
- rebuild against for librsvg-2.22.2-3m

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Fri Apr  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-3m)
- add BuildPrereq: gnome-settings-daemon-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sun Feb 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-2m)
- rebuild against libxklavier-3.4

* Wed Jan  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0.1-1m)
- update to 2.20.0.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0
- rename tarname control-center -> gnome-control-center
- do not use desktop-file-install for a while

* Thu May 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Sun Mar 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-2m)
- fix spec (gnome-font-properties)

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Fri Feb 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-8m)
- delete desktopfile vender again

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-7m)
- delete desktopfile vender (gnome)

* Wed Feb 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-6m)
- add patch3 for default-application xml path
- rewind %%post script (gconftool-2)

* Wed Feb 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-5m)
- add SOURCE2 momonga-gnomecc.menu

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-4m)
- GConf2 version down

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- revice %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- do not remove glade file

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.16.2-2m)
- add BuildPrereq: libxml2-python >= 2.6.26, gst-plugins-base-devel

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.2-4m)
- rebuild against expat-2.0.0-1m

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-3m)
- delete libtool library

* Mon Jun 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-2m)
- enable <print> key for shortcut

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Sun May 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-4m)
- delete patch4: fixed by gnome-vfs-2.14.1-4m

* Mon May 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-3m)
- use gstreamer010 instead of gstreamer-0.8

* Mon May  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-2m)
- change default mail reader to sylpheed

* Fri Apr 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Thu Feb  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Mon Nov 28 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-5m)
- add %%preum for schemas
- use desktop-file-install in %%install (for KDE) 

* Fri Nov 25 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-4m)
- add autoreconf for patch 2

* Fri Nov 25 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-3m)
- fix wrong installed directory (GNOME_SettingsDaemon.server)

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-2m)
- delete autoreconf and make check

* Wed Nov 16 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up.
- GNOME 2.12.1 Desktop

* Sat Apr  9 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-4m)
- remove background setting files ... sunmaso
- adjustment default application list for momonga

* Tue Apr  5 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-3m)
- add momonga backup image (momonga-background-img.tar.gz)
- add background imag list (desktop-backgrounds-basic.xml)

* Fri Jan 28 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-2m)
- add apps_gnome_settings_daemon_keybindings.schemas
- add desktop_gnome_peripherals_keyboard_xkb.schemas

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-1m)
- version 2.8.1
- GNOME 2.8 Desktop

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (2.6.1-2m)
- revised spec for rpm 4.2.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.0-2m)
- revised spec for enabling rpm 4.2.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Fri Jun 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Fri Jun 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-2m)
- apply fontilus extention patch.

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Sun May 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-2m)
- gnome-display-properties: centering dialog.

* Fri May 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0.1-2m)
  rebuild against openssl 0.9.7a

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-1m)
- version 2.2.0.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.7-1m)
- version 2.1.7

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.6-1m)
- version 2.1.6

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Sat Nov 23 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.1.2-2m)
- fix nigiri

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Tue Oct 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0.1-1m)
- version 2.1.0.1

* Sat Sep 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-4m)
- remove Obsoletes: control-center-devel

* Tue Sep 03 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (2.1.0-3m)
- fix defattr
- why Obsolete control-center-devel ?

* Sun Sep 01 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.1.0-2m)
- add newer ja.po, will also report to bugzilla, so should probably
  remove in future releases

* Sat Aug 31 2002 Shingo AKagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Thu Aug 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1.1-1m)
- version 2.0.1.1

* Wed Aug 07 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-10m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-9m)
- rebuild against for gnome-desktop-2.0.4
- rebuild against for gnome-session-2.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-8m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-7m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Tue Jun 18 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-40k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-38k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-36k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-34k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-32k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-30k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-28k)
- rebuild against for gnome-desktop-2.0.0
- rebuild against for gnome-session-2.0.0
- rebuild against for gnome-panel-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-26k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-24k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-22k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-20k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-18k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-16k)
- rebuild against for libgtkhtml-1.99.9
- rebuild against for libzvt-1.117.0
- rebuild against for gdm-2.3.90.5
- rebuild against for yelp-0.10
- rebuild against for eel-1.1.17
- rebuild against for nautilus-1.1.19
- rebuild against for gnome-desktop-1.5.22
- rebuild against for gnome-session-1.5.21
- rebuild against for gnome-panel-1.5.24

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-14k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-12k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Fri May 24 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.99.10-12k)
- BuildPrereq: perl-base -> perl

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-10k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Mon May 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-8k)
- rebuild against for glade-1.1.0
- rebuild against for gnome-desktop-1.5.20
- rebuild against for gnome-panel-1.5.22
- rebuild against for gnome-system-monitor-1.1.7
- rebuild against for libwnck-0.12

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-6k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-4k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-2k)
- version 1.99.10

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-2k)
- version 1.99.9

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-2k)
- version 1.99.7

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-14k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-12k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-10k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-8k)
- rebuild against for gtk+-2.0.2

* Thu Apr  4 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-6k)
- remove sawfish capplets .desktop

* Tue Apr 02 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-4k)
- rebuild against for gnome-desktop-1.5.15
- rebuild against for gnome-panel-1.5.16
- rebuild against for gnome-session-1.5.15

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-2k)
- version 1.99.6

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.4-10k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.4-6k)
- fix up gnomecc dirs

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.4-6k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.4-4k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.4-2k)
- version 1.99.4

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-44k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-42k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Fri Mar  8 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.99.3-40k)
- add BuildPerreq: audiofile >= 0.2.3-4k

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-38k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-36k)
- rebuild against for libwnck-0.6

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-34k)
- rebuild against for yelp-0.3
- rebuild against for libgnomeui-1.112.1
- rebuild against for nautilus-1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-32k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-30k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-28k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-26k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-24k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-22k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-20k)
- rebuild against for libwnck-0.5

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-16k)
- rebuild against for gedit2-1.112.0
- rebuild against for gnome-desktop-1.5.10
- rebuild against for gnome-session-1.5.10
- rebuild against for gnome-panel-1.5.10
- modify file list

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-14k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-12k)
- rebuild against for libwnck-0.4

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-10k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-8k)
- remove gnome-core require
- add gnome-desktop require

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-6k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-4k)
- rebuild against for libzvt-1.111.0
- rebuild against for metatheme-0.9.3

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-2k)
- version 1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-34k)
- rebuild against for libbonoboui-1.111.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-32k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-30k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-28k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-26k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-24k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-22k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-20k)
- rebuild against for gnome-core-1.5.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-18k)
- rebuild against for libbonoboui-1.110.2

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-16k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-14k)
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-10k)
- rebuild against for libglade-1.99.6

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-8k)
- rebuild against for gnome-vfs-1.9.5

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-6k)
- rebuild against for gnome-core-1.5.6

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-8k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-2k)
- version 1.99.2
- rebuild against for gnome-core-1.5.5
- rebuild against for libzvt-1.110.0
- rebuild against for libgnomeui-1.110.0
- rebuild against for libgnomecanvas-1.110.0
- rebuild against for libgnome-1.110.0
- rebuild against for libbonoboui-1.110.0
- rebuild against for libbonobo-1.110.0

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.1-2k)
- version 1.99.1
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.0-8k)
- rebuild against for linc-0.1.15

* Tue Jan  1 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.0-0.02002010802k)
- cvs version 1.99.0

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (1.4.0.1-30k)
- move zh_CN.GB2312 => zh_CN
- move zh_TW.Big5 => zh_TW

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.1-28k)
- miss packaging!

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.1-26k)
- mada nigirisugi

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.1-24k)
- gnome-filesystemized

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.1-22k)
- bug fix gnome-programs-properties

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.4.0.1-14k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (1.4.0.1-12k)
- rebuild against libpng 1.2.0.

* Fri Aug 22 2001 Shingo Akagaki <dora@kondara.org>
- clean up spec file
- for xscreensaver >= 3.32

* Wed Jun  6 2001 Shingo Akagaki <dora@kondara.org>
- fix gnome-kondara-properties session name match bug

* Thu May 24 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- add control-center-1.4.0.1-background.patch

* Mon Apr 16 2001 Shingo Akagaki <dora@kondara.org>
- fix programs properties

* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.0.1

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.0

* Sun Mar 18 2001 Shingo Akagaki <dora@kondara.org>
- version 1.3.1
- K2K

* Mon Nov 13 2000 Shingo Akagaki <dora@kondara.org>
- roll back to 1.2.x
- version 1.2.2

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Mon Jun 19 2000 Shingo Akagaki <dora@kondara.org>
- fix gnome-programs-properties.c

* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.0

* Mon Apr 10 2000 Shingo Akagaki <dora@kondara.org>
- version 1.0.53

* Sun Apr  9 2000 SUGAWARA Zenta <sugazen@pop02.odn.ne.jp>
- Force to use /usr/X11R6/lib/xscreensaver/{gears,morph3d}
  even if /usr/X11R6/bin/{gears,morph3d} of Mesa-demos exist.

* Tue Mar 07 2000 Shingo Akagaki <dora@kondara.org>
- add kondara screensavers data

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Sun Feb 20 2000 Shigno Akagaki <dora@kondara.org>
- add default back ground patch

* Sat Feb 19 2000 Shingo Akagaki <dora@kondara.org>
- add fontset patch for theme-switcher

* Thu Feb 03 2000 Shingo Akagaki <dora@kondara.org>
- bugfix gnome-programs-properties.c

* Sat Jan 08 2000 Shingo Akagaki <dora@kondara.org>
- bugfix gnome-programs-properties
    eterm -> Eterm
- bug fix gnome-kondara-properties

* Fri Dec 24 1999 Shingo Akagaki <dora@kondara.org>
- Removed gnome-edit-properties
- Added gnome-programs-properties

* Sat Dec 4 1999 Hidetomo Machi <mcHT@kondara.org>
- added kondara icon for kondara setup page

* Fri Nov 26 1999 Shingo Akagaki <dora@kondara.org>
- bug fix kondara setup page

* Wed Nov 24 1999 Shingo Akagaki <dora@kondara.org>
- bug fix kondara setup page

* Wed Nov 24 1999 Shingo Akagaki <dora@kondara.org>
- Removed XIM select page
- Added Kondara setup page

* Wed Oct 27 1999 Shingo Akagaki <dora@kondara.org>
- Added XIM select page
- version 1.0.51

* Fri Sep 17 1999 Jonathan Blandford <jrb@redhat.com>
- Added fixrevert bug to fix bug in theme selector.

* Sun Jun 13 1999 Jonathan Blandford <jrb@redhat.com>
- updated RPM to use new control-center.

* Wed Apr 07 1999 Michael Fulbright <drmike@redhat.com>
- fixed sound-properties to only disable sound when run with --init-settings-..
- removed debugging output from several capplets, fixed try behaviour of sm and
  gnome-edit capplets
- fixed bug in screensaver and background props
- added new icons

* Mon Apr 05 1999 Jonathan Blandford <jrb@redhat.com>
- added a patch to fix the close dialog
- added a patch to limit the number of bg's in the history.

* Fri Apr 02 1999 Jonathan Blandford <jrb@redhat.com>
- vesion 1.0.5
- removed all patches >10 other then dontstartesd.

* Thu Apr 01 1999 Michael Fulbright <drmike@redhat.com>
- removed UI props till it works better

* Wed Mar 31 1999 Michael Fulbright <drmike@redhat.com>
- make sure we DONT inadvertantly start esd by calling esd_open_...

* Tue Mar 30 1999 Michael Fulbright <drmike@redhat.com>
- changed default bg color to '#356390'

* Thu Mar 25 1999 Michael Fulbright <drmike@redhat.com>
- prime file selector path for browse in background-props if
  "/usr/share/pixmaps/backgrounds/" exists.
- fix behavior of file selector when you delete/cancel/ok it

* Wed Mar 24 1999 Michael Fulbright <drmike@redhat.com>
- added patch to fix trying in theme selector
- disabled crystal screensaver, it does evil things to preview in capplet

* Mon Mar 22 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.4, fixes problems with sndprops and theme props among
  other things.

* Thu Mar 18 1999 Michael Fulbright <drmike@redhat.com>
- fix sound-properties capplet so Try/Revert doesnt come on unless user
  changes something
- fixed theme-selector to not leave processes behind on Linux 2.2 kernels
- strip binaries

* Sun Mar 14 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.3
- added patch to make esd release after 30 sec of inactivity

* Wed Mar 10 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.2
- turned off sound by default

* Thu Mar 04 1999 Michael Fulbright <drmike@redhat.com>
- version 1.0.1

* Mon Feb 15 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.8.1
- added etc/CORBA/servers/* to file list

* Fri Feb 12 1999 Michael Fulbright <drmike@redhat.com>
- update to 0.99.8
- added /usr/lib/cappletConf.sh

* Mon Feb 08 1999 The Rasterman <raster@redhat.com>
- update to 0.99.5.1

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- update to 0.99.5

* Mon Jan 20 1999 Michael Fulbright <drmike@redhat.com>
- update to 0.99.3.1

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- update to 0.99.3
- seems like patch for non-standard xscreensaver placement was already in
  prestine sources(?)

* Wed Jan 06 1999 Jonathan Blandford <jrb@redhat.com>
- updated to 0.99.1
- temporary hack patch to get path to work to non-standard placement
  of xscreensaver binaries in RH 5.2

* Wed Dec 16 1998 Jonathan Blandford <jrb@redhat.com>
- Created for the new control-center branch
