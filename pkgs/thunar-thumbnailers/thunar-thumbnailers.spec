%global momorel 9

%global thunarver 1.5.0

Name:		thunar-thumbnailers
Version:	0.4.1
Release:	%{momorel}m%{?dist}
Summary:	Thumbnailers plugin for the Thunar file manager

Group:		Applications/Multimedia
License:	GPL
Source0:	http://archive.xfce.org/src/apps/%{name}/0.4/%{name}-%{version}.tar.bz2
NoSource:	0
URL:		http://goodies.xfce.org/projects/thunar-plugins/%{name}
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	autoconf >= 2.50
BuildRequires:	automake >= 1.8
# Requires(post,postun):	shared-mime-info
Requires:	Thunar >= %{thunarver}
Requires:	ImageMagick
Requires:	dcraw
# Suggests:	ffmpegthumbnailer
# Suggests:	grace
# Suggests:	raw-thumbnailer
# Suggests:	tetex-format-latex

%description
Thunar-thumbnailers provides additional thumbnailers for use by the
Thunar file manager.

%prep
%setup -q -n %{name}-%{version}

%build
%configure \
	DCRAW=/usr/bin/dcraw \
	FFMPEG=/usr/bin/ffmpegthumbnailer \
	GRACE=/usr/bin/gracebat \
	LATEX=/usr/bin/latex \
	RAWTHUMBNAILER=/usr/bin/raw-thumbnailer \
	--enable-raw \
	--enable-tex \
	--enable-grace \
	--enable-ffmpeg \
	--disable-update-mime-database \
	--libexecdir=%{_libdir}
%{__make}

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot} transform='s,x,x,'

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
/sbin/ldconfig
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files
%defattr(644,root,root,755)
%doc AUTHORS ChangeLog README
%attr(755,root,root) %{_libdir}/agr-thumbnailer
%attr(755,root,root) %{_libdir}/dvi-thumbnailer
%attr(755,root,root) %{_libdir}/eps-thumbnailer
%attr(755,root,root) %{_libdir}/ffmpeg-thumbnailer
%attr(755,root,root) %{_libdir}/fig-thumbnailer
%attr(755,root,root) %{_libdir}/odf-thumbnailer
%attr(755,root,root) %{_libdir}/ogg-thumbnailer
%attr(755,root,root) %{_libdir}/pdf-thumbnailer
%attr(755,root,root) %{_libdir}/ps-thumbnailer
%attr(755,root,root) %{_libdir}/psd-thumbnailer
%attr(755,root,root) %{_libdir}/raw-thumbnailer
%attr(755,root,root) %{_libdir}/svgz-thumbnailer
%attr(755,root,root) %{_libdir}/tex-thumbnailer
%attr(755,root,root) %{_libdir}/xcf-thumbnailer
%{_datadir}/mime/packages/thunar-thumbnailers.xml
%{_datadir}/thumbnailers/agr-thumbnailer.desktop
%{_datadir}/thumbnailers/dvi-thumbnailer.desktop
%{_datadir}/thumbnailers/eps-thumbnailer.desktop
%{_datadir}/thumbnailers/ffmpeg-thumbnailer.desktop
%{_datadir}/thumbnailers/fig-thumbnailer.desktop
%{_datadir}/thumbnailers/odf-thumbnailer.desktop
%{_datadir}/thumbnailers/ogg-thumbnailer.desktop
%{_datadir}/thumbnailers/pdf-thumbnailer.desktop
%{_datadir}/thumbnailers/ps-thumbnailer.desktop
%{_datadir}/thumbnailers/psd-thumbnailer.desktop
%{_datadir}/thumbnailers/raw-thumbnailer.desktop
%{_datadir}/thumbnailers/svgz-thumbnailer.desktop
%{_datadir}/thumbnailers/tex-thumbnailer.desktop
%{_datadir}/thumbnailers/xcf-thumbnailer.desktop


%changelog
* Mon Oct 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-9m)
- BR: Thunar-devel >= 1.5.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.1-8m)
- rebuild against Thunar 1.5.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.1-7m)
- rebuild against xfce4-4.10.0

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-6m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-5m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-1m)
- update
- rebuild against xfce4 4.6.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-6m)
- good-bye autotools

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-2m)
- rebuild against xfce4 4.6.0, Thunar 1.0.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-1m)
- import to Momonga from PLD

#* %%{date} PLD Team <feedback@pld-linux.org>
#All persons listed below can be reached at <cvs_login>@pld-linux.org

$Log: Thunar-thumbnailers.spec,v $
Revision 1.5  2007-08-25 09:24:41  qboosh
- don't BR thumbnailers

Revision 1.4  2007/08/16 19:00:37  czarny
- up to 0.3.0
- Patch0 not needed anymore
- new thunbmnailers from Suggested raw-thumbnailer

Revision 1.3  2007/07/27 20:25:09  megabajt
- updated to 0.2.2

Revision 1.2  2007/04/16 12:51:25  qboosh
- pl

Revision 1.1  2007/04/05 20:27:33  megabajt
- new
