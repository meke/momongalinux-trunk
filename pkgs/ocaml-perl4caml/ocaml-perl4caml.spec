%global momorel 25
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-perl4caml
Version:        0.9.5
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for calling Perl libraries and code

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"
URL:            http://merjis.com/developers/perl4caml
Source0:        http://merjis.com/_file/perl4caml-%{version}.tar.gz
#NoSource:       0
Patch0:         perl4caml-0.9.5-perl512.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-ocamldoc
BuildRequires:  perl-devel >= 5.10.0

# Perl4caml provides type-safe wrappers for these Perl modules:
#Requires:  perl-Date-Calc
##Requires:  perl-Date-Format
##Requires:  perl-Date-Parse
##Requires:  perl-Net-Google
##Requires:  perl-HTML-Element
#Requires:  perl-HTML-Parser
#Requires:  perl-HTML-Tree
#Requires:  perl-libwww-perl
#Requires:  perl-Template-Toolkit
#Requires:  perl-URI
#Requires:  perl-WWW-Mechanize

# Because we're embedding an rpath into the library, we need to
# depend on that path.  See:
# https://www.redhat.com/archives/fedora-packaging/2008-March/thread.html#00070
# https://www.redhat.com/archives/fedora-devel-list/2008-March/msg00922.html
%global libperldir %(perl -MExtUtils::Embed -e ldopts 2> /dev/null | egrep -o '/[^[:space:]]*CORE' | head -1)
%if "%{?libperldir}" != "%{nil}"
#Requires: %{?libperldir}/libperl.so
Requires: perl
%endif

# We're also going to pick up a versioned dependency, to help track things:
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))


%description
Perl4caml allows you to use Perl code within Objective CAML (OCaml),
thus neatly side-stepping the (old) problem with OCaml which was that
it lacked a comprehensive set of libraries. Well now you can use any
part of CPAN in your OCaml code.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n perl4caml-%{version}
%patch0 -p1 -b .perl512
find -name .cvsignore -exec rm {} \;


%build
make EXTRA_EXTRA_CFLAGS="$RPM_OPT_FLAGS"
rm -f examples/*.{cmi,cmo,cmx,o,bc,opt}


%check
# Set the library path used by ocamlrun so it uses the library
# we just built in the current directory.
CAML_LD_LIBRARY_PATH=`pwd` make test


%install
rm -rf $RPM_BUILD_ROOT

export DESTDIR=$RPM_BUILD_ROOT
mkdir -p $DESTDIR/%{_libdir}/ocaml/stublibs

make install

strip $DESTDIR/%{_libdir}/ocaml/stublibs/dll*.so

# Don't delete rpath!  See:
# https://www.redhat.com/archives/fedora-packaging/2008-March/thread.html#00070


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_libdir}/ocaml/perl
%if %opt
%exclude %{_libdir}/ocaml/perl/*.a
%exclude %{_libdir}/ocaml/perl/*.cmxa
%endif
%exclude %{_libdir}/ocaml/perl/*.mli
%exclude %{_libdir}/ocaml/perl/*.ml
%{_libdir}/ocaml/stublibs/*.so


%files devel
%defattr(-,root,root,-)
%doc COPYING.LIB AUTHORS doc/* examples html README
%if %opt
%{_libdir}/ocaml/perl/*.a
%{_libdir}/ocaml/perl/*.cmxa
%endif
%{_libdir}/ocaml/perl/*.mli
%{_libdir}/ocaml/perl/*.ml


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-25m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-24m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-23m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-22m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-21m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-20m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-19m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-18m)
- rebuild against perl-5.16.0

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-17m)
- rebuild against ocaml-3.12.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.5-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-9m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-8m)
- rebuild against perl-5.12.0

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-7m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-5m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-4m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-3m)
- rebuild against ocaml-3.11.0

* Wed Jul 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.5-2m)
- modify Requires

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.5-5
- Rebuild for OCaml 3.10.2.

* Tue Mar 18 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.9.5-4
- add Requires for versioned perl (libperl.so)

* Wed Mar 12 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.5-3
- Fix %check rule (#436785).
- Use rpath for dllperl4caml.so as per this thread:
  https://www.redhat.com/archives/fedora-packaging/2008-March/thread.html#00070
  (#436807).
- Require rpath.

* Tue Mar  4 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.5-2
- Rebuild for ppc64.

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.5-1
- New upstream release 0.9.5.
- Clarify license is LGPLv2+ with exceptions
- Remove excessive BuildRequires - Perl modules not needed for building.
- Pass RPM C flags to the make.
- 'make test' fails where perl4caml is already installed.

* Sat Feb 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.4-1
- Initial RPM release.
