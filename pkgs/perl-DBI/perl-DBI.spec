%global         momorel 2

Name:           perl-DBI
Version:        1.631
Release:        %{momorel}m%{?dist}
Summary:        Database independent interface for Perl
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBI/
Source0:        http://www.cpan.org/authors/id/T/TI/TIMB/DBI-%{version}.tar.gz
NoSource:       0
Source1:        filter-requires-dbi.sh
Patch0:         DBI-1.20-win32odbc.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%{?include_specopt}
%{?!with_SQL_Statement: %global with_SQL_Statement 1}

BuildRequires:  perl >= 5.008
BuildRequires:  perl-Clone >= 0.31
BuildRequires:  perl-DB_File
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MLDBM
BuildRequires:  perl-Net-Daemon
BuildRequires:  perl-PlRPC >= 0.2001
%if 0%{with_SQL_Statement}
BuildRequires:  perl-SQL-Statement >= 1.28
%endif
BuildRequires:  perl-Test-Simple >= 0.90
Requires:       perl-Clone >= 0.31
Requires:       perl-DB_File
Requires:       perl-MLDBM
Requires:       perl-Net-Daemon
Requires:       perl-PlRPC >= 0.2001
%if 0%{with_SQL_Statement}
Requires:       perl-SQL-Statement >= 1.28
%endif
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?!do_test: %global do_test 1}

%description
The DBI is a database access module for the Perl programming language.
It defines a set of methods, variables, and conventions that provide a
consistent database interface, independent of the actual database
being used.

%package apache
Summary: capture DBI profiling data from Apache/mod_perl
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: mod_perl

%description apache
DBI::ProfileDumper::Apache - capture DBI profiling data from Apache/mod_perl

# Provide perl-specific find-{provides,requires}.
%define __find_provides /usr/lib/rpm/find-provides.perl
# %%define __find_requires /usr/lib/rpm/find-requires.perl
%define __find_requires %{SOURCE1}

%prep
%setup -q -n DBI-%{version}
%patch0 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

(
  cd %{buildroot}%{perl_vendorarch}/Win32
  ln -s DBIODBC.pm ODBC.pm
)

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}
rm -f /tmp/dbiproxy.pid

%files
%defattr(-,root,root)
%doc Changes README*
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man3/Bundle*
%{_mandir}/man3/DBD*
%{_mandir}/man3/DBI.*
%{_mandir}/man3/DBI::Const*
%{_mandir}/man3/DBI::DBD*
%{_mandir}/man3/DBI::FAQ*
%{_mandir}/man3/DBI::Gofer*
%{_mandir}/man3/DBI::Profile.*
%{_mandir}/man3/DBI::ProfileData.*
%{_mandir}/man3/DBI::ProfileDumper.*
%{_mandir}/man3/DBI::ProfileSubs.*
%{_mandir}/man3/DBI::ProxyServer*
%{_mandir}/man3/DBI::PurePerl*
%{_mandir}/man3/DBI::SQL*
%{_mandir}/man3/DBI::Util*
%{_mandir}/man3/DBI::W32ODBC*
%{_mandir}/man3/Win32*
%{perl_vendorarch}/Bundle/DBI.pm
%{perl_vendorarch}/DBD
%{perl_vendorarch}/DBI.pm
%{perl_vendorarch}/dbixs_rev.pl
%dir %{perl_vendorarch}/DBI
%{perl_vendorarch}/DBI/*.pm
%{perl_vendorarch}/DBI/Const
%{perl_vendorarch}/DBI/DBD
%dir %{perl_vendorarch}/DBI/ProfileDumper
%{perl_vendorarch}/DBI/SQL
%{perl_vendorarch}/Win32/*.pm
%{perl_vendorarch}/auto/DBI
%dir %{perl_vendorarch}/DBI/Gofer
%{perl_vendorarch}/DBI/Gofer/*.pm
%{perl_vendorarch}/DBI/Gofer/Transport
%{perl_vendorarch}/DBI/Gofer/Serializer
%dir %{perl_vendorarch}/DBI/Util
%{perl_vendorarch}/DBI/Util/*.pm

%files apache
%defattr(-,root,root)
%{_mandir}/man3/DBI::ProfileDumper::Apache.*
%{perl_vendorarch}/DBI/ProfileDumper/Apache.pm

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.631-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.631-1m)
- update to 1.631
- rebuild against perl-5.18.2

* Mon Oct 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.630-1m)
- update to 1.630

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.628-2m)
- rebuild against perl-5.18.1

* Thu Jul 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.628-1m)
- update to 1.628

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.627-2m)
- rebuild against perl-5.18.0

* Fri May 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.627-1m)
- update to 1.627

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.626-1m)
- update to 1.626

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.625-1m)
- update to 1.625

* Sat Mar 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.624-1m)
- update to 1.624

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.623-2m)
- rebuild against perl-5.16.3

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.623-1m)
- update to 1.623

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.622-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.622-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.622-1m)
- update to 1.622
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.618-1m)
- update to 1.618

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.616-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.616-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.616-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.616-2m)
- rebuild for new GCC 4.6

* Thu Dec 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.616-1m)
- update to 1.616

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.615-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.615-2m)
- rebuild against perl-5.12.2

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.615-1m)
- update to 1.615

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.614-1m)
- update to 1.614

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.611-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.611-3m)
- version down to 1.611
- SQL::Statement conflicts with DBI > 1.611

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.613-1m)
- update to 1.613

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.612-1m)
- update to 1.612

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.611-2m)
- rebuild against perl-5.12.1

* Fri Apr 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.611-1m)
- update to 1.611

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.609-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.609-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.609-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.609-1m)
- update to 1.609

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.607-5m)
- rebuild against perl-5.10.1

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.607-4m)
- version down to 1.607, perl-SQL-Statement could be built

* Wed May  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.608-1m)
- update to 1.608

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.607-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.607-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Wed Jul 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.607-1m)
- update to 1.607

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.605-1m)
- update to 1.605

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.604-2m)
- rebuild against gcc43

* Wed Mar 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.604-1m)
- update to 1.604

* Sun Mar 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.603-1m)
- update to 1.603

* Sun Feb 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.602-1m)
- update to 1.602

* Mon Oct 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.601-1m)
- update to 1.601

* Fri Aug 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.59-1m)
- update to 1.59
- delete BuildRequires: perl-PlRPC

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.58-1m)
- udate to 1.58

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.56-1m)
- update to 1.56

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.54-2m)
- use vendor

* Sun Feb 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-1m)
- update to 1.54

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.53-1m)
- update to 1.53

* Mon Aug 28 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (1.52-1m)
- update to 1.52

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.51-1m)
- update to 1.51

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.50-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi Narita <pulsar@sea.plala.or.jp>
- (1.50-1m)
- update to 1.50

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.48-2m)
- rebuilt against perl-5.8.7

* Thu May  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.48-1m)
- update to 1.48
- revise %%files
- add Requires: mod_perl to perl-DBI-apache package

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.42-4m)
- rebuild against perl-5.8.5
- rebuild against perl-PlRPC 0.2016-8m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.42-3m)
- remove Epoch from BuildRequires

* Sun Jul  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.42-2m)
- make perl-DBI-apache package

* Tue Apr 20 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.42-1m)
- update to 1.42
- revise %%files

* Tue Jan 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.40-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.38-3m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.38-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Sep  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.38-1m)

* Sun Apr 13 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Sat Jan 4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.32-1m)
- update to 1.32
- why need LANG=C when make test?

* Mon Dec  2 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.31-1m)
- update to 1.31

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.30-2m)
- rebuild against perl-5.8.0

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.30-1m)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.28-2m)
- remove BuildRequires: gcc2.95.3
- add BuildRequires: perl-PlRPC

* Sat Jul  6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.28-1m)
- remove a temporary file at %clean section

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (1.21-2k)
- ver up.

* Fri Mar  1 2002 Shingo Akagaki <dora@kondara.org>
- (1.20-8k)
- add win32odbc.patch

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.20-6k)
- rebuild against for perl-5.6.1

* Mon Sep 17 2001 Toru Hoshina <toru@df-usa.com>
- (1.20-4k)
- no more fixpack...

* Fri May  4 2001 Toru Hoshina <toru@df-usa.com>
- (1.14-12k)
- rebuild by gcc 2.95.3 :-P

* Tue Nov 27 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- use perl_sitearch macro

* Thu Oct 12 2000 Motonobu Ichimura <famao@kondara.org>
- fixed Group Name ;-<

* Fri Aug 18 2000 Toru Hoshina <t@kondara.org>
- rebuild against perl 5.600.

* Thu Jul 20 2000 Toru Hoshina <t@kondara.org>
- merge from Mandrake.

* Fri Jul 07 2000 Chmouel Boudjnah <chmouel@mandrakesoft.com> 1.13-4mdk
- FIx build as user.

* Wed May 17 2000 David BAUDENS <baudens@mandrakesoft.com> 1.13-3mdk
- Fix build for i486

* Mon Apr  3 2000 Guillaume Cottenceau <gc@mandrakesoft.com> 1.13-2mdk
- fixed group
- rebuild with new perl
- fixed location

* Sun Dec 05 1999 Axalon Bloodstone <axalon@linux-mandrake.com>
- First specfile
