%global momorel 1

Name:           python-ipaddr
Version:        2.1.10
Release:        %{momorel}m%{?dist}
Summary:        A python library for working with IP addresses, both IPv4 and IPv6

Group:          Development/Languages
License:        ASL 2.0
URL:            http://code.google.com/p/ipaddr-py/
Source0:        http://ipaddr-py.googlecode.com/files/ipaddr-%{version}.tar.gz
NoSource:       0

BuildArch:      noarch
BuildRequires:  python-devel

%description
python-ipaddr is a library for working with IP addresses, both IPv4 and IPv6.
It was developed by Google for internal use, and is now open source.

%prep
%setup -q -n ipaddr-%{version}
# remove unneeded shebang
sed -i 1d ipaddr.py

%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING README RELEASENOTES
%{python_sitelib}/*


%changelog
* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.10-1m)
- Initial commit Momonga Linux
- need virt-manager

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.10-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed Aug 07 2013 Adam Williamson <awilliam@redhat.com> - 2.1.10-1
- latest upstream bugfix release, modernize spec

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.9-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.9-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.9-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.9-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 23 2011 L.S. Keijser <keijser@stone-it.com> - 2.1.9-1
- new version from upstream

* Fri Feb 11 2011 L.S. Keijser <keijser@stone-it.com> - 2.1.8-1
- new version from upstream

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri Jan 14 2011 L.S. Keijser <keijser@stone-it.com> - 2.1.7-1
- new version from upstream

* Tue Sep 14 2010 L.S. Keijser <keijser@stone-it.com> - 2.1.5-1
- new version from upstream (included releasenotes again)

* Sat Aug 21 2010 L.S. Keijser <keijser@stone-it.com> - 2.1.4-2
- upstream didn't include RELEASENOTES file

* Tue Aug 17 2010 L.S. Keijser <keijser@stone-it.com> - 2.1.4-1
- new version from upstream

* Fri Jul 30 2010 L.S. Keijser <keijser@stone-it.com> - 2.1.3-2
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Tue Jun 15 2010 L.S. Keijser <keijser@stone-it.com> - 2.1.3-1
- new version from upstream
- added releasenotes file

* Tue Jun 01 2010 L.S. Keijser <keijser@stone-it.com> - 2.1.2-1
- new version from upstream

* Thu May 06 2010 L.S. Keijser <keijser@stone-it.com> - 2.1.1-1
- initial release

