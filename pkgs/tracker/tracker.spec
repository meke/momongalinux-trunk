%global momorel 3
%global with_evo 0

%global firefox_plugindir %{_libdir}/firefox/extensions
%global thunderbird_plugindir %{_libdir}/thunderbird/extensions

Summary:	An object database, tag/metadata database, search tool and indexer
Name:		tracker
Version:	0.14.5
Release:	%{momorel}m%{?dist}
License:	GPLv2+
Group:		Applications/System
URL:		http://projects.gnome.org/tracker/
Source0:	http://ftp.gnome.org/pub/GNOME/sources/tracker/0.14/tracker-%{version}.tar.xz
NoSource:	0
Source1:	http://git.gnome.org/browse/tracker/plain/docs/manpages/tracker-search-bar.1
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	poppler-glib-devel libxml2-devel libgsf-devel
%if %{with_evo}
BuildRequires:  evolution-devel
%endif
BuildRequires:	libuuid-devel dbus-glib-devel
BuildRequires:	nautilus-devel
BuildRequires:	libjpeg-devel libexif-devel exempi-devel raptor-devel
BuildRequires:	libiptcdata-devel libtiff-devel libpng-devel giflib-devel
BuildRequires:	sqlite-devel vala-devel libgee-devel
BuildRequires:  gstreamer-plugins-base-devel gstreamer-devel id3lib-devel
BuildRequires:	totem-pl-parser-devel libvorbis-devel flac-devel enca-devel
BuildRequires:	upower-devel gnome-keyring-devel NetworkManager-glib-devel
BuildRequires:	libunistring-devel gupnp-dlna-devel taglib-devel rest-devel
BuildRequires:	libicu-devel >= 52
BuildRequires:	libosinfo-devel libcue-devel
BuildRequires:	firefox thunderbird
BuildRequires:	gdk-pixbuf2-devel
BuildRequires:	desktop-file-utils intltool gettext
BuildRequires:	gtk-doc graphviz dia
BuildRequires:	gobject-introspection
Obsoletes: %{name}-search-tool

%description
Tracker is a powerful desktop-neutral first class object database,
tag/metadata database, search tool and indexer. 

It consists of a common object database that allows entities to have an
almost infinte number of properties, metadata (both embedded/harvested as
well as user definable), a comprehensive database of keywords/tags and
links to other entities.

It provides additional features for file based objects including context
linking and audit trails for a file object.

It has the ability to index, store, harvest metadata. retrieve and search
all types of files and other first class objects

%package devel
Summary:	Headers for developing programs that will use %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig
Requires:	dbus-glib-devel gtk2-devel

%description devel
This package contains the static libraries and header files needed for
developing with tracker

%package ui-tools
Summary:	Tracker search tool(s)
Group:		User Interface/Desktops
Requires:	%{name} = %{version}-%{release}
Obsoletes:	paperbox <= 0.4.4
Obsoletes:	tracker-search-tool <= 0.12.0

%description ui-tools
Graphical frontend to tracker search (tracker-needle) and configuration
(tracker-preferences) facilities. This also contains A test tool to navigate
around objects in the database based on their relationships (tracker-explorer)

%if %{with_evo}
%package evolution-plugin
Summary:	Tracker's evolution plugin
Group:		User Interface/Desktops
Requires:	%{name} = %{version}-%{release}

%description evolution-plugin
Tracker's evolution plugin
%endif

%package firefox-plugin
Summary:	A simple bookmark exporter for Tracker
Group:		User Interface/Desktops
Requires:	%{name} = %{version}-%{release}

%description firefox-plugin
This Firefox addon exports your bookmarks to Tracker, so that you can search
for them for example using tracker-needle.

%package nautilus-plugin
Summary:	Tracker's nautilus plugin
Group:		User Interface/Desktops
Requires:	%{name} = %{version}-%{release}

%description nautilus-plugin
Tracker's nautilus plugin, provides 'tagging' functionality. Ability to perform
search in nuautilus using tracker is built-in directly in the nautilus package.

%package miner-flickr
Summary:	Tracker's Flickr data miner
Group:		User Interface/Desktops
Requires:	%{name} = %{version}-%{release}

%description miner-flickr
Tracker's Flickr data miner.

%package thunderbird-plugin
Summary:	Thunderbird extension to export mails to Tracker
Group:		User Interface/Desktops
Requires:	%{name} = %{version}-%{release}

%description thunderbird-plugin
A simple Thunderbird extension to export mails to Tracker.

%package docs
Summary:	Documentations for tracker
Group:		Documentation
BuildArch:      noarch

%description docs
This package contains the documentation for tracker

%prep
%setup -q

%if %{with_evo}
%global evo_plugins_dir %(pkg-config evolution-plugin-3.0 --variable=plugindir)
%endif

## nuke unwanted rpaths, see also
## https://fedoraproject.org/wiki/Packaging/Guidelines#Beware_of_Rpath
sed -i -e 's|"/lib /usr/lib|"/%{_lib} %{_libdir}|' configure

%build

%if %{with_evo}
%global evo_flag --enable-miner-evolution
%else
%global evo_flag --disable-miner-evolution
%endif

%configure --disable-static		\
	--disable-tracker-search-bar	\
	--enable-gtk-doc		\
	--with-firefox-plugin-dir=%{_libdir}/firefox/extensions		\
	--with-thunderbird-plugin-dir=%{_libdir}/thunderbird/extensions	\
	%evo_flag \
	--disable-functional-tests
# Disable the functional tests for now, they use python bytecodes.

make V=1 %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/tracker-0.14"	\
	> %{buildroot}%{_sysconfdir}/ld.so.conf.d/tracker-%{_arch}.conf

desktop-file-install --delete-original			\
	--vendor=					\
	--dir=%{buildroot}%{_datadir}/applications	\
	%{buildroot}%{_datadir}/applications/%{name}-needle.desktop

find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'
rm -rf %{buildroot}%{_datadir}/tracker-tests

%find_lang %{name}

%post -p /sbin/ldconfig

%post ui-tools
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%postun ui-tools
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%files -f %{name}.lang
%defattr(-, root, root, -)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_bindir}/tracker*
%{_libexecdir}/tracker*
%{_datadir}/tracker/
%{_datadir}/dbus-1/services/org.freedesktop.Tracker*
%{_libdir}/*.so.*
%{_libdir}/tracker-0.14/
%{_libdir}/girepository-1.0/Tracker-0.14.typelib
%{_libdir}/girepository-1.0/TrackerExtract-0.14.typelib
%{_libdir}/girepository-1.0/TrackerMiner-0.14.typelib
%{_mandir}/*/tracker*.*
%{_sysconfdir}/ld.so.conf.d/tracker-%{_arch}.conf
%{_sysconfdir}/xdg/autostart/tracker*.desktop
%{_datadir}/glib-2.0/schemas/*
%exclude %{_bindir}/tracker-needle
%exclude %{_bindir}/tracker-preferences
%exclude %{_mandir}/man1/tracker-preferences.1.*
%exclude %{_mandir}/man1/tracker-needle.1.*
%exclude %{_libexecdir}/tracker-miner-flickr

%files devel
%defattr(-, root, root, -)
%{_includedir}/tracker-0.14/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/vala/vapi/tracker*.*
%{_datadir}/gir-1.0/Tracker-0.14.gir
%{_datadir}/gir-1.0/TrackerExtract-0.14.gir
%{_datadir}/gir-1.0/TrackerMiner-0.14.gir

%files ui-tools
%defattr(-, root, root, -)
%{_bindir}/tracker-needle
%{_bindir}/tracker-preferences
%{_datadir}/icons/*/*/apps/tracker.*
%{_datadir}/applications/*.desktop
%{_mandir}/man1/tracker-preferences.1.*
%{_mandir}/man1/tracker-needle.1.*
%exclude %{_datadir}/applications/trackerbird-launcher.desktop

%if %{with_evo}
%files evolution-plugin
%defattr(-, root, root, -)
%{evo_plugins_dir}/liborg-freedesktop-Tracker-evolution-plugin.so
%{evo_plugins_dir}/org-freedesktop-Tracker-evolution-plugin.eplug
%endif

%files firefox-plugin
%defattr(-, root, root, -)
%{_datadir}/xul-ext/trackerfox/
%{_libdir}/firefox/extensions/trackerfox@bustany.org

%files nautilus-plugin
%defattr(-, root, root, -)
%{_libdir}/nautilus/extensions-3.0/libnautilus-tracker-tags.so

%files miner-flickr
%defattr(-, root, root, -)
%{_libexecdir}/tracker-miner-flickr

%files thunderbird-plugin
%defattr(-, root, root, -)
%{_datadir}/xul-ext/trackerbird/
%{_libdir}/thunderbird/extensions/trackerbird@bustany.org
%{_datadir}/applications/trackerbird-launcher.desktop

%files docs
%defattr(-, root, root, -)
%doc docs/reference/COPYING
%{_datadir}/gtk-doc/html/libtracker-miner/
%{_datadir}/gtk-doc/html/libtracker-extract/
%{_datadir}/gtk-doc/html/libtracker-sparql/
%{_datadir}/gtk-doc/html/ontology/

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.14.5-3m)
- rebuild against graphviz-2.36.0-1m

* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.5-2m)
- rebuild against icu-52

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.5-1m)
- update to 0.14.5
- remove fedora from vendor (desktop-file-install)

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.4-1m)
- update 0.14.4

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.2-1m)
- reimport from fedora
- temporary disable evolution miner

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.9-6m)
- rebuild for evolution-data-server-devel-3.5.3

* Sat Jun 30 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.9-5m)
- fix build with evolution-3.5.2

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.9-4m)
- fix build failure with glib 2.33+
- rebuild against evolution-data-server-3.5.x
- temporary disable evolution data miner

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.9-3m)
- rebuild against libtiff-4.0.1

* Wed Jan 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.9-2m)
- add BuildRequires

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.9-1m)
- update 0.12.9

* Tue Dec 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.8-2m)
- fix firefox/thunderbird extension support

* Fri Dec  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.8-1m)
- update to 0.12.8

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.7-2m)
- rebuild against poppler-0.18.2

* Sun Nov  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.7-1m)
- update to 0.12.7

* Sat Oct 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.6-1m)
- update to 0.12.6

* Fri Oct 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.5-1m)
- update to 0.12.5

* Sat Oct  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.4-1m)
- update to 0.12.4

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.3-2m)
- rebuild against poppler-0.18.0

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.3-1m)
- update to 0.12.3

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.0-2m)
- fix build failure on x86_64

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Thu Aug 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.20-3m)
- add BuildRequires

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.20-2m)
- rebuild against icu-4.6

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.20-1m)
- update to 0.10.20

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.18-1m)
- update to 0.10.18

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.13-1m)
- update to 0.10.13

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-3m)
- rebuild against gupnp-dlna-0.6.1

* Tue May  3 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-2m)
- rebuild against NetworkManager-0.8.998

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-1m)
- update to 0.10.11
- delete search-tool

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.17-7m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.17-6m)
- rebuild against poppler-0.16.4

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.17-5m)
- add patch1 (enable to build with new poppler-0.16.0)

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.17-4m)
- rebuild against poppler-0.16.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.17-3m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.17-3m)
- rebuild against upower-0.9.7

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.17-2m)
- rebuild against evolution-2.32.0 (add patch0)
- rebuild against gtkhtml3-3.32.0

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.17-1m)
- update to 0.8.17

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.15-3m)
- rebuild against poppler-0.14.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.15-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.15-1m)
- update to 0.8.15

* Fri Jun 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.13-1m)
- update to 0.8.13

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.12-3m)
- rebuild against evolution-2.30.2

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.12-2m)
- vapi and gir to devel

* Fri Jun 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.12-1m)
- update to 0.8.12

* Sat Jun 12 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.11-1m)
- update to 0.8.11

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.10-3m)
- rebuild against poppler-0.14.0

* Mon Jun  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.10-2m)
- add BuildRequires and --enable-tracker-explorer

* Fri Jun  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.10-1m)
- update to 0.8.10

* Tue Jun  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.95-9m)
- change BuildRequires: from poppler-devel to poppler-glib-devel

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.95-8m)
- rebuild against libjpeg-8a

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.95-7m)
- rebuild against totem-pl-parser-2.29.1

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.95-6m)
- rebuild against gnome-desktop-2.29.90

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.95-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.95-4m)
- rebuild against libjpeg-7

* Fri Aug 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.95-3m)
- rebuild against gmime22

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.95-2m)
- fix BuildRequires

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.95-1m)
- update to 0.6.95

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-7m)
- do not autostart in GNOME

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-6m)
- rebuild against gnome-desktop-2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-5m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.6-4m)
- fix force bz2 man

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-3m)
- rebuild against gnome-desktop-2.24.0

* Mon May 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.6-2m)
- remove vendor="fedora" from desktop file

* Sun May 18 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.6-1m)
- import from Fedora

* Fri Mar 14 2008 Deji Akingunola <dakingun@gmail.com> - 0.6.6-2
- BR poppler-glib-devel instead of poppler-devel for pdf extract module (Thanks to Karsten Hopp mass rebuild work for bringing this to light)

* Sun Mar 02 2008 Deji Akingunola <dakingun@gmail.com> - 0.6.6-1
- New release 0.6.6

* Thu Feb 28 2008 Deji Akingunola <dakingun@gmail.com> - 0.6.5-1
- New release 0.6.5

* Fri Feb 22 2008 Deji Akingunola <dakingun@gmail.com> - 0.6.4-7
- Ship the tracker-applet program in the tracker-search-tool subpackage
  (Bug #434551)

* Sun Feb 10 2008 Deji Akingunola <dakingun@gmail.com> - 0.6.4-6
- Rebuild for gcc43

* Thu Jan 24 2008 Deji Akingunola <dakingun@gmail.com> - 0.6.4-5
- Backport assorted fixes from upstream svn (Fix Fedora bug 426060)

* Mon Jan 21 2008 Deji Akingunola <dakingun@gmail.com> - 0.6.4-4
- Now require the externally packaged o3read to provide o3totxt

* Fri Dec 14 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.4-3
- Undo the patch, seems to be issues (bug #426060)

* Fri Dec 14 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.4-2
- Backport crasher fixes from upstream svn trunk

* Mon Dec 11 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.4-1
- Version 0.6.4

* Tue Dec 04 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.3-3
- Rebuild for exempi-1.99.5

* Sun Nov 25 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.6.3-2
- Add missing gtk+ icon cache scriptlets.

* Tue Sep 25 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.3-1
- Version 0.6.3

* Tue Sep 11 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.2-2
- Make trackerd start on x86_64 (Bug #286361, fix by Will Woods)

* Wed Sep 05 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.2-1
- Version 0.6.2

* Sat Aug 25 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.1-2
- Rebuild

* Wed Aug 08 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.1-1
- Update to 0.6.1

* Fri Aug 03 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.0-3
- License tag update

* Wed Jul 25 2007 Jeremy Katz <katzj@redhat.com> - 0.6.0-2.1
- rebuild for toolchain bug

* Mon Jul 23 2007 Deji Akingunola <dakingun@gmail.com> - 0.6.0-1
- Update to 0.6.0
- Manually specify path to deskbar-applet handler directory, koji can't find it

* Mon Jan 29 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.4-2
- Split out tracker-search-tool sub-packages, for the GUI facility
- Add proper requires for the -devel subpackage
- Deal with the rpmlint complaints on rpath

* Sat Jan 27 2007 Deji Akingunola <dakingun@gmail.com> - 0.5.4-1
- Update to 0.5.4

* Tue Dec 26 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.3-1
- Update to 0.5.3

* Mon Nov 27 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.2-2
- Apply patch on Makefile.am instead of Makefile.in
- Add libtool to BR

* Mon Nov 06 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.2-1
- Update to 0.5.2

* Mon Nov 06 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.1-1
- Update to new version

* Mon Nov 06 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.0-7
- Have the devel subpackage require pkgconfig
- Make the description field not have more than 76 characters on a line
- Fix up the RPM group

* Mon Nov 06 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.0-6
- Explicitly require dbus-devel and dbus-glib (needed for FC < 6) 

* Sun Nov 05 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.0-5
- Remove unneeded BRs (gnome-utils-devel and openssl-devel) 

* Sun Nov 05 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.0-4
- Add autostart desktop file.
- Edit the package description as suggested in review

* Sat Nov 04 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.0-3
- More cleaups to the spec file.

* Sat Nov 04 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.0-2
- Add needed BRs

* Sat Nov 04 2006 Deji Akingunola <dakingun@gmail.com> - 0.5.0-1
- Initial packaging for Fedora Extras
