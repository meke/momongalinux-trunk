%global         momorel 1

Summary:	Off-The-Record Messaging library and toolkit
Name:		libotr
Version:	3.2.1
Release:	%{momorel}m%{?dist}
License:	"GPL, LGPL"
Group:		System Environment/Libraries
Url:		http://otr.cypherpunks.ca/
Source0:	http://otr.cypherpunks.ca/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	libgcrypt >= 1.2.0
Requires:	pkgconfig
BuildRequires:	libgcrypt-devel >= 1.2.0, libgpg-error-devel 

%description
Off-the-Record Messaging Library and Toolkit
This is a library and toolkit which implements Off-the-Record (OTR) Messaging.
OTR allows you to have private conversations over IM by providing Encryption,
Authentication, Deniability and Perfect forward secrecy.

%package devel
Summary: Development library and include files for libotr
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}, libgcrypt-devel >= 1.2.0

%description devel

The devel package contains the libotr library and the include files

%prep
%setup -q

%build

%configure --with-pic --disable-rpath
make %{?_smp_mflags} all

%install
rm -rf $RPM_BUILD_ROOT
make \
	DESTDIR=%{buildroot} \
	LIBINSTDIR=%{_libdir} \
	install
rm -rf %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-,root,root)
%doc AUTHORS README COPYING COPYING.LIB NEWS Protocol*
%{_libdir}/libotr.so.*
%{_bindir}/*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%doc ChangeLog
%{_libdir}/libotr.so
%{_libdir}/libotr.a
%{_libdir}/pkgconfig/libotr.pc
%dir %{_includedir}/libotr
%{_includedir}/libotr/*
%{_datadir}/aclocal/*


%changelog
* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- [SECURITY] CVE-2012-3461
- update to 3.2.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-2m)
- rebuild against rpm-4.6

* Mon Jul 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- import from Fedora

* Sun Jun 15 2008 Paul Wouters <paul@cypherpunks.ca> 3.2.0-1
- Upgraded to 3.2.0

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 3.1.0-2
- Autorebuild for GCC 4.3

* Wed Aug  1 2007 Paul Wouters <paul@cypherpunks.ca> 3.1.0-1
- Upgraded to current version
- Updated URLS and configure line

* Mon Sep 11 2006 Paul Wouters <paul@cypherpunks.ca> 3.0.0-2
- Rebuild requested for PT_GNU_HASH support from gcc

* Mon Oct 17 2005 Paul Wouters <paul@cypherpunks.ca> 3.0.0-1
- Minor change to allow for new documentation files. Fixed Requires:

* Sat Jun 19 2005 Paul Wouters <paul@cypherpunks.ca>
- Fixed defattr, groups, description and duplicate files in devel

* Fri Jun 17 2005 Tom "spot" Callaway <tcallawa@redhat.com>
- reworked for Fedora Extras

* Tue May  3 2005 Ian Goldberg <ian@cypherpunks.ca>
- Bumped version number to 2.0.2
* Wed Feb 16 2005 Ian Goldberg <ian@cypherpunks.ca>
- Bumped version number to 2.0.1
* Tue Feb  8 2005 Ian Goldberg <ian@cypherpunks.ca>
- Bumped version number to 2.0.0
* Wed Feb  2 2005 Ian Goldberg <ian@cypherpunks.ca>
- Added libotr.m4 to the devel package
- Bumped version number to 1.99.0
* Wed Jan 19 2005 Paul Wouters <paul@cypherpunks.ca>
- Updated spec file for the gaim-otr libotr split
* Tue Dec 21 2004 Ian Goldberg <otr@cypherpunks.ca>
- Bumped to version 1.0.2.
* Fri Dec 17 2004 Paul Wouters <paul@cypherpunks.ca>
- instll fix for x86_64
* Sun Dec 12 2004 Ian Goldberg <otr@cypherpunks.ca>
- Bumped to version 1.0.0.
* Fri Dec 10 2004 Ian Goldberg <otr@cypherpunks.ca>
- Bumped to version 0.9.9rc2. 
* Thu Dec  9 2004 Ian Goldberg <otr@cypherpunks.ca>
- Added CFLAGS to "make all", removed DESTDIR
* Wed Dec  8 2004 Ian Goldberg <otr@cypherpunks.ca>
- Bumped to version 0.9.9rc1. 
* Fri Dec  3 2004 Ian Goldberg <otr@cypherpunks.ca>
- Bumped to version 0.9.1. 
* Wed Dec  1 2004 Paul Wouters <paul@cypherpunks.ca>
- Bumped to version 0.9.0. 
- Fixed install for tools and cos
- Added Obsoletes: target for otr-plugin so rpm-Uhv gaim-otr removes it.
* Mon Nov 22 2004 Ian Goldberg <otr@cypherpunks.ca>
- Bumped version to 0.8.1
* Sun Nov 21 2004 Paul Wouters <paul@cypherpunks.ca>
- Initial version

