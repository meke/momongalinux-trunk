%global momorel 1

Summary: ACPI Event Daemon
Name: acpid
Version: 2.0.22
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
Source: http://dl.sourceforge.net/sourceforge/acpid2/acpid-%{version}.tar.xz
NoSource: 0
#Source1: momonga.conf
Source1: acpid.init
Source2: acpid.video.conf
Source3: acpid.power.conf
Source4: acpid.power.sh
Source5: acpid.service
Source6: acpid.sysconfig

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://acpid.sourceforge.net/
ExclusiveArch: %{ix86} x86_64 ia64
BuildRequires: systemd
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
acpid is a daemon that dispatches ACPI events to user-space programs.

%package sysvinit
Summary: ACPI Event Daemon
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}
Requires(preun): chkconfig

%description sysvinit
The acpid-sysvinit contains SysV initscript.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
make install DESTDIR=%{buildroot} docdir=%{_docdir}/%{name}-%{version}

mkdir -p %{buildroot}%{_sysconfdir}/acpi/events
mkdir -p %{buildroot}%{_sysconfdir}/acpi/actions
mkdir -p %{buildroot}/lib/systemd/system
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig

chmod 755 %{buildroot}%{_sysconfdir}/acpi/events

install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/acpi/events/videoconf
install -m 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/acpi/events/powerconf
install -m 755 %{SOURCE4} %{buildroot}%{_sysconfdir}/acpi/actions/power.sh
install -m 644 %{SOURCE5} %{buildroot}/lib/systemd/system
install -m 644 %{SOURCE6} %{buildroot}%{_sysconfdir}/sysconfig/acpid

mkdir -p %{buildroot}%{_initscriptdir}
install -m 755 %{SOURCE1} %{buildroot}%{_initscriptdir}/acpid

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}
/lib/systemd/system/%{name}.service
%dir %{_sysconfdir}/acpi
%dir %{_sysconfdir}/acpi/events
%dir %{_sysconfdir}/acpi/actions
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/acpi/events/videoconf
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/acpi/events/powerconf
%config(noreplace) %attr(0755,root,root) %{_sysconfdir}/acpi/actions/power.sh
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/sysconfig/acpid
%{_bindir}/acpi_listen
%{_sbindir}/kacpimon
%{_sbindir}/acpid
%{_bindir}/acpi_listen
%{_mandir}/man8/acpid.8*
%{_mandir}/man8/acpi_listen.8*
%{_mandir}/man8/kacpimon.8.*

%files sysvinit
%attr(0755,root,root) %{_initscriptdir}/acpid

%pre
if [ "$1" = "2" ]; then
        conflist=`ls %{_sysconfdir}/acpi/events/*.conf 2> /dev/null`
        RETCODE=$?
        if [ $RETCODE -eq 0 ]; then
                for i in $conflist; do
                        rmdot=`echo $i | sed 's/.conf/conf/'`
                        mv $i $rmdot
                done
        fi
fi

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%triggerun -- %{name} < 2.0.10
        /sbin/chkconfig --del acpid >/dev/null 2>&1 || :
        /bin/systemctl try-restart acpid.service >/dev/null 2>&1 || :

%triggerpostun -n %{name}-sysvinit -- %{name} < 2.0.10
        /sbin/chkconfig --add acpid >/dev/null 2>&1 || :


%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.22-1m)
- update 2.0.22

* Sun Mar 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.21-1m)
- update 2.0.21

* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.19-2m)
- update acpid.power.sh

* Mon Jul 29 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.19-1m)
- update 2.0.19

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.13-1m)
- update 2.0.13

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.10-2m)
- Requires chkconfig
 
* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.10-1m)
- update 2.0.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.10-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-3m)
- fix file permission
-- %%{_initscriptdir}/acpid and %%{_sbindir}/acpid should be executable

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.10-1m)
- [SECURITY] CVE-2009-0798
- update to 1.0.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-8m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.6-7m)
- change start up priority from 44 to 26

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.6-6m)
- rebuild against gcc43

* Tue Feb 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.6-5m)
- fix copying daemon start script file

* Tue Feb 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.6-4m)
- revive %%post

* Sun Feb 24 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-3m)
- glibc-2.8 fix

* Tue Nov 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-2m)
- fix %%files

* Tue Nov 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-1m)
- [SECURITY] CVE-2009-4033 CVE-2009-4235
- update 1.0.6
- move changelog

* Wed May 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-5m)
- import fc-patches 

* Thu Nov  3 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-4m)
- add gcc4 patch. import from Fedora
- Patch3: acpid-1.0.4-warning.patch

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.4-3m)
- revised docdir permission

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.4-2m)
- enable x86_64.

* Sun Oct 24 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.0.4-1m)
- update to 1.0.4
- add %%doc samples/battery samples/panasonic

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.3-2m)
- revised spec for enabling rpm 4.2.

* Wed Feb 25 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.0.3-1m)
- update to 1.0.3
- remove acpid-1.0.0-init.kiriwake.patch and add acpid.init
  redhat/acpid.init has gone from source tar ball

* Sun Dec 14 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.2-3m)
- add ExclusiveArch: %%{ix86}
- condrestart daemon on upgrade
- stop daemon before removing

* Sun Dec 14 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.2-2m)
- fix sample configuration files to adapt to latest format of /proc/acpi/event
- install acpi_handler.sh to %{_libexecdir}
- add configuration file which calls acpi_handler.sh
- %%config(noreplace) to config files

* Thu Jul 24 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.0.2-1m)
- update to 1.0.2
- use %%{momorel} macro
- add %%doc
- fix %%{_mandir}/man8/acpid.8.* -> %%{_mandir}/man8/acpid.8*

* Mon May 20 2002 Kenta MURATA <muraken@kondara.org>
- (1.0.1-4k)
- sourceforge sabaki

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.0.1-4k)
- /sbin/chkconfig -> chkconfig in PreReq.

* Mon Mar 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0.1-2k)
 update to 1.0.1

* Mon Nov 19 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.0-4k)
- add acpid-1.0.0-init.kiriwake.patch not to boot on acpi disable

* Sat Oct 20 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.0.0-2k)
- First Kondarization

* Thu Aug 16 2001  Tim Hockin <thockin@sun.com>
  - Added commandline options to actions

* Wed Aug 15 2001  Tim Hockin <thockin@sun.com>
  - Added UNIX domain socket support
  - Changed /etc/acpid.d to /etc/acpid/events

* Mon Aug 13 2001  Tim Hockin <thockin@sun.com>
  - added changelog
  - 0.99.1-1

