%global	momorel		1
%define glib2_version             2.31.0
%define gtk3_version              3.3.11
%define udisks_version            1.97.0

Summary: Disks
Name: gnome-disk-utility
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://git.gnome.org/browse/gnome-disk-utility
Source0: http://download.gnome.org/sources/gnome-disk-utility/3.6/%{name}-%{version}.tar.xz
NoSource: 0
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gtk3-devel >= %{gtk3_version}
BuildRequires: gettext
BuildRequires: desktop-file-utils
BuildRequires: libudisks2-devel >= %{udisks_version}
BuildRequires: gnome-common
BuildRequires: intltool
BuildRequires: libgnome-keyring-devel >= 3.4
BuildRequires: systemd-devel
# for xsltproc
BuildRequires: libxslt
BuildRequires: docbook-style-xsl
BuildRequires: libpwquality-devel

Requires: udisks2

Obsoletes: gnome-disk-utility-format
Obsoletes: nautilus-gdu
Obsoletes: gnome-disk-utility-libs
Obsoletes: gnome-disk-utility-devel
Obsoletes: gnome-disk-utility-ui-libs
Obsoletes: gnome-disk-utility-ui-devel
Obsoletes: gnome-disk-utility-nautilus

%description
This package contains the Disks and Disk Image Mounter applications.
Disks supports partitioning, file system creation, encryption,
fstab/crypttab editing, ATA SMART and other features

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

desktop-file-install --delete-original  \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  $RPM_BUILD_ROOT%{_datadir}/applications/gnome-disks.desktop \
  $RPM_BUILD_ROOT%{_datadir}/applications/gnome-disk-image-mounter.desktop

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor || :
update-desktop-database %{_datadir}/applications &> /dev/null

%postun
update-desktop-database %{_datadir}/applications &> /dev/null
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache -q %{_datadir}/icons/hicolor || :


%files -f %{name}.lang
%defattr(-,root,root,-)

%{_bindir}/gnome-disks
%{_bindir}/gnome-disk-image-mounter
%{_datadir}/applications/gnome-disks.desktop
%{_datadir}/applications/gnome-disk-image-mounter.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.Disks.gschema.xml
%dir %{_datadir}/gnome-disk-utility
%{_datadir}/gnome-disk-utility/*.ui
%{_datadir}/icons/hicolor/*

%{_mandir}/man1/*

%doc README AUTHORS NEWS COPYING

%changelog
* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Sat Jun 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- update to 3.5.2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-2m)
- rebuild for glib 2.33.2

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-7m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-6m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.1-5m)
- add Requires: udisks

* Thu Jul 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-4m)
- fix req

* Wed Jul 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-4m)
- split libs and nautilus

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.30.1-3m)
- gtkdocize again (with gtk-doc-1.14)

* Mon Jul  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- do not use gtkdocize ;-) (gtk-doc-1.15)

* Thu Mar 25 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Thu Mar 18 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- initial build
