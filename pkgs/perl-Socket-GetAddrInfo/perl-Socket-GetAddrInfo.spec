%global         momorel 8

Name:           perl-Socket-GetAddrInfo
Version:        0.22
Release:        %{momorel}m%{?dist}
Summary:        Wrapper for Socket6's getaddrinfo and getnameinfo, or emulation for platforms that do not support it
License:        GPL or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Socket-GetAddrInfo/
Source0:	http://www.cpan.org/authors/id/P/PE/PEVANS/Socket-GetAddrInfo-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-CBuilder
BuildRequires:  perl-ExtUtils-CChecker >= 0.06
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-XSLoader
Requires:       perl-XSLoader
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
BuildArch:      noarch

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The intention of this module is that any code wishing to perform name-to-
address or address-to-name resolutions should use this instead of using
Socket6 directly. If the underlying platform has Socket6 installed, then it
will be used, and the complete range of features it provides can be used.
If the platform does not support it, then this module will instead provide
emulations of the relevant functions, using the legacy resolver functions
of gethostbyname(), etc...

%prep
%setup -q -n Socket-GetAddrInfo-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Socket)/d'

EOF
%define __perl_requires %{_builddir}/Socket-GetAddrInfo-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
I_CAN_HAS_INTERNETS=1 ./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{_bindir}/getaddrinfo
%{_bindir}/getnameinfo
%{perl_vendorlib}/Socket/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-2m)
- rebuild for new GCC 4.6

* Sat Feb  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-2m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-2m)
- rebuild against perl-5.12.0

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-2m)
- rebuild against perl-5.10.1

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-2m)
- rebuild against rpm-4.6

* Sat Jun 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.08-2m)
- rebuild against gcc43

* Tue Mar  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08
- no noarch

* Wed Jan 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- Specfile autogenerated by cpanspec 1.74 for Momonga Linux.
