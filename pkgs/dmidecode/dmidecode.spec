%global momorel 1

Summary:        Tool to analyse BIOS DMI data
Name:           dmidecode
Version:        2.12
Release:        %{momorel}m%{?dist}
Group:          System Environment/Base
License:        GPLv2+
URL:		http://www.nongnu.org/dmidecode/
Source0:	http://download.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
Patch1:         dmidecode-2.12-smbios_fix.patch
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: 	automake autoconf
ExclusiveArch:	%{ix86} x86_64 ia64
Obsoletes:	kernel-utils

%description
dmidecode reports information about x86 & ia64 hardware as described in the
system BIOS according to the SMBIOS/DMI standard. This information
typically includes system manufacturer, model name, serial number,
BIOS version, asset tag as well as a lot of other details of varying
level of interest and reliability depending on the manufacturer.

This will often include usage status for the CPU sockets, expansion
slots (e.g. AGP, PCI, ISA) and memory module slots, and the list of
I/O ports (e.g. serial, parallel, USB).

%prep
%setup -q
%patch1 -p1 -b .smbios

%build
make CFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=%{buildroot} prefix=%{_prefix} install-bin install-man

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc AUTHORS CHANGELOG LICENSE README
%{_sbindir}/dmidecode
%ifnarch ia64
%{_sbindir}/vpddecode
%{_sbindir}/ownership
%{_sbindir}/biosdecode
%endif
%{_mandir}/man8/*

%changelog
* Wed May 14 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.12-1m)
- update to 2.12

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11-1m)
- update 2.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-5m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10-4m)
- build fix make-3.82

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10-1m)
- update to 2.10
- import Patch0-4 from Fedora 13 (1:2.1.0-1.40)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9-2m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.9-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7-2m)
- rebuild against gcc43

* Sat May 20 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.7-1m)
- import from fc

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov 28 2005 Dave Jones <davej@redhat.com>
- Integrate several specfile cleanups from Robert Scheck. (#172543)

* Sat Sep 24 2005 Dave Jones <davej@redhat.com>
- Revert yesterdays patch, its unneeded in 2.7

* Fri Sep 23 2005 Dave Jones <davej@redhat.com>
- Don't try to modify areas mmap'd read-only.
- Don't build on ia64 any more.
  (It breaks on some boxes very badly, and works on very few).

* Mon Sep 12 2005 Dave Jones <davej@redhat.com>
- Update to upstream 2.7

* Fri Apr 15 2005 Florian La Roche <laroche@redhat.com>
- remove empty scripts

* Wed Mar  2 2005 Dave Jones <davej@redhat.com>
- Update to upstream 2.6

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4

* Tue Feb  8 2005 Dave Jones <davej@redhat.com>
- Rebuild with -D_FORTIFY_SOURCE=2

* Tue Jan 11 2005 Dave Jones <davej@redhat.com>
- Add missing Obsoletes: kernel-utils

* Mon Jan 10 2005 Dave Jones <davej@redhat.com>
- Update to upstream 2.5 release.

* Sat Dec 18 2004 Dave Jones <davej@redhat.com>
- Initial packaging, based upon kernel-utils package.

