# Generated from heroku-2.26.7.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname heroku

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Client library and CLI to deploy apps on Heroku
Name: rubygem-%{gemname}
Version: 2.26.7
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: MIT
URL: http://heroku.com/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(heroku-api) => 0.2.4
Requires: rubygem(heroku-api) < 0.3
Requires: rubygem(netrc) => 0.7.4
Requires: rubygem(netrc) < 0.8
Requires: rubygem(rest-client) => 1.6.1
Requires: rubygem(rest-client) < 1.7
Requires: rubygem(launchy) >= 0.3.2
Requires: rubygem(rubyzip) 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Client library and command-line tool to deploy and manage apps on Heroku.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/heroku
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26.7-1m)
- update 2.26.7

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.23.0-1m)
- updte 2.23

* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.22.0-1m)
- update 2.22.0

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.18.1-1m)
- udpate 2.18.1

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.1-1m)
- update 2.11.1

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.11.0-1m)
- update 2.11.0

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.6-1m) 
- update 2.8.6

* Wed Sep 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.0-1m) 
- Initial package for Momonga Linux
