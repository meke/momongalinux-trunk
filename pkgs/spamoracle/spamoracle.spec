%global momorel 10
%global ocamlver 3.11.2

Summary: A spam classification tool
Name: spamoracle
Version: 1.4
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2
URL: http://pauillac.inria.fr/~xleroy/software.html
Source0: http://pauillac.inria.fr/~xleroy/software/%{name}-%{version}.tar.gz
BuildRequires: ocaml >= %{ocamlver}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
SpamOracle is a tool to help detect and filter away "spam"
(unsolicited commercial e-mail).  It proceeds by statistical analysis
of the words that appear in the e-mail, comparing the frequencies of
words with those found in a user-provided corpus of known spam and
known legitimate e-mail.  The classification algorithm is based on
Bayes' formula, and is described in Paul Graham's paper, "A plan for
spam", http://www.paulgraham.com/spam.html.

%prep
%setup -q

%build
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%__mkdir_p %{buildroot}%{_bindir}
%__mkdir_p %{buildroot}%{_mandir}/man1
%__mkdir_p %{buildroot}%{_mandir}/man5
%makeinstall BINDIR=%{buildroot}%{_bindir} MANDIR=%{buildroot}%{_mandir}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README Changes
%{_bindir}/spamoracle
%{_mandir}/man1/spamoracle.1*
%{_mandir}/man5/spamoracle.conf.5*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-8m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-7m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-5m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-4m)
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-3m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against ocaml-3.10.2

* Wed Nov 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-1m)
- import to Momonga from Mandriva

* Fri Nov 24 2006 Gotz Waschk <waschk@mandriva.org> 1.4-4mdv2007.0
+ Revision: 86893
- Import spamoracle

* Fri Nov 24 2006 Gotz Waschk <waschk@mandriva.org> 1.4-4mdv2007.1
- rebuild

* Tue Nov 08 2005 Gotz Waschk <waschk@mandriva.org> 1.4-3mdk
- Rebuild

* Fri Nov  5 2004 Gotz Waschk <waschk@linux-mandrake.com> 1.4-2mdk
- rebuild

* Wed Oct 15 2003 Gotz Waschk <waschk@linux-mandrake.com> 1.4-1mdk
- new version

* Tue Jul 29 2003 Gotz Waschk <waschk@linux-mandrake.com> 1.3.1-1mdk
- initial package inspired by an Alt Linux rpm

* Fri Oct 25 2002 Vitaly Lugovsky <vsl@altlinux.ru> 1.2-alt2
- initial release
