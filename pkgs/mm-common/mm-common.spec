%global momorel 1

Summary: GNOME C++ bindings effort
Name: mm-common
Version: 0.9.6
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: GPLv2
URL: http://www.gtkmm.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.9/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch

%description
The mm-common module provides the build infrastructure and utilities
shared among the GNOME C++ binding libraries.  It is only a required
dependency for building the C++ bindings from the gnome.org version
control repository.  An installation of mm-common is not required for
building tarball releases, unless configured to use maintainer-mode.

%prep
%setup -q

%build
%configure --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/mm-common-prepare
%{_datadir}/aclocal/*
%{_datadir}/doc/mm-common
%{_datadir}/mm-common
%{_mandir}/man1/mm-common-prepare.1.*

%{_datadir}/pkgconfig/mm-common-libstdc++.pc
%{_datadir}/pkgconfig/mm-common-util.pc

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6-1m)
- update to 0.9.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-2m)
- full rebuild for mo7 release

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Wed Jan 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Thu Sep 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.3-1m)
- initial build
