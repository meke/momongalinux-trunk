%global momorel 7

%global xfcever 4.10
%global xfce4ver 4.10.0

Name: 		xfconf
Version: 	4.10.0
Release:	%{momorel}m%{?dist}
Summary: 	multi-channel settings management support for xfce

Group: 		Development/Libraries
License:	LGPL
URL: 		http://www.xfce.org/
Source0:        http://archive.xfce.org/xfce/%{xfcever}/src/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	libxfce4util-devel >= %{version}
BuildRequires:	libxml2-devel >= 2.7.2
BuildRequires:	pkgconfig
BuildRequires:	gtk-doc
BuildRequires:	imake
BuildRequires:	libXt-devel
BuildRequires:  dbus-glib-devel >= 0.72
Obsoletes: libxfce4mcs
Provides: libxfce4mcs

%description
This package includes multi-channel settings management support for Xfce.

%package devel
Summary:	developpment tools for xfconf library
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	libX11-devel
Requires:	glib-devel
Obsoletes: libxfce4mcs-devel
Provides: libxfce4mcs-devel

%description devel
This package includes the static libraries and header files you will need
to compile applications for Xfce.

%prep
%setup -q

%build
%configure --disable-static --with-perl-options=INSTALLDIRS="vendor"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}
find %{buildroot} -name "*.la" -delete
find %{buildroot} -name "perllocal.pod" |xargs rm -f
find %{buildroot} -name ".packlist" |xargs rm -f

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING
%{_bindir}/*
%{_libdir}/lib*.so.*
%dir %{_libdir}/xfce4
%dir %{_libdir}/xfce4/xfconf
%{_libdir}/xfce4/xfconf/xfconfd
%{_datadir}/dbus-1/services/*
%dir %{_datadir}/gtk-doc/html/xfconf
%{_datadir}/gtk-doc/html/xfconf/*
%{_datadir}/locale/*/*/*
%{_mandir}/man3/*.3*
%dir %{perl_vendorarch}/Xfce4/
%{perl_vendorarch}/Xfce4/*
%dir %{perl_vendorarch}/auto/Xfce4
%{perl_vendorarch}/auto/Xfce4/*

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/xfce4/xfconf-0
%dir %{_includedir}/xfce4/xfconf-0/xfconf
%{_includedir}/xfce4/xfconf-0/xfconf/xfconf*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-7m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-6m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-2m)
- rebuild against perl-5.16.2

* Thu Sep  6 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to version 4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-5m)
- change Source0 URI

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-3m)
- rebuild against perl-5.16.0

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- rebuild against perl-5.14.1

* Sat May  7 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-6m)
- rebuild against perl-5.14.0-0.2.1m

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.4-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-1m)
- update

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-8m)
- rebuild against perl-5.12.1

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (4.6.1-7m)
- add %%dir

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-4m)
- rebuild against perl-5.10.1

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-3m)
- use vendor

* Fri Apr 24 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-2m)
- add BuildRequires:

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%{_libexecdir}/xfconfd in %%files section


* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-2m)
- add Provides

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- initial Momonga package
