%global momorel 6

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:		python-jinja
Version:	1.2
Release:	%{momorel}m%{?dist}
Summary:	Sandboxed template engine

Group:		Development/Languages
License:	Modified BSD
URL:		http://jinja.pocoo.org/
Source0:	http://pypi.python.org/packages/source/J/Jinja/Jinja-%{version}.tar.gz
NoSource: 0
Patch0:		%{name}-docs.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	python-devel >= 2.7
BuildRequires:	python-setuptools-devel

%description
Jinja is a sandboxed template engine written in pure Python. It
provides a Django-like non-XML syntax and compiles templates into
executable python code. It's basically a combination of Django
templates and python code.


%prep
%setup -q -n Jinja-%{version}
%patch0 -p0 -b .docs


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

# fix EOL
sed -i 's|\r$||g' LICENSE


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS CHANGES LICENSE TODO docs/html
%{python_sitearch}/*


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-1m)
- import from Fedora 11
- License: Modified BSD

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.2-2
- Rebuild for Python 2.6

* Tue Mar 11 2008 Thomas Moschny <thomas.moschny@gmx.de> - 1.2-1
- Initial build.
