%global momorel 6

Summary: A Suite of Portable Game Libraries
Name: plib
Version: 1.8.5
Release: %{momorel}m%{?dist}
Source0: http://plib.sourceforge.net/dist/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-format.patch
URL: http://plib.sourceforge.net/
License: LGPLv2+
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Prefix: %{_prefix}
BuildRequires: libtool
BuildRequires: SDL-devel
BuildRequires: freeglut-devel

%description
PLIB is comprised of a number of semi-autonomous libraries that you can pretty
much mix and match - using as much or as little PLIB as you need.

%prep
%setup -q 
%patch0 -p1 -b .format

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall
mkdir -p %{buildroot}/usr/include/plib
cp %{buildroot}/usr/include/*.h %{buildroot}/usr/include/plib

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS INSTALL NEWS NOTICE COPYING README
%{_libdir}/lib*.a
%{_includedir}/*.h
%dir %{_includedir}/plib
%{_includedir}/plib/*.h

%changelog
* Mon Mar  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5-6m)
- [SECURITY] CVE-2011-4620
- import patch from debian

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.4-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4-4m)
- %%NoSource -> NoSource

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.4-3m)
- rebuild against freeglut

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4-2m)
- add gcc-4.1 patch
-- Patch0: plib-1.8.4-gcc41.patch

* Thu May 19 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.8.4-1m)
- 1.8.4 has been released

* Sat Apr  3 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6.0-2m)
- revised spec for rpm 4.2.

* Tue Nov 12 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (1.6.0-1m)
- remove configure-patch. 1.6.0 seems to handle CFLAGS correctly.

* Thu May  9 2002 Toru Hoshina <t@kondara.org>
- (1.4.2-6k)
- rebuild against glut 3.7.

* Fri Sep  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.4.2-2k)
- 1st release.
