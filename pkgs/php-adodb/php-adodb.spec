%global         momorel 1

Name:           php-adodb
Summary:        Active Data Objects Data Base
Version:        5.16
Release:        %{momorel}m%{?dist}

Source0:        http://dl.sourceforge.net/adodb/adodb516a.tgz
NoSource:	0
License:        BSD or LGPLv2+
URL:            http://adodb.sf.net
Group:          Development/Libraries
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
Requires:       php >= 5.4.1

%description
PHP is a wonderful language for building dynamic web pages.
Unfortunately, PHP's database access functions are not
standardised. Every database extension uses a different and
incompatibile API. This creates a need for a database class
library to hide the differences between the different databases
(encapsulate the differences) so we can easily switch databases.

!! TODO !! MAKE A SUBPACKAGE FOR THE PEAR::AUTH DRIVER

%prep
%setup -q -n adodb5

%build
# fix dir perms
find . -type d | xargs chmod 755
# fix file perms
find . -type f | xargs chmod 644

%install
rm -rf $RPM_BUILD_ROOT

install -d $RPM_BUILD_ROOT%{_var}/www/icons
install -d $RPM_BUILD_ROOT%{_datadir}/php/adodb
cp -pr * $RPM_BUILD_ROOT%{_datadir}/php/adodb/

install -m644 cute_icons_for_site/* $RPM_BUILD_ROOT%{_var}/www/icons/

# cleanup
rm -rf $RPM_BUILD_ROOT%{_datadir}/php/adodb/cute_icons_for_site
rm -rf $RPM_BUILD_ROOT%{_datadir}/php/adodb/docs
rm -f $RPM_BUILD_ROOT%{_datadir}/adodb/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc *.txt docs/*
%{_datadir}/php/adodb
%{_var}/www/icons/*

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.16-1m)
- update to 5.16
- rebuild against php-5.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.11-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11-1m)
- update to 5.11

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.95-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul  2 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.95-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.95-2.a
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Aug 26 2007 Aurelien Bompard <abompard@fedoraproject.org> 4.95-1.a
- version 4.95a
- fix license tag

* Fri Apr 06 2007 Aurelien Bompard <abompard@fedoraproject.org> 4.94-1
- version 4.94
- move install path to %%_datadir/php/adodb (#235461)

* Wed Aug 30 2006 Aurelien Bompard <abompard@fedoraproject.org> 4.92-1
- version 4.92

* Fri Apr 14 2006 Aurelien Bompard <gauret[AT]free.fr> 4.80-1
- version 4.80

* Wed Feb 22 2006 Aurelien Bompard <gauret[AT]free.fr> 4.72-1
- version 4.72

* Fri Dec 23 2005 Aurelien Bompard <gauret[AT]free.fr> 4.68-1
- version 4.68

* Fri Nov 18 2005 Aurelien Bompard <gauret[AT]free.fr> 4.67-1
- version 4.67

* Sun May 08 2005 Aurelien Bompard <gauret[AT]free.fr> 4.62-1%{?dist}
- version 4.62
- use disttag

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Wed Aug 11 2004 Aurelien Bompard <gauret[AT]free.fr> 0:4.52-0.fdr.1
- update to 4.52

* Sat Jul 31 2004 Aurelien Bompard <gauret[AT]free.fr> 0:4.51-0.fdr.1
- update to 4.51

* Sat Jul 03 2004 Aurelien Bompard <gauret[AT]free.fr> 0:4.23-0.fdr.2
- move to _datadir instead of _libdir
- use the _var macro

* Wed Jun 30 2004 Aurelien Bompard <gauret[AT]free.fr> 0:4.23-0.fdr.1
- Initial Fedora package (from Mandrake)

