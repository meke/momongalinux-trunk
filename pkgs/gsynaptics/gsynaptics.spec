%global momorel 6
%global relnum 38463

Name:           gsynaptics
Version:        0.9.16
Release:        %{momorel}m%{?dist}
Summary:        Settings tool for Synaptics touchpad driver

Group:          Applications/System
License:        GPLv2+
URL:            http://gsynaptics.sourceforge.jp/
Source0:        http://dl.sourceforge.jp/%{name}/%{relnum}/%{name}-%{version}.tar.gz
NoSource:       0
# Source1 taken from http://dl.sourceforge.jp/gsynaptics/15189/gsynaptics-0.9.0.tar.gz
# until http://sourceforge.jp/tracker/index.php?func=detail&aid=11265&group_id=1720&atid=6433 get fixed
Source1:        %{name}-touchpad.png
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl-XML-Parser
BuildRequires:  libgnomeui-devel
BuildRequires:  libglade2-devel
BuildRequires:  desktop-file-utils gettext
BuildRequires:  gnome-doc-utils-devel
BuildRequires:  xorg-x11-drv-synaptics-devel

Requires:       xorg-x11-drv-synaptics

Obsoletes:      gsynaptics-mcs-plugin

# Use ExclusiveArch just as synaptics does; see
# http://cvs.fedora.redhat.com/viewcvs/rpms/synaptics/devel/synaptics.spec?root=extras&view=markup
# ExcludeArch tracker for now (until proper solution is found):
# https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=242323
ExclusiveArch:  %{ix86} x86_64 ppc ppc64

%description
A GTK+ tool to set scroll and tap preferences for the Synaptics touchpad
driver.

%prep
%setup -q
cp data/gsynaptics-init.desktop.in.in data/gsynaptics-init.desktop.in.in.org
grep -v -e "^Categories=$" data/gsynaptics-init.desktop.in.in.org > data/gsynaptics-init.desktop.in.in
install -p %{SOURCE1} data/touchpad.png

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
desktop-file-install --vendor=                                  \
        --dir %{buildroot}%{_datadir}/applications              \
        --add-category HardwareSettings                         \
        --delete-original                                       \
        %{buildroot}%{_datadir}/applications/gsynaptics.desktop

desktop-file-install --vendor=                                  \
        --dir %{buildroot}%{_datadir}/gnome/autostart/          \
        --add-category GNOME                                    \
        --delete-original                                       \
        %{buildroot}%{_datadir}/gnome/autostart/gsynaptics-init.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README TODO
%{_bindir}/*
%{_datadir}/%{name}
%{_datadir}/pixmaps/touchpad.png
%{_datadir}/applications/*%{name}.desktop
%{_datadir}/gnome/autostart/*%{name}*.desktop
%{_datadir}/gnome/help/%{name}
%{_mandir}/man1/gsynaptics*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.16-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.16-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.16-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.16-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.16-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.16-1m)
- update to 0.9.16

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.13-5m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.13-4m)
- Obsoletes: gsynaptics-mcs-plugin

* Sat Oct 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.13-3m)
- change Requires from synaptics to xorg-x11-drv-synaptics

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.13-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.13-1m)
- import to Momonga from Fedora

* Sat Dec 01 2007 Thorsten Leemhuis <fedora [AT] leemhuis [DOT] info> - 0.9.13-2
- BR gnome-doc-utils

* Sat Dec 01 2007 Thorsten Leemhuis <fedora [AT] leemhuis [DOT] info> - 0.9.13-1
- Update to 0.9.13
- ship touchpad.png, fixes #401891

* Fri Aug 03 2007 Thorsten Leemhuis <fedora [AT] leemhuis [DOT] info>
- Update License field due to the "Licensing guidelines changes"

* Sun Jun 03 2007 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.12-2
- Use ExclusiveArch just as synaptics does

* Sun Jun 03 2007 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.12-1
- Update to 0.9.12
- Add category HardwareSettings to desktop file (#242345)

* Sat Feb 24 2007 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.11-1
- Update to 0.9.11

* Mon Aug 28 2006 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.8-2
- Rebuild

* Sat Jul 08 2006 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.8-1
- Add BuildRequires: perl-XML-Parser (fixes #197955)
- update to 0.9.8

* Mon Feb 13 2006 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.5-2
- Rebuild for Fedora Extras 5

* Sun Nov 13 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.5-1
- Update to 0.9.5

* Sat Oct 22 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.4-3
- Use dist

* Tue Oct 04 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.4-2
- Improved Summary and description as suggested by ignacio

* Mon Oct 03 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.9.4-1
- Initial package.
