%global momorel 23

Summary: Client for sending messages to a host's logged in users.
Name: rwall
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Daemons
URL: http://www.hcs.harvard.edu/~dholland/computers/old-netkit.html
Source0: ftp://sunsite.unc.edu/pub/Linux/system/network/netkit/netkit-rwall-%{version}.tar.gz
NoSource: 0
Source1: rwalld.service
Patch1: netkit-rwalld-0.10-banner.patch
Patch2: netkit-rwall-0.17-strip.patch
Patch3: netkit-rwall-0.17-netgroup.patch
Patch4: netkit-rwall-0.17-droppriv.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The rwall command sends a message to all of the users logged into a
specified host.  Actually, your machine's rwall client sends the
message to the rwall daemon running on the specified host, and the
rwall daemon relays the message to all of the users logged in to that
host.

Install rwall if you'd like the ability to send messages to users
logged in to a specified host machine.

%package server
Summary: Server for sending messages to a host's logged in users.
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(post): systemd-sysv
Requires: portmap
BuildRequires: systemd-units
Group: System Environment/Daemons
Requires: portmap

%description server
The rwall command sends a message to all of the users logged into
a specified host.  The rwall-server package contains the daemon for
receiving such messages, and is disabled by default on Red Hat Linux
systems (it can be very annoying to keep getting all those messages
when you're trying to play Quake--I mean, trying to get some work done).

Install rwall-server if you'd like the ability to receive messages
from users on remote hosts.

%prep
%setup -q -n netkit-rwall-%{version}
%patch1 -p1 -b .banner
%patch2 -p1 -b .strip
%patch3 -p1 -b .netgroup
%patch4 -p1 -b .droppriv

%build
sh configure --with-c-compiler=gcc
perl -pi -e 's,-O2,\$(RPM_OPT_FLAGS),' MCONFIG

make RPM_OPT_FLAGS="%{optflags}" %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_mandir}/man8
mkdir -p %{buildroot}%{_unitdir}

make INSTALLROOT=%{buildroot} MANDIR=%{_mandir} install

install -m 755 %{SOURCE1} %{buildroot}%{_unitdir}/

%clean
rm -rf %{buildroot}

%preun server
%systemd_preun rwalld.service

%postun server
%systemd_postun_with_restart rwalld.service

%files
%defattr(-,root,root)
%{_bindir}/rwall
%{_mandir}/man1/rwall.1*

%files server
%defattr(-,root,root)
%{_sbindir}/rpc.rwalld
%{_mandir}/man8/rpc.rwalld.8*
%{_mandir}/man8/rwalld.8*
%{_unitdir}/*

%changelog
* Fri Dec 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17-23m)
- support systemd

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-22m)
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-19m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-18m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-16m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-15m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-14m)
- %%NoSource -> NoSource

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.17-13m)
- rebuild against new environment.

* Fri Apr 13 2001 Shingo Akagaki <dora@kondara.org>
- /etc/rc.d/init.d -> /etc/init.d

* Sat Feb  3 2001 Daiki Matsuda <dyky@df-usa.com>
- (0.16-15k)
- rebuild againt rpm-3.0.5-39k

* Wed Nov 29 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to %{_initscriptdir}

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Apr 24 2000 tom <tom@kondara.org>
- Kondarize :-P

* Fri Feb 11 2000 Bill Nottingham <notting@redhat.com>
- fix description

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Sat Feb  5 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- change %postun to %preun

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix descriptions and summary
- man pages are compressed

* Mon Jan  4 2000 Bill Nottingham <notting@redhat.com>
- split client and server

* Tue Dec 21 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- enhanced initscript

* Tue Oct 28 1997 Erik Troan <ewt@redhat.com>
- fixed init script (didn't include function library)
- doesn't invoke wall with -n anymore

* Sun Oct 19 1997 Erik Troan <ewt@redhat.com>
- added a chkconfig compatible initscript
- added %attr attributes

* Tue Jul 15 1997 Erik Troan <ewt@redhat.com>
- initial build
