%global momorel 1

%if %{?WITH_SELINUX:0}%{!?WITH_SELINUX:1}
%define WITH_SELINUX 1
%endif

Summary: Utilities for managing accounts and shadow password files.
Name: shadow-utils
Version: 4.2.1
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Base
URL: http://pkg-shadow.alioth.debian.org/
Source0: http://pkg-shadow.alioth.debian.org/releases/shadow-%{version}.tar.xz
NoSource: 0
Source1: shadow-utils.login.defs
Source2: shadow-utils.useradd

Patch0: shadow-4.1.5-redhat.patch
Patch1: shadow-4.1.5.1-goodname.patch
Patch3: shadow-4.1.5.1-fixes.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf, automake, libtool, gettext-devel
BuildRequires: libselinux-devel >= 1.25.2
BuildRequires: audit-libs-devel >= 2.0.4
Requires: libselinux >= 1.25.2
Requires: audit-libs >= 2.0.4

%description
The shadow-utils package includes the necessary programs for
converting UNIX password files to the shadow password format, plus
programs for managing user and group accounts. The pwconv command
converts passwords to the shadow password format. The pwunconv command
unconverts shadow passwords and generates an npasswd file (a standard
UNIX password file). The pwck command checks the integrity of password
and shadow files. The lastlog command prints out the last login times
for all users. The useradd, userdel, and usermod commands are used for
managing user accounts. The groupadd, groupdel, and groupmod commands
are used for managing group accounts.

%prep
%setup -q -n shadow-%{version}

iconv -f ISO88591 -t utf-8  doc/HOWTO > doc/HOWTO.utf8
cp -f doc/HOWTO.utf8 doc/HOWTO

%patch0 -p1 -b .redhat
%patch1 -p1 -b .goodname
%patch3 -p1 -b .sysAccountDownhill

#rm po/*.gmo
#rm po/stamp-po
#
#autoreconf -fvi

%build
%configure \
	--enable-shadowgrp \
	--with-audit \
	--with-sha-crypt \
	--with-selinux \
	--without-libcrack \
	--without-libpam \
	--disable-shared \
        --with-group-name-max-length=32

make 

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot} gnulocaledir=%{buildroot}/%{_datadir}/locale MKINSTALLDIRS=`pwd`/mkinstalldirs
install -d -m 755 %{buildroot}/etc/default
install -c -m 0644 %{SOURCE1} %{buildroot}/etc/login.defs
install -c -m 0600 %{SOURCE2} %{buildroot}/etc/default/useradd


ln -s useradd %{buildroot}%{_sbindir}/adduser
#ln -s %{_mandir}/man8/useradd.8 %{buildroot}/%{_mandir}/man8/adduser.8
ln -s useradd.8 %{buildroot}%{_mandir}/man8/adduser.8
for subdir in %{buildroot}%{_mandir}/{??,??_??,??_??.*}/man* ; do
	test -d $subdir && test -e $subdir/useradd.8 && echo ".so man8/useradd.8" > $subdir/adduser.8
done

# Remove binaries we don't use.
rm -f %{buildroot}%{_bindir}/chfn
rm -f %{buildroot}%{_bindir}/chsh
rm -f %{buildroot}%{_bindir}/expiry
rm -f %{buildroot}%{_bindir}/groups
rm -f %{buildroot}%{_bindir}/login
rm -f %{buildroot}%{_bindir}/passwd
rm -f %{buildroot}%{_bindir}/su
rm -f %{buildroot}%{_sysconfdir}/login.access
rm -f %{buildroot}%{_sysconfdir}/limits
rm -f %{buildroot}%{_sbindir}/logoutd
rm -f %{buildroot}%{_sbindir}/nologin
rm -f %{buildroot}%{_sbindir}/chgpasswd
rm -f %{buildroot}%{_mandir}/man1/chfn.*
rm -f %{buildroot}%{_mandir}/*/man1/chfn.*
rm -f %{buildroot}%{_mandir}/man1/chsh.*
rm -f %{buildroot}%{_mandir}/*/man1/chsh.*
rm -f %{buildroot}%{_mandir}/man1/expiry.*
rm -f %{buildroot}%{_mandir}/*/man1/expiry.*
rm -f %{buildroot}%{_mandir}/man1/groups.*
rm -f %{buildroot}%{_mandir}/*/man1/groups.*
rm -f %{buildroot}%{_mandir}/man1/login.*
rm -f %{buildroot}%{_mandir}/*/man1/login.*
rm -f %{buildroot}%{_mandir}/man1/passwd.*
rm -f %{buildroot}%{_mandir}/*/man1/passwd.*
rm -f %{buildroot}%{_mandir}/man1/su.*
rm -f %{buildroot}%{_mandir}/*/man1/su.*
rm -f %{buildroot}%{_mandir}/man5/limits.*
rm -f %{buildroot}%{_mandir}/*/man5/limits.*
rm -f %{buildroot}%{_mandir}/man5/login.access.*
rm -f %{buildroot}%{_mandir}/*/man5/login.access.*
rm -f %{buildroot}%{_mandir}/man5/passwd.*
rm -f %{buildroot}%{_mandir}/*/man5/passwd.*
rm -f %{buildroot}%{_mandir}/man5/porttime.*
rm -f %{buildroot}%{_mandir}/*/man5/porttime.*
rm -f %{buildroot}%{_mandir}/man5/suauth.*
rm -f %{buildroot}%{_mandir}/*/man5/suauth.*
rm -f %{buildroot}%{_mandir}/man8/logoutd.*
rm -f %{buildroot}%{_mandir}/*/man8/logoutd.*
rm -f %{buildroot}%{_mandir}/man8/nologin.*
rm -f %{buildroot}%{_mandir}/*/man8/nologin.*
rm -f %{buildroot}%{_mandir}/man8/chgpasswd.*
rm -f %{buildroot}%{_mandir}/*/man8/chgpasswd.*

%find_lang shadow
find %{buildroot}%{_mandir} -depth -type d -empty -delete
for dir in $(ls -1d %{buildroot}%{_mandir}/{??,??_??}) ; do
    dir=$(echo $dir | sed -e "s|^%{buildroot}||")
    lang=$(basename $dir)
    echo "%%lang($lang) $dir/man*/*" >> shadow.lang
done

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files -f shadow.lang
%defattr(-,root,root)
%doc NEWS doc/HOWTO README
%attr(0644,root,root)	%config(noreplace) %{_sysconfdir}/login.defs
%attr(0600,root,root)	%config(noreplace) %{_sysconfdir}/default/useradd
%{_bindir}/sg
%{_bindir}/chage
%{_bindir}/faillog
%{_bindir}/gpasswd
%{_bindir}/lastlog
%{_bindir}/newgrp
%{_bindir}/newgidmap
%{_bindir}/newuidmap
%{_sbindir}/adduser
%attr(0750,root,root)	%{_sbindir}/user*
%attr(0750,root,root)	%{_sbindir}/group*
%{_sbindir}/grpck
%{_sbindir}/pwck
%{_sbindir}/*conv
%{_sbindir}/chpasswd
%{_sbindir}/newusers
%{_sbindir}/vipw
%{_sbindir}/vigr
%{_mandir}/man1/chage.1*
%{_mandir}/man1/gpasswd.1*
%{_mandir}/man1/sg.1*
%{_mandir}/man1/newgrp.1*
%{_mandir}/man1/newgidmap.1.*
%{_mandir}/man1/newuidmap.1.*
%{_mandir}/man3/shadow.3*
%{_mandir}/man3/getspnam.3*
%{_mandir}/man5/shadow.5*
%{_mandir}/man5/login.defs.5*
%{_mandir}/man5/gshadow.5*
%{_mandir}/man5/faillog.5*
%{_mandir}/man5/subgid.5.*
%{_mandir}/man5/subuid.5.*
%{_mandir}/man8/adduser.8*
%{_mandir}/man8/group*.8*
%{_mandir}/man8/user*.8*
%{_mandir}/man8/pwck.8*
%{_mandir}/man8/grpck.8*
%{_mandir}/man8/chpasswd.8*
%{_mandir}/man8/newusers.8*
%{_mandir}/man8/*conv.8*
%{_mandir}/man8/lastlog.8*
%{_mandir}/man8/faillog.8*
%{_mandir}/man8/vipw.8*
%{_mandir}/man8/vigr.8*

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.5.1-1m)
- update to 4.1.5.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.4.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.4.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.4.2-2m)
- full rebuild for mo7 release

* Wed Dec 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.1.4.2-1m)
- update 4.1.4.2
- rebuild against audit-2.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.2.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.2.1-5m)
- revert to version 4.1.2.1
- useradd of version 4.1.3 is broken
- https://www.redhat.com/archives/fedora-devel-list/2009-April/msg01124.html

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.3-1m)
- sync with Fedora 11 (2:4.1.3-2)

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.2.1-4m)
- fix build

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.2.1-3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.2.1-2m)
- update to Patch2 for fuzz=0
- License: Modified BSD

* Fri Sep  5 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.2.1-1m)
- Source update to 4.1.2.1

* Sat Jul 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.2-1m)
- sync with Fedora devel (4.1.2-1)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.0-3m)
- %%NoSource -> NoSource

* Wed Jan  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- release %%{_sysconfdir}/default, it's already provided by glibc-common

* Wed Jan  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.0-1m)
- sync FC devel
- provide vipw and vigr

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.18.1-2m)
- modify %%files

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.0.18.1-1m)
- sync FC7
- update to 4.0.18.1

* Sat Sep 23 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.0.17-1m)
- version up
- build against gettext-0.15-1m
- Patches copied from Red Hat
  Patch0: shadow-4.0.17-redhat.patch
  Patch5: shadow-4.0.16-lOption.patch
- Patches removed
  Patch6: shadow-4.0.14-symlinks.patch
  Patch7: shadow-4.0.14-nscdFlushCash.patch

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.14-2m)
- delete duplicate dir

* Tue Jun  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.14-1m)
- version up

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-17m)
- revise spec file for rpm-4.4.2

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.3-16m)
- add patch for gcc4
- Patch9: shadow-4.0.7-gcc4.patch

* Wed Nov  3 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (4.0.3-15m)
- add patch for gcc 3.4

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.0.3-14m)
- remove Epoch

* Sat May 22 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.3-13m)
- remove NoSource

* Tue Jan 27 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.3-12m)
- modified spec

* Tue Jan 27 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.0.3-11m)
- add Provides: /usr/sbin/userdel
- remove shadow-4.0.3-mkinstalldirs.patch

* Sat Nov 08 2003 Kenta MURATA <muraken2@nifty.com>
- (4.0.3-10m)
- correct error on %%install (shadow-4.0.3-mkinstalldirs.patch).
- pretty spec file.

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.0.3-9m)
- from RawHide 4.0.3-9
-
-* Tue Dec 10 2002 Kazuhiko <kazuhiko@fdiary.net>
-- (20000902-9m)
-- Provides: /usr/sbin/useradd
-
-* Sun Dec  1 2002 YAMAZAKI Makoto <uomaster@nifty.com>
-- (20000902-8m)
-- override AUTOhoges in Makefile
-
-* Mon Oct  7 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
-- (20000902-7m)
-- fix buffer reallocation bug
-
-* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
-- (20000902-6k)
-- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k
-
-* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
-- (20000902-4k)
-- automake autoconf 1.5
-
-* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
-- (20000902-2k)
-- version up.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun  4 2003 Nalin Dahyabhai <nalin@redhat.com> 4.0.3-8
- rebuild

* Tue Jun  3 2003 Nalin Dahyabhai <nalin@redhat.com> 4.0.3-7
- run autoconf to generate updated configure at compile-time

* Wed Feb 12 2003 Nalin Dahyabhai <nalin@redhat.com> 4.0.3-6
- adjust mailspool patch to complain if no group named "mail" exists, even
  though that should never happen

* Tue Feb 11 2003 Nalin Dahyabhai <nalin@redhat.com> 4.0.3-5
- fix perms on mailspools created by useradd to be owned by the "mail"
  group (#59810)

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Mon Dec  9 2002 Nalin Dahyabhai <nalin@redhat.com> 4.0.3-3
- install the shadow.3 man page

* Mon Nov 25 2002 Nalin Dahyabhai <nalin@redhat.com> 4.0.3-2
- disable use of cracklib at build-time
- fixup reserved-account changes for useradd

* Thu Nov 21 2002 Nalin Dahyabhai <nalin@redhat.com> 4.0.3-1
- update to 4.0.3, bumping epoch

* Mon Nov 18 2002 Nalin Dahyabhai <nalin@redhat.com> 20000902-14
- remove man pages which conflict with the man-pages package(s)

* Fri Nov 15 2002 Nalin Dahyabhai <nalin@redhat.com> 20000902-13
- prevent libshadow from being built more than once, to keep automake happy
- change how md5 and md5crypt are enabled, to keep autoconf happy
- remove unpackaged files after %%install

* Thu Aug 29 2002 Nalin Dahyabhai <nalin@redhat.com> 20000902-12
- force .mo files to be regenerated with current gettext to flush out possible
  problems
- fixup non-portable encodings in translations
- make sv translation header non-fuzzy so that it will be included (#71281)

* Fri Aug 23 2002 Nalin Dahyabhai <nalin@redhat.com> 20000902-11
- don't apply aging parameters when creating system accounts (#67408)

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun May 26 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Fri May 17 2002 Nalin Dahyabhai <nalin@redhat.com> 20000902-8
- rebuild in new environment

* Wed Mar 27 2002 Nalin Dahyabhai <nalin@redhat.com> 20000902-7
- rebuild with proper defines to get support for large lastlog files (#61983)

* Fri Feb 22 2002 Nalin Dahyabhai <nalin@redhat.com> 20000902-6
- rebuild

* Fri Jan 25 2002 Nalin Dahyabhai <nalin@redhat.com> 20000902-5
- fix autoheader breakage and random other things autotools complain about

* Mon Aug 27 2001 Nalin Dahyabhai <nalin@redhat.com> 20000902-4
- use -O0 instead of -O on ia64
- build in source directory
- don't leave lock files on the filesystem when useradd creates a group for
  the user (#50269)
- fix the -o option to check for duplicate UIDs instead of login names (#52187)

* Thu Jul 26 2001 Bill Nottingham <notting@redhat.com> 20000902-3
- build with -O on ia64

* Fri Jun 08 2001 Than Ngo <than@redhat.com> 20000902-2
- fixup broken specfile

* Tue May 22 2001 Bernhard Rosenkraenzer <bero@redhat.com> 20000902-1
- Create an empty mailspool when creating a user so non-setuid/non-setgid
  MDAs (postfix+procmail) can deliver mail (#41811)
- 20000902
- adapt patches

* Fri Mar  9 2001 Nalin Dahyabhai <nalin@redhat.com>
- don't overwrite user dot files in useradd (#19982)
- truncate new files when moving overwriting files with the contents of other
  files while moving directories (keeps files from looking weird later on)
- configure using %%{_prefix} as the prefix

* Fri Feb 23 2001 Trond Eivind Glomsrod <teg@redhat.com>
- langify

* Wed Aug 30 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up chage behavior (Bug #15883)

* Wed Aug 30 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 20000826
- Fix up useradd man page (Bug #17036)

* Tue Aug  8 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- check for vipw lock before adding or deleting users (Bug #6489)

* Mon Aug  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- take LOG_CONS out of the openlog() call so that we don't litter the
  screen during text-mode upgrades

* Tue Jul 18 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Remove a fixed-size buffer that caused problems when adding a huge number
  of users to a group (>8192 bytes) (Bugs #3809, #11930)

* Tue Jul 18 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- remove dependency on util-linux because it causes prereq loops

* Tue Jul 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- change symlinked man pages to includers
- require /usr/bin/newgrp (util-linux) so that /usr/bin/sg isn't left dangling

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Matt Wilson <msw@redhat.com>
- use mandir for FHS
- added patches in src/ and po/ to honor DESTDIR
- use make install DESTDIR=$RPM_BUILD_ROOT

* Wed Feb 16 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up usermod's symlink behavior (Bug #5458)

* Fri Feb 11 2000 Cristian Gafton <gafton@redhat.com>
- get rid of mkpasswd

* Mon Feb  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix usermod patch to check for shadow before doing any shadow-specific stuff
  and merge it into the pwlock patch

* Sat Feb  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- fix man symlinks

* Wed Feb  2 2000 Nalin Dahyabhai <gafton@redhat.com>
- make -p only change shadow password (bug #8923)

* Mon Jan 31 2000 Cristian Gafton <gafton@redhat.com>
- rebuild to fix dependeencies
- man pages are compressed

* Wed Jan 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix a security bug (adduser could overwrite previously existing
  groups, Bug #8609)

* Sun Jan  9 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- unset LINGUAS before building
- Fix typo in newusers manpage (Bug #8258)
- libtoolize

* Wed Sep 22 1999 Cristian Gafton <gafton@redhat.com>
- fix segfault for userdel when the primary group for the user is not
  defined

* Tue Sep 21 1999 Cristian Gafton <gafton@redhat.com>
- Serial: 1 because now we are using 19990827 (why the heck can't they have
  a normal version just like everybody else?!)
- ported all patches to the new code base

* Thu Apr 15 1999 Bill Nottingham <notting@redhat.com>
- SIGHUP nscd from usermod, too

* Fri Apr 09 1999 Michael K. Johnson <johnsonm@redhat.com>
- added usermod password locking from Chris Adams <cadams@ro.com>

* Thu Apr 08 1999 Bill Nottingham <notting@redhat.com>
- have things that modify users/groups SIGHUP nscd on exit

* Wed Mar 31 1999 Michael K. Johnson <johnsonm@redhat.com>
- have userdel remove user private groups when it is safe to do so
- allow -f to force user removal even when user appears busy in utmp

* Tue Mar 23 1999 Preston Brown <pbrown@redhat.com>
- edit out unused CHFN fields from login.defs.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 7)

* Wed Jan 13 1999 Bill Nottingham <notting@redhat.com>
- configure fix for arm

* Wed Dec 30 1998 Cristian Gafton <gafton@redhat.com>
- build against glibc 2.1

* Fri Aug 21 1998 Jeff Johnson <jbj@redhat.com>
- Note that /usr/sbin/mkpasswd conflicts with /usr/bin/mkpasswd;
  one of these (I think /usr/sbin/mkpasswd but other opinions are valid)
  should probably be renamed.  In any case, mkpasswd.8 from this package
  needs to be installed. (problem #823)

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Apr 21 1998 Cristian Gafton <gafton@redhat.com>
- updated to 980403
- redid the patches

* Tue Dec 30 1997 Cristian Gafton <gafton@redhat.com>
- updated the spec file
- updated the patch so that new accounts created on shadowed system won't
  confuse pam_pwdb anymore ('!!' default password instead on '!')
- fixed a bug that made useradd -G segfault
- the check for the ut_user is now patched into configure

* Thu Nov 13 1997 Erik Troan <ewt@redhat.com>
- added patch for XOPEN oddities in glibc headers
- check for ut_user before checking for ut_name -- this works around some
  confusion on glibc 2.1 due to the utmpx header not defining the ut_name
  compatibility stuff. I used a gross sed hack here because I couldn't make
  automake work properly on the sparc (this could be a glibc 2.0.99 problem
  though). The utuser patch works fine, but I don't apply it.
- sleep after running autoconf

* Thu Nov 06 1997 Cristian Gafton <gafton@redhat.com>
- added forgot lastlog command to the spec file

* Mon Oct 26 1997 Cristian Gafton <gafton@redhat.com>
- obsoletes adduser

* Thu Oct 23 1997 Cristian Gafton <gafton@redhat.com>
- modified groupadd; updated the patch

* Fri Sep 12 1997 Cristian Gafton <gafton@redhat.com>
- updated to 970616
- changed useradd to meet RH specs
- fixed some bugs

* Tue Jun 17 1997 Erik Troan <ewt@redhat.com>
- built against glibc
