%global         momorel 2

Name:           perl-MooseX-Types-Path-Tiny
Version:        0.010
Release:        %{momorel}m%{?dist}
Summary:        Path::Tiny types and coercions for Moose
License:        Apache
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Types-Path-Tiny/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/MooseX-Types-Path-Tiny-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Cwd
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-pushd
BuildRequires:  perl-File-Temp >= 0.18
BuildRequires:  perl-List-Util
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Moose >= 2
BuildRequires:  perl-MooseX-Types
BuildRequires:  perl-MooseX-Types-Stringlike
BuildRequires:  perl-Path-Tiny
BuildRequires:  perl-Test-CheckDeps >= 0.002
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-Simple >= 0.96
Requires:       perl-Moose >= 2
Requires:       perl-MooseX-Types
Requires:       perl-MooseX-Types-Stringlike
Requires:       perl-Path-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides Path::Tiny types for Moose. It handles two important
types of coercion:

%prep
%setup -q -n MooseX-Types-Path-Tiny-%{version}

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install --destdir=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes CONTRIBUTING dist.ini LICENSE META.json perlcritic.rc README tidyall.ini
%{perl_vendorlib}/MooseX/Types/Path/Tiny.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.010-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.010-1m)
- update to 0.010

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.007-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.007-1m)
- update to 0.007

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.006-2m)
- rebuild against perl-5.18.1

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.005-1m)
- update to 0.005

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.002-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.002-2m)
- rebuild against perl-5.16.3

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.002-1m)
- update to 0.002

* Sun Feb 03 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.001-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
