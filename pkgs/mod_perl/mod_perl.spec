%global momorel 5
%global contentdir /var/www

Summary: An embedded Perl interpreter for the Apache Web server
Name: mod_perl
Version: 2.0.8
Release: %{momorel}m%{?dist}
Group: System Environment/Daemons
URL: http://perl.apache.org/
#Source0: http://perl.apache.org/dist/mod_perl-#{version}.tar.gz
Source0: http://www.cpan.org/authors/id/P/PH/PHRED/%{name}-%{version}.tar.gz
NoSource: 0
Source1: perl.conf
Source2: perl.module.conf
Source3: filter-requires.sh
Source4: filter-provides.sh
Patch0: mod_perl-2.0.8-apache24.patch
Patch1: mod_perl-2.0.4-inline.patch
Patch2: mod_perl-2.0.5-nolfs.patch
License: "ASL 2.0"
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 5.8.1
BuildRequires: httpd-devel >= 2.4.3, httpd, gdbm-devel
BuildRequires: apr-devel >= 1.2.0, apr-util-devel
BuildRequires: libdb-devel >= 5.3.15
BuildRequires: perl-Data-Dumper
BuildRequires: perl-Data-Flow
BuildRequires: perl-Tie-IxHash
Requires: perl >= 5.8.1
Requires: httpd-mmn
Requires: perl-Linux-Pid
Conflicts: perl-Apache-Fake
Conflicts: perl-Apache-SizeLimit
Conflicts: perl-Apache-Test
Obsoletes: perl-Apache-Fake
Obsoletes: perl-Apache-SizeLimit
Obsoletes: perl-Apache-Test

%define __perl_requires %{SOURCE3}
%define __perl_provides %{SOURCE4}

%description
Mod_perl incorporates a Perl interpreter into the Apache web server,
so that the Apache web server can directly execute Perl code.
Mod_perl links the Perl runtime library into the Apache web server and
provides an object-oriented Perl interface for Apache's C language
API.  The end result is a quicker CGI script turnaround process, since
no external Perl interpreter has to be started.

Install mod_perl if you're installing the Apache web server and you'd
like for it to directly incorporate a Perl interpreter.

%package devel
Summary: Files needed for building XS modules that use mod_perl
Group: Development/Libraries
Requires: mod_perl = %{version}-%{release}, httpd-devel

%description devel 
The mod_perl-devel package contains the files needed for building XS
modules that use mod_perl.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1 -b .inline
%patch2 -p1 -b .nolfs

%build
for i in Changes SVN-MOVE; do
    iconv --from=ISO-8859-1 --to=UTF-8 $i > $i.utf8
    mv $i.utf8 $i
done

cd docs
for i in devel/debug/c.pod devel/core/explained.pod user/Changes.pod; do
    iconv --from=ISO-8859-1 --to=UTF-8 $i > $i.utf8
    mv $i.utf8 $i
done
cd ..


CFLAGS="$RPM_OPT_FLAGS -fpic" %{__perl} Makefile.PL </dev/null \
         PREFIX=%{buildroot}/%{_prefix} \
         INSTALLDIRS=vendor \
         MP_APXS=%{_httpd_apxs} \
         MP_APR_CONFIG=%{_bindir}/apr-1-config

#make -C src/modules/perl #{?_smp_mflags} OPTIMIZE="$RPM_OPT_FLAGS -fpic"
make -C src/modules/perl OPTIMIZE="$RPM_OPT_FLAGS -fpic"
make

%install
rm -rf %{buildroot}
install -d -m 755 %{buildroot}%{_httpd_moddir}
make install \
    MODPERL_AP_LIBEXECDIR=%{buildroot}%{_httpd_moddir} \
    MODPERL_AP_INCLUDEDIR=%{buildroot}%{_includedir}/httpd

# Remove the temporary files.
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -type f -name perllocal.pod -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
find %{buildroot} -type d -depth -exec rmdir {} 2>/dev/null ';'

# Fix permissions to avoid strip failures on non-root builds.
chmod -R u+w %{buildroot}/*

# Install the config file
install -d -m 755 %{buildroot}%{_httpd_confdir}
install -d -m 755 %{buildroot}%{_httpd_modconfdir}
install -p -m 644 %{SOURCE1} %{buildroot}%{_httpd_confdir}/perl.conf.dist
install -p -m 644 %{SOURCE2} %{buildroot}%{_httpd_modconfdir}/02-perl.conf

# Move set of modules to -devel
devmods="ModPerl::Code ModPerl::BuildMM ModPerl::CScan \
          ModPerl::TestRun ModPerl::Config ModPerl::WrapXS \
          ModPerl::BuildOptions ModPerl::Manifest \
          ModPerl::MapUtil ModPerl::StructureMap \
          ModPerl::TypeMap ModPerl::FunctionMap \
          ModPerl::ParseSource ModPerl::MM \
          Apache2::Build Apache2::ParseSource Apache2::BuildConfig \
          Bundle::ApacheTest"
for m in $devmods; do
   test -f %{buildroot}%{_mandir}/man3/${m}.3pm &&
     echo "%{_mandir}/man3/${m}.3pm*"
   fn=${m//::/\/}
   test -f %{buildroot}%{perl_vendorarch}/${fn}.pm &&
        echo %{perl_vendorarch}/${fn}.pm
   test -d %{buildroot}%{perl_vendorarch}/${fn} && 
        echo %{perl_vendorarch}/${fn}
   test -d %{buildroot}%{perl_vendorarch}/auto/${fn} && 
        echo %{perl_vendorarch}/auto/${fn}
done | tee devel.files | sed 's/^/%%exclude /' > exclude.files
echo "%%exclude %{_mandir}/man3/Apache::Test*.3pm*" >> exclude.files

# perl build script generates *.orig files, they get installed and later they
# break provides so mod_perl requires mod_perl-devel. We remove them here.
find "%{buildroot}" -type f -name *.orig -exec rm -f {} \;

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README* STATUS SVN-MOVE docs/
%config(noreplace) %{_sysconfdir}/httpd/conf.d/perl.conf.dist
%config(noreplace) %{_httpd_modconfdir}/02-perl.conf
%{_bindir}/*
%{_libdir}/httpd/modules/mod_perl.so
%{perl_vendorarch}/auto/*
%dir %{perl_vendorarch}/Apache
%{perl_vendorarch}/Apache/Reload.pm
%{perl_vendorarch}/Apache/SizeLimit*
%{perl_vendorarch}/Apache2
%{perl_vendorarch}/Bundle/*
%{perl_vendorarch}/APR
%{perl_vendorarch}/ModPerl
%{perl_vendorarch}/*.pm
%{_mandir}/man3/*.3*
%exclude %{_mandir}/man3/Apache::Test*.3pm*

%files devel
%defattr(-,root,root,-)
%{_includedir}/httpd/*
%{perl_vendorarch}/Apache/Test*.pm
%{_mandir}/man3/Apache::Test*.3pm*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-2m)
- rebuild against perl-5.18.0

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-6m)
- rebuild against perl-5.16.3

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-5m)
- resolve conflicting with perl-Apache-SizeLimit and perl-Apache-Test

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-4m)
- rebuild against httpd-2.4.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7
- rebuild against perl-5.16.0

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-2m)
- remove conflicting man pages with perl-Apache-SizeLimit

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5
- rebuild against libdb-5.3.15

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-18m)
- rebuild against perl-5.14.2

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.4-17m)
- rebuild against libdb

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-16m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-15m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-13m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-12m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-11m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-10m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-9m)
- rebuild against perl-5.12.0

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-8m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Nov  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-6m)
- all module files should be in main package. if devel package
  includes any of module files, main package (mod_perl) requires many
  -devel packages (e.g. httpd-devel, db4-devel, openldap-devel)

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-5m)
- rebuild against perl-5.10.1

* Fri Jul 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-4m)
- [SECURITY] CVE-2009-0796
- import upstream patch

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-3m)
- remove duplicate directory

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-2m)
- use vendor
- revise %%files so that unprovide %%{perl_vendorarch}/Apache/

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4
-- import Source3 and Patch0,1 from Fedora 11 (2.0.4-8)

* Sun Mar  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-9m)
- add Obsoletes: perl-Apache-Fake for yum upgrade

* Sun Mar  1 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.3-8m)
- add Conflicts: perl-Apache-Fake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-7m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.3-6m)
- rebuild against db4-4.7.25-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-5m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-4m)
- rebuild against openldap-2.4.8

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.3-3m)
- rebuild against perl-5.10.0-1m
- add patches from fc-devel:
  Patch2: mod_perl-2.0.3-perl510.patch
  Patch3: mod_perl-2.0.3-perl510attrs.patch

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.3-2m)
- rebuild against db4-4.6.21

* Fri Jun  8 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.3-1m)
- [SECURITY] CVE-2007-1349 "path_info" DoS Vulnerability
- update to 2.0.3
- sync FC-devel
- add Patch1 mod_perl-2.0.3-CVE-2007-1349.patch
- delete old patch

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.2-3m)
- rebuild against db-4.5

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.2-2m)
- rebuild against expat-2.0.0-1m

* Wed May  3 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.2-1m)
- version up
- use apr-1

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.99_16-6m)
- rebuild against perl-5.8.8

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.99_16-5m)
- rebuild against httpd-2.0.55-3m

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.99_16-4m)
- rebuild against db4.3

* Mon Jun 13 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.99_16-3m)
- rebuilt against perl-5.8.7

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.99_16-2m)
- large file support. rebuild against httpd 2.0.53.

* Thu Aug 26 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.99_16-1m)

* Sun Aug 22 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.99_15-1m)
  update to 1.99_15

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.99_13-6m)
- build against perl-5.8.5

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.99_13-5m)
- stop service

* Mon Jul 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99_13-4m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.99_13-3m)
- remove Epoch from BuildPrereq

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.99_13-2m)
- rebuild against db4.2

* Tue May 18 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.99_13-1m)
- verup

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (1.99_10-3m)
- revised spec for enabling rpm 4.2.
- add sub package named mod_perl-devel.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99_10-2m)
- rebuild against perl-5.8.2

* Thu Nov  6 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.99_10-1m)
- update to 1.99_10

* Sun Nov 02 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99_09-1m)
- rebuild against perl-5.8.1
- update to 1.99_09
- add Patch0: mod_perl-1.99_09-aprinc.patch
- add Patch2: mod_perl-1.99_09-hash.patch
- comment out perlver
- add Requires: httpd-mmn
- add BuildPrereq: gdbm-devel

* Fri Feb  7 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.99_08-1m)
- upgrade 1.99_08 import from rawhide

* Thu Jan  9 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.27-5m)
- remove ToDo from %%files
- add Provides: perl(Apache)

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.27-4m)
- rebuild against perl-5.8.0

* Thu Nov  7 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.27-3m)
- delete BuildPreReq: gcc2.95.3

* Thu Jun  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.27-2k)
- revise Source0 URI and add URL tag
- remove fixpack.pl

* Mon Jun  3 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.26-14k)
- force disable ipv6(define _ipv6 as 0)

* Fri May 24 2002 kourin <kourin@kondara.org>
- (1.26-12k)
- perl-base -> perl in Prereq.

* Fri Apr  5 2002 Toru Hoshina <t@kondara.org>
- (1.26-10k)
- rebuild against rpm 4.0.4-26k, because of libperl.so...
- if you want to use this spec on Asumi, comment buildprereq out please.

* Sun Nov  4 2001 Toru Hoshina <t@kondara.org>
- (1.26-6k)
- rebuild against new environment. because gcc 2.95.3 doesn't recognize
  -fno-merge-constants, use gcc_2_95_3 instead of gcc -V 2.95.3.

* Thu Aug  2 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.26-2k)
- version 1.26
- add PERL_USELARGEFILES=0 option

* Mon May 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.25-14k)
- modified contentdir to /home/httpd

* Sun May 13 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.25-12k)
- remove PERL_USELARGEFLAGS=0

* Sun May 13 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.25-10k)
- Rebuild mod_perl with Makefile.PL PERL_USELARGEFILES=0

* Fri May  4 2001 Toru Hoshina <toru@df-usa.com>
- (1.25-6k)
- rebuild by gcc 2.95.3 :-P

* Sun Mar 15 2001 Daiki Matsdau <dyky@df-usa.com>
- (1.25-4k)
- add /usr/include/apache/modules/perl/startup.perl to start up apache

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.25-3k)
- update to 1.25
- modified Patch1 for fixing
- changable IPv6 function with %{_ipv6} macro
- modified required package from perl to perl-base

* Sat Nov 04 2000 Motonobu Ichimura <famao@kondara.org>
- some changes added to mod_perl-v6.patch

* Sun Oct 29 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against Jirai.

* Mon Jul 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- remove backup files from docs (#14174)

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jun 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- remove workarounds for broken Perl

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- get rid of multiple prefixes

* Wed May 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.24
- remove pre- and post-install scripts and triggers

* Thu May 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- work around weird Perl version reporting problems with a suitably weird check

* Fri Apr 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- modify to be able to rebuild on both 5.003 and 5.6.0
- update to 1.23

* Thu Mar 30 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild with perl 5.6.0
- add perlver macro to spec file to make handling of other perl versions easier

* Thu Mar 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.22

* Fri Mar 03 2000 Cristian Gafton <gafton@redhat.com>
- fixed the postun script to check for upgrades. doh
- add triggerpostun to fix older versions of the package

* Mon Feb 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- make perl a prereq because it's used in %post

* Fri Feb 25 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild against Apache 1.3.12 and EAPI (release 8)

* Mon Feb 21 2000 Preston Brown <pbrown@redhat.com>
- incorporate fixes from Markus Pilzecker <mp@rhein-neckar.netsurf.de>:
- Prefix: /usr
- find apxs binary and package directories automatically

* Thu Feb 17 2000 Preston Brown <pbrown@redhat.com>
- automatically enable/disable in httpd.conf in post/postun.

* Thu Feb 10 2000 Preston Brown <pbrown@redhat.com>
- fix up some strange permissions

* Sun Feb 06 2000 Preston Brown <pbrown@redhat.com>
- rebuild to pick up gzipped man pages, new descr.

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- changed paths for perl 5.00503 (RHL 6.1 version)

* Fri Jul 09 1999 Preston Brown <pbrown@redhat.com>
- added -fPIC to correct functionality on SPARC
- upgrade to 1.21, removed build cruft from old buggy mod_perl days
- added extra documentation that was missing

* Fri Apr 16 1999 Preston Brown <pbrown@redhat.com>
- bump ver. # so SWS mod_perl gets auto-upgraded

* Wed Apr 07 1999 Preston Brown <pbrown@redhat.com>
- bugfix 1.19 release from Doug

* Wed Mar 24 1999 Preston Brown <pbrown@redhat.com>
- experimental patch from Doug MacEachern <dougm@pobox.com> to fix segfault
- rebuilt against apache 1.3.6

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 3)

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Sun Feb 07 1999 Preston Brown <pbrown@redhat.com>
- upgraded to mod_perl 1.18.

* Mon Dec 21 1998 Preston Brown <pbrown@redhat.com>
- Upgraded to mod_perl 1.16.

* Thu Sep 03 1998 Preston Brown <pbrown@redhat.com>
- disabled stacked_handlers.  They still seem busted!
- minor updates so no conflicts with either apache / secureweb
- fixed bug building on multiple architectures

* Wed Sep 02 1998 Preston Brown <pbrown@redhat.com>
- Updates for apache 1.3.x, and mod_perl 1.15

* Fri Feb 27 1998 Cristian Gafton <gafton@redhat.com>
- added a patch to compile it as a shared object for the apache/ssl (and
  future revisions of apache)
