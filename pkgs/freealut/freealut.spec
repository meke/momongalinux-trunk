%global momorel 10

Name: freealut
Version: 1.1.0
Release: %{momorel}m%{?dist}
Summary: implementation of OpenAL's ALUT standard
#'
Group: System Environment/Libraries
License: GPL
URL: http://openal.org/
Source0: http://connect.creativelabs.com/openal/Downloads/ALUT/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openal-devel

%description
freealut is a free implementation of OpenAL's ALUT standard. See the file
AUTHORS for the people involved.
#'

%package devel
Summary: Development files for freealut
Group: Development/Libraries
Requires: %{name} = %{version}-%{release} 
Requires: pkgconfig
Requires: openal-soft-devel

%description devel
Development headers and libraries needed for freealut development

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_libdir}/libalut.so.*

%files devel
%defattr(-, root, root)
%{_bindir}/freealut-config
%{_includedir}/AL/alut.h
%{_libdir}/libalut.a
%{_libdir}/libalut.so
%{_libdir}/pkgconfig/freealut.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-8m)
- full rebuild for mo7 release

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-7m)
- rebuild against openal-soft-1.10.622

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- %%NoSource -> NoSource

* Sun Sep 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- To Main

* Fri Sep 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- initial build
