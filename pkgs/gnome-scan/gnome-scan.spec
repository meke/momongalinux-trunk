%global momorel 12

Summary: Gnome solution for scanning in the desktop on top of libsane
Name: gnome-scan
Version: 0.6.2
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Development/Tools
URL: http://home.gna.org/gnomescan/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.6/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: gnome-scan_babl-0.1.0_api.patch
Patch1: gegl-0.2-support.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: sane-backends-devel >= 1.0.18
BuildRequires: glib2-devel >= 2.14.1
BuildRequires: gtk2-devel >= 2.12.0
BuildRequires: cairo-devel >= 1.4.10
BuildRequires: gimp-devel >= 2.2.17
BuildRequires: libgnome-devel >= 2.20.0
BuildRequires: libgnomeui-devel >= 2.20.0
BuildRequires: libglade2-devel >= 2.6.2
BuildRequires: gegl-devel >= 0.2.0
BuildRequires:	desktop-file-utils gtk-doc gnome-doc-utils-devel
Requires:	gimp
Provides:	flegita = %{version}-%{release}
Provides:	gnomescan = %{version}-%{release}
Obsoletes:	gnomescan < 0.5.2

%description
GnomeScan provide libgnomescan to access scanner and libgnomescanui to
build scan dialogs. It also provide flegita and scan app and
flegita-gimp, a gimp plugin that use GnomeScan libraries. Flegita
allow to simply scan an area from a device and saving to a file in
various format (currently, PNG, JPEG, TIFF and PDF are supported).

%package libs
Summary:	Gnome-scan library
Group:		Development/Libraries
Obsoletes:	gnomescan < 0.5.2

%description libs
Libraries for using gnome-scan

%package devel
Summary: Development files for gnome-scan
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: gtk2-devel
Requires: libgnome
Requires: gegl-devel libgnomeui-devel
Requires: cairo-devel
Requires: gimp-devel
Obsoletes:      gnomescan-devel < 0.5.2

%description devel
Contains development headers and libraries for gnome-scan


%prep
%setup -q
%patch0 -p0 -b .babl
%patch1 -p1 -b .gegl020

%build
autoreconf -vfi
%configure --disable-static \
    --enable-gtk-doc

%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

desktop-file-install --vendor=""			\
	--remove-category Application			\
	--dir %{buildroot}%{_datadir}/applications	\
	--mode 0644					\
	%{buildroot}%{_datadir}/applications/flegita.desktop

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post libs -p /sbin/ldconfig

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/flegita
%{_libdir}/gimp/2.0/plug-ins/flegita-gimp
%{_datadir}/applications/flegita.desktop
%{_datadir}/icons/hicolor/*/*/*.svg
%{_datadir}/gnome-scan/

%files libs
%defattr(-,root,root,-)
%{_libdir}/libgnomescan.so.*
%exclude %{_libdir}/libgnomescan.la
%{_libdir}/gnome-scan-1.0/

%files devel
%defattr(-,root,root,-)
%{_includedir}/gnome-scan-1.0/
%{_libdir}/libgnomescan.so
%{_libdir}/pkgconfig/gnome-scan.pc
%doc %{_datadir}/gtk-doc/html/gnome-scan*/

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-12m)
- rebuild for glib 2.33.2

* Wed Jun 13 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.2-11m)
- import gegl-0.2-support.patch from ubuntu and modify it to enable build
 +- gnome-scan (0.6.2-1.1ubuntu3) quantal; urgency=low
 +- * debian/patches/gegl-0.2-support.patch: look for libgegl as gegl-0.2 with
 +-   pkgconfig; also port to fix uses of deprecated calls.

* Tue Jun 12 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.2-10m)
- [CAN NOT BUILD] rebuild against gegl-0.2.0

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.2-9m)
- rebuild against gegl

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.2-6m)
- full rebuild for mo7 release

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-5m)
- import gnome-scan_babl-0.1.0_api.patch from Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-3m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Sat May  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-2m)
- build fix (use rpath)

* Mon Mar  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-2m)
- rebuild against rpm-4.6

* Mon Dec  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1
- delete patch0
- add patch1

* Tue May  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6-2m)
- comment out gtk-doc (build fix only gtk-doc-1.10)

* Tue May 06 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6-1m)
- update to 0.6 (resync with Fedora, rename from gnomescan to gnome-scan)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gcc43

* Sun Sep 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1
- tarname gnomescan -> gnome-scan

* Sun Feb 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0.4-2m)
- revise %%post %%postun script

* Sat Feb 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0.4-1m)
- initial build
