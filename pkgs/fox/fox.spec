%global momorel 1
%global fox_ver 1.7

Summary: The FOX C++ GUI Toolkit
Name: fox
Version: 1.7.32
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.fox-toolkit.org/
Source0: ftp://ftp.fox-toolkit.org/pub/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: mesa-libGL, mesa-libGLU, mesa-libGLw
Requires: cups, fontconfig, freetype
BuildRequires: cups-devel, fontconfig-devel, freetype-devel
BuildRequires: libjpeg-devel >= 8a
Buildrequires: libtiff-devel >= 4.0.1
BuildRequires: gcc-c++ >= 3.4.1-1m
BuildRequires: mesa-libGL-devel, mesa-libGLU-devel, mesa-libGLw-devel

%define prefix %{_prefix}

%description
FOX is a C++-Based Library for Graphical User Interface Development
FOX supports modern GUI features, such as Drag-and-Drop, Tooltips, Tab
Books, Tree Lists, Icons, Multiple-Document Interfaces (MDI), timers,
idle processing, automatic GUI updating, as well as OpenGL/Mesa for
3D graphics.  Subclassing of basic FOX widgets allows for easy
extension beyond the built-in widgets by application writers.

%package devel
Summary: FOX headers and libraries for build
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
FOX headers and libraries for build

%package example-apps
Summary: FOX example applications
Group: Development/Tools

%description example-apps
editor and file browser, written with FOX

%prep
%setup -q

%build
autoreconf -fiv
#export CFLAGS="%{optflags} -frtti"
#export CPPFLAGS="%{optflags} -frtti"
export GL_LIBS="-lGL -lGLU -lXft -lXrender -lfontconfig -lfreetype -lz -lX11"
%configure --with-xft --enable-cups --with-opengl=yes --enable-release  --enable-threadsafe 
export GL_LIBS="-lGL -lGLU -lXft -lXrender -lfontconfig -lfreetype -lz -lX11"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
cp -p pathfinder/PathFinder %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}
mv  %{buildroot}%{_docdir}/fox-%{fox_ver} %{buildroot}%{_datadir}/fox
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/reswrap
%{_libdir}/*.so.*
%{_mandir}/man1/reswrap.1*
%{_datadir}/fox
%doc doc
%doc ADDITIONS AUTHORS INSTALL LICENSE LICENSE_ADDENDUM README TRACING

%files devel
%defattr(-,root,root)
%{_includedir}/fox*
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/fox17.pc

%files example-apps
%defattr(-,root,root)
%{_bindir}/adie
%{_bindir}/Adie.stx
%{_bindir}/ControlPanel
%{_bindir}/PathFinder
%{_bindir}/calculator
%{_bindir}/shutterbug
%{_mandir}/man1/adie.1*
%{_mandir}/man1/shutterbug.1*
%{_mandir}/man1/ControlPanel.1*
%{_mandir}/man1/PathFinder.1*
%{_mandir}/man1/calculator.1*

%changelog
* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.32-1m)
- update to 1.7.32
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.33-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.33-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.33-8m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.33-7m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.33-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.33-5m)
- rebuild against libjpeg-7

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.33-4m)
- add Patch0: fox-1.6.33-doc-widgets.patch

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (1.6.33-3m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.33-2m)
- rebuild against rpm-4.6

* Thu May 15 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.33-1m)
- update to 1.6.33

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.31-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.31-2m)
- %%NoSource -> NoSource

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.31-1m)
- update to 1.6.31

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.29-1m)
- update to 1.6.29

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.28-1m)
- update to 1.6.28

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.6-4m)
- Requires: freetype2 -> freetype

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.6-3m)
- delete libtool library

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.6.6-2m)
- rebuild against expat-2.0.0-1m

* Sat Jun 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.6-1m)
- updated to 1.6.6

* Mon Jun 05 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.5-1m)
- use latest stable

* Sun Jun  4 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.7.0-1m)
- up to 1.7.0

* Mon Mar 27 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.6.0-3m)
- change Mesa -> mesa-*

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-2m)
- revise for xorg-7.0
- add export GL_LIBS to front of %configure

* Sun Jan 01 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Tue Aug 02 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.16-1m)
- update to 1.2.16
- add %{_bindir}/fox-config to devel package
- add BuileRequires: fontconfig-devel, freetype2-devel
- add Requires: cups, fontconfig, freetype2

* Thu Aug 19 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.5-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++
- remove "-frtti" from CFLAGS and CPPFLAGS, thanks to Hiromasa YOSHIMOTO <yosimoto@limu.is.kyushu-u.ac.jp>

* Fri Jun 25 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.5-1m)
- Minor bugfixes

* Wed Jun 09 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.3-1m)
- Minor bugfixes

* Thu May 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.1-1m)
- update to new stable version 1.2.1

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.49-2m)
- revised spec for enabling rpm 4.2.

* Fri Mar 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.49-1m)
- update to 1.1.49

* Wed Oct  1 2003 zunda <zunda at freeshell.org>
- (1.1.26-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Aug 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.26-2m)
- add defattr

* Tue May  6 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.26-1m)
- update to 1.1.26
- add TODO to %%doc
- add --enable-threadsafe to %%configure
- change BuildRoot

* Sun Apr 27 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.25-2m)
- add Requires for devel package
- make simultaneously

* Sun Apr 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.25-1m)
- import fox for cuppa

* Tue Oct 10 2000 David Sugar <dyfet@ostel.com> 0.99.132-3
- rtti forced for rpm build specs that use -fno-rtti.

* Fri Mar 24 2000 Jose Romildo Malaquias <romildo@iceb.ufpo.b> 0.99.122-1
- new version

* Fri Mar 24 2000 Jose Romildo Malaquias <romildo@iceb.ufpo.b> 0.99.119-1
- new version

* Sun Mar 05 2000 Jose Romildo Malaquias <romildo@iceb.ufpo.b>
- some adaptations

* Tue Nov 10 1998 Rene van Paassen <M.M.vanPaassen@lr.tudelft.nl>
- initial package





