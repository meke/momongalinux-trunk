%global momorel 1

Summary: a set of plugins for gedit
Name:    gedit-plugins
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/
Requires(pre): GConf2
Requires(pre): scrollkeeper
Requires(pre): desktop-file-utils
Requires(pre): rarian
BuildRequires: gedit
BuildRequires: GConf2
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: rarian

BuildRequires: glib2-devel >= 2.20.0
BuildRequires: gtk3-devel
BuildRequires: gtksourceview3-devel
BuildRequires: gedit-devel >= 3.1
BuildRequires: libglade2-devel >= 2.6.3
BuildRequires: gnome-vfs2-devel >= 2.24.0
BuildRequires: pygtk2-devel >= 2.14.1
BuildRequires: pygtksourceview-devel >= 2.4.0
BuildRequires: gnome-python2-desktop >= 2.26.0
BuildRequires: gucharmap-devel >= 3.0

%description
gedit-plugins are a set of plugins for gedit.

%prep
%setup -q

%build
%configure --enable-silent-rules \
    --disable-schemas-install \
    --disable-scrollkeeper \
    --enable-python \
    CFLAGS="$CFLAGS -I/usr/include/python2.6"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
update-desktop-database %{_datadir}/applications &> /dev/null ||:
rarian-sk-update

%postun
update-desktop-database %{_datadir}/applications &> /dev/null ||:
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc README COPYING ChangeLog NEWS AUTHORS
%{_libdir}/gedit/plugins/*
%{_datadir}/gedit/plugins/*
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.drawspaces.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.plugins.terminal.gschema.xml

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Fri Sep  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.1-1m)
- update to 3.5.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.5-1m)
- update to 3.1.5

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.4-1m)
- update to 3.1.4

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.0-4m)
- rebuild for new GCC 4.5

* Thu Sep  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.30.0-3m)
- [BUG FIX] fix %%post and %%postun

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-2m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.4-1m)
- update to 2.29.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.1-1m)
- update to 2.27.1

* Mon Jul 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Sun Apr 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Feb 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.3-1m)
- update to 2.25.3

* Thu Jan 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.3-4m)
- add BuildPrereq:

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.3-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.22.3-2m)
- rebuild against python-2.6.1-1m

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.2-2m)
- change %%preun script

* Mon Apr 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.0-2m)
- %%NoSource -> NoSource

* Sun Sep 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Fri Mar  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.2-1m)
- initial build
