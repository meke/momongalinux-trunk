# Generated from loquacious-1.9.1.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname loquacious

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Descriptive configuration files for Ruby written in Ruby
Name: rubygem-%{gemname}
Version: 1.9.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubygems.org/gems/loquacious
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Descriptive configuration files for Ruby written in Ruby.
Loquacious provides a very open configuration system written in ruby and
descriptions for each configuration attribute. The attributes and descriptions
can be iterated over allowing for helpful information about those attributes
to
be displayed to the user.
In the simple case we have a file something like
Loquacious.configuration_for('app') {
name 'value', :desc => "Defines the name"
foo  'bar',   :desc => "FooBar"
id   42,      :desc => "Ara T. Howard"
}
Which can be loaded via the standard Ruby loading mechanisms
Kernel.load 'config/app.rb'
The attributes and their descriptions can be printed by using a Help object
help = Loquacious.help_for('app')
help.show :values => true        # show the values for the attributes, too
Descriptions are optional, and configurations can be nested arbitrarily deep.
Loquacious.configuration_for('nested') {
desc "The outermost level"
a {
desc "One more level in"
b {
desc "Finally, a real value"
c 'value'
}
}
}
config = Loquacious.configuration_for('nested')
p config.a.b.c  #=> "value"
And as you can see, descriptions can either be given inline after the value or
they can appear above the attribute and value on their own line.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-1m)
- update 1.9.1

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-1m)
- update 1.9.0

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-1m)
- update 1.8.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-1m)
- update 1.7.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.4-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- Initial package for Momonga Linux
