%global momorel 1
Name:           baobab
Version:        3.6.2
Release: %{momorel}m%{?dist}
Summary:        A graphical directory tree analyzer

Group:          Applications/System
License:        GPLv2+ and GFDL
URL:            https://live.gnome.org/GnomeUtils/Baobab
Source0:        http://download.gnome.org/sources/baobab/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  gtk3-devel
BuildRequires:  libgtop2-devel
BuildRequires:  intltool
BuildRequires:  gnome-doc-utils
BuildRequires:  desktop-file-utils
BuildRequires:  itstool

Obsoletes: gnome-utils < 3.3
Obsoletes: gnome-utils-devel < 3.3
Obsoletes: gnome-utils-libs < 3.3

%description
Baobab is able to scan either specific directories or the whole filesystem, in
order to give the user a graphical tree representation including each
directory size or percentage in the branch.  It also auto-detects in real-time
any change made to your home folder as far as any mounted/unmounted device.

%prep
%setup -q


%build
%configure
%make 


%install
make install DESTDIR=%{buildroot}

desktop-file-validate %{buildroot}%{_datadir}/applications/baobab.desktop

%find_lang %{name} --with-gnome


%post
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :


%postun
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :


%files -f %{name}.lang
%doc AUTHORS COPYING NEWS README COPYING.docs
%{_bindir}/baobab
#%{_datadir}/baobab
%{_datadir}/applications/baobab.desktop
%{_datadir}/help/*/baobab
%{_datadir}/icons/hicolor/*/apps/baobab.png
%{_datadir}/glib-2.0/schemas/org.gnome.baobab.gschema.xml
%{_mandir}/man1/baobab.1.bz2
%{_datadir}/icons/hicolor/scalable/actions/view-ringschart-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/view-treemap-symbolic.svg

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Wed Aug 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Sat Aug 04 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- import from fedora

