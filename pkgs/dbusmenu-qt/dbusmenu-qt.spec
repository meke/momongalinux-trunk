%global         momorel 1
%global         src_name lib%{name}

Summary:        A Qt implementation of the DBusMenu protocol
Name:           dbusmenu-qt
Version:        0.9.2
Release:        %{momorel}m%{?dist}
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            https://launchpad.net/libdbusmenu-qt
Source0:        http://launchpad.net/%{src_name}/trunk/%{version}/+download/%{src_name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake
BuildRequires:  pkgconfig
BuildRequires:  qjson-devel
BuildRequires:  qt-devel >= 4.8.2

%description
This library provides a Qt implementation of the DBusMenu protocol.

The DBusMenu protocol makes it possible for applications to export and import
their menus over DBus.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
%{summary}.

%prep
%setup -q -n %{src_name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} .. 
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%check
# unfortunately, most of these require an active X/dbus session
make -C %{_target_platform}/tests check ||:

%clean
rm -rf %{buildroot} 

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING NEWS README
%{_libdir}/libdbusmenu-qt.so.2*
%{_docdir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_includedir}/dbusmenu-qt
%{_libdir}/pkgconfig/dbusmenu-qt.pc
%{_libdir}/libdbusmenu-qt.so

%changelog
* Tue Jul 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Sun Oct  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Sat May 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-2m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Sat Mar 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-4m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-3m)
- specify PATH for Qt4

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-2m)
- fix build failure

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-2m)
- full rebuild for mo7 release

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-2m)
- rebuild against qt-4.6.3-1m

* Sat May 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-1m)
- version 0.3.3

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-1m)
- import from Fedora devel

* Sun Apr 25 2010 Rex Dieter <rdieter@fedoraproject.org> 0.3.2-2
- pkg rename s/libdbusmenu-qt/dbusmenu-qt/
- Provides: libdbusmenu-qt(-devel)

* Sun Apr 25 2010 Rex Dieter <rdieter@fedoraproject.org> 0.3.2-1
- dbusmenu-qt-0.3.2

