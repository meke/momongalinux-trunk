%global momorel 8

Summary: A tool for adjusting the volume of wave files
Name: normalize
Version: 0.7.7
Release: %{momorel}m%{?dist}
License: GPL
URL: http://normalize.nongnu.org/
Group: Applications/Multimedia
Source0: http://savannah.nongnu.org/download/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: audiofile-devel
BuildRequires: flac-devel
BuildRequires: libmad
BuildRequires: xmms-devel
BuildRequires: glib1-devel
BuildRequires: gtk+1-devel
BuildRequires: gawk
BuildRequires: gettext

%description
normalize is an overly complicated tool for adjusting the volume of
wave files to a standard volume level.  This is useful for things like
creating mp3 mixes, where different recording levels on different
albums can cause the volume to vary greatly from song to song.

%package xmms
Summary: normalize effect plugin for xmms
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: xmms

%description xmms
volume adjust plugin for xmms.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of librva.la
rm -f %{buildroot}%{_libdir}/xmms/Effect/librva.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING INSTALL NEWS README THANKS TODO
%{_bindir}/normalize
%{_bindir}/normalize-mp3
%{_bindir}/normalize-ogg
%{_datadir}/locale/*/LC_MESSAGES/normalize.mo
%{_mandir}/man1/normalize-mp3.1*
%{_mandir}/man1/normalize.1*

%files xmms
%defattr(-,root,root)
%{_libdir}/xmms/Effect/librva.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.7-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.7-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.7-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.7-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.7-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.7-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.7-2m)
- %%NoSource -> NoSource

* Thu Apr  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.7-1m)
- initial package for k3b-1.0
- Summary and %%description are imported from cooker
