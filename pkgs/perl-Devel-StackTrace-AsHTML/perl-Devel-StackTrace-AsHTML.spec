%global         momorel 6

Name:           perl-Devel-StackTrace-AsHTML
Version:        0.14
Release:        %{momorel}m%{?dist}
Summary:        Displays stack trace in HTML
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Devel-StackTrace-AsHTML/
Source0:        http://www.cpan.org/authors/id/M/MI/MIYAGAWA/Devel-StackTrace-AsHTML-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Devel-StackTrace
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Filter-Util-Call
Requires:       perl-Devel-StackTrace
Requires:       perl-Filter-Util-Call
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Devel::StackTrace::AsHTML adds as_html method to Devel::StackTrace which
displays the stack trace in beautiful HTML, with code snippet context and
function parameters. If you call it on an instance of
Devel::StackTrace::WithLexicals, you even get to see the lexical variables
of each stack frame.

%prep
%setup -q -n Devel-StackTrace-AsHTML-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Devel/StackTrace/AsHTML.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-2m)
- rebuild against perl-5.16.3

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Tue Feb 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-2m)
- rebuild for new GCC 4.6

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.09-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.09-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-2m)
- rebuild against perl-5.12.0

* Tue Mar 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Fri Mar 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.03-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 NARITA Koichi <pulsar@momonga-linuxorg>
- (0.03-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
