%global         momorel 2

Name:           perl-Module-Install
Version:        1.08
Release:        %{momorel}m%{?dist}
Summary:        Standalone, extensible Perl module installer
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Module-Install/
Source0:        http://www.cpan.org/authors/id/B/BI/BINGOS/Module-Install-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-test.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.005
BuildRequires:  perl-Archive-Tar >= 1.44
BuildRequires:  perl-Devel-PPPort >= 3.16
BuildRequires:  perl-ExtUtils-Install >= 1.52
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-ExtUtils-ParseXS >= 2.19
BuildRequires:  perl-File-Remove >= 1.42
BuildRequires:  perl-JSON >= 2.14
BuildRequires:  perl-libwww-perl >= 5.812
BuildRequires:  perl-Module-Build >= 0.29
BuildRequires:  perl-Module-CoreList >= 2.17
BuildRequires:  perl-Module-ScanDeps >= 0.89
BuildRequires:  perl-PAR-Dist >= 0.29
BuildRequires:  perl-Parse-CPAN-Meta >= 1.39
BuildRequires:  perl-PathTools >= 3.28
BuildRequires:  perl-Test-Harness >= 3.13
BuildRequires:  perl-Test-Simple >= 0.86
BuildRequires:  perl-YAML-Tiny >= 1.38
Requires:       perl-Archive-Tar >= 1.44
Requires:       perl-Devel-PPPort >= 3.16
Requires:       perl-ExtUtils-Install >= 1.52
Requires:       perl-ExtUtils-ParseXS >= 2.19
Requires:       perl-File-Remove >= 1.42
Requires:       perl-JSON >= 2.14
Requires:       perl-libwww-perl >= 5.812
Requires:       perl-Module-Build >= 0.29
Requires:       perl-Module-CoreList >= 2.17
Requires:       perl-Module-ScanDeps >= 0.89
Requires:       perl-PAR-Dist >= 0.29
Requires:       perl-Parse-CPAN-Meta >= 1.39
Requires:       perl-PathTools >= 3.28
Requires:       perl-YAML-Tiny >= 1.38
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Module::Install is a package for writing installers for CPAN (or CPAN-like)
distributions that are clean, simple, minimalist, act in a strictly correct
manner with ExtUtils::MakeMaker, and will run on any Perl installation
version 5.005 or newer.

%prep
%setup -q -n Module-Install-%{version}
%patch0 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Module/*.pm
%{perl_vendorlib}/Module/*.pod
%{perl_vendorlib}/Module/Install
%{perl_vendorlib}/inc/Module/Install.pm
%{perl_vendorlib}/inc/Module/Install
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-2m)
- rebuild against perl-5.14.2

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.00-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.00-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99-2m)
- full rebuild for mo7 release

* Tue Jun  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Mon May 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-2m)
- rebuild against perl-5.12.1

* Mon May 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-2m)
- rebuild against perl-5.12.0

* Thu Mar 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- Specfile re-generated by cpanspec 1.78.

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Sun Feb  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.91-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-2m)
- rebuild against perl-5.10.1

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-1m)
- version down to 0.86, perl-Plagger could be built

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.87-1m)
- version down to 0.87, perl-Benchmark-Timer could be built

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Sun May  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-1m)
- update to 0.86

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-1m)
- update to 0.85

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-1m)
- update to 0.81

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-1m)
- update to 0.79

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.77-2m)
- rebuild against rpm-4.6

* Sun Dec 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.77-1m)
- update to 0.77
- use make pure_install instead of make install

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.68-3m)
- rebuild against gcc43

* Sat Mar 29 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.68-2m)
- version down 0.68 for perl-Plagger

* Tue Mar 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71
- add BuildRequires: perl-YAML-Tiny

* Wed Mar 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Thu Nov  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.65-2m)
- use vendor

* Thu Mar  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.65-1m)
- update to 0.65

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.64-1m)
- spec file was autogenerated
