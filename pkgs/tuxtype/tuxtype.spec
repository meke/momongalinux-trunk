%global momorel 12
%global srcname tuxtype2

Summary:	Tux Typing - Graphical, educational typing tutorial game
Name:		tuxtype
Version:	1.5.3
Release:        %{momorel}m%{?dist}
License:	GPL
Group:		Amusements/Games
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{srcname}-%{version}.tar.bz2 
NoSource:       0
Source1:	%{name}.desktop
Patch0:         tuxtype2-1.5.3-linking.patch
URL:		http://tuxtype.sourceforge.net/
BuildRequires:	SDL-devel
BuildRequires:	SDL_image-devel
BuildRequires:	SDL_mixer-devel
BuildRequires:	ImageMagick
BuildRequires:	SDL_ttf
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Educational typing tutor starring Tux, the Linux Penguin. Object of
the game is to catch fish as they drop from the top of the screen.
Each fish has a letter or a word written on it, and Tux eats the fish
by the player pressing the associated key or typing the appropriate
word. Intended to be cute and fun for children learning to type and
spell.

%prep
%setup -q -n %{srcname}-%{version}
%patch0 -p1 -b .linking

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR="%{buildroot}" transform='s,x,x'

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48}/apps
convert -scale 16x16 %{srcname}.ico %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 %{srcname}.ico %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert -scale 48x48 %{srcname}.ico %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# clean up
rm -rf %{buildroot}%{_prefix}/doc/%{srcname}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(644,root,root,755)
%doc AUTHORS COPYING ChangeLog README TODO
%doc tuxtype/docs/en/howtotheme.html
%attr(755,root,root) %{_bindir}/%{srcname}
%{_datadir}/%{srcname}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.3-10m)
- full rebuild for mo7 release

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-9m)
- explicitly link libm

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.3-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.3-5m)
- %%NoSource -> NoSource

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-4m)
- fix permission of desktop file

* Fri Apr 13 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildRequires: SDL_ttf

* Wed Mar 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-3m)
- install icons
- add Icon to desktop file
- add Categories=KidsGame; to desktop file
- add tuxtype/docs/en/howtotheme.html to %%doc
- add BuildRequires: ImageMagick
- remove BuildRequires: libvorbis

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.3-2m)
- To Main

* Sat Sep 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3
- modify desktop file

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0-5m)
- revised spec for rpm 4.2.

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-4m)
  rebuild against DirectFB 0.9.18 

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.0-3m)
- added BuildPreReq: libvorbis-devel >= 1.0-2m

* Thu May  8 2002 Toru Hoshina <t@Kondara.org>
- (1.0-2k)
- 1st release.
