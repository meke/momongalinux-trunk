%global momorel 44

%global debug_package %{nil}

%global DATE 20040701
%global gcc_version 3.2.3
%global gcc_release %{momorel}m%{?dist}
%global _unpackaged_files_terminate_build 0
%define multilib_64_archs sparc64 ppc64 s390x x86_64
%ifarch s390x
%define multilib_32_arch s390
%endif
%ifarch sparc64
%define multilib_32_arch sparc
%endif
%ifarch ppc64
%define multilib_32_arch ppc
%endif
%ifarch x86_64
%define multilib_32_arch i686
%endif
%global pkg_suffix 3.2
%global bin_suffix _3_2

Summary: Compatibility GNU Compiler Collection
Name: gcc3.2
Version: %{gcc_version}
Release: %{gcc_release}
License: "GPLv2+ with exceptions"
Group: Development/Languages
URL: http://gcc.gnu.org/

### include local configuration
%{?include_specopt}
## Configuration
# apply stack-smashing protection patch
%{?!with_ssp: %global with_ssp 1}
# do test
%{?!do_test: %global do_test 1}

Source0: gcc-3.2.3-20040701.tar.bz2
#http://www.research.ibm.com/trl/projects/security/ssp/
Source2: http://www.research.ibm.com/trl/projects/security/ssp/gcc3_2_2/protector-3.2.2-10.tar.gz
NoSource: 2
Source3: gcc-libstdc++-compat.tar.bz2

# rediff of Source2
Patch0: gcc-3.2.3-20040701-stack-protector.patch
Patch1: gcc32-sparc32-hack.patch
Patch4: gcc32-bison-1.875c.patch
Patch20: gcc-3.2.2-libjava.patch
Patch60: gcc32-obstack-lvalues.patch
Patch61: gcc32-fc4-compile.patch
Patch63: gcc32-bison.patch
Patch100: gcc-3.2.3-trap-0.patch
Patch101: gcc32-ppc64.patch
Patch1000: gcc-3.2.3-fix_tail.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: binutils >= 2.12.90.0.10-2m
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: gettext
BuildRequires: momonga-rpmmacros >= 20090121-1m
# Make sure pthread.h doesn't contain __thread tokens
BuildRequires: glibc-devel >= 2.2.90-12
%ifarch x86_64
BuildConflicts: libgcc(x86-32)
%endif

Requires: cpp >= %{version}-%{release}
# Need .eh_frame ld optimizations
# Need proper visibility support
Requires: binutils >= 2.12.90.0.7-1
# Make sure gdb will understand DW_FORM_strp
Conflicts: gdb < 5.1-2
Requires: glibc-devel >= 2.2.90-12
Requires: libgcc >= %{version}-%{release}

%global _gnu %{nil}
%{expand: %%{global} _gcc_is_%{_target_cpu} %%{nil}}
%{?_gcc_is_sparc: %global gcc_target_platform sparc64-%{_vendor}-%{_target_os}}
%{!?_gcc_is_sparc: %global gcc_target_platform %{_target_platform}}

%description
The gcc package contains the GNU Compiler Collection version 3.2.

%package c++
Summary: C++ support for GCC
Group: Development/Languages
Requires: gcc%{pkg_suffix} = %{version}-%{release}
Requires: libstdc++%{pkg_suffix} = %{version}-%{release}
Requires: libstdc++%{pkg_suffix}-devel = %{version}-%{release}

%description c++
This package adds C++ support to the GNU Compiler Collection.
It includes support for most of the current C++ specification,
including templates and exception handling.

%package -n libstdc++%{pkg_suffix}
Summary: GNU Standard C++ Library
Group: System Environment/Libraries
Requires(post): coreutils
Provides: libstdc++-compat
%ifnarch ppc64
Provides: libstdc++-libc6.1-1.so.2
Obsoletes: libstdc++2.95.3
Provides: libstdc++2.95.3
Obsoletes: libstdc++2.96
Provides: libstdc++2.96
%endif

%description -n libstdc++%{pkg_suffix}
The libstdc++ package contains a rewritten standard compliant GCC Standard
C++ Library.

%package -n libstdc++%{pkg_suffix}-devel
Summary: Header files and libraries for C++ development
Group: Development/Libraries
Requires: libstdc++%{pkg_suffix} = %{version}-%{release}

%description -n libstdc++%{pkg_suffix}-devel
This is the GNU implementation of the standard C++ libraries. This
package includes the header files and libraries needed for C++
development. This includes rewritten implementation of STL.

%ifarch sparc
%package sparc32
Summary: The C compiler optimized for generating SPARC 32bit code
Group: Development/Languages
Requires: gcc%{pkg_suffix} = %{version}-%{release}, %{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/64/libgcc.a

%description sparc32
This package contains the GNU C compiler which only supports generating
32bit SPARC code, but should be faster than the 32/64bit gcc package. You
should install this package if you want to trade disk space required for
this package for faster compilation of 32bit code.

%package c++-sparc32
Summary: The C++ compiler optimized for generating SPARC 32bit code
Group: Development/Languages
Requires: gcc%{pkg_suffix}-c++ = %{version}-%{release}, gcc%{pkg_suffix}-sparc32 = %{version}-%{release}
Requires: %{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/64/libstdc++.so

%description c++-sparc32
This package contains the GNU C++ compiler which only supports generating
32bit SPARC code, but should be faster than the 32/64bit gcc package. You
should install this package if you want to trade disk space required for
this package for faster compilation of 32bit code.
%endif # sparc

%prep
%setup -q -n gcc-%{gcc_version}-%{DATE}

%if %{with_ssp}
%patch0 -p1 -b .stack-protector~
pushd gcc
tar xzf %{SOURCE2}
rm -f ./protector.dif ./protectonly.dif
popd
%endif # with_ssp

%ifarch sparc
%patch1 -p0 -b .sparc32-hack~
%endif # sparc
%ifarch ppc64
%patch101 -p1 -b .ppc64~
%endif

%patch4 -p0 -b .bison-1.875c~
%patch60 -p0 -b .obstack-lvalues~
%patch61 -p0 -b .fc4-compile~
%patch63 -p0 -b .bison~
%patch1000 -p1 -b .fix_tail~

# perl -pi -e 's/3\.2\.1/3.2/' gcc/version.c gcc/f/version.c gcc/ada/gnatvsn.ads
perl -pi -e 's/"%{gcc_version}"/"%{gcc_version} \(release\)"/' gcc/version.c
perl -pi -e 's/\((prerelease|experimental|release|Red Hat[^)]*)\)/\(\Momonga Linux %{momonga} %{version}-%{gcc_release}\)/' gcc/version.c
#perl -pi -e 's/\((prerelease|experimental|release|Red Hat[^)]*)\)/\(%{version}-%{gcc_release}\)/' gcc/ada/gnatvsn.ads
# perl -pi -e 's/#define GCCBUGURL.*$/#define GCCBUGURL "<URL:http:\/\/www.momonga-linux.org\/>"/' gcc/system.h
# The version string is now longer than 32 bytes and the line containing it
# doesn't fit into 80 columns
#perl -pi -e 's/:= 32/:= 64/;s/(Gnat_Version_String.*:=)/\1\n    /' gcc/ada/gnatvsn.ads

./contrib/gcc_update --touch

mkdir compat
tar xzf %{SOURCE3} -C compat
%patch100 -p1 -b .trap-0

%build
%ifarch ppc64
%global optflags %(echo %{optflags} -mminimal-toc)
export CPPFLAGS="-mminimal-toc"
%endif

rm -fr obj-%{gcc_target_platform}
mkdir obj-%{gcc_target_platform}
cd obj-%{gcc_target_platform}

if [ ! -f /usr/lib/locale/de_DE/LC_CTYPE ]; then
  mkdir locale
  localedef -f ISO-8859-1 -i de_DE locale/de_DE
  export LOCPATH=`pwd`/locale:/usr/lib/locale
fi

CC=gcc
OPT_FLAGS=`echo %{optflags} | sed -e 's/-fno-rtti//g' -e 's/-fno-exceptions//g'`
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-m64//g;s/-m32//g;s/-m31//g'`
%ifarch %{ix86}
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-mtune=pentium4/-mcpu=i686/g'`
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-mtune=generic/-mcpu=i686/g'`
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-mtune=atom/-mcpu=i686/g'`
%endif
%ifarch x86_64
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-mtune=nocona//g'`
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-mtune=generic//g'`
%endif
%ifarch sparc sparcv9 sparc64
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-mcpu=ultrasparc/-mtune=ultrasparc/g'`
%endif
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-Wall//g' -e 's/-Wp,-D_FORTIFY_SOURCE=2//g'`
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-fexceptions//g' -e 's/-fasynchronous-unwind-tables//g'`
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-fstack-protector//g' -e 's/--param=ssp-buffer-size=[0-9]*//g'`
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-fPIC//g'`
OPT_FLAGS=`echo $OPT_FLAGS | sed -e 's/-Wno-unused-but-set-variable//g' -e 's/-Wno-unused-but-set-parameter//g'`
%ifarch sparc64
cat > gcc64 <<"EOF"
#!/bin/sh
exec /usr/bin/gcc -m64 "$@"
EOF
chmod +x gcc64
CC=`pwd`/gcc64
%endif
%ifarch ppc64
if gcc -m64 -xc -S /dev/null -o - > /dev/null 2>&1; then
  cat > gcc64 <<"EOF"
#!/bin/sh
exec /usr/bin/gcc -m64 "$@"
EOF
  chmod +x gcc64
  CC=`pwd`/gcc64
fi
%endif
CC="$CC" CFLAGS="$OPT_FLAGS" CXXFLAGS="$OPT_FLAGS" XCFLAGS="$OPT_FLAGS" TCFLAGS="$OPT_FLAGS" \
	GCJFLAGS="$OPT_FLAGS" \
	../configure --prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
	--enable-shared --enable-threads=posix --disable-checking \
	--with-system-zlib --enable-__cxa_atexit \
	--enable-languages='c,c++' \
	--disable-libgcj --disable-libjava --disable-libobjc \
%ifarch sparc sparcv9
	--host=%{gcc_target_platform} --build=%{gcc_target_platform} --target=%{gcc_target_platform} --with-cpu=v7
%endif
%ifarch ppc
	--host=%{gcc_target_platform} --build=%{gcc_target_platform} --target=%{gcc_target_platform} --with-cpu=default32
%endif
%ifnarch sparc sparcv9 ppc
	--host=%{gcc_target_platform}
%endif

%if %{with_ssp}
SFLAGS=-j1
%else
SFLAGS="%{_smp_mflags}"
%endif

make $SFLAGS BOOT_CFLAGS="$OPT_FLAGS" bootstrap-lean
#make $SFLAGS BOOT_CFLAGS="$OPT_FLAGS" bootstrap

%if %{do_test}
# run the tests.
make %{?_smp_mflags} -k check || :
echo ====================TESTING=========================
( ../contrib/test_summary || : ) 2>&1 | sed -n '/^cat.*EOF/,/^EOF/{/^cat.*EOF/d;/^EOF/d;/^LAST_UPDATED:/d;p;}'
echo ====================TESTING END=====================
%endif

# Make protoize
make -C gcc CC="./xgcc -B ./ -O2" proto

%ifarch sparc
# Build the -m32 only compiler which does not use long long for HOST_WIDE_INT
mkdir gcc32
cd gcc32
OPT_FLAGS=`echo "$OPT_FLAGS"|sed -e 's/-fno-rtti//g' -e 's/-fno-exceptions//g'`
echo "#!/bin/sh" > gcc32
echo "exec `cd ../gcc; pwd`/xgcc -B `cd ../gcc; pwd`/ $OPT_FLAGS \$*" >> gcc32
chmod +x gcc32
CC=`pwd`/gcc32 CFLAGS="$OPT_FLAGS" CXXFLAGS="$OPT_FLAGS" XCFLAGS="$OPT_FLAGS" \
  TCFLAGS="$OPT_FLAGS" ../../gcc/configure --prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
  --enable-shared --enable-threads=posix --disable-checking \
  --with-system-zlib --enable-__cxa_atexit \
  --host=%{_target_platform}
make cc1 cc1plus
cd ..
%endif # sparc

%install
rm -fr %{buildroot}

cd obj-%{gcc_target_platform}

if [ ! -f /usr/lib/locale/de_DE/LC_CTYPE ]; then
  export LOCPATH=`pwd`/locale:/usr/lib/locale
fi

TARGET_PLATFORM=%{gcc_target_platform}

# There are some MP bugs in libstdc++ and libjava Makefiles
make -C %{gcc_target_platform}/libstdc++-v3

make prefix=%{buildroot}%{_prefix} mandir=%{buildroot}%{_mandir} \
  infodir=%{buildroot}%{_infodir} install

# multilib gcc use %{_prefix}/lib/gcc-lib (not %{_libdir}/gcc-lib)
FULLPATH=%{buildroot}%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}

file %{buildroot}/%{_prefix}/bin/* | grep ELF | cut -d':' -f1 | xargs strip || :
strip $FULLPATH/{cc1,cc1plus,cpp0,tradcpp0}

#install the compatibility libstdc++ library
LIB_ARCH=`echo %{_target_cpu} | sed -e 's/i.86/i386/' -e 's/alphaev5/alpha/'`
[ -d ../compat/$LIB_ARCH ] && install -m 755 ../compat/$LIB_ARCH/* %{buildroot}%{_prefix}/lib/

# This is required for the old RedHat 6. based programs ...
pushd %{buildroot}%{_prefix}/lib
%ifarch ppc
ln -sf libstdc++-2-libc6.1-1.1-2.9.0.so libstdc++-libc6.1-1.1.so.2 
%endif
%ifnarch ppc ppc64
ln -sf libstdc++-2-libc6.2-1-2.9.0.so libstdc++-libc6.1-1.so.2
%endif
popd

# fix some things
ln -sf gcc %{buildroot}%{_prefix}/bin/cc
mkdir -p %{buildroot}/lib
ln -sf ..%{_prefix}/bin/cpp %{buildroot}/lib/cpp
rm -f %{buildroot}%{_infodir}/dir
#ln -sf gcc %{buildroot}%{_prefix}/bin/gnatgcc

%ifarch sparc
# Install the sparc -m32 only compilers
FULLPATH32=%{buildroot}%{_libdir}/gcc-lib/%{_target_platform}/%{gcc_version}
mkdir -p $FULLPATH32
install -m 755 gcc32/cc1 $FULLPATH32/
install -m 755 gcc32/cc1plus $FULLPATH32/
ln -sf ../../%{gcc_target_platform}/%{gcc_version}/include $FULLPATH32/
strip $FULLPATH32/{cc1,cc1plus}
ln -f %{buildroot}%{_prefix}/bin/%{gcc_target_platform}-gcc \
  %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc
%endif # sparc
%ifarch sparc64
ln -f %{buildroot}%{_prefix}/bin/%{gcc_target_platform}-gcc \
  %{buildroot}%{_prefix}/bin/sparc-%{_vendor}-%{_target_os}-gcc
%endif # sparc64

%ifarch sparc
FULLLPATH=$FULLPATH/lib32
%endif # sparc
%ifarch sparc64
FULLLPATH=$FULLPATH/lib64
%endif # sparc64
if [ -n "$FULLLPATH" ]; then
  mkdir -p $FULLLPATH
else
  FULLLPATH=$FULLPATH
fi

mkdir -p %{buildroot}/%{_lib}
%ifarch ppc64
mkdir -p %{buildroot}%{_libdir}
mv -f %{buildroot}%{_prefix}/lib/libgcc_s.so.1 %{buildroot}/%{_lib}/libgcc_s-%{gcc_version}-%{DATE}.so.1
%else
mv -f %{buildroot}%{_libdir}/libgcc_s.so.1 %{buildroot}/%{_lib}/libgcc_s-%{gcc_version}-%{DATE}.so.1
%endif
chmod 755 %{buildroot}/%{_lib}/libgcc_s-%{gcc_version}-%{DATE}.so.1
ln -sf libgcc_s-%{gcc_version}-%{DATE}.so.1 %{buildroot}/%{_lib}/libgcc_s.so.1
ln -sf /%{_lib}/libgcc_s.so.1 $FULLPATH/libgcc_s.so

%ifarch sparc
ln -sf /lib64/libgcc_s.so.1 $FULLPATH/libgcc_s_64.so
%endif # sparc
%ifarch %{multilib_64_archs}
ln -sf /lib/libgcc_s.so.1 $FULLPATH/libgcc_s_32.so
%endif

pushd $FULLPATH
%ifarch ppc64
mv -f %{buildroot}%{_prefix}/lib/libstdc++.*a $FULLLPATH/
%else
mv -f %{buildroot}%{_libdir}/libstdc++.*a $FULLLPATH/
%endif
mv -f %{buildroot}%{_prefix}/lib/libsupc++.*a .

%ifarch sparc
mv -f %{buildroot}%{_libdir}/64/libsupc++.*a 64/
ln -sf lib32/libstdc++.a libstdc++.a
ln -sf ../lib64/libstdc++.a 64/libstdc++.a
%endif # sparc
%ifarch sparc64 s390x
mv -f %{buildroot}%{_libdir}/32/libsupc++.*a 32/
%endif # sparc64 || s390x
%ifarch sparc64
ln -sf ../lib32/libstdc++.a 32/libstdc++.a
ln -sf lib64/libstdc++.a libstdc++.a
%endif # sparc64
%ifarch x86_64
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libstdc++.a 32/libstdc++.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_version}/libsupc++.a 32/libsupc++.a
%endif # x86_64
%ifarch s390x
ln -sf ../../../s390-%{_vendor}-%{_target_os}/%{gcc_version}/libstdc++.a 32/libstdc++.a
%endif # s390x

popd

pushd %{buildroot}%{_bindir}
mv -f gcc gcc%{bin_suffix}
mv -f g++ g++%{bin_suffix}
%ifarch ppc ppc64
mv -f %{gcc_target_platform}-gcc %{gcc_target_platform}-gcc%{bin_suffix}
mv -f %{gcc_target_platform}-g++ %{gcc_target_platform}-g++%{bin_suffix}
%endif #ppc ppc64
popd

cd ..

%ifarch %{ix86}
pushd %{buildroot}%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}
mv specs specs~
## fix specs for ssp
sed -e 's/^%%{static|static-libgcc:-lgcc -lgcc_eh}/%%{static|static-libgcc:-lgcc -lgcc_eh %%{profile:-lc_p} %%{!profile: -lc}}/' < specs~ > specs
popd
%endif

%ifarch ppc64
mv -f %{buildroot}%{_prefix}/lib/libstdc++* %{buildroot}%{_libdir}
%endif

# remove. Please modify %%files if needed.
rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/*.la
rm -f %{buildroot}%{_prefix}/lib/libiberty.a
rm -f %{buildroot}%{_prefix}/lib/libstdc++.so
%ifarch %{multilib_64_archs}
rm -rf %{buildroot}%{_prefix}/lib/32
rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_version}/32/*.la
rm -f %{buildroot}%{_prefix}/lib/libffi*
rm -f %{buildroot}%{_prefix}/lib/libg2c.so.*
rm -f %{buildroot}%{_prefix}/lib/libgcc_s.so.*
rm -f %{buildroot}%{_prefix}/lib/libobjc.so.*
rm -f %{buildroot}%{_prefix}/lib/libstdc++.*
%endif
rm -rf %{buildroot}/{%{_lib},%{_datadir}}

%clean
rm -rf %{buildroot}

%post -n libstdc++%{pkg_suffix}
rm -f %{_libdir}/libstdc++.so || :
/sbin/ldconfig

%postun -n libstdc++%{pkg_suffix} -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_bindir}/gcc%{bin_suffix}
%ifarch sparc
%{_bindir}/%{_target_platform}-gcc%{bin_suffix}
%endif # sparc
%ifarch sparc64
%{_bindir}/sparc-%{_vendor}-%{_target_os}-gcc%{bin_suffix}
%endif # sparc64
#%dir %{_prefix}/lib/gcc-lib
%dir %{_prefix}/lib/gcc-lib/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}
%dir %{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/stddef.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/stdarg.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/varargs.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/float.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/limits.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/stdbool.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/iso646.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/syslimits.h
%ifarch %{ix86} x86_64
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/mmintrin.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/xmmintrin.h
%endif # ix86 && x86_64
%ifarch ia64
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/ia64intrin.h
%endif # ia64
%ifarch ppc
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/ppc-asm.h
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/altivec.h
%endif # ppc
%ifarch ppc64
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/ppc-asm.h
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/altivec.h
%endif
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/include/README
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/cc1
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/collect2
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/crt*.o
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libgcc.a
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libgcc_eh.a
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libgcc_s.so
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/cpp0
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/tradcpp0
%ifarch sparc
%dir %{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/64
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/64/crt*.o
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/64/libgcc.a
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/64/libgcc_eh.a
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libgcc_s_64.so
%endif # sparc
%ifarch sparc64 x86_64 s390x
%dir %{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/32
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/32/crt*.o
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/32/libgcc.a
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/32/libgcc_eh.a
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libgcc_s_32.so
%endif # sparc64 && s390x
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/specs
%doc gcc/README* gcc/*ChangeLog*

%files c++
%defattr(-,root,root)
%ifnarch i386 i586 i686 x86_64
%{_bindir}/%{gcc_target_platform}-g++%{bin_suffix}
%endif
%{_bindir}/g++%{bin_suffix}
#%dir %{_prefix}/lib/gcc-lib
#%dir %{_prefix}/lib/gcc-lib/%{gcc_target_platform}
#%dir %{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/cc1plus
%ifarch sparc
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/64/libstdc++.a
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/64/libsupc++.a
%endif # sparc
%ifarch sparc64 x86_64 s390x
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/32/libstdc++.a
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/32/libsupc++.a
%endif # sparc64 && x86_64 && s390x
%ifarch sparc sparc64 x86_64 s390x
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libsupc++.a
%endif # sparc && sparc64 && x86_64 && s390x
%ifarch sparc sparc64
%{_libdir}/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libstdc++.a
%endif # sparc && sparc64
%doc gcc/cp/ChangeLog*

%files -n libstdc++%{pkg_suffix}
%defattr(-,root,root)
%ifnarch ppc ppc64 x86_64
%{_libdir}/libg++*
%endif
%{_libdir}/libstdc++*

%files -n libstdc++%{pkg_suffix}-devel
%defattr(-,root,root)
%{_includedir}/c++/3.2.3
%ifarch sparc
%dir %{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/lib32
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/lib32/libstdc++.a
%endif # sparc
%ifarch sparc64
%dir %{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/lib64
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/lib64/libstdc++.a
%endif # sparc64
%ifnarch sparc sparc64
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libstdc++.a
%endif # sparc && sparc64
%ifnarch sparc sparc64 x86_64 s390x
%{_prefix}/lib/gcc-lib/%{gcc_target_platform}/%{gcc_version}/libsupc++.a
%endif # sparc && sparc64 && s390x
%doc libstdc++-v3/ChangeLog* libstdc++-v3/README* libstdc++-v3/docs/html/

%ifarch sparc
%files sparc32
%defattr(-,root,root)
%dir %{_libdir}/gcc-lib/%{_target_platform}/%{gcc_version}
%{_libdir}/gcc-lib/%{_target_platform}/%{gcc_version}/cc1
%{_libdir}/gcc-lib/%{_target_platform}/%{gcc_version}/include

%files c++-sparc32
%defattr(-,root,root)
%{_libdir}/gcc-lib/%{_target_platform}/%{gcc_version}/cc1plus
%endif

%changelog
* Tue Aug  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-44m)
- rebuild without libgcc.i686 on x86_64

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-43m)
- remove -Wno-unused-but-set-{variable,parameter} for gcc3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-42m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-41m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.3-40m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-39m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-38m)
- update to 3.2.3-20040701 because 27_io/ios_base_storage.cc test
  consumes huge amount of memory (32GB) on 3.2.3-37m.x86_64
- regenerate stack protector patch (Patch0)
- remove -fPIC

* Fri Oct  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-37m)
- enable to build with new optflags
-- sanitize OPT_FLAGS
- remove bootstrap_build stuff

* Wed Jan 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-36m)
- %%global momonga_release 6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-35m)
- rebuild against rpm-4.6

* Wed Apr  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-34m)
- add specopt do_test

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-33m)
- rebuild against gcc43

* Sat Sep 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-32m)
- disable debug_package

* Sun Aug 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-31m)
- revise %%files to avoid conflicting on x86_64

* Thu Jul 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-30m)
- add PreReq: coreutils to libstdc++3.2

* Wed Mar 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.3-29m)
- -mtune=generic was invalidated 

* Tue Jan 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-28m)
- use "tail -c +16" instead of "tail +16c"

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.2.3-27m)
- enable ppc64

* Mon Jul 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.3-26m)
- delete duplicated dir (libstdc++3.2-devel)

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.3-25m)
- delete duplicated dir

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-24m)
- change momonga_release to 3

* Tue May  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-23m)
- %%define with_ssp 1 (apply stack-smashing protection patch again)
- %%define bootstrap_build 1 (remove -fstack-protector from %%{optflags} for Nonfree)

* Mon May  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.3-22m)
- %%define with_ssp 0 for Nonfree

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.3-21m)
- add gcc4 patch
- Patch200: gcc32-obstack-lvalues.patch

* Thu Apr 21 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-20m)
- change momonga_release to 2.0

* Wed Feb 23 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.3-19m)
- rebuild against bash3.0(trap-0.patch)

* Sat Feb  5 2005 Toru Hoshina <t@momonga-linux.org>
- (3.2.3-18m)
- %%{_libdir}/libstdc++.so is going be deleted explicitly at %%post phase.

* Sat Feb  5 2005 Toru Hoshina <t@momonga-linux.org>
- (3.2.3-17m)
- compat-libstdc++ comtained libstdc++.so orz

* Wed Feb  2 2005 Toru Hoshina <t@momonga-linux.org>
- (3.2.3-16m)
- provides compat-libstdc++ so that gcc2.95.3 and gcc2.96 is obsoleted.

* Tue Jan 18 2005 Toru Hoshina <t@momonga-linux.org>
- (3.2.3-15m)
- %{_libdir}/gcc -> %{_prefix}/lib/gcc for multilib gcc

* Mon Jan 10 2005 mutecat <mutecat@momonga-linux.org>
- (3.2.3-14m)
- arrange ppc

* Wed Sep 08 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.2.3-13m)
- rename gcc to gcc3.2
- provide only gcc and g++

* Fri Jan 09 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-12m)
- fix alternatives about libgcj

* Thu Jan  8 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.3-11m)
- change momonga_release to 1.0

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-10m)
- s/Copyright:/License:/

* Fri Oct 10 2003 OZAWA Sakuro <crouton@momonga-linux.org>
- (3.2.3-9m)
- alternatives(jar, rmic, rmiregistry and their manpages)

* Mon Oct  6 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (3.2.3-8m)
- fix typo   s/bootstarp/bootstrap/g
- it is made not to make gcj-jar when bootstrap_build

* Thu Aug  7 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2.3-7m)
- update ssp patch to 3.2.2-10

* Thu Jul 24 2003 HOSONO Hidetomo <h12o@h12o.org>
- (3.2.3-6m)
- remove gcc-specs-sspfix.diff
- add a sed one-liner instead of patching gcc-specs-sspfix.diff

* Sat Jul 19 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2.3-5m)
- rebuild against glibc-2.3.2

* Sun Jul  6 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.3-4m)
- rebuild against rpm.

* Sat Jun 28 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.3-3m)
- mv jar gcj-jar for kaffe.

* Thu May 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-2m)
- update ssp protector patch to 3.2.2-7

* Sat Apr 26 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3
- fix rpm macros miss in files section at ifarch sparc (s|{_libdir}|%%{_libdir}|g)
- change "%%{__tar} zxvf" to "%%{__tar} zxf" %%{SOURCE1} (ssp patch)
- update DATE macro, unneed?

* Sun Apr 20 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.2-15m)
- cancel patch0 because can not build kernel.

* Sun Apr 20 2003 HOSONO Hidetomo <h12o@h12o.org>
- (3.2.2-14m)
- fix the URL of Source1:

* Fri Apr 18 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.2-13m)
- synchronize to RH9.

* Tue Apr 15 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.2-12m)
- syncronize to rawhide gcc (3.2.2-10).

* Sun Apr 13 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.2-11m)
- modify argument of install-info.

* Fri Apr 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-10m)
- rebuild against zlib 1.1.4-5m
- add BuildPrereq: zlib-devel >= 1.1.4-5m

* Sun Mar 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.2-9m)
- move Name: section top of spec file [Momonga-devel.ja:01504] 
- comment out <URL:http:\/\/www.momonga-linux.org\/> gcc/system.h
  using URL when cc1 internal compile error...
- change %%{_prefix}/%%{_lib} to %%{_libdir}

* Sun Mar 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2.2-8m)
- update ssp patch to 3.2.2-3 
- fix specs for ssp(gcc-specs-sspfix.diff)

* Fri Feb 28 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2.2-7m)
- update ssp patch

* Tue Feb 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2.2-6m)
- install hoge.info instead of hoge.info.* in %%post and %%preun
- fix error of %%post on libgcj

* Mon Feb 17 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.2-5m)
- libgcj must contain libgcjx.

* Mon Feb 17 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.2-4m)
- modify the %%install section to fix to unmatch pushd/popd pair in
  positive bootstrap_build.
- use --enable-languages option in configure.

* Mon Feb 17 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.2-3m)
- enable java-awt support.
- disable compile g77, objc and java support when bootstrap_build is
  positive.

* Tue Feb 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2.2-2m)
- fix ssp

* Sat Feb  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.2.2-1m)
  update to 3.2.2

* Mon Jan 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2-9m)
- fix %%post and %%preun of libgcj for compressed info file

* Wed Nov 27 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2-8m)
- sync with redhat(gcc-3.2-14)
- add changelog of redhat gcc-3.2-14(rawhide) below
-
-* Sun Nov 17 2002 Jakub Jelinek  <jakub@redhat.com> 3.2-14
-- really fix check-abi problem on ia64/alpha
-
-* Sat Nov 16 2002 Jakub Jelinek  <jakub@redhat.com> 3.2-13
-- update from gcc-3_2-rhl8-branch
-  - fix flow (Graham Scott, Jan Hubicka)
-  - fix check-abi problem on ia64/alpha (Benjamin Kosnik)
-  - fix objc on x86_64 (Jan Hubicka, Nicola Pero)
-  - fix x86_64 profiling (Jan Hubicka)
-  - better s390* .eh_frame encoding (Ulrich Weigand)
-- build ada on x86_64
-- add fastjar info and jar and grepjar manual pages
-
-* Tue Nov 12 2002 Jakub Jelinek  <jakub@redhat.com> 3.2-12
-- update from gcc-3_2-rhl8-branch
-  - PRs c/8467, preprocessor/4890, 8502, c/5351, optimization/7591,
-    bootstrap/8146, c/8252, optimization/7520, c/8451, c++/8391,
-    target/7856, target/7133, target/6890, middle-end/6994, opt/7944,
-    c/761, c++/7788, c++/2521, c++/8160, c++/8149, c++/8287,
-    middle-end/6994, c++/7266, c++/7228, c++/8067, c++/7679,
-    c++/6579, java/7830, libstdc++/8362, libstdc++/8258, libstdc++/7219,
-    libstdc++/8466, libstdc++/8172, libstdc++/8197, libstdc++/8318,
-    libstdc++/8348, libstdc++/7961, other/3337, bootstrap/6763,
-    bootstrap/8122, libstdc++/8347
-  - &&lab1 - &&lab2 doesn't need to be in rw section
-  - x86_64 %rip fixes
-  - fix jar c without f (#77062)
-  - backport all fastjar changes from mainline
-
-* Wed Oct 23 2002 Jakub Jelinek  <jakub@redhat.com> 3.2-11
-- update from gcc-3_2-rhl8-branch
-  - PRs target/7693, opt/7630, c++/6419, target/7396, c++/8218,
-    c++/7676, c++/7584, c++/7478, c++/8134, c++/7524, c++/7176,
-    c++/5661, c++/6803, c++/7721, c++/7803, c++/7754, c++/7188,
-    libstdc++/8071, libstdc++/8127, c++/6745, libstdc++/8096,
-    libstdc++/7811
-- fix x86-64 ICE with stdarg in -fpic (#76491)
-- fix IA-32 miscompilation of DImode code (Jim Wilson, PR target/6981)
-
-* Wed Oct 16 2002 Jakub Jelinek  <jakub@redhat.com> 3.2-10
-- update from gcc-3_2-rhl8-branch
-  - PRs target/7370, target/8232, opt/7409, preprocessor/7862,
-    preprocessor/8190, optimization/6631, target/5610, optimization/7951,
-    target/7723
-- allow building even if de_DE locale is not installed (#74503, #75889)
-- s390x multilib
-- x86-64 TLS fixes
-- 15 Java fixes (Anthony Green, Andrew Haley, Tom Tromey,
-  PRs java/6005, java/7611, java/8003, java/7950, java/5794, libgcj/7073)
-- %%define _unpackaged_files_terminate_build 0
-- fix make check-abi
-
-* Fri Oct 11 2002 Jakub Jelinek  <jakub@redhat.com> 3.2-9
-- update from gcc-3_2-rhl8-branch
-  - __attribute__((tls_model("")))
-  - PRs c/7353, opt/7124, opt/7912, opt/7390, doc/7484,
-        c/7411, target/8087, optimization/6713
-- x86-64 TLS
-
-* Tue Oct  8 2002 Jakub Jelinek  <jakub@redhat.com> 3.2-8
-- switch to gcc-3_2-rhl8-branch snapshots
-  - thus most of the patches went away as they are in CVS
-- merge from gcc-3_2-branch between 20020903 and 20021007
-  - PRs target/7434, optimization/6627, preprocessor/8120,
-       middle-end/7151, preprocessor/8055, optimization/7335,
-       c/7160, target/7842, opt/7515, optimization/7130,
-       optimization/6984, c/7150, target/7380, other/7114,
-       target/5967, optimization/7120, target/7374, opt/7814,
-       c/7102
-- backported libffi and libjava bits for x86-64 and s390*
-- added sparc* support
-- multilib setup for sparc* and x86-64
-- some IA-32 TLS fixes (Richard Henderson)

* Thu Nov 27 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2-7m)
- update protector patch protector-3.2-3 to protector-3.2-5
- add BuildPreReq: binutils >= 2.13.90.0.10-2m

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2-6m)
- add Obsoletes: gcc-chill

* Sun Nov  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2-5m)
- add bootstrap_build macro(to remove -fstack-protector from %%optflags)

* Thu Oct 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2-4m)
- enable ssp by default

* Wed Oct  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2-3m)
- %%{?include_specopt}
- add ssp patch, See <http://www.trl.ibm.com/projects/security/ssp/>
  disabled by default

* Sun Oct  6 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2-2m)
- delete ada

* Tue Oct  1 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.2-1m)
- update to 3.2
  sync with gcc-3.2 of redhat 8.0(gcc-3.2-7.src.rpm)

* Thu Aug  1 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1-1m)
  update to 3.1.1

* Tue Jul 17 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.96-9m)
- s/Kondara MNU 2.1 Linux/Momonga Linux/
- change Source0 URI

* Wed May 29 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1-8k)
  applied patch
  see http://gcc.gnu.org/cgi-bin/gnatsweb.pl?cmd=view%20audit-trail&database=gcc&pr=6703

* Sat May 25 2002 Toru Hoshina <t@kondara.org>
- (3.1-6k)
- remove disgusting make check.
- apply athlon optimize patch,
  See http://gcc.gnu.org/ml/gcc/2002-05/msg01743.html

* Fri May 24 2002 Toru Hoshina <t@kondara.org>
- (3.1-4k)
- revised spec.

* Thu May 23 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1-2k)
  merge from rawhide
  3.1 first release
