%global momorel 1
%global qtver 4.8.1
%global kdever 4.8.4
%global kdelibsrel 1m

Summary:        MIDI metronome with KDE interface
Name:           kmetronome
Version:        0.10.1
Release:        %{momorel}m%{?dist}
License:        GPLv2+
URL:            http://kmetronome.sourceforge.net/
Group:          Applications/Multimedia
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         %{name}-0.10.0-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  alsa-lib-devel
BuildRequires:  drumstick-devel
BuildRequires:  cmake
BuildRequires:  coreutils
BuildRequires:  gettext

%description
KMetronome is a MIDI metronome with KDE interface, based on the ALSA sequencer.
The intended audience is musicians and music students. Like solid, real metronomes
it is a tool to keep the rhythm while playing musical instruments. It uses MIDI for
sound generation instead of digital audio, allowing low CPU usage, and very accurate
timing thanks to the ALSA sequencer.

%prep
%setup -q
%patch0 -p1 -b .desktop

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_kde4_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_kde4_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
update-mime-database %{_datadir}/mime &> /dev/null || :

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_kde4_bindir}/%{name}
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/dbus-1/interfaces/net.sourceforge.kmetronome.xml
%{_kde4_datadir}/dbus-1/services/net.sourceforge.kmetronome.service
%{_kde4_docdir}/HTML/en/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/scalable/apps/%{name}.svgz
%{_kde4_datadir}/pixmaps/%{name}.png
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}.*

%changelog
* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.0-4m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.0-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-1m)
- initial release for Momonga Linux
