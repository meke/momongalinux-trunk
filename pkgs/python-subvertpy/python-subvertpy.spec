%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define debug_package %{nil}

Name: python-subvertpy
Summary: Alternative Python bindings for Subversion
Version: 0.8.9
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Development/Libraries
URL: http://samba.org/~jelmer/subvertpy/
# and see https://launchpad.net/subvertpy
Source0: http://samba.org/~jelmer/subvertpy/subvertpy-%{version}.tar.gz
NoSource: 0

BuildRequires: python-devel subversion-devel
Requires: subversion

%description
Alternative Python bindings for Subversion, split out from
bzr-svn. The goal is to have complete, portable and "Pythonic" Python
bindings.

%prep
%setup -q -n subvertpy-%{version}

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --root %{buildroot} --skip-build

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL NEWS TODO examples
%{_bindir}/subvertpy-fast-export
%{python_sitearch}/subvertpy/
%{python_sitearch}/subvertpy-*.egg-info

%changelog
* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.9-1m)
- update to 0.8.9 need bzr

* Tue Sep  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5-1m)
- update to 0.8.5 for subversion 1.7.x

* Tue Jun  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update to 0.8.1

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-1m)
- update to 0.8.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.8-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.8-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.8-1m)
- update to 0.6.8

* Thu Apr  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5
-- support subversion-1.6.0

* Sun Mar  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Thu Feb 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-1m)
- initial packaging
