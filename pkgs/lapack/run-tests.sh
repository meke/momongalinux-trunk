#!/bin/sh

ln -sf liblapack.a lapack.a
ln -sf libblas.a blas.a
ln -sf libblas.a librefblas.a

make lapack_testing
make blas_testing

cat<<EOF
------------ summary of lapack_testing -----

# of passes    : `grep "passed" TESTING/*.out 2>/dev/null | wc -l `
# of failures  : `grep "fail" TESTING/*.out 2>/dev/null | wc -l `

------------ summary of blas_testing -------

# of passes    : `grep "PASSED" BLAS/*.out 2>/dev/null | wc -l `
# of failures  : `grep "\*\*\*" BLAS/*.out 2>/dev/null | wc -l `

EOF

exit 0






