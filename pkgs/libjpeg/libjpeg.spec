%global momorel 1

Summary: A library for manipulating JPEG image format files
Name: libjpeg
Version: 8d
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Libraries
URL: http://www.ijg.org/
Source0: http://www.ijg.org/files/jpegsrc.v%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The libjpeg package contains a library of functions for manipulating
JPEG images, as well as simple client programs for accessing the
libjpeg functions.  Libjpeg client programs include cjpeg, djpeg,
jpegtran, rdjpgcom and wrjpgcom.  Cjpeg compresses an image file into
JPEG format.  Djpeg decompresses a JPEG file into a regular image
file.  Jpegtran can perform various useful transformations on JPEG
files.  Rdjpgcom displays any text comments included in a JPEG file.
Wrjpgcom inserts text comments into a JPEG file.

%package devel
Summary: Development tools for programs which will use the libjpeg library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The libjpeg-devel package includes the header files and static libraries
necessary for developing programs which will manipulate JPEG files using
the libjpeg library.

If you are going to develop programs which will manipulate JPEG images,
you should install libjpeg-devel.  You'll also need to have the libjpeg
package installed.
#'

%package static
Summary: Static JPEG image format file library
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
The libjpeg-static package contains the statically linkable version of libjpeg.
Linking to static libraries is discouraged for most applications, but it is
necessary for some boot packages.

%prep
%setup -q -n jpeg-%{version}

%build
%configure --enable-shared --enable-static
%make libdir=%{_libdir}

%install
rm -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_bindir}
%{__mkdir_p} %{buildroot}%{_includedir}
%{__mkdir_p} %{buildroot}%{_libdir}
%{__mkdir_p} %{buildroot}%{_mandir}/man1
%makeinstall

find %{buildroot} -name "*.la" -delete

%check
make check

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README usage.txt 
%{_bindir}/cjpeg
%{_bindir}/djpeg
%{_bindir}/jpegtran
%{_bindir}/rdjpgcom
%{_bindir}/wrjpgcom
%{_libdir}/libjpeg.so.8
%{_libdir}/libjpeg.so.8.*
%{_mandir}/man1/cjpeg.1*
%{_mandir}/man1/djpeg.1*
%{_mandir}/man1/jpegtran.1*
%{_mandir}/man1/rdjpgcom.1*
%{_mandir}/man1/wrjpgcom.1*

%files devel
%defattr(-,root,root,-)
%doc coderules.txt libjpeg.txt structure.txt wizard.txt
%{_libdir}/libjpeg.so
%{_includedir}/jconfig.h
%{_includedir}/jerror.h
%{_includedir}/jmorecfg.h
%{_includedir}/jpeglib.h

%files static
%defattr(-,root,root,-)
%{_libdir}/libjpeg.a

%changelog
* Tue Mar 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8d-1m)
- update to 8d

* Sun Apr 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8c-1m)
- update to 8c

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8b-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8b-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8b-2m)
- full rebuild for mo7 release

* Tue May 25 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8b-1m)
- update to 8b

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8a-1m)
- update to 8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7-1m)
- update version 7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6b-35m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6b-34m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6b-33m)
- %%NoSource -> NoSource

* Tue Apr  3 2007 zunda <zunda at freeshell.org>
- (6b-32m)
- Modified to delete libjpeg.la. The problem below seems to be a local one.

* Tue Apr  3 2007 zunda <zunda at freeshell.org>
- (6b-31m)
- Modified not to delete libjpeg.la that is needed to build avahi

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6b-30m)
- delete libtool library

* Fri May 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6b-29m)
- revise %%files for rpm-4.4.2

* Wed Nov 30 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6b-28m)
- fix for ppc64

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6b-26m)
- enable ia64, ppc64, alpha

* Mon Oct  3 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (6b-24m)
- revised libjpeg-6b-x86_64.patch

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6b-25m)
- fix libjpeg.la.

* Sat Jan 15 2005 Toru Hoshina <t@momonga-linux.org>
- (6b-24m)
- enable x86_64.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (6b-23m)
- pretty spec file.
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- (6b-22k)
- add alphaev5 support.

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (6b-18k)
- modified spec file and errased jpeg-6b-fhs.patch for compatibility

* Wed Aug 23 2000 Kenichi Matsubara <m@kondara.org>
- Change PATH (FHS).

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (6b-10).

* Sat Feb  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- rebuild to get compressed man pages
- fix description
- some minor tweaks to the spec file
- add docs
- fix build on alpha (alphaev6 stuff)

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 9)

* Wed Jan 13 1999 Cristian Gafton <gafton@redhat.com>
- patch to build on arm
- build for glibc 2.1

* Mon Oct 12 1998 Cristian Gafton <gafton@redhat.com>
- strip binaries

* Mon Aug  3 1998 Jeff Johnson <jbj@redhat.com>
- fix buildroot problem.

* Tue Jun 09 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Thu Jun 04 1998 Marc Ewing <marc@redhat.com>
- up to release 4
- remove patch that set (improper) soname - libjpeg now does it itself

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri May 01 1998 Cristian Gafton <gafton@redhat.com>
- fixed build on manhattan

* Wed Apr 08 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to version 6b

* Wed Oct 08 1997 Donnie Barnes <djb@redhat.com>
- new package to remove jpeg stuff from libgr and put in it's own package
