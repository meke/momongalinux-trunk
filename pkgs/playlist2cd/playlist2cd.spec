%global momorel 4
%global srcrel 95972
%global unpackedname burn%{name}
%global srcname %{unpackedname}-%{version}.amarokscript
%global amarokver 2.3.1

Summary: A script for amarok creating an k3b project from the current playlist
Name: playlist2cd
Version: 0.3
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://kde-apps.org/content/show.php/playlist+2+cd?content=95972
Source0: http://kde-apps.org/CONTENT/content-files/%{srcrel}-%{srcname}.tar.gz
# NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: amarok >= %{amarokver}
Requires: k3b
# _kde4 macros need cmake
BuildRequires: cmake
BuildRequires: coreutils

%description
Creates an k3b project from the current playlist. Tested with amarok 2.3.0 on Ubuntu 10.04. After you installed the script there are 4 new entries in the tools-menu to write the playlist or the selection to a data- or audio-disc.

%prep
%setup -q -n %{unpackedname}

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{_kde4_appsdir}/amarok/scripts/%{unpackedname}
install -m 644 main.js %{buildroot}%{_kde4_appsdir}/amarok/scripts/%{unpackedname}/
install -m 644 script.spec %{buildroot}%{_kde4_appsdir}/amarok/scripts/%{unpackedname}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_kde4_appsdir}/amarok/scripts/%{unpackedname}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-2m)
- full rebuild for mo7 release

* Sun Jun  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-1m)
- initial package for music freaks using Momonga Linux
