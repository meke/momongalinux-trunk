%global momorel 12

Summary: A RFC 1413 ident protocol daemon
Name: authd
Version: 1.4.3
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Daemons
Obsoletes: pidentd
Provides: pidentd = 3.2
Requires(post): openssl
Source0: %{name}-%{version}.tar.gz
Patch0: authd-1.4.3-gcc4.patch
Patch1: authd-1.4.3-disable.patch
Patch2: authd-1.4.3-ipv6-mapping.patch
Patch3: authd-1.4.3-locale.patch
Patch4: authd-1.4.3-longopt-identifier.patch
Patch5: authd-1.4.3-jiffies64.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel >= 1.0.0 gettext
Requires(post): shadow-utils openssl coreutils
Requires(postun): initscripts xinetd

%description
authd is a small and fast RFC 1413 ident protocol daemon
with both xinetd server and interactive modes that
supports IPv6 and IPv4 as well as the more popular features
of pidentd.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1 -b .ipv6map
%patch3 -p1 -b .locale
%patch4 -p1
%patch5 -p1

sed -i -e "s|/etc|%{_sysconfdir}|" config.h

%build
make prefix=%{_prefix}

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

install -d ${RPM_BUILD_ROOT}%{_sysconfdir}/xinetd.d

install -m 644 xinetd.conf.auth ${RPM_BUILD_ROOT}%{_sysconfdir}/xinetd.d/auth
sed -i -e 's|/usr/local|/usr|' ${RPM_BUILD_ROOT}%{_sysconfdir}/xinetd.d/auth

touch ${RPM_BUILD_ROOT}%{_sysconfdir}/ident.key

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
/usr/sbin/adduser -s /sbin/nologin -u 98 -r ident 2>/dev/null || true
/usr/bin/openssl rand -base64 -out %{_sysconfdir}/ident.key 32
echo CHANGE THE LINE ABOVE TO A PASSPHRASE >> %{_sysconfdir}/ident.key
/bin/chown ident:ident %{_sysconfdir}/ident.key
chmod o-rw %{_sysconfdir}/ident.key

%postun
service xinetd reload

%files -f authd.lang
%defattr(-,root,root,-)
%doc COPYING README.html rfc1413.txt
%verify(not md5 size mtime user group) %config %{_sysconfdir}/ident.key
%config(noreplace) %{_sysconfdir}/xinetd.d/auth
%{_sbindir}/in.authd

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.3-10m)
- full rebuild for mo7 release

* Fri Apr 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-9m)
- fix up permission, %%{_sbindir}/in.authd should be executable file

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3-8m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3-5m)
- rebuild against rpm-4.6

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3-3m)
- rebuild against gcc43

* Sun Mar  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.3-2m)
- import fedora patch
- fix glibc-2.8

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-1m)
- import from FC

* Mon Jul 24 2006 Martin Stransky <stransky@redhat.com> - 1.4.3-9
- added locale patch (#199721)

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.4.3-8.1
- rebuild

* Sun May 28 2006 Martin Stransky <stransky@redhat.com> - 1.4.3-8
- added gettext dependency (#193350)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.4.3-7.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Martin Stransky <stransky@redhat.com> - 1.4.3-7
- re-tag

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.4.3-6.devel.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Nov 8  2005 Martin Stransky <stransky@redhat.com>
- rebuilt

* Thu Jun 24 2005 Martin Stransky <stransky@redhat.com> - 1.4.3-5.devel
- add xinetd to Prereq
- fix for #150502 (authd doesn't map IPv6 to IPv4 from xinetd)

* Fri Apr  8 2005 Martin Stransky <stransky@redhat.com> - 1.4.3-4.devel
- clear last update

* Fri Apr  8 2005 Martin Stransky <stransky@redhat.com> - 1.4.3-3.devel
- delete user "ident" after uninstalation

* Thu Apr  7 2005 Martin Stransky <stransky@redhat.com> - 1.4.3-2.devel
- in.authd disabled by default (#151905)

* Mon Mar  7 2005 Martin Stransky <stransky@redhat.com> - 1.4.3-1.devel
- update to 1.4.3
- gcc4.0 patch
- add post-uninstall reconfiguration (#150460)

* Mon Feb 14 2005 Adrian Havill <havill@redhat.com>
- rebuilt

* Fri Oct 15 2004 Adrian Havill <havill@redhat.com> - 1.4.2-8
- tweak setting of uid/gid for key file so systems with no prior
  ident user/group don't generate a warning (#135837)

* Thu Oct 14 2004 Adrian Havill <havill@redhat.com> - 1.4.2-4
- slightly better error checking for insane cases
- tweak of the openssl requires dependency loop (#131291)
- as ident.key is created in %%post, tweak so verify passes (#131530)
- make the uid/gid for ident conform to the past (#135752)

* Wed Jul 28 2004 Adrian Havill <havill@redhat.com> - 1.4.1-1
- only scan for ESTABLISHED connections
- extra debug output for crypto

* Mon Jul 26 2004 Adrian Havill <havill@redhat.com> - 1.4.0-1
- revise makefile; don't over-optimize as gcc can produce bad code
- ptr cleanup when multiquery and missing /proc/net/tcp*
- improve create_opt (error handling, debugging, identifiers)
- add --prefix option for matching IPv4 to IPv6

* Tue Jul 13 2004 Adrian Havill <havill@redhat.com> - 1.3.4-1
- retry reading proc with pauses to reduce false negatives
- match IPv4 addresses against IPv6 compatibility addresses

* Mon Jul 12 2004 Adrian Havill <havill@redhat.com> - 1.3.3-1
- use gnu *_unlocked stream funcs for faster I/O

* Sat Jul 10 2004 Adrian Havill <havill@redhat.com> - 1.3.2-1
- enforce rfc restriction limiting port search to the connected
  local/foreign pair

* Fri Jul 08 2004 Adrian Havill <havill@redhat.com> - 1.3.1-1
- increase default connections-per-sec/max-instances for HP
- more doc cleanup
- remove unnecessary rootdir check for -N/--ident

* Fri Jul 02 2004 Adrian Havill <havill@redhat.com> - 1.3.0-1
- add unknown-error only -e option
- edit readme, add rfc to docdir
- code cleanup; remove static buffers, orthagonalize id names
- ipv6 hybrid addr zero run correction
- extra eight bits added to random key

* Wed Jun 30 2004 Adrian Havill <havill@redhat.com> - 1.2.8-1
- zero out invalid port(s)

* Tue Jun 29 2004 Adrian Havill <havill@redhat.com> - 1.2.7-1
- added Provides to satisfy HP pkg rpm dep (#121447, #111640)
- more code cleanup; minimize --resolve dns lookups

* Mon Jun 28 2004 Adrian Havill <havill@redhat.com> - 1.2.6-1
- incorporated suggestions from Thomas Zehetbauer (#124914)

* Sat Jun 26 2004 Adrian Havill <havill@redhat.com> - 1.2.5-1
- clean up src

* Thu Jun 24 2004 Adrian Havill <havill@redhat.com> - 1.2.4-1
- code vet and minor changes re alan@'s comments
- default operating mode to alias all usernames as 'nobody'
  to prevent noobies from getting their mail addr harvested
- clean up README documentation

* Wed Jun 23 2004 Adrian Havill <havill@redhat.com> - 1.2.3-1
- mark xinetd conf file as a noreplace config file
- more robust error checking for proper rfc1413 tokens

* Tue Jun 22 2004 Adrian Havill <havill@redhat.com> - 1.2.1-1
- add Requires and BuildRequires

* Mon Jun 21 2004 Adrian Havill <havill@redhat.com> - 1.2.0-1
- A few tweaks in the cmdline options for orthagonality
- minor bug fix regarding reading from stdin in some multiquery cmdline cases
- add --resolve

* Sun Jun 20 2004 Adrian Havill <havill@redhat.com> - 1.1.0-1
- add extra options for --help, --usage

* Sat Jun 19 2004 Adrian Havill <havill@redhat.com> - 1.0.0-2
- Obsolete pidentd -- authd and pidentd can't/shouldn't coexist on FC/RHEL
- license tweak to allow openssl under any condition
- no spec url needed; package is not worthy enough.

* Fri Jun 18 2004 Jens Petersen <petersen@redhat.com> - 1.0.0-1
- Initial packaging
