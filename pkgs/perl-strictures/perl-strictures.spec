%global         momorel 2

Name:           perl-strictures
Version:        1.005004
Release:        %{momorel}m%{?dist}
Summary:        Turn on strict and make all warnings fatal
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/strictures/
Source0:        http://www.cpan.org/authors/id/H/HA/HAARG/strictures-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
I've been writing the equivalent of this module at the top of my code for
about a year now. I figured it was time to make it shorter.

%prep
%setup -q -n strictures-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/strictures.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005004-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005004-1m)
- update to 1.005004

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005003-1m)
- update to 1.005003

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005002-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005002-1m)
- update to 1.005002

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005001-1m)
- update to 1.005001

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004004-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004004-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004004-2m)
- rebuild against perl-5.16.3

* Tue Nov 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004004-1m)
- update to 1.004004

* Sun Nov 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004003-1m)
- update to 1.004003

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004002-2m)
- rebuild against perl-5.16.2

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004002-1m)
- update to 1.004002

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004001-2m)
- rebuild against perl-5.16.1

* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004001-1m)
- update to 1.004001

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003001-1m)
- update to 1.003001
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.002002-4m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.002002-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.002002-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.002002-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
