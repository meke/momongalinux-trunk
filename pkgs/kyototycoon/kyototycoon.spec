%global momorel 1
%global cabinetrel 1m

Name:		kyototycoon
Summary:	a lightweight database server with auto expiration mechanism
Version:	0.9.56
Release:	%{momorel}m%{?dist}
License:	GPL
Group:		Development/Libraries
URL:		http://fallabs.com/kyototycoon/
Source0:	http://fallabs.com/kyototycoon/pkg/%{name}-%{version}.tar.gz 
NoSource: 	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:	kyotocabinet >= 1.2.74-%{cabinetrel}
BuildRequires:	kyotocabinet >= 1.2.74-%{cabinetrel}
BuildRequires:	sed

%description
Kyoto Tycoon is a lightweight database server with auto expiration mechanism, 
which is useful to handle cache data of various applications. Kyoto Tycoon is 
also a package of network interface to the DBM called Kyoto Cabinet. 

%prep
%setup -q

sed -i 's/-march=native//g' configure*

%build
%configure --libexecdir=%{_libdir}
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/*/*
%{_datadir}/doc/%{name}*
# Todo: split -devel package
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Aug  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.56-1m)
- update 0.9.56

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.54-1m)
- update 0.9.54

* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.52-1m)
- update 0.9.52

* Wed Nov  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.51-2m)
- remove -march=native from flags to enable build

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.51-1m)
- update 0.9.51

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.49-1m)
- update 0.9.49

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.24-2m)
- rebuild for new GCC 4.6

* Tue Jan 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.24-1m)
- update 0.9.24

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-1m)
- Initial commit Momonga Linux

