%global momorel 2

# TODO:
# - caraca driver (req: caraca, http://caraca.sf.net/)
# - irman driver (req: libirman, http://lirc.sf.net/software/snapshots/)
# - iguanaIR driver (req: http://iguanaworks.net/ir/usb/installation.shtml)
#   -> would cause license change to "GPLv2"
# - move to -devel (?): irw, *mode2, others?
#   note: xmode2 inflicts a dependency on X, and smode2 on svgalib
#   - does someone actually need xmode2/smode2 for something?
# - split utils into subpackage (keep daemons in main package)
# - don't run as root and/or create dedicated group, reduce fifo permissions?
# - Fixup /etc/lirc(m)d.conf %%ghost'ification, existence after erase etc.

%bcond_without  alsa
%bcond_without  portaudio
%bcond_without  x
%bcond_with     svgalib

Name:           lirc
Version:        0.9.0
Release:        %{momorel}m%{?dist}
Summary:        The Linux Infrared Remote Control package

Group:          System Environment/Daemons
License:        GPLv2+
URL:            http://www.lirc.org/
Source0:        http://dl.sourceforge.net/sourceforge/lirc/%{name}-%{version}.tar.bz2
NoSource:       0
Source1:        %{name}.service
Source2:        %{name}.sysconfig
Source3:	%{name}md.service
Patch0:         %{name}-%{version}-libusb-config-is-no-more.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl
BuildRequires:  libusb-devel, python-devel
BuildRequires:  libftdi-devel, libirman-devel
BuildRequires:  automake libtool
%if %{with alsa}
BuildRequires:  alsa-lib-devel
%endif
%if %{with portaudio}
BuildRequires:  portaudio-devel >= 19
%endif
%if %{with svgalib}
BuildRequires:  svgalib-devel
%endif
%if %{with x}
BuildRequires:  libXt-devel
%endif
Requires:       %{name}-libs = %{version}-%{release}
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
Requires(post): glibc
Requires(postun): glibc

%description
LIRC is a package that allows you to decode and send infra-red and
other signals of many (but not all) commonly used remote controls.
Included applications include daemons which decode the received
signals as well as user space applications which allow controlling a
computer with a remote control.

%package        libs
Summary:        LIRC libraries
Group:          System Environment/Libraries

%description    libs
LIRC is a package that allows you to decode and send infra-red and
other signals of many (but not all) commonly used remote controls.
Included applications include daemons which decode the received
signals as well as user space applications which allow controlling a
computer with a remote control.  This package includes shared libraries
that applications use to interface with LIRC.

%package        devel
Summary:        Development files for LIRC
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}

%description    devel
LIRC is a package that allows you to decode and send infra-red and
other signals of many (but not all) commonly used remote controls.
Included applications include daemons which decode the received
signals as well as user space applications which allow controlling a
computer with a remote control.  This package includes files for
developing applications that use LIRC.

%package        doc
Summary:        LIRC documentation
Group:          Documentation

%description    doc
LIRC is a package that allows you to decode and send infra-red and
other signals of many (but not all) commonly used remote controls.
Included applications include daemons which decode the received
signals as well as user space applications which allow controlling a
computer with a remote control.  This package contains LIRC
documentation.

%package        remotes
Summary:        LIRC remote definitions
Group:          System Environment/Daemons

%description    remotes
LIRC is a package that allows you to decode and send infra-red and
other signals of many (but not all) commonly used remote controls.
Included applications include daemons which decode the received
signals as well as user space applications which allow controlling a
computer with a remote control.  This package contains a collection
of remote control configuration files.

%prep
%setup -q -n %{name}-%{version}%{?pre}
%patch0 -p1

chmod 644 contrib/*
chmod +x contrib/hal

sed -i -e 's|/usr/local/etc/|/etc/|' contrib/irman2lirc

sed -i -e 's/\r//' remotes/hercules/lircd.conf.smarttv_stereo \
    remotes/adstech/lircd.conf.usbx-707

sed -i -e 's|/sbin/init.d/lircd|%{_initscriptdir}/lirc|' contrib/lircs

for f in remotes/chronos/lircd.conf.chronos \
    remotes/creative/lircd.conf.livedrive remotes/atiusb/lircd.conf.atiusb \
    NEWS ChangeLog AUTHORS contrib/lircrc ; do
    iconv -f iso-8859-1 -t utf-8 $f > $f.utf8 ; mv $f.utf8 $f
done

sed -i -e 's|"/lib /usr/lib |"/%{_lib} %{_libdir} |' configure # lib64 rpath

# *cough* I wish there was a good way to disable alsa/portaudio/svgalib...
%if ! %{with alsa}
sed -i -e 's/asoundlib.h/ALSA_DISABLED/g' configure*
%endif
%if ! %{with portaudio}
sed -i -e 's/portaudio.h/PORTAUDIO_DISABLED/g' configure*
%endif
%if ! %{with svgalib}
sed -i -e 's/vga.h/SVGALIB_DISABLED/g' configure*
%endif

touch -r aclocal.m4 configure.ac # avoid autofoo re-run

# Re-run autofoo for new cvs features
#autoreconf -i -f
#automake
chmod +x daemons/input_map.sh

%build
%configure \
  --disable-static \
  --disable-dependency-tracking \
  --enable-sandboxed \
%if ! %{with x}
  --without-x \
%endif
  --with-syslog=LOG_DAEMON \
  --with-driver=userspace
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT __docs

make install DESTDIR=$RPM_BUILD_ROOT
install -pm 755 contrib/irman2lirc $RPM_BUILD_ROOT%{_bindir}
%if ! %{with svgalib}
rm -f $RPM_BUILD_ROOT%{_mandir}/man1/smode2.1*
%endif
%if ! %{with x}
rm -f $RPM_BUILD_ROOT%{_mandir}/man1/irxevent.1*
rm -f $RPM_BUILD_ROOT%{_mandir}/man1/xmode2.1*
%endif

install -Dpm 644 doc/lirc.hwdb $RPM_BUILD_ROOT%{_datadir}/lirc/lirc.hwdb

install -Dpm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/lirc.service
install -Dpm 644 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}/lircmd.service
install -Dpm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/lirc

mkdir __docs
cp -pR doc contrib __docs
cd __docs
rm -rf doc/Makefile* doc/.libs doc/man* doc/lirc.hwdb
rm -rf contrib/irman2lirc contrib/lirc.* contrib/sendxevent.c
cd ..

touch $RPM_BUILD_ROOT%{_sysconfdir}/lirc{d,md}.conf

install -dm 755 $RPM_BUILD_ROOT/dev
touch $RPM_BUILD_ROOT/dev/lirc{d,m}

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/tmpfiles.d
echo "d	%{_localstatedir}/run/lirc	0755	root	root	10d" > $RPM_BUILD_ROOT%{_sysconfdir}/tmpfiles.d/lirc.conf

rm $RPM_BUILD_ROOT%{_libdir}/liblirc_client.la

# Put remote definitions in place
cp -ar remotes $RPM_BUILD_ROOT%{_datadir}/lirc-remotes

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi
# If we're upgrading, move config files into their new location, if need be
if [ $1 -ge 2 ] ; then
  if [ -e %{_sysconfdir}/lircd.conf -a ! -e %{_sysconfdir}/lirc/lircd.conf ]; then
    mv %{_sysconfdir}/lircd.conf %{_sysconfdir}/lirc/lircd.conf
  fi
  if [ -e %{_sysconfdir}/lircmd.conf -a ! -e %{_sysconfdir}/lirc/lircmd.conf ]; then
    mv %{_sysconfdir}/lircmd.conf %{_sysconfdir}/lirc/lircmd.conf
  fi
fi

%post libs -p /sbin/ldconfig

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable lirc.service > /dev/null 2>&1 || :
    /bin/systemctl stop lirc.service > /dev/null 2>&1 || :
    /bin/systemctl --no-reload disable lircmd.service > /dev/null 2>&1 || :
    /bin/systemctl stop lircmd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart lirc.service >/dev/null 2>&1 || :
    /bin/systemctl try-restart lircmd.service >/dev/null 2>&1 || :
fi

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ANNOUNCE AUTHORS ChangeLog COPYING NEWS README TODO
%ghost %config(noreplace) %{_sysconfdir}/lirc*d.conf
%config(noreplace) %{_sysconfdir}/sysconfig/lirc
%config(noreplace) %{_sysconfdir}/tmpfiles.d/lirc.conf
%{_unitdir}/lirc*
%{_bindir}/*ir*
%{_bindir}/*mode2
%{_sbindir}/lirc*d
%{_datadir}/lirc/
%{_mandir}/man1/*ir*.1*
%{_mandir}/man1/*mode2*.1*
%{_mandir}/man8/lirc*d.8*
%ghost /dev/lirc*

%files libs
%defattr(-,root,root,-)
%{_libdir}/liblirc_client.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/lirc/
%{_libdir}/liblirc_client.so

%files doc
%defattr(-,root,root,-)
%doc __docs/*

%files remotes
%defattr(-,root,root,-)
%dir %{_datadir}/lirc-remotes
%{_datadir}/lirc-remotes/*

%changelog
* Fri Sep 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-2m)
- import patch from Fedora
- support systemd

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.5-1m)
- sync with Fedora 11 (0.8.5-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3-3m)
- rebuild against rpm-4.6

* Mon Jul 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-2m)
- use %%{_initscriptdir} instead of %%{_initrddir}

* Mon Jul 28 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.3-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-3m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-2m)
- delete libtool library

* Mon Jul 31 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.0-1m)
- version up 0.8.0

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0.2-2m)
- revise spec file for rpm-4.4.2

* Sat Dec 24 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.0.2-1m)
- update to 0.8.0pre2

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.8.0.1-1m)
- update to 0.8.0pre1
- modify patch0

* Tue Jun 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.0.4-2m)
- revise PreReq

* Wed May 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.7.0.4-1m)
- import from paken.

* Tue Feb  3 2004 Takeru Komoriya <komoriya@paken.org>
- Repackaged based on lirc-0.7.0-pre2_16.rhfc1.at
- not buildung kernel modules any more (for linux2.6)
- deleted requires: help2man, portaudio

* Mon Sep 22 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Remove dependency of lirc to %kmodnamepure. Reported by
  Ludo Stellingwerff <ludo@protactive.nl>

* Tue Sep  9 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.7.0pre2.

* Mon Jun 16 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Updated to 0.7.0 cvs 20030616.
- Adapted for kernel module building.

* Mon Mar 31 2003 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Rebuilt for Red Hat Linux 9... this spec file needs some reworking!

* Mon Oct  7 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.6.6 final.

* Mon Sep 16 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Updated to latest pre-version.
- Kernel modules still need to be compiled separately and with a custom
  kernel :-(

* Thu May  2 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.6.5.
- Rebuilt against Red Hat Linux 7.3.
- Added the %%{?_smp_mflags} expansion.

* Thu Oct  4 2001 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Initial RPM release.

