%global momorel 1	
%global debug_package %{nil}

Name:           gsettings-desktop-schemas
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        A collection of GSettings schemas

Group:          System Environment/Libraries
License:        LGPLv2+
# no homepage exists for this component
URL:            http://bugzilla.gnome.org/enter_bug.cgi?product=gsettings-desktop-schemas
#VCS: git:git://git.gnome.org/gsettings-desktop-schemas
Source:         http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires: glib2-devel >= 2.31.0
BuildRequires: intltool
BuildRequires: gobject-introspection-devel

Requires: glib2 >= 2.31.0

%description
gsettings-desktop-schemas contains a collection of GSettings schemas for
settings shared by various components of a desktop.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries
and header files for developing applications that use %{name}.


%prep
%setup -q

%build
%configure --disable-schemas-compile --enable-introspection=yes
make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

%find_lang %{name} --with-gnome

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%postun
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi


%files -f %{name}.lang
%doc AUTHORS COPYING MAINTAINERS NEWS README
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/GConf/gsettings/gsettings-desktop-schemas.convert
%{_datadir}/GConf/gsettings/wm-schemas.convert
%{_libdir}/girepository-1.0/GDesktopEnums-3.0.typelib

%files devel
%doc HACKING
%{_includedir}/*
%{_datadir}/pkgconfig/*
%{_datadir}/gir-1.0/GDesktopEnums-3.0.gir


%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Wed Aug 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Jul 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Sat Mar 17 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.0-2m)
- update default picture patch

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- change default background image

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.0-1m)
- initial build

