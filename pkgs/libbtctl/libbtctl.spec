%global momorel 9

%ifarch %ix86 x86_64 ppc ia64 armv4l sparc s390 s390x
# defined to zero, because beehive doesn't do macros for BuildRequires
%define with_mono 0
%else
%define with_mono 0
%endif
ExcludeArch: s390 s390x
Summary: Library for the GNOME Bluetooth Subsystem
Name: libbtctl
Version: 0.11.1
Release: %{momorel}m%{?dist}
License: GPL+
Group: System Environment/Libraries
URL: http://live.gnome.org/GnomeBluetooth 
Source0: http://ftp.gnome.org/pub/gnome/sources/libbtctl/0.11/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf >= 2.57, bluez-libs-devel >= 4.0 , glib2-devel, gtk-doc
BuildRequires: openobex-devel >= 1.4, pygtk2-devel, gtk2-devel
BuildRequires: python, python-devel, gettext, automake, libtool, intltool

%if %{with_mono}
%{expand BuildRequires: mono-devel gtk-sharp-gapi gtk-sharp}
%endif

Patch2: libbtctl-0.4.1-pydir.patch
Patch3: libbtctl-libtool2.patch
Patch4: libbtctl-0.11.1-gtk-doc-1.11.patch

%description
Library for the GNOME Bluetooth Subsystem

%package devel
Summary: Development files for libbtctl
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel

%description devel
This package contains the files needed for developing applications,
which use libbtctl.

%package doc
Summary: Documentation files for libbtctl
Group: Documentation

%description doc
%{summary}

%prep
%setup -q
%patch2 -p1 -b .pydir
%patch3 -p1 -b .libtool2
%patch4 -p1 -b .gtk-doc
mkdir m4
gtkdocize --copy
libtoolize
aclocal
automake -a
intltoolize -c -f
autoconf

%build
%configure --enable-gtk-doc \
%if %{with_mono}
	;
%else
	--disable-mono
%endif

# %%{?_smp_mflags} does not work
make 

%install
rm -rf --preserve-root $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

# we do not want .la files
rm -f $RPM_BUILD_ROOT/%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT/%{_libdir}/python?.?/site-packages/*.a
rm -f $RPM_BUILD_ROOT/%{_libdir}/python?.?/site-packages/*.la

%find_lang %name


%clean
rm -rf --preserve-root $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS ChangeLog NEWS README 
%{_libdir}/lib*.so.*
%{_libdir}/python?.?/site-packages/*btctl*
%if %{with_mono}
%{_libdir}/mono/gac/btctl/*/btctl.dll
%{_libdir}/mono/libbtctl/btctl.dll
%{_datadir}/gapi/btctl-api.xml
%endif

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/pkgconfig/libbtctl.pc
%{_includedir}/libbtctl
%if %{with_mono}
%{_libdir}/pkgconfig/libbtctl-sharp.pc
%endif

%files doc
%defattr(-, root, root)
%{_datadir}/gtk-doc/html/libbtctl

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.1-9m)
- rebuild for glib 2.33.2

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.1-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.1-5m)
- full rebuild for mo7 release

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.1-4m)
- --enable-gtk-doc

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.1-3m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.1-1m)
- sync with Fedora 11 (0.11.1-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.0-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.10.0-2m)
- rebuild against python-2.6.1-2m

* Fri Dec 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.0-1m)
- update 0.10.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-2m)
- rebuild against gcc43

* Sun Sep 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-1m%{?dist})
- import from fc7 to Momonga

* Wed Feb 28 2007 Harald Hoyer <harald@redhat.com> - 0.8.2-3%{?dist}
- specfile review
- portet crash patch to 0.8.2

* Thu Dec  7 2006 Jeremy Katz <katzj@redhat.com> - 0.8.2-2%{?dist}
- rebuild for python 2.5

* Mon Nov 13 2006 Harald Hoyer <harald@redhat.com> - 0.8.2-1%{?dist}
- version 0.8.2
- Resolves: rhbz#215230

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.6.0-9.1
- rebuild

* Wed Jun 14 2006 Harald Hoyer <harald@redhat.com> - 0.6.0-9
- rebuilt for openobex

* Sun Jun 11 2006 Jesse Keating <jkeating@redhat.com> - 0.6.0-8
- Bump for new libbluetooth

* Wed Jun  7 2006 Harald Hoyer <harald@redhat.com> - 0.6.0-7
- printf format corrected
- more build requires

* Wed May 17 2006 Harald Hoyer <harald@redhat.com> 0.6.0-6
- added more build requirements (bug #191981)

* Thu Feb 23 2006 Matthias Clasen <mclasen@redhat.com> 0.6.0-5
- try a fix for (#179413)

* Thu Feb 23 2006 Harald Hoyer <harald@redhat.com> 0.6.0-2
- rebuild

* Thu Feb 16 2006 Harald Hoyer <harald@redhat.com> 0.6.0-1
- version 0.6.0

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.5.0-1.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.5.0-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Sep 28 2005 Harald Hoyer <harald@redhat.com> 0.5.0-1
- new version 0.5.0

* Tue Jul 19 2005 Harald Hoyer <harald@redhat.com> 0.4.1-8
- added gtk2-devel to BuildRequires

* Thu Apr 21 2005 Harald Hoyer <harald@redhat.com> 0.4.1-7
- rebuild with requires python-abi

* Wed Mar 30 2005 Warren Togami <wtogami@redhat.com> 0.4.1-6
- devel req glib2-devel for pkgconfig (#152497)

* Wed Mar 02 2005 Harald Hoyer <harald@redhat.com> 
- rebuilt

* Wed Feb 09 2005 Harald Hoyer <harald@redhat.com>
- rebuilt

* Fri Oct 08 2004 Harald Hoyer <harald@redhat.de> 0.4.1-3
- buildrequires pygtk2-devel

* Fri Sep 24 2004 Harald Hoyer <harald@redhat.de> 0.4.1-2
- split out devel package
- require openobex >= 1.0.1
- patch for lib64 python path

* Thu Jul 22 2004 Harald Hoyer <harald@redhat.de> 0.4.1-1
- version 0.4.1

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue May 25 2004 Harald Hoyer <harald@redhat.de> 0.3-6
- buildrequire bluez-libs-devel and glib2-devel

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Aug  5 2003 Harald Hoyer <harald@redhat.de> 0.3-4
- make shared library

* Tue Aug  5 2003 Harald Hoyer <harald@redhat.de> 0.3-3
- BuildRequires bluez-sdp

* Tue Jun 24 2003 Harald Hoyer <harald@redhat.de> 0.3-2
- exclude arch s390

* Wed Jun  5 2003 Harald Hoyer <harald@redhat.de> 0.3-1
- initial RPM


