%global momorel 7

Summary: A set of audio plugins for LADSPA by Fons Adriaensen.
Name: ladspa-vco-plugins
Version: 0.3.0
Release: %{momorel}m%{?dist}
License: GPL
URL: http://kokkinizita.linuxaudio.org/linuxaudio/
Group: Applications/Multimedia
Source0:  http://kokkinizita.linuxaudio.org/linuxaudio/downloads/VCO-plugins-%{version}.tar.bz2
NoSource: 0
Requires: ladspa
BuildRequires: ladspa-devel

%description
Pulse-VCO: Anti-aliased dirac pulse oscillator (flat amplitude spectrum) 
Saw-VCO:   Anti-aliased sawtooth oscillator (1/F amplitude spectrum)

Both oscillators are based on the same principle of using a precomputed interpolated dirac pulse. For the sawtooth version, the 'edge' is made by integrating the anti-aliased pulse. Aliases should be below -80dB for fundamental frequencies below Fsamp / 6 (i.e. up to 8 kHz at Fsamp = 48 kHz). This frequency range includes the fundamental frequencies all known musical instruments.

%prep
%setup -q -n VCO-plugins-%{version}

%build
%make

%install
install -d %{buildroot}%{_libdir}/ladspa
install -m 755 *.so   %{buildroot}%{_libdir}/ladspa

%files
%defattr(-,root,root)
%doc AUTHORS COPYING INSTALL README
%{_libdir}/ladspa/*.so

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Sun May 29 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.0-7m)
- change URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.3.0-1m)
- Initial Build for Momonga Linux

