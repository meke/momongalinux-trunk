%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(0)")}
%{!?python_version: %global python_version %(%{__python} -c "from distutils.sysconfig import get_python_version; print get_python_version()")}

Summary: Samba server configuration tool
Name: system-config-samba
Version: 1.2.93
Release: %{momorel}m%{?dist}
URL: http://fedorahosted.org/system-config-samba
License: GPLv2+
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Source0: %{name}-%{version}.tar.bz2
BuildRequires: python
BuildRequires: python-devel >= 2.7
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: intltool
Obsoletes: redhat-config-samba
Requires: dbus-python
Requires: PolicyKit-authentication-agent
Requires: pygtk2
Requires: pygtk2-libglade
Requires: python
Requires: python-slip-dbus >= 0.1.7
Requires: samba
Requires: samba-common
Requires: system-config-samba-docs
Requires(post): hicolor-icon-theme
Requires(postun): hicolor-icon-theme

%description
system-config-samba is a graphical user interface for creating, 
modifying, and deleting samba shares.

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

desktop-file-install --vendor system --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
  %{buildroot}%{_datadir}/applications/system-config-samba.desktop

%find_lang %name

%clean
rm -rf --preserve-root %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/system-config-samba
%{_datadir}/system-config-samba
%{_datadir}/applications/system-config-samba.desktop
%{_datadir}/icons/hicolor/*/apps/system-config-samba.png
%{_sysconfdir}/dbus-1/system.d/*.conf
%{_datadir}/dbus-1/system-services/*.service
%{_datadir}/PolicyKit/policy/*.policy
%{_datadir}/polkit-1/actions/org.fedoraproject.config.samba.policy
%{python_sitelib}/scsamba
%{python_sitelib}/scsamba-%{version}-py%{python_version}.egg-info
%{python_sitelib}/scsamba.dbus-%{version}-py%{python_version}.egg-info

%changelog
* Thu Mar 22 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (1.2.93-1m)
- update 1.2.93

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.92-1m)
- update 1.2.92

* Thu May 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.91-1m)
- update 1.2.91

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.89-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.89-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.89-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.89-2m)
- full rebuild for mo7 release

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.89-1m)
- update 1.2.89

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.75-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.75-1m)
- sync with Fedora 11 (1.2.75-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.63-2m)
- rebuild against rpm-4.6

* Sun Jun 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.63-1m)
- update to 1.2.63

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.41-2m)
- rebuild against gcc43

* Sun Jun  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.41-1m)
- import from Fedra

* Tue Apr 17 2007 Nils Philippsen <nphilipp@redhat.com> - 1.2.41
- cope with separate nmb service (#234687)

* Tue Apr 17 2007 Nils Philippsen <nphilipp@redhat.com> - 1.2.40
- don't access smbpasswd directly but use pdbedit to allow for tdb/ldap
  backends (#236557)

* Thu Mar 22 2007 Nils Philippsen <nphilipp@redhat.com>
- update URL

* Tue Mar 20 2007 Nils Philippsen <nphilipp@redhat.com>
- mention that we are upstream
- clean buildroot before installing
- fix licensing blurb in PO files

* Mon Jan 15 2007 Nils Philippsen <nphilipp@redhat.com> - 1.2.39
- handle synonyms and inverted synonyms gracefully (#222595)

* Wed Jan 10 2007 Nils Philippsen <nphilipp@redhat.com> - 1.2.38
- get list of configuration parameters and defaults from testparm, fix
  duplicate dict keys (#219308)

* Tue Dec 12 2006 Nils Philippsen <nphilipp@redhat.com> - 1.2.37
- pick up updated translations (#216611)

* Fri Nov 24 2006 Nils Philippsen <nphilipp@redhat.com> - 1.2.36
- pick up updated translations (#216611)
- add dist tag

* Wed Mar 29 2006 Nils Philippsen <nphilipp@redhat.com> - 1.2.35
- don't require gnome module (#187200)
- don't wrap text in About dialog

* Fri Mar 03 2006 Nils Philippsen <nphilipp@redhat.com> - 1.2.34
- require hicolor-icon-theme (#182874, #182875)

* Mon Feb 06 2006 Nils Philippsen <nphilipp@redhat.com> - 1.2.33
- fix typo in PAM file (#179937)

* Thu Feb 02 2006 Nils Philippsen <nphilipp@redhat.com> - 1.2.32
- bump version

* Thu Dec 22 2005 Nils Philippsen <nphilipp@redhat.com>
- add sr@Latn.po to enable Serbian Latin translation

* Fri Oct 14 2005 Nils Philippsen <nphilipp@redhat.com>
- don't use pam_stack (#170642)

* Tue Jun 14 2005 Nils Philippsen <nphilipp@redhat.com>
- fix format string bug that broke lines with trailing comments in smb.conf
  (#160166)

* Fri May 06 2005 Nils Philippsen <nphilipp@redhat.com>
- make desktop file rebuild consistently

* Fri May 06 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.31
- use DESTDIR consistently

* Fri May 06 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.30
- make desktop file translatable (#156798)

* Wed Apr 27 2005 Jeremy Katz <katzj@redhat.com> - 1.2.29-2
- silence %%post

* Fri Apr 01 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.29-1
- fix deprecation warnings (#153051) with patch by Colin Charles
- update the GTK+ theme icon cache on (un)install (Christopher Aillon)

* Tue Mar 15 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.28-1
- fix dialog when share name is missing (#135119) again

* Wed Mar 09 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.27-1
- let users configure whether a share is browsable ("visible") or not ("hidden")

* Wed Feb 16 2005 Nils Philippsen <nphilipp@redhat.com>
- use UIManager when possible to avoid deprecation warnings (#134367, #143678)

* Wed Jan 12 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.26-1
- ignore case of share name when deleting share (#144504)
- when double clicking share, open properties dialog

* Tue Jan 11 2005 Nils Philippsen <nphilipp@redhat.com>
- assume default is "security == user" to avoid traceback on users dialog
  (#144511)
- update main window when changing share path (#144168)
- include Ukrainian translation in desktop file (#143659)

* Thu Jan 06 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.25-1
- use correct option menu for encrypt password
- use lower case for security value
- mask default values with a ";" instead of removing them altogether

* Sun Jan 02 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.24-1
- revamp BasicPreferencesWin.readFile() (#143951)
- remove stray debugging statement
- pick up new strings to be translated

* Sat Jan 01 2005 Nils Philippsen <nphilipp@redhat.com> - 1.2.23-1
- totally revamp how parsed smb.conf is handled internally (class
  SambaSection), among other things to not screw up smb.conf when editing share
  names (#143291)
- don't assume all users selected == "guest ok"
- make About/Copyright easily extensible without screwing up translations
- admit complicity
- remove tab indentation
- some more code consolidation
- pick up updated translations

* Tue Nov 23 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.22-1
- add missing options (#137756)

* Tue Oct 19 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.21-1
- don't raise exception when writing /etc/samba/smb.conf (#135946)
- updated translations

* Sun Oct 10 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.20-1
- use template file if smb.conf is missing (#131323)
- when saving smb.conf, write to new file and rename (#131323)
- set safe umask (#131323)

* Sat Oct 09 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.19-1
- fix dialog when share name is missing (#135119)

* Tue Oct 05 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.18-1
- use gtk.main() and gtk.main_quit() if available to avoid DeprecationWarnings
  (#134367)

* Mon Oct 04 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.17-1
- updated translations

* Thu Sep 23 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.16-1
- fix up translation of about window text (#133234)

* Wed Sep 15 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.15-1
- write smbpasswd file when adding user (#132084)

* Sun Aug 15 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.14-1
- make share name configurable (#110804, use patch from Philip Van Hoof, fix
  it up a bit)
- do some more code consolidation

* Tue Jul 20 2004 Brent Fox <bfox@redhat.com> - 1.2.13-1
- add 'cups option' entry (bug #128245)

* Wed Jun 23 2004 Brent Fox <bfox@redhat.com> - 1.2.12-1
- use popen instead of system (bug #112528)

* Tue Jun 22 2004 Brent Fox <bfox@redhat.com> - 1.2.11-1
- fix security and guest account defaults (bug #121745)

* Mon Jun 21 2004 Brent Fox <bfox@redhat.com> - 1.2.10-1
- write workgroup name explicitly (bug #126435)

* Mon Apr 12 2004 Brent Fox <bfox@redhat.com> 1.2.9-2
- fix icon path (bug #120181)

* Wed Mar 24 2004 Brent Fox <bfox@redhat.com> 1.2.9-1
- show all users (bug #118569)

* Tue Mar 23 2004 Brent Fox <bfox@redhat.com> 1.2.8-1
- add widgets for kerberos realm (bug #113094)

* Thu Mar 11 2004 Brent Fox <bfox@redhat.com> 1.2.7-1
- don't crash on ldap lines (bug #117812)

* Thu Mar 11 2004 Brent Fox <bfox@redhat.com> 1.2.6-1
- skip over blank lines in smbusers (bug #118017)

* Mon Mar  8 2004 Brent Fox <bfox@redhat.com> 1.2.5-1
- when using user level access, force the user to choose at least one user (bug #116566)

* Fri Mar  5 2004 Brent Fox <bfox@redhat.com> 1.2.4-1
- skip comments in smbpasswd file (bug #117604)

* Thu Mar  4 2004 Brent Fox <bfox@redhat.com> 1.2.3-1
- apply patch from bug #116564

* Mon Jan 12 2004 Brent Fox <bfox@redhat.com> 1.2.2-1
- fix glade file path problem
- wrap smbpasswd in quotes (bug #112528)
- don't call lower() on server string (bug #111758)

* Wed Jan  7 2004 Brent Fox <bfox@redhat.com> 1.2.1-1
- add a Requires for pygtk2-libglade

* Fri Nov 14 2003 Brent Fox <bfox@redhat.com> 1.2.0-1
- rename from redhat-config-samba to system-config-samba
- add Obsoletes for redhat-config-samba
- update for Python2.3

* Wed Oct 15 2003 Brent Fox <bfox@redhat.com> 1.1.4-1
- clarify error message (bug #105045)

* Wed Oct 15 2003 Brent Fox <bfox@redhat.com> 1.1.3-1
- don't allow whitespace in section headers (bug #106880)

* Mon Oct  6 2003 Brent Fox <bfox@redhat.com> 1.1.2-1
- add 'client plaintext auth' to sambaDefaults.py (bug #106072)

* Tue Sep 23 2003 Brent Fox <bfox@redhat.com> 1.1.1-1
- rebuild with the latest docs

* Wed Sep 10 2003 Brent Fox <bfox@redhat.com> 1.0.15-2
- bump relnum and rebuild

* Wed Sep 10 2003 Brent Fox <bfox@redhat.com> 1.0.15-1
- check for empty description before slicing (bug #103604)

* Wed Sep 10 2003 Brent Fox <bfox@redhat.com> 1.0.14-1
- rebuild for latest docs

* Tue Sep  9 2003 Brent Fox <bfox@redhat.com> 1.0.13-4
- bump relnum and rebuld

* Fri Sep  5 2003 Brent Fox <bfox@redhat.com> 1.0.13-3
- bump relnum and rebuild

* Fri Sep  5 2003 Brent Fox <bfox@redhat.com> 1.0.13-2
- bump relnum and rebuild

* Fri Sep  5 2003 Brent Fox <bfox@redhat.com> 1.0.13-1
- apply patch from Nalin to handle Samba3 changes
- fix selection problem with security option menu for ADS

* Thu Sep  4 2003 Brent Fox <bfox@redhat.com> 1.0.12-2
- bump relnum and rebuild

* Thu Sep  4 2003 Brent Fox <bfox@redhat.com> 1.0.12-1
- sambaDefaults.py: encrypt password default is "Yes" (bug #103752)
- basicPreferencesWin.py: Make encrypt passwd menu default to "Yes" (bug #103752)

* Thu Aug 28 2003 Brent Fox <bfox@bfox.devel.redhat.com> 1.0.11-2
- bump relnum and rebuild

* Thu Aug 28 2003 Brent Fox <bfox@bfox.devel.redhat.com> 1.0.11-1
- hook up help toolbar button

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.0.10-1
- tag on every build

* Thu Jul  3 2003 Brent Fox <bfox@redhat.com> 1.0.8-2
- bump relnum and rebuild

* Thu Jul  3 2003 Brent Fox <bfox@redhat.com> 1.0.8-1
- don't let comment field end in backslash or whitespace (bug #92165)

* Tue Jul  1 2003 Brent Fox <bfox@redhat.com> 1.0.7-2
- bump and rebuild

* Tue Jul  1 2003 Brent Fox <bfox@redhat.com> 1.0.7-1
- the word 'server' is redundant (bug #98026)

* Fri Jun 27 2003 Brent Fox <bfox@redhat.com> 1.0.6-2
- bump version num and rebuild

* Fri Jun 27 2003 Brent Fox <bfox@redhat.com> 1.0.6-1
- add a synonym for 'browsable' (bug #97357)

* Mon May 19 2003 Brent Fox <bfox@redhat.com> 1.0.5-2
- add the 'share modes' flag to sambaDefaults.py

* Mon May 19 2003 Brent Fox <bfox@redhat.com> 1.0.5-1
- rebuild with the latest translations

* Wed May  7 2003 Brent Fox <bfox@redhat.com> 1.0.4-4
- we were omitting 'pid directory' in sambaDefaults.py

* Fri Mar 21 2003 Brent Fox <bfox@redhat.com> 1.0.4-3
- make 'only guest' a synonym (bug #84272)

* Tue Mar 18 2003 Brent Fox <bfox@redhat.com> 1.0.4-2
- remove some french translations that were breaking the menu system

* Mon Feb 17 2003 Brent Fox <bfox@redhat.com> 1.0.4-1
- handle problems with valid/invalid users (bug #84271)
- handle 'public' as a synonym of 'guest ok' (bug #84272)

* Thu Feb  6 2003 Brent Fox <bfox@redhat.com> 1.0.3-1
- return if value == None (bug #83660)

* Tue Feb  4 2003 Brent Fox <bfox@redhat.com> 1.0.2-2
- print an error if we can't import gtk

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.0.2-1
- bump and build

* Wed Jan 22 2003 Tammy Fox <tfox@redhat.com> 1.0.1-7
- added docs (#80762)

* Thu Jan 16 2003 Brent Fox <bfox@redhat.com> 1.0.1-6
- add 'authentication server' (bug #82049)
- remove 'update encrypted'
- force encrypted passwords with domain security (bug #82050)

* Tue Jan 14 2003 Brent Fox <bfox@redhat.com> 1.0.1-5
- enable username map when using 'user' security
- add accelerators to notebook tabs

* Mon Jan 13 2003 Brent Fox <bfox@redhat.com> 1.0.1-4
- fix up the windows username problems

* Fri Jan 10 2003 Brent Fox <bfox@redhat.com> 1.0.1-3
- replace guest user GtkEntry widget with an OptionMenu (bug #81414)

* Fri Jan 10 2003 Tammy Fox <tfox@redhat.com> 1.0.1-2
- forgot to import string in addUserWin.py

* Thu Jan 9 2003 Tammy Fox <tfox@redhat.com> 1.0.1-1
- make popup windows transient (bug #81402)
- require a workgroup name
- strip strings before comparing to empty

* Wed Jan 8 2003 Tammy Fox <tfox@redhat.com>
- remove netbios
- GUI spacing tweaks
- make strings more user-friendly
- bug fixes

* Tue Jan  7 2003 Brent Fox <bfox@redhat.com> 1.0.0-5
- handle window destroys correctly (bug #81101)
- manually destroy about box

* Sat Jan  4 2003 Brent Fox <bfox@redhat.com> 1.0.0-4
- convert basicPreferences from combos to menu (bug #81035)

* Mon Dec 16 2002 Brent Fox <bfox@redhat.com> 1.0.0-3
- Don't allow a user to be added more than once.
- Fix strings for bug #79382

* Tue Dec 10 2002 Brent Fox <bfox@redhat.com> 1.0.0-1
- Clean up add directory dialog
- handle missing smbpasswd and smbusers files better

* Tue Nov 12 2002 Brent Fox <bfox@redhat.com> 0.9.9-3
- Pull in latest translations

* Wed Aug 14 2002 Tammy Fox <tfox@redhat.com>
- new icon

* Tue Aug 06 2002 Brent Fox <bfox@redhat.com> 0.9.9-1
- make sharing the root dir possible
- set defaults in UI for basic preferences (bug 70805)

* Fri Aug 02 2002 Brent Fox <bfox@redhat.com> 0.9.8-1
- Use new pam timestamp rules

* Thu Aug 01 2002 Tammy Fox <tfox@redhat.com>
- use stock icons for menu items
- add about box
- center popup windows
- set modal to true for popup windows

* Thu Aug 01 2002 Brent Fox <bfox@redhat.com> 0.9.7-1
- replace line for the console.apps  in spec file

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-2
- update spec file for public beta 2

* Tue Jul 16 2002 Brent Fox <bfox@redhat.com> 0.9.4-3
- Fixed bug #68740

* Tue Jul 16 2002 Brent Fox <bfox@redhat.com> 0.9.4-2
- Fixed a few bugs with adding users
- Removed placeholder widgets

* Wed Jun 26 2002 Brent Fox <bfox@redhat.com> 0.9.1-1
- Fixed description

* Thu May 30 2002 Brent Fox <bfox@redhat.com> 0.1.0-3
- Fixed Requires 

* Tue Apr 30 2002 Brent Fox <bfox@redhat.com>
- Added lots of code.  App has basic functionality at this point.

* Mon Mar 10 2002 Brent Fox <bfox@redhat.com>
- initial coding and packaging
