%global momorel 1

Summary: Class Library for Numbers
Name: cln
Version: 1.3.2
Release: %{momorel}m%{?dist}
License: GPL
URL: http://www.ginac.de/CLN/
Group: System Environment/Libraries
Source0: http://www.ginac.de/CLN/cln-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gmp-devel >= 5.0

%description
A GPLed collection of math classes and functions, that will bring
efficiency, type safety, algebraic syntax to everyone in a memory
and speed efficient library.

%package devel
Summary: Development files for programs using the CLN library
Group: Development/Libraries
Requires: %{name} = %{version}
Requires(post): info
Requires(preun): info

%description devel
This package is necessary if you wish to develop software based on
the CLN library.

%prep
%setup -q

%build

%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
rm -rf %{buildroot}%{_datadir}/dvi
rm -rf %{buildroot}%{_datadir}/html
rm -rf %{buildroot}%{_infodir}/dir

find %{buildroot} -name "*.la" -delete

rm -rf %{buildroot}%{_bindir} %{buildroot}%{_mandir}

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/cln.info --dir-file=%{_infodir}/dir

%preun devel
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/cln.info --dir-file=%{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc COPYING ChangeLog NEWS README TODO*
%doc doc examples
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/cln.pc
%{_includedir}/cln
%{_infodir}/*.info*

%changelog
* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-6m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-5m)
- rebuild against gmp-5.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-4m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-3m)
- we do not need pi command which conflicts with puppet

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-2m)
- use Requires

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1
- cln could be built with default CFLAGS
- cln does not conflict with any packages (remove REMOVEME.hogehoge)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-3m)
- rebuild against rpm-4.6

* Tue Aug  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-2m)
- enable to build with -O2 option on x86_64

* Thu Apr 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Sun Apr  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-3m)
- filter -O2 on x86_64 to enable build with gcc43

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-2m)
- rebuild against gcc43

* Wed Apr 02 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sun Feb 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.13-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.13-2m)
- delete libtool library

* Tue Nov 28 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.13-1m)
- 1.1.13 has been released

* Wed May 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.11-1m)
- import to Momonga for octave-forge

* Wed Apr 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.11-0.0.1m)
- update to 1.1.11

* Sun Feb 22 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.6-0.0.1m)
- build for Momonga
- modified src.rpm at ftp://ftpthep.physik.uni-mainz.de/pub/gnu/

* Thu Nov 20 2003 Christian Bauer <Christian.Bauer@uni-mainz.de>
  Added pkg-config metadata file to devel package
* Wed Nov  6 2002 Christian Bauer <Christian.Bauer@uni-mainz.de>
  Added HTML and DVI docs to devel package
* Tue Nov  5 2001 Christian Bauer <Christian.Bauer@uni-mainz.de>
  Added Packager
