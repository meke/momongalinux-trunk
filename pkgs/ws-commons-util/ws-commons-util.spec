%global momorel 7

%global         with_maven 1

Name:           ws-commons-util
Version:        1.0.1
Release:        %{momorel}m%{?dist}
Summary:        Common utilities from the Apache Web Services Project 

Group:          System Environment/Libraries
License:        "ASL 2.0"
URL:            http://apache.osuosl.org/ws/commons/util/
Source0:        http://apache.osuosl.org/ws/commons/util/sources/ws-commons-util-1.0.1-src.tar.gz
NoSource:       0
%if ! %{with_maven}
# Generated with mvn ant:ant and MANIFEST.MF added to jar task
Source1:        build.xml
Source2:        maven-build.xml
Source3:        MANIFEST.MF
%else
Patch0:         %{name}-addosgimanifest.patch
%endif
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  jpackage-utils >= 1.5
%if %{with_maven}
BuildRequires:  maven2
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-source
BuildRequires:  maven2-plugin-assembly
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-surefire
BuildRequires:  maven-surefire-provider-junit
BuildRequires:  maven2-plugin-eclipse
BuildRequires:  maven-doxia
BuildRequires:  maven-doxia-sitetools
%else
BuildRequires:  ant
%endif
BuildRequires:  junit
BuildRequires:  java-javadoc

%description 
This is version 1.0.1 of the common utilities from the Apache Web
Services Project.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description    javadoc
%{summary}.

%prep
%setup -q -n %{name}-%{version}
%if ! %{with_maven}
cp -p %{SOURCE1} .
cp -p %{SOURCE2} .
cp -p %{SOURCE3} .
%else
%patch0
%endif

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL
%if %{with_maven}
mvn-jpp \
  -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
  install javadoc:javadoc
%else
ant -Dmaven.mode.offline=true -Dmaven.repo.local=`pwd`/.m2 \
  -Djunit.skipped=true -Dmaven.test.skip=true javadoc package
%endif

%install
rm -rf $RPM_BUILD_ROOT

install -dm 755 $RPM_BUILD_ROOT%{_javadir}
install -pm 644 target/%{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

install -dm 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pR target/site/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644,root,root,755)
%doc LICENSE.txt
%{_javadir}/*.jar

%files javadoc
%defattr(644,root,root,755)
%doc %{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-5m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-4m)
- sync with Fedora 13 (1.0.1-14)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-2m)
- rebuild against rpm-4.6

* Thu Sep 13 2007 Andrew Overholt <overholt@redhat.com> 1.0.1-6
- Add BR on maven surefire resources, eclipse, and install plugins.

* Thu Sep 13 2007 Andrew Overholt <overholt@redhat.com> 1.0.1-5
- ExcludeArch ppc64 until maven is built on ppc64.

* Thu Sep 13 2007 Andrew Overholt <overholt@redhat.com> 1.0.1-4
- Bump again.

* Thu Sep 13 2007 Andrew Overholt <overholt@redhat.com> 1.0.1-3
- Bump release.

* Thu Sep 06 2007 Andrew Overholt <overholt@redhat.com> 1.0.1-2
- maven-ify.
- Add OSGi MANIFEST information.

* Fri Mar 16 2007 Anthony Green <green@redhat.com> - 1.0.1-1
- Created.
