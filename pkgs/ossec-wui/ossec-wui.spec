%global momorel 1

Summary:        OSSEC Web Interface
Name:           ossec-wui
Version:        0.3
Release:        %{momorel}m%{?dist}
URL:            http://www.ossec.net/
Source0:        http://www.ossec.net/files/ui/ossec-wui-%{version}.tar.gz
NoSource:       0
Source1:	ossec.conf
Source2:	htaccess
Source3:	htpasswd
License:        GPL
Group:          Applications/System
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:      noarch

%description
OSSEC Web Interface

%prep
%setup 

%build

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} -m 755 %{buildroot}/%{_sysconfdir}/httpd/conf.d/
%{__mkdir_p} -m 755 %{buildroot}/usr/share/%{name}/
%{__mkdir_p} -m 755 %{buildroot}/usr/share/%{name}/tmp
%{__install} -m 755 %{SOURCE1}  %{buildroot}/%{_sysconfdir}/httpd/conf.d/ossec.conf
%{__cp} -r * %{buildroot}/usr/share/%{name}/
%{__install} -m 644 %{SOURCE2}  %{buildroot}/usr/share/%{name}/.htaccess
%{__install} -m 644 %{SOURCE3}  %{buildroot}/usr/share/%{name}/.htpasswd

%clean
%{__rm} -rf %{buildroot}

%post 
# add apache to the ossec group
if groups apache | grep -qv ossec; then
  /usr/sbin/usermod -G ossec$(groups apache | awk -F: '{print $2}' |sed 's/ /,/g') apache 1>/dev/null 2>&1
fi

%files
%defattr(-,root,root)
%dir /usr/share/ossec-wui/
/usr/share/ossec-wui/*
%attr(0755,apache,apache) %dir /usr/share/ossec-wui/tmp
%config(noreplace)/usr/share/ossec-wui/.htaccess
%config(noreplace)/usr/share/ossec-wui/.htpasswd
/etc/httpd/conf.d/ossec.conf

%changelog
* Thu Apr 28 2011 Matthew Hall <yohgaki@ohgaki.net> 2.5.1-1m
- Import form www.firehat.org/SRPM/

* Wed Jan 19 2010 Matthew Hall <matt@ecsc.co.uk> 0.3-1%{?dist}
- Build for FireHat 2.4
