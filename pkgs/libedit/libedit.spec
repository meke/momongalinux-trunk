%global momorel 1
%global snap 20140618

Summary:	The NetBSD Editline library
Name:		libedit
Version:	3.1
Release:	0.%{snap}.%{momorel}m%{?dist}
License:	BSD
Group:		System Environment/Libraries
URL:		http://www.thrysoee.dk/editline/
Source0:	http://www.thrysoee.dk/editline/%{name}-%{snap}-%{version}.tar.gz 
#NoSource:	0

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	gawk
BuildRequires:	ncurses-devel

%description
Libedit is an autotool- and libtoolized port of the NetBSD Editline library.
It provides generic line editing, history, and tokenization functions, similar
to those found in GNU Readline.

%package devel
Summary:	Development files for %{name}
Group:		Development/Libraries

Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig
Requires:	ncurses-devel

%description devel
This package contains development files for %{name}.

%prep
%setup -q -n %{name}-%{snap}-%{version}

# Suppress rpmlint error.
iconv --from-code ISO8859-1 --to-code UTF-8 ./ChangeLog \
  --output ChangeLog.utf-8 && mv ChangeLog.utf-8 ./ChangeLog

%build
%configure --disable-static --enable-widec
# Trying to omit unused direct shared library dependencies leads to
# undefined non-weak symbols.

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install INSTALL="%{__install} -p" DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING THANKS
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root,-)
%doc examples/fileman.c examples/tc1.c examples/wtc1.c
%doc %{_mandir}/man3/*
%doc %{_mandir}/man5/editrc.5*
%{_includedir}/histedit.h
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc

%dir %{_includedir}/editline
%{_includedir}/editline/readline.h

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-0.20140618.1m)
- update 20140618

* Wed Mar 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-0.20140213.1m)
- update 20140213

* Mon Jul 22 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1-0.20130712.1m)
- update 20130712

* Sun Jan 27 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-0.20121213.1m)
- update 20121213

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-0.20120601.1m)
- update 20120601

* Fri Mar 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-0.20120311.1m)
- update 20120311

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-0.20118027.1m)
- update 20110802

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-0.20110227.2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-0.20110227.1m)
- update 20110227

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-0.20100424.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-0.20100424.3m)
- full rebuild for mo7 release

* Wed May  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-0.20100424.2m)
- add --enable-widec

* Wed May  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-0.20100424.1m)
- update 20100424

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11-0.20080712.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11-0.20080712.1m)
- update for ghc

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10-0.20061228.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10-0.20061228.3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10-0.20061228.2m)
- %%NoSource -> NoSource

* Sun Jan 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10-0.20061228.1m)
- initial package for uim-1.4.0
