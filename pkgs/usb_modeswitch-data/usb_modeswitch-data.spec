%global momorel 1

%define source_name	usb-modeswitch-data

Name:		usb_modeswitch-data
Version:	20111023
Release:	%{momorel}m%{?dist}
Summary:	USB Modeswitch gets 4G cards in operational mode 
Group:		Applications/System
License:	GPLv2+
URL:		http://www.draisberghof.de/usb_modeswitch/
Source0:	http://www.draisberghof.de/usb_modeswitch/%{source_name}-%{version}.tar.bz2
NoSource:	0
BuildArch:	noarch
Requires:	tcl
Requires:	udev
Requires:	usb_modeswitch >= 1.1.2

%description
USB Modeswitch brings up your datacard into operational mode. When plugged
in they identify themselves as cdrom and present some non-Linux compatible
installation files. This tool deactivates this cdrom-devices and enables
the real communication device. It supports most devices built and
sold by Huawei, T-Mobile, Vodafone, Option, ZTE, Novatel.

This package contains the data files needed for usb_modeswitch to function.


%prep
%setup -q -n %{source_name}-%{version}


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/usb_modeswitch.d/
mkdir -p $RPM_BUILD_ROOT/lib/udev/rules.d/

install -p -m 644  usb_modeswitch.d/* $RPM_BUILD_ROOT%{_sysconfdir}/usb_modeswitch.d/
install -p -m 644 40-usb_modeswitch.rules $RPM_BUILD_ROOT/lib/udev/rules.d

%clean
rm -rf $RPM_BUILD_ROOT

%post 
udevadm control --reload-rules

%postun
udevadm control --reload-rules

%files
%defattr(-,root,root,-)
/lib/udev/rules.d/40-usb_modeswitch.rules
%config %{_sysconfdir}/usb_modeswitch.d/
%doc ChangeLog COPYING README

%changelog
* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20111023-1m)
- update 20111023

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100707-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100707-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100707-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100707-1m)
- import from Fedora 13 and update to 20100707

* Tue Jun 22 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> 20100621-1
- New upstream  

* Tue Apr 20 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> 20100418-2
- Remove buildroot, make package noarch

* Tue Apr 20 2010 Huzaifa Sidhpurwala <huzaifas@redhat.com> 20100418-1
- First build
