%global momorel 2

Summary: standalone graphical debugger
Name: nemiver

Version: 0.9.3
Release: %{momorel}m%{?dist}
License: LGPL
Group: Development/Tools
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.9/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: ghex-devel
BuildRequires: glibmm-devel
BuildRequires: sqlite-devel
BuildRequires: gsettings-desktop-schemas-devel
BuildRequires: GConf2-devel
BuildRequires: glib2-devel
BuildRequires: libxml2-devel
BuildRequires: libgtop2-devel
BuildRequires: gtkmm3-devel >= 3.5.12-2m
BuildRequires: gtk3-devel
BuildRequires: gtksourceviewmm-devel
BuildRequires: vte3-devel
BuildRequires: libgdl-devel

%description
Nemiver is a project to write a standalone graphical debugger that integrates
well in the GNOME desktop environment.
It currently features a backend which uses the well known GNU Debugger gdb.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-scrollkeeper \
	--disable-schemas-compile \
	--disable-schemas-install \
 	--enable-dynamiclayout=no \
	--disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_datadir}/applications/nemiver.desktop
%{_datadir}/glib-2.0/schemas/org.nemiver.gschema.xml
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/apps/*
%{_mandir}/man1/%{name}.1.*
%{_datadir}/omf/%{name}

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_datadir}/gnome/help/%{name}


%changelog
* Sun Oct 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-2m)
- specify gtkmm3 version and release

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-5m)
- rebuild without libgdlmm

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-4m)
- revise BuildRequires

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-3m)
- rebuild for vte3-devel

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-2m)
- rebuild for glib 2.33.2

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

