%global         momorel 1

%define         cbq_version v0.7.3

Summary:        Advanced IP routing and network device configuration tools
Name:           iproute
Version:        3.15.0
Release:        %{momorel}m%{?dist}
Group:          Applications/System
License:        GPLv2+
URL:		http://www.linuxfoundation.org/collaborate/workgroups/networking/iproute2
Source0:	http://kernel.org/pub/linux/utils/net/iproute2/iproute2-%{version}.tar.xz
NoSource: 0
Source1:        cbq-0000.example
Source2:        avpkt
Patch0:             man-pages.patch
Patch1:             iproute2-3.4.0-kernel.patch
Patch2:             iproute2-3.15.0-optflags.patch
Patch3:             iproute2-3.9.0-IPPROTO_IP_for_SA.patch
Patch4:             iproute2-example-cbq-service.patch
Patch5:             iproute2-2.6.35-print-route.patch
Patch6:             iproute2-3.12.0-lnstat-dump-to-stdout.patch
# Rejected by upstream <http://thread.gmane.org/gmane.linux.network/284101>
Patch7:             iproute2-3.11.0-tc-ok.patch
Patch8:            iproute2-3.11.0-rtt.patch
Patch9:            iproute2-3.12.0-lnstat-interval.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  bison
BuildRequires:  libdb-devel >= 5.3.15
BuildRequires:  flex
BuildRequires:  iptables-devel >= 1.4.5
BuildRequires:  linuxdoc-tools
BuildRequires:  linux-atm-libs-devel
BuildRequires:  tetex-dvips
BuildRequires:  tetex-latex
BuildRequires:  psutils
Requires:       iptables >= 1.4.5
Provides:       /sbin/ip

%description
The iproute package contains networking utilities (ip and rtmon, for
example) which are designed to use the advanced networking
capabilities of the Linux 2.4.x and 2.6.x kernel.

%package doc
Summary:        ip and tc documentation with examples
Group:          Applications/System
License:        GPLv2+

%description doc
The iproute documentation contains howtos and examples of settings.

%package devel
Summary:            iproute development files
Group:              Development/Libraries
License:            GPLv2+
Provides:           iproute-static = %{version}-%{release}

%description devel
The libnetlink static library.

%prep
%setup -q -n iproute2-%{version}
%patch0 -p1
%patch1 -p1 -b .kernel
%patch2 -p1 -b .opt_flags
%patch3 -p1 -b .ipproto
%patch4 -p1 -b .fix_cbq
%patch5 -p1 -b .print-route
%patch6 -p1 -b .lnstat-dump-to-stdout
%patch7 -p1 -b .tc_ok
%patch8 -p1 -b .rtt
%patch9 -p1 -b .lnstat-interval
sed -i 's/^LIBDIR=/LIBDIR?=/' Makefile

%build
export LIBDIR=/%{_libdir}
export IPT_LIB_DIR=/%{_lib}/xtables
./configure
make %{?_smp_mflags}
make -C doc


%install
rm --preserve-root -rf %{buildroot}

mkdir -p \
    %{buildroot}%{_includedir} \
    %{buildroot}%{_sbindir} \
    %{buildroot}%{_mandir}/man3 \
    %{buildroot}%{_mandir}/man7 \
    %{buildroot}%{_mandir}/man8 \
    %{buildroot}%{_libdir}/tc \
    %{buildroot}%{_sysconfdir}/iproute2 \
    %{buildroot}%{_sysconfdir}/sysconfig/cbq

for binary in \
    bridge/bridge \
    examples/cbq.init-%{cbq_version} \
    genl/genl \
    ip/ifcfg \
    ip/ip \
    ip/routef \
    ip/routel \
    ip/rtmon \
    ip/rtpr \
    misc/arpd \
    misc/ifstat \
    misc/lnstat \
    misc/nstat \
    misc/rtacct \
    misc/ss \
    tc/tc
    do install -m755 ${binary} %{buildroot}%{_sbindir}
done
mv %{buildroot}%{_sbindir}/cbq.init-%{cbq_version} %{buildroot}%{_sbindir}/cbq
cd %{buildroot}%{_sbindir}
    ln -s lnstat ctstat
    ln -s lnstat rtstat
cd -

# Libs
install -m644 netem/*.dist %{buildroot}%{_libdir}/tc
install -m755 tc/q_atm.so %{buildroot}%{_libdir}/tc
install -m755 tc/m_xt.so %{buildroot}%{_libdir}/tc
cd %{buildroot}%{_libdir}/tc
    ln -s m_xt.so m_ipt.so
cd -

# libnetlink
install -m644 include/libnetlink.h %{buildroot}%{_includedir}
install -m644 lib/libnetlink.a %{buildroot}%{_libdir}

# Manpages
iconv -f latin1 -t utf8 man/man8/ss.8 > man/man8/ss.8.utf8 &&
    mv man/man8/ss.8.utf8 man/man8/ss.8
install -m644 man/man3/*.3 %{buildroot}%{_mandir}/man3
install -m644 man/man7/*.7 %{buildroot}%{_mandir}/man7
install -m644 man/man8/*.8 %{buildroot}%{_mandir}/man8

# Config files
install -m644 etc/iproute2/* %{buildroot}%{_sysconfdir}/iproute2
for config in \
    %{SOURCE1} \
    %{SOURCE2}
    do install -m644 ${config} %{buildroot}%{_sysconfdir}/sysconfig/cbq
done

%clean
rm -rf --preserve-root %{buildroot}

%files
%dir %{_sysconfdir}/iproute2
%doc COPYING
%doc README README.decnet README.iproute2+tc README.distribution README.lnstat
%{_mandir}/man7/*
%{_mandir}/man8/*
%attr(644,root,root) %config(noreplace) %{_sysconfdir}/iproute2/*
%{_sbindir}/*
%dir %{_libdir}/tc/
%{_libdir}/tc/*
%dir %{_sysconfdir}/sysconfig/cbq
%config(noreplace) %{_sysconfdir}/sysconfig/cbq/*

%files doc
%doc COPYING
%doc doc/*.ps
%doc examples

%files devel
%doc COPYING
%{_mandir}/man3/*
%{_libdir}/libnetlink.a
%{_includedir}/libnetlink.h

%changelog
* Mon Jun 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.15.0-1m)
- update to 3.15.0

* Sun Mar 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.12.0-1m)
- update to 3.12.0

* Mon Oct 15 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0
- remove patches:
  Patch0:         man-pages.patch
  Patch3:         iproute2-2.6.25-segfault.patch
  Patch4:         iproute2-sharepath.patch
  Patch8:         iproute2-tc-priority.patch
  Patch9:         iproute2-2.6.34-build-fix.patch

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.34-8m)
- rebuild against libdb-5.3.15

* Mon Jan 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.34-7m)
- add source tarball

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.34-6m)
- rebuild against libdb

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.34-5m)
- fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.34-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.34-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.34-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linu.org>
- (2.6.34-1m)
- update to 2.6.34
- modified based on Fedora devel's 2.6.34-5

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.29-3m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.29-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.29-1m)
- sync with Fedora 11 (2.6.29-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.23-4m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-3m)
- rebuild against db4-4.7.25-1m

* Mon May 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.23-2m)
- depend on linux-atm-libs

* Tue Apr 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.23-1m)
- update to 2.6.23
-- sync fedora (almost :-)
-- no tc depend on linux-atm-libs
-- build fix only

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.20-3m)
- rebuild against gcc43

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.20-2m)
- rebuild against db4-4.6.21

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.20-1m)
- update 2.6.20
- sync fc-devel

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-2m)
- rebuild against db-4.5

* Sun May  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.15-1m)
- version up
- based on fc-devel

* Tue Dec 16 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.4.7-8m)
- [SECURITY] CAN-2003-0856
  iproute 2.4.7 and earlier allows local users to cause a denial of service via spoofed messages as other users to the kernel netlink interface. 

* Fri Sep 24 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.4.7-7m)
- add patch8 for kernel 2.6

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.7-6m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m
- change _ipv6 to usagi_ipv6

* Sun Nov  9 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.4.7-5m)
- use %%{momorel}

* Sat May 18 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.4.7-4k)
- add usagi patch

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (2.4.7-2k)
- ver up.

* Sat Jun 23 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.2.4-16k)
- add iproute-2.2.4-kernel-2.4.4.patch for kernel 2.4.4

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.2.4-14k)
- rebuild against disable IPv6 environment

* Tue Nov 28 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- remake Patch1

* Fri Oct 27 2000 Kenichi Matsubara <m@kondara.org>
- add BuildPrereq: dvipsk

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org>
- Kodaraized.

* Tue Jul 25 2000 Jakub Jelinek <jakub@redhat.com>
- fix include-glibc/ to cope with glibc 2.2 new resolver headers

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Than Ngo <than@redhat.de>
- rebuilt in the new build environment
- use RPM macros
- handle RPM_OPT_FLAGS

* Sat Jun 03 2000 Than Ngo <than@redhat.de>
- fix iproute to build with new glibc

* Fri May 26 2000 Ngo Than <than@redhat.de>
- update to 2.2.4-now-ss000305
- add configuration files

* Mon Sep 13 1999 Bill Nottingham <notting@redhat.com>
- strip binaries

* Mon Aug 16 1999 Cristian Gafton <gafton@redhat.com>
- first build
