%global momorel 9

%define dropdir %(pkg-config libpcsclite --variable usbdropdir 2>/dev/null)
%define pcsc_cflags %(pkg-config libpcsclite --cflags 2>/dev/null)
%define libusb_version 0.1.7

Name: ifd-egate
Version: 0.05 
Release: %{momorel}m%{?dist}
Summary: Axalto Egate SmartCard device driver for PCSC-lite
Group: System Environment/Base
License: BSD or LGPL
URL: http://secure.netroedge.com/~phil/egate/
Source: ifd-egate-%{version}.tar.gz
Patch0: ifd-egate-0.05-timing.patch
Patch1: ifd-egate-0.05-makefile.patch
Patch2: ifd-egate-0.05-write.patch
Patch3: ifd-egate-0.05-multiple_cards.patch
Patch4: ifd-egate-0.05-slow_hubs.patch
Patch5: ifd-egate-0.05-udev.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pcsc-lite-devel
BuildRequires: libusb-devel >= %{libusb_version}
Requires: pcsc-lite
Requires: libusb >= %{libusb_version}
Requires: udev >= 151
Requires(post): initscripts
Requires(postun): initscripts
Provides: pcsc-ifd-handler
# 390 does not have libusb or smartCards
ExcludeArch: s390 s390x

%description
The Axalto Egate device driver enables PCSC-lite to communicate with Axalto
Egate cards, which CoolKey is based off of. 

%prep
%setup -q
%patch0 -p1 -b .timing
%patch1 -p1 -b .makefile
%patch2 -p1 -b .write
%patch3 -p1 -b .multiple_cards
%patch4 -p1 -b .slow_hubs
%patch5 -p1 -b .udev

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%post
[ -x %{_initscriptdir}/pcscd ] && %{_initscriptdir}/pcscd status > /dev/null 2>&1 && pcscd -H  > /dev/null 2>&1
exit 0

%postun
[ -x %{_initscriptdir}/pcscd ] && %{_initscriptdir}/pcscd status > /dev/null 2>&1 && pcscd -H  > /dev/null 2>&1
exit 0

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING.LIB COPYRIGHT LICENSE PROTOCOL README
%{dropdir}/ifd-egate.bundle/
%{_sysconfdir}/udev/rules.d/85-pcscd_egate.rules

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.05-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.05-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.05-7m)
- full rebuild for mo7 release

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.05-6m)
- update udev.patch for udev-151

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.05-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.05-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.05-3m)
- rebuild against gcc43

* Tue Jul 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.05-2m)
- use %%{_initrddir} instead of %%{_initscriptdir}

* Wed Mar 14 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.05-1m)
- import from fc

* Tue Feb 06 2007 Bob Relyea <rrelyea@redhat.com>     0.05-16
- support new pcsc-lite udev functions

* Mon Sep 28 2006 Bob Relyea <rrelyea@redhat.com>     0.05-15
- Fix slow hub issues

* Mon Sep 11 2006 Bob Relyea <rrelyea@redhat.com>     0.05-14
- Fix multiple card support

* Thu Jul 20 2006 Florian La Roche <laroche@redhat.com>
- the post/postun code at least needs inintscripts installed

* Sun Jul 16 2006 Florian La Roche <laroche@redhat.com> - 0.05-12
- fix excludearch line

* Mon Jul 13 2006 Bob Relyea <rrelyea@redhat.com>     0.05-11
- Fix long standing incorrect error message. patch by tmraz

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.05-10.1
- rebuild

* Mon Jul 10 2006 Bob Relyea <rrelyea@redhat.com>     0.05-10
- remove s390 from the build

* Thu Jun  9 2006 Bob Relyea <rrelyea@redhat.com>     0.05-9
- integrate review comments:
- rename timing patch.
- make libusb-version a define.
- use pkg-config to find the appropriate flags and directories for pcsc-lite
- fix service restart to be more friendly
- remove mac specific README

* Thu Jun  8 2006 Bob Relyea <rrelyea@redhat.com>     0.05-8
- add provides pcsc-ifd-handler so which fulfills the pcsc-lite dependency for
  at least one reader.

* Thu May 24 2006 Bob Relyea <rrelyea@redhat.com>     0.05-7
- update spec to meet Fedora package requirements

* Thu May 17 2006 Bob Relyea <rrelyea@redhat.com>     0.05-6
- Increased timing again for FC5

* Thu Apr 20 2006 Bob Relyea <rrelyea@redhat.com>     0.05-5
- rebuild for FC5

* Thu Jan 26 2005 Bob Relyea <rrelyea@redhat.com>     0.05-4
- Added Requires for pcsc-lite
- fix attributes

* Thu Jan 26 2005 Bob Relyea <rrelyea@redhat.com>     0.05-3
- Fixed problem where insertion did not always get detected

* Wed Jan 4 2005 Bob Relyea <rrelyea@redhat.com>      0.05-2
- correct timing sleeps so slow tokens still work on fast machines.
- add make variables to control the install target location
