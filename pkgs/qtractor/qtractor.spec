%global momorel 1
%global qtver 4.8.5
%global rubberbandver 1.5.0

Summary: An Audio/MIDI multi-track sequencer
Name: qtractor
Version: 0.5.12
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://qtractor.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.4.6-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: ImageMagick
BuildRequires: alsa-lib-devel
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: dssi-devel
BuildRequires: jack-devel
BuildRequires: ladspa-devel
BuildRequires: libmad
BuildRequires: liblo-devel
BuildRequires: libsamplerate-devel
BuildRequires: libsndfile-devel
BuildRequires: libvorbis-devel
BuildRequires: rubberband-devel >= %{rubberbandver}
BuildRequires: slv2-devel >= 0.6.6-5m

%description
Qtractor is an Audio/MIDI multi-track sequencer application
written in C++ with the Qt4 framework.
Target platform is Linux, where the Jack Audio Connection Kit (JACK)
for audio, and the Advanced Linux Sound Architecture (ALSA) for MIDI,
are the main infrastructures to evolve as a fairly-featured Linux desktop
audio workstation GUI, specially dedicated to the personal home-studio.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-key X-SuSE-translate \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

# install icon
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32}/apps
convert -scale 16x16 src/images/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README* TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/mimetypes/*.png
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/icons/hicolor/scalable/mimetypes/*.svg
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/locale/%{name}_cs.qm
%{_datadir}/locale/%{name}_de.qm
%{_datadir}/locale/%{name}_fr.qm
%{_datadir}/locale/%{name}_it.qm
%{_datadir}/locale/%{name}_ja.qm
%{_datadir}/locale/%{name}_ru.qm

%changelog
* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.12-1m)
- version 0.5.12

* Mon Oct  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.11-1m)
- version 0.5.11

* Sun Jul 21 2013 NARITA Koichi <pulsar[momonga-linux.org>
- (0.5.10-1m)
- version 0.5.10

* Sat Jun  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.9-1m)
- version 0.5.9

* Wed Mar 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-1m)
- version 0.5.8

* Fri Dec 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.7-1m)
- version 0.5.7

* Fri Oct  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.6-2m)
- source tarball was replaced, use --rmsrc option to build

* Tue Oct  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.6-1m)
- version 0.5.6

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.5-1m)
- version 0.5.5

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-1m)
- version 0.5.4

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- version 0.5.3

* Sat Dec 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.2-1m)
- version 0.5.2

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-1m)
- version 0.5.1

* Sat Jul 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- version 0.5.0

* Fri May 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.9-1m)
- version 0.4.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-2m)
- rebuild for new GCC 4.6

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.8-1m)
- version 0.4.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.7-2m)
- rebuild for new GCC 4.5

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.7-1m)
- version 0.4.7

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.6-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.6-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.6-2m)
- rebuild against qt-4.6.3-1m

* Sat May 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-1m)
- version 0.4.6
- update desktop.patch
- remove linking.patch

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-3m)
- explicitly link libX11

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5-2m)
- touch up spec file

* Wed Jan 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5-1m)
- version 0.4.5

* Mon Jan 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-1m)
- version 0.4.4
- build with slv2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3-1m)
- version 0.4.3

* Sun Jun 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-1m)
- initial package for music freaks using Momonga Linux
