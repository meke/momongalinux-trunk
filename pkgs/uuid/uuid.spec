%global momorel 23

Name:           uuid
Version:        1.6.2
Release:        %{momorel}m%{?dist}
Summary:        Universally Unique Identifier library
License:        MIT
Group:          System Environment/Libraries
URL:            http://www.ossp.org/pkg/lib/uuid/
Source0:        ftp://ftp.ossp.org/pkg/lib/uuid/uuid-%{version}.tar.gz
NoSource:	0
Patch0:         uuid-1.6.1-ossp.patch
Patch1:         uuid-1.6.2-pgsql-mkdir.patch
Patch2:         uuid-1.6.2-php54.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libtool

%description
OSSP uuid is a ISO-C:1999 application programming interface (API)
and corresponding command line interface (CLI) for the generation
of DCE 1.1, ISO/IEC 11578:1996 and RFC 4122 compliant Universally
Unique Identifier (UUID). It supports DCE 1.1 variant UUIDs of version
1 (time and node based), version 3 (name based, MD5), version 4
(random number based) and version 5 (name based, SHA-1). Additional
API bindings are provided for the languages ISO-C++:1998, Perl:5 and
PHP:4/5. Optional backward compatibility exists for the ISO-C DCE-1.1
and Perl Data::UUID APIs.

%package devel
Summary:        Development support for Universally Unique Identifier library
Group:          Development/Libraries
Requires:       pkgconfig
Requires:       %{name} = %{version}-%{release}

%description devel
Development headers and libraries for OSSP uuid.

%package c++
Summary:        C++ support for Universally Unique Identifier library
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}

%description c++
C++ libraries for OSSP uuid.

%package c++-devel
Summary:        C++ development support for Universally Unique Identifier library
Group:          Development/Libraries
Requires:       %{name}-c++ = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}

%description c++-devel
C++ development headers and libraries for OSSP uuid.

%package perl
Summary:        Perl support for Universally Unique Identifier library
Group:          Development/Libraries
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires:       %{name} = %{version}-%{release}
Obsoletes:	perl-Data-UUID
Provides:	perl-Data-UUID

%description perl
Perl OSSP uuid modules, which includes a Data::UUID replacement.

%package php
Summary:        PHP support for Universally Unique Identifier library
Group:          Development/Libraries
BuildRequires:  php-devel >= 5.4.1
Requires:       php-common
Requires:       %{name} = %{version}-%{release}

%description php
PHP OSSP uuid module.

%package pgsql
Summary:        PostgreSQL support for Universally Unique Identifier library
Group:          Development/Libraries
BuildRequires:  postgresql-devel
Requires:       postgresql
Requires:       postgresql-server
Requires:       %{name} = %{version}-%{release}

%description pgsql
PostgreSQL OSSP uuid module.

%package dce
Summary:        DCE support for Universally Unique Identifier library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description dce
DCE OSSP uuid library.

%package dce-devel
Summary:        DCE development support for Universally Unique Identifier library
Group:          Development/Libraries
Requires:       %{name}-dce = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}

%description dce-devel
DCE development headers and libraries for OSSP uuid.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
# Build the library.
export LIB_NAME=libossp-uuid.la
export DCE_NAME=libossp-uuid_dce.la
export CXX_NAME=libossp-uuid++.la
export PHP_NAME=$RPM_SOURCE_DIR/php/modules/ossp-uuid.so
export PGSQL_NAME=$RPM_SOURCE_DIR/pgsql/libossp-uuid.so
%configure \
    --disable-static \
    --without-perl \
    --without-php \
    --with-dce \
    --with-cxx \
    --with-pgsql

make LIBTOOL="%{_bindir}/libtool --tag=CC" %{?_smp_mflags}

# Build the Perl module.
pushd perl
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS" COMPAT=1
%{__perl} -pi -e 's/^\tLD_RUN_PATH=[^\s]+\s*/\t/' Makefile
make %{?_smp_mflags}
popd

# Build the PHP module.
pushd php
phpize
CFLAGS="$RPM_OPT_FLAGS -I.. -L.. -L../.libs"
%configure --enable-uuid
sed -i -e '/^LDFLAGS =/s/-Wl,-rpath,[^[:space:]]*//' Makefile
make LIBTOOL="%{_bindir}/libtool --tag=CC" %{?_smp_mflags}
popd

%install
rm -rf $RPM_BUILD_ROOT

make install LIBTOOL="%{_bindir}/libtool" DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la $RPM_BUILD_ROOT%{_libdir}/*.a
chmod 755 $RPM_BUILD_ROOT%{_libdir}/*.so.*.*.*

# Install the Perl modules.
pushd perl
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*
popd

# Install the PHP module.
pushd php
make install INSTALL_ROOT=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_libdir}/php/modules/*.a
popd

%check
make check

pushd perl
LD_LIBRARY_PATH=../.libs make test
popd

pushd php
LD_LIBRARY_PATH=../.libs make test LIBTOOL="%{_bindir}/libtool --tag=CC"
popd

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%post c++ -p /sbin/ldconfig

%post dce -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%postun c++ -p /sbin/ldconfig

%postun dce -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog HISTORY NEWS PORTING README SEEALSO THANKS TODO USERS
%{_bindir}/uuid
%{_libdir}/libossp-uuid.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%{_bindir}/uuid-config
%{_includedir}/uuid.h
%{_libdir}/libossp-uuid.so
%{_libdir}/pkgconfig/ossp-uuid.pc
%{_mandir}/man3/ossp-uuid.3*

%files c++
%defattr(-,root,root,-)
%{_libdir}/libossp-uuid++.so.*

%files c++-devel
%defattr(-,root,root,-)
%{_includedir}/uuid++.hh
%{_libdir}/libossp-uuid++.so
%{_mandir}/man3/uuid++.3*

%files perl
%defattr(-,root,root,-)
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Data*
%{perl_vendorarch}/OSSP*
%{_mandir}/man3/Data::UUID.3*
%{_mandir}/man3/OSSP::uuid.3*

%files php
%defattr(-,root,root,-)
%{_libdir}/php/modules/*

%files pgsql
%defattr(-,root,root,-)
%{_libdir}/pgsql/*
%{_datadir}/pgsql/*

%files dce
%defattr(-,root,root,-)
%{_libdir}/libossp-uuid_dce.so.*

%files dce-devel
%defattr(-,root,root,-)
%{_includedir}/uuid_dce.h
%{_libdir}/libossp-uuid_dce.so

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-23m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-22m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-21m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-20m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-19m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-18m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-17m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-16m)
- rebuild against perl-5.16.0

* Sun Apr 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-15m)
- rebuild against php-5.4.x

* Thu Apr  5 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (1.6.2-14m)
- add patch1 (uuid-1.6.2-pgsql-mkdir.patch)

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.2-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.2-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.2-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-6m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-3m)
- rebuild against perl-5.10.1

* Mon May  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-2m)
- use %%{_bindir}/libtool in "make install" to enable build

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2-1m)
- update 1.6.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1-4m)
- rebuild against gcc43

* Mon Mar 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-3m)
- add Obsoletes and Provides for perl-Data-UUID

* Mon Mar 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-2m)
- modify Requires
- uuid-perl is conlicting with perl-Data-UUID, please fix it

* Mon Mar 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- import from Fedora

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.6.1-3
Rebuild for new perl

* Thu Mar  6 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.6.1-2
- forgot to cvs add patch

* Thu Mar  6 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.6.1-1
- 1.6.1

* Thu Mar  6 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.6.0-4
- rebuild for new perl

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.6.0-3
- Autorebuild for GCC 4.3

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 1.6.0-2
- Rebuild for selinux ppc32 issue.

* Tue Jul 24 2007 Steven Pritchard <steve@kspei.com> 1.6.0-1
- Update to 1.6.0.
- BR Test::More.

* Tue Apr 17 2007 Steven Pritchard <steve@kspei.com> 1.5.1-3
- Fix find option order.
- Use fixperms macro instead of our own chmod incantation.
- BR ExtUtils::MakeMaker.

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 1.5.1-2
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Aug 28 2006 Steven Pritchard <steve@kspei.com> 1.5.1-1
- Update to 1.5.1.

* Sat Jul 29 2006 Steven Pritchard <steve@kspei.com> 1.5.0-1
- Update to 1.5.0.
- Rename libuuid* to libossp-uuid*, uuid.3 to ossp-uuid.3, and uuid.pc
  to ossp-uuid.pc to avoid conflicts with e2fsprogs-devel (#198520).
- Clean out the pgsql directory.  (Some cruft shipped with this release.)

* Wed May 24 2006 Steven Pritchard <steve@kspei.com> 1.4.2-4
- Remove static php module.

* Tue May 23 2006 Steven Pritchard <steve@kspei.com> 1.4.2-3
- Force use of system libtool.
- Make libs executable.

* Tue May 23 2006 Steven Pritchard <steve@kspei.com> 1.4.2-2
- License is MIT(-ish).

* Fri May 19 2006 Steven Pritchard <steve@kspei.com> 1.4.2-1
- Initial packaging attempt.
