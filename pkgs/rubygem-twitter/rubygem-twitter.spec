# Generated from twitter-2.5.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname twitter

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Twitter API wrapper
Name: rubygem-%{gemname}
Version: 2.5.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: https://github.com/jnunemaker/twitter
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.3.6
Requires: ruby 
Requires: rubygem(faraday) => 0.8
Requires: rubygem(faraday) < 1
Requires: rubygem(multi_json) => 1.3
Requires: rubygem(multi_json) < 2
Requires: rubygem(simple_oauth) => 0.1.6
Requires: rubygem(simple_oauth) < 0.2
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.3.6
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
A Ruby wrapper for the Twitter API.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.0-1m)
- update 2.5.0

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.0-1m)
- update 2.2.0

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.2-1m)
- update 2.0.2

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-1m)
- update 1.7.2

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.1-1m)
- update 1.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.12-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.12-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.12-1m)
- update to 0.9.12

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-1m)
- update to 0.9.8

* Thu Jan 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.15-1m)
- update to 0.6.15
- Requires: rubygem(oauth) >= 0.3.5

* Tue Jul 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.13-1m)
- update 0.6.13

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.12-1m)
- update 0.6.12

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.5-1m)
- update 0.6.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-2m)
- rebuild against rpm-4.6

* Fri Dec 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.8-1m)
- initial made for Momonga Linux
