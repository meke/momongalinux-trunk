%global momorel 1

%define examplesdir __tmp_examples

Name:           libmowgli
Version:        0.9.50
Release:        %{momorel}m%{?dist}
Summary:        Library of many utility functions and classes
Group:          System Environment/Libraries
License:        MIT
URL:            http://atheme.org/projects/mowgli.shtml
Source0:        http://distfiles.atheme.org/libmowgli-%{version}.tar.bz2
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
libmowgli is a development framework for C (like GLib), which provides high
performance and highly flexible algorithms. It can be used as a suppliment to
GLib (to add additional functions (dictionaries, hashes), or replace some of
the slow GLib list manipulation functions), or stand alone. It also provides a
powerful hook system and convenient logging for your code, as well as a high
performance block allocator.

%package devel
Summary:        Development files for libmowgli
Group:          Development/Libraries

Requires:       libmowgli = %{version}-%{release}
Requires:       pkgconfig

%description devel
This package contains the files that are needed when building
software that uses libmowgli.

%prep
%setup -q

# Make the build system more verbose
sed -i '\,^.SILENT:,d' buildsys.mk.in

# Prepare the examples for %%doc inclusion.
rm -rf %{examplesdir} ; mkdir %{examplesdir}
cp -a src/examples %{examplesdir}
find %{examplesdir} -name Makefile | xargs rm -f


%build
%configure \
    --enable-examples \
    --disable-dependency-tracking

make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"


%check
# Execute examples for some testing. Not really a test-suite.
for f in $(find src/examples -type f -executable) ; do
    LD_LIBRARY_PATH=${RPM_BUILD_ROOT}%{_libdir}  $f || true
done
exit 0


%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc src/examples
%{_libdir}/*.so
%{_includedir}/libmowgli
%{_libdir}/pkgconfig/libmowgli.pc

%changelog
* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.50-1m)
- update to 0.9.50

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-1m)
- sync with Fedora 13 (0.7.0-4)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-2m)
- rebuild against gcc43

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.1-1m)
- import to Momonga from Fedora
- update to 0.6.1

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.5.0-2
- Autorebuild for GCC 4.3

* Sat Nov 10 2007 Ralf Ertzinger <ralf@skytale.net> 0.5.0-1
- Initial build for FE
