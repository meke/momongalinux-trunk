%global momorel 8

Summary: A dictionary server for the SKK Japanese input method system
Name: dbskkd-cdb
Version: 2.00
Release: %{momorel}m%{?dist}
License: Creative Commons or Modified BSD
Group: System Environment/Daemons
Source0: http://dbskkd-cdb.googlecode.com/files/dbskkd-cdb-%{version}.tar.gz
NoSource: 0
Source1: skkserv-xinetd
URL: http://code.google.com/p/dbskkd-cdb/
Requires: chkconfig, xinetd
Requires(triggerin): util-linux-ng grep awk procps skk-jisyo >= 1.0-13m
Requires(pre): procps grep awk util-linux-ng
Requires(post): util-linux-ng

BuildRequires: tinycdb, tinycdb-devel, skk-jisyo >= 1.0-13m
Requires: tinycdb
Obsoletes: rskkserv
Provides: dbskkd-cdb, skkserv

%description
dbskkd-cdb is a dictionary server for SKK Japanese Input Method system.

%prep
%setup -q
sed -i -e 's|#define JISYO_FILE	"/usr/local/share/skk/SKK-JISYO.L.cdb"|#define JISYO_FILE	"/usr/share/skk/SKK-JISYO.Momonga.cdb"|' dbskkd-cdb.c

%build
make SERVERDIR=%{_sbindir} CC='gcc $(CFLAGS)' \
    CFLAGS="%(echo '%{optflags}' | sed -e 's/-fomit-frame-pointer//g')" \
    CDBLIB=%{_libdir}/libcdb.a
sh skktocdbm.sh < %{_datadir}/skk/SKK-JISYO.Momonga | \
    cdb -c -t - SKK-JISYO.Momonga.cdb

%install
mkdir -p %{buildroot}%{_sbindir}
install -c -s -m 0755 dbskkd-cdb %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_bindir}
install -c -m 0755 skktocdbm.sh %{buildroot}%{_bindir}/skktocdbm
mkdir -p %{buildroot}%{_datadir}/skk
install -c -m 0644 SKK-JISYO.Momonga.cdb %{buildroot}%{_datadir}/skk

mkdir -p %{buildroot}%{_sysconfdir}/xinetd.d
install -c -m 0644 %{SOURCE1} %{buildroot}/etc/xinetd.d/skkserv

%clean
rm -rf %{buildroot}

%triggerin -- skk-jisyo
for pid in `ps ax | grep dbskkd-cdb | grep -v 'grep\|yum\|mph\|rpm\|OmoiKondara' | awk '{print $1}'`; do
    kill -TERM $pid
done

export LANG=C
if test "$2" = "2"; then
    pushd %{_datadir}/skk >/dev/null
    if ps aux | grep dbskkd -q; then
	skktocdbm < SKK-JISYO.Momonga | \
	    cdb -c -t - SKK-JISYO.Momonga.cdb.rpmnew
    else
	skktocdbm < SKK-JISYO.Momonga | \
	    cdb -c -t - SKK-JISYO.Momonga.cdb
    fi
    popd >/dev/null
fi

%pre
for pid in `ps ax | grep dbskkd-cdb | grep -v 'grep\|yum\|mph\|rpm\|OmoiKondara' | awk '{print $1}'`; do
    kill -TERM $pid
done

%postun
if test "$1" = "0"; then
    (test -e /var/run/xinetd.pid && kill -USR2 `cat /var/run/xinetd.pid`) || true
fi


%files
%defattr(-,root,root)
%doc CHANGES LICENSE README READMEJP
%config(missingok,noreplace) %{_sysconfdir}/xinetd.d/skkserv
%{_bindir}/skktocdbm
%{_sbindir}/dbskkd-cdb
%{_datadir}/skk/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.00-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.00-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.00-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.00-5m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.00-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.00-3m)
- stop dbskkd-cdb when update SKK-JISYO.Momonga.cdb

* Wed Feb 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.00-2m)
- set JISYO_FILE to /usr/share/skk/SKK-JISYO.Momonga.cdb

* Mon Feb 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.00-1m)
- update to 2.00

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.01-16m)
- rebuild against rpm-4.6

* Sun Sep  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.01-15m)
- LANG=C in %%triggerin -- skk-jisyo
- revise %%post

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.01-14m)
- rebuild against gcc43

* Wed Aug  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.01-13m)
- momonganize
- stop service

* Wed Apr  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.01-12m)
- add PreReq: procps (/bin/ps)

* Wed Aug 13 2003 HOSONO Hidetomo <h12o@h12o.org>
- (1.01-11m)
- add BuildPreReq: freecdb (/usr/bin/cdbmake is needed)

* Mon Jan 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.01-10m)
- drop -fomit-frame-pointer from CFLAGS to avoid infinite loop

* Fri Nov 22 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.01-9m)
- correct dbskkd-cdb-1.01-sighup.patch to avoid infinite loop
- modify dbskkd-cdb-1.01-args.patch to use JISHO_FILE 
  if no command line argument is supplied.
- do not send SIGHUP to rpm ;P

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (1.01-8k)
- minagorosi...

* Sat Nov 24 2001 Toru Hoshina <t@kondara.org>
- (1.01-6k)
- needs PreReq xinetd.
- create new patch for SIGHUP behavior.

* Mon Nov 12 2001 Toru Hoshina <t@kondara.org>
- (1.01-4k)
- needs BuildPreReq skk-jisyo.

* Sun Nov  4 2001 Kenta MURATA <muraken2@nifty.com>
- build against Kondara.

* Fri Nov 05 1999 Atsushi Yamagata <yamagata@plathome.co.jp>
- built against RHL-5.2/i386

* Fri Nov 05 1999 Atsushi Yamagata <yamagata@plathome.co.jp>
- 1st release
