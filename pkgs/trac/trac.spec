%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           trac
Version:        0.11.7
Release:        %{momorel}m%{?dist}
Summary:        Enhanced wiki and issue tracking system
Group:          Applications/Internet
License:        Modified BSD
URL:            http://trac.edgewall.com/
Source0:        http://ftp.edgewall.com/pub/trac/Trac-%{version}.tar.gz
Nosource:       0
Source1:        trac.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  sed >= 3.95
Requires:       python >= 2.7
Requires:       python-genshi
Requires:       python-pygments
Requires:       subversion >= 1.1
Requires:       httpd
Requires:       python-sqlite >= 1.0

%description
Trac is an integrated system for managing software projects, an
enhanced wiki, a flexible web-based issue tracker, and an interface to
the Subversion revision control system.  At the core of Trac lies an
integrated wiki and issue/bug database. Using wiki markup, all objects
managed by Trac can directly link to other issues/bug reports, code
changesets, documentation and files.  Around the core lies other
modules, providing additional features and tools to make software
development more streamlined and effective.

%prep
%setup -q -n Trac-%{version}

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT

%{__python} setup.py install --optimize=1 --single-version-externally-managed --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

cat INSTALLED_FILES | sort | uniq | sed /tracd/d > INSTALLED_FILES_RPM

rm -rf contrib/rpm
rm -rf contrib/trac-post-commit-hook.cmd

install -dm 755 $RPM_BUILD_ROOT%{_var}/www/cgi-bin
install cgi-bin/trac.*cgi \
  $RPM_BUILD_ROOT%{_var}/www/cgi-bin

install -Dpm 644 %{SOURCE1} $RPM_BUILD_ROOT/etc/httpd/conf.d/trac.conf.dist
install -dm 755 $RPM_BUILD_ROOT%{_docdir}/trac/contrib
cp -r contrib/* $RPM_BUILD_ROOT%{_docdir}/trac/contrib


install -dm 755 $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT{%{_bindir}/tracd,%{_sbindir}/tracd}
#install -dm 755 $RPM_BUILD_ROOT%{_mandir}/man8
#mv $RPM_BUILD_ROOT%{_mandir}/{man1/tracd.1,man8/tracd.8}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc %{_docdir}/trac/contrib
%config(noreplace) /etc/httpd/conf.d/trac.conf.dist
%{_var}/www/cgi-bin/trac.cgi
%{_var}/www/cgi-bin/trac.fcgi
%{_sbindir}/tracd
%{_bindir}/trac-admin
%{python_sitelib}/trac
%{python_sitelib}/Trac-*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.7-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.7-2m)
- full rebuild for mo7 release

* Wed Mar 31 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.7-1m)
- [SECURITY] http://trac.edgewall.org/wiki/ChangeLog#a0.11.7
- update to 0.11.7

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.6-2m)
- revise for rpm48

* Sun Jan 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.6-1m)
- [SECURITY] CVE-2009-4405
- update to 0.11.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.4-1m)
- update to 0.11.4

* Mon Feb 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.3-1m)
- update to 0.11.3
- License: Modified BSD

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.2-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11.2-2m)
- rebuild against python-2.6.1-1m

* Tue Nov 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2-1m)
- [SECURITY] CVE-2008-5646 CVE-2008-5647
- update to 0.11.2

* Mon Sep 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.11.1-1m)
- version up 0.11.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11b2-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.11b2-1m)
- up to 0.11b2

* Mon May 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.4-1m)
- [SECURITY] CVE-2007-1405
- update 0.10.4

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.3-1m)
- update 0.10.3

* Fri Nov 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- [SECURITY]  SA22789  2006-11-09
- update to 0.10.1

* Wed Aug  2 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.6-1m)
- import from fc extra

* Thu Jul  6 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.6-1
- upstream release 0.9.6

* Tue Apr 18 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.5-1
- bug fix release 0.9.5

* Wed Feb 15 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.4-1
- 0.9.4
 * Deletion of reports has been fixed.
 * Various encoding issues with the timeline RSS feed have been fixed.
 * Fixed a memory leak when syncing with the repository.
 * Milestones in the roadmap are now ordered more intelligently.
 * Fixed bugs: 
   1064, 1150, 2006, 2253, 2324, 2330, 2408, 2430, 2431, 2459, 2544, 
   2459, 2481, 2485, 2536, 2544, 2553, 2580, 2583, 2606, 2613, 2621, 
   2664, 2666, 2680, 2706, 2707, 2735

* Mon Feb 13 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.3-5
- Rebuild for Fedora Extras 5

* Mon Jan 16 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.3-4
- updated trac.conf to allow for trac.*cgi 

* Mon Jan 16 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.3-3
- re-added tracd and trac.fcgi by user request.

* Tue Jan 10 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.3-2
- removed trac.fcgi (bugzilla #174546, comment #11)
- applied patch (bugzilla #174546, attachment id=123008)

* Mon Jan  9 2006 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.3-1
- 0.9.3
- removed tracd (bugzilla #174546, comment #6)
- added trac.conf for httpd
- removed %%{python_sitelib}/trac/test.py
- removed comments

* Tue Dec  6 2005 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.2-2
- added /etc/init.d/tracd
- added /etc/sysconfig/tracd

* Tue Dec  6 2005 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.2-1
- 0.9.2
- fixes SQL Injection Vulnerability in ticket search module.
- fixes broken ticket email notifications.

* Sat Dec  3 2005 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9.1-1
- 0.9.1
- fixes SQL Injection Vulnerability

* Tue Nov 29 2005 Joost Soeterbroek <fedora@soeterbroek.com> - 0.9-1
- Rebuild for Fedora Extras

* Tue Nov  1 2005 Ville Skytta <ville.skytta at iki.fi> - 0.9-1
- 0.9.

* Mon Jun 20 2005 Ville Skytta <ville.skytta at iki.fi> - 0.8.4-0.1
- 0.8.4.
- Move tracd to %%{_sbindir} and man page to section 8.

* Thu Jun 16 2005 Ville Skytta <ville.skytta at iki.fi> - 0.8.3-0.1
- 0.8.3.

* Wed Jun  1 2005 Ville Skytta <ville.skytta at iki.fi> - 0.8.2-0.1
- 0.8.2.

* Sun May 29 2005 Ville Skytta <ville.skytta at iki.fi> - 0.8.1-0.2
- Rebuild for FC4.

* Fri Apr  8 2005 Ville Skytta <ville.skytta at iki.fi> - 0.8.1-0.1
- First build.
