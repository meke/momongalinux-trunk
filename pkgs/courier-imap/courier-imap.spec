%global momorel 4

Summary: Maildir-oriented IMAP4/POP3 server
Name: courier-imap
Version: 4.8.0
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: System Environment/Daemons
URL: http://www.courier-mta.org/imap/
Source0: http://dl.sourceforge.net/sourceforge/courier/courier-imap-%{version}.tar.bz2
NoSource: 0
Patch0: courier-imap.sysvinit.patch
Patch1: courier-imap-4.1.3-nostack.patch
Patch2: courier-imap-4.1.1-pam_service_name.diff
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: courier-authlib-devel >= 0.62.1
BuildRequires: coreutils openssl-devel >= 1.0.0 perl sed rpm >= 4.0.2
BuildRequires: openldap-devel openldap-servers
BuildRequires: gamin-devel
Requires(post): chkconfig coreutils
Requires(preun): chkconfig coreutils
Obsoletes: %{name}-ldap
Obsoletes: %{name}-mysql
Obsoletes: %{name}-pgsql

%define _sysconfdir /etc/courier-imap
%define _localstatedir /var/run
%define _mandir /usr/share/man
%define _datadir /usr/share/courier-imap
%define _prefix /usr/lib/courier-imap
%define initdir /etc/init.d
%define pamconfdir	/etc/pam.d

%description
Courier-IMAP is an IMAP/POP3 server for Maildir mailboxes.  This package
contains the standalone version of the IMAP server that's included in the
Courier mail server package.  This package is a standalone version for use with
other mail servers.  Do not install this package if you intend to install the
full Courier mail server.  Install the Courier package instead.

%prep
%setup -q
%patch0 -p0 -b .sysvinit
%patch1 -p1 -b .nostack
%patch2 -p1 -b .pam_service_name~

%if %(test '%{xflags}' = '%%{xflags}' && echo 1 || echo 0)
%global xflags --enable-unicodes=iso-2022-jp,iso-8859-1,utf-8
%endif

%build

%configure \
	--with-redhat \
	%{?xflags: %{xflags}}

%make
%{__make} check

%install

%{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{pamconfdir}
%{__mkdir_p} %{buildroot}%{initdir}
%{__make} install DESTDIR=%{buildroot}

# Copy standard sysvinit file
install -Dm 744 courier-imap.sysvinit $RPM_BUILD_ROOT/%{initdir}/courier-imap

#
# Fix imapd.dist
#

%{__sed} 's/^IMAPDSTART=.*/IMAPDSTART=YES/' \
	<$RPM_BUILD_ROOT%{_sysconfdir}/imapd.dist \
	>$RPM_BUILD_ROOT%{_sysconfdir}/imapd.dist.tmp

%{__mv} $RPM_BUILD_ROOT%{_sysconfdir}/imapd.dist.tmp \
	$RPM_BUILD_ROOT%{_sysconfdir}/imapd.dist

%{__sed} 's/^IMAPDSSLSTART=.*/IMAPDSSLSTART=YES/' \
	<$RPM_BUILD_ROOT%{_sysconfdir}/imapd-ssl.dist \
	>$RPM_BUILD_ROOT%{_sysconfdir}/imapd-ssl.dist.tmp

%{__mv} $RPM_BUILD_ROOT%{_sysconfdir}/imapd-ssl.dist.tmp \
	$RPM_BUILD_ROOT%{_sysconfdir}/imapd-ssl.dist

%{__chmod} 600 $RPM_BUILD_ROOT%{_sysconfdir}/imapd.dist
%{__chmod} 600 $RPM_BUILD_ROOT%{_sysconfdir}/imapd-ssl.dist

%{__sed} 's/^POP3DSTART=.*/POP3DSTART=YES/' \
	<$RPM_BUILD_ROOT%{_sysconfdir}/pop3d.dist \
	>$RPM_BUILD_ROOT%{_sysconfdir}/pop3d.dist.tmp

%{__mv} $RPM_BUILD_ROOT%{_sysconfdir}/pop3d.dist.tmp \
	$RPM_BUILD_ROOT%{_sysconfdir}/pop3d.dist

%{__sed} 's/^POP3DSSLSTART=.*/POP3DSSLSTART=YES/' \
	<$RPM_BUILD_ROOT%{_sysconfdir}/pop3d-ssl.dist \
	>$RPM_BUILD_ROOT%{_sysconfdir}/pop3d-ssl.dist.tmp

%{__mv} $RPM_BUILD_ROOT%{_sysconfdir}/pop3d-ssl.dist.tmp \
	$RPM_BUILD_ROOT%{_sysconfdir}/pop3d-ssl.dist

%{__chmod} 600 $RPM_BUILD_ROOT%{_sysconfdir}/pop3d.dist
%{__chmod} 600 $RPM_BUILD_ROOT%{_sysconfdir}/pop3d-ssl.dist

%{__cp} imap/README README.imap
%{__cp} imap/README.proxy* .
%{__cp} maildir/README.maildirquota.txt README.maildirquota
%{__cp} maildir/README.sharedfolders.txt README.sharedfolders

####
## Create config files for sysconftool-rpmupgrade (see below)

%{__mkdir_p} $RPM_BUILD_ROOT%{_datadir}
%{__cp} -f sysconftool $RPM_BUILD_ROOT%{_datadir}/sysconftool
%{__chmod} 555 $RPM_BUILD_ROOT%{_datadir}/sysconftool
%{__cat} >$RPM_BUILD_ROOT%{_datadir}/configlist <<EOF
%{_sysconfdir}/imapd.dist
%{_sysconfdir}/imapd-ssl.dist
%{_sysconfdir}/pop3d.dist
%{_sysconfdir}/pop3d-ssl.dist
EOF

%{__chmod} 644 $RPM_BUILD_ROOT%{_datadir}/configlist*

######
##
## Ok, upgrades are going to get ugly.  Because we install $filename.dist
## instead of $filename, and the old package has $filename listed as a config,
## RPM will back up $filename to $filename.rpmsave.  This will happen AFTER
## we will run the post scripts, effectively blowing away our configurations.
## PUT THEM BACK BY RUNNING THE FOLLOWING SCRIPT FROM A TRIGGER.

%{__cat} >$RPM_BUILD_ROOT%{_datadir}/sysconftool-rpmupgrade <<EOF
#!/bin/sh

for f in \$* "."
do
	if test \$f = "."
	then
		continue
	fi

	base=\`echo \$f | sed 's/\\.dist\$//'\`
	if test -f \$base.dist -a ! -f \$base
	then
		%{__cp} -pr \$base.dist \$base
	fi
done
EOF

%{__chmod} 555 $RPM_BUILD_ROOT%{_datadir}/sysconftool-rpmupgrade

%{__mkdir_p} $RPM_BUILD_ROOT%{_localstatedir}
touch $RPM_BUILD_ROOT%{_localstatedir}/imapd.pid
touch $RPM_BUILD_ROOT%{_localstatedir}/imapd-ssl.pid
touch $RPM_BUILD_ROOT%{_localstatedir}/imapd.pid.lock
touch $RPM_BUILD_ROOT%{_localstatedir}/imapd-ssl.pid.lock

touch $RPM_BUILD_ROOT%{_localstatedir}/pop3d.pid
touch $RPM_BUILD_ROOT%{_localstatedir}/pop3d-ssl.pid
touch $RPM_BUILD_ROOT%{_localstatedir}/pop3d.pid.lock
touch $RPM_BUILD_ROOT%{_localstatedir}/pop3d-ssl.pid.lock

### rename pam service name
mv %{buildroot}%{pamconfdir}/imap %{buildroot}%{pamconfdir}/courier-imap
mv %{buildroot}%{pamconfdir}/pop3 %{buildroot}%{pamconfdir}/courier-pop3

%post
/sbin/chkconfig --del courier-imap
/sbin/chkconfig --add courier-imap
%{_datadir}/sysconftool `%{__cat} %{_datadir}/configlist` >/dev/null

%preun
if test "$1" = "0"
then
	rm -f %{_localstatedir}/couriersslcache
	/sbin/chkconfig --del courier-imap
fi

%{_libexecdir}/imapd.rc stop
%{_libexecdir}/imapd-ssl.rc stop
%{_libexecdir}/pop3d.rc stop
%{_libexecdir}/pop3d-ssl.rc stop

%triggerpostun -- courier-imap

test ! -f %{_datadir}/configlist || %{_datadir}/sysconftool-rpmupgrade `%{__cat} %{_datadir}/configlist` >/dev/null

%files
%defattr(-, bin, bin)
%attr(644, root, root) %config(noreplace) %{pamconfdir}/courier-imap
%attr(644, root, root) %config(noreplace) %{pamconfdir}/courier-pop3
%attr(755, bin, bin) %{initdir}/courier-imap
%dir %{_prefix}
%{_libexecdir}
%dir %{_sysconfdir}
%dir %{_sysconfdir}/shared
%dir %{_sysconfdir}/shared.tmp
%config %{_sysconfdir}/imap*
%config %{_sysconfdir}/pop3*
%config %{_sysconfdir}/quotawarnmsg.example
%{_bindir}
%{_sbindir}
%{_mandir}/man1/*
%{_mandir}/man8/*
%{_datadir}

%doc NEWS AUTHORS COPYING imap/BUGS README README.imap README.maildirquota
%doc README.sharedfolders
%doc README.proxy*

%ghost %attr(600, root, root) %{_localstatedir}/imapd.pid
%ghost %attr(600, root, root) %{_localstatedir}/imapd-ssl.pid
%ghost %attr(600, root, root) %{_localstatedir}/imapd.pid.lock
%ghost %attr(600, root, root) %{_localstatedir}/imapd-ssl.pid.lock

%ghost %attr(600, root, root) %{_localstatedir}/pop3d.pid
%ghost %attr(600, root, root) %{_localstatedir}/pop3d-ssl.pid
%ghost %attr(600, root, root) %{_localstatedir}/pop3d.pid.lock
%ghost %attr(600, root, root) %{_localstatedir}/pop3d-ssl.pid.lock

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8.0-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.0-1m)
- update to 4.8.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.0-2m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.0-1m)
- update to 4.7.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.1-1m)
- update to 4.5.1

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.0-1m)
- update to 4.5.0
- %%define __libtoolize :

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-5m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-4m)
- apply Patch2 from Cooker (4.4.1-1) to avoid conflicting with uw-imap

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.1-3m)
- rebuild against courier-authlib-0.62.1
- License: GPLv3+

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-2m)
- rebuild against openssl-0.9.8h-1m

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.1-1m)
- update 4.3.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.3-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.3-2m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.3-1m)
- update to 4.1.3
- rebuilt against pam-0.99.7.1-1m
- Patch2: courier-imap-4.1.3-nostack.patch

* Thu Feb 15 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.1.2-1m)
- up to 4.1.2
- modify directory construction

* Sun Jun 25 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1.1-1m)
- version up 4.1.1

* Wed Jun 06 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.0-3m)
- fix %%build position
- comment out %%attr(755, bin, bin) %config /etc/profile.d/courier-imap.sh
- add iso-2022-jp to xflags

* Sat May 20 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (4.1.0-2m)
- stop daemon

* Fri May 19 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.0-1m)
- import to Momonga
