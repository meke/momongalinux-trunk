%global momorel 10

Name: hesiod
Version: 3.1.0
Release: %{momorel}m%{?dist}
Source: ftp://athena-dist.mit.edu/pub/ATHENA/hesiod/hesiod-%{version}.tar.gz
NoSource: 0
Patch0: hesiod-3.1.0-env.patch
Patch1: hesiod-3.1.0-str.patch
Patch2: hesiod-3.1.0-dnsparse.patch
Patch3: hesiod-3.1.0-libresolv.patch
Patch4: hesiod-3.1.0-perms.patch
Summary: Hesiod libraries and sample programs
Group: System Environment/Libraries
License: MIT/X
BuildRoot: %{_tmppath}/hesiod-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: coreutils
BuildRequires: libtool

%description
Hesiod is a system which uses existing DNS functionality to provide access
to databases of information that changes infrequently.  It is often used to
distribute information kept in the /etc/passwd, /etc/group, and /etc/printcap
files, among others.

%package devel
Summary: Development libraries and headers for Hesiod
Group: Development/Libraries
Requires: hesiod = %{version}-%{release}

%description devel
Hesiod is a system which uses existing DNS functionality to provide access
to databases of information that changes infrequently.  It is often used to
distribute information which might otherwise kept in the /etc/passwd,
/etc/group, and /etc/printcap files over a network, eliminating the need to
ensure synchronize the files among multiple hosts.  This package contains
the header files and libraries required for building programs which use Hesiod.


%prep
%setup -q
%patch0 -p1 -b .env
%patch1 -p1 -b .str
%patch2 -p1 -b .dnsparse
%patch3 -p1 -b .libresolv
%patch4 -p1 -b .perms

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

autoreconf
for manpage in *.3; do
	if grep -q '^\.so man3/hesiod.3' $manpage ; then
		echo .so hesiod.3 > $manpage
	elif grep -q '^\.so man3/hesiod_getmailhost.3' $manpage ; then
		echo .so hesiod_getmailhost.3 > $manpage
	elif grep -q '^\.so man3/hesiod_getpwnam.3' $manpage ; then
		echo .so hesiod_getpwnam.3 > $manpage
	elif grep -q '^\.so man3/hesiod_getservbyname.3' $manpage ; then
		echo .so hesiod_getservbyname.3 > $manpage
	fi
done

%build
%configure 
make LIBTOOL='libtool --tag=CC'

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall LIBTOOL='libtool --tag=CC'
rm -f %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README NEWS
%{_libdir}/libhesiod.so.*
%{_mandir}/man5/*

%files devel
%defattr(-,root,root)
%{_libdir}/libhesiod.a
%{_libdir}/libhesiod.so
%{_includedir}/hesiod.h
%{_mandir}/man3/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-8m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-5m)
- fix build on x86_64

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-4m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-3m)
- update Patch0,2 for fuzz=0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.0-2m)
- rebuild against gcc43

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0 (sync with FC-devel)
- import some patches from FC

* Fri Jul  9 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.2-1m)
- import from FC2

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Sun Oct 19 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- add a %%clean specfile target

* Mon Jun 16 2003 Nalin Dahyabhai <nalin@redhat.com> 3.0.2-28
- rebuild

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jan 27 2003 Nalin Dahyabhai <nalin@redhat.com> 3.0.2-26
- link libhesiod with libresolv on all platforms

* Wed Jan 22 2003 Tim Powers <timp@redhat.com> 3.0.2-25
- rebuilt

* Fri Jan 10 2003 Phil Knirsch <pknirsch@redhat.com> 3.0.2-24
- Fixed wrong .so name for s390/s390x.

* Fri Jan 10 2003 Phil Knirsch <pknirsch@redhat.com> 3.0.2-23
- Build shared lib correctly on s390 and s390x (with gcc -shared -fPIC).

* Wed Sep 24 2002 Nalin Dahyabhai <nalin@redhat.com> 3.0.2-22
- look harder for res_mkquery() in libresolv

* Wed Aug 21 2002 Nalin Dahyabhai <nalin@redhat.com>
- don't choke on large response packets

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun May 26 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 16 2002 Nalin Dahyabhai <nalin@redhat.com> 3.0.2-19
- rebuild in new environment

* Mon Apr 15 2002 Nalin Dahyabhai <nalin@redhat.com> 3.0.2-18
- add missing post/postun calls to ldconfig

* Wed Feb 20 2002 Nalin Dahyabhai <nalin@redhat.com> 3.0.2-17
- rebuild in new environment

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Fri Oct 26 2001 Nalin Dahyabhai <nalin@redhat.com> 3.0.2-15
- actually set the soname in the shared library (ld doesn't automatically
  set the soname to the output file's name, oops)

* Fri Oct  5 2001 Nalin Dahyabhai <nalin@redhat.com> 3.0.2-14
- on second thought, put the shared library back in, using a soversion of 0
  to have a chance at providing compatibility with apps linked dynamically
  on other distributions
- make -devel depend on the same version of the main package

* Wed Oct  3 2001 Nalin Dahyabhai <nalin@redhat.com>
- remove the shared library patch -- different packages with shared libraries
  tend to use different sonames, so we'd run inevitably run into problems

* Thu Aug 21 2001 Nalin Dahyabhai <nalin@redhat.com>
- remove pre and post scripts -- authconfig handles that stuff now
- add the hesiod man page back in, as bind-devel doesn't provide it any more

* Wed Jan 17 2001 Jeremy Katz <jlkatz@eos.ncsu.edu>
- hesiod-devel requires hesiod (bug #128)

* Thu Sep 14 2000 Jeremy Katz <jlkatz@eos.ncsu.edu>
- remove hesiod man page from hesiod-devel as it conflicts with the one 
  from bind-devel

* Thu Sep 14 2000 Jeremy Katz <jlkatz@eos.ncsu.edu>
- use rpm macros where possible and FHS-ify
- split into main and devel packages
- add back requires for nscd

* Fri Jul 28 2000 Jeremy Katz <jlkatz@eos.ncsu.edu>
- rebuild in new environment

* Thu Mar 16 2000 Jeremy Katz <jlkatz@unity.ncsu.edu>
- rebuild in new environment

* Thu Sep  2 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- removed dependency on nscd
- changed requires: nscd back to caching-nameserver

* Mon May 17 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- started changelog
- moved addition of hesiod to nsswitch.conf to this package because we
  no longer use a separate libnss_hesiod.so
- changed requires: caching-nameserver to nscd
- added post-install script snippet to activate nscd on install
