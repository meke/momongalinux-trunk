%global         momorel 18

Name:           perl-MooseX-Types-DateTimeX
Version:        0.10
Release:        %{momorel}m%{?dist}
Summary:        Extensions to MooseX::Types::DateTime::ButMaintained
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Types-DateTimeX/
Source0:        http://www.cpan.org/authors/id/E/EC/ECARROLL/MooseX-Types-DateTimeX-%{version}.tar.gz
NoSource:       0
Patch0:         https://rt.cpan.org/Ticket/Attachment/1028152/536412/MooseX-Types-DateTimeX-0.10-fix_subtypes.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-use-ok >= 0.02
BuildRequires:  perl-Test-Exception >= 0.27
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Moose >= 2.0010-2m
BuildRequires:  perl-MooseX-Types >= 0.27-1m
BuildRequires:  perl-namespace-clean >= 0.20-4m
BuildRequires:  perl-Time-Duration-Parse >= 0.06
BuildRequires:  perl-MooseX-Types-DateTime-ButMaintained >= 0.14
BuildRequires:  perl-DateTimeX-Easy >= 0.089-6m
Requires:       perl-ExtUtils-MakeMaker
Requires:       perl-Test-use-ok >= 0.02
Requires:       perl-Test-Exception >= 0.27
Requires:       perl-Test-Simple
Requires:       perl-Moose >= 0.41
Requires:       perl-MooseX-Types >= 0.04
Requires:       perl-namespace-clean >= 0.08
Requires:       perl-Time-Duration-Parse >= 0.06
Requires:       perl-MooseX-Types-DateTime-ButMaintained >= 0.14
Requires:       perl-DateTimeX-Easy >= 0.085
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module builds on MooseX::Types::DateTime to add additional custom
types and coercions. Since it builds on an existing type, all coercions and
constraints are inherited.

%prep
%setup -q -n MooseX-Types-DateTimeX-%{version}
%patch0 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/MooseX/Types/DateTimeX*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-18m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-17m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-16m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-15m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-14m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-13m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-12m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-11m)
- rebuild against perl-5.16.0

* Sat Feb  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-10m)
- enable to build with perl-MooseX-Types-DateTime-ButMaintained-0.14

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-9m)
- support specopt for test

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-8m)
- rebuild against perl-5.14.2

* Sun Jun 26 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-6m)
- more strict BuildRequires

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.06-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-8m)
- rebuild against perl-5.12.0

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-7m)
- fix typo for rpm48

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-6m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.06-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-3m)
- rebuild against perl-5.10.1

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-2m)
- modify BuildRequires

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
