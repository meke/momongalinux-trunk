# - patch add
# - /etc/init.d/halt add cd eject patch
# - rpm -e rhgb 
#
lang ja_JP.UTF-8
keyboard jp106
timezone Asia/Tokyo
auth --useshadow  --enablemd5
#selinux --enforcing
selinux --disabled
firewall --disabled

#rootpw --iscrypted $1$BYgVel.d$eiK583xp./sBbU3cLpcHn0
#network --device eth0 --bootproto dhcp

xconfig --startxonboot
services --enabled=NetworkManager --disabled=network,sshd

##repo --name=a-dev --baseurl=http://dist.momonga-linux.org/pub/momonga/development/i686/os
repo --name='Momonga Linux 5 - i686 - Base' --baseurl=http://dist.momonga-linux.org/pub/momonga/5/Everything/i686/os/
repo --name='Momonga Linux 5 - i686 - Released Updates' --baseurl=http://dist.momonga-linux.org/pub/momonga/updates/5/i686/

%packages
# basic desktop packages
#@gnome-desktop
#@admin-tools
#@graphical-internet
@base-x
@base
@core

#@admin-tools
#@hardware-support
#@dial-up
#@gnome-desktop

bash
kernel
syslinux
passwd
policycoreutils
chkconfig
authconfig
rootfiles
kbd
vim-minimal
dhclient
netconfig
eject
tree
patch

# japanese langauge
opfc-ModuleHP
VLGothic-fonts*
mplus*
anthy
scim-anthy
jfbterm
lv

fonts-*
#fonts-ISO8859-2
#fonts-ISO8859-2-100dpi
#fonts-ISO8859-2-75dpi
##fonts-KOI8-R
##fonts-KOI8-R-100dpi
##fonts-KOI8-R-75dpi
#fonts-chinese
#fonts-japanese
#fonts-korean

-suikyo*

# lots of people want...
gparted
ntfs-3g
ntfsprogs

# livecd bits to set up the livecd and be able to install
anaconda
anaconda-runtime
momonga-images

###
###  GNOME desktop
###
control-center
#esound
gnome-applets
gnome-panel
gnome-session
gnome-terminal
metacity
nautilus
yelp
at-spi
bug-buddy
dvd+rw-tools
##evince
file-roller
#gcalctool
gedit
#gnome-audio
gnome-backgrounds
#gnome-bluetooth
gnome-media
gnome-netstatus
gnome-power-manager
gnome-screensaver
gnome-system-monitor
gnome-system-tools
gnome-themes
gnome-utils
gnome-vfs2
gnome-volume-manager
#gthumb
gtk2-engines
nautilus-cd-burner
notification-daemon
gnome-icon-theme
gnome-keyring
gnome-menus
gnome-mime-data
hicolor-icon-theme
htmlview
#eel2
gnome-desktop
#gstreamer
#redhat-artwork
fedora-gnome-theme
vte
#beagle
#deskbar-applet
#gamin
#gnome-games
#gnome-doc-utils
#gnome-mag
#gnome-spell

##
## graphical internet
##
firefox
jd
thunderbird
xchat
#gftp

##
## graphic
##
#ImageMagick
#gimp
-graphviz

##
## admin-tools
##
authconfig-gtk
gparted
pirut
system-config-boot
system-config-date
system-config-display
system-config-keyboard
system-config-language
system-config-network
system-config-network-tui
system-config-printer
system-config-printer-libs
system-config-rootpassword
system-config-securitylevel
system-config-securitylevel-tui
system-config-soundcard
system-config-users

#samba-client
screen
#-gimp-print
-ghostscript-*
-cups
-foomatic
-system-config-printer-libs

open-vm-tools
open-vm-tools-gtk

# sound and video
#rhythmbox
#sound-juicer
#totem
##totem-mozilla-plugin
##xine-ui
##xine-skins
##xine-lib-*

#alsa-firmware
alsa-lib
alsa-plugins-pulseaudio
alsa-utils
pulseaudio
pulseaudio-core-libs
pulseaudio-esound-compat
pulseaudio-libs
pulseaudio-module-gconf
pulseaudio-module-x11
pulseaudio-utils
esound-libs
arts-artsc
gstreamer
gstreamer-ffmpeg
gstreamer-plugins-base
gstreamer-plugins-good

# space sucks
-kernel-headers
-specspo
-vino
-redhat-lsb
-sox

-man-pages*
-sdr

-aspell-*
-ccid
-pinfo

-compiz
-vnc-server
-rxvt*
-postfix

-dbus-qt
-qt
-qt3*
-avahi-qt3
#-kde-i18n*
-kdelibs
-kdebase

-OpenEXR
#-facile

-lohit-*
-baekmuk-ttf-fonts-*
-baekmuk-bdf-fonts
-kacst-fonts
-taipeifonts
-cjkunifonts-*
-fonts-punjabi
-fonts-chinese
-fonts-arabic
-fonts-tamil
-fonts-korean
-fonts-hindi
-fonts-oriya
-fonts-telugu
-fonts-gujarati
-fonts-malayalam
-fonts-bengali
-fonts-kannada
-fonts-hebrew 
-fonts-sinhala


#-xorg-x11-xinit-french
#-xorg-x11-xinit-german
#-xorg-x11-xinit-spanish

xorg-x11-*

-rhgb

%end


%post
# FIXME: it'd be better to get this installed from a package
cat > /etc/rc.d/init.d/momonga-live << EOF
#!/bin/bash
#
# live: Init script for live image
#
# chkconfig: 345 00 99
# description: Init script for live image.

. /etc/init.d/functions

if ! strstr "\`cat /proc/cmdline\`" liveimg || [ "\$1" != "start" ] || [ -e /.liveimg-configured ] ; then
    exit 0
fi

exists() {
    which \$1 >/dev/null 2>&1 || return
    \$*
}

touch /.liveimg-configured

# read some variables out of /proc/cmdline
xdriver=""
for o in \`cat /proc/cmdline\` ; do
    case \$o in
    ks=*)
        ks="\${o#ks=}"
        ;;
    xdriver=*)
        xdriver="--set-driver=\${o#xdriver=}"
        ;;
    esac
done

# if liveinst or textinst is given, start anaconda
if strstr "\`cat /proc/cmdline\`" liveinst ; then
   /usr/sbin/liveinst \$ks
fi
if strstr "\`cat /proc/cmdline\`" textinst ; then
   /usr/sbin/liveinst --text \$ks
fi

## mount live image
if [ -b /dev/live ]; then
   mkdir -p /mnt/live
   mount -o ro /dev/live /mnt/live
fi

## enable swaps unless requested otherwise
swaps=\`blkid -t TYPE=swap -o device\`
if ! strstr "\`cat /proc/cmdline\`" noswap -a [ -n "\$swaps" ] ; then
  for s in \$swaps ; do
    action "Enabling swap partition \$s" swapon \$s
  done
fi

# unmute sound card
exists alsaunmute 0 2> /dev/null

# add momonga user with no passwd
useradd -c "Momonga Live" momonga
passwd -d momonga > /dev/null

echo -e "[Desktop]\nSession=gnome\nLanguage=ja_JP.UTF-8" > /home/momonga/.dmrc
chown momonga.momonga /home/momonga/.dmrc
chmod 0600 /home/momonga/.dmrc

mkdir /home/momonga/Desktop -m 0755
chown momonga.momonga /home/momonga/Desktop

# change mail client
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t string /desktop/gnome/url-handlers/mailto/command 'thunderbird %s'  >/dev/null

# disable screensaver locking
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t bool /apps/gnome-screensaver/lock_enabled false >/dev/null

# sound
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t string /system/gstreamer/0.10/default/audiosink 'osssink'  >/dev/null
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t string /system/gstreamer/0.10/default/musicaudiosink 'osssink'  >/dev/null
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t string /system/gstreamer/0.10/default/chataudiosink 'osssink'  >/dev/null
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t string /system/gstreamer/0.10/default/videosink 'osssink'  >/dev/null

# set up timed auto-login for after 60 seconds
##sed -i -e 's/^TimedLoginEnable=false$/#TimedLoginEnable=false/' -e 's/^TimedLogin=$/#TimedLogin=/' -e 's/^TimedLoginDelay=30$/#TimedLoginDelay=30/' /usr/share/gdm/defaults.conf
##sed -i -e 's/\[daemon\]/[daemon]\nTimedLoginEnable=true\nTimedLogin=momonga\nTimedLoginDelay=10/' /usr/share/gdm/defaults.conf

##sed -i -e 's/AutomaticLoginEnable=false/AutomaticLoginEnable=true/' -e 's/AutomaticLogin=/AutomaticLogin=momonga/' /usr/share/gdm/defaults.conf
##sed -i -e 's/\[daemon\]/[daemon]\nAutomaticLoginEnable=true\nAutomaticLogin=momonga/' /etc/X11/gdm/custom.conf

if [ -e /usr/share/icons/hicolor/96x96/apps/momonga-logo-icon.png ] ; then
    cp /usr/share/icons/hicolor/96x96/apps/momonga-logo-icon.png /home/momonga/.face
    chown momonga:momonga /home/momonga/.face
    # TODO: would be nice to get e-d-s to pick this one up too... but how?
fi

# turn off firstboot for livecd boots
echo "RUN_FIRSTBOOT=NO" > /etc/sysconfig/firstboot

# don't start yum-updatesd for livecd boots
if [ -e /etc/init.d/yum-updatesd ] ; then
    chkconfig --level 345 yum-updatesd off
fi

# don't start cron/at as they tend to spawn things which are
# disk intensive that are painful on a live image
chkconfig --level 345 crond off
chkconfig --level 345 atd off
chkconfig --level 345 anacron off

EOF

cat > /etc/rc.d/init.d/momonga-live-xconfig << EOF
#!/bin/bash
#
# live: Init script for live image
#
# chkconfig: 345 99 99
# description: Init script for live image.

. /etc/init.d/functions

exists() {
    which \$1 >/dev/null 2>&1 || return
    \$*
}

# read some variables out of /proc/cmdline
xdriver=""
for o in \`cat /proc/cmdline\` ; do
    case \$o in
    xdriver=*)
        xdriver="--set-driver=\${o#xdriver=}"
        ;;
    esac
done

# configure X, allowing user to override xdriver
exists system-config-display --noui --reconfig --set-depth=24 --output=/etc/X11/xorg.conf \$xdriver

# configure keyboard
#exists system-config-keyboard --text  # <-- kore yaruto hangup suru ... naze?

EOF

chmod 755 /etc/rc.d/init.d/momonga-live
/sbin/restorecon /etc/rc.d/init.d/momonga-live
/sbin/chkconfig --add momonga-live

chmod 755 /etc/rc.d/init.d/momonga-live-xconfig
/sbin/restorecon /etc/rc.d/init.d/momonga-live-xconfig
/sbin/chkconfig --add momonga-live-xconfig

# save a little bit of space at least...
rm -f /boot/initrd*

rm -f /var/lib/rpm/__db.00{1,2,3,4,5,6,7,8.9}
rpm -e --nodeps bind bind-libs bind-utils
rpm -e --nodeps xorg-x11-oclock xorg-x11-xbiff xorg-x11-xcalc xorg-x11-xclock xorg-x11-xeyes xorg-x11-xload 
rpm -e --nodeps xorg-x11-luit
rpm -e --nodeps xorg-x11-x11perf xorg-x11-xclipboard xorg-x11-xconsole xorg-x11-xcursorgen
rpm -e --nodeps xorg-x11-xkill xorg-x11-xlogo xorg-x11-xmag xorg-x11-xmessage xorg-x11-xpr xorg-x11-xwd xorg-x11-xwud
rpm -e --nodeps rhgb

#
# add CD eject patch
#
cd /etc/init.d
cat <<'EOP' | patch
--- halt.org    2008-02-04 21:51:22.000000000 +0900
+++ halt        2008-02-05 00:18:27.000000000 +0900
@@ -62,6 +62,13 @@
        ;;
 esac
 
+# Read in boot parameters
+CMDLINE="`cat /proc/cmdline 2>/dev/null`"
+NOPROMPT=""
+case "$CMDLINE" in *noprompt*) NOPROMPT="yes"; ;; esac
+NOEJECT=""
+case "$CMDLINE" in *noeject*) NOEJECT="yes"; ;; esac
+
 # Kill all processes.
 [ "${BASH+bash}" = bash ] && enable kill
 
@@ -219,4 +226,19 @@
 HALTARGS="-d"
 [ -f /poweroff -o ! -f /halt ] && HALTARGS="$HALTARGS -p"
 
+export NOEJECT
+
+case "$0" in
+    *halt)
+        if [ -z "$NOEJECT" ]; then
+            # Preload "halt" command into memory before it is gone.
+            $command --help >/dev/null 2>&1
+            eject -m -p /cdrom >/dev/null 2>&1 &
+            if [ -z "$NOPROMPT" ]; then
+                read -s -p "Please remove CD, close cdrom drive and hit return [auto 2 minutes]." -t 120 a </dev/console
+            fi
+        fi
+        ;;
+esac
+
 exec $command $HALTARGS
EOP

sed -i -e 's/\[daemon\]/[daemon]\nAutomaticLoginEnable=true\nAutomaticLogin=momonga\nTimedLoginEnable=true\nTimedLogin=momonga\nTimedLoginDelay=0/' /etc/X11/gdm/custom.conf

sed -i -e 's/root	ALL=(ALL) ALL/root	ALL=(ALL) ALL\nmomonga	ALL=(ALL) ALL/' /etc/sudoers

sed -i -e 's/pulse:x:[0-9][0-9][0-9]:/&momonga/' /etc/group
sed -i -e 's/pulse-rt:x:[0-9][0-9][0-9]:/&momonga/' /etc/group
sed -i -e 's/pulse-access:x:[0-9][0-9][0-9]:/&momonga/' /etc/group

%end
