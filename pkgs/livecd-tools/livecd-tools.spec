%global momorel 2

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "import distutils.sysconfig as d; print d.get_python_lib()")}

%define debug_package %{nil}

Summary: Tools for building live CDs
Name: livecd-tools
Version: 16.10
Release: %{momorel}m%{?dist}
Epoch: 1
License: GPLv2
Group: System Environment/Base
URL: http://git.fedorahosted.org/git/livecd
# To make source tar ball:
# git clone git://git.fedorahosted.org/livecd
# cd livecd
# make dist
Source0: %{name}-%{version}.tar.bz2
Source1: makeGNOME
Source40: livecd-Momonga4-minimal.ks
Source41: livecd-Momonga4-desktop-i686.ks
Source42: livedvd-Momonga4-KDE-i686.ks
Source50: livecd-Momonga5-desktop-i686.ks
Source60: livecd-Momonga6-desktop-i686.ks
# Temporary patch until next livecd-tools rollup
Patch0: gzip.patch
Patch10: livecd-tools-024-lang.patch
Patch11: livecd-tools-017-i686.patch
#Patch12: livecd-tools-009-momonga-remount.patch
Patch13: livecd-tools-16.3-dmsetup-out.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python-imgcreate = %{epoch}:%{version}-%{release}
Requires: mkisofs
Requires: isomd5sum
Requires: parted
%ifarch %{ix86} x86_64
Requires: syslinux
%endif
%ifarch ppc
Requires: yaboot
%endif
BuildRequires: python >= 2.7
BuildRequires: perl


%description 
Tools for generating live CDs on Fedora based systems including
derived distributions such as RHEL, CentOS, Momonga and others. See
http://fedoraproject.org/wiki/FedoraLiveCD for more details.

%package -n python-imgcreate
Summary: Python modules for building system images
Group: System Environment/Base
Requires: util-linux
Requires: coreutils
Requires: e2fsprogs
Requires: yum >= 3.2.18
Requires: squashfs-tools
Requires: pykickstart >= 0.96
Requires: dosfstools >= 2.11-2m
Requires: system-config-keyboard >= 1.3.0
Requires: python-urlgrabber
Requires: libselinux-python
Requires: dbus-python

%description -n python-imgcreate
Python modules that can be used for building images for things
like live image or appliances.


%prep
%setup -q
%patch10 -p1 -b .lang
%patch11 -p1 -b .i686
%patch13 -p1 -b .dmsetup

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -m 0644 %{SOURCE1}  %{buildroot}%{_datadir}/livecd-tools
install -m 0644 %{SOURCE40} %{buildroot}%{_datadir}/livecd-tools
install -m 0644 %{SOURCE41} %{buildroot}%{_datadir}/livecd-tools
install -m 0644 %{SOURCE42} %{buildroot}%{_datadir}/livecd-tools
install -m 0644 %{SOURCE50} %{buildroot}%{_datadir}/livecd-tools
install -m 0644 %{SOURCE60} %{buildroot}%{_datadir}/livecd-tools

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README HACKING
%doc config/livecd-fedora-minimal.ks
%{_mandir}/man*/*
%{_bindir}/edit-livecd
%{_bindir}/livecd-creator
%{_bindir}/livecd-iso-to-disk
%{_bindir}/livecd-iso-to-pxeboot
%{_bindir}/image-creator
%{_bindir}/liveimage-mount
%{_bindir}/mkbiarch

%{_datadir}/livecd-tools/makeGNOME
%{_datadir}/livecd-tools/*.ks

%files -n python-imgcreate
%defattr(-,root,root,-)
%doc API
%dir %{python_sitelib}/imgcreate
%{python_sitelib}/imgcreate/*.py
%{python_sitelib}/imgcreate/*.pyo
%{python_sitelib}/imgcreate/*.pyc

%changelog
* Fri Dec 14 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (16.10-2m)
- rebuild against syslinux-5.00-1m

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.10-1m)
- update 16.10

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.8-1m)
- update 16.8

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (16.6-1m)
- update 16.6

* Thu Jun 23 2011 Masaru SANUKI <sanuki@momonga-linux.org>
- (16.3-1m)
- update 16.3
- add Patch13 (sync to output format of dmsetup command)

* Mon May 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (15.7-1m)
- update 15.7

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (15.5-1m)
- update 15.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (033-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (033-2m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (033-1m)
- update to 033

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (031-2m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (024-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (024-1m)
- sync with Fedora 11 (024-1)
-- tz hack is merged upstream

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (017-6m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (017-5m)
- rebuild against python-2.6.1-1m

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (017-4m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2

* Mon Sep 15 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (017-3m)
- add patch1

* Wed Jul 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (017-2m)
- Require squashfs-tools

* Wed May 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (017-1m)
- update 017-1

* Mon May 26 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (015-2m)
- delete *.pyo and *.pyc

* Mon Apr 21 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (015-1m)
- update 015-1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (009-5m)
- rebuild against gcc43

* Sun Feb 10 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (009-3m)
- add remount patch(Patch2)
- update Momonga4 ks(kick start) files

* Tue Jul  3  2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (009-3m)
- add livecd-Momonga-desktop.ks(Source2)

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (009-2m)
- good-bye cdrtools and welcome cdrkit

* Wed Jun  6 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (009-1m)
- update to 009-1

* Thu Apr 26 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (007-1m)
- import from FC
- add Patch0(livecd-tools-007-momonga-i18n.patch)
- add Patch1(livecd-tools-007-localtime.patch)
- add Source1(livecd-Momonga-minimal.ks) and install

* Tue Apr 24 2007 Jeremy Katz <katzj@redhat.com> - 007-1
- Disable prelinking by default
- Disable some things that slow down the live boot substantially
- Lots of tweaks to the default package manifests
- Allow setting the root password (Jeroen van Meeuwen)
- Allow more specific network line setting (Mark McLoughlin)
- Don't pollute the host yum cache (Mark McLoughlin)
- Add support for mediachecking

* Wed Apr  4 2007 Jeremy Katz <katzj@redhat.com> - 006-1
- Many fixes to error handling from Mark McLoughlin
- Add the KDE config
- Add support for prelinking
- Fixes for installing when running from RAM or usb stick
- Add sanity checking to better ensure that USB stick is bootable

* Thu Mar 29 2007 Jeremy Katz <katzj@redhat.com> - 005-3
- have to use excludearch, not exclusivearch

* Thu Mar 29 2007 Jeremy Katz <katzj@redhat.com> - 005-2
- exclusivearch since it only works on x86 and x86_64 for now

* Wed Mar 28 2007 Jeremy Katz <katzj@redhat.com> - 005-1
- some shell quoting fixes
- allow using UUID or LABEL for the fs label of a usb stick
- work with ext2 formated usb stick

* Mon Mar 26 2007 Jeremy Katz <katzj@redhat.com> - 004-1
- add livecd-iso-to-disk for setting up the live CD iso image onto a usb 
  stick or similar

* Fri Mar 23 2007 Jeremy Katz <katzj@redhat.com> - 003-1
- fix remaining reference to run-init

* Thu Mar 22 2007 Jeremy Katz <katzj@redhat.com> - 002-1
- update for new version

* Fri Dec 22 2006 David Zeuthen <davidz@redhat.com> - 001-1%{?dist}
- Initial build.

