lang ja_JP.UTF-8
keyboard jp106
timezone Asia/Tokyo
auth --enableshadow  --enablemd5
#selinux --enforcing
selinux --disabled
firewall --disabled

#network --device eth0 --bootproto dhcp

xconfig --startxonboot
services --enabled=NetworkManager,dhcdbd --disabled=network,sshd,postfix

repo --name=released --baseurl=http://dist.momonga-linux.org/pub/momonga/4/i686/os
repo --name=updates --baseurl=http://dist.momonga-linux.org/pub/momonga/updates/4/i686



%packages
# basic desktop packages
#@gnome-desktop
#@admin-tools
#@graphical-internet
@base-x
@base
@core

bash
kernel
btrfs-progs
nilfs-utils
syslinux
passwd
policycoreutils
chkconfig
authconfig
rootfiles
kbd
vim-minimal
dhclient
netconfig
eject
tree
patch

# japanese langauge
opfc-ModuleHP
anthy
scim-anthy
jfbterm
lv

fonts-*
#fonts-ISO8859-2
#fonts-ISO8859-2-100dpi
#fonts-ISO8859-2-75dpi
##fonts-KOI8-R
##fonts-KOI8-R-100dpi
##fonts-KOI8-R-75dpi
#fonts-chinese
#fonts-japanese
#fonts-korean

-suikyo*

# lots of people want...
gparted
ntfs-3g
ntfsprogs

# livecd bits to set up the livecd and be able to install
anaconda
anaconda-runtime
momonga-images

###
###  GNOME desktop
###
control-center
esound
gnome-applets
gnome-panel
gnome-session
gnome-terminal
metacity
nautilus
yelp
at-spi
bug-buddy
dvd+rw-tools
##evince
file-roller
gcalctool
gedit
gnome-audio
gnome-backgrounds
gnome-bluetooth
gnome-media
gnome-netstatus
gnome-power-manager
gnome-screensaver
gnome-system-monitor
gnome-system-tools
gnome-themes
gnome-utils
gnome-vfs2
gnome-volume-manager
gthumb
gtk2-engines
nautilus-cd-burner
notification-daemon
gnome-icon-theme
gnome-keyring
gnome-menus
gnome-mime-data
hicolor-icon-theme
htmlview
eel2
gnome-desktop
gstreamer
redhat-artwork
vte
#beagle
#deskbar-applet
gamin
gnome-games
gnome-doc-utils
gnome-mag
gnome-spell

##
## graphical internet
##
firefox
jd
thunderbird
xchat
gftp

##
## graphic
##
#ImageMagick
gimp
-graphviz

##
## admin-tools
##
authconfig-gtk
gparted
pirut
system-config-boot
system-config-date
system-config-display
system-config-keyboard
system-config-language
system-config-network
system-config-network-tui
system-config-printer
system-config-printer-libs
system-config-rootpassword
system-config-securitylevel
system-config-securitylevel-tui
system-config-soundcard
system-config-users

#samba-client
screen
-gimp-print
-ghostscript-*
-cups
-foomatic
-system-config-printer-libs

# sound and video
rhythmbox
sound-juicer
totem
totem-mozilla-plugin
##xine-ui
##xine-skins
##xine-lib-*

# space sucks
-specspo
-vino
-redhat-lsb
-sox

-man-pages*
-sdr

-aspell-*
-ccid
-pinfo


-compiz
-vnc-server
-rxvt*
-postfix

-dbus-qt
-qt
-qt4*
-avahi-qt3
-kde-i18n*
-kdelibs
-kdebase

-OpenEXR
-facile

-xorg-x11-xinit-french
-xorg-x11-xinit-german
-xorg-x11-xinit-spanish


%post
# FIXME: it'd be better to get this installed from a package
cat > /etc/rc.d/init.d/momonga-live << EOF
#!/bin/bash
#
# live: Init script for live image
#
# chkconfig: 345 00 99
# description: Init script for live image.

. /etc/init.d/functions

if ! strstr "\`cat /proc/cmdline\`" liveimg || [ "\$1" != "start" ] || [ -e /.liveimg-configured ] ; then
    exit 0
fi

exists() {
    which \$1 >/dev/null 2>&1 || return
    \$*
}

touch /.liveimg-configured

# mount live image
if [ -b /dev/live ]; then
   mkdir -p /mnt/live
   mount -o ro /dev/live /mnt/live
fi

# configure keyboard
exists system-config-keyboard --text

## configure network
#exists system-config-network-tui 

# configure X
exists system-config-display --noui --reconfig --set-depth=24

# unmute sound card
exists alsaunmute 0 2> /dev/null

# add fedora user with no passwd
useradd -c "Momonga Live" momonga
passwd -d momonga > /dev/null

echo -e "[Desktop]\nSession=gnome\nLanguage=ja_JP.UTF-8" > /home/momonga/.dmrc
chown momonga.momonga /home/momonga/.dmrc
chmod 0600 /home/momonga/.dmrc

mkdir /home/momonga/Desktop -m 0755
chown momonga.momonga /home/momonga/Desktop

# change mail client
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t string /desktop/gnome/url-handlers/mailto/command 'thunderbird %s'

# disable screensaver locking
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t bool /apps/gnome-screensaver/lock_enabled false >/dev/null
# set up timed auto-login for after 60 seconds
sed -i -e 's/\[daemon\]/[daemon]\nTimedLoginEnable=true\nTimedLogin=momonga\nTimedLoginDelay=60/' /usr/share/gdm/defaults.conf
if [ -e /usr/share/icons/hicolor/96x96/apps/momonga-logo-icon.png ] ; then
    cp /usr/share/icons/hicolor/96x96/apps/momonga-logo-icon.png /home/momonga/.face
    chown momonga:momonga /home/momonga/.face
    # TODO: would be nice to get e-d-s to pick this one up too... but how?
fi


# turn off firstboot for livecd boots
echo "RUN_FIRSTBOOT=NO" > /etc/sysconfig/firstboot

# don't start yum-updatesd for livecd boots
chkconfig --level 345 yum-updatesd off

# don't start cron/at as they tend to spawn things which are
# disk intensive that are painful on a live image
chkconfig --level 345 crond off
chkconfig --level 345 atd off
chkconfig --level 345 anacron off

# Stopgap fix for RH #217966; should be fixed in HAL instead
touch /media/.hal-mtab
EOF

chmod 755 /etc/rc.d/init.d/momonga-live
/sbin/restorecon /etc/rc.d/init.d/momonga-live
/sbin/chkconfig --add momonga-live

# save a little bit of space at least...
rm -f /boot/initrd*

rpm -e --nodeps bind bind-libs bind-utils
rpm -e --nodeps cvs gettext
rpm -e dejavu-lgc-fonts
#rpm -e --nodeps scrollkeeper
rpm -e --nodeps xorg-x11-oclock xorg-x11-xbiff xorg-x11-xcalc xorg-x11-xclock xorg-x11-xeyes xorg-x11-xload 
rpm -e --nodeps xorg-x11-bdftopcf xorg-x11-fonttosfnt xorg-x11-luit xorg-x11-mkfontdir xorg-x11-mkfontscale 
rpm -e --nodeps xorg-x11-x11perf xorg-x11-xclipboard xorg-x11-xconsole xorg-x11-xcursorgen
rpm -e --nodeps xorg-x11-xkill-1.0.1-1m.mo4 xorg-x11-xlogo-1.0.1-1m.mo4 xorg-x11-xmag-1.0.1-1m.mo4 xorg-x11-xmessage-1.0.1-1m.mo4 xorg-x11-xpr-1.0.2-1m.mo4 xorg-x11-xwd-1.0.1-1m.mo4 xorg-x11-xwud-1.0.1-1m.mo4

rm -f /usr/share/backgrounds/images/mo[23]bg*
rm -f /usr/share/backgrounds/images/mo*800x600.png
rm -f /usr/share/backgrounds/images/mo*1024x768.png
rm -f /usr/share/backgrounds/images/mo*1600x1200.png
rm -f /usr/share/backgrounds/images/mo*1680x1050.png
rm -f /usr/share/backgrounds/images/momonga-chibi-fly.png

#
cd /etc/init.d
cat <<'EOP' | patch
--- halt.org    2008-02-04 21:51:22.000000000 +0900
+++ halt        2008-02-05 00:18:27.000000000 +0900
@@ -62,6 +62,13 @@
        ;;
 esac
 
+# Read in boot parameters
+CMDLINE="`cat /proc/cmdline 2>/dev/null`"
+NOPROMPT=""
+case "$CMDLINE" in *noprompt*) NOPROMPT="yes"; ;; esac
+NOEJECT=""
+case "$CMDLINE" in *noeject*) NOEJECT="yes"; ;; esac
+
 # Kill all processes.
 [ "${BASH+bash}" = bash ] && enable kill
 
@@ -219,4 +226,19 @@
 HALTARGS="-d"
 [ -f /poweroff -o ! -f /halt ] && HALTARGS="$HALTARGS -p"
 
+export NOEJECT
+
+case "$0" in
+    *halt)
+        if [ -z "$NOEJECT" ]; then
+            # Preload "halt" command into memory before it is gone.
+            $command --help >/dev/null 2>&1
+            eject -m -p /cdrom >/dev/null 2>&1 &
+            if [ -z "$NOPROMPT" ]; then
+                read -s -p "Please remove CD, close cdrom drive and hit return [auto 2 minutes]." -t 120 a </dev/console
+            fi
+        fi
+        ;;
+esac
+
 exec $command $HALTARGS
EOP
