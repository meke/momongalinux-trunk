lang ja_JP.UTF-8
keyboard jp106
timezone Asia/Tokyo
auth --enableshadow  --enablemd5
#selinux --enforcing
selinux --disabled
firewall --disabled

# TODO: how to replace i386 with $basearch

# TODO: apparently calling it fedora-dev instead of a-dev makes things
# not work. Perhaps it has something to do with the default repos in
# /etc/yum.repos.d not getting properly disabled?

repo --name=released --baseurl=http://dist.momonga-linux.org/pub/momonga/4/i686/os
repo --name=updates --baseurl=http://dist.momonga-linux.org/pub/momonga/updates/4/i686



%packages
bash
kernel
syslinux
passwd
policycoreutils
chkconfig
authconfig
rootfiles
kbd
eject
tree
patch

libattr
libattr-devel

vim-minimal

%post
cd /etc/init.d
cat <<'EOP' | patch
--- halt.org    2008-02-04 21:51:22.000000000 +0900
+++ halt        2008-02-05 00:18:27.000000000 +0900
@@ -62,6 +62,13 @@
        ;;
 esac
 
+# Read in boot parameters
+CMDLINE="`cat /proc/cmdline 2>/dev/null`"
+NOPROMPT=""
+case "$CMDLINE" in *noprompt*) NOPROMPT="yes"; ;; esac
+NOEJECT=""
+case "$CMDLINE" in *noeject*) NOEJECT="yes"; ;; esac
+
 # Kill all processes.
 [ "${BASH+bash}" = bash ] && enable kill
 
@@ -219,4 +226,19 @@
 HALTARGS="-d"
 [ -f /poweroff -o ! -f /halt ] && HALTARGS="$HALTARGS -p"
 
+export NOEJECT
+
+case "$0" in
+    *halt)
+        if [ -z "$NOEJECT" ]; then
+            # Preload "halt" command into memory before it is gone.
+            $command --help >/dev/null 2>&1
+            eject -m -p /cdrom >/dev/null 2>&1 &
+            if [ -z "$NOPROMPT" ]; then
+                read -s -p "Please remove CD, close cdrom drive and hit return [auto 2 minutes]." -t 120 a </dev/console
+            fi
+        fi
+        ;;
+esac
+
 exec $command $HALTARGS
EOP
