lang ja_JP.UTF-8
keyboard jp106
timezone Asia/Tokyo
auth --enableshadow  --enablemd5
#selinux --enforcing
selinux --disabled
firewall --disabled

#network --device eth0 --bootproto dhcp

xconfig --startxonboot
services --enabled=NetworkManager,dhcdbd --disabled=network,sshd,postfix

repo --name=released --baseurl=http://dist.momonga-linux.org/pub/momonga/4/i686/os
repo --name=updates --baseurl=http://dist.momonga-linux.org/pub/momonga/updates/4/i686


%packages
# KDE desktop packages
@core
@base
@base-x
@dial-up
@admin-tools
@system-tools
#@legacy-software-support
#@hardware-support
@kde-desktop
-bcop
@office
@graphical-internet
@graphics
@sound-and-video
@games
@authoring-and-publishing
@editors
#@engineering-and-scientific
#@education
#@development-tools
#@kde-software-development
#@x-software-development
@ruby

@japanese-support

fonts-*

# lots of people want...
gparted
ntfs-3g
ntfsprogs

# livecd bits to set up the livecd and be able to install
anaconda
anaconda-runtime
momonga-images

-gnome-session


%post
# FIXME: it'd be better to get this installed from a package
cat > /etc/rc.d/init.d/momonga-live << EOF
#!/bin/bash
#
# live: Init script for live image
#
# chkconfig: 345 00 99
# description: Init script for live image.

. /etc/init.d/functions

if ! strstr "\`cat /proc/cmdline\`" liveimg || [ "\$1" != "start" ] || [ -e /.liveimg-configured ] ; then
    exit 0
fi

exists() {
    which \$1 >/dev/null 2>&1 || return
    \$*
}

touch /.liveimg-configured

# mount live image
if [ -b /dev/live ]; then
   mkdir -p /mnt/live
   mount -o ro /dev/live /mnt/live
fi

# configure keyboard
exists system-config-keyboard --text

# configure X
exists system-config-display --noui --reconfig --set-depth=24

# unmute sound card
exists alsaunmute 0 2> /dev/null

# add fedora user with no passwd
useradd -c "Momonga Live" momonga
passwd -d momonga > /dev/null

echo -e "[Desktop]\nSession=kde\nLanguage=ja_JP.UTF-8" > /home/momonga/.dmrc
chown momonga.momonga /home/momonga/.dmrc
chmod 0600 /home/momonga/.dmrc

# disable screensaver locking
gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.defaults -s -t bool /apps/gnome-screensaver/lock_enabled false >/dev/null
# set up timed auto-login for after 60 seconds
sed -i -e 's/\[daemon\]/[daemon]\nTimedLoginEnable=true\nTimedLogin=momonga\nTimedLoginDelay=60/' /usr/share/gdm/defaults.conf
if [ -e /usr/share/icons/hicolor/96x96/apps/momonga-logo-icon.png ] ; then
    cp /usr/share/icons/hicolor/96x96/apps/momonga-logo-icon.png /home/momonga/.face
    chown momonga:momonga /home/momonga/.face
    # TODO: would be nice to get e-d-s to pick this one up too... but how?
fi

# liveinst.desktop move
if [ -e /usr/share/applications/liveinst.desktop ]; then
    sed -i 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop
    cp /usr/share/applications/liveinst.desktop /usr/share/apps/kdesktop/DesktopLinks/
fi
if [ -e /etc/X11/xinit/xinitrc.d/zz-liveinst.sh ]; then
    rm -f /etc/X11/xinit/xinitrc.d/zz-liveinst.sh
fi

# not execute kpersonalizerrc
sed -i -e 's/kpersonalizerrc General FirstLogin true/kpersonalizerrc General FirstLogin false/' /usr/bin/startkde


# turn off firstboot for livecd boots
echo "RUN_FIRSTBOOT=NO" > /etc/sysconfig/firstboot

# don't start yum-updatesd for livecd boots
chkconfig --level 345 yum-updatesd off

# don't start cron/at as they tend to spawn things which are
# disk intensive that are painful on a live image
chkconfig --level 345 crond off
chkconfig --level 345 atd off
chkconfig --level 345 anacron off

# Stopgap fix for RH #217966; should be fixed in HAL instead
touch /media/.hal-mtab
EOF

chmod 755 /etc/rc.d/init.d/momonga-live
/sbin/restorecon /etc/rc.d/init.d/momonga-live
/sbin/chkconfig --add momonga-live

# save a little bit of space at least...
rm -f /boot/initrd*

#
cd /etc/init.d
cat <<'EOP' | patch
--- halt.org    2008-02-04 21:51:22.000000000 +0900
+++ halt        2008-02-05 00:18:27.000000000 +0900
@@ -62,6 +62,13 @@
        ;;
 esac
 
+# Read in boot parameters
+CMDLINE="`cat /proc/cmdline 2>/dev/null`"
+NOPROMPT=""
+case "$CMDLINE" in *noprompt*) NOPROMPT="yes"; ;; esac
+NOEJECT=""
+case "$CMDLINE" in *noeject*) NOEJECT="yes"; ;; esac
+
 # Kill all processes.
 [ "${BASH+bash}" = bash ] && enable kill
 
@@ -219,4 +226,19 @@
 HALTARGS="-d"
 [ -f /poweroff -o ! -f /halt ] && HALTARGS="$HALTARGS -p"
 
+export NOEJECT
+
+case "$0" in
+    *halt)
+        if [ -z "$NOEJECT" ]; then
+            # Preload "halt" command into memory before it is gone.
+            $command --help >/dev/null 2>&1
+            eject -m -p /cdrom >/dev/null 2>&1 &
+            if [ -z "$NOPROMPT" ]; then
+                read -s -p "Please remove CD, close cdrom drive and hit return [auto 2 minutes]." -t 120 a </dev/console
+            fi
+        fi
+        ;;
+esac
+
 exec $command $HALTARGS
EOP
