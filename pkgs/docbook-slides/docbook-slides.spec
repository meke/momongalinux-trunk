%global momorel 9

%define pkg slides
Summary: DocBook Slides document type and stylesheets
Name: docbook-slides
Version: 3.4.0
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/Text
URL: http://sourceforge.net/projects/docbook
Source0: http://dl.sourceforge.net/sourceforge/docbook/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.xml
Source2: %{name}.cat
Source3: %{name}.README.redhat
Patch0: %{name}.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: docbook-dtds
Requires: docbook-xsl
Requires: docbook-simple
Requires: sgml-common
Requires: libxml2


%description
DocBook Slides provides customization layers of the both the
Simplified and the full DocBook XML DTD, as well as the DocBook XSL
Stylesheets. This package contains the XML document type definition
and stylesheets for processing DocBook Slides XML. The slides doctype
and stylesheets are for generating presentations, primarily in HTML.

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT

DESTDIR=$RPM_BUILD_ROOT%{_datadir}/xml/docbook/slides/%{version}
mkdir -p $DESTDIR
cp -a browser $DESTDIR
cp -a graphics $DESTDIR
cp -a schema $DESTDIR
cp -a xsl $DESTDIR
cp -a VERSION $DESTDIR
cp -a catalog.xml $DESTDIR

## Install package catalogs into /etc/*ml/ ##

XML_CAT_DIR=$RPM_BUILD_ROOT%{_sysconfdir}/xml
mkdir -p $XML_CAT_DIR
install -p -m 644 %{SOURCE1} $XML_CAT_DIR

SGML_CAT_DIR=$RPM_BUILD_ROOT%{_sysconfdir}/sgml
mkdir -p $SGML_CAT_DIR
install -p -m 644 %{SOURCE2} $SGML_CAT_DIR

cp -p %{SOURCE3} ./README.fedora

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr (-,root,root)
%doc doc
#%doc tests
%doc README
%doc NEWS
%doc README.fedora
%{_datadir}/xml/docbook/slides/%{version}
%config(noreplace) %{_sysconfdir}/sgml/docbook-slides.cat
%config(noreplace) %{_sysconfdir}/xml/docbook-slides.xml


%post

##################  XML catalog registration #######################

## Define handy variables ##

ROOT_XML_CATALOG=%{_sysconfdir}/xml/catalog
PKG_XML_CATALOG=%{_sysconfdir}/xml/docbook-slides.xml
#LOCAL_XML_CATALOG=%{_datadir}/xml/docbook/slides/3.4.0/catalog.xml

#
# Register it in the super catalog with the appropriate delegates
#
if [ -w $ROOTCATALOG ]
then
        %{_bindir}/xmlcatalog --noout --add "delegatePublic" \
                "-//Norman Walsh//DTD Slides" \
                "file://$PKG_XML_CATALOG" $ROOT_XML_CATALOG

        %{_bindir}/xmlcatalog --noout --add "delegateSystem" \
                "http://docbook.sourceforge.net/release/slides" \
                "file://$PKG_XML_CATALOG" $ROOT_XML_CATALOG
        %{_bindir}/xmlcatalog --noout --add "delegateURI" \
                "http://docbook.sourceforge.net/release/slides" \
                "file://$PKG_XML_CATALOG" $ROOT_XML_CATALOG
fi
####################################################################


#################  SGML catalog registration  ######################

ROOT_SGML_CATALOG=%{_sysconfdir}/sgml/catalog
PKG_SGML_CATALOG=%{_sysconfdir}/sgml/docbook-slides.cat

#### Root SGML Catalog Entries ####
#### "Delegate" appropriate lookups to package catalog ####

############## use install-catalog ######################

if [ -w $ROOT_SGML_CATALOG ]
then
# xmlcatalog deletes OVERRIDE YES directive, use install-catalog instead
#         /usr/bin/xmlcatalog --sgml --noout --add \
# 		"/etc/sgml/docbook-slides.cat"

	install-catalog --add \
	$PKG_SGML_CATALOG \
	$ROOT_SGML_CATALOG 1>/dev/null

# Hack to workaround bug in install-catalog
	sed -i 's/^CATALOG.*log$//g' $PKG_SGML_CATALOG
fi

####################################################################

# Finally, make sure everything in /etc/*ml is readable!
/bin/chmod a+r %{_sysconfdir}/sgml/*
/bin/chmod a+r %{_sysconfdir}/xml/*

%postun
##
## SGML and XML catalogs
##
## Jobs: remove package catalog entries from both root catalogs &
##       remove package catalogs

##### SGML catalogs #####

## Remove package catalog entry from SGML root catalog
%{_bindir}/xmlcatalog --sgml --noout --del \
	%{_sysconfdir}/sgml/catalog \
	"%{_sysconfdir}/sgml/docbook-slides.cat" 
	
## Remove SGML package catalog
rm -f %{_sysconfdir}/sgml/docbook-slides.cat


##### XML catalogs #####

## Remove package catalog entry from XML root catalog
%{_bindir}/xmlcatalog --noout --del \
	"file://%{_sysconfdir}/xml/docbook-slides.xml" \
	%{_sysconfdir}/xml/catalog 

## Remove XML package catalog
rm -f %{_sysconfdir}/sgml/docbook-slides.xml

## Remove dtd directory
rm -rf %{_datadir}/xml/docbook/slides


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.0-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-4m)
- revise for rpm46 (s/Patch/Patch0/)
- License: Modified BSD

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.0-3m)
- rebuild against gcc43

* Thu Mar 20 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0-2m)
- fix a bug which breaks catalogs in /etc/sgml/ down
-- this fix will solve the build failures related to jade and so on

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.0-1m)
- update 3.4.0

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.1-1m)
- import from Fedora

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 3.3.1-2.1.1
- rebuild

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Sep  8 2004 Mark Johnson <mjohnson@redhat.com> 3.3.1-1
- Initial public release
- Moved files to /usr/share/xml
- Added SGML catalog registration
- Fixed catalog.xml, which gets broken by xmlcatalog
- Composed README.fedora

* Mon Feb  2 2004 Tim Waugh <twaugh@redhat.com> 3.3.1-0.1
- 3.3.1.

* Tue Dec 23 2003 Tim Waugh <twaugh@redhat.com> 3.3.0-0.1
- 3.3.0.

* Wed Oct 22 2003 Tim Waugh <twaugh@redhat.com> 3.2.0-0.1
- Initial build.


