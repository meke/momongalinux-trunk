%global momorel 24

# Find a free display (resources generation requires X) and sets XDISPLAY
%define init_xdisplay XDISPLAY=2; while [ $XDISPLAY -lt 13 ]; do if [ ! -f /tmp/.X$XDISPLAY-lock ]; then sleep 2s; ( /usr/bin/Xvfb -ac :$XDISPLAY >& /dev/null & ); sleep 15s; if [ -f /tmp/.X$XDISPLAY-lock ]; then export DISPLAY=:$XDISPLAY; break ; fi; fi; XDISPLAY=$(($XDISPLAY+1)); done; if [ $XDISPLAY -ge 13 ]; then echo No free display found; exit 1; fi
# The virtual X server PID
%define kill_xdisplay kill $(cat /tmp/.X$XDISPLAY-lock)


Name: gdk-pixbuf
Summary: The GdkPixBuf image handling library
Version: 0.22.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.22/%{name}-%{version}.tar.bz2
Nosource: 0
Patch0: gdk-pixbuf-0.22.0-auto.patch
Patch5: gdk-pixbuf-0.22.0-bmp-colormap.patch
Patch6: gdk-pixbuf-0.22.0-ico-width.patch
Patch7: gdk-pixbuf.m4.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool >= 1.5
BuildRequires: automake >= 1.5
BuildRequires: autoconf >= 2.56
BuildRequires: libtiff-devel >= 4.0.1, libjpeg-devel >= 8a
BuildRequires: libpng-devel >= 1.2.2
BuildRequires: gnome-libs-devel >= 1.4.2-12m
BuildRequires: xorg-x11-server-Xvfb

%description
The GdkPixBuf library provides a number of features, including :

- Reference counting for libart's ArtPixBuf structure.
- Image loading facilities.
- Rendering of a GdkPixBuf into various formats:
  drawables (windows, pixmaps), GdkRGB buffers.
- Fast scaling and compositing of pixbufs.
- Simple animation loading (ie. animated gifs)
#'

%package devel
Summary: Libraries and include files for developing GdkPixBuf applications.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gnome-libs-devel
Requires: libtiff-devel, libjpeg-devel, libpng-devel >= 1.2.0

%description devel
Libraries and include files for developing GdkPixBuf applications.


%prep
%setup -q
%patch0 -p1 -b .auto~
%patch5 -p1
%patch6 -p1
%patch7 -p0 -b .m4~

%build

%{init_xdisplay}
export DISPLAY=":$XDISPLAY"

rm -f aclocal.m4 acconfig.h
autoreconf -vfi

%ifarch %{ix86} athlon
%configure  --enable-mmx  --libexecdir=%{_libdir}/gdk-pixbuf/loaders \
    --enable-gtk-doc=no
%else
%configure  --libexecdir=%{_libdir}/gdk-pixbuf/loaders --enable-gtk-doc=no
%endif
%make

%{kill_xdisplay}

%install
rm -rf --preserve-root %{buildroot}
%makeinstall libexecdir=%{buildroot}/%{_libdir}/gdk-pixbuf/loaders

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README TODO doc/*.txt
%{_libdir}/lib*.so.*
%exclude %{_libdir}/lib*.la
%dir %{_libdir}/gdk-pixbuf
%dir %{_libdir}/gdk-pixbuf/loaders
%{_libdir}/gdk-pixbuf/loaders/lib*.so*


%files devel
%defattr(-, root, root)
%{_bindir}/gdk-pixbuf-config
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/*.sh
%{_libdir}/gdk-pixbuf/loaders/lib*a
%{_datadir}/aclocal/*
%{_includedir}/*
%doc %{_datadir}/gnome/html/gdk-pixbuf

%changelog
* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22.0-24m)
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22.0-23m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22.0-22m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.0-21m)
- build fix with libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22.0-20m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-19m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22.0-18m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.22.0-16m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-15m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22.0-14m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (9.22.0-13m)
- delete libtool library

* Fri Apr 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (9.22.0-12m)
- delete document (gtk-doc >= 1.5 cannot create document)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.0-11m)
- revise for xorg-7.0
- revise init_xdisplay macro

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.22.0-10m)
- suppress AC_DEFUN warning.

* Sun Nov 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22.0-9m)
- change BuildRequires: xorg-x11-Xvfb

* Mon Jul  5 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.22.0-8m)
- bug fix %%init_xdisplay

* Sat Jul  3 2004 zunda <zunda at freeshell.org>
- (kossori)
- gives up when no free display found after 10 retries (for non-X machiens)

* Wed Apr 21 2004 Toru Hoshina <t@momonga-linux.org>
- (0.22.0-7m)
- export DISPLAY=:$XDISPLAY

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.22.0-6m)
- rebuild w/o db1

* Tue Mar 16 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.22.0-5m)
- import bugfix patches from rawhide

* Tue Mar 16 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.22.0-4m)
- use Xvfb

* Tue Sep 30 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.22.0-3m)
- rebuild against autoconf-2.57-1m,
  use /usr/bin/auto{conf,header} instead of /usr/bin/auto{conf,header}-2.56

* Sat Jun 21 2003 Kenta MURATA <muraken2@nifty.com>
- (0.22.0-2m)
- use newly version of libtool because old version can not resolve
  inter library dependencies.

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.22.0-1m)
- version 0.22.0

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.21.0-1m)
- version 0.21.0

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.20.0-1m)
- version 0.20.0

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.19.0-1m)
- version 0.19.0

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.18.0-3m)
- add buildprereq: gnome-libs-devel

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.18.0-2k)
- version 0.18.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.17.0-2k)
- version 0.17.0

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.16.0-4k)
- rebuild against libpng 1.2.2.

* Mon Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.16.0-2k)
- version 0.16.0

* Fri Dec 14 2001 Shingo Akagaki <dora@kondara.org>
- (0.14.0-2k)
- version 0.14.0

* Tue Nov 13 2001 Tsutomu Yasuda <tom@kondara.org>
- (0.13.0-4k)
  added configure option 

* Fri Nov  2 2001 Shingo Akagaki <dora@kondara.org>
- (0.13.0-2k)
- version 0.13.0

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (0.11.0-10k)
- rebuild against libpng 1.2.0.

* Thu May 24 2001 Shingo Akagaki <dora@kondara.org>
- enable mmx

* Wed May 23 2001 Shingo Akagaki <dora@kondara.org>
- change spec file

* Sun Apr 19 2001 Shingo Akagaki <dora@kondara.org>
- version 0.11.0

* Thu Apr  5 2001 Shingo Akagaki <dora@Kondara.org>
- fix release

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 0.10.1

* Sun Mar 18 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.9.0-5k)
- modified Docdir with macros

* Fri Sep 1 2000 Shingo Akagaki <dora@kondara.org>
- version 0.9.0

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 0.8.0

* Wed Apr 23 2000 Shingo Akagaki <dora@kondara.org>
- version 0.7.0

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file
- version 0.6.0

* Sun Jan 30 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)

* Sat Jan 22 2000 Ross Golder <rossigee@bigfoot.com>

- Borrowed from gnome-libs to integrate into gdk-pixbuf source tree

