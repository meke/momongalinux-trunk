%global momorel 4

Summary: Fast paced, arcade-style, top-scrolling space shooter
Name: chromium-bsu
Version: 0.9.14.1
Release: %{momorel}m%{?dist}
License: Artistic
Group: Amusements/Games
URL: http://chromium-bsu.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/Chromium%20B.S.U.%20source%20code/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: ftgl-devel >= 2.1.3
BuildRequires: libglpng-devel
BuildRequires: SDL-devel
BuildRequires: openal-soft-devel

%description
You are captain of the cargo ship Chromium B.S.U., responsible for delivering
supplies to our troops on the front line. Your ship has a small fleet of
robotic fighters which you control from the relative safety of the Chromium
vessel.
This is an OpenGL-based shoot them up game with fine graphics.

%prep
%setup -q

%build
%configure
%make

%install
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog NEWS README*
%{_bindir}/chromium-bsu
%{_datadir}/%{name}
%{_datadir}/locale/*/*/%{name}.mo
%{_datadir}/doc/%{name}
%{_mandir}/man6/%{name}.6*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.14.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.14.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.14.1-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.14.1-1m)
- update to 0.9.14.1

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.14-3m)
- rebuild against openal-soft-1.10.622

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.14-1m)
- update to 0.9.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.12-20m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.12-19m)
- rebuild against gcc43

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.9.12-18m)
- use autoconf-2.13 and autoheader-2.12 insted of autoconf-old and autoheader-old

* Wed May 5 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9.12-17m)
- chromium-setup is missed.

* Fri Jun  6 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.12-16m)
- chromium-0.9.12-fix-qt3.patch (devel.ja:01727)

* Thu Apr 10 2003 zunda <zunda at freeshell.org>
- (0.9.12-15m)
- It seems this actually does not require qt2

* Fri Apr 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.12-14m)
- rebuild against zlib 1.1.4-5m
- add BuildPrereq: zlib-devel >= 1.1.4-5m
- use rpm macros

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.12-13m)
- for gcc 3.2

* Fri May 24 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.12-10k)
  for gcc 3.1

* Mon Mar 25 2002 Toru Hoshina <t@kondara.org>
- (0.9.12-8k)
- rebuild against qt2 2.3.1 :-P

* Tue Mar 12 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.12-6k)
- use system zlib

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (0.9.12-4k)
- rebuild against qt 2.3.2.

* Fri Sep  7 2001 Toru Hoshina <t@kondara.org>
- (0.9.12-2k)
- 1st release.
