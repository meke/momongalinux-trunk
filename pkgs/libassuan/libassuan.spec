%global         momorel 1

Name:           libassuan
Summary:        GnuPG IPC library
Version:        2.1.1
Release:        %{momorel}m%{?dist}
License:        LGPLv2+
Source0:        ftp://ftp.gnupg.org/gcrypt/libassuan/libassuan-%{version}.tar.bz2
NoSource:       0
URL:            http://www.gnupg.org/
Group:          System Environment/Libraries
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1:         libassuan-2.0.0-multilib.patch

# -debuginfo useless for (only) static libs
%define         debug_package   %{nil}

BuildRequires:  gawk
BuildRequires:  pth-devel

%description
This is the IPC library used by GnuPG 2, GPGME and a few other
packages.

%package devel 
Summary:        GnuPG IPC library 
Group:          Development/Libraries
Requires:       pth-devel
Requires(post): /sbin/install-info
Requires(postun): /sbin/install-info
Obsoletes:      %{name}-static < %{version}-%{release}
Provides:       %{name}-static = %{version}-%{release}

%description devel 
This is the IPC static library used by GnuPG 2, GPGME and a few other
packages.

This package contains files needed to develop applications using %{name}.

%prep
%setup -q

%patch1 -p1 -b .multilib

%build
#ifarch x86_64
export CFLAGS="%{optflags} -fPIC"
#endif
%configure --enable-static

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

## Unpackaged files
rm -f %{buildroot}%{_infodir}/dir

# delete .la file
rm -rf %{buildroot}%{_libdir}/lib*.la

%check
make check

%post devel 
/sbin/install-info %{_infodir}/assuan.info %{_infodir}/dir &>/dev/null || :

%postun devel 
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/assuan.info %{_infodir}/dir &>/dev/null || :
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING* INSTALL NEWS README* THANKS TODO VERSION
%{_libdir}/lib*.so.*
%{_datadir}/aclocal/*
%{_infodir}/assuan.info*

%files devel
%defattr(-,root,root,-)
%{_bindir}/libassuan-config
%{_includedir}/*
%{_libdir}/lib*.so
%{_libdir}/lib*.a

%changelog
* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Sat Jul 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0 again

* Sun Apr 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-6m)
- roll back to 1.0.5

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-4m)
- Obsoletes: libassuan

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.5-3m)
- sync Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-2m)
- rebuild against rpm-4.6

* Tue May 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- %%NoSource -> NoSource

* Fri Dec 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sun Jul  8 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2
  switch license to GPLv3

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.1-2m)
- add BuildPreReq: pth-devel

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Mon Nov 20 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sun Oct 29 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Thu Oct  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Fri Aug 12 2005 TAKAHASHI Tamotsu <tamo>
- (0.6.10-1m)

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (0.6.9-1m)
- Alter -> Main

* Tue Nov 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.6.8-1m)
- updated to 0.6.8

* Mon Jun 14 2004 TAKAHASHI Tamotsu <tamo>
- (0.6.6-1m)

* Thu Sep 11 2003 TAKAHASHI Tamotsu <tamo>
- (0.6.0-1)
- add THANKS, src/ChangeLog
- install-info assuan.info if $1 is 1

* Fri Sep  5 2003 Robert Schiele <rschiele@uni-mannheim.de>
- initial specfile.
