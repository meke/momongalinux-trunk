%global         momorel 9

Name:           perl-Catalyst-Model-REST
Version:        0.27
Release:        %{momorel}m%{?dist}
Summary:        VERSION
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Model-REST/
Source0:        http://www.cpan.org/authors/id/K/KA/KAARE/Catalyst-Model-REST-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.010
BuildRequires:  perl-Catalyst-Runtime
BuildRequires:  perl-Data-Serializable
BuildRequires:  perl-HTTP-Tiny
BuildRequires:  perl-JSON-XS
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Moose
BuildRequires:  perl-Role-REST-Client
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Try-Tiny
Requires:       perl-Catalyst-Runtime
Requires:       perl-Data-Serializable
Requires:       perl-HTTP-Tiny
Requires:       perl-libwww-perl
Requires:       perl-Moose
Requires:       perl-Role-REST-Client
Requires:       perl-Try-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This model class makes REST connectivety easy.

%prep
%setup -q -n Catalyst-Model-REST-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README TODO
%{perl_vendorlib}/Catalyst/Model/REST.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Tue Oct 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Sun Jun 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-2m)
- rebuild against perl-5.14.1

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.14.0-0.2.1m

* Wed Apr 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15-2m)
- rebuild for new GCC 4.6

* Wed Mar 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sat Feb  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13-2m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sat Oct 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
