%global         momorel 2

Name:           alure
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        Audio Library Tools REloaded
Group:          System Environment/Libraries
# ALURE code is LGPLv2+; note -devel subpackage has its own license tag
License:        LGPLv2+ 
URL:            http://kcat.strangesoft.net/alure.html
Source0:        http://kcat.strangesoft.net/%{name}-releases/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# flac-devel not BRed as build against flac currently fails
BuildRequires:  cmake, libvorbis-devel, libsndfile-devel, openal-soft-devel

%description
ALURE is a utility library to help manage common tasks with OpenAL 
applications. This includes device enumeration and initialization, 
file loading, and streaming.  

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
# Devel doc includes some files under GPLv2+ from NaturalDocs
License:        LGPLv2+ and GPLv2+
Requires:       %{name} = %{version}-%{release}
Requires:       openal-soft-devel
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q 

%build
# disable build against FLAC (currently fails)
%cmake . -DBUILD_STATIC=OFF -DFLAC:BOOL=OFF 
make VERBOSE=1 %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'
# strip installed html doc
rm -rf %{buildroot}%{_docdir}/alure/html

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc docs/html examples
%{_includedir}/AL/%{name}.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-2m)
- release a directory provided by openal-soft-devel

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- import Fedora devel and update to 1.1

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Oct 01 2009 Guido Grazioli <guido.grazioli@gmail.com> - 1.0-4
- Fixed license for -devel subpackage
- Included sample code in -devel subpackage
- Sanitized %%files

* Tue Sep 29 2009 Guido Grazioli <guido.grazioli@gmail.com> - 1.0-3
- Renamed from libalure to alure
- Fixed license

* Mon Sep 28 2009 Guido Grazioli <guido.grazioli@gmail.com> - 1.0-2
- Fix multilib pkgconfig path

* Sat Sep 26 2009 Guido Grazioli <guido.grazioli@gmail.com> - 1.0-1
- Initial packaging 
