%global         momorel 2

Name:           perl-HTML-FormHandler
Version:        0.40056
Release:        %{momorel}m%{?dist}
Summary:        HTML forms using Moose
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/HTML-FormHandler/
Source0:        http://www.cpan.org/authors/id/G/GS/GSHANK/HTML-FormHandler-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-aliased
BuildRequires:  perl-Data-Clone
BuildRequires:  perl-Data-Printer
BuildRequires:  perl-DateTime
BuildRequires:  perl-DateTime-Format-Strptime
BuildRequires:  perl-Email-Valid
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-ShareDir
BuildRequires:  perl-File-ShareDir-Install
BuildRequires:  perl-List-AllUtils
BuildRequires:  perl-Locale-Maketext >= 1.09
BuildRequires:  perl-Moose >= 0.90
BuildRequires:  perl-MooseX-Getopt >= 0.16
BuildRequires:  perl-MooseX-Types >= 0.20
BuildRequires:  perl-MooseX-Types-Common
BuildRequires:  perl-MooseX-Types-LoadableClass
BuildRequires:  perl-namespace-autoclean >= 0.09
BuildRequires:  perl-PathTools
BuildRequires:  perl-Test-Differences
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Try-Tiny
Requires:       perl-aliased
Requires:       perl-Data-Clone
Requires:       perl-Data-Printer
Requires:       perl-DateTime
Requires:       perl-DateTime-Format-Strptime
Requires:       perl-Email-Valid
Requires:       perl-File-ShareDir
Requires:       perl-File-ShareDir-Install
Requires:       perl-GD-SecurityImage
Requires:       perl-List-AllUtils
Requires:       perl-Locale-Maketext >= 1.09
Requires:       perl-Moose >= 0.90
Requires:       perl-MooseX-Getopt >= 0.16
Requires:       perl-MooseX-Types >= 0.20
Requires:       perl-MooseX-Types-Common
Requires:       perl-MooseX-Types-LoadableClass
Requires:       perl-namespace-autoclean >= 0.09
Requires:       perl-PathTools
Requires:       perl-Try-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
*** Although documentation in this file provides some overview, it is
mainly intended for API documentation. See HTML::FormHandler::Manual::Intro
for a more detailed introduction.

%prep
%setup -q -n HTML-FormHandler-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(HTML::FormHandler::Meta::Role)/d'

EOF
%define __perl_requires %{_builddir}/HTML-FormHandler-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test ||:
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README TODO xt
%{perl_vendorlib}/auto/share/dist/HTML-FormHandler
%{perl_vendorlib}/HTML/FormHandler.pm
%{perl_vendorlib}/HTML/FormHandler
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40056-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40056-1m)
- update to 0.40056
- rebuild against perl-5.18.2

* Sat Jan  4 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40055-1m)
- update to 0.40055

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40054-1m)
- update to 0.40054

* Wed Oct 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40053-1m)
- update to 0.40053

* Wed Oct 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40052-1m)
- update to 0.40052

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40051-1m)
- update to 0.40051

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40050-1m)
- update to 0.40050

* Sun Sep 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40028-1m)
- update to 0.40028

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40027-2m)
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40027-1m)
- update to 0.40027

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40026-1m)
- update to 0.40026

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40025-2m)
- rebuild against perl-5.18.0

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40025-1m)
- update to 0.40025

* Thu May  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40024-1m)
- update to 0.40024

* Wed May  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40023-1m)
- update to 0.40023

* Tue Mar 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40022-1m)
- update to 0.40022

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40021-2m)
- rebuild against perl-5.16.3

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40021-1m)
- update to 0.40021

* Mon Feb 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40020-1m)
- update to 0.40020

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40019-1m)
- update to 0.40019

* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40017-1m)
- update to 0.40017

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40016-2m)
- rebuild against perl-5.16.2

* Tue Oct 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40016-1m)
- update to 0.40016

* Mon Oct 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40015-1m)
- update to 0.40015

* Sun Oct 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40014-1m)
- update to 0.40014

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40013-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40013-1m)
- update to 0.40013
- rebuild against perl-5.16.0

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40005-1m)
- update to 0.40005

* Sat Mar 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40004-1m)
- update to 0.40004

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40003-1m)
- update to 0.40003

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40002-1m)
- update to 0.40002

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40001-1m)
- update to 0.40001

* Sun Oct  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35005-1m)
- update to 0.35005

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35004-1m)
- update to 0.35004

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35003-2m)
- rebuild against perl-5.14.2

* Thu Sep  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35003-1m)
- update to 0.35003

* Tue Aug  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35002-1m)
- update to 0.35002

* Fri Jul 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35000-1m)
- update to 0.35000

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34001-2m)
- rebuild against perl-5.14.1

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34001-1m)
- update to 0.34001

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34000-1m)
- update to 0.34000

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33002-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.33002-2m)
- rebuild for new GCC 4.6

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33002-1m)
- update to 0.33002

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.32005-2m)
- rebuild for new GCC 4.5

* Thu Oct 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32005-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
