%global momorel 4

Name: hunspell-fr
Summary: French hunspell dictionaries
Version: 3.5
Release: %{momorel}m%{?dist}
Source: http://www.dicollecte.org/download/fr/hunspell-fr-classique-reforme1990-v3.5.zip
Group: Applications/Text
URL: http://www.dicollecte.org/home.php?prj=fr
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2 or LGPLv2 or MPLv1.1
BuildArch: noarch

Requires: hunspell

%description
French (France, Belgium, etc.) hunspell dictionaries.

%prep
%setup -q -c -n hunspell-fr

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p fr-classique-reforme1990.dic $RPM_BUILD_ROOT/%{_datadir}/myspell/fr_FR.dic
cp -p fr-classique-reforme1990.aff $RPM_BUILD_ROOT/%{_datadir}/myspell/fr_FR.aff

pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
fr_FR_aliases="fr_BE fr_CA fr_CH fr_LU fr_MC"
for lang in $fr_FR_aliases; do
	ln -s fr_FR.aff $lang.aff
	ln -s fr_FR.dic $lang.dic
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_fr.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-1m)
- sync with Fedora 13 (3.5-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.0-1m)
- import from Fedora to Momonga

* Mon Mar 10 2008 Caolan McNamara <caolanm@redhat.com> - 2.2.0-1
- latest version

* Thu Feb 07 2008 Caolan McNamara <caolanm@redhat.com> - 2.1.0-1
- latest version

* Fri Dec 21 2007 Caolan McNamara <caolanm@redhat.com> - 2.0.5-1
- project moved to http://dico.savant.free.fr and a new release

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 0.20060915-2
- clarify license version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20060915-1
- initial version
