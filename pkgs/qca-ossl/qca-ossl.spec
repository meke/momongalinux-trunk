%global momorel 13
%global beta 3

Name:       qca-ossl
Version:    2.0.0
Release:    0.%{beta}.%{momorel}m%{?dist}

Summary:    OpenSSL plugin for the Qt Cryptographic Architecture v2
License:    LGPLv2+
Group:      System Environment/Libraries
URL:        http://delta.affinix.com/qca/
Source0:    http://delta.affinix.com/download/qca/2.0/plugins/qca-ossl-%{version}-beta%{beta}.tar.bz2
NoSource:   0
Patch1:	    qca-ossl-2.0.0-beta3-openssl098i.patch
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: qt-devel >= 4.7.0
BuildRequires: qca2-devel >= 2.0.0
BuildRequires: openssl-devel >= 1.0.0


%description
This is a plugin to provide SSL/TLS capability to programs that use the Qt
Cryptographic Architecture (QCA).  QCA is a library providing an easy API
for several cryptographic algorithms to Qt programs.  This package only
contains the TLS plugin.

%prep
%setup -q -n %{name}-%{version}-beta%{beta}
%patch1 -p1 -b .openssl098j~


%build
unset QTDIR
./configure \
  --no-separate-debug-info \
  --verbose
make %{?_smp_mflags}


%install
rm -rf %{buildroot}

export INSTALL_ROOT=%{buildroot}
mkdir -p %{buildroot}%{_qt4_plugindir}/crypto
make install


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README COPYING
%{_qt4_plugindir}/crypto/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-0.3.13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-0.3.12m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.3.11m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.3.10m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.3.9m)
- rebuild against qt-4.6.3-1m

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.3.8m)
- %%{_qt4_plugindir}/crypto is owned by qt-4.7.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-0.3.7m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-0.3.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-0.3.5m)
- rebuild against openssl-0.9.8k

* Tue Jan 27 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-0.3.4m)
- import gentoo's patch to fix compilation failure with openssl-0.9.8i or later
-- see http://bugs.gentoo.org/show_bug.cgi?format=multiple&id=239449

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-0.3.3m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.3.2m)
- rebuild against openssl-0.9.8h-1m

* Thu May 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-0.3.1m)
- import from Fedora devel

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.0.0-0.4.beta3
- Autorebuild for GCC 4.3

* Thu Dec 13 2007 Aurelien Bompard <abompard@fedoraproject.org> 2.0.0-0.3.beta3
- version 2.0.0 beta 3

* Fri Dec 07 2007 Release Engineering <rel-eng at fedoraproject dot org> - 2.0.0-0.2.beta1
- Rebuild for deps

* Tue Nov 06 2007 Aurelien Bompard <abompard@fedoraproject.org> 2.0.0-0.1.beta1
- version 2.0.0 beta 1

* Sat Oct 27 2007 Aurelien Bompard <abompard@fedoraproject.org> 0.1-4.20070904
- update Source1 URL

* Thu Oct 25 2007 Aurelien Bompard <abompard@fedoraproject.org> 0.1-3.20070904
- update to 20070904

* Thu Sep 13 2007 Aurelien Bompard <abompard@fedoraproject.org> 0.1-2.20070706
- fixes from review in bug 289701 (thanks Rex)

* Thu Sep 13 2007 Aurelien Bompard <abompard@fedoraproject.org> 0.1-1.20070706
- initial package
