%global         momorel 1
%global         date 20140222

Summary:        executing jobs in parallel using one or more computers.
Name:           parallel
Version:        0.%{date}
Release:        %{momorel}m%{?dist}
License:        GPLv3+
Group:          Applications/Text
URL:            http://www.gnu.org/software/parallel/
Source0:        ftp://ftp.gnu.org/pub/gnu/%{name}/%{name}-%{date}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
BuildRequires:  perl-Pod-Html

%description
GNU parallel is a shell tool for executing jobs in parallel using one or more computers.
A job is can be a single command or a small script that has to be run for each of 
the lines in the input. The typical input is a list of files, a list of hosts, 
a list of users, a list of URLs, or a list of tables. A job can also be a command that 
reads from a pipe. GNU parallel can then split the input and pipe it into commands in parallel. 

%prep
%setup -q -n %{name}-%{date}

%build
%configure

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%makeinstall

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING NEWS README
%{_bindir}/*
%{_mandir}/*/*
%{_datadir}/doc/%{name}/

%changelog
* Fri Feb 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20140222-1m)
- update to 20140222

* Tue Sep 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20130822-1m)
- update to 20130822

* Sun Jun 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20130622-1m)
- update to 20130622

* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20130522-1m)
- update to 20130522

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20130422-1m)
- update to 20130422

* Sun Feb 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20130222-1m)
- update 20130222

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20121122-1m)
- update 20121122

* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20120822-1m)
- update 20120822

* Mon Jun 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20120622-1m)
- update 20120622

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20120522-1m)
- update 20120522

* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20111222-1m)
- update 20111222

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20111122-1m)
- update 20111122

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20111022-1m)
- update 20111022

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20110822-1m)
- update 20110822

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20110722-1m)
- update 20110722

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20110622-1m)
- Initial Commit Momonga Linux
