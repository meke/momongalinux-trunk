%global momorel 1
Name:           sushi
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        A quick previewer for Nautilus
Group:          User Interface/Desktops

License:        "GPLv2+ with exceptions"
URL:            http://live.gnome.org/ThreePointOne/Features/FilePreviewing
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  gtksourceview3-devel
BuildRequires:  intltool
BuildRequires:  gjs-devel
BuildRequires:  glib2-devel
BuildRequires:  clutter-devel
BuildRequires:  clutter-gtk-devel
BuildRequires:  clutter-gst2-devel
BuildRequires:  evince-devel
BuildRequires:  gtk3-devel
BuildRequires:  libmusicbrainz5-devel
BuildRequires:  webkitgtk3-devel

Obsoletes:      sushi-devel < 0.5.1

#Description from upstream's README.
%description
This is sushi, a quick previewer for Nautilus, the GNOME desktop
file manager.


%prep
%setup -q


%build
%configure --disable-static
%make 


%install
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'
%find_lang %{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f %{name}.lang
%doc README COPYING AUTHORS NEWS TODO
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/dbus-1/services/*
%{_datadir}/glib-2.0/schemas/org.gnome.sushi.gschema.xml
%{_libexecdir}/*
%{_libdir}/sushi/


%changelog
* Fri Oct  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5-1m)
- update to 0.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-1m)
- reimport from fedora
- update to 0.5.4

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-2m)
- rebuild for evince-3.5.3

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Sat Sep 24 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.92-2m)
- add BuildRequires

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.92-1m)
- update to 0.1.92

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.90-1m)
- initial build

