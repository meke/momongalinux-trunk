%global         momorel 2
%global         srcver 1.11

Name:           perl-SOAP-Lite
Version:        %{srcver}0
Release:        %{momorel}m%{?dist}
Summary:        Perl's Web Services Toolkit
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/SOAP-Lite/
Source0:        http://www.cpan.org/authors/id/P/PH/PHRED/SOAP-Lite-%{srcver}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

%{?include_specopt}
%{?!with_ADDITIONAL_MODULES: %global with_ADDITIONAL_MODULES 1}

BuildRequires:  perl >= 5.006
BuildRequires:  perl-Class-Inspector
BuildRequires:  perl-constant
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO
BuildRequires:  perl-IO-Compress
BuildRequires:  perl-IO-Socket-SSL
BuildRequires:  perl-libnet
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-MIME-Base64
BuildRequires:  perl-MIME-tools
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-URI
BuildRequires:  perl-XML-Parser-Lite
Requires:       perl-Class-Inspector
Requires:       perl-constant
%if 0%{with_ADDITIONAL_MODULES}
Requires:       perl-DIME-Tools
%endif
Requires:       perl-IO
Requires:       perl-IO-Compress
Requires:       perl-IO-Socket-SSL
Requires:       perl-libnet
Requires:       perl-libwww-perl
Requires:       perl-MIME-Base64
Requires:       perl-MIME-tools
%if 0%{with_ADDITIONAL_MODULES}
Requires:       perl-SOAP-Transport-FTP
%endif
Requires:       perl-Scalar-Util
Requires:       perl-Task-Weaken
Requires:       perl-Test-Simple
Requires:       perl-URI
Requires:       perl-XMLRPC-Lite
Requires:       perl-XML-Parser-Lite 
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
## SOAP/Transport/TCP.pm was merged into this package
Obsoletes:      perl-SOAP-Transport-TCP

## if you want to execute test, turn on do_test by specopt
%{?!do_test: %global do_test 1}

%description
SOAP::Lite is a collection of Perl modules which provides a simple and
lightweight interface to the Simple Object Access Protocol (SOAP) both on
client and server side.

%prep
%setup -q -n SOAP-Lite-%{srcver}

%build
%{__perl} Makefile.PL --noprompt INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes HACKING README ReleaseNotes.txt
%{_bindir}/SOAPsh.pl
%{_bindir}/stubmaker.pl
%{perl_vendorlib}/Apache/SOAP.pm
%{perl_vendorlib}/SOAP/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.110-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.110-1m)
- update to 1.11

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.100-1m)
- update to 1.10
- rebuild against perl-5.18.2

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.080-1m)
- update to 1.08

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.070-1m)
- update to 1.07

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.060-1m)
- update to 1.06

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.050-2m)
- Obsoletes perl-SOAP-Transport-TCP

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.050-1m)
- update to 1.05
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.030-1m)
- update to 1.03

* Sat Aug  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.020-1m)
- update to 1.02

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.000-1m)
- update to 1.0

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.716-2m)
- rebuild against perl-5.18.0

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.716-1m)
- update to 0.716

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.715-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.715-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.715-4m)
- rebuild against perl-5.16.1

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.715-3m)
- remove wrong filter...

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.715-2m)
- filter unwanted Requires
- disable tests

* Sun Jul 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.715-1m)
- update to 0.715

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.714-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.714-2m)
- rebuild against perl-5.14.2

* Fri Aug 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.714-1m)
- update to 0.714

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.713-1m)
- update to 0.713

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.712-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.712-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.712-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.712-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.712-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.712-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.712-1m)
- update to 0.712
- Specfile re-generated by cpanspec 1.78.

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.710.10-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.710.10-4m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.710.10-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.710.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.710.10-1m)
- update to 0.710.10

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.710.08-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.710.08-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.710.08-1m)
- update to 0.710.08

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.69-3m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.69-2m)
- use vendor

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.69-1m)
- spec file was autogenerated
