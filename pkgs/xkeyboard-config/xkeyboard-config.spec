%global momorel 1

# INFO: Package contains data-only, no binaries, so no debuginfo is needed
%define debug_package %{nil}

Summary: X Keyboard Extension configuration data
Name: xkeyboard-config
Version: 2.9
Release: %{momorel}m%{?dist}
License: MIT
Group: User Interface/X
URL: http://www.freedesktop.org/wiki/Software/XKeyboardConfig
Source0: http://xorg.freedesktop.org/archive/individual/data/xkeyboard-config/%{name}-%{version}.tar.bz2
NoSource: 0
BuildArch: noarch

BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros
BuildRequires: xorg-x11-xkbcomp
BuildRequires: perl(XML::Parser)
BuildRequires: intltool
BuildRequires: gettext
BuildRequires: automake autoconf libtool pkgconfig
BuildRequires: glib2-devel
BuildRequires: xorg-x11-proto-devel libX11-devel
BuildRequires: libxslt

%description
This package contains configuration data used by the X Keyboard Extension 
(XKB), which allows selection of keyboard layouts when using a graphical 
interface. 

%package devel
Summary: Development files for %{name}
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
%{name} development package

%prep
%setup -q

%build
AUTOPOINT="intltoolize --automake --copy" autoreconf -v --force --install || exit 1
%configure \
    --enable-compat-rules \
    --with-xkb-base=%{_datadir}/X11/xkb \
    --disable-xkbcomp-symlink \
    --with-xkb-rules-symlink=xorg

%make

%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

# Remove unnecessary symlink
rm -f $RPM_BUILD_ROOT%{_datadir}/X11/xkb/compiled
%find_lang %{name} 

# Create filelist
{
   FILESLIST=${PWD}/files.list
   pushd $RPM_BUILD_ROOT
   find .%{_datadir}/X11/xkb -type d | sed -e "s/^\./%dir /g" > $FILESLIST
   find .%{_datadir}/X11/xkb -type f | sed -e "s/^\.//g" >> $FILESLIST
   popd
}

%files -f files.list -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS README NEWS TODO COPYING CREDITS docs/README.* docs/HOWTO.*
%{_datadir}/X11/xkb/rules/xorg
%{_datadir}/X11/xkb/rules/xorg.lst
%{_datadir}/X11/xkb/rules/xorg.xml
%{_mandir}/man7/xkeyboard-config.*

%files devel
%defattr(-,root,root,-)
%{_datadir}/pkgconfig/xkeyboard-config.pc

%changelog
* Thu Jul 18 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.9-1m)
- update to 2.9
- revise spec

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-2m)
- reimport from fedora

* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Thu Mar  8 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Sat Jan 21 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5-1m)
- update to 2.5
-- fix high visibility security issue 

* Wed Oct  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3-2m)
- generate man

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3-1m)
- update to 2.3

* Sat May 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2-1m)
- update 2.2

* Sat Feb  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1-1m)
- update to 2.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-2m)
- rebuild for new GCC 4.5

* Fri Nov  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-3m)
- update to 2.0
- [BUG FIX] https://bugzilla.gnome.org/show_bug.cgi?id=633599
-- add patch0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9-2m)
- full rebuild for mo7 release

* Fri Jun 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9-1m)
- update 1.9

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8-1m)
- update 1.8

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-3m)
- does not conflict with xorg-x11-xkbdata, this package obsoletes xorg-x11-xkbdata

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-1m)
- sync with Fedora 11 (1.5-6)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against rpm-4.6

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-1m)
- update 1.4

* Sun Sep 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3-2m)
- update 20080919

* Sat Jul 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-1m)
- sync with Fedora devel (1.3-1)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8-5m)
- rebuild against gcc43

* Mon Sep 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.8-4m)
- add BuildRequires: xorg-x11-xkbcomp

* Sun Jul 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-3m)
- add Prereq: coreutils

* Wed Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-2m)
- add %%pre for smooth upgrading

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8-1m)
- import from f7 to Momonga

* Fri Sep  1 2006 Alexander Larsson <alexl@redhat.com> - 0.8-7
- Update macbook patch to be closer to what got in upstream
- (kp enter is ralt, not the option key)

* Fri Sep  1 2006 Matthias Clasen <mclasen@redhat.com> - 0.8-6
- Add support for Korean 106 key keyboards (204158)

* Tue Aug 29 2006 Alexander Larsson <alexl@redhat.com> - 0.8-5
- Add MacBook model and geometry, plus alt_win option

* Thu Aug 22 2006 Matthias Clasen <mclasen@redhat.com> 0.8-4
- Fix geometry description for Thinkpads
- Add a Kinesis model
- Add Dell Precision M65 geometry and model

* Tue Aug 22 2006 Adam Jackson <ajackson@redhat.com> 0.8-3
- Add Compose semantics to right Alt when that's ISO_Level3_Shift (#193922)

* Fri Jul 07 2006 Mike A. Harris <mharris@redhat.com> 0.8-2
- Rename spec file from xorg-x11-xkbdata to xkeyboard-config.spec

* Fri Jul 07 2006 Mike A. Harris <mharris@redhat.com> 0.8-1
- Renamed package from 'xorg-x11-xkbdata' to 'xkeyboard-config' to match the
  upstream project name and tarball.  I kept the rpm changelog intact however
  to preserve history, so all entries older than today, are from the
  prior 'xorg-x11-xkbdata' package.  (#196229,197939)
- Added "Obsoletes: xorg-x11-xkbdata"
- Removed 'pre' script from spec file, as that was a temporary hack to help
  transition from modular X.Org xkbdata to modular xkeyboard-config during
  FC5 development.  The issue it resolved is not present in any officially
  released distribution release or updates, so the hack is no longer needed.

* Wed Jun 21 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-8.xkbc0.8.0
- Embed xkeyboard-config version in Release field so we can tell from the
  filename what is really in this package without having to look in the
  spec file.  We should rename the package to xkeyboard-config and restart
  the versioning.

* Tue Jun 06 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-8
- Added "BuildRequires: perl(XML::Parser)" for (#194188)

* Sat Mar 04 2006 Ray Strode <rstrode@redhat.com> 1.0.1-7
- Update to 0.8.

* Wed Mar 01 2006 Ray Strode <rstrode@redhat.com> 1.0.1-6
- Turn on compat symlink (bug 183521)

* Tue Feb 28 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-5
- Fixed rpm pre script upgrade/install testing
- Rebuild package as 1.0.1-5 in rawhide, completing the transition to
  xkeyboard-config.

* Tue Feb 28 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-4.0.7.xkbcfg.5
- Added rpm pre script, to pre-remove the symbols/pc during package upgrades,
  to avoid an rpm cpio error if the X11R7.0 modular xkbdata package is already
  installed, because rpm can not replace a directory with a file.  

* Fri Feb 24 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-4.0.7.xkbcfg.1
- Package renamed to xorg-x11-xkbdata and version/release tweaked since it
  is too late to add new package names to Fedora Core 5 development.
- Added "Provides: xkeyboard-config" virtual provide.

* Fri Feb 24 2006 Mike A. Harris <mharris@redhat.com> 0.7-1
- Initial package created with xkeyboard-config-0.7.

* Tue Feb 21 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-3
- Added xkbdata-1.0.1-greek-fix-bug181313.patch to fix (#181313,181313)
- Added xkbdata-1.0.1-cz-fix-bug177362.patch to fix (#177362,178892)

* Thu Feb 09 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-2
- Added xkbdata-1.0.1-sysreq-fix-bug175661.patch to fix (#175661)

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated to xbitmaps 1.0.1 from X11R7.0

* Sat Dec 17 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated to xbitmaps 1.0.0 from X11R7 RC4.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2
- Add a few missing rpm 'dir' directives to file manifest.
- Bump release, and build as a 'noarch' package.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated to xkbdata 0.99.1 from X11R7 RC2.
