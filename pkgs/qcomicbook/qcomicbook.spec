%global         momorel 2
%global         qtver 4.8.2
%global         cmakever 2.6.4
%global         cmakerel 1m

Summary:        A viewer for comic book archives
Name:           qcomicbook
Version:        0.8.1
Release:        %{momorel}m%{?dist}
License:        GPLv2
Group:          Amusements/Graphics
URL:            http://qcomicbook.linux-projects.net/
Source0:        http://qcomicbook.linux-projects.net/releases/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-0.5.0-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root%(%{__id_u} -n)
Requires:       qt >= %{qtver}
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  desktop-file-utils
# BuildRequires:  imlib2-devel
BuildRequires:  libXmu-devel
BuildRequires:  libXi-devel
BuildRequires:  poppler-qt4-devel >= 0.20.1

%description
QComicBook is a viewer for comic book archives: rar, cbr,
zip, cbz, ace, cba,tar.gz, cbg, tar.bz2, cbb. QComicBook 
aims at convenience and simplicity. Features include: 
automatic decompression, full-screen mode, two pages mode, 
japanese mode, thumbnails view, page scaling and rotating, 
page preloading and caching, mouse or keyboard navigation, 
bookmarks etc.

QComicBook requires zip/unzip, rar/unrar, tar with 
gzip+bzip2 support and unace to handle archives.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja

%build
PATH="%{_qt4_bindir}:$PATH" %{cmake} .

make %{?_smp_mflags}

%install
rm -rf %{buildoot}
make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{_datadir}/{applications,pixmaps}
cp -a data/%{name}.png %{buildroot}%{_datadir}/pixmaps

desktop-file-install --vendor=""                           \
        --dir %{buildroot}%{_datadir}/applications         \
        data/%{name}.desktop

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS README ChangeLog COPYING THANKS TODO
%docdir %{_datadir}/%{name}/help
%{_bindir}/%{name}
%{_mandir}/man1/*
%{_datadir}/%{name}
%{_datadir}/applications/*%{name}.desktop
%{_datadir}/pixmaps/%{name}*

%changelog
* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-2m)
- rebuild against poppler-0.20.1

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Wed Jul  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-2m)
- rebuild for new GCC 4.6

* Tue Dec 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-2m)
- specify PATH for Qt4

* Wed Nov 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-4m)
- fix build failure

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-4m)
- rebuild against qt-4.6.3-1m

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-3m)
- fix up desktop file

* Thu Jan 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-2m)
- change primary site URI and source URI

* Mon Nov 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- import from Fedora devel and update to 0.5.0

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep  3 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.4.0-3
- fix license tag

* Tue Apr 01 2008 Scott Baker <scott@perturb.org> - 0.4.0-2
- QT4 -> QT convesions (WOOHOO KDE4)

* Wed Nov 07 2007 Scott Baker <scott@perturb.org> - 0.4.0-1
- Added the requirment for QT4

* Wed Nov 07 2007 Scott Baker <scott@perturb.org> - 0.4.0-1
- Update to the latest version and drop the imlib dependency

* Thu Nov 11 2006 Scott Baker <scott@perturb.org> - 0.3.4-1
- Update to the latest version

* Mon Oct 09 2006 Scott Baker <scott@perturb.org> - 0.3.3-5
- Build on FC6

* Tue Sep 20 2006 Scott Baker <scott@perturb.org> - 0.3.3-2
- Bumped release to 0.3.3

* Sun Sep 03 2006 Scott Baker <scott@perturb.org> - 0.3.2-6
- Fixed building for SMP arches

* Thu Aug 31 2006 Scott Baker <scott@perturb.org> - 0.3.2-5
- Removed the BuildArch

* Mon Aug 28 2006 Scott Baker <scott@perturb.org> - 0.3.2-4
- Updated how the .desktop file is handled
- Updated the make to include SMP options
- Remove bogus "requires"
- Update the rm -rf in clean and install

* Mon Aug 28 2006 Scott Baker <scott@perturb.org> - 0.3.2-3
- Removed requirement for unrar since it's not available via Fedora

* Sun Aug 27 2006 Scott Baker <scott@perturb.org> - 0.3.2-2
- Begin packaging for Fedora Extras.
