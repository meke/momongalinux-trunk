%global momorel 1

%global plugins down-root auth-pam

Summary: A Secure UDP Tunneling Daemon
Name: openvpn
Version: 2.2.2
Release: %{momorel}m%{?dist}
URL: http://openvpn.net/
Source0: http://swupdate.openvpn.net/community/releases/%{name}-%{version}.tar.gz
NoSource: 0
# Sample 2.0 config files
Source2:           roadwarrior-server.conf
Source3:           roadwarrior-client.conf
# Don't start openvpn by default. 
Patch0:            openvpn-init.patch
Patch1:            openvpn-script-security.patch
Patch2:            openvpn-2.1.1-init.patch
Patch3:            openvpn-2.1.1-initinfo.patch
# Systemd service
Source4:           openvpn.service
# Tmpfile.d config
Source5:           %{name}-tmpfile.conf

License: "GPLv2 with exceptions"
Group: Applications/Internet
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: lzo-devel >= 2.01, openssl-devel >= 1.0.0
BuildRequires: pam-devel iproute pkcs11-helper-devel
BuildRequires: systemd-units
Requires: udev, iproute, net-tools
Requires(pre): shadow-utils
Requires(post):    systemd-sysv
Requires(post):    systemd-units
Requires(preun):   systemd-units
Requires(postun):  systemd-units

# Filter out the perl(Authen::PAM) dependency.
# No perl dependency is really needed at all.
%define __perl_requires sh -c 'cat > /dev/null'

%description
OpenVPN is a robust and highly flexible tunneling application that
uses all of the encryption, authentication, and certification features
of the OpenSSL library to securely tunnel IP networks over a single
UDP port. It can use the Marcus Franz Xaver Johannes Oberhumer's LZO
library for compression.

%define debug_package %{nil}

%prep
%setup -q
%patch0 -p0
%patch1 -p1
%patch2 -p0
%patch3 -p0

sed -i -e 's,%{_datadir}/openvpn/plugin,%{_libdir}/openvpn/plugin,' openvpn.8

# %%doc items shouldn't be executable.
find contrib sample-config-files sample-keys sample-scripts -type f -perm +100 \
    -exec chmod a-x {} \;

%build
%configure \
    --enable-pthread \
    --enable-password-save \
    --enable-iproute2 \
    --with-ifconfig-path=/sbin/ifconfig \
    --with-iproute-path=/sbin/ip \
    --with-route-path=/sbin/route \
    --with-lzo-headers=%{_includedir}/lzo
%make

# Build plugins
for plugin in %{plugins} ; do
    %{__make} -C plugin/$plugin
done

%check
# Test Crypto:
./openvpn --genkey --secret key
./openvpn --test-crypto --secret key

# Randomize ports for tests to avoid conflicts on the build servers.
cport=$[ 50000 + ($RANDOM % 15534) ]
sport=$[ $cport + 1 ]
sed -e 's/^\(rport\) .*$/\1 '$sport'/' \
    -e 's/^\(lport\) .*$/\1 '$cport'/' \
    < sample-config-files/loopback-client \
    > %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-client
sed -e 's/^\(rport\) .*$/\1 '$cport'/' \
    -e 's/^\(lport\) .*$/\1 '$sport'/' \
    < sample-config-files/loopback-server \
    > %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-server

# Test SSL/TLS negotiations (runs for 2 minutes):
./openvpn --config \
    %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-client &
./openvpn --config \
    %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-server
wait

rm -f %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-client \
    %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})-loopback-server

%install
[ %{buildroot} != "/" ] && rm -rf %{buildroot}

install -D -m 0644 %{name}.8 $RPM_BUILD_ROOT%{_mandir}/man8/%{name}.8
install -D -m 0755 %{name} $RPM_BUILD_ROOT%{_sbindir}/%{name}
install -d -m 0755 $RPM_BUILD_ROOT%{_sysconfdir}/%{name}

mkdir -p %{buildroot}%{_unitdir}
install -D -m 0755 %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/openvpn@.service

rm -rf $RPM_BUILD_ROOT%{_initscriptdir}

mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -pR easy-rsa $RPM_BUILD_ROOT%{_datadir}/%{name}/
rm -rf $RPM_BUILD_ROOT%{_datadir}/%{name}/easy-rsa/Windows
cp %{SOURCE2} %{SOURCE3} sample-config-files/

mkdir -p $RPM_BUILD_ROOT%{_libdir}/%{name}/plugin/lib
for plugin in %{plugins} ; do
    install -m 0755 plugin/$plugin/openvpn-$plugin.so \
        $RPM_BUILD_ROOT%{_libdir}/%{name}/plugin/lib/openvpn-$plugin.so
    cp plugin/$plugin/README plugin/$plugin.txt
done

# tmpfiles.d
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d
install -m 0644 %{SOURCE5} %{buildroot}%{_sysconfdir}/tmpfiles.d/%{name}.conf
mkdir -p %{buildroot}%{_localstatedir}/run/
install -d -m 0710 %{buildroot}%{_localstatedir}/run/%{name}/

%clean
[ %{buildroot} != "/" ] && rm -rf %{buildroot}

%pre
getent group openvpn &>/dev/null || groupadd -r openvpn
getent passwd openvpn &>/dev/null || \
    /usr/sbin/useradd -r -g openvpn -s /sbin/nologin -c OpenVPN \
        -d /etc/openvpn openvpn

%post
if [ $1 -eq 1 ] ; then 
  # Initial installation 
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable openvpn.service > /dev/null 2>&1 || :
  /bin/systemctl stop openvpn.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
# Normally, we'd try a restart here, but in this case, it could be troublesome.

%triggerun -- openvpn < 2.2.1-2m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply openvpn
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save openvpn >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del openvpn >/dev/null 2>&1 || :
/bin/systemctl try-restart openvpn.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYRIGHT.GPL INSTALL PORTS README 
%doc plugin/*.txt
%doc contrib sample-config-files sample-keys sample-scripts
%{_mandir}/man8/%{name}.8*
%{_sbindir}/%{name}
%{_datadir}/%{name}
%{_libdir}/%{name}
%{_unitdir}/%{name}@.service
%dir %{_localstatedir}/run/%{name}/
%config(noreplace) %{_sysconfdir}/tmpfiles.d/%{name}.conf
%config %dir %{_sysconfdir}/%{name}

%changelog
* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.2-1m)
- update 2.2.2
- Requires modefied. dev to udev

* Thu Sep 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.1-2m)
- support systemd

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.1-1m)
- update 2.2.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.4-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.4-1m)
- update to 2.1.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-2m)
- full rebuild for mo7 release

* Mon May 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-1m)
- update 2.1.1

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-0.26.3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-0.26.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-0.26.1m)
- update to 2.1-rc20 based on Rawhide (2.1-0.36.rc20)
-- http://pc11.2ch.net/test/read.cgi/linux/1188293074/634-635n

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-0.25.3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-0.25.2m)
- rebuild against rpm-4.6

* Wed Aug  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-0.25.1m)
- [SECURITY] CVE-2008-3459
- version up 2.1-rc9

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-0.14.5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-0.14.4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-0.14.3m)
- %%NoSource -> NoSource

* Mon Jul 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1-0.14.2m)
- add Requires: chkconfig

* Sun May 28 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1-0.14.1m)
- version up 2.1-beta14

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.6-1m)
- version up
- rebuild against openssl-0.9.8a

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.5-1m)
- up to 2.0.5
- [SECURITY] CVE-2005-3393, CVE-2005-3409, 

* Wed Aug 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.1-1m)
- up to 2.0.1
- [SECURITY] CAN-2005-2531 CAN-2005-2532 CAN-2005-2533 CAN-2005-2534
- build with plugins

* Thu Jul 21 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0-3m)
  rebuild against lzo-2.01

* Sun Jun 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.0-2m)
- rebuild against lzo-2.00

* Sat Jun 11 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0-1m)
- bug fix and support multi mode

* Thu Feb 17 2005 Toru Hoshina <t@momonga-linux.org>
- (1.6.0-2m)
- do not start/stop at %%post/%%postun.

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.0-1m)
- initial import to Momonga
