# Generated from ruby_parser-2.3.1.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname ruby_parser

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: ruby_parser (RP) is a ruby parser written in pure ruby (utilizing racc--which does by default use a C extension)
Name: rubygem-%{gemname}
Version: 2.3.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: https://github.com/seattlerb/ruby_parser
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(sexp_processor) => 3.0
Requires: rubygem(sexp_processor) < 4
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
ruby_parser (RP) is a ruby parser written in pure ruby (utilizing
racc--which does by default use a C extension). RP's output is
the same as ParseTree's output: s-expressions using ruby's arrays and
base types.
As an example:
def conditional1(arg1)
if arg1 == 0 then
return 1
end
return 0
end
becomes:
s(:defn, :conditional1,
s(:args, :arg1),
s(:scope,
s(:block,
s(:if,
s(:call, s(:lvar, :arg1), :==, s(:arglist, s(:lit, 0))),
s(:return, s(:lit, 1)),
nil),
s(:return, s(:lit, 0)))))


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

sed -i -e 's|/usr/local/bin|/usr/bin|' %{buildroot}%{geminstdir}/test/*.rb

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/ruby_parse
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-1m)
- update 2.3.1

* Tue Sep 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-1m)
- update 2.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.5-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.5-1m)
- update 2.0.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-2m)
- full rebuild for mo7 release

* Sat Jan 02 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-1m)
- Initial package for Momonga Linux
