%global         momorel 1

Name:           perl-Test-TCP
Version:        2.05
Release:        %{momorel}m%{?dist}
Summary:        Testing TCP program
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Test-TCP/
Source0:        http://www.cpan.org/authors/id/T/TO/TOKUHIROM/Test-TCP-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008001
BuildRequires:  perl-ExtUtils-MakeMaker >= 6.64
BuildRequires:  perl-File-Temp
BuildRequires:  perl-IO
BuildRequires:  perl-Socket
BuildRequires:  perl-Test-Simple >= 0.98
BuildRequires:  perl-Test-SharedFork >= 0.19
BuildRequires:  perl-Time-HiRes
Requires:       perl-IO
Requires:       perl-Test-Simple
Requires:       perl-Test-SharedFork >= 0.19
Requires:       perl-Time-HiRes
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Test::TCP is test utilities for TCP/IP programs.

%prep
%setup -q -n Test-TCP-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes cpanfile LICENSE META.json META.json README.md
%{perl_vendorlib}/Net/EmptyPort.pm
%{perl_vendorlib}/Test/TCP*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-1m)
- rebuild against perl-5.20.0
- update to 2.05

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-2m)
- rebuild against perl-5.18.2

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02

* Sun Sep 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-2m)
- rebuild against perl-5.18.1

* Thu Jun 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-1m)
- update to 2.00

* Mon Jun  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Wed May 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-2m)
- rebuild against perl-5.18.0

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-2m)
- rebuild against perl-5.16.3

* Sun Mar  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-2m)
- rebuild against perl-5.16.2

* Thu Oct 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-2m)
- rebuild against perl-5.16.1

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Wed Nov 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-2m)
- rebuild against perl-5.14.1

* Wed Jun  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-2m)
- rebuild for new GCC 4.6

* Thu Mar  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Tue Dec 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Sun Dec 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Sun Dec 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Fri Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.07-2m)
- rebuild for new GCC 4.5

* Fri Nov 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.00-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against perl-5.12.0

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Wed Dec  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sat Sep 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Thu Sep 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.05-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-2m)
- rebuild against perl-5.10.1

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.04-3m)
- remove duplicate directories

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.04-2m)
- modify BuildRequires

* Thu May 28 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (0.04-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
