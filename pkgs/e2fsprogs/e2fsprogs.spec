%global momorel 2

Summary: Utilities for managing ext2, ext3, and ext4 filesystems
Name: e2fsprogs
Version: 1.42.10
Release: %{momorel}m%{?dist}

# License tags based on COPYING file distinctions for various components
License: GPLv2
Group: System Environment/Base
Source0: http://dl.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: ext2_types-wrapper.h
Source2: e2fsck.conf

Patch1: e2fsprogs-1.40.4-sb_feature_check_ignore.patch
Patch2: e2fsprogs-1.42.10-largefile.patch

Url: http://e2fsprogs.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: e2fsprogs-libs = %{version}-%{release}

BuildRequires: pkgconfig, texinfo, libselinux-devel
BuildRequires: libsepol-devel
BuildRequires: libblkid-devel
BuildRequires: libuuid-devel

%description
The e2fsprogs package contains a number of utilities for creating,
checking, modifying, and correcting any inconsistencies in second,
third and fourth extended (ext2/ext3/ext4) filesystems. E2fsprogs
contains e2fsck (used to repair filesystem inconsistencies after an
unclean shutdown), mke2fs (used to initialize a partition to contain
an empty ext2 filesystem), debugfs (used to examine the internal
structure of a filesystem, to manually repair a corrupted
filesystem, or to create test cases for e2fsck), tune2fs (used to
modify filesystem parameters), and most of the other core ext2fs
filesystem utilities.

You should install the e2fsprogs package if you need to manage the
performance of an ext2, ext3, or ext4 filesystem.

%package libs
Summary: Ext2/3/4 filesystem-specific shared libraries
Group: Development/Libraries
License: GPLv2 and LGPLv2

%description libs
E2fsprogs-libs contains libe2p and libext2fs, the libraries of the
e2fsprogs package.

These libraries are used to directly acccess ext2/3/4 filesystems
from userspace.

%package devel
Summary: Ext2/3/4 filesystem-specific static libraries and headers
Group: Development/Libraries
License: GPLv2 and LGPLv2
Provides: %{name}-static = %{version}-%{release}
Requires: e2fsprogs-libs = %{version}-%{release}
Requires: gawk
Requires: libcom_err-devel
Requires: pkgconfig
Requires(post): info
Requires(preun): info

%description devel
E2fsprogs-devel contains the libraries and header files needed to
develop second, third and fourth extended (ext2/ext3/ext4)
filesystem-specific programs.

You should install e2fsprogs-devel if you want to develop ext2/3/4
filesystem-specific programs. If you install e2fsprogs-devel, you'll
also want to install e2fsprogs.
#'

%package -n libcom_err
Summary: Common error description library
Group: Development/Libraries
License: MIT

%description -n libcom_err
This is the common error description library, part of e2fsprogs.

libcom_err is an attempt to present a common error-handling mechanism.

%package -n libcom_err-devel
Summary: Common error description library
Group: Development/Libraries
License: MIT
Provides: libcom_err-static = %{version}-%{release}
Requires: libcom_err = %{version}-%{release}
Requires: pkgconfig

%description -n libcom_err-devel
This is the common error description development library and headers,
part of e2fsprogs.  It contains the compile_et commmand, used
to convert a table listing error-code names and associated messages
messages into a C source file suitable for use with the library.

libcom_err is an attempt to present a common error-handling mechanism.

%package -n libss
Summary: Command line interface parsing library
Group: Development/Libraries
License: MIT

%description -n libss
This is libss, a command line interface parsing library, part of e2fsprogs.

This package includes a tool that parses a command table to generate
a simple command-line interface parser, the include files needed to
compile and use it, and the static libs.

It was originally inspired by the Multics SubSystem library.

%package -n libss-devel
Summary: Command line interface parsing library
Group: Development/Libraries
License: MIT
Provides: libss-static = %{version}-%{release}
Requires: libss = %{version}-%{release}
Requires: pkgconfig

%description -n libss-devel
This is the command line interface parsing (libss) development library
and headers, part of e2fsprogs.  It contains the mk_cmds command, which
parses a command table to generate a simple command-line interface parser.

It was originally inspired by the Multics SubSystem library.

%prep
%setup -q
# ignore some flag differences on primary/backup sb feature checks
# mildly unsafe but 'til I get something better, avoid full fsck
# after an selinux install...
%patch1 -p1 -b .featurecheck
%patch2 -p1

%build
%configure --enable-elf-shlibs --enable-nls --disable-uuidd --disable-fsck \
	   --disable-e2initrd-helper --disable-libblkid --disable-libuuid \
       --enable-quota --with-root-prefix=/usr
make %{?_smp_mflags} V=1

%install
rm -rf %{buildroot}
export PATH=/sbin:$PATH
make install install-libs DESTDIR=%{buildroot} INSTALL="%{__install} -p" \
        root_sbindir=%{_sbindir} root_libdir=%{_libdir}

# ugly hack to allow parallel install of 32-bit and 64-bit -devel packages:
%define multilib_arches %{ix86} x86_64 ppc ppc64 s390 s390x sparcv9 sparc64

%ifarch %{multilib_arches}
mv -f %{buildroot}%{_includedir}/ext2fs/ext2_types.h \
      %{buildroot}%{_includedir}/ext2fs/ext2_types-%{_arch}.h
install -p -m 644 %{SOURCE1} %{buildroot}%{_includedir}/ext2fs/ext2_types.h
%endif

chmod 644 %{buildroot}%{_libdir}/*.a

# Let boot continue even if *gasp* clock is wrong
install -p -m 644 %{SOURCE2} %{buildroot}/etc/e2fsck.conf

%find_lang %{name}

%check
make check

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%post devel
if [ -f %{_infodir}/libext2fs.info.gz ] ; then
    /sbin/install-info %{_infodir}/libext2fs.info.bz2 %{_infodir}/dir || :
fi

%preun devel
if [ $1 = 0 ]; then
   /sbin/install-info --delete %{_infodir}/libext2fs.info.bz2 %{_infodir}/dir || :
fi
exit 0

%post -n libcom_err -p /sbin/ldconfig
%postun -n libcom_err -p /sbin/ldconfig

%post -n libss -p /sbin/ldconfig
%postun -n libss -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING README RELEASE-NOTES

%config(noreplace) /etc/mke2fs.conf
%config(noreplace) /etc/e2fsck.conf
%{_sbindir}/badblocks
%{_sbindir}/debugfs
%{_sbindir}/dumpe2fs
%{_sbindir}/e2fsck
%{_sbindir}/e2image
%{_sbindir}/e2label
%{_sbindir}/e2undo
%{_sbindir}/fsck.ext2
%{_sbindir}/fsck.ext3
%{_sbindir}/fsck.ext4
%{_sbindir}/fsck.ext4dev
%{_sbindir}/logsave
%{_sbindir}/mke2fs
%{_sbindir}/mkfs.ext2
%{_sbindir}/mkfs.ext3
%{_sbindir}/mkfs.ext4
%{_sbindir}/mkfs.ext4dev
%{_sbindir}/resize2fs
%{_sbindir}/tune2fs
%{_sbindir}/filefrag
%{_sbindir}/e2freefrag
%{_sbindir}/e4defrag
%{_sbindir}/mklost+found

%{_bindir}/chattr
%{_bindir}/lsattr
%{_mandir}/man1/chattr.1*
%{_mandir}/man1/lsattr.1*

%{_mandir}/man5/ext2.5*
%{_mandir}/man5/ext3.5*
%{_mandir}/man5/ext4.5*
%{_mandir}/man5/e2fsck.conf.5*
%{_mandir}/man5/mke2fs.conf.5*

%{_mandir}/man8/badblocks.8*
%{_mandir}/man8/debugfs.8*
%{_mandir}/man8/dumpe2fs.8*
%{_mandir}/man8/e4defrag.8*
%{_mandir}/man8/e2fsck.8*
%{_mandir}/man8/filefrag.8*
%{_mandir}/man8/e2freefrag.8*
%{_mandir}/man8/fsck.ext2.8*
%{_mandir}/man8/fsck.ext3.8*
%{_mandir}/man8/fsck.ext4.8*
%{_mandir}/man8/fsck.ext4dev.8*
%{_mandir}/man8/e2image.8*
%{_mandir}/man8/e2label.8*
%{_mandir}/man8/e2undo.8*
%{_mandir}/man8/logsave.8*
%{_mandir}/man8/mke2fs.8*
%{_mandir}/man8/mkfs.ext2.8*
%{_mandir}/man8/mkfs.ext3.8*
%{_mandir}/man8/mkfs.ext4.8*
%{_mandir}/man8/mkfs.ext4dev.8*
%{_mandir}/man8/mklost+found.8*
%{_mandir}/man8/resize2fs.8*
%{_mandir}/man8/tune2fs.8*

%files libs
%defattr(-,root,root)
%doc COPYING
%{_libdir}/libe2p.so.*
%{_libdir}/libext2fs.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/*.h
%{_infodir}/libext2fs.info*
%{_libdir}/libe2p.a
%{_libdir}/libe2p.so
%{_libdir}/libext2fs.a
%{_libdir}/libext2fs.so
%{_libdir}/pkgconfig/e2p.pc
%{_libdir}/pkgconfig/ext2fs.pc

%{_includedir}/e2p
%{_includedir}/ext2fs

%files -n libcom_err
%defattr(-,root,root)
%doc COPYING
%{_libdir}/libcom_err.so.*

%files -n libcom_err-devel
%defattr(-,root,root)
%{_bindir}/compile_et
%{_libdir}/libcom_err.a
%{_libdir}/libcom_err.so
%{_datadir}/et
%{_includedir}/et
%{_mandir}/man1/compile_et.1*
%{_mandir}/man3/com_err.3*
%{_libdir}/pkgconfig/com_err.pc

%files -n libss
%defattr(-,root,root)
%doc COPYING
%{_libdir}/libss.so.*

%files -n libss-devel
%defattr(-,root,root)
%{_bindir}/mk_cmds
%{_libdir}/libss.a
%{_libdir}/libss.so
%{_datadir}/ss
%{_includedir}/ss
%{_mandir}/man1/mk_cmds.1*
%{_libdir}/pkgconfig/ss.pc

%changelog
* Thu Jun  4 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42.10-2m)
- add e2fsck.conf to avoid running fsck at every boot

* Wed May 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.42.10-1m)
- update to 1.42.10

* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.42.9-2m)
- support UserMove env

* Mon Dec 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.42.9-1m)
- update to 1.42.9

* Fri Jun 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42.8-1m)
- update to 1.42.8

* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42.7-1m)
- update to 1.42.7

* Mon Oct  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42.6-1m)
- update to 1.42.6

* Sun Aug  5 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42.5-1m)
- update to 1.42.5

* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42.4-1m)
- update to 1.42.4
-- fixed dumpe2fs caused 64bit address

* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42.3-1m)
- update to 1.42.3
-- fixed dumpe2fs caused over 16TB disk

* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42.2-1m)
- update to 1.42.2

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42.1-1m)
- update 1.42.1

* Wed Nov 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.42-1m)
- update 1.42

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.41.14-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.41.14-1m)
- update to 1.41.14

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41.13-1m)
- update to 1.41.13

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.41.12-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.41.12-2m)
- full rebuild for mo7 release

* Wed May 19 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.41.12-1m)
- update 1.41.12

* Wed May  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.41.11-2m)
- fix %%post devel and %%preun devel

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.41.11-1m)
- update 1.41.11

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.41.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.41.9-3m)
- update ext2_types-wrapper.h for i686 arch
- remove unused patches

* Mon Sep 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.41.9-2m)
- fix: strip in static library cannot be done

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.41.9-1m)
- sync Fedora
 +* Thu Sep 14 2009 Eric Sandeen <sandeen@redhat.com> 1.41.9-3
 +- Drop defrag bits for now, not ready yet.

* Tue Jun 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.41.4-2m)
- import fedora patches.
-- fix resize2fs broken ext4-resize 

* Thu Jun 18 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.41.4-1m)
- sync Fedora 11 release version
- rollback oldversion

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.41.6-2m)
- fix install-info

* Mon Jun  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.41.6-1m)
- update 1.41.6

* Sun Apr 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.41.5-1m)
- update 1.41.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.41.3-4m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.41.3-3m)
- blkid support btrfs.

* Tue Dec 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.41.3-2m)
- sync with fedora spec

* Tue Oct 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.41.3-1m)
- update 1.41.3

* Fri Jul 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.41.0-1m)
- update 1.41.0

* Sat Jul  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40.11-1m)
- update 1.40.11

* Sat May 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40.10-1m)
- update 1.40.10

* Sat May 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40.8-2m)
- import fedora patches

* Sat May 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40.8-1m)
- update 1.40.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.40.6-2m)
- rebuild against gcc43

* Tue Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40.6-1m)
- update 1.40.6

* Fri Dec 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40.2-3m)
- fails when arch is i686

* Mon Dec 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.40.2-2m)
- fails when arch is i686

* Mon Dec 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40.2-1m)
- [SECURITY] CVE-2007-5497
- update to 1.40.2 and almost sync with Fedora devel
- clean up repository

* Mon Nov 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.39-3m)
- - sync with 1.39-7 from fc-devel
- - rename patch40 to patch140
- - add patch40 .. patch60
- - add Obsoletes: ext2resize
- - changelog is below
-* Wed Sep 20 2006 Jarod Wilson <jwilson@redhat.com> - 1.39-7
-- 32-bit 16T fixups from esandeen (#202807)
-- Update summaries and descriptions
-
-* Sun Sep 17 2006 Karel Zak <kzak@redhat.com> - 1.39-6
-- Fix problem with empty FAT label (#206656)
-
-* Tue Sep  5 2006 Peter Jones <pjones@redhat.com> - 1.39-5
-- Fix memory leak in device probing.

* Mon Oct  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.39-2m)
- add patch40 (for mkinstalldirs)

* Sun Jul 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.39-1m)
- update to 1.39
  - sync with 1.39-4 from fc-devel

* Sun May 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.38-2m)
- modify for 64 bit arch

* Sun May  7 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.38-1m)
- version up based on Fedora (1.38-12)
-  Wed Mar  8 2006 Peter Jones <pjones@redhat.com> - 1.38-12
- - Move /etc/blkid.tab to /etc/blkid/blkid.tab
- 
- * Tue Mar  7 2006 David Cantrell <dcantrell@redhat.com> - 1.38-11
- - BuildRequires pkgconfig
- 
- * Tue Mar  7 2006 David Cantrell <dcantrell@redhat.com> - 1.38-10
- - Disable /etc/blkid.tab caching if time is set before epoch (#182188)
- 
- * Fri Feb 24 2006 Peter Jones <pjones@redhat.com> - 1.38-9
- - _don't_ handle selinux context on blkid.tab, dwalsh says this is a no-no.
- #'
- * Wed Feb 22 2006 Peter Jones <pjones@redhat.com> - 1.38-8
- - handle selinux context on blkid.tab
- 
- * Mon Feb 20 2006 Karsten Hopp <karsten@redhat.de> 1.38-7
- - BuildRequires: gettext-devel
- 
- * Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.38-6.2
- - bump again for double-long bug on ppc(64)
- 
- * Tue Feb  7 2006 Jesse Keating <jkeating@redhat.com> - 1.38-6.1
- - rebuilt for new gcc4.1 snapshot and glibc changes
- 
- * Wed Jan 11 2006 Karel Zak <kzak@redhat.com> 1.38-6
- - cleanup device-mapper patch
- - use pkg-config for device-mapper
- 
- * Mon Jan  9 2006 Peter Jones <pjones@redhat.com> 1.38-5
- - fix some more minor logic errors in dm probing
- 
- * Wed Jan  4 2006 Peter Jones <pjones@redhat.com> 1.38-4
- - fix a logic error in dm probing
- - add priority group for dm devices, so they'll be preferred
- #'
- * Tue Jan  3 2006 Peter Jones <pjones@redhat.com> 1.38-3
- - added support for device-mapper devices
- 
- * Fri Dec  9 2005 Jesse Keating <jkeating@redhat.com>
- - rebuilt
- 
- * Thu Nov 10 2005 Thomas Woerner <twoerner@redhat.com> 1.38-2.1
- - fixed file conflicts between 32bit and 64bit packages (#168815)
- - fixed mklost+found crashes with buffer overflow (#157773)
-   Thanks to Arjan van de Ven for the patch
- 
- * Wed Nov  9 2005 Thomas Woerner <twoerner@redhat.com> 1.38-2
- - splitted up libs from main package, into a new e2fsprogs-libs package
- - fixed requires and prereqs
- 
- * Thu Sep  8 2005 Thomas Woerner <twoerner@redhat.com> 1.38-1
- - new version 1.38
- - Close File descriptor for unregognized devices (#159878)
-   Thanks to David Milburn for the patch.
-   Merged from RHEL-4
- - enable tune2fs to set and clear feature resize_inode (#167816)
- - removed outdated information from ext2online man page (#164383)

* Wed Jan  4 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (1.37-2m)
- set svn:eol-style property to
   e2fsprogs-1.1589.patch
   e2fsprogs-1.1590.patch
   e2fsprogs-1.1591.patch

* Sun May 22 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.37-1m)
- version up based on Fedora (1.37-4)

  * Tue May 10 2005 Jeremy Katz <katzj@redhat.com> - 1.37-4
  - added libblkid.so to devel package

  * Wed May  4 2005 Jeremy Katz <katzj@redhat.com> - 1.37-3
  - fix cramfs detection bug in libblkid

  * Fri Apr  8 2005 Thomas Woerner <twoerner@redhat.com> 1.37-2
  - upstream fixes 1.1589, 1.1590 and 1.1591:
  - add include of stdlib.h to fix a core dump bug on IA64
  - ignore environment variables in blkid and ext2fs for setuid and setguid
    programs
  - no LOW_DTIME checks if the superblock last mount time looks insane

  * Fri Apr  8 2005 Thomas Woerner <twoerner@redhat.com> 1.37-1
  - new version 1.37
  - dropped upstream merged getsize-wrap patch

  * Wed Mar 16 2005 Stephen C. Tweedie <sct@redhat.com> 1.36-1.4
  - Fix the getsize-wrap patch for >4TB filesystems

  * Mon Feb 21 2005 Stephen C. Tweedie <sct@redhat.com> 1.36-1.2
  - Re-enable resize2fs
  - Add bigendian byte-swapping fix when growing the gdt table

  * Fri Feb 11 2005 Stephen C. Tweedie <sct@redhat.com> 1.36-1.1
  - Fix for >=4TB devices

* Tue Jan 25 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.35-2m)
- merge FC3 (1.35-11.2)

* Thu Sep 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.35-2m)
- use %%{_lib} instead of lib

* Wed Mar 17 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.35-1m)
- update to 1.35

* Wed Mar 17 2004 Toru Hoshina <t@momonga-linux.org>
- (1.34-2m)
- revised spec for enabling rpm 4.2.

* Mon Aug 25 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Mon Mar 17 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.32-1m)
- update to 1.32
- unapply e2fsprogs-1.19-mountlabel3.patch
  (I am not sure whether it is still needed). 
- use * and macros in %%files section
- add more documents

* Wed Oct 30 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.29-1m)
- update to 1.29
- s/telia/us/

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.27-6k)
- omit /sbin/ldconfig in PreReq.
- /sbin/install-info -> info in PreReq.

* Mon Apr  8 2002 Toru Hoshina <t@@kondara.org>
- (1.27-4k)
- a couple of commands lost.

* Wed Mar 20 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.27-2k)
- update to 1.27

* Fri May  4 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.19-8k)
- merged RedHat esfsprogs-1.19-4.src.rpm patches for mountlabel.
- append URL:
- %{_tmppath}

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Tue Aug 30 2000 Kazuhiko <kazuhiko@kondara.org>
- version 1.19
- remove e2fsprogs-debugfsy2k.patch
- add fsck.ext3, resize2fs, resize2fs.8*

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Jun  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- uncheck fsck.reiserfs

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat6.2(Release 5)
- add patch, Prereq, post devel , postun devel.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Sat Feb  5 2000 Bill Nottingham <notting@redhat.com>
- add install-info scripts

* Thu Feb 03 2000 Elliot Lee <sopwith@redhat.com>
- Fix bug #8585 (Y2K problems in debugfs)

* Wed Feb 02 2000 Jakub Jelinek <jakub@redhat.com>
- allow multiline errors in et, so that other programs
  can use compile_et (from Kerberos)

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-e2fsprogs-20000115

* Thu Jan 13 2000 Jeff Johnson <jbj@redhat.com>
- build 1.18 for 6.2.

* Sun Nov 28 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-e2fsprogs-19991115

* Thu Nov 18 1999 Norihito Ohmori <nono@kondara.org>
- be a NoSrc :-P

* Tue Oct 26 1999 Bill Nottingham <notting@redhat.com>
- update to 1.17

* Mon Oct 25 1999 Bill Nottingham <notting@redhat.com>
- update to 1.16

* Thu Oct 21 1999 Bill Nottingham <notting@redhat.com>
- add patch to fix SIGUSR1 kills.

* Mon Oct 04 1999 Cristian Gafton <gafton@redhat.com>
- rebuild against new glibc in the sparc tree

* Thu Sep 23 1999 Jakub Jelinek <jakub@redhat.com>
- update mke2fs man page so that it reflects changes in mke2fs
  netweem 1.14 and 1.15

* Thu Aug  5 1999 Bill Nottingham <notting@redhat.com>
- fix lsattr on alpha

* Thu Jul 29 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.15.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Tue Mar 16 1999 Cristian Gafton <gafton@redhat.com>
- fix fsck segfault

* Tue Feb  2 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.14
- use %configure to generate config.sub on arm

* Thu Jan 14 1999 Jeff Johnson <jbj@redhat.com>
- fix /usr/bin/compile_et and doco for com_err.h (#673)

* Thu Jan 07 1999 Cristian Gafton <gafton@redhat.com>
- build with prefix=/usr
- add arm patch

* Mon Dec 28 1998 Jeff Johnson  <jbj@redhat.com>
- update to 1.13.

* Fri Aug 28 1998 Jeff Johnson <jbj@redhat.com>
- recompile statically linked binary for 5.2/sparc

* Mon Jul 13 1998 Jeff Johnson <jbj@redhat.com>
- upgrade to 1.12.

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- include <asm/types.h> to match kernel types in utils

* Tue Oct 14 1997 Donnie Barnes <djb@redhat.com>
- spec file cleanups

* Wed Oct 01 1997 Erik Troan <ewt@redhat.com>
- fixed broken llseek() prototype 

* Wed Aug 20 1997 Erik Troan <ewt@redhat.com>
- added patch to prototype llseek

* Tue Jun 17 1997 Erik Troan <ewt@redhat.com>
- built against glibc
