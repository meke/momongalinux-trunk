%global         momorel 3
%global         origver 3.030

Name:           perl-MIME-Lite
Version:        %{origver}
Release:        %{momorel}m%{?dist}
Summary:        Low-calorie MIME generator
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MIME-Lite/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/MIME-Lite-%{origver}.tar.gz
NoSource:       0
Patch0:         %{name}-3.02.8-no-prompt.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Email-Date-Format >= 1.000
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Cwd
BuildRequires:  perl-MIME-Types >= 1.28
BuildRequires:  perl-MailTools >= 1.62
Requires:       perl-Email-Date-Format >= 1.000
Requires:       perl-Cwd
Requires:       perl-MIME-Types >= 1.28
Requires:       perl-MailTools >= 1.62
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
In the never-ending quest for great taste with fewer calories, we proudly
present: MIME::Lite.

%prep
%setup -q -n MIME-Lite-%{origver}
%patch0 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING INSTALLING LICENSE META.json README
%{perl_vendorlib}/MIME/Lite.pm
%{perl_vendorlib}/MIME/changes.pod
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.030-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.030-2m)
- rebuild against perl-5.18.2

* Sun Nov 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.030-1m)
- update to 3.030

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.9-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.9-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.9-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.9-2m)
- rebuild against perl-5.16.2

* Sat Aug 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.9-1m)
- update to 3.029

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.8-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.8-2m)
- rebuild against perl-5.16.0

* Thu Nov 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.8-1m)
- update to 3.028

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.7-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.7-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.7-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.02.7-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.02.7-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.7-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.02.7-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.7-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.7-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.7-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.7-1m)
- update to 3.027

* Sat Sep 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.6-1m)
- update to 3.026

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.5-1m)
- update to 3.025

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.4-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02.4-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.4-1m)
- update to 3.024

* Fri Nov 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.3-1m)
- update to 3.023

* Mon Nov  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02.2-1m)
- update to 3.022

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.02.1-2m)
- rebuild against gcc43

* Sun Dec  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02.1-1m)
- update to 3.021
-- Plagger::Plugin::Publish::Gmail: support higher version of MIME::Lite
-- ref. <http://www.karashi.org/~poppen/d/20070921.html>

* Thu Oct 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.01-3m)
- version down to 3.01 perl-Plagger does not work...
- http://www.karashi.org/~poppen/d/20070921.html

* Wed Aug 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.020-1m)
- update to 3.020
- modify BuildRequires: and Requires: sections

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.01-2m)
- use vendor

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.01-1m)
- spec file was autogenerated
