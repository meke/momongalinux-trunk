%global momorel 1
%global remove_kernel_firmware 0

%define _binaries_in_noarch_packages_terminate_build 0

Summary: Firmware for several ALSA-supported sound cards
Name: alsa-firmware
Version: 1.0.27
Release: %{momorel}m%{?dist}
# See later in the spec for a breakdown of licensing
License: GPL+ and BSD and GPLv2+ and GPLv2 and LGPLv2+ and "see emi_26_62_license.txt"
Group: Applications/Multimedia
URL: http://www.alsa-project.org/
Source0: ftp://ftp.alsa-project.org/pub/firmware/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: alsa-tools-firmware >= %{version}
Requires: udev
BuildRequires: autoconf
BuildRequires: automake
BuildArch: noarch

%description
This package contains the firmware binaries for a number of sound cards.
Some (but not all of these) require firmware loaders which are included in
the alsa-tools-firmware package.

%prep
%setup -q

# Leaving this directory in place ends up with the following crazy, broken
# symlinks in the output RPM, with no sign of the actual firmware (*.bin) files
# themselves:
#
# /lib/firmware/turtlebeach:
# msndinit.bin -> /etc/sound/msndinit.bin
# msndperm.bin -> /etc/sound/msndperm.bin
# pndsperm.bin -> /etc/sound/pndsperm.bin
# pndspini.bin -> /etc/sound/pndspini.bin
#
# Probably an upstream package bug.
sed -i s#'multisound/Makefile \\'## configure.in
sed -i s#multisound## Makefile.am

aclocal
automake
autoconf

%build
%configure --disable-loader
%make

# Rename README files from firmware subdirs that have them
for i in hdsploader mixartloader pcxhrloader usx2yloader vxloader
do
  mv ${i}/README README.${i}
done

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%if %{remove_kernel_firmware}
rm -rf %{buildroot}/lib/firmware/ess
rm -rf %{buildroot}/lib/firmware/korg
rm -rf %{buildroot}/lib/firmware/sb16
rm -rf %{buildroot}/lib/firmware/yamaha
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README*

# License: KOS (BSD-alike)
/lib/firmware/aica_firmware.bin

# License: No explicit license; default package license is GPLv2+
/lib/firmware/asihpi

# License: GPL (undefined version)
/lib/firmware/digiface_firmware*

%dir /lib/firmware/ea
# The licenses for the Echo Audio firmware vary slightly so each is enumerated
# separately, to be really sure.
# LGPLv2.1+
/lib/firmware/ea/3g_asic.fw
# GPL (undefined version)
/lib/firmware/ea/darla20_dsp.fw
# LGPLv2.1+
/lib/firmware/ea/darla24_dsp.fw
# LGPLv2.1+
/lib/firmware/ea/echo3g_dsp.fw
# GPL (undefined version)
/lib/firmware/ea/gina20_dsp.fw
# GPL (undefined version)
/lib/firmware/ea/gina24_301_asic.fw
# GPL (undefined version)
/lib/firmware/ea/gina24_301_dsp.fw
# GPL (undefined version)
/lib/firmware/ea/gina24_361_asic.fw
# GPL (undefined version)
/lib/firmware/ea/gina24_361_dsp.fw
# LGPLv2.1+
/lib/firmware/ea/indigo_dj_dsp.fw
# LGPLv2.1+
/lib/firmware/ea/indigo_djx_dsp.fw
# LGPLv2.1+
/lib/firmware/ea/indigo_dsp.fw
# LGPLv2.1+
/lib/firmware/ea/indigo_io_dsp.fw
# LGPLv2.1+
/lib/firmware/ea/indigo_iox_dsp.fw
# GPL (undefined version)
/lib/firmware/ea/layla20_asic.fw
# GPL (undefined version)
/lib/firmware/ea/layla20_dsp.fw
# GPL (undefined version)
/lib/firmware/ea/layla24_1_asic.fw
# GPL (undefined version)
/lib/firmware/ea/layla24_2A_asic.fw
# GPL (undefined version)
/lib/firmware/ea/layla24_2S_asic.fw
# GPL (undefined version)
/lib/firmware/ea/layla24_dsp.fw
# GPL (undefined version)
/lib/firmware/ea/loader_dsp.fw
# LGPLv2.1+
/lib/firmware/ea/mia_dsp.fw
# GPL (undefined version)
/lib/firmware/ea/mona_2_asic.fw
# GPL (undefined version)
/lib/firmware/ea/mona_301_1_asic_48.fw
# GPL (undefined version)
/lib/firmware/ea/mona_301_1_asic_96.fw
# GPL (undefined version)
/lib/firmware/ea/mona_301_dsp.fw
# GPL (undefined version)
/lib/firmware/ea/mona_361_1_asic_48.fw
# GPL (undefined version)
/lib/firmware/ea/mona_361_1_asic_96.fw
# GPL (undefined version)
/lib/firmware/ea/mona_361_dsp.fw

%dir /lib/firmware/emu
# Licenses vary so are enumerated separately
# GPLv2
/lib/firmware/emu/audio_dock.fw
# GPLv2
/lib/firmware/emu/emu0404.fw
# GPLv2
/lib/firmware/emu/emu1010_notebook.fw
# GPLv2
/lib/firmware/emu/emu1010b.fw
# GPLv2
/lib/firmware/emu/hana.fw
# GPLv2+
/lib/firmware/emu/micro_dock.fw

%if ! %{remove_kernel_firmware}
# License: GPL (undefined version)
/lib/firmware/ess
%endif

%if ! %{remove_kernel_firmware}
# License: No explicit license; default package license is GPLv2+
/lib/firmware/korg
%endif

# License: GPL (undefined version)
/lib/firmware/mixart

# License: GPL (undefined version)
/lib/firmware/multiface_firmware*

# License: GPL (undefined version)
/lib/firmware/pcxhr

# License: GPL (undefined version)
/lib/firmware/rpm_firmware.bin

%if ! %{remove_kernel_firmware}
# License: GPLv2+
/lib/firmware/sb16
%endif

# License: GPL (undefined version)
/lib/firmware/vx

%if ! %{remove_kernel_firmware}
# License: No explicit license; default package license is GPLv2+
# See ALSA bug #3412
/lib/firmware/yamaha
%endif

# Licence: Redistribution allowed, see ca0132/creative.txt
/lib/firmware/ctefx.bin
/lib/firmware/ctspeq.bin

# Even with --disable-loader, we still get usxxx firmware here; looking at the
# alsa-tools-firmware package, it seems like these devices probably use an old- 
# style hotplug loading method
# License: GPL (undefined version)
%{_datadir}/alsa/firmware

%changelog
* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.27-1m)
- update to 1.0.27

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.25-1m)
- version 1.0.25

* Sat Jun 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.24.1-1m)
- version 1.0.24.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.23-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.23-1m)
- version 1.0.23

* Sat Jul 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.20-4m)
- set remove_kernel_firmware 0
- we are using linux-firmware now, it's not conflicting with this package

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.20-2m)
- remove yss225_registers.bin conflicting with kernel-firmware-2.6.30.5-1m

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.20-1m)
- version 1.0.20

* Sat Apr 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.17-4m)
- %%define _binaries_in_noarch_packages_terminate_build 0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.17-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-2m)
- modify spec file to avoid conflicting with kernel-firmware-2.6.27.10

* Mon Aug 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-1m)
- version 1.0.17

* Mon Aug 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.16-1m)
- import from Fedora

* Mon May 12 2008 Tim Jackson <rpm@timj.co.uk> - 1.0.16-1
- Update to upstream 1.0.16
- Clarify licensing conditions

* Tue Aug 14 2007 Tim Jackson <rpm@timj.co.uk> - 1.0.14-1
- Update to upstream 1.0.14, but skip turtlebeach firmware as it doesn't seem 
  to install properly
- Remove files from old-style firmware loader locations
- Spec file cosmetics, keep rpmlint quiet

* Sat Nov 25 2006 Tim Jackson <rpm@timj.co.uk> - 1.0.12-1
- Update to 1.0.12
- Add udev dep

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sat Apr 03 2004 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 1.0.4-0.fdr.1
- Update to 1.0.4

* Fri Jan 16 2004 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 1.0.1-0.fdr.2
- add missing rm in install section

* Fri Jan 09 2004 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 1.0.1-0.fdr.1
- Update to 1.0.1
- Contains now the license -- is "Distributable under GPL"

* Thu Dec 04 2003 Thorsten Leemhuis <fedora[AT]leemhuis.info> 1.0.0-0.fdr.0.1.rc1
- Initial build.
