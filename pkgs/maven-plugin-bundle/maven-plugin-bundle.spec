%global momorel 4

Name:           maven-plugin-bundle
Version:        2.0.0
Release:        %{momorel}m%{?dist}
Summary:        Maven Bundle Plugin

Group:          Development/Tools
License:        "ASL 2.0"
URL:            http://felix.apache.org
Source0:        http://www.apache.org/dist/felix/maven-bundle-plugin-2.0.0-project.tar.gz
Source2:        felix-settings.xml
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: aqute-bndlib >= 0.0.363
BuildRequires: plexus-utils >= 1.4.5
BuildRequires: felix-osgi-obr
BuildRequires: kxml
BuildRequires: maven-shared-dependency-tree >= 1.1-1m
BuildRequires: maven-wagon >= 1.0-0.3.1m
BuildRequires: maven2-plugin-compiler >= 2.0.8
BuildRequires: maven2-plugin-install >= 2.0.8
BuildRequires: maven2-plugin-jar >= 2.0.8
BuildRequires: maven2-plugin-javadoc >= 2.0.8
BuildRequires: maven2-plugin-plugin >= 2.0.8
BuildRequires: maven2-plugin-resources >= 2.0.8
BuildRequires: maven-surefire-maven-plugin >= 2.3
BuildRequires: maven-surefire-provider-junit >= 2.3
BuildRequires: maven-doxia-sitetools
Requires: aqute-bndlib >= 0.0.363
Requires: plexus-utils >= 1.4.5
Requires: felix-osgi-obr
Requires: kxml
Requires: maven2
Requires: maven-shared-archiver
Requires: maven-shared-dependency-tree
Requires: maven-wagon
Requires: plexus-archiver
Requires: plexus-containers-container-default

BuildArch: noarch


%description
Provides a maven plugin that supports creating an OSGi bundle
from the contents of the compilation classpath along with its
resources and dependencies. Plus a zillion other features.

%package javadoc
Group:          Documentation
Summary:        Javadoc for %{name}

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n maven-bundle-plugin-%{version}
cp %{SOURCE2} settings.xml

%build
sed -i -e \
 "s|<url>__JPP_URL_PLACEHOLDER__</url>|<url>file://`pwd`/.m2/repository</url>|g" \
 settings.xml
sed -i -e \
 "s|<url>__JAVADIR_PLACEHOLDER__</url>|<url>file://`pwd`/external_repo</url>|g" \
 settings.xml
sed -i -e \
 "s|<url>__MAVENREPO_DIR_PLACEHOLDER__</url>|<url>file://`pwd`/.m2/repository</url>|g" \
 settings.xml
sed -i -e \
 "s|<url>__MAVENDIR_PLUGIN_PLACEHOLDER__</url>|<url>file:///usr/share/maven2/plugins</url>|g" \
 settings.xml
sed -i -e \
 "s|<url>__ECLIPSEDIR_PLUGIN_PLACEHOLDER__</url>|<url>file:///usr/share/eclipse/plugins</url>|g" \
 settings.xml

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mvn-jpp \
        -e \
        -s $(pwd)/settings.xml \
        -Dmaven2.jpp.mode=true \
        -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
        install javadoc:javadoc

%install
rm -rf %{buildroot}

# jars
install -d -m 0755 %{buildroot}%{_javadir}
install -m 644 target/maven-bundle-plugin-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

%add_to_maven_depmap org.apache.felix maven-bundle-plugin %{version} JPP %{name}

# poms
install -d -m 755 %{buildroot}%{_datadir}/maven2/poms
install -pm 644 pom.xml \
    %{buildroot}%{_datadir}/maven2/poms/JPP-%{name}.pom

# javadoc
install -d -m 0755 %{buildroot}%{_javadocdir}/%{name}-%{version}
cp -pr target/site/api*/* %{buildroot}%{_javadocdir}/%{name}-%{version}/
ln -s %{name}-%{version} %{buildroot}%{_javadocdir}/%{name}
rm -rf target/site/api*

%post
%update_maven_depmap

%postun
%update_maven_depmap

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_javadir}/*
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-1m)
- import from Fedora 13

* Fri Sep 18 2009 Alexander Kurtakov <akurtako@redhat.com> 2.0.0-4
- Add missing Requires.

* Wed Sep 9 2009 Alexander Kurtakov <akurtako@redhat.com> 2.0.0-3
- BR doxia-sitetools.

* Mon Sep 7 2009 Alexander Kurtakov <akurtako@redhat.com> 2.0.0-2
- Fix BR/Rs.

* Thu Sep 3 2009 Alexander Kurtakov <akurtako@redhat.com> 2.0.0-1
- Initial import.
