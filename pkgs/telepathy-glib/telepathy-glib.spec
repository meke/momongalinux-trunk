%global         momorel 1

Name:           telepathy-glib
Version:        0.21.0
Release:        %{momorel}m%{?dist}
Summary:        GLib bindings for Telepathy

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://telepathy.freedesktop.org/wiki/FrontPage
Source0:        http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk-doc >= 1.5
BuildRequires:  pkgconfig
BuildRequires:  glib2-devel >= 2.20.1
BuildRequires:  dbus-devel >= 1.2.4
BuildRequires:  dbus-glib-devel >= 0.80
BuildRequires:	libxslt
BuildRequires:	python
BuildRequires:  vala >= 0.12.0

%description
Telepathy-glib is the glib bindings for the telepathy unified framework
for all forms of real time conversations, including instant messaging, IRC, 
voice calls and video calls.


%package vala
Summary:	Vala bindings for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	vala


%description vala
Vala bindings for %{name}.


%package 	devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	telepathy-filesystem
Requires:	dbus-devel >= 0.95
Requires:	dbus-glib-devel >= 0.73
Requires:	glib2-devel >= 2.10
Requires:	pkgconfig


%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --enable-gtk-doc \
    --disable-static \
    --enable-shared \
    --enable-installed-examples \
    --enable-vala-bindings \
    LIBS="-ldbus-glib-1"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog NEWS
%{_libdir}/libtelepathy-glib.so.*
%{_libdir}/girepository-1.0/*.typelib
%{_datadir}/gir-1.0/*.gir
%exclude %{_libdir}/libtelepathy-glib.la

%files vala
%defattr(-,root,root,-)
%{_datadir}/vala/vapi/telepathy-glib.deps
%{_datadir}/vala/vapi/telepathy-glib.vapi

%files devel
%defattr(-, root, root)
%{_libdir}/libtelepathy-glib.so
%{_libdir}/pkgconfig/telepathy-glib.pc
%{_includedir}/telepathy-1.0/telepathy-glib
%doc %{_datadir}/gtk-doc/html/%{name}
#{_datadir}/vala/vapi/telepathy-glib.deps
#{_datadir}/vala/vapi/telepathy-glib.vapi

# example
%{_bindir}/telepathy-example-*
%{_libexecdir}/telepathy-example-*
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.*
%{_datadir}/telepathy/managers/example_*

%changelog
* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21.0-1m)
- update to 0.21.0

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20.1-1m)
- update to 0.20.1

* Fri Oct  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20.0-1m)
- update to 0.20.0

* Fri Sep 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19.10-1m)
- update to 0.19.10

* Sun Aug 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19.6-1m)
- update to 0.19.6

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19.4-1m)
- update to 0.19.4

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19.3-1m)
- update to 0.19.3

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19.2-1m)
- update to 0.19.2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17.7-2m)
- rebuild for glib 2.33.2

* Fri Mar 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.7-1m)
- update to 0.17.7

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.6-1m)
- update to 0.17.6

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.5-1m)
- update to 0.17.5

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.4-1m)
- update to 0.17.4

* Wed Nov 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.3-1m)
- update to 0.17.3

* Fri Nov 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.2-1m)
- update to 0.17.2

* Sun Nov 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.1-1m)
- update to 0.17.1

* Sat Nov 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.0-1m)
- update to 0.17.0

* Fri Oct 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.1-1m)
- update to 0.16.1

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.0-1m)
- update to 0.16.0

* Thu Oct 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.9-1m)
- update to 0.15.9

* Wed Oct  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.7-1m)
- update to 0.15.7

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.6-1m)
- update to 0.15.6

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.5-1m)
- update to 0.15.5

* Wed Jul 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.4-1m)
- update to 0.15.4

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.3-1m)
- update to 0.15.3

* Wed Jun 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.2-1m)
- update to 0.15.2

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.1-1m)
- update to 0.15.1

* Wed May 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.0-1m)
- update to 0.15.0

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.5-1m)
- update to 0.14.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.3-3m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.3-2m)
- enable vala bindings
- separate telepathy-glib-vala subpackage

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.3-1m)
- update to 0.14.3

* Tue Mar 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.1-1m)
- update to 0.14.1

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.18-1m)
- update to 0.13.18

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.17-1m)
- update to 0.13.17

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.16-1m)
- update to 0.13.16

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.14-1m)
- update to 0.13.14
- disable vala bindings

* Sat Dec 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.9-1m)
- update to 0.13.9

* Sat Dec  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.8-1m)
- update to 0.13.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.7-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.7-1m)
- update to 0.13.7

* Thu Nov 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.6-1m)
- update to 0.13.6

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.4-1m)
- update to 0.13.4

* Wed Oct 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.3-1m)
- update to 0.13.3

* Fri Oct 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.2-1m)
- update to 0.13.2

* Fri Oct  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.1-1m)
- update to 0.13.1

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.0-1m)
- update to 0.13.0

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.0-2m)
- build with vala
- delete patch0 (gtk-doc version up)

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.11-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.11-1m)
- update to 0.11.11

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.10-1m)
- update to 0.11.10

* Mon Jul  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.9-1m)
- update to 0.11.9

* Fri Jun 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.5-1m)
- update to 0.11.5

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.3-2m)
- explicitly link libdbus-glib-1

* Sun Apr 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.3-1m)
- update to 0.11.3

* Sat Jan 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.36-1m)
- update to 0.7.36

* Sun Jun  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.31-1m)
- update to 0.7.31

* Sun May 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.30-1m)
- update to 0.7.30

* Sun Apr 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.29-1m)
- update to 0.7.29

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.26-1m)
- update to 0.7.26

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.25-1m)
- update to 0.7.25

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.17-3m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.17-2m)
- modify %%files for telepathy-filesystem

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.17-1m)
- update to 0.7.17

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.15-1m)
- update to 0.7.15

* Tue May 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.5-1m)
- import from Fedora

* Mon Mar 24 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.5-4
- Bump.

* Fri Mar 21 2008 - Bastien Nocera <bnocera@redhat.com> - 0.7.5-3
- Really fix #436773

* Mon Mar 10 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.5-2
- Add requires for glib2-devel to devel package. (#436773)

* Fri Mar  7 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.5-1
- Update to 0.7.5.
- Remove hack to fix ppc64 build.  fixed upstream.

* Thu Mar  6 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.4-1
- Update to 0.7.4.
- Disable test for now.
- Add hack to fix build on ppc64.

* Fri Feb  8 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.0-2
- Rebuild for gcc-4.3.

* Fri Nov 23 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.7.0-1
- Update to 0.7.0.
- Drop unstable-static subpackage.
- Bump min. versions of dbus-devel & dbus-glib-devel.

* Mon Nov 12 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.6.1-1
- Update to 0.6.1.

* Wed Aug 29 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.14-1
- Update to 0.5.14.

* Tue Aug 28 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.13-6
- Add BR on python.

* Tue Aug 21 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.13-5
- Rebuild.

* Sun Aug  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.13-4
- Update license tag.

* Mon Jun 18 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.13-3
- Add check section to run test suite.
- Mark gtk-docs as docs.

* Sat Jun 16 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.13-2
- Change Group to more accurate categoryn.
- Require pkgconfig on the devel package.
- Move headers for unstable libs to unstable-static sub-pacakge.

* Tue Jun  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.13-1
- Update to 0.5.13.

* Wed May 16 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.11-1
- Update to 0.5.11.

* Sat Apr 21 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.10-1
- Intial spec.

