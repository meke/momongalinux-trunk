%global         momorel 1
%global         srcver 1.10

Summary:        Date and time object
Name:           perl-DateTime
Version:        %{srcver}
Release:        %{momorel}m%{?dist}
Epoch:          1
License:        GPL+ or Artistic
Group:          Development/Languages
Source0:        http://www.cpan.org/authors/id/D/DR/DROLSKY/DateTime-%{srcver}.tar.gz
NoSource:       0
URL:            http://www.cpan.org/modules/by-module/DateTime/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-DateTime-Locale >= 0.41
BuildRequires:  perl-DateTime-TimeZone >= 1.09
BuildRequires:  perl-ExtUtils-CBuilder
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Params-Validate >= 0.76
BuildRequires:  perl-podlators >= 2.3.1
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Time-Local >= 1.04
Requires:       perl-DateTime-Locale >= 0.41
Requires:       perl-DateTime-TimeZone >= 1.09
Requires:       perl-Params-Validate >= 0.76
Requires:       perl-Scalar-Util
Requires:       perl-Time-Local >= 1.04
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
DateTime is a class for the representation of date/time combinations, and
is part of the Perl DateTime project. For details on this project please
see http://datetime.perl.org/. The DateTime site has a FAQ which may help
answer many "how do I do X?" questions. The FAQ is at
http://datetime.perl.org/?FAQ.

%prep
%setup -q -n DateTime-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(DateTimePPExtra)/d'

EOF
%define __perl_requires %{_builddir}/DateTime-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Build.PL installdirs=vendor optimize="%{optflags}"
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc c Changes CREDITS dist.ini leaptab.txt LICENSE META.json README TODO tools
%{perl_vendorarch}/auto/DateTime
%{perl_vendorarch}/DateTime
%{perl_vendorarch}/DateTime*.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-1m)
- rebuild against perl-5.20.0
- update to 1.10

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.09-1m)
- update to 1.09

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.08-1m)
- update to 1.08

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.07-1m)
- update to 1.07

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.03-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.03-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.03-2m)
- rebuild against perl-5.18.0

* Thu Apr 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.03-1m)
- update to 1.03

* Tue Apr 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.02-1m)
- update to 1.02

* Tue Apr  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.01-1m)
- update to 1.01

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.00-1m)
- update to 1.00

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.78-2m)
- rebuild against perl-5.16.3

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.78-1m)
- update to 0.78

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.77-2m)
- rebuild against perl-5.16.2

* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.77-1m)
- update to 0.77

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.76-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.76-1m)
- update to 0.76
- rebuild against perl-5.16.0

* Fri Mar 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.74-1m)
- update to 0.74

* Sun Mar 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.73-1m)
- update to 0.73

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.72-1m)
- update to 0.72

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.71-1m)
- update to 0.71

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.70-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.70-2m)
- rebuild against perl-5.14.1

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.70-1m)
- update to 0.70

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.69-2m)
- rebuild against perl-5.14.0-0.2.1m

* Wed May  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.69-1m)
- update to 0.69

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.68-1m)
- update to 0.68

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.66-2m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.66-1m)
- update to 0.66

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.65-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.65-1m)
- update to 0.65

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.63-2m)
- rebuild against perl-5.12.2

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.63-1m)
- update to 0.63

* Fri Sep 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.62-1m)
- update to 0.62

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.61-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.61-2m)
- add epoch to %%changelog

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.61-1m)
- update to 0.61

* Sun Jul  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.60-1m)
- update to 0.60

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.59-1m)
- update to 0.59

* Tue Jun 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.58-1m)
- update to 0.58

* Sun Jun 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.57-1m)
- update to 0.57

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.55-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.55-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.55-1m)
- update to 0.55

* Mon Mar 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.54-1m)
- update to 0.54

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.53-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.53-1m)
- update to 0.53

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.51-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.51-1m)
- update to 0.51
- Specfile re-generated by cpanspec 1.78 for Momonga Linux.

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.50-3m)
- rebuild against perl-5.10.1

* Wed Jul 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.50-2m)
- add Epoch: 1 to enable upgrading from STABLE_5
- DO NOT REMOVE Epoch FOREVER

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49

* Wed Mar  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Sun Mar  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.45.01-2m)
- rebuild against rpm-4.6

* Wed Nov 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45.01-1m)
- update to 0.4501

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Tue Nov  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44.01-1m)
- update to 0.4401

* Sun Nov  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Sat Oct  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4305-1m)
- update to 0.4305

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4304-1m)
- update to 0.4304

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4303-1m)
- update to 0.4303

* Wed May 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4302-1m)
- update to 0.4302

* Mon May 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4301-1m)
- update to 0.4301

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.42-2m)
- rebuild against gcc43
 
* Sat Mar  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Tue Sep 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41
- do not use %%NoSource macro
- adjust BuildRequires: and Requires:

* Fri Aug 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.37-2m)
- use vendor

* Sat Mar 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Fri Jan 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.34-1m)
- spec file was autogenerated
- add Provides: perl(DateTimePPExtra)
