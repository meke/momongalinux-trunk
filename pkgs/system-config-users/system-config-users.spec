%global momorel 1

Summary: A graphical interface for administering users and groups
Name: system-config-users
Version: 1.2.110
Release: %{momorel}m%{?dist}
URL: http://fedoraproject.org/wiki/SystemConfig/users
License: GPLv2+
Group: Applications/System
BuildRoot: %{_tmppath}/system-config-users-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Source: system-config-users-%{version}.tar.bz2
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: findutils
Obsoletes: redhat-config-users
Requires: libuser-python >= 0.56
Requires: python >= 2.0
Requires: pygtk2 >= 2.6
Requires: pygtk2-libglade
Requires: usermode-gtk >= 1.94
Requires: xdg-utils
Requires: rpm-python
Requires: procps
Requires: hicolor-icon-theme
Requires: system-config-users-docs
Requires: cracklib >= 2.8.6

BuildRequires: python >= 2.0

%description
system-config-users is a graphical utility for administrating 
users and groups.  It depends on the libuser library.

%prep
%setup -q

%build
make CONSOLE_USE_CONFIG_UTIL=1 %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
%make DESTDIR=%{buildroot} install

desktop-file-install --vendor system --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category System \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%find_lang %name
find %{buildroot}%{_datadir} -name "*.mo" | xargs ./utf8ify-mo

%clean
rm -rf --preserve-root %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_mandir}/man8/system-config-users*
%attr(0644,root,root) %{_datadir}/applications/%{name}.desktop
%attr(0644,root,root) %config /etc/security/console.apps/system-config-users
%attr(0644,root,root) %config /etc/pam.d/system-config-users
%attr(0644,root,root) %config (noreplace) /etc/sysconfig/system-config-users

%changelog
* Thu Mar 22 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (1.2.110-1m)
- update to 1.2.110

* Thu May 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.107-1m)
- update to 1.2.107

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.99-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.99-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.99-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.99-1m)
- update to 1.2.99

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.97-1m)
- update to 1.2.97

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.86-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.86-2m)
- CONSOLE_USE_CONFIG_UTIL=1

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.86-1m)
- update to 1.2.86

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.80-2m)
- rebuild against rpm-4.6

* Sun Jun 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.80-1m)
- update to 1.2.80

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.56-2m)
- rebuild against gcc43

* Sun Jun  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.56-1m)
- update to 1.2.56

* Tue Feb 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.52-1m)
- update 1.2.52

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.42-2m)
- remove category X-Red-Hat-Base SystemSetup Application

* Tue Jun  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.42-1m)
- update 1.2.42

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.28-2m)
- add System to Categories of desktop file for KDE

* Mon Dec  6 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.28-1m)
  update to 1.2.28

* Wed Nov 10 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.28-1
- check for running processes of a user about to be deleted (#132902)

* Mon Nov 08 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.27-1
- some sanity testing to avoid deleting system directories when deleting a user
  (#138093)
- eventually delete mail spool (#102637) and temporary files (#126756)

* Fri Nov 05 2004 Nils Philippsen <nphilipp@redhat.com>
- set password and confirm password entries (in)sensitive based on whether
  account is locked or not (#131180)

* Tue Nov 02 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.26-1
- use libuser defaults for password aging (#130379, original patch by Dave
  Lehman)

* Wed Oct 13 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.25-1
- when renaming users, ensure that groups forget about the old user name
  (#135280)

* Mon Oct 11 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.24-1
- use user/group names for indexing, avoid unnecessary user/group lookups
  (#135223, original patch by Miloslav Trmac)
- remove some debugging statements
- updated translations

* Fri Oct 08 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.23-1
- try to fix 32bit uids/gids (#134803)
- fix gtk.main*() related DeprecationWarnings
- byte-compile python files in "make install"
- updated translations

* Mon Oct 04 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.22-1
- updated translations

* Sun Sep 26 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.21-1
- updated translations

* Fri Sep 24 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.20-1
- allow UTF-8 in user's full name (#133137)
- require new libuser version so that fix for #80624 doesn't throw exception
  (#133479)
- admit complicity

* Wed Sep 15 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.19-1
- try to use gid as specified in /etc/libuser.conf, only if that fails use next
  free (#80624)

* Mon Sep 13 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.18-1
- use F1 instead of Ctrl+H as accelerator for Help/Contents (#132163)
- use "mkdir -p" to fix make install glitch
- use absolute paths in *.glade to fix pygtk/pyglade subtleties

* Sun Sep 05 2004 Nils Philippsen <nphilipp@redhat.com> - 1.2.15-1
- add manpage (Chris Spencer, #115316)
- add Slovenian translation to desktop file (Roman Maurer, #131835)

* Mon Jun 21 2004 Brent Fox <bfox@redhat.com> - 1.2.14-1
- fix password expiration bug (bug #125234)

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.12-5m)
- import from Fedora.

* Wed Apr 21 2004 Brent Fox <bfox@redhat.com> 1.2.13-1
- allow columns to be resized (bug #121174)

* Tue Apr 20 2004 Brent Fox <bfox@redhat.com> 1.2.12-5
- call self.ready() if no is clicked (bug #121364)

* Mon Apr 19 2004 Brent Fox <bfox@redhat.com> 1.2.12-4
- apply patch from bug #72058 to localize pw last changed time

* Mon Apr 19 2004 Brent Fox <bfox@redhat.com> 1.2.12-3
- hide SELinux widgets for now (bug #119941)

* Mon Apr 19 2004 Brent Fox <bfox@redhat.com> 1.2.12-2
- remove *pyc files on ininstall

* Thu Apr 15 2004 Brent Fox <bfox@redhat.com> 1.2.12-1
- fix bug #120669

* Tue Apr 13 2004 Brent Fox <bfox@redhat.com> 1.2.11-6
- remove print statements in mainWindow.py

* Mon Apr 12 2004 Brent Fox <bfox@redhat.com> 1.2.11-5
- fix icon path (bug #120186)

* Wed Apr  7 2004 Brent Fox <bfox@redhat.com> 1.2.11-4
- disable SELinux widgets if it isn't running or isn't enabled (bug #120193)

* Tue Apr  6 2004 Brent Fox <bfox@redhat.com> 1.2.11-3
- remove Requires on policy-sources and setools (bug #120193)

* Mon Apr  5 2004 Brent Fox <bfox@redhat.com> 1.2.11-2
- rebuild for SELinux 
- add Requires on policy-sources

* Wed Mar 31 2004 Brent Fox <bfox@redhat.com> 1.2.11-1
- first stab at SELinux bits

* Wed Mar 24 2004 Brent Fox <bfox@redhat.com> 1.2.10-1
- reset user home dir check button (bug #119068)

* Tue Feb  3 2004 Brent Fox <bfox@redhat.com> 1.2.9-1
- remove comparison to gtk.TRUE (bug #114266)

* Mon Jan 12 2004 Brent Fox <bfox@redhat.com> 1.2.8-1
- rename redhat-config-users.png to system-config-users.png (bug #113311)

* Mon Dec  1 2003 Brent Fox <bfox@redhat.com> 1.2.7-1
- preserve existing group selection in userProperties.py (bug #111199)
- handle munged config file (bug #108400)

* Mon Nov 24 2003 Brent Fox <bfox@redhat.com> 1.2.6-1
- remove Red Hat reference in the window title

* Wed Nov 19 2003 Brent Fox <bfox@redhat.com> 1.2.5-1
- rename from redhat-config-users
- add Obsoletes for redhat-config-users
- make changes for Python2.3

* Mon Oct 27 2003 Brent Fox <bfox@redhat.com> 1.2.4-1
- call self.ready() if the user clicks cancel in the existing group dialog (bug #107991)

* Mon Oct 20 2003 Brent Fox <bfox@redhat.com> 1.2.3-1
- use htmlview to find default browser (bug #107604)

* Mon Oct  6 2003 Brent Fox <bfox@redhat.com> 1.2.2-1
- don't allow the root's username to be changed (bug #105632)

* Tue Sep 23 2003 Brent Fox <bfox@redhat.com> 1.2.1-1
- rebuild with latest docs

* Tue Sep 16 2003 Brent Fox <bfox@redhat.com> 1.1.17-4
- bump release

* Tue Sep 16 2003 Brent Fox <bfox@redhat.com> 1.1.17-3
- turn off SELinux

* Tue Sep 16 2003 Brent Fox <bfox@redhat.com> 1.1.17-2
- bump release

* Tue Sep 16 2003 Brent Fox <bfox@redhat.com> 1.1.17-1
- if shadow passwords are not enabled, do not show certain widgets (bug #104536)
- don't modify password expiration data by accident (bug #88190)

* Thu Sep 4 2003 Dan Walsh <dwalsh@redhat.com> 1.1.16-3
- Turn off SELinux 

* Thu Sep 4 2003 Dan Walsh <dwalsh@redhat.com> 1.1.16-2.sel
- add SELinux support

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.1.16-1
- clarify error dialog message (bug #101607)
- allow underscores and dashes in usernames and groupnames (bug #99115)

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.1.15-1
- tag on every build

* Wed Aug 13 2003 Brent Fox <bfox@redhat.com> 1.1.14-2
- bump relnum and rebuild

* Wed Aug 13 2003 Brent Fox <bfox@redhat.com> 1.1.14-1
- use UTC instead of GMT (bug #102251)

* Wed Aug 13 2003 Brent Fox <bfox@redhat.com> 1.1.13-1
- add BuildRequires on gettext

* Wed Jul 23 2003 Brent Fox <bfox@redhat.com> 1.1.12-2
- bump relnum and rebuild

* Wed Jul 23 2003 Brent Fox <bfox@redhat.com> 1.1.12-1
- use GMT time on password last changed (bug #89759)

* Wed Jul 23 2003 Brent Fox <bfox@redhat.com> 1.1.11-2
- bump relnum and rebuild

* Wed Jul 23 2003 Brent Fox <bfox@redhat.com> 1.1.11-1
- don't create new user with an existing uid (bug #90911)
- use the messageDialog module in groupWindow.py
- don't create group with an existing gid (bug #90911)

* Fri Jul 11 2003 Brent Fox <bfox@redhat.com> 1.1.10-2
- bump relnum and rebuild

* Fri Jul 11 2003 Brent Fox <bfox@redhat.com> 1.1.10-1
- display an error if no X server is running (bug #97148)

* Mon Jun  2 2003 Brent Fox <bfox@redhat.com> 1.1.9-1
- popup a confirmation dialog when deleting groups
- popup a confirmation dialog when deleting users

* Tue May 27 2003 Brent Fox <bfox@redhat.com> 1.1.8-1
- don't require a full user name (bug #91718)

* Fri May 23 2003 Brent Fox <bfox@redhat.com> 1.1.7-1
- don't allow colons in username or homedir names (bug #90481)
- check for zero length in usernames, groupnames, gecos, and homedirs

* Thu May 22 2003 Brent Fox <bfox@redhat.com> 1.1.6-1
- change label in glade file (bug #86323)

* Mon May 19 2003 Brent Fox <bfox@redhat.com> 1.1.5-9
- create a 'users' group if a new user is getting added to a non-existing users group (bug #89895)

* Thu Apr  3 2003 Brent Fox <bfox@redhat.com> 1.1.5-8
- don't automatically delete system groups (bug #78620)

* Wed Feb 26 2003 Jeremy Katz <katzj@redhat.com> 1.1.5-7
- use rm for rmrf instead (#85175)

* Mon Feb 17 2003 Brent Fox <bfox@redhat.com> 1.1.5-6
- update desktop file (bug #84360)

* Thu Feb 13 2003 Brent Fox <bfox@redhat.com> 1.1.5-5
- make double-click launch properties box (#84231)

* Tue Feb 11 2003 Brent Fox <bfox@redhat.com> 1.1.5-4
- call self.rmrf

* Mon Feb 10 2003 Brent Fox <bfox@redhat.com> 1.1.5-3
- rebuild to pull in fix for bug #83341

* Fri Feb  7 2003 Brent Fox <bfox@redhat.com> 1.1.5-2
- fix bug #83341 for real this time

* Wed Feb  5 2003 Brent Fox <bfox@redhat.com> 1.1.5-1
- don't allow root account to be locked
- make default values for SHADOW* on user creation so we can do password aging properly later

* Tue Feb  4 2003 Brent Fox <bfox@redhat.com> 1.1.4-2
- fix bug with deleting user homeDir (bug #83341)

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.1.4-1
- bump and build

* Fri Jan 24 2003 Brent Fox <bfox@redhat.com> 1.1.3-4
- better error checking for user names and group names (bug #82607)
* Thu Jan 16 2003 Brent Fox <bfox@redhat.com> 1.1.3-3
- force ascii input (bug #74058)
- make sure that groupWindow calls self.ready() after showing dialogs
- remove sentence hacks in asciiCheck.py (bug #82015)
- fix typo in about box (bug #82016)
- do not create homedir if they user doesn't want to
- only offer to delete homedirs if they actually exist  (bug #78619)
- if the user is the only member of their group, delete the group automatically (bug #78620)
* Wed Jan 15 2003 Brent Fox <bfox@redhat.com> 1.1.3-1
- don't try to delete the group if the groupEnt is None (bug #68950)
* Tue Jan 14 2003 Tammy Fox <tfox@redhat.com> 1.1.2-2
- update help with new screenshots and tweak content
* Wed Jan  8 2003 Brent Fox <bfox@redhat.com> 1.1.2-1
- ask if they want to delete the home directory
* Tue Jan  7 2003 Brent Fox <bfox@redhat.com> 1.1.1-9
- handle window delete-events correctly
- call ready() after dialog is destroyed (bug #80625)
- if a deleted user is the only user in his primary group, offer to delete the group too (bug #78620)
* Mon Dec 23 2002 Brent Fox <bfox@redhat.com> 1.1.1-6
- replace some message dialogs with calls to show_message_dialog
* Tue Dec 17 2002 Brent Fox <bfox@redhat.com> 1.1.1-5
- Do a lot of input checking on the userWindow.py and userProperties.py (bug #79246)
* Tue Nov 12 2002 Brent Fox <bfox@redhat.com> 1.1.1-4
- Rebuild with latest translations
* Thu Oct 10 2002 Brent Fox <bfox@redhat.com> 1.1.1-3
- Make the upper limit on UIDs and GIDs  (pow(2, 32)).  Fixes bug 75605
* Mon Sep 16 2002 Brent Fox <bfox@redhat.com>
- groupWindow.py, groupProperties.py, userProperties.py...desensitize when performing actions
* Fri Sep 13 2002 Brent Fox <bfox@redhat.com>
- Make the window insensitive when adding a user to prevent double clicks
* Tue Sep 10 2002 Brent Fox <bfox@redhat.com> 
- Applied patch for Norwegian translation to desktop file
* Tue Sep 03 2002 Brent Fox <bfox@redhat.com> 1.1.1-2
- Pull in latest translations
* Thu Aug 29 2002 Brent Fox <bfox@redhat.com> 1.1.1-1
- Pull in latest translations
* Thu Aug 22 2002 Dan Walsh <dwalsh@redhat.com> 1.1-16
- Fix traceback bug, caused by unitialized variable
- Fix traceback bug, caused by primary gid missing from /etc/group
* Tue Aug 20 2002 Brent Fox <bfox@redhat.com> 1.1-15
- Convert desktop file to UTF8
- Pull in new translations into desktop file
* Mon Aug 19 2002 Brent Fox <bfox@redhat.com> 1.1-14
- Apply patch from twaugh to fix bug 68778
- Change widths on account expriation widgets
* Thu Aug 15 2002 Brent Fox <bfox@redhat.com> 1.1-13
- enlarge window startup size for verbose langs
* Wed Aug 14 2002 Brent Fox <bfox@redhat.com> 1.1-12
- rebuild to pull in latest translations
* Mon Aug 12 2002 Tammy Fox <tfox@redhat.com> 1.1-11
- replace System with SystemSetup in desktop file categories
* Wed Aug 07 2002 Tammy Fox <tfox@redhat.com>
- UI tweaks
* Tue Aug 06 2002 Brent Fox <bfox@redhat.com> 1.1-10
- Increase default window size
* Tue Aug 06 2002 Brent Fox <bfox@redhat.com> 1.1-9
- fix bug 70783
* Fri Aug 02 2002 Brent Fox <bfox@redhat.com> 1.1-8
- Use new pam timestamp rules
* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 1.1-7
- Use new icons from garrett
* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 1.1-6
- Fix glade path typo
* Wed Jul 24 2002 Tammy Fox <tfox@redhat.com> 1.1-4
- Fix desktop file (bug #69488)
* Thu Jul 18 2002 Karsten Hopp <karsten@redhat.de> 1.1-3
- prepare for new pygtk2
* Sat Jul 13 2002 Brent Fox <bfox@redhat.com> 1.1-2
- Fixed bug #68639
- Make properties and delete widgets desensitized when necessary
* Fri Jul 12 2002 Tammy Fox <tfox@redhat.com>
- Updated docs
- Moved desktop file to /usr/share/applications only
* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 1.1-1
- Add a "Search Filter" label
* Fri Jun 14 2002 Brent Fox <bfox@redhat.com> 1.0.2-3
- Typo bug on my part
* Thu Jun 13 2002 Brent Fox <bfox@redhat.com> 1.0.2-1
- Use spiffy new toolbar icons
* Thu May 2 2002 Brent Fox <bfox@redhat.com> 1.0.1-7
- Update translations
* Mon Apr 22 2002 Brent Fox <bfox@redhat.com> 1.0.1-6
- Bring in the latest translations and rebuild in the latest dist
* Thu Apr 18 2002 Nalin Dahyabhai <nalin@redhat.com> 1.0.1-5
- Convert .mo files to UTF-8 at install-time, fixing #63815 correctly (probably)
* Thu Apr 18 2002 Nalin Dahyabhai <nalin@redhat.com> 1.0.1-4
- Don't bail on LookupErrors when recoding strings, just punt (#63815)
* Tue Apr 16 2002 Nalin Dahyabhai <nalin@redhat.com> 1.0.1-3
- Don't bail on IOErrors when saving preferences to /etc/sysconfig
* Tue Apr 16 2002 Brent Fox <bfox@redhat.com> 1.0.1-2
- Add set_transient_for calls to bring dialogs to the front in KDE (#61590)
* Tue Apr 16 2002 Nalin Dahyabhai <nalin@redhat.com> 1.0.1-1
- Handle cases where translations are encoded in non-UTF8 encodings (#63334)
* Mon Apr 15 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.0-14
- Update translations
* Tue Apr 09 2002 Brent Fox <bfox@redhat.com>
- Added workaround for bug 62919
* Fri Apr 05 2002 Brent Fox <bfox@redhat.com>
- Added changes to use a new icon
* Thu Mar 28 2002 Brent Fox <bfox@redhat.com>
- Finished port to Python2.2/Gtk2
* Wed Feb 27 2002 Brent Fox <bfox@redhat.com> 
- Added sortable columns
* Fri Jan 25 2002 Nalin Dahyabhai <nalin@redhat.com> 0.9.2-7
- rebuild for completeness
* Wed Aug 29 2001 Brent Fox <bfox@redhat.com>
- Fixed desktop file problem
- Nakai added Japanese support to the desktop file
* Thu Aug 9 2001 Tammy Fox <tfox@redhat.com>
- added documentation
* Thu Aug 9 2001 Nalin Dahyabhai <nalin@redhat.com>
- Attempt to minimize enumerations where possible
- Always use defined constants for attribute names
* Thu Aug 9 2001 Brent Fox <bfox@redhat.com>
- fixes for password aging
* Wed Aug  8 2001 Nalin Dahyabhai <nalin@redhat.com>
- byte-compile python modules in %%install and include them in the package
* Wed Jul 26 2001 Yukihiro Nakai <ynakai@redhat.com>
- Add Japanese translation
* Wed Jul 26 2001  Yukihiro Nakai <ynakai@redhat.com>
- Directory restructure for i18n
* Tue Jul 10 2001 Brent Fox <bfox@redhat.com>
- some glade fixups and packaging work
* Mon Jul 09 2001 Tammy Fox <tfox@redhat.com>
- added usermode files to spec file
* Tue Jul 05 2001 Brent Fox <bfox@redhat.com>
- initial packaging
* Wed Jun 13 2001 Brent Fox <bfox@redhat.com>
- mainWindow.glade: Changed GUI per jrb's recommendations
- mainWindow.py: added interfacing with libuser backend
* Wed Jun 6 2001 Jonathan Blandford  <jrb@redhat.com>
- mainWindow.glade: Cleaned up glade file a bunch.
- mainWindow.py: modified to deal with updated glade.


