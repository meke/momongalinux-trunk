%global momorel 5
%global srcname libgmail
%global pythonver 2.7
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Library to provide access to Gmail via Python
Name: python-libgmail
Version: 0.1.11
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Languages
URL: http://libgmail.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{srcname}/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python >= %{pythonver}
Requires: python-mechanize
BuildRequires: coreutils
BuildRequires: python-devel >= %{pythonver}
BuildRequires: python-mechanize
BuildRequires: python-setuptools
BuildArch: noarch 

%description
Library to provide access to Gmail via Python.

%prep
%setup -q -n %{srcname}-%{version}

%build
python setup.py build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install -O1 --skip-build --root=%{buildroot}

# fix up permission
chmod 755 %{buildroot}%{python_sitelib}/libgmail.py

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGELOG COPYING README
%{python_sitelib}/gmail_transport.py*
%{python_sitelib}/lgconstants.py*
%{python_sitelib}/%{srcname}-%{version}-py?.?.egg-info
%{python_sitelib}/%{srcname}.py*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.11-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.11-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.11-1m)
- initial package for fuse-gmailfs
