%global momorel 1

Summary: D-BUS message bus glib
Name: dbus-glib
Version: 0.98
Release: %{momorel}m%{?dist}
URL: http://www.freedesktop.org/Software/dbus
Source0: http://dbus.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: dbus-bus-introspect.xml

Patch10: http://ftp.gnome.org/pub/GNOME/teams/releng/2.16.2/dbus-glib-build.patch
License: "AFL" and GPLv2+
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: libxml2-devel >= 2.6.30
BuildRequires: dbus-devel >= 1.2.3
BuildRequires: xmlto
BuildRequires: doxygen

%description
A core concept of the D-BUS implementation is that "libdbus" is
intended to be a low-level API, similar to Xlib. Most programmers are
intended to use the bindings to GLib, Qt, Python, Mono, Java, or
whatever. These bindings have varying levels of completeness.

%package devel
Summary: D-BUS message bus glib devel
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
dbus-glib devel

%prep
%setup -q

%patch10 -p1 -b .gnome-2.16.2

%build
%configure --enable-doxygen-docs \
    --program-prefix="" \
    --enable-gtk-doc \
    --enable-bash-completion \
    --enable-silent-rules \
    --disable-static \
    --with-introspect-xml=%{SOURCE1}
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HACKING NEWS README
%{_sysconfdir}/bash_completion.d/dbus-bash-completion.sh
%{_bindir}/dbus-binding-tool
%{_libdir}/libdbus-glib-1.so.*
%exclude %{_libdir}/libdbus-glib-1.la
%{_libexecdir}/dbus-bash-completion-helper
%{_mandir}/man1/dbus-binding-tool.1.*

%files devel
%defattr(-,root,root)
%{_includedir}/dbus-1.0/dbus/dbus-glib-bindings.h
%{_includedir}/dbus-1.0/dbus/dbus-glib-lowlevel.h
%{_includedir}/dbus-1.0/dbus/dbus-glib.h
%{_includedir}/dbus-1.0/dbus/dbus-gtype-specialized.h
%{_includedir}/dbus-1.0/dbus/dbus-gvalue-parse-variant.h
%{_libdir}/libdbus-glib-1.so
%{_libdir}/pkgconfig/dbus-glib-1.pc
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.94-1m)
- update 0.94

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.92-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.88-2m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.86-3m)
- full rebuild for mo7 release

* Sat Aug 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.86-2m)
- [SECURITY] CVE-2010-1172
- import upstream patches (Patch1,2)

* Fri Jun  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.86-1m)
- update to 0.86

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.82-4m)
- use BuildRequires

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.82-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.82-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.76-2m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.76-1m)
- update to 0.76
- comment out patch0 patch1 patch2 patch3 patch4

* Sat May 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.74-4m)
- import Fedora Patches. need NetworkManager...

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.74-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.74-2m)
- %%NoSource -> NoSource

* Tue Oct 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.74-1m)
- update to 0.74

* Fri Mar  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.73-1m)
- update to 0.73

* Tue Feb 20 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.72-2m)
- add BuildPrereq: doxygen

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Wed Jan 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.71-3m)
- add patch for gnome-2.16.2

* Thu Jan  4 2007 zunda <zunda at fresehll.org>
- (kossori)
- added BuildPrereq: xmlto which is checked by the configure script

* Sat Sep  2 2006  Nishio Futoshi <futoshi@momonga-linux.org>
- (0.71-2m)
- add %%post %%postun script

* Thu Aug 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.71-1m)
- initial build
