%global momorel 5

Summary: A GNU tool which simplifies the build process for users
Name: make
Version: 3.82
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
URL: http://www.gnu.org/software/make/
Source0: ftp://ftp.gnu.org/pub/gnu/make/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch1: make-3.82-noclock_gettime.patch
Patch2: make-3.82-j8k.patch
Patch3: make-3.82-getcwd.patch
Patch4: make-3.82-err-reporting.patch
Patch6: make-3.82-weird-shell.patch
Patch7: make-3.82-newlines.patch
Patch8: make-3.82-jobserver.patch
Patch9: make-3.82-bugfixes.patch
Patch10: make-3.82-sort-blank.patch
Patch11: make-3.82-copy-on-expand.patch
Patch12: make-3.82-parallel-remake.patch
Patch13: make-3.82-warn_undefined_function.patch
Patch14: make-3.82-trace.patch
Patch15: make-3.82-expensive_glob.patch
Patch16: make-3.82-dont-prune-intermediate.patch
Patch17: make-3.82-aarch64.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): info
Requires(preun): info
Provides: %{_bindir}/%{name}

%description
A GNU tool for controlling the generation of executables and other
non-source files of a program from the program's source files. Make
allows users to build and install packages without any significant
knowledge about the details of the build process. The details about
how the program should be built are provided for make in the program's
makefile.

The GNU make tool should be installed on your system because it is
commonly used to simplify the process of installing programs.

%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p0
%patch13 -p2
%patch14 -p1
%patch15 -p0
%patch16 -p0
%patch17 -p1
rm -f tests/scripts/features/parallelism.orig


%build
autoreconf -vfi
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
ln -sf make %{buildroot}/%{_bindir}/gmake
rm -f %{buildroot}/%{_infodir}/dir

%check
echo ============TESTING===============
/usr/bin/env LANG=C make check
echo ============END TESTING===========

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/install-info %{_infodir}/make.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/make.info %{_infodir}/dir || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README*
%{_bindir}/make
%{_bindir}/gmake
%{_mandir}/man1/make.1*
%{_infodir}/make.info*
%{_datadir}/locale/*/LC_MESSAGES/make.mo

%changelog
* Wed May  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.82-4m)
- add patches. import from fedora

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.87-4m)
- Provides: %%{_bindir}/%%{name}

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.82-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.82-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.82-1m)
- update 3.82

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.81-14m)
- full rebuild for mo7 release

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.81-13m)
- fix tests/scripts/features/recursion

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.81-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.81-11m)
- merge the following Rawhide changes to enable to build xulrunner,
  thunderbird, seamonkey and etc. on glibc-2.10.90.x86_64
-- https://bugzilla.redhat.com/show_bug.cgi?id=514721
-- * Fri Jul 31 2009 Petr Machata <pmachata@redhat.com> - 1:3.81-17
-- - Replace the use of strcpy on overlapping areas with memmove
-- - Resolves: #514721

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.81-10m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.81-9m)
- import Patch11 from Rawhide (1:3.81-14)
- update Patch0,5,6 for fuzz=0

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.81-8m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.81-7m)
- remove --entry option of install-info

* Wed Jul  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.81-6m)
- add make-3.81-jobserver.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.81-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.81-4m)
- %%NoSource -> NoSource

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.81-3m)
- fix %%changelog section
 
* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.81-2m)
- one more try version up
- import many patches from Fedora

* Fri May  5 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.80-5m)
- version down 3.80
-- because any package cannot do build

* Wed May  3 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.81-1m)
- update 3.81

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.80-4m)
- remove Epoch

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (3.80-3m)
- revised spec for enabling rpm 4.2.

* Sun Mar 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.80-2m)
- arrange URL tag
- use macros

* Sat Oct 05 2002 Kenta MURATA <muraken2@nifty.com>
- (3.80-1m)
- update to 3.80.

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (3.79.1-14k)
- rebuild against gettext 0.10.40.

* Mon Oct 15 2001 Masaru Sato <masachan@kondara.org>
- add url tag

* Sat Apr 28 2001 Toru Hoshina <toru@df-usa.com>
- (3.79.1-12k)
- no more pachinko.

* Tue Mar 13 2001 AYUHANA Tomonori <l@kondara.org>
- (3.79.1-10k)
- from Jirai 11k
- %files grob /usr/share/locale/*/LC_MESSAGES/make.mo

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Mon Jun 26 2000 AYUHANA Tomonori <l@kondara.org>
- (3.79.1-2k)
- use %makeinstall

* Mon Jun 26 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- (3.79.1-1k)
- up to ver 3.79.1

* Thu Apr 13 2000 Daiki Masuda <dyky@df-usa.com>
- Version up 3.79

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (3.78.1-4).

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Thu Feb 24 2000 Masako Hattori <maru@kondara.org>
- add man-paged-ja-GNU_make-20000115

* Thu Feb 24 2000 Cristian Gafton <gafton@redhat.com>
- add patch from Andreas Jaeger to fix dtype lookups (for glibc 2.1.3
  builds)

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man page.

* Fri Jan 21 2000 Cristian Gafton <gafton@redhat.com>
- apply patch to fix a /tmp race condition from Thomas Biege
- simplify %%install

* Sat Nov 27 1999 Jeff Johnson <jbj@redhat.com>
- update to 3.78.1.

* Thu Apr 15 1999 Bill Nottingham <notting@redhat.com>
- added a serial tag so it upgrades right

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Wed Sep 16 1998 Cristian Gafton <gafton@redhat.com>
- added a patch for large file support in glob
 
* Tue Aug 18 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.77

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 16 1997 Donnie Barnes <djb@redhat.com>
- udpated from 3.75 to 3.76
- various spec file cleanups
- added install-info support

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
