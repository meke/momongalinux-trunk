%global momorel 2

%global rsyslog_statedir %{_sharedstatedir}/rsyslog
%global rsyslog_pkidir %{_sysconfdir}/pki/rsyslog

%global want_hiredis 0
%global want_mongodb 0
%global want_rabbitmq 0
%global want_lognorm 0

Summary: Enhanced system logging and kernel message trapping daemons
Name: rsyslog
Version: 7.4.2
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: System Environment/Daemons
URL: http://www.rsyslog.com/
Source0: http://www.rsyslog.com/files/download/rsyslog/%{name}-%{version}.tar.gz
NoSource: 0
Source2: rsyslog.conf
Source3: rsyslog.sysconfig
Source4: rsyslog.log
# tweak the upstream service file to honour configuration from /etc/sysconfig/rsyslog
Patch0: rsyslog-7.4.1-sd-service.patch
Patch1: rsyslog-7.2.2-manpage-dbg-mode.patch
# prevent modification of trusted properties (proposed upstream)
Patch2: rsyslog-7.2.1-msg_c_nonoverwrite_merge.patch
# merged upstream
Patch3: rsyslog-7.3.15-imuxsock-warning.patch
BuildRequires: zlib-devel
BuildRequires: autoconf automake
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: libestr-devel >= 0.1.5
BuildRequires: librelp-devel >= 1.0.7
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: libee-devel >= 0.4.0
Requires: logrotate >= 3.5.2
Requires: bash >= 2.0
Requires(post): chkconfig coreutils systemd-units
Requires(preun): chkconfig initscripts systemd-units
Requires(postun): initscripts systemd-units
Provides: syslog
Provides: sysklogd
Obsoletes: sysklogd
Provides: rsyslog-sysvinit
Obsoletes: rsyslog-sysvinit
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%package crypto
Summary: Encryption support
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: libgcrypt-devel

%package doc
Summary: Documentation for rsyslog
Group: Documentation

%package elasticsearch
Summary: ElasticSearch output module for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: libcurl-devel

%if %{want_hiredis}
%package hiredis
Summary: Redis support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: hiredis-devel
%endif

%package mmjsonparse
Summary: JSON enhanced logging support
Group: System Environment/Daemons
Requires: %name = %version-%release

%if %{want_lognorm}
%package mmnormalize
Summary: Log normalization support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: libestr-devel libee-devel liblognorm-devel
%endif

%package mmaudit
Summary: Message modification module supporting Linux audit format
Group: System Environment/Daemons
Requires: %name = %version-%release

%package mmsnmptrapd
Summary: Message modification module for snmptrapd generated messages
Group: System Environment/Daemons
Requires: %name = %version-%release

%package libdbi
Summary: Libdbi database support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: libdbi-devel

%package mysql
Summary: MySQL support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: mysql-devel >= 4.0

%if %{want_mongodb}
%package mongodb
Summary: MongoDB support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: libmongo-client-devel
%endif

%package pgsql
Summary: PostgresSQL support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: postgresql-devel

%if %{want_rabbitmq}
%package rabbitmq
Summary: RabbitMQ support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: librabbitmq-devel >= 0.2
%endif

%package gssapi
Summary: GSSAPI authentication and encryption support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: krb5-devel

%package relp
Summary: RELP protocol support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: librelp-devel >= 1.0.3

%package gnutls
Summary: TLS protocol support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: gnutls-devel

%package snmp
Summary: SNMP protocol support for rsyslog
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: net-snmp-devel

%package udpspoof
Summary: Provides the omudpspoof module
Group: System Environment/Daemons
Requires: %name = %version-%release
BuildRequires: libnet-devel

%description
Rsyslog is an enhanced, multi-threaded syslog daemon. It supports MySQL,
syslog/TCP, RFC 3195, permitted sender lists, filtering on any message part,
and fine grain output format control. It is compatible with stock sysklogd
and can be used as a drop-in replacement. Rsyslog is simple to set up, with
advanced features suitable for enterprise-class, encryption-protected syslog
relay chains.

%description crypto
This package containes a module providing log file encryption and a
command line tool to process encrypted logs.

%description doc
This subpackage contains documentation for rsyslog.

%description elasticsearch
This module provides the capability for rsyslog to feed logs directly into
Elasticsearch.

%if %{want_hiredis}
%description hiredis
This module provides output to Redis.
%endif

%description mmjsonparse
This module provides the capability to recognize and parse JSON enhanced
syslog messages.

%if %{want_lognorm}
%description mmnormalize
This module provides the capability to normalize log messages via liblognorm.
%endif

%description mmaudit
This module provides message modification supporting Linux audit format
in various settings.

%description mmsnmptrapd
This message modification module takes messages generated from snmptrapd and
modifies them so that they look like they originated from the read originator.

%description libdbi
This module supports a large number of database systems via
libdbi. Libdbi abstracts the database layer and provides drivers for
many systems. Drivers are available via the libdbi-drivers project.

%description mysql
The rsyslog-mysql package contains a dynamic shared object that will add
MySQL database support to rsyslog.

%if %{want_mongodb}
%description mongodb
The rsyslog-mongodb package contains a dynamic shared object that will add
MongoDB database support to rsyslog.
%endif

%description pgsql
The rsyslog-pgsql package contains a dynamic shared object that will add
PostgreSQL database support to rsyslog.

%if %{want_rabbitmq}
%description rabbitmq
This module allows rsyslog to send messages to a RabbitMQ server.
%endif

%description gssapi
The rsyslog-gssapi package contains the rsyslog plugins which support GSSAPI
authentication and secure connections. GSSAPI is commonly used for Kerberos
authentication.

%description relp
The rsyslog-relp package contains the rsyslog plugins that provide
the ability to receive syslog messages via the reliable RELP
protocol.

%description gnutls
The rsyslog-gnutls package contains the rsyslog plugins that provide the
ability to receive syslog messages via upcoming syslog-transport-tls
IETF standard protocol.

%description snmp
The rsyslog-snmp package contains the rsyslog plugin that provides the
ability to send syslog messages as SNMPv1 and SNMPv2c traps.

%description udpspoof
This module is similar to the regular UDP forwarder, but permits to
spoof the sender address. Also, it enables to circle through a number
of source ports.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

%build
export CFLAGS="$RPM_OPT_FLAGS -DSYSLOGD_PIDNAME=\\\"syslogd.pid\\\""
# we have to modify LDFLAGS because
# our postgresql-devel-8.4.4-1m still has a problem in ImplicitDSOLinking
export LDFLAGS=`pg_config --ldflags`
%configure \
        --disable-static \
        --disable-testbench \
        --enable-elasticsearch \
        --enable-gnutls \
        --enable-gssapi-krb5 \
        --enable-imdiag \
        --enable-imfile \
        --enable-imjournal \
        --enable-impstats \
        --enable-imptcp \
        --enable-libdbi \
        --enable-mail \
        --enable-mmanon \
        --enable-mmaudit \
        --enable-mmjsonparse \
%if %{want_lognorm}
        --enable-mmnormalize \
%endif
        --enable-mmsnmptrapd \
        --enable-mysql \
%if %{want_hiredis}
        --enable-omhiredis \
%endif
        --enable-omjournal \
%if %{want_mongodb}
        --enable-ommongodb \
%endif
        --enable-omprog \
%if %{want_rabbitmq}
        --enable-omrabbitmq \
%endif
        --enable-omstdout \
        --enable-omudpspoof \
        --enable-omuxsock \
        --enable-pgsql \
        --enable-pmaixforwardedfrom \
        --enable-pmcisconames \
        --enable-pmlastmsg \
        --enable-pmrfc3164sd \
        --enable-pmsnare \
        --enable-relp \
        --enable-snmp \
        --enable-unlimited-select \
        --enable-usertools \

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

install -d -m 755 %{buildroot}%{_sysconfdir}/sysconfig
install -d -m 755 %{buildroot}%{_sysconfdir}/logrotate.d
install -d -m 755 %{buildroot}%{_sysconfdir}/rsyslog.d
install -d -m 700 %{buildroot}%{rsyslog_statedir}
install -d -m 700 %{buildroot}%{rsyslog_pkidir}

install -p -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/rsyslog.conf
install -p -m 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/sysconfig/rsyslog
install -p -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/logrotate.d/syslog

# get rid of *.la
rm -f %{buildroot}%{_libdir}/rsyslog/*.la
# get rid of socket activation by default
sed -i '/^Alias/s/^/;/;/^Requires=syslog.socket/s/^/;/' %{buildroot}%{_unitdir}/rsyslog.service

%clean
rm -rf %{buildroot}

%post
for n in /var/log/{messages,secure,maillog,spooler}
do
        [ -f $n ] && continue
        umask 066 && touch $n
done
%systemd_post rsyslog.service

%preun
%systemd_preun rsyslog.service

%postun
%systemd_postun_with_restart rsyslog.service

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING* NEWS README ChangeLog
%dir %{_libdir}/rsyslog
%dir %{_sysconfdir}/rsyslog.d
%dir %{rsyslog_statedir}
%dir %{rsyslog_pkidir}
%{_sbindir}/rsyslogd
%{_mandir}/*/*
%{_unitdir}/rsyslog.service
%config(noreplace) %{_sysconfdir}/rsyslog.conf
%config(noreplace) %{_sysconfdir}/sysconfig/rsyslog
%config(noreplace) %{_sysconfdir}/logrotate.d/syslog
# plugins
%{_libdir}/rsyslog/imdiag.so
%{_libdir}/rsyslog/imfile.so
%{_libdir}/rsyslog/imjournal.so
%{_libdir}/rsyslog/imklog.so
%{_libdir}/rsyslog/immark.so
%{_libdir}/rsyslog/impstats.so
%{_libdir}/rsyslog/imptcp.so
%{_libdir}/rsyslog/imtcp.so
%{_libdir}/rsyslog/imudp.so
%{_libdir}/rsyslog/imuxsock.so
%{_libdir}/rsyslog/lmnet.so
%{_libdir}/rsyslog/lmnetstrms.so
%{_libdir}/rsyslog/lmnsd_ptcp.so
%{_libdir}/rsyslog/lmregexp.so
%{_libdir}/rsyslog/lmstrmsrv.so
%{_libdir}/rsyslog/lmtcpclt.so
%{_libdir}/rsyslog/lmtcpsrv.so
%{_libdir}/rsyslog/lmzlibw.so
%{_libdir}/rsyslog/mmanon.so
%{_libdir}/rsyslog/omjournal.so
%{_libdir}/rsyslog/ommail.so
%{_libdir}/rsyslog/omprog.so
%{_libdir}/rsyslog/omruleset.so
%{_libdir}/rsyslog/omstdout.so
%{_libdir}/rsyslog/omtesting.so
%{_libdir}/rsyslog/omuxsock.so
%{_libdir}/rsyslog/pmaixforwardedfrom.so
%{_libdir}/rsyslog/pmcisconames.so
%{_libdir}/rsyslog/pmlastmsg.so
%{_libdir}/rsyslog/pmrfc3164sd.so
%{_libdir}/rsyslog/pmsnare.so

%files crypto
%{_bindir}/rscryutil
%{_libdir}/rsyslog/lmcry_gcry.so

%files doc
%doc doc/*html

%files elasticsearch
%defattr(-,root,root)
%{_libdir}/rsyslog/omelasticsearch.so

%if %{want_hiredis}
%files hiredis
%defattr(-,root,root)
%{_libdir}/rsyslog/omhiredis.so
%endif

%files libdbi
%defattr(-,root,root)
%{_libdir}/rsyslog/omlibdbi.so

%files mmaudit
%defattr(-,root,root)
%{_libdir}/rsyslog/mmaudit.so

%files mmjsonparse
%defattr(-,root,root)
%{_libdir}/rsyslog/mmjsonparse.so

%if %{want_lognorm}
%files mmnormalize
%defattr(-,root,root)
%{_libdir}/rsyslog/mmnormalize.so
%endif

%files mmsnmptrapd
%defattr(-,root,root)
%{_libdir}/rsyslog/mmsnmptrapd.so

%files mysql
%defattr(-,root,root)
%doc plugins/ommysql/createDB.sql
%{_libdir}/rsyslog/ommysql.so

%if %{want_mongodb}
%files mongodb
%defattr(-,root,root)
%{_bindir}/logctl
%{_libdir}/rsyslog/ommongodb.so
%endif

%files pgsql
%defattr(-,root,root)
%doc plugins/ompgsql/createDB.sql
%{_libdir}/rsyslog/ompgsql.so

%if %{want_rabbitmq}
%files rabbitmq
%defattr(-,root,root)
%{_libdir}/rsyslog/omrabbitmq.so
%endif

%files gssapi
%defattr(-,root,root)
%{_libdir}/rsyslog/lmgssutil.so
%{_libdir}/rsyslog/imgssapi.so
%{_libdir}/rsyslog/omgssapi.so

%files relp
%defattr(-,root,root)
%{_libdir}/rsyslog/imrelp.so
%{_libdir}/rsyslog/omrelp.so

%files gnutls
%defattr(-,root,root)
%{_libdir}/rsyslog/lmnsd_gtls.so

%files snmp
%defattr(-,root,root)
%{_libdir}/rsyslog/omsnmp.so

%files udpspoof
%defattr(-,root,root)
%{_libdir}/rsyslog/omudpspoof.so

%changelog
* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.4.1-2m)
- support UserMove env

* Fri Nov 29 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (7.4.1-1m)
- update 7.4.1

* Mon Jun 10 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.2.7-2m)
- add BuildRequires

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2.7-1m)
- update to 7.2.7
- rebuild against gnutls-3.2.0

* Wed Aug  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.8.12-2m)
- import and replace systemd.patch for new systemd

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.12-1m)
- update 5.8.12

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.7-2m)
- enable systemd patch

* Sat Feb  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.7-1m)
- update 5.8.7
-- fix message lost

* Sun Jan  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.6-1m)
- update 5.8.6

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.8.5-3m)
- rebuild against net-snmp-5.7.1

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.5-2m)
- split sysvinit package

* Fri Sep  9 2011 NARITA Koichi <pular@momonga-linux.org>
- (5.8.5-1m)
- [SECURITY] CVE-2011-3200
- update to 5.8.5

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.4-1m)
- update 5.8.4

* Sun Jul 24 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.8.3-2m)
- rebuild against net-snmp-5.7

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.3-1m)
- update 5.8.3
 
* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.2-2m)
- support systemd

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.2-1m)
- update to 5.8.2

* Fri May 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.1-1m)
- update to 5.8.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.7.10-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.10-1m)
- update to 5.7.10

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.9-3m)
- rebuild against net-snmp-5.6.1

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.9-2m)
- rebuild against mysql-5.5.10

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7.9-1m)
- update to 5.7.9
- add rsyslog-snmp subpackage

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-3m)
- full rebuild for mo7 release

* Sat May 22 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m) 
- fix build failure caused by implicit DSO linking issue

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-1m)
- update 4.6.2

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.21.11-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.21.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.21.11-1m)
- update to 3.21.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.21.10-3m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.21.10-2m)
- rebuild against mysql-5.1.32

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.21.10-1m)
- update to 3.21.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.21.9-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.21.9-1m)
- [SECURITY] CVE-2008-5618
- update to 3.21.9

* Fri Dec  5 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.21.3-1m)
- [SECURITY] CVE-2008-5617
- [SECURITY] RSyslog "AllowedSender" Security Bypass Vulnerability
- - http://secunia.com/Advisories/32857/
- update 3.21.3
- sync with fedora 3.21.3-4

* Mon Aug 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.18.1-1m)
- update 3.18.1

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.16.1-1m)
- update 3.16.1

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.1-2m)
- rebuild against openssl-0.9.8h-1m

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.14.1-1m)
- version 3.14.1
- fix up starting order of services at boot up a system
- do the following 3 commands after installing this package
- sudo /sbin/chkconfig messagebus resetpriorities
- sudo /sbin/chkconfig haldaemon resetpriorities
- sudo /sbin/chkconfig NetworkManager resetpriorities
- boot up sequence is all clear now

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19.11-4m)
- rebuild against gcc43

* Thu Mar  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.19.11-3m)
- modify Provides and Obsoletes

* Wed Dec 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.19.11-2m)
- modify Requires

* Sat Dec  8 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19.11-1m)
- update

* Thu Nov  8 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19.6-1m)
- imoort from Fedora-devel to Momonga

* Wed Oct 03 2007 Peter Vrabec <pvrabec@redhat.com> 1.19.6-3
- remove NUL character from recieved messages

* Tue Sep 25 2007 Tomas Heinrich <theinric@redhat.com> 1.19.6-2
- fix message suppression (303341)

* Tue Sep 25 2007 Tomas Heinrich <theinric@redhat.com> 1.19.6-1
- upstream bugfix release

* Tue Aug 28 2007 Peter Vrabec <pvrabec@redhat.com> 1.19.2-1
- upstream bugfix release
- support for negative app selector, patch from 
  theinric@redhat.com

* Fri Aug 17 2007 Peter Vrabec <pvrabec@redhat.com> 1.19.0-1
- new upstream release with MySQL support(as plugin)

* Wed Aug 08 2007 Peter Vrabec <pvrabec@redhat.com> 1.18.1-1
- upstream bugfix release

* Mon Aug 06 2007 Peter Vrabec <pvrabec@redhat.com> 1.18.0-1
- new upstream release

* Thu Aug 02 2007 Peter Vrabec <pvrabec@redhat.com> 1.17.6-1
- upstream bugfix release

* Mon Jul 30 2007 Peter Vrabec <pvrabec@redhat.com> 1.17.5-1
- upstream bugfix release
- fix typo in provides 

* Wed Jul 25 2007 Jeremy Katz <katzj@redhat.com> - 1.17.2-4
- rebuild for toolchain bug

* Tue Jul 24 2007 Peter Vrabec <pvrabec@redhat.com> 1.17.2-3
- take care of sysklogd configuration files in %%post

* Tue Jul 24 2007 Peter Vrabec <pvrabec@redhat.com> 1.17.2-2
- use EVR in provides/obsoletes sysklogd

* Mon Jul 23 2007 Peter Vrabec <pvrabec@redhat.com> 1.17.2-1
- upstream bug fix release

* Fri Jul 20 2007 Peter Vrabec <pvrabec@redhat.com> 1.17.1-1
- upstream bug fix release
- include html docs (#248712)
- make "-r" option compatible with sysklogd config (248982)

* Tue Jul 17 2007 Peter Vrabec <pvrabec@redhat.com> 1.17.0-1
- feature rich upstream release

* Thu Jul 12 2007 Peter Vrabec <pvrabec@redhat.com> 1.15.1-2
- use obsoletes and hadle old config files

* Wed Jul 11 2007 Peter Vrabec <pvrabec@redhat.com> 1.15.1-1
- new upstream bugfix release

* Tue Jul 10 2007 Peter Vrabec <pvrabec@redhat.com> 1.15.0-1
- new upstream release introduce capability to generate output 
  file names based on templates

* Tue Jul 03 2007 Peter Vrabec <pvrabec@redhat.com> 1.14.2-1
- new upstream bugfix release

* Mon Jul 02 2007 Peter Vrabec <pvrabec@redhat.com> 1.14.1-1
- new upstream release with IPv6 support

* Tue Jun 26 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.5-3
- add BuildRequires for  zlib compression feature

* Mon Jun 25 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.5-2
- some spec file adjustments.
- fix syslog init script error codes (#245330)

* Fri Jun 22 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.5-1
- new upstream release

* Fri Jun 22 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.4-2
- some spec file adjustments.

* Mon Jun 18 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.4-1
- upgrade to new upstream release

* Wed Jun 13 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.2-2
- DB support off

* Tue Jun 12 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.2-1
- new upstream release based on redhat patch

* Fri Jun 08 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.1-2
- rsyslog package provides its own kernel log. daemon (rklogd)

* Mon Jun 04 2007 Peter Vrabec <pvrabec@redhat.com> 1.13.1-1
- Initial rpm build
