%global momorel 2

Summary: Network-related giomodules for glib
Name: glib-networking
Version: 2.34.2
Release: %{momorel}m%{?dist}
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.34/%{name}-%{version}.tar.xz
NoSource: 0
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.gnome.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glib2-devel
BuildRequires: libproxy-devel
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: libgcrypt-devel
BuildRequires: gsettings-desktop-schemas-devel
BuildRequires: intltool

%description
Network-related giomodules for glib.

%prep
%setup -q

%build
%configure --disable-static \
	--enable-silent-rules \
	--with-gnome-proxy
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %name

%clean
rm -rf --preserve-root %{buildroot}

%files -f %name.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/gio/modules/libgiognomeproxy.so
%{_libdir}/gio/modules/libgiognutls.so
%{_libdir}/gio/modules/libgiolibproxy.so
%{_libdir}/gio/modules/*.la
%{_libexecdir}/glib-pacrunner
%{_datadir}/dbus-1/services/org.gtk.GLib.PACRunner.service

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.34.2-2m)
- rebuild against gnutls-3.2.0

* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.34.2-1m)
- update to 2.34.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.34.0-1m)
- update to 2.34.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-2m)
- rebuild for glib 2.33.2

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.30.2-1m)
- update to 2.30.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.92-1m)
- update to 2.29.92

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.18-1m)
- update to 2.29.18

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.7-1m)
- update to 2.28.7

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.6.1-1m)
- update to 2.28.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.5-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.5-1m)
- initial build
