%global momorel 1

Summary: X.Org X11 libX11 runtime library
Name: libX11
Version: 1.6.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/lib/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: xorg-x11-proto-devel >= 7.2-2
BuildRequires: xorg-x11-xtrans-devel
BuildRequires: libXau-devel
BuildRequires: libXdmcp-devel
BuildRequires: libxcb-devel >= 1.2
BuildRequires: fop >= 0.95
BuildRequires: xorg-x11-sgml-doctools >= 1.7
 
BuildRequires: autoconf

# FIXME: check if still needed for X11R7
Requires(pre): filesystem >= 2.3.6-1

Requires: %{name}-common = %{version}-%{release}

%description
X.Org X11 libX11 runtime library

%package common
Summary: Common data for libX11
Group: System Environment/Libraries
BuildArch: noarch

%description common
libX11 common data

%package devel
Summary: X.Org X11 libX11 development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3
Requires: xorg-x11-proto-devel
Requires: libXau-devel, libXdmcp-devel, libxcb-devel

%description devel
X.Org X11 libX11 development package

%prep
%setup -q

%build
%configure \
    --disable-dependency-tracking \
    --with-xcb \
    --disable-static \
    --enable-specs \
    --enable-xlocaledir \
    --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

# delete file
rm -f %{buildroot}%{_libdir}/*.la

rm -rf %{buildroot}%{_docdir}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libX11.so.6
%{_libdir}/libX11.so.6.?.0
%{_libdir}/libX11-xcb.so.1
%{_libdir}/libX11-xcb.so.1.?.0

%files common
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README NEWS
%{_datadir}/X11/locale
%{_datadir}/X11/XErrorDB
%{_datadir}/X11/Xcms.txt
%{_mandir}/man5/*.5*
#%%{_datadir}/sgml/X11/dbs/

%files devel
%defattr(-,root,root,-)
%{_includedir}/X11/ImUtil.h
%{_includedir}/X11/XKBlib.h
%{_includedir}/X11/Xcms.h
%{_includedir}/X11/Xlib.h
%{_includedir}/X11/XlibConf.h
%{_includedir}/X11/Xlibint.h
%{_includedir}/X11/Xlocale.h
%{_includedir}/X11/Xregion.h
%{_includedir}/X11/Xresource.h
%{_includedir}/X11/Xutil.h
%{_includedir}/X11/cursorfont.h
%{_includedir}/X11/Xlib-xcb.h
%{_libdir}/libX11.so
%{_libdir}/libX11-xcb.so
%{_libdir}/pkgconfig/x11.pc
%{_libdir}/pkgconfig/x11-xcb.pc
%{_mandir}/man3/*.3*

%changelog
* Fri Sep 13 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.2-1m)
- update 1.6.2

* Fri Jun  7 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update 1.6.0
- [SECURITY] CVE-2013-1981, CVE-2013-1997, CVE-2013-2004

* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update 1.5.0(Xorg-7.7 katamari)

* Sat Mar 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.901-1m)
- update 1.4.99.901

* Fri Jan  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.1-1m)
- update 1.4.99.1

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.4-1m)
- update 1.4.4

* Sun Jun 26 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (1.4.3-3m)
- add BR fop, xorg-x11-sgml-doctools

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.3-1m)
- update to 1.4.3

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-1m)
- update to 1.4.2

* Wed Jan 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.6-1m)
- update to 1.3.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-4m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-3m)
- release %%dir %%{_datadir}/X11
- it's already provided by xorg-x11-filesystem
- and it will soon be provided by new filesystem
- own %%{_datadir}/X11/locale

* Sat Jul 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-2m)
- split out common package

* Fri Jun  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-1m)
- update 1.3.4

* Mon Jan 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-1m)
- update 1.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-1m)
- update 1.3.2

* Sun Oct 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Tue Oct  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3-1m)
- update 1.3

* Sat Jul 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2

* Thu Apr  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Tue Feb 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-4m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-3m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-2m)
- iiimx crash fix
- import xnextevent-fine.diff from Ubuntu-jp for ATOK X3
 +-  libx11 (2:1.1.5-2ubuntu1ja1) intrepid; urgency=low
 +-* debian/patches/xnextevent-fine.diff: added.
 +-  http://bugs.freedesktop.org/show_bug.cgi?id=17923
 +-  Thanks to hito and Justsystems.
 +-- Ikuya Awashiro <ikuya@fruitsbasket.info>  Fri, 17 Oct 2008 15:06:42 +0000
- http://d.hatena.ne.jp/hito-d/20081001

* Sat Sep 06 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-3m)
- add memleak patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-1m)
- update 1.1.4

* Sun Aug 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-2m)
- Bug Fix (BuildRequires: libxcb-devel)
- http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=190

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3
- delete patch0 (Fixes bug 11222)

* Sun Jul  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-2m)
- import libX11-1.1.2-fix-XGetMotionEvents.patch from archlinux
- http://bugs.archlinux.org/task/7429

* Thu Jun  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2

* Sat Jun  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.1-2m)
- [SECURITY] CVE-2007-1667
- add patch0 xorg-libX11-1.1.1-xinitimage.diff

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1

* Fri Nov 10 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.99.2-2m)
- add Patch libX11-1.0.99.2-dpy.patch

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.99.2-1m)
- update 1.0.99.2

* Tue Jun 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- Security advisory http://xorg.freedesktop.org/releases/X11R7.1/patches/

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- delete duplicated files

* Sat May 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-1m)
- update 1.0.1

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-3m)
- To trunk

* Tue Mar  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2.3m)
- Commentout Obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-2.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-2.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Sat Dec 24 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-2
- Added "Requires: libXau-devel, libXdmcp-devel" to -devel subpackage (#176313)

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated libX11 to version 1.0.0 from X11R7 RC4

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.4-1
- Updated libX11 to version 0.99.4 from X11R7 RC3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", to ensure
  that /usr/lib/X11 and /usr/include/X11 pre-exist.
- Removed 'x' suffix from manpage directories to match RC3 upstream.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Nov 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-4
- Added libX11-0.99.3-datadir-locale-dir-fix.patch, to fix build to install
  the locale data files into datadir instead of libdir. (#173282)

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.3-3
- require newer filesystem package (#172610)

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-2
- Moved _smp_mflags from 'make install' to 'make' invocation, duh.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Updated libX11 to version 0.99.3 from X11R7 RC2
- Changed 'Conflicts: XFree86-devel, xorg-x11-devel' to 'Obsoletes'
- Changed 'Conflicts: XFree86-libs, xorg-x11-libs' to 'Obsoletes'

* Mon Nov 7 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Fix devel subpackage summary and description with s/libXdmcp/libX11/

* Fri Oct 21 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated to libX11-0.99.2 from the X11R7 RC1 release.
- Added en_GR.UTF-8 locale to file manifest.
- Forcibly remove Xcms.txt

* Sun Oct  2 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-4
- Added _smp_mflags to make invocation to speed up SMP builds

* Thu Sep 29 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-3
- Renamed package to remove xorg-x11 from the name due to unanimous decision
  between developers.
- Use Fedora Extras style BuildRoot tag.
- Disable static library creation by default.
- Add missing defattr to devel subpackage
- Add missing documentation files to doc macro

* Tue Aug 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-2
- Renamed package to prepend "xorg-x11" to the name for consistency with
  the rest of the X11R7 packages.
- Added "Requires: %%{name} = %%{version}-%%{release}" dependency to devel
  subpackage to ensure the devel package matches the installed shared libs.
- Added virtual "Provides: lib<name>" and "Provides: lib<name>-devel" to
  allow applications to use implementation agnostic dependencies.
- Added post/postun scripts which call ldconfig.
- Added Conflicts with XFree86-libs and xorg-x11-libs to runtime package,
  and Conflicts with XFree86-devel and xorg-x11-devel to devel package.

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1.1
- Added Requires: xorg-x11-proto-devel to libX11-devel subpackage

* Mon Aug 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
