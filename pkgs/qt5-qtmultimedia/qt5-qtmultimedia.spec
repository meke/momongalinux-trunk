%global momorel 1
%global qt_module qtmultimedia

# define to build docs, need to undef this for bootstrapping
# where qt5-qttools builds are not yet available
%define docs 1

Summary: Qt5 - Multimedia support
Name:    qt5-%{qt_module}
Version: 5.2.1
Release: %{momorel}m%{?dist}

# See LGPL_EXCEPTIONS.txt, LICENSE.GPL3, respectively, for exception details
License: "LGPLv2 with exceptions or GPLv3 with exceptions"
Group: System Environment/Libraries
Url: http://qt-project.org/
Source0: http://download.qt-project.org/official_releases/qt/5.2/%{version}/submodules/%{qt_module}-opensource-src-%{version}.tar.xz
NoSource: 0
# qt5-qtjsbackend (and so qt5-qtdeclarative) supports only ix86, x86_64 and arm , and so do we here
ExclusiveArch: %{ix86} x86_64 %{arm}

BuildRequires: qt5-qtbase-devel >= %{version}
BuildRequires: qt5-qtdeclarative-devel >= %{version}
BuildRequires: alsa-lib-devel
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: libXv-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: openal-soft-devel

%{?_qt5_version:Requires: qt5-qtbase%{?_isa} >= %{_qt5_version}}

%description
The Qt Multimedia module provides a rich feature set that enables you to
easily take advantage of a platforms multimedia capabilites and hardware.
This ranges from the playback and recording of audio and video content to
the use of available devices like cameras and radios.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: qt5-qtbase-devel%{?_isa}
Requires: qt5-qtdeclarative-devel%{?_isa}

%description devel
%{summary}.

%if 0%{?docs}
%package doc
Summary: API documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
# for qhelpgenerator
BuildRequires: qt5-qttools-devel
BuildArch: noarch

%description doc
%{summary}.
%endif

%package examples
Summary: Programming examples for %{name}
Group: Documentation
Requires: %{name}%{?_isa} = %{version}-%{release}

%description examples
%{summary}.

%prep
%setup -q -n %{qt_module}-opensource-src-%{version}%{?pre:-%{pre}}

%build

%{_qt5_qmake}

make %{?_smp_mflags}

%if 0%{?docs}
make %{?_smp_mflags} docs
%endif

%install
rm -rf --preserve-root %{buildroot}
make install INSTALL_ROOT=%{buildroot}

%if 0%{?docs}
make install_docs INSTALL_ROOT=%{buildroot}
%endif

## .prl file love (maybe consider just deleting these -- rex
# nuke dangling reference(s) to %%buildroot, excessive (.la-like) libs
sed -i \
  -e "/^QMAKE_PRL_BUILD_DIR/d" \
  -e "/^QMAKE_PRL_LIBS/d" \
  %{buildroot}%{_qt5_libdir}/*.prl

## unpackaged files
# .la files, die, die, die.
rm -fv %{buildroot}%{_qt5_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LGPL_EXCEPTION.txt LICENSE.GPL LICENSE.LGPL
%{_qt5_libdir}/libQt5Multimedia.so.5*
%{_qt5_libdir}/libQt5MultimediaQuick_p.so.5*
%{_qt5_libdir}/libQt5MultimediaWidgets.so.5*
%{_qt5_libdir}/libqgsttools_p.so.1*
%{_qt5_archdatadir}/qml/QtAudioEngine
%{_qt5_archdatadir}/qml/QtMultimedia
%{_qt5_plugindir}/audio
%{_qt5_plugindir}/mediaservice
%{_qt5_plugindir}/playlistformats

%files devel
%defattr(-,root,root,-)
%{_qt5_headerdir}/QtMultimedia
%{_qt5_headerdir}/QtMultimediaQuick_p
%{_qt5_headerdir}/QtMultimediaWidgets
%{_qt5_libdir}/libQt5Multimedia.so
%{_qt5_libdir}/libQt5Multimedia.prl
%{_qt5_libdir}/libQt5MultimediaQuick_p.so
%{_qt5_libdir}/libQt5MultimediaQuick_p.prl
%{_qt5_libdir}/libQt5MultimediaWidgets.so
%{_qt5_libdir}/libQt5MultimediaWidgets.prl
%{_qt5_libdir}/libqgsttools_p.so
%{_qt5_libdir}/libqgsttools_p.prl
%{_qt5_libdir}/cmake/Qt5*
%{_qt5_libdir}/pkgconfig/Qt5Multimedia.pc
%{_qt5_libdir}/pkgconfig/Qt5MultimediaQuick_p.pc
%{_qt5_libdir}/pkgconfig/Qt5MultimediaWidgets.pc
%{_qt5_archdatadir}/mkspecs/modules/*.pri

%if 0%{?docs}
%files doc
%defattr(-,root,root,-)
%{_qt5_docdir}/qtmultimedia.qch
%{_qt5_docdir}/qtmultimedia/
%{_qt5_docdir}/qtmultimediawidgets.qch
%{_qt5_docdir}/qtmultimediawidgets/
%endif

%if 0%{?_qt5_examplesdir:1}
%files examples
%defattr(-,root,root,-)
%{_qt5_examplesdir}/
%endif

%changelog
* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-1m)
- update to 5.2.1
- add doc and examples sub packages

* Wed Sep 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.1-1m)
- import from Fedora

* Sat Sep 07 2013 Rex Dieter <rdieter@fedoraproject.org> 5.1.1-2
- ExclusiveArch: %%{ix86} x86_64 %%{arm} (to match qt5-qtdeclarative)

* Thu Aug 29 2013 Rex Dieter <rdieter@fedoraproject.org> 5.1.1-1
- 5.1.1

* Wed Aug 28 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-3
- update Source URL (and refetch tarball)
- improved summary/description

* Thu May 09 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-2
- BR: qt5-qtdeclarative-devel

* Thu Apr 11 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.2-1
- 5.0.2

* Sat Feb 23 2013 Rex Dieter <rdieter@fedoraproject.org> 5.0.1-1
- first try

