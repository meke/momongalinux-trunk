%global	momorel 1
%global octpkg control

Name:           octave-%{octpkg}
Version:        2.3.50
Release: 	%{momorel}m%{?dist}
Summary:        Control systems for Octave
Group:          Applications/Engineering
License:        GPLv3+
URL:            http://octave.sourceforge.net/control/
Source0:        http://downloads.sourceforge.net/octave/%{octpkg}-%{version}.tar.gz
NoSource:	0
BuildRequires:  octave-devel  

Requires:       octave(api) = %{octave_api}
Requires(post): octave
Requires(postun): octave


Obsoletes:      octave-forge <= 20090607


%description
The Octave control systems package contains functions for analyzing
and designing automatic control systems and algorithms.

%prep
%setup -q -n %{octpkg}

%build
%octave_pkg_build

%install
rm -rf %{buildroot}
%octave_pkg_install
for i in %{octpkgdir}/doc/references.txt; do
  iconv -f iso8859-1 -t utf-8 %{buildroot}/$i > %{buildroot}/$i.conv && mv -f %{buildroot}/$i.conv %{buildroot}/$i
done;


%post
%octave_cmd pkg rebuild

%preun
%octave_pkg_preun

%postun
%octave_cmd pkg rebuild

%files
%defattr(-,root,root,-)
%{octpkglibdir}

%dir %{octpkgdir}
%doc %{octpkgdir}/doc-cache
%{octpkgdir}/*.m
%{octpkgdir}/packinfo
%doc %{octpkgdir}/packinfo/COPYING
%{octpkgdir}/@frd
%{octpkgdir}/@lti
%{octpkgdir}/@ss
%{octpkgdir}/@tf
%{octpkgdir}/@tfpoly
%doc %{octpkgdir}/doc


%changelog
* Tue Mar 20 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.3.50-1m)
- update to 2.3.50
- build against octave-3.6.1-1m

* Sat Jun 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-1m)
- initial import from fedora

