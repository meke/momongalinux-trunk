%global         momorel 1
%global         srcver 0.7.1

%global         qtver 4.8.5
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         phononver 4.7.1
%global         sourcedir stable/phonon/%{name}/%{version}

Summary:        VLC backend for Phonon
Name:           phonon-backend-vlc
Version:        %{srcver}
Release:        %{momorel}m%{?dist}
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://phonon.kde.org/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       phonon >= %{phononver}
BuildRequires:  automoc4 >= 0.9.86
BuildRequires:  cmake >= %{cmakever}
BuildRequires:  glib2-devel
BuildRequires:  libxcb-devel
BuildRequires:  libxml2-devel
BuildRequires:  phonon-devel >= %{phononver}
BuildRequires:  pkgconfig
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRequires:  vlc-devel >= 2.1.0
Requires:       phonon => %{phononver}
Requires:       qt >= %{qtver}

%description
Phonon is the Qt 4 multimedia API, which provides a task-oriented abstraction layer for
capturing, mixing, processing, and playing audio and video content.

This package contains VLC backend for Phonon.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
PATH="%{_qt4_bindir}:$PATH" %{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING.LIB
%{_libdir}/kde4/plugins/phonon_backend/phonon_vlc.so
%{_datadir}/kde4/services/phononbackends/vlc.desktop

%changelog
* Sat Dec  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Tue Nov  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Tue Oct 15 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-2m)
- rebuild against vlc-2.1.0

* Wed Jan  2 2013 NARITA koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Mon Aug 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-2m)
- rebuild for glib 2.33.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0 official

* Wed Dec 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.0-0.20111228.1m)
- update to 0.5.0 (git 20111228 snapshot)

* Mon Dec 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-2m)
- rebuild against vlc-1.2.0

* Wed Apr 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-2m)
- rebuild for new GCC 4.6

* Sat Jan 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-7m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-6m)
- specify PATH for Qt4

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-5m)
- fix build failure

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-3m)
- full rebuild for mo7 release

* Thu Jul 29 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-2m)
- add BuildRequires

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- initial build for Momonga Linux
