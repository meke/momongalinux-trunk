%global momorel 4

Name:           libopenraw
Version:        0.0.9
Release:        %{momorel}m%{?dist}
Summary:        Decode camera RAW files

Group:          System Environment/Libraries
License:        LGPL
URL:            http://%{name}.freedesktop.org/wiki
Source0:        http://%{name}.freedesktop.org/download/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  boost-devel
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  glib2-devel
BuildRequires:  gtk2-devel

%description
libopenraw is an ongoing project to provide a free software
implementation for camera RAW files decoding. One of the main reason is
that dcraw is not suited for easy integration into applications, and
there is a need for an easy to use API to build free software digital
image processing application.

%package        gnome
Summary:        GUI components of %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    gnome 
The %{name}-gnome package contains gui components of %{name}.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        gnome-devel
Summary:        Development files for %{name}-gnome
Group:          Development/Libraries
Requires:       %{name}-gnome = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       pkgconfig

%description    gnome-devel
The %{name}-gnome-devel package contains libraries and header files for
developing applications that use %{name}-gnome.

%prep
%setup -q
autoreconf -ivf

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%post gnome -p /sbin/ldconfig
%postun gnome -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README TODO 
%{_libdir}/%{name}.so.*

%files gnome
%defattr(-,root,root,-)
%{_libdir}/%{name}gnome.so.*
%{_libdir}/gdk-pixbuf-2.0/2.10.0/loaders/%{name}_pixbuf.so

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/%{name}-1.0
%{_includedir}/%{name}-1.0/%{name}
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}-1.0.pc

%files gnome-devel
%defattr(-,root,root,-)
%{_includedir}/%{name}-1.0/%{name}-gnome
%{_libdir}/%{name}gnome.so
%{_libdir}/pkgconfig/%{name}-gnome-1.0.pc

%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9-4m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.0.9-3m)
- rebuild for boost 1.50.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.9-2m)
- rebuild for glib 2.33.2

* Tue Jan  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.9-1m)
- update 0.0.9

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-15m)
- rebuild for boost-1.48.0

* Tue Aug 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.8-14m)
- update libopenraw-HEAD.
- support any camera

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-13m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-12m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.8-11m)
- update libopenraw-HEAD.
- support many camera

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-10m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-9m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-8m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-7m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.8-6m)
- full rebuild for mo7 release

* Wed Jul  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.8-5m)
- add Head patch
-- support many camera

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.8-4m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.8-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.8-1m)
- update 0.0.8

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.5-4m)
- define __libtoolize :

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.5-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.5-2m)
- rebuild against rpm-4.6

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.5-1m)
- update 0.0.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-1m)
- import from f7 to Momonga

* Thu May 03 2007 Trond Danielsen <trond.danielsen@gmail.com> - 0.0.2-5
- Added unowned directory to list of files.
- Changed license from GPL to LGPL.

* Wed May 02 2007 Trond Danielsen <trond.danielsen@gmail.com> - 0.0.2-4
- Moved gui components to a separate package.

* Tue May 01 2007 Trond Danielsen <trond.danielsen@gmail.com> - 0.0.2-3
- Added missing BuildRequirement.

* Mon Apr 30 2007 Trond Danielsen <trond.danielsen@gmail.com> - 0.0.2-2
- Added missing BuildRequirement.

* Sun Apr 29 2007 Trond Danielsen <trond.danielsen@gmail.com> - 0.0.2-1
- Inital version.
