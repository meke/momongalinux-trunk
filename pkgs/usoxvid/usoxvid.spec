%global momorel 4

Summary: A fake library of xvid
Name: usoxvid
Version: 1.0
Release: %{momorel}m%{?dist}
License: LGPLv3+
Group: Applications/Multimedia
URL: http://www.momonga-linux.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: xvid
Obsoletes: xvid

%description
Usoxvid is just a fake library of XVID.

You SHOULD install REAL xvid library if you want to use XVID codec.

%prep

%build

%install
rm -rf %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- initial packaging
