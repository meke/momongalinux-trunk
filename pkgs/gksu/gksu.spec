%global momorel 14

Summary: Gtk+ frontend to su and sudo
Name: gksu

Version: 2.0.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://www.nongnu.org/gksu/

Source0: http://people.debian.org/~kov/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: ja.po
Patch0: gksu-2.0.0-ja.patch
Patch1: gksu-2.0.0-gnome-vfs.patch
Patch2: gksu-2.0.0-nautils.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: intltool
BuildRequires: GConf2
BuildRequires: pkgconfig
BuildRequires: libgksu-devel >= 2.0.6
BuildRequires: nautilus-devel
BuildRequires: gnome-vfs2-devel >= 2.22.0
Requires: GConf2

%description
GKSu is a library that provides a Gtk+ frontend to su and sudo. It
supports login shells and preserving environment when acting as a su
frontend. It is useful to menu items or other graphical programs that
need to ask a user's password to run another program as another user.
#'

%prep
%setup -q
cp %{SOURCE1} po
%patch0 -p1 -b .ja
%patch1 -p1 -b .gnome-vfs2
%patch2 -p1 -b .nautilus
%build
autoconf -v
automake-1.9
%configure \
    --enable-nautilus-extension \
	CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot}%{_libdir} -maxdepth 1 -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/gksu
%{_bindir}/gksudo
%{_libdir}/nautilus/extensions-2.0/*
%{_datadir}/applications/gksu.desktop
%{_datadir}/%{name}
%{_datadir}/locale/*/*/*
%{_datadir}/pixmaps/gksu-*.png
%{_mandir}/man1/gksu*

%changelog
* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-14m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-11m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-10m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-8m)
- rebuild against rpm-4.6

* Sat May 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-7m)
- rebuild against libgksu 2.0.6

* Sat May 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-6m)
- enable nautilus-extension

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-5m)
- rebuild against gcc43

* Mon Jan 21 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-3m)
- added BuildPrereq: nautilus-devel

* Fri Dec 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-2m)
- add ja.po (to be continued)

* Wed Dec 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- initial build

