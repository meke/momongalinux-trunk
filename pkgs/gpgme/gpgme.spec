%global momorel 1

Summary: GnuPG Made Easy (GPGME)
Name: gpgme
Version: 1.5.0
Release: %{momorel}m%{?dist}
URL: http://www.gnupg.org/related_software/gpgme/
Source0: ftp://ftp.gnupg.org/gcrypt/gpgme/%{name}-%{version}.tar.bz2 
NoSource: 0
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Prefix: %{_prefix}
Requires: gnupg2 >= 2
BuildRequires: gnupg
BuildRequires: gnupg2 >= 2.0.15
BuildRequires: glibc >= 2.2
BuildRequires: gcc
BuildRequires: libassuan-devel >= 2.0.0
BuildRequires: libtool >= 1.4
BuildRequires: libgpg-error-devel >= 1.5-2m

%description
GnuPG Made Easy (GPGME) is a library designed to make access to GnuPG
easier for applications.

%package devel
Summary: GnuPG Made Easy (GPGME) Header files and libraries for development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libassuan-devel
Requires: info

%description devel
GnuPG Made Easy (GPGME) is a library designed to make access to GnuPG
easier for applications.

Install the gpgme-devel package if you want to develop applications 
that will use the gpgme library.

%prep
%setup -q

%build
export RPM_OPT_FLAGS="-D_FILE_OFFSET_BITS=64 $RPM_OPT_FLAGS"
%configure --enable-static \
           --with-gpg=%{_bindir}/gpg2 \
	   LIBS="-lpthread -lgpg-error -lassuan"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# remove
rm -f %{buildroot}%{_infodir}/dir

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post devel
if [ "$1" = 1 ]; then
 /sbin/install-info %{_infodir}/gpgme.info %{_infodir}/dir
fi

%preun devel
if [ "$1" = 0 ]; then
 /sbin/install-info --delete %{_infodir}/gpgme.info %{_infodir}/dir
fi


%files
%defattr(-,root,root)
%doc README ChangeLog NEWS AUTHORS COPYING COPYING.LESSER THANKS
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%doc README TODO INSTALL ChangeLog NEWS AUTHORS COPYING COPYING.LESSER THANKS
%{_bindir}/gpgme-config
%{_includedir}/*.h
%{_libdir}/*.a
%{_libdir}/*.so
%{_datadir}/aclocal/*
%{_datadir}/common-lisp/source/gpgme
%{_infodir}/gpgme*

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-6m)
- rebuild for new GCC 4.6

* Mon Dec 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-5m)
- gpgme-devel Requires libassuan-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-3m)
- full rebuild for mo7 release

* Wed Aug 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-2m)
- fix %%preun 

* Wed Jul 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-4m)
- explicitly link libgpg-error and libpthread

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.8-1m)
- update to 1.1.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-2m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7

* Tue May 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-2m)
- %%NoSource -> NoSource

* Mon Mar 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4
- [SECURITY] CVE-2007-1263

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-2m)
- delete libtool library

* Sun May 21 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.1.2-1m)
- update to 1.1.2

* Sun Jan  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-2m)
- BuildPreReq: libgpg-error -> libgpg-error-devel

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (1.0.2-1m)
- sylpheed 1.0.1 can use this version!
- GPL -> LGPL
- remove tests dir from doc,
 because the tests are done via "make"

* Fri May 07 2004 TAKAHASHI Tamotsu <tamo>
- (0.3.16-1m)

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.15-3m)
- revised spec for enabling rpm 4.2.

* Mon Nov 03 2003 TAKAHASHI Tamotsu <tamo>
- (0.3.15-2m)
- delete "Requires: newpg"
  leave "configure --with-gpgsm"
- global momorel (instead of "define")
- install-info gpgme.info (instead of "gpgme.info.*")
- use NoSource macro

* Wed Feb 19 2003 TAKAHASHI Tamotsu <tamo>
- (0.3.15-1m)
- bugfix: gpgsm assertion failure

* Thu Dec 05 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.3.14-1m)
- "This version fixes a segv and a race condition with locales.
  gpgmeplug is not anymore included but diverted to a separate
  package named cryptplug." -- Werner
- on Dec 6th, i changed URL (not Source) but didn't change Release

* Mon Dec 02 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.3.13-1m)
- bugfix
- source URI changed (alpha/gpgme -> gpgme)

* Fri Oct 25 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.3.12-1m)
- bugfix (keylisting)
- docs added to non-devel pkgs
- info added

* Sat Oct 05 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.3.11-1m)

* Wed Sep 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.3.10-1m)

* Wed Aug 21 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.3.9-1m)

* Thu Jul 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.3.8-1m)
- updated
- enabled gpgsm(in newpg pkg)

* Tue May  7 2002 WATABE Toyokazu <toy2@kondara.org>
- (0.3.6-2k)
- update to 0.3.6

* Sat Mar 23 2002 Toru Hoshina <t@kondara.org>
- (0.2.3-4k)
- va_list on alpha...

* Sat Mar  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.3-2k)
- merge from Jirai

* Tue Oct 09 2001 Motonobu Ihimura <famao@kondara.org>
- (0.2.3-3k)
- first release

