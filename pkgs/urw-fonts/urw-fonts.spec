%global momorel 6

%define filippov_version 1.0.7pre44
%define fontdir %{_datadir}/fonts/default/Type1
%define catalogue /etc/X11/fontpath.d

Summary: Free versions of the 35 standard PostScript fonts
Name: urw-fonts
Epoch: 1
Version: 2.4
Release: %{momorel}m%{?dist}
Group: User Interface/X
License: GPL+
URL: http://svn.ghostscript.com/ghostscript/tags/urw-fonts-1.0.7pre44/
Source0: %{name}-%{filippov_version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: fontpackages-filesystem
Requires(post): fontconfig xorg-x11-font-utils
Requires(postun): fontconfig xorg-x11-font-utils

%description 
Free, good quality versions of the 35 standard PostScript(TM) fonts,
donated under the GPL by URW++ Design and Development GmbH.  The
fonts.dir file font names match the original Adobe names of the fonts
(e.g., Times, Helvetica, etc.).

Install the urw-fonts package if you need free versions of standard
PostScript fonts.

%prep
%setup -q -c

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

mkdir -p %{buildroot}%{fontdir}
install -m 0644 *.afm *.pfb %{buildroot}%{fontdir}/

# Touch ghosted files
touch %{buildroot}%{fontdir}/{fonts.{dir,scale,cache-1},encodings.dir}

# Install catalogue symlink
mkdir -p %{buildroot}%{catalogue}
ln -sf %{fontdir} %{buildroot}%{catalogue}/fonts-default

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
{
   umask 133
   mkfontscale %{fontdir} || :
   `which mkfontdir` %{fontdir} || :
   fc-cache %{_datadir}/fonts
} &> /dev/null || :

%postun
{
   if [ "$1" = "0" ]; then
      fc-cache %{_datadir}/fonts
   fi
} &> /dev/null || :

%files
%defattr(0644,root,root,0755)
%doc COPYING ChangeLog README README.tweaks
%dir %{fontdir}
%{catalogue}/fonts-default
%ghost %verify(not md5 size mtime) %{fontdir}/fonts.dir
%ghost %verify(not md5 size mtime) %{fontdir}/fonts.scale
%ghost %verify(not md5 size mtime) %{fontdir}/fonts.cache-1
%ghost %verify(not md5 size mtime) %{fontdir}/encodings.dir
%{fontdir}/*.afm
%{fontdir}/*.pfb

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:2.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:2.4-4m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.4-3m)
- add epoch to %%changelog

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:2.4-1m)
- change upstream
- add Epoch: 1
- follow Fedora's font packaging guideline

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.11-8m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.11-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.11-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (8.11-5m)
- %%NoSource -> NoSource

* Sun Mar 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (8.11-4m)
- XFree86 -> xorg-x11

* Sun Mar 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.11-3m)
- change docdir %%defattr

* Tue Oct 28 2003 zunda <zunda at freeshell.org>
- (8.11-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- documets added

* Thu Aug 21 2003 Kenta MURATA <muraken2@nifty.com>
- (8.11-1m)
- version up to ghostscript-fonts-std-8.11.
- change directory to install.

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (2.0-12k)
- mkfontdir tukattendaro korua.

* Mon Aug 07 2000 Toru Hoshina <t@kondara.org> 
- Kodaraized. 

* Fri Jul 14 2000 Preston Brown <pbrown@redhat.com>
- renamed fonts from URW names to Adobe names for better compatibility

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jul 09 2000 Than Ngo <than@redhat.de>
- update gnu-gs-fonts-std-6.0
- fix Url

* Sat May 27 2000 Ngo Than <than@redhat.de>
- rebuild for 7.0

* Wed Mar 08 2000 Preston Brown <pbrown@redhat.com> 
- argh! fonts.scale shouldn't have been symlinked to fonts.dir.  fixed.

* Mon Feb 28 2000 Preston Brown <pbrown@redhat.com>
- noreplace the fonts.dir config file

* Wed Feb 16 2000 Bill Nottingham <notting@redhat.com>
- need .pfb files too

* Mon Feb 14 2000 Preston Brown <pbrown@redhat.com>
- new URW++ fonts that include extra glyphs.

* Thu Jan 13 2000 Preston Brown <pbrown@redhat.com>
- remove vendor tag.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Tue Mar 09 1999 Preston Brown <pbrown@redhat.com>
- fixed up chkfontpath stuff

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Mon Feb 15 1999 Preston Brown <pbrown@redhat.com>
- added missing fonts.dir, fonts.scale, %post, %postun using chkfontpath
- changed foundary from Adobe (which was a lie) to URW.

* Sat Feb 06 1999 Preston Brown <pbrown@redhat.com>
- fonts now live in /usr/share/fonts/default/Type1

* Fri Nov 13 1998 Preston Brown <pbrown@redhat.com>
- eliminated section that adds to XF86Config
- changed fonts to reside in /usr/share/fonts/default/URW, so they can be
  shared between X and Ghostscript (and other, future programs/applications)

* Fri Sep 11 1998 Preston Brown <pbrown@redhat.com>
- integrate adding fontdir to XF86Config

* Wed Aug 12 1998 Jeff Johnson <jbj@redhat.com>
- eliminate %post output

* Wed Jul  8 1998 Jeff Johnson <jbj@redhat.com>
- create from Stefan Waldherr <swa@cs.cmu.edu> contrib package.
