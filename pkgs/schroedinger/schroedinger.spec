%global momorel 3
%define abi 1.0


Name:           schroedinger
Version:        1.0.11
Release:        %{momorel}m%{?dist}
Summary:        Portable libraries for the high quality Dirac video codec

Group:          System Environment/Libraries
# No version is given for the GPL or the LGPL
License:        GPL+ or LGPLv2+ or MIT or MPLv1.1
URL:            http://www.diracvideo.org/
Source0:        http://www.diracvideo.org/download/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-1.0.8-gtk-doc.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  liboil-devel >= 0.3.16
BuildRequires:  glew-devel >= 1.9.0
BuildRequires:  gstreamer-devel >= 0.10
BuildRequires:  gstreamer-plugins-base-devel >= 0.10
BuildRequires:  gtk-doc
BuildRequires:  orc-devel >= 0.4.16

%description
The Schroedinger project will implement portable libraries for the high
quality Dirac video codec created by BBC Research and
Development. Dirac is a free and open source codec producing very high
image quality video.

The Schroedinger project is a project done by BBC R&D and Fluendo in
order to create a set of high quality decoder and encoder libraries
for the Dirac video codec.

%package devel
Group:          Development/Libraries
Summary:        Development files for schroedinger
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires:       liboil-devel >= 0.3.16
Requires:       orc-devel >= 0.4.6

%description devel
Development files for schroedinger

%prep
%setup -q
%patch0 -p1 -b .gtk-doc

%build
autoreconf -vif
%configure --disable-static --enable-gtk-doc

# remove rpath from libtool
sed -i.rpath 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i.rpath 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name \*.la -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING* NEWS TODO
%{_libdir}/libschroedinger-%{abi}.so.*

%files devel
%defattr(-,root,root,-)
%doc %{_datadir}/gtk-doc/html/schroedinger
%{_includedir}/schroedinger-%{abi}
%{_libdir}/*.so
%{_libdir}/pkgconfig/schroedinger-%{abi}.pc


%changelog
* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.11-3m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.11-2m)
- rebuild for glew-1.9.0

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.11-1m)
- update to 1.0.11

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-4m)
- rebuild against glew-1.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-2m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.10-1m)
- update to 1.0.10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.9-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9

* Wed Feb 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-3m)
- set NoSource: 0

* Fri Feb 19 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.8-2m)
- add gtk-doc patch

* Thu Feb 18 2010 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.0.8-1m)
- initial momonga release

* Fri Apr 24 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 1.0.7-1
- Update to 1.0.7

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.5-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Oct 29 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 1.0.5-4
- Fix some typos [BZ#469133]

* Fri Sep 12 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 1.0.5-3
- Bump release and rebuild against latest gstreamer-* packages to pick
- up special gstreamer codec provides.

* Thu Sep  4 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.0.5-2
- fix license tag

* Wed Aug 27 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 1.0.5-1
- Update to 1.0.5

* Fri Jul  2 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 1.0.3-2
- Devel subpackage needs to require liboil-devel.

* Fri Jun 27 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 1.0.3-1
- Update to 1.0.3.
- Update URLs.

* Fri Feb 22 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 1.0.0-1
- Update to 1.0.0

* Mon Feb 11 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.9.0-2
- Rebuild for GCC 4.3

* Mon Nov 12 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.9.0-1
- Update to 0.9.0

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.6.1-3
- Rebuild for selinux ppc32 issue.

* Wed Jun 20 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.6.1-2
- Fix license field
- Add pkgconfig as a requirement for the devel subpackage

* Sun Jun 10 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.6.1-1
- First version for Fedora
