%global momorel 10
%global srcver 211

Summary: The quintessential all-purpose communications program
Name: ckermit
Version: 8.0.%{srcver}
Release: %{momorel}m%{?dist}
License: see "COPYING.TXT"
Group: Applications/Communications
Source0: ftp://kermit.columbia.edu/kermit/archives/cku%{srcver}.tar.gz
Source1: cku-%{name}.local.ini
Source2: cku-%{name}.modem.generic.ini
Source3: cku-%{name}.locale.ini
Source4: cku-%{name}.phone
#Patch0: ckermit-8.0.206-errno.patch
Patch0: ckermit-avoid-open-conflict.patch
Patch1: ckermit-8.0.211-lib64.patch
URL: http://www.columbia.edu/kermit/
BuildRequires: pam-devel
BuildRequires: pkgconfig
BuildRequires: openssl-devel >= 0.9.8a
BuildRequires: gmp-devel >= 3.1.1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: gkermit

%description
C-Kermit is a combined serial and network communication software
package offering a consistent, medium-independent, cross-platform
approach to connection establishment, terminal sessions, file transfer
and management, character-set translation, and automation of
communication tasks. For more information please see:

http://www.columbia.edu/kermit/

C-KERMIT 7.0 COPYRIGHT NOTICE:

The C-Kermit license does not fall into any convenient category. It is
not commercial, not shareware, not freeware, not GPL. The terms can be
summarized as follows:

1. You may download C-Kermit without license or fee for your own use
or internal use within your company or institution.

2. You may install C-Kermit without license or fee as a service or
application on a computer within your company that is accessed by
customers or clients. This provision would apply, for example, to an
ISP or a medical claims clearinghouse.

3. You may include C-Kermit with a "Free UNIX" or other Open Source
operating-system distribution such as GNU/Linux, FreeBSD, NetBSD,
OpenBSD, etc.

4. Except as in (3), you may not sell or otherwise furnish C-Kermit as
a software product, or a component of any product, to actual or
potential customers or clients without a commercial license; to see
the commercial license terms, see http://www.columbia.edu/kermit/.

In addition, we request that those who make more than casual use of
C-Kermit purchase the published manual, Using C-Kermit. This helps
them to get the most out of the software, it reduces the load on our
help desk, and it helps to fund the Kermit Project.

The Kermit Project must fund itself entirely out of income, which
comes from software licenses, book sales, and support contracts. The
C-Kermit licensing terms are designed to be as generous and fair as
possible within this framework. Simply stated: if you just want to use
it, be our guest. If you want us to help you use it, please consult
the manual first. If you want to make a product or commodity of it,
you have to pay for it.

%prep
%setup -q -c
#%%patch0 -p1 -b .errno
%patch0 -p1 -b .open
%if %{_lib} == "lib64"
%patch1 -p1 -b .lib64~
%endif


%build
%{__make} linux %{?_smp_mflags} LIBS="-lncurses -lresolv -lcrypt"

%install
rm -rf %{buildroot}
install -d %{buildroot}{%{_bindir},%{_mandir}/man1,%{_sysconfdir}/kermit}

perl -pi -e "s|%{_prefix}/local/bin/kermit|%{_bindir}/kermit|g" ckermit.ini

#install krbmit %{buildroot}%{_bindir}/kermit
#Is this krb-kermit for RH8.0?
#
install wermit %{buildroot}%{_bindir}/kermit
# for make linux
#
install ckuker.nr %{buildroot}%{_mandir}/man1/kermit.1
install ckermit.ini %{buildroot}%{_sysconfdir}/kermit/
install %{SOURCE1} %{buildroot}%{_sysconfdir}/kermit/ckermit.local.ini
install %{SOURCE2} %{buildroot}%{_sysconfdir}/kermit/ckermit.modem.ini
install %{SOURCE3} %{buildroot}%{_sysconfdir}/kermit/ckermit.locale.ini
install %{SOURCE4} %{buildroot}%{_sysconfdir}/kermit/ckermit.phone

#gzip -9nf *.txt COPYING.TXT

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc *.txt COPYING.TXT
%dir %{_sysconfdir}/kermit
%config(noreplace) %verify(not size mtime md5) %{_sysconfdir}/kermit/*
%attr(755, root, root) %{_bindir}/kermit
%{_mandir}/man1/kermit.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.211-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.211-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.0.211-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.0.211-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.0.211-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.0.211-5m)
- rebuild against gcc43

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (8.0.211-4m)
- rebuild against openssl-0.9.8a

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (8.0.211-3m)
- enable x86_64.

* Sat Jan  8 2005 Toru Hoshina <t@momonga-linux.org>
- (8.0.211-2m)
- applied avoid-open-conflict.patch
- I'm not sure that this patch cause something bad behavior on other env.

* Fri Sep 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.0.211-1m)
- version up
- comment out included Patch0: ckermit-8.0.206-errno.patch

* Wed Sep 08 2004 TABUCHI Takaaki <tab@momonga-linux.org> 
- (8.0.206-1m)
- adapt for momonga

* Thu Jan 23 2003 Tim Powers <timp@redhat.com> 8.0.206-0.6
- rebuild

* Tue Jan 21 2003 Jeff Johnson <jbj@redhat.com> 8.0.26-0.5
- remove "CLICK HERE" from description (#82133).

* Tue Jan  7 2003 Nalin Dahyabhai <nalin@redhat.com> 8.0.206-0.4
- rebuild

* Fri Jan  3 2003 Nalin Dahyabhai <nalin@redhat.com>
- Build using predefined redhat80 target
- Pass include and library paths for Kerberos and SSL directly to make
- Define OPENSSL_097 in KFLAGS to build with OpenSSL 0.9.7

* Thu Dec 12 2002 Elliot Lee <sopwith@redhat.com> 8.0.206-0.3
- Add patch2 to include errno.h
- Change cku-makefile to not build KRB4 & KRB524, because kerberosIV/des.h
  conflicts with openssl/des.h

* Fri Nov 29 2002 Jeff Johnson <jbj@redhat.com> 8.0.206-0.2
- obsolete gkermit

* Mon Nov 25 2002 Jeff Johnson <jbj@redhat.com> 8.0.206-0.1
- create (with thanks to PLD, who packaged C-Kermit before Red Hat did).
