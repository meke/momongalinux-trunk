%global momorel 1
%define gtk3_version 3.3.6
%define glib2_version 2.31.0
%define gnome_desktop_version 2.91.2
%define gnome_icon_theme_version 2.19.1
%define desktop_file_utils_version 0.9
%define libexif_version 0.6.14

Summary: Eye of GNOME image viewer
Name:    eog
Version: 3.6.2
Release: %{momorel}m%{?dist}
URL: http://projects.gnome.org/eog/
#VCS: git:git://git.gnome.org/eog
Source: http://download.gnome.org/sources/eog/3.6/%{name}-%{version}.tar.xz
NoSource: 0
# The GFDL has an "or later version" clause embedded inside the license.
# There is no need to add the + here.
License: GPLv2+ and GFDL
Group: User Interface/Desktops
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gtk3-devel >= %{gtk3_version}
BuildRequires: libexif-devel >= %{libexif_version}
BuildRequires: exempi-devel
BuildRequires: lcms2-devel
BuildRequires: intltool >= 0.50.0-1
BuildRequires: libjpeg-devel
BuildRequires: scrollkeeper
BuildRequires: gettext
BuildRequires: desktop-file-utils >= %{desktop_file_utils_version}
BuildRequires: gnome-doc-utils
BuildRequires: gnome-desktop3-devel >= %{gnome_desktop_version}
BuildRequires: gnome-icon-theme >= %{gnome_icon_theme_version}
BuildRequires: libXt-devel
BuildRequires: libxml2-devel
BuildRequires: librsvg2-devel
BuildRequires: libpeas-devel >= 0.7.4
BuildRequires: gdk-pixbuf2-devel
BuildRequires: shared-mime-info
BuildRequires: gsettings-desktop-schemas-devel
BuildRequires: dbus-glib-devel
BuildRequires: gobject-introspection-devel
BuildRequires: zlib-devel
Requires:      gsettings-desktop-schemas

Requires(post):   desktop-file-utils >= %{desktop_file_utils_version}
Requires(postun): desktop-file-utils >= %{desktop_file_utils_version}

%description
The Eye of GNOME image viewer (eog) is the official image viewer for the
GNOME desktop. It can view single image files in a variety of formats, as
well as large image collections.

eog is extensible through a plugin system.

%package devel
Summary: Support for developing plugins for the eog image viewer
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk3-devel

%description devel
The Eye of GNOME image viewer (eog) is the official image viewer for the
GNOME desktop. This package allows you to develop plugins that add new
functionality to eog.

%prep
%setup -q

%build
%configure --disable-scrollkeeper
%make 

%install
make install DESTDIR=%{buildroot}

desktop-file-install --vendor gnome --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
  %{buildroot}%{_datadir}/applications/*

%find_lang %{name} --with-gnome

# grr, --disable-scrollkeeper seems broken
rm -rf %{buildroot}/var/scrollkeeper

rm -rf %{buildroot}%{_libdir}/eog/plugins/*.la


%post
update-desktop-database >&/dev/null || :
touch %{_datadir}/icons/hicolor >&/dev/null || :

%postun
update-desktop-database >&/dev/null || :
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :

%files -f %{name}.lang
%doc AUTHORS COPYING NEWS README
%{_datadir}/eog
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/*/apps/*
%{_bindir}/*
%{_libdir}/eog
%{_datadir}/help/*/eog
%{_datadir}/GConf/gsettings/eog.convert
%{_datadir}/glib-2.0/schemas/org.gnome.eog.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.eog.gschema.xml

%files devel
%{_includedir}/eog-3.0
%{_libdir}/pkgconfig/eog.pc
%{_datadir}/gtk-doc/

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update 3.6.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update 3.6.1

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-2m)
- rebuild against gnome-desktop-3.1

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-4m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-3m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- hide warning %%post

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.29.91-2m)
- rebuild against libjpeg-8a

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-2m)
- rebuild against gnome-desktop

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26.3-4m)
- rebuild against libjpeg-7

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.3-3m)
- add BuildPrereq

* Thu Jul  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-2m)
- add Requires(post): gnome-icon-theme

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3.1-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3.1-1m)
- update to 2.24.3.1

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild against python-2.6.1-2m

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.3-2m)
- change %%preun script

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Wed Apr 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.4-1m)
- update to 2.20.4

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Wed Oct 31 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0
- add devel package

* Tue May 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0.1-1m)
- update to 2.18.0.1

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Fri Feb 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-4m)
- add CFLAGS=-O0 (GLib-CRITICAL **: g_ascii_strcasecmp: assertion)

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Fri Oct 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1.1-1m)
- update to 2.16.1.1

* Sat Sep 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Fri Apr 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Thu Feb  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Sat Dec  3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Mon Nov 28 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-3m)
- add %%preun for schemas

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-2m)
- delete autoreconf and make check

* Thu Nov 17 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-1m)
- version 2.8.2
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.1-3m)
- revised spec for enabling rpm 4.2.

* Sat Feb  7 2004 Masaru Sanuki <sanuki@hh.iij4u.or.jp>
- (2.4.1-2m)
- add eog-collection-view

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Wed Aug 6 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (2.3.5-2m)
- rebuild against libexif 0.5.12

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Thu Jul 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Tue Mar 25 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0-2m)
  rebuild against openssl 0.9.7a

* Wed Jan 29 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.4-1m)
- version 1.1.4

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.3-1m)
- version 1.1.3

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.2-1m)
- version 1.1.2

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.1-1m)
- version 1.1.1

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.0-1m)
- version 1.1.0

* Fri Oct  4 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.3-2m)
- add BuildPreReq: libgnomeprint-devel
- add BuildPreReq: librsvg-devel

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.3-1m)
- version 1.0.3

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.2-1m)
- version 1.0.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-7m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-6m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-5m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-4k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-2k)
- version 1.0.1

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-14k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-12k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-10k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-8k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-6k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-4k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-2k)
- version 1.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (0.118.0-16k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.118.0-14k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.118.0-12k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.118.0-10k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.118.0-8k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.118.0-6k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.118.0-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Wed May 22 2002 Shingo Akagaki <dora@kondara.org>
- (0.118.0-2k)
- version 0.118.0

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.117.0-12k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.117.0-10k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.117.0-8k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.117.0-6k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Wed May  8 2002 Shingo Akagaki <dora@kondara.org>
- (0.117.0-4k)
- add Prereq: scrollkeeper

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.117.0-2k)
- version 0.117.0

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (0.116.1-4k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Thu Apr 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.116.1-2k)
- version 0.116.1

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.116.0-4k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.116.0-2k)
- version 0.116.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.115.0-8k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.115.0-6k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.115.0-4k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.115.0-2k)
- version 0.115.0

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.114.0-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Thu Mar 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.114.0-2k)
- version 0.114.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-20k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-18k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-16k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-14k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-12k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-10k)
- change schema handring

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-8k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-6k)
- rebuild against for yelp-0.3
- rebuild against for libgnomeui-1.112.1
- rebuild against for nautilus-1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-4k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (0.113.0-2k)
- version 0.113.0

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.112.0-8k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.112.0-6k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.112.0-4k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.112.0-2k)
- version 0.112.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.111.0-16k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.111.0-14k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.111.0-12k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.111.0-10k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.111.0-8k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.111.0-6k)
- rebuild against for libbonoboui-1.111.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.111.0-4k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.111.0-2k)
- version 0.111.0
* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.110.0-16k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.110.0-14k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.110.0-12k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.110.0-10k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.110.0-8k)
- rebuild against for libbonoboui-1.110.2

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.110.0-6k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.110.0-4k)
- rebuild against for libIDL-0.7.4

* Mon Feb  4 2002 Shingo Akagaki <dora@kondara.org>
- (0.110.0-2k)
- version 0.110.0

* Wed Jan 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.109.0-2k)
- version 0.109.0
- gnome2 env

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6-18k)
- move zh_CN.GB2312 => zh_CN
- move zh_TW.Big5 => zh_TW

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.6-10k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (0.6-8k)
- rebuild against libpng 1.2.0.

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- (0.6-6k)
- rebuild against fro gnome-print-0.29

* Tue May  2 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- rebuild against gnome-print-0.25

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 0.6
- K2K

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5-9k)
- modified spec file with macros about docdir

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5-7k)
- added gdk-pixbuf-devel >= 0.9.0 to BuildRequires tag

* Tue Nov 07 2000 Shingo Akagaki <dora@kondara.org>
- rebuild for bonobo-0.28

* Thu Oct 12 2000 Shingo Akagaki <dora@kondara.org>
- just rebuild for new gnome-print

* Tue Aug 22 2000 Shingo Akagaki <dora@kondara.org>
- re created the .spec file TT
