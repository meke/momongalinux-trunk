%global momorel 1

Summary: An SSL-encrypting socket wrapper
Name: stunnel
Version: 5.00
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Internet
URL: http://stunnel.mirt.net/
Source0: https://www.stunnel.org/downloads/%{name}-%{version}.tar.gz
NoSource: 0
Source2: Certificate-Creation
Source3: sfinger.xinetd
Source4: stunnel-sfinger.conf
Source5: pop3-redirect.xinetd
Source6: stunnel-pop3s-client.conf
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: pkgconfig
BuildRequires: tcp_wrappers-devel
BuildRequires: util-linux-ng
Patch0: stunnel-5-authpriv.patch
Patch1: stunnel-5-sample.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Stunnel is a socket wrapper which can provide SSL (Secure Sockets
Layer) support to ordinary applications. For example, it can be used
in conjunction with imapd to create an SSL secure IMAP server.

%prep
%setup -q
%patch0 -p1 -b .authpriv
%patch1 -p1 -b .sample

iconv -f iso-8859-1 -t utf-8 < doc/stunnel.fr.8 > doc/stunnel.fr.8_
mv doc/stunnel.fr.8_ doc/stunnel.fr.8

%build
CFLAGS="$RPM_OPT_FLAGS -fPIC"; export CFLAGS
if pkg-config openssl ; then
	CFLAGS="$CFLAGS `pkg-config --cflags openssl`";
	LDFLAGS="`pkg-config --libs-only-L openssl`"; export LDFLAGS
fi
%configure --disable-fips --enable-ipv6 \
	CPPFLAGS="-UPIDFILE -DPIDFILE='\"%{_localstatedir}/run/stunnel.pid\"'"
make LDADD="-pie -Wl,-z,defs,-z,relro"

%install
rm -rf %{buildroot}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/stunnel
touch $RPM_BUILD_ROOT%{_sysconfdir}/stunnel/stunnel.pem
make install DESTDIR=$RPM_BUILD_ROOT
# Move the translated man pages to the right subdirectories, and strip off the
# language suffixes.
for lang in fr pl ; do
	mkdir -p $RPM_BUILD_ROOT/%{_mandir}/${lang}/man8
	mv $RPM_BUILD_ROOT/%{_mandir}/man8/*.${lang}.8* $RPM_BUILD_ROOT/%{_mandir}/${lang}/man8/
	rename ".${lang}" "" $RPM_BUILD_ROOT/%{_mandir}/${lang}/man8/*
done

mkdir srpm-docs
cp %{SOURCE2} %{SOURCE3} %{SOURCE4} %{SOURCE5} %{SOURCE6} srpm-docs

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS ChangeLog COPY* CREDITS PORTS README TODO
%doc tools/stunnel.conf-sample
%doc srpm-docs/*
%lang(en) %doc doc/en/*
%lang(po) %doc doc/pl/*
%{_bindir}/stunnel
%exclude %{_bindir}/stunnel3
%exclude %{_datadir}/doc/stunnel
%{_libdir}/stunnel
%exclude %{_libdir}/stunnel/libstunnel.la
%{_mandir}/man8/stunnel.8*
%lang(fr) %{_mandir}/fr/man8/stunnel.8*
%lang(pl) %{_mandir}/pl/man8/stunnel.8*
%dir %{_sysconfdir}/%{name}
%exclude %{_sysconfdir}/stunnel/*

%changelog
* Fri Mar 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.00-1m)
- [SECURITY] CVE-2014-0016
- update to 5.00

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.53-1m)
- update to 4.53

* Mon Sep 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.44-1m)
- update to 4.44

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.33-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.33-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.33-2m)
- full rebuild for mo7 release

* Wed May 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.33-1m)
- update to 4.33

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.31-2m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.31-1m)
- update to 4.31

* Sat Jan  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.29-1m)
- update to 4.29

* Sun Nov 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.28-1m)
- update to 4.28

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.27-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.27-1m)
- update to 4.27

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.26-4m)
- rebuild against openssl-0.9.8k

* Sun Apr  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.26-3m)
- build with --disable-fips

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.26-2m)
- rebuild against rpm-4.6

* Mon Nov 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.26-1m)
- update to 4.26
- drop Patch2, merged upstream

* Wed Jul 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.25-1m)
- update to 4.25
- sync with Fedora devel
-- * Sun Jun  8 2008 Miloslav Trmac <mitr@redhat.com> - 4.25-2
-- - Use a clearer error message if the service name is unknown in "accept"
--   Resolves: #450344
-- 
-- * Mon Jun  2 2008 Miloslav Trmac <mitr@redhat.com> - 4.25-1
-- - Update to stunnel-4.25
-- 
-- * Tue May 20 2008 Miloslav Trmac <mitr@redhat.com> - 4.24-2
-- - Drop stunnel3
--   Resolves: #442842
-- 
-- * Mon May 19 2008 Miloslav Trmac <mitr@redhat.com> - 4.24-1
-- - Update to stunnel-4.24
-- 
-- * Fri Mar 28 2008 Miloslav Trmac <mitr@redhat.com> - 4.22-1
-- - Update to stunnel-4.22
-- 
-- * Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 4.20-6
-- - Autorebuild for GCC 4.3
-- 
-- * Tue Dec  4 2007 Miloslav Trmac <mitr@redhat.com> - 4.20-5
-- - Rebuild with openssl-0.9.8g
-- 
-- * Tue Oct 16 2007 Miloslav Trmac <mitr@redhat.com> - 4.20-4
-- - Revert the port to NSS, wait for NSS-based stunnel 5.x instead
--   Resolves: #301971
-- - Mark localized man pages with %%lang (patch by Ville Skytta)
--   Resolves: #322281
-- 
-- * Tue Aug 28 2007 Miloslav Trmac <mitr@redhat.com> - 4.20-3.nss
-- - Port to NSS
-- 
-- * Mon Dec  4 2006 Miloslav Trmac <mitr@redhat.com> - 4.20-2
-- - Update BuildRequires for the separate tcp_wrappers-devel package

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.20-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.20-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.20-2m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.20-1m)
- update to 4.20
-- add some files from Fedora

* Tue Oct 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.16-1m)
- update to 4.16
- add patch2 (for autoconf-2.60)

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.15-1m)
- update to 4.15

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.09-2m)
- rebuild against openssl-0.9.8a

* Fri Apr  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.09-1m)
- major bugfixes

* Tue Mar  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.08-1m)
- minor bugfixes

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.07-1m)
- major bugfixes

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.04-2m)
- rebuild against for openssl-0.9.7

* Fri Mar 7 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.04-1m)
- initial stunnel release based on rawhide src.rpm

* Mon Feb 10 2003 Nalin Dahyabhai <nalin@redhat.com> 4.04-3
- rebuild

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Jan 15 2003 Nalin Dahyabhai <nalin@redhat.com> 4.04-1
- update to 4.04

* Tue Jan  7 2003 Nalin Dahyabhai <nalin@redhat.com> 4.03-1
- use pkgconfig for information about openssl, if available

* Fri Jan  3 2003 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.03

* Mon Oct 21 2002 Nalin Dahyabhai <nalin@redhat.com> 4.02-1
- update to 4.02

* Fri Oct  4 2002 Nalin Dahyabhai <nalin@redhat.com> 4.00-1
- don't create a dummy cert

* Wed Sep 25 2002 Nalin Dahyabhai <nalin@redhat.com>
- update to 4.00
- remove textutils and fileutils as buildreqs, add automake/autoconf

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun May 26 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Fri May 17 2002 Nalin Dahyabhai <nalin@redhat.com> 3.22-2
- rebuild in new environment

* Wed Jan  2 2002 Nalin Dahyabhai <nalin@redhat.com> 3.22-1
- update to 3.22, correcting a format-string vulnerability

* Wed Oct 31 2001 Nalin Dahyabhai <nalin@redhat.com> 3.21a-1
- update to 3.21a

* Tue Aug 28 2001 Nalin Dahyabhai <nalin@redhat.com> 3.20-1
- log using LOG_AUTHPRIV facility by default (#47289)
- make permissions on stunnel binary 0755
- implicitly trust certificates in %%{_datadir}/ssl/trusted (#24034)

* Fri Aug 10 2001 Nalin Dahyabhai <nalin@redhat.com> 3.19-1
- update to 3.19 to avoid problems with stunnel being multithreaded, but
  tcp wrappers not being thrad-safe

* Mon Jul 30 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.17

* Mon Jul 23 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.16

* Mon Jul 16 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.15
- enable tcp-wrappers support

* Tue May 29 2001 Nalin Dahyabhai <nalin@redhat.com>
- remove explicit requirement on openssl (specific version isn't enough,
  we have to depend on shared library version anyway)

* Fri Apr 27 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.14

* Mon Mar 26 2001 Preston Brown <pbrown@redhat.com>
- depend on make (#33148)

* Fri Mar  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Tue Feb  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.13 to get pthread, OOB, 64-bit fixes
- don't need sdf any more

* Thu Dec 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- pull in sdf to build the man page (#22892)

* Fri Dec 22 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.11
- chuck the SIGHUP patch (went upstream)
- chuck parts of the 64-bit clean patch (went upstream)

* Thu Dec 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.10
- more 64-bit clean changes, hopefully the last bunch

* Wed Dec 20 2000 Nalin Dahyabhai <nalin@redhat.com>
- change piddir from the default /var/stunnel to /var/run
- clean out pid file on SIGHUP

* Fri Dec 15 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 3.9 to get a security fix

* Wed Oct 25 2000 Matt Wilson <msw@redhat.com>
- change all unsigned longs to u_int32_t when dealing with network
  addresses

* Fri Aug 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- make stunnel.pem also be (missingok)

* Thu Jun 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- move to Applications/Internet group
- clean up %post script
- make stunnel.pem %ghost %config(noreplace)
- provide a sample file for use with xinetd

* Thu Jun  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- FHS compliance fixes
- modify defaults

* Tue Mar 14 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to 3.8
- do not create certificate if one already exists

* Mon Feb 21 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- update to 3.7
- add patch to find /usr/share/ssl
- change some perms

* Sat Oct 30 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- Modify spec file to match Red Hat standards

* Fri Aug 12 1999 Damien Miller <damien@ibs.com.au>
- Updated to 3.4a
- Patched for OpenSSL 0.9.4
- Cleaned up files section

* Sun Jul 11 1999 Damien Miller <dmiller@ilogic.com.au>
- Updated to 3.3

* Sat Nov 28 1998 Damien Miller <dmiller@ilogic.com.au>
- Initial RPMification
