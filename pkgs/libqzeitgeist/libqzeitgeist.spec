%global        momorel 1
%global        srcname QtZeitgeist

Name:           libqzeitgeist
Summary:        Qt Zeitgeist Library
Version:        0.7.0
Release:        %{momorel}m%{?dist}
License:        LGPLv2+
URL:            http://projects.kde.org/projects/kdesupport/libqzeitgeist
Group:          System Environment/Libraries
Source0:        http://releases.zeitgeist-project.com/qzeitgeist/%{srcname}-%{version}.tar.bz2
NoSource:       0
## upstreamable patches
Patch50:        %{name}-%{version}-pkgconfig_version.patch
## upstream patches
Patch100:       %{name}-%{version}-libsuffix.patch

BuildRequires:  cmake
BuildRequires:  qt-devel

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
%{summary}

%prep
%setup -q -n %{srcname}-%{version}-Source
%patch100 -p1 -b .libsuffix
%patch50 -p1 -b .pkgconfig_version

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%check
export PKG_CONFIG_PATH=%{buildroot}%{_datadir}/pkgconfig:%{buildroot}%{_libdir}/pkgconfig
test "$(pkg-config --modversion QtZeitgeist)" = "%{version}"

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc COPYING README
%{_libdir}/libqzeitgeist.so.0*

%files devel
%dir %{_datadir}/qzeitgeist/
%{_datadir}/qzeitgeist/cmake/
%{_includedir}/QtZeitgeist/
%{_libdir}/libqzeitgeist.so
%{_libdir}/pkgconfig/QtZeitgeist.pc

%changelog
* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- import from Fedora devel

* Fri May 20 2011 Rex Dieter <rdieter@fedoraproject.org> 0.7.0-1
- first try

