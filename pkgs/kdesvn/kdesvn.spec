%global         momorel 1
%global         qtver 4.8.5
%global         kdever 4.11.0
%global         kdelibsrel 1m
%global         qtdir %{_libdir}/qt4
%global         kdedir /usr
%global         subversionver 1.8.1

Summary:        A KDE Subversion Client
Name:           kdesvn
Version:        1.6.0
Release:        %{momorel}m%{?dist}
License:        GPLv2+
URL:            http://www.alwins-world.de/wiki/programs/kdesvn
Group:          Applications/Internet
Source0:        http://kdesvn.alwins-world.de/downloads/%{name}-%{version}.tar.bz2
Patch0:         %{name}-%{version}-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       kdelibs >= %{kdever}-%{kdelibsrel}
Requires:       %{name}-svnqt = %{version}-%{release}
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  apr-devel >= 1.2.7-2m
BuildRequires:  apr-util-devel >= 1.2.7-3m
BuildRequires:  neon-devel
BuildRequires:  subversion-devel >= %{subversionver}
BuildRequires:  expat-devel >= 2.0.0-2m
BuildRequires:  openldap-devel >= 2.4.0
BuildRequires:  cmake
BuildRequires:  libdb-devel >= 5.3.15
Obsoletes:      %{name}-kiosvn

%description
KDESvn is a frontend to the subversion vcs. In difference to most other
tools it uses the subversion C-Api direct via a c++ wrapper made by Rapid
SVN and doesn't parse the output of the subversion client. So it is a real
client itself instead of a frontend to the command line tool.

It is designed for the K-Desktop environment and uses all of the goodies
it has. It is planned for future that based on the native client some plugins
for konqueror and/or kate will made.

%package svnqt
Summary: Wrapper lib for subversion QT integration
Group: System Environment/Libraries
Requires: subversion >= %{subversionver}
Obsoletes: qsvn

%description svnqt
Shared lib which contains a QT C++ wrapper for subversion. It is core part
of kdesvn but is designed to not require KDE so plain QT programs may use
it.

%package svnqt-devel
Summary: Wrapper lib for subversion QT integration
Group: Development/Libraries
Requires: %{name}-svnqt = %{version}-%{release}
Requires: subversion-devel >= %{subversionver}
Obsoletes: qsvn-devel

%description svnqt-devel
Development files for kdesvn-svnqt

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"

%ifarch x86_64
%define lib_suffix "-DLIB_SUFFIX=64"
%else
%define lib_suffix %{nil}
%endif

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} .. \
	%{lib_suffix} \
	-DCMAKE_INSTALL_PREFIX=%{_prefix} \
	-DAPR_CONFIG=%{_bindir}/apr-1-config \
	-DAPU_CONFIG=%{_bindir}/apu-1-config \
	-DHAVE_GCC_VISIBILITY=FALSE
popd

make %{?_smp_mflags} VERBOSE=1 -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# conflict with kdesdk-4.7.0
rm -f %{buildroot}%{_kde4_datadir}/kde4/services/svn*.protocol

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post svnqt
/sbin/ldconfig

%postun svnqt
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog GPL.txt INSTALL-cmake TODO
%{_kde4_bindir}/kdesvn
%{_kde4_bindir}/kdesvnaskpass
%{_kde4_libdir}/kde4/kded_kdesvnd.so
%{_kde4_libdir}/kde4/kdesvnpart.so
%{_kde4_libdir}/kde4/kio_ksvn.so
%{_kde4_datadir}/applications/kde4/kdesvn.desktop
%{_kde4_datadir}/apps/kconf_update/*
%{_kde4_datadir}/apps/kdesvn
%{_kde4_datadir}/apps/kdesvnpart
%{_kde4_datadir}/config.kcfg/kdesvn_part.kcfg
%{_kde4_datadir}/doc/HTML/en/kdesvn
%{_kde4_datadir}/doc/HTML/nl/kdesvn
%{_kde4_datadir}/icons/hicolor/*/*/*.png
%{_kde4_datadir}/icons/hicolor/*/*/*.svgz
%{_kde4_datadir}/locale/*/LC_MESSAGES/kdesvn.mo
%{_datadir}/dbus-1/interfaces/*
%{_mandir}/man1/kdesvn.1*
%{_mandir}/man1/kdesvnaskpass.1*
%{_kde4_datadir}/kde4/services/kded/kdesvnd.desktop
%{_kde4_datadir}/kde4/services/kdesvnpart.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/kdesvn_subversion.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/kdesvn_subversion_toplevel.desktop
%{_kde4_datadir}/kde4/services/ksvn*.protocol

%files svnqt
%defattr(-, root, root)
%{_kde4_libdir}/libsvnqt.so.*
%{_kde4_datadir}/svnqt

%files svnqt-devel
%defattr(-, root, root)
%{_kde4_includedir}/svnqt
%{_kde4_libdir}/libsvnqt.so

%changelog
* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-7m)
- rebuild against libdb-5.3.15

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-6m)
- rebuild with new qt-4.8.0

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.5-5m)
- rebuild against libdb

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-4m)
- rebuild against KDE 4.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-2m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-1m)
- update to 1.5.5
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.3-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-3m)
- fix build with new kdelibs

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-2m)
- rebuild against qt-4.6.3-1m

* Sat Apr 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-2m)
- rebuild against db-4.8.26

* Sun Jan 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sat Aug  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Mon Jul 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2 (bug fix release)

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Fri Apr 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-2m)
- apply svn160 patch

* Fri Feb 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-2m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Sat Nov 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-3m)
- qsvn was obsoleted, use kdesvn

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.1-2m)
- rebuild against db4-4.7.25-1m

* Sun Oct  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-0.20080920.1m)
- update 1.2.0 (2008.09.20 svn snapshot)
- KDE4 porting version

* Sat Aug 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Wed Aug  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.6-1m)
- update to 0.14.6

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.5-1m)
- update to 0.14.5

* Mon May 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.4-1m)
- update to 0.14.4

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.3-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.3-2m)
- rebuild against gcc43

* Sat Mar 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.3-1m)
- update to 0.14.3

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.2-3m)
- move headers to %%{_includedir}/kde

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.2-2m)
- rebuild against openldap-2.4.8

* Wed Feb 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.2-1m)
- update to 0.14.2

* Mon Nov 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.1-1m)
- update to 0.14.1

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.14.0-2m)
- rebuild against db4-4.6.21

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0
- add source tar ball to repo.

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.0-1m)
- update to 0.13.0

* Sat Jul 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.1-2m)
- move libsvnqt.la to svnqt package from svnqt-devel package

* Thu Jun 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Wed Mar 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2-1m)
- update to 0.11.2

* Mon Feb 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.0-1m)
- version 0.11.0
- revive a package kdesvn-kiosvn
- modify spec file to use cmake

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.3-5m)
- rebuild against kdelibs etc.

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.3-4m)
- rebuild against db4-4.5.20-1m

* Sat Nov  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.3-3m)
- rebuild against apr-1.2.7-2m

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.3-2m)
- rebuild against expat-2.0.0-2m

* Mon Sep 18 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.9.3-1m)
- update to 0.9.3
- add "make -f admin/Makefile.common cvs" before configure
- delete po.patch (patch1)

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.1-2m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m

* Wed Aug 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-2m)
- add --with-apr-config and --with-apu-config to configure
- remove kdesvn-kiosvn, it's conflicting with kdesdk-3.5.4

* Wed Aug 30 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.9.1-1m)
- initial build for Momonga Linux 3
