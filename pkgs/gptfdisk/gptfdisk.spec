%global momorel 1

Summary: GPT partitioning and MBR repair software
Name: gptfdisk
Version: 0.7.1
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://www.rodsbooks.com/gdisk
Group: Applications/System
Source: http://www.rodsbooks.com/gdisk/%{name}-%{version}.tgz
NoSource: 0

Patch0: gptfdisk.Makefile.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libuuid-devel libicu popt-devel
%description


Partitioning software for GPT disks and to repair MBR
disks. The gdisk and sgdisk utilities (in the gdisk
package) are GPT-enabled partitioning tools; the
fixparts utility (in the fixparts package) fixes some
problems with MBR disks that can be created by buggy
partitioning software.


%prep
%setup -q

#Makefile patch needed for build; push upstream?
%patch0 -p1


%build
CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_CXX_FLAGS" make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/sbin
install -Dp -m0755 gdisk $RPM_BUILD_ROOT/usr/sbin
install -Dp -m0755 sgdisk $RPM_BUILD_ROOT/usr/sbin
install -Dp -m0755 fixparts $RPM_BUILD_ROOT/usr/sbin
install -Dp -m0644 gdisk.8 $RPM_BUILD_ROOT/%{_mandir}/man8/gdisk.8
install -Dp -m0644 sgdisk.8 $RPM_BUILD_ROOT/%{_mandir}/man8/sgdisk.8
install -Dp -m0644 fixparts.8 $RPM_BUILD_ROOT/%{_mandir}/man8/fixparts.8

%clean
rm -rf $RPM_BUILD_ROOT

%files 
%defattr(-,root,root -)
%doc NEWS COPYING README
/usr/sbin/gdisk
/usr/sbin/sgdisk
/usr/sbin/fixparts
%doc %{_mandir}/man8/gdisk.8*
%doc %{_mandir}/man8/sgdisk.8*
%doc %{_mandir}/man8/fixparts.8*


%changelog
* Wed May 11 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.7.1-1m)
- initial momonga release; unified package
* Mon Mar 21 2011 R Smith <rodsmith@rodsbooks.com> - 0.7.1
- Created spec file for 0.7.1 release
