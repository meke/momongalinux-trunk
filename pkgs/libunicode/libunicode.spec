%global momorel 21

Summary: A unicode manipulation library
Name: libunicode
Version: 0.4
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.gnome.org/
Source: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.4/%{name}-%{version}.gnome.tar.gz
Nosource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: libtool

%description
A library to handle unicode strings

%package devel
Summary: A unicode manipulation library
Group: Development/Libraries
Requires: libunicode = %{version}-%{release}

%description devel
The libunicode-devel package includes the static libraries and header files
for the libunicode package.

Install libunicode-devel if you want to develop programs which will use
libunicode.

%prep
%setup -q

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

%build
# do not use %%configure macro due to accustomed libtool problem
./configure \
    --prefix=%{_prefix} \
    --bindir=%{_bindir} \
    --libdir=%{_libdir} \
    --includedir=%{_includedir}
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README AUTHORS COPYING ChangeLog TODO
%{_libdir}/libunicode.so.*

%files devel
%defattr(-, root, root)
%{_bindir}/unicode-config
%{_libdir}/libunicode.so
%{_libdir}/libunicode.a
%{_libdir}/unicodeConf.sh
%{_includedir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-20m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-19m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-18m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-17m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-16m)
- do not use %%configure macro

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4-15m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-14m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-13m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-12m)
- delete libtool library

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4-11m)
- enable x86_64.

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4-10m)
- revised spec for enabling rpm 4.2.

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4-9m)
- fukkatu

* Sun Aug 26 2001 Shingo Akagaki <dora@kondara.org>
- (0.4-8k)
- create -devel package

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (0.4-6k)
- no more ifarch alpha.

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4-3k)
- fixed for FHS

* Thu Oct 12 2000 Shingo Akagaki <dora@kondara.org>
- use libunicode-hoge-gnome tar ball

* Wed Sep 20 2000 Shingo Akagaki <dora@kondara.org>
- add unicodeConf.sh

* Sun May 14 2000 Shingo Akagaki <dora@kondara.org>
- version 0.4
