%global	momorel 1
%global enable_debugging 0

Summary:  A menu system for the GNOME project
Name: gnome-menus
Version: 3.6.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.gnome.org/

#VCS: git:git://git.gnome.org/gnome-menus
Source0: http://download.gnome.org/sources/gnome-menus/3.6/%{name}-%{version}.tar.xz
NoSource: 0

# Keep release notes from showing up in Applications>Other
Patch0: other-docs.patch

# !!FIXME!!
#Requires:  redhat-menus
BuildRequires: gamin-devel
BuildRequires: gawk
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: pkgconfig
BuildRequires: python2-devel
BuildRequires: intltool
BuildRequires: gobject-introspection-devel

%description
gnome-menus is an implementation of the draft "Desktop
Menu Specification" from freedesktop.org. This package
also contains the GNOME menu layout configuration files,
.directory files and assorted menu related utility programs,
Python bindings, and a simple menu editor.

%package devel
Summary: Libraries and include files for the GNOME menu system
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package provides the necessary development libraries for
writing applications that use the GNOME menu system.

%prep
%setup -q
%patch0 -p1 -b .other-docs

%build
%configure --disable-static \
   --enable-introspection \
%if %{enable_debugging}
   --enable-debug=yes
%else
   --enable-debug=no
%endif

make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

# This probably dates back to Bluecurve days; rather than simply deleting
# deleting it, we'll ship it as applications-gnome.desktop
mv $RPM_BUILD_ROOT%{_sysconfdir}/xdg/menus/applications{,-gnome}.menu

# We use alacarte now
rm -rf $RPM_BUILD_ROOT%{_bindir}/gmenu-simple-editor
rm -rf $RPM_BUILD_ROOT%{_datadir}/applications/gmenu-simple-editor.desktop
rm -rf $RPM_BUILD_ROOT%{_datadir}/gnome-menus/glade/gmenu-simple-editor.glade
rm -rf $RPM_BUILD_ROOT%{python_sitearch}/GMenuSimpleEditor/*

# No point in shipping this either
rm -rf $RPM_BUILD_ROOT%{_bindir}/gnome-menu-spec-test
rm -rf $RPM_BUILD_ROOT%{_datadir}/gnome-menus/

%find_lang gnome-menus-3.0

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f gnome-menus-3.0.lang
%doc AUTHORS NEWS COPYING.LIB
%{_sysconfdir}/xdg/menus/applications-gnome.menu
%{_libdir}/lib*.so.*
%{_datadir}/desktop-directories/*
%{_libdir}/girepository-1.0/GMenu-3.0.typelib

%files devel
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_includedir}/gnome-menus-3.0
%{_datadir}/gir-1.0/GMenu-3.0.gir

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0.1-2m)
- rebuild for glib 2.33.2

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0.1-1m)
- update to 3.2.0.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.30.5-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.5-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.5-1m)
- update to 2.30.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.4-1m)
- update to 2.30.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-4m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-3m)
- good-bye <KDELegacyDirs/>

* Mon Jul 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- add menu prefix
- good-bye gmenu-simple-editor
- add patch0 for settings.menu

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.6-1m)
- update to 2.29.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0.1-1m)
- update to 2.28.0.1

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun Jul 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.26.2-3m)
- use make instead of %%make

* Sat Jul 25 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.26.2-2m)
- modify menu file momonga-settings.menu

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Sun May 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.26.1-2m)
- add autoreconf

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Tue Mar 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Feb 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91
- update Source1, 3 (delete Source 2, 4, 5)

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.5-1m)
- update to 2.25.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild against python-2.6.1-1m

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Wed Oct 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Mon Oct  6 2008  Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-2m)
- modify menu files applications.menu settings.menu

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Mon May 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Wed Mar 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-4m)
- delete duplicated dir

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-3m)
- modify spec

* Sun Mar 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-2m)
- modify momonga-setting.menu

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- good-bye %%makeinstall

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- modify settings.menu(source3) for new control-center

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.1-2m)
- rebuild against python-2.5

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update 2.16.1

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-2m)
- remove category Application

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Mon Aug 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-3m)
- modify patch0 for alacarte

* Sun Aug 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-2m)
- add patch0 gmenu for python module

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Sun Jul  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-3m)
- Adjustment momonga-applications.menu and momonga-preferences.menu

* Fri Jun 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- modify momonga-preferences.menu (Not KDE)

* Mon May  8 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Mon Apr 24 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.13.5-2m)
- add patch0

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.5-1m)
- update to 2.13.5

* Thu Apr  6 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.0-7m)
- revised packages owner

* Mon Mar  6 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.0-6m)
- update application menu

* Sun Dec  4 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.0-5m)
- update gmenu-simple-editor.desktop
- change applications menu
- add Source4,Source5 (menu-directory file)

* Sat Nov 26 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.0-4m)
- revise GNOME application menu setting files
- add Source1,2,3

* Thu Nov 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.0-3m)
- revise desktop file (for KDE)

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-2m)
- Delete under %{_sysconfdir} 
 
* Wed Nov 16 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- start
- GNOME 2.12.1 Desktop
