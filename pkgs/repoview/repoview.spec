%global momorel 1

Name:           repoview
Version:        0.6.6
Release:        %{momorel}m%{?dist}
Summary:        Creates a set of static HTML pages in a yum repository

Group:          Applications/System
License:        GPL
URL:		https://fedorahosted.org/repoview/
Source0:	http://icon.fedorapeople.org/%{name}/%{name}-%{version}.tar.xz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       python-kid >= 0.6.3, yum >= 3.0
Requires:       python

%description
RepoView creates a set of static HTML pages in a yum repository for easy
browsing.


%prep
%setup -q
##
# Fix version and default templates dir.
#
sed -i -e \
    "s|^VERSION =.*|VERSION = '%{version}-%{release}'|g" repoview.py
sed -i -e \
    "s|^DEFAULT_TEMPLATEDIR =.*|DEFAULT_TEMPLATEDIR = '%{_datadir}/%{name}/templates'|g" \
    repoview.py

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p -m 755                         \
    $RPM_BUILD_ROOT/%{_datadir}/%{name} \
    $RPM_BUILD_ROOT/%{_bindir}          \
    $RPM_BUILD_ROOT/%{_mandir}/man8
install -p -m 755 repoview.py  $RPM_BUILD_ROOT/%{_bindir}/repoview
install -p -m 644 repoview.8   $RPM_BUILD_ROOT/%{_mandir}/man8/
cp -rp templates               $RPM_BUILD_ROOT/%{_datadir}/%{name}/


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING ChangeLog
%{_datadir}/%{name}
%{_bindir}/*
%{_mandir}/man*/*


%changelog
* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.6-1m)
- update 0.6.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.5-2m)
- full rebuild for mo7 release

* Tue Apr  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.4-1m)
- update to 0.6.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2
- change URL

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-2m)
- rebuild against gcc43

* Tue Jan 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-1m)
- update 0.6.1

* Mon Mar  5 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.2-2m)
- apply patch Patch0: yum-2.9-api.patch

* Sun Jul 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.2-1m)
- import from fedora extra

* Tue Jul 04 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.5.2-1
- Version 0.5.2
- Use yum-2.9 API patch (Jesse Keating)

* Wed Feb 15 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.5.1-1
- Version 0.5.1

* Fri Jan 13 2006 Konstantin Ryabitsev <icon@fedoraproject.org> - 0.5-1
- Version 0.5

* Sun Oct 09 2005 Konstantin Ryabitsev <icon@linux.duke.edu> - 0.4.1-1
- Version 0.4.1

* Fri Sep 23 2005 Konstantin Ryabitsev <icon@linux.duke.edu> - 0.4-1
- Version 0.4
- Require yum >= 2.3
- Drop requirement for python-elementtree, since it's required by yum
- Disttagging.

* Mon Apr 04 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 0.3-3
- Do not BuildRequire sed -- basic enough dependency, even for version 4.

* Tue Mar 29 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 0.3-2
- Preserve timestamps on installed files
- Do not use macros in source tags
- Omit Epoch

* Fri Mar 25 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 0.3-1
- Version 0.3

* Thu Mar 10 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 0.2-1
- Version 0.2
- Fix URL
- Comply with fedora extras specfile format.
- Depend on python-elementtree and python-kid -- the names in extras.

* Thu Mar 03 2005 Konstantin Ryabitsev <icon@linux.duke.edu> 0.1-1
- Initial build
