%global momorel 7
%global rversion v19_20071207

Summary: Free, cross platform, open-source, audio I/O library
Name: portaudio
Version: 19
Release: %{momorel}m%{?dist}
# This software's license is essentially MIT/X (BSD?), see author's comment : http://techweb.rfa.org/pipermail/portaudio/2004-November/003942.html
License: see "LICENSE.txt"
Group: System Environment/Libraries
URL: http://www.portaudio.com/
Source0: http://www.portaudio.com/archives/pa_stable_%{rversion}.tar.gz
NoSource: 0
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: coreutils
BuildRequires: doxygen
BuildRequires: jack-devel >= 0.109.2-4m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
PortAudio is a free, cross platform, open-source, audio I/O library.  
It lets you write simple audio programs in 'C' that will compile and 
run on many platforms including Windows, Macintosh (8,9,X), Unix (OSS), SGI, and BeOS. 
PortAudio is intended to promote the exchange of audio synthesis software 
between developers on different platforms, and was recently selected 
as the audio component of a larger PortMusic project that includes MIDI and sound file support.

PortAudio provides a very simple API for recording and/or playing sound using 
a simple callback function.  Example programs are included that synthesize 
sine waves and pink noise, perform fuzz distortion on a guitar, list available audio devices, etc. 

%package devel
Summary: Development files for the portaudio audio I/O library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}, pkgconfig

%description devel
PortAudio is a portable audio I/O library designed for cross-platform
support of audio. It uses a callback mechanism to request audio processing.
Audio can be generated in various formats, including 32 bit floating point,
and will be converted to the native format internally.

This package contains files required to build applications that will use the
portaudio library.

%prep
%setup -q -n %{name} 

autoreconf -fi

%build
%configure \
	--disable-static		\
	--enable-cxx

make %{?_smp_mflags}

# Build html devel documentation
doxygen

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/libportaudio*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, -)
%doc LICENSE.txt README.txt
%{_libdir}/libportaudio*.so.*

%files devel
%defattr(-, root, root, -)
%doc V19-devel-readme.txt doc
%{_includedir}/portaudio.h
%{_includedir}/portaudiocpp
%{_libdir}/pkgconfig/portaudio*.pc
%{_libdir}/libportaudio*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (19-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (19-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (19-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (19-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (19-3m)
- rebuild against rpm-4.6

* Sun Aug 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (19-2m)
- build with jack
- enable %%{optflags}
- BuildRequires: doxygen

* Thu Jul 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (19-1m)
- update to latest stable version, v19_20071207

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (18.1-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (18.1-4m)
- %%NoSource -> NoSource

* Mon Jul 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (18.1-3m)
- use tr instead of nkf and BuildPreReq: coreutils

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (18.1-2m)
- enable lib64
-- Patch1: portaudio-v18-lib64.patch

* Mon Dec 19 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (18.1-1m)
- import to Momonga (to build OOo2 with "--with-system-portaudio")
