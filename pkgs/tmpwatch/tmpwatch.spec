%global momorel 4

Summary: A utility for removing files based on when they were last accessed
Name: tmpwatch
Version: 2.10.1
Release: %{momorel}m%{?dist}
URL: https://fedorahosted.org/tmpwatch/
Source0: https://fedorahosted.org/releases/t/m/tmpwatch/tmpwatch-%{version}.tar.bz2
Source1: tmpwatch.daily
License: GPLv2
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: psmisc

%description
The tmpwatch utility recursively searches through specified
directories and removes files which have not been accessed in a
specified period of time.  Tmpwatch is normally used to clean up
directories which are used for temporarily holding files (for example,
/tmp).  Tmpwatch ignores symlinks, won't switch filesystems and only
removes empty directories and regular files.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} INSTALL='install -p' install

mkdir -p %{buildroot}/etc/cron.daily
cp %{SOURCE1} %{buildroot}/etc/cron.daily/tmpwatch
chmod +x %{buildroot}/etc/cron.daily/tmpwatch

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog NEWS README
%{_sbindir}/tmpwatch
%{_mandir}/man8/tmpwatch.8*
%config(noreplace) /etc/cron.daily/tmpwatch

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.1-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.15-1m)
- update to 2.9.15

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.13-2m)
- rebuild against rpm-4.6

* Sat Jul 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.9.13-1m)
- sync with Fedora devel (2.9.13-2)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9.11-2m)
- rebuild against gcc43

* Fri Jun  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.9.11-1m)
- sync Fedora

* Mon May 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.9.7-1m)
- update to 2.9.7

* Thu Mar 10 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (2.9.1-1m)
- version up

* Sat Aug  2 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.9.0-1m)
- update to 2.9.0
- use %%{momorel} macro
- use %%{name} on Source:
- use %%make, %%makeinstall, %%__rm, %%__mkdir_p, %%__cat, %%__chmod
- remove -D_GNU_SOURCE from RPM_OPT_FLAGS
- tmpwatch-2.9.0.tar.gz - RawHide (tmpwatch-2.9.0-2)

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (2.8.3-2k)
- ver up.

* Fri Oct 19 2001 Toru Hoshina <t@Kondara.org>
- (2.2-2k)
- merge from rawhide. skipped Jirai...

* Thu Aug 23 2001 Motonobu Ichimura <famao@kondara.org>
- up to 2.7.4
- some changes added
- now not nosrc

* Mon Feb 12 2001 Uechi Yasumasa <uh@u.dhis.portside.net>
- change path in cron script (/vat/catman -> /var/cache/man)

* Thu Oct 12 2000 Toyokazu WATABE <toy2@kondar.org>
- update for the version 2.6.2 including security update.

* Tue Apr 25 2000 tom <tom@kondara.org>
- add defattr

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.2-1).

* Mon Feb 14 2000 Preston Brown <pbrown@redhat.com>
- option to use fuser to see if file in use before removing

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description
- man pages are compressed

* Tue Jan 18 2000 Preston Brown <pbrown@redhat.com>
- null terminal opt struct (#7836)
- test flag implies verbose (#2383)

* Wed Jan 12 2000 Paul Gear <paulgear@bigfoot.com>
- HP-UX port (including doco update)
- Tweaked Makefile to allow installation into different base directory
- Got rid of GETOPT_... defines which didn't do anything, so that short
  equivalents for all long options could be defined.
- Fixed bug in message() where 'where' file handle was set but unused
- Changed most fprintf() calls to message()

* Fri Dec 10 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Aug 30 1999 Preston Brown <pbrown@redhat.com>
- skip lost+found directories
- option to use file's atime instead of mtime (# 4178)

* Mon Jun  7 1999 Jeff Johnson <jbj@redhat.com>
- cleanup more man pages, this time adding in cvs (#224).

* Thu Apr 08 1999 Preston Brown <pbrown@redhat.com>
- I am the new maintainer
- fixed cleanup of directories
- added --quiet flag
- freshen manpage
- nice patch from Kevin Vajk <kvajk@ricochet.net> integrated

* Wed Jun 10 1998 Erik Troan <ewt@redhat.com>
- make /etc/cron.daily/tmpwatch executable

* Tue Jan 13 1998 Erik Troan <ewt@redhat.com>
- version 1.5
- fixed flags passing
- cleaned up message()

* Wed Oct 22 1997 Erik Troan <ewt@redhat.com>
- added man page to package
- uses a buildroot and %attr
- fixed error message generation for directories
- fixed flag propagation

* Mon Mar 24 1997 Erik Troan <ewt@redhat.com>
- Don't follow symlinks which are specified on the command line
- Added a man page

* Sun Mar 09 1997 Erik Troan <ewt@redhat.com>
- Rebuilt to get right permissions on the Alpha (though I have no idea
  how they ended up wrong).
