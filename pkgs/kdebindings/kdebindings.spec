%global momorel 1
%global kdever 4.13.1

Summary: KDE bindings to non-C++ languages
Name: kdebindings
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPLv2

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: kross-interpreters = %{version}
Requires: kross-java = %{version}
Requires: kross-python = %{version}
Requires: kross-ruby = %{version}
Requires: pykde4 = %{version}
Requires: pykde4-akonadi = %{version}
Requires: smokegen = %{version}
Requires: smokeqt = %{version}
Requires: smokekde = %{version}
Requires: smokekde-akonadi = %{version}
Requires: qyoto = %{version}
Requires: kimono = %{version}
Requires: kimono-akonadi = %{version}
Requires: qtruby = %{version}
Requires: korundum = %{version}
Requires: korundum-akonadi = %{version}
Requires: perlqt = %{version}
Requires: perlkde = %{version}

%description
kdebindings metapackage.

%package devel
Summary: Development files for kdebindings
Group: Development/Libraries

Requires: pykde4-devel = %{version}
Requires: smokegen-devel = %{version}
Requires: smokeqt-devel = %{version}
Requires: smokekde-devel = %{version}
Requires: qyoto-devel = %{version}
Requires: qtruby-devel = %{version}
Requires: korundum-devel = %{version}
Requires: perlqt-devel = %{version}

%description devel
%{summary}.

%prep

%build

%install

%files

%files devel

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Sat Mar 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-2m)
- add Requires: smokekde-akonadi
- add Requires: kimono-akonadi
- add Requires: korundum-akonadi

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0
- revive as a meta package

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-2m)
- rebuild against qscintilla-2.5

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-2m)
- add patch51 to fix #684419
- add pqtch52 to fix build failure with phonon-4.5.0

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-2m)
- rebuild against sip-4.12.1 and PyQt4-4.8.3

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Sat Dec 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-2m)
- rebuild against sip-4.12.0 and PyQt4-4.8.2

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Sat Oct 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-5m)
- rebuild against sip-4.11.2 and PyQt4-4.8

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-4m)
- remade patch for mono-2.8
- add BuildRequires: glib2-devel

* Thu Oct 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.5.2-3m)
- rebuild against mono-2.8

* Mon Oct 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-2m)
- patch smoke generator invalid reads found by valgrind

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Oct  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.1-2m)
- rebuild against PyQt4-4.7.7-2m

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.95-2m)
- build with ruby18

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Sat Jul 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-3m)
- enable to build with new sip

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-2m)
- change BuildRequires: from libattica-devel to attica-devel

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-2m)
- rebuild against qt-4.7.0-0.1.3m (fix smoke build failure)

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-1m)
- update to KDE 4.4.3

* Tue May  4 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.2-3m)
- add BuildRequires: QtRuby-devel

* Thu Apr 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-2m)
- fix build with libattica-0.1.3
- revive BuildRequires: qwt-devel

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Tue Jan 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-2m)
- remove polkit-qt support, kdebindings does not support polkit-qt-1
- correct pykde4 examples' path and qyoto files' permissions

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sun Dec 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-2m)
- fix %%files

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 44 beta1 (4.3.80)

* Sat Dec  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-2m)
- kdebindings does not conflict with dbus-qt3-devel any more

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-2m)
- rebuild against sip-4.9.1/PyQt4-4.6.1

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-2m)
- modify BRs for automatic building of STABLE_6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Tue Jun  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Tue Jun  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-4m)
- add PyKDE4-akonadi sub package

- * Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.89-1m)
- - update to 4.2.89

- * Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

* Fri Jun  5 2009 NARITA Koichi <pulsar@momonga-linux>
- (4.2.4-3m)
- add patch101 for new soprano

* Fri Jun  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.4-2m)
- install Source2-5 to enable build

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Tue Jun  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-2m)
- - fix pykde4 compile issues

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-2m)
- - update upstream modifications

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - apply upstream modification, again

- * Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - apply upstream modification

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Sat May  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - apply upstream modification

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sun Apr 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- sync with Fedora devel

* Fri Feb  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-2m)
- Obsoletes: kdebindings3

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0
- remove unused patches
- sync with Fedora devel

* Sun Jan 25 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.96-3m)
- revise %%files

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.85-2m)
- rebuild against python-2.6.1-1m

* Sun Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Sun Nov  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.72-1m)
- update to KDE 4.1.72
- set -DENABLE_PHONON_SMOKE=OFF for the moment (to enable build)
- remove merged nepomuk-build-fix.patch
- make VERBOSE=1

* Sat Nov  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-3m)
- rebuild against qscintilla-2.3-1m

* Wed Oct 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-2m)
- add Patch0 to fix build break

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Tue Sep 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-2m)
- add patch0 to fix build breaks

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.63-1m)
- update to 4.1.63

* Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.62-1m)
- update to 4.1.62

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.61-1m)
- update to 4.1.61

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-3m)
- revise %%files section

* Wed Jul 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- remove merged kate.patch
- modify %%files for official release

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Fri Jul 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.98-3m)
- fix %%files to avoid conflicting

* Fri Jul 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.98-2m)
- fix Requires of PyKDE4

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Mon Jun 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.84-2m)
- apply rev824845.patch again (to enable build)

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.83-2m)
- fix build on x86_64

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.83-1m)
- update to KDE 4.1 beta 2
- import ruby-cmakelists.patch from Fedora
 +* Fri Jun 20 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.83-3
 +- fix CMake target name conflict between Ruby and Python bindings
- update fix-ruby187.patch

* Thu Jun 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.80-3m)
- support ruby-1.8.7

* Wed Jun  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-2m)
- enable build on trunk
- sort BR and %%files
- clean up spec file

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2
- delete patches
- disable some features at this time...

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Wed Feb  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-2m)
- build with qscintilla-2.1

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to KDE4

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8
- PyQt-3.14.1-13m
- sip-4.2.1-13m
- qtruby-1.0.13-5m
- delete unused patches

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.7-2m)
- rebuild against ruby-1.8.6-4m

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- PyQt-3.14.1-12m
- sip-4.2.1-12m
- qtruby-1.0.13-4m

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against kdelibs etc.

* Sun Jan 28 2007 NARITA Koichi <pulsar@momonga-linunx.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete patch 11 (kdebindings-3.5.5-type.patch)

* Wed Dec 27 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.5.5-5m)
- add pyssizet patch from FC

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.5-4m)
- rebuild against python-2.5

* Wed Nov 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.5.5-3m)
- separated sip, PyQt, PyKDE to other packages since bundled ones has not been updated for a long time.
- fix qtrubyver and qtrubyrel since qtruby has been updated to 1.0.13 since KDE-3.5.5

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.5-2m)
- rebuild against expat-2.0.0-2m
- PyQt-3.14.1-11m
- sip-4.2.1-11m
- qtruby-1.0.10-16m

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5
- PyQt-3.14.1-10m
- sip-4.2.1-10m
- qtruby-1.0.10-15m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4
- PyQt-3.14.1-9m
- sip-4.2.1-9m
- qtruby-1.0.10-14m

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3
- PyQt-3.14.1-8m
- sip-4.2.1-8m
- qtruby-1.0.10-13m

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2
- update python.patch
- PyQt-3.14.1-7m
- sip-4.2.1-7m
- qtruby-1.0.10-12m

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.5.1-2m)
- build against perl-5.8.8 (mjsk (^^;;;
- PyQt-3.14.1-6m
- sip-4.2.1-6m
- qtruby-1.0.10-11m

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1
- PyQt-3.14.1-5m
- sip-4.2.1-5m
- qtruby-1.0.10-10m

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1
- remove BuildConflicts: mono
- PyQt-3.14.1-4m
- sip-4.2.1-4m
- qtruby-1.0.10-9m

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5
- PyQt-3.14.1-3m
- sip-4.2.1-3m
- qtruby-1.0.10-8m

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- use make, "make -j2" doesn't work
- disable hidden visibility
- PyQt-3.14.1-2m
- sip-4.2.1-2m
- qtruby-1.0.10-7m

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- PyQt-3.14.1-1m
- sip-4.2.1-1m
- qtruby-1.0.10-6m

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-2m)
- rebuild against python-2.4.2
- PyQt-3.13-7m
- sip-4.1.1-7m
- qtruby-1.0.10-5m

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3
- PyQt-3.13-6m
- sip-4.1.1-6m
- qtruby-1.0.10-4m

* Mon Sep 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-3m)
- rebuild against qscintilla-1.6-1m
- PyQt-3.13-5m
- sip-4.1.1-5m
- qtruby-1.0.10-3m

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure
- PyQt-3.13-4m
- sip-4.1.1-4m
- qtruby-1.0.10-2m

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- PyQt-3.13-3m
- sip-4.1.1-3m
- qtruby-1.0.10-1m
- add %%doc

* Mon Jun 13 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.4.1-2m)
- rebuilt against perl-5.8.7

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1
- --disable-final

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- update python(version).patch
- import kdebindings-3.3.92-xdg.patch from Fedora Core
- add kdebindings-3.4.0-rbkconfig_compiler-Makefile.patch

* Sun Mar 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.3.2-4m)
- rebuild against qscintilla-1.5.1

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-3m)
- $(prefix)/lib -> $(libdir)

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1
- versions of sip, PyQt and qtruby are 4.0.2-4m, 3.12-4m, 1.0.3-1m respectively

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-3m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Sun Sep 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.0-2m)
- revise %%files section
  %%{_libdir}/*.so* -> %%{_libdir}/*.so.*

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0
- make sip, PyQt, qtruby subpackages
- add Patch100

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.1-2m)
- revised spec for enabling rpm 4.2.

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- modified %%files

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Fri Dec 26 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-4m)
- rebuild against for qt-3.2.3

* Mon Nov 10 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-3m)
- rebuild against for qt-3.2.3

* Fri Oct 3 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-2m)
- change includedir

* Wed Sep 24 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Wed Aug 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- delete export _POSIX2_VERSION=199209 and add BuildPreReq: coreutils >= 5.0-1m (for glibc-2.3.2 environment)

* Mon May 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-3m)
- export _POSIX2_VERSION=199209

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
  Changelog from 3.0.5 to 3.1
    bindings updated.
  Changelog from 3.1 to 3.1.1
    Removing Qt bindings for private interfaces that were accidentally included and now caused compilation failures with Qt 3.1.2.
    Several other build related fixes.

* Thu Mar  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.5a-2m)
- remove kmozilla

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Wed Dec 11 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.

* Sun Jun  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up.

* Thu Apr  4 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-2k)
  update to 3.0 release.

* Sat Feb 23 2002 Shigeru Yamazaki <s@kondara.org>
- (2.2.2-4k)
- rebuild against python 2.2 and mozilla 0.9.8

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Fri Nov  2 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- rebuild against mozilla 0.9.5.

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- revised spec file.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- rebuild against libpng 1.2.0.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Wed Aug 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release, however still no fam, no python2...

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)
- based on 2.2alpha2.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Wed Mar 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.1-2k)

* Wed Feb 28 2001 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild

* Wed Feb 21 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.1

* Sun Jan 28 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- initial RPM
