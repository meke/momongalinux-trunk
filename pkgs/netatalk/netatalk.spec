%global momorel 1

Summary: AppleTalk networking programs
Name: netatalk
Version: 3.0.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://netatalk.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/netatalk/netatalk-%{version}.tar.bz2
NoSource: 0
Source2: netatalk.pam-system-auth

Patch1:  netatalk-2.0.2-uams_no_pie.patch
Patch2:  netatalk-2.0.4-extern_ucreator.patch
Patch3:  netatalk-2.2.3-sigterm.patch
Patch4:  netatalk-2.2.3-libdb4.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cracklib-devel
BuildRequires: libdb-devel >= 5.3.15
BuildRequires: libgcrypt-devel
BuildRequires: libgssglue-devel 
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: pam-devel
BuildRequires: perl
BuildRequires: quota-devel
BuildRequires: tcp_wrappers-devel
Requires: pam
Requires(post): chkconfig
Requires(preun): chkconfig initscripts
Requires(postun): initscripts

Obsoletes: %{name}-sysvinit

%description
This package enables Linux to talk to Macintosh computers via the
AppleTalk networking protocol. It includes a daemon to allow Linux
to act as a file server over EtherTalk or IP for Mac's.

%package devel
Summary: Headers and static libraries for Appletalk development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibc-headers

%description devel
This package contains the header files, and static libraries for building
Appletalk networking programs.

%prep
%setup -q

#%patch1  -p1 -b .uams_no_pie
%patch2  -p1 -b .extern_ucreator
#%patch3  -p1 -b .sigterm
#%patch4  -p1 -b .libdb4

ln -s ./NEWS ChangeLog

touch AUTHORS
libtoolize --force
aclocal -I macros
automake --add-missing
autoconf
autoheader
export CFLAGS="$RPM_OPT_FLAGS"
%ifnarch x86_64
export CFLAGS="$CFLAGS -fPIE"
export LDFLAGS="-pie -Wl,-z,relro,-z,now,-z,noexecstack,-z,nodlopen"
%endif

%build
%configure \
        --with-pkgconfdir=/etc/atalk/ \
        --with-cracklib \
        --with-pam \
        --with-shadow \
        --with-uams-path=%{_libdir}/atalk \
        --enable-shared \
        --enable-krbV-uam \
        --enable-overwrite \
        --with-gnu-ld \
        --with-init-style=redhat-systemd \
        --with-libevent-header=/usr/include/event2 \
        --with-libevent-lib=%{_libdir} \
        --with-libgcrypt

# Grrrr. Fix broken libtool/autoFOO Makefiles.
if [ "%{_lib}" != lib ]; then
        find . -name Makefile | xargs perl -pi \
        -e 's,-L/usr/lib,-L%{_libdir},g'
        find . -name Makefile | xargs perl -pi \
        -e 's,-L/lib,-L/%{_lib},g'
fi

%make all

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} mandir=%{_mandir} install

# install example config files in doc
mkdir config.example
cp -fp config/afp.conf config.example

install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/netatalk

# XXX bad hack until this file is updated in glibc-headers:
rm -f $RPM_BUILD_ROOT/usr/include/netatalk/at.h

# FIXME conflict libevent2 headers
rm -f $RPM_BUILD_ROOT/usr/include/*.h 
rm -rf $RPM_BUILD_ROOT/usr/include/event2/

# Clean up .a and .la files
find $RPM_BUILD_ROOT -name \*.a -exec rm {} \;
find $RPM_BUILD_ROOT -name \*.la -exec rm {} \;

%clean
rm -rf %{buildroot}

%post
%systemd_post netatalk.service

%preun
%systemd_preun netatalk.service

%postun
%systemd_postun_with_restart netatalk.service


%files
%defattr(-,root,root,-)
%doc COPYRIGHT COPYING ChangeLog VERSION NEWS
%doc doc
%doc config.example
%dir /etc/atalk
%{_unitdir}/netatalk.service
#%{_libexecdir}/netatalk/netatalk.sh
%config(noreplace) %{_sysconfdir}/atalk/afp.conf
%config(noreplace) %{_sysconfdir}/pam.d/netatalk
%{_sbindir}/*
%{_bindir}/*
%exclude %{_bindir}/netatalk-config
%{_mandir}/man*/*
%exclude %{_mandir}/man*/netatalk-config*
#%{_datadir}/netatalk
%dir %{_libdir}/atalk
%{_libdir}/atalk/*.so
%{_libdir}/libatalk.so.*
#%{_libexecdir}/*
%dir /var/netatalk
%dir /var/netatalk/CNID
/var/netatalk/README
/var/netatalk/CNID/README

%files devel
%defattr(-,root,root)
%dir %{_includedir}/atalk
#%%dir %{_includedir}/netatalk
%attr(0644,root,root) %{_includedir}/atalk/*
#%attr(0644,root,root) %{_includedir}/netatalk/*
%{_libdir}/libatalk.so
%{_datadir}/aclocal/netatalk.m4
%{_bindir}/netatalk-config
%{_mandir}/man*/netatalk-config.1*

%changelog
* Tue Jan 22 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update to 3.0.1

* Thu Sep 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-1m)
- update to 3.0

* Tue Mar 27 2012 NARITA Koichi <pulsar@momoga-linux.org>
- (2.2.2-1m)
- update to 2.2.2
- rebuild against libdb-5.3.15

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.0-3m)
- rebuild against libdb

* Mon Aug  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-2m)
- modify Requires of package sysvinit

* Mon Aug  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.0-1m)
- update 2.2.0
-- support MacOS Lion!

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.5-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.5-5m)
- full rebuild for mo7 release

* Sun Aug 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-4m)
- stop atalk daemon at start up time

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.5-3m)
- XXX: -devel temporarily requires glibc-headers

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.5-2m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5
-- rebase and drop all patches

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-20m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-18m)
- rebuild against openssl-0.9.8k

* Sun Mar 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-17m)
- add a package timeout and %%global with_timeout 0
- to avoid conflicting with new coreutils

* Sun Mar  1 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-16m)
- fix BuildPrereq

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-15m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-14m)
- [SECURITY] CVE-2008-5718
- import a security patch (Patch6) from Debian stable (2.0.3-4+etch1)
- License: GPLv2+

* Thu Oct  9 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.3-13m)
- update Patch0: netatalk-2.0.3-db4.7.patch 
- build against db4-4.7.25

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-12m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-11m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-10m)
- rebuild against libgssglue

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.3-9m)
- rebuild against perl-5.10.0-1m

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@mmonga-linux.org>
- (2.0.3-8m)
- rebuild against db4-4.6.21
- update Patch0: netatalk-2.0.3-db4.6.patch

* Fri Jul 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3-7m)
- modify patch0 for db4.5

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3-6m)
- delete libtool library

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.3-5m)
- rebuild against db4-4.5.20-1m

* Mon Jul 31 2006 Nishio Futoshi <futoshi@momonga-linuc.org>
- (2.0.3-4m)
- add patch5 (conflict swab.h)

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.3-3m)
- rebuild against openssl-0.9.8a

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.3-2m)
- rebuild against db4.3

* Sat Jan  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.3-1m)
- sync with fc-devel

* Sun May 29 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.4-5m)
- suppress AC_DEFUN warning.

* Fri Jul  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.4-4m)
- stop daemon

* Mon Jun 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.4-3m)
- rebuild against db4.2

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6.4-2m)
- revised spec for rpm 4.2.

* Fri Dec 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.4-1m)

* Sat Jul 19 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.6.3-2m)
- remove %{buildroot}%{_includedir}/netatalk/at.h (conflicts glibc-devel >= 2.3)

* Fri Jul 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.3-1m)

* Tue Jun 10 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.2-1m)

* Tue Mar 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-2m)
- revice %doc

* Mon Mar 10 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-1m)

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.0-2m)
  rebuild against openssl 0.9.7a

* Tue Jan 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.0-1m)
- minor feature enhancements

* Tue Dec  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.5-1m)

* Mon Jun 17 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.5.3.1-2k)

* Sun Apr 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.5.2-8k)
- add pagecount.ps

* Mon Mar 11 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.5.2-6k)
- specify 'noreplace' to config files

* Mon Feb 24 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.2-4k)
- just rebuild for .la fix

* Sun Feb 17 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.5.2-2k)
- change the License to GPL

* Mon Jan  7 2002 Tsutomu Yasuda <tom@kondara.org>
- update to 1.5.0

* Fri Nov 30 2001 Toru Hoshina <t@kondara.org>
- (1.5-0.0008006k)
- comment out %chkconfig

* Sat Nov 03 2001 Motonobu Ichimura <famao@kondara.org>
- (1.5-0.0008004k)
- add patch to build pam related modules correctly

* Mon Oct 22 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5-0.0008002k)
- update to 1.5pre8
- add quota to BuildPreReq tag

* Tue Oct 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5-0.0007004k)
- errased the process concerning with /etc/service

* Tue Oct  2 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.5-0.0007002k)
- remove '--with-pam' option because pam is enaled by default in this
- version and specifying '--with-pam' causes disabling pam. Why?

* Mon Sep 10 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (1.5-0.0006010k)
- Change Group

* Mon Aug  3 2001 Toru Hoshina <toru@df-usa.com>
- (1.5-0.0006008k)
- fixed system-auth issue.

* Sun Apr 29 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5-0.0006004k)
- change %postun to %preun

* Mon Apr 16 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
-(1.5-0.0006002k)
 version 1.5pre6

* Thu Apr 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.5pre5-2k)
- version 1.5-pre5

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.4b2+asun2.1.3-12k)
- modified spec file with macro to smart!

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Fri Jul 28 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.4b2+asun2.1.3-8k)
- fix for rpm-3.0.5

* Fri Jul 28 2000 HOSONO Hidetomo <h@kondara.org>
- (1.4b2+asun2.1.3-7k)
- fix of %post and %postun about module configuration file.

* Wed May 24 2000 Daisuke Sato <d@kondara.org>
- fix to set 'alias net-pf-5 appletalk'
- fix to set allmulti mode to eth0

* Fri Feb 25 2000 Toru Hoshina <t@kondara.org>
- fix install.patch, don't make any symlinks to RPM_BUILD_ROOT!

* Sat Dec 11 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Oct 11 1999 Norihito Ohmori <nono@kondara.org>
- /usr/include/netatalk/at.h remove (conflict glibc-devel)

* Tue Aug 3 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- rpm-3.0 needs to remove vogus files from source.
  Removed files: etc/papd/.#magics.c, etc/.#diff
* Fri Jul 30 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- Change Copyright tag to BSD.
  Add /usr/bin/adv1tov2.
* Thu Apr 22 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- Correct librpcsvc.patch.  Move %changelog section last.
  Uncomment again -DNEED_QUOTA_WRAPPER in sys/linux/Makefile since
  LinuxPPC may need.
* Wed Mar 31 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- Comment out -DNEED_QUOTA_WRAPPER in sys/linux/Makefile.
* Sat Mar 20 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- Correct symbolic links to psf.
  Remove asciize function from nbplkup so as to display Japanese hostname.
* Thu Mar 11 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- Included MacPerl 5 script ICDumpSuffixMap which dumps suffix mapping
  containd in Internet Config Preference.
* Tue Mar 2 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- [asun2.1.3]
* Mon Feb 15 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- [pre-asun2.1.2-8]
* Sun Feb 7 1999 iNOUE Koich! <inoue@ma.ns.musashi-tech.ac.jp>
- [pre-asun2.1.2-6]
* Mon Jan 25 1999 iNOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- [pre-asun2.1.2-3]
* Thu Dec 17 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- [pre-asun2.1.2]
  Remove crlf patch. It is now a server's option.
* Thu Dec 3 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use stable version source netatalk-1.4b2+asun2.1.1.tar.gz
  Add uams directory
* Sat Nov 28 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use pre-asun2.1.1-3 source.
* Mon Nov 23 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use pre-asun2.1.1-2 source.
* Mon Nov 16 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Fix rcX.d's symbolic links.
* Wed Oct 28 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use pre-asun2.1.0a-2 source. Remove '%exclusiveos linux' line.
* Sat Oct 24 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use stable version source netatalk-1.4b2+asun2.1.0.tar.gz.
* Mon Oct 5 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use pre-asun2.1.0-10a source.
* Thu Sep 19 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use pre-asun2.1.0-8 source. Add chkconfig support.
* Sat Sep 12 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Comment out -DCRLF. Use RPM_OPT_FLAGS.
* Mon Sep 8 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use pre-asun2.1.0-7 source. Rename atalk.init to atalk.
* Mon Aug 22 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use pre-asun2.1.0-6 source.
* Mon Jul 27 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use pre-asun2.1.0-5 source.
* Tue Jul 21 1998 INOUE Koichi <inoue@ma.ns.musashi-techa.c.jp>
- Use pre-asun2.1.0-3 source.
* Tue Jul 7 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Add afpovertcp entries to /etc/services
- Remove BuildRoot in man8 pages
* Mon Jun 29 1998 INOUE Koichi <inoue@ma.ns.musashi-tech.ac.jp>
- Use modified sources 1.4b2+asun2.1.0 produced by Adrian Sun
  <asun@saul9.u.washington.edu> to provide an AppleShareIP file server
- Included AppleVolumes.system file maintained by Johnson
  <johnson@stpt.usf.edu>
* Mon Aug 25 1997 David Gibson <D.Gibson@student.anu.edu.au>
- Used a buildroot
- Use RPM_OPT_FLAGS
- Moved configuration parameters/files from atalk.init to /etc/atalk
- Separated devel package
- Built with shared libraries
* Sun Jul 13 1997 Paul H. Hargrove <hargrove@sccm.Stanford.EDU>
- Updated sources from 1.3.3 to 1.4b2
- Included endian patch for Linux/SPARC
- Use all the configuration files supplied in the source.  This has the
  following advantages over the ones in the previous rpm release:
	+ The printer 'lp' isn't automatically placed in papd.conf
	+ The default file conversion is binary rather than text.
- Automatically add and remove DDP services from /etc/services
- Placed the recommended /etc/services in the documentation
- Changed atalk.init to give daemons a soft kill
- Changed atalk.init to make configuration easier

* Wed May 28 1997 Mark Cornick <mcornick@zorak.gsfc.nasa.gov>
Updated for /etc/pam.d
