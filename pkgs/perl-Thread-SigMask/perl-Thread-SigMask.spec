%global         momorel 2

Name:           perl-Thread-SigMask
Version:        0.004
Release:        %{momorel}m%{?dist}
Summary:        Thread specific signal masks
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Thread-SigMask/
Source0:        http://www.cpan.org/authors/id/L/LE/LEONT/Thread-SigMask-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Cwd
BuildRequires:  perl-IO
BuildRequires:  perl-IPC-Open3
BuildRequires:  perl-Module-Build
BuildRequires:  perl-POSIX
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-XSLoader
Requires:       perl-XSLoader
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides per-thread signal masks. On non-threaded perls it will
be effectively the same as POSIX::sigprocmask. The interface works exactly
the same as sigprocmask.

%prep
%setup -q -n Thread-SigMask-%{version}

%build
%{__perl} Build.PL installdirs=vendor optimize="%{optflags}"
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorarch}/auto/Thread/SigMask
%{perl_vendorarch}/Thread/SigMask.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.004-2m)
- rebuild against perl-5.20.0

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.004-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
