%global momorel 1

Name:           graphite2
Version:        1.1.1
Release:        %{momorel}m%{?dist}
Summary:        Font rendering capabilities for complex non-Roman writing systems
Group:          Development/Tools

License:        LGPLv2+
URL:            http://sourceforge.net/projects/silgraphite/
Source0:        http://downloads.sourceforge.net/silgraphite/graphite2-%{version}.tgz
NoSource: 	0

BuildRequires:  cmake
BuildRequires:  freetype-devel
BuildRequires:  doxygen,asciidoc

%description
Graphite2 is a project within SILâ€™s Non-Roman Script Initiative and Language
Software Development groups to provide rendering capabilities for complex
non-Roman writing systems. Graphite can be used to create â€œsmart fontsâ€ capable
of displaying writing systems with various complex behaviors. With respect to
the Text Encoding Model, Graphite handles the "Rendering" aspect of writing
system implementation.

%package devel
Requires: %{name}%{?_isa} = %{version}-%{release}
Summary: Files for developing with graphite2
Group: Development/Libraries

Obsoletes: silgraphite-devel < 2.3.1-5

%description devel
Includes and definitions for developing with graphite2.

%prep
%setup -q

%build
%cmake -DGRAPHITE2_COMPARE_RENDERER=OFF .
make %{?_smp_mflags}
make docs

%check
ctest

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}/%{_libdir}/*.la

%files
%defattr(-,root,root,-)
%doc LICENSE COPYING ChangeLog doc/manual.html
%{_bindir}/gr2fonttest
%{_libdir}/libgraphite2.so.2.0.0

%files devel
%defattr(-,root,root,-)
%doc doc/doxygen/latex/refman.pdf
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/graphite2-release.cmake
%{_datadir}/%{name}/graphite2.cmake
%{_includedir}/%{name}
%{_libdir}/libgraphite2.so
%{_libdir}/pkgconfig/graphite2.pc

%changelog
* Thu May 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- Initial Commit Momonga Linux

* Tue Apr  3 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 1.1.1-2
- Fix FTBFS on ARM
* Wed Feb 27 2012 Nicholas van Oudtshoorn <vanoudt@gmail.com> - 1.1.1-1
- New upstream release
* Wed Feb 8 2012 Nicholas van Oudtshoorn <vanoudt@gmail.com> - 1.1.0-1
- New upstream release
* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild
* Fri Sep 23 2011 Nicholas van Oudtshoorn <vanoudt@gmail.com> - 1.0.3-1
- New upstream release
* Fri Aug 26 2011 Nicholas van Oudtshoorn <vanoudt@gmail.com> - 1.0.2-3
- Obsolete silgraphite
* Fri Aug 26 2011 Nicholas van Oudtshoorn <vanoudt@gmail.com> - 1.0.2-2
- Removed dependency on silgraphite-devel
- Stopped building comparerenderer - the only thing that depended on silgraphite
* Fri Aug 19 2011 Nicholas van Oudtshoorn <vanoudt@gmail.com> - 1.0.2-1
- Rebase to new release
- SPEC Cleanup
- Documentation is now properly installed
* Wed Aug 17 2011 Nicholas van Oudtshoorn <vanoudt@gmail.com> - 1.0.1-2
- Added some necessary requires
* Wed Aug 10 2011 Nicholas van Oudtshoorn <vanoudt@gmail.com> - 1.0.1-1
- Initial build of graphite2
