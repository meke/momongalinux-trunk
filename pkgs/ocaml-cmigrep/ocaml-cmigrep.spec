%global momorel 10

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

%global ocaml_major 3.12
%global ocaml_minor 1

%global _default_patch_fuzz 2

Name:           ocaml-cmigrep
Version:        1.5
Release:        %{momorel}m%{?dist}
Summary:        Search OCaml compiled interface (cmi) files

Group:          Development/Libraries
License:        GPLv2+
URL:            http://homepage.mac.com/letaris/
Source0:        http://homepage.mac.com/letaris/cmigrep-%{version}.tar.bz2
Source1:        http://caml.inria.fr/distrib/ocaml-%{ocaml_major}/ocaml-%{ocaml_major}.%{ocaml_minor}.tar.bz2
NoSource:       1
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:         cmigrep-make-without-godi-debian.patch
Patch2:         ocaml-user-cflags.patch
Patch3:         ocaml-3.11.0-ppc64.patch

# Sent upstream on 2008-11-20.
Patch10:        ocaml-cmigrep-3.11.0-updated-types.patch

BuildRequires:  ocaml(runtime) = %{ocaml_major}.%{ocaml_minor}
BuildRequires:  ocaml-findlib-devel >= 1.2.7-1m
BuildRequires:  ocaml-pcre-devel >= 6.2.3-1m
BuildRequires:  ocaml-ocamldoc
BuildRequires:  pcre-devel >= 8.31


%description
A utility to mine the data in Caml compiled interface (cmi) files, and
elisp that allows emacs to use cmigrep for completion.


%prep
%setup -q -n cmigrep-%{version}
%patch0 -p1

# Unpack OCaml sources into compiler/ subdirectory.
# XXX On Debian the compiled compiler libs are shipped in a
# +compiler-libs directory.  It would be good to copy this,
# however in Debian the only packages which actually use
# compiler-libs are camlp5 & cmigrep.
bzcat %{SOURCE1} | tar xf -
mv ocaml-%{ocaml_major}.%{ocaml_minor} compiler
pushd compiler
%patch2 -p1
%patch3 -p1
popd

%patch10 -p1


%build
# Build the compiler libs.
pushd compiler
CFLAGS="$RPM_OPT_FLAGS" ./configure \
    -bindir %{_bindir} \
    -libdir %{_libdir}/ocaml \
    -x11lib %{_libdir} \
    -x11include %{_includedir} \
    -mandir %{_mandir}/man1
make world
%if %opt
make opt.opt
%endif
popd

# Build cmigrep itself.
make byte
%if %opt
make all
%endif

strip cmigrep


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -m 0755 cmigrep $RPM_BUILD_ROOT%{_bindir}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc GPL README cmigrep.el
%{_bindir}/cmigrep


%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-10m)
- rebuild against pcre-8.31

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-9m)
- add source (Source0)

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-8m)
- rebuild against ocaml-3.12.1

* Wed Apr 13 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5-7m)
- apply upstream patch to enable build on new binutils

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-3m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Dec  4 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-9
- Rebuild for OCaml 3.11.0.

* Wed Nov 26 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-8
- Rebuild for OCaml 3.11.0+rc1.
- Remove string-index-from patch.

* Thu Nov 20 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-7
- Apply string-index-from patch from OCaml package.

* Thu Nov 20 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-6
- Fix for OCaml 3.11.0+beta1.

* Tue Nov 18 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-5
- Rebuild against OCaml 3.11.0+beta1.

* Tue Aug 26 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-4
- Restore ordinary patch fuzz.

* Tue Aug 26 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-3
- Rebuild.

* Mon Jun  9 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-2
- Include ppc64 compiler patch.
- Include MAP_32BITS compiler patch.
- Include no-executable-stack compiler patch.
- Rebuild for OCaml 3.10.2-4
  (https://bugzilla.redhat.com/show_bug.cgi?id=444428#c5)

* Mon Apr 28 2008 Richard W.M. Jones <rjones@redhat.com> - 1.5-1
- Initial RPM release.
