%global         momorel 2
%global         srcname telepathy-python

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-telepathy
Version:        0.15.19
Release:        %{momorel}m%{?dist}
Summary:        Python libraries for Telepathy
Group:          Development/Languages
License:        LGPLv2+
URL:            http://telepathy.freedesktop.org/wiki/
Source0:        http://telepathy.freedesktop.org/releases/%{srcname}/%{srcname}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-autotools.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-devel >= 2.7
BuildRequires:  dbus-python >= 0.80
BuildRequires:  libxslt
Requires:       dbus-python >= 0.80
BuildArch:      noarch

%description
Python libraries for use in Telepathy clients and connection managers.

%prep
%setup -q -n %{srcname}-%{version}
%patch0 -p1 -b .autotools

%build
%configure 
make
chmod 0644 examples/*

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} install_sh="./install-sh"
 
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS README NEWS examples/
%{python_sitelib}/telepathy

%changelog
* Mon Mar 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.19-2m)
- import build fix patch from Fedora

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.19-1m)
- update to 0.15.19

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.17-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.17-5m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.17-4m)
- build fix only (for make-3.82)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.17-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15.17-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.17-1m)
- import from Fedora devel

* Sat Mar 13 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.15.17-1
- Update to 0.15.17.

* Wed Mar 10 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.15.16-1
- Update to 0.15.16.

* Wed Jan 20 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.15.15-1
- Update to 0.15.15.

* Tue Jan 19 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.15.14-1
- Update to 0.15.14.

* Mon Dec 14 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.13-1
- Update to 0.15.13.

* Mon Oct 12 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.12-1
- Update to 0.15.12.
- Modify to use autotools.

* Tue Aug 18 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.11-1
- Update to 0.15.11.

* Fri Aug 14 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.10-1
- Update to 0.15.10.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.15.8-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jun 15 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.8-1
- Update to 0.15.8.
- Add NEWS file to docs.

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.15.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 17 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.7-1
- Update to 0.15.7.

* Wed Jan  7 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.6-1
- Update to 0.15.6.

* Tue Jan  6 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.5-1
- Update to 0.15.5.

* Mon Jan  5 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.15.4-1
- Update to 0.15.4.

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.15.3-2
- Rebuild for Python 2.6

* Tue Oct 21 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.15.3-1
- Update to 0.15.3.

* Mon Oct 20 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.15.2-1
- Update to 0.15.2

* Tue Aug 26 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.15.1-1
- Update to 0.15.1.
- Drop examples-README.  Included in upstream now.

* Sun Mar  2 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.15.0-1
- Update to 0.15.0.

* Thu Nov 29 2007 Matej Cepl <mcepl@redhat.com> 0.14.0-4
- apparently some part of setup.py decided that everything in examples/
  directory, which doesn't have .py extension should be executable.

* Wed Nov 28 2007 Matěj Cepl <mcepl@redhat.com> 0.14.0-3
- Add examples/README missing from the upstream tarball (examples doesn't
  make much sense without that).
- Fix bloody TABs into spaces so that rpmlint is silent.

* Thu Nov  1 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.14.0-2
- Add examples to docs. (#362161)

* Sat Oct 13 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.14.0-1
- Update to 0.14.0.

* Sun Aug  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.13.13-2
- Update license tag.

* Mon Jul  2 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.13.13-1
- Update to 0.13.13.
- Add minimum ver of dbus-python.

* Mon Jun  4 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.13.12-1
- Update to 0.13.12.

* Fri May 18 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.13.11-1
- Update to 0.13.11.
- Add BR for libxslt.

* Mon Feb 19 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.13.8-1
- Update to 0.13.8.

* Sun Jan  7 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.13.7-4
- Rebuild against the new python should actually work now.

* Fri Dec  8 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.13.7-3
- Rebuild against new python.

* Fri Nov 24 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.13.7-2
- Add BR & Requires on dbus-python.

* Fri Nov 24 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.13.7-1
- Update to 0.13.7.

* Tue Oct 10 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.13.3-1
- Update to 0.13.3.
- Drop connection-manager patch, fixed upstream.

* Mon Oct  2 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.13.2-3
- Actually use the name variable for the patch.

* Mon Oct  2 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.13.2-2
- Patch for connection manager.

* Sat Sep  2 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.13.2-1
- Initial FE spec.

