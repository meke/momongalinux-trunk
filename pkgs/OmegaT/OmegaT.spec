%global momorel 3

Name:		OmegaT
%define namer	omegat
Summary:	Computer Aid Translation tool
Version:	2.2.3
%define versionr	2.2.3_02_Beta
Release:	%{momorel}m%{?dist}
Source0:	http://dl.sourceforge.net/sourceforge/omegat/%{name}_%{versionr}_Source.zip
#NoSource:       0
Source2:	OmegaT-lib-mnemonics-build.xml
Source3:	OmegaT-build.xml
Source4:	OmegaT.sh
Url:		http://www.omegat.org/
Group:		Applications/Text
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires:	ant, java-devel >= 1.6.0, jpackage-utils
BuildRequires:	desktop-file-utils, dos2unix
BuildRequires:	htmlparser, vldocking >= 2.1.4
BuildRequires:	jna
BuildRequires:	ws-jaxme
BuildRequires:	swing-layout

Requires:	java, jpackage-utils
Requires:	vldocking >= 2.1.4
Requires:	htmlparser
Requires:	hunspell
Requires:	jna
Requires:	swing-layout
Requires:	ws-jaxme

License:	GPLv2+
BuildArch:	noarch
# http://svn.debian.org/wsvn/pkg-java/trunk04-get-rid-of-MRJAdapter.patch
Patch1:		OmegaT-04-get-rid-of-MRJAdapter.patch
Patch2:		OmegaT-help-path.patch
# http://svn.debian.org/wsvn/pkg-java/trunk/omegat/debian/patches/05-remove-jmyspell-alternative.patch
Patch3:		OmegaT-05-remove-jmyspell-alternative.patch
# based on http://svn.debian.org/wsvn/pkg-java/trunk/omegat/debian/patches/06-use-external-hunspell.patch
Patch4:		OmegaT-06-use-external-hunspell.patch
Patch5:		OmegaT-07-use-openjdk-swingworker.patch

%description
OmegaT is a free translation memory application written in Java.
It is a tool intended for professional translators. It does not
translate for you!

OmegaT has the following features:

 * Fuzzy matching
 * Match propagation
 * Simultaneous processing of multiple-file projects
 * Simultaneous use of multiple translation memories
 * External glossaries
 * Document file formats:
	XHTML and HTML
	Microsoft Office 2007 XML
	OpenOffice.org/StarOffice
	XLIFF (Okapi)
	MediaWiki (Wikipedia)
	Plain text
 * Unicode (UTF-8) support: can be used with non-Latin alphabets
 * Support for right-to-left languages
 * Compatible with other translation memory applications (TMX)

%prep
%setup -q -c -n %{name}-%{version}
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1

# not needed outside Netbeans
rm nbproject/org-netbeans-modules-java-j2seproject-copylibstask.jar

# clean dependencies:
rm lib/vldocking_2.1.4.jar
rm lib/htmlparser.jar
rm lib/sources/htmlparser1_6_20060610.zip-source.zip
rm lib/jmyspell-core-1.0.0-beta-2.jar
rm lib/jna.jar
rm -r native/*
rm lib/swing-layout-1.0.jar

###  JAXB dependencies
rm lib/activation.jar
rm lib/jaxb-api.jar
rm lib/jaxb-impl.jar
rm lib/jsr173_1.0_api.jar 
rm lib/sources/JAXB/jsr173_1.0_src.jar

# not needed outside MacOSX:
rm lib/MRJAdapter.jar
rm lib/sources/MRJAdapter-source.zip

# not needed outside windows
rm -rf release/win32-specific/

# seems they are not really needed... 
rm -rf ./test/lib/junit-4.4.jar
rm -rf ./test/lib/xmlunit-1.1.jar

## gen/lib/jaxb-xjc.jar
rm -rf gen/lib/jaxb-xjc.jar

# this shows the netbeans jar needed to compile nmonics...
find . -name \*.jar -exec echo {}  \;


rm -rf lib/swing-worker-1.2.jar

%build

#limpieza:
sed -i '/class-path/I d' manifest-template.mf

pushd lib-mnemonics
cp %{SOURCE2} build.xml
ant dist
popd

cp %{SOURCE3} build.xml
ant dist

%install 
rm -Rf $RPM_BUILD_ROOT

#install our jar file
#make some install dirs
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_javadir}
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{namer}/docs
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{namer}/images

install -pm  0755 dist/OmegaT.jar $RPM_BUILD_ROOT%{_javadir}/OmegaT-%{version}.jar
install -pm  0755 lib-mnemonics/dist/lib-mnemonics.jar $RPM_BUILD_ROOT%{_javadir}/OmegaT-lib-mnemonics-%{version}.jar

pushd $RPM_BUILD_ROOT%{_javadir}
	ln -s OmegaT-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/OmegaT.jar
	ln -s OmegaT-lib-mnemonics-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/lib-mnemonics.jar
popd

cp -pR release/index.html docs/ images/ $RPM_BUILD_ROOT%{_datadir}/%{namer}/

#create our launch wrapper script
install -pm  0755 %{SOURCE4} $RPM_BUILD_ROOT%{_bindir}/%{namer}

#make our launch wrapper executable
chmod +x $RPM_BUILD_ROOT%{_bindir}/*

#Menu entry
install -d -m755 %{buildroot}%{_datadir}/applications

cat > %{buildroot}%{_datadir}/applications/%{namer}.desktop <<EOF
[Desktop Entry]
Encoding=UTF-8
Name=%name
Exec=%{namer}
Icon=/usr/share/omegat/images/OmegaT.png
Comment=Computer Aid Translation tool
Terminal=false
Type=Application
Categories=Translation;Java;Office;
X-AppInstall-Package=%{namer}
EOF

desktop-file-install  --vendor= --dir=%{buildroot}%{_datadir}/applications/ %{buildroot}%{_datadir}/applications/%{namer}.desktop 

# fixing end of line making rpmlint happy
dos2unix -k release/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,0755)
%dir %{_datadir}/%{namer}
%{_datadir}/%{namer}/*
%{_bindir}/*
%{_javadir}/*
%{_datadir}/applications/%{namer}.desktop

%doc ./release/changes.txt release/doc-license.txt release/license.txt release/readme*.txt release/join.html


%changelog
* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.2-3m)
- remove fedora from vendor (desktop-file-install)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-2m)
- rebuild for new GCC 4.6

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.3-0.2.beta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Dec 29 2010 <ismael@olea.org> - 2.2.3-0.1.Beta
- updated to 2.2.3_Beta
- fixed hunspell integration
- swing-worker patch

* Wed Aug 11 2010 Ismael Olea <ismael@olea.org> 2.0.5_04-1
- updating to 2.0.5_04
- removing references to javadoc generation
- removing support for jmyspell (from Tiago's OmegaT-05-remove-jmyspell-alternative.patch)
- using local hunspell (from Tiago's OmegaT-06-use-external-hunspell.patch )
- spec cleaning

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.7.3_04-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.7.3_04-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.7.3_04-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Nov 26 2008 Ismael Olea <ismael@olea.org> 2.0.0_Beta-2
- Porting MRJAdapter removing patch
- Porting OmegaT-help-path.patch from http://svn.debian.org/wsvn/pkg-java/trunk/omegat/debian/patches/03-location-of-help-files.dpatch?op=file&rev=0&sc=0)

* Wed Nov 26 2008 Ismael Olea <ismael@olea.org> 2.0.0_Beta-1
- First 2.0 beta version


* Fri Nov 21 2008 Ismael Olea <ismael@olea.org> 1.7.3_04-2
- stupid new release caused by my fault

* Fri Nov 21 2008 Ismael Olea <ismael@olea.org> 1.7.3_04-1
- updating to 1.7.3_04

* Wed Nov 18 2008 Ismael Olea <ismael@olea.org> 1.7.3_03-6
- Fixing htmlparser non-present dependency (bug #471573)

* Thu Sep 18 2008 Ismael Olea <ismael@olea.org> 1.7.3_03-5
- QA changes

* Wed Sep 17 2008 Ismael Olea <ismael@olea.org> 1.7.3_03-4
- minor and cosmetic changes

* Wed Aug 27 2008 Ismael Olea <ismael@olea.org> 1.7.3_03-3olea
- new build.xml for removing org-netbeans-modules-java-j2seproject-copylibstask.jar dependency
- not javadoc support now
- OmegaT-ant.properties not needed by this build.xml

* Wed Aug 27 2008 Ismael Olea <ismael@olea.org> 1.7.3_03-2olea
- cleaning dependencies, apparently running, lack paths configs

* Tue Aug 26 2008 Ismael Olea <ismael@olea.org> 1.7.3_03-1olea
- updating to 1.7.3_03 version

* Fri Jul 4 2008 Ismael Olea <ismael@olea.org> 1.7.3_02-1olea
- updating to 1.7.3_02 version

* Mon Feb 11 2008 Ismael Olea <ismael@olea.org> 1.7.3-5olea
- fixing according to https://bugzilla.redhat.com/show_bug.cgi?id=428798#c3
- comments on https://bugzilla.redhat.com/show_bug.cgi?id=428798#c4

* Sun Feb 10 2008 Ismael Olea <ismael@olea.org> 1.7.3-4olea
- fixing according to https://bugzilla.redhat.com/show_bug.cgi?id=428798#c1

* Mon Jan 21 2008 Ismael Olea <ismael@olea.org> 1.7.3-3olea
- rpmlinting

* Wed Jan 9 2008 Ismael Olea <ismael@olea.org> 1.7.3-2olea
- Compiling from sources

* Wed Jan 9 2008 Ismael Olea <ismael@olea.org> 1.7.3-1olea
- updating to 1.7.3

* Wed Jan 9 2008 Ismael Olea <ismael@olea.org> 1.7.2-1olea
- compiling on Fedora

* Fri Nov 12 2007 Patred Theknight <edupclos@gmail.com>1.7.2-1pclos_edulos
- Initial Build 
