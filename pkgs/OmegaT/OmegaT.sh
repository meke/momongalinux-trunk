#!/bin/sh
. /usr/share/java-utils/java-functions

BASE_JARS="OmegaT htmlparser vldocking jna lib-mnemonics jaxme/jaxme2 jaxme/jaxme2-rt jaxme/jaxmejs jaxme/jaxmepm jaxme/jaxmexs swing-layout"

MAIN_CLASS=org.omegat.Main

set_jvm
set_flags $BASE_FLAGS
set_options $BASE_OPTIONS
set_classpath $BASE_JARS

run  "$@"
