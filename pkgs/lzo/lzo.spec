%global momorel 1

Summary: Data compression library with very fast (de)compression
Name: lzo
Version: 2.06
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://www.oberhumer.com/opensource/lzo/

Source0: http://www.oberhumer.com/opensource/lzo/download/%{name}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: nasm

%description
LZO is a portable lossless data compression library written in ANSI C.
It offers pretty fast compression and *extremly* fast decompression.
Decompression requires no memory.

In addition there are slower compression levels achieving a quite
competitive compression ratio while still decompressing at
this very high speed.

%package minilzo
Summary: Mini version of lzo for apps which don't need the full version
Group: System Environment/Libraries

%description minilzo
A small (mini) version of lzo for embedding into applications which don't need
full blown lzo compression support.

%package devel
Summary: Development libraries, header files for lzo
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: %{name}-minilzo = %{version}-%{release}

%description devel
Development libraries, Header files for LZO.

%prep
%setup -q
%build
%configure \
	--disable-dependency-tracking \
	--disable-static \
	--enable-shared \
	--enable-asm

%make

# build minilzo
gcc %{optflags} -fpic -Iinclude/lzo -o minilzo/minilzo.o -c minilzo/minilzo.c
gcc -g -shared -o libminilzo.so.0 -Wl,-soname,libminilzo.so.0 minilzo/minilzo.o

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

install -m 755 libminilzo.so.0 %{buildroot}%{_libdir}
ln -s libminilzo.so.0 %{buildroot}%{_libdir}/libminilzo.so
install -p -m 644 minilzo/minilzo.h %{buildroot}%{_includedir}/lzo

rm -rf %{buildroot}/%{_datadir}/doc/lzo

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post minilzo -p /sbin/ldconfig

%postun minilzo -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS NEWS README THANKS doc/LZO.FAQ doc/LZO.TXT
%doc doc/LZOAPI.TXT
%{_libdir}/liblzo2.so.*

%files minilzo
%defattr(-,root,root,-)
%doc minilzo/README.LZO
%{_libdir}/libminilzo.so.0

%files devel
%defattr(-,root,root,-)
%{_includedir}/lzo
%{_libdir}/lib*lzo*.so

%changelog
* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.06-1m)
- update 2.06

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.03-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.03-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.03-5m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.03-4m)
- split off minilzo subpackage
- --disable-dependency-tracking --disable-static

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.03-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.03-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.03-1m)
- update 2.03

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.02-2m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02-2m)
- %%NoSource -> NoSource

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.01-2m)
- delete libtool library

* Thu Jul 21 2005 Toru Hoshina <t@momonga-linux.org>
- (2.01-1m)
  update to 2.01

* Sat Jun 11 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.00-1m)
  update to 2.00

* Fri Oct 24 2003 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.08-1m)
- first release for momonga

