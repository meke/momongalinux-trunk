%global momorel 1
%global qtdir %{_libdir}/qt3

Summary: PIN entry
Name: pinentry
Version: 0.8.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: User Interface/X
URL: http://www.gnupg.org/
Source0: ftp://ftp.gnupg.org/gcrypt/pinentry/%{name}-%{version}.tar.bz2
NoSource: 0
# borrowed from opensuse
Source10: pinentry-wrapper

## Patches not yet in SVN
Patch53: 0001-Fix-qt4-pinentry-window-created-in-the-background.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ncurses
Requires: info
BuildRequires: ncurses-devel
Provides: pinentry-curses = %{version}-%{release}
Provides: pinentry-gui = %{version}-%{release}

%{?include_specopt}
%{?!pingtk:  %global pingtk  1}
%{?!pingtk2: %global pingtk2 1}
%{?!pinqt:   %global pinqt   1}
%{?!pinqt4:  %global pinqt4  1}
##
%global gtk_package 1
%if !%{pingtk}
%if !%{pingtk2}
%global gtk_package 0
%endif
%endif
##
%if %{pingtk}
Requires: gtk+1
BuildRequires: gtk+1-devel
Provides: pinentry-gtk
%endif
%if %{pingtk2}
Requires: gtk2
BuildRequires: gtk2-devel
Provides: pinentry-gtk-2
%endif
%if %{pinqt}
Requires: qt3
BuildRequires: qt3-devel
Provides: pinentry-qt
%endif
%if %{pinqt4}
Requires: qt >= 4.8.4
BuildRequires: qt-devel >= 4.8.4
Provides: pinentry-qt4
%endif

%description
The pinentry is a small GUI application used to enter PINs or
Passphrases. It is usually invoked by gpg-agent.

%package gtk
Summary: Passphrase/PIN entry dialog based on GTK+
Group:   Applications/System
Requires: %{name} = %{version}-%{release}
%description gtk
Pinentry is a collection of simple PIN or passphrase entry dialogs which
utilize the Assuan protocol as described by the aegypten project; see
http://www.gnupg.org/aegypten/ for details.
This package contains the GTK GUI based version of the PIN entry dialog.

%package qt
Summary: Passphrase/PIN entry dialog based on Qt3
Group:   Applications/System
Requires: %{name} = %{version}-%{release}

%description qt
Pinentry is a collection of simple PIN or passphrase entry dialogs which
utilize the Assuan protocol as described by the aegypten project; see
http://www.gnupg.org/aegypten/ for details.
This package contains the Qt3 GUI based version of the PIN entry dialog.

%package qt4
Summary: Passphrase/PIN entry dialog based on Qt4
Group:   Applications/System
Requires: %{name} = %{version}-%{release}

%description qt4
Pinentry is a collection of simple PIN or passphrase entry dialogs which
utilize the Assuan protocol as described by the aegypten project; see
http://www.gnupg.org/aegypten/ for details.
This package contains the Qt4 GUI based version of the PIN entry dialog.
Support for Qt4 is new, and a bit experimental.

%prep
%setup -q

%patch53 -p1 -b .rhbug_589532

%build
%if %{pinqt}
export QTDIR=%{qtdir} QTLIB=%{qtdir}/lib
%endif
%if %{pinqt4}
pushd qt4
moc-qt4 pinentrydialog.h > pinentrydialog.moc
moc-qt4 qsecurelineedit.h > qsecurelineedit.moc
popd
%endif
./autogen.sh
%configure \
	--disable-rpath \
	--disable-dependency-tracking \
%if %{pingtk}
	--enable-pinentry-gtk \
%else
	--disable-pinentry-gtk \
%endif
%if %{pingtk2}
	--enable-pinentry-gtk2 \
%else
	--disable-pinentry-gtk2 \
%endif
	--enable-pinentry-curses \
	--enable-fallback-curses \
%if %{pinqt}
	--enable-pinentry-qt \
%else
	--disable-pinentry-qt \
%endif
%if %{pinqt4}
	--enable-pinentry-qt4 \
%else
	--disable-pinentry-qt4 \
%endif
	--disable-embedded --disable-palmtop --enable-maintainer-mode
%make


%install
rm -rf %{buildroot}
%makeinstall
rm -f %{buildroot}%{_infodir}/dir

install -p -m755 -D %{SOURCE10} %{buildroot}%{_bindir}/pinentry

%clean
rm -rf %{buildroot}


%post
/sbin/install-info %{_infodir}/pinentry.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/pinentry.info %{_infodir}/dir || :
fi


%files
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%doc ChangeLog
%doc NEWS
%doc README
%doc THANKS
%doc TODO
%{_bindir}/pinentry
%{_bindir}/pinentry-curses
%{_infodir}/pinentry*

%if %{gtk_package}
%files gtk
%defattr(-,root,root,-)
%if %{pingtk}
%{_bindir}/pinentry-gtk
%endif
%if %{pingtk2}
%{_bindir}/pinentry-gtk-2
%endif
%endif

%if %{pinqt}
%files qt
%defattr(-,root,root,-)
%{_bindir}/pinentry-qt
%endif

%if %{pinqt4}
%files qt4
%defattr(-,root,root,-)
%{_bindir}/pinentry-qt4
%endif

%changelog
* Tue Apr 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.3-1m)
- enable gtk2, qt, qt4 frontend
- update to 0.8.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-2m)
- rebuild for new GCC 4.6

* Tue Dec 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-4m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-2m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0
- import ptches from Fedora (includes upstream patches)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.6-2m)
- use BuildRequires

* Sun Nov 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.6-1m)
- update to 0.7.6 and modify based on Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-7m)
- Provides: pinentry-gui for mozilla-opensc-signer

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-6m)
- rebuild against rpm-4.6

* Sun Dec  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-5m)
- remove --entry option of install-info

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-4m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.5-3m)
- rebuild against gcc43

* Fri Mar 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-2m)
- build fix
- import glib2.patch from Fedora
 +* Tue Mar 25 2008 Rex Dieter <rdieter@fedoraproject.org> - 0.7.4-5
 +- pinentry failed massrebuild attempt for GCC 4.3 (#434400)

* Wed Mar 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-3m)
- revise %%files for rpm-4.4.2

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.7.2-2m)
- rebuild against libtermcap and ncurses

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (0.7.2-1m)
- Alter -> Main

* Mon Jun 14 2004 TAKAHASHI Tamotsu <tamo>
- (0.7.1-2m)
- does not conflict with newpg
 because old newpg has been removed

* Fri May 07 2004 TAKAHASHI Tamotsu <tamo>
- (0.7.1-1m)

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6.9-3m)
- rebuild against ncurses 5.3.

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6.9-2m)
- revised spec for rpm 4.2.

* Mon Nov 02 2003 TAKAHASHI Tamotsu <tamo>
- (0.6.9-1m)
- pinentry, divided from newpg
- Group: User Interface/X
- --enable-maintainer-mode
- correct and install info file
- BuildPreReq: gtk+1-devel, ncurses-devel
- Requires: gtk+1, ncurses, info
- Conflicts: newpg
