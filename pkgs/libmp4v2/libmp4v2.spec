%global momorel 5
%global src_name mp4v2

Summary: Library for working with files using the mp4 container format
Name: libmp4v2
Version: 1.9.1
Release: %{momorel}m%{?dist}
License: MPLv1.1
Group: System Environment/Libraries
URL: http://resare.com/libmp4v2/
Source0: http://mp4v2.googlecode.com/files/%{src_name}-%{version}.tar.bz2
NoSource: 0
# Only here to be in the source package, "just in case, and FYI"
# Source1: http://resare.com/libmp4v2/mklibmp4v2/mklibmp4v2-r51.tar.bz2
#Patch0: libmp4v2-1.5.0.1-glibc210.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The libmp4v2 library provides an abstraction layer for working with files
using the mp4 container format. This library is developed by mpeg4ip project
and is an exact copy of the library distributed in the mpeg4ip package.


%package devel
Summary: Development files for the mp4v2 library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Development files and documentation needed to develop and compile programs
using the libmp4v2 library.


%prep
%setup -q -n %{src_name}-%{version}
#%patch0 -p1 -b .glibc210


%build
%configure \
    --disable-static \
    --disable-dependency-tracking
%{__make} %{?_smp_mflags}


%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}
%{__rm} -rf %{buildroot}%{_mandir}/manm/


%clean
%{__rm} -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,0755)
%doc COPYING
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,0755)
%doc README 
%dir %{_includedir}/mp4v2
%{_includedir}/mp4v2/*.h
%exclude %{_libdir}/*.la
%{_libdir}/*.so
%{_mandir}/man?/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.1-3m)
- full rebuild for mo7 release

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (1.9.1-1m)
- add %%dir

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-1m)
- update 1.9.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0.1-3m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0.1-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.0.1-1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.5.0.1-6
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 Matthias Saou <http://freshrpms.net/> 1.5.0.1-5
- Rebuild for new BuildID feature.

* Sun Aug  5 2007 Matthias Saou <http://freshrpms.net/> 1.5.0.1-4
- Update License field.

* Fri Dec 15 2006 Matthias Saou <http://freshrpms.net/> 1.5.0.1-3
- Spec file cleanup (habits, mostly) preparing to submit for Extras inclusion.

* Fri Oct 06 2006 Thorsten Leemhuis <fedora [AT] leemhuis [DOT] info> 1.5.0.1-2
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Tue Jul 18 2006 Noa Resare <noa@resare.com> 1.5.0.1-1
- new upstream release

* Sat May 13 2006 Noa Resare <noa@resare.com> 1.4.1-3
- disabled static lib
- use DESTDIR
- disable-dependency-tracking for faster builds
- removed a manpage template file apt.mpt.gz

* Mon May 08 2006 Noa Resare <noa@resare.com> 1.4.1-2
- specfile cleanups

* Fri May 05 2006 Noa Resare <noa@resare.com> 1.4.1-1.lvn5
- initial release

