%global momorel 5
%global capseo_version 0.3.0

# Tarfile created using git
# git clone git://gitorious.org/capseo/mainline.git libcapseo
# cd libcapseo
# git archive --format=tar --prefix=libcapseo-%{capseo_version}/ master . | bzip2 > libcapseo-%{capseo_version}-%{gitdate}.tar.bz2

%global gitdate 20090214

Summary:        A realtime encoder/decoder library
Name:           libcapseo
Version:        %{capseo_version}
Release:        0.%{gitdate}.%{momorel}m%{?dist}
License:        GPLv2+
Group:          System Environment/Libraries
URL:            http://gitorious.org/projects/capseo/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libtool automake autoconf 
BuildRequires:  pkgconfig
BuildRequires:  libtheora-devel
BuildRequires:  libogg-devel
BuildRequires:  libX11-devel
BuildRequires:  mesa-libGL-devel

# Fedora specific snapshot no upstream release (yet)
Source0:        %{name}-%{version}-%{gitdate}.tar.bz2

%description
Capseo is a realtime video codec being used by libcaptury/captury
for encoding captured video frames in realtime. (think of FRAPS codec).

Applications using capseo currently are libcaptury for encoding
captured data, e.g. currently from third-party OpenGL applications
via captury, the OpenGL video capturing tool.

%package devel
Summary: Files needed for development using %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libtheora-devel
Requires: libogg-devel
Requires: libX11-devel
Requires: mesa-libGL-devel
Requires: pkgconfig

%description devel
This package contains libraries and header files for
developing applications that use %{name}.

%package tools
Summary: Encoding/Decoding tools for capseo
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description tools
Utilities for capseo

%prep
%setup -q -n %{name}-%{version}
./autogen.sh

%build
%configure --disable-static --enable-theora --disable-examples
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING TODO
%{_libdir}/*.so.*

%files tools
%defattr(-,root,root,-)
%{_bindir}/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*.h
%{_libdir}/libcapseo.so
%{_libdir}/pkgconfig/capseo.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-0.20090214.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-0.20090214.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-0.20090214.3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-0.20090214.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-0.20090214.1m)
- update 0.3.0 git20090214 version 

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-0.20080323.2m)
- rebuild against rpm-4.6

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-0.20080323.1m)
- import from Fedora devel

* Sat May 3 2008 Shawn Starr <shawn.starr@rogers.com> 0.2.0-0.1.20080323git1c5f3e5
- Initial Fedora package.
