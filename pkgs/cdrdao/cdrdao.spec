%global momorel 6

Summary: A Disk-At-Once (DAO) Audio CD writer and GUI editor
Name: cdrdao

### include local configuration
%{?include_specopt}

### default configurations
# If you'd like to change these configurations, please copy them to
# ${HOME}/rpm/specopt/cdrdao.specopt and edit it.

## Configuration
%{?!with_gcdmaster:     %global with_gcdmaster             1}
%{?!with_lame:          %global with_lame                  1}

Version: 1.2.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://cdrdao.sourceforge.net/
Source0: http://dl.sourceforge.net/project/cdrdao/cdrdao/%{version}/cdrdao-%{version}.tar.bz2
NoSource: 0
Patch1: %{name}-%{version}-version.patch
Patch2: cdrdao-1.2.3rc2-glibc212.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: desktop-file-utils
BuildRequires: libao-devel >= 1.0.0
BuildRequires: libmad
BuildRequires: libogg-devel
BuildRequires: libvorbis-devel
BuildRequires: libvorbis-devel
%if %{with_gcdmaster}
BuildRequires: gtkmm-devel
BuildRequires: libgnomeuimm-devel >= 2.6.0
BuildRequires: libsigc++-devel
BuildRequires: gnome-libs-devel
%endif
%if %{with_lame}
BuildRequires: lame-devel
%endif

%description
Writes audio CD-Rs in disc-at-once (DAO) mode allowing control over
pre-gaps (length down to 0, nonzero audio data) and sub-channel
information like ISRC codes. All data that is written to the disc must
be specified with a text file. Audio data may be in WAVE or raw
format. Gcdmaster is a GNOME2 GUI front-end to cdrdao that makes it easy to
visualize and manipulate audio information before burning it onto
CD. Its features include: cut/copy/paste of sound samples, track marks
edition and silence insertion.

%if %{with_gcdmaster}
%package gcdmaster
Summary: Graphical front end to cdrdao for composing audio CDs
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires(post): GConf2
Requires(postun): GConf2
Requires(preun): GConf2
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires(post): psmisc
Requires(postun): psmisc
Requires(preun): psmisc
Requires(post): shared-mime-info
Requires(postun): shared-mime-info

%description gcdmaster
gcdmaster allows the creation of toc-files for cdrdao and
can control the recording process. Its main application is
the composition of audio CDs from one or more audio files.
It supports PQ-channel editing, entry of meta data like
ISRC codes/CD-TEXT and non destructive cut of the audio data.
%endif

%prep
%setup -q

%patch1 -p1 -b .version
%patch2 -p1 -b .glibc212

%build
%configure \
%if ! %{with_gcdmaster}
	--without-xdao \
%endif
%if ! %{with_lame}
	--without-lame \
%endif
	--with-ogg-support

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%if %{with_gcdmaster}
# fix up gcdmaster.desktop
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category X-Fedora \
  --remove-category Application \
  %{buildroot}%{_datadir}/applications/gcdmaster.desktop
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%if %{with_gcdmaster}
%post gcdmaster
%{_bindir}/update-mime-database %{_datadir}/mime > /dev/null || :
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null || :
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
%{_bindir}/gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/gcdmaster.schemas > /dev/null  || :
killall -HUP gconfd-2 &>/dev/null || :

%postun gcdmaster
%{_bindir}/update-mime-database %{_datadir}/mime > /dev/null
if [ -x %{_bindir}/update-desktop-database ]; then %{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null || : ; fi
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  %{_bindir}/gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/gcdmaster.schemas >/dev/null || :
  killall -HUP gconfd-2 &>/dev/null || :
fi

%preun gcdmaster
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  %{_bindir}/gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/gcdmaster.schemas > /dev/null || :
  killall -HUP gconfd-2 &>/dev/null || :
fi
%endif

%files
%defattr(-,root,root)
%doc AUTHORS COPYING CREDITS ChangeLog README*
%{_bindir}/cdrdao
%{_bindir}/cue2toc
%{_bindir}/toc2cddb
%{_bindir}/toc2cue
%if %{with_lame}
%{_bindir}/toc2mp3
%endif
%{_mandir}/man1/cdrdao.1*
%{_mandir}/man1/cue2toc.1*
%{_mandir}/man1/toc2cddb.1*
%{_mandir}/man1/toc2cue.1*
%{_datadir}/cdrdao/drivers

%if %{with_gcdmaster}
%files gcdmaster
%defattr(-,root,root)
%config %{_sysconfdir}/gconf/schemas/gcdmaster.schemas
%{_bindir}/gcdmaster
%{_datadir}/application-registry/gcdmaster.applications
%{_datadir}/applications/gcdmaster.desktop
%{_datadir}/gcdmaster
%{_mandir}/man1/gcdmaster.1*
%{_datadir}/mime/packages/gcdmaster.xml
%{_datadir}/mime-info/gcdmaster.keys
%{_datadir}/mime-info/gcdmaster.mime
%{_datadir}/pixmaps/gcdmaster-doc.png
%{_datadir}/pixmaps/gcdmaster.png
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-6m)
- rebuild for new GCC 4.6

* Mon Nov 29 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (1.2.3-5m)
- add BuildRequires: gnome-libs-devel
- fix for without gcd_master

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-4m)
- rebuild for new GCC 4.5

* Thu Sep  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-3m)
- [BUG FIX] fix %%post and %%postun

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-2m)
- full rebuild for mo7 release

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-1m)
- udpate 1.2.3-release

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-0.1.3m)
- apply glibc212 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-0.1.1m)
- update to version 1.2.3rc2
- import version.patch from Fedora
 +* Thu Apr 16 2009 Denis Leroy <denis@poolshark.org> - 1.2.3-0.rc2.2
 +- Make sure version is printed with usage, to fix k3b
- remove merged sigc.patch and gcc43.patch
- modify %%post and %%postun gcdmaster
- add %%preun gcdmaster
- fix up gcdmaster.desktop

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2-8m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-7m)
- %%NoSource -> NoSource

* Sun Jan 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-6m)
- add patch for gcc43, generated by gen43patch(v1)
- revise BuildRequires:

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-5m)
- import sigc.patch from cooker
 +* Tue Sep 11 2007 Adam Williamson <awilliamson@mandriva.com> 1.2.2-5mdv2008.0
 +- add patch0 to fix compilation issue caused by use of old sigc++ API

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-4m)
- rebuild against libvorbis-1.2.0-1m

* Wed Jul  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.2-3m)
- add BuildRequires: libgnomeuimm >= 2.6.0 for build gcdmaster

* Sun Jun 17 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.2-2m)
- delete BuildRequires: pccts >= 1.0.0
  cdrdao use own pccts(antlr)
- delete BuildRequires: cdrtools-devel >= 2.01-0.0.25.3m

* Thu Mar 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-1m)
- version 1.2.2
- add a package cdrdao-gcdmaster
- use specopt to control with_gcdmaster and with_lame

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-2m)
- delete duplicated dir

* Fri Dec 23 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.2.1-1m)
- update to 1.2.1
- change source URI

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.1.9-1m)
- update to 1.1.9 (rebuild against gcc-c++-3.4.1)
- add BuildPrereq: gcc-c++

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.8-3m)
- tarball changed

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.8-2m)
- rebuild without libgnomemm.

* Sun Feb 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.8-1m)
- major feature enhancements

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-6m)
- rebuild against lib*mm

* Wed Aug 27 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.7-5m)
- rebuild against ORBit2-2.7.6, libbonobomm-1.3.6, libbonobouimm-1.3.6, libgnomeuimm-1.3.17

* Mon Jun 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-4m)
- rebuild against orbitcpp

* Thu Apr 12 2003 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.1.7-3m)
- BuildPrereq: cdrtools-devel.

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.7-2m)
- use gtk2. (TORIAE-ZU)

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.7-1m)

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.6-2m)
- remove gcc-2.96.patch
- add gcc-3 patch

* Tue Sep 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.1.6-1m)

* Mon May 27 2002 Toru Hoshina <t@kondara.org>
- (1.1.5-14k)                      
- rebuild by g++ 3.1.

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (1.1.5-12k)                      
- add BuildPreReq: autoconf >= 2.52-8k

* Tue Jan  8 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.5-10k)
- autoconf 1.5

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.1.5-8k)
- nigittenu

* Sun Nov  4 2001 Toru Hoshina <t@kondara.org>
- (1.1.5-6k)
- Uuummmm....

* Tue Sep  4 2001 Shingo Akagaki <dora@kondara.org>
- (1.1.5-4k)
- revuild against for gnomemm-1.2.1

* Tue May  1 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.1.5

* Wed Nov 22 2000 Kenichi Matsubara <m@kondara.org>
- BuildPreReq add gnomemm-devel.

* Thu Oct  5 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.1.4

* Tue Aug 29 2000 tom <tom@kondara.org>
- remove xcdrdao

* Sat Apr 29 2000 tom <tom@kondara.org>
- change requires gtk-- to gtkmm10

* Sat Feb 2 2000 Norihito Ohmori <nono@kondara.org>
- fix Requires: (jitterbug #345)


* Sat Jan 29 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)
