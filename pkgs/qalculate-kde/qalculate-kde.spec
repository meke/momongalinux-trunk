%global momorel 5
%global		qtver 3.3.7
%global		kdever 3.5.10
%global		qtdir %{_libdir}/qt3
%global		kdedir /usr
%global		enable_final 0
%global		enable_gcc_check_and_hidden_visibility 0

Summary:	A multi-purpose desktop calculator for GNU/Linux
Name:		qalculate-kde
Version:	0.9.7
Release:	%{momorel}m%{?dist}
License:	GPLv2+
Group:		Applications/Engineering
URL:		http://qalculate.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/qalculate/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:		qalculate_kde-desktop.patch
Patch1:		%{name}-%{version}-autoconf265.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	gnuplot
BuildRequires:	libqalculate-devel >= %{version}
BuildRequires:	kdelibs3-devel
BuildRequires:	gettext
BuildRequires:	automake
BuildRequires:	autoconf
BuildRequires:	intltool
BuildRequires:	libtool
BuildRequires:	pkgconfig

%description
Qalculate! is a modern multi-purpose desktop calculator for GNU/Linux. It is
small and simple to use but with much power and versatility underneath.
Features include customizable functions, units, arbitrary precision, plotting.

This package provides a QT/KDE graphical interface for Qalculate! 

%prep
%setup -q

%patch0 -p0 -b .desktop
%patch1 -p1 -b .autoconf265

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

%configure \
	--disable-rpath \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3

make %{?_smp_mflags}
										
%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'

%find_lang qalculate_kde || touch qalculate_kde.lang
HTML_DIR=$(kde-config --expandvars --install html)
if [ -d %{buildroot}$HTML_DIR ]; then
	for lang_dir in %{buildroot}$HTML_DIR/* ; do
		lang=$(basename $lang_dir)
		echo "%lang($lang) %doc $HTML_DIR/$lang/*" >> qalculate_kde.lang
	done
fi

rm -rf %{buildroot}/%{_bindir}/qalculate

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files -f qalculate_kde.lang
%defattr(-, root, root,-)
%doc AUTHORS ChangeLog COPYING
%{_bindir}/qalculate-kde
%{_datadir}/applications/kde/qalculate_kde.desktop
%{_datadir}/apps/qalculate_kde/
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/icons/hicolor/*/actions/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-3m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.7-2m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf
- use new automake

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-6m)
- rebuild against cln-1.3.0

* Sat May 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.6-5m)
- fix build with new automake and libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-4m)
- rebuild against rpm-4.6

* Tue Aug  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-3m)
- enable to build with -O2 option on x86_64

* Wed Jul  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.6-2m)
- do not move qalculate_kde.desktop
- qalculate_kde is a KDE application

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-1m)
- import from Fedora devel

* Wed Feb 27 2008 Deji Akingunola <dakingun@gmail.com> - 0.9.6-5
- Rebuild for cln-1.2

* Sun Feb 10 2008 Deji Akingunola <dakingun@gmail.com> - 0.9.6-4
- Rebuild for gcc43

* Thu Jan 10 2008 Deji Akingunola <dakingun@gmail.com> - 0.9.6-3
- Now BR kdelibs3 instead of kdelibs

* Sat Aug 25 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.6-2
- Rebuild

* Fri Aug 03 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.6-2
- License tag update

* Sun Jul 01 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.6-1
- Update to new release

* Sat Jun 09 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.5-2
- Modify the Name field in the desktop file to distinguish it from that of
  qalculate-gtk (BZ #241024)

* Tue Jan 02 2007 Deji Akingunola <dakingun@gmail.com> - 0.9.5-1
- New release

* Mon Aug 28 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.4-3
- Rebuild for FC6

* Mon Jul 24 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.4-2
- Add another BR on autoconf and automake16

* Wed Jun 27 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.4-1
- New version 0.9.4

* Thu Mar 30 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.3.1-2
- Update the icons location

* Thu Mar 30 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.3.1-1
- Update to newer version

* Mon Feb 13 2006 Deji Akingunola <dakingun@gmail.com> - 0.9.2-2
- Rebuild for Fedora Extras 5

* Tue Dec 27 2005 Deji Akingunola <dakingun@gmail.com> - 0.9.2-1
- Upgrade to new version

* Sat Nov 05 2005 Deji Akingunola <dakingun@gmail.com> - 0.9.0-1
- Upgrade to new version

* Thu Oct 13 2005 Paul Howarth <paul@city-fan.org> - 0.8.2-2
- Rationalise buildreqs and runtime dependencies
- Rationalise desktop entries
- Use standard Fedora Extras idioms for QT env setup & file lists
- Add %%clean section
- Don't generate rpaths
- Don't include empty TODO file

* Tue Oct 11 2005 Deji Akingunola <deji.aking@gmail.com> - 0.8.2-1
- Upgraded to new version
- Install the desktop file
- Miscellaneous spec file cleanup

* Wed Oct 05 2005 Deji Akingunola <deji.aking@gmail.com> - 0.8.1.1
- Initial package
