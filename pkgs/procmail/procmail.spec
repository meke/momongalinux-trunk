%global momorel 14

Summary: The procmail mail processing program
Name: procmail
Version: 3.22
Release: %{momorel}m%{?dist}
License: GPLv2+ or Artistic
Group: System Environment/Daemons
Source: http://www.procmail.org/procmail-%{version}.tar.gz
URL: http://www.procmail.org/
Patch0: procmail-3.22-config.patch
Patch1: procmail-3.22-glibc210.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The procmail program is used by Red Hat Linux for all local mail
delivery.  In addition to just delivering mail, procmail can be used
for automatic filtering, presorting and other mail handling jobs.
Procmail is also the basis for the SmartList mailing list processor.

%prep
%setup -q
%patch0 -p1 -b .config
%patch1 -p1 -b .glibc210
find . -type d -exec chmod 755 {} \;

%build
make RPM_OPT_FLAGS="%{optflags}" LOCKINGTEST=010

%install
rm -rf %{buildroot}

make BASENAME=%{buildroot}/%{_prefix} MANDIR=%{buildroot}/%{_mandir} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Artistic COPYING FAQ HISTORY README KNOWN_BUGS FEATURES examples

%{_bindir}/formail
%attr(2755,root,mail)	%{_bindir}/lockfile
%{_bindir}/mailstat
%attr(6755,root,mail)	%{_bindir}/procmail
%{_mandir}/man[15]/*

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.22-14m)
- add source
- http://www.procmail.org/procmail-3.22-tar.gz is 403 Forbidden...

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.22-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.22-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.22-11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.22-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.22-9m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.22-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.22-7m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+ or Artistic

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.22-6m)
- rebuild against gcc43

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.22-5m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.22-4m)
- add '/' at the end of URLOA

* Thu Oct 18 2001 Motonobu Ichimura <famao@kondara.org>
- (3.22-3k)
- up to 3.22

* Fri Aug 24 2001 Motonobu Ichimura <famao@kondara.org>
- up to 3.21
- it also fixed security hole (http://www.secutiryfocus.com/bid/3071)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (3.14-2).
- can't use maildir patch for procmail-3.14.

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Fri Jan 14 2000 Jeff Johnson <jbj@redhat.com>
- update to 3.14.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Sep 19 1999 Toru Hoshina <toru@gte.net>
- applied maildir.patch.

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- fix doc perms.

* Wed Apr 21 1999 Cristian Gafton <gafton@redhat.com>
- turn on GROUP_PER_USER
- add some docs to the package

* Mon Apr 05 1999 Cristian Gafton <gafton@redhat.com>
- version 3.13

* Fri Mar 26 1999 Cristian Gafton <gafton@redhat.com>
- fixed Group line

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 16)

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc
