%global momorel 20

Summary: Meta package for prepare build env used by buildtools (ex. OmoiKondara)
Name: build-momonga
Version: 1
Release: %{momorel}m%{?dist}
License: Public Domain
Group: Applications/System
URL: http://www.momonga-linux.org/docs/OmoiKondara-HOWTO/ja/index.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# this packages name from http://www.momonga-linux.org/docs/OmoiKondara-HOWTO/ja/requires.html

Requires: automake autoconf
Requires: binutils
Requires: bzip2
Requires: gcc gcc-c++
Requires: glibc-devel
Requires: gtk2-devel
Requires: jpackage-utils
Requires: libtool
Requires: libXtst-devel
Requires: libicu-devel
Requires: liburiparser-devel
Requires: make
Requires: momonga-rpmmacros
Requires: openssh
Requires: patch
Requires: popt-devel
Requires: procps
Requires: ruby ruby-rpm
Requires: ruby18 ruby18-rpm
Requires: rpm-build rpm-devel rpm-python
Requires: subversion
Requires: subversion-ruby
Requires: createrepo yum
Requires: aria2 curl lftp ncftp wget 
Requires: xmlto
Requires: xz

%description
This package provides only work OmoiKondara enviroment.
For resolve packages build dependency, 
you *must* do "yum install -y '*'"
before execute OmoiKondara.

Usage exapmle.
1. Install Momonga Linux
2. yum install -y build-momonga (and yum install -y '*')
3. cd $TOPDIR
4. svn co http://svn.momonga-linux.org/svn/pkgs/trunk/tools/
5. cd tools; make (or cp v2/rpmvercmp .); cd - 
6. svn co http://svn.momonga-linux.org/svn/pkgs/trunk/pkgs/
7. cd pkgs
8. cp ../tools/example.OmoiKondara .OmoiKondara # and edit .OmoiKondara config file
9. ../tools/OmoiKondara <options> <packages>

%files

%changelog
* Fri Mar 16 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-20m)
- add binutils for ld
- add curl for catch up example.OmoiKondara
- add bzip2 and xz for COMPRESS_CMD in example.OmoiKondara

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-19m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-18m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1-17m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1-16m)
- change Requires for new ruby

* Mon Aug  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1-15m)
- remove excessive reqs
-- gcc-objc gcc-objc++ gcc-gfortran gcc-java
-- gcc3.2 gcc3.2-c++
-- gcc3.4 gcc3.4-c++

* Sun Jun 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1-14m)
- add Requires: createrepo yum

* Tue Mar 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-13m)
- add Requires: subversion-ruby for OmoiKondara use svn/client.rb

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1-11m)
- add rpm-python

* Wed Feb 11 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-10m)
- add gtk2-devel xmlto libXtst-devel libicu-devel liburiparser-devel procps for build kernel
- add make
- sort Requires

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1-9m)
- rebuild against rpm-4.6

* Wed Nov 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1-8m)
- add Requires: jpackage-utils
- add BuildArch: noarch

* Sat Jul 26 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1-7m)
- rename ruby19-rpm

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-6m)
- rebuild against gcc43

* Fri Mar 21 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-5m)
- add Requires: popt-devel
- add Requires: aria2

* Tue Feb  5 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-4m)
- add Requires: ruby19 and Requires: ruby-rpm19

* Fri Jul  6 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-3m)
- add gcc* and ncftp and rpm-build

* Mon May 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-2m)
- add ruby-rpm , momonga-rpmmacros

* Mon May 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1-1m)
- first made for momonga
