%global momorel 5
%global qtver 4.7.1
%global kdever 4.5.95
%global kdelibsrel 1m
%global kdemultimediarel 1m

Summary: Excellent CD/DVD-Burner for KDE4
Name: k3b
Version: 2.0.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.k3b.org/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: %{name}rc
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-ffmpeg.patch
Patch2: %{name}-%{version}-libavformat54.patch
Patch3: %{name}-%{version}-libav9.patch
Patch4: %{name}-%{version}-ffmpeg2.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(post): shared-mime-info
Requires(postun): desktop-file-utils
Requires(postun): shared-mime-info
Requires: %{name}-libs = %{version}-%{release}
Requires: kdemultimedia-libs >= %{kdever}-%{kdemultimediarel}
Requires: audiofile
Requires: cdparanoia
Requires: cdrdao >= 1.1
Requires: dbus-qt >= 0.70
Requires: dvd+rw-tools >= 7.0
Requires: ffmpeg >= 0.6.1-0.20110514
Requires: flac >= 1.1.4
Requires: genisoimage
Requires: id3lib
Requires: lame
Requires: libmad
Requires: libmpcdec
Requires: libmusicbrainz
Requires: libogg
Requires: libsndfile
Requires: libvorbis
Requires: normalize
Requires: oxygen-icons
Requires: taglib
Requires: transcode
Requires: vcdimager
Requires: wodim
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdemultimedia-devel >= %{kdever}-%{kdemultimediarel}
BuildRequires: alsa-lib-devel >= 1.0.13-2m
BuildRequires: audiofile-devel
BuildRequires: cdparanoia-devel
BuildRequires: cmake
BuildRequires: dbus-devel >= 0.92
BuildRequires: dbus-qt-devel >= 0.70
BuildRequires: desktop-file-utils
BuildRequires: flac-devel >= 1.1.4
BuildRequires: ffmpeg-devel >= 0.4.9-0.20071219
BuildRequires: gettext
BuildRequires: id3lib-devel >= 3.8.3-5m
BuildRequires: lame-devel >= 3.97-2m
BuildRequires: libdvdread-devel
BuildRequires: libmad >= 0.15.1b-0.1.2m
BuildRequires: libmpcdec-devel
BuildRequires: libmusicbrainz-devel
BuildRequires: libogg-devel >= 1.1.3-2m
BuildRequires: libsndfile-devel >= 1.0.17
BuildRequires: libvorbis-devel >= 1.1.2-2m
BuildRequires: phonon-devel
BuildRequires: taglib-devel
BuildRequires: zlib-devel

%description
K3b provides a comfortable user interface to perform most CD/DVD
burning tasks. While the experienced user can take influence in all
steps of the burning process the beginner may find comfort in the
automatic settings and the reasonable k3b defaults which allow a quick
start.

%package libs
Summary: Shared runtime libraries of k3b
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of k3b.

%package devel
Summary: K3b - files for developing
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on k3b.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja
%patch1 -p1 
%patch2 -p1
%patch3 -p1
%patch4 -p1

%build
# workaround for ffmpeg
export CFLAGS=`pkg-config libavcodec --cflags`
export CXXFLAGS=`pkg-config libavcodec --cflags`

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	-DK3B_BUILD_K3BSETUP:BOOL=OFF \
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# install k3brc (set theme 73lab)
mkdir -p %{buildroot}%{_kde4_configdir}
install -m 644 %{SOURCE1} %{buildroot}%{_kde4_configdir}/

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# clean up
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/%{name}setup.mo

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database &> /dev/null ||:
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
%{_bindir}/update-desktop-database &> /dev/null ||:
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc COPYING* ChangeLog FAQ INSTALL PERMISSIONS
%doc README RELEASE_HOWTO # TODO
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/*%{name}*.so
%{_kde4_libdir}/kde4/kio_videodvd.so
%{_kde4_datadir}/applications/kde4/*.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/konqsidebartng/virtual_folders/services/videodvd.desktop
%{_kde4_appsdir}/solid/actions/%{name}_*.desktop
%{_kde4_configdir}/%{name}rc
## reserve
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/scalable/apps/%{name}.svgz
%{_kde4_datadir}/kde4/services/ServiceMenus/%{name}_*.desktop
%{_kde4_datadir}/kde4/services/*%{name}*.desktop
%{_kde4_datadir}/kde4/services/videodvd.protocol
%{_kde4_datadir}/kde4/servicetypes/%{name}plugin.desktop
%{_datadir}/locale/*/LC_MESSAGES/*%{name}*.mo
%{_datadir}/locale/*/LC_MESSAGES/kio_videodvd.mo
%{_datadir}/mime/packages/x-%{name}.xml
%{_datadir}/pixmaps/%{name}.png

%files libs
%defattr(-, root, root)
%{_kde4_libdir}/lib%{name}*.so.*

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/%{name}*.h
%{_kde4_libdir}/lib%{name}*.so

%changelog
* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-5m)
- rebuild against ffmpeg

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.2-4m)
- remove BR hal

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.2-3m)
- rebuild for ffmpeg-0.6.1-0.20110514

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-2m)
- rebuild for new GCC 4.6

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- version 2.0.2

* Sat Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-6m)
- fix crash on startup time
- http://websvn.kde.org/?revision=1173575&view=revision

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-5m)
- rebuild for new GCC 4.5

* Mon Oct 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-4m)
- fix k3b setting crash (patch1)
-- but Fedora says this is fixed in KDE 4.5.2...

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-2m)
- full rebuild for mo7 release

* Thu Aug 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-1m)
- [BUG FIXES] version 2.0.1
- update desktop.patch
- update k3brc

* Sun Jul 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-2m)
- split package libs

* Wed Jun 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1m)
- version 2.0.0
- update k3brc
- clean up spec file

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.93.0-0.1.2m)
- rebuild against qt-4.6.3-1m

* Sun Jun 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.93.0-0.1.1m)
- update to version 1.93.0 rc4
- update k3brc

* Fri Jun 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.92.0-0.1.2m)
- update desktop.patch

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.92.0-0.1.1m)
- update to version 1.92.0 rc3
- update desktop.patch
- update k3brc
- remove merged qt47.patch

* Sun May  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.91.0-0.1.4m)
- fix build with new qt

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.91.0-0.1.3m)
- clean up k3bsetup.mo
- back from the living to trunk

* Sun Mar 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.91.0-0.1.2m)
- fix up Requires

* Sun Mar 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.91.0-0.1.1m)
- update to version 1.91.0 rc2
- add a package devel
- add a switch withlangpack
- update desktop.patch
- update k3brc
- modify %%post and %%postun
- add Requires(post) and Requires(postun)
- fix up BRs
- remove icons and Requires: oxygen-icons
- remove AUTHORS, KNOWNBUGS and src/IDEAS from %%doc

* Sun Dec 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.69.0-0.1.2m)
- update momonga-language-pack

* Fri Nov 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.69.0-0.1.1m)
- update to version 1.69.0 alpha4
- update momonga-language-pack
- update desktop.patch
- update k3brc

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.68.0-0.1.1m)
- update to version 1.68.0 alpha3
- update momonga-language-pack
- update desktop.patch
- update k3brc

* Thu May 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.66.0-0.1.1m)
- update to version 1.66.0 alpha2
- update desktop.patch
- update k3brc

* Thu Apr 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.65.0-0.1.1m)
- update to version 1.65.0 alpha1
- update desktop.patch

* Sun Feb 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-7m)
- remove k3bsetup (mobug#00216)
- move headers to %%{_includedir}/kde

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-6m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-5m)
- update Patch1 for fuzz=0
- License: GPLv2+ and GFDL

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.5-4m)
- rebuild against ffmpeg and x264

* Wed Oct 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-0.20081022.1m)
- update to 20081022 svn snapshot
- update desktop.patch

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-3m)
- rebuild against libmpcdec-1.2.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-2m)
- rebuild against libdvdread-4.1.2

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-1m)
- version 1.0.5
- update k3brc
- update desktop.patch

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-6m)
- revise %%{_docdir}/HTML/*/k3b/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-5m)
- rebuild against qt3

* Thu Apr 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-0.20080629.1m)
- update to 20080629 svn snapshot

 Thu Apr 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-4m)
- back to version 1.0.4 for the moment
- import kde#151816.patch from Fedora
 +* Sat Dec 08 2007 Rex Dieter <rdieter[AT]fedoraproject.org> - 0:1.0.4-5
 +- patch for "k3b can't reload media for verification" (kde#151816)
- http://bugs.kde.org/151816

* Thu Apr 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-0.20080424.1m)
- update to 20080424 svn snapshot

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.95-0.20080319.3m)
- rebuild against gcc43

* Sat Mar 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.95-0.20080319.2m)
- add workaround for ffmpeg

* Wed Mar 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-0.20080319.1m)
- update to 20080319 svn snapshot

* Mon Mar 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-0.20080310.1m)
- update to 20080310 svn snapshot

* Sun Feb 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-0.20080217.1m)
- update to 20080217 svn snapshot

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-3m)
- %%NoSource -> NoSource

* Wed Jan 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.95-0.20080123.1m)
- update to 20080123 svn snapshot for KDE4
- remove desktop.patch

* Wed Dec 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- rebuild against ffmpeg-0.4.9-0.20071219

* Sat Nov  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-1m)
- version 1.0.4
- update k3brc

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-2m)
- rebuild against libvorbis-1.2.0-1m

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-1m)
- version 1.0.3
- remove merged fix_utf8_filenames.patch
- update k3brc

* Sun Jul 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-1m)
- version 1.0.2
- import k3b-1.0.2-fix_utf8_filenames.patch from cooker
- http://qa.mandriva.com/show_bug.cgi?id=31246
 +* Wed Jul 11 2007 Gustavo Pichorim Boiko <boiko@mandriva.com> 1.0.2-3mdv2008.0
 ++ Revision: 51427
 +- Fix handling of filenames in utf8 (#31246)
- update k3brc

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- good-bye cdrtools and welcome cdrkit

* Thu Apr 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- version 1.0.1
- update k3brc

* Thu Apr  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-4m)
- Requires: normalize

* Thu Apr 05 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-3m)
- rebuild against ffmpeg

* Fri Mar 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- revise k3bsetup2.desktop
- use desktop-file-utils again

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- version 1.0

* Fri Mar 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99-0.20070313.2m)
- add fix-theme-loading.patch
- add kde#142027.patch
- import new pics from svn.kde.org
- update k3brc

* Tue Mar 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99-0.20070313.1m)
- update to 20070313 SVN snapshot (1.0rc7)
- I can not save theme settings... :(
- But brand-new theme "quant" is excellent ;)

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99-0.20070210.3m)
- rebuild against flac-1.1.4

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99-0.20070210.2m)
- rebuild against kdelibs etc.

* Sat Feb 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99-0.20070210.1m)
- update to 20070210 SVN snapshot (1.0rc6)

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99-0.20070127.1m)
- update to 20070127 SVN snapshot (1.0rc5)

* Wed Jan 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99-0.20070110.1m)
- update to 20070110 SVN snapshot (1.0rc4)

* Tue Jan  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99-0.20070102.1m)
- update to 20070102 SVN snapshot (1.0rc3)
- update desktop.patch

* Wed Nov 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.93-0.20061122.1m)
- update to 20061122 SVN snapshot (1.0beta1)
- update desktop.patch

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.17-6m)
- update desktop.patch
- good-bye desktop-file-utils

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.17-5m)
- BuildPreReq: hal-devel >= 0.5.7.1 -> 0.5.7

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.17-4m)
- rebuild against dbus-qt-0.70

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.17-3m)
- rebuild against dbus-qt-0.63-2m

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.17-2m)
- rebuild against dbus 0.92 dbus-qt 0.63

* Wed Aug 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.17-1m)
- update to 0.12.17

* Fri Jun 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.16-1m)
- update to 0.12.16
- update desktop.patch
- remove scsi.patch

* Fri Jun 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.15-6m)
- add k3b-0.12.15-scsi.patch from SVN, thanks to trueg-san

* Sat Jun 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.15-5m)
- fix symlinks

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.12.15-4m)
- rebuild against ffmpeg-cvs20060517

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.12.15-3m)
- rebuild against dbus-0.61

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.12.15-2m)
- rebuild against libmusicbrainz-2.1.2

* Wed Apr 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.15-1m)
- update to 0.12.15

* Wed Mar  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.14-1m)
- update to 0.12.14

* Fri Mar  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.13-1m)
- update to 0.12.13

* Wed Feb 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.12-1m)
- update to 0.12.12
- update desktop.patch
- remove make-ja.patch and ja.po (i18n tar-ball is including ja)

* Sat Feb 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.11-1m)
- update to 0.12.11
- update k3b-ja.po
- import k3bsetup-ja.po and libk3b-ja.po
  from branches/stable/l10n/ja/messages/extragear-multimedia

* Fri Jan 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.10-4m)
- add --enable-new-ldflags to configure

* Fri Jan 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.10-3m)
- modify %%prep section for make-ja.patch

* Fri Dec 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.10-2m)
- rebuild against dbus-0.60

* Sat Dec 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.10-1m)
- update to 0.12.10
- update k3b-ja.po

* Mon Dec 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.9-1m)
- update to 0.12.9
- update k3b-ja.po

* Fri Nov 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.8-1m)
- update to 0.12.8
- update k3b-ja.po

* Tue Nov 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.7-3m)
- rebuild with musicbrainz (enable MusicBrainz support)

* Mon Nov 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.7-2m)
- rebuild against KDE 3.5 RC1

* Wed Nov  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.7-1m)
- update to 0.12.7
- update k3b-ja.po
- update k3b-i18n-make-ja.patch
- enable HAL support
- BuildPreReq: hal-devel >= 0.5.4, dbus-qt, dbus-devel
- Requires: hal, dbus-qt

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.6-1m)
- update to 0.12.6
- update k3b-ja.po

* Wed Oct 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.5-1m)
- update to 0.12.5
- update k3b-ja.po
- add k3b-0.12.5-desktopfile.patch
- remove k3b-0.12-kde34.patch
- remove k3b-0.12.4a-uic.patch

* Sat Sep 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.4a-3m)
- fix uic build problem

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.4a-2m)
- add --disable-rpath to configure

* Sat Sep 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.4a-1m)
- update to 0.12.4a (compile fix)
- remove fix-compile.patch

* Sat Sep 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.4-1m)
- update to 0.12.4
- update k3b-ja.po
- update k3b-i18n-make-ja.patch
- add fix-compile.patch

* Sat Jul 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.3-1m)
- update to 0.12.3
- import k3b-ja.po from SVN (kde.org)
- remove nokdelibsuff.patch

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.2-2m)
- rebuild against flac-1.1.2

* Sat Jul  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.2-1m)
- update to 0.12.2

* Sat Jun 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.1-1m)
- update to 0.12.1
- add KNOWNBUGS and PERMISSIONS to %%doc

* Mon Jun 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12-1m)
- version 0.12
- update kde3x.patch
- remove desktopfile.patch
- BuildPreReq: taglib-devel, libsndfile-devel, fam-devel
- BuildPreReq: lame-devel, ffmpeg-devel

* Fri May 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.24-2m)
- replace Source0: k3b-0.11.24.tar.bz2 (fix documentation issue)

* Thu May 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.24-1m)
- update to 0.11.24 (bugfix release)
- add k3b-i18n-0.11-nokdelibsuff.patch

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (0.11.23-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Sat Mar 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.23-1m)
- update to 0.11.23

* Wed Mar 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.22-1m)
- update to 0.11.22

* Tue Mar 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.21-1m)
- update to 0.11.21
- remove k3b-0.11.20-fix-detect-device.patch
- remove k3b-0.11.20-fix-sound-file-type.patch

* Tue Feb 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.20-4m)
- import k3b-0.11.20-fix-sound-file-type.patch from cooker
 +* Tue Feb 15 2005 Laurent MONTEL <lmontel@mandrakesoft.com> 0.11.20-3mdk
 +- Add patch8: fix sound file filter (bug found by Wilman)
- import k3b-0.11.20-fix-detect-device.patch from cooker
 +* Mon Feb 14 2005 Laurent MONTEL <lmontel@mandrakesoft.com> 0.11.20-2mdk
 +- Add patch7: fix detect device on buggy firmwares
- update Source2: k3brc
  change default theme to 73lab

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.11.20-3m)
- enable x86_64.

* Thu Feb 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.20-2m)
- Requires: lame, transcode
- clean up spec file

* Fri Feb  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.20-1m)
- update to 0.11.20
- remove k3b-0.11.14-version.patch

* Thu Jan 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.19-1m)
- update to 0.11.19

* Sun Dec 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.18-1m)
- update to 0.11.18
- import k3b-0.11.14-version.patch from Fedora Core
 +* Tue Oct 05 2004 Harald Hoyer <harald@redhat.com> 0:0.11.14-2
 +- fixed version string parsing, which fixes bug 134642

* Sat Nov 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.17-4m)
- rebuild against flac 1.1.1
- remove XFree86 from Requires
- BuildPreReq: XFree86-devel -> xorg-x11-devel

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.11.17-3m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Mon Sep 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.17-2m)
- rebuild against KDE 3.3.0

* Thu Sep 23 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.17-1m)
- update to 0.11.17

* Fri Sep 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.16-1m)
- update to 0.11.16
- update desktopfile.patch
- remove Keywords from k3bsetup2.desktop
- BuildPreReq: alsa-lib-devel

* Wed Aug 11 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.14-1m)
- update to 0.11.14

* Tue Aug 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.13-2m)
- BuildPreReq: desktop-file-utils

* Wed Aug 04 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.13-1m)
- update to 0.11.13

* Fri Jul 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.12-4m)
- import k3b-0.11.3-kde32.patch from FC2
- import k3b-0.11.6-desktopfile.patch from FC2
- import k3brc from FC2
- add %%{_datadir}/pixmaps/k3b.png for GNOME
- fix category of desktop files

* Thu Jul 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.12-3m)
- move desktop files from %%{_datadir}/applnk to %%{_datadir}/applications/kde

* Sat Jun 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.12-2m)
- modify %%setup

* Sat Jun 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.12-1m)
- version 0.11.12
- modify %%install and %%clean sections
- %%global kdever 3.2.3

* Thu Jun 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.11-1m)
- version 0.11.11

* Wed May 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.10-1m)
- version 0.11.10
- Requires: cdparanoia, libart_lgpl
- BuildPreReq: cdparanoia-devel, libart_lgpl-devel

* Wed May 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.9-2m)
- rebuild against id3lib 3.8.3

* Tue Mar 30 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.9-1m)
- version 0.11.9

* Wed Mar 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.7-1m)
- version 0.11.7

* Wed Mar 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.6-2m)
- change i18n package

* Thu Mar  4 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.6-1m)
- version 0.11.6
- add i18n package again

* Tue Feb 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.5-2m)
- add dvd+rw-tools to Requires:

* Mon Feb 23 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.5-1m)
- version 0.11.5

* Thu Feb 19 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.4-1m)
- version 0.11.4

* Mon Feb 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.3-1m)
- version 0.11.3

* Thu Jan 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11.1-1m)
- version 0.11.1

* Sat Jan 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11-2m)
- add %%post and %%postun sections

* Sat Jan 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.11-1m)
- version 0.11
- Requires: flac
- BuildPreReq: flac-devel
- temporarily remove i18n package

* Sat Jan 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.3-2m)
- s/%%define/%%global/g

* Sat Dec  6 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.3-1m)
- version 0.10.3

* Sun Nov 16 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.2-1m)
- version 0.10.2

* Thu Sep 25 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-4m)
- clean up spec file

* Thu Sep 18 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-3m)
- add %%doc

* Thu Sep 18 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-2m)
- revise %%files section

* Wed Sep 17 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-1m)
- import from Suzuka

* Thu Aug  5 2003 Noriyuki Suzuki <noriyuki@turbolinux.co.jp>
- added --disable-warnings flag.
- edit menu entry.

* Thu Jul 31 2003 Toshihiro Yamagishi<toshihiro@turbolinux.co.jp>
- fixed head command option

* Tue Jul 29 2003 Noriyuki Suzuki <noriyuki@turbolinux.co.jp>
- version 0.9 with i18n package.

* Fri Jun 27 2003 Noriyuki Suzuki <noriyuki@turbolinux.co.jp>
- version 0.9pre2
