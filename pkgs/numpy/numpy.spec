%global momorel 2

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
# eval to 2.3 if python isn't yet present, workaround for no python in fc4 minimal buildroot
%{!?python_version: %define python_version %(%{__python} -c 'import sys; print sys.version.split(" ")[0]' || echo "2.3")}

Name:           numpy
Version:        1.6.0
Release:        %{momorel}m%{?dist}
Summary:        A fast multidimensional array facility for Python

Group:          Development/Languages
License:        Modified BSD
URL:            http://numeric.scipy.org/
Source0:        http://dl.sourceforge.net/sourceforge/numpy/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         numpy-1.0.1-f2py.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7 lapack-devel python-setuptools gcc-gfortran atlas python-nose >= 0.10.0
Requires:	python-nose

%description
NumPy is a general-purpose array-processing package designed to
efficiently manipulate large multi-dimensional arrays of arbitrary
records without sacrificing too much speed for small multi-dimensional
arrays.  NumPy is built on the Numeric code base and adds features
introduced by numarray as well as an extended C-API and the ability to
create arrays of arbitrary type.

There are also basic facilities for discrete fourier transform,
basic linear algebra and random number generation. Also included in
this package is a version of f2py that works properly with NumPy.

%package f2py
Summary:        f2py for numpy
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       python-devel
Provides:       f2py
Obsoletes:      f2py <= 2.45.241_1927

%description f2py
This package includes a version of f2py that works properly with NumPy.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .f2py

%build
env ATLAS=%{_libdir} FFTW=%{_libdir} BLAS=%{_libdir} \
    LAPACK=%{_libdir} CFLAGS="$RPM_OPT_FLAGS" \
    %{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
#%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
# skip-build currently broken, this works around it for now
env ATLAS=%{_libdir} FFTW=%{_libdir} BLAS=%{_libdir} \
    LAPACK=%{_libdir} CFLAGS="$RPM_OPT_FLAGS" \
    %{__python} setup.py install --root $RPM_BUILD_ROOT
rm -rf docs-f2py ; mv $RPM_BUILD_ROOT%{python_sitearch}/%{name}/f2py/docs docs-f2py
mv -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/f2py/f2py.1 f2py.1
rm -rf doc ; mv -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/doc .
install -D -p -m 0644 f2py.1 $RPM_BUILD_ROOT%{_mandir}/man1/f2py.1
pushd $RPM_BUILD_ROOT%{_bindir} &> /dev/null
# symlink for anyone who was using f2py.numpy
ln -s f2py f2py.numpy
popd &> /dev/null

# Remove doc files. They should in in %doc
rm -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/COMPATIBILITY
rm -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/DEV_README.txt
rm -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/INSTALL.txt
rm -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/LICENSE.txt
rm -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/README.txt
rm -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/THANKS.txt
rm -f $RPM_BUILD_ROOT%{python_sitearch}/%{name}/site.cfg.example

%check
pushd doc &> /dev/null
PYTHONPATH="%{buildroot}%{python_sitearch}" %{__python} -c "import pkg_resources, numpy ; numpy.test()"
popd &> /dev/null

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc docs-f2py doc/* LICENSE.txt README.txt THANKS.txt DEV_README.txt COMPATIBILITY site.cfg.example
%dir %{python_sitearch}/%{name}
%{python_sitearch}/%{name}/*.py*
%{python_sitearch}/%{name}/core
%{python_sitearch}/%{name}/compat
%{python_sitearch}/%{name}/fft
%{python_sitearch}/%{name}/lib
%{python_sitearch}/%{name}/linalg
%{python_sitearch}/%{name}/ma
%{python_sitearch}/%{name}/matrixlib
%{python_sitearch}/%{name}/numarray
%{python_sitearch}/%{name}/oldnumeric
%{python_sitearch}/%{name}/polynomial
%{python_sitearch}/%{name}/random
%{python_sitearch}/%{name}/testing
%{python_sitearch}/%{name}/tests
%{python_sitearch}/%{name}-*.egg-info

%files f2py
%defattr(-,root,root,-)
%{_mandir}/man*/*
%{_bindir}/f2py
%{_bindir}/f2py.numpy
%{python_sitearch}/%{name}/distutils
%{python_sitearch}/%{name}/f2py

%changelog
* Wed Dec 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-2m)
- rebuild for lapack-3.4.0

* Wed Oct  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update 1.6.0

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update 1.5.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0b1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0b1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0b1-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0b1-1m)
- update to 1.5.0b1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-1m)
- sync with Fedora 11 (1.3.0-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.0-2m)
- rebuild agaisst python-2.6.1-1m

* Sat Jul 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-1m)
- rename package (python-numpy to numpy)
- re-import from Fedora for pytz

* Thu May 29 2008 Jarod Wilson <jwilson@redhat.com> 1.1.0-1
- New upstream release

* Tue May 06 2008 Jarod Wilson <jwilson@redhat.com> 1.0.4-1
- New upstream release

* Sat Apr 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-2m)
- add egg-info to %%files (to fix build with new python)

* Sat Apr 26 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.4-1m)
- import to Momonga

* Mon Feb 11 2008 Jarod Wilson <jwilson@redhat.com> 1.0.3.1-2
- Add python egg to %%files on f9+

* Wed Aug 22 2007 Jarod Wilson <jwilson@redhat.com> 1.0.3.1-1
- New upstream release

* Wed Jun 06 2007 Jarod Wilson <jwilson@redhat.com> 1.0.3-1
- New upstream release

* Mon May 14 2007 Jarod Wilson <jwilson@redhat.com> 1.0.2-2
- Drop BR: atlas-devel, since it just provides binary-compat
  blas and lapack libs. Atlas can still be optionally used
  at runtime. (Note: this is all per the atlas maintainer).

* Mon May 14 2007 Jarod Wilson <jwilson@redhat.com> 1.0.2-1
- New upstream release

* Tue Apr 17 2007 Jarod Wilson <jwilson@redhat.com> 1.0.1-4
- Update gfortran patch to recognize latest gfortran f95 support 
- Resolves rhbz#236444

* Fri Feb 23 2007 Jarod Wilson <jwilson@redhat.com> 1.0.1-3
- Fix up cpuinfo bug (#229753). Upstream bug/change:
  http://projects.scipy.org/scipy/scipy/ticket/349

* Thu Jan 04 2007 Jarod Wilson <jwilson@redhat.com> 1.0.1-2
- Per discussion w/Jose Matos, Obsolete/Provide f2py, as the
  stand-alone one is no longer supported/maintained upstream

* Wed Dec 13 2006 Jarod Wilson <jwilson@redhat.com> 1.0.1-1
- New upstream release

* Tue Dec 12 2006 Jarod Wilson <jwilson@redhat.com> 1.0-2
- Rebuild for python 2.5

* Wed Oct 25 2006 Jarod Wilson <jwilson@redhat.com> 1.0-1
- New upstream release

* Tue Sep 06 2006 Jarod Wilson <jwilson@redhat.com> 0.9.8-1
- New upstream release

* Wed Apr 26 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.9.6-1
- Upstream update

* Thu Feb 16 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.9.5-1
- Upstream update

* Mon Feb 13 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.9.4-2
- Rebuild for Fedora Extras 5

* Thu Feb  2 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.9.4-1
- Initial RPM release
- Added gfortran patch from Neal Becker
