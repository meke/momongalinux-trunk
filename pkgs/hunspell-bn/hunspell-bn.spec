%global momorel 5

%define upstreamid 20080201
%define upstreamver 0.02

Name: hunspell-bn
Summary: Bengali hunspell dictionaries
Version: %{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://www.ankur.org.bd/downloads/spell_check/hunspell/%{name}-BD-%{upstreamver}.tar.bz2
Group: Applications/Text
URL: http://ankur.org.bd/wiki/Documentation#OpenOffice.org
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Bengali hunspell dictionaries.

%prep
%setup -q -n %{name}-BD-%{upstreamver}
find -type f | xargs chmod -x
iconv -f ISO-8859-1 -t UTF-8 Copyright > Copyright.utf8
mv Copyright.utf8 Copyright

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
bn_BD_aliases="bn_IN"
for lang in $bn_BD_aliases; do
        ln -s bn_BD.aff $lang.aff
        ln -s bn_BD.dic $lang.dic
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README COPYING Copyright
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080201-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080201-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20080201-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080201-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080201-1m)
- update to 20080201

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20050726-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20050726-1m)
- import from Fedora to Momonga

* Sun Jan 06 2008 Parag <pnemade@redhat.com> - 20050726-2
- Added Copyright and corrected changelog version.

* Thu Jan 03 2008 Parag <pnemade@redhat.com> - 20050726-1
- Initial Fedora release
