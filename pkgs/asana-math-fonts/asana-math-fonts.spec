%global momorel 6

%define fontname asana-math
%define fontconf 63-%{fontname}.conf

Name:           %{fontname}-fonts
Version:        0.914
Release:        %{momorel}m%{?dist}
Summary:        An OpenType font with a MATH table

Group:          User Interface/X
License:        OFL
URL:            http://openfontlibrary.org/media/files/asyropoulos/219
Source0:        http://openfontlibrary.org/people/asyropoulos/asyropoulos_-_Asana_Math.otf
Source1:        %{name}-fontconfig.conf
Source2:        README.license
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
An OpenType font with a MATH table that can be used with XeTeX to typeset math
content


%prep
cp %{SOURCE2} $RPM_BUILD_DIR


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{SOURCE0} %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.otf

%doc README.license

%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.914-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.914-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.914-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.914-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.914-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.914-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.914-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Feb 20 2009 Jon Stanley <jonstanley@gmail.com> - 0.914-5
- Fix various build failures

* Fri Feb 20 2009 Jon Stanley <jonstanley@gmail.com> - 0.914-4
- Fix *.ttf to *.otf

* Fri Feb 20 2009 Jon Stanley <jonstanley@gmail.com> - 0.914-3
- Remove comments from spec

* Fri Feb 20 2009 Jon Stanley <jonstanley@gmail.com> - 0.914-2
- Update to new packaging guidelines

* Sun Jul 13 2008 Jon Stanley <jonstanley@gmail.com> - 0.914-1
- Change version
- Include license readme
- Fix fontconfig file

* Sat Jul 12 2008 Jon Stanley <jonstanley@gmail.com> - 20080120-1
- Initial package
