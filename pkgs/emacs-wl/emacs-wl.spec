%global momorel 5
%global pkg wl
%global pkgname Wanderlust

%global apelver 10.8-6m
%global flimver 1.14.9-27m
%global semiver 1.14.6-41m
%global srcname wanderlust-201107200116
%global cvs_date 20110720

Summary: Yet Another Message Interface on Emacsen
Name: emacs-%{pkg}
Version: 2.15.9
Release: 0.0.%{cvs_date}.%{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source0: ftp://ftp.jpl.org/pub/elisp/wl/snapshots/%{srcname}.tar.gz
Source1: http://www.h6.dion.ne.jp/~nytheta/software/pub/prom-wl-2.7.0.tar.gz
Patch0: wl-2.8.x-CFG.patch
Patch1: wl-2.12.x-ELS.patch
Patch2: wl-set-icondir.patch
URL: http://www.gohome.org/wl/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
BuildRequires: emacs-apel >= %{apelver}
BuildRequires: emacs-flim >= %{flimver}
BuildRequires: emacs-semi >= %{semiver}
BuildRequires: emacs-w3m >= 1.4.371-4m
Requires: emacs >= %{_emacs_version}
Requires: emacs-apel >= %{apelver}
Requires: emacs-flim >= %{flimver}
Requires: emacs-semi >= %{semiver}
Requires: emacs-w3m >= 1.4.371-4m
Obsoletes: wl-emacs wl-beta-emacs
Obsoletes: wl-xemacs wl-beta-xemacs
Obsoletes: elisp-wl
Provides: elisp-wl
Requires(post): info
Requires(preun): info

%description
Wanderlust is a mail/news management system with IMAP4rev1 support for
Emacs.

%package el
Summary:	Elisp source files for %{pkgname} under GNU Emacs
Group:		Applications/Text
Requires:	%{name} = %{version}-%{release}

%description el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.

%prep
%setup -q -n %{srcname}
%patch0 -p1 -b .cfg
%patch1 -p1 -b .els
%patch2 -p1 -b .icondir

tar xzf %{SOURCE1}
mv prom-wl-2.7.0/prom-wl.el utils/

%build
make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{_emacs_sitelispdir} \
  INFODIR=%{buildroot}%{_infodir} \
  PIXMAPDIR=%{buildroot}%{_emacs_sitelispdir}/%{pkg}/icons
LANG=ja_JP.UTF-8 make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{_emacs_sitelispdir} \
  INFODIR=%{buildroot}%{_infodir} \
  info

cat > %{pkg}-init.el <<"EOF"
(autoload 'wl "wl" "Wanderlust" t)
(autoload 'wl-other-frame "wl" "Wanderlust on new frame." t)
(autoload 'wl-draft "wl-draft" "Write draft with Wanderlust." t)
EOF

%install
rm -rf %{buildroot}

make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{_emacs_sitelispdir} \
  INFODIR=%{buildroot}%{_infodir} \
  PIXMAPDIR=%{buildroot}%{_emacs_sitelispdir}/%{pkg}/icons \
  install
mkdir -p %{buildroot}%{_infodir}
install -m 644 doc/wl-ja.info %{buildroot}%{_infodir}
install -m 644 doc/wl.info %{buildroot}%{_infodir}

%{__cp} doc/TODO .
%{__cp} doc/TODO.ja .
%{__cp} wl/ChangeLog ChangeLog.wl
%{__cp} elmo/ChangeLog ChangeLog.elmo

%{__mkdir_p} %{buildroot}%{_datadir}/config-sample/%{name}
%{__install} -m 644 samples/en/dot.addresses %{buildroot}%{_datadir}/config-sample/%{name}
%{__install} -m 644 samples/en/dot.folders %{buildroot}%{_datadir}/config-sample/%{name}
%{__install} -m 644 samples/en/dot.wl %{buildroot}%{_datadir}/config-sample/%{name}

%__mkdir_p %{buildroot}%{_emacs_sitestartdir}
install -m 644 %{pkg}-init.el %{buildroot}%{_emacs_sitestartdir}/%{pkg}-init.el

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/wl-ja.info %{_infodir}/dir --section="Emacs"
/sbin/install-info %{_infodir}/wl.info %{_infodir}/dir --section="Emacs"

%preun
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/wl-ja.info %{_infodir}/dir \
  --entry="* Wanderlust: (wl-ja).         Yet Another Message Interface On Emacsen" --section="Emacs"
  /sbin/install-info --delete %{_infodir}/wl.info %{_infodir}/dir \
  --entry="* Wanderlust: (wl).            Yet Another Message Interface On Emacsen" --section="Emacs"
fi

%files
%defattr(-,root,root,-)
%doc BUGS COPYING ChangeLog INSTALL INSTALL.ja NEWS NEWS.ja README README.ja
%doc doc/TODO doc/TODO.ja
%doc ChangeLog.wl ChangeLog.elmo
%doc prom-wl-2.7.0
%{_datadir}/config-sample/%{name}
%dir %{_emacs_sitelispdir}/%{pkg}
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitelispdir}/%{pkg}/icons
%{_emacs_sitelispdir}/%{pkg}/wl-news.el
%{_emacs_sitestartdir}/*.el
%{_infodir}/*.info*

%files -n %{name}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el
%exclude %{_emacs_sitelispdir}/%{pkg}/wl-news.el

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15.9-0.0.20110720.5m)
- add source

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.9-0.0.20110720.4m)
- rebuild for emacs-24.1

* Wed Dec 21 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.9-0.0.20110720.3m)
- remove wl-news.elc

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.9-0.0.20110720.2m)
- add wl-news.elc

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.9-0.0.20110720.1m)
- update to 20110720 snapshot
- add -el package
- add wl-init.el

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.9-0.0.20100804.7m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.15.9-0.0.20100804.6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.9-0.0.20100804.5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.9-0.0.20100804.4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.9-0.0.20100804.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.15.9-0.0.20100804.2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.9-0.0.20100804.1m)
- update to cvs snapshot

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.9-0.0.20100303.4m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.9-0.0.20100303.3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.9-0.0.20100303.2m)
- merge wl-emacs to elisp-wl
- kill wl-xemacs

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.9-0.0.20100303.1m)
- update to cvs snapshot

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.9-0.0.20100216.2m)
- LANG=ja_JP.UTF-8 when make info

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.9-0.0.20100216.1m)
- update to cvs snapshot

* Sat Jan 30 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.9-0.0.20100125.1m)
- update to cvs snapshot

* Mon Jan 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.8-0.0.20100106.1m)
- update to cvs snapshot

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.7-0.0.20090820.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.7-0.0.20090820.1m)
- update to cvs snapshot (2009-08-20)

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.7-0.0.20090528.4m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.7-0.0.20090528.3m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.7-0.0.20090528.2m)
- rebuild against emacs-23.0.95

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.7-0.0.20090528.1m)
- update to cvs snapshot

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20090322.4m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20090322.3m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20090322.2m)
- rebuild against emacs-23.0.93

* Mon Apr 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.15.6-0.20090322.1m)
- update to 20090322 cvs snapshot

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20090101.3m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20090101.2m)
- rebuild against rpm-4.6

* Tue Jan  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20090101.1m)
- update to cvs snapshot
- License: GPLv2+

* Sun Sep 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20080905.1m)
- update to cvs snapshot

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20080706.2m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.15.6-0.20080706.1m)
- update to 20080706 cvs snapshot

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.6-0.20080213.5m)
- rebuild against apelver 10.7-11m
- rebuild against flimver 1.14.9-5m
- rebuild against semiver 1.14.6-14m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20080213.4m)
- rebuild against xemacs-21.5.28
- add Patch3: WL-MK-xemacs.patch

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20080213.3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.6-0.20080213.2m)
- rebuild against gcc43

* Thu Feb 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.6-0.20080213.1m)
- update to cvs snapshot

* Mon Dec 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-0.20071215.1m)
- update to cvs snapshot

* Thu Nov 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-0.20071110.1m)
- update to cvs snapshot
- License: GPLv2

* Wed Aug 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-0.20070805.1m)
- update to cvs snapshot

* Mon Jul  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-0.20070620.3m)
- no %%NoSource
- install config-sample files

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-0.20070620.2m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Wed Jun 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.15.5-0.20070620.1m)
- update to 20070620 cvs snapshot
- this is a very important bug fix release
- 2.15.5-0.20070424.1m can not open archive folders

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-0.20070424.1m)
- update to cvs snapshot

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.5-0.20061105.9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.5-0.20061105.8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.5-0.20061105.7m)
- rebuild against emacs-22.0.95

* Sun Feb 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.5-0.20061105.6m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-0.20061105.5m)
- rebuild against apel-10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.5-0.20061105.4m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.5-0.20061105.3m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.5-0.20061105.2m)
- rebuild against emacs-22.0.91

* Sun Nov  5 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.5-0.20061105.1m)
- update to cvs snapshot

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.4-0.20061002.2m)
- rebuild against emacs-22.0.90

* Wed Oct 11 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.4-0.20061002.1m)
- update to cvs snapshot

* Fri Sep  8 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.3-0.20060906.1m)
- update to cvs snapshot
- no %%NoSource 0

* Fri Aug 18 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.3-0.20060812.1m)
- update to cvs snapshot
  add Patch2: wl-set-icondir.patch

* Thu Apr 14 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.14.0-1m)
- version up

* Mon Feb 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.12.2-1m)
- bugfixes

* Sat Feb 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.0-6m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12.0-5m)
- use %%{sitepdir}

* Sun Feb 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.12.0-4m)
- rebuild aginst w3m-emacs-1.4.3-5m

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.0-3m)
- rebuild against emacs 22.0.50

* Tue Feb  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.12.0-2m)
- change etcdir, don't use version specific directory

* Tue Jan 25 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.12.0-1m)
- major version up

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.10.1-4m)
- rebuild against emacs-21.3.50

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.10.1-3m)
- change Source/Patch URI

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.10.1-2m)
- revised spec for enabling rpm 4.2.

* Sat Jul 12 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.10.1-1m)

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.0-3m)
- rebuild against emacs-21.3
- define emikover
- update emiko 1.14.1-23m
- use %%{_prefix} %%{_datadir} %%{_libdir} macro

* Sun Jan 12 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (2.10.0-2m)
- Rebuild against elisp-apel-10.4-1m, elisp-limit-1.14.7-21m
   and elisp-emiko-1.14.1-22m

* Thu Jan  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.10.0-1m)

* Sat Aug 17 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (2.8.1-14m)
- remove kondara logo
- rebuild against elisp-apel-10.3-21m and elisp-limit-1.14.7-19m

* Thu Jul 18 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.1-13m)
- change Source1 uri

* Tue Jul 09 2002 Kenta MURATA <muraken2@nifty.com>
- (2.8.1-12m)
- typo...

* Tue Jul 09 2002 Kenta MURATA <muraken2@nifty.com>
- (2.8.1-11m)
- move icon directory.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.8.1-10k)
- /sbin/install-info -> info in PreReq.

* Tue Mar 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.8.1-8k)
- rebuild against emacs-21.2

* Tue Mar 12 2002 Hidetomo Machi <mcHT@kondara.org>
- (2.8.1-6k)
- Add wl-2.8.1_wl-draft_insert-x-face.diff (backport wl-ML:[09614])
- Revise Source

* Wed Feb 20 2002 Toshiro Hikita <toshi@sodan.org>
- (2.8.1-4k)
- add elmo-imap4.diff for fix clear problem

* Sat Dec 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.8.1-2k)
- update version 2.8.1
- sync with latest cvs (doc)

* Tue Dec 11 2001 Kenta MURATA <muraken2@nifty.com>
- (2.7.6-6k)
- replace splash image.

* Sat Nov 17 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.7.6-4k)
- update cvs date
- "Merry Christmas!" ;)

* Wed Nov 14 2001 Toshiro Hikita <toshi@sodan.org>
- (2.7.6-2k)
- update cvs date

* Wed Nov  7 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.7.5-12k)
- rebuild against emiko-1.14.1-18k

* Wed Nov  7 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.7.5-10k)
- update cvs date
- BuildPreReq: w3m-emacs, w3m-xemacs

* Fri Nov  2 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.7.5-8k)
- pixmapdir is under %{emacsver}/etc

* Fri Nov  2 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.7.5-6k)
- update cvs date
- update some required version

* Fri Oct 26 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.7.5-4k)
- update cvs date
- never specify 'NoSource: 0' because snapshot tarball will be removed
  soon.

* Thu Oct 25 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.7.5-2k)
- merge wl-emacs and wl-xemacs
- adopt beta version ;p

* Mon Oct 22 2001 Toshiro Hikita <toshi@sodan.org>
- (2.6.0-6k)
- add icondir patch for Emacs-21 Support

* Tue Sep 11 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.6.0-4k)
- sync with latest CVS (wl-2_6)

* Wed Jul  4 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.6.0-2k)
- version 2.6.0 (stable release)

* Wed Jun  6 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.5.8-2k)
- version 2.5.8 (cvs Main Trunk)

* Mon May 14 2001 Hidetomo Machi <mcHT@kondara.org>
- (2.4.1-2k)
- version 2.4.1
- include prom-wl
- sync with latest CVS (wl-20010412-cvs.patch)

* Sun Dec  3 2000 KIM Hyeong Cheol <kim@kondara.org>
- (2.4.0-3k)
- merged from incoming

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.91-9k)
- modified spec file about treating info

* Thu Nov 30 2000 Toshiro HIKITA <toshi@sodan.org>
- (2.4.0-1k)
- update to 2.4.0

* Wed Nov 22 2000 Toshiro HIKITA <toshi@sodan.org>
- (2.4.0pre3-1k)
- update to 2.4.0pre3

* Thu Nov  9 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- change BuildPreReq & Requires TAG

* Wed Nov 08 2000 Toshiro HIKITA <toshi@sodan.org>
- (2.4.0pre-1k)
- update to 2.4.0pre
- fixed to use normal tar.gz

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Oct 23 2000 Hidetomo Machi <mcHT@kondara.org>
- (2.3.91-3k)
- change release number
- modify specfiel (License)

* Thu Oct 19 2000 Toshiro HIKITA <toshi@sodan.org>
- (2.3.91-1.200010181525k)
- merged with wl-beta-emacs.
- update to current cvs branch.
- New variable %time
- remove cvs.patch
- add Obsoletes: wl-beta-emacs

* Mon Oct 02 2000 Toshiro HIKITA <toshi@sodan.org>
- (1.1.1-7k)
- Updated cvs.patch.

* Sat Aug 19 2000 Toshiro HIKITA <toshi@sodan.org>
- Updated cvs.patch.

* Mon Jul 10 2000 AYUHANA Tomonori <l@kondara.org>
- (1.1.1-2k)
- %patch0 fix -p1 -> -p0
- %patch1 add -b

* Sat Jul  1 2000 SAKA Toshihide <saka@yugen.org>
- correct the URI of the source

* Sat Jun 10 2000 Hidetomo Machi <mcHT@kondara.org>
- modifed "BuildRoot"
- add "-q" at %setup

* Fri May 18 2000 Toshiro Hikita <toshi@sodan.org>
- Added English Info

* Thu May 11 2000 Toshiro Hikita <toshi@sodan.org>
- version 1.1.1.
- modified %doc

* Fri Mar 10 2000 Hidetomo Machi <mcHT@kondara.org>
- version 1.1.0pre3 with some patch
- modified %post
- add %dir

* Sun Mar  5 2000 Toshiro Hikita <toshi@sodan.org>
- Adapted to 1.1.0pre1.

* Wed Jan 12 2000 Toru Hoshina <t@kondara.org>
- Version up.

* Fri Jan  7 2000 Toru Hoshina <t@kondara.org>
- Version up.

* Sun Jan  2 2000 SAKA Toshihide <saka@yugen.org>
- Adapted to GNU Emacs.
