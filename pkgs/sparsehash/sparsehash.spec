%global	momorel 1
# disable -debuginfo subpackage
%global debug_package %{nil}

Name:           sparsehash
Version:        1.12
Release:        %{momorel}m%{?dist}
Summary:        Extremely memory-efficient C++ hash_map implementation

Group:          Development/Libraries
License:        BSD
URL:            http://code.google.com/p/sparsehash
Source0:        http://sparsehash.googlecode.com/files/sparsehash-%{version}.tar.gz
NoSource:	0

%description
The Google SparseHash project contains several C++ template hash-map
implementations with different performance characteristics, including
an implementation that optimizes for space and one that optimizes for
speed.

# all files are in -devel package
%package        devel
Summary:        Extremely memory-efficient C++ hash_map implementation
Group:          Development/Libraries

%description    devel
The Google SparseHash project contains several C++ template hash-map
implementations with different performance characteristics, including
an implementation that optimizes for space and one that optimizes for
speed.

%prep
%setup -q

%build
%configure
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT

# Remove unneeded files
rm $RPM_BUILD_ROOT%{_defaultdocdir}/sparsehash-%{version}/INSTALL
rm $RPM_BUILD_ROOT%{_defaultdocdir}/sparsehash-%{version}/README_windows.txt

%check
make check

%files devel
%doc %{_defaultdocdir}/sparsehash-%{version}/
%{_includedir}/google/
%{_libdir}/pkgconfig/libsparsehash.pc

%changelog
* Mon Jul  8 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-1m)
- import from fedora
