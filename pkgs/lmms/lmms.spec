%global momorel 1
%global qtver 4.8.0

Summary: Powerful sequencer-, synthesizer- and sample-studio for Linux
Name: lmms
Version: 0.4.13
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://lmms.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.4.7-desktop.patch
Patch1: %{name}-0.4.11-libdir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
BuildRequires: qt-devel >= %{qtver}
BuildRequires: ImageMagick
BuildRequires: SDL-devel
BuildRequires: alsa-lib-devel
BuildRequires: cmake
BuildRequires: fftw-devel
BuildRequires: flac-devel
BuildRequires: fluidsynth-devel >= 1.1.5
BuildRequires: gzip
BuildRequires: jack-devel
BuildRequires: ladspa-devel
BuildRequires: libsndfile-devel
BuildRequires: libsamplerate-devel
BuildRequires: libvorbis-devel
BuildRequires: pulseaudio-libs-devel
%ifarch %{ix86}
BuildRequires: wine-devel >= 1.0-0.994.1m
BuildRequires: wine-utils
%endif
Obsoletes: %{name}-extras

%description
LMMS aims to be a free alternative to popular (but commercial and closed-
source) programs like FruityLoops, Cubase and Logic giving you the ability of
producing music with your computer by creating/synthesizing sounds, arranging
samples, playing live with keyboard and much more...

LMMS combines the features of a tracker-/sequencer-program (pattern-/channel-/
sample-/song-/effect-management) and those of powerful synthesizers and
samplers in a modern, user-friendly and easy to use graphical user-interface.

LMMS is still in heavy development, so with this version please don't expect a
complete, ready and bugfree program!!

%package devel
Summary: Header files from lmms
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Includes files for developing programs based on lmms.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .libdir

%build
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"

PATH="%{_qt4_bindir}:$PATH" %{cmake} .

make %{?_smp_mflags} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64}/apps
convert -scale 16x16 data/themes/default/icon.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 32x32 data/themes/default/icon.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
convert -scale 48x48 data/themes/default/icon.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
install -m 644 data/themes/default/icon.png  %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png

# link icon
rm -f  %{buildroot}%{_datadir}/pixmaps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# unpack man file
pushd %{buildroot}%{_mandir}/man1
gzip -d %{name}.1.gz
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null || :

%postun
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null || :

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README TODO
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_datadir}/menu/%{name}
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/pixmaps/%{name}.png

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}

%changelog
* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.13-1m)
- update to 0.4.13
- rebuild against fluidsynth-1.1.5

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.11-2m)
- rebuild against fluidsynth-1.1.4

* Fri Jul  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.11-1m)
- version 0.4.11
- update libdir.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-4m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.8-3m)
- specify PATH for Qt4

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.8-2m)
- fix build failure

* Wed Sep 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8-1m)
- version 0.4.8

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.7-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.7-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-1m)
- version 0.4.7
- update desktop.patch
- remove linking.patch

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.6-5m)
- rebuild against qt-4.6.3-1m

* Thu May 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-4m)
- add BuildRequires

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.6-3m)
- explicitly link libpthread and libfontconfig

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.6-2m)
- use BuildRequires

* Wed Jan  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-1m)
- version 0.4.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5-1m)
- version 0.4.5

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-2m)
- Obsoletes: lmms-extras

* Tue May  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-1m)
- version 0.4.4
- remove wineg++.patch

* Sun Mar 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-2m)
- follow wineg++ changes

* Tue Feb 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3-1m)
- version 0.4.3

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-2m)
- rebuild against rpm-4.6

* Thu Dec 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-1m)
- version 0.4.2

* Sun Nov  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-2m)
- fix up libdir

* Sun Nov  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- version 0.4.0
- add a package devel
- add desktop.patch
- modify install icons section

* Sat Jun  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-4m)
- rebuild against wine-devel

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.2-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-1m)
- version 0.3.2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-2m)
- %%NoSource -> NoSource

* Wed Nov 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-1m)
- version 0.3.1
- update lmms.desktop
- revise lmms-16.png
- add lmms-64.png
- License: GPLv2

* Thu Aug 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-2m)
- build without wine on i686 for the moment

* Thu Aug 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- version 0.3.0
- build without festival and libstk for the moment

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-3m)
- rebuild against libvorbis-1.2.0-1m

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-2m)
- revise lmms.desktop

* Thu Apr 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-1m)
- initial package for Momonga Linux
- import plugindir.patch from packman lmms-0.2.1-0.pm.1.src.rpm
- import and modify lmms.desktop from packman lmms-0.2.1-0.pm.1.src.rpm
- import icons from cooker
