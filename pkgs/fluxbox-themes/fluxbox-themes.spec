%global momorel 14

Summary: themes for Fluxbox
Name: fluxbox-themes
Version: 1.0
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: fbfixstyle
BuildRequires: ruby >= 1.9
BuildArch: noarch
Requires: fluxbox >= 0.9.6

# Themes
Source100: BabyMushroom.tar.gz
Source101: BeOS.tar.gz
Source102: Black.tar.gz
Source103: Clouds.tar.gz
Source104: Cyclosys.tar.gz
Source105: GoldOrb.tar.gz
Source106: Ingeborg_Sorensen.tar.gz
Source107: Jumpman.tar.gz
Source108: Linux_PX.tar.gz
Source109: LuluLake.tar.gz
Source110: Mannsomikkerakkbuss.tar.gz
Source111: Mayhem_in_Monsterland.tar.gz
Source112: MindGame.tar.gz
Source113: Novellenet-2.tar.gz
Source114: PurpleSteel.tar.gz
Source115: Ques_Girl.tar.gz
Source116: Terra.tar.gz
Source117: TwO.tar.gz
Source118: UnderDarkGreen.tar.gz
Source119: aquagreen.tar.gz
Source120: bomb_fractal.tar.gz
Source121: crap.tar.gz
Source122: foggy_morning.tar.gz
Source123: lemmons.tar.gz
Source124: reactor-keygen.tar.gz
Source125: wombat.tar.gz
Source126: aleczapka_combination.tar.bz2
Source127: aleczapka_digitalshe.tar.bz2
Source128: aleczapka_fluxcarnation.tar.bz2
Source129: aleczapka_minime.tar.bz2
Source130: aleczapka_posthuman.tar.bz2
Source131: XmaN_TuxSun.tar.bz2
Source132: aki_kharisma.tar.bz2
Source133: eddiex_flatED.tar.bz2
Source134: eddiex_rumpa.tar.bz2
Source135: fab_smaller-is-better.tar.bz2
Source136: jar_blues.tar.bz2
Source137: ma3j_goldengreen.tar.bz2
Source138: olav_violetvalentine.tar.bz2
Source139: pekdon_openbsd.tar.bz2
Source140: pekdon_posthuman.tar.bz2
Source141: rj_daft.tar.bz2
Source142: sibbe_snowmas.tar.bz2
Source143: vlaad_vlaadworld.tar.bz2
Source144: xfs_simplesmoothgray.tar.bz2
Source145: momonga.tar.gz
Source146: seer-o_influx.tar.bz2
Source147: flatBlack.tar.bz2
Source148: irex-fluxbox.tar.bz2
%description
themes for Fluxbox.

%prep
rm -rf   %{buildroot}

%install
%__mkdir_p %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE100 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE101 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE102 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE103 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE104 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE105 -C %{buildroot}%{_datadir}/fluxbox-themes
# tar xzf %SOURCE106 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE107 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE108 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE109 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE110 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE111 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE112 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE113 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE114 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE115 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE116 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE117 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE118 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE119 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE120 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE121 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE122 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE123 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE124 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE125 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE126 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE127 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE128 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE129 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE130 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE131 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE132 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE133 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE134 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE135 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE136 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE137 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE138 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE139 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE140 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE141 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE142 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE143 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE144 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE145 -C %{buildroot}%{_datadir}/fluxbox-themes
tar xzf %SOURCE146 -C %{buildroot}%{_datadir}/fluxbox-themes

pushd %{buildroot}%{_datadir}/fluxbox-themes
mv fluxbox/styles/* styles/
mv fluxbox/backgrounds/* backgrounds/
rm -rf fluxbox gtk
rm style/README
mv style/* styles/
rmdir style
for i in styles/*
do
  ruby %SOURCE0 $i
done
tar xzf %SOURCE147 -C .
tar xzf %SOURCE148 -C ./styles/

mv Backgrounds/* backgrounds
rmdir Backgrounds
chmod -x styles/*
chmod -x backgrounds/*
rm *.pcf.gz
rm *.lsm
popd

### to be read by all users
chmod -R a+r %{buildroot}%{_datadir}/fluxbox-themes/styles/
chmod -R a+r %{buildroot}%{_datadir}/fluxbox-themes/backgrounds/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_datadir}/fluxbox-themes

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-12m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-11m)
- revise SOURCE0 for ruby19

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-10m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-7m)
- rebuild against gcc43

* Mon Jan  5 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0-6m)
- modify flatBlack theme for fluxbox-0.9.7.

* Thu Jan  1 2004 Junichiro Kita <kita@kitaj.no-ip.com>
- (1.0-5m)
- use fbsetbg

* Mon Aug 18 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-4m)
- update flatBlack theme.(flatBlackXft for AntiAlias)

* Wed Sep 25 2002 kourin <kourin@fh.freeserve.ne.jp>
- (1.0-3m)
- add new theme irex-fluxbox by takeshi
  ( see http://takeshi.tdiary.net/20020923.html#p03 )

* Tue Sep 17 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-2m)
- add new theme flatBlack (Oh! kuro guro.)

* Mon Sep 16 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (1.0-1m)
- separated from fluxbox
