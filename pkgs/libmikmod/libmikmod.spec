%global momorel 11
%global betaver 2

Summary: A MOD music file player library
Name: libmikmod
Version: 3.2.0
Release: 0.%{betaver}.%{momorel}m%{?dist}
License: GPLv2 and LGPLv2+
Group: Applications/Multimedia
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: esound-devel
URL: http://mikmod.raphnet.net/
Source0: http://mikmod.raphnet.net/files/libmikmod-%{version}-beta%{betaver}.tar.bz2
NoSource: 0
Patch0:  libmikmod-64bit.patch
Patch1:  libmikmod-esd.patch
Patch2:  libmikmod-strip-lib.patch
Patch3:  libmikmod-multilib.patch
Patch4:  libmikmod-autoconf.patch
Patch5:  libmikmod-info.patch

Patch10: libmikmod-3.1.11-CVE-2007-6720.patch
Patch11: libmikmod-3.1.11-CVE-2009-0179.patch
Patch12: libmikmod-CVE-2010-3995,3996.patch
Patch13: libmikmod-3.1.11-CVE-2010-2546.patch

%description
libmikmod is a library used by the mikmod MOD music file player for
UNIX-like systems. Supported file formats include MOD, STM, S3M, MTM,
XM, ULT and IT.

%package devel
Group: Development/Libraries
Summary: Header files and documentation for compiling mikmod applications
Requires: %{name} = %{version}-%{release}
Provides: mikmod-devel = 3.2.2-4
Obsoletes: mikmod-devel < 3.2.2-4

%description devel
This package includes the header files you will need to compile
applications for mikmod.

%prep
%setup -q -n %{name}-%{version}-beta2
%patch0 -p1 -b .64bit
%patch1 -p1 -b .esd
%patch2 -p1 -b .strip-lib
%patch3 -p1 -b .multilib
%patch4 -p1 -b .autoconf
%patch5 -p1 -b .info

%patch10 -p1 -b .CVE-2007-6720
%patch11 -p1 -b .CVE-2009-0179
%patch12 -p1 -b .CVE-2010-3995,3996
%patch13 -p1 -b .CVE-2010-2546

%build
%configure
make %{?_smp_flags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
rm -f $RPM_BUILD_ROOT%{_infodir}/dir $RPM_BUILD_ROOT%{_libdir}/*.a
find $RPM_BUILD_ROOT | grep "\\.la$" | xargs rm -f

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%post devel
[ -x /sbin/install-info ] && /sbin/install-info %{_infodir}/mikmod.info %{_infodir}/dir || :

%postun -p /sbin/ldconfig

%postun devel
if [ $1 = 0 ] ; then
	[ -x /sbin/install-info ] && /sbin/install-info  --delete %{_infodir}/mikmod.info %{_infodir}/dir || :
fi

%files
%defattr(-, root, root)
%doc AUTHORS COPYING.LIB COPYING.LESSER NEWS README TODO
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%{_bindir}/*-config
%{_libdir}/*.so
%{_datadir}/aclocal/*
%{_includedir}/*
%{_infodir}/mikmod*
%{_mandir}/man1/libmikmod-config*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-0.2.11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-0.2.10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-0.2.9m)
- full rebuild for mo7 release

* Tue Aug 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.2.3m)
- [SECURITY] CVE-2010-2546
- import a security patch from Debian stable (3.1.11-6.0.1+lenny1)

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.2.7m)
- [SECURITY] CVE-2010-3995 CVE-2010-3996
- import security patch from Fedora

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-0.2.6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.2.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.2.4m)
- [SECURITY] CVE-2007-6720 CVE-2009-0179
- import security patches (Patch10,11) from Debian unstable (3.1.11-6.1)

* Sun May 24 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2.0-0.2.3m)
- add __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.2.2m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-0.2.1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.11-2m)
- rebuild against gcc43

* Mon Feb 18 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 3.2.0-3.beta2
- Fix MikMod_InfoLoader() and MikMod_InfoDriver() functions, fixing mikmod -n
  output

* Mon Feb 18 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 3.2.0-2.beta2
- Replace completely braindead (hint to author, drink coffee first, then code)
  esd non blocking patch with one that actually works. This fixes using mikmod
  with pulseaudio (bz 247865)
- Note: this makes the 2 supported output devices oss and esd (and pulseaudio's
  esd emulation) alsa is not supported, this requires a rewrite of the mikmod
  alsa code which was written for alsa-0.5 and never updated for the new alsa
  0.9/1.0 api

* Fri Feb 15 2008 Jindrich Novy <jnovy@redhat.com> 3.2.0-1
- update to libmikmod-3.2.0-beta2
- fix playback on 64bit arches

* Thu Feb 14 2008 Jindrich Novy <jnovy@redhat.com> 3.1.11-5
- fix rpath patch so that there are no undefined symbols in
  libmikmod.so (#431745)

* Thu Oct 25 2007 Jindrich Novy <jnovy@redhat.com> 3.1.11-4
- virtually provide mikmod-devel

* Wed Oct 24 2007 Jindrich Novy <jnovy@redhat.com> 3.1.11-3
- add multilib patch

* Tue Oct 23 2007 Jindrich Novy <jnovy@redhat.com> 3.1.11-2
- update description
- add smp_flags to make
- don't ship static library
- update upstream patch, drop texinfo dependency (thanks to Stepan Kasal)

* Wed Oct 18 2007 Jindrich Novy <jnovy@redhat.com> 3.1.11-1
- package libmikmod

* Mon May 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.1.11-1m)
- update to 3.1.11

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.10-11m)
- delete libtool library

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.1.10-10m)
- enable ia64, ppc64

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.10-9m)
- suppress AC_DEFUN warning.

* Mon Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.1.10-8m)
- libmikmod-3.1.10-64bit.patch from fc3.

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.1.10-7m)
- enable x86_64.
  use %{_libdir}.
  use %{_bindir}/libtool for x86_64.

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (3.1.10-6m)
- revised spec for enabling rpm 4.2.

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.10-5m)
- putosource into repository

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.1.10-4k)

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.1.10-2k)
- update to 3.1.10
  update source URL

* Mon Dec 04 2000 Kenichi Matsubara <m@kondara.org>
- [3.1.9-7k]
- bugfix %post %postun.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Feb 15 2000 Yuichiro Orino <yuu_@pop21.odn.ne.jp>
- update to 3.1.9

* Fri Dec 18 1999 Yuichiro Orino <yuu_@pop21.odn.ne.jp>
- devide from mikmod package

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Aug  2 1999 Bill Nottingham <notting@redhat.com>
- add more patches

* Fri Jul 30 1999 Bill Nottingham <notting@redhat.com>
- update to 3.1.6/3.1.7

* Mon Mar 22 1999 Cristian Gafton <gafton@redhat.com>
- fixed spec file description and group according to sepcspo

* Mon Mar 22 1999 Michael Maher <mike@redhat.com>
- changed spec file, updated package
- added libmikmod to the package

* Mon Feb 15 1999 Miodrag Vallat <miodrag@multimania.com>
- Created for 3.1.5

* Tue Jan 24 1999 Michael Maher <mike@redhat.com>
- changed group
- fixed bug #145

* Fri Sep 04 1998 Michael Maher <mike@redhat.com>
- added patch for alpha

* Wed Sep 02 1998 Michael Maher <mike@redhat.com>
- built package; obsoletes the ancient tracker program.
