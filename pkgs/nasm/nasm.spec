%global momorel 1

Summary: The Netwide Assembler, a portable x86 assembler with Intel-like syntax
Name: nasm
Version: 2.11.05
Release: %{momorel}m%{?dist}
License: LGPL
Group: Development/Languages
Source: http://www.nasm.us/pub/nasm/releasebuilds/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRequires: ghostscript >= 7.07-5m
URL: http://www.nasm.us/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%package doc
Summary: Extensive documentation for NASM
Group: Development/Languages
Requires(pre,preun): info

%package rdoff
Summary: Tools for the RDOFF binary format, sometimes used with NASM.
Group: Development/Tools

%description
NASM is the Netwide Assembler, a free portable assembler for the Intel
80x86 microprocessor series, using primarily the traditional Intel
instruction mnemonics and syntax.

%description doc
Extensive documentation for the Netwide Assembler, NASM, in HTML,
info, PostScript and text formats.

%description rdoff
Tools for the operating-system independent RDOFF binary format, which
is sometimes used with the Netwide Assembler (NASM).  These tools
include linker, library manager, loader, and information dump.

%prep
%setup -q

%build
rm -f config.cache config.status config.log
sh ./configure --prefix=%{_prefix}

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p "%{buildroot}"%{_mandir}/man1
mkdir -p "%{buildroot}"%{_infodir}
DOC="%{buildroot}"%{_docdir}/%{name}-%{version}
rm -rf "$DOC"
mkdir -p "$DOC"/rdoff
%makeinstall docdir=%{buildroot}/%{_docdir}/nasm-%{version} install_everything
# mv "%{buildroot}"/usr/man/* "%{buildroot}"%{_mandir}
# mv "%{buildroot}"/usr/info/* "%{buildroot}"%{_infodir}
# gzip -9 "%{buildroot}"%{_infodir}/nasm.*
gzip -9 "$DOC"/*.txt "$DOC"/*.ps
cp CHANGES AUTHORS README doc/*.doc test/changed.asm "$DOC"
cp rdoff/README "$DOC"/rdoff

%clean
rm -rf "%{buildroot}"

%post doc
/sbin/install-info %{_infodir}/nasm.info.* %{_infodir}/dir

%preun doc
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/nasm.info.* %{_infodir}/dir
fi

%files
%attr(-,root,root)	%{_bindir}/nasm
%attr(-,root,root)	%{_bindir}/ndisasm
%attr(-,root,root) %doc %{_mandir}/man1/ldrdf.1*
%attr(-,root,root) %doc %{_mandir}/man1/nasm.1*
%attr(-,root,root) %doc %{_mandir}/man1/ndisasm.1*
%attr(-,root,root) %doc %{_mandir}/man1/rdf2bin.1*
%attr(-,root,root) %doc %{_mandir}/man1/rdf2com.1*
%attr(-,root,root) %doc %{_mandir}/man1/rdf2ihx.1*
%attr(-,root,root) %doc %{_mandir}/man1/rdf2ith.1*
%attr(-,root,root) %doc %{_mandir}/man1/rdf2srec.1*
%attr(-,root,root) %doc %{_mandir}/man1/rdfdump.1*
%attr(-,root,root) %doc %{_mandir}/man1/rdflib.1*
%attr(-,root,root) %doc %{_mandir}/man1/rdx.1*
%attr(-,root,root) %doc %{_docdir}/nasm-%{version}/CHANGES
%attr(-,root,root) %doc %{_docdir}/nasm-%{version}/AUTHORS
%attr(-,root,root) %doc %{_docdir}/nasm-%{version}/README

%files doc
%attr(-,root,root) %doc %{_infodir}/nasm.info*
%attr(-,root,root) %doc %{_docdir}/nasm-%{version}/nasmdoc.*
%attr(-,root,root) %doc %{_docdir}/nasm-%{version}/internal.*
%attr(-,root,root) %doc %{_docdir}/nasm-%{version}/changed.asm
%attr(-,root,root) %doc %{_docdir}/nasm-%{version}/html

%files rdoff
%attr(-,root,root)	%{_bindir}/ldrdf
%attr(-,root,root)	%{_bindir}/rdf2bin
%attr(-,root,root)	%{_bindir}/rdf2com
%attr(-,root,root)	%{_bindir}/rdf2ihx
%attr(-,root,root)	%{_bindir}/rdf2ith
%attr(-,root,root)	%{_bindir}/rdf2srec
%attr(-,root,root)	%{_bindir}/rdfdump
%attr(-,root,root)	%{_bindir}/rdflib
%attr(-,root,root)	%{_bindir}/rdx
%attr(-,root,root) %doc	%{_docdir}/nasm-%{version}/rdoff

%changelog
* Tue Jun 10 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11.05-1m)
- update to 2.11.05

* Sun Jan 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Fri Aug 16 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.09-1m)
- update to 2.10.09

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.07-1m)
- update to 2.10.07

* Sat Sep 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.05-1m)
- update to 2.10.05

* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.04-1m)
- update to 2.10.04

* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.01-1m)
- update to 2.10.01

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.09.10-1m)
- update to 2.09.10

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.09.08-1m)
- update to 2.09.08

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.09.07-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.09.07-1m)
- update to 2.09.07

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.09.04-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09.04-1m)
- update to 2.09.04

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.09.03-1m)
- update to 2.09.03

* Fri Oct 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.09.02-1m)
- update to 2.09.02

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.08.02-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.08.02-1m)
- update to 2.08.02

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.08.01-1m)
- update to 2.08.01

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.07-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.07-1m)
- update to 2.07

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.05.01-2m)
- rebuild against rpm-4.6

* Sun Nov  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.05.01-1m)
- [SECURITY] CVE-2008-7177
- update to 2.05.01

* Thu Jun 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- [SECURITY] CVE-2008-2719
- update to 2.03

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.02-2m)
- rebuild against gcc43

* Sun Mar  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02-1m)
- update 2.02

* Thu Jan 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.01-1m)
- update 2.01

* Fri Dec  7 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.00-1m)
- update 2.00

* Fri Nov  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.05-1m)
- update 0.99.05

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.39-2m)
- change Source URL

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.98.39-1m)
- update to 0.98.39

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (0.98.36-4m)
- revised spec for rpm 4.2.

* Wed Nov 12 2003 zunda <zunda at freeshell.org>
- (0.98.36-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Sep 17 2003 zunda <zunda at freeshell.org>
- (kossori)
- BuildpreReq: ghostscript >= 7.07-5m for proper ps2pdf

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.98.36-2m)
- add BuildpreReq: ghostscript for ps2pdf -dOptimize=true   nasmdoc.ps nasmdoc.pdf

* Mon Mar 17 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.98.36-1m)
  update to 0.98.36
  
* Tue Mar  4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.98.35-1m)
- version up to 0.98.35
- use %%{_bindir}
- fix URL
- change --prefix=%%{_prefix}
- add doc/*.doc

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.98.22-4k)

* Fri Apr  12 2002 Toru Hoshina <t@kondara.org>
- (0.98.22-2k)
- ver up. from sourceforge...

* Tue Sep  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.98-14k)
- import sse2 patch

* Fri Oct 27 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modified spec file

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.98-7k)
- fixed for FHS

* Sun Jul  2 2000 Hidetomo Machi <mcHT@kondara.org>
- modified %files (for auto gzipped man by rpm-3.0.5)
- rebuild for Jirai ;p
- added "-q" at %setup
