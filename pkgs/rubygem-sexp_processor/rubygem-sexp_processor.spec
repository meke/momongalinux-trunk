# Generated from sexp_processor-3.1.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname sexp_processor

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: sexp_processor branches from ParseTree bringing all the generic sexp processing tools with it
Name: rubygem-%{gemname}
Version: 3.1.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: https://github.com/seattlerb/sexp_processor
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
sexp_processor branches from ParseTree bringing all the generic sexp
processing tools with it. Sexp, SexpProcessor, Environment, etc... all
for your language processing pleasure.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

sed -i -e 's|/usr/local/bin|/usr/bin|' %{buildroot}%{geminstdir}/test/*.rb

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.0-1m)
- update 3.1.0

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.10-1m)
- update 3.0.10

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.7-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.7-1m)
- update 3.0.7

* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.6-1m)
- update 3.0.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.5-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.5-1m)
- update 3.0.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.4-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.4-1m)
- update 3.0.4

* Sat Jan 02 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-1m)
- Initial package for Momonga Linux
