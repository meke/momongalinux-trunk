%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

# Prevent RPM from stripping the binaries.
%global __strip /bin/true

Name:           ocaml-ocamlnet
Version:        3.6
Release:        %{momorel}m%{?dist}
Summary:        Network protocols for OCaml

Group:          Development/Libraries
License:        Modified BSD and GPLv2+
URL:            http://projects.camlcity.org/projects/ocamlnet.html
Source0:        http://download.camlcity.org/download/ocamlnet-%{version}.tar.gz
NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-pcre-devel >= 6.2.3-1m
BuildRequires:  ocaml-ssl-devel >= 0.4.3-1m
BuildRequires:  ocaml-lablgtk-devel >= 2.14.2-1m
BuildRequires:  pcre-devel >= 8.31

%global __ocaml_requires_opts -i Asttypes -i Outcometree -i Parsetree

%description
Ocamlnet is an ongoing effort to collect modules, classes and
functions that are useful to implement network protocols. Since
version 2.2, Ocamlnet incorporates the Equeue, RPC, and Netclient
libraries, so it now really a big player.

In detail, the following features are available:

 * netstring is about processing strings that occur in network
   context. Features: MIME encoding/decoding, Date/time parsing,
   Character encoding conversion, HTML parsing and printing, URL
   parsing and printing, OO-representation of channels, and a lot
   more.

 * netcgi1 and netcgi2 focus on portable web applications. netcgi1 is
   the older, backward-compatible version, whereas netcgi2 bases on a
   revised design, and is only partly backward-compatible. Supported
   are CGI, FastCGI, AJP (mod_jk), and SCGI.

 * rpc implements ONCRPC (alias SunRPC), the remote procedure call
   technology behind NFS and other Unix services.

 * netplex is a generic server framework. It can be used to build
   stand-alone server programs from individual components like those
   from netcgi2, nethttpd, and rpc.

 * netclient implements clients for HTTP (version 1.1, of course), FTP
   (currently partially), and Telnet.

 * equeue is an event queue used for many protocol implementations. It
   makes it possible to run several clients and/or servers in parallel
   without having to use multi-threading or multi-processing.

 * shell is about calling external commands like a Unix shell does.

 * netshm provides shared memory for IPC purposes.

 * netsys contains bindings for system functions missing in core O'Caml.

 * smtp and pop are two further client implementations for the SMTP
   and POP3 protocols.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%package        nethttpd
Summary:        Ocamlnet HTTP daemon
License:        GPLv2+
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    nethttpd
Nethttpd is a web server component (HTTP server implementation). It
can be used for web applications without using an extra web server, or
for serving web services.


%package        nethttpd-devel
Summary:        Development files for %{name}-nethttpd
License:        GPLv2+
Group:          Development/Libraries
Requires:       %{name}-nethttpd = %{version}-%{release}


%description    nethttpd-devel
The %{name}-nethttpd-devel package contains libraries and signature
files for developing applications that use %{name}-nethttpd.


%prep
%setup -q -n ocamlnet-%{version}

./configure -enable-tcl \
  -equeue-tcl-libs -ltcl \
  -with-nethttpd \
  -bindir %{_bindir} \
  -datadir %{_datadir}/%{name} \
  -prefer-netcgi2 \
  -with-nethttpd \
  -disable-apache \
  -enable-ssl \
  -enable-gtk2
# In future:
# -with-rpc-auth-dh (requires cryptgps)


%build
# Bletcherous hack to get that extra include path in camlp4 builds.
echo -e '#!/bin/sh\n%{_bindir}/camlp4 -I %{_libdir}/ocaml/camlp4/Camlp4Parsers "$@"' > camlp4; chmod 0755 camlp4; export PATH=`pwd`:$PATH
make all
%if %opt
make opt
%endif

%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR
mkdir -p $OCAMLFIND_DESTDIR/stublibs
make install

# rpc-generator/dummy.mli is empty and according to Gerd Stolpmann can
# be deleted safely.  This avoids an rpmlint warning.
rm -f $RPM_BUILD_ROOT%{_libdir}/ocaml/rpc-generator/dummy.mli

# Strip libraries.
strip $RPM_BUILD_ROOT%{_libdir}/ocaml/stublibs/*.so

# NB. Do NOT strip the binaries and prevent prelink from stripping them too.
# (https://bugzilla.redhat.com/show_bug.cgi?id=435559)
mkdir -p $RPM_BUILD_ROOT/etc/prelink.conf.d
echo -e '-b /usr/bin/netplex-admin\n-b /usr/bin/ocamlrpcgen' \
  > $RPM_BUILD_ROOT/etc/prelink.conf.d/ocaml-ocamlnet.conf


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc ChangeLog RELNOTES LICENSE LICENSE.GPL
%{_libdir}/ocaml/equeue
%{_libdir}/ocaml/equeue-gtk2
%{_libdir}/ocaml/equeue-ssl
%{_libdir}/ocaml/equeue-tcl
%{_libdir}/ocaml/netcamlbox
%{_libdir}/ocaml/netcgi2
%{_libdir}/ocaml/netcgi2-plex
%{_libdir}/ocaml/netclient
%{_libdir}/ocaml/netgssapi
%{_libdir}/ocaml/netmulticore
%{_libdir}/ocaml/netplex
%{_libdir}/ocaml/netshm
%{_libdir}/ocaml/netstring
%{_libdir}/ocaml/netsys
%{_libdir}/ocaml/pop
%{_libdir}/ocaml/rpc
%{_libdir}/ocaml/rpc-auth-local
%{_libdir}/ocaml/rpc-generator
%{_libdir}/ocaml/rpc-ssl
%{_libdir}/ocaml/shell
%{_libdir}/ocaml/smtp
%if %opt
%exclude %{_libdir}/ocaml/*/*.a
%exclude %{_libdir}/ocaml/*/*.cmxa
%exclude %{_libdir}/ocaml/*/*.cmx
%exclude %{_libdir}/ocaml/*/*.o
%endif
%exclude %{_libdir}/ocaml/*/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner
%{_datadir}/%{name}/
%{_bindir}/*
%config(noreplace) /etc/prelink.conf.d/ocaml-ocamlnet.conf


%files devel
%defattr(-,root,root,-)
%doc ChangeLog RELNOTES LICENSE LICENSE.GPL
%if %opt
%{_libdir}/ocaml/equeue/*.a
%{_libdir}/ocaml/equeue/*.cmxa
%{_libdir}/ocaml/equeue-gtk2/*.a
%{_libdir}/ocaml/equeue-gtk2/*.cmxa
%{_libdir}/ocaml/equeue-ssl/*.a
%{_libdir}/ocaml/equeue-ssl/*.cmxa
%{_libdir}/ocaml/equeue-tcl/*.a
%{_libdir}/ocaml/equeue-tcl/*.cmxa
%{_libdir}/ocaml/netcgi2/*.a
%{_libdir}/ocaml/netcgi2/*.cmxa
%{_libdir}/ocaml/netcgi2-plex/*.a
%{_libdir}/ocaml/netcgi2-plex/*.cmxa
%{_libdir}/ocaml/netclient/*.a
%{_libdir}/ocaml/netclient/*.cmxa
%{_libdir}/ocaml/netplex/*.a
%{_libdir}/ocaml/netplex/*.cmxa
%{_libdir}/ocaml/netplex/*.cmx
%{_libdir}/ocaml/netplex/*.o
%{_libdir}/ocaml/netshm/*.a
%{_libdir}/ocaml/netshm/*.cmxa
%{_libdir}/ocaml/netstring/*.a
%{_libdir}/ocaml/netstring/*.cmxa
%{_libdir}/ocaml/netstring/*.cmx
%{_libdir}/ocaml/netstring/*.o
%{_libdir}/ocaml/netsys/*.a
%{_libdir}/ocaml/netsys/*.cmxa
%{_libdir}/ocaml/pop/*.a
%{_libdir}/ocaml/pop/*.cmxa
%{_libdir}/ocaml/rpc/*.a
%{_libdir}/ocaml/rpc/*.cmxa
%{_libdir}/ocaml/rpc-auth-local/*.a
%{_libdir}/ocaml/rpc-auth-local/*.cmxa
%{_libdir}/ocaml/rpc-generator/*.a
%{_libdir}/ocaml/rpc-generator/*.cmxa
%{_libdir}/ocaml/rpc-ssl/*.a
%{_libdir}/ocaml/rpc-ssl/*.cmxa
%{_libdir}/ocaml/shell/*.a
%{_libdir}/ocaml/shell/*.cmxa
%{_libdir}/ocaml/smtp/*.a
%{_libdir}/ocaml/smtp/*.cmxa
%endif
%{_libdir}/ocaml/equeue/*.mli
%{_libdir}/ocaml/equeue-gtk2/*.mli
%{_libdir}/ocaml/equeue-ssl/*.mli
%{_libdir}/ocaml/equeue-tcl/*.mli
%{_libdir}/ocaml/netcgi2/*.mli
%{_libdir}/ocaml/netcgi2-plex/*.mli
%{_libdir}/ocaml/netclient/*.mli
%{_libdir}/ocaml/netplex/*.mli
%{_libdir}/ocaml/netshm/*.mli
%{_libdir}/ocaml/netstring/*.mli
%{_libdir}/ocaml/netsys/*.mli
%{_libdir}/ocaml/pop/*.mli
%{_libdir}/ocaml/rpc/*.mli
%{_libdir}/ocaml/rpc-auth-local/*.mli
%{_libdir}/ocaml/rpc-generator/*.mli
%{_libdir}/ocaml/rpc-ssl/*.mli
%{_libdir}/ocaml/shell/*.mli
%{_libdir}/ocaml/smtp/*.mli


%files nethttpd
%defattr(-,root,root,-)
%doc ChangeLog RELNOTES LICENSE LICENSE.GPL
%{_libdir}/ocaml/nethttpd-for-netcgi2
%{_libdir}/ocaml/nethttpd
%if %opt
%exclude %{_libdir}/ocaml/*/*.a
%exclude %{_libdir}/ocaml/*/*.cmxa
%endif
%exclude %{_libdir}/ocaml/*/*.mli


%files nethttpd-devel
%defattr(-,root,root,-)
%doc ChangeLog RELNOTES LICENSE LICENSE.GPL
## empty


%changelog
* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6-1m)
- update to 3.6
- rebuild against pcre-8.31

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.9-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.9-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.9-11m)
- full rebuild for mo7 release

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-10m)
- rebuild against ocaml-lablgtk-2.14.1

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-9m)
- rebuild against the following pacakges
-- ocaml-3.11.2
-- ocaml-lablgtk-2.14.0
-- ocaml-pcre-6.1.0
-- ocaml-ssl-0.4.2-4m
- build with -equeue-tcl-libs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-7m)
- fix /etc/prelink.conf.d/ocaml-ocamlnet.conf

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-6m)
- rebuild against ocaml-pcre-6.0.0

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-5m)
- rebuild against ocaml-lablgtk-2.12.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-4m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-3m)
- import Fedora devel changes (2.2.9-7)
- rebuild against ocaml-3.11.0

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.9-2m)
- revise %%files to avoid conflicting

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.9-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 2.2.9-6
- Rebuild for OCaml 3.10.2

* Mon Apr 21 2008 Richard W.M. Jones <rjones@redhat.com> - 2.2.9-5
- New upstream URL.

* Mon Mar  3 2008 Richard W.M. Jones <rjones@redhat.com> - 2.2.9-4
- Do not strip binaries (bz 435559).

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 2.2.9-3
- Rebuild for ppc64.

* Tue Feb 12 2008 Richard W.M. Jones <rjones@redhat.com> - 2.2.9-2
- Rebuild for OCaml 3.10.1.

* Wed Nov  7 2007 Richard W.M. Jones <rjones@redhat.com> - 2.2.9-1
- New upstream release 2.2.9.
- A more bletcherous, but more working, patch to fix the camlp4
  missing path bug.  Hopefully this is very temporary.
- Fixes for mock build under F8:
  + BR tcl-devel

* Thu Sep 13 2007 Richard W.M. Jones <rjones@redhat.com> - 2.2.8.1-1
- New upstream version 2.2.8.1.
- License of the base package is in fact BSD.  Clarified also that
  the license of nethttpd is GPLv2+.
- Removed the camlp4 paths patch as it doesn't seem to be necessary.
- Add BRs for camlp4, ocaml-pcre-devel, ocaml-lablgtk-devel,
  openssl-devel
- Removed explicit requires, they're not needed.
- Strip binaries and libraries.
- Ignore Parsetree module in deps.

* Thu Aug  2 2007 Richard W.M. Jones <rjones@redhat.com> - 2.2.7-6
- ExcludeArch ppc64
- BR ocaml-pcre-devel
- Fix for camlp4 missing path

* Mon Jun 11 2007 Richard W.M. Jones <rjones@redhat.com> - 2.2.7-5
- Updated to latest packaging guidelines.

* Tue May 29 2007 Richard W.M. Jones <rjones@redhat.com> - 2.2.7-4
- Added support for ocaml-lablgtk2

* Tue May 29 2007 Richard W.M. Jones <rjones@redhat.com> - 2.2.7-3
- Remove empty file rpc-generator/dummy.mli.

* Sat May 26 2007 Richard W.M. Jones <rjones@redhat.com> - 2.2.7-2
- Added support for SSL.

* Sat May 26 2007 Richard W.M. Jones <rjones@redhat.com> - 2.2.7-1
- Initial RPM.
