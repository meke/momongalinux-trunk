%global momorel 6

%{!?with_sqlite: %global with_sqlite 1}
%{!?with_docs: %global with_docs 1}
%{!?with_crash: %global with_crash 0}
%{!?with_rpm: %global with_rpm 1}
%{!?with_bundled_elfutils: %global with_bundled_elfutils 0}
%{!?elfutils_version: %global elfutils_version 0.142}
%{!?pie_supported: %global pie_supported 1}
%{!?with_grapher: %global with_grapher 1}
%{!?with_boost: %global with_boost 0}
%{!?with_publican: %global with_publican 0}
%{!?publican_brand: %global publican_brand momonga}

Name: systemtap
Version: 1.6
Release: %{momorel}m%{?dist}
Summary: Instrumentation System
Group: Development/System
License: GPLv2+
URL: http://sourceware.org/systemtap/
Source0: http://sourceware.org/systemtap/ftp/releases/systemtap-%{version}.tar.gz
NoSource: 0

Obsoletes: systemtap-client < 1.5

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: kernel >= 2.6.9-11
BuildRequires: gettext
%if %{with_sqlite}
BuildRequires: sqlite-devel
%endif
# Needed for libstd++ < 4.0, without <tr1/memory>
%if %{with_boost}
BuildRequires: boost-devel
%endif
%if %{with_crash}
BuildRequires: crash-devel zlib-devel
%endif
%if %{with_rpm}
BuildRequires: rpm-devel >= 4.8.0 glibc-headers
%endif
# Alternate kernel packages kernel-PAE-devel et al have a virtual
# provide for kernel-devel, so this requirement does the right thing.
Requires: kernel-devel
Requires: gcc make
# Suggest: kernel-debuginfo
Requires: systemtap-runtime = %{version}-%{release}
BuildRequires: nss-devel avahi-devel pkgconfig
BuildRequires: gettext-devel

# Additional requires for things spawned by stap
Requires: coreutils grep sed unzip zip
Requires: openssh-clients

%if %{with_bundled_elfutils}
Source1: elfutils-%{elfutils_version}.tar.gz
Patch1: elfutils-portability.patch
BuildRequires: m4
%global setup_elfutils -a1
%else
BuildRequires: elfutils-devel >= %{elfutils_version}
%endif

%if %{with_docs}
BuildRequires: tetex-latex tetex-dvips ghostscript latex2html
BuildRequires: xmlto xmlto-tex
%if %{with_publican}
BuildRequires: publican >= 1.3
BuildRequires: publican-%{publican_brand} >= 1.2-2m
%endif
%endif

%if %{with_grapher}
BuildRequires: gtkmm-devel >= 2.8
BuildRequires: libglademm-devel >= 2.6.7
# If 'with_boost' isn't set, the boost-devel build requirement hasn't
# been specified yet.
%if ! %{with_boost}
BuildRequires: boost-devel
%endif
%endif
BuildRequires: ImageMagick-perl

Provides: %{_bindir}/stap

%description
SystemTap is an instrumentation system for systems running Linux.
Developers can write instrumentation to collect data on the operation
of the system.

%package runtime
Summary: Instrumentation System Runtime
Group: Development/System
License: GPLv2+
URL: http://sourceware.org/systemtap/
Requires: kernel >= 2.6.9-11
Requires(pre): shadow-utils

%description runtime
SystemTap runtime is the runtime component of an instrumentation
system for systems running Linux.  Developers can write
instrumentation to collect data on the operation of the system.

%package testsuite
Summary: Instrumentation System Testsuite
Group: Development/System
License: GPLv2+
URL: http://sourceware.org/systemtap/
Requires: systemtap = %{version}-%{release}
Requires: systemtap-sdt-devel = %{version}-%{release}
Requires: dejagnu which prelink

%description testsuite
The testsuite allows testing of the entire SystemTap toolchain
without having to rebuild from sources.

%package server
Summary: Instrumentation System Server
Group: Development/System
License: GPLv2+
URL: http://sourceware.org/systemtap/
Requires: systemtap = %{version}-%{release}
Requires: avahi nss nss-tools mktemp
Requires: zip unzip
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(postun): initscripts

%description server
This is the remote script compilation server component of systemtap.
It announces itself to local clients with avahi, and compiles systemtap
scripts to kernel objects on their demand.

%package sdt-devel
Summary: Static probe support tools
Group: Development/System
License: GPLv2+ and Public Domain
URL: http://sourceware.org/systemtap/

%description sdt-devel
Support tools to allow applications to use static probes.

%package initscript
Summary: Systemtap Initscripts
Group: Development/System
License: GPLv2+
URL: http://sourceware.org/systemtap/
Requires: systemtap-runtime = %{version}-%{release}
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(postun): initscripts

%description initscript
Initscript for Systemtap scripts

%if %{with_grapher}
%package grapher
Summary: Instrumentation System Grapher
Group: Development/System
License: GPLv2+
URL: http://sourceware.org/systemtap/
Requires: systemtap-runtime = %{version}-%{release}

%description grapher
SystemTap grapher is a utility for real-time visualization of
data from SystemTap instrumentation scripts.
%endif

%prep
%setup -q %{?setup_elfutils}

%if %{with_bundled_elfutils}
cd elfutils-%{elfutils_version}
%patch1 -p1
sleep 1
find . \( -name Makefile.in -o -name aclocal.m4 \) -print | xargs touch
sleep 1
find . \( -name configure -o -name config.h.in \) -print | xargs touch
cd ..
%endif

%build
CFLAGS="%{optflags}"
CXXFLAGS="%{optflags}"
LDFLAGS="%{optflags}"

%if %{with_bundled_elfutils}
# Build our own copy of elfutils.
%global elfutils_config --with-elfutils=elfutils-%{elfutils_version}

# We have to prevent the standard dependency generation from identifying
# our private elfutils libraries in our provides and requires.
%global _use_internal_dependency_generator	0
%global filter_eulibs() /bin/sh -c "%{1} | sed '/libelf/d;/libdw/d;/libebl/d'"
%global __find_provides %{filter_eulibs /usr/lib/rpm/find-provides}
%global __find_requires %{filter_eulibs /usr/lib/rpm/find-requires}

# This will be needed for running stap when not installed, for the test suite.
%global elfutils_mflags LD_LIBRARY_PATH=`pwd`/lib-elfutils
%endif

# Enable/disable the sqlite coverage testing support
%if %{with_sqlite}
%global sqlite_config --enable-sqlite
CFLAGS="`pkg-config sqlite3 --cflags` $CFLAGS"
LDFLAGS="`pkg-config sqlite3 --libs` $LDFLAGS"
%else
%global sqlite_config --disable-sqlite
%endif

# Enable/disable the crash extension
%if %{with_crash}
%global crash_config --enable-crash
%else
%global crash_config --disable-crash
%endif

# Enable/disable the code to find and suggest needed rpms
%if %{with_rpm}
%global rpm_config --with-rpm
%else
%global rpm_config --without-rpm
%endif

%if %{with_docs}
%global docs_config --enable-docs
%else
%global docs_config --disable-docs
%endif

# Enable pie as configure defaults to disabling it
%if %{pie_supported}
%global pie_config --enable-pie
%else
%global pie_config --disable-pie
%endif

%if %{with_grapher}
%global grapher_config --enable-grapher
%else
%global grapher_config --disable-grapher
%endif

%if %{with_publican}
%global publican_config --enable-publican --with-publican-brand=%{publican_brand}
%else
%global publican_config --disable-publican
%endif

export CFLAGS LDFLAGS
%configure %{?elfutils_config} %{sqlite_config} %{crash_config} %{docs_config} %{pie_config} %{grapher_config} %{publican_config} %{rpm_config} --disable-silent-rules

# temporary FIX (gcc-4.4.4 and boost-1.43.0 error)
sed -i 's/-Werror//g' grapher/Makefile

make %{?_smp_mflags}

%install
rm -rf ${RPM_BUILD_ROOT}
make DESTDIR=$RPM_BUILD_ROOT install
%find_lang %{name}

# We want the examples in the special doc dir, not the build install dir.
# We build it in place and then move it away so it doesn't get installed
# twice. rpm can specify itself where the (versioned) docs go with the
# %doc directive.
mv $RPM_BUILD_ROOT%{_datadir}/doc/systemtap/examples examples

# Fix paths in the example & testsuite scripts
find examples testsuite -type f -name '*.stp' -print0 | xargs -0 sed -i -r -e '1s@^#!.+stap@#!%{_bindir}/stap@'

# To make rpmlint happy, remove any .gitignore files in the testsuite.
find testsuite -type f -name '.gitignore' -print0 | xargs -0 rm -f

# Because "make install" may install staprun with whatever mode, the
# post-processing programs rpmbuild runs won't be able to read it.
# So, we change permissions so that they can read it.  We'll set the
# permissions back to 04110 in the %files section below.
chmod 755 $RPM_BUILD_ROOT%{_bindir}/staprun

# Copy over the testsuite
cp -rp testsuite $RPM_BUILD_ROOT%{_datadir}/systemtap

%if %{with_docs}
# We want the manuals in the special doc dir, not the generic doc install dir.
# We build it in place and then move it away so it doesn't get installed
# twice. rpm can specify itself where the (versioned) docs go with the
# %doc directive.
mkdir docs.installed
mv $RPM_BUILD_ROOT%{_datadir}/doc/systemtap/*.pdf docs.installed/
mv $RPM_BUILD_ROOT%{_datadir}/doc/systemtap/tapsets docs.installed/
%if %{with_publican}
mv $RPM_BUILD_ROOT%{_datadir}/doc/systemtap/SystemTap_Beginners_Guide docs.installed/
%endif
%endif

mkdir -p $RPM_BUILD_ROOT%{_initscriptdir}
install -m 755 initscript/systemtap $RPM_BUILD_ROOT%{_initscriptdir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/systemtap
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/systemtap/conf.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/systemtap/script.d
install -m 644 initscript/config.systemtap $RPM_BUILD_ROOT%{_sysconfdir}/systemtap/config
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/cache/systemtap
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/run/systemtap

install -m 755 initscript/stap-server $RPM_BUILD_ROOT%{_initscriptdir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/stap-server
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/stap-server/conf.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -m 644 initscript/config.stap-server $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/stap-server
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/log/stap-server
touch $RPM_BUILD_ROOT%{_localstatedir}/log/stap-server/log
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -m 644 initscript/logrotate.stap-server $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/stap-server

%clean
rm -rf ${RPM_BUILD_ROOT}

%pre
getent group stap-server >/dev/null || groupadd -g 155 -r stap-server || groupadd -r stap-server

%pre runtime
getent group stapdev >/dev/null || groupadd -r stapdev
getent group stapusr >/dev/null || groupadd -r stapusr
exit 0

%pre server
getent group stap-server >/dev/null || groupadd -r stap-server
getent passwd stap-server >/dev/null || useradd -c "Systemtap Compile Server" -g stap-server -d %{_localstatedir}/lib/stap-server -m -r -s /sbin/nologin stap-server
chmod 755 %{_localstatedir}/lib/stap-server
exit 0

%post server
test -e %{_localstatedir}/log/stap-server/log || {
     touch %{_localstatedir}/log/stap-server/log
     chmod 664 %{_localstatedir}/log/stap-server/log
     chown stap-server:stap-server %{_localstatedir}/log/stap-server/log
}
# If it does not already exit, as stap-server, generate the certificate
# used for signing and for ssl.
if test ! -e ~stap-server/.systemtap/ssl/server/stap.cert; then
runuser -s /bin/sh - stap-server -c %{_libexecdir}/%{name}/stap-gen-cert >/dev/null
# Authorize the certificate as a trusted ssl peer and as a trusted signer
   # on the local host.
   %{_bindir}/stap-authorize-server-cert ~stap-server/.systemtap/ssl/server/stap.cert
   %{_bindir}/stap-authorize-signing-cert ~stap-server/.systemtap/ssl/server/stap.cert
fi


# Activate the service
/sbin/chkconfig --add stap-server
exit 0

%preun server
# Check that this is the actual deinstallation of the package, as opposed to
# just removing the old package on upgrade.
if [ $1 = 0 ] ; then
    /sbin/service stap-server stop >/dev/null 2>&1
    /sbin/chkconfig --del stap-server
fi
exit 0

%postun server
# Check whether this is an upgrade of the package.
# If so, restart the service if it's running
if [ "$1" -ge "1" ] ; then
    /sbin/service stap-server condrestart >/dev/null 2>&1 || :
fi
exit 0

%post initscript
/sbin/chkconfig --add systemtap
exit 0

%preun initscript
# Check that this is the actual deinstallation of the package, as opposed to
# just removing the old package on upgrade.
if [ $1 = 0 ] ; then
    /sbin/service systemtap stop >/dev/null 2>&1
    /sbin/chkconfig --del systemtap
fi
exit 0

%postun initscript
# Check whether this is an upgrade of the package.
# If so, restart the service if it's running
if [ "$1" -ge "1" ] ; then
    /sbin/service systemtap condrestart >/dev/null 2>&1 || :
fi
exit 0

%post
# Remove any previously-built uprobes.ko materials
(make -C %{_datadir}/%{name}/runtime/uprobes clean) >/dev/null 2>&1 || true
(/sbin/rmmod uprobes) >/dev/null 2>&1 || true

%preun
# Ditto
(make -C %{_datadir}/%{name}/runtime/uprobes clean) >/dev/null 2>&1 || true
(/sbin/rmmod uprobes) >/dev/null 2>&1 || true

%files -f %{name}.lang
%defattr(-,root,root)

%doc README README.unprivileged AUTHORS NEWS COPYING examples
%if %{with_docs}
%doc docs.installed/*.pdf
%doc docs.installed/tapsets
%if %{with_publican}
%doc docs.installed/SystemTap_Beginners_Guide
%endif
%endif

%{_bindir}/stap
%{_mandir}/man1/*
%exclude %{_mandir}/man1/dtrace.1*
%exclude %{_mandir}/man1/stapgraph.1*
%{_mandir}/man3/*

%dir %{_datadir}/%{name}
%{_datadir}/%{name}/runtime
%{_datadir}/%{name}/tapset

%if %{with_bundled_elfutils}
%{_libdir}/%{name}/lib*.so*
%endif

%files runtime
%defattr(-,root,root)
%attr(4110,root,stapusr) %{_bindir}/staprun
%{_bindir}/stapsh
%{_bindir}/stap-merge
%{_bindir}/stap-report
%{_libexecdir}/%{name}/stapio
%{_libexecdir}/%{name}/stap-env
%{_libexecdir}/%{name}/stap-authorize-cert
%if %{with_crash}
%{_libdir}/%{name}/staplog.so*
%endif
%{_mandir}/man7/stappaths.7*
%{_mandir}/man8/staprun.8*

%doc README README.security AUTHORS NEWS COPYING

%files testsuite
%defattr(-,root,root)
%{_datadir}/%{name}/testsuite

%files server
%defattr(-,root,root)
%{_bindir}/stap-server
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/stap-serverd
%{_libexecdir}/%{name}/stap-start-server
%{_libexecdir}/%{name}/stap-stop-server
%{_libexecdir}/%{name}/stap-gen-cert
%{_libexecdir}/%{name}/stap-sign-module
%{_mandir}/man8/stap-server.8*
%{_initscriptdir}/stap-server
%config(noreplace) %{_sysconfdir}/logrotate.d/stap-server
%dir %{_sysconfdir}/stap-server
%dir %{_sysconfdir}/stap-server/conf.d
%config(noreplace) %{_sysconfdir}/sysconfig/stap-server
%dir %attr(0755,stap-server,stap-server) %{_localstatedir}/log/stap-server
%ghost %config %attr(0644,stap-server,stap-server) %{_localstatedir}/log/stap-server/log
%doc initscript/README.stap-server

%files sdt-devel
%defattr(-,root,root)
%{_bindir}/dtrace
%{_includedir}/sys/sdt.h
%{_includedir}/sys/sdt-config.h
%{_mandir}/man1/dtrace.1*
%doc README AUTHORS NEWS COPYING

%files initscript
%defattr(-,root,root)
%{_initscriptdir}/systemtap
%dir %{_sysconfdir}/systemtap
%dir %{_sysconfdir}/systemtap/conf.d
%dir %{_sysconfdir}/systemtap/script.d
%config(noreplace) %{_sysconfdir}/systemtap/config
%dir %{_localstatedir}/cache/systemtap
%ghost %{_localstatedir}/run/systemtap
%doc initscript/README.systemtap
%{_mandir}/man1/stapgraph.1*

%if %{with_grapher}
%files grapher
%defattr(-,root,root)
%{_bindir}/stapgraph
%{_datadir}/%{name}/*.glade
%endif

%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-6m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (1.6-5m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-4m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-3m)
- rebuild for boost

* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6-2m)
- fix %%files to avoid conflicting

* Wed Aug  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6-1m)
- update 1.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-9m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-8m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-7m)
- rebuild against boost-1.46.0
- revise spec; %%{optflags} should be used

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-6m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-5m)
- rebuild against boost-1.44.0

* Fri Sep 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-4m)
- update CFLAGS and LDFLAGS for sqlite3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-3m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- disable publican

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2-1m)
- update 1.2

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1-7m)
- add %%dir

* Mon May  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- add BuildRequires

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-5m)
- [SECURITY] CVE-2010-0411 CVE-2010-0412 CVE-2009-4273
- merge the Fedora 13 changes
-- * Mon Feb 15 2010 Mark Wielaard <mjw@redhat.com> - 1.1-2
-- - Add systemtap-1.1-cfi-cfa_ops-fixes.patch
--   - Resolves RHBZ #564429
-- - Add systemtap-1.1-get_argv.patch
--   - Resolves CVE-2010-0411
-- - Add systemtap-1.1-tighten-server-params.patch (excluding testsuite)
--   - Resolves CVE-2010-0412, CVE-2009-4273

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against rpm-4.8.0

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-3m)
- Provides: %%{_bindir}/stap again

* Thu Jan 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- systemtap-runtime has stap-authorize-server-cert, stap-report and
  stap-find-servers

* Thu Jan 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- [SECURITY] CVE-2009-2911
- update to 1.1
- a bit momonganize

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.9-2m)
- remove stap-authorize-cert and stap-gen-cert from package server
  to avoid conflicting with main package: systemtap
- stap-authorize-cert and stap-gen-cert are already provided by main package
  and systemtap-server Requires: systemtap (main package)

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-1m)
- sync with Rawhide (0.9.9-4)

* Mon Jun  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-3m)
- revise BuildRequires

* Sun May 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.7-2m)
- revise %%files to avoid conflicting

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-1m)
- sync with Fedora 11 (0.9.7-1)
-- add latex workaround (Patch100)
-- build with --disable-grapher 

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-4m)
- apply gcc44 patch
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-3m)
- rebuild against rpm-4.6

* Thu Jul 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-2m)
- Provides: %%{_bindir}/stap again

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-1m)
- update to 0.7

* Tue Jul 15 2008 Frank Ch. Eigler <fche@redhat.com> - 0.7-1
- Upstream release.

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-3m)
- rebuild against gcc43

* Sun Feb 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-2m)
- Provides: %%{_bindir}/stap

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-1m)
- update 0.6.1

* Thu Jul  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.13-2m)
- modify %%files to avoid conflicting

* Sat Jun 30 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.13-1m)
- spec imported from fc devel and modified

* Mon Jan  1 2007 Frank Ch. Eigler <fche@redhat.com> - 0.5.12-1
- Many changes, see NEWS file.

* Tue Sep 26 2006 David Smith <dsmith@redhat.com> - 0.5.10-1
- Added 'systemtap-runtime' subpackage.

* Wed Jul 19 2006 Roland McGrath <roland@redhat.com> - 0.5.9-1
- PRs 2669, 2913

* Fri Jun 16 2006 Roland McGrath <roland@redhat.com> - 0.5.8-1
- PRs 2627, 2520, 2228, 2645

* Fri May  5 2006 Frank Ch. Eigler <fche@redhat.com> - 0.5.7-1
- PRs 2511 2453 2307 1813 1944 2497 2538 2476 2568 1341 2058 2220 2437
  1326 2014 2599 2427 2438 2465 1930 2149 2610 2293 2634 2506 2433

* Tue Apr  4 2006 Roland McGrath <roland@redhat.com> - 0.5.5-1
- Many changes, affected PRs include: 2068, 2293, 1989, 2334,
  1304, 2390, 2425, 953.

* Wed Feb  1 2006 Frank Ch. Eigler <fche@redhat.com> - 0.5.4-1
- PRs 1916, 2205, 2142, 2060, 1379

* Mon Jan 16 2006 Roland McGrath <roland@redhat.com> - 0.5.3-1
- Many changes, affected PRs include: 2056, 1144, 1379, 2057,
  2060, 1972, 2140, 2148

* Mon Dec 19 2005 Roland McGrath <roland@redhat.com> - 0.5.2-1
- Fixed build with gcc 4.1, various tapset changes.

* Wed Dec  7 2005 Roland McGrath <roland@redhat.com> - 0.5.1-1
- elfutils update, build changes

* Fri Dec 02 2005  Frank Ch. Eigler  <fche@redhat.com> - 0.5-1
- Many fixes and improvements: 1425, 1536, 1505, 1380, 1329, 1828, 1271,
  1339, 1340, 1345, 1837, 1917, 1903, 1336, 1868, 1594, 1564, 1276, 1295

* Mon Oct 31 2005 Roland McGrath <roland@redhat.com> - 0.4.2-1
- Many fixes and improvements: PRs 1344, 1260, 1330, 1295, 1311, 1368,
  1182, 1131, 1332, 1366, 1456, 1271, 1338, 1482, 1477, 1194.

* Wed Sep 14 2005 Roland McGrath <roland@redhat.com> - 0.4.1-1
- Many fixes and improvements since 0.2.2; relevant PRs include:
  1122, 1134, 1155, 1172, 1174, 1175, 1180, 1186, 1187, 1191, 1193, 1195,
  1197, 1205, 1206, 1209, 1213, 1244, 1257, 1258, 1260, 1265, 1268, 1270,
  1289, 1292, 1306, 1335, 1257

* Wed Sep  7 2005 Frank Ch. Eigler <fche@redhat.com>
- Bump version.

* Wed Aug 16 2005 Frank Ch. Eigler <fche@redhat.com>
- Bump version.

* Wed Aug  3 2005 Martin Hunt <hunt@redhat.com> - 0.2.2-1
- Add directory /var/cache/systemtap
- Add stp_check to /usr/libexec/systemtap

* Wed Aug  3 2005 Roland McGrath <roland@redhat.com> - 0.2.1-1
- New version 0.2.1, various fixes.

* Fri Jul 29 2005 Roland McGrath <roland@redhat.com> - 0.2-1
- New version 0.2, requires elfutils 0.111

* Mon Jul 25 2005 Roland McGrath <roland@redhat.com>
- Clean up spec file, build bundled elfutils.

* Thu Jul 21 2005 Martin Hunt <hunt@redhat.com>
- Set Version to use version from autoconf.
- Fix up some of the path names.
- Add Requires and BuildRequires.

* Wed Jul 19 2005 Will Cohen <wcohen@redhat.com>
- Initial creation of RPM.
