%global momorel 1
%global betaver 1
%global prever 1

Summary:      Advanced drum machine for GNU/Linux
Name:         hydrogen
Version:      0.9.6
Release:      %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License:      GPLv2+
Group:        Applications/Multimedia
URL:          http://www.hydrogen-music.org/
Source0:      http://dl.sourceforge.net/project/%{name}/Hydrogen/%{version}%20Sources/%{name}-%{version}%{?betaver:-beta%{betaver}}.tar.gz
NoSource:     0
# For convenience, to take the svn snapshot:
# Source9:      %{name}-snapshot.sh
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils
Requires(postun): coreutils
Requires(postun): gtk2
Requires(posttrans): gtk2
BuildRequires: alsa-lib-devel
BuildRequires: desktop-file-utils
BuildRequires: flac-devel 
BuildRequires: jack-devel
BuildRequires: lash-devel 
BuildRequires: liblrdf-devel
BuildRequires: libsndfile-devel
BuildRequires: libtar-devel
# BuildRequires: portaudio-devel
# BuildRequires: portmidi-devel
BuildRequires: qt-devel >= 4.7.0
BuildRequires: cmake

%description
Hydrogen is an advanced drum machine for GNU/Linux. It's main goal is to bring 
professional yet simple and intuitive pattern-based drum programming.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries

%description devel
%{summary}.

%prep
%setup -q -n %{name}-%{version}%{?betaver:-beta%{betaver}}

%build
export QTDIR=%{_qt4_prefix}

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# No need to package these (they will not be installed automatically in rc3?):
rm -f %{buildroot}%{_datadir}/%{name}/data/doc/{Makefile,README}* \
      %{buildroot}%{_datadir}/%{name}/data/doc/*.{docbook,po,pot} \
      %{buildroot}%{_datadir}/applications/momonga-%{name}.desktop \
      %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/h2-icon.svg
%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
        touch --no-create %{_datadir}/icons/hicolor &>/dev/null
        gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING* README.txt
%{_bindir}/%{name}
%{_bindir}/h2cli
%{_bindir}/h2player
%{_bindir}/h2synth
%{_libdir}/libhydrogen-core-0.9.6.so
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
# %%{_datadir}/icons/hicolor/scalable/apps/*.svg

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}

%changelog
* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-0.1.1m)
- update to 0.9.6-beta1

* Sat Aug 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-2m)
- fix build failure; add patch for linux 3.0

* Sun May 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-0.1.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-0.1.5m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-0.1.4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.5-0.1.3m)
- full rebuild for mo7 release

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-0.1.2m)
- fix build error

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-0.1.1m)
- update to 0.9.5rc1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-6m)
- rebuild against qt-4.6.3-1m

* Mon May 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-5m)
- fix build with qt-4.7.0

* Sun May  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-4m)
- fix build with new gcc on i686

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-3m)
- explicitly link libz

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-2m)
- NoSource
- don't commit .specdb

* Tue Jan 29 2010 Daniel Mclellan <daniel.mclellan@gmail.com>
- (0.9.4-1m)
- first momonga release

* Tue Sep 15 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.9.4-1
- Update to 0.9.4

* Sat Aug 22 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.9.4-0.7.rc2
- Update to 0.9.4-rc2

* Wed Aug 05 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.9.4-0.6.rc1.1
- Update .desktop file

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.4-0.5.rc1.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jul 14 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.9.4-0.4.rc1.1
- Rebuild against new lash build on F-12 due to the e2fsprogs split

* Tue Apr 14 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.9.4-0.3.rc1.1
- Update to 0.9.4-rc1-1

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.4-0.2.790svn
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Feb 13 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.9.4-0.1.790svn
- Update to 0.9.4-beta3 (uses scons and qt4)

* Fri Apr 04 2008 Lubomir Kundrak <lkundrak@redhat.com> - 0.9.3-13
- QT3 changes by rdieter
- Fix build

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.3-12
- Autorebuild for GCC 4.3

* Thu Jan 03 2008 Lubomir Kundrak <lkundrak@redhat.com> 0.9.3-11
- Previous change was not a good idea
- Adding missing includes to fix build with gcc-4.3

* Sun Oct 14 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.9.3-10
- Remove unneeded dependencies on desktop-file-utils

* Mon Oct 09 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.9.3-9
- Incorporate fixes from #190040, thanks to Hans de Goede
- Removed useless LIBDIR introduced in previous revision
- Fixed desktop file installation
- Call gtk-update-icon-cache only if it is present

* Sun Oct 07 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.9.3-8
- Remove -j from make to fix concurrency problems
- Handle libdir on 64bit platforms correctly
- Rename patches

* Sat Oct 06 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.9.3-7.1
- Fix desktop file
- Fix compatibility with new FLAC
- Fix linking for Build ID use

* Mon Mar 26 2007 Anthony Green <green@redhat.com> 0.9.3-7
- Improve Source0 link.
- Add %%post(un) scriptlets for MimeType update.
- Add update-desktop-database scriptlets.

* Sat Jul 22 2006 Anthony Green <green@redhat.com> 0.9.3-6
- Add hydrogen-null-sample.patch to fix crash.

* Sun Jul 02 2006 Anthony Green <green@redhat.com> 0.9.3-5
- Clean up BuildRequires.
- Configure with --disable-oss-support
- Don't run ldconfig (not needed)
- Remove post/postun scriptlets.

* Sat May 13 2006 Anthony Green <green@redhat.com> 0.9.3-4
- BuildRequire libxml2-devel.
- Remove explicit Requires for some runtime libraries.
- Set QTDIR via /etc/profile.d/qt.sh.
- Update desktop icons and icon cache in post and postun.
- Don't use __rm or __make macros.

* Sat May 13 2006 Anthony Green <green@redhat.com> 0.9.3-3
- Conditionally apply ardour-lib64-ladspa.patch.

* Sat May 13 2006 Anthony Green <green@redhat.com> 0.9.3-2
- Build fixes for x86_64.

* Wed Apr 26 2006 Anthony Green <green@redhat.com> 0.9.3-1
- Created.
