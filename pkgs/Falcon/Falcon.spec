%global         momorel 1
%global         _pkgdocdir %{_docdir}/%{name}-%{version}

%global         mainver  0.9.6
%global         subver 8
%global docver  %{version}

Name:           Falcon
Version:        %{mainver}.%{subver}
Release:        %{momorel}m%{?dist}
Summary:        The Falcon Programming Language
License:        GPLv2+
Group:          Development/Languages
URL:            http://www.falconpl.org/
Source0:        http://www.falconpl.org/project_dl/_official_rel/%{name}-%{version}.tgz
NoSource:       0
Source1:        http://www.falconpl.org/project_dl/_official_rel/%{name}-docs-%{docver}.tgz
NoSource:       1
# Patches from Git for Falcon's mongo modules
Patch0:         Falcon-0.9.6.8-mongo-cmake-linux-x64.patch
Patch1:         Falcon-0.9.6.8-mongo-stdint.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  bison 
BuildRequires:  cmake 
BuildRequires:  pcre-devel >= 8.31
BuildRequires:  zlib-devel

%description
The Falcon Programming Language is an embeddable scripting language
aiming to empower even simple applications with a powerful,
flexible, extensible and highly configurable scripting engine.

Falcon is also a standalone multiplatform scripting language that
aims to be both simple and powerful.

%package   devel
Summary:   Development files for %{name}
Group:     Development/Libraries
Requires:  %{name} = %{version}-%{release}

%description devel
The Falcon Programming Language is an embeddable scripting language
aiming to empower even simple applications with a powerful,
flexible, extensible and highly configurable scripting engine.

Falcon is also a standalone multiplatform scripting language that
aims to be both simple and powerful.

This package contains development files for %{name}. This is not
necessary for using the %{name} interpreter.

%package   doc
Summary:   Documentation for %{name}
Group:     Documentation
Requires:  %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
The Falcon Programming Language is an embeddable scripting language
aiming to empower even simple applications with a powerful,
flexible, extensible and highly configurable scripting engine.

Falcon is also a standalone multiplatform scripting language that
aims to be both simple and powerful.

This package contains HTML documentation for %{name}.

%prep
%setup -q -a 1
%patch0 -p1 -b .mongo-cmake-linux-x64
%patch1 -p1 -b .mongo-stdint

%build
reldocdir=$(echo %{_pkgdocdir} | sed -e 's|^%{_prefix}/||')
%cmake . \
        -DFALCON_SHARE_DIR=$reldocdir \
        -DMYSQL_LIBRARY:FILEPATH=%{_libdir}/mysql/libmysqlclient.so

make VERBOSE=1 %{?_smp_flags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog copyright README RELNOTES LICENSE LICENSE_GPLv2
%exclude %{_bindir}/falcon-conf
%exclude %{_bindir}/falconeer.fal
%exclude %{_bindir}/faltest
%{_bindir}/*
%exclude %{_mandir}/man1/falcon-conf*
%exclude %{_mandir}/man1/falconeer.fal*
%exclude %{_mandir}/man1/faltest*
%{_libdir}/falcon
%{_libdir}/*.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%{_bindir}/falcon-conf
%{_bindir}/falconeer.fal
%{_bindir}/faltest
%{_includedir}/*
%{_libdir}/*.so
%{_datadir}/cmake/faldoc
%{_mandir}/man1/falcon-conf*
%{_mandir}/man1/falconeer.fal*
%{_mandir}/man1/faltest*

%files doc
%defattr(-,root,root,-)
%doc %{docver}-html

%changelog
* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6.8-1m)
- update to 0.9.6.8

* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.14.2-7m)
- rebuild against pcre-8.31

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.14.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.14.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.14.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.14.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.14.2-2m)
- version down to 0.8.14, we can not build kdebindings with 0.9.0 or later

* Wed Jul 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.14.2-1m)
- import from Fedora devel

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.14.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Feb  6 2009 Michel Salim <salimma@fedoraproject.org> - 0.8.14.2-1
- Update to 0.8.14.2

* Mon Jun  9 2008 Michel Alexandre Salim <salimma@fedoraproject.org> - 0.8.10-3
- Revert r401 patch; does not fix cmake-2.6 problem on Rawhide
  Reverting to manually using 'make install' in individual subdirectories

* Mon Jun  9 2008 Michel Alexandre Salim <salimma@fedoraproject.org> - 0.8.10-2
- Merge in cmake fixes from core/trunk r401
- Patch core/CMakeLists.txt to default to /usr, as it appears that the
  requested prefix is not properly used
- Fix incorrect #! interpreter in falconeer.fal

* Sat Jun  7 2008 Michel Alexandre Salim <salimma@fedoraproject.org> - 0.8.10-1
- Update to 0.8.10

* Wed May 21 2008 Michel Salim <salimma@fedoraproject.org> - 0.8.8-3
- Use correct libdir for module path

* Thu Apr 24 2008 Michel Alexandre Salim <salimma@fedoraproject.org> - 0.8.8-2
- Updated license
- Changed source URL to one that includes license grant

* Fri Jan 25 2008 Michel Salim <michel.sylvan@gmail.com> - 0.8.8-1
- Initial Fedora package
  Based on initial spec by Giancarlo Niccolai <gc@falconpl.org>
