%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global atticaver 0.4.2
%global phononver 4.7.1
%global sopranover 2.9.4
%global strigiver 0.7.8
%global qimageblitzver 0.0.6
%global obso_name kdebase-runtime

%define flags 1
%define slp 1

Summary:       K Desktop Environment - Runtime
Name:          kde-runtime
Version:       %{kdever}
Release:       %{momorel}m%{?dist}
License:       GPLv2
Group:         User Interface/Desktops
URL:           http://www.kde.org/
Source0:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:      0
Patch1:        kdebase-runtime-4.6.95-knetattach.patch
# add OnlyShowIn=KDE  to Desktop/Home.desktop (like trash.desktop)
Patch6:        kdebase-runtime-4.3.3-home_onlyshowin_kde.patch
Patch7:        kdebase-runtime-4.5.3-htsearch.patch
# Launch compiz via compiz-manager so we get window decorations and
# other such decadent luxuries (AdamW 2011/01)
Patch8:        kdebase-runtime-4.5.95-compiz.patch
# add overrides in default manpath
Patch9:        kdebase-runtime-4.3.4-man-overrides.patch
# disable making files read only when moving them into trash
# (Upstream wouldn't accept this)
Patch11:       kde-runtime-4.10.4-trash-readonly.patch

## upstreamable patches
# make installdbgsymbols.sh use pkexec instead of su 
# increase some timeouts in an effort to see (some) errors before close
Patch50:       kde-runtime-4.9.0-installdbgsymbols.patch
# use packagekit to install a possibly-missing gdb
Patch51:       kde-runtime-4.11.2-install_gdb.patch
# avoid X3 mouse events
# https://bugs.kde.org/show_bug.cgi?id=316546
Patch52:       kde-runtime-mouseeventlistener.patch
# Volume gets restored to 100% after each knotify event
# https://bugs.kde.org/show_bug.cgi?id=324975
# knotify/libcanberra support from mageia
# http://svnweb.mageia.org/packages/cauldron/kdebase4-runtime/current/SOURCES/kdebase-runtime-4.6.0-canberra.patch
Patch60:       kdebase-runtime-4.6.0-canberra.patch

## upstream patches

BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: kdelibs >= %{version}-%{kdelibsrel}
Requires: kdepimlibs >= %{version}
Requires: %{name}-libs = %{version}-%{release}
%if 0%{?flags}
Requires: %{name}-flags = %{version}-%{release}
%endif
%if 0%{?no_activitymanager}
Requires: libkactivities
%endif
Requires: nepomuk-core >= %{version}
# ensure default/fallback icon theme present
# beware of bootstrapping, there be dragons
Requires: oxygen-icon-theme >= %{version}
Requires: eject
Requires: hicolor-icon-theme
Requires: htdig
Requires: ntrack-qt
Requires: virtuoso-opensource
BuildRequires: alsa-lib-devel
BuildRequires: attica-devel >= %{atticaver}
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: chrpath
BuildRequires: clucene-core-devel
BuildRequires: exiv2-devel >= 0.23
BuildRequires: giflib-devel
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: kdelibs-devel >= %{version}-%{kdelibsrel}
BuildRequires: kdepimlibs-devel >= %{version}
BuildRequires: libcanberra-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libqzeitgeist-devel
BuildRequires: libsmbclient-devel
BuildRequires: libssh-devel
BuildRequires: libXScrnSaver-devel
BuildRequires: ntrack-devel
BuildRequires: ntrack-qt-devel >= 014-2m
BuildRequires: OpenEXR-devel >= 1.7.1
%if 0%{?slp}
BuildRequires: openslp-devel
%endif
BuildRequires: openssl-devel
BuildRequires: pcre-devel
BuildRequires: pkgconfig
BuildRequires: phonon-devel >= %{phononver}
BuildRequires: qimageblitz-devel >= %{qimageblitzver}
BuildRequires: redland-devel >= 1:1.0.10
BuildRequires: soprano-devel >= %{sopranover}
BuildRequires: strigi-devel >= %{strigiver}
BuildRequires: xine-lib-devel
# needed for phonon-xine VideoWidget, also need xine-lib built with libxcb support
BuildRequires: libxcb-devel
# needed?
BuildRequires: xorg-x11-font-utils
BuildRequires: xorg-x11-proto-devel
BuildRequires: zlib-devel
BuildRequires: ImageMagick
# avoid to conflicting 'kdesu'
BuildRequires: kdebase3 >= 3.5.10-8m
BuildRequires: nepomuk-core-devel
Obsoletes: nepomukcontroller
Obsoletes: %{obso_name} < %{version}
Provides:  %{obso_name} = %{version}-%{release}

%description
Core runtime for the K Desktop Environment 4.

%package devel
Group:    Development/Libraries
Summary:  Developer files for %{name}
Requires: %{name}-libs = %{version}-%{release}
Obsoletes: %{obso_name}-devel < %{version}
Provides:  %{obso_name}-devel = %{version}-%{release}

%description devel
%{summary}.

%package drkonqi
Summary: DrKonqi KDE crash handler
Requires: %{name} = %{version}-%{release}
# drkonqi patch51 uses pkexec
Requires: polkit

%description drkonqi
%{summary}.

%package libs
Summary: Runtime libraries for %{name}
Group:   System Environment/Libraries
Requires: kdelibs >= %{version}
Requires: kdepimlibs >= %{version}
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-libs < %{version}
Provides:  %{obso_name}-libs = %{version}-%{release}

%description libs
%{summary}.

%package flags
Summary: Geopolitical flags
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-flags < %{version}
Provides:  %{obso_name}-flags = %{version}-%{release}
BuildArch: noarch

%description flags
%{summary}.

%prep
%setup -q

%patch1 -p1 -b .knetattach
%patch6 -p1 -b .home_onlyshowin_kde
%patch7 -p1 -b .htsearch
%patch8 -p1 -b .config
%patch9 -p1 -b .man-overrides
%patch11 -p1 -b .trash-readonly
%patch50 -p1 -b .installdgbsymbols
%patch51 -p1 -b .install_gdb
%patch52 -p1 -b .mouseeventlistener
%patch60 -p1 -b .canberra

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

# kdesu symlink
ln -s %{_kde4_libexecdir}/kdesu %{buildroot}%{_kde4_bindir}/kdesu

# omit hicolor index.theme, use one from hicolor-icon-theme
rm -f %{buildroot}%{_kde4_iconsdir}/hicolor/index.theme

# remove country flags because some people/countries forbid some other
# people/countries' flags :-(
%{!?flags:rm -f %{buildroot}%{_kde4_datadir}/locale/l10n/*/flag.png}

# install this service for KDE 3 applications
mkdir %{buildroot}%{_datadir}/services
ln -s %{_kde4_datadir}/kde4/services/khelpcenter.desktop \
      %{buildroot}%{_datadir}/services/khelpcenter.desktop

# installdbgsymbols script
install -p -D -m755 drkonqi/doc/examples/installdbgsymbols_fedora.sh \
  %{buildroot}%{_kde4_libexecdir}/installdbgsymbols.sh

# FIXME: -devel type files, omit for now
rm -vf  %{buildroot}%{_kde4_libdir}/lib{kwalletbackend,molletnetwork}.so

# rpaths
# use chrpath hammer for now, find better patching solutions later -- Rex
chrpath --list   %{buildroot}%{_libdir}/kde4/plugins/phonon_platform/kde.so ||:
chrpath --delete %{buildroot}%{_libdir}/kde4/plugins/phonon_platform/kde.so

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/crystalsvg &> /dev/null || :
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/crystalsvg &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
update-desktop-database -q &> /dev/null ||:
update-mime-database %{_kde4_datadir}/mime &> /dev/null

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_kde4_iconsdir}/crystalsvg &> /dev/null || :
    touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
    gtk-update-icon-cache %{_kde4_iconsdir}/crystalsvg &> /dev/null || :
    gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
    update-desktop-database -q &> /dev/null ||:
    update-mime-database %{_kde4_datadir}/mime &> /dev/null
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post drkonqi
# make DrKonqi work by default by taming SELinux enough (suggested by dwalsh)
# if KDE_DEBUG is set, DrKonqi is disabled, so do nothing
# if it is unset (or empty), check if deny_ptrace is already disabled
# if not, disable it
if [ -z "$KDE_DEBUG" ] ; then
  if [ "`getsebool deny_ptrace 2>/dev/null`" == 'deny_ptrace --> on' ] ; then
    setsebool -P deny_ptrace off &> /dev/null || :
  fi
fi

%files
%defattr(-,root,root,-)
%{_kde4_bindir}/*
%{_kde4_appsdir}/desktoptheme/appdashboard
%{_kde4_appsdir}/desktoptheme/default/colors
%{_kde4_appsdir}/desktoptheme/default/dialogs
%{_kde4_appsdir}/desktoptheme/default/icons
%{_kde4_appsdir}/desktoptheme/default/metadata.desktop
%{_kde4_appsdir}/desktoptheme/default/opaque
%{_kde4_appsdir}/desktoptheme/default/toolbar-icons
%{_kde4_appsdir}/desktoptheme/default/translucent
%{_kde4_appsdir}/desktoptheme/default/widgets/*
%{_kde4_appsdir}/desktoptheme/oxygen
%{_kde4_appsdir}/hardwarenotifications
%{_kde4_appsdir}/kcm_componentchooser/*
%{_kde4_appsdir}/kcm_phonon
%{_kde4_appsdir}/kcmlocale
%{_kde4_appsdir}/kconf_update/*
%{_kde4_appsdir}/kde
%{_kde4_appsdir}/kglobalaccel
%{_kde4_appsdir}/khelpcenter
%{_kde4_appsdir}/kio_bookmarks
%{_kde4_appsdir}/kio_desktop
%{_kde4_appsdir}/kio_docfilter
%{_kde4_appsdir}/kio_finger
%{_kde4_appsdir}/kio_info
%{_kde4_appsdir}/konqsidebartng/virtual_folders/remote/virtualfolder_network.desktop
%{_kde4_appsdir}/konqueror
%{_kde4_appsdir}/ksmserver
%{_kde4_appsdir}/kwalletd
%{_kde4_appsdir}/libphonon
%{_kde4_appsdir}/phonon
%{_kde4_appsdir}/remoteview
%{_kde4_datadir}/config.kcfg/
%{_datadir}/dbus-1/interfaces/*
%{_datadir}/dbus-1/services/*
%{_datadir}/dbus-1/system-services/*.service
%{_kde4_iconsdir}/default.kde4
%{_kde4_iconsdir}/hicolor/*/apps/*.png
%{_kde4_iconsdir}/hicolor/*/apps/*.svgz
%{_kde4_datadir}/config/khotnewstuff.knsrc
%{_kde4_datadir}/config/khotnewstuff_upload.knsrc
%{_kde4_datadir}/config/emoticons.knsrc
%{_kde4_datadir}/config/icons.knsrc
%{_kde4_datadir}/kde4/services/*.protocol
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/kde4/services/qimageioplugins/webp.desktop
%{_kde4_datadir}/kde4/services/kded/*
%{_kde4_datadir}/kde4/services/searchproviders
%{_kde4_datadir}/kde4/servicetypes/*
%{_kde4_datadir}/mime/packages/network.xml
%{_kde4_datadir}/mime/packages/webp.xml
%{_kde4_datadir}/sounds/*
%{_kde4_docdir}/HTML/en/fundamentals
%{_kde4_docdir}/HTML/en/kcontrol
%{_kde4_docdir}/HTML/en/kdebugdialog
%{_kde4_docdir}/HTML/en/kdesu
%{_kde4_docdir}/HTML/en/khelpcenter
%{_kde4_docdir}/HTML/en/kioslave/*
%{_kde4_docdir}/HTML/en/knetattach
%{_kde4_docdir}/HTML/en/onlinehelp
%{_kde4_libdir}/libkdeinit4_*.so
%{_kde4_libdir}/kde4/kcm_*.so
%{_kde4_libdir}/kde4/kded_*.so
%{_kde4_libdir}/kde4/platformimports
%{_kde4_libdir}/kconf_update_bin/*
%{_kde4_libexecdir}/kcmremotewidgetshelper
%{_kde4_libexecdir}/kdeeject
%{_kde4_libexecdir}/kdesu
%attr(2755,root,nobody) %{_kde4_libexecdir}/kdesud
%{_kde4_libexecdir}/kdontchangethehostname
%{_kde4_libexecdir}/khc_docbookdig.pl
%{_kde4_libexecdir}/khc_htdig.pl
%{_kde4_libexecdir}/khc_htsearch.pl
%{_kde4_libexecdir}/khc_indexbuilder
%{_kde4_libexecdir}/khc_mansearch.pl
%{_kde4_libexecdir}/kioexec
%{_kde4_libexecdir}/knetattach
%{_mandir}/man1/*
%{_kde4_sysconfdir}/xdg/menus/kde-information.menu
%{_kde4_datadir}/applications/kde4/Help.desktop
%{_kde4_datadir}/applications/kde4/knetattach.desktop
%{_kde4_configdir}/kshorturifilterrc
%{_kde4_datadir}/desktop-directories/*.directory
%{_kde4_datadir}/emoticons/kde4/
%{_kde4_datadir}/locale/currency
%{_kde4_datadir}/locale/l10n/*.desktop
%{_kde4_datadir}/locale/l10n/*/entry.desktop
%{_datadir}/services/khelpcenter.desktop
%{_datadir}/polkit-1/actions/*.policy
%{_sysconfdir}/dbus-1/system.d/*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/*.h
%{_kde4_appsdir}/cmake/modules/*.cmake


%files drkonqi
%{_kde4_libexecdir}/drkonqi
%{_kde4_libexecdir}/installdbgsymbols.sh
%{_kde4_appsdir}/drkonqi

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/attica_kde.so
%{_kde4_libdir}/libknotifyplugin.so
%{_kde4_libdir}/libkwalletbackend.so.*
%{_kde4_libdir}/libmolletnetwork.so.*
# FIXME: move some of these to main pkg, e.g. kcm*.so, kded_*.so
%{_kde4_libdir}/kde4/*.so
%exclude %{_kde4_libdir}/kde4/kcm_*.so
%exclude %{_kde4_libdir}/kde4/kded_*.so
%{_kde4_libdir}/kde4/imports
%{_kde4_libdir}/kde4/plugins/phonon_platform/*
%{_kde4_libdir}/kde4/plugins/imageformats/kimg_webp.so

%if 0%{?flags}
%files flags
%defattr(-,root,root,-)
%{_kde4_datadir}/locale/l10n/*/flag.png
%endif

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-4m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Sun Dec 23 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9.95-3m)
- revert a change of 2m, it's my mistake

* Sat Dec 22 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9.95-2m)
- fix build, we have no samba3

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-2m)
- rebuild against exiv2-0.23

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.97-2m)
- add BuildRequires: nepomuk-core-devel

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-2m)
- apply upstream listview margins patch

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Tue Feb  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- fix kde#771053

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0
- rename from kdebase-runtime to kde-runtime

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.97-2m)
- remove BR hal

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-2m)
- rebuild against attica-0.3.0

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Mon Nov  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-4m)
- add BuildRequires: ntrack-qt-devel

* Sun Nov  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-3m)
- support QNtrack

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-2m)
- no activitymanager

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-2m)
- rebuild against exiv2-0.22

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-2m)
- add BuildRequires: libqzeitgeist-devel

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-3m)
- rebuild against exiv2-0.21.1

* Fri Mar 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-2m)
- import patch to support nm-0.9

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-3m)
- modify %%files to avoid conflicting

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Sat Nov 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-3m)
- fix kdesu problem
- fix htsearch path

* Fri Nov 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-2m)
- import patch101 from Fedora devel (kde#255736)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-2m)
- import patch100 from cooker

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-2m)
- change BuildRequires: from libattica-devel to attica-devel

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-3m)
- fix build with attica-0.1.4 by attica.patch imported from PLD Linux
 +Revision 1.112  2010/05/29 00:52:42  arekm
 +- rel 2; fix build with attica 0.1.4

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-2m)
- rebuild against exiv2-0.20

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-2m)
- rebuild against libjpeg-8a

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-2m)
- rebuild against redland-1.0.10

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-3m)
- rebuild against exiv2-0.19

* Wed Dec 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.85-2m)
- add attica-desktop.patch for menu of systemsettings

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.80-2m)
- add Requires: virtuoso-opensource for nepomukservices

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.3-4m)
- change BR from xine-lib to xine-lib-devel
- remove switch stable5

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-3m)
- import Patch6 from Fedora devel

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.1-2m)
- rebuild against libjpeg-7

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-2m)
- set BR: redland-devel = 1:1.0.8 for automatic building of STABLE_6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Sun Jul  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-2m)
- Requires: kdelibs-experimental-devel -> kdelibs-experimental

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rebuild with new KDE 4.2.85
- - separate kdebase-runtime-libs sub package

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sat Apr 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sat Apr 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- Requires: kdebase-runtime-flags on stable5
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2
- add flags subpkg

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- apply upstream patch

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Sun Jan 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-3m)
- adjust %%files to avoid conflicting

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-2m)
- rebuild against strigi-0.6.2
- adjust %%files

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Oct 24 2008 NAEITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sun Sep  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-2m)
- fix #460710, import patch1 from Fedora

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Thu Sep  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Wed Sep  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- update pulseaudio.patch
 +* Wed Aug 13 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.1.0-3
 +- fix PA not being default in the Xine backend (KCM part, see phonon-4.2.0-4)

- * Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.64-1m)
- - update to 4.1.64

- * Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.63-1m)
- - update to 4.1.63

- * Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.62-1m)
- - update to 4.1.62

- * Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.61-1m)
- - update to 4.1.61

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.83-2m)
- modify %%files for smart handling of a directory

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Mon May 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-3m)
- remove kmplayer.svgz to avoid conflicting with kmplayer

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-2m)
- add missing icons for menu entry
- Requires: hicolor-icon-theme

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-2m)
- import Patch1 and Patch2 from Fedora devel

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3
- import Patch0 from Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-5m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-4m)
- rebuild against OpenEXR-1.6.1

* Tue Mar 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-3m)
- add Requires: htdig for khelpcenter

* Tue Mar 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-2m)
- remove Obsoletes and Provides: kdebluetooth

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-2m)
- release _kde4_docdir/HTML/en, it's provided by kdelibs

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- KDE4
- import from Fedora devel

* Wed Dec 05 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.97.0-1
- kde-3.97.0

* Tue Dec 04 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.2-4
- disable kioslave/smb (f9+, samba-3.2.x/gplv3 ickiness)

* Sun Dec 02 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.96.2-3
- build without libxcb in F7 as we STILL don't have it (see #373361)

* Sat Dec 01 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.96.2-2
- no longer set kde3_desktop on F9
- update file list for !kde3_desktop (Sebastian Vahl)
- don't ship country flags even for full version (as in kdebase 3)

* Thu Nov 29 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.2-1
- kde-3.96.2

* Tue Nov 27 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.1-1
- kde-3.96.1

* Sun Nov 18 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.0-3
- fix %%files (unpackaged %%_libdir/strigi/strigiindex_sopranobackend.so)

* Sat Nov 17 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.0-2
- BR: clucene-core-devel libsmbclient-devel libXScrnSaver-devel

* Thu Nov 15 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.0-1
- kde-3.96.0

* Fri Nov 09 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.95.2-1
- kdebase-runtime-3.95.2

* Wed Nov 07 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.95.0-1
- kdebase-runtime-3.95.0

* Fri Nov 02 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.94.0-3
- Provides: oxygen-icon-theme ...

* Thu Oct 25 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.94.0-2
- patch dolphin.desktop to get Dolphin to start from the menu

* Fri Oct 19 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.94.0-1
- update to 3.94.0

* Thu Oct 4 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-5
- don't make this the default kdebase on F9 yet
- drop ExcludeArch: ppc64 (#300601)

* Fri Sep 21 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-4
- ExcludeArch: ppc64 (#300601)
- update description

* Thu Sep 13 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-3
- add missing BR alsa-lib-devel

* Wed Sep 12 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-2
- remove files which conflict with KDE 3
- move devel symlinks to %%{_kde4_libdir}/kde4/devel/
- Conflicts with KDE 3 versions of dolphin pre d3lphin rename

* Wed Sep 12 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.93.0-1
- update to 3.93.0
- drop kde4home patch (no longer applied)
- drop KDM ConsoleKit patch (KDM is now in kdebase-workspace)
- remove kdebase-kdm Obsoletes/Provides (for the same reason)
- remove KDM (and KDM session) setup code (for the same reason)
- remove rss-glx conflict (Plasma is now in kdebase-workspace)
- remove redhat-startkde patch (startkde is now in kdebase-workspace)
- remove kde4-opt.sh (all the code in it is commented out)
- remove kde4-xdg_menu_prefix.sh (only needed for kdebase-workspace)
- remove bogus BRs on automake and libtool
- remove workspace-only BRs
- add BR qimageblitz-devel, xine-lib-devel (all), libxcb-devel (F8+)
- remove workspace files and directories
- handle icons (moved from kdelibs4)
- add mkdir %%{buildroot} in %%install


* Mon Jul 30 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.92.0-3
- bump rss-glx Conflicts because the conflict is still there in 0.8.1.p-7.fc8
- rss-glx conflict only needed if "%%{_prefix}" == "/usr"
- consolekit_kdm patch only needs BR dbus-devel, not ConsoleKit-devel

* Mon Jul 30 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.92.0-2
- consolekit_kdm patch (#228111, kde#147790)
- update startkde patch

* Sat Jul 28 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.92.0-1
- kde-3.92 (kde-4-beta1)

* Wed Jul 25 2007 Than Ngo <than@redhat.com> - 3.91.0-6
- fix startkde
- add env/shutdown directory

* Thu Jul 19 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.91.0-5
- kde4.desktop: fix session Name

* Tue Jul 17 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.91.0-4
- cleanup/fix kde4.desktop
- kdepimlibs4->kdepimlibs

* Thu Jun 29 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.91.0-3
- fix %%_sysconfdir for %%_prefix != /usr case.

* Thu Jun 28 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.91.0-2
- updated kde4home.diff
- CMAKE_BUILD_TYPE=RelWithDebInfo (we're already using %%optflags)

* Wed Jun 27 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.91.0-1
- kde-3.91.0
- CMAKE_BUILD_TYPE=debug

* Sat Jun 23 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.90.1-2
- specfile cleanup (%%prefix issues mostly)

* Sun May 13 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.90.1-1
- update to 3.90.1
- bump cmake BR to 2.4.5 as required upstream now
- don't set execute bits by hand anymore, cmake has been fixed
- use multilibs in /opt/kde4
- add BR openssl-devel, NetworkManager-devel, bluez-libs-devel
- add explicit BRs on strigi-devel, zlib-devel, bzip2-devel, libpng-devel
  in case we want to drop the Rs on these from kdelibs4-devel
- consistently add all BRs as -devel Rs, not just almost all, until we can
  figure out which, if any, are really needed
- BR libsmbclient-devel instead of samba on F>=7, EL>=6

* Fri Mar 23 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.3-4
- restore minimum version requirement for cmake
- build against libxklavier on EL5
- don't set QT4DIR and PATH anymore, qdbuscpp2xml has been fixed

* Mon Mar 05 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.80.3-3
- +eXecute perms for %%{_prefix}/lib/*

* Fri Feb 23 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.3-2
- rebuild for patched FindKDE4Internal.cmake

* Wed Feb 21 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.3-1
- update to 3.80.3
- update and improve parallel-installability patch
- drop obsolete joydevice.h patch
- remove translations of "KDE" without the "4" from kde4.desktop
- resync BR and -devel Requires
- don't set LD_LIBRARY_PATH
- set QT4DIR and PATH so CMake's direct $QT4DIR/qdbuscpp2xml calls work
- fix missing underscore in _datadir
- install kde4.desktop in install, not prep
- fix invalid syntax in kde4.desktop

* Wed Nov 29 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.80.2-0.3.20061003svn
- dropped -DCMAKE_SKIP_RPATH=TRUE from cmake
- compiling with QA_RPATHS=0x0003; export QA_RPATHS

* Sun Nov 26 2006 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.80.2-0.2.20061003svn
- parallel build support
- added -DCMAKE_SKIP_RPATH=TRUE to cmake to skip rpath
- dropped qt4-devel >= 4.2.0, kdelibs4-devel as BR
- spec file cleanups and added clean up in %%install
- fixed PATH for libkdecore.so.5; cannot open shared object file;
- added Logitech mouse support
- added dbus-devel, hal-devel and more as BR
- fixed broken joydevice.h - Kevin Kofler
- added file kde4.desktop

* Sun Oct 08 2006 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.2-0.1.20061003svn
- first Fedora RPM (parts borrowed from the OpenSUSE kdebase 4 RPM and the Fedora kdebase 3 RPM)
- apply parallel-installability patch
* Sun Oct 08 2006 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.80.2-0.1.20061003svn
- first Fedora RPM (parts borrowed from the OpenSUSE kdebase 4 RPM and the Fedora kdebase 3 RPM)
- apply parallel-installability patch
