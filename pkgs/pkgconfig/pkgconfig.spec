%global momorel 1
%define src_name pkg-config

Summary: A tool for determining compilation options
Name: pkgconfig
Version: 0.26
Release: %{momorel}m%{?dist}
Group: Development/Tools
License: GPLv2+
URL: http://pkgconfig.freedesktop.org

# If you upgrade pkgconfig, please rebuild and check following package.
# gtk+ group(glib, atk, pango, gtk+), inkscape
Source0: http://pkgconfig.freedesktop.org/releases/%{src_name}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glibc-devel
Provides: %{_bindir}/pkg-config
Provides: pkgconfig(pkg-config) = %{version}

%description
The pkgconfig tool determines compilation options. For each required
library, it reads the configuration file and outputs the necessary
compiler and linker flags.

%prep
%setup -q -n %{src_name}-%{version}

%build
%configure --disable-static --with-pc-path=%{_libdir}/pkgconfig:%{_datadir}/pkgconfig
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%__mkdir -p %{buildroot}/usr/share/man/man1
%__cp pkg-config.1 %{buildroot}/usr/share/man/man1
%__mkdir -p %{buildroot}%{_libdir}/pkgconfig
%__mkdir -p %{buildroot}%{_datadir}/pkgconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/pkg-config
%{_libdir}/pkgconfig
%{_datadir}/pkgconfig
%{_mandir}/man1/pkg-config.1*
%{_datadir}/aclocal/pkg.m4
%{_datadir}/doc/pkg-config/pkg-config-guide.html

%changelog
* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25-2m)
- full rebuild for mo7 release

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.25-1m)
- update to 0.25
- comment out patch1

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.23-7m)
- add autoreconf. need libtool-2.2.x

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.23-6m)
- Provides: pkgconfig(pkg-config) = %%{version}

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-5m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-4m)
- Provides: /usr/bin/pkg-config
- License: GPLv2+

* Mon Jan 12 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-3m)
- import 3 patches from fc-devel

* Sat May 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.23-2m)
- add %%{_datadir}/pkgconfig

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.21-2m)
- %%NoSource -> NoSource

* Thu Sep 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Sun May  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20-1m)
- update to 0.20
-- change Source url

* Tue Nov 15 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.19-1m)
- version 0.19
- remove Patch0: pkgconfig-Xfree86_430-scapn.patch
- remove Patch1: pkg.m4.patch
- change source file name to pkg-config from pkgconfig

* Sun Jul 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.15.0.6m)
- reverse to 0.15.0

* Wed Jun 22 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.17.2-1m)
- version 0.17.2
- remove Patch0: pkgconfig-Xfree86_430-scapn.patch
- remove Patch1: pkg.m4.patch

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.0.5m)
- suppress AC_DEFUN warning.

* Fri Oct  1 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.15.0.4m)
- used %%{_libdir} for pkgconfig data directory

* Fri Oct  1 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.15.0.3m)
- do not use %%{_libdir} for pkgconfig data directory

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (0.15.0-2m)
- pretty spec file.

* Fri Mar  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.15.0-2m)
- scan /usr/X11R6/lib/pkgconfig

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.15.0-1m)
- version 0.15.0

* Wed Dec  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.14.0-1m)
- version 0.14.0

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.13.0-1m)
- version 0.13.0

* Wed Sep 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.12.0-3m)
- remove BuildPrereq: glib1-devel

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.0-2k)
- version 0.12.0

* Fri Feb 22 2002 Shingo Akagaki <dora@kondara.org>
- (0.11.0-2k)
- version 0.11.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.10.0-10k)
- change source line

* Mon Sep 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.8.0-8k)
- modify spec file for jitterbug #906

* Fri Aug 31 2001 Motonobu Ichimura <famao@kondara.org>
- add pkg.m4

* Tue Jul 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.8.0

* Tue Jul 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.6.0

* Thu Apr 26 2001 Shingo Akagaki <dora@konddara.org>
- first release
