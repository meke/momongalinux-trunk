%global momorel 12


Name: libavc1394
Version: 0.5.3
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
URL: http://sourceforge.net/projects/libavc1394/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libraw1394-devel >= 2.0.2
Requires: libraw1394 >= 2.0.2

Summary: programming interface to AV/C on FireWire

%description 
libavc1394 is a programming interface to the AV/C specification from
the 1394 Trade Assocation. AV/C stands for Audio/Video Control.  Currently,
applications use the library to control the tape transport mechansim on DV
camcorders. However, there are many devices and functions of devices that
can be controlled via AV/C. Eventually, the library will be expanded to
implement more of the specification and to provide high level interfaces
to various devices.

%package devel
Summary: Development and include files for libavc1394
Group: Development/Languages
Requires: %{name} == %{version}-%{release}

%description devel
libavc1394 is a programming interface to the AV/C specification from
the 1394 Trade Assocation. AV/C stands for Audio/Video Control.  Currently,
applications use the library to control the tape transport mechansim on DV
camcorders. However, there are many devices and functions of devices that
can be controlled via AV/C. Eventually, the library will be expanded to
implement more of the specification and to provide high level interfaces
to various devices.

This archive contains the header-files and static libraries for libraw1394 development

%prep
rm -rf %{buildroot}

%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%post
ldconfig

%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README NEWS INSTALL COPYING AUTHORS ChangeLog TODO
%{_bindir}/mkrfc2734
%{_bindir}/panelctl
%{_libdir}/libavc1394.so.*
%{_libdir}/librom1394.so.*
%attr(0755,root,root) %{_bindir}/dvcont
%{_mandir}/man1/dvcont.1*
%{_mandir}/man1/mkrfc2734.1*
%{_mandir}/man1/panelctl.1*

%files devel
%defattr(-,root,root)
%attr(0755,root,root) %dir %{_includedir}/libavc1394
%{_includedir}/libavc1394/avc1394.h
%{_includedir}/libavc1394/avc1394_vcr.h
%{_includedir}/libavc1394/rom1394.h
%{_libdir}/pkgconfig/libavc1394.pc
%{_libdir}/libavc1394.so
%{_libdir}/libavc1394.a
%{_libdir}/librom1394.so
%{_libdir}/librom1394.a

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3-9m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-6m)
- rebuild against libraw1394-2.0.2

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.5.3-5m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.3-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.3-1m)
- delete libtool library

* Sat May 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.5.3-1m)
- update to 0.5.3

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (0.5.0-1m)
- ver up.

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.4.1-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-1m)
- version up to 0.4.1
- move .so to libavc1394-devel

* Thu Mar 21 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.3.1-2k)
- write kondara spec file
