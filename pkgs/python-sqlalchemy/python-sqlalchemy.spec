%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}
%global with_python3 1

%define srcname SQLAlchemy

Name:           python-sqlalchemy
Version:        0.8.4
Release:        %{momorel}m%{?dist}
Summary:        Modular and flexible ORM library for python

Group:          Development/Libraries
License:        MIT
URL:            http://www.sqlalchemy.org/
Source0:        http://pypi.python.org/packages/source/S/%{srcname}/%{srcname}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel >= 0.6c9-2m
%if 0%{?with_python3}
BuildRequires:  python3-devel >= 3.4
BuildRequires:  python3-setuptools
BuildRequires:  python3-nose
%endif

%{?include_specopt}
%{?!do_test:  %global do_test  1}

%description
SQLAlchemy is an Object Relational Mappper (ORM) that provides a flexible,
high-level interface to SQL databases.  Database and domain concepts are
decoupled, allowing both sides maximum flexibility and power. SQLAlchemy
provides a powerful mapping layer that can work as automatically or as manually
as you choose, determining relationships based on foreign keys or letting you
define the join conditions explicitly, to bridge the gap between database and
domain.

%if 0%{?with_python3}
%package -n python3-sqlalchemy
Summary:        Modular and flexible ORM library for python
Group:          Development/Libraries

%description -n python3-sqlalchemy
SQLAlchemy is an Object Relational Mappper (ORM) that provides a flexible,
high-level interface to SQL databases.  Database and domain concepts are
decoupled, allowing both sides maximum flexibility and power. SQLAlchemy
provides a powerful mapping layer that can work as automatically or as manually
as you choose, determining relationships based on foreign keys or letting you
define the join conditions explicitly, to bridge the gap between database and
domain.

This package includes the python 3 version of the module.
%endif # with_python3

# Filter unnecessary dependencies
%{?filter_setup:
%filter_provides_in %{python_sitearch}.*\.so$
%filter_provides_in %{python3_sitearch}.*\.so$
%filter_setup
}

%prep
%setup -q -n %{srcname}-%{version}

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif # with_python3

%build
CFLAGS="%{optflags}" %{__python} setup.py --with-cextensions build

%if 0%{?with_python3}
pushd %{py3dir}
# Convert tests and examples to python3
%{__python3} sa2to3.py --no-diffs -w test examples
# Currently the cextension doesn't work with python3
CFLAGS="%{optflags}" %{__python3} setup.py build
popd
%endif

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{python_sitelib}
%{__python} setup.py --with-cextensions install --skip-build --root %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
mkdir -p %{buildroot}%{python3_sitelib}
# Currently the cextension doesn't work with python3
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif

# remove unnecessary scripts for building documentation
rm -rf doc/build

%clean
rm -rf $RPM_BUILD_ROOT

%if 0%{do_test}
#%%check
#%%{__python} ./sqla_nose.py

#%%if 0%{?with_python3}
#pushd %{py3dir}
#%%{__python3} ./sqla_nose.py
#popd
#%%endif
%endif

%files
%defattr(-,root,root,-)
%doc README.rst LICENSE PKG-INFO CHANGES doc examples
%{python_sitearch}/*

%if 0%{?with_python3}
%files -n python3-sqlalchemy
%defattr(-,root,root,-)
%doc LICENSE PKG-INFO doc examples
%{python3_sitelib}/*
%endif # with_python3

%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-1m)
- update 0.8.4

* Thu May  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.6-1m)
- [SECURITY] CVE-2012-0805
- update to 0.7.6

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.8-1m)
- update 0.6.8

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.7-1m)
- update 0.6.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-2m)
- rebuild for new GCC 4.6

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.8-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.8-4m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.8-3m)
- version down 0.5.8

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.3-1m)
- update to 0.6.3

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.8-2m)
- disable test_join_cache because it seems python-sqlite2 is a bit newer
- modify the thresholds of test_first_connect and test_second_connect

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-1m)
- update to 0.5.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4p2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4p2-1m)
- update to 0.5.4p2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-0.1.2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-0.1.1m)
- update to version 0.5.0rc4
- import sqlalchemy-sqlite-unicode-test.patch from Fedora

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.7-1m)
- version 0.4.7
- fix up testtypes.py

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.3-1m)
- import from Fedora

* Fri Feb 22 2008 Toshio Kuratomi <toshoi@fedoraproject.org> 0.4.3-1
- Update to 0.4.3.

* Tue Dec 11 2007 Toshio Kuratomi <a.badger@gmail.com> 0.4.2-1.p3
- Update to 0.4.2p3.

* Tue Dec 11 2007 Toshio Kuratomi <a.badger@gmail.com> 0.4.1-1
- Update to 0.4.1.

* Wed Oct 17 2007 Toshio Kuratomi <a.badger@gmail.com> 0.4.0-1
- SQLAlchemy-0.4.0 final
- Run the testsuite

* Wed Oct  3 2007 Luke Macken <lmacken@redhat.com> 0.4.0-0.4.beta6
- SQLAlchemy-0.4.0beta6

* Tue Sep 11 2007 Toshio Kuratomi <a.badger@gmail.com> - 0.4.0-0.4.beta5
- Update to 0.4beta5.

* Fri Sep 06 2007 Toshio Kuratomi <a.badger@gmail.com> - 0.4.0-0.4.beta4
- setuptools has been fixed.

* Fri Aug 31 2007 Toshio Kuratomi <a.badger@gmail.com> - 0.4.0-0.3.beta4
- setuptools seems to be broken WRT having an active and inactive version
  of an egg.  Have to make both versions inactive and manually setup a copy
  that can be started via import. (Necessary for the sqlalchemy0.3 compat
  package.)

* Tue Aug 28 2007 Toshio Kuratomi <a.badger@gmail.com> - 0.4.0-0.2.beta4
- Modify setuptools to handle the -devel subpackage split in F-8.

* Mon Aug 27 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.4.0-0.1.beta4
- Update to 0.4 beta4.

* Tue Jul 24 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.10-2
- Remove python-abi Requires.  This is automatic since FC4+.

* Tue Jul 24 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.10-1
- Update to new upstream version 0.3.10

* Fri Mar 23 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.6-1
- Update to new upstream version 0.3.6

* Sat Mar 10 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.5-1
- Update to new upstream version 0.3.5
- Simplify the files listing

* Tue Jan 23 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.4-2
- Remember to upload the source tarball to the lookaside cache.

* Tue Jan 23 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.4-1
- Update to new upstream version 0.3.4

* Mon Jan 01 2007 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.3-1
- Update to new upstream version 0.3.3

* Sat Dec 09 2006 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.1-2
- Bump and rebuild for python 2.5 on devel.
- BuildRequire: python-devel as a header is missing otherwise.

* Fri Nov 24 2006 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.3.1-1
- Update to new upstream version 0.3.1

* Sat Sep 16 2006 Shahms E. King <shahms@shahms.com> 0.2.7-2
- Rebuild for FC6

* Thu Aug 17 2006 Shahms E. King <shahms@shahms.com> 0.2.7-1
- Update to new upstream version

* Fri Aug 11 2006 Shahms E. King <shahms@shahms.com> 0.2.6-2
- Include, don't ghost .pyo files per new guidelines

* Tue Aug 08 2006 Shahms E. King <shahms@shahms.com> 0.2.6-1
- Update to new upstream version

* Fri Jul 07 2006 Shahms E. King <shahms@shahms.com> 0.2.4-1
- Update to new upstream version

* Mon Jun 26 2006 Shahms E. King <shahms@shahms.com> 0.2.3-1
- Update to new upstream version

* Wed May 31 2006 Shahms E. King <shahms@shahms.com> 0.2.1-1
- Update to new upstream version
