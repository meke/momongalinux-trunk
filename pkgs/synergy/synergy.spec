%global momorel 11

Summary:   Mouse and keyboard sharing utility
Name:      synergy
Version:   1.3.1
Release: %{momorel}m%{?dist}
License:   GPLv2
URL:       http://synergy2.sourceforge.net/
Group:     System Environment/Daemons
Source0: http://dl.sourceforge.net/sourceforge/synergy2/synergy-1.3.1.tar.gz 
NoSource: 0
Patch0: synergy-1.3.1-gcc43.patch
Patch1: synergy-1.3.1-warnings.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Synergy lets you easily share a single mouse and keyboard between
multiple computers with different operating systems, each with its
own display, without special hardware.  It is intended for users
with multiple computers on their desk since each system uses its
own display.

%prep
%setup -q
%patch0 -p1 -b .gcc43
%patch1 -p1 -b .glibc210
%configure --program-prefix=""

%build
%make

%install
%makeinstall
strip %{buildroot}/usr/bin/synergyc
strip %{buildroot}/usr/bin/synergys

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_bindir}/synergyc
%{_bindir}/synergys
%doc AUTHORS
%doc COPYING
%doc ChangeLog
%doc INSTALL
%doc NEWS
%doc README
%doc doc/about.html
%doc doc/authors.html
%doc doc/autostart.html
%doc doc/banner.html
%doc doc/border.html
%doc doc/compiling.html
%doc doc/configuration.html
%doc doc/contact.html
%doc doc/developer.html
%doc doc/faq.html
%doc doc/history.html
%doc doc/home.html
%doc doc/index.html
%doc doc/license.html
%doc doc/news.html
%doc doc/roadmap.html
%doc doc/running.html
%doc doc/security.html
%doc doc/tips.html
%doc doc/toc.html
%doc doc/trouble.html
%doc doc/synergy.css
%doc examples/synergy.conf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-7m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-5m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-4m)
- rebuild against gcc43

* Sat Feb 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-3m)
- support gcc-4.3
 
* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- %%NoSource -> NoSource
 
* Thu Feb 22 2007 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.3.1-1m)
- initial build
