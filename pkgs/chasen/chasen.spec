%global momorel 1

Summary: Japanese Morphological Analysis System, ChaSen
Name: chasen
Version: 2.4.4
Release: %{momorel}m%{?dist}
Source0: http://dl.sourceforge.jp/chasen-legacy/32224/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-2.4.2-nmz.patch
Patch10: http://dl.sourceforge.jp/chasen-legacy/54111/chasen244-secfix.diff
#Copyright: 1999 Nara Institute of Science and Technology, Japan.
License: see "COPYING"
URL: http://chasen-legacy.sourceforge.jp/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group: Applications/Text
Requires: ipadic
BuildRequires: darts >= 0.3
BuildRequires: gcc-c++ >= 3.4.1-1m

%description
Japanese Morphological Analysis System

%package devel
Summary: Libraries and header files for ChaSen developers
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
Libraries and header files for ChaSen developers.

%prep
%setup -q
%patch0 -p1
%patch10 -p1 -b .CVE-2011-4000

%build
export CFLAGS="%{optflags}"
libtoolize -c -f
aclocal
autoconf
%configure --with-darts=%{_prefix}
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS* README*
%doc doc/*.tex doc/*.pdf
%{_bindir}/chasen
%{_libdir}/*.so.*
# %{_datadir}/%{name}
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/makeda
%{_libexecdir}/%{name}/makemat

%files devel
%defattr(-,root,root)
%{_bindir}/chasen-config
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/*.a

%changelog
* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- [SECURITY] CVE-2011-4000
- update to 2.4.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-4m)
- rebuild against rpm-4.6

* Fri May  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.2-3m)
- apply darts032.patch
- http://lists.macosforge.org/pipermail/macports-changes/2008-April/016691.html

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.2-2m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2
- update Patch0 and remove unused patches

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.3-6m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.3-5m)
- delete libtool library

* Sat Jan  7 2006 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.3-4m)
- rebuild against darts-0.3

* Wed Sep  1 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.3.3-3m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++
- add gcc34 patch

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (2.3.3-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 25 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.3.3-1m)

* Tue Feb 11 2003 zunda <zunda@freeshell.org>
- (2.2.9-4m)
- apply chasen-2.2.8-nmz.diff to extract URLs and e-mail addresses

* Fri Nov 15 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.9-3m)
- modify spec file

* Fri Feb  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.2.9-2k)

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.2.8-4k)
- nigittenu

* Sat Sep  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.2.8-2k)

* Mon Jun 25 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.2.7-3k)

* Thu Mar 22 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.2.4-3k)

* Mon Mar  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.2.3-3k)

* Fri Dec 15 2000 Kazuhiko <kazuhiko@kondara.org>
- (2.2-3k)
- move ipadic to another package

* Sun Dec 10 2000 Kazuhiko <kazuhiko@kondara.org>
- (2.02_2.1-5k)
- move perl-module to another package

* Sat Nov  4 2000 AYUHANA Tomonori <l@kondara.org>
- (2.02_2.1-1k)
- 1st release.
- SPEC fixed ( %description )
- add patch0 for make error
