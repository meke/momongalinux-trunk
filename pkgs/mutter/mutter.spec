%global        momorel 1
Name:          mutter
Version:       3.6.2
Release:       %{momorel}m%{?dist}
Summary:       Window and compositing manager based on Clutter

Group:         User Interface/Desktops
License:       GPLv2+
#VCS:	       git:git://git.gnome.org/mutter
Source0:       http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource:      0
#Patch1:	       mutter-upstream.patch

BuildRequires: clutter-devel >= 1.11.14
BuildRequires: pango-devel
BuildRequires: startup-notification-devel
BuildRequires: gtk3-devel >= 3.6.0
BuildRequires: pkgconfig
BuildRequires: gobject-introspection-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXdamage-devel
BuildRequires: libXext-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libXcursor-devel
BuildRequires: libXcomposite-devel
BuildRequires: zenity
BuildRequires: gnome-doc-utils
BuildRequires: desktop-file-utils
# Bootstrap requirements
BuildRequires: gtk-doc intltool
BuildRequires: gnome-common >= 3.5.91
BuildRequires: libcanberra-devel
BuildRequires: gsettings-desktop-schemas-devel >= 3.6.0

# Make sure this can't be installed with an old gnome-shell build because of
# an ABI change in mutter 3.4.1 / gnome-shell 3.4.1
Conflicts: gnome-shell < 3.4.1

Requires: control-center-filesystem
Requires: startup-notification
Requires: dbus-x11
Requires: zenity

%description
Mutter is a window and compositing manager that displays and manages
your desktop via OpenGL. Mutter combines a sophisticated display engine
using the Clutter toolkit with solid window-management logic inherited
from the Metacity window manager.

While Mutter can be used stand-alone, it is primarily intended to be
used as the display core of a larger system such as gnome-shell or
Moblin. For this reason, Mutter is very extensible via plugins, which
are used both to add fancy visual effects and to rework the window
management behaviors to meet the needs of the environment.

%package devel
Summary: Development package for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Header files and libraries for developing Mutter plugins. Also includes
utilities for testing Metacity/Mutter themes.

%prep
%setup -q
#patch1 -p1 -b .upstream~

%build
(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; fi;
 %configure --disable-static --enable-compile-warnings=maximum)

SHOULD_HAVE_DEFINED="HAVE_SM HAVE_SHAPE HAVE_RANDR HAVE_STARTUP_NOTIFICATION"

for I in $SHOULD_HAVE_DEFINED; do
  if ! grep -q "define $I" config.h; then
    echo "$I was not defined in config.h"
    grep "$I" config.h
    exit 1
  else
    echo "$I was defined as it should have been"
    grep "$I" config.h
  fi
done

make %{?_smp_mflags} V=1

%install
make install DESTDIR=$RPM_BUILD_ROOT

#Remove libtool archives.
rm -rf %{buildroot}/%{_libdir}/*.la

%find_lang %{name}

# Mutter contains a .desktop file so we just need to validate it
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop

%post -p /sbin/ldconfig

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files -f %{name}.lang
%doc README AUTHORS COPYING NEWS HACKING doc/theme-format.txt
%doc %{_mandir}/man1/mutter.1.*
%doc %{_mandir}/man1/mutter-message.1.*
%{_bindir}/mutter
%{_bindir}/mutter-message
%{_datadir}/applications/*.desktop
%{_datadir}/gnome/wm-properties/mutter-wm.desktop
%{_datadir}/mutter
%{_libdir}/lib*.so.*
%{_libdir}/mutter/
%{_datadir}/GConf/gsettings/mutter-schemas.convert
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.gschema.xml
%{_datadir}/gnome-control-center/keybindings/50-mutter-windows.xml


%files devel
%{_bindir}/mutter-theme-viewer
%{_bindir}/mutter-window-demo
%{_includedir}/*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%doc %{_mandir}/man1/mutter-theme-viewer.1.*
%doc %{_mandir}/man1/mutter-window-demo.1.*

%changelog
* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Wed Aug 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- reimport from fedora

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-1m)
- update 3.2.2

* Fri Oct 28 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1-2m)
- require adjustment

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91.1-2m)
- rebuild against cogl-1.8.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91.1-1m)
- update to 3.1.91.1

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90.1-1m)
- update to 3.1.90.1

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2.1-1m)
- update to 3.0.2.1

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31.4-2m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.4-1m)
- update to 2.31.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.31.2-2m)
- full rebuild for mo7 release

* Thu May 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.31.2-1m)
- update to 2.31.2

* Fri Mar 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.1-1m)
- update to 2.29.1

* Mon Mar 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.0-2m)
- add patch0 for clutter-1.2.0

* Sat Feb 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.0-1m)
- update to 2.29.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Wed Sep 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.5-1m)
- update to 2.27.5

* Sun Sep  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.4-1m)
- update to 2.27.4

* Thu Sep  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.27.3-3m)
- rebuild against clutter-1.0.4-3m

* Wed Sep  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.3-2m)
- to main

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.27.3-1m)
- initial build
