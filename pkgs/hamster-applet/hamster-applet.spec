%global momorel 2
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Hamster GNOME applet
Name: hamster-applet

Version: 2.91.2
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.91/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: python >= 2.7
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: pygtk2-devel >= 2.14.1
BuildRequires: pygobject-devel >= 2.16.1
BuildRequires: gnome-python2-devel >= 2.26.0
BuildRequires: gnome-control-center-devel >= 3.0
BuildRequires: rarian

%description
This applet allows you to keep track on where are you spending your time.

%prep
%setup -q

%build
CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./waf -j1 configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--nocache 

./waf -j1 build -v

%install
rm -rf --preserve-root %{buildroot}
./waf -j1 --destdir=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
    gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/hamster-applet.schemas \
    > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
      %{_sysconfdir}/gconf/schemas/hamster-applet.schemas \
      > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/hamster-applet.schemas \
	> /dev/null || :
fi

%postun
/sbin/ldconfig
gtk-update-icon-cache -q -t -f %{_datadir}/icons/hicolor
rarian-sk-update

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING NEWS README TODO
%{_sysconfdir}/gconf/schemas/hamster-applet.schemas
%{_bindir}/gnome-time-tracker
%{_bindir}/hamster-cli
%{_bindir}/hamster-service
%{_bindir}/hamster-time-tracker
%{_libdir}/%{name}
%{_libdir}/bonobo/servers/Hamster_Applet.server
%{python_sitelib}/hamster
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/gnome/help/%{name}
%{_datadir}/applications/hamster-applet.desktop
%{_datadir}/applications/hamster-time-tracker.desktop
%{_datadir}/dbus-1/services/org.gnome.hamster.service
%{_datadir}/dockmanager
%{_datadir}/docky
%{_datadir}/gnome-control-center/keybindings/90-hamster-applet.xml

%changelog
* Tue Jun 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.91.2-2m)
- add -j1 to avoid a possible dead lock with python-2.7.2

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.91.2-1m)
- update to 2.91.2

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sun Oct  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.32.0-3m)
- enable parallel build and optflags

* Sun Oct  3 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.32.0-2m)
- revised waf option and added python_sitelib macro for %%files

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-3m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Mon Sep 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0.1-1m)
- initial build
