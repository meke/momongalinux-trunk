%global momorel 1
%define pkgname xclipboard
Summary: X.Org X11 %{pkgname}
Name: xorg-x11-%{pkgname}
Version: 1.1.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: libXt-devel

%description
%{pkgname}

%prep
%setup -q -n %{pkgname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{_bindir}/%{pkgname}
%{_bindir}/xcutsel
%{_datadir}/X11/app-defaults/XClipboard
%{_mandir}/man1/%{pkgname}.1.*
%{_mandir}/man1/xcutsel.1.*

%changelog
* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Wed Nov  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- %%NoSource -> NoSource

* Thu Nov  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1m)
- sepalated from xorg-x11-apps
