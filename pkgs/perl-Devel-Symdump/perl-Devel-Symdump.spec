%global         momorel 1

Name:           perl-Devel-Symdump
Version:        2.12
Release:        %{momorel}m%{?dist}
Summary:        Dump symbol names or the symbol table
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Devel-Symdump/
Source0:        http://www.cpan.org/authors/id/A/AN/ANDK/Devel-Symdump-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.004
BuildRequires:  perl-Compress-Zlib
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl-Compress-Zlib
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This little package serves to access the symbol table of perl.

%prep
%setup -q -n Devel-Symdump-%{version}

%build
CFLAGS="%optflags" %{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes META.json README
%{perl_vendorlib}/Devel/Symdump.pm
%{perl_vendorlib}/Devel/Symdump
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-1m)
- rebuild against perl-5.20.0
- update to 2.12

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-2m)
- rebuild against perl-5.18.2

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-2m)
- rebuild against perl-5.18.0

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.08-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.08-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.08-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.08-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.08-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.08-2m)
- rebuild against gcc43

* Thu Oct 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-1m)
- update to 2.08

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.07-2m)
- use vendor

* Fri Jan  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0604-1m)
- update to 2.0604

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0603-1m)
- update to 2.0603

* Sat Jul 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0602-1m)
- update to 2.0602

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0601-1m)
- update to 2.0601

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.04-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.04-1m)
- update to 2.04

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.03-11m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.03-10m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.03-9m)
- remove Epoch from BuildRequires

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (2.03-8m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.03-7m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.03-6m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.03-5m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.03-4m)
- rebuild against perl-5.8.0

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.03-3m)
- remove BuildRequires: gcc2.95.3

* Thu Feb 28 2002 Junichiro Kita<kita@kondara.org>
- (2.03-2k)
- ver up

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (2.01-2k)
- create
