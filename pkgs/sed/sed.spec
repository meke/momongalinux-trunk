%global momorel 1

%ifos linux
%define _bindir /bin
%endif

Summary: A GNU stream text editor
Name: sed
Version: 4.2.2
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Text
URL: http://sed.sourceforge.net/
Source0: ftp://ftp.gnu.org/pub/gnu/sed/sed-%{version}.tar.bz2
NoSource: 0
Source1: http://sed.sourceforge.net/sedfaq.txt
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: /bin/sed
BuildRequires: glibc-devel, libselinux-devel
Requires(post): info
Requires(preun): info

%description
The sed (Stream EDitor) editor is a stream or batch (non-interactive)
editor.  Sed takes text as input, performs an operation or set of
operations on the text and outputs the modified text.  The operations
that sed performs (substitutions, deletions, insertions, etc.) can be
specified in a script file or from the command line.

%prep
%setup -q

%build
%configure --without-included-regex
make %{_smp_mflags}
install -m 644 -p %{SOURCE1} sedfaq.txt
gzip -9 sedfaq.txt

%check
echo ====================TESTING=========================
make check
echo ====================TESTING END=====================

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
rm -f %{buildroot}/%{_infodir}/dir

%find_lang %{name}

%post
/sbin/install-info %{_infodir}/sed.info %{_infodir}/dir || &> /dev/null
:

%preun
if [ $1 = 0 ]; then
   /sbin/install-info --delete %{_infodir}/sed.info %{_infodir}/dir || &> /dev/null
fi
:

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc BUGS NEWS THANKS README AUTHORS sedfaq.txt.gz COPYING COPYING.DOC
%{_bindir}/sed 
%{_infodir}/*.info*
%{_mandir}/man*/*

%changelog
* Tue Sep 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.2-1m)
- update 4.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.1-5m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2.1-4m)
- import patch from Fedora13

* Mon Mar  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.1-3m)
- rebuild against glibc-2.11.90 and libselinx-2.0.91

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1
-- import Patch0 from Rawhide (4.2.1-3), which add dummy -c/--copy
   param to keep backward compatibility

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.5-2m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.5-2m)
- %%NoSource -> NoSource

* Sun May 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.1.5-1m)
- update to 4.1.5

* Tue Dec 27 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (4.1.4-2m)
- sync with fc 

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.1.4-1m)
  update to 4.1.4

* Thu Aug 19 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.1.1-1m)
- updated to 4.1.1
- added a patch to fix a problem with the y command.

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (4.0.7-2m)
- revised spec for rpm 4.2.

* Fri Aug 22 2003 Hiroyuki KOGA <kuma@momonga-linux.org>
- (4.0.7-1m)
- version 4.0.7

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.0.2-20k)
- Provides: /bin/sed

* Fri Jan 04 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (3.0.2-18k)
- Forgotton release increment.

* Thu Jan 03 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- Fixed Source0 path.

* Thu Oct 25 2001 Motonobu Ichimura <famao@kondara.org>
- (3.0.2-16k)
- change i18n patch to IBM's one

* Fri Oct 12 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Mon Jun 12 2000 Akira Higuchi <a@kondara.org>
- ceased to use multibyte patch. wrote kondara patch.

* Tue Jun 6 2000 Akira Higuchi <a@kondara.org>
- s/re_max_failures/re_max_failures_/g

* Mon Jun 5 2000 Toru Hoshina <t@kondara.org>
- force -O0 for Alpha platform to avoid SEGV :-P

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_sed-20000115

* Thu Nov 25 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_sed-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Tue Aug 18 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.02

* Sun Jul 26 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.01

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- removed references to the -g option from the man page that we add

* Fri Oct 17 1997 Donnie Barnes <djb@redhat.com>
- spec file cleanups
- added BuildRoot

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
