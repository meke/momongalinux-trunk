%global momorel 1
%global srcname openexr_viewers

Summary: Viewers programs for OpenEXR
Name: OpenEXR_Viewers
Version: 1.0.2
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/Multimedia
URL: http://www.openexr.com/
Source0: https://github.com/downloads/openexr/openexr/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: OpenEXR_CTL >= 1.0.1-5m
BuildRequires: OpenEXR_CTL-devel >= 1.0.1-5m
BuildRequires: fltk-devel >= 1.3.2
BuildRequires: freeglut-devel
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: pkgconfig
BuildRequires: sed

%description
exrdisplay is a simple still image viewer that optionally applies color
transforms to OpenEXR images, using ctl as explained in this document:
doc/OpenEXRViewers.pdf

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure LIBS="-lGL"

# missing libs for playexr
sed -i -e 's|LIBS =|LIBS = -lglut|' playexr/Makefile

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# clean up
rm -rf %{buildroot}%{_includedir}/OpenEXR

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL LICENSE NEWS README*
%{_bindir}/exrdisplay

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Mon Dec 31 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-5m)
- rebuild against fltk-1.3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- initial package for Momonga Linux 7
- import 2 patches from Fedora
