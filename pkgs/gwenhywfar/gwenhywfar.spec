%global momorel 2

Summary: A multi-platform helper library for other libraries
Name: gwenhywfar
Version: 4.3.3
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://sourceforge.net/projects/gwenhywfar/
Group: System Environment/Libraries
Source0: %{name}-%{version}.tar.gz 
Patch0: gwenhywfar-3.4.1-pkgconfig.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: gettext 
BuildRequires: libgcrypt-devel 
BuildRequires: openssl-devel >= 1.0.0 
BuildRequires: pkgconfig
BuildRequires: gtk2-devel
BuildRequires: qt-devel

%description
This is Gwenhywfar, a multi-platform helper library for networking and
security applications and libraries. It is heavily used by libchipcard
and AqBanking/AqHBCI, the German online banking libraries.

%package devel
Summary: Header files and static libraries from gwenhywfar
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on gwenhywfar.

%package gui-gtk2
Summary: Gwenhywfar GUI framework for GTK2
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description gui-gtk2
This package contains the gtk2 gwenhywfar GUI backend.

%package gui-qt4
Summary: Gwenhywfar GUI framework for Qt4
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description gui-qt4
This package contains the qt4 gwenhywfar GUI backend.

%prep
%setup -q
%patch0 -p1 -b .pkgconfig

%build
%configure \
	--disable-static \
	--with-openssl-libs=%{_libdir} \
	--with-qt4-libs=%{_qt4_libdir} \
	--with-qt4-includes=%{_qt4_headerdir}
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
find %{buildroot} -name *.la -exec rm -f {} \;

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post gui-gtk2 -p /sbin/ldconfig

%postun gui-gtk2 -p /sbin/ldconfig

%post gui-qt4 -p /sbin/ldconfig

%postun gui-qt4 -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS README COPYING ChangeLog
%{_bindir}/gct-tool
%{_libdir}/*.so.*
%{_libdir}/%{name}
%{_datadir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%exclude %{_datadir}/%{name}/typemaker*
%exclude %{_libdir}/*gui-*.so.*

%files devel
%defattr(-,root,root)
%exclude %{_bindir}/gct-tool
%{_bindir}/*
%{_includedir}/gwenhywfar4
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_datadir}/aclocal/gwenhywfar.m4
%{_datadir}/%{name}/typemaker*

%files gui-gtk2
%defattr(-,root,root)
%{_libdir}/libgwengui-gtk2.so.*

%files gui-qt4
%defattr(-,root,root)
%{_libdir}/libgwengui-qt4.so.*

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gnutls-3.2.0

* Thu Sep 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- update to 4.3.3

* Tue Jan  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to 4.3.1
- add REMOVE.PLEASE

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.3-4m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.11.3-3m)
- version down 

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0.2-1m)
- update to 4.0.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.11.3-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11.3-1m)
- update to 3.11.3

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.2-4m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.8.2-2m)
- own %%{_datadir}/gwenhywfar

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.2-1m)
- update to 3.8.2
-- build with --disable-static

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-4m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.2-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.2-2m)
- rebuild against gcc43

* Mon Mar 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.2-1m)
- version 2.6.2

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.1-3m)
- %%NoSource -> NoSource

* Fri Jul 27 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.1-2m)
- with --with-openssl-libs option for x86_64

* Thu Jul 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-1m)
- initial package for aqbanking-2.3.2

