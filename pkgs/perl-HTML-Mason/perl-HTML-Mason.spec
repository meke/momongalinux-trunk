%global         momorel 2

Name:           perl-HTML-Mason
Version:        1.54
Release:        %{momorel}m%{?dist}
Summary:        HTML::Mason Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/HTML-Mason/
Source0:        http://www.cpan.org/authors/id/J/JS/JSWARTZ/HTML-Mason-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Cache-Cache >= 1
BuildRequires:  perl-CGI >= 2.46
BuildRequires:  perl-Class-Container >= 0.07
BuildRequires:  perl-Cwd >= 0.8
BuildRequires:  perl-Exception-Class >= 1.15
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-List-Util >= 1.01
BuildRequires:  perl-Log-Any >= 0.08
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Params-Validate >= 0.7
BuildRequires:  perl-Test
BuildRequires:  perl-Test-Deep
BuildRequires:  perl-Test-Simple
Requires:       perl-Cache-Cache >= 1
Requires:       perl-CGI >= 2.46
Requires:       perl-Class-Container >= 0.07
Requires:       perl-Cwd >= 0.8
Requires:       perl-Exception-Class >= 1.15
Requires:       perl-HTML-Parser
Requires:       perl-List-Util >= 1.01
Requires:       perl-Log-Any >= 0.08
Requires:       perl-Params-Validate >= 0.7
Requires:       perl-Test-Deep
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Mason is a tool for building, serving and managing large web sites. Its
features make it an ideal backend for high load sites serving dynamic
content, such as online newspapers or database driven e-commerce sites.

%prep
%setup -q -n HTML-Mason-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes CREDITS LICENSE README UPGRADE
%{_bindir}/*
%{perl_vendorlib}/HTML/Mason*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-1m)
- update to 1.54
- rebuild against perl-5.18.2

* Wed Oct  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-2m)
- rebuild against perl-5.18.0

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-2m)
- rebuild against perl-5.16.1

* Thu Jul 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-1m)
- update to 1.50

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-1m)
- update to 1.48

* Sat Oct 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.47-1m)
- update to 1,47

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-1m)
- update to 1.46

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.45-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.45-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-2m)
- rebuild against perl-5.12.0

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-1m)
- update to 1.45

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.43-1m)
- update to 1.43

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.42-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-2m)
- rebuild against perl-5.10.1

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Wed May  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- update to 1.41

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.40-2m)
- rebuild against rpm-4.6

* Fri Jul 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.39-2m)
- rebuild against gcc43

* Fri Feb  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-1m)
- update to 1.39

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Sun Sep  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37

* Tue Jul 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.35-2m)
- use vendor

* Sun Apr 08 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- Specfile autogenerated by cpanspec 1.69.1 for Momonga Linux.
