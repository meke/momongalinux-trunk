%global momorel 3

Summary:        Clean up and pretty-print HTML/XHTML/XML
Name:           tidyp
Version:        1.04
Release:        %{momorel}m%{?dist}
License:        BSD
Group:          Applications/Text
URL:            http://github.com/petdance/tidyp
Source0:        http://github.com/downloads/petdance/%{name}/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libxslt
Requires:       libtidyp = %{version}-%{release}
Obsoletes:      libtidyp < %{version}-%{release}

%description
tidyp is a fork of tidy on SourceForge. The library name is "tidyp", and the
command-line tool is also "tidyp" but all internal API stays the same.

%package -n libtidyp
Summary:        Shared libraries for tidyp
Group:          System Environment/Libraries

%description -n libtidyp
Shared libraries for tidyp.

%package -n libtidyp-devel
Summary:        Development files for libtidyp
Group:          Development/Libraries
Requires:       libtidyp = %{version}-%{release}

%description -n libtidyp-devel
Development files for libtidyp.

%prep
%setup -q

# Fix permissions for debuginfo
chmod -x src/{mappedio.*,version.h}

# Fix timestamp order to avoid trying to re-run autotools
touch aclocal.m4
find . -name Makefile.in -exec /bin/touch {} \;
touch configure

%build
%configure --disable-static --disable-dependency-tracking
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name *.la -delete

%check
#make check

%clean
rm -rf %{buildroot}

%post -n libtidyp -p /sbin/ldconfig

%postun -n libtidyp -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ChangeLog INSTALL README
%{_bindir}/tidyp

%files -n libtidyp
%defattr(-,root,root,-)
%{_libdir}/libtidyp-%{version}.so.0*

%files -n libtidyp-devel
%defattr(-,root,root,-)
%{_includedir}/tidyp/
%{_libdir}/libtidyp.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.04-2m)
- rebuild for new GCC 4.5

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04
- disable test

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.02-2m)
- full rebuild for mo7 release

* Thu May 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- initial build for Momonga Linux
