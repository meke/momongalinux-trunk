%global momorel 6

%define fontname roadstencil
%define fontconf 63-%{fontname}.conf

Name:           %{fontname}-fonts
Version:        1.0
Release:        %{momorel}m%{?dist}
Summary:        Roadstencil Fonts

Group:          User Interface/X
License:        OFL
URL:            http://openfontlibrary.org/media/files/andyfitz/237
Source0:        http://openfontlibrary.org/people/andyfitz/andyfitz_-_roadstencil.otf
Source1:        %{fontname}-fontconfig.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
A rough font influenced by roadwork stencils

%prep

%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p %{SOURCE0} %{buildroot}%{_fontdir}/roadstencil.otf

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.otf

%doc

%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Sun Mar 8 2009 Jon Stanley <jonstanley@gmail.com> - 1.0-7
- Update for new guidelines

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jul 12 2008 Jon Stanley <jonstanley@gmail.com> - 1.0-5
- Fix the fontconfig file

* Sat Jul 12 2008 Jon Stanley <jonstanley@gmail.com> - 1.0-4
- fix build failure from previous

* Sat Jul 12 2008 Jon Stanley <jonstanley@gmail.com> - 1.0-3
- Add fontconfig file (#445136)

* Sun May 04 2008 Jon Stanley <jonstanley@gmail.com> - 1.0-2
- Correct source

* Sun May 04 2008 Jon Stanley <jonstanley@gmail.com> - 1.0-1
- Initial package

