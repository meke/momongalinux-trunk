%global         momorel 1

Name:           bochs
Version:        2.6
Release:        %{momorel}m%{?dist}
Summary:        Portable x86 PC emulator
Group:          Applications/Emulators
License:        LGPLv2+
URL:            http://bochs.sourceforge.net/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
NoSource: 	0
Patch0: %{name}-0001_bx-qemu.patch
Patch1: %{name}-0006_qemu-bios-use-preprocessor-for-pci-link-routing.patch
Patch2: %{name}-0007_bios-add-26-pci-slots,-bringing-the-total-to-32.patch
Patch3: %{name}-0008_qemu-bios-provide-gpe-_l0x-methods.patch
Patch4: %{name}-0009_qemu-bios-pci-hotplug-support.patch
Patch7: %{name}-nonet-build.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libXt-devel libXpm-devel SDL-devel readline-devel byacc
BuildRequires:  docbook-utils
BuildRequires:  gtk2-devel
#BuildRequires:  wxGTK-devel
%ifarch %{ix86} x86_64
BuildRequires:  svgalib-devel
BuildRequires:  dev86 iasl
%endif
Requires:       %{name}-bios = %{version}-%{release}
Requires:       vgabios

%description
Bochs is a portable x86 PC emulation software package that emulates
enough of the x86 CPU, related AT hardware, and BIOS to run DOS,
Windows '95, Minix 2.0, and other OS's, all on your workstation.


%package        debugger
Summary:        Bochs with builtin debugger
Group:          Applications/Emulators
Requires:       %{name} = %{version}-%{release}

%description    debugger
Special version of bochs compiled with the builtin debugger.


%package        gdb
Summary:        Bochs with support for debugging with gdb
Group:          Applications/Emulators
Requires:       %{name} = %{version}-%{release}

%description    gdb
Special version of bochs compiled with a gdb stub so that the software running
inside the emulator can be debugged with gdb.

%ifarch %{ix86} x86_64
# building firmwares are quite tricky, because they often have to be built on
# their native architecture (or in a cross-capable compiler, that we lack in
# koji), and deployed everywhere. Recent koji builders support a feature
# that allow us to build packages in a single architecture, and create noarch
# subpackages that will be deployed everywhere. Because the package can only
# be built in certain architectures, the main package has to use
# BuildArch: <nativearch>, or something like that.
# Note that using ExclusiveArch is _wrong_, because it will prevent the noarch
# packages from getting into the excluded repositories.
%package	bios
Summary:        Bochs bios
Group:          Applications/Emulators
BuildArch:      noarch
Provides:       bochs-bios-data = 2.3.8.1
Obsoletes:      bochs-bios-data < 2.3.8.1

%description bios
Bochs BIOS is a free implementation of a x86 BIOS provided by the Bochs project.
It can also be used in other emulators, such as QEMU
%endif

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch7 -p0 -z .nonet

# Fix up some man page paths.
sed -i \
  -e 's|/usr/local/share/doc/bochs/|%{_docdir}/%{name}-%{version}/|' \
  -e 's|/usr/local/share/|%{_datadir}/|' \
  doc/man/*.*
# remove executable bits from sources to make rpmlint happy with the debuginfo
chmod -x `find -name '*.cc' -o -name '*.h' -o -name '*.inc'`
# Fix CHANGES encoding
iconv -f ISO_8859-2 -t UTF8 CHANGES > CHANGES.tmp
mv CHANGES.tmp CHANGES

%build
%ifarch %{ix86} x86_64
ARCH_CONFIGURE_FLAGS=--with-svga
%endif
# Note: the CPU level, MMX et al affect what the emulator will emulate, they
# are not properties of the build target architecture.
# Note2: passing --enable-pcidev will change bochs license from LGPLv2+ to
# LGPLv2 (and requires a kernel driver to be usefull)
CONFIGURE_FLAGS=" \
  --enable-plugins \
  --enable-ne2000 \
  --enable-pci \
  --enable-all-optimizations \
  --enable-clgd54xx \
  --enable-sb16=linux \
  --enable-3dnow \
  --with-x11 \
  --with-nogui \
  --with-term \
  --with-rfb \
  --with-sdl \
  --without-wx \
  --enable-cpu-level=6 \
  --enable-disasm \
  --enable-usb \
  --enable-usb-ohci \
  $ARCH_CONFIGURE_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS -DPARANOID"

%configure $CONFIGURE_FLAGS --enable-x86-debugger --enable-debugger
make %{?_smp_mflags}
mv bochs bochs-debugger
make dist-clean

%configure $CONFIGURE_FLAGS --enable-x86-debugger --enable-gdb-stub
make %{?_smp_mflags}
mv bochs bochs-gdb
make dist-clean

%configure $CONFIGURE_FLAGS
make %{?_smp_mflags}

%ifarch %{ix86} x86_64
cd bios
make bios
cp BIOS-bochs-latest BIOS-bochs-kvm
%endif

%install
rm -rf $RPM_BUILD_ROOT _installed-docs
make install DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT%{_prefix}/share/bochs/VGABIOS*
ln -s %{_prefix}/share/vgabios/VGABIOS-lgpl-latest.bin $RPM_BUILD_ROOT%{_prefix}/share/bochs/VGABIOS-lgpl-latest
ln -s %{_prefix}/share/vgabios/VGABIOS-lgpl-latest.cirrus.bin $RPM_BUILD_ROOT%{_prefix}/share/bochs/VGABIOS-lgpl-latest.cirrus
ln -s %{_prefix}/share/vgabios/VGABIOS-lgpl-latest.cirrus.debug.bin $RPM_BUILD_ROOT%{_prefix}/share/bochs/VGABIOS-lgpl-latest.cirrus.debug
ln -s %{_prefix}/share/vgabios/VGABIOS-lgpl-latest.debug.bin $RPM_BUILD_ROOT%{_prefix}/share/bochs/VGABIOS-lgpl-latest.debug
%ifnarch %{ix86} x86_64
rm -rf $RPM_BUILD_ROOT%{_prefix}/share/bochs/*BIOS*
%endif
install -m 755 bochs-debugger bochs-gdb $RPM_BUILD_ROOT%{_bindir}
mv $RPM_BUILD_ROOT%{_docdir}/bochs _installed-docs
rm $RPM_BUILD_ROOT%{_mandir}/man1/bochs-dlx.1*

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc _installed-docs/* README-*
%{_bindir}/bochs
%{_bindir}/bxcommit
%{_bindir}/bximage
# Note: must include *.la in %{_libdir}/bochs/plugins/
%{_libdir}/bochs/
%{_mandir}/man1/bochs.1*
%{_mandir}/man1/bxcommit.1*
%{_mandir}/man1/bximage.1*
%{_mandir}/man5/bochsrc.5*
%dir %{_datadir}/bochs/
%{_datadir}/bochs/keymaps/

%ifarch %{ix86} x86_64
%files bios
%defattr(-,root,root,-)
%{_datadir}/bochs/BIOS*
%{_datadir}/bochs/VGABIOS*
%endif

%files debugger
%defattr(-,root,root,-)
%{_bindir}/bochs-debugger

%files gdb
%defattr(-,root,root,-)
%{_bindir}/bochs-gdb

%changelog
* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.6-2m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.6-1m)
- update for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.2-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2 based on Fedora 13 (2.4.2-1)

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.8-0.0.04387139e3b.5m)
- rebuild against readline6

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.8-0.0.04387139e3b.4m)
- delete __libtoolize hack

* Sun Dec  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.8-0.0.04387139e3b.3m)
- update kvm-bios

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.8-0.0.04387139e3b.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.8-0.0.04387139e3b.1m)
- sync with Fedora 11 for qemu (kvm)
-- 
-- * Wed Mar 11 2009 Glauber Costa <glommer@redhat.com> 2.3.8-0.6.git04387139e3b
-- - Fix Obsoletes/Provides pair.
-- 
-- * Wed Mar 11 2009 Glauber Costa <glommer@redhat.com> 2.3.8-0.5.git04387139e3b
-- - kvm needs a slightly different bios due to irq routing, so build it too.
--   from kvm source. This is not ideal, but avoids the creation of yet another
--   noarch subpackage.
-- 
-- * Fri Mar 06 2009 Glauber Costa <glommer@redhat.com> 2.3.8-0.4.git04387139e3b
-- - Provide and Obsolete bochs-bios-data to make sure no one is harmed during
--   updates.
-- 
-- * Thu Mar 05 2009 Glauber Costa <glommer@redhat.com> 2.3.8-0.3.git04387139e3b
-- - added patches ;-)
-- 
-- * Thu Mar 05 2009 Glauber Costa <glommer@redhat.com> 2.3.8-0.2.git04387139e3b
-- - this time with sources added.
-- 
-- * Thu Mar 05 2009 Glauber Costa <glommer@redhat.com> 2.3.8-0.1.git04387139e3b
-- - updated to git 04387139e3b, and applied qemu's patch ontop.

* Wed Jun  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.7-4m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.7-3m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.7-2m)
- update Patch0 for fuzz=0
- License: LGPLv2+

* Mon Nov 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.7-1m)
- update to 2.3.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.6-2m)
- %%NoSource -> NoSource

* Mon Dec 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.6-1m)
- update 2.3.6

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3-4m)
- rebuild without wxGTK (use Fedora's patch)
 +* Mon Dec 18 2006 Hans de Goede <j.w.r.degoede@hhs.nl> 2.3-4
 +- rebuilt without wxGTK as wxGTK is even more broken with wxGTK 2.8 then it
 +  was with 2.6

* Mon Aug 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-3m)
- [SECURITY] CVE-2007-2894
- import security patch from Fedora devel

* Fri Aug 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-2m)
- [SECURITY] CVE-2007-2893
- import security patch from FC-devel

* Tue May 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.3-1m)
- update to 2.3

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.6-2m)
- delete libtool library, again

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.6-2m)
- delete libtool library

* Sat Mar 25 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.6-1m)
- update to 2.2.6
- build with wxGTK-2.6
- update bochs-config-sample.diff (Patch0)
- update gcc41 patch (bochs-2.2.1-gcc41.patch > bochs-2.2.6-gcc41.patch)
- add bochs-2.2.6-gtkflags.patch (Patch2): build fix

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.1-1m)
- update 2.2.1
- change SOURCE URL
- add gcc-4.1 patch
- Patch3: bochs-2.2.1-gcc41.patch

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.1-5m)
- rebuild against SDL-1.2.7-9m

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.1-4m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++
- add patch2

* Sun Aug 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.1-3m)
- rebuild against DirectFB-0.9.21

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.1-2m)
- revised spec for enabling rpm 4.2.

* Thu Feb 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.1-1m)

* Fri Jan 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.1-1m)
- New disassembler                       
- 3DNow!/SSE/SSE2/PNI instruction support                
- Vmware3/Sparse/Undoable/Growing harddisk images support               
- many VGA emulation improvements (e.g. high/true color VBE modes added)
- No more X11 vga font required

* Sat Jul 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.0.2-4m)
- add bochs-2.0.2-gcc33.patch

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.2-3m)
  rebuild against DirectFB 0.9.18

* Tue Feb 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-2m)
- add BuildPreReq: docbook-dtds >= 1.0-11m for /etc/sgml/sgml-docbook-4.1.cat, etc.

* Tue Feb  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.2-1m)

* Sat Jan 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.1-1m)

* Fri Dec 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.0-1m)

* Tue Nov 26 2002 zunda <zunda@freeshell.org>
- kossori
- BuildPreReq: XFree86-devel

* Sun Jul 14 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.4.1-1m)
- first release for momonga
