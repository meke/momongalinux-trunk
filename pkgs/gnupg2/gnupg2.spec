%global momorel 1

Summary: A GNU utility for data encryption and digital signatures
Name: gnupg2
Version: 2.0.22
Release: %{momorel}m%{?dist}
URL: http://www.gnupg.org/
Source0: ftp://ftp.gnupg.org/gcrypt/gnupg/gnupg-%{version}.tar.bz2
NoSource: 0
Source1: dot.gpgsm.conf
Source2: dot.gpg-agent.conf
Provides: gpg-agent
Provides: gpgsm
Provides: newpg

License: GPLv3+ and LGPLv3+
Group: Applications/File
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libgcrypt >= 1.2.3-1m
Requires: libassuan >= 2.0.0
Requires: pth
BuildRequires: zlib-devel
BuildRequires: bzip2-devel
BuildRequires: libcurl-devel >= 7.18.2
BuildRequires: libksba >= 1.0.2
BuildRequires: libgcrypt-devel >= 1.2.3-1m
BuildRequires: libgpg-error-devel >= 1.4
BuildRequires: libassuan-devel >= 2.0.0
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: pth-devel
BuildRequires: readline-devel
BuildRequires: gettext >= 0.14.1

%description
The GNU Privacy Guard (GnuPG) is a GNU tool for secure communication
and data storage. It is a complete and free replacement for PGP and
can be used to encrypt data and to create digital signatures. It
includes an advanced key management facilitiy and is compliant with
the proposed OpenPGP Internet standard (and its optional features)
as described in RFC2440. There are a lot of useful extra features
like anonymous message recipients and integrated keyserver support.
The program does not use any patented algorithms, and can be used as a
filter program. Can handle all OpenPGP messages and messages generated
by PGP 5.0 and newer unless they use the IDEA algorithm.

%prep
%setup -q -n gnupg-%{version}

%build
#export AUTOMAKE=automake-1.9
#export ACLOCAL=aclocal-1.9
#./autogen.sh
# --with-scdaemon-pgm=%{_sbindir}/scdaemon
# --disable-optimization. temporary build fix ofr x86_64 2006-10-31
%configure --disable-scdaemon --enable-gpgsm \
	--program-prefix="" \
	--with-agent-pgm=%{_bindir}/gpg-agent \
	--with-pinentry-pgm=%{_sbindir}/pinentry-gtk \
	--with-dirmngr-pgm=%{_bindir}/dirmngr \
%ifarch x86_64
	--disable-optimization
%endif

%make

%install
rm -rf %{buildroot}
%makeinstall

# docs
rm -fr	rpmdoc
mkdir	rpmdoc
for i in agent common jnlib kbx sm; do
 rm -fr	rpmdoc/${i}
 mkdir	rpmdoc/${i}/
 cp	${i}/ChangeLog-2011 \
 	rpmdoc/${i}/
done
rm -f %{buildroot}%{_infodir}/dir

# sample
mkdir -p %{buildroot}%{_datadir}/config-sample/gnupg2
chmod 755 %{buildroot}%{_datadir}/config-sample/gnupg2
install -m644 %{SOURCE1} \
 %{buildroot}%{_datadir}/config-sample/gnupg2/dot.gpgsm.conf
install -m644 %{SOURCE2} \
 %{buildroot}%{_datadir}/config-sample/gnupg2/dot.gpg-agent.conf

# delete conflict files with gnupg
rm -rf %{buildroot}%{_bindir}/gpg-zip
rm -rf %{buildroot}%{_bindir}/gpgsplit
rm -rf %{buildroot}%{_datadir}/gnupg/faq.html
rm -rf %{buildroot}%{_datadir}/gnupg/FAQ
rm -rf %{buildroot}%{_mandir}/man1/gpg-zip.1
rm -rf %{buildroot}%{_mandir}/man7

# documents
rm -rf %{buildroot}%{_datadir}/doc/gnupg

%clean
rm -rf %{buildroot}

%post
if [ "$1" = 1 ]; then
/sbin/install-info %{_infodir}/gnupg.info   %{_infodir}/dir
fi

%preun
if [ "$1" = 0 ]; then
/sbin/install-info --delete %{_infodir}/gnupg.info   %{_infodir}/dir
fi

%files
%defattr(-,root,root)
%doc doc/{DETAILS,FAQ,HACKING,KEYSERVER,OpenPGP,TRANSLATE,examples}
%doc README
%doc TODO
%doc ChangeLog
%doc NEWS
%doc AUTHORS
%doc THANKS
%doc rpmdoc/*
%{_bindir}/gpg2
%{_bindir}/gpgv2
%{_bindir}/gpg-agent
%{_bindir}/gpg-connect-agent
%{_bindir}/gpgkey2ssh
%{_bindir}/gpgsm
%{_bindir}/kbxutil
%{_bindir}/gpgconf
%{_bindir}/gpgparsemail
%{_bindir}/watchgnupg
%{_bindir}/gpgsm-gencert.sh
#%%{_bindir}/gpg-zip
#%%{_bindir}/gpgsplit
%{_sbindir}/addgnupghome
%{_sbindir}/applygnupgdefaults
%{_infodir}/gnupg*
%{_libexecdir}/gpg-check-pattern
%{_libexecdir}/gpg-preset-passphrase
%{_libexecdir}/gpg-protect-tool
%{_libexecdir}/gpg2keys_curl
%{_libexecdir}/gpg2keys_finger
%{_libexecdir}/gpg2keys_hkp
%{_libexecdir}/gpg2keys_ldap
%{_datadir}/gnupg/*.skel
%{_datadir}/gnupg/com-certs.pem
#%%{_datadir}/gnupg/faq.html
#%%{_datadir}/gnupg/FAQ
%{_datadir}/gnupg/qualified.txt
%{_datadir}/gnupg/help.*
%{_datadir}/locale/*/LC_MESSAGES/gnupg2.mo
%{_datadir}/config-sample/gnupg2
%{_mandir}/man*/*

%changelog
* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.22-1m)
- update to 2.0.22

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.19-1m)
- update to 2.0.19

* Fri Aug 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.18-1m)
- update to 2.0.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.17-2m)
- rebuild for new GCC 4.6

* Fri Jan 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.17-1m)
- update to 2.0.17

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.16-4m)
- full rebuild for mo7 release

* Wed Aug 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-3m)
- fix %%preun

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.16-2m)
- [SECURITY] CVE-2010-2547
- [SECURITY] patch from http://lists.gnupg.org/pipermail/gnupg-announce/2010q3/000302.html

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.16-1m)
- update to 2.0.16

* Wed Jul 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.15-1m)
- update to 2.0.15 again

* Sun Apr 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.14-2m)
- roll back to 2.0.14

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.15-1m)
- update to 2.0.15

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.14-1m)
- update to 2.0.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.13-2m)
- remove conflicting files

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.13-1m)
- update to 2.0.13

* Sun Jul  5 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.12-2m)
- change BuildRequires: libassuan -> libassuan-devel 

* Tue Jun 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.12-1m)
- update to 2.0.12

* Tue Mar 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.11-1m)
- update to 2.0.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.10-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.10-1m)
- update to 2.0.10
-- drop Path10, merged upstream
- License: GPLv3+ and LGPLv3+

* Fri Jul 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.9-4m)
- add patch10 to enable build with new curl

* Sat Jun  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.9-3m)
- do not use force bz2 setting in man

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.9-2m)
- rebuild against openssl-0.9.8h-1m

* Fri Apr 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.9-1m)
- update to 2.0.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.8-5m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.8-3m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.8-2m)
- %%NoSource -> NoSource

* Fri Dec 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8

* Tue Sep 25 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7

* Fri Aug 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.6-1m)
- update to 2.0.6

* Sun Jul  8 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5
  switch license to GPLv3
  BuildPreReq: libksba >= 1.0.2
  BuildPreReq: libassuan >= 1.0.2

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- update to 2.0.3

* Tue Feb  6 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-1m)
- [CVE-2006-6235] http://lists.gnupg.org/pipermail/gnupg-announce/2006q4/000246.html
- update to 2.0.2
- delete imported Patch1: gnupg2-2.0.1-CVE-2006-6235.patch

* Sun Dec 10 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-2m)
- [SECURITY] CVE-2006-6235
- http://lists.gnupg.org/pipermail/gnupg-announce/2006q4/000246.html
- CVE-2006-6169 was already resolved in this version

* Fri Dec  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Tue Nov 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.9.94-3m)
- rebuild against curl-7.16.0

* Tue Oct 31 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.94-2m)
- add disable-optimization option for x86_64 build fix

* Sun Oct 29 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.94-1m)
- update to 1.9.94
- delete patch1 (no longer needed)

* Sat Oct  7 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.91-2m)
- delete conflict files with gnupg

* Fri Oct  6 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.91-1m)
- update to 1.9.91
- revise related libraries version in BuildPrereq
- add patch1 to fix build error (mmm...)

* Sun Aug  6 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.9.22-1m)
- update to 1.9.22

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.20-2m)
- revise %%files
- BuildPreReq: libksba >= 0.9.14

* Sun May 21 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.9.20-1m)
- update to 1.9.20

* Sun Jan  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.18-2m)
- BuildPreReq: libgpg-error -> libgpg-error-devel

* Fri Aug 12 2005 TAKAHASHI Tamotsu <tamo>
- (1.9.18-1m)

* Sat Apr 23 2005 TAKAHASHI Tamotsu <tamo>
- (1.9.16-1m)
- gpg2 and gpgv2 are obsolete

* Tue Mar 29 2005 TAKAHASHI Tamotsu <tamo>
- (1.9.15-2m)
- add config-sample

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (1.9.15-1m)
- move from Alter to Main

* Tue Nov 30 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.9.12-1m)
- updated to 1.9.12

* Mon Jun 14 2004 TAKAHASHI Tamotsu <tamo>
- (1.9.9-1m)

* Thu Sep 11 2003 TAKAHASHI Tamotsu <tamo>
- (1.9.1-1m)
- gpg2, divided from newpg

* Thu Jun 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.4-6m)
  rebuild against cyrus-sasl2
  
* Wed May 14 2003 TAKAHASHI Tamotsu <tamo>
- (0.9.4-5m)
- pinentry 0.6.9 (has only qt-related changes)
- dirmngr  0.4.5 (src/ChangeLog has been merged to
 toplevel ChangeLog file)
- no longer declare CC=gcc
- make with _smp_mflags
- enable pinentry-qt when pinqt is 1 (default 0)

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.4-4m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.4-3m)
- rebuild against for gdbm

* Sun Feb 09 2003 TAKAHASHI Tamotsu <tamo>
- (0.9.4-2m)
- dir 0.4.4
- pin 0.6.8 (URL changed?)
- BuildPreReq: libgcrypt-devel >= 1.1.12
- autogen pinentry
- BuildPreReq: db2

* Thu Dec 05 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.9.4-1m)
- BuidPreReq: libksba >= 0.4.6

* Mon Dec 02 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.9.3-1m)
- npg 0.9.3
- dir 0.4.3
- pin 0.6.7 (URI changed)

* Sat Oct 26 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.9.2-3m)
- "LIBS=-ldb2" no longer needed

* Fri Oct 11 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (0.9.2-2m)
- all: misc docs added (rpmdoc/*)
- pinentry: option --enable-fallback-curses added

* Sat Oct 05 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.9.2-1m)
- now preun/post is in if-fi
- npg 0.9.2
- dir 0.4.2
- pin 0.6.5
- (libksba >= 0.4.5)
- (libgcrypt-devel >= 1.1.10)

* Sun Sep 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.1-2m)
- BuildPreReq: libksba >= 0.4.4

* Fri Sep 13 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.9.1-1m)
- dirver 0.4.1-p1
- pinver 0.6.4
- added dirmngr.info, renamed gpgsm.info to gnupg.info
- added newpg.mo
- removed README-alpha, agent/keyformat.txt and doc
- corrected info installation

* Fri Jul 12 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.3.9-2m)
- sorry, --with-*-pgm's should be ProGraMs (not dir)

* Thu Jul 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (0.3.9-1m)
- update
- install-info
- --disable-scdaemon --enable-gpg --without-pth
- URL: http://www.gnupg.org/
- dirver 0.3.0
- pinver 0.6.3 ENable-curses ENable-gtk DISable-qt
- libdir/newpg/protect-tools

* Thu May 02 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (0.3.5-0.020020501002k)
- from anoncvs
- url unknown
- not committed yet
