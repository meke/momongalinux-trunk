%global momorel 5
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: menu editor for GNOME
Name: alacarte
Version: 0.13.2
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://www.realistanew.com/projects/alacarte/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.13/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:    python >= 2.7
BuildRequires: pkgconfig
BuildRequires: python-devel >= 2.7
BuildRequires: gnome-menus-devel >= 3.1.90
BuildRequires: intltool
Requires: pygtk2 >= 2.14.1
Requires: gnome-python2-bonobo >= 2.26.0

%description
Alacarte is a menu editor for GNOME using the freedesktop.org menu
specification.

%prep
%setup -q
sed "s/libgnome-menu/libgnome-menu-3.0/" -i configure.ac
autoconf

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
%{_bindir}/gtk-update-icon-cache -q %{_datadir}/icons/hicolor

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
%{python_sitelib}/Alacarte
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*

%changelog
* Sun Sep 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.2-5m)
- rebuild against gnome-menus-3.1.90

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.2-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.2-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.2-1m)
- update to 0.13.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.1-2m)
- full rebuild for mo7 release

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.1-1m)
- update to 0.13.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Masanobu Sato <satosihga@momonga-linux.org>
- (0.12.4-2m)
- use %%{pyhon_sitelib} instead of %%{python_sitearch}

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.4-1m)
- update to 0.12.4

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.10-1m)
- update to 0.11.10

* Sun Feb 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.9-1m)
- update to 0.11.9

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.8-1m)
- update to 0.11.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.6-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.11.6-2m)
- rebuild against python-2.6.1-1m

* Sat Sep 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.6-1m)
- update to 0.11.6

* Wed May 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.5-3m)
- no more needed desktop-file-utils

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.5-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.5-1m)
- update to 0.11.5

* Fri Feb 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.3-2m)
- add req

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11.3-1m)
- update to 0.11.3 (unstable)

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.2-2m)
- rebuild against python-2.5

* Tue Dec 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Mon Aug 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.90-2m)
- rebuild against gnome-menus-2.14.3-3m

* Sun Aug 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.90-1m)
- initial build
