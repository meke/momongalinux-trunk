%global momorel 11

Summary: IRC client for Gtk2
Name: loqui
Version: 0.4.4
Release: %{momorel}m%{?dist}
Group: Applications/Internet
Source0: http://loqui.good-day.net/src/%{name}-%{version}.tar.gz 
NoSource: 0
License: GPL
BuildRequires: gtk2-devel >= 2.10.3, gnet-devel >= 2.0.7-2m
URL: http://loqui.good-day.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
An IRC client that has these features:
- 4 paned.
- Supports multiple servers.
- Simple UI.

%prep
%setup -q

%build
%configure LIBS="-lX11"
%make

%install
rm -rf %{buildroot}
%makeinstall
%{find_lang} %{name}

# move desktop file
mkdir -p %{buildroot}%{_datadir}/applications
mv %{buildroot}%{_sysconfdir}/X11/applnk/Internet/loqui.desktop %{buildroot}%{_datadir}/applications/

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr (-,root,root)
%doc README COPYING COPYING.LIB INSTALL ChangeLog AUTHORS NEWS NEWS.ja
%{_bindir}/loqui
%{_datadir}/applications/loqui.desktop
%{_datadir}/pixmaps/loqui.png

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-11m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-8m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.4-7m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.4-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-3m)
- %%NoSource -> NoSource

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-2m)
- rebuild against gnet-2.0.7-2m

* Sat Jan 28 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.4.4-1m)
- version up

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.9-2m)
- move loqui.desktop to %%{_datadir}/applications/

* Thu Sep 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.9-1m)
- bugfixes

* Thu Aug 26 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.8-1m)
- cleanup, bugfixes

* Tue Aug 10 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.7-1m)
- initial import to Momonga

* Thu Apr  8 2004 Yoichi Imai <yoichi@silver-forest.com>
- Added icon / desktop file.

* Thu Jan 22 2004 Yoichi Imai <yoichi@silver-forest.com>
- Improvement by iwaim. Thanks.

* Sat Jun 7 2003 Yoichi Imai <yoichi@silver-forest.com>
- created spec.in
