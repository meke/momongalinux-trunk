%global momorel 2
%global reqiure_ibus_version 1.4.99.20121006
%global dbver 1.2.99

Summary: The PinYin engine for IBus platform
Name: ibus-pinyin
Version: 1.4.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://code.google.com/p/ibus/
Source0: http://ibus.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0
Source1: http://ibus.googlecode.com/files/pinyin-database-%{dbver}.tar.bz2
NoSource: 1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ibus >= %{reqiure_ibus_version}
Requires: python-enchant
BuildRequires: coreutils
BuildRequires: gettext-devel
BuildRequires: ibus-devel >= %{reqiure_ibus_version}
BuildRequires: lua-devel
BuildRequires: pkgconfig
BuildRequires: python-devel

%description
The Anthy engine for IBus platform. It provides Japanese input method from
libanthy.

%package    open-phrase
Summary:    The open phrase database for ibus Pinyin
Group:      System Environment/Libraries
Requires(post): sqlite

%description open-phrase
The open phrase database for ibus Pinyin engine.

%prep
%setup -q

cp %{SOURCE1} data/db/open-phrase

%build
%configure --disable-static --enable-db-open-phrase \
           --disable-boost
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} NO_INDEX=true

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
# cd %%{_datadir}/ibus-pinyin/db
# sqlite3 android.db ".read create_index.sql"

%post open-phrase
# cd %%{_datadir}/ibus-pinyin/db
# sqlite3 open-phrase.db ".read create_index.sql"

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libexecdir}/ibus-engine-pinyin
%{_libexecdir}/ibus-setup-pinyin
%dir %{_datadir}/ibus-pinyin
%dir %{_datadir}/ibus-pinyin/db
%{_datadir}/ibus-pinyin/db/android.db
%{_datadir}/ibus-pinyin/db/create_index.sql
%{_datadir}/ibus-pinyin/db/english.db
%{_datadir}/ibus-pinyin/base.lua
%{_datadir}/ibus-pinyin/icons
%{_datadir}/ibus-pinyin/phrases.txt
%{_datadir}/ibus-pinyin/setup
%{_datadir}/ibus/component/pinyin.xml

%files open-phrase
%{_datadir}/ibus-pinyin/db/open-phrase.db

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-2m)
- rebuild against ibus-1.4.99.20121006

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update 1.4.0

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.99.20110217-1m)
- update 1.3.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.11-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.11-2m)
- rebuild for new GCC 4.5

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.11-1m)
- update to 1.3.11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.9-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.9-1m)
- update to 1.3.9

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8

* Sat May  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-2m)
- fix %%files to avoid conflicting

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.5-1m)
- update 1.3.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090915-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.20090915-1m)
- update to 1.2.0.20090915

* Sat May  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-0.20090303.1m)
- version 1.1.0

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080819.1m)
- update to 20080819 git snapshot

* Tue Aug 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-0.20080810.1m)
- initial package for Momonga Linux
