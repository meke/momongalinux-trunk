%global momorel 2
%global pythonver 2.7

# This is not defined on Fedora buildsystems
%{!?python_sitearch:%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%bcond_without pygtk

Summary: Next generation package handling tool
Name: smart
Version: 1.4.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://labix.org/smart/
Source0: http://launchpad.net/%{name}/trunk/%{version}/+download/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: smart.console
Source2: smart.pam
Source3: smart.desktop
Source4: distro.py
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python-devel >= %{pythonver}
BuildRequires: desktop-file-utils
BuildRequires: gettext
#Requires: python-abi = %(python -c "import sys ; print sys.version[:3]")
Requires: rpm-python >= 4.4
%{!?with_pygtk:Requires: usermode}
Requires: smart-config
Obsoletes: ksmarttray

%description
Smart Package Manager is a next generation package handling tool.

%package update
Summary: Allows execution of 'smart update' by normal users (suid)
Group: Applications/System
Requires: smart = %{version}-%{release}

%description update
Allows execution of 'smart update' by normal users through a
special suid command.

%if %{with pygtk}
%package gui
Summary: Graphical user interface for the smart package manager
Group: Applications/System
Requires: smart = %{version}-%{release}
Requires: pygtk2 >= 2.4
Requires: usermode
Provides: smart-gtk = %{version}-%{release}

%description gui
Graphical user interface for the smart package manager.
%endif

%prep
%setup -q

# /usr/lib is hardcoded 
perl -pi -e's,/usr/lib/,%{_libdir}/,' smart/const.py
install -p -m 644 %{SOURCE2} .
# Detect whether the system is using pam_stack
if test -f /%{_lib}/security/pam_stack.so \
   && ! grep "Deprecated pam_stack module" /%{_lib}/security/pam_stack.so \
      2>&1 > /dev/null; then
  perl -pi -e's,include(\s*)(.*),required\1pam_stack.so service=\2,' smart.pam
  touch -r %{SOURCE2} smart.pam
fi

%build
CFLAGS="%{optflags}"
export CFLAGS
python setup.py build

# smart-update
make -C contrib/smart-update

%install
rm -rf %{buildroot}
python setup.py install -O1 --root=%{buildroot}

mkdir -p %{buildroot}%{_libdir}/smart/plugins
mkdir -p %{buildroot}%{_sysconfdir}/smart/channels
mkdir -p %{buildroot}%{_localstatedir}/lib/smart{/packages,/channels}

# usermode support
ln -sf consolehelper %{buildroot}%{_bindir}/smart-root

mkdir -p %{buildroot}/etc/security/console.apps
mkdir -p %{buildroot}/etc/pam.d
install -p -m 644 %{SOURCE1} %{buildroot}/etc/security/console.apps/smart-root
install -p -m 644 smart.pam %{buildroot}/etc/pam.d/smart-root

# smart-update
install -p -m 4755 contrib/smart-update/smart-update %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor=""             \
  --dir %{buildroot}%{_datadir}/applications \
  %{SOURCE3}

mkdir -p %{buildroot}%{_datadir}/icons/hicolor/48x48/apps
install -p -m 644 smart/interfaces/images/smart.png \
  %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/smart.png

# distro.py and distro.d support
install -p -m 644 %{SOURCE4} %{buildroot}%{_libdir}/smart/
mkdir -p %{buildroot}%{_sysconfdir}/smart/distro.d

%find_lang %{name}

# Create a list w/o smart/interfaces/gtk to avoid warning of duplicate
# in the %files section (otherwise including all and %excluding works,
# too

echo "%%defattr(-,root,root,-)" > %{name}.fileslist
find %{buildroot}%{python_sitearch}/smart* -type d \
  | grep -v %{python_sitearch}/smart/interfaces/gtk \
  | sed -e's,%{buildroot},%%dir ,' \
  >> %{name}.fileslist
find %{buildroot}%{python_sitearch}/smart* \! -type d \
  | grep -v %{python_sitearch}/smart/interfaces/gtk \
  | sed -e's,%{buildroot},,' \
  >> %{name}.fileslist

# %files does not take two -f arguments
cat %{name}.lang >> %{name}.fileslist

%clean
rm -rf %{buildroot}

%files -f %{name}.fileslist
%defattr(-,root,root,-)
%doc HACKING README LICENSE TODO IDEAS
%{_bindir}/smart
%{_libdir}/smart
%{_sysconfdir}/smart/distro.d
%{_localstatedir}/lib/smart
%{_mandir}/man8/smart.8*

%files update
%defattr(-,root,root,-)
%{_bindir}/smart-update

%if %{with pygtk}
%files gui
%defattr(-,root,root,-)
%{python_sitearch}/smart/interfaces/gtk
%{_datadir}/applications/smart.desktop
%{_datadir}/icons/hicolor/48x48/apps/smart.png
%else
%exclude %{python_sitearch}/smart/interfaces/gtk
%exclude %{_datadir}/applications/smart.desktop
%exclude %{_datadir}/icons/hicolor/48x48/apps/smart.png
%endif

%{_bindir}/smart-root
%config %{_sysconfdir}/security/console.apps/smart-root
%config %{_sysconfdir}/pam.d/smart-root

%changelog
* Tue Jan 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-2m)
- Obsoletes: ksmarttray

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.1-1m)
- update 1.4.1

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-1m)
- update 1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-6m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-3m)
- fix build with new automake and libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- sync with Rawhide (1.1-58)

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.52-9m)
- rebuild agaisst python-2.6.1-1m

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-8m)
- rebuild against qt3

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.52-7m)
- restrict python ver-rel for egginfo

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.52-6m)
- repair broken smart.pam
- add egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.52-5m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-4m)
- specify KDE3 headers and libs
- modify BuildRequires

* Mon Dec 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.52-3m)
- release %%{_sysconfdir}/smart/channels
- it's already provided by momonga-package-config-smart

* Sun Dec  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-2m)
- change Requires: kdebase instead of Requires: %%{_bindir}/kdesu

* Thu Nov 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.52-1m)
- imoort to Momonga from RHEL5 spec
- change "with X" macro to "with_X", with_pygtk and with_ksmarttray

* Sun Oct  7 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.52-50
- Update to 0.52.
- Fix pam stack type detection.

* Sat Sep 22 2007 Ville Skytta <ville.skytta at iki.fi> - 0.51-49
- 0.51; autofs, ccache, and autotools (partially) patches addressed upstream.

* Sat Sep  8 2007 Ville Skytta <ville.skytta at iki.fi> - 0.50-48
- KSmartTray desktop entry fixes.
- License: GPLv2+

* Thu Aug  2 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.50-47
- Add kernel-tuxonice series support.

* Sun Jun  3 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.50-46
- Autodetect pam_stack module at build time.

* Mon Feb  5 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.50-45
- Adjust checks for autotools.

* Mon Jan 22 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.50-44
- Add ccache fix from svn trunk.

* Sun Jan 21 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.50-43
- gettext is BR'd outside of ksmarttray.

* Thu Jan 18 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.50-42
- Update to 0.50.

* Fri Dec  8 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.50-41_rc1
- Update to 0.50rc1.

* Sat Nov 25 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.42-40
- Start preparing virtual provides for upcoming qt gui.

* Sat Nov 25 2006 Ville Skytta <ville.skytta at iki.fi>
- Flag -xen and -PAE kernels as multi-version.
- Update desktop entry categories and icon installation paths.
- Avoid lib64 RPATHs in ksmarttray, fix menu icon.

* Sat Sep 30 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.42-39
- Fix the autofs5 patch.

* Sat Sep 16 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.42-38
- Un%%ghost pyo files.

* Sat Sep  2 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.42-37
- Add ksmarttray.desktop from ensc.
- Add missing dependency from ksmarttray on smart-gui.
- Ignore new autofs5 solaris-like NIS lookup syntax.

* Wed Aug  9 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.42-36
- Make smart-update suid.

* Mon Aug  7 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.42-35
- Add ksmarttray dependency to kdesu.

* Sat Aug  5 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.42-34
- Merge back ksmarttray package.

* Mon Jun 26 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.42-33
- Update to 0.42.
- Remove unneeded patched that have been applied upstream.

* Thu Apr 20 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.41-31
- Add virtual smart-config dependency (#175630 comment 13).

* Tue Apr 11 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.41-30
- Move the disttag to the Release: tag.

* Mon Apr 10 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.41-29
- Fix typos in distro.py, there were %% missing.
- /usr/bin/smart-root should had been %%{_bindir}/smart-root ...
- Make dependent on fedora-package-config-smart.

* Sun Apr  2 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.41-28
- Move usermode support to the gui package.
- Add cluster/gfs *-kernel variants.

* Fri Mar 31 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.41-27
- Sync with specfile from Ville Skytta <ville.skytta at iki.fi>
- Add empty-description patch (upstream issue 64).
- Update multi-version to include more kernel-* variants.
- Add distro.d support.
- Make owner of %%{_sysconfdir}/smart and %%{_localstatedir}/lib/smart{,/packages,/channels}.

* Wed Dec 21 2005 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.41-26
- Update to 0.41.

* Tue Dec 13 2005 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.40-23
- Adapted to Fedora Extras guidelines for submission.

* Sun Oct  9 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.40.

* Thu Sep 15 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.39.

* Fri Aug 19 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.37.

* Sat Jun 18 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.35.

* Fri Jun 10 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.35.

* Fri Apr  1 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.30.2.

* Fri Mar 25 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.30.

* Wed Mar 16 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.29.2.

* Sat Mar  5 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.29.1.

* Wed Dec 29 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Merge smart and smart-gui src.rpm back together again.
  (all dependencies resolved for all supported platforms)

* Mon Dec 13 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Split out smart-gui and ksmarttray to manage build dependencies better.

* Wed Dec  8 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.28.

* Sun Dec  5 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.27.1

* Fri Dec  3 2004 Axel Thimm <Axel.Thimm@ATrpms.net>
- Initial specfile from Guilherme Manika <guilherme@zorked.net>.
- Some reordering and cleanups.
- Remove binary rpmmodule.so lib.
- Split the gui into a separate package, so the non-gui packages have
  lower requirements.
