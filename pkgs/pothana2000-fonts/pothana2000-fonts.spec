%global momorel 4

%global fontname pothana2000
%global fontconf 69-%{fontname}.conf

Name: %{fontname}-fonts
Version: 1.3.2
Release: %{momorel}m%{?dist}
Summary: Unicode compliant OpenType font for Telugu

Group: User Interface/X
License: "GPLv2+ with exceptions"
URL: http://www.kavya-nandanam.com/dload.htm

Source0: Pothana2k-Li.zip
Source1: %{name}-fontconfig.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch
BuildRequires: fontpackages-devel
Requires: fontpackages-filesystem

%description
A Free OpenType font for Telugu created by
Dr. Tirumala Krishna Desikacharyulu. 

%prep
%setup -q -c -n %{name}
sed -i 's/\r//' gpl.txt

%build

%install
rm -fr %{buildroot}
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p Pothana2000.ttf %{buildroot}%{_fontdir}
install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
 %{buildroot}%{_fontconfig_confdir}
install -m 0644 -p %{SOURCE1} \
 %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
 %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -fr %{buildroot}

%_font_pkg -f %{fontconf} Pothana2000.ttf

%doc gpl.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-1m)
- import from Fedora 13

* Wed Jun 30 2010 <sshedmak@redhat.com> - 1.3.2-2
- NVR upgrade fix while moving from F12 to F13

* Mon Dec 14 2009 <sshedmak@redhat.com> - 1.3.2-1
- Fixed FSType, Preferred Style, UinqueID and Fullname
- Fixed Invalid Glyph Names reported by fontlint
- with exceptions string added in License

* Wed Mar 25 2009 <sshedmak@redhat.com> - 1.3.1-2
- Fixed download URL

* Tue Mar 24 2009 <sshedmak@redhat.com> - 1.3.1-1
- Font Exception text added to font license

* Tue Jan 15 2008 <sshedmak@redhat.com> - 1.3-1
- Initial packaging
