%global momorel 7

# Generated from rfeedfinder-0.9.11.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux
%define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname rfeedfinder
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: rFeedFinder uses RSS autodiscovery, Atom autodiscovery, spidering, URL correction, and Web service queries -- whatever it takes -- to find the feed
Name: rubygem-%{gemname}
Version: 0.9.13
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rfeedfinder.rubyforge.org
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Patch0: updated_for_isAValidURL_and_makeFullURI_methods.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(hpricot) >= 0.6
Requires: rubygem(htmlentities) >= 4.0.0
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
rFeedFinder uses RSS autodiscovery, Atom autodiscovery, spidering, URL
correction, and Web service queries -- whatever it takes -- to find the feed.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

pushd %{buildroot}%{geminstdir}
%{__patch} -p0 < %{PATCH0}
popd

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/License.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%doc %{geminstdir}/website/index.txt
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Nov 21 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.13-7m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.13-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.13-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.13-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.13-2m)
- apply http://groups.google.com/group/rfeedfinder/browse_thread/thread/24a51fc683e821ad

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.13-1m)
- update 0.9.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.11-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.11-2m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.11-1m)
- Initial package for Momonga Linux
