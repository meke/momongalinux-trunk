%global momorel 2
%global xinever 1.2.4

Name: xine-ui
Summary: A Free Video Player.
Version: 0.99.7
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.xine-project.org/
Source0: http://dl.sourceforge.net/sourceforge/xine/%{name}-%{version}.tar.xz 
NoSource: 0
Patch0: %{name}-0.9.13-dfb-0.9.13.patch
Patch1: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(post): gtk2
Requires(post): shared-mime-info
Requires(postun): desktop-file-utils
Requires(postun): gtk2
Requires(postun): shared-mime-info
Requires: xine-lib >= %{xinever}
BuildRequires: xine-lib-devel >= %{xinever}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: curl-devel >= 7.16.0
BuildRequires: desktop-file-utils
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gettext
BuildRequires: libXft-devel
BuildRequires: libtool
BuildRequires: lirc-devel
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: openssl-devel >= 0.9.8a
BuildRequires: readline-devel >= 5.0

%description
xine is a free gpl-licensed video player for unix-like systems.
We support mpeg-2 and mpeg-1 system (audio + video multiplexed) streams,
eventually mpeg-4 and other formats might be added.

xine plays the video and audio data of mpeg-2 videos and synchronizes
the playback of both. Depending on the properties of the mpeg stream,
playback will need more or less processor power, 100% frame rate
has been seen on a 400 MHz P II system.

%prep
%setup -q

%patch0 -p1 -b .dfb~
%patch1 -p1 -b .dekstop-ja~

# for %%doc
install -m 644 src/xitk/xine-toolkit/README README.xitk

# reserve for Patch2
# ./autogen.sh

%build
%configure \
	 --enable-vdr-keys \
	--disable-lirc \
	--disable-nls \
	--without-aalib

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# link icons
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/xine.png %{buildroot}%{_datadir}/pixmaps/xine.png

# remove aaxine manual
rm -f %{buildroot}%{_mandir}/*/aaxine.*
rm -f %{buildroot}%{_mandir}/*/*/aaxine.*

# clean up doc
rm -rf %{buildroot}%{_docdir}/%{name}
rm -rf %{buildroot}%{_docdir}/xitk

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README* doc/README*
%{_bindir}/fbxine
%{_bindir}/xine
%{_bindir}/xine-bugreport
%{_bindir}/xine-check
%{_bindir}/xine-remote
%{_datadir}/applications/xine.desktop
%{_datadir}/icons/hicolor/*/apps/xine.png
# %%{_datadir}/locale/*/LC_MESSAGES/%%{name}.mo
# %%{_datadir}/locale/*/LC_MESSAGES/xitk.mo
%{_datadir}/pixmaps/xine.png
%{_datadir}/pixmaps/xine.xpm
%{_mandir}/*/man1/xine-bugreport.1*
%{_mandir}/*/man1/xine-check.1*
%{_mandir}/*/man1/xine-remote.1*
%{_mandir}/*/man1/xine.1*
%{_mandir}/man1/xine-bugreport.1*
%{_mandir}/man1/xine-check.1*
%{_mandir}/man1/xine-remote.1*
%{_mandir}/man1/xine.1*
%{_datadir}/applications/xine.desktop
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/xine/oxine
%{_datadir}/xine/skins
%{_datadir}/xine/visuals

%changelog
* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.7-2m)
- rebuild against xine-lib-1.2.4

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.7-1m)
- update to 0.99.7

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.6-6m)
- fix build failure; add patch for curl

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.6-3m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.6-2m)
- rebuild against readline6

* Sat Mar  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.6-1m)
- version 0.99.6
- set --disable-lirc for the moment
- add detect-lirc.patch
  http://bugs.xine-project.org/show_bug.cgi?id=229
  http://hg.debian.org/hg/xine-lib/gxine/rev/a79307228d06
- add %%post and %%postun
- remove NEWS from %%doc
- update URL
- License: GPLv2

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.5-9m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.5-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.5-7m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.5-6m)
- rebuild against openssl-0.9.8h-1m

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.5-5m)
- set --disable-nls for the moment

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.5-4m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.5-3m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.5-2m)
- %%NoSource -> NoSource

* Sun May  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.5-1m)
- [SECURITY] CVE-2007-0254
- version 0.99.5
- remove merged string-format.patch

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99.4-8m)
- rebuild against curl-7.16.0

* Sun Sep 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.4-7m)
- remove category Application

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.99.4-6m)
- rebuild against expat-2.0.0-1m

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99.4-5m)
- rebuild against readline-5.0

* Sat Jun 10 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.99.4-4m)
- add patch1 (fix multiple format string vulnerabilities in xiTK) [CVE-2006-2230]

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99.4-3m)
- rebuild against openssl-0.9.8a

* Sun Apr  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.4-2m)
- rewind with xine-lib

* Sun Apr  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.5-0.20060409.1m)
- update to 20060409 cvs snapshot

* Wed Jul 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.4-1m)
- update to 0.99.4

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99.3-2m)
- disable aalib

* Mon Jan 03 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.3-1m)
- update to 0.99.3

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99.2-3m)
- rebuild against curl-7.12.2

* Sun Oct 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.2-2m)
- add desktop file
- BuildPreReq: desktop-file-utils

* Mon Sep  6 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.99.2-1m)
- version update to 0.99.2

* Sat Apr 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.99.1-1m)
- this release fixes some security issues

* Sat Jan  3 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.9.23-1m)
- update to 0.9.23

* Sat Dec 20 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.9.22-1m)
- update to 0.9.22

* Sun May 18 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.21-1m)

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.20-2m)
- rebuild against slang(utf8 version)
- use %%{?_smp_mflags} 

* Wed Mar 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.20-1m)
- minor bugfixes
- revise URL

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.19-1m)
  update to 0.9.19
  
* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.18-3m)
  rebuild against openssl 0.9.7a

* Thu Feb 27 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.18-2m)
- modified BuildRequire

* Fri Jan 31 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.18-1m)

* Fri Sep  6 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.13-2m)
  applied patch for DirectFB 0.9.13

* Sun Aug  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.13-1m)

* Wed Jul 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.12-2m)
- remove TO.Nonfree

* Sun Jun 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.8-2m)
- version down...

* Sat Jun 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.12-1m)
- aa (ASCII art) sub package :p

* Fri Mar  1 2002 Tsutomu Yasuda <tom@kondara.org>
- first release
