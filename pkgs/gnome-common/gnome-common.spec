%global momorel 3
Name:           gnome-common
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        Useful things common to building gnome packages from scratch

Group:          Development/Tools
BuildArch:      noarch
License:        GPLv3
URL:            http://developer.gnome.org
Source0:        http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
Patch0:	        gnome-common-automake-1.13.patch

# This will pull in the latest version; if your package requires something older,
# well, BuildRequire it in that spec.  At least until such time as we have a
# build system that is intelligent enough to inspect your source code
# and auto-inject those requirements.
Requires: automake
Requires: autoconf
Requires: libtool
Requires: gettext
Requires: pkgconfig

%description
This package contains sample files that should be used to develop pretty much
every GNOME application.  The programs included here are not needed for running
gnome apps or building ones from distributed tarballs.  They are only useful
for compiling from CVS sources or when developing the build infrastructure for
a GNOME application.

%prep
%setup -q

%patch0 -p1 -b .automake13

%build
%configure
%make 
cp doc-build/README doc-README
# No sense making a doc subdir in the rpm pkg for one file.
cp doc/usage.txt usage.txt

%install
make install DESTDIR=%{buildroot}

%files
%doc README usage.txt ChangeLog
%{_bindir}/*
%{_datadir}/aclocal/*
%{_datadir}/%{name}

%changelog
* Wed May 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.0-3m)
- update automake patch

* Wed May 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.0-2m)
- update automake patch

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update to 3.6.0

* Tue Sep 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Fri Sep  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0.1-2m)
- import patch from fedora

* Sat Jul 07 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0.1-1m)
- reimport from fedora

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.0-2m)
- change tarball bz2 -> xz

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.34.0-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.34.0-1m)
- update to 2.34.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.28.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.20.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20.0-2m)
- rebuild against gcc43

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- version 2.20.0

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.0-1m)
- version 2.18.0

* Tue Nov 15 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.0-1m)
- version 2.12.0

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.0-1m)
- version 2.8.0
- GNOME 2.8 Desktop

* Tue Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.0-2m)
- pretty spec file.
- add filelist.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0-1m)
- version 2.4.0

* Sun Jan 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.4-4k)
- don't know noarch-redhat-linux. user ./confiugre

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.4-2k)
- version 1.2.4

* Thu Apr 26 2001 Shingo Akagaki <dora@kondara.org>
- add require pkgconfig in devel package
- remove devel package

* Thu Apr  5 2001 Shingo Akagaki <dora@kondara.org>
- fix source URL

* Thu Mar 29 2001 Shingo Akagaki <dora@kondara.org>
- version 1.2.1
