%global		momorel	1
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary:	Framework providing Desktop activity awareness
Name:		zeitgeist
Version:	0.9.0
Release:	%{momorel}m%{?dist}
Group:		User Interface/Desktops
License:	LGPLv2+
URL:		https://launchpad.net/zeitgeist
Source0:	http://launchpad.net/%{name}/0.9/0.9.0/+download/%{name}-%{version}.tar.bz2
NoSource:	0
Patch1:		zeitgeist-0.9.0-crash-fix.patch
Patch2:		zeitgeist-dso-build-fix.patch
BuildRequires:	vala, python-devel, python-rdflib
BuildRequires:	glib2-devel, dbus-devel, sqlite-devel
BuildRequires:	raptor2, xapian-core-devel
BuildRequires:	gettext, perl(XML::Parser), intltool
Requires:	dbus zeitgeist-datahub
Requires:	dbus-python pygobject228 pyxdg

%description
Zeitgeist is a service which logs the users's activities and events (files
opened, websites visites, conversations hold with other people, etc.) and makes
relevant information available to other applications. 

Note that this package only contains the daemon, which you can use
together with several different user interfaces.

%prep
%setup -q
%patch1 -p1
%patch2 -p0

## nuke unwanted rpaths, see also
## https://fedoraproject.org/wiki/Packaging/Guidelines#Beware_of_Rpath
sed -i -e 's|"/lib /usr/lib|"/%{_lib} %{_libdir}|' configure

%build
%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

#%find_lang %{name}

#%files -f %{name}.lang
%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS
%{_bindir}/zeitgeist-daemon
%{_libexecdir}/zeitgeist-fts
%{_datadir}/%{name}/
%{python_sitelib}/zeitgeist/
%{_datadir}/dbus-1/services/org.gnome.zeitgeist*.service
%{_mandir}/man1/zeitgeist-*.*
%exclude %{_prefix}/doc/zeitgeist/

%changelog
* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-1m)
- import from fedora