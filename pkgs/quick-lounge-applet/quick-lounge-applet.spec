%global momorel 7

Summary: quick-lounge-applet
Name: quick-lounge-applet
Version: 2.14.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.14/%{name}-%{version}.tar.bz2
Nosource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): GConf2
Requires(post): gtk2
Requires(preun): GConf2
Requires(pre): GConf2
BuildRequires: glib-devel >= 2.18.1
BuildRequires: gtk2-devel >= 2.14.3
BuildRequires: gnome-desktop-devel >= 2.25.90
BuildRequires: gnome-panel-devel >= 2.24.0
BuildRequires: gnome-vfs-devel >= 2.24.0
BuildRequires: intltool
BuildRequires: libglade-devel >= 2.6.3
BuildRequires: libgnome-devel >= 2.24.1
BuildRequires: libgnomeui-devel >= 2.24.0
BuildRequires: pkgconfig

%description
quick-lounge-applet

%prep
%setup -q

%build
%configure \
    --disable-schemas-install \
    LIBS="-lm"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}


%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/quick-lounge.schemas \
    > /dev/null || :

gtk-update-icon-cache -q -f -t /usr/share/icons/hicolor


%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/quick-lounge.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/quick-lounge.schemas \
	> /dev/null || :
fi

%files 
%defattr(-, root, root)
%doc README ChangeLog AUTHORS NEWS
%{_sysconfdir}/gconf/schemas/quick-lounge.schemas
%{_libexecdir}/quick-lounge-applet
%{_libdir}/bonobo/servers/GNOME_QuickLoungeApplet_Factory.server
%{_datadir}/gnome-2.0/ui/GNOME_QuickLoungeApplet.xml
%{_datadir}/quick-lounge-applet
%{_datadir}/locale/*/*/*
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/gnome/help/quick-lounge

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14.0-5m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.0-4m)
- fix build

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14.0-3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.14.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Apr 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.1-1m)
- update to 2.13.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12.5-2m)
- rebuild against rpm-4.6

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.5-1m)
- update to 2.12.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-6m)
- rebuild against gcc43

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-5m)
- delete libtool library

* Thu May 18 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.1-4m)
- add localedir at makeinstall

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-3m)
- add Patch14: quick-lounge-applet-2.0.1-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (2.0.1-2m)
- rebuild against new environment.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Fri Jun 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.4-1m)
- version 1.1.4

