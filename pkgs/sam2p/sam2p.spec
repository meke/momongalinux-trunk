%global         momorel 1

%global		tif22pnmver	0.12

Summary:	Convert raster images to PostScript or PDF
Name:		sam2p
Version:	0.49.1
Release:	%{momorel}m%{dist}
License:	GPL
URL:		http://sam2p.googlecode.com/
Source0:	http://sam2p.googlecode.com/files/sam2p-%{version}.tar.gz
Source1:	http://sam2p.googlecode.com/files/tif22pnm-%{tif22pnmver}.tar.gz
NoSource:	0
NoSource:	1

Patch0:		sam2p-0.44-64bit-fixes.patch
Patch2:		tif22pnm-0.12-libm.patch
Patch3:		06_update-ccdep.patch
Patch4:		07_fix-rule.hpp.patch
Group:		Applications/File
Requires:	ghostscript
Requires:	libjpeg
BuildRequires:	libjpeg-devel
Requires:	netpbm
BuildRequires:	libtiff-devel >= 4.0.1
BuildRequires:	libpng-devel

%description
sam2p is a UNIX command line utility written in ANSI C++ that converts
many raster (bitmap) image formats into Adobe PostScript or PDF files
and several other formats. The images are not vectorized. sam2p gives
full control to the user to specify standards-compliance, compression,
and bit depths. In some cases sam2p can compress an image 100 times
smaller than the PostScript output of many other common image
converters. sam2p provides ZIP, RLE and LZW (de)compression filters
even on Level1 devices.

%prep
%setup -q -a 1
# don't use ccache,distcc
PATH=/bin:/usr/bin
export PATH
%patch0 -p1 -b .64bit-fixes
%patch3 -p1 -b .ccdep~
%patch4 -p1 -b .rule~

pushd tif22pnm-%{tif22pnmver}
%patch2 -p1 -b .libm
popd

%build

pushd tif22pnm-%{tif22pnmver}
%configure \

#	--with-libtiff-idir=%{_includedir} \
#	--with-libpng-idir=%{_includedir} \
#	--with-libtiff-ldir=%{_libdir} \
#	--with-libpng-ldir=%{_libdir}

%make

cp tif22pnm png22pnm ../
cp -p README ../README.tif22pnm
popd

%configure \
	--enable-lzw \
	--enable-gif

make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m 755 sam2p tif22pnm png22pnm %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%doc README README.tif22pnm examples contrib
%{_bindir}/*


%changelog
* Wed Sep  4 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.49.1-1m)
- update to 0.49.1

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-3m)
- rebuild against libtiff-4.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48-2m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.28-1m)
- Initial Commit Momonga Linux

