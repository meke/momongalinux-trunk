%global momorel 2
%global emacsver 23.2
#%%global gitcoredir %{_bindir}
%global gitcoredir %{_libexecdir}/git-core

# Pass --without docs to rpmbuild if you don't want the documentation
Name:           git
Version:        2.0.1
Release:        %{momorel}m%{?dist}
Summary:        Fast Version Control System
License:        GPLv2
Group:          Development/Tools
URL:            http://git-scm.com/
Source0:        http://www.kernel.org/pub/software/scm/git/%{name}-%{version}.tar.xz
NoSource:	0
Source1:        git-init.el
Source2:        git.xinetd.in
Source3:        git.conf.httpd
Source4:        git-gui.desktop
Source5:        gitweb.conf.in
Patch0:         git-1.7-gitweb-home-link.patch
# https://bugzilla.redhat.com/490602
Patch1:         git-cvsimport-Ignore-cvsps-2.2b1-Branches-output.patch

BuildRequires:	zlib-devel >= 1.2, openssl-devel >= 1.0.0, curl-devel >= 7.16.0, expat-devel
BuildRequires:	%{!?_without_docs:, xmlto, asciidoc >= 8.6.2-3m}
BuildRequires:	emacs >= %{emacsver}, gettext
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  desktop-file-utils
BuildRequires:  emacs >= 22.2
BuildRequires:  libcurl-devel

BuildRequires:  expat-devel
BuildRequires:  gettext
BuildRequires:  openssl-devel
BuildRequires:  zlib-devel >= 1.2
%{!?_without_docs:BuildRequires: asciidoc > 8.6.2-3m, xmlto}

Requires:       less
Requires:       openssh-clients
Requires:       perl(Error)
Requires:	perl-Git = %{version}-%{release}
Requires:       rsync
Requires:       zlib >= 1.2
Requires:	curl, expat
Requires:	bash-completion

Provides:       git-core = %{version}-%{release}
Obsoletes:	git-core

%description
Git is a fast, scalable, distributed revision control system with an
unusually rich command set that provides both high-level operations
and full access to internals.

The git rpm installs the core tools with minimal dependencies.  To
install all git packages, including tools for integrating with other
SCMs, install the git-all meta-package.

%package all
Summary:        Meta-package to pull in all git tools
Group:          Development/Tools
BuildArch:      noarch
Requires:       git = %{version}-%{release}
Requires:       git-cvs = %{version}-%{release}
Requires:       git-email = %{version}-%{release}
Requires:       gitk = %{version}-%{release}
Requires:       git-svn = %{version}-%{release}
Requires:       git-gui = %{version}-%{release}
Requires:       perl-Git = %{version}-%{release}
Requires:       git-emacs = %{version}-%{release}
Requires:       git-arch = %{version}-%{release}
Obsoletes:	git <= 1.5.4.3

%description all
Git is a fast, scalable, distributed revision control system with an
unusually rich command set that provides both high-level operations
and full access to internals.

This is a dummy package which brings in all subpackages.

%package daemon
Summary:        Git protocol daemon
Group:          Development/Tools
Requires:       git = %{version}-%{release}, xinetd
Requires:	zlib >= 1.2, rsync, rcs, curl, less, openssh-clients, python >= 2.3, expat
%description daemon
The git daemon for supporting git:// access to git repositories

%package -n gitweb
Summary:        Simple web interface to git repositories
Group:          Development/Tools
BuildArch:      noarch
Requires:       git = %{version}-%{release}

%description -n gitweb
Simple web interface to track changes in git repositories


%package svn
Summary:        Git tools for importing Subversion repositories
Group:          Development/Tools
#BuildArch:      noarch
Requires:       git = %{version}-%{release}, subversion, perl(Term::ReadKey)

%description svn
Git tools for importing Subversion repositories.

%package cvs
Summary:        Git tools for importing CVS repositories
Group:          Development/Tools
BuildArch:      noarch
Requires:       git = %{version}-%{release}, cvs
Requires:       cvsps

%description cvs
Git tools for importing CVS repositories.

%package arch
Summary:        Git tools for importing Arch repositories
Group:          Development/Tools
BuildArch:      noarch
Requires:       git = %{version}-%{release}, tla

%description arch
Git tools for importing Arch repositories.

%package email
Summary:        Git tools for sending email
Group:          Development/Tools
BuildArch:      noarch
Requires:	git = %{version}-%{release}, perl-Git = %{version}-%{release}
Requires:       perl(Authen::SASL)
# Requires:       perl(Net::SMTP::SSL)

%description email
Git tools for sending email.

%package gui
Summary:        Git GUI tool
Group:          Development/Tools
BuildArch:      noarch
Requires:       git = %{version}-%{release}, tk >= 8.4
Requires:       gitk = %{version}-%{release}

%description gui
Git GUI tool.

%package -n gitk
Summary:        Git revision tree visualiser
Group:          Development/Tools
BuildArch:      noarch
Requires:       git = %{version}-%{release}, tk >= 8.4

%description -n gitk
Git revision tree visualiser.

%package -n perl-Git
Summary:        Perl interface to Git
Group:          Development/Libraries
BuildArch:      noarch
Requires:       git = %{version}-%{release}
BuildRequires:  perl(Error), perl(ExtUtils::MakeMaker)
Requires:       perl(Error)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description -n perl-Git
Perl interface to Git.

%package -n git-emacs
Summary:        Git version control system support for Emacs
Group:          Applications/Editors
BuildArch:      noarch
Requires:       git = %{version}-%{release}
Requires:       emacs-common >= %{emacsver}
Obsoletes:      emacs-git

%description -n git-emacs
%{summary}.

# %%define path_settings ETC_GITCONFIG=/etc/gitconfig prefix=%{_prefix} mandir=%{_mandir} htmldir=%{_docdir}/%{name}-core-%{version}

%prep
%setup -q
%patch0 -p1
%patch1 -p1

# Use these same options for every invocation of 'make'.
# Otherwise it will rebuild in %%install due to flags changes.
cat << \EOF > config.mak
V = 1
CFLAGS = %{optflags}
BLK_SHA1 = 1
NEEDS_CRYPTO_WITH_SSL = 1
NO_PYTHON = 1
ETC_GITCONFIG = %{_sysconfdir}/gitconfig
DESTDIR = %{buildroot}
INSTALL = install -p
GITWEB_PROJECTROOT = %{_var}/lib/git
htmldir = %{_docdir}/%{name}-%{version}
prefix = %{_prefix}
gitwebdir = %{_var}/www/git
EOF

cat << \EOF >> config.mak
ASCIIDOC8 = 1
ASCIIDOC_NO_ROFF = 1
EOF

# echo gitexecdir = %{_bindir} >> config.mak

# Filter bogus perl requires
# packed-refs comes from a comment in contrib/hooks/update-paranoid
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
sed -e '/perl(packed-refs)/d'
EOF

%global __perl_requires %{_builddir}/%{name}-%{version}/%{name}-req
chmod +x %{__perl_requires}

%build
make %{?_smp_mflags} all %{!?_without_docs: doc}
make -C contrib/emacs

# Remove shebang from bash-completion script
sed -i '/^#!bash/,+1 d' contrib/completion/git-completion.bash

%install
rm -rf %{buildroot}
make %{?_smp_mflags} INSTALLDIRS=vendor install %{!?_without_docs: install-doc}
make -C contrib/emacs install \
    emacsdir=%{buildroot}%{_datadir}/emacs/site-lisp
for elc in %{buildroot}%{_datadir}/emacs/site-lisp/*.elc ; do
    install -pm 644 contrib/emacs/$(basename $elc .elc).el \
    %{buildroot}%{_datadir}/emacs/site-lisp
done
install -Dpm 644 %{SOURCE1} \
    %{buildroot}%{_datadir}/emacs/site-lisp/site-start.d/git-init.el

mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
install -pm 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/httpd/conf.d/git.conf.dist
sed "s|@PROJECTROOT@|%{_var}/lib/git|g" \
    %{SOURCE5} > %{buildroot}%{_sysconfdir}/gitweb.conf

find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -empty -exec rm -f {} ';'
find %{buildroot} -type f -name perllocal.pod -exec rm -f {} ';'

# find %{buildroot} Documentation -type f -name 'git-archimport*' -exec rm -f {} ';'

(find %{buildroot}{%{_bindir},%{_libexecdir}} -type f | grep -vE "archimport|svn|cvs|email|gitk|git-gui|git-citool|git-daemon" | sed -e s@^%{buildroot}@@) > bin-man-doc-files
(find %{buildroot}%{perl_vendorlib} -type f | sed -e s@^%{buildroot}@@) >> perl-files
%if %{!?_without_docs:1}0
(find %{buildroot}%{_mandir} -type f | grep -vE "archimport|svn|git-cvs|email|gitk|git-gui|git-citool|git-daemon" | sed -e s@^%{buildroot}@@ -e 's/$/*/' ) >> bin-man-doc-files
%else
rm -rf %{buildroot}%{_mandir}
%endif

mkdir -p %{buildroot}%{_var}/lib/git
mkdir -p %{buildroot}%{_sysconfdir}/xinetd.d
# On EL <= 5, xinetd does not enable IPv6 by default
enable_ipv6="        # xinetd does not enable IPv6 by default
        flags           = IPv6"
perl -p \
    -e "s|\@GITCOREDIR\@|%{gitcoredir}|g;" \
    -e "s|\@BASE_PATH\@|%{_var}/lib/git|g;" \
    -e "s|^}|$enable_ipv6\n$&|;" \
    %{SOURCE2} > %{buildroot}%{_sysconfdir}/xinetd.d/git

mkdir -p %{buildroot}%{_sysconfdir}/bash_completion.d
install -pm 644 contrib/completion/git-completion.bash %{buildroot}%{_sysconfdir}/bash_completion.d/git

# Move contrib/hooks out of %%docdir and make them executable
mkdir -p %{buildroot}%{_datadir}/git-core/contrib
mv contrib/hooks %{buildroot}%{_datadir}/git-core/contrib
chmod +x %{buildroot}%{_datadir}/git-core/contrib/hooks/*
pushd contrib > /dev/null
ln -s ../../../git-core/contrib/hooks
popd > /dev/null

# install git-gui .desktop file
desktop-file-install \
    --vendor="" \
    --dir=%{buildroot}%{_datadir}/applications %{SOURCE4}

# quiet some rpmlint complaints
chmod -R g-w %{buildroot}
find %{buildroot} -name git-mergetool--lib | xargs chmod a-x
rm -f {Documentation/technical,contrib/emacs}/.gitignore
chmod a-x Documentation/technical/api-index.sh
find contrib -type f | xargs chmod -x

%find_lang %{name}

%clean
rm -rf %{buildroot}


%files -f bin-man-doc-files -f %{name}.lang
%defattr(-,root,root)
%{_datadir}/git-core/
%dir %{gitcoredir}
%doc README COPYING Documentation/*.txt contrib/
%{!?_without_docs: %doc Documentation/*.html Documentation/docbook-xsl.css}
%{!?_without_docs: %doc Documentation/howto Documentation/technical}
%{_sysconfdir}/bash_completion.d/*


%files svn
%defattr(-,root,root)
%{gitcoredir}/*svn*
%doc Documentation/*svn*.txt
%{!?_without_docs: %{_mandir}/man1/*svn*.1*}
%{!?_without_docs: %doc Documentation/*svn*.html }

%files cvs
%defattr(-,root,root)
#%%{_bindir}/git-cvsserver
%doc Documentation/*git-cvs*.txt
%{_bindir}/git-cvsserver
%{gitcoredir}/*cvs*
%{!?_without_docs: %{_mandir}/man1/*cvs*.1*}
%{!?_without_docs: %doc Documentation/*git-cvs*.html }

%files arch
%defattr(-,root,root)
%doc Documentation/git-archimport.txt
%{gitcoredir}/git-archimport
%{!?_without_docs: %{_mandir}/man1/git-archimport.1*}
%{!?_without_docs: %doc Documentation/git-archimport.html }

%files email
%defattr(-,root,root)
%doc Documentation/*email*.txt
%{gitcoredir}/*email*
%{!?_without_docs: %{_mandir}/man1/*email*.1*}
%{!?_without_docs: %doc Documentation/*email*.html }

%files gui
%defattr(-,root,root)
%{gitcoredir}/git-gui*
%{gitcoredir}/git-citool
%{_datadir}/applications/*git-gui.desktop
%{_datadir}/git-gui/
%{!?_without_docs: %{_mandir}/man1/git-gui.1*}
%{!?_without_docs: %doc Documentation/git-gui.html}
%{!?_without_docs: %{_mandir}/man1/git-citool.1*}
%{!?_without_docs: %doc Documentation/git-citool.html}

%files -n gitk
%defattr(-,root,root)
%doc Documentation/*gitk*.txt
%{_bindir}/*gitk*
%{_datadir}/gitk
%{!?_without_docs: %{_mandir}/man1/*gitk*.1*}
%{!?_without_docs: %doc Documentation/*gitk*.html }

%files -n perl-Git -f perl-files
%defattr(-,root,root)

%files -n git-emacs
%defattr(-,root,root)
%doc contrib/emacs/README
%{_datadir}/emacs/site-lisp/*git*.el*
%{_datadir}/emacs/site-lisp/site-start.d/git-init.el

%files daemon
%defattr(-,root,root)
%doc Documentation/git-daemon*.txt
%config(noreplace)%{_sysconfdir}/xinetd.d/git
%{gitcoredir}/git-daemon
%{_var}/lib/git
%{!?_without_docs: %{_mandir}/man1/git-daemon*.1*}
%{!?_without_docs: %doc Documentation/git-daemon*.html}

%files -n gitweb
%defattr(-,root,root)
%doc gitweb/INSTALL gitweb/README
%config(noreplace)%{_sysconfdir}/gitweb.conf
%config(noreplace)%{_sysconfdir}/httpd/conf.d/git.conf.dist
%{_var}/www/git/


%files all
# No files for you!

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-2m)
- rebuild against perl-5.20.0

* Fri Jun 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1

* Thu May 29 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- update 2.0.0

* Mon May 12 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.3-1m)
- update 1.9.3

* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.1-1m)
- update 1.9.1

* Mon Feb 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-1m)
- update 1.9.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5.2-2m)
- rebuild against perl-5.18.2

* Wed Dec 18 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5.2-1m)
- update 1.8.5.2

* Thu Dec 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5.1-1m)
- update 1.8.5.1

* Wed Nov 13 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4.3-1m)
- update 1.8.4.3

* Wed Oct 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4.2-1m)
- update 1.8.4.2

* Mon Aug 26 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.4-1m)
- update 1.8.4

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.3-2m)
- rebuild against perl-5.18.1

* Sat May 25 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.3-1m)
- update 1.8.3

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.2.1-2m)
- rebuild against perl-5.18.0

* Sun Apr 28 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2.1-1m)
- update 1.8.2.1

* Sun Mar 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2-1m)
- update 1.8.2

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1.3-2m)
- rebuild against perl-5.16.3

* Sat Feb 23 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1.3-1m)
- update 1.8.1.3

* Wed Jan  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-1m)
- update 1.8.1

* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0.2-1m)
- update 1.8.0.2

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0.1-1m)
- update 1.8.0.1

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-2m)
- rebuild against perl-5.16.2

* Wed Oct 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0-1m)
- update 1.8.0

* Tue Sep 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.12.1-1m)
- update 1.7.12.1

* Mon Aug 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.12-1m)
- update 1.7.12

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.11.4-1m)
- update 1.7.11.4

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.11.2-3m)
- rebuild against perl-5.16.1

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.11.2-2m)
- fix %%files to avoid conflicting

* Mon Jul 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.11.2-1m)
- update 1.7.11.2

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.11-2m)
- rebuild against perl-5.16.0

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.11.1-1m)
- update 1.7.11.1

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.11-2m)
- rebuild for emacs-24.1

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.11-1m)
- update 1.7.11

* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.10.4-1m)
- update 1.7.10.4

* Mon May 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.10.2-1m)
- update 1.7.10.2

* Sun Apr 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.10-1m)
- update 1.7.10

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.9.3-1m)
- update 1.7.9.3

* Sat Feb 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.9.1-1m)
- update 1.7.9.1

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.8.3-1m)
- update 1.7.8.3

* Sat Dec 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.8.2-1m)
- update 1.7.8.2

* Mon Dec 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.8.1-1m)
- update 1.7.8.1

* Sun Dec 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.8-1m)
- update 1.7.8

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.7.4-1m)
- update 1.7.7.4

* Sat Nov 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.7.3-1m)
- update 1.7.7.3

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.7.2-1m)
- update 1.7.7.2

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.7.1-1m)
- update 1.7.7.1

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.7-1m)
- update 1.7.7

* Fri Oct  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.6.4-1m)
- update 1.7.6.4

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.6.1-2m)
- rebuild against perl-5.14.2

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.6.1-1m)
- update 1.7.6.1

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.6-1m)
- update 1.7.6

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.5.4-2m)
- rebuild against perl-5.14.1

* Sun Jun  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5.4-1m)
- update 1.7.5.4

* Sat May 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5.2-1m)
- update 1.7.5.2

* Sun May  8 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.5.1-1m)
- update 1.7.5.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.5-2m)
- rebuild against perl-5.14.0-0.2.1m

* Tue Apr 26 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.7.5-1m)
- update 1.7.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.4.4-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4.4-1m)
- update 1.7.4.4

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4.3-1m)
- update 1.7.4.3

* Mon Mar 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4.2-1m)
- update 1.7.4.2

* Sun Feb 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4.1-1m)
- update 1.7.4.1

* Sat Feb  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4-1m)
- update 1.7.4

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3.5-1m)
- update 1.7.3.5

* Tue Dec 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3.3-1m)
- update 1.7.3.3

* Fri Dec  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.3.2-3m)
- documentation needs asciidoc >= 8.6.2-3m

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.3.2-2m)
- rebuild for new GCC 4.5

* Fri Nov  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3.2-1m)
- update 1.7.3.2

* Wed Oct  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3.1-1m)
- update 1.7.3.1

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.2.1-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2.1-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2.1-1m)
- update 1.7.2.1

* Sun Jul 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.2-1m)
- [SECURITY] CVE-2010-2542
- update 1.7.2
- update Patch0

* Thu Jul 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.1.1-1m)
- update 1.7.1.1

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.1-3m)
- rebuild against emacs-23.2

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-2m)
- rebuild against perl-5.12.1

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.1-1m)
- update 1.7.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.0.4-2m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.0.4-1m)
- update 1.7.0.4

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.0-2m)
- rebuild against openssl-1.0.0

* Wed Feb 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.0-1m)
- update 1.7.0
- merge fedora patches

* Mon Feb 15 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.0-0.1m)
- update 1.7.0 : local test version

* Fri Dec 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.6-1m)
- update 1.6.6

* Wed Dec 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.5.6-1m)
- update 1.6.5.6

* Mon Dec  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.5.5-1m)
- update 1.6.5.5

* Fri Dec  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.5.4-1m)
- update 1.6.5.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.5.1-1m)
- update 1.6.5.1

* Wed Sep 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4.3-1m)
- update 1.6.4.3

* Mon Sep  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4.2-1m)
- update 1.6.4.2

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4.1-1m)
- update 1.6.4.1

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.4-3m)
- rebuild against perl-5.10.1

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-2m)
- rebuild against emacs 23.1

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.3.3-2m)
- rebuild against emacs 23.0.96

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3.3-1m)
- update to 1.6.3.3

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3.2-2m)
- rebuild against emacs-23.0.95

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3.2-1m)
- [SECURITY] CVE-2009-2108
- update to 1.6.3.2

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3.1-2m)
- rebuild against emacs-23.0.94

* Mon May 18 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.3.1-1m)
- update to 1.6.3.1

* Fri May  8 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3
- update to emacsver 23.0.93
- update asciidoc >= 8.4.4 for avoid print wierd warning

* Fri May  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.2.4-1m)
- update

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2.2-2m)
- rebuild against openssl-0.9.8k

* Mon Apr  6 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.2.2-1m)
- update 1.6.2.2

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2.1-1m)
- rebuild against emacs-23.0.92

* Wed Apr  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2.1-1m)
- update 1.6.2.1

* Mon Mar 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-2m)
- update git.xinetd

* Wed Mar  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.2-1m)
- update 1.6.2

* Wed Feb 11 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1.3-1m)
- update 1.6.1.3
- rename emacs-git to git-emacs

* Sat Jan 31 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1.2-1m)
- update 1.6.1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-2m)
- rebuild against rpm-4.6

* Thu Dec 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1-1m)
- [SECURITY] CVE-2008-5516 CVE-2008-5517
- update 1.6.1

* Mon Dec 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.6-1m)
- [SECURITY] CVE-2008-5916
- update to 1.6.0.6

* Fri Dec 12 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.5-1m)
- update 1.6.0.5

* Tue Nov 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.4-1m)
- update 1.6.0.4

* Thu Oct 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.3-1m)
- update 1.6.0.3

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.2-1m)
- update 1.6.0.2

* Wed Sep  3 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.1-1m)
- update 1.6.0.1

* Thu Aug 21 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-2m)
- merge T4R changes

* Thu Aug 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update 1.6.0

* Thu Jul 24 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.6.4-1m)
- [SECURITY] CVE-2008-3546
- update 1.5.6.4

* Sat Jul 12 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.6.2-1m)
- update 1.5.6.2

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.6.1-2m)
- rename git.conf to git.conf.dist

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.6.1-1m)
- update 1.5.6.1

* Sat Jun 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.6-1m)
- update 1.5.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5.3-2m)
- rebuild against openssl-0.9.8h-1m

* Sun Jun  1 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.5.3-1m)
- update 1.5.5.3

* Fri May 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.5.2-3m)
- add Requires: bash-completion

* Thu May 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.5.2-2m)
- fix %%install section to avoid conflicting

* Wed May 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.5.2-1m)
- merge fedora patches
- Source1: git-init.el
- Source2: git.xinetd
- Source3: git.conf.httpd
- Patch0:  git-1.5-gitweb-home-link.patch
- add Requires

* Thu Apr 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.5.1-1m)
- update 1.5.5.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.4.5-2m)
- rebuild against gcc43

* Tue Apr  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.4.5-1m)
- update 1.5.4.5

* Thu Mar 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.4.4-1m)
- update 1.5.4.4

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.4.3-1m)
- update 1.5.4.3

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.4.2-1m)
- update 1.5.4.2

* Mon Feb 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.4.1-1m)
- update 1.5.4.1

* Sat Feb  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.4-1m)
- update 1.5.4

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.3.8-2m)
- rebuild against perl-5.10.0-1m

* Fri Jan 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.3.8-1m)
- update 1.5.3.8

* Wed Dec 5 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.5.3.7-1m)
- update to 1.5.3.7

* Tue Oct 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3.4-2m)
- use build_p4 option and set %%global build_p4 0

* Tue Oct 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.3.4-1m)
- update 1.5.3.4

* Tue Aug 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.2.4-1m)
- update 1.5.2.4

* Thu Jul 12 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.2.3-1m)
- update 1.5.2.3

* Sun Jun 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2.1-1m)
- update 1.5.2.1

* Thu May 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.2-2m)
- define PYTHON_PATH=%%{python_path}
- %%files fix

* Mon May 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-1m)
- update 1.5.2

* Tue May  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.1.3-1m)
- update

* Tue Apr 17 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.1.1-1m)
- update

* Sun Dec 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4.2-1m)
- update

* Fri Nov 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4.1-1m)
- update

* Wed Nov 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-1m)
- update

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.3.4-2m)
- rebuild against curl-7.16.0

* Fri Nov 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3.4-1m)
- update

* Tue Oct 31 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3.3-1m)
- update

* Sat Oct 21 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3.1-1m)
- update

* Thu Oct 19 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3-1m)
- update
- delete patch0
- add Git.pm into %%files core

* Tue Oct 17 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2.4-1m)
- update

* Mon Oct 02 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2.3-1m)
- update

* Sun Oct 01 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2.2-1m)
- update

* Wed Sep 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2.1-2m)
- add patch0

* Mon Sep 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2.1-1m)
- import to Momonga

* Fri Sep 22 2006 Chris Wright <chrisw@redhat.com> 1.4.2.1-1
- git-1.4.2.1

* Mon Sep 11 2006 Chris Wright <chrisw@redhat.com> 1.4.2-1
- git-1.4.2

* Thu Jul 6 2006 Chris Wright <chrisw@redhat.com> 1.4.1-1
- git-1.4.1

* Tue Jun 13 2006 Chris Wright <chrisw@redhat.com> 1.4.0-1
- git-1.4.0

* Thu May 4 2006 Chris Wright <chrisw@redhat.com> 1.3.3-1
- git-1.3.3
- enable git-email building, prereqs have been relaxed

* Thu May 4 2006 Chris Wright <chrisw@redhat.com> 1.3.2-1
- git-1.3.2

* Fri Apr 28 2006 Chris Wright <chrisw@redhat.com> 1.3.1-1
- git-1.3.1

* Wed Apr 19 2006 Chris Wright <chrisw@redhat.com> 1.3.0-1
- git-1.3.0

* Mon Apr 10 2006 Chris Wright <chrisw@redhat.com> 1.2.6-1
- git-1.2.6

* Wed Apr 5 2006 Chris Wright <chrisw@redhat.com> 1.2.5-1
- git-1.2.5

* Wed Mar 1 2006 Chris Wright <chrisw@redhat.com> 1.2.4-1
- git-1.2.4

* Wed Feb 22 2006 Chris Wright <chrisw@redhat.com> 1.2.3-1
- git-1.2.3

* Tue Feb 21 2006 Chris Wright <chrisw@redhat.com> 1.2.2-1
- git-1.2.2

* Thu Feb 16 2006 Chris Wright <chrisw@redhat.com> 1.2.1-1
- git-1.2.1

* Mon Feb 13 2006 Chris Wright <chrisw@redhat.com> 1.2.0-1
- git-1.2.0

* Tue Feb 1 2006 Chris Wright <chrisw@redhat.com> 1.1.6-1
- git-1.1.6

* Tue Jan 24 2006 Chris Wright <chrisw@redhat.com> 1.1.4-1
- git-1.1.4

* Sun Jan 15 2006 Chris Wright <chrisw@redhat.com> 1.1.2-1
- git-1.1.2

* Tue Jan 10 2006 Chris Wright <chrisw@redhat.com> 1.1.1-1
- git-1.1.1

* Tue Jan 10 2006 Chris Wright <chrisw@redhat.com> 1.1.0-1
- Update to latest git-1.1.0 (drop git-email for now)
- Now creates multiple packages:
-	 git-core, git-svn, git-cvs, git-arch, gitk

* Mon Nov 14 2005 H. Peter Anvin <hpa@zytor.com> 0.99.9j-1
- Change subpackage names to git-<name> instead of git-core-<name>
- Create empty root package which brings in all subpackages
- Rename git-tk -> gitk

* Thu Nov 10 2005 Chris Wright <chrisw@osdl.org> 0.99.9g-1
- zlib dependency fix
- Minor cleanups from split
- Move arch import to separate package as well

* Tue Sep 27 2005 Jim Radford <radford@blackbean.org>
- Move programs with non-standard dependencies (svn, cvs, email)
  into separate packages

* Tue Sep 27 2005 H. Peter Anvin <hpa@zytor.com>
- parallelize build
- COPTS -> CFLAGS

* Fri Sep 16 2005 Chris Wright <chrisw@osdl.org> 0.99.6-1
- update to 0.99.6

* Fri Sep 16 2005 Horst H. von Brand <vonbrand@inf.utfsm.cl>
- Linus noticed that less is required, added to the dependencies

* Sun Sep 11 2005 Horst H. von Brand <vonbrand@inf.utfsm.cl>
- Updated dependencies
- Don't assume manpages are gzipped

* Thu Aug 18 2005 Chris Wright <chrisw@osdl.org> 0.99.4-4
- drop sh_utils, sh-utils, diffutils, mktemp, and openssl Requires
- use RPM_OPT_FLAGS in spec file, drop patch0

* Wed Aug 17 2005 Tom "spot" Callaway <tcallawa@redhat.com> 0.99.4-3
- use dist tag to differentiate between branches
- use rpm optflags by default (patch0)
- own %{_datadir}/git-core/

* Mon Aug 15 2005 Chris Wright <chrisw@osdl.org>
- update spec file to fix Buildroot, Requires, and drop Vendor

* Sun Aug 07 2005 Horst H. von Brand <vonbrand@inf.utfsm.cl>
- Redid the description
- Cut overlong make line, loosened changelog a bit
- I think Junio (or perhaps OSDL?) should be vendor...

* Thu Jul 14 2005 Eric Biederman <ebiederm@xmission.com>
- Add the man pages, and the --without docs build option

* Wed Jul 7 2005 Chris Wright <chris@osdl.org>
- initial git spec file
