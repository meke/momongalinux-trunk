%global momorel 1
%define glib2_version 2.40.0
%define gobject_introspection_version 1.40.0

Summary: Interfaces for accessibility support
Name: atk
Version: 2.13.1
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
#VCS: git:git://git.gnome.org/atk
Source: http://download.gnome.org/sources/atk/2.13/atk-%{version}.tar.xz
NoSource: 0
URL: http://developer.gnome.org/projects/gap/
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gnome-doc-utils
BuildRequires: gettext
BuildRequires: gobject-introspection-devel >= %{gobject_introspection_version}
# Bootstrap requirements
BuildRequires: gnome-common gtk-doc

%description
The ATK library provides a set of interfaces for adding accessibility
support to applications and graphical user interface toolkits. By
supporting the ATK interfaces, an application or toolkit can be used
with tools such as screen readers, magnifiers, and alternative input
devices.

%package devel
Summary: Development files for the ATK accessibility toolkit
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package includes libraries, header files, and developer documentation
needed for development of applications or toolkits which use ATK.

%prep
%setup -q

%build
(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; CONFIGFLAGS=--enable-gtk-doc; fi;
 %configure $CONFIGFLAGS)
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%find_lang atk10

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f atk10.lang
%doc README AUTHORS COPYING NEWS
%{_libdir}/libatk-1.0.so.*
%{_libdir}/girepository-1.0

%files devel
%{_libdir}/libatk-1.0.so
%{_includedir}/atk-1.0
%{_libdir}/pkgconfig/atk.pc
%{_datadir}/gtk-doc/html/atk
%{_datadir}/gir-1.0

%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.13.1-1m)
- update to 2.13.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.91-1m)
- update to 2.5.91

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.3-1m)
- reimport from fedora

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-2m)
- fix build failure with glib 2.33

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.92-1m)
- update to 2.1.92

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.91-1m)
- update to 2.1.91

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.32.0-3m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.32.0-2m)
- fix BR:

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.30.0-2m)
- full rebuild for mo7 release

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.30.0-1m)
- update to 2.30.0

* Wed Jan 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.29.4-1m)
- update to 1.29.4

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28.0-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.0-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.28.0-1m)
- update to 1.28.0

* Wed Mar 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.26.0-1m)
- update to 1.26.0
-- disable-gtk-doc (something wrong)

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.25.2-1m)
- update to 1.25.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24.0-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.24.0-1m)
- update to 1.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.22.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.22.0-1m)
- update to 1.22.0

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.20.0-1m)
- update to 1.20.0

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.18.0-1m)
- update to 1.18.0

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.17.0-1m)
- update to 1.17.0 (unstable)

* Mon Dec 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.4-1m)
- update to 1.12.4

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.3-1m)
- update to 1.12.3

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.2-2m)
- delete libtool library

* Fri Aug 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.2-1m)
- update to 1.12.2
- Not delete libtool library (gtk+-devel requires)

* Wed Apr  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.4-1m)
- update to 1.11.4

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.3-4m)
- comment out unnessesary autoreconf and make check
 
* Fri Nov 18 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.3-3m)
- enable gtk-doc

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.3-2m)
- does not use automake 1.7
- GNOME 2.12.1 Desktop

* Wed Oct  5 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.10.3-1m)
- version 1.10.3

* Mon May 16 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.10.1-1m)
- version 1.10.1

* Fri Apr 22 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.1-1m)
- version 1.9.1

* Fri Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.8.0-3m)
- GNOME 2.8 Desktop
- rebuild against for glib-2.6.1

* Sun Dec 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0-2m)
- add auto* commands for make .so files

* Mon Oct 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.0-1m)
- version 1.8.0

* Thu Jun 17 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.1-1m)
- version 1.7.1

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.6.0-2m)
- adjustment BuildPreReq

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.6.0-1m)
- version 1.6.0
- GNOME 2.6 Desktop

* Wed Nov 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-2m)
- add exec ldconfig at %%post and %%postun

* Wed Nov 05 2003 Kenta MURATA <muraken2@nifty.com>
- (1.4.1-2m)
- includes .la file.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (1.4.1-1m)
- pretty spec file.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.4.1-1m)
- version 1.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.4.0-1m)
- version 1.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.5-1m)
- version 1.3.5

* Mon Jun 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.4-1m)
- version 1.3.4

* Mon Jun 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.3-1m)
- version 1.3.3

* Mon May 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.2-1m)
- version 1.3.2

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.1-1m)
- version 1.3.1

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.0-1m)
- version 1.3.0

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.2-1m)
- version 1.2.2

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-1m)
- version 1.2.1

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.0-1m)
- version 1.2.0

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.5-1m)
- version 1.1.5

* Mon Dec 09 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.4-1m)
- version 1.1.4

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.3-1m)
- version 1.1.3

* Wed Nov 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.2-1m)
- version 1.1.2

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.1-1m)
- version 1.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.0-1m)
- version 1.1.0

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.3-1m)
- version 1.0.3

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.2-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.2-2k)
- version 1.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.1-2k)
- version 1.0.1

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0-2k)
- version 1.0.0

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0.rc1-2k)
- version 1.0.0.rc1

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.0.rc1-2k)
- version 1.0.0.rc1

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (0.13-4k)
- modify dependency list

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.13-2k)
- version 0.13

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.12-4k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.12-2k)
- version 0.12

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.11-2k)
- version 0.11

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.10-8k)
- rebuild against for glib-1.3.13

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (0.8-2k)
- port from Jirai
- version 0.8

* Wed Jul 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- kondaraize

* Wed Jun 13 2001 Havoc Pennington <hp@redhat.com>
- 0.2

* Fri May  4 2001 Owen Taylor <otaylor@redhat.com>
- Initial version
