%global momorel 9

Summary: GNU arch revision control system
Name: tla
Version: 1.3.5
Release: %{momorel}m%{?dist}
Group: Development/Tools
URL: http://regexps.srparish.net/www/#Gnu-arch
License: GPL

Source0: ftp://ftp.gnu.org/gnu/gnu-arch/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: tla-1.2-texi.patch
# tla-1.2-texi.patch from SuSE tla-1.2-15.4

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: info

Requires: info

# required commands from info arch
# awk
Requires: gawk
# cat,chmod,date,echo,fold,,head,ls,mkdir,printf,pwd,rm,tee,test,touch,tsort,wc
Requires: coreutils
# find,xargs
Requires: findutils
Requires: grep
Requires: sed
# sh
Requires: bash
# more commands
Requires: patch

%description
Gnu arch is a modern and remarkable revision control system. It helps
programmers to coordinate and share their changes to a project's source
code. It helps project managers to organize, track, and control
multi-branch development. It supports both centralized and distributed
projects.

%prep
%setup -q
#%%patch -p1

%build
mkdir build
cd build
../src/configure --prefix=%{_prefix}
%make
#make test
# all test passed on 2.6.8-11m i686 (Nov 11 2004)

%install
rm -rf %{buildroot}
cd build
make install destdir=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/tla
%doc src/docs-tla

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.5-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.5-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.5-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.5-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.5-2m)
- %%NoSource -> NoSource

* Fri Nov 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.5-1m)
- update

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.3.4-2m)
- rebuild against expat-2.0.0-1m

* Sun Jan  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.4-1m)
- version up 1.3.4

* Tue Feb 15 2005 zunda <zunda at freeshell.org>
- (1.2.1-2m)
- added Requires: for required commands

* Wed Nov 10 2004 zunda <zunda at freeshell.org>
- (1.2.1-1m)
- initial import
