%global         momorel 1

Summary:        A library for reading EPub documents
Name:           ebook-tools
Version:        0.2.2
Release:        %{momorel}m%{?dist}
License:        MIT
Group:          System Environment/Libraries
URL:            http://sourceforge.net/projects/ebook-tools
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libzip-devel >= 0.10
BuildRequires:  libxml2-devel
BuildRequires:  cmake

%description
Tools for accessing and converting various ebook file formats

%package devel
Summary: Files needed for development using %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"

%{cmake} .
make %{?_smp_mflags} VERBOSE=1

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc INSTALL LICENSE README TODO
%{_bindir}/einfo
%{_bindir}/lit2epub
%{_libdir}/libepub.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/epub.h
%{_includedir}/epub_shared.h
%{_includedir}/epub_version.h
%{_libdir}/libepub.so

%changelog
* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-4m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-3m)
- rebuild against libzip-0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-2m)
- rebuild for new GCC 4.5

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Mar 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-3m)
- fix up package splitting

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-2m)
- rebuild against rpm-4.6

* Thu Jul 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-1m)
- initial build for Momonga Linux
