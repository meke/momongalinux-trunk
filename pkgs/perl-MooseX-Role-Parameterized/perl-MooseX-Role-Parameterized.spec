%global         momorel 3

Name:           perl-MooseX-Role-Parameterized
Version:        1.02
Release:        %{momorel}m%{?dist}
Summary:        Roles with composition parameters
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Role-Parameterized/
Source0:        http://www.cpan.org/authors/id/S/SA/SARTAK/MooseX-Role-Parameterized-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Moose >= 0.78
BuildRequires:  perl-Test-Exception >= 0.27
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-Moose >= 0.78
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Your parameterized role consists of two new things: parameter declarations
and a role block.

%prep
%setup -q -n MooseX-Role-Parameterized-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/MooseX/Role/Parameterized.pm
%{perl_vendorlib}/MooseX/Role/Parameterized
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-2m)
- rebuild against perl-5.18.2

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-2m)
- rebuild against perl-5.14.2

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-2m)
- rebuild for new GCC 4.6

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Tue Mar  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Sat Dec 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sun Nov 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.21-2m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Tue Nov  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.19-2m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-2m)
- rebuild against perl-5.12.0

* Thu Mar 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Fri Feb 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Sat Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Wed Jan  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
