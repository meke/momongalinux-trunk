%global momorel 1
%define strace64_arches ppc64

Summary: Tracks and displays system calls associated with a running process.
Name: strace
Version: 4.8
Release: %{momorel}m%{?dist}
License: BSD
Group: Development/Debuggers
URL: http://sourceforge.net/projects/strace/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
Patch1:	strace-4.8-glibc2.17.90.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool

%description
The strace program intercepts and records the system calls called and
received by a running process.  Strace can print a record of each
system call, its arguments and its return value.  Strace is useful for
diagnosing problems and debugging, as well as for instructional
purposes.

Install strace if you need a tool to track the system calls made and
received by a process.

%ifarch %{strace64_arches}
%package -n strace64
Summary: Tracks and displays system calls associated with a running process.
Group: Development/Debuggers

%description -n strace64
The strace program intercepts and records the system calls called and
received by a running process.  Strace can print a record of each
system call, its arguments and its return value.  Strace is useful for
diagnosing problems and debugging, as well as for instructional
purposes.

Install strace if you need a tool to track the system calls made and
received by a process.

This package provides the `strace64' program to trace 64-bit processes.
The `strace' program in the `strace' package is for 32-bit processes.
%endif

%prep
%setup -q
%patch1 -p1 -b .glibc2.17.90~
%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%ifarch %{strace64_arches}
ln %{buildroot}%{_bindir}/strace %{buildroot}%{_bindir}/strace64
%endif

# remove unpackaged files from the buildroot
rm -f %{buildroot}%{_bindir}/strace-graph
 
%check
make check

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CREDITS ChangeLog* INSTALL NEWS README
%{_bindir}/strace
%{_bindir}/strace-log-merge
%{_mandir}/man1/*

%ifarch %{strace64_arches}
%files -n strace64
%defattr(-,root,root)
%{_bindir}/strace64
%endif

%changelog
* Sun Sep  1 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8-1m)
- update to 4.8
- add patch to fix build failure
- add %%check

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6-2m)
- rebuild for new GCC 4.6

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6-1m)
- update to 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.20-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.20-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.20-1m)
- update to 4.5.20

* Thu Jan  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.19-2m)
- [BUILD FIX] apply upstream fix for kernel-2.6.32.2

* Fri Nov 20 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.5.19-1m)
- update 4.5.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.18-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.18-2m)
- rebuild against rpm-4.6

* Fri Sep 05 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.5.18-1m)
- update 4.5.18

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.16-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.16-2m)
- %%NoSource -> NoSource

* Thu Aug 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.16-1m)
- update 4.6.16
- sync with fc-devel (strace-4_5_16-1_fc8)
--* Fri Aug  3 2007 Roland McGrath <roland@redhat.com> - 4.5.16-1
--- fix multithread issues (#240962, #240961, #247907)
--- fix spurious SIGSTOP on early interrupt (#240986)
--- fix utime for biarch (#247185)
--- fix -u error message (#247170)
--- better futex syscall printing (##241467)
--- fix argv/envp printing with small -s settings, and for biarch
--- new syscalls: getcpu, eventfd, timerfd, signalfd, epoll_pwait,
--- move_pages, utimensat

* Sat Jun 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.15-1m)
- update 4.5.15
- sync with fc-devel (strace-4_5_15-1_fc7)

* Wed Feb 28 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.5.14-3m)
- Patch1: strace-4.5.14-NoCTL_PROC.patch

* Thu Oct 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.14-2m)
- add strace-4.5.14-fix-undeclared.patch

* Fri May 19 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.5.14-1m)
- update to 4.5.14

* Fri Jan  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.13-1m)
- version 4.5.13
- No %%NoSource

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.5.8-1m)
- version up

* Fri Jun  4 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.5.4-1m)
- version update to 4.5.4

* Wed Oct 29 2003 zunda <zunda at freeshell.org>
- (4.4.95-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft).

* Mon May 19 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.4.95-1m)
- update to 4.4.95
- stop strip
- stop NoSource

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (4.4-2k)
- ver up.

* Sun Apr 15 2001 Toru Hoshina <toru@df-usa.com>
- (4.3-4k)
- Whhhhhhhhhhy CHAR ?

* Thu Mar 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (4.2-10k)
- rebuild against glibc222 and add strace.glibc222.time.patch

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.
- merge from rawhide.

* Fri Jun  2 2000 Matt Wilson <msw@redhat.com>
- use buildinstall for FHS

* Wed May 24 2000 Jakub Jelinek <jakub@redhat.com>
- make things compile on sparc
- fix sigreturn on sparc

* Fri Mar 31 2000 Bill Nottingham <notting@redhat.com>
- fix stat64 misdef (#10485)

* Tue Mar 21 2000 Michael K. Johnson <johnsonm@redhat.com>
- added ia64 patch

* Thu Feb 17 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Kondara Adaptations.

* Thu Feb 03 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed
- version 4.2 (why are we keeping all these patches around?)

* Sat Nov 27 1999 Jeff Johnson <jbj@redhat.com>
- update to 4.1 (with sparc socketcall patch).

* Fri Nov 12 1999 Jakub Jelinek <jakub@redhat.com>
- fix socketcall on sparc.

* Thu Sep 02 1999 Cristian Gafton <gafton@redhat.com>
- fix KERN_SECURELVL compile problem

* Tue Aug 31 1999 Cristian Gafton <gafton@redhat.com>
- added alpha patch from HJLu to fix the osf_sigprocmask interpretation

* Sat Jun 12 1999 Jeff Johnson <jbj@redhat.com>
- update to 3.99.1.

* Wed Jun  2 1999 Jeff Johnson <jbj@redhat.com>
- add (the other :-) jj's sparc patch.

* Wed May 26 1999 Jeff Johnson <jbj@redhat.com>
- upgrade to 3.99 in order to
-    add new 2.2.x open flags (#2955).
-    add new 2.2.x syscalls (#2866).
- strace 3.1 patches carried along for now.

* Sun May 16 1999 Jeff Johnson <jbj@redhat.com>
- don't rely on (broken!) rpm %%patch (#2735)

* Tue Apr 06 1999 Preston Brown <pbrown@redhat.com>
- strip binary

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 16)

* Tue Feb  9 1999 Jeff Johnson <jbj@redhat.com>
- vfork est arrive!

* Tue Feb  9 1999 Christopher Blizzard <blizzard@redhat.com>
- Add patch to follow clone() syscalls, too.

* Sun Jan 17 1999 Jeff Johnson <jbj@redhat.com>
- patch to build alpha/sparc with glibc 2.1.

* Thu Dec 03 1998 Cristian Gafton <gafton@redhat.com>
- patch to build on ARM

* Wed Sep 30 1998 Jeff Johnson <jbj@redhat.com>
- fix typo (printf, not tprintf).

* Sat Sep 19 1998 Jeff Johnson <jbj@redhat.com>
- fix compile problem on sparc.

* Tue Aug 18 1998 Cristian Gafton <gafton@redhat.com>
- buildroot

* Mon Jul 20 1998 Cristian Gafton <gafton@redhat.com>
- added the umoven patch from James Youngman <jay@gnu.org>
- fixed build problems on newer glibc releases

* Mon Jun 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr
