%global momorel 4

# Prevent brp-java-repack-jars from being run.
%define __jar_repack %{nil}

Name:           netbeans-javaparser
Version:        6.8
Release:        %{momorel}m%{?dist}
Summary:        NetBeans Java Parser
License:        "GPLv2 with exceptions"
Url:            http://java.netbeans.org/javaparser/
Group:          Development/Libraries
# The source for this package was pulled from upstream's vcs.  Use the
# following commands to generate the tarball:
# hg clone http://hg.netbeans.org/main/nb-javac/
# cd nb-javac/
# hg update -r 1c46268162cd
# tar -czvf ../nb-javac-6.8.tar.gz .
Source0:        nb-javac-%{version}.tar.gz

BuildRequires:  ant
BuildRequires:  ant-nodeps
BuildRequires:  java-devel >= 1.6.0
BuildRequires:  jpackage-utils

Requires:       java >= 1.6.0
Requires:       jpackage-utils

BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root

%description
Java parser to analyse Java source files inside of the NetBeans IDE

%prep
%setup -q -c
# remove all binary libs
find . -name "*.jar" -exec %__rm -f {} \;

%build
[ -z "$JAVA_HOME" ] && export JAVA_HOME=%{_jvmdir}/java 
%ant -f make/netbeans/nb-javac/build.xml jar

%install
%__rm -fr %{buildroot}

# jar
%__install -d -m 755 %{buildroot}%{_javadir}
%__install -m 644 make/netbeans/nb-javac/dist/javac-api.jar %{buildroot}%{_javadir}/%{name}-api-%{version}.jar
%__ln_s %{name}-api-%{version}.jar %{buildroot}%{_javadir}/%{name}-api.jar
%__install -m 644 make/netbeans/nb-javac/dist/javac-impl.jar %{buildroot}%{_javadir}/%{name}-impl-%{version}.jar
%__ln_s %{name}-impl-%{version}.jar %{buildroot}%{_javadir}/%{name}-impl.jar

%clean
%__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ASSEMBLY_EXCEPTION LICENSE README
%{_javadir}/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8-2m)
- full rebuild for mo7 release

* Fri Feb 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.8-1m)
- update to 6.8

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.7.1-1m)
- sync with Fedora 12 (6.7.1-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 6.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Dec 24 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.5-1
- New vesion of sources
- Call of the brp-java-repack-jars script is disabled

* Wed Aug 13 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-2
- java-devel & jpackage-utils are added as the build requirements
- jpackage-utils is added as the run-time requirement
- An appropriate value of Group Tag is chosen from the official list
- The package documentation is added

* Fri Jul 11 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-1
- Use ant build.xml updated for 6.1
- Sources for 6.1
- Changing for Fedora
* Wed Jan 23 2008 Jaroslav Tulach <jtulach@mandriva.org> 6.0-3mdv2008.1
+ Revision: 157149
- Updated to new upstream source package

  + Olivier Blin <oblin@mandriva.com>
    - restore BuildRoot

  + Thierry Vignaud <tvignaud@mandriva.com>
    - kill re-definition of %%buildroot on Pixel's request

* Sun Dec 16 2007 Anssi Hannula <anssi@mandriva.org> 0:6.0-2mdv2008.1
+ Revision: 120969
- buildrequire java-rpmbuild, i.e. build with icedtea on x86(_64)

* Tue Dec 11 2007 Jaroslav Tulach <jtulach@mandriva.org> 0:6.0-1mdv2008.1
+ Revision: 117585
- Initial version of the NetBeans javaparser library
- create libnb-javaparser
