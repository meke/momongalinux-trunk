%global	momorel 2
%global with_gweather 1

%define glib2_version 2.22.0
%define pango_version 1.2.0
%define gtk3_version 3.5.18
# %define libgnome_version 2.8.0
# %define libgnomeui_version 2.8.0
# %define libglade_version 2.4.0
%define gnome_panel_version 3.6.0
%define libgtop2_version 2.12.0
# %define gail_version 1.2.0
# %define libbonoboui_version 2.3.0
%define gstreamer_version 1.0.0
%define gstreamer_plugins_version 1.0.0
# %define gstreamer_plugins_good_version 1.0.0
%define libxklavier_version 4.0
%define libwnck_version 2.91.0
# %define libgnome_desktop_version 2.11.1
# %define gnome_utils_version 2.8.1
%define dbus_version 1.1.2
%define dbus_glib_version 0.74
%define libnotify_version 0.7
%define pygobject_version 2.26
# %define pygtk_version 2.6
# %define gnome_python_version 2.10
%define gconf_version 2.14.0
# %define libgnomekbd_version 2.27.4
%define libgweather_version 3.6.0

%define po_package gnome-applets-2.0

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define build_stickynotes 0

Summary:        Small applications for the GNOME panel
Name:		gnome-applets
Version:	3.6.0
Release: 	%{momorel}m%{?dist}
License:	GPLv2+ and GFDL
Group:          User Interface/Desktops
URL:		http://www.gnome.org/
# VCS: git:git://git.gnome.org/gnome-applets
Source:		http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
#NoSource: 0
#Patch1: gnome-applets-cpupower.patch

BuildRequires:  glib2-devel >= %{glib2_version}
BuildRequires:  gtk3-devel >= %{gtk3_version}
BuildRequires:  gnome-panel-devel >= %{gnome_panel_version}
BuildRequires:  libgtop2-devel >= %{libgtop2_version}
BuildRequires:  pango-devel >= %{pango_version}
BuildRequires:  libxklavier-devel >= %{libxklavier_version}
BuildRequires:  gstreamer1-devel >= %{gstreamer_version}
BuildRequires:  gstreamer1-plugins-base-devel >= %{gstreamer_plugins_version}
#BuildRequires:  gstreamer-plugins-good-devel >= %{gstreamer_plugins_good_version}
BuildRequires:  /usr/bin/automake
BuildRequires:  libwnck3-devel >= %{libwnck_version}
BuildRequires:  libnotify-devel >= %{libnotify_version}
BuildRequires:  pygobject2-devel >= %{pygobject_version}
BuildRequires:  gucharmap-devel
BuildRequires:  dbus-devel >= %{dbus_version}
BuildRequires:  dbus-glib-devel >= %{dbus_glib_version}
BuildRequires:  xorg-x11-proto-devel
BuildRequires:  gnome-doc-utils
BuildRequires:  which
BuildRequires:  libtool autoconf gettext intltool
BuildRequires:  pkgconfig
BuildRequires:  gnome-icon-theme
BuildRequires:  libxslt
BuildRequires:  NetworkManager-devel
%if %{with_gweather}
BuildRequires:  libgweather-devel >= %{libgweather_version}
%endif
# For cpufreq
BuildRequires:  cpufrequtils

BuildRequires:  autoconf automake libtool

Requires:	gnome-panel >= %{gnome_panel_version}
Requires:	libxklavier >= %{libxklavier_version}
Requires:	gstreamer1-plugins-base >= %{gstreamer_plugins_version}
#Requires:	gstreamer-plugins-good >= %{gstreamer_plugins_good_version}

Requires:	dbus >= %{dbus_version}

Requires(pre): GConf2 >= %{gconf_version}
Requires(preun): GConf2 >= %{gconf_version}
Requires(post): GConf2 >= %{gconf_version}

Obsoletes:      battstat_applet
Obsoletes:      gnome-cpufreq-applet

# since we are installing .pc files
Requires:	pkgconfig

%description
The gnome-applets package contains small applications which generally
run in the background and display their output to the GNOME  panel.
It includes a clock, a character palette, load monitors, little toys,
and more.

%prep
%setup -q
sh ./autogen.sh


%build
%configure			\
	--enable-suid=no 	\
	--disable-battstat	\
	--disable-scrollkeeper 	\
	--enable-mini-commander \
	--enable-gtk-doc	\
	--without-hal

# drop unneeded direct library deps with --as-needed
# libtool doesn't make this easy, so we do it the hard way
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' -e 's/    if test "$export_dynamic" = yes && test -n "$export_dynamic_flag_spec"; then/      func_append compile_command " -Wl,-O1,--as-needed"\n      func_append finalize_command " -Wl,-O1,--as-needed"\n\0/' libtool

make %{?_smp_mflags}

%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot}
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

%find_lang %{po_package} --all-name --with-gnome

# Clean up unpackaged files
rm -rf %{buildroot}%{_localstatedir}/scrollkeeper
rm -f %{buildroot}%{_libdir}/libgweather.la
rm -f %{buildroot}%{_libdir}/libgweather.a

# drop non-XKB support files
rm -rf %{buildroot}%{_datadir}/xmodmap

%if ! %{build_stickynotes}
	rm -f %{buildroot}%{_libexecdir}/stickynotes_applet
%endif


%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule                                        \
	    %{_sysconfdir}/gconf/schemas/charpick.schemas                  \
	    %{_sysconfdir}/gconf/schemas/cpufreq-applet.schemas            \
	    %{_sysconfdir}/gconf/schemas/drivemount.schemas                \
	    %{_sysconfdir}/gconf/schemas/geyes.schemas                     \
%if %{with_gweather}
	    %{_sysconfdir}/gconf/schemas/gweather.schemas                  \
%endif
	    %{_sysconfdir}/gconf/schemas/mini-commander-global.schemas     \
	    %{_sysconfdir}/gconf/schemas/mini-commander.schemas            \
%if %{build_stickynotes}
	    %{_sysconfdir}/gconf/schemas/stickynotes.schemas               \
%endif
	    %{_sysconfdir}/gconf/schemas/multiload.schemas > /dev/null
%{_libexecdir}/gnome-applets/mc-install-default-macros >& /dev/null || :

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule                                    \
	    %{_sysconfdir}/gconf/schemas/battstat.schemas                  \
	    %{_sysconfdir}/gconf/schemas/charpick.schemas                  \
	    %{_sysconfdir}/gconf/schemas/cpufreq-applet.schemas            \
	    %{_sysconfdir}/gconf/schemas/drivemount.schemas                \
	    %{_sysconfdir}/gconf/schemas/geyes.schemas                     \
	    %{_sysconfdir}/gconf/schemas/gswitchit.schemas                 \
%if %{with_gweather}
	    %{_sysconfdir}/gconf/schemas/gweather.schemas                  \
%endif
	    %{_sysconfdir}/gconf/schemas/mini-commander-global.schemas     \
	    %{_sysconfdir}/gconf/schemas/mini-commander.schemas            \
%if %{build_stickynotes}
	    %{_sysconfdir}/gconf/schemas/stickynotes.schemas               \
%endif
	    %{_sysconfdir}/gconf/schemas/multiload.schemas >& /dev/null || :
  if [ -f %{_sysconfdir}/gconf/schemas/battstat.schemas ]; then
    gconftool-2 --makefile-uninstall-rule 				\
	    %{_sysconfdir}/gconf/schemas/battstat.schemas >& /dev/null || :
  fi
  if [ -f %{_sysconfdir}/gconf/schemas/mixer.schemas ]; then
     gconftool-2 --makefile-uninstall-rule 				\
     	     %{_sysconfdir}/gconf/schemas/mixer.schemas >& /dev/null || :
  fi
fi

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule                                    \
	    %{_sysconfdir}/gconf/schemas/charpick.schemas                  \
	    %{_sysconfdir}/gconf/schemas/cpufreq-applet.schemas            \
	    %{_sysconfdir}/gconf/schemas/drivemount.schemas                \
	    %{_sysconfdir}/gconf/schemas/geyes.schemas                     \
%if %{with_gweather}
	    %{_sysconfdir}/gconf/schemas/gweather.schemas                  \
%endif
	    %{_sysconfdir}/gconf/schemas/mini-commander-global.schemas     \
	    %{_sysconfdir}/gconf/schemas/mini-commander.schemas            \
%if %{build_stickynotes}
	    %{_sysconfdir}/gconf/schemas/stickynotes.schemas               \
%endif
	    %{_sysconfdir}/gconf/schemas/multiload.schemas >& /dev/null || :
fi

%postun
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%files -f %{po_package}.lang
%doc AUTHORS COPYING NEWS README
%{_datadir}/pixmaps/*
%{_datadir}/icons/hicolor/16x16/apps/*
%{_datadir}/icons/hicolor/22x22/apps/*
%{_datadir}/icons/hicolor/24x24/apps/*
%{_datadir}/icons/hicolor/32x32/apps/*
%{_datadir}/icons/hicolor/48x48/apps/*
%{_datadir}/icons/hicolor/scalable/apps/*
# %{_datadir}/gnome-2.0/ui/*
%{_datadir}/gnome-applets
%{_bindir}/*
# %{_libdir}/bonobo/servers/*
%{python_sitelib}/invest/
%{_libexecdir}/accessx-status-applet
%{_libexecdir}/charpick_applet2
%{_libexecdir}/cpufreq-applet
%{_libexecdir}/drivemount_applet2
%{_libexecdir}/geyes_applet2
%{_libexecdir}/gnome-applets/
%if %{with_gweather}
%{_libexecdir}/gweather-applet-2
%{_datadir}/glib-2.0/schemas/org.gnome.applets.GWeatherApplet.gschema.xml
%endif
%{_libexecdir}/mini_commander_applet
# %{_libexecdir}/modemlights_applet2
%{_libexecdir}/multiload-applet-2
%{_libexecdir}/null_applet

%if %{build_stickynotes}
%{_libexecdir}/stickynotes_applet
%endif

%{_libexecdir}/trashapplet
%{_libexecdir}/invest-applet
%{_sysconfdir}/gconf/schemas/*
%{_sysconfdir}/dbus-1/system.d/org.gnome.CPUFreqSelector.conf
%{_datadir}/polkit-1/actions/org.gnome.cpufreqselector.policy
%{_datadir}/dbus-1/system-services/org.gnome.CPUFreqSelector.service
%{_datadir}/dbus-1/services/*
%{_datadir}/gnome-panel/4.0/applets/*

%changelog
* Sun Oct 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.0-2m)
- revise some BR versions

* Sat Oct 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Tue Sep  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.1-2m)
- rebuild for pygobject2-devel

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.1-1m)
- import from fedora
-- disable gweather for a while

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-4m)
- rebuild for glib 2.33.2

* Mon Feb 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.1-3m)
- rebuild without hal

* Sun Jan  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-2m)
- remove BR hal-devel

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.1.1-5m)
- rebuild against python-2.7

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1.1-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1.1-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1.1-1m)
- update to 2.32.1.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.0-7m)
- fix BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-6m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-5m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-4m)
- add Requires: GConf2

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-3m)
- build fix 

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- build fix gcc-4.4.4

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.5-1m)
- update to 2.29.5

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-6m)
- rebuild against libxklavier-5.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-3m)
- build with polkit-gnome

* Sat Sep 26 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.28.0-2m)
- replace %%{python_sitearch} to %%{python_sitelib}

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
-- disable-polkit (need upgrade polkit)

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-2m)
- rebuild against libxklavier-4.0

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-2m)
- add--enable-mixer-applet

* Wed Feb 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3.1-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3.1-1m)
- update to 2.24.3.1

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild agaisst python-2.6.1-2m

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Mon Sep 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0.1-1m)
- update to 2.24.0.1

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.3-2m)
- change BuildPrereq: gst-plugins-base-devel to gstreamer-plugins-base-devel

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0
- delete devel package

* Sun Feb 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-2m)
- rebuild against libxklavier-3.4

* Mon Jan  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-2m)
- rebuild against libwnck

* Sun Sep 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Sun Aug 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-2m)
- fix %%files

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Sun Mar 11 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.16.2-3m)
- add python libdir macro and modify python libdir

* Sat Mar  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.90-2m)
- good-bye gnome-system-tools

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.90-1m)
- update to 2.17.90

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.1-4m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.1-3m)
- revice %%post script (gconftool-2)

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.1-2m)
- fix %%postun script

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.1-1m)
- update to 2.17.1 (unstable)

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.16.2-2m)
- rebuild against python-2.5-9m
- delete configure.patch

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.16.1-4m)
- add BuildPrereq: gnome-python-desktop-applet

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16.1-3m)
- rebuild against python-2.5

* Mon Nov  6 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.16.1-2m)
- rebuild against cpufreq-utils

* Fri Oct  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update 2.16.1

* Wed Sep 13 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.16.0.1-2m)
- add configure patch for finding site-packages dir on lib64 env.

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0.1-1m)
- update to 2.16.1

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-2m)
- rebuild against dbus 0.92

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Tue Jun 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-2m)
- delete libtool library *.la

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu May 25 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1-2m)
- add patch's
- pretty spec file.
#'

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1-2m)
- rebuild against dbus-0.61

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Tue Mar  7 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.12.3-3m)
- add BuildPrereq: gstreamer-devel

* Sat Feb 25 2006 mutecat <mutecat@momonga-linux.org>
- (2.12.3-2m)
- arrange ppc.

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Tue Jan  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-2m)
- rebuild against dbus-0.60-2m

* Sun Dec 4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up
- comment out 2 patches (for utf-8)
- GNOME 2.12.1 Desktop

* Tue Sep  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.2-5m)
- rebuild (mixer_applet2) against gstreamer-0.8.11-1m

* Fri Mar  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.2-4m)
- rebuild (mixer_applet2) against gstreamer-0.8.9-1m

* Thu Mar  3 2005 Shotaro Kamio <skamio@momonga-linux.org>
- (2.8.2-3m)
- bug fix: convert the title string for notes to utf-8 in stickynotes_applet (gnome-applets-2.8.2-stickynotes-title-utf8.patch).

* Thu Feb 17 2005 Shotaro Kamio <skamio@momonga-linux.org>
- (2.8.2-2m)
- bug fix of localized messages for stickynotes_applet (gnome-applets-2.8.2-stickynotes-ja_po.patch).

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.2-1m)
- version 2.8.2
- GNOME 2.8 Desktop

* Fri Jan  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.2.1-4m)
- rebuild (mixer_applet2) against gstreamer-0.8.8-1m

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.2.1-3m)
- rebuild (mixer_applet2) against gstreamer-0.8.7-1m

* Sun Sep 19 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.6.2.1-2m)
- rebuild (mixer_applet2) against gstreamer-0.8.5-1m

* Fri Aug 13 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.6.2.1-1m)
- ver up (fix the volume control applet (i.e., mixer_applet2).)

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (2.6.0-2m)
- revised spec for rpm 4.2.

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.2-2m)
- revised spec for enabling rpm 4.2.

* Thu Feb 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sat Jan 03 2004 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-4m)
- gweather.

* Fri Dec 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.1-3m)
- rebuild against libgtop-2.0.7

* Tue Oct 28 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-2m)
- add %%preun script.
- pretty spec file.

* Wed Sep 24 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Wed Aug 27 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.6-2m)
- rebuild against libgtop-2.0.4

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-1m)
- version 2.3.6

* Mon Jun 30 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Mon Jun 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-3m)
- rebuild against for libgtop

* Wed May 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-2m)
- remove gweather_applet-it.omf,mixer_applet-it.omf

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Tue Apr 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Fri Mar 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0-2m)
  rebuild against openssl 0.9.7a

* Tue Jan 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Sun Dec 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.2-2m)
- disable cdplayer applet on ppc

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Sat Sep 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-5m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-4m)
- rebuild against for gnome-panel-2.0.3

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-3m)
- rebuild against for gnome-desktop-2.0.4
- rebuild against for gnome-session-2.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-2m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Mon Jul 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-18m)
- add omf patch
- fix post script

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-17m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-16k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-14k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-12k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-10k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.105.0-16k)
- rebuild against for gnome-desktop-2.0.0
- rebuild against for gnome-session-2.0.0
- rebuild against for gnome-panel-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.105.0-14k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.105.0-12k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.105.0-10k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.105.0-8k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.105.0-6k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (1.105.0-4k)
- rebuild against for libgtkhtml-1.99.9
- rebuild against for libzvt-1.117.0
- rebuild against for gdm-2.3.90.5
- rebuild against for yelp-0.10
- rebuild against for eel-1.1.17
- rebuild against for nautilus-1.1.19
- rebuild against for gnome-desktop-1.5.22
- rebuild against for gnome-session-1.5.21
- rebuild against for gnome-panel-1.5.24

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.105.0-2k)
- version 1.105.0

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.104.0-2k)
- version 1.104.0

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.103.0-2k)
- version 1.103.0

* Mon May 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.102.1-4k)
- rebuild against for glade-1.1.0
- rebuild against for gnome-desktop-1.5.20
- rebuild against for gnome-panel-1.5.22
- rebuild against for gnome-system-monitor-1.1.7
- rebuild against for libwnck-0.12

* Wed May 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.102.1-2k)
- version 1.102.1

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.102.0-2k)
- version 1.102.0

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.101.0-4k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.101.0-2k)
- version 1.101.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.100.0-2k)
- version 1.100.0

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.0-4k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.0-2k)
- version 1.99.0

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.98.0-10k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.98.0-8k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.98.0-6k)
- rebuild against for gtk+-2.0.2

* Tue Apr 02 2002 Shingo Akagaki <dora@kondara.org>
- (1.98.0-4k)
- rebuild against for gnome-desktop-1.5.15
- rebuild against for gnome-panel-1.5.16
- rebuild against for gnome-session-1.5.15

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.98.0-2k)
- version 1.98.0

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.97.0-4k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.97.0-2k)
- version 1.97.0

* Tue Mar 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.96.0-6k)
- rebuild against for gnome-panel-1.5.13

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.96.0-4k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.96.0-2k)
- version 1.96.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.95.0-16k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.95.0-14k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.95.0-12k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.95.0-10k)
- change schema handring

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.95.0-8k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.95.0-6k)
- rebuild against for libwnck-0.6

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.95.0-4k)
- rebuild against for yelp-0.3
- rebuild against for libgnomeui-1.112.1
- rebuild against for nautilus-1.1.7

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.95.0-2k)
- version 1.95.0

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-6k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-4k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.94.0-2k)
- version 1.94.0

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-12k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-10k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-8k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-6k)
- rebuild against for libwnck-0.5

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-4k)
- rebuild against for gedit2-1.112.0
- rebuild against for gnome-desktop-1.5.10
- rebuild against for gnome-session-1.5.10
- rebuild against for gnome-panel-1.5.10

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.93.0-2k)
- version 1.93.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.2-12k)
- rebuild against for libwnck-0.4

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.2-10k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-8k)
- remove gnome-core require
- add gnome-desktop require

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.2-6k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.2-4k)
- rebuild against for libzvt-1.111.0
- rebuild against for metatheme-0.9.3

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.2-2k)
- version 1.92.2

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.1-6k)
- rebuild against for libbonoboui-1.111.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.1-4k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.92.1-2k)
- version 1.92.1
* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.1-14k)
- rebuild against for librsvg-1.1.3
- rebuild against for eel-1.1.4
- rebuild against for nautilus-1.1.5
- rebuild against for libgtop-1.90.2
- rebuild against for gnome-system-monitor-1.1.4
- rebuild against for gnome-utils-1.99.0

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.1-12k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.1-10k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.1-8k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.1-6k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.1-4k)
- rebuild against for gnome-core-1.5.7

* Mon Feb  4 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.1-2k)
- version 1.91.1

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.0-8k)
- rebuild against for gnome-vfs-1.9.5

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.0-6k)
- rebuild against for gnome-core-1.5.6

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.0-4k)
- rebuild against for gtk+-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.91.0-2k)
- version 1.91.0
- rebuild against for gnome-core-1.5.5
- rebuild against for libgnomecanvas-1.110.0

* Tue Jan 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.90.1-2k)
- release version

* Mon Jan  7 2002 Shingo Akagaki <dora@kondara.org>
- (2.0-0.02002010702k)
- cvs version 2.0
- gnome2 env

* Mon Dec 17 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.4-22k)
- add omfenc patch

* Mon Dec 17 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.4-20k)
- bug fix slash_app. thanks to zoe.
  (http://www.kasumi.sakura.ne.jp/~zoe/tdiary/patch/slashapp.patch)

* Thu Dec 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.4-18k)
- add japanese transrated manual

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (1.4.0.4-16k)
- move zh_CN.GB2312 => zh_CN

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.4-14k)
- gnome-filesystemized

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.4.0.4-8k)
- rebuild against gettext 0.10.40.

* Mon Oct 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.4.0.4-6k)
- slashapp4 bug fix TT

* Mon Oct 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.4.0.4-4k)
- slashapp4

* Thu Sep 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.4.0.4-2k)
- version 1.4.0.4

* Mon Sep 10 2001 Toru Hoshina <t@kondara.org>
- (1.4.0.3-4k)
- no batt support for ppc :-P

* Tue Sep  4 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.0.3-2k)
- version 1.4.0.3

* Fri Apr 20 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- add kondara-slashapp2.patch written by famao@kondara.org

* Thu Apr 19 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- add kondara-slashapp.patch written by famao@kondara.org

* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.0.1

* Wed Mar 21 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.0

* Mon Mar 19 2001 Shingo Akagaki <dora@kondara.org>
- version 1.3.0
- K2K

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.4-5k)
- modified Docdir with macros

* Thu Nov 16 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.4

* Thu Oct 12 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.2

* Mon Jul 10 2000 Shingo Akagaki <dora@kondara.org>
- rebuild against libpng-1.0.7 

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Jun 12 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.1

* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.0

* Sat May 13 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.90

* Mon Apr 10 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.9

* Mon Apr 10 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.8

* Wed Mar 15 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.5

* Mon Mar  6 2000 Akira Higuchi <a@kondara.org>
- fixed a minor bug

* Tue Feb 22 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.4

* Fri Feb 18 2000 Shingo Akagaki <dora@kondara.org>
- add many many fix

* Mon Feb 14 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.3
- gnotes, mini-commander style patch

* Tue Feb  1 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.2

* Thu Jan 27 2000 Shingo Akagaki <dora@kondara.org>
- version 1.1.1

* Wed Oct 24 1999 Shingo Akagaki <dora@kondara.org>
- first release
