%global momorel 1
%global do_test 1

Summary: A utility for getting files from remote servers (FTP, HTTP, and others)
Name: curl
Version: 7.36.0
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Applications/Communications
URL: http://curl.haxx.se/
Source: http://curl.haxx.se/download/%{name}-%{version}.tar.lzma
NoSource: 0

Source2: curlbuild.h

# patch making libcurl multilib ready
Patch101: 0101-curl-7.32.0-multilib.patch

# prevent configure script from discarding -g in CFLAGS (#496778)
Patch102: 0102-curl-7.32.0-debug.patch

# make the curl tool link SSL libraries also used by src/tool_metalink.c
Patch103: 0103-curl-7.32.0-metalink.patch

# use localhost6 instead of ip6-localhost in the curl test-suite
Patch104: 0104-curl-7.19.7-localhost6.patch

# disable valgrind for certain test-cases (libssh2 problem)
Patch106: 0106-curl-7.21.0-libssh2-valgrind.patch

# work around valgrind bug (#678518)
Patch107: 0107-curl-7.21.4-libidn-valgrind.patch

# Fix character encoding of docs, which are of mixed encoding originally so
# a simple iconv can't fix them
Patch108: 0108-curl-7.32.0-utf8.patch

Provides: webclient
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: momonga-rpmmacros >= 20040310-6m
BuildRequires: c-ares-devel
BuildRequires: pkgconfig, libidn-devel, zlib-devel, libssh2-devel
BuildRequires: nss-devel >= 3.11.7-7, openldap-devel, krb5-devel
BuildRequires: libidn-devel, libssh2-devel
BuildRequires: libcom_err-devel
BuildRequires: openssl-devel >= 1.0.0

%description
cURL is a tool for getting files from HTTP, FTP, FILE, LDAP, LDAPS,
DICT, TELNET and TFTP servers, using any of the supported protocols.
cURL is designed to work without user interaction or any kind of
interactivity. cURL offers many useful capabilities, like proxy support,
user authentication, FTP upload, HTTP post, and file transfer resume.

%package -n libcurl
Summary: A library for getting files from web servers
Group: Development/Libraries

%description -n libcurl
This package provides a way for applications to use FTP, HTTP, Gopher and
other servers for getting files.

%package -n libcurl-devel
Summary: Files needed for building applications with libcurl
Group: Development/Libraries
Requires: libcurl = %{version}-%{release}
Requires: libidn-devel, pkgconfig, automake
Provides: curl-devel = %{version}-%{release}
Obsoletes: curl-devel < %{version}-%{release}

%description -n libcurl-devel
cURL is a tool for getting files from FTP, HTTP, Gopher, Telnet, and
Dict servers, using any of the supported protocols. The libcurl-devel
package includes files needed for developing applications which can
use cURL's capabilities internally.

%prep
%setup -q

# upstream patches

# Fedora patches
%patch101 -p1
%patch102 -p1
%patch103 -p1
%patch104 -p1
%patch106 -p1
%patch107 -p1
%patch108 -p1

# replace hard wired port numbers in the test suite (this only boosts test
# coverage by enabling tests that would otherwise be disabled due to using
# runtests.pl -b)
cd tests/data/
sed -i s/899\\\([0-9]\\\)/%{?__isa_bits}9\\1/ test{309,1028,1055,1056}
cd -

# disable test 1112 (#565305)
printf "1112\n" >> tests/data/DISABLED

%build
unset KRB5_PREFIX
[ -x /usr/kerberos/bin/krb5-config ] && KRB5_PREFIX="=/usr/kerberos"
%configure \
    --disable-static \
    --enable-hidden-symbols \
    --enable-ipv6 \
    --enable-ldaps \
    --enable-manual \
    --enable-threaded-resolver \
    --with-ca-bundle=%{_sysconfdir}/pki/tls/certs/ca-bundle.crt \
    --with-gssapi${KRB5_PREFIX} \
    --with-libidn \
    --with-libmetalink \
    --with-libssh2 \
    --without-ssl \
    --with-nss \
    LIBS="-lrt"

# Remove bogus rpath
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%if %{do_test}
%check
LD_LIBRARY_PATH=$RPM_BUILD_ROOT%{_libdir}
export LD_LIBRARY_PATH
cd tests
make %{?_smp_mflags}

### FIX ME !!
# use different port range for 32bit and 64bit build, thus make it possible
# to run both in parallel on the same machine
./runtests.pl -a -b%{?__isa_bits}90 -p -v ||:
%endif

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make DESTDIR=$RPM_BUILD_ROOT INSTALL="%{__install} -p" install

rm -f %{buildroot}%{_libdir}/libcurl.la
install -d $RPM_BUILD_ROOT/%{_datadir}/aclocal
install -m 644 docs/libcurl/libcurl.m4 $RPM_BUILD_ROOT/%{_datadir}/aclocal

# don't need curl's copy of the certs; use openssl's
find %{buildroot} -name ca-bundle.crt -exec rm -f '{}' \;

# drop man page for a script we do not distribute
rm -f ${RPM_BUILD_ROOT}%{_mandir}/man1/mk-ca-bundle.1

# Make libcurl-devel multilib-ready (bug #488922)
%if 0%{?__isa_bits} == 64
%define _curlbuild_h curlbuild-64.h
%else
%define _curlbuild_h curlbuild-32.h
%endif
mv $RPM_BUILD_ROOT%{_includedir}/curl/curlbuild.h \
   $RPM_BUILD_ROOT%{_includedir}/curl/%{_curlbuild_h}

install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_includedir}/curl/curlbuild.h

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post -n libcurl -p /sbin/ldconfig

%postun -n libcurl -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc CHANGES README* COPYING
%doc docs/BUGS docs/FAQ docs/FEATURES
%doc docs/MANUAL docs/RESOURCES
%doc docs/TheArtOfHttpScripting docs/TODO
%attr(0755,root,root) %{_bindir}/curl
%attr(0644,root,root) %{_mandir}/man1/curl.1*

%files -n libcurl
%{_libdir}/libcurl.so.*

%files -n libcurl-devel
%defattr(-,root,root)
%doc docs/examples/*.c docs/examples/Makefile.example docs/INTERNALS
%doc docs/CONTRIBUTE docs/libcurl/ABI
%attr(0755,root,root) %{_bindir}/curl-config
%attr(0644,root,root) %{_mandir}/man1/curl-config.1*
%attr(0644,root,root) %{_mandir}/man3/*
%{_includedir}/curl
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libcurl.so
#%%{_libdir}/libcurl.a
%{_datadir}/aclocal/libcurl.m4

%changelog
* Tue Apr 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.36.0-1m)
- [SECURITY] CVE-2014-0138 CVE-2014-0139 CVE-2014-1263 CVE-2014-2522
- update to 7.36.0

* Sun Feb  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.35.0-1m)
- [SECURITY] CVE-2014-0015
- update to 7.35.0

* Thu Dec 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.34.0-1m)
- [SECURITY] CVE-2013-6422
- update to 7.34.0

* Mon Nov 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.33.0-1m)
- [SECURITY] CVE-2013-4545
- update to 7.33.0

* Mon Sep 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.32.0-1m)
- update to 7.32.0

* Fri Jun 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.31.0-1m)
- [SECURITY] CVE-2013-2174
- update to 7.31.0

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.30.0-1m)
- [SECURITY] CVE-2013-1944
- update to 7.30.0

* Mon Feb 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.29.0-2m)
- fix a SIGSEGV when closing an unused multi handle (#914411)

* Tue Feb 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.29.0-1m)
- [SECURITY] CVE-2013-0249
- update to 7.29.0

* Sun Jul 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.27.0-1m)
- update to 7.27.0

* Sat Mar 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.25.0-1m)
- update to 7.25.0

* Wed Feb  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.24.0-1m)
- [SECURITY] CVE-2012-0036
- [SECURITY] http://curl.haxx.se/docs/adv_20120124B.html (CVE-2011-3389)
- update to 7.24.0

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.23.1-1m)
- update to 7.23.1

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.22.0-1m)
- update to 7.22.0

* Mon Jul 18 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.21.7-2m)
- fix build failure with new krb5-1.9.1

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.21.7-1m)
- [SECURITY] CVE-2011-2192
- update to 7.21.7

* Sun May 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.21.6-1m)
- update to 7.21.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.21.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.21.0-6m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.21.0-5m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.21.0-4m)
- full rebuild for mo7 release

* Thu Jun 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.21.0-3m)
- force rebuild with krb5 not libgssglue
- option --with-gssapi is using libgssapi_krb5 of krb5-libs
- Fedora's krb5 moved from %%{_prefix}/kerberos to %%{_prefix}
- and Fedora's curl links krb5-libs installed to %%{_prefix}
- but Momonga's curl links krb5-libs installed to %%{_prefix}/kerberos yet
- DO NOT FORGET change option from "--with-gssapi=%%{_prefix}/kerberos" to "--with-gssapi", if krb5 will be moved to %%{_prefix}

* Thu Jun 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.21.0-2m)
- force rebuild with libgssglue not libgssapi

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.21.0-1m)
- update 7.21.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.7-6m)
- rebuild against openssl-1.0.0

* Sat Mar  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.19.7-5m)
- increase Release for upgrading from STABLE_6

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.7-4m)
- [SECURITY] CVE-2010-0734
- apply the upstream patch

* Sun Nov 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.7-3m)
- enable SSL (use nss)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.7-1m)
- update to 7.19.7

* Sun Aug 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.6-1m)
- [SECURITY] CVE-2009-2417
- updat to 7.19.6

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.5-1m)
- updat to 7.19.5

* Wed Mar  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.4-1m)
- [SECURITY] CVE-2009-0037
- update to 7.19.4
- drop meaningless Patch10,11

* Sat Feb  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.3-1m)
- update to 7.19.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.2-3m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.2-2m)
- drop Patch4 for fuzz=0, already merged upstream

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.2-1m)
- update to 7.19.2

* Mon Nov 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.1-1m)
- update to 7.19.1
- drop Patch3 and Patch5, merged upstream

* Sat Oct  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.18.2-2m)
- import thread safety patch from fedora (7.18.2-7)
-- this patch fixes firefox's segmentation fault

* Tue Jul  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.18.2-1m)
- update to 7.18.2

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.18.0-5m)
- add BuildPreReq: libssh2-devel

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (7.18.0-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.18.0-3m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.18.0-2m)
- rebuild against openldap-2.4.8

* Sun Feb 10 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (7.18.0-1m)
- up to 7.18.0

* Sun Feb 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.16.4-2m)
- build without libssh2 (--without-libssh2)

* Sat Jul 21 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.16.4-1m)
- [SECURITY] CVE-2007-3564
- update to 7.16.4

* Mon Jul  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.16.3-1m)
- update to 7.16.3

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (7.16.2-1m)
- update to 7.16.2 (sync with FC-devel)

* Sat Nov 11 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (7.16.0-1m)
- update to 7.16.0
- sync with Fedora Core (curl-7.16.0-2.src.rpm)
-
- * Tue Oct 31 2006 Jindrich Novy <jnovy@redhat.com> - 7.16.0-2
- - fix BuildRoot
- - add Requires: pkgconfig for curl-devel
- - move LDFLAGS and LIBS to Libs.private in libcurl.pc.in (#213278)
- * Mon Mar 20 2006 Ivana Varekova <varekova@redhat.com> - 7.15.3-1
- - fix multilib problem using pkg-config
- - update to 7.15.3
- * Wed Nov 30 2005 Ivana Varekova <varekova@redhat.com> 7.15.0-3
- - fix curl-config bug 174556 - missing vernum value

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (7.15.3-2m)
- rebuild against openssl-0.9.8a

* Mon Mar 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.15.3-1m)
- update to 7.15.3
- [SECURITY] CVE-2006-1061
- http://curl.haxx.se/docs/adv_20060320.html

* Sat Mar 18 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.15.2-1m)
- update to 7.15.2
- [SECURITY] CVE-2005-4077
- http://curl.haxx.se/docs/adv_20051207.html

* Sat Mar 19 2005 TAKAHASHI Tamotsu <tamo>
- (7.13.1-1m)
- [SECURITY] NTLM/krb4 buffer overflow (CAN-2005-0490)

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.13.0-1m)
- update to 7.13.0
- update Patch0: curl-7.13.0-libdir.patch

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (7.12.2-2m)
- enable x86_64.

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (7.12.2-1m)
- update to 7.12.2

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.10.5-4m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (7.10.5-3m)
- revised spec for enabling rpm 4.2.

* Fri Oct 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.10.5-2m)
- change License: to MIT/X

* Tue Jul  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (7.10.5-1m)
- minor bugfixes

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (7.10.2-3m)
  rebuild against openssl 0.9.7a

* Fri Jan 10 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (7.10.2-2m)
- Fix License and doc files 

* Fri Jan 10 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (7.10.2-1m)
- update to 7.10.2

* Thu Aug 1 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (7.9.8-1m)
- update to 7.9.8

* Mon May 20 2002 Masaru Sato <masachan@kondara.org>
- (7.9.7-2k)
- update to 7.9.7

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (7.9.6-2k)
- update to 7.9.6

* Wed Mar 13 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (7.9.5-2k)
- update to 7.9.5

* Tue Dec 25 2001 Masaru Sato <masachan@kondara.org>
- (7.9.2-2k)
- up to 7.9.2

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (7.9-8k)
- No docs could be excutable :-p

* Tue Nov 27 2001 Motonobu Ichimura <famao@kondara.org>
- (7.9-6k)
- bug fixed

* Mon Oct  8 2001 Masaru Sato <masachan@kondara.org>
- (7.9-2k)
- Initial Release from curl offical rpm.

