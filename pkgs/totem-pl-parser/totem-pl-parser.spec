%global momorel 1
Name:		totem-pl-parser
Version:	3.4.3
Release: %{momorel}m%{?dist}
Summary:	Totem Playlist Parser library

Group:		System Environment/Libraries
License:	LGPLv2+
Url:		http://www.gnome.org/projects/totem/
Source0:	http://download.gnome.org/sources/%{name}/3.4/%{name}-%{version}.tar.xz
NoSource: 0
Obsoletes:	totem-plparser

BuildRequires:	glib2-devel
BuildRequires:	gmime-devel
BuildRequires:	libxml2-devel
BuildRequires:	libsoup-devel
BuildRequires:	gobject-introspection-devel
BuildRequires:	gettext
BuildRequires:	libquvi-devel
BuildRequires:	libarchive-devel
BuildRequires:	perl(XML::Parser) intltool

%description
A library to parse and save playlists, as used in music and movie players.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Obsoletes:	totem-devel < 2.21.90
Requires:       %{name} = %{version}-%{release}
Requires:	pkgconfig
Requires:	gobject-introspection-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --enable-static=no
%make 

%install
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%find_lang %{name} --with-gnome

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING.LIB NEWS README
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gtk-doc/html/totem-pl-parser
%{_datadir}/gir-1.0/*.gir

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.3-1m)
- update 3.4.3

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.2-1m)
- reimport from fedora

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.6-5m)
- rebuild for evolution-data-server-devel-3.5.3

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.6-4m)
- rebuild for glib 2.33.2

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.32.6-3m)
- rebuild against libarchive-3.0.4

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.6-2m)
- remove BR hal-devel

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.6-1m)
- update to 2.32.6

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.5-1m)
- update to 2.32.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.4-3m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.4-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.4-1m)
- update to 2.32.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu May 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.1-1m)
- update to 2.29.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-3m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Dec 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.26.1-2m)
- define __libtoolize (build fix)

* Sun May  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.2

* Fri Apr  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Wed Dec 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Thu Oct 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Thu Oct  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Tue Sep 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-2m)
- welcome evolution-data-server (delete patch)

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0
- good-bye evolution-data-server

* Sun Jun  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-2m)
- delete some script in %%post

* Sat May 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.1-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- initial build
