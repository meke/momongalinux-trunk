%global momorel 21

Summary: A set of utilities to manage your TV viewing
Name: xmltv
Version: 0.5.53
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
License: GPLv2+
URL: http://membled.com/work/apps/xmltv/
Source0: http://dl.sourceforge.net/sourceforge/xmltv/%{name}-%{version}.tar.bz2
NoSource: 0
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl-Tk, perl >= 5.10.0
BuildRequires: perl-XML-Twig >= 3.09, perl-XML-Parser >= 2.34
BuildRequires: perl-XML-Writer
BuildRequires: perl-DateManip, perl-libwww-perl >= 5.65
BuildRequires: perl-HTML-Parser >= 3.34, perl-HTML-TableExtract >= 1.08
BuildRequires: perl-Tk-TableMatrix, perl-XML-Simple
BuildRequires: perl-HTML-Tree
BuildRequires: perl-Term-ProgressBar >= 2.00
BuildRequires: perl-Compress-Zlib
BuildRequires: perl-Lingua-EN-Numbers-Ordinate
BuildRequires: perl-Lingua-Preferred
Requires: perl-DateManip >= 5.41, perl-Term-ProgressBar >= 2.00
Requires: perl-HTML-Parser >= 3.34
Requires: perl-XMLTV >= %{version}-%{release}, xmltv-grabbers >= %{version}-%{release}, xmltv-gui >= %{version}-%{release}

%description
XMLTV is a set of utilities to manage your TV viewing. They work with
TV listings stored in the XMLTV format, which is based on XML. The
idea is to separate out the backend (getting the listings) from the
frontend (displaying them for the user), and to implement useful
operations like picking out your favourite programmes as filters that
read and write XML documents.

%package -n perl-XMLTV
Summary: Perl modules for managing your TV viewing
Group: System Environment/Libraries
Requires: perl-XML-Twig >= 3.09, perl-DateManip >= 5.41, 

%description -n perl-XMLTV
XMLTV is a set of utilities to manage your TV viewing. They work with
TV listings stored in the XMLTV format, which is based on XML. The
idea is to separate out the backend (getting the listings) from the
frontend (displaying them for the user), and to implement useful
operations like picking out your favourite programmes as filters that
read and write XML documents.

This package contains the perl modules from xmltv.

%package grabbers
Summary: Backends for xmltv
Group: Applications/Multimedia
Requires: perl-DateManip >= 5.41, perl-libwww-perl >= 5.65, perl-HTML-TableExtract >= 1.08, perl-Term-ProgressBar >= 2.00
Requires: perl-XMLTV >= %{version}-%{release}

%description grabbers
XMLTV is a set of utilities to manage your TV viewing. They work with
TV listings stored in the XMLTV format, which is based on XML. The
idea is to separate out the backend (getting the listings) from the
frontend (displaying them for the user), and to implement useful
operations like picking out your favourite programmes as filters that
read and write XML documents.

This package contains the backends (grabbers) for xmltv.

%package gui
Summary: Graphical frontends to xmltv
Group: Applications/Multimedia
Requires: perl-DateManip >= 5.41, perl-libwww-perl >= 5.65
Requires: perl-XMLTV >= %{version}-%{release}

%description gui
XMLTV is a set of utilities to manage your TV viewing. They work with
TV listings stored in the XMLTV format, which is based on XML. The
idea is to separate out the backend (getting the listings) from the
frontend (displaying them for the user), and to implement useful
operations like picking out your favourite programmes as filters that
read and write XML documents.

This package contains graphical frontends to xmltv.

%prep
%setup -q -n %{name}-%{version}

%build
echo 'yes' | perl Makefile.PL PREFIX=%{buildroot}%{_prefix} INSTALLDIRS=vendor
make

%install
rm -rf %{buildroot}
make install
rm -f %{buildroot}%{perl_archlib}/perllocal.pod
rm -rf %{buildroot}%{perl_vendorarch}/auto/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_docdir}/%{name}-%{version}
%{_bindir}/tv_cat
%{_bindir}/tv_extractinfo_ar
%{_bindir}/tv_extractinfo_en
%{_bindir}/tv_find_grabbers
%{_bindir}/tv_grep
%{_bindir}/tv_imdb
%{_bindir}/tv_remove_some_overlapping
%{_bindir}/tv_sort
%{_bindir}/tv_split
%{_bindir}/tv_to_latex
%{_bindir}/tv_to_text
%{_bindir}/tv_to_potatoe
%{_bindir}/tv_validate_file
%{_bindir}/tv_validate_grabber
%{_mandir}/man1/tv_cat.1*
%{_mandir}/man1/tv_extractinfo_ar.1*
%{_mandir}/man1/tv_extractinfo_en.1*
%{_mandir}/man1/tv_find_grabbers.1*
%{_mandir}/man1/tv_grep.1*
%{_mandir}/man1/tv_imdb.1*
%{_mandir}/man1/tv_remove_some_overlapping.1*
%{_mandir}/man1/tv_sort.1*
%{_mandir}/man1/tv_split.1*
%{_mandir}/man1/tv_to_latex.1*
%{_mandir}/man1/tv_to_text.1*
%{_mandir}/man1/tv_to_potatoe.1*
%{_mandir}/man1/tv_validate_file.1*
%{_mandir}/man1/tv_validate_grabber.1*

%files -n perl-XMLTV
%defattr(-,root,root,-)
%{perl_vendorlib}/XMLTV.pm
%{perl_vendorlib}/XMLTV
%{_mandir}/man1/*
%exclude %{_mandir}/man1/tv_cat.1*
%exclude %{_mandir}/man1/tv_extractinfo_ar.1*
%exclude %{_mandir}/man1/tv_extractinfo_en.1*
%exclude %{_mandir}/man1/tv_find_grabbers.1*
%exclude %{_mandir}/man1/tv_grep.1*
%exclude %{_mandir}/man1/tv_imdb.1*
%exclude %{_mandir}/man1/tv_remove_some_overlapping.1*
%exclude %{_mandir}/man1/tv_sort.1*
%exclude %{_mandir}/man1/tv_split.1*
%exclude %{_mandir}/man1/tv_to_latex.1*
%exclude %{_mandir}/man1/tv_to_text.1*
%exclude %{_mandir}/man1/tv_to_potatoe.1*
%exclude %{_mandir}/man1/tv_validate_file.1*
%exclude %{_mandir}/man1/tv_validate_grabber.1*
%exclude %{_mandir}/man1/tv_grab_*.1*
%exclude %{_mandir}/man1/tv_check.1*
%{_mandir}/man3/*

%files grabbers
%defattr(-,root,root,-)
%{_bindir}/tv_grab_*
%{_mandir}/man1/tv_grab_*.1*
%dir %{_datadir}/xmltv
%{_datadir}/xmltv/tv_grab_*
%{_datadir}/xmltv/xmltv.dtd

%files gui
%defattr(-,root,root,-)
%{_bindir}/tv_check
%{_mandir}/man1/tv_check.1*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.53-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.53-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.53-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.53-6m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.53-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.53-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.5.53-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.53-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.53-1m)
- update to 0.5.53
-- drop Patch0, not needed
-- drop Patch1,2, merged upstream
-- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.42-4m)
- rebuild against gcc43

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.42-3m)
- rebuild against perl-5.10.0

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.42-2m)
- use vendor

* Sun Feb 26 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.42-1m)
- version up

* Fri Apr  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.32-2m)
- remove duplicated files

* Tue Feb  8 2005 Toru Hoshina <t@momonga-linux.org>
- (0.5.32-1m)
- ver up.

* Wed May 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.5.27-1m)
- import from paken.

* Fri Jan 31 2004 Takeru Komoriya <komoriya@paken.org>
- Repackaged based on xmltv-0.5.27-41.rhfc1.at
- added Japanese patch

* Sun Jan  4 2004 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Update to 0.5.26.
- remove patch to try and save tv_grab_de.
- Update to 0.5.27.

* Thu Nov 20 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Update to 0.5.23.

* Sun Nov  9 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.22.

* Tue Nov  4 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.21
- Split into subpackages as suggested by Risto Kankkunen.

* Mon Nov  3 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.20.

* Sun Sep 28 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.19.

* Sun Sep 14 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.18.

* Wed Aug 27 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.17.

* Sat Aug 16 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.16.

* Wed Jul  2 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.14.

* Tue Jul  1 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.13.

* Mon Jun 30 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.12.

* Mon Jun 23 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de> 
- Updated to 0.5.11.

* Sun Apr 27 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Update to 0.5.10.

* Mon Apr  7 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Updated to 0.5.9.
- Adapted to new macros.

* Fri Mar 14 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Fix dependeny to new perl-DateManip package.

* Mon Feb 10 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Upgraded to 0.5.7

* Sat Nov 30 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- add description
- make noarch

* Sat Nov 30 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- fixes reported by author (Ed Avis <ed@membled.com>).
- fix answers to perl Makefile.PL
- add license

* Mon Nov 25 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- upgrade to 0.5.3

* Wed Nov 13 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Initial build.


