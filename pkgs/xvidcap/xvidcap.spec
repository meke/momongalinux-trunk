%global momorel 8

Summary: A screen capture to capture videos off desktop 
Name: xvidcap
Version: 1.1.7
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPLv3+
URL: http://xvidcap.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: xvidcap-1.1.7-xorg75.patch
Requires: dbus
Requires: gtk2
BuildRequires: dbus-devel
BuildRequires: gtk2-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is about a small tool to capture things going on on an X-Windows
display to either individual frames or an MPEG video. On Windows machines there
is a commercial tool called Lotus ScreenCam which does a very good job esp.
for the purpose I was needing such a tool for: Software documentation, esp.
installation procedures and such.

%prep
%setup -q
%patch0 -p1 -b .xorg75

%build
%configure --with-gtk2 LIBS="-lz -lX11 -lXext" CPPFLAGS=-DGLIB_COMPILATION
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# clean up docs
rm -rf %{buildroot}%{_docdir}/%{name}_%{version}
rm -rf %{buildroot}%{_docdir}/%{name}

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS INSTALL README
%{_bindir}/*
%{_datadir}/applications/*
%{_datadir}/dbus-1/services/net.jarre_de_the.Xvidcap.service
%{_datadir}/gnome/help/*
%{_datadir}/omf/*
%{_datadir}/pixmaps/*
%{_datadir}/xvidcap
%{_mandir}/man1/*
%{_mandir}/de/man1/*
%{_mandir}/es/man1/*
%{_mandir}/it/man1/*

%changelog
* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.7-8m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.7-5m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-4m)
- fix build

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.7-3m)
- build fix gcc-4.4.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-0.1.2m)
- rebuild against rpm-4.6

* Sat Jul 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-0.1.1m)
- update to version 1.1.7rc1 (x86_64 build fix)
- use internal ffmpeg
- remove merged installdata.patch

* Tue May 13 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.4-1m)
- import to Momonga

* Tue Oct 31 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.1.4-0.0.1m)
- update to 1.1.4

* Mon Nov 08 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.1.3-0.0.1m)
- update to 1.1.3

* Sun May  2 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.1.2-0.0.2m)
- rebuild against xvid api4 (1.0.0-rc4)

* Sun Jan 11 2004 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.1.2-0.0.1m)
- version 1.1.2

* Mon Oct 27 2003 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.0.19-0.0.1m)
- build for Momonga
