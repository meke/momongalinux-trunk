%global         momorel 4
%global boost_version 1.55.0
Name:           xsd
Version:        3.3.0
Release:        %{momorel}m%{?dist}
Summary:        W3C XML schema to C++ data binding compiler
Group:          Development/Tools
License:        GPLv2
URL:            http://www.codesynthesis.com/products/xsd/
Source0:        http://www.codesynthesis.com/download/xsd/3.3/xsd-%{version}-2+dep.tar.bz2
Nosource:       0
Patch0:         xsd-3.3.0-xsdcxx-rename.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  m4
BuildRequires:  boost-devel >= %{boost_version}
BuildRequires:  xerces-c-devel >= 3.1.1
Requires:       xerces-c-devel

%description
CodeSynthesis XSD is an open-source, cross-platform W3C XML Schema to
C++ data binding compiler. Provided with an XML instance specification
(XML Schema), it generates C++ classes that represent the given
vocabulary as well as parsing and serialization code.
You can then access the data stored in XML using types and functions
that semantically correspond to your application domain rather than
dealing with intricacies of reading and writing XML.

%package        doc
Group:          Documentation
Summary:        API documentation files for %{name}

%description    doc
This package contains API documentation for %{name}.

%prep
%setup -q -n xsd-%{version}-2+dep
pushd xsd
%patch0 -p1 -b .xsdcxx-rename
popd

%build
make verbose=1 CXXFLAGS="$RPM_OPT_FLAGS -g1" LIBS="-lboost_system" BOOST_LINK_SYSTEM=n

%install
rm -rf %{buildroot}
rm -rf apidocdir

make install_prefix="$RPM_BUILD_ROOT%{_prefix}" BOOST_LINK_SYSTEM=n install

# Split API documentation to -doc subpackage.
mkdir apidocdir
mv %{buildroot}%{_datadir}/doc/xsd/*.{xhtml,css} apidocdir/
mv %{buildroot}%{_datadir}/doc/xsd/cxx/ apidocdir/
mv %{buildroot}%{_datadir}/doc/xsd/ docdir/

# Convert to utf-8.
for file in docdir/NEWS; do
    mv $file timestamp
    iconv -f ISO-8859-1 -t UTF-8 -o $file timestamp
    touch -r timestamp $file
done

# Rename binary to xsdcxx to avoid conflicting with mono-web package.
# Sent suggestion to upstream via e-mail 20090707
# they will consider renaming in 4.0.0
mv %{buildroot}%{_bindir}/xsd %{buildroot}%{_bindir}/xsdcxx
mv %{buildroot}%{_mandir}/man1/xsd.1 %{buildroot}%{_mandir}/man1/xsdcxx.1

# Remove duplicate docs.
rm -rf %{buildroot}%{_datadir}/doc/libxsd

# Remove Microsoft Visual C++ compiler helper files.
rm -rf %{buildroot}%{_includedir}/xsd/cxx/compilers

# Remove redundant PostScript files that rpmlint grunts about not being UTF8
# See: https://bugzilla.redhat.com/show_bug.cgi?id=502024#c27
# for Boris Kolpackov's explanation about those
find apidocdir -name "*.ps" | xargs rm -f
# Remove other unwanted crap
find apidocdir -name "*.doxygen" \
            -o -name "makefile" \
            -o -name "*.html2ps" | xargs rm -f

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc docdir/*
%{_bindir}/xsdcxx
%{_includedir}/xsd/
%{_mandir}/man1/xsdcxx.1*

%files doc
%defattr(-,root,root,-)
%doc apidocdir/*

%changelog
* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-4m)
- rebuild against xerces-c-3.1.1

* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.0-3m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-2m)
- rebuild against boost-1.52.0

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-1m)
- import from Fedora

* Wed Apr 18 2012 Kalev Lember <kalevlember@gmail.com> - 3.3.0-12
- Update to xsd-3.3.0-1+dep upstream tarball, which includes the gcc 4.7 patch

* Tue Feb 28 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.3.0-11
- Rebuilt for c++ ABI breakage

* Thu Jan 19 2012 Ralf Corsépius <corsepiu@fedoraproject.org> - 3.3.0-10
- Add xsd-3.3.0-gcc47.patch (Fix mass rebuild FTBFS).

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.3.0-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sat Nov 26 2011 Thomas Spura <tomspur@fedoraproject.org> - 3.3.0-8
- rebuild for https://fedoraproject.org/wiki/Features/F17Boost148

* Fri Jul 22 2011 Antti Andreimann <Antti.Andreimann@mail.ee> - 3.3.0-7
- Rebuilt for boost 1.47.0

* Wed Apr 06 2011 Kalev Lember <kalev@smartlink.ee> - 3.3.0-6
- Rebuilt for boost 1.46.1 soname bump

* Thu Mar 10 2011 Kalev Lember <kalev@smartlink.ee> - 3.3.0-5
- Rebuilt with xerces-c 3.1

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.3.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Feb 06 2011 Thomas Spura <tomspur@fedoraproject.org> - 3.3.0-3
- rebuild for new boost (thanks Petr Machata for the fix)

* Mon Aug 02 2010 Antti Andreimann <Antti.Andreimann@mail.ee> 3.3.0-2
- Rebuild for new boost

* Sun Jun 20 2010 Antti Andreimann <Antti.Andreimann@mail.ee> 3.3.0-1
- Updated to version 3.3.0
- Implemented a workaround for gcc segfault on el5

* Sun Feb 07 2010 Caolán McNamara <caolanm@redhat.com> - 3.2.0-7
- Rebuild for xerces soname bump

* Fri Jan 22 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 3.2.0-6
- Rebuild for Boost soname bump

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jul 07 2009 Antti Andreimann <Antti.Andreimann@mail.ee> 3.2.0-4
- Removed redundant PostScript files from the doc package

* Mon Jul 06 2009 Antti Andreimann <Antti.Andreimann@mail.ee> 3.2.0-3
- Added ACE homepage to SPEC file comments
- Added verbose=1 to MAKEFLAGS so compiler flags could be
  verified from build logs.

* Mon Jul 04 2009 Antti Andreimann <Antti.Andreimann@mail.ee> 3.2.0-2
- Changed License tag to clarify which exceptions we are talking about

* Wed May 20 2009 Antti Andreimann <Antti.Andreimann@mail.ee> 3.2.0-1
- Initial RPM release.
