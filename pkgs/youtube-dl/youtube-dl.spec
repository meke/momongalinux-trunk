%global         momorel 1
%global         srcdate 2013.08.02

Summary:        Download videos from YouTube.com
Name:           youtube-dl
Version:        %{srcdate}
Release:        %{momorel}m%{?dist}
License:        GPL
Group:          Applications/Internet
URL:            http://rg3.github.com/youtube-dl/
Source0:        http://youtube-dl.org/downloads/%{version}/%{name}-%{version}.tar.gz
NoSource:       0
Source1:        %{name}.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Buildarch: noarch
Requires: python >= 2.6
# Used in Makefile to generate youtube-dl
BuildRequires:  zip
BuildRequires:  python >= 2.6

%description
youtube-dl is a small command-line program for downloading videos from 
YouTube.com.

%prep
%setup -cqTn %{name}-%{version}
%{__gzip} -dc %{SOURCE0} | tar --strip-components=1 -xvvf -
%{__rm} -f youtube-dl{,.exe}

%build
%{__make}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR="%{buildroot}" PREFIX="%{_prefix}" MANDIR="%{_mandir}"
%{__mkdir_p} %{buildroot}%{_sysconfdir}
%{__install} -p -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*
%config(noreplace) %{_sysconfdir}/%{name}.conf
%{_sysconfdir}/bash_completion.d/%{name}

%changelog
* Sun Aug  4 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2013.08.02-1m)
- update to 2013.08.02

* Sun Mar 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2013.03.29-1m)
- update to 2013.03.29

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0-0.20120227.1m)
- update to 2012.02.27

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0-0.20110804.1m)
- update to 2011.08.04

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0-0.20110329.1m)
- update to 2011.03.29

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.20101003.3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.20101003.2m)
- rebuild for new GCC 4.5

* Sun Oct 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0-0.20101003.1m)
- update to 2010.10.03

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.20100722.2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0-0.20100722.1m)
- update to 2010.07.22

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0-0.20100714.1m)
- update to 2010.07.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.20090513.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0-0.20090513.1m)
- update to 2009.05.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.20080124.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-0.20080124.3m)
- rebuild against gcc43

* Fri Feb 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0-0.20080124.2m)
- remove checking section of md5sum from %%prep

* Sat Feb  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-0.20080124.1m)
- update to 2008.01.24

* Mon Nov 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-0.20071012.1m)
- update to 2007.10.12

* Fri Feb 23 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-0.20070218.1m)
- update to 2007.02.18
- No NoSource0

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0-0.20070119.1m)
- import to Momonga from dag
- update to 2007.01.19

* Mon Jan 01 2007 Moritz Barsnick <moritz+rpm@barsnick.net> 2007.01.01-1
- Updated to release 2007.01.01
- Preserve mtime on install
- don't BR python
- require python >= 2.4 instead of just python (at least 2.3 required
    for optparse, 2.4 required for cookielib; the program indeed checks
    for >= 2.4 at runtime)

* Sat Dec 09 2006 Dries Verachtert <dries@ulyssis.org> - 2006.12.07-1
- Updated to release 2006.12.07.

* Sun Dec 03 2006 Dries Verachtert <dries@ulyssis.org> - 2006.12.03-1
- Updated to release 2006.12.03.

* Sun Nov 12 2006 Dries Verachtert <dries@ulyssis.org> - 2006.11.12-1
- Updated to release 2006.11.12.

* Tue Aug 15 2006 Dries Verachtert <dries@ulyssis.org> - 2006.08.15-1
- Updated to release 2006.08.15.

* Sun Aug 13 2006 Dries Verachtert <dries@ulyssis.org> - 2006.08.13-1
- Updated to release 2006.08.13.

* Fri Aug 11 2006 Dries Verachtert <dries@ulyssis.org> - 2006.08.11-1
- Initial package.
