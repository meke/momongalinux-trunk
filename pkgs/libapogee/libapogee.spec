%global momorel 7

Summary:	Library for Apogee CCD Cameras.
Name:		libapogee
Version:	2.2
Release:	%{momorel}m%{?dist}
License:	BSD
Group:		System Environment/Libraries
Url:		http://www.indilib.org/index.php?title=Main_Page
Source0:	http://dl.sourceforge.net/project/indi/indilib/0.6/%{name}2_%{version}.tar.gz
NoSource:	0
Patch0:		libapogee-suffix.patch
Patch1:		libapogee-sysio.patch
Patch2:		libapogee2-2.2-curl.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	gcc-c++
BuildRequires:	libcurl-devel
BuildRequires:	libusb-devel
BuildRequires:	cmake 

%description
Apogee library is used by applications to control Apogee CCDs.

%package devel
License:      BSD
Group:        Development/Libraries
Summary:      Library for Apogee CCD Cameras.
Requires:     %{name} >= %{version}

%description devel
Apogee library is used by applications to control Apogee CCDs. 
libapogee-devel contain header files required for development.

%prep
%setup -q -n %{name}2-%{version}
%patch0 -p1 -b .suffix
%patch1 -p1 -b .sysio
%patch2 -p1 -b .curl~

%build
%{cmake} .
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-,root,root)
%doc AUTHORS ChangeLog README LICENSE
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/libapogee
%{_libdir}/*.so

%changelog
* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-7m)
- fix build failure; add patch for curl

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-4m)
- full rebuild for mo7 release

* Wed Dec 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-3m)
- set NoSource
- import Patch1 from Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Mar 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8-2m)
- rebuild against rpm-4.6

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8-1m)
- import from OpenSuSE
