%global momorel 2

Summary: Mailing list manager with built in Web access
Name: mailman
Version: 2.1.14
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
URL: http://www.list.org/
#Source0: http://dl.sourceforge.net/sourceforge/mailman/mailman-%{version}.tgz 
Source0: ftp://ftp.gnu.org/pub/gnu/mailman/mailman-%{version}.tgz
NoSource: 0
Source1: mm_cfg.py
Source3: httpd-mailman.conf
Source4: mailman.logrotate
Source5: mailman.INSTALL.REDHAT.in
Source6: mailman-crontab-edit
Source7: mailman-migrate-fhs
Source8: mailman-update-cfg
Source9: mailman-tmpfiles.conf
Source10: mailman.service

Patch1: mailman-2.1.12-multimail.patch
Patch2: mailman-2.1-build.patch
Patch3: mailman-2.1-mailmanctl-status.patch
Patch4: mailman-2.1.11-cron.patch
Patch5: mailman-2.1.13-FHS.patch
Patch6: mailman-python-compile.patch
Patch7: mailman-2.1.13-archive-reply.patch
Patch8: mailman-2.1.9-LC_CTYPE.patch
Patch9: mailman-2.1.9-ctypo-new.patch
Patch10: mailman-2.1.10-ctypefix.patch
Patch11: mailman-2.1.9-header-folding.patch
Patch12: mailman-2.1.9-selinux.patch
Patch13: mailman-2.1.9-unicode.patch
Patch14: mailman-2.1.11-fhsinit.patch
Patch15: mailman-2.1.13-lctype.patch
#Patch15: mailman-2.1.11-footer.patch
Patch16: mailman-2.1.12-privurl.patch
Patch17: mailman-2.1.12-mmcfg.patch
Patch18: mailman-2.1.12-initcleanup.patch
Patch19: mailman-2.1.12-codage.patch
# the service is now off by default
Patch20: mailman-2.1.12-init-not-on.patch
Patch21: mailman-2.1.13-env-python.patch
Patch22: mailman-2.1.9-CVE-2011-0707.patch
Patch23: mailman-2.1.13-subject-decode.patch

# JP patch
#Source50: http://www.python.jp/doc/contrib/mailman/_static/mailman-2.1.14+j7.tgz
#Patch50: diff-mailman-2.1.14-jp7.patch.xz
#
Requires(triggerun): chkconfig
Requires(pre): grep shadow-utils coreutils initscripts
#Requires: vixie-cron >= 4.1, httpd, python >= 2.2, mktemp
Requires: cronie >= 1.0, webserver, python >= 2.2, mktemp
Requires(post): systemd-units
Requires(post): systemd-sysv
Requires(preun): systemd-units
Requires(postun): systemd-units
BuildRequires: python-devel >= 2.2, automake
BuildRequires: systemd-units
Conflicts: fml
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define contentdir /var/www

# Installation directories
# rpmlint will give an error about hardcoded library path,
# but this is necessary, because there are python executables inside,
# which the user can run in their scripts. 
# see rhbz#226117 for more information
%define mmdir /usr/lib/%{name}
%define varmmdir /var/lib/%{name}
%define docdir /usr/share/doc/%{name}-%{version}
%define configdir /etc/%{name}
%define datadir %{varmmdir}/data
%define archivesdir %{varmmdir}/archives
%define lockdir /var/lock/%{name}
%define logdir /var/log/%{name}
%define piddir /var/run/%{name}
%define queuedir /var/spool/%{name}
%define httpdconfdir /etc/httpd/conf.d
%define restart_flag /var/run/%{name}-restart-after-rpm-install
%define mmbuilddir %{_builddir}/%{name}-%{version}
 
#%%define httpdconffile %{name}.conf
%global httpdconffile %{name}.conf.dist

# Now, the user and group the CGIs will expect to be run under.  This should
# match the user and group the web server is configured to run as.  The scripts
# will error out if they are invoked by any other user.
%define cgiuser    apache
%define cgigroup   apache

# Now, the user and group the scripts will actually execute as.
%define mmuser       mailman
%define mmuserid     41
%define mmgroup      mailman
%define mmgroupid    41

# Directory/File modes & permissions
%define dirmode 2775
%define exemode 2755

# Now, the groups your mail spoolers run as.  Sendmail uses 'mail'(12)
# and postfix used to use 'nobody', but now uses 'postfix'
%define mailgroup  "mail postfix mailman nobody daemon"

# The mail wrapper program
%define mail_wrapper mailman

%description
Mailman is software to help manage email discussion lists, much like
Majordomo and Smartmail. Unlike most similar products, Mailman gives
each mailing list a webpage, and allows users to subscribe,
unsubscribe, etc. over the Web. Even the list manager can administer
his or her list entirely from the Web. Mailman also integrates most
things people want to do with mailing lists, including archiving, mail
<-> news gateways, and so on.

Documentation can be found in: %{docdir}

When the package has finished installing, you will need to perform some
additional installation steps, these are described in:
%{docdir}/INSTALL.REDHAT

%prep
#%%setup -q -a 50
%setup -q
%patch1 -p1 -b .multimail
%patch2 -p1 -b .permissions
%patch3 -p1 -b .status
%patch4 -p1 -b .cron
%patch5 -p1 -b .FHS
%patch6 -p1 -b .python-compile
%patch7 -p1 -b .archive-in-reply-to
%patch8 -p1 -b .lctype
%patch9 -p1 -b .ctypo
%patch10 -p1 -b .ctypefix
#!%%patch11 -p1 -b .header
%patch12 -p1 -b .selinux
%patch13 -p1 -b .unicode
%patch14 -p1 -b .fhsinit
%patch16 -p1 -b .privurl
%patch17 -p1 -b .mmcfg
%patch18 -p1 -b .initcleanup
#u%%patch19 -p1
%patch20 -p1
%patch21 -p1
%patch22
%patch23 -p1

#%%patch50 -p1

#cp $RPM_SOURCE_DIR/mailman.INSTALL.REDHAT.in INSTALL.REDHAT.in
cp %{SOURCE5} INSTALL.REDHAT.in

%build
CFLAGS="%{optflags}"; export CFLAGS
rm -f configure
aclocal
autoconf
# rpmlint will give an error about hardcoded library path,
# but this is necessary, because there are python executables inside,
# which the user can run in their scripts. 
# see rhbz#226117 for more information
./configure \
        --libdir=/usr/lib \
        --prefix=%{mmdir} \
        --with-var-prefix=%{varmmdir} \
        --with-config-dir=%{configdir} \
        --with-lock-dir=%{lockdir} \
        --with-log-dir=%{logdir} \
        --with-pid-dir=%{piddir} \
        --with-queue-dir=%{queuedir} \
        --with-python=%{__python} \
        --with-mail-gid=%{mailgroup} \
        --with-cgi-id=%{cgiuser} \
        --with-cgi-gid=%{cgigroup} \
        --with-mailhost=localhost.localdomain \
        --with-urlhost=localhost.localdomain \
        --without-permcheck

function SubstituteParameters()
{
sed -e 's|@VAR_PREFIX@|%{varmmdir}|g' \
    -e 's|@VARMMDIR@|%{varmmdir}|g' \
    -e 's|@prefix@|%{mmdir}|g' \
    -e 's|@MMDIR@|%{mmdir}|g' \
    -e 's|@CONFIG_DIR@|%{configdir}|g' \
    -e 's|@DATA_DIR@|%{datadir}|g' \
    -e 's|@LOCK_DIR@|%{lockdir}|g' \
    -e 's|@LOG_DIR@|%{logdir}|g' \
    -e 's|@PID_DIR@|%{piddir}|g' \
    -e 's|@QUEUE_DIR@|%{queuedir}|g' \
    -e 's|@DOC_DIR@|%{docdir}|g' \
    -e 's|@HTTPD_CONF_DIR@|%{httpdconfdir}|g' \
    -e 's|@HTTPD_CONF_FILE@|%{httpdconffile}|g' \
    $1 > $2
}

SubstituteParameters "INSTALL.REDHAT.in" "INSTALL.REDHAT"
SubstituteParameters "%{SOURCE1}" "Mailman/mm_cfg.py.dist"
SubstituteParameters "%{SOURCE3}" "httpd-mailman.conf"
SubstituteParameters "%{SOURCE4}" "mailman.logrotate"

make
	
%install
rm -fr --preserve-root %{buildroot}
# Normal install.
make DESTDIR=%{buildroot} install
#make install prefix=%{buildroot}%{mmdir} var_prefix=%{buildroot}%{varmmdir}

# Install the mailman cron.d script
mkdir -p %{buildroot}/etc/cron.d
cat > %{buildroot}/etc/cron.d/%{name} <<EOF
# DO NOT EDIT THIS FILE!
#
# Contents of this file managed by /etc/init.d/%{name}
# Master copy is %{mmdir}/cron/crontab.in
# Consult that file for documentation
EOF

# Copy the icons into the web server's icons directory.
mkdir -p %{buildroot}%{contentdir}/icons
cp %{buildroot}/%{mmdir}/icons/* %{buildroot}%{contentdir}/icons

# Create a link to the wrapper in /etc/smrsh to allow sendmail to run it.
# The link should be relative in order to make it work in chroot
mkdir -p %{buildroot}/etc/smrsh
ln -s ../..%{mmdir}/mail/%{mail_wrapper} %{buildroot}/etc/smrsh

# Create a link so that the config file mm_cfg.py appears in config
# directory /etc/mailman. We don't put mm_cfg.py in the config directory
# because its executable code (python file) and the security policy wants
# to keep executable code out of /etc and inside of a lib directory instead,
# and because traditionally mm_cfg.py was in the Mailman subdirectory and
# experienced mailman admins will expect to find it there. But having it 
# "appear" in the config directory is good practice and heading in the 
# right direction for FHS compliance.
mkdir -p %{buildroot}%{configdir}
ln -s %{mmdir}/Mailman/mm_cfg.py %{buildroot}%{configdir}

# sitelist.cfg used to live in the DATA_DIR, now as part of the 
# FHS reoraganization it lives in the CONFIG_DIR. Most of the
# documentation refers to it in its DATA_DIR location and experienced
# admins will expect to find it there, so create a link in DATA_DIR to
# point to it in CONFIG_DIR so people aren't confused.
ln -s %{configdir}/sitelist.cfg %{buildroot}%{datadir}

# Install a logrotate control file.
mkdir -p %{buildroot}/etc/logrotate.d
install -m644 %{mmbuilddir}/%{name}.logrotate %{buildroot}/etc/logrotate.d/%{name}

# Install the httpd configuration file.
install -m755 -d %{buildroot}%{httpdconfdir}
install -m644 %{mmbuilddir}/httpd-mailman.conf %{buildroot}%{httpdconfdir}/%{httpdconffile}

# Install the documentation files
install -m755 -d %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/INSTALL.REDHAT   %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/ACKNOWLEDGMENTS  %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/FAQ              %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/NEWS             %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/README           %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/README.CONTRIB   %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/README-I18N.en   %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/README.NETSCAPE  %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/README.USERAGENT %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/STYLEGUIDE.txt   %{buildroot}%{docdir}
install -m644 %{mmbuilddir}/UPGRADING        %{buildroot}%{docdir}

cp -r %{mmbuilddir}/contrib %{buildroot}%{docdir}
install -m644 %{SOURCE7} %{buildroot}%{docdir}/contrib/migrate-fhs
install -m755 -d %{buildroot}%{docdir}/admin
cp -r %{mmbuilddir}/doc %{buildroot}%{docdir}/admin

#install the script for updating the config (bz#484328)
mkdir -p %{buildroot}%{_bindir}
install -m755 %{SOURCE8} %{buildroot}%{_bindir}
# set library path in mailman-update-cfg script.
sed -i 's,@mmdir@,%{mmdir},g' %{buildroot}%{_bindir}/mailman-update-cfg

# remove dir/files from %{buildroot} that we aren't shipping
rm -rf %{buildroot}%{varmmdir}/icons

# The file fblast confuses /usr/lib/rpm/find-requires because its an executable
# script file that does not have the interpreter as the first line, its not
# executable by itself so turn off its execute permissions
chmod 0644 %{buildroot}/%{mmdir}/tests/fblast.py

# Security issues...
#chmod 0755 $RPM_BUILD_ROOT/%{mmdir}/pythonlib/japanese/c/_japanese_codecs.so
#chmod 0755 $RPM_BUILD_ROOT/%{mmdir}/pythonlib/korean/c/hangul.so
#chmod 0755 $RPM_BUILD_ROOT/%{mmdir}/pythonlib/korean/c/_koco.so

# Directories...
#mkdir -p $RPM_BUILD_ROOT/%{lockdir}
mkdir -p $RPM_BUILD_ROOT/%{logdir}
#mkdir -p $RPM_BUILD_ROOT/%{piddir}
mkdir -p $RPM_BUILD_ROOT/%{queuedir}

install -p -D %{SOURCE9} %{buildroot}%{_sysconfdir}/tmpfiles.d/mailman.conf

# Systemd service file
mkdir -p %{buildroot}%{_unitdir}
install -m644 %{SOURCE10} %{buildroot}%{_unitdir}

%clean
rm -fr --preserve-root %{buildroot}
rm -rf %{_builddir}/files.%{name}

%pre

# Make sure the user "mailman" exists on this system and has the correct values
if grep -q "^mailman:" /etc/group 2> /dev/null ; then
  /usr/sbin/groupmod -g %{mmgroupid} -n %{mmgroup} %{mmgroup} 2> /dev/null || :
else
  /usr/sbin/groupadd -g %{mmgroupid} %{mmgroup} 2> /dev/null || :
fi
if grep -q "^mailman:" /etc/passwd 2> /dev/null ; then
  /usr/sbin/usermod -s /sbin/nologin -c "GNU Mailing List Manager" -d %{mmdir} -u %{mmuserid} -g %{mmgroupid}       %{mmuser} 2> /dev/null || :
else
  /usr/sbin/useradd -s /sbin/nologin -c "GNU Mailing List Manager" -d %{mmdir} -u %{mmuserid} -g %{mmgroupid} -M -r %{mmuser} 2> /dev/null || :
fi

# Mailman should never be running during an install, but a package upgrade
# shouldn't silently stop the service, so if mailman was running
# we'll leave a temp file in the lock directory as a flag so in
# the post install phase we can restart it.
#
# rpmlint will complain here about "dangerous use of rm"
# but this is OK because we are only rm-ing our temporary file
if [ -d %{lockdir} ]; then
  rm -f %{restart_flag}
  /sbin/service %{name} status >/dev/null 2>&1
  if [ $? -eq 0 ]; then
      touch %{restart_flag}
      /sbin/service %{name} stop >/dev/null 2>&1
  fi
fi
# rpm should not abort if last command run had non-zero exit status, exit cleanly
exit 0

%post
# We no longer use crontab, but previous versions of the spec file did, so clean up
if [ -f /var/spool/cron/%{mmuser} ]; then
  crontab -u %{mmuser} -r
fi

# Restart mailman if it had been running before installation
if [ -e %{restart_flag} ]; then
  rm %{restart_flag}
  /sbin/service %{name} start >/dev/null 2>&1
fi

# systemd
if [ $1 -eq 1 ] ; then
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

# rpm should not abort if last command run had non-zero exit status, exit cleanly
exit 0

%preun

# if [ $1 = 0 ]' checks that this is the actual deinstallation of
# the package, as opposed to just removing the old package on upgrade.

if [ $1 = 0 ]; then
  # These statements stop the service, and remove the /etc/rc*.d links.
  /sbin/service %{name} stop >/dev/null 2>&1
  /bin/systemctl --no-reload mailman.service > /dev/null 2>&1 || :
  /bin/systemctl stop mailman.service > /dev/null 2>&1 || :
fi
# rpm should not abort if last command run had non-zero exit status, exit cleanly
exit 0

%postun
if [ $1 = 0 ]; then
  crontab -u %{mmuser} -r 2>/dev/null
fi

# systemd
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    /bin/systemctl try-restart mailman.service >/dev/null 2>&1 || :
fi

# rpm should not abort if last command run had non-zero exit status, exit cleanly
exit 0

%triggerun -- mailman < 2.1.12-13m
%{_bindir}/systemd-sysv-convert --save mailman >/dev/null 2>&1 ||:
/bin/systemctl enable mailman.service >/dev/null 2>&1
/sbin/chkconfig --del mailman >/dev/null 2>&1 || :
/bin/systemctl try-restart mailman.service >/dev/null 2>&1 || :


# rpm should not abort if last command run had non-zero exit status, exit cleanly
exit 0


%files
%defattr(-,root,%{mmgroup})
%attr(2755,root,%{mmgroup}) %dir %{mmdir}
#%%{mmdir}/Mailman
%attr(2755,root,%{mmgroup}) %{mmdir}/bin
%attr(2755,root,%{mmgroup}) %{mmdir}/cgi-bin
#%%{mmdir}/cron
%attr(2755,root,%{mmgroup}) %{mmdir}/icons
%attr(2755,root,%{mmgroup}) %{mmdir}/mail
%attr(2755,root,%{mmgroup}) %{mmdir}/messages
%attr(2755,root,%{mmgroup}) %{mmdir}/pythonlib
%attr(2755,root,%{mmgroup}) %{mmdir}/scripts
# rpmlint will complain here about config files being in /usr
# but these are both data files -parts of mailman's web UI-
# and config files - user can change them to match the design
# and/or content of their web pages
%attr(2755,root,%{mmgroup}) %config(noreplace) %{mmdir}/templates
%attr(2755,root,%{mmgroup}) %{mmdir}/tests
%{varmmdir}
#cron dir minus one file which is listed later
%{mmdir}/cron/bumpdigests
%{mmdir}/cron/checkdbs
%{mmdir}/cron/cull_bad_shunt
%{mmdir}/cron/disabled
%{mmdir}/cron/gate_news
%{mmdir}/cron/mailpasswds
%{mmdir}/cron/nightly_gzip
%{mmdir}/cron/paths.py
%{mmdir}/cron/paths.pyc
%{mmdir}/cron/paths.pyo
%{mmdir}/cron/senddigests
#Mailman dir minus one file which is listed later
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/Archiver
%{mmdir}/Mailman/Autoresponder.py
%{mmdir}/Mailman/Autoresponder.pyc
%{mmdir}/Mailman/Autoresponder.pyo
%{mmdir}/Mailman/Bouncer.py
%{mmdir}/Mailman/Bouncer.pyc
%{mmdir}/Mailman/Bouncer.pyo
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/Bouncers
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/Cgi
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/Commands
%{mmdir}/Mailman/Defaults.py
%{mmdir}/Mailman/Defaults.pyc
%{mmdir}/Mailman/Defaults.pyo
%{mmdir}/Mailman/Deliverer.py
%{mmdir}/Mailman/Deliverer.pyc
%{mmdir}/Mailman/Deliverer.pyo
%{mmdir}/Mailman/Digester.py
%{mmdir}/Mailman/Digester.pyc
%{mmdir}/Mailman/Digester.pyo
%{mmdir}/Mailman/Errors.py
%{mmdir}/Mailman/Errors.pyc
%{mmdir}/Mailman/Errors.pyo
%{mmdir}/Mailman/GatewayManager.py
%{mmdir}/Mailman/GatewayManager.pyc
%{mmdir}/Mailman/GatewayManager.pyo
#%%{mmdir}/Mailman/Generator.py
#%%{mmdir}/Mailman/Generator.pyc
#%%{mmdir}/Mailman/Generator.pyo
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/Gui
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/Handlers
%{mmdir}/Mailman/htmlformat.py
%{mmdir}/Mailman/htmlformat.pyc
%{mmdir}/Mailman/htmlformat.pyo
%{mmdir}/Mailman/HTMLFormatter.py
%{mmdir}/Mailman/HTMLFormatter.pyc
%{mmdir}/Mailman/HTMLFormatter.pyo
%{mmdir}/Mailman/i18n.py
%{mmdir}/Mailman/i18n.pyc
%{mmdir}/Mailman/i18n.pyo
%{mmdir}/Mailman/__init__.py
%{mmdir}/Mailman/__init__.pyc
%{mmdir}/Mailman/__init__.pyo
%{mmdir}/Mailman/ListAdmin.py
%{mmdir}/Mailman/ListAdmin.pyc
%{mmdir}/Mailman/ListAdmin.pyo
%{mmdir}/Mailman/LockFile.py
%{mmdir}/Mailman/LockFile.pyc
%{mmdir}/Mailman/LockFile.pyo
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/Logging
%{mmdir}/Mailman/Mailbox.py
%{mmdir}/Mailman/Mailbox.pyc
%{mmdir}/Mailman/Mailbox.pyo
%{mmdir}/Mailman/MailList.py
%{mmdir}/Mailman/MailList.pyc
%{mmdir}/Mailman/MailList.pyo
%{mmdir}/Mailman/MemberAdaptor.py
%{mmdir}/Mailman/MemberAdaptor.pyc
%{mmdir}/Mailman/MemberAdaptor.pyo
%{mmdir}/Mailman/Message.py
%{mmdir}/Mailman/Message.pyc
%{mmdir}/Mailman/Message.pyo
%{mmdir}/Mailman/mm_cfg.py.dist
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/MTA
%{mmdir}/Mailman/OldStyleMemberships.py
%{mmdir}/Mailman/OldStyleMemberships.pyc
%{mmdir}/Mailman/OldStyleMemberships.pyo
%{mmdir}/Mailman/Pending.py
%{mmdir}/Mailman/Pending.pyc
%{mmdir}/Mailman/Pending.pyo
%{mmdir}/Mailman/Post.py
%{mmdir}/Mailman/Post.pyc
%{mmdir}/Mailman/Post.pyo
%attr(2755,root,%{mmgroup}) %{mmdir}/Mailman/Queue
%{mmdir}/Mailman/SafeDict.py
%{mmdir}/Mailman/SafeDict.pyc
%{mmdir}/Mailman/SafeDict.pyo
%{mmdir}/Mailman/SecurityManager.py
%{mmdir}/Mailman/SecurityManager.pyc
%{mmdir}/Mailman/SecurityManager.pyo
%{mmdir}/Mailman/Site.py
%{mmdir}/Mailman/Site.pyc
%{mmdir}/Mailman/Site.pyo
%{mmdir}/Mailman/TopicMgr.py
%{mmdir}/Mailman/TopicMgr.pyc
%{mmdir}/Mailman/TopicMgr.pyo
%{mmdir}/Mailman/UserDesc.py
%{mmdir}/Mailman/UserDesc.pyc
%{mmdir}/Mailman/UserDesc.pyo
%{mmdir}/Mailman/Utils.py
%{mmdir}/Mailman/Utils.pyc
%{mmdir}/Mailman/Utils.pyo
%{mmdir}/Mailman/Version.py
%{mmdir}/Mailman/Version.pyc
%{mmdir}/Mailman/Version.pyo
%{mmdir}/Mailman/versions.py
%{mmdir}/Mailman/versions.pyc
%{mmdir}/Mailman/versions.pyo
%{_unitdir}/mailman.service
%doc %{docdir}
#%%dir %attr(0755,root,root) %{contentdir}/icons
%attr(0644,root,root) %{contentdir}/icons/*
%attr(0644, root, %{mmgroup}) %config(noreplace) %verify(not md5 size mtime) %{mmdir}/Mailman/mm_cfg.py
%verify(not md5 size mtime) %{mmdir}/Mailman/mm_cfg.pyc
%verify(not md5 size mtime) %{mmdir}/Mailman/mm_cfg.pyo
%config(noreplace) %{httpdconfdir}/%{httpdconffile}
/etc/logrotate.d/%{name}
/etc/smrsh/%{mail_wrapper}
%dir %attr(2775,root,%{mmgroup}) %{configdir}
%attr(0644, root, %{mmgroup}) %config(noreplace) %verify(not md5 size mtime) %{configdir}/sitelist.cfg
%{configdir}/mm_cfg.*
#%attr(2775,root,%{mmgroup}) %{lockdir}
%attr(2775,root,%{mmgroup}) %{logdir}
%config(noreplace) %{_sysconfdir}/tmpfiles.d/mailman.conf
%attr(2775,root,%{mmgroup}) %{queuedir}
#%attr(2775,root,%{mmgroup}) %{piddir}
%attr(0644,root,root) %config(noreplace) %verify(not md5 size mtime) /etc/cron.d/mailman
%attr(0644,root,%{mmgroup}) %config(noreplace) %{mmdir}/cron/crontab.in
%attr(0755,root,root) %{_bindir}/mailman-update-cfg
# fix for security issue #459530
%attr(2770,%{cgiuser},%{mmgroup}) %{archivesdir}/private

%changelog
* Sat Mar 31 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.14-2m)
- add Patch23: mailman-2.1.13-subject-decode.patch

* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.14-1m)
- update to 2.1.14 and support systemd
- fix duplicate glob %%{contentdir}/icons

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.12-13m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.12-12m)
- rebuild for new GCC 4.6

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.12-11m)
- [SECURITY] CVE-2011-0707

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.12-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.12-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.12-8m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.12-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.12-6m)
- stop daemon

* Fri Apr  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.12-5m)
- fix installation of codecs (Patch17)

* Thu Apr  2 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.12-4m)
- add Patch16: mailman-2.1.12-privurl.patch
- add "AddDefaultCharset Off" in httpd configuration (#463115) from Fedora 2.1.12-2

* Wed Mar  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.12-3m)
- comment out Patch20: mailman-2.1.11-dont-import-japanese.patch for build fix

* Wed Mar  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.12-2m)
- comment out chmod %%{mmdir}/pythonlib/*/c/*.so for build fix

* Tue Mar  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.12-1m)
- version up to 2.1.12
- update Patch1: mailman-2.1.12-multimail.patch
- update Patch4: mailman-2.1.12-cron.patch
- update Patch5: mailman-2.1.12-FHS.patch
- update Patch8: mailman-2.1.12-lctype.patch
- delete included Patch15: mailman-2.1.11-footer.patch

* Tue Mar  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.11-1m)
- sync with fedora
- version to to 2.1.11
- update Patch5: mailman-2.1.11-FHS.patch
- update Patch20: mailman-2.1.11-dont-import-japanese.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.9-7m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.9-6m)
- update Patch5 for fuzz=0
- License: GPLv2+

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.9-5m)
- rebuild against python-2.6.1-2m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.9-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.9-3m)
- %%NoSource -> NoSource

* Fri Dec 29 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.1.9-2m)
- add mailman-2.1.9-dont-import-japanese.patch
- see <http://mm.tkikuchi.net/pipermail/mmjp-users/2006-October/001882.html>

* Thu Sep 13 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.1.9-1m)
- version up 2.1.9
- [SECURITY] CVE-2006-2191 CVE-2006-2941 CVE-2006-3636 CVE-2006-4624
- http://secunia.com/advisories/21732/

* Thu Sep 07 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.1.9-0.1.1m)
- version up 2.1.9rc1
- [SECURITY] CVE-2006-2941 CVE-2006-3636
- http://secunia.com/advisories/21732/

* Fri Jun 09 2006 TABUCHi Takaaki <tab@momonga-linux.org>
- (2.1.8-2m)
- delete dup dir between httpd (/var/www/icons/)

* Sun Apr 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.8-1m)
- update to 2.1.8 
- [SECURITY] CVE-2006-1712
- http://mail.python.org/pipermail/mailman-announce/2006-April/000084.html

* Wed Mar  1 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.1.7-2m)
- fix /etc/logrotate.d/mailman

* Thu Feb 23 2006 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.7-1m)
- bugfix release

* Wed Jun  1 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.6-1m)
- include security features

* Wed Apr  6 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.5-3m)
- fix attribution of docdir

* Thu Mar 17 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.5-2m)
- [SECURITY] CAN-2005-0202

* Sun Feb 13 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.5-1m)
- import from FC3
- add jp patch (include Patch4)
