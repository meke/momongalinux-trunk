%global         momorel 2

Name:           perl-Email-MIME
Version:        1.926
Release:        %{momorel}m%{?dist}
Summary:        Easy MIME message parsing
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Email-MIME/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/Email-MIME-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Email-MessageID
BuildRequires:  perl-Email-MIME-ContentType >= 1.016
BuildRequires:  perl-Email-MIME-Encodings >= 1.314
BuildRequires:  perl-Email-Simple >= 2.004
BuildRequires:  perl-Email-Simple-Creator
BuildRequires:  perl-Encode >= 1.9801
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MIME-Types >= 1.13
BuildRequires:  perl-parent
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Simple >= 0.47
Requires:       perl-Email-MessageID
Requires:       perl-Email-MIME-ContentType >= 1.016
Requires:       perl-Email-MIME-Encodings >= 1.314
Requires:       perl-Email-Simple >= 2.004
Requires:       perl-Email-Simple-Creator
Requires:       perl-Encode >= 1.9801
Requires:       perl-MIME-Types >= 1.13
Requires:       perl-parent
Requires:       perl-Scalar-Util
Requires:       perl-Test-Simple >= 0.47
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is an extension of the Email::Simple module, to handle MIME encoded
messages. It takes a message as a string, splits it up into its constituent
parts, and allows you access to various parts of the message. Headers are
decoded from MIME encoding.

%prep
%setup -q -n Email-MIME-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/Email/MIME*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.926-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.926-1m)
- update to 1.926
- rebuild against perl-5.18.2

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.925-1m)
- update to 1.925

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.924-2m)
- rebuild against perl-5.18.1

* Sun Aug 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.924-1m)
- update to 1.924

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.923-1m)
- update to 1.923

* Fri Jul 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.922-1m)
- update to 1.922

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.921-1m)
- update to 1.921

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.920-1m)
- update to 1.920

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.911-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.911-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.911-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.911-2m)
- rebuild against perl-5.16.1

* Tue Jul 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.911-1m)
- update to 1.911

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.910-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.910-2m)
- rebuild against perl-5.14.2

* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.910-1m)
- update to 1.910

* Sat Sep 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.909-1m)
- update to 1.909

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.908-2m)
- rebuild against perl-5.14.1

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.908-1m)
- update to 1.908

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.907-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.907-2m)
- rebuild for new GCC 4.6

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.907-1m)
- update to 1.907

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.906-2m)
- rebuild for new GCC 4.5

* Fri Oct  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.906-1m)
- update to 1.906

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.905-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.905-1m)
- update to 1.905

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.903-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.903-4m)
- rebuild against perl-5.12.1

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.903-3m)
- add some BuildRequires: and Requies:
- enable test

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.903-2m)
- rebuild against perl-5.12.0
- disable test for a while...

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.903-1m)
- update to 1.903

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.902-1m)
- update to 1.902

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.901-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Nov  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.901-1m)
- update to 1.901

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.900-1m)
- update to 1.900
- Specfile re-generated by cpanspec 1.78 for Momonga Linux.

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.863-2m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.863-1m)
- update to 1.863

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.861-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.861-2m)
- rebuild against gcc43

* Tue Nov  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.861-1m)
- update to 1.861

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.860-1m)
- update to 1.860

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.859-2m)
- use vendor

* Wed Mar 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.859-1m)
- update to 1.859

* Sun Feb 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.858-1m)
- update to 1.858

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.857-1m)
- update to 1.857

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.855-1m)
- update to 1.855

* Fri Sep 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.852-1m)
- spec file was autogenerated
