%global momorel 6

Summary:        Super Read Ahead: A readahead implementation optimized for solid state devices
Name:           sreadahead
Version:        1.0
Release:        %{momorel}m%{?dist}
Group:          System Environment/Base
License:        GPLv2
URL:            http://code.google.com/p/sreadahead/
Source0:        http://sreadahead.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
%{name} speeds up booting by pre-reading disk blocks used during
previous boots.

%prep
%setup -q

%build
export CFLAGS="%{optflags}"

%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING
/sbin/sreadahead
%dir /var/lib/sreadahead/debugfs

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- build with %%{optflags} (fix build on i686)
- remove unnecessary Makefile.patch

* Wed Jan 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1m)
- initial made for Momonga
- add Patch0
