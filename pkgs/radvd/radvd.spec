%global momorel 2

%define initdir /etc/init.d
%define RADVD_UID 75
Summary:   A Router Advertisement daemon
Name:      radvd
Version:   1.9.2
Release:   %{momorel}m%{?dist}
License:   BSD
Group:     System Environment/Daemons
URL:       http://www.litech.org/radvd/
Source:    http://www.litech.org/radvd/dist/radvd-%{version}.tar.gz
NoSource:  0
Source1:    radvd-tmpfs.conf
Source2:    radvd.service
BuildRequires:      systemd-units
Requires(postun):   systemd-units
Requires(preun):    systemd-units
Requires(post):     systemd-units
Requires(pre):      shadow-utils
BuildRequires:      flex, flex-static, byacc
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
radvd is the router advertisement daemon for IPv6.  It listens to router
solicitations and sends router advertisements as described in "Neighbor
Discovery for IP Version 6 (IPv6)" (RFC 2461).  With these advertisements
hosts can automatically configure their addresses and some other
parameters.  They also can choose a default router based on these
advertisements.

Install radvd if you are setting up IPv6 network and/or Mobile IPv6
services.

%prep
%setup -q

%build
export CFLAGS="%{optflags} -D_GNU_SOURCE -fPIE" 
export LDFLAGS='-pie -Wl,-z,relro,-z,now,-z,noexecstack,-z,nodlopen'
%configure --with-pidfile=%{_localstatedir}/run/radvd/radvd.pid
make
# make %{?_smp_mflags} 
# Parallel builds still fail because seds that transform y.tab.x into
# scanner/gram.x are not executed before compile of scanner/gram.x
#

%install
[ %{buildroot} != "/" ] && rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

%makeinstall transform='s,x,x,'

mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
mkdir -p %{buildroot}%{_localstatedir}/run/radvd
mkdir -p %{buildroot}%{_unitdir}

install -m 644 redhat/radvd.conf.empty %{buildroot}%{_sysconfdir}/radvd.conf
install -m 644 redhat/radvd.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/radvd

install -d -m 755 %{buildroot}%{_sysconfdir}/tmpfiles.d
install -p -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/tmpfiles.d/radvd.conf
install -m 644 %{SOURCE2} %{buildroot}%{_unitdir}

rm -rf %{buildroot}/etc/rc.d/ 

%clean
[ %{buildroot} != "/" ] && rm -rf %{buildroot}

%postun
%systemd_postun_with_restart radvd.service

%post
%systemd_post radvd.service

%preun
%systemd_preun radvd.service

# Static UID and GID defined by /usr/share/doc/setup-*/uidgid
%pre
getent group radvd >/dev/null || groupadd -r -g 75 radvd
getent passwd radvd >/dev/null || \
  useradd -r -u 75 -g radvd -d / -s /sbin/nologin -c "radvd user" radvd
exit 0

%files
%defattr(-,root,root,-)
%doc COPYRIGHT README CHANGES INTRO.html TODO
%{_unitdir}/radvd.service
%config(noreplace) %{_sysconfdir}/radvd.conf
%config(noreplace) %{_sysconfdir}/sysconfig/radvd
%config(noreplace) %{_sysconfdir}/tmpfiles.d/radvd.conf
%dir %attr(-,radvd,radvd) %{_localstatedir}/run/radvd/
%doc radvd.conf.example
%{_mandir}/*/*
%{_sbindir}/radvd
%{_sbindir}/radvdump

%changelog
* Sun Oct 06 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.2-1m)
- version up 1.9.2

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2-1m)
- version up 1.8.2
- [SECURITY] CVE-2011-3601 CVE-2011-3602 CVE-2011-3603
- [SECURITY] CVE-2011-3604 CVE-2011-3605

* Sun Sep 11 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8-1m)
- version up 1.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1
- use transform='s,x,x,' at make install time

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-2m)
- rebuild against gcc43

* Mon May  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.1-1m)
- import from fc

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.9.1-1.1.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jason Vas Dias <jvdias@redhat.com> - 0.9.1-1.1
- rebuild for new gcc, glibc, glibc-kernheaders

* Mon Jan 16 2006 Jason Vas Dias<jvdias@redhat.com> - 0.9.1-1
- Upgrade to upstream version 0.9.1

* Sun Dec 18 2005 Jason Vas Dias<jvdias@redhat.com>
- Upgrade to upstream version 0.9

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jul 18 2005 Jason Vas Dias <jvdias@redhat.com> 0.8.2.FC5
- fix bug 163593: must use '%%configure' to get correct conf file location

* Mon Jul 18 2005 Jason Vas Dias <jvdias@redhat.com> 0.8-1.FC5
- Upgrade to upstream version 0.8

* Fri Jul  8 2005 Pekka Savola <pekkas@netcore.fi> 0.8-1
- 0.8.
- Ship the example config file as %%doc (Red Hat's #159005)

* Fri Feb 25 2005 Jason Vas Dias <jvdias@redhat.com> 0.7.3-1_FC4
- make version compare > that of FC3

* Mon Feb 21 2005 Jason Vas Dias <jvdias@redhat.com> 0.7.3-1
- Upgrade to radvd-0.7.3
- add execshield -fPIE / -pie compile / link options

* Mon Feb 21 2005 Pekka Savola <pekkas@netcore.fi> 0.7.3-1
- 0.7.3.

* Mon Oct 28 2002 Pekka Savola <pekkas@netcore.fi>
- 0.7.2.

* Tue May  7 2002 Pekka Savola <pekkas@netcore.fi>
- remove '-g %%{RADVD_GID}' when creating the user, which may be problematic
  if the user didn't exist before.

* Fri Apr 12 2002 Bernhard Rosenkraenzer <bero@redhat.com> 0.7.1-1
- 0.7.1 (bugfix release, #61023), fixes:
  - Check that forwarding is enabled when starting radvd
    (helps avoid odd problems) 
  - Check configuration file permissions (note: in setuid operation, must not
    be writable by the user.group) 
  - Cleanups and enhancements for radvdump
  - Ensure NULL-termination with strncpy even with overlong strings
    (non-criticals, but better safe than sorry) 
  - Update config.{guess,sub} to cope with some newer architectures 
  - Minor fixes and cleanups 

* Wed Jan 14 2002 Pekka Savola <pekkas@netcore.fi>
- 0.7.1.

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Jan  8 2002 Pekka Savola <pekkas@netcore.fi>
- Change 'reload' to signal HUP to radvd instead or restarting.

* Fri Dec 28 2001 Pekka Savola <pekkas@netcore.fi>
- License unfortunately is BSD *with* advertising clause, so to be pedantic,
  change License: to 'BSD-style'.

* Thu Nov 22 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.7.0

* Wed Nov 14 2001 Pekka Savola <pekkas@netcore.fi>
- spec file cleanups
- update to 0.7.0.

* Mon Jul  9 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- initial Red Hat Linux build

* Sun Jun 24 2001 Pekka Savola <pekkas@netcore.fi>
- add a patch from USAGI for overflow, Copyright -> License.

* Wed Jun 20 2001 Pekka Savola <pekkas@netcore.fi>
- use /sbin/service.
- update to 0.6.2pl4.

* Sat Apr 28 2001 Pekka Savola <pekkas@netcore.fi>
- update to 0.6.2pl3.

* Wed Apr 11 2001 Pekka Savola <pekkas@netcore.fi>
- update to 0.6.2pl2.

* Wed Apr  4 2001 Pekka Savola <pekkas@netcore.fi>
- update to 0.62pl1.  Bye bye patches!
- Require: initscripts (should really be with a version providing IPv6)
- clean up the init script, make condrestart work properly
- Use a static /etc/rc.d/init.d; init.d/radvd required it anyway.

* Sun Apr  1 2001 Pekka Savola <pekkas@netcore.fi>
- add patch to chroot (doesn't work well yet, as /proc is used directly)
- clean up droproot patch, drop the rights earlier; require user-writable
pidfile directory
- set up the pidfile directory at compile time.

* Sat Mar 31 2001 Pekka Savola <pekkas@netcore.fi>
- add select/kill signals patch from Nathan Lutchansky <lutchann@litech.org>.
- add address syntax checked fix from Marko Myllynen <myllynen@lut.fi>.
- add patch to check the pid file before fork.
- add support for OPTIONS sourced from /etc/sysconfig/radvd, provide a nice
default one.
- add/delete radvd user, change the pidfile to /var/run/radvd/radvd.pid.
- fix initscript NETWORKING_IPV6 check.

* Sun Mar 18 2001 Pekka Savola <pekkas@netcore.fi>
- add droproot patch, change to nobody by default (should use radvd:radvd or
the like, really).

* Mon Mar  5 2001 Tim Powers <timp@redhat.com>
- applied patch supplied by Pekka Savola in #30508
- made changes to initscript as per Pekka's suggestions

* Thu Feb 15 2001 Tim Powers <timp@redhat.com>
- needed -D_GNU_SOURCE to build properly

* Tue Feb  6 2001 Tim Powers <timp@redhat.com>
- use %%configure and %%makeinstall, just glob the manpages, cleans
  things up
- fixed initscript so that it can be internationalized in the future

* Fri Feb 2 2001 Pekka Savola <pekkas@netcore.fi>
- Create a single package(source) for glibc21 and glibc22 (automatic
Requires can handle this just fine).
- use %%{_mandir} and friends
- add more flesh to %doc
- streamline %config file %attrs
- streamline init.d file a bit:
   * add a default chkconfig: (default to disable for security etc. reasons; 
     also, the default config isn't generic enough..)
   * add reload/condrestart
   * minor tweaks
   * missing: localization support (initscripts-5.60)
- use %%initdir macro

* Thu Feb 1 2001 Lars Fenneberg <lf@elemental.net>
- updated to new release 0.6.2

* Thu Feb 1 2001 Marko Myllynen <myllynen@lut.fi>
- initial version, radvd version 0.6.1
