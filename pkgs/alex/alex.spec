%global momorel 4

Name:           alex
Summary:        A lexer generator for Haskell
Version:        2.3.3
Release:        %{momorel}m%{?dist}
License:        BSD

Group:          Development/Tools
URL:            http://hackage.haskell.org/cgi-bin/hackage-scripts/package/alex
Source0:        http://hackage.haskell.org/packages/archive/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRequires:  ghc >= 6.10.4-4m
BuildRequires:  autoconf
BuildRequires:  docbook-dtds
BuildRequires:  docbook-style-xsl
BuildRequires:  libxslt
BuildRequires:  libxml2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch:  %{ix86} x86_64 ppc alpha

%description
Alex is a tool for generating lexical analysers in Haskell, given a
description of the tokens to be recognised in the form of regular
expressions.  It is similar to the tool lex or flex for C/C++.

%prep
%setup -q

%build
%cabal_configure
%cabal build

cd doc
autoreconf
./configure --prefix=%{_prefix} --libdir=%{_libdir}
make html
cd -


%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%cabal_install


%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ANNOUNCE LICENSE README TODO doc/alex examples
%{_bindir}/%{name}
%{_datadir}/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-3m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.3-3m)
- rebuild against ghc-6.10.4-4m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.3-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-1m)
- update to 2.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.1-1m)
- update


* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-3m)
- rebuild against gcc43

* Tue Dec 25 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- BuildRequires: ghc >= 6.8.1

* Mon Dec 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-1m)
- import to Momonga from Fedora devel
- version 2.2

* Fri Nov 23 2007 Bryan O'Sullivan <bos@serpentine.com> - 2.1.0-6
- Exclude alpha

* Tue Sep 25 2007 Bryan O'Sullivan <bos@serpentine.com> - 2.1.0-5
- don't try to build on ppc64

* Tue Sep 25 2007 Bryan O'Sullivan <bos@serpentine.com> - 2.1.0-4
- build requires autoconf

* Sun Jul 22 2007 Bryan O'Sullivan <bos@serpentine.com> - 2.1.0-3
- apply a few cleanups from Jens Petersen

* Tue Apr 26 2007 Bryan O'Sullivan <bos@serpentine.com> - 2.1.0-2
- fix a few style issues

* Fri Jan 19 2007 Bryan O'Sullivan <bos@serpentine.com> - 2.1.0-1
- update to 2.1.0
- fix rpmlint errors

* Fri May  6 2005 Jens Petersen <petersen@redhat.com> - 2.0.1-1
- initial packaging for Fedora Haskell based on upstream spec file
