%global momorel 1

Summary: Documentation for configuring a Samba server
Name: system-config-samba-docs
Version: 1.0.9
Release: %{momorel}m%{?dist}
URL: https://fedorahosted.org/system-config-samba-docs
License: GPLv2+
Group: Documentation
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%__id_u -n)
BuildArch: noarch
Source0: %{name}-%{version}.tar.bz2
BuildRequires: gettext
BuildRequires: pkgconfig
BuildRequires: gnome-doc-utils-devel
BuildRequires: docbook-dtds
BuildRequires: rarian
Requires: system-config-samba >= 1.2.68
Requires: rarian
Requires: yelp

%description
This package contains the online documentation for system-config-samba which is
a graphical user interface for creating, modifying, and deleting samba shares.

%prep
%setup -q

%build
# do not use _smp_mflags
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%post
%{_bindir}/scrollkeeper-update -q || :

%postun
%{_bindir}/scrollkeeper-update -q || :

%files
%defattr(-,root,root,-)
%doc COPYING
%doc %{_datadir}/omf/system-config-samba
%doc %{_datadir}/gnome/help/system-config-samba

%changelog
* Thu May 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.9-1m)
- update 1.0.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-2m)
- full rebuild for mo7 release

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-1m)
- update 1.0.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-3m)
- do not use _smp_mflags

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-1m)
- import from Fedora 11

* Tue Apr 14 2009 Nils Philippsen <nils@redhat.com> - 1.0.6-1
- add sr@latin structure (#495589, sr@latin.po by Milos Komarcevic)
- pick up updated translations

* Wed Apr 08 2009 Nils Philippsen <nils@redhat.com> - 1.0.5-1
- pull in updated translations

* Thu Dec 18 2008 Nils Philippsen <nils@redhat.com> - 1.0.4-1
- add runtime requirements for rarian-compat/scrollkeeper

* Wed Dec 17 2008 Nils Philippsen <nils@redhat.com>
- add yelp dependency

* Mon Dec 15 2008 Nils Philippsen <nils@redhat.com> - 1.0.3-1
- remove unnecessary "Obsoletes: redhat-config-samba < 1.1.5"

* Mon Dec 08 2008 Nils Philippsen <nils@redhat.com> - 1.0.2-1
- remove unnecessary "Conflicts: system-config-samba < 1.2.68"

* Fri Nov 28 2008 Nils Philippsen <nils@redhat.com> - 1.0.1-1
- separate documentation from system-config-samba
- remove stuff not related to documentation
- add source URL
