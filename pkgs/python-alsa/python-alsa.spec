%global momorel 1
%global srcname pyalsa
%global libver 1.0.24.1
%global pythonver 2.7.2
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: A Python binding for the ALSA library
Name: python-alsa
Version: 1.0.24
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Languages
URL: http://www.alsa-project.org/
Source0: ftp://ftp.alsa-project.org/pub/%{srcname}/%{srcname}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python >= %{pythonver}
BuildRequires: alsa-lib-devel >= %{libver}
BuildRequires: coreutils
BuildRequires: python-devel >= %{pythonver}
BuildRequires: python-setuptools

%description
A Python binding for the ALSA library.

%prep
%setup -q -n %{srcname}-%{version}

%build
CFLAGS="%{optflags}" python setup.py build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install -O1 --skip-build --root=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{python_sitearch}/%{srcname}
%{python_sitearch}/%{srcname}-%{version}-py2.7.egg-info

%changelog
* Sat Jun 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.24-1m)
- version 1.0.24

* Thu May  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.22-5m)
- rebuild against python-2.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.22-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.22-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.22-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.22-1m)
- initial package for alsa-tools-1.0.23
