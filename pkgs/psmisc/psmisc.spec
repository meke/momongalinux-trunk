%global momorel 1

Summary: Utilities for managing processes on your system.
Name: psmisc
Version: 22.21
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source: http://dl.sourceforge.net/sourceforge/psmisc/psmisc-%{version}.tar.gz
NoSource: 0
URL: http://psmisc.sourceforge.net
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libselinux-devel
BuildRequires: libtool
BuildRequires: ncurses-devel

Provides: /sbin/fuser

%description
The psmisc package contains utilities for managing processes on your
system: pstree, killall and fuser.  The pstree command displays a tree
structure of all of the running processes on your system.  The killall
command sends a specified signal (SIGTERM if nothing is specified) to
processes identified by name.  The fuser command identifies the PIDs
of processes that are using specified files or filesystems.

%prep
%setup -q

autoreconf -fi

%build
%configure --enable-selinux
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
# The rpm makeinstall macro breaks the build, so we do it the old way
make install DESTDIR="%{buildroot}"

mkdir -p %{buildroot}/%{_sbindir}
mv %{buildroot}%{_bindir}/fuser %{buildroot}/%{_sbindir}/

%find_lang %name

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%{_sbindir}/fuser
%{_bindir}/killall
%{_bindir}/prtstat
%{_bindir}/pstree
%{_bindir}/pstree.x11
%{_mandir}/man1/fuser.1*
%{_mandir}/man1/killall.1*
%{_mandir}/man1/prtstat.1*
%{_mandir}/man1/pstree.1*
%ifarch %ix86 x86_64 ppc ppc64
%{_bindir}/peekfd
%{_mandir}/man1/peekfd.1*
%endif

%changelog
* Sat Mar 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (22.21-1m)
- update to 22.21

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (22.19-1m)
- update to 22.19

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (22.6-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (22.6-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (22.6-6m)
- full rebuild for mo7 release

* Thu Dec 03 2009 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (22.6-5m)
- imported psmisc-22.6-fuser-remove-mountlist.patch
  and psmisc-22.6-overflow2.patch from Fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (22.6-3m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (22.6-2m)
- rebuild against rpm-4.6

* Mon Jul 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (22.6-1m)
- update to 22.6, almost sync with Fedora

* Wed Apr 23 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (22.5-4m)
- add patch for kernel-2.6.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (22.5-3m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (22.5-2m)
- remove BPR libtermcap-devel

* Sun Jun 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (22.5-1m)
- update to 22.5
- remove unused patch
- exclude peekfd on non x86 architectures

* Thu Nov 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (22.2-2m)
- move killall from %%{_bindir} to /bin for initscripts
  thanks >>822
  http://pc8.2ch.net/test/read.cgi/linux/1092539027/822
- use symbolic link to keep backwards compatibility

* Mon May 15 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (22.2-1m)
- version up

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (21.4-2m)
- revised spec for rpm 4.2.

* Wed Jan 21 2004 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (21.4-1m)
- update to 21.4
- use %%global momorel
- remove --prefix from %%configure
- use %%make and %%makeinstall
- use %%find_lang

* Tue Jul 29 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (21.3-1m)
- update to 21.3
- use %%{momorel} macro
- remove patch: psmisc-17-buildroot.patch
- add patch: psmisc-20.2-56186.patch from RawHide(psmisc-21.3-2.RHEL.0)
- use %%{_datadir} in %%build section
- add %%doc section
- use %%{_bindir} in %%files section
- remove %%{_mandir}/man1/pidof.1* from %%files section

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (20.2-2k)
- version up.

* Sat Jul 21 2001 Bernhard Rosenkraenzer <bero@redhat.com> 20.1-2
- Add BuildRequires (#49562)
- s/Copyright/License/
- Fix license (it's actually dual-licensed BSD/GPL, not just "distributable")

* Wed Apr 25 2001 Bernhard Rosenkraenzer <bero@redhat.com> 20.1-1
- 20.1

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 18 2000 Matt Wilson <msw@redhat.com>
- FHS man paths
- patch makefile to enable non-root builds

* Sat Feb  5 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Deal with compressed man pages

* Sun Nov 21 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- update to v19
- handle RPM_OPT_FLAGS

* Mon Sep 27 1999 Bill Nottingham <notting@redhat.com>
- move fuser to /sbin

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Sat Mar 13 1999 Michael Maher <mike@redhat.com>
- updated package

* Fri May 01 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 30 1998 Cristian Gafton <gafton@redhat.com>
- renamed the patch file .patch instead of .spec

* Thu Apr 09 1998 Erik Troan <ewt@redhat.com>
- updated to psmisc version 17
- buildrooted

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- updated from version 11 to version 16
- spec file cleanups

* Tue Jun 17 1997 Erik Troan <ewt@redhat.com>
- built against glibc
