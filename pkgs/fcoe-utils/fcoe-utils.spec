%global momorel 1

Name:           fcoe-utils
Version:        1.0.18
Release:        %{momorel}m%{?dist}
Summary:        Fibre Channel over Ethernet utilities

Group:          Applications/System
License:        GPLv2
URL:            http://www.open-fcoe.org
# This source was pulled from upstream git repository
# To make a tarball, just run:
# git clone git://open-fcoe.org/fcoe/fcoe-utils.git && cd fcoe-utils
# git archive --prefix=fcoe-utils-%{version}/ v%{version} > ../fcoe-utils-%{version}.tar
# cd .. && xz fcoe-utils-%{version}
Source0:        %{name}-%{version}.tar.xz
Source1:        quickstart.txt
Patch0:         fcoe-utils-1.0.12-makefile-data-hook.patch
Patch1:         fcoe-utils-1.0.14-no-vconfig.patch
Patch2:         fcoe-include-headers.patch
Patch10:        fcoe-utils-1.0.17-lldpad.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExcludeArch:	s390 ppc

BuildRequires:    libhbaapi-devel lldpad-devel
BuildRequires:    libtool automake autoconf
Requires:         lldpad libhbalinux >= 1.0.9 iproute device-mapper-multipath
Requires(post):   chkconfig
Requires(preun):  chkconfig initscripts
Requires(postun): initscripts

%description
Fibre Channel over Ethernet utilities
fcoeadm - command line tool for configuring FCoE interfaces
fcoemon - service to configure DCB Ethernet QOS filters, works with dcbd or lldpad

%prep
%setup -q
%patch0 -p1 -b .data-hook
%patch1 -p1 -b .no-vconfig
%patch2 -p1 -b .headers

%patch10 -p1 -b .lldpad

%build
./bootstrap.sh
%configure
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
install -m 644 %SOURCE1 quickstart.txt
mkdir -p %{buildroot}%{_sysconfdir}/fcoe/
cp etc/config %{buildroot}%{_sysconfdir}/fcoe/config
mkdir -p %{buildroot}%{_libexecdir}/fcoe

install -m 755 contrib/fcc.sh %{buildroot}%{_libexecdir}/fcoe/fcc.sh
install -m 755 contrib/fcoe_edd.sh %{buildroot}%{_libexecdir}/fcoe/fcoe_edd.sh
install -m 755 contrib/fcoe-setup.sh %{buildroot}%{_libexecdir}/fcoe/fcoe-setup.sh
install -m 755 debug/fcoedump.sh %{buildroot}%{_libexecdir}/fcoe/fcoedump.sh
install -m 755 debug/dcbcheck.sh %{buildroot}%{_libexecdir}/fcoe/dcbcheck.sh


%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add fcoe

%triggerun -- fcoe-utils <= 1.0.7-5
if [ -x %{_initscriptdir}/fcoe-utils ]; then
  /sbin/service fcoe-utils stop > /dev/null 2>&1
  /sbin/chkconfig fcoe-utils off
  # now copy an updated file, which we need to do proper condrestart
  sed 's/\/var\/lock\/subsys\/fcoe/\/var\/lock\/subsys\/fcoe-utils/' %{_initscriptdir}/fcoe > %{_initscriptdir}/fcoe-utils
fi

%triggerpostun -- fcoe-utils <= 1.0.7-5
if [ -x %{_initscriptdir}/fcoe-utils ]; then
  rm -f %{_initscriptdir}/fcoe-utils # this file should be already deleted, but just in case ...
fi

%preun
if [ $1 = 0 ]; then
        /sbin/service fcoe stop > /dev/null 2>&1
        /sbin/chkconfig --del fcoe
fi

%postun
if [ "$1" -ge "1" ]; then
        /sbin/service fcoe condrestart > /dev/null  2>&1 || :
fi


%files
%defattr(-,root,root,-)
%doc README COPYING quickstart.txt
%{_sbindir}/*
%{_mandir}/man8/*
%dir %{_sysconfdir}/fcoe/
%config(noreplace) %{_sysconfdir}/fcoe/config
%config(noreplace) %{_sysconfdir}/fcoe/cfg-ethx
%{_initscriptdir}/fcoe
%attr(0755,root,root) %{_libexecdir}/fcoe/fcoe_edd.sh
%attr(0755,root,root) %{_libexecdir}/fcoe/fcoe-setup.sh
%attr(0755,root,root) %{_libexecdir}/fcoe/fcc.sh
%attr(0755,root,root) %{_libexecdir}/fcoe/fcoedump.sh
%attr(0755,root,root) %{_libexecdir}/fcoe/dcbcheck.sh


%changelog
* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.18-1m)
- Initial Commit MomongaLinux

* Thu Feb 24 2011 Fabio M. Di Nitto <fdinitto@redhat.com> - 1.0.17-1
- Pull in new upstream release (required to build)
- Fix git clone URL in comments
- Drop fcoe-utils-1.0.7-init.patch, fcoe-utils-1.0.7-init-condrestart.patch
  and fcoe-utils-1.0.8-init-LSB.patch that are now upstream
- Drop fcoe-utils-1.0.8-includes.patch and use a copy of kernel headers
  for all architectures (rename fcoe-sparc.patch to fcoe-include-headers.patch)
  Upstream added detection to avoid inclusion of kernel headers in the build
  and it expects to find the userland headers installed. Those have not
  yet propagated in Fedora.
  Use temporary this workaround, since fcoe is a requiment for anaconda
  and it failed to build for a while
- Drop BuildRequires on kernel-devel
- Add BuildRequires on autoconf (it is used and not installed by default
  on all build chroots)

* Wed Feb 23 2011 Dennis Gilmore <dennis@ausil.us> - 1.0.14-5
- patch in headers used from kernel-devel on 32 bit sparc 

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.14-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Nov 30 2010 Petr Sabata <psabata@redhat.com> - 1.0.14-3
- Removing dependency on vconfig, rhbz#658525

* Mon Jun 28 2010 Jan Zeleny <jzeleny@redhat.com> - 1.0.14-2
- added device-mapper-multipath to requires (#603242)
- added missing man pages for fcrls, fcnsq and fcping
- update of init script - added condrestart, try-restart
  and force-reload options
- added vconfig to requires (#589608)

* Mon May 24 2010 Jan Zeleny <jzeleny@redhat.com> - 1.0.14-1
- rebased to 1.0.14, see bug #593824 for complete changelog

* Mon Apr 12 2010 Jan Zeleny <jzeleny@redhat.com> - 1.0.13-1
- rebased to v1.0.13, some bugfixes, new fcoe related scripts

* Tue Mar 30 2010 Jan Zeleny <jzeleny@redhat.com> - 1.0.12-2.20100323git
- some upstream updates
- better fipvlan support
- added fcoe_edd.sh script

* Tue Mar 16 2010 Jan Zeleny <jzeleny@redhat.com> - 1.0.12-1
- rebased to version 1.0.12, improved functionality with lldpad
  and dcbd
- removed /etc/fcoe/scripts/fcoeplumb

* Thu Dec 10 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.9-2.20091204git
- excluded s390 and ppc

* Fri Dec 04 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.9-1.20091204git
- rebase to latest version of fcoe-utils

* Mon Sep 14 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.8-3
- update of init script to be LSB-compliant

* Fri Jul 31 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.8-2
- patch for clean compilation without usage of upstream's ugly hack

* Thu Jul 30 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.8-1
- rebase of fcoe-utils to 1.0.8, adjusted spec file

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.7-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jun 9 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.7-7
- added quickstart file to %doc (#500759)

* Thu May 14 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.7-6
- renamed init script to fcoe, changed lock filename to fcoe
  (#497604)
- init script modified to do condrestart properly
- some modifications in spec file to apply previous change
  to older versions od init script during update
- fixed issue with accepting long options (#498551)

* Mon May 4 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.7-5
- fixed SIGSEGV when fcoe module isn't loaded (#498550)

* Wed Apr 27 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.7-4
- added libhbalinux to Requires (#497605)
- correction of spec file (_initddir -> _initrddir)

* Wed Apr 8 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.7-3
- more minor corrections in spec file

* Thu Apr 2 2009 Jan Zeleny <jzeleny@redhat.com> - 1.0.7-2
- minor corrections in spec file
- moved init script to correct location
- correction in the init script (chkconfig directives)

* Mon Mar 2 2009 Chris Leech <christopher.leech@intel.com> - 1.0.7-1
- initial rpm build of fcoe tools

