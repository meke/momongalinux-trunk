%global momorel 9

Name:           lxlauncher
Version:        0.2.1
Release:        %{momorel}m%{?dist}
Summary:        Open source replacement for Asus Launcher on the EeePC

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         lxlauncher-0.2.1-high-resolution-time-fs.patch

# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxlauncher;a=commit;h=cb99b126dd90a8460c5bd4a837fdb7505658ba52
Patch10:         lxlauncher-0.2.1-fix-SUSE-lint-warnings.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxlauncher;a=commit;h=a7dad81b883a783bc1ac4f8092a1571b7f843914
Patch11:         lxlauncher-0.2.1-fix-for-the-new-behavior-of-libmenu-cache-0.3-series.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.12 startup-notification-devel
BuildRequires:  menu-cache-devel >= 0.3.2
BuildRequires:  gettext

%description
LXLauncher is designed as an open source replacement for the Asus Launcher
included in their EeePC. It is desktop-independent and follows 
freedesktop.org specs, so newly added applications will automatically show 
up in the launcher, and vice versa for the removed ones.
LXLauncher is part of LXDE, the Lightweight X11 Desktop Environment.

%prep
%setup -q
%patch0 -p1 -b .high-resolution-time-fs

%patch10 -p1 -b .suse-lint
%patch11 -p1 -b .menu-cache-0.3.0


%build
autoreconf -fi
%configure LIBS="-lX11"
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'
mkdir -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}
mkdir -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/backgrounds
mkdir -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/icons
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%dir %{_sysconfdir}/xdg/lxlauncher/
%config(noreplace) %{_sysconfdir}/xdg/lxlauncher/gtkrc
%config(noreplace) %{_sysconfdir}/xdg/lxlauncher/settings.conf
%config(noreplace) %{_sysconfdir}/xdg/menus/lxlauncher-applications.menu
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/desktop-directories/lxde-*.directory


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-7m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-6m)
- import upstream patches (Patch10,11)

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-5m)
- fix build

* Wed Jun  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-4m)
- add BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-2m)
- fix infinite loop when build on tmpfs, ext4, xfs, ...
-- import Patch0 from Gentoo

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1
- NO.TMPFS

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-1m)
- import from Rawhide

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jun 21 2008 Christoph Wickert <fedora christoph-wickert de> - 0.2-1
- Update to 0.2
- Remove empty ChangeLog

* Mon May 12 2008 Christoph Wickert <fedora christoph-wickert de> - 0.1.6-1
- Initial Fedora RPM
