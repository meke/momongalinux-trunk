%global momorel 2
%global gcrylibdir %{_libdir}

Name: libgcrypt
Version: 1.5.3
Release: %{momorel}m%{?dist}
URL: http://www.gnupg.org/
Source0: ftp://ftp.gnupg.org/gcrypt/libgcrypt/libgcrypt-%{version}.tar.bz2
NoSource: 0
# The original libgcrypt sources now contain potentially patented ECC
# cipher support. We have to remove it in the tarball we ship with
# the hobble-libgcrypt script.
#Source0: ftp://ftp.gnupg.org/gcrypt/libgcrypt/libgcrypt-%{version}.tar.bz2
#Source1: ftp://ftp.gnupg.org/gcrypt/libgcrypt/libgcrypt-%{version}.tar.bz2.sig
Source3: hobble-libgcrypt
# Approved ECC support (from 1.5.3)
Source4: ecc.c
Source5: curves.c
# make FIPS hmac compatible with fipscheck - non upstreamable
Patch2: libgcrypt-1.5.0-use-fipscheck.patch
# fix tests in the FIPS mode, fix the FIPS-186-3 DSA keygen
Patch5: libgcrypt-1.5.0-tests.patch
# add configurable source of RNG seed and seed by default
# from /dev/urandom in the FIPS mode
Patch6: libgcrypt-1.5.0-fips-cfgrandom.patch
# make the FIPS-186-3 DSA CAVS testable
Patch7: libgcrypt-1.5.0-fips-cavs.patch
# fix for memory leaks an other errors found by Coverity scan
Patch9: libgcrypt-1.5.0-leak.patch
# use poll instead of select when gathering randomness
Patch11: libgcrypt-1.5.1-use-poll.patch
# compile rijndael with -fno-strict-aliasing
Patch12: libgcrypt-1.5.2-aliasing.patch
# slight optimalization of mpicoder.c to silence Valgrind (#968288)
Patch13: libgcrypt-1.5.2-mpicoder-gccopt.patch
# fix tests to work with approved ECC
Patch14: libgcrypt-1.5.3-ecc-test-fix.patch
# pbkdf2 speedup - upstream
Patch15: libgcrypt-1.5.3-pbkdf-speedup.patch
# fix bug in whirlpool implementation (for backwards compatibility
# with files generated with buggy version set environment
# varible GCRYPT_WHIRLPOOL_BUG
Patch16: libgcrypt-1.5.3-whirlpool-bug.patch

# Technically LGPLv2.1+, but Fedora's table doesn't draw a distinction.
# Documentation and some utilities are GPLv2+ licensed. These files
# are in the devel subpackage.
License: LGPLv2+
Summary: A general-purpose cryptography library
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gawk, libgpg-error-devel >= 1.4, pkgconfig
BuildRequires: fipscheck
Group: System Environment/Libraries

%package devel
Summary: Development files for the %{name} package
License: "LGPLv2+ and GPLv2+"
Group: Development/Libraries
Requires(pre): info
Requires(post): info
Requires: libgpg-error-devel
Requires: %{name} = %{version}-%{release}

%description
Libgcrypt is a general purpose crypto library based on the code used
in GNU Privacy Guard.  This is a development version.

%description devel
Libgcrypt is a general purpose crypto library based on the code used
in GNU Privacy Guard.  This package contains files needed to develop
applications using libgcrypt.

%prep
%setup -q
%{SOURCE3}
%patch2 -p1 -b .use-fipscheck
%patch5 -p1 -b .tests
%patch6 -p1 -b .cfgrandom
%patch7 -p1 -b .cavs
%patch9 -p1 -b .leak
%patch11 -p1 -b .use-poll
%patch12 -p1 -b .aliasing
%patch13 -p1 -b .gccopt
%patch14 -p1 -b .eccfix
%patch15 -p1 -b .pbkdf-speedup
%patch16 -p1 -b .whirlpool-bug
cp %{SOURCE4} cipher/
rm -rf tests/curves.c
cp %{SOURCE5} tests/curves.c

mv AUTHORS AUTHORS.iso88591
iconv -f ISO-8859-1 -t UTF-8 AUTHORS.iso88591 >AUTHORS

%build
%configure --disable-static \
%ifarch sparc64
     --disable-asm \
%endif
     --enable-noexecstack \
     --enable-hmac-binary-check \
     --enable-pubkey-ciphers='dsa elgamal rsa ecc' \
     --disable-O-flag-munging
make %{?_smp_mflags}

%check
fipshmac src/.libs/libgcrypt.so.??
make check

# Add generation of HMAC checksums of the final stripped binaries
%define __spec_install_post \
    %{?__debug_package:%{__debug_install_post}} \
    %{__arch_install_post} \
    %{__os_install_post} \
    fipshmac $RPM_BUILD_ROOT%{gcrylibdir}/*.so.?? \
%{nil}

%install
rm -fr %{buildroot}
make install DESTDIR=%{buildroot}

# Change /usr/lib64 back to /usr/lib.  This saves us from having to patch the
# script to "know" that -L/usr/lib64 should be suppressed, and also removes
# a file conflict between 32- and 64-bit versions of this package.
# Also replace my_host with none.
sed -i -e 's,^libdir="/usr/lib.*"$,libdir="/usr/lib",g' $RPM_BUILD_ROOT/%{_bindir}/libgcrypt-config
sed -i -e 's,^my_host=".*"$,my_host="none",g' $RPM_BUILD_ROOT/%{_bindir}/libgcrypt-config

rm -f ${RPM_BUILD_ROOT}/%{_infodir}/dir ${RPM_BUILD_ROOT}/%{_libdir}/*.la
/sbin/ldconfig -n $RPM_BUILD_ROOT/%{_libdir}

%if "%{gcrylibdir}" != "%{_libdir}"
# Relocate the shared libraries to %{gcrylibdir}.
mkdir -p $RPM_BUILD_ROOT%{gcrylibdir}
for shlib in $RPM_BUILD_ROOT%{_libdir}/*.so* ; do
        if test -L "$shlib" ; then
                rm "$shlib"
        else
                mv "$shlib" $RPM_BUILD_ROOT%{gcrylibdir}/
        fi
done

# Overwrite development symlinks.
pushd $RPM_BUILD_ROOT/%{_libdir}
for shlib in %{gcrylibdir}/lib*.so.* ; do
        shlib=`echo "$shlib" | sed -e 's,//,/,g'`
        target=`basename "$shlib" | sed -e 's,\.so.*,,g'`.so
        ln -sf $shlib $target
done
popd

# Add soname symlink.
/sbin/ldconfig -n $RPM_BUILD_ROOT/%{_lib}/
%endif


# Create /etc/gcrypt (hardwired, not dependent on the configure invocation) so
# that _someone_ owns it.
mkdir -p -m 755 $RPM_BUILD_ROOT/etc/gcrypt

%clean
rm -fr %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
[ -f %{_infodir}/gcrypt.info.bz2 ] && \
    /sbin/install-info %{_infodir}/gcrypt.info %{_infodir}/dir
exit 0

%preun devel
if [ $1 = 0 -a -f %{_infodir}/gcrypt.info.bz2 ]; then
    /sbin/install-info --delete %{_infodir}/gcrypt.info %{_infodir}/dir
fi
exit 0

%files
%defattr(-,root,root,-)
%dir /etc/gcrypt
%{_libdir}/libgcrypt.so.*
%{_libdir}/.libgcrypt.so.*.hmac
%doc COPYING.LIB AUTHORS NEWS THANKS

%files devel
%defattr(-,root,root,-)
%{_bindir}/%{name}-config
%{_bindir}/dumpsexp
%{_bindir}/hmac256
%{_includedir}/*
%{_libdir}/*.so
%{_datadir}/aclocal/*

%{_infodir}/gcrypt.info*
%doc COPYING

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.3-2m)
- support UserMove env

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-1m)
- [SECURITY] CVE-2013-4242
- update to 1.5.3

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Thu Nov 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-2m)
- import bug fix patches from fedora

* Mon Jul  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update to 1.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.6-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-2m)
- explicitly link libgpg-error

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-1m)
- update to 1.4.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-4m)
- do not specify info file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-3m)
- rebuild against rpm-4.6

* Sat Jun 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-2m)
- add --enable-static again
-- we still need static library for cyrptsetup-lunks

* Mon Jun  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-1m)
- update 1.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.4-3m)
- rebuild against gcc43

* Mon Mar 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-2m)
- libgcyrpt-devel requires libgpg-error-devel

* Sun Feb 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-2m)
- delete libtool library

* Thu Oct  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Fri May 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-3m)
- revise %%files for rpm-4.4.2

* Sun Jan  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-2m)
- BuildPreReq: libgpg-error -> libgpg-error-devel

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.2-1m)
- up to 1.2.2

* Sat Jan 29 2005 TAKAHASHI Tamotsu <tamo>
- (1.2.1-1m)

* Fri May 07 2004 TAKAHASHI Tamotsu <tamo>
- (1.2.0-1m)
- the first stable version
- api changed

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.12-2m)
- revised spec for enabling rpm 4.2.

* Tue Jan 21 2003 TAKAHASHI Tamotsu
- (1.1.12-1m)
- bugfixes, new functions and manual enhancements
- configure: without --disable-asm
- URL: http://www.gnu.org/directory/security/libgcrypt.html
- doc: one entry per line
- doc: new stuff COPYING.DOC and COPYING.LIB
- doc: doc/ commented out

* Wed Jan 01 2003 TAKAHASHI Tamotsu
- (1.1.11-1m)
- libgcrypt.txt unlisted from files section
- minor changes (MD4, ciphertext stealing, bugfixes, etc)

* Sat Oct 05 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.1.10-1m)
- verup
- pre/post are processed in if-fi

* Fri Sep 13 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.1.9-1m)
- corrected info (un)installation

* Thu Jul 04 2002 TAKAHASHI Tamotsu <arms405@jade.dti.ne.jp>
- (1.1.8-1m)
- updated and imported to momonga
- gnu.org/aegypten not found. URL: gnupg.org/
- a dot was needed in info --entry

* Thu May 02 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.1.6-0k)
- kondarized
- modified summary and description (from libgcrypt.txt)
- added docs
- CC=gcc (for ccache... really needed?)
- added URL tag... where's the page in gnupg.org?
- not committed yet

* Tue Apr  2 2002 Nalin Dahyabhai <nalin@redhat.com> 1.1.6-1
- update to 1.1.6

* Thu Jan 10 2002 Nalin Dahyabhai <nalin@redhat.com> 1.1.5-1
- fix the Source tag so that it's a real URL

* Wed Dec 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- initial package
