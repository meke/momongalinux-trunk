%global momorel 14
%global ocamlver 3.11.2

%global ruby18_sitearchdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')
%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')

Summary: Program analyzer for source code search engine
Name: langscan
Version: 1.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
Source0: http://dl.sourceforge.net/sourceforge/gonzui/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: langscan-1.2-ruby18.patch
URL: http://gonzui.sourceforge.net/langscan/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18
BuildRequires: flex >= 2.5.31
BuildRequires: ocaml >= %{ocamlver}
Requires: ocaml-runtime >= %{ocamlver}

%description
LangScan is a program analyzer for source code search engine.

%prep
%setup -q
%patch0 -p1 -b .ruby18

%build
autoreconf -fi
%configure \
    --with-ruby=%{_bindir}/ruby18 \
    --with-rubydir=%{ruby18_sitelibdir} \
    --with-rubyarchdir=%{ruby18_sitearchdir}
%{__make}

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install
%{__chmod} 755 %{buildroot}%{ruby18_sitearchdir}/langscan/ocaml/camlexer

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{ruby18_sitelibdir}/langscan.rb
%{ruby18_sitelibdir}/langscan
%{ruby18_sitearchdir}/langscan

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-12m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-11m)
- build with ruby18

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-10m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-8m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-7m)
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-6m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-5m)
- rebuild against ocaml-3.10.2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-4m)
- %%NoSource -> NoSource

* Sat Sep 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-3m)
- rebuild against ocaml-3.10.0-1m

* Mon Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-2m)
- rebuild against ruby-1.8.6-4m

* Thu Oct 12 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-1m)
- first package
