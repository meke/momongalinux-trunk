%global momorel 1

Name:		tokyopromenade
Summary:	Content Management System use tokyocabinet
Version:	0.9.25
Release:	%{momorel}m%{?dist}
License:	GPLv3+
Group:		Development/Libraries
URL:		http://fallabs.com/tokyopromenade/
Source0:        http://fallabs.com/tokyopromenade/pkg/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  tokyocabinet >= 1.4.45
BuildRequires:  zlib-devel, bzip2-devel

%description
Tokyo Promenade is a content management system.

%prep
%setup -q

%build
%configure --libexecdir=%{_libdir}
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%{_bindir}/*
%{_mandir}/*/*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/*
%{_libdir}/promenade.cgi
%{_libdir}/promscrcount.lua
%{_libdir}/promscrsample.lua
%{_libdir}/promupdiff.sh
%{_libdir}/promupping.sh

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.25-1m)
- update to 0.9.25

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.22-2m)
- rebuild for new GCC 4.6

* Sun Dec 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.22-1m)
- update to 0.9.22
- change URI

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.21-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.21-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.21-1m)
- update 0.9.21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.14-1m)
- update 0.9.14

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.11-1m)
- update 0.9.11

* Thu Aug 13 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-1m)
- initial package
