%global		momorel 1
Name:          clutter
Version:       1.12.2
Release:       %{momorel}m%{?dist}
Summary:       Open Source software library for creating rich graphical user interfaces

Group:         Development/Libraries
License:       LGPLv2+
URL:           http://www.clutter-project.org/
Source0:       http://ftp.gnome.org/pub/gnome/sources/%{name}/1.12/%{name}-%{version}.tar.xz
NoSource:      0

BuildRequires: glib2-devel mesa-libGL-devel pkgconfig pango-devel
BuildRequires: cairo-gobject-devel gdk-pixbuf2-devel atk-devel
BuildRequires: cogl-devel >= 1.10.0
BuildRequires: gobject-introspection-devel >= 1.33.10
BuildRequires: gtk3-devel
BuildRequires: json-glib-devel >= 0.12.0
BuildRequires: libXcomposite-devel
BuildRequires: libXdamage-devel
BuildRequires: libXi-devel
Requires:      gobject-introspection

# Obsolete clutter packages that have been removed from Fedora
Obsoletes:     clutter-gesture < 0.0.2-3
Obsoletes:     clutter-gesture-devel < 0.0.2-3
Obsoletes:     clutter-imcontext < 0.1.6-5
Obsoletes:     clutter-imcontext-devel < 0.1.6-5
Obsoletes:     clutter-imcontext-docs < 0.1.6-5

%description
Clutter is an open source software library for creating fast,
visually rich graphical user interfaces. The most obvious example
of potential usage is in media center type applications.
We hope however it can be used for a lot more.

%package devel
Summary:       Clutter development environment
Group:         Development/Libraries
Requires:      %{name} = %{version}-%{release}
Requires:      pkgconfig glib2-devel pango-devel fontconfig-devel
Requires:      mesa-libGL-devel
Requires:      gobject-introspection-devel

%description devel
Header files and libraries for building a extension library for the
clutter

%package       doc
Summary:       Documentation for %{name}
Group:         Documentation
Requires:      %{name} = %{version}-%{release}

%description   doc
Clutter is an open source software library for creating fast,
visually rich graphical user interfaces. The most obvious example
of potential usage is in media center type applications.
We hope however it can be used for a lot more.

This package contains documentation for clutter.

%prep
%setup -q

%build
(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; CONFIGFLAGS=--enable-gtk-doc; fi;
 %configure $CONFIGFLAGS \
	--enable-xinput
 # clutter git ships with some magic to put the git log in shipped tarballs
 # which gets listed in files; don't blow up if it's missing
 if ! test -f ChangeLog; then
   echo "Created from snapshot" > ChangeLog
 fi
)

make V=1

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%find_lang clutter-1.0

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f clutter-1.0.lang
%doc COPYING NEWS README
%{_libdir}/*.so.0
%{_libdir}/*.so.0.*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%doc ChangeLog
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/*.gir

%files doc
%{_datadir}/gtk-doc/html/clutter
%{_datadir}/gtk-doc/html/cally

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.2-1m)
- update to 1.12.2

* Wed Sep 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.0-1m)
- update to 1.12.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.16-1m)
- update to 1.11.16

* Mon Sep 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.14-1m)
- update to 1.11.14

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.12-1m)
- update to 1.11.12

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.10-1m)
- update to 1.11.10

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.8-1m)
- update to 1.11.8

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.4-1m)
- reimport from fedora
- rebuild for cogl-1.10.2 

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.4-2m)
- rebuild for glib 2.33.2

* Mon Jan 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.4-1m)
- update 1.8.4

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-1m)
- update 1.8.2

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0-1m)
- update 1.8.0

* Wed Sep 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.90-1m)
- update 1.7.90

* Wed Jun 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.16-1m)
- update to 1.6.16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.14-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.14-1m)
- update to 1.6.14

* Tue Apr  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.12-1m)
- update to 1.6.12

* Wed Feb 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.6-1m)
- update to 1.6.6

* Mon Jan 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-1m)
- update to 1.4.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.12-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.12-1m)
- update 1.2.12

* Wed Jun  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.10-1m)
- update 1.2.10

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.8-2m)
- add --with-json=internal for gnome-shell (json-glib)

* Thu May 20 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.8-1m)
- update 1.2.8

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.6-1m)
- update 1.2.6

* Mon Mar  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0

* Mon Jan 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.10-1m)
- update 1.0.10

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-1m)
- update 1.0.8

* Thu Sep 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6

* Tue Sep 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-4m)
- fix files

* Thu Sep  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- BuildRequires: gir-repository

* Wed Sep  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-2m)
- enable introspection

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-1m)
- update to 1.0.4

* Sun Aug 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-1m)
- update to 1.0.2

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- update to 1.0.0-stable

* Tue Jul 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-1m)
- update to 0.9.8

* Wed Jun 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-1m)
- update to 0.9.4

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2-1m)
- update to 0.9.2

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.8-1m)
- update to 0.8.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-2m)
- rebuild against rpm-4.6

* Thu Dec  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-1m)
- update to 0.8.4

* Tue Nov 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- Initial commit Momonga Linux

* Mon Oct  6 2008 Allisson Azevedo <allisson@gmail.com> 0.8.2-1
- Update to 0.8.2
- Removed clutter-0.8.0-clutter-fixed.patch

* Sat Sep  6 2008 Allisson Azevedo <allisson@gmail.com> 0.8.0-1
- Update to 0.8.0
- Added clutter-0.8.0-clutter-fixed.patch

* Sat Jun 14 2008 Allisson Azevedo <allisson@gmail.com> 0.6.4-1
- Update to 0.6.4

* Sat May 17 2008 Allisson Azevedo <allisson@gmail.com> 0.6.2-1
- Update to 0.6.2

* Tue Feb 19 2008 Allisson Azevedo <allisson@gmail.com> 0.6.0-1
- Update to 0.6.0

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.4.2-2
- Autorebuild for GCC 4.3

* Wed Oct  3 2007 Allisson Azevedo <allisson@gmail.com> 0.4.2-1
- Update to 0.4.2

* Mon Sep  3 2007 Allisson Azevedo <allisson@gmail.com> 0.4.1-1
- Update to 0.4.1

* Sat Jul 21 2007 Allisson Azevedo <allisson@gmail.com> 0.3.1-1
- Update to 0.3.1

* Thu Apr 12 2007 Allisson Azevedo <allisson@gmail.com> 0.2.3-1
- Update to 0.2.3

* Sun Mar 28 2007 Allisson Azevedo <allisson@gmail.com> 0.2.2-4
- Changed buildrequires and requires

* Sun Mar 27 2007 Allisson Azevedo <allisson@gmail.com> 0.2.2-3
- Fix .spec

* Sun Mar 24 2007 Allisson Azevedo <allisson@gmail.com> 0.2.2-2
- Fix .spec

* Sun Mar 23 2007 Allisson Azevedo <allisson@gmail.com> 0.2.2-1
- Initial RPM release
