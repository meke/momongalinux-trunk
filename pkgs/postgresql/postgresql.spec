%global momorel 5

# This is the PostgreSQL Global Development Group Official RPMset spec file,
# or a derivative thereof.
# Copyright 2003-2009 Lamar Owen <lowen@pari.edu> <lamar.owen@wgcr.org>
# and others listed.

# Major Contributors:
# ---------------
# Lamar Owen
# Trond Eivind Glomsrd <teg@redhat.com>
# Thomas Lockhart
# Reinhard Max
# Karl DeBisschop
# Peter Eisentraut
# Joe Conway
# Andrew Overholt
# David Jee
# Kaj J. Niemi
# Sander Steffann
# Tom Lane
# and others in the Changelog....

# This spec file and ancilliary files are licensed in accordance with
# The PostgreSQL license.

# In this file you can find the default build package list macros.
# These can be overridden by defining on the rpm command line:
# rpm --define 'packagename 1' .... to force the package to build.
# rpm --define 'packagename 0' .... to force the package NOT to build.
# The base package, the lib package, the devel package, and the server package
# always get built.

%global beta 0
%{?beta:%global __os_install_post /usr/lib/rpm/brp-compress}

%{!?test:%global test 1}
%{!?upgrade:%global upgrade 1}
%{!?plpython:%global plpython 1}
%{!?pltcl:%global pltcl 1}
%{!?plperl:%global plperl 1}
%{!?ssl:%global ssl 1}
%{!?kerberos:%global kerberos 1}
%{!?ldap:%global ldap 1}
%{!?nls:%global nls 1}
%{!?uuid:%global uuid 1}
%{!?xml:%global xml 1}
%{!?pam:%global pam 1}
%{!?sdt:%global sdt 1}
%{!?selinux:%global selinux 1}
%{!?runselftest:%global runselftest 1}

Summary: PostgreSQL client programs
Name: postgresql
%global majorversion 9.2
Version: 9.2.4
Release: %{momorel}m%{?dist}
# The PostgreSQL license is very similar to other MIT licenses, but the OSI
# recognizes it as an independent license, so we do as well.
License: "PostgreSQL"
Group: Applications/Databases
Url: http://www.postgresql.org/

# This SRPM includes a copy of the previous major release, which is needed for
# in-place upgrade of an old database.  In most cases it will not be critical
# that this be kept up with the latest minor release of the previous series.
%global prevversion 9.1.9
%global prevmajorversion 9.1

Source0: ftp://ftp.postgresql.org/pub/source/v%{version}/postgresql-%{version}.tar.bz2
NoSource: 0
## http://www.postgresql.org/files/documentation/pdf/9.2/postgresql-9.2-A4.pdf
Source1: http://www.postgresql.org/files/documentation/pdf/%{majorversion}/postgresql-9.2.3-A4.pdf
Source3: ftp://ftp.postgresql.org/pub/source/v%{prevversion}/postgresql-%{prevversion}.tar.bz2
NoSource: 3
Source4: postgresql-check-db-dir
Source5: Makefile.regress
Source6: pg_config.h
Source7: ecpg_config.h
Source8: README.rpm-dist
Source9: postgresql-setup
Source10: postgresql.service
Source14: postgresql.pam
Source15: postgresql-bashprofile
Source16: filter-requires-perl-Pg.sh

Patch0: %{name}-%{version}-ac-version.patch
Patch1: rpm-pgsql.patch
Patch2: postgresql-logging.patch
Patch3: postgresql-perl-rpath.patch

BuildRequires: perl-ExtUtils-MakeMaker glibc-devel bison flex autoconf gawk
BuildRequires: perl-ExtUtils-Embed perl-devel
BuildRequires: readline-devel zlib-devel
BuildRequires: systemd-units

%if %plpython
BuildRequires: python-devel >= 2.7
%endif

%if %pltcl
BuildRequires: tcl
BuildRequires: tcl-devel
%endif

%if %ssl
BuildRequires: openssl-devel >= 1.0.0
%endif

%if %kerberos
BuildRequires: krb5-devel
%endif

%if %ldap
BuildRequires: openldap-devel
%endif

%if %nls
BuildRequires: gettext >= 0.10.35
%endif

%if %uuid
BuildRequires: uuid-devel
%endif

%if %xml
BuildRequires: libxml2-devel libxslt-devel
%endif

%if %pam
BuildRequires: pam-devel
%endif

%if %sdt
BuildRequires: systemtap-sdt-devel
%endif

%if %selinux
BuildRequires: libselinux-devel
%endif

# main package requires -libs subpackage
Requires: postgresql-libs = %{version}-%{release}

Obsoletes: postgresql-clients
Obsoletes: postgresql-perl
Obsoletes: postgresql-tk
Obsoletes: rh-postgresql

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
PostgreSQL is an advanced Object-Relational database management system
(DBMS) that supports almost all SQL constructs (including
transactions, subselects and user-defined types and functions). The
postgresql package includes the client programs and libraries that
you'll need to access a PostgreSQL DBMS server.  These PostgreSQL
client programs are programs that directly manipulate the internal
structure of PostgreSQL databases on a PostgreSQL server. These client
programs can be located on the same machine with the PostgreSQL
server, or may be on a remote machine which accesses a PostgreSQL
server over a network connection. This package contains the docs
in HTML for the whole package, as well as command-line utilities for
managing PostgreSQL databases on a PostgreSQL server.

If you want to manipulate a PostgreSQL database on a local or remote PostgreSQL
server, you need this package. You also need to install this package
if you're installing the postgresql-server package.

%package libs
Summary: The shared libraries required for any PostgreSQL clients
Group: Applications/Databases
Provides: libpq.so = %{version}-%{release}
Obsoletes: rh-postgresql-libs
# for /sbin/ldconfig
Requires(post): glibc
Requires(postun): glibc

%description libs
The postgresql-libs package provides the essential shared libraries for any
PostgreSQL client program or interface. You will need to install this package
to use any other PostgreSQL package or any clients that need to connect to a
PostgreSQL server.

%package server
Summary: The programs needed to create and run a PostgreSQL server
Group: Applications/Databases
Requires: postgresql = %{version}-%{release}
Requires(pre): shadow-utils
# for /sbin/ldconfig
Requires(post): glibc
Requires(postun): glibc
# pre/post stuff needs systemd too
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
# This is actually needed for the %%triggerun script but Requires(triggerun)
# is not valid.  We can use post because this particular %%triggerun script
# should fire just after this package is installed.
Requires(post): systemd-sysv
Requires(post): chkconfig
Obsoletes: rh-postgresql-server

%description server
The postgresql-server package includes the programs needed to create
and run a PostgreSQL server, which will in turn allow you to create
and maintain PostgreSQL databases.  PostgreSQL is an advanced
Object-Relational database management system (DBMS) that supports
almost all SQL constructs (including transactions, subselects and
user-defined types and functions). You should install
postgresql-server if you want to create and maintain your own
PostgreSQL databases and/or your own PostgreSQL server. You also need
to install the postgresql package.


%package docs
Summary: Extra documentation for PostgreSQL
Group: Applications/Databases
Requires: postgresql = %{version}-%{release}
Obsoletes: rh-postgresql-docs

%description docs
The postgresql-docs package includes some additional documentation for
PostgreSQL.  Currently, this includes the main documentation in PDF format
and source files for the PostgreSQL tutorial.


%package contrib
Summary: Contributed modules distributed with PostgreSQL
Group: Applications/Databases
Requires: postgresql = %{version}-%{release}
Obsoletes: rh-postgresql-contrib

%description contrib
The postgresql-contrib package contains contributed packages that are
included in the PostgreSQL distribution.


%package devel
Summary: PostgreSQL development header files and libraries
Group: Development/Libraries
Requires: postgresql = %{version}-%{release}
Obsoletes: rh-postgresql-devel

%description devel
The postgresql-devel package contains the header files and libraries
needed to compile C or C++ applications which will directly interact
with a PostgreSQL database management server and the ecpg Embedded C
Postgres preprocessor. You need to install this package if you want to
develop applications which will interact with a PostgreSQL server.

%if %upgrade
%package upgrade
Summary: Support for upgrading from the previous major release of PostgreSQL
Group: Applications/Databases
Requires: %{name}-server = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description upgrade
The postgresql-upgrade package contains the pg_upgrade utility and supporting
files needed for upgrading a PostgreSQL database from the previous major
version of PostgreSQL.
%endif

%if %plperl
%package plperl
Summary: The Perl procedural language for PostgreSQL
Group: Applications/Databases
Requires: postgresql-server = %{version}-%{release}
Obsoletes: rh-postgresql-pl
Obsoletes: postgresql-pl
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description plperl
PostgreSQL is an advanced Object-Relational database management
system.  The postgresql-plperl package contains the PL/Perl
procedural language for the backend.
%endif

%if %plpython
%package plpython
Summary: The Python procedural language for PostgreSQL
Group: Applications/Databases
Requires: postgresql-server = %{version}-%{release}
Obsoletes: rh-postgresql-pl
Obsoletes: postgresql-pl
Obsoletes: postgresql-python

%description plpython
PostgreSQL is an advanced Object-Relational database management
system.  The postgresql-plpython package contains the PL/Python
procedural language for the backend.
%endif

%if %pltcl
%package pltcl
Summary: The Tcl procedural language for PostgreSQL
Group: Applications/Databases
Requires: postgresql-server = %{version}-%{release}
Obsoletes: rh-postgresql-pl
Obsoletes: postgresql-pl
Obsoletes: postgresql-tcl

%description pltcl
PostgreSQL is an advanced Object-Relational database management
system.  The postgresql-pltcl package contains the PL/Tcl
procedural language for the backend.
%endif

%if %test
%package test
Summary: The test suite distributed with PostgreSQL
Group: Applications/Databases
Requires: postgresql-server = %{version}-%{release}
Obsoletes: rh-postgresql-test

%description test
PostgreSQL is an advanced Object-Relational database management
system. The postgresql-test package includes the sources and pre-built
binaries of various tests for the PostgreSQL database management
system, including regression tests and benchmarks.
%endif

%global __perl_requires %{SOURCE16}

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

cp -p %{SOURCE1} .

%if %upgrade
tar xfj %{SOURCE3}
%endif

%build

# fail quickly and obviously if user tries to build as root
%if %runselftest
	if [ x"`id -u`" = x0 ]; then
		echo "postgresql's regression tests fail if run as root."
		echo "If you really need to build the RPM as root, use"
		echo "--define='runselftest 0' to skip the regression tests."
		exit 1
	fi
%endif

CFLAGS="${CFLAGS:-%optflags}" ; export CFLAGS

# Strip out -ffast-math from CFLAGS....
CFLAGS=`echo $CFLAGS|xargs -n 1|grep -v ffast-math|xargs -n 100`
# Add LINUX_OOM_ADJ=0 to ensure child processes reset postmaster's oom_adj
CFLAGS="$CFLAGS -DLINUX_OOM_ADJ=0"
# let's try removing this kluge, it may just be a workaround for bz#520916
# # use -O1 on sparc64 and alpha
# %ifarch sparc64 alpha
# CFLAGS=`echo $CFLAGS| sed -e "s|-O2|-O1|g" `
# %endif

%configure --disable-rpath \
%if %beta
	--enable-debug \
	--enable-cassert \
%endif
%if %plperl
	--with-perl \
%endif
%if %pltcl
	--with-tcl \
	--with-tclconfig=%{_libdir} \
%endif
%if %plpython
	--with-python \
%endif
%if %ldap
	--with-ldap \
%endif
%if %ssl
	--with-openssl \
%endif
%if %pam
	--with-pam \
%endif
%if %kerberos
	--with-krb5 \
	--with-gssapi \
%endif
%if %uuid
	--with-ossp-uuid \
%endif
%if %xml
	--with-libxml \
	--with-libxslt \
%endif
%if %nls
	--enable-nls \
%endif
%if %sdt
	--enable-dtrace \
%endif
%if %selinux
	--with-selinux \
%endif
	--with-system-tzdata=/usr/share/zoneinfo \
	--datadir=/usr/share/pgsql

make %{?_smp_mflags} world

# Have to hack makefile to put correct path into tutorial scripts
sed "s|C=\`pwd\`;|C=%{_libdir}/pgsql/tutorial;|" < src/tutorial/Makefile > src/tutorial/GNUmakefile
make %{?_smp_mflags} -C src/tutorial NO_PGXS=1 all
rm -f src/tutorial/GNUmakefile

%if %runselftest
	pushd src/test/regress
	make all
	make MAX_CONNECTIONS=5 check
	make clean
	popd
	pushd src/pl
	make MAX_CONNECTIONS=5 check ||:
	popd
	pushd contrib
	make MAX_CONNECTIONS=5 check
	popd
%endif

%if %test
	pushd src/test/regress
	make all
	popd
%endif

%if %upgrade
	pushd postgresql-%{prevversion}

	# The upgrade build can be pretty stripped-down, but make sure that
	# any options that affect on-disk file layout match the previous
	# major release!  Also, note we intentionally do not use %%configure
	# here, because we *don't* want its ideas about installation paths.
	./configure --build=%{_build} --host=%{_host} \
		--prefix=%{_libdir}/pgsql/postgresql-%{prevmajorversion} \
		--disable-rpath \
		--with-system-tzdata=/usr/share/zoneinfo

	make %{?_smp_mflags} all

	popd
%endif

%install
rm -rf $RPM_BUILD_ROOT

make DESTDIR=$RPM_BUILD_ROOT install-world

# multilib header hack; note pg_config.h is installed in two places!
# we only apply this to known Red Hat multilib arches, per bug #177564
case `uname -i` in
  i386 | x86_64 | ppc | ppc64 | s390 | s390x | sparc | sparc64 )
    mv $RPM_BUILD_ROOT/usr/include/pg_config.h $RPM_BUILD_ROOT/usr/include/pg_config_`uname -i`.h
    install -m 644 %{SOURCE6} $RPM_BUILD_ROOT/usr/include/
    mv $RPM_BUILD_ROOT/usr/include/pgsql/server/pg_config.h $RPM_BUILD_ROOT/usr/include/pgsql/server/pg_config_`uname -i`.h
    install -m 644 %{SOURCE6} $RPM_BUILD_ROOT/usr/include/pgsql/server/
    mv $RPM_BUILD_ROOT/usr/include/ecpg_config.h $RPM_BUILD_ROOT/usr/include/ecpg_config_`uname -i`.h
    install -m 644 %{SOURCE7} $RPM_BUILD_ROOT/usr/include/
    ;;
  *)
    ;;
esac

install -d -m 755 $RPM_BUILD_ROOT%{_libdir}/pgsql/tutorial
cp src/tutorial/* $RPM_BUILD_ROOT%{_libdir}/pgsql/tutorial

# prep the setup script, including insertion of some values it needs
sed -e 's|^PGVERSION=.*$|PGVERSION=%{version}|' \
    -e 's|^PGENGINE=.*$|PGENGINE=%{_bindir}|' \
    -e 's|^PREVMAJORVERSION=.*$|PREVMAJORVERSION=%{prevmajorversion}|' \
    -e 's|^PREVPGENGINE=.*$|PREVPGENGINE=%{_libdir}/pgsql/postgresql-%{prevmajorversion}/bin|' \
    <%{SOURCE9} >postgresql-setup
install -m 755 postgresql-setup $RPM_BUILD_ROOT%{_bindir}/postgresql-setup

# prep the startup check script, including insertion of some values it needs
sed -e 's|^PGVERSION=.*$|PGVERSION=%{version}|' \
	-e 's|^PREVMAJORVERSION=.*$|PREVMAJORVERSION=%{prevmajorversion}|' \
	-e 's|^PGDOCDIR=.*$|PGDOCDIR=%{_docdir}/%{name}-%{version}|' \
	<%{SOURCE4} >postgresql-check-db-dir
touch -r %{SOURCE4} postgresql-check-db-dir
install -m 755 postgresql-check-db-dir $RPM_BUILD_ROOT%{_bindir}/postgresql-check-db-dir

install -d $RPM_BUILD_ROOT%{_unitdir}
install -m 644 %{SOURCE10} $RPM_BUILD_ROOT%{_unitdir}/postgresql.service

%if %pam
install -d $RPM_BUILD_ROOT/etc/pam.d
install -m 644 %{SOURCE14} $RPM_BUILD_ROOT/etc/pam.d/postgresql
%endif

# PGDATA needs removal of group and world permissions due to pg_pwd hole.
install -d -m 700 $RPM_BUILD_ROOT/var/lib/pgsql/data

# backups of data go here...
install -d -m 700 $RPM_BUILD_ROOT/var/lib/pgsql/backups

# postgres' .bash_profile
install -m 644 %{SOURCE15} $RPM_BUILD_ROOT/var/lib/pgsql/.bash_profile

%if %upgrade
	pushd postgresql-%{prevversion}
	make DESTDIR=$RPM_BUILD_ROOT install
	popd

	# remove stuff we don't actually need for upgrade purposes
	pushd $RPM_BUILD_ROOT%{_libdir}/pgsql/postgresql-%{prevmajorversion}
	rm bin/clusterdb
	rm bin/createdb
	rm bin/createlang
	rm bin/createuser
	rm bin/dropdb
	rm bin/droplang
	rm bin/dropuser
	rm bin/ecpg
	rm bin/initdb
	rm bin/pg_config
	rm bin/pg_controldata
	rm bin/pg_dump
	rm bin/pg_dumpall
	rm bin/pg_restore
	rm bin/psql
	rm bin/reindexdb
	rm bin/vacuumdb
	rm -rf include
	rm lib/dict_snowball.so
	rm lib/libecpg*
	rm lib/libpg*
	rm lib/libpq*
	rm -rf lib/pgxs
	rm lib/plpgsql.so
	rm -rf share/doc
	rm -rf share/man
	rm -rf share/tsearch_data
	rm share/*.bki
	rm share/*description
	rm share/*.sample
	rm share/*.sql
	rm share/*.txt
	popd
%endif

%if %test
	# tests. There are many files included here that are unnecessary,
	# but include them anyway for completeness.  We replace the original
	# Makefiles, however.
	mkdir -p $RPM_BUILD_ROOT%{_libdir}/pgsql/test
	cp -a src/test/regress $RPM_BUILD_ROOT%{_libdir}/pgsql/test
	# pg_regress binary should be only in one subpackage,
	# there will be a symlink from -test to -devel
	rm -f $RPM_BUILD_ROOT%{_libdir}/pgsql/test/regress/pg_regress
	ln -sf ../../pgxs/src/test/regress/pg_regress $RPM_BUILD_ROOT%{_libdir}/pgsql/test/regress/pg_regress
	pushd  $RPM_BUILD_ROOT%{_libdir}/pgsql/test/regress
	rm -f GNUmakefile Makefile *.o
	chmod 0755 pg_regress regress.so
	popd
	cp %{SOURCE5} $RPM_BUILD_ROOT%{_libdir}/pgsql/test/regress/Makefile
	chmod 0644 $RPM_BUILD_ROOT%{_libdir}/pgsql/test/regress/Makefile
%endif

# Fix some more documentation
# gzip doc/internals.ps
cp %{SOURCE8} README.rpm-dist
mv $RPM_BUILD_ROOT%{_docdir}/pgsql/html doc
rm -rf $RPM_BUILD_ROOT%{_docdir}/pgsql

# remove files not to be packaged
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a
%if !%upgrade
rm -f $RPM_BUILD_ROOT%{_bindir}/pg_upgrade
rm -f $RPM_BUILD_ROOT%{_libdir}/pgsql/pg_upgrade_support.so
%endif

%if %nls
%find_lang ecpg-%{majorversion}
cat ecpg-%{majorversion}.lang >>devel.lst
%find_lang ecpglib6-%{majorversion}
cat ecpglib6-%{majorversion}.lang >>libs.lst
%find_lang initdb-%{majorversion}
cat initdb-%{majorversion}.lang >>server.lst
%find_lang libpq5-%{majorversion}
cat libpq5-%{majorversion}.lang >>libs.lst
%find_lang pg_basebackup-%{majorversion}
cat pg_basebackup-%{majorversion}.lang >>server.lst
%find_lang pg_controldata-%{majorversion}
cat pg_controldata-%{majorversion}.lang >>server.lst
%find_lang pg_ctl-%{majorversion}
cat pg_ctl-%{majorversion}.lang >>server.lst
%find_lang pg_config-%{majorversion}
cat pg_config-%{majorversion}.lang >>main.lst
%find_lang pg_dump-%{majorversion}
cat pg_dump-%{majorversion}.lang >>main.lst
%find_lang pg_resetxlog-%{majorversion}
cat pg_resetxlog-%{majorversion}.lang >>server.lst
%find_lang pgscripts-%{majorversion}
cat pgscripts-%{majorversion}.lang >>main.lst
%if %plperl
%find_lang plperl-%{majorversion}
cat plperl-%{majorversion}.lang >>plperl.lst
%endif
%find_lang plpgsql-%{majorversion}
cat plpgsql-%{majorversion}.lang >>server.lst
%if %plpython
%find_lang plpython-%{majorversion}
cat plpython-%{majorversion}.lang >>plpython.lst
%endif
%if %pltcl
%find_lang pltcl-%{majorversion}
cat pltcl-%{majorversion}.lang >>pltcl.lst
%endif
%find_lang postgres-%{majorversion}
cat postgres-%{majorversion}.lang >>server.lst
%find_lang psql-%{majorversion}
cat psql-%{majorversion}.lang >>main.lst
%endif

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%pre server
groupadd -g 26 -o -r postgres >/dev/null 2>&1 || :
useradd -M -N -g postgres -o -r -d /var/lib/pgsql -s /bin/bash \
	-c "PostgreSQL Server" -u 26 postgres >/dev/null 2>&1 || :

%post server
/sbin/ldconfig
if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

# Run this when upgrading from SysV initscript to native systemd unit
%triggerun server -- postgresql-server < %{first_systemd_version}
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply postgresql
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save postgresql >/dev/null 2>&1 || :

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del postgresql >/dev/null 2>&1 || :
/bin/systemctl try-restart postgresql.service >/dev/null 2>&1 || :

%preun server
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable postgresql.service >/dev/null 2>&1 || :
    /bin/systemctl stop postgresql.service >/dev/null 2>&1 || :
fi

%postun server
/sbin/ldconfig
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart postgresql.service >/dev/null 2>&1 || :
fi

%if %plperl
%post -p /sbin/ldconfig plperl
%postun -p /sbin/ldconfig plperl
%endif

%if %plpython
%post -p /sbin/ldconfig plpython
%postun -p /sbin/ldconfig plpython
%endif

%if %pltcl
%post -p /sbin/ldconfig pltcl
%postun -p /sbin/ldconfig pltcl
%endif

%clean
rm -rf $RPM_BUILD_ROOT

# FILES section.

%files -f main.lst
%defattr(-,root,root)
%doc doc/KNOWN_BUGS doc/MISSING_FEATURES doc/TODO
%doc COPYRIGHT README HISTORY doc/bug.template
%doc README.rpm-dist
%doc doc/html
%{_bindir}/clusterdb
%{_bindir}/createdb
%{_bindir}/createlang
%{_bindir}/createuser
%{_bindir}/dropdb
%{_bindir}/droplang
%{_bindir}/dropuser
%{_bindir}/pg_config
%{_bindir}/pg_dump
%{_bindir}/pg_dumpall
%{_bindir}/pg_restore
%{_bindir}/psql
%{_bindir}/reindexdb
%{_bindir}/vacuumdb
%{_mandir}/man1/clusterdb.*
%{_mandir}/man1/createdb.*
%{_mandir}/man1/createlang.*
%{_mandir}/man1/createuser.*
%{_mandir}/man1/dropdb.*
%{_mandir}/man1/droplang.*
%{_mandir}/man1/dropuser.*
%{_mandir}/man1/pg_config.*
%{_mandir}/man1/pg_dump.*
%{_mandir}/man1/pg_dumpall.*
%{_mandir}/man1/pg_restore.*
%{_mandir}/man1/psql.*
%{_mandir}/man1/reindexdb.*
%{_mandir}/man1/vacuumdb.*
%{_mandir}/man7/*
%dir %{_libdir}/pgsql

%files docs
%defattr(-,root,root)
%doc *-A4.pdf
%{_libdir}/pgsql/tutorial/

%files contrib
%defattr(-,root,root)
%{_datadir}/pgsql/extension/adminpack*
%{_datadir}/pgsql/extension/autoinc*
%{_datadir}/pgsql/extension/btree_gin*
%{_datadir}/pgsql/extension/btree_gist*
%{_datadir}/pgsql/extension/chkpass*
%{_datadir}/pgsql/extension/citext*
%{_datadir}/pgsql/extension/cube*
%{_datadir}/pgsql/extension/dblink*
%{_datadir}/pgsql/extension/dict_int*
%{_datadir}/pgsql/extension/dict_xsyn*
%{_datadir}/pgsql/extension/earthdistance*
%{_datadir}/pgsql/extension/file_fdw*
%{_datadir}/pgsql/extension/fuzzystrmatch*
%{_datadir}/pgsql/extension/hstore*
%{_datadir}/pgsql/extension/insert_username*
%{_datadir}/pgsql/extension/intagg*
%{_datadir}/pgsql/extension/intarray*
%{_datadir}/pgsql/extension/isn*
%{_datadir}/pgsql/extension/lo*
%{_datadir}/pgsql/extension/ltree*
%{_datadir}/pgsql/extension/moddatetime*
%{_datadir}/pgsql/extension/pageinspect*
%{_datadir}/pgsql/extension/pg_buffercache*
%{_datadir}/pgsql/extension/pg_freespacemap*
%{_datadir}/pgsql/extension/pg_stat_statements*
%{_datadir}/pgsql/extension/pg_trgm*
%{_datadir}/pgsql/extension/pgcrypto*
%{_datadir}/pgsql/extension/pgrowlocks*
%{_datadir}/pgsql/extension/pgstattuple*
%{_datadir}/pgsql/extension/refint*
%{_datadir}/pgsql/extension/seg*
%{_datadir}/pgsql/extension/tablefunc*
%{_datadir}/pgsql/extension/tcn*
%{_datadir}/pgsql/extension/test_parser*
%{_datadir}/pgsql/extension/timetravel*
%{_datadir}/pgsql/extension/tsearch2*
%{_datadir}/pgsql/extension/unaccent*
%{_libdir}/pgsql/_int.so
%{_libdir}/pgsql/adminpack.so
%{_libdir}/pgsql/auth_delay.so
%{_libdir}/pgsql/autoinc.so
%{_libdir}/pgsql/auto_explain.so
%{_libdir}/pgsql/btree_gin.so
%{_libdir}/pgsql/btree_gist.so
%{_libdir}/pgsql/chkpass.so
%{_libdir}/pgsql/citext.so
%{_libdir}/pgsql/cube.so
%{_libdir}/pgsql/dblink.so
%{_libdir}/pgsql/dict_int.so
%{_libdir}/pgsql/dict_xsyn.so
%{_libdir}/pgsql/dummy_seclabel.so
%{_libdir}/pgsql/earthdistance.so
%{_libdir}/pgsql/file_fdw.so
%{_libdir}/pgsql/fuzzystrmatch.so
%{_libdir}/pgsql/hstore.so
%{_libdir}/pgsql/insert_username.so
%{_libdir}/pgsql/isn.so
%{_libdir}/pgsql/lo.so
%{_libdir}/pgsql/ltree.so
%{_libdir}/pgsql/moddatetime.so
%{_libdir}/pgsql/pageinspect.so
%{_libdir}/pgsql/passwordcheck.so
%{_libdir}/pgsql/pg_buffercache.so
%{_libdir}/pgsql/pg_freespacemap.so
%{_libdir}/pgsql/pg_trgm.so
%{_libdir}/pgsql/pgcrypto.so
%{_libdir}/pgsql/pgrowlocks.so
%{_libdir}/pgsql/pgstattuple.so
%{_libdir}/pgsql/pg_stat_statements.so
%{_libdir}/pgsql/refint.so
%{_libdir}/pgsql/seg.so
%{_libdir}/pgsql/tablefunc.so
%{_libdir}/pgsql/tcn.so
%{_libdir}/pgsql/test_parser.so
%{_libdir}/pgsql/timetravel.so
%{_libdir}/pgsql/tsearch2.so
%{_libdir}/pgsql/unaccent.so
%if %selinux
%{_datadir}/pgsql/contrib/sepgsql.sql
%{_libdir}/pgsql/sepgsql.so
%endif
%if %ssl
%{_datadir}/pgsql/extension/sslinfo*
%{_libdir}/pgsql/sslinfo.so
%endif
%if %uuid
%{_datadir}/pgsql/extension/uuid-ossp*
%{_libdir}/pgsql/uuid-ossp.so
%endif
%if %xml
%{_datadir}/pgsql/extension/xml2*
%{_libdir}/pgsql/pgxml.so
%endif
%{_bindir}/oid2name
%{_bindir}/pg_archivecleanup
%{_bindir}/pg_standby
%{_bindir}/pg_test_fsync
%{_bindir}/pg_test_timing
%{_bindir}/pgbench
%{_bindir}/vacuumlo
%{_mandir}/man1/oid2name.*
%{_mandir}/man1/pg_archivecleanup.*
%{_mandir}/man1/pg_standby.*
%{_mandir}/man1/pg_test_fsync.*
%{_mandir}/man1/pg_test_timing.*
%{_mandir}/man1/pgbench.*
%{_mandir}/man1/vacuumlo.*
%{_mandir}/man3/dblink*
%doc contrib/spi/*.example
%dir %{_datadir}/pgsql/contrib/

%files libs -f libs.lst
%defattr(-,root,root)
%{_libdir}/libpq.so.*
%{_libdir}/libecpg.so.*
%{_libdir}/libpgtypes.so.*
%{_libdir}/libecpg_compat.so.*

%files server -f server.lst
%defattr(-,root,root)
%{_unitdir}/postgresql.service
%if %pam
%config(noreplace) /etc/pam.d/postgresql
%endif
%{_bindir}/initdb
%{_bindir}/pg_basebackup
%{_bindir}/pg_controldata
%{_bindir}/pg_ctl
%{_bindir}/pg_receivexlog
%{_bindir}/pg_resetxlog
%{_bindir}/postgres
%{_bindir}/postmaster
%{_bindir}/postgresql-check-db-dir
%{_bindir}/postgresql-setup
%{_mandir}/man1/initdb.*
%{_mandir}/man1/pg_basebackup.*
%{_mandir}/man1/pg_controldata.*
%{_mandir}/man1/pg_ctl.*
%{_mandir}/man1/pg_receivexlog.*
%{_mandir}/man1/pg_resetxlog.*
%{_mandir}/man1/postgres.*
%{_mandir}/man1/postmaster.*
%{_datadir}/pgsql/postgres.bki
%{_datadir}/pgsql/postgres.description
%{_datadir}/pgsql/postgres.shdescription
%{_datadir}/pgsql/system_views.sql
%{_datadir}/pgsql/*.sample
%{_datadir}/pgsql/timezonesets/
%{_datadir}/pgsql/tsearch_data/
%{_libdir}/pgsql/dict_snowball.so
%{_libdir}/pgsql/euc2004_sjis2004.so
%{_libdir}/pgsql/libpqwalreceiver.so
%{_libdir}/pgsql/plpgsql.so
%dir %{_datadir}/pgsql
%{_datadir}/pgsql/extension/plpgsql*
%attr(700,postgres,postgres) %dir /var/lib/pgsql
%attr(700,postgres,postgres) %dir /var/lib/pgsql/data
%attr(700,postgres,postgres) %dir /var/lib/pgsql/backups
%attr(644,postgres,postgres) %config(noreplace) /var/lib/pgsql/.bash_profile
%{_libdir}/pgsql/*_and_*.so
%{_datadir}/pgsql/conversion_create.sql
%{_datadir}/pgsql/information_schema.sql
%{_datadir}/pgsql/snowball_create.sql
%{_datadir}/pgsql/sql_features.txt

%files devel -f devel.lst
%defattr(-,root,root)
%{_includedir}/*
%{_bindir}/ecpg
%{_libdir}/libpq.so
%{_libdir}/libecpg.so
%{_libdir}/libecpg_compat.so
%{_libdir}/libpgtypes.so
%{_libdir}/pgsql/pgxs/
%{_mandir}/man1/ecpg.*
%{_mandir}/man3/SPI_*

%if %upgrade
%files upgrade
%defattr(-,root,root)
%{_bindir}/pg_upgrade
%{_libdir}/pgsql/pg_upgrade_support.so
%{_libdir}/pgsql/postgresql-%{prevmajorversion}
%{_mandir}/man1/pg_upgrade.*
%endif

%if %plperl
%files plperl -f plperl.lst
%defattr(-,root,root)
%{_datadir}/pgsql/extension/plperl*
%{_libdir}/pgsql/plperl.so
%endif

%if %pltcl
%files pltcl -f pltcl.lst
%defattr(-,root,root)
%{_libdir}/pgsql/pltcl.so
%{_bindir}/pltcl_delmod
%{_bindir}/pltcl_listmod
%{_bindir}/pltcl_loadmod
%{_datadir}/pgsql/extension/pltcl*
%{_datadir}/pgsql/unknown.pltcl
%endif

%if %plpython
%files plpython -f plpython.lst
%defattr(-,root,root)
%{_datadir}/pgsql/extension/plpython*
%{_libdir}/pgsql/plpython2.so
%endif

%if %test
%files test
%defattr(-,postgres,postgres)
%attr(-,postgres,postgres) %{_libdir}/pgsql/test/*
%attr(-,postgres,postgres) %dir %{_libdir}/pgsql/test
%endif

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.4-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.4-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.4-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.4-2m)
- rebuild against perl-5.18.0

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.4-1m)
- [SECURITY] CVE-2013-1899 CVE-2013-1900 CVE-2013-1901 CVE-2013-1902
- [SECURITY] CVE-2013-1903
- update to 9.2.4
- PDF document for 9.2.4 was not released

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.3-2m)
- rebuild against perl-5.16.3

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.3-1m)
- [SECURITY] CVE-2013-0255
- update to 9.2.3

* Fri Jan  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.2-1m)
- update to 9.2.2

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.1-2m)
- rebuild against perl-5.16.2

* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.2.1-1m)
- update to 9.2.1

* Sat Aug 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.5-1m)
- [SECURITY] CVE-2012-3488 CVE-2012-3489
- update to 9.1.5

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.4-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.4-2m)
- rebuild against perl-5.16.0

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.4-1m)
- [SECURITY] CVE-2012-2143 CVE-2012-2655
- update to 9.1.4

* Sat Jun  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.3-2m)
- [SECURITY] CVE-2012-2143

* Sun Mar 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.1.3-1m)
- update to 9.1.3

* Tue Feb 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.11-1m)
- [SECURITY] CVE-2012-0866 CVE-2012-0867 CVE-2012-0868
- update to 8.4.11

* Fri Nov 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.9-2m)
- support systemd

* Wed Nov  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.9-1m)
- [SECURITY] CVE-2011-2483
- update to 8.4.9

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.8-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.8-4m)
- rebuild against perl-5.14.1

* Thu May  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.8-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon May  2 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (8.4.8-2m)
- rebuild against python-2.7

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.8-1m)
- update to 8.4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.4.7-2m)
- rebuild for new GCC 4.6

* Tue Feb  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.7-1m)
- [SECURITY] CVE-2010-4015
- update to 8.4.7

* Sat Dec 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.6-1m)
- update to 8.4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.4.5-2m)
- rebuild for new GCC 4.5

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.5-1m)
- [SECURITY] CVE-2010-3433
- http://www.postgresql.org/about/news.1244
- update to 8.4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.4-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.4.4-2m)
- full rebuild for mo7 release

* Wed May 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.4-1m)
- [SECURITY] CVE-2010-1169 CVE-2010-1170 CVE-2010-1447 CVE-2010-1975
- http://www.postgresql.org/about/news.1203
- update to 8.4.4

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.3-5m)
- rebuild against perl-5.12.1

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.4.3-4m)
- rebuild against readline6

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.4.3-3m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.4.3-2m)
- rebuild against openssl-1.0.0

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.4.3-1m)
- [SECURITY] CVE-2010-0442
- update to 8.4.3

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.4.2-2m)
- Obsoletes: postgresql-python
- Obsoletes: postgresql-tcl

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.4.2-1m)
- [SECURITY] CVE-2010-0733
- sync with Fedora 13 (8.4.2-7)

* Thu Dec 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.9-1m)
- [SECURITY] CVE-2009-4034 CVE-2009-4136
- update to 8.3.9 for mo6 updates

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.8-1m)
- [SECURITY] CVE-2009-3229 CVE-2009-3230 CVE-2009-3231
- update to 8.3.8

* Wed Sep 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.7-6m)
- temporarily disable dtrace (Patch7). can not build
  src/backend/utils/probes.o with systemtap-0.9.9-2m.
  passing an -o parameter to gcc in dtrace is probably incorrect.
  see /usr/bin/dtarce.
-- https://bugzilla.redhat.com/show_bug.cgi?id=520469

* Fri Sep 11 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (8.3.7-5m)
- make utf8 for default encoding when initdb

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (8.3.7-4m)
- rebuild against perl-5.10.1

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.7-3m)
- enable sdt

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.7-2m)
- rebuild against openssl-0.9.8k

* Wed Mar 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.7-1m)
- [SECURITY] CVE-2009-0922
- update to 8.3.7
- disable dtrace

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.5-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.3.5-2m)
- rebuild against python-2.6.1-1m

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.5-1m)
- sync with Rawhide (8.3.5-2)
- License: Modified BSD

* Thu Jul 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.3.3-1m)
- sync with Fedora devel (8.3.3-2)

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.3.0-3m)
- rebuild against openssl-0.9.8h-1m

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (8.3.0-3m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.3.0-2m)
- rebuild against gcc43

* Mon Mar 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.3.0-1m)
- update to 8.3.0
- sync with Fedora devel

* Wed Jan  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2.6-1m)
- [SECURITY] CVE-2007-4769 CVE-2007-4772 CVE-2007-6067 CVE-2007-6600 CVE-2007-6601
- update to 8.2.6
- sync with Fedora devel

* Sat Sep 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (8.2.5-1m)
- [SECURITY] fix following two security issues
-- Restrict /contrib/pgstattuple functions to superusers
-- Require non-superusers who use /contrib/dblink to use only password authentication
- update to 8.2.5

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.2.4-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Mon Jun  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.2.4-1m)
- [SECURITY] CVE-2007-2138
- update 8.2.4
- update pgtcl1.6.0.tar.gz pgtcldocs-20070115.zip

* Sun Apr 22 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (8.2.3-4m)
- rebuild against perl-5.8.8-8m

* Sun Apr  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (8.2.3-3m)
- require python-libs

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.2.3-2m)
- modify Requires for mph-get-check

* Fri Mar  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.2.3-1m)
- merge from fc-devel
- changelog is below
- * Wed Feb 14 2007 Karsten Hopp <karsten@redhat.com> 8.2.3-2
- - rebuild with tcl-8.4
-
- * Wed Feb  7 2007 Tom Lane <tgl@redhat.com> 8.2.3-1
- - Update to PostgreSQL 8.2.3 due to regression induced by security fix
- Resolves: #227522
-
- * Sun Feb  4 2007 Tom Lane <tgl@redhat.com> 8.2.2-1
- - Update to PostgreSQL 8.2.2 to fix CVE-2007-0555, CVE-2007-0556
- Related: #225496
-
- * Fri Jan 12 2007 Tom Lane <tgl@redhat.com> 8.2.1-2
- - Split -pl subpackage into three new packages to reduce dependencies
-   and track upstream project's packaging.
-
- * Wed Jan 10 2007 Tom Lane <tgl@redhat.com> 8.2.1-1
- - Update to PostgreSQL 8.2.1
- - Update to pgtcl 1.5.3
- - Be sure we link to libncurses, not libtermcap which is disappearing in Fedora
-
- * Thu Dec  7 2006 Jeremy Katz <katzj@redhat.com> - 8.2.0-2
- - rebuild for python 2.5
-
- * Mon Dec  4 2006 Tom Lane <tgl@redhat.com> 8.2.0-1
- - Update to PostgreSQL 8.2.0
- - Update to PyGreSQL 3.8.1
- - Fix chcon arguments in test/regress/Makefile
- Related: #201035
- - Adjust init script to not fool /etc/rc.d/rc
- Resolves: #161470
- - Change init script to not do initdb automatically, but require
-   manual "service postgresql initdb" for safety.  Per upstream discussions.

* Sun Jul 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1.4-4m)
- add Patch5: pgtcl-no-rpath.patch , Patch6: postgresql-perl-rpath.patch
  and Patch7: pgtcl-quote.patch from fc-devel
  for resolve wired quote problem (bash-3.1 makes this)
- add -q at unzip %%{S:9}

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (8.1.4-3m)
- rebuild against readline-5.0

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (8.1.4-2m)
- delete conflict files (pg_controldata.mo plpython.so)

* Thu May 25 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.1.4-1m)
- update to 8.1.4
- [SECURITY] CVE-2006-2313 CVE-2006-2314

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (8.1.3-3m)
- rebuild against openssl-0.9.8a

* Mon Mar 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.1.3-2m)
- add src Source6 Source7 (kossori shi sugi)

* Thu Feb 16 2006 Kazuhiko <kazuhiko@fdiary.net>
- (8.1.3-1m)
- containing a patch for a serious security issue

* Wed Jan 11 2006 Kazuhiko <kazuhiko@fdiary.net>
- (8.1.2-1m)
- critical fix repairs an error in ReadBuffer that can cause data loss
  due to overwriting recently-added pages

* Thu Dec 22 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (8.1.1-3m)
- update Japanese html documents and Japanese manpages.

* Fri Dec 16 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (8.1.1-2m)
- revise specfile for JDBC. (conflicted source numbering)
- PyGreSQL update to 3.7
- update JDBC version.(build 8.1-404)

* Fri Dec 16 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (8.1.1-1m)
- minor version up. bug fix release.

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (8.1.0-1m)
- major version up.
- This update requires dump and restore.
  Please execute 'pg_dumpall' before update.

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
-  (8.0.4-2m)
- build against python-2.4.2

* Wed Oct 12 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (8.0.4-1m)
- up to 8.0.4
- bugfix release

* Thu May 19 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.0.3-1m)
- up to 8.0.3
- [SECURITY] CAN-2005-1409 CAN-2005-1410

* Mon Apr 25 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (8.0.2-1m)
- up to 8.0.2
- [SECURITY] CAN-2005-0247

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (8.0.1-4m)
- rebuild against libtermcap and ncurses

* Tue Feb 8 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (8.0.1-3m)
- add nls support patch
- update document

* Sat Feb 5 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (8.0.1-2m)
- add Require: postgresql-libs to postgresql package
- remove Provides: libpq.so from postgresql-libs

* Fri Feb 4 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (8.0.1-1m)
- upgrade to 8.0.1
- pgtclsh, pgtksh is removed.
- JDBC Type1 driver is not avaiable.
- PyGreSQL 3.6.1

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (7.4.6-2m)
- enable x86_64.

* Tue Jan 11 2005 zunda <zunda at freeshell.org>
- (kossori)
- add BuildPrereq: gettext for msgfmt

* Mon Oct 25 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.4.6-1m)
- security fix release.
  see. http://www.postgresql.org/news/234.html

* Mon Sep 27 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.4.5-1m)
- ver. up

* Sun Sep 12 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (7.4.3-4m)
- rebuild against python2.3

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.4.3-3m)
- build against perl-5.8.5

* Mon Jul 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.4.3-2m)
- No NoSource 9 ftp://ftp.pygresql.org/pub/distrib/PyGreSQL-3.4.tgz

* Mon Jul 12 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (7.4.3-1m)
- update to 7.4.3

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (7.4.1-6m)
- remove Epoch from BuildPrereq

* Mon Jun 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (7.4.1-5m)
- rebuild against db4

* Fri May 22 2004 Toru Hoshina <t@momonga-linux.org>
- (7.4.1-4m)
- with icc.  orz...

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (7.4.1-3m)
- revised spec for rpm 4.2.

* Tue Jan 27 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.4.1-2m)
- update PyGreSQL.

* Wed Dec 24 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (7.4.1-1m)
- update.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.3.4-4m)
- rebuild against perl-5.8.2

* Tue Nov  4 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.3.4-3m)
- update japanese html docs.

* Fri Oct 24 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.3.4-2m)
- change jdbc source URI.

* Wed Aug 13 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.3.4-1m)
- update upstream version.
- add japanese html docs.

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.3.2-3m)
- rebuild against zlib 1.1.4-5m
- change Name: tag to top of spec file

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (7.3.2-2m)
  rebuild against openssl 0.9.7a

* Wed Feb 12 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.3.2-1m)
- update to upstream version.

* Tue Jan 21 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.3.1-3m)
- fix specfile for libpq.so.

* Fri Jan 17 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.3.1-2m)
- patch ate wasure...

* Thu Jan 16 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.3.1-1m)
- major version up.

* Wed Jan 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (7.2.3-2m)
- [security] patches to fix vulnerabilities (from postgresql-7.2.3-5.73.src.rpm)
   Patch2: buffer overrun patch for geo_ops
   Patch3: pg_hba.conf tightening patch

* Mon Dec  2 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.2.3-1m)
- minor update.

* Sat Nov 30 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.2.2-5m)
- update tcl, tk at BuildPrereq and Requires

* Mon Nov 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (7.2.2-4m)
- rebuild against perl-5.8.0

* Sun Oct 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (7.2.2-3m)
- remove needless symlinks

* Wed Sep 4 2002 kikutani <poo@momonga-linux.org>
- (7.2.2-2m)
- Provides: relational-database

* Sat Aug 31 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (7.2.2-1m)
- Security Release
  see http://archives.neohapsis.com/archives/bugtraq/2002-08/0258.html

* Fri Jul 12 2002 Kikutani Makoto <poo@momonga-linux.org>
- (7.2.1-11m)
- 1st Momonga version
- udc_to_utf.diff.gz
- postgresql-7.2.newencoding.diff.gz

* Sat Jun  1 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (7.2.1-10k)
- BuildPreReq: pam-devel.

* Tue May 14 2002 Toru Hoshina <t@kondara.org>
- (7.2.1-8k)
- No more meaningless files.lst.

* Tue May 14 2002 Shigeyuki Yamashita <shige@kondara.org>
- (7.2.1-6k)
- ukekeketohehehe

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (7.2.1-4k)
- sumanu-.

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (7.2.1-2k)
- ver up.

* Thu Feb 21 2002 Tsutomu Yasuda <tom@kondara.org>
- (7.1.3-8k)
- supported python2.2

* Mon Nov 19 2001 Toru Hoshina <t@kondara.org>
- Change some require tags to prereq.

* Wed Oct 31 2001 Masaru Sato <masachan@kondara.org>
- change PostgreSQL source file URI.

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (7.1.3-4k)
- no more --with-template=linux_alpha, right?

* Sat Oct 27 2001 Masaru Sato <masachan@kondara.org>
- (7.1.3-2k)
- up to 7.1.3

* Sat Oct 14 2001 Masaru Sato <masachan@kondara.org>
- Modify URL tag, http://www.postgresql.org/ to http://www.us.postgresql.org/
- Because old url is mirror site list

* Sat Sep 22 2001 Toru Hoshina <t@kondara.org>
- (7.0.3-10k)
- applied new jumbo patch (jumbo-20010528).

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- (7.0.3-8k)
- add alphaev5 support.

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (7.0.3-7k)
- rebuild againt rpm-3.0.5-39k

* Sun Jan  7 2001 Toru Hoshina <toru@df-usa.com>
- remove JDBC support, because we don't have Java to redistribute.
- fixed initscript file.lst. knock knock, who's there?
- Why do you use redhat-style-files.lst for both /etc/init.d and /etc/rc.d ?

* Wed Jan 03 2001 Kusunoki Masanori <nori@kondara.org>
- (7.0.3-3k)
- Marge 7.0.2-5k(Kondara Project) and 7.0.3-2(PostgreSQL Official)
- initscriptdir detect method changed. for compatible with Official RPM.
- Multibyte supported JDBC Driver added.

* Thu Nov 30 2000 AYUHANA Tomonori <l@kondara.org>
- (7.0.2-5k)
- grob /var/lib/pgsql by postgres:postgres
- Buildroot: /var/tmp/%{name}-%{version}-root

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Wed Nov 15 2000 Lamar Owen <lamar@postgresql.org>
- Buggy dependency on /lib/cpp -- not a PreReq, but a BuildPreReq.

* Tue Nov 14 2000 Trond Eivind Glomsrod <teg@redhat.com>
- made a template for IA64 (symlink to i386)

* Mon Nov 13 2000 Trond Eivind Glomsrod <teg@redhat.com>
- add patch for IA64 (I got one from Lamar and modified it)

* Mon Nov 13 2000 Lamar Owen <lamar@postgresql.org>
- RELEASE: 7.0.3-1
- Minor edits to initscript.
- PRE-RELEASE: 7.0.3-0.6 (final test)
- Final 7.0.3 tarball.
- Cross-dist changes to RedHat initscript for better portability.
- changed README.rpm name to README.rpm-dist (the .rpm extension confused
--some GUI file managers, as RealPlayer also uses that extension).
- Edited README.rpm-dist to reflect cross-distribution packaging.
- NOTE: SuSE cross-compatibility is not yet complete  SuSE RPM's for 7.0.3
--that are tailored for SuSE will be made available.
- Fixed stupid dependency -- the main .so's were in the devel package (Arggh)

* Sat Nov 11 2000 Lamar Owen <lamar@postgresql.org>
- PRE-RELEASE: 7.0.3-0.5
- Framework for better distribution-independent build
- Conditional around libtool conf stuff copy (For RedHat 6.1 and such).
- Conditional around RedHat-style initscript stuff.
- Comments at top of spec file now list copyright and license for spec file,
--as the license and copyright for the spec file itself has not previously
--been stated.
- Envvars for postgres home dir login in .bash_profile.
- initscript now checks for success and failure functions using:
--typeset -f|grep "declare -f function_name" construct.

* Fri Nov 10 2000 Lamar Owen <lamar@postgresql.org>
- PRE-RELEASE: 7.0.3-0.4
- Directory ownership on /usr/lib/pgsql/test/regress was root.root.
- Patch Makefile.shlib to not use -soname for RPM's.
- Newer prerelease 7.0.3 tarball.

* Thu Nov 09 2000 Lamar Owen <lamar@postgresql.org>
- PRE-RELEASE: 7.0.3-0.3
- Don't bother copying the test stuff I'm not packaging.
- Fix group add/del stuff in server subpackage post and postun scriptlets.
- symlink /usr/lib/libpq.so.2.0 -> libpq.so.2.1

* Mon Nov 06 2000 Lamar Owen <lamar@postgresql.org>
- PRE-RELEASE: 7.0.3-0.2
- preliminary 7.0.3 distribution tarball -- reinstated man pages, etc.

* Sat Nov 04 2000 Lamar Owen <lamar@postgresql.org>
- PRE-RELEASE: 7.0.3-0.1.
- Improved test subpackage -- only package regression.
- Latest pre-7.0.3 snapshot.
- Split out SuSE and Caldera spec files.  Caldera COL eServer 2.3 requires
-- RPM 2.5.5-compatible spec files.  SuSE is just too different from the others
-- for the spec file to cope, for now.
- TODO: man pages and documentation.

* Mon Oct 30 2000 Lamar Owen <lamar@postgresql.org>
- pre-7.0.3

* Mon Oct 30 2000 Lamar Owen <lamar@postgresql.org>
- Reenabled the test subpackage for ftp.postgresql.org dist.
- Backported to RH 6.2 -- involved the Pg man page, doesn't appear to require
-- other stuff to be done.


* Sun Oct 22 2000 Trond Eivind Glomsrod <teg@redhat.com>
- make /etc/logrotate.d/postgres 0644 instead of 0700 (#19520)


* Thu Oct 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild with new glibc which has semaphore fixes for Alpha

* Tue Sep 05 2000 Trond Eivind Glomsrod <teg@redhat.com>
- add documention for python interface (#17261)
- move the python interface tutorial to the %%doc section

* Thu Aug 24 2000 Trond Eivind Glomsrod <teg@redhat.com>
- the old dump script didn't work - added rh-pgdump.sh
  to handle this. Point docs at it, and tell how it is to be used.

* Mon Aug 21 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fix the initscript so it points you at the 7.0.2 directory
  in /usr/share/doc, not 7.0  (#16163). Also, remove statement
  it was built on a 6.2 system.
- prereq /lib/cpp and initscripts
- fix backups of existing files (#16706)
- fix conditional restart

* Sat Aug 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fix README.rpm to it points at /usr/share/doc, not /usr/doc
  (part of #16416)

* Wed Aug 16 2000 Trond Eivind Glomsrod <teg@redhat.com>
- don't build test package anymore, it's broken. These
  tests should be run by pgsql developers and not
  by db-developers, so it's not a big loss (#16165).
  Obsolete it in the main package, so it doesn't get left over

* Mon Aug 14 2000 Trond Eivind Glomsrod <teg@redhat.com>
- reference docs in /usr/share/doc, not /usr/doc (#16163)
- add python-devel, perl and tcl as build prereqs
- use /dev/null as STDIN for su commands in initscripts,
  to avoid error messages from not being able to read from
  tty

* Sat Aug 05 2000 Bill Nottingham <notting@redhat.com>
- condrestart fixes

* Mon Jul 31 2000 Trond Eivind Glomsrod <teg@redhat.com>
- remove all plperl references, to avoid confusing post install scripts
- cleanups

* Mon Jul 17 2000 Trond Eivind Glomsrod <teg@redhat.com>
- remove the symlink from libpq.so.2.0 to libpq.so.2.1
- remove some binaries from docs
- fix dangling symlink os.h
- use /sbin/service

* Thu Jul 13 2000 Trond Eivind Glomsrod <teg@redhat.com>
- don't strip manually
- fixes to init script so they look more like the rest
  (#13749, from giulioo@pobox.com)
- use /etc/rc.d/init.d again (sigh)

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jul 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- "Prereq:", not "Requires:" for /etc/init.d

* Thu Jul 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- require /etc/init.d

* Wed Jun 21 2000 Trond Eivind Glomsrod <teg@redhat.com>
- remove perl kludge as perl 5.6 is now fixed
- include the man page for the perl module
- fix the init script and spec file to handle conditional
  restart
- move the init file to /etc/init.d
- use License instead of Copyright

* Mon Jun 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild

* Tue Jun 13 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%defattr on postgresql-perl
- use %%{_tmppath}
- Don't use release number in patch
- Don't build on ia64 yet

* Mon Jun 12 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0.2-2
- Corrected misreporting of version.
- Corrected for non-root build clean script.

* Mon Jun 05 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0.2
- Postgresql-dump manpage to man1, and to separate source file to facilitate
-- _mandir macro expansion correctness.
- NOTE: The PostScript documentation is no longer being included in the
-- PostgreSQL tarball.  If demand is such, I will pull together a
-- postgresql-ps-docs subpackage or pull in the PostScript docs into the
-- main package.
- RPM patchset has release number, now, to prevent patchfile confusion :-(.


* Sat Jun 03 2000 Lamar Owen <lamar.owen@wgcr.org>
- Incorporate most of Trond's changes (reenabled the alpha
-- patches, as it was a packaging error on my part).
- Trimmed changelog history to Version 7.0beta1 on. To see the
-- previous changelog, grab the 6.5.3 RPM from RedHat 6.2 and pull the spec.
- Rev to 7.0.1 (which incorporates the syslog patch, which has
-- been removed from rpm-pgsql-7.0.1-1.patch)

* Fri May 26 2000 Trond Eivind Glomsrod <teg@redhat.com>
- disable the alpha patch, as it doesn't apply cleanly
- removed distribution, packager, vendor
- renamed spec file
- don't build pl-perl
- use %%{_mandir}
- now includes vacuumdb.1*

* Thu May 25 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0-3
- Incorporated Tatsuo's syslog segmentation patches
- Incorporated some of Trond's changes (see below)
-- Fixed some Perl 5.6 oddness in Rawhide
- Incorporated some of Karl's changes (see below)
-- PL/Perl should now work.
- Fixed missing /usr/bin/pg_passwd.

* Mon May 22 2000 Karl DeBisschop <kdebisschop@infoplease.com>
- 7.0-2.1
- make plperl module (works for linux i386, your guess for other platforms)
- use "make COPT=" because postgreSQL configusre script ignores CFLAGS

* Sat May 20 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0-2
- pg_options default values changed.
- SPI headers (again!) fixed in a permanent manner  -- hopefully!
- Alpha patches!

* Wed May 17 2000 Trond Eivind Glomsrod <teg@redhat.com>
- changed bug in including man pages

* Tue May 16 2000 Trond Eivind Glomsrod <teg@redhat.com>
- changed buildroot, removed packager, vendor, distribution
-- [Left all but buildroot as-is for PostgreSQL.org RPMS. LRO]
- don't strip in package [strip in PostgreSQL.org RPMS]
- fix perl weirdnesses (man page in bad location, remove
  perllocal.pod from file list)

* Mon May 15 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0 final -1
- Man pages restructured
- Changed README.rpm notices about BETA
- incorporated minor changes from testing
- still no 7.0 final alpha patches -- for -2 or -3, I guess.
- 7.0 JDBC jars!

* Sat May 06 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0RC5-0.5
- UserID of 26 to conform to RedHat Standard, instead of 40.  This only
-- is for new installs -- upgrades will use what was already there.
- Waiting on built jar's of JDBC.  If none are forthcoming by release,
-- I'm going to have to bite the bullet and install the jdk....

* Mon May 01 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0RC2-0.5
- Fixed /usr/src/redhat/BUILD path to $RPM_BUILD_DIR for portability
-- and so that RPM's can be built by non-root.
- Minor update to README.rpm

* Tue Apr 18 2000 Lamar Owen <lamar.owen@wgcr.org>
- 0.6
- Fixed patchset: wasn't patching pgaccess or -i in postmaster.opts.default
- minor update to README.rpm

* Mon Apr 17 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0RC1-0.5 (release candidate 1.)
- Fixed SPI header directories' permisssions.
- Removed packaging of Alpha patches until Ryan releases RC1-tested set.

* Mon Apr 10 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0beta5-0.1 (released instead of the release candidate)

* Sat Apr 08 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0beta4-0.2 (pre-release-candidate CVS checkout)
- Alpha patches!
- pg_options.sample

* Fri Mar 24 2000 Lamar Owen <lamar.owen@wgcr.org>
- 7.0beta3-0.1

* Mon Feb 28 2000 Lamar Owen <lamar.owen@wgcr.org>
- Release 0.3
- Fixed stderr redir problem in init script
- Init script now uses pg_ctl to start postmaster
- Packaged inital pg_options for good logging
- built with timestamped logging.

* Tue Feb 22 2000 Lamar Owen <lamar.owen@wgcr.org>
- Initial 7.0beta1 build
- Moved PGDATA to /var/lib/pgsql/data
- First stab at logging and logrotate functionality -- test carefully!
- -tcl subpackage split -- tcl client and pltcl lang separated from
-- the Tk stuff.  PgAccess and the tk client are now in the -tk subpackage.
- No patches for Alpha as yet.
