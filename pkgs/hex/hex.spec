%global momorel 18

Name: hex
Summary: hexadecimal dumping tool for Japanese
Version: 2.04
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/File
Source0: http://www.tsg.ne.jp/GANA/S/%{name}/%{name}204.tgz
Patch0: hex.diff
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gcc-c++ >= 3.4.1-1m

%description
hexdump program that distinguish Japanese 2bytes code character. 

%prep
%setup -q -n %{name}204

%patch0 -p0

%build
xmkmf
make CCLINK=c++

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/ja/man1

make install DESTDIR=%{buildroot}%{_prefix}/
make install.man DESTDIR=%{buildroot}%{_mandir}/ja/

# convert Japanese manual page from EUC-JP to UTF-8
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
        iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}


%files
%defattr(-,root,root)
%doc INSTALL.txt MANUAL.txt
%{_bindir}/hex
%{_mandir}/ja/man1/hex.1*

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-18m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.04-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.04-15m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.04-14m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-12m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-11m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.04-10m)
- rebuild against gcc43

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.04-9m)
- convert ja.man from EUC-JP to UTF-8

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.04-8m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.04-7m)
- use gcc-3.2

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (2.04-6k)
- cancel gcc-3.1 autoconf-2.53

* Tue May 28 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.04-4k)

* Sat Jan 15 2000 AYUHANA Tomonori <info@infoblue.net>
- change install dir /usr/local -> /usr

* Fri Jan  7 2000 AYUHANA Tomonori <info@infoblue.net>
- Fix spec file install.man miss

* Mon Jan  3 2000 AYUHANA Tomonori <info@infoblue.net>
- enable to install in Kondara MNU/Linux
