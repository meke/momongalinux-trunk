%global momorel 12
%global php_ver 5.4.1

%global pkg_name simplate

Summary: simplate module for PHP
Name: php-%{pkg_name}
Version: 0.3.7
Release: %{momorel}m%{?dist}
License: BSD
Group: Development/Languages
URL: http://simplate.aimy.jp/
#Source0: http://pecl.php.net/get/%{pkg_name}-%{version}.tgz
#NoSource: 0
Source0: http://simplate.aimy.jp/archive/%{pkg_name}-%{version}.tar.gz
#NoSource: 0
Patch0: simplate-0.3.7-php-3.5.2.patch
Patch1: %{name}-%{version}-php54.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source10: %{pkg_name}.ini

Requires: php >= %{php_ver}
BuildRequires: php-devel >= %{php_ver}

%description
Simplate is simple template module that has similar function to Smarty-Light

%prep
# Setup main module
%setup -q -n %{pkg_name}-%{version}
%patch0 -p1 -b .php
%patch1 -p1 -b .php54

%{_bindir}/phpize
%configure

%build
# Build main module
%make

%install
rm -rf %{buildroot}
%makeinstall INSTALL_ROOT=%{buildroot}
%{__install} -m 755 -d %{buildroot}%{_sysconfdir}/php.d
%{__install} -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/php.d


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CREDITS ChangeLog simplate.php
%doc template
%{_sysconfdir}/php.d/%{pkg_name}.ini
%{_libdir}/php/modules/%{pkg_name}.so

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.7-12m)
- rebuild against php-5.4.1
- add source tarball

* Sat Jul 16 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.7-11m)
- INI comments char fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.7-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.7-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.7-8m)
- full rebuild for mo7 release

* Sun May 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.7-7m)
- biuld fix php-5.3.2 (patch0)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.7-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.7-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.7-4m)
- rebuild against rpm-4.6

* Mon May 05 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.3.7-3m)
- rebuild against php 5.2.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.7-2m)
- rebuild against gcc43

* Wed Feb 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.7-1m)
- update to 0.3.7

* Mon Sep 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.6-3m)
- rebuild against php-5.2.1

* Sat May 20 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.2.6-2m)
- use %{pkg_name}

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.2.6-2m)
- rebuild against PHP 5.1.4

* Wed Mar 1 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- initial version
