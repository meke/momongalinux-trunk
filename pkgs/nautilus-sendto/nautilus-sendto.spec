# note; gnome-bluetooth-3.4.1 supports 3.0.x only.

%global		momorel 1
Name:           nautilus-sendto
Version:        3.6.0
Release:        %{momorel}m%{?dist}
Summary:        Nautilus context menu for sending files

Group:          User Interface/Desktops
License:        GPLv2+
URL:            ftp://ftp.gnome.org/pub/gnome/sources/%{name}
Source0:        http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires:  gtk3-devel
BuildRequires:  evolution-data-server-devel >= 3.5.90
BuildRequires:  nautilus-devel >= 2.31.3
BuildRequires:  gettext
BuildRequires:  perl-XML-Parser intltool
BuildRequires:  dbus-glib-devel >= 0.70
BuildRequires:  gupnp-devel >= 0.13
BuildRequires:  GConf2-devel >= 3.2.3
BuildRequires:  gsettings-desktop-schemas-devel

Requires(pre): GConf2
Requires(post): GConf2
Requires(preun): GConf2

# For compat with old nautilus-sendto packaging
Provides: nautilus-sendto-gaim
Obsoletes: nautilus-sendto-bluetooth
Provides: nautilus-sendto-bluetooth

%description
The nautilus-sendto package provides a Nautilus context menu for
sending files via other desktop applications.  These functions are
implemented as plugins, so nautilus-sendto can be extended with
additional features.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
License:        LGPLv2+
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains the libraries amd header files that are needed
for writing plugins for nautilus-sendto.

%prep
%setup -q

%build
%configure
%make


%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=$RPM_BUILD_ROOT
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

find $RPM_BUILD_ROOT \( -name '*.a' -o -name '*.la' \) -exec rm -f {} \;

rm -f $RPM_BUILD_ROOT/%{_libdir}/nautilus-sendto/plugins/libnstbluetooth.so
# now shipped with nautilus itself
rm -f $RPM_BUILD_ROOT/%{_libdir}/nautilus/extensions-3.0/libnautilus-sendto.so

%find_lang %{name}

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%pre
%gconf_schema_prepare nst

%preun
%gconf_schema_remove nst

%files -f %{name}.lang
%doc AUTHORS ChangeLog ChangeLog.pre-1.1.4.1 COPYING NEWS
%dir %{_libdir}/nautilus-sendto
%dir %{_libdir}/nautilus-sendto/plugins
%{_libdir}/nautilus-sendto/plugins/*.so
%{_datadir}/nautilus-sendto
%{_bindir}/nautilus-sendto
%{_mandir}/man1/nautilus-sendto.1.*
%{_datadir}/glib-2.0/schemas/org.gnome.Nautilus.Sendto.gschema.xml
%{_datadir}/GConf/gsettings/nautilus-sendto-convert

%files devel
%{_datadir}/gtk-doc
%{_libdir}/pkgconfig/nautilus-sendto.pc
%dir %{_includedir}/nautilus-sendto
%{_includedir}/nautilus-sendto/nautilus-sendto-plugin.h

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Aug 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- rebuild against evolution-data-server-3.5.90

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-2m)
- rebuild for evolution-data-server-devel

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.3-1m)
- update to 3.5.3

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.3-2m)
- fix BuildRequires

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.3-1m)
- import from fedora
