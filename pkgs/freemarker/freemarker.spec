%global momorel 6

# Prevent brp-java-repack-jars from being run.
%define __jar_repack %{nil}

%define checkForbiddenJARFiles F=`find -type f -iname '*.jar'`; [ ! -z "$F" ] && \
echo "ERROR: Sources should not contain JAR files:" && echo "$F" && exit 1

%define fm_compatible_ver 2.3
%define fm_ver %{fm_compatible_ver}.13

Name:           freemarker
Version:        %{fm_ver}
Release:        %{momorel}m%{?dist}
Summary:        A template engine

Group:          Development/Libraries
License:        BSD
URL:            http://freemarker.sourceforge.net/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz

# disabled functionality: ext/jdom, ext/jsp/FreeMarkerPageContext1, ext/xml/JdomNavigator
Patch0:         %{name}-%{version}-build.patch
# 
Patch1:         %{name}-%{version}-PyObject.__class__.patch
# http://netbeans.org/bugzilla/show_bug.cgi?id=156876
Patch2:         %{name}-%{version}-logging.patch
# illegal character in the javadoc comment
Patch3:         %{name}-%{version}-encoding.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires: ant >= 1.6
BuildRequires: ant-nodeps >= 1.6
BuildRequires: avalon-logkit >= 1.2
BuildRequires: dom4j >= 1.6.1
BuildRequires: dos2unix
BuildRequires: emma >= 2.0
BuildRequires: java-devel >= 1.6.0
BuildRequires: javacc >= 4.0
BuildRequires: jaxen >= 1.1
BuildRequires: jdom >= 1.0
BuildRequires: jpackage-utils
BuildRequires: junit >= 3.8.2
BuildRequires: jython >= 2.2.1
BuildRequires: log4j >= 1.2
BuildRequires: rhino >= 1.6
BuildRequires: struts >= 1.2.9
BuildRequires: tomcat5-jsp-2.0-api >= 5.5.26
BuildRequires: tomcat5-servlet-2.4-api >= 5.5
BuildRequires: tomcat6-servlet-2.5-api >= 6.0
BuildRequires: tomcat6-lib >= 6.0.16
BuildRequires: xalan-j2 >= 2.7.0

Requires: java >= 1.6.0
Requires: jpackage-utils

%description
FreeMarker is a Java tool to generate text output based on templates.
It is designed to be practical as a template engine to generate web
pages and particularly for servlet-based page production that follows
the MVC (Model View Controller) pattern. That is, you can separate the
work of Java programmers and website designers - Java programmers
needn't know how to design nice websites, and website designers needn't
know Java programming.

%package javadoc
Summary:        Javadocs for %{name}
Group:          Documentation
Requires:       %{name} = %{version}-%{release}
Requires:       jpackage-utils

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}-%{version}

find -type f \( -iname '*.jar' -o -iname '*.class' \)  -exec rm -f '{}' \;

%patch0 -p1 -b .sav
%patch1 -p1
%patch2 -p1
%patch3 -p1

# %{__rm} -rf src/freemarker/core/ParseException.java
%{__rm} -rf src/freemarker/core/FMParser.java
%{__rm} -rf src/freemarker/core/FMParserConstants.java
%{__rm} -rf src/freemarker/core/FMParserTokenManager.java
%{__rm} -rf src/freemarker/core/SimpleCharStream.java
%{__rm} -rf src/freemarker/core/Token.java
%{__rm} -rf src/freemarker/core/TokenMgrError.java

%{__ln_s} -f %{_javadir}/ant.jar           lib/ant.jar
%{__ln_s} -f %{_javadir}/dom4j.jar         lib/dom4j.jar
%{__ln_s} -f %{_javadir}/emma_ant.jar      lib/emma_ant.jar
%{__ln_s} -f %{_javadir}/emma.jar          lib/emma.jar
%{__ln_s} -f %{_javadir}/javacc.jar        lib/javacc.jar
%{__ln_s} -f %{_javadir}/jaxen.jar         lib/jaxen.jar
%{__ln_s} -f %{_javadir}/jdom.jar          lib/jdom.jar
# js.jsr provided by rhino package
%{__ln_s} -f %{_javadir}/js.jar            lib/js.jar

# The JavaServer Pages 1.2 technology isn't provided in Fedora 10
#%{__ln_s} -f %{_javadir}/jsp-api-1.2.jar   lib/jsp-api-1.2.jar

%{__ln_s} -f %{_javadir}/tomcat5-jsp-2.0-api.jar  lib/jsp-api-2.0.jar

%{__ln_s} -f %{_javadir}/tomcat6-jsp-2.1-api.jar  lib/jsp-api-2.1.jar

%{__ln_s} -f %{_javadir}/junit.jar         lib/junit.jar
%{__ln_s} -f %{_javadir}/jython.jar        lib/jython.jar
%{__ln_s} -f %{_javadir}/log4j.jar         lib/log4j.jar
%{__ln_s} -f %{_javadir}/avalon-logkit.jar lib/logkit.jar

# It doesn't required due to OpenJDK 6 is used
#%{__ln_s} -f %{_javadir}/rt122.jar         lib/rt122.jar

# SAXPath has been merged into the Jaxen codebase and is 
# no longer being maintained separately. See jaxen-1.1.jar
#%{__ln_s} -f %{_javadir}/saxpath.jar       lib/saxpath.jar

# The package javax.el isn't included in:
%{__ln_s} -f %{_javadir}/tomcat6-servlet-2.5-api.jar lib/servlet.jar
# so, el-api.jar is additionally used.
%{__ln_s} -f %{_javadir}/tomcat6/el-api.jar lib/el-api.jar

%{__ln_s} -f %{_javadir}/struts.jar        lib/struts.jar
%{__ln_s} -f %{_javadir}/xalan-j2.jar      lib/xalan.jar

dos2unix -k docs/docs/api/stylesheet.css
dos2unix -k docs/docs/api/package-list

%checkForbiddenJARFiles

%build
%{ant}

%install
%{__rm} -rf %{buildroot}

# jars
%{__install} -d -m 755 %{buildroot}%{_javadir}
%{__install} -m 644 lib/%{name}.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
%{__ln_s} %{name}-%{version}.jar %{buildroot}%{_javadir}/%{name}.jar
# The freemarker 2.2 isn't compatible with 2.3.
%{__ln_s} %{name}-%{version}.jar %{buildroot}%{_javadir}/%{name}-%{fm_compatible_ver}.jar

# javadoc
%{__install} -d -m 755 %{buildroot}%{_javadocdir}/%{name}
%{__cp} -pr docs/docs/api/* %{buildroot}%{_javadocdir}/%{name}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_javadir}/*.jar
%doc LICENSE.txt README.txt

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.13-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.13-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.13-4m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.13-3m)
- import Patch0,2,3 from Fedora 13 (2.3.13-8)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.13-1m)
- import from Fedora 11 for netbeans

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.13-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Sep 01 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 2.3.13-4
- Redundant dependency upon xerces-j2 is removed (#456276#c6)
- The dos2unix package is added as the build requirements
- The ant-nodeps build-time requirement is added

* Wed Aug 20 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 2.3.13-3
- The downloads.sourceforge.net host is used in the source URL
- %%{__install} and %%{__cp} are used everywhere
- %%defattr(-,root,root,-) is used everywhere

* Fri Aug 14 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 2.3.13-2
- Appropriate values of Group Tags are chosen from the official list
- Versions of java-devel & jpackage-utils are corrected
- Name of dir for javadoc is changed
- Manual is removed due to http://freemarker.org/docs/index.html

* Fri Jun 06 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 2.3.13-1
- Initial version
