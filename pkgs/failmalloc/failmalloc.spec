%global momorel 11

Summary: failmalloc
Name: failmalloc
Version: 1.0
Release: %{momorel}m%{?dist}
Source0: http://download.savannah.nongnu.org/releases/failmalloc/%{name}-%{version}.tar.gz
Patch0: %{name}-%{version}-glibc214.patch
License: GPL
Group: System Environment/Libraries
URL: http://www.nongnu.org/failmalloc/
BuildRequires: glibc-devel >= 2.14
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Failmalloc force memory allocation fails

%prep
%setup -q
%patch0 -p1 -b .glibc214

%build
%configure
%make 

%install
rm -rf   %{buildroot}
mkdir -p %{buildroot}%{_libdir}/
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_libdir}/lib*

%changelog
* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-11m)
- update glibc214 patch

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-10m)
- enable to build with glibc-2.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc43

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- revised spec for debuginfo

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-2m)
- delete libtool library

* Sat Jul 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1m)
- initial
