%global momorel 1

Summary: The NIS (Network Information Service) server
Url: http://www.linux-nis.org/nis/ypserv/index.html
Name: ypserv
Version: 2.32
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Daemons
Source0: http://www.linux-nis.org/download/ypserv/ypserv-%{version}.tar.bz2
NoSource: 0
Source1: ypserv.service
Source2: yppasswdd.service
Source3: ypxfrd.service
Source4: rpc.yppasswdd.env
Source5: yppasswdd-pre-setdomain

Requires: gawk, make, portmap, bash >= 2.0
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(post): systemd-sysv

Patch0: ypserv-2.5-redhat.patch
Patch1: ypserv-2.21-path.patch
Patch2: ypserv-2.5-nfsnobody2.patch
Patch3: ypserv-2.13-ypxfr-zeroresp.patch
Patch4: ypserv-2.13-nonedomain.patch
Patch5: ypserv-2.19-slp-warning.patch
Patch6: ypserv-2.24-manfix.patch
Patch7: ypserv-2.24-aliases.patch
Patch8: ypserv-2.27-confpost.patch
Patch9: ypserv-2.29-relro.patch
Patch10: ypserv-2.31-netgrprecur.patch

BuildRequires: tokyocabinet
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Network Information Service (NIS) is a system that provides
network information (login names, passwords, home directories, group
information) to all of the machines on a network. NIS can allow users
to log in on any machine on the network, as long as the machine has
the NIS client programs running and the user's password is recorded in
the NIS passwd database. NIS was formerly known as Sun Yellow Pages
(YP).

This package provides the NIS server, which will need to be running on
your network. NIS clients do not need to be running the server.

Install ypserv if you need an NIS server for your network. You also
need to install the yp-tools and ypbind packages on any NIS client
machines.

%prep
%setup -q
%patch0 -p1 -b .redhat
%patch1 -p1 -b .path
%patch2 -p1 -b .nfsnobody
%patch3 -p1 -b .respzero
%patch4 -p1 -b .nonedomain
%patch5 -p1 -b .slp-warning
%patch6 -p1 -b .manfix
%patch7 -p1 -b .aliases
%patch8 -p1 -b .confpost
%patch9 -p1 -b .relro
%patch10 -p1 -b .netgrprecur

autoreconf

%build
cp etc/README etc/README.etc
%ifarch s390 s390x
export CFLAGS="$RPM_OPT_FLAGS -fPIC"
%else
export CFLAGS="$RPM_OPT_FLAGS -fpic"
%endif
%configure \
	--enable-checkroot \
	--enable-fqdn \
	--libexecdir=%{_libdir}/yp \
	--with-dbmliborder=tokyocabinet
make

%install
#make install ROOT=$RPM_BUILD_ROOT
%makeinstall libexecdir=$RPM_BUILD_ROOT%{_libdir}/yp INSTALL_PROGRAM=install
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}
mkdir -p $RPM_BUILD_ROOT%{_libexecdir}
install -m 644 etc/ypserv.conf $RPM_BUILD_ROOT%{_sysconfdir}
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}/ypserv.service
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}/yppasswdd.service
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}/ypxfrd.service
install -m 755 %{SOURCE5} $RPM_BUILD_ROOT%{_libexecdir}/yppasswdd-pre-setdomain

mkdir -p $RPM_BUILD_ROOT/etc/sysconfig
cat >$RPM_BUILD_ROOT/etc/sysconfig/yppasswdd <<EOF
# The passwd and shadow files are located under the specified
# directory path. rpc.yppasswdd will use these files, not /etc/passwd
# and /etc/shadow.
#ETCDIR=/etc

# This option tells rpc.yppasswdd to use a different source file
# instead of /etc/passwd
# You can't mix usage of this with ETCDIR
#PASSWDFILE=/etc/passwd

# This option tells rpc.yppasswdd to use a different source file
# instead of /etc/passwd.
# You can't mix usage of this with ETCDIR
#SHADOWFILE=/etc/shadow

# Additional arguments passed to yppasswd
YPPASSWDD_ARGS=
EOF

# We need to pass all environment variables set in /etc/sysconfig/yppasswdd 
# only if they are not empty. However, this simple logic is not supported 
# by systemd. The script rpc.yppasswdd.env wraps the main binary and 
# prepares YPPASSWDD_ARGS variable to include all necessary variables 
# (ETCDIR, PASSWDFILE and SHADOWFILE). The script ensures, that the 
# rpc.yppasswdd arguments are not used when the appropriate environment 
# variables are empty.
install -m 755 %{SOURCE4} $RPM_BUILD_ROOT%{_libexecdir}/rpc.yppasswdd.env

%post
%systemd_post ypserv.service
%systemd_post ypxfrd.service
%systemd_post yppasswdd.service

%preun
%systemd_preun ypserv.service
%systemd_preun ypxfrd.service
%systemd_preun yppasswdd.service

%postun
%systemd_postun_with_restart ypserv.service
%systemd_postun_with_restart ypxfrd.service
%systemd_postun_with_restart yppasswdd.service

# After switching from gdbm to Tokyo Cabinet we need to rebuild maps
# during update, but without pushing to slave servers
# In case domainname is not set, but it is defined in 
# /etc/sysconfig/network, we do the same work as service yppasswdd 
# do before starting.
# The original domainname value is set back in the end.
# The whole work is created before installing new ypserv, so we use old
# utilities and commands are stored into temporary file (that is necessary,
# because we cannot read old maps using new package)
# If old package used gdbm, the prepared script is executed after new package
# is installed.
%global rebuild_maps_script /var/yp/rpm_rebuild_maps
%pre
if [ $1 == 2 ] ; then
    # stop ypserv if running and then start it again
    ypservactive=0
    if /usr/bin/systemctl is-active ypserv.service>/dev/null 2>&1 ; then
        ypservactive=1
        /usr/bin/systemctl stop ypserv.service>/dev/null 2>&1
    fi
    # store old domainname and set the correct one
    olddomain=`domainname`
    . /etc/sysconfig/network
    DOMAINNAME=`domainname`
    if [ "$olddomain" = "(none)" -o "x$olddomain" = "x" ]; then
        if [ -n "$NISDOMAIN" ]; then
            domainname $NISDOMAIN
        fi
    fi

    newdomain=`domainname`
    if [ "$newdomain" != "(none)" -a "x$newdomain" != "x" ]; then
        pushd "/var/yp/$newdomain">/dev/null
        echo "" > %rebuild_maps_script
        chmod 0600 %rebuild_maps_script
        # loop through maps
        for map in * ; do
            # this server is a master for this map
            if %{_libdir}/yp/yphelper -i "$map" >/dev/null 2>&1 ; then
                echo "rm -f `pwd`/$map" >> %rebuild_maps_script
            # this server is a slave for this map
            else
                master=`%{_libdir}/yp/makedbm -u $map 2>/dev/null | grep  YP_MASTER_NAME | sed -e 's/YP_MASTER_NAME//'`
                if [ "x$master" != "x" ] ; then
                    echo "%{_libdir}/yp/ypxfr -f -h $master -c -d $newdomain $map" >> %rebuild_maps_script
                fi
            fi
        done
        echo "make NOPUSH=true -C /var/yp" >> %rebuild_maps_script
    fi
    /bin/domainname "$olddomain"
    # if ypserv was running before, start it again
    if [ $ypservactive -eq 1 ] ; then
        /usr/bin/systemctl start ypserv.service>/dev/null 2>&1
    fi
fi

%triggerpostun -- ypserv < %{first_tc_version}
if [ -e %rebuild_maps_script ] ; then
    bash %rebuild_maps_script >/dev/null 2>&1 || :
    rm -f %rebuild_maps_script >/dev/null 2>&1
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc AUTHORS README INSTALL ChangeLog TODO NEWS COPYING
%doc etc/ypserv.conf etc/securenets etc/README.etc
%doc etc/netgroup etc/locale etc/netmasks etc/timezone
%config(noreplace) %{_sysconfdir}/ypserv.conf
%config(noreplace) %{_sysconfdir}/sysconfig/yppasswdd
%config(noreplace) /var/yp/*
%{_unitdir}/*
%{_libexecdir}/*
%{_libdir}/yp
%{_sbindir}/*
%{_mandir}/*/*
%{_includedir}/*/*

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.32-1m)
- update 2.32

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26-1m)
- update 2.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.23-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.23-1m)
- sync with Rawhide (2.23-4)

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.19-6m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19-4m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.19-3m)
- sync with Rawhide (2.19-10)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.19-2m)
- rebuild against gcc43

* Fri May 19 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.19-1m)
- update to 2.19

* Wed Nov  9 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.18-1m)
- up to 2.18

* Sun May 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13-1m)
- update to 2.13
- comment out Patch3 nor Source3
- update Patch4: ypserv-path.patch

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.4-6m)
- accept gdbm-1.8.0 or newer

* Mon Nov 24 2003 zunda <zunda at freeshell.org>
- (2.4-5m)
- ypxfrd.x is under BSD.
- README.secure is not found in the source distribution.

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.4m)
- rebuild against gdbm-1.8.0

* Sun Oct 26 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (2.8-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- fix %%patch1 and %%patch7 in comments
- use %%{SOURCE1}, %%{SOURCE2}, and %%{SOURCE3}
- in place of making /etc/sysconfig/yppasswdd script in spec file,
  make yppasswdd.sysconfig as %%{SOURCE4}
- use %%{momorel}

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.8-2m)
- rebuild against for gdbm

* Sun Feb  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.8-1m)
  update 2.8

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.7-1m)
  update 2.7

* Tue Dec 03 2002 TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
- (2.6-1m)

* Tue Jun 11 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.4-2k)
  update to 2.4

* Thu May 23 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.3-2k)
  update to 2.3

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (2.2-2k)
- ver up.

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.3.12-8k)
- Prereq: /sbin/chkconfig -> chkconfig
- Prereq: /sbin/service   -> initscripts

* Tue Jan  8 2002 Masahiro Takahata <takahata@kondara.org>
- (1.3.12-6k)
- modified ypserv-ypxfrd.init script

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.3.12-4k)
- nigirisugi /var/yp

* Wed Oct  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.3.12-2k)
- update to 1.3.12 and merge rh 1.3.12-2

* Tue Aug  7 2001 Akira Higuchi <a@kondara.org>
- added missing /etc/netgroup file

* Fri Apr 13 2001 Shingo Akagaki <dora@kondara.org>
- /etc/rc.d/init.d -> /etc/init.d

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.3.9-11k)
- rebuild againt rpm-3.0.5-39k

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.3.9-9k)
- modified ypserv.fhs.patch for compatibility

* Tue Nov 28 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to %{_initscriptdir}.

* Mon Aug 21 2000 MATSUDA, Daiki <dyky@df-usa.com>
- fix for FHS

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.3.9-3).

* Mon Mar 06 2000 Cristian Gafton <gafton@redhat.com>
- add patch to avoid potential deadlock on the server (fix #9968)

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Wed Feb  2 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- fix typo in %triggerpostun

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-ypserv-20000115

* Tue Nov 30 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-ypserv-19991115

* Mon Oct 25 1999 Bill Nottingham <notting@redhat.com>
- update to 1.3.9
- use gdbm, move back to /usr/sbin

* Tue Aug 17 1999 Bill Nottingham <notting@redhat.com>
- initscript munging
- ypserv goes on root partition

* Fri Aug 13 1999 Cristian Gafton <gafton@redhat.com>
- version 1.3.7

* Thu Jul  1 1999 Bill Nottingham <notting@redhat.com>
- start after network FS

* Tue Jun  1 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.3.6.94.

* Sun May 30 1999 Jeff Johnson <jbj@redhat.com>
- improved daemonization.

* Sat May 29 1999 Jeff Johnson <jbj@redhat.com>
- fix buffer overflow in rpc.yppasswd (#3126).

* Fri May 28 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.3.6.92.

* Fri Mar 26 1999 Cristian Gafton <gafton@redhat.com>
- version 1.3.6.91

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Mon Feb  8 1999 Bill Nottingham <notting@redhat.com>
- move to start before ypbind

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1
- upgraded to 1.3.5

* Tue Aug  4 1998 Jeff Johnson <jbj@redhat.com>
- yppasswd.init: lock file must have same name as init.d script, not daemon

* Sat Jul 11 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.3.4
- fixed the fubared Makefile
- link against gdbm instead of ndbm (it seems to work better)

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.3.1
- enhanced init scripts

* Fri May 01 1998 Jeff Johnson <jbj@redhat.com>
- added triggerpostun
- Use libdb fro dbp_*().

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Apr 13 1998 Cristian Gafton <gafton@redhat.com>
- updated to 1.3.0

* Wed Dec 03 1997 Cristian Gafton <gafton@redhat.com>
- updated to 1.2.5
- added buildroot; updated spec file
- added yppasswdd init file

* Tue Nov 04 1997 Erik Troan <ewt@redhat.com>
- init script shouldn't set the domain name

* Tue Oct 14 1997 Erik Troan <ewt@redhat.com>
- supports chkconfig
- updated initscript for status and restart
- turned off in all runlevels, by default
- removed postinstall script which didn't do anything

* Thu Oct 09 1997 Erik Troan <ewt@redhat.com>
- added patch to build against later glibc

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Wed Apr 23 1997 Erik Troan <ewt@redhat.com>
- updated to 1.1.7.

* Fri Mar 14 1997 Erik Troan <ewt@redhat.com>
- Updated to ypserv 1.1.5, ported to Alpha (glibc).

* Fri Mar 07 1997 Erik Troan <ewt@redhat.com>
- Removed -pedantic which confuses the SPARC :-(
