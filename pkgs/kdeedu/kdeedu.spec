%global momorel 1
%global kdever 4.13.1

#global python_marble 1
#global touch 1

Summary: K Desktop Environment - Education
Name: kdeedu
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Amusements/Graphics
URL: http://edu.kde.org/
# basic applications and required libraries
Requires: libkdeedu = %{version}
Requires: artikulate = %{version}
Requires: blinken = %{version}
Requires: kalzium = %{version}
Requires: kalzium-libs = %{version}
Requires: kanagram = %{version}
Requires: kanagram-libs = %{version}
Requires: kgeography = %{version}
Requires: khangman = %{version}
Requires: khangman-libs = %{version}
Requires: kiten = %{version}
Requires: kiten-libs = %{version}
Requires: klettres = %{version}
Requires: kqtquickcharts = %{version}
Requires: kstars = %{version}
Requires: ktouch = %{version}
Requires: kturtle = %{version}
Requires: kwordquiz = %{version}
Requires: marble = %{version}
Requires: marble-common = %{version}
Requires: marble-mobile = %{version}
Requires: marble-qt = %{version}
%if 0%{?touch}
Requires: marble-touch = %{version}
%endif
Requires: marble-libs = %{version}
%if 0%{?python_marble}
Requires: python-marble = %{version}
%endif
Requires: parley = %{version}
Requires: step = %{version}

# Obsoletes packeges
Obsoletes: %{name}-libs < 4.7.1-2m
Obsoletes: %{name}-kstars < 4.7.1-2m
Obsoletes: %{name}-kstars-libs < 4.7.1-2m
Obsoletes: %{name}-marble < 4.7.1-2m
Obsoletes: %{name}-marble-libs < 4.7.1-2m

%description
kdeedu metapackage.

%package devel
Group:    Development/Libraries
Summary:  Developer files for %{name}
Requires: libkdeedu-devel = %{version}
Requires: libkdeedu-static = %{version}
Requires: analitza-devel = %{version}
Requires: cantor-devel = %{version}
Requires: kalzium-devel = %{version}
Requires: kanagram-devel = %{version}
Requires: khangman-devel = %{version}
Requires: kiten-devel = %{version}
Requires: marble-devel = %{version}
Requires: rocs-devel = %{version}

%description devel
kdeedu-devel metapackage

%package math
Group:	 Amusements/Games
Summary: Math applications
Requires: analitza = %{version}
Requires: cantor = %{version}
Requires: cantor-R = %{version}
Requires: cantor-libs = %{version}
Requires: kalgebra = %{version}
Requires: kbruch = %{version}
Requires: kig = %{version}
Requires: kmplot = %{version}
Requires: rocs = %{version}
Requires: rocs-libs = %{version}

# Obsoletes packeges
Obsoletes: %{name}-math-cantor-R < 4.7.1-2m
Obsoletes: %{name}-math-libs < 4.7.1-2m

%description math
kdeedu-math metapackage

%prep

%build

%install

%clean

%files

%files devel

%files math

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-2m)
- kdeedu metapackage

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-3m)
- rebuild against gpsd-3.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (4.7.0-2m)
- rebuild for boost

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-2m)
- rebuild against boost-1.46.0

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.2-3m)
- rebuild against boost-1.44.0

* Tue Oct 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-2m)
- import patch from Fedora
-- kalzium.desktop: drop MimeTypes=chemical/x-cml (kde235563)

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Wed Sep 29 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.1-2m)
- rebuild against eigen2-devel-2.0.15

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-2m)
- remove Game from Categories of khangman.desktop
- add icon and Japanese translation to rocs.desktop

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-2m)
- change BuildRequires: from libattica-devel to attica-devel

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-3m)
- rebuild against qt-4.6.3-1m

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-2m)
- rebuild against boost-1.43.0

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against readline6

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-2m)
- rebuild against ocaml-3.11.2

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-2m)
- rebuild against libqalculate-0.9.7

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-2m)
- build with R

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sun Nov 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.3-3m)
- rebuild agaisnt boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Tue Oct 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-2m)
- rebuild agaisnt boost-1.40.0

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-2m)
- rebuild against cln-1.3.0

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-3m)
- set BR: eigen2-devel >= 2.0.52-0.20090729.2m for automatic building of STABLE_6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-2m)
- remove conflicting icon(s) with oxygen-icons

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.3-3m)
- rebuild against gpsd-2.39

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.3-2m)
- rebuilt for boost-1.39.0

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sat Apr 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- Requires: kdeedu-marble on stable5
- Requires: kdeedu-math on stable5
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- update all patches

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0

* Tue Jan 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.87-2m)
- rebuild against boost-1.37.0

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.85-2m)
- rebuild against python-2.6.1-1m

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.81-3m)
- rebuild against ocaml-3.11.0

* Mon Dec  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-2m)
- add some BuildRequires' for Kalzium

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80
- separate kdeedu-math sub package (sync with Fedora devel)

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Nov  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.71-2m)
- fix up marble python modules to enable build without kdeedu-devel-4.1.7x

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.63-1m)
- update to 4.1.63

* Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.62-1m)
- update to 4.1.62

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.61-2m)
- add BuildRequires: libapogee-devel to support for controlling FLI CCDS & Filter Wheels (KStars)
- add BuildRequires: libfli-devel to support for controlling Apogee CCDS (KStars)

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.61-1m)
- update to 4.1.61

* Tue Aug  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-2m)
- enable to build -O2 option on x86_64

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-2m)
- rebuild against gpsd-2.37

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-4m)
- build with libqalculate again

* Tue Jul  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.85-3m)
- build without libqalculate for the moment

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.85-2m)
- build with libqalculate

* Sun Jul  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85
- update marble to svn revision 828379 (to enable build)

* Sun Jul  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-3m)
- build with gmm++
- add BuildRequires: gmm-devel >= 3.0

* Sat Jul  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.84-2m)
- build with new openbabel
- sort %%files

* Sun Jun 29 2008 NARTIA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.80-2m)
- set -DWITH_SBIG:BOOL=OFF again
- force compile internal libraries to resolve dependency
- revise %%postun kstars

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-2m)
- update Patch0 (import from Fedora 9)

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-5m)
- rebuild against gcc43

* Fri Mar 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-4m)
- set -DWITH_SBIG:BOOL=OFF
- force compile internal libraries (-DSBIG_LIBRARIES-ADVANCED:INTERNAL=1)


* Wed Mar 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-3m)
- build with cfitsio and libnova
- import cfitsio.patch from Fedora
 +* Fri Mar 07 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.2-3
 +- fix cfitsio detection by adding another hardcoded directory
- remove COPYING-CMAKE-SCRIPTS from %%doc

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-2m)
- release %%{_kde4_libdir}/kde4/plugins from main-package
- it's already provided by kdelibs
- and main-package Requires: kdelibs

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2
- delete unused patch

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-2m)
- build with eigen

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Wed Jan 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-4m)
- enable parallel build

* Wed Jan 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-3m)
- add COPYING-CMAKE-SCRIPTS to %%doc

* Wed Jan 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-2m)
- disable openbabel-devel (disable %%{_libdir}/pkgconfig/openbabel-2.0.pc)
  to avoid BuildConflicts: openbabel-devel
- Obsoletes and Provides kalgebra
- Obsoletes and Provides marble
- modify %%files
- modify %%description devel

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to KDE4

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- delete unused upstream patches

* Mon Mar  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-3m)
- import upstream patches to fix following problems
  #118862, accents dead keys do not work
  #142329, The capital of Myanmar is not Rangoon

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against kdelibs etc.

* Sun Jan 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete unused upstream patches but kdeedu-3.5.5-python-2.5-support.patch is still needed

* Fri Jan 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-6m)
- import upstream patch to support python-2.5

* Sat Jan  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-5m)
- import upstream patch to fix following problem
  #139616, Division names in 'Division by flag' result view not translated

* Fri Jan  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- import upstream patch to fix following problem
  #139554, Capital of South Africa is not Cape Town but Pretoria

* Sun Dec  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-3m)
- add Requires: facile

* Sun Nov 12 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- rebuild against facile-1.1-1m
- add BuildPreReq: facile

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-2m)
- rebuild against kdelibs-3.5.4-3m kdebase-3.5.4-11m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Fri Mar 10 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.5.1-2m)
- fix for ppc, ppc64

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Mon Nov 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- revise kwordquiz.desktop for GNOME
- BuildPreReq: desktop-file-utils
- disable hidden visibility

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- import kdeedu-3.4.91-s390.patch from Fedora Core devel

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3
- remove kdeedu-3.4.2-uic.patch
- remove post-3.4.2-kdeedu.diff

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-4m)
- import kdeedu-3.4.2-uic.patch from Fedora Core devel
 +* Wed Sep 21 2005 Than Ngo <than@redhat.com> 3.4.2-3
 +- fix uic build problem

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-3m)
- add --disable-rpath to configure

* Tue Aug 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- [SECURITY] post-3.4.2-kdeedu.diff for "langen2kvtml tempfile vulnerability"
  http://www.kde.org/info/security/advisory-20050815-1.txt

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- remove nokdelibsuff.patch
- import gcc4.patch from Fedora Core
- add %%doc

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- modify %%files section

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.
- --disable-kig-python-scripting due to version mismatch.

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- BuildPreReq: kdebase
- remove post-3.3.2-kdeedu-kstars.diff

* Thu Mar 10 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.2-4m)
- build against boost 1.32.0.

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-3m)
- [SECURITY] post-3.3.2-kdeedu-kstars.diff for "Buffer overflow in fliccd of kdeedu/kstars/indi"
  http://kde.org/info/security/advisory-20050215-1.txt

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Fri Oct 22 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.3.1-2m)
- add patch0 for boost-python-1.31

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- modified %%files

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Sat Dec 27 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-3m)
- rebuild against for qt-3.2.3

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-2m)
- rebuild against for qt-3.2.3

* Wed Aug 24 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Thu Aug 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Tue Jul  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-2m)
- add -fno-stack-protector if gcc is 3.3

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- no changes since 3.1.1

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-3m)
- remove requires: qt-Xt

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    KStars:
      Fixed bug #51708: No longer exits if starting position is below horizon (only affected some systems)
      Fixed bug #52205: Country of Lhasa is China, not Tibet.
      Fixed too-narrow coordinates field in statusbar.
      Fixed bug in "length of day" calculator module; it now properly accounts for latitude and longitude

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-2m)
- add URL

* Sun Feb  2 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Wed Dec 11 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.

* Sun Jun  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up.

* Fri Apr  5 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.
