%global momorel 1

%define gssdp_ver 0.12.0
%define gupnp_ver 0.18.4

Name: gupnp-vala
Version: 0.10.5
Release: %{momorel}m%{?dist}
Summary: Vala bindings to GUPnP framework.
Group: Development/Languages
License: LGPLv2+
URL: http://www.gupnp.org/
Source0: http://ftp.gnome.org/pub/gnome/sources/%{name}/0.10/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: vala-devel >= 0.17.5
BuildRequires: gssdp-devel >= %{gssdp_ver}
BuildRequires: gupnp-devel >= %{gupnp_ver}
Requires: vala

%description
GUPnP is an object-oriented open source framework for creating UPnP
devices and control points, written in C using GObject and
libsoup. The GUPnP API is intended to be easy to use, efficient and
flexible.

gupnp-vala offers Vala bindings to GUPnP framework.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_datadir}/vala/vapi/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/gupnp-vala-1.0.pc

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.5-1m)
- update to 0.10.5

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.12-2m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.12-1m)
- update to 0.6.12

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.6-2m)
- add vala-devel BR version

* Fri Apr 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.4-1m)
- initial build
-- need gupnp-ui and gupnp-av
