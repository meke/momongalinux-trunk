%global momorel 1
%global with_python3 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-pygments
Version:        1.6
Release:        %{momorel}m%{?dist}
Summary:        A syntax highlighting engine written in Python

Group:          Development/Libraries
License:        BSD
URL:            http://pygments.org/
Source0:        http://pypi.python.org/packages/source/P/Pygments/Pygments-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7, python-setuptools >= 0.6.15
Requires:       python-setuptools

%if 0%{?with_python3}
BuildRequires:  python3-devel, python3-setuptools
BuildRequires: python3-nose
%endif # if with_python3

%description
Pygments is a generic syntax highlighter for general use in all kinds
of software such as forum systems, wikis or other applications that
need to prettify source code. Highlights are:

  * a wide range of common languages and markup formats is supported
  * special attention is paid to details that increase highlighting
    quality
  * support for new languages and formats are added easily; most
    languages use a simple regex-based lexing mechanism
  * a number of output formats is available, among them HTML, RTF,
    LaTeX and ANSI sequences
  * it is usable as a command-line tool and as a library
  * ... and it highlights even Brainf*ck!

%if 0%{?with_python3}
%package -n python3-pygments
Summary:        Syntax highlighting engine written in Python 3
Group:          Development/Libraries
Requires:       python3-setuptools

%description -n python3-pygments
Pygments is a generic syntax highlighter for general use in all kinds
of software such as forum systems, wikis or other applications that
need to prettify source code. Highlights are:

  * a wide range of common languages and markup formats is supported
  * special attention is paid to details that increase highlighting
    quality
  * support for new languages and formats are added easily; most
    languages use a simple regex-based lexing mechanism
  * a number of output formats is available, among them HTML, RTF,
    LaTeX and ANSI sequences
  * it is usable as a command-line tool and as a library
  * ... and it highlights even Brainf*ck!
%endif # if with_python3

%prep
%setup -q -n Pygments-%{version}

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # with_python3

%build
%{__python} setup.py build
%{__sed} -i 's/\r//' LICENSE

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif # with_python3

%install
rm -rf $RPM_BUILD_ROOT

# Run the Python 3 build first so that the Python 2 version of
# /usr/bin/pygmentize "wins":
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
popd
%endif # with_python3

# Run the Python 2.6 build first so that the Python 2 version of
# /usr/bin/pygmentize "wins":
%if 0%{?with_python26}
pushd %{py26dir}
%{__python26} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
popd
%endif # with_python26

%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
pushd docs
install -d %{buildroot}%{_mandir}/man1
mv pygmentize.1 $RPM_BUILD_ROOT%{_mandir}/man1/pygmentize.1
mv build html
mv src reST
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS CHANGES docs/html docs/reST LICENSE TODO
# For noarch packages: sitelib
%{python_sitelib}/*
%{_bindir}/pygmentize
%lang(en) %{_mandir}/man1/pygmentize.1.*

%if 0%{?with_python3}
%files -n python3-pygments
%defattr(-,root,root,-)
%doc AUTHORS CHANGES docs/html docs/reST LICENSE TODO
%{python3_sitelib}/*
%endif # with_python3

%changelog
* Sat Jan 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5-1m)
- update to 1.5

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-1m)
- update to 1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-3m)
- rebuild against python-2.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.9-1m)
- initial import

* Thu Nov 29 2007 Steve 'Ashcrow' Milner <me@stevemilner.org> - 0.9-2
- Added python-setuptools as a Requires per bz#403601.

* Mon Nov 12 2007 Steve 'Ashcrow' Milner <me@stevemilner.org> - 0.9-1
- Updated for upstream 0.9.

* Thu Aug 17 2007 Steve 'Ashcrow' Milner <me@stevemilner.org> - 0.8.1-2
- Removed the dos2unix build dependency.

* Thu Jun 28 2007 Steve 'Ashcrow' Milner <me@stevemilner.org> - 0.8.1-1
- Initial packaging for Fedora.
