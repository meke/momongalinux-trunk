%global         momorel 1

Name:           perl-SQL-Abstract
Version:        1.78
Release:        %{momorel}m%{?dist}
Summary:        Generate SQL from Perl data structures
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/SQL-Abstract/
Source0:        http://www.cpan.org/authors/id/R/RI/RIBASUSHI/SQL-Abstract-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.2
BuildRequires:  perl-Class-Accessor-Grouped >= 0.09005
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Getopt-Long-Descriptive >= 0.086
BuildRequires:  perl-Hash-Merge >= 0.12
BuildRequires:  perl-List-Util
BuildRequires:  perl-Storable
BuildRequires:  perl-Test-Deep >= 0.106
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple >= 0.92
BuildRequires:  perl-Test-Warn
Requires:       perl-Class-Accessor-Grouped >= 0.09005
Requires:       perl-Getopt-Long-Descriptive >= 0.086
Requires:       perl-Hash-Merge >= 0.12
Requires:       perl-List-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module was inspired by the excellent DBIx::Abstract. However, in using
that module I found that what I really wanted to do was generate SQL, but
still retain complete control over my statement handles and use the DBI
interface. So, I set out to create an abstract SQL generation module.

%prep
%setup -q -n SQL-Abstract-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/DBIx/Class/Storage/Debug/PrettyPrint.pm
%{perl_vendorlib}/SQL/Abstract
%{perl_vendorlib}/SQL/Abstract.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-1m)
- rebuild against perl-5.20.0
- update to 1.78

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-1m)
- update to 1.77
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.75-1m)
- update to 1.75

* Sun Sep 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.74-1m)
- update to 1.74

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.73-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.73-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.73-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.73-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.73-2m)
- rebuild against perl-5.16.1

* Wed Jul 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.73-1m)
- update to 1.73

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.72-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.71-2m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.71-1m)
- update to 1.71

* Tue Nov  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-1m)
- update to 1.70

* Sat Oct 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.69-1m)
- update to 1.69

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.68-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.68-1m)
- update to 1.68

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.67-2m)
- full rebuild for mo7 release

* Tue Jun  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.67-1m)
- update to 1.67

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.66-2m)
- rebuild against perl-5.12.1

* Tue Apr 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.66-1m)
- update to 1.66

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.65-2m)
- rebuild against perl-5.12.0

* Mon Apr 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.65-1m)
- update to 1.65

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.63-1m)
- update to 1.63

* Tue Mar 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.62-1m)
- update to 1.62
- Specfile re-generated by cpanspec 1.78.

* Sat Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.61-1m)
- update to 1.61

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.60-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.60-1m)
- update to 1.60

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.58-1m)
- update to 1.58

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.56-2m)
- rebuild against perl-5.10.1

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.56-1m)
- update to 1.56

* Sun May 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.55-1m)
- update to 1.55

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.54-1m)
- update to 1.54

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.53-1m)
- update to 1.53

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Fri Mar 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-1m)
- update to 1.50

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-2m)
- rebuild against rpm-4.6

* Thu Jul 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.22-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.22-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.22-2m)
- use vendor

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.21-1m)
- spec file was autogenerated
