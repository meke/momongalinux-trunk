# Generated from ParseTree-3.0.8.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname ParseTree

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: ParseTree is a C extension (using RubyInline) that extracts the parse tree for an entire class or a specific method and returns it as a s-expression (aka sexp) using ruby's arrays, strings, symbols, and integers
Name: rubygem-%{gemname}
Version: 3.0.8
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: https://github.com/seattlerb/parsetree
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(RubyInline) >= 3.7.0
Requires: rubygem(sexp_processor) >= 3.0.0
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
ParseTree is a C extension (using RubyInline) that extracts the parse
tree for an entire class or a specific method and returns it as a
s-expression (aka sexp) using ruby's arrays, strings, symbols, and
integers.
As an example:
def conditional1(arg1)
if arg1 == 0 then
return 1
end
return 0
end
becomes:
[:defn,
:conditional1,
[:scope,
[:block,
[:args, :arg1],
[:if,
[:call, [:lvar, :arg1], :==, [:array, [:lit, 0]]],
[:return, [:lit, 1]],
nil],
[:return, [:lit, 0]]]]]


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

sed -i -e 's|/usr/local/bin|/usr/bin|' %{buildroot}%{geminstdir}/*/*

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/parse_tree_abc
%{_bindir}/parse_tree_audit
%{_bindir}/parse_tree_deps
%{_bindir}/parse_tree_show
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.8-2m)
- re-generate spec

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.8-1m)
- update 3.0.8

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.7-1m)
- update 3.0.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.6-2m)
- rebuild for new GCC 4.5

* Thu Oct 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.6-1m)
- update 3.0.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.5-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.5-2m)
- add Requires: ruby(abi) = 1.9.1

* Mon Aug  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.5-1m)
- update 3.0.5

* Sat Jan 02 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-1m)
- Initial package for Momonga Linux
