%global momorel 6

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: Langpacks plugin for yum
Name: yum-langpacks
Version: 0.1.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
Source0: http://petersen.fedorapeople.org/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: yum-langpacks-0.1.5-rm-rf-fix.patch
Patch1: yum-langpacks-0.1.5-momonga.patch
URL: https://fedoraproject.org/wiki/Features/YumLangpackPlugin
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: python-setuptools
Requires: yum >= 3.0

%description
Yum-langpacks is a plugin for YUM that looks for langpacks for your native
language for packages you install.

%prep
%setup -q

%patch0 -p1
%patch1 -p1

%build

%install
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root, -)
%doc README COPYING
%{python_sitelib}/*
%{_prefix}/lib/yum-plugins/langpacks.py*
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/langpacks.conf

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.5-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.5-3m)
- full rebuild for mo7 release

* Mon Aug  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.5-2m)
- modify for kde3-i18n and koffice-l10n

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.5-1m)
- import from Fedora

* Sat Apr 24 2010 James Antill <james.antill@redhat.com> - 0.1.5-2
- Fix the problems where langpacks could delete the system.
- Resolves: bug#585424
- Register the package name, for yum history etc.

* Thu Mar 11 2010 Jens Petersen <petersen@redhat.com> - 0.1.5-1
- fixes and improvements from James Antill (#571845):
  - Move print's => yum logger uses.
  - Move tsinfo ops to real install/remove ops, so deps will be solved
  - Handles erase properly.
  - Doesn't force upgrade langpacks on upgrades/downgrades/etc.
  - Doesn't try to install multiple versions of langpacks. (#569352)
  - Move from d.has_key(v) to "v in d", as py3 removes the former.

* Tue Dec  8 2009 Jens Petersen <petersen@redhat.com> - 0.1.4-2
- fix source url (#536737)
- drop python requires (#536737)
- drop buildroot and cleaning of buildroot (#536737)
- use global (#536737)

* Mon Nov 16 2009 Jens Petersen <petersen@redhat.com> - 0.1.4-1
- BR python-setuptools (#536737)
- remove shebang from langpacks.py (#536737)

* Wed Nov 11 2009 Jens Petersen <petersen@redhat.com> - 0.1.3-1
- initial package (#433512)
