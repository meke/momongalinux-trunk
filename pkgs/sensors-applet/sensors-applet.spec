%global momorel 6

Summary: hardware sensors for Gnome
Name: sensors-applet
Version: 2.2.5
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://sensors-applet.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: sensors-applet-2.2.5-linking.patch
Patch1: sensors-applet-2.2.7-libnotify-0.7.2.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig, intltool, gnome-common, glib2-devel
BuildRequires: libgnome-devel
BuildRequires: libgnomeui-devel
BuildRequires: gnome-panel-devel
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: gnome-doc-utils-devel
BuildRequires: dbus-devel
BuildRequires: avahi-devel
BuildRequires: lm_sensors-devel >= 3.0.2
Requires: gtk2-common

%description
GNOME Sensors Applet is an applet for the GNOME Panel to display
readings from hardware sensors, including CPU temperature, fan speeds
and voltage readings under Linux.

%package devel
Summary: sensor-applet-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
sensor-appplet-devel

%prep
%setup -q
%patch0 -p1 -b .linking
%patch1 -p1 -b .libnotify

%build
%configure --enable-libnotify --disable-scrollkeeper --enable-static=no
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%find_lang %{name}
rm -f %{buildroot}/%{_datadir}/icons/hicolor/icon-theme.cache

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
if [ -x /usr/bin/gtk-update-icon-cache ]; then
    gtk-update-icon-cache -q -t -f /usr/share/icons/hicolor
fi
rarian-sk-update

%postun
/sbin/ldconfig
if [ -x /usr/bin/gtk-update-icon-cache ]; then
    gtk-update-icon-cache -q -t -f /usr/share/icons/hicolor
fi
rarian-sk-update

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/libsensors-applet-plugin.so.*
%exclude %{_libdir}/libsensors-applet-plugin.la
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/plugins
%{_libdir}/%{name}/plugins/*.so
%{_libdir}/%{name}/plugins/*.la

%{_libdir}/bonobo/servers/SensorsApplet.server
%{_libexecdir}/%{name}
%{_datadir}/gnome-2.0/ui/SensorsApplet.xml
%{_datadir}/gnome/help/%{name}
%{_datadir}/icons/hicolor/*/devices/*
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/omf/%{name}
%{_datadir}/pixmaps/%{name}

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}
%{_libdir}/libsensors-applet-plugin.so

%changelog
* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.5-6m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.5-3m)
- full rebuild for mo7 release

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.5-2m)
- explicitly link libdl (Patch0)

* Sun Apr 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.5-1m)
- update to 2.2.5

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.4-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.4-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Wed Apr 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.4-1m)
- update to 2.2.4

* Tue Mar 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.1-5m)
- rebuild against rpm-4.6

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1-4m)
- rebuild against lm_sensors-3.0.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-3m)
- rebuild against gcc43

* Wed Jan  9 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.1-2m)
- change Source0's URL'

* Sat Dec 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Mon Dec 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0
- add devel library

* Tue Dec 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Sat Oct 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Wed Jul  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Sat Feb 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.11-1m)
- update to 1.7.11

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.10-2m)
- to main

* Sat Nov 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.10-1m)
- update to 1.7.10

* Thu Aug 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.4-2m)
- rebuild against dbus-0.92

* Sat Jul 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.4-1m)
- update 1.7.4

* Tue Jun 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.0-1m)
- initial build
