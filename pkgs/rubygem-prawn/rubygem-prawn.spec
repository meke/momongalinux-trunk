%global momorel 1

# Generated from prawn-0.8.4.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname prawn
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: A fast and nimble PDF generator for Ruby
Name: rubygem-%{gemname}
Version: 0.12.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://wiki.github.com/sandal/prawn
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(prawn-core) >= 0.8.4
Requires: rubygem(prawn-core) < 0.9
Requires: rubygem(prawn-layout) >= 0.8.4
Requires: rubygem(prawn-layout) < 0.9
Requires: rubygem(prawn-security) >= 0.8.4
Requires: rubygem(prawn-security) < 0.9
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
Prawn is a fast, tiny, and nimble PDF generator for Ruby


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Tue Sep 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.0-1m)
- update 0.12.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.4-1m)
- Initial package for Momonga Linux
