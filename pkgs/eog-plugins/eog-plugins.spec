%global momorel 3

Summary: Eye Of Gnome plugins
Name: eog-plugins
Version: 3.2.1
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Base
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.2/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/
BuildRequires: pkgconfig
BuildRequires: intltool

BuildRequires: gtk3-devel
BuildRequires: glib2-devel
BuildRequires: eog-devel >= 3.0.0
BuildRequires: pygobject-devel
BuildRequires: gnome-python2
BuildRequires: pygtk2-devel
BuildRequires: libchamplain-devel >= 0.11.0
BuildRequires: clutter-devel >= 1.11.8
BuildRequires: clutter-gtk-devel
BuildRequires: cogl-devel >= 1.10.2
BuildRequires: libexif-devel
BuildRequires: libchamplain-devel >= 0.7.2
BuildRequires: libchamplain-gtk-devel

%description
Eye of Gnome plugins


%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --disable-schemas-install \
    --enable-python
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%postun
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/eog/plugins/libexif-display.*
%{_libdir}/eog/plugins/libfit-to-width.*
%{_libdir}/eog/plugins/libmap.*
%{_libdir}/eog/plugins/libsend-by-mail.*
%{_libdir}/eog/plugins/libpostasa.*

%{_libdir}/eog/plugins/pythonconsole
%{_libdir}/eog/plugins/slideshowshuffle.py*

%{_libdir}/eog/plugins/exif-display.plugin
%{_libdir}/eog/plugins/fit-to-width.plugin
%{_libdir}/eog/plugins/pythonconsole.plugin
%{_libdir}/eog/plugins/send-by-mail.plugin
%{_libdir}/eog/plugins/slideshowshuffle.plugin
%{_libdir}/eog/plugins/postasa.plugin
%{_libdir}/eog/plugins/map.plugin
%{_datadir}/eog/plugins/exif-display
%{_datadir}/eog/plugins/postasa

%{_datadir}/glib-2.0/schemas/org.gnome.eog.plugins.exif-display.gschema.xml

%changelog
* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.1-3m)
- rebuild against clutter-1.11.8 and cogl-1.10.2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Thu Oct 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-2m)
- rebuild for new GCC 4.6

* Wed Feb  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-5m)
- rebuild against libchamplain-0.7.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-4m)
- full rebuild for mo7 release

* Sun Aug  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-3m)
- add BuildRequires

* Sun Jun 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- rebuild against libchamplain-0.6.1

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Mon Oct  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- initial build
