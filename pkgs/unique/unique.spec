%global momorel 9
Name:           unique
Version:        1.1.6
Release: %{momorel}m%{?dist}
Summary:        Single instance support for applications

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.gnome.org/~ebassi/source/
Source0:        http://download.gnome.org/sources/libunique/1.1/libunique-%{version}.tar.bz2
NoSource: 0

# Fix build -- upstream dead (replaced with GtkApplication)
Patch0:    fix-unused-but-set-variable.patch
Patch1:    fix-disable-deprecated.patch

BuildRequires:  dbus-glib-devel
BuildRequires:  gnome-doc-utils >= 0.3.2
BuildRequires:  libtool
BuildRequires:  glib2-devel >= 2.12.0
BuildRequires:  gtk2-devel >= 2.11.0
BuildRequires:  gtk-doc >= 1.11

%description
Unique is a library for writing single instance applications, that is
applications that are run once and every further call to the same binary
either exits immediately or sends a command to the running instance.

%package devel
Summary: Libraries and headers for Unique
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: dbus-glib-devel
Requires: gtk2-devel

%description devel
Headers and libraries for Unique.

%prep
%setup -q -n libunique-%{?version}
%patch0 -p1 -b .unused-but-set-variable
%patch1 -p1 -b .disable-deprecated

%build
%configure --enable-gtk-doc --disable-static --enable-introspection=no --enable-maintainer-flags=no
%make 

%install
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_libdir}/*.la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%doc %{_datadir}/gtk-doc
%{_includedir}/unique-1.0/
%{_libdir}/pkgconfig/*
%{_libdir}/lib*.so

%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-9m)
- reimport from fedora
-- import bug fix patches

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-8m)
- fix build failure with glib 2.33+

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.6-7m)
- rebuild with new glib

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-5m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.6-4m)
- rebuild against gobject-introspection-0.9.10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-3m)
- full rebuild for mo7 release

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.6-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sun Nov 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.6-1m)
- update 1.1.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Sun Mar 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-1m)
- update 1.0.8
-- rename unique -> libunique (tarname)

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-2m)
- rebuild against glib2 >= 2.19.8
-- remove -fno-strict-aliasing workaround for gcc44

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-3m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-2m)
- rebuild against rpm-4.6

* Wed Jul  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4-1m)
- Initial commit Momonga Linux. import from Fedora

* Fri May 16 2008 Richard Hughes  <rhughes@redhat.com> - 0.9.4-5
- More updates to the spec file from Dan Horak, rh#446407

* Thu May 15 2008 Richard Hughes  <rhughes@redhat.com> - 0.9.4-4
- Updates to the spec file from Dan Horak, rh#446407

* Thu May 08 2008 Richard Hughes  <rhughes@redhat.com> - 0.9.4-3
- Initial version

