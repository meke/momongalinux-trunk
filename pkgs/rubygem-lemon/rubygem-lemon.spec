# Generated from lemon-0.9.1.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname lemon

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Pucker-strength Unit Testing
Name: rubygem-%{gemname}
Version: 0.9.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: BSD
URL: http://rubyworks.github.com/lemon
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(rubytest) 
Requires: rubygem(ae) 
Requires: rubygem(ansi) >= 1.3
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Lemon is a unit testing framework that tightly correlates
class to test case and method to test unit.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/lemons
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE.txt
%doc %{geminstdir}/HISTORY.md
%doc %{geminstdir}/README.md
%doc %{geminstdir}/SPECSHEET.md
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.1-1m)
- update 0.9.1

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-3m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-2m)
- rename /usr/bin/lemon to /usr/bin/ruby-lemon

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- Initial package for Momonga Linux
