%global momorel 14
%global tomoever 0.6.0

Summary: A tool for providing tomoe support to uim
Name: uim-tomoe-gtk
Version: 0.6.0
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://tomoe.sourceforge.jp/
Group: User Interface/X
Source0: http://dl.sourceforge.net/sourceforge/tomoe/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: %{name}-%{version}-no-e.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: uim
Requires: tomoe >= %{tomoever}
Requires: libtomoe-gtk >= %{tomoever}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gettext-devel
BuildRequires: gtk2-devel >= 2.4.0
BuildRequires: libtomoe-gtk-devel >= %{tomoever}
BuildRequires: libtool
BuildRequires: pkgconfig
BuildRequires: uim-devel >= 1.8.4
BuildRequires: gucharmap_2_22-devel

%description
A tool for providing tomoe support to uim.

%prep
%setup -q
%patch0 -p1

%build
autoreconf -vfi
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/uim-tomoe-gtk
%{_datadir}/locale/*/LC_MESSAGES/uim-tomoe-gtk.mo

%changelog
* Sun Jan 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-14m)
- rebuild against uim-1.8.4

* Wed May 18 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-13m)
- rebuild against uim-1.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-7m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-6m)
- use autoreconf (for new autoconf-2.63)

* Sat Nov  1 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-5m)
- add BuildPreReq

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-4m)
- rebuild against uim-1.5.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-2m)
- %%NoSource -> NoSource

* Thu Jul  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-1m)
- version 0.6.0
- update no-e.patch

* Wed Feb  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-2m)
- rebuild against uim-1.4.0

* Fri Dec 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-1m)
- version 0.5.0

* Thu Nov 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- version 0.4.0

* Wed Nov 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- version 0.3.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.0-8m)
- rebuild against expat-2.0.0-1m

* Sat Aug  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-7m)
- rebuild against uim-1.2.0

* Sat Jul  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-6m)
- remove desktop file (uim-tomoe-gtk was merged to uim-toolbar, it's smart)

* Sat Jun 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-5m)
- rebuild against uim-1.1.0

* Fri Jan 13 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.0-4m)
- apply 'uim-tomoe-gtk-0.2.0-no-e.patch' to avoid dependency with enlightenment

* Fri Dec  2 2005 Masaru SANUKI <sanuki@mmonga-linux.org>
- (0.2.0-3m)
- update uim-tomoe-gtk.desktop

* Sun Nov  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-2m)
- update uim-tomoe-gtk.desktop

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-1m)
- version 0.2.0
- update uim-tomoe-gtk.desktop

* Wed Mar  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-1m)
- initial package for Momonga Linux
- import %%description from cooker
