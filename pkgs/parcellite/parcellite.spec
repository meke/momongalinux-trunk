%global momorel 5

Name:           parcellite
Version:        0.9.2
Release:        %{momorel}m%{?dist}
Summary:        A lightweight GTK+ clipboard manager

Group:          User Interface/Desktops
License:        GPLv3+
URL:            http://parcellite.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/parcellite/parcellite-%{version}.tar.gz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.10.0 
BuildRequires:  desktop-file-utils, intltool >= 0.23

%description
Parcellite is a stripped down, basic-features-only clipboard manager with a 
small memory footprint for those who like simplicity.

In GNOME and Xfce the clipboard manager will be started automatically. For 
other desktops or window managers you should also install a panel with a 
system tray or notification area if you want to use this package.


%prep
%setup -q

%build
%configure LIBS="-lX11" CPPFLAGS=-DGLIB_COMPILATION
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p'
%find_lang %{name}

desktop-file-install --vendor=                             \
  --delete-original                                        \
  --remove-category=Application                            \
  --dir=${RPM_BUILD_ROOT}%{_datadir}/applications          \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop

desktop-file-install --vendor=                             \
  --delete-original                                        \
  --add-category=TrayIcon                                  \
  --dir=${RPM_BUILD_ROOT}%{_sysconfdir}/xdg/autostart/     \
  ${RPM_BUILD_ROOT}%{_sysconfdir}/xdg/autostart/%{name}-startup.desktop


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README NEWS
%config(noreplace) %{_sysconfdir}/xdg/autostart/%{name}-startup.desktop
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*


%changelog
* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.2-5m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-3m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-1m)
- import from Rawhide

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Nov 23 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.9-1
- Update to 0.9
- Fix Control+Click behaviour
- Small corrections to German translation

* Sat Apr 19 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.8-1
- Update to 0.8

* Sat Apr 19 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.7-2
- No longer require lxpanel
- Preserve timestamps during install
- Include NEWS in doc

* Sat Apr 12 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.7-1
- Initial Fedora RPM
