%global qtdir %{_libdir}/qt3
%global momorel 12

Summary: gnuplot frontend
Name:    qgfe
Version: 1.0
Release: %{momorel}m%{?dist}
Group:   Applications/Engineering
License: GPLv2
URL:     http://www.xm1math.net/qgfe/
Source0: http://www.xm1math.net/qgfe/qgfe-1.0.tar.bz2
Requires: qt3 >= 3.3.7, gnuplot
BuildRequires: qt3-devel >= 3.3.7, desktop-file-utils
BuildRequires: libjpeg-devel >= 8a
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
Qgfe is a graphical interface to the powerful gnuplot program for Unix systems. A good knowledge of Gnuplot is required to use Qgfe.

%prep
%setup -q -n %{name}-%{version}

%build
export QTDIR=%{qtdir}
qmake -unix QTDIR=$QTDIR PREFIX=%{_prefix} %{name}.pro
%make

%install
rm -rf %{buildroot}
make INSTALL_ROOT=%{buildroot} install

## desktop
%__mkdir_p %{buildroot}%{_datadir}/applications
%__cat > qgfe.desktop <<EOF
[Desktop Entry]
Name=Qgfe
GenericName=Qgfe Gnuplot Frontend
Comment=Gnuplot Frontend
Exec=qgfe
Terminal=false
Type=Application
Icon=/usr/share/qgfe/qgfe16x16.png
EOF
# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
  --add-category Science \
  --add-category Math \
    ./qgfe.desktop


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc utilities/AUTHORS utilities/COPYING
%{_bindir}/*
%{_datadir}/%{name}
%{_datadir}/applications/*.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-10m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-9m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-7m)
- rebuild against libjpeg-7

* Fri Jun 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-6m)
- no NoSource, because primary site has gone

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against rpm-4.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-4m)
- rebuild against qt3

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-3m)
- revised qgfe.desktop

* Sat Apr 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-2m)
- build fix under KDE4 environment
- enable parallel build

* Sat Apr 19 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-1m)
- build for Momonga
