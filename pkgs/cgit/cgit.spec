%global momorel 1

# Review bug: https://bugzilla.redhat.com/479723
%define gitver      1.7.4.4
%define cachedir    %{_localstatedir}/cache/cgit
%define scriptdir   %{_localstatedir}/www/cgi-bin
%define cgitdata    %{_datadir}/cgit

%define make_cgit \
export CFLAGS="%{optflags}" \
export LDFLAGS="-lpthread" \
make V=1 %{?_smp_mflags} \\\
     DESTDIR=%{buildroot} \\\
     INSTALL="install -p"  \\\
     CACHE_ROOT=%{cachedir} \\\
     CGIT_SCRIPT_PATH=%{scriptdir} \\\
     CGIT_SCRIPT_NAME=cgit \\\
     CGIT_DATA_PATH=%{cgitdata}

Name:           cgit
Version:        0.9.0.3
Release:        %{momorel}m%{?dist}
Summary:        A fast webinterface for git

Group:          Development/Tools
License:        GPLv2
URL:            http://hjemli.net/git/cgit/
Source0:        http://hjemli.net/git/cgit/snapshot/cgit-%{version}.tar.bz2
NoSource:	0
Source1:        http://www.kernel.org/pub/software/scm/git/git-%{gitver}.tar.bz2
Source2:        cgitrc
Source3:        cgit.httpd
Source4:        README.SELinux
# Applied upstream stable branch (4ac89ec and d529c6f)
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if 0%{?fedora}
BuildRequires:  libcurl-devel
%else
BuildRequires:  curl-devel
%endif
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  zlib-devel >= 1.2.5-2m
Requires:       httpd

%description
This is an attempt to create a fast web interface for the git scm,
using a builtin cache to decrease server io-pressure.

%prep
%setup -q -a 1

# setup the git dir
rm -rf git
mv git-%{gitver} git
sed -i 's/^\(CFLAGS = \).*/\1%{optflags}/' git/Makefile

# add README.SELinux
cp -p %{SOURCE4} .


%build
%{make_cgit}


%install
rm -rf %{buildroot}
%{make_cgit} install
install -d -m0755 %{buildroot}%{_sysconfdir}/httpd/conf.d
install -p -m0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/cgitrc
install -p -m0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/httpd/conf.d/cgit.conf.dist
install -d -m0755 %{buildroot}%{cachedir}

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING README* cgitrc.5.txt
%config(noreplace) %{_sysconfdir}/cgitrc
%config(noreplace) %{_sysconfdir}/httpd/conf.d/cgit.conf.dist
%dir %attr(-,apache,root) %{cachedir}
%{cgitdata}
%{scriptdir}/*
%dir %{_prefix}/lib/cgit
%dir %{_prefix}/lib/cgit/filters/
%{_prefix}/lib/cgit/filters/*sh


%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0.3-1m)
- update 0.9.0.3

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0.2-2m)
- add older git source

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0.2-1m)
- update 0.9.0.2

* Sun May  8 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-1m)
- [SECURITY] CVE-2011-1027
-  http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-1027
- update to 0.9
- update gitver to 1.7.5.1 but canceled for build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3.5-4m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.5-3m)
- update gitver to 1.7.4.4

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.5-2m)
- update gitver to 1.7.4.3

* Mon Mar 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.5-1m)
- update gitver to 1.7.4.2

* Mon Feb 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.4-6m)
- update gitver to 1.7.4.1

* Sat Feb  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.4-5m)
- update gitver to 1.7.4

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.4-4m)
- update gitver to 1.7.3.5

* Tue Dec 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.4-3m)
- update gitver to 1.7.3.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3.4-2m)
- rebuild for new GCC 4.5

* Fri Nov  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.4-1m)
- update to 0.8.3.4
- update gitver to 1.7.3.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.3.2-5m)
- full rebuild for mo7 release

* Fri Jul 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.2-4m)
- update gitver to 1.7.2.1

* Fri Jul 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.2-3m)
- update gitver to 1.7.2

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.3.2-2m)
- add BuildRequires: zlib-devel >= 1.2.5-2m

* Thu Jul 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3.2-1m)
- update to 0.8.3.2
- update gitver to 1.7.1.1

* Sun Apr  4 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3.1-3m)
- update gitver to 1.7.0.4

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3.1-2m)
- rebuild against openssl-1.0.0

* Fri Dec 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3.1-1m)
- update to 0.8.3.1
- update gitver to 1.6.6

* Wed Dec 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3-5m)
- update gitver to 1.6.5.6
- rename cgit.conf to cgit.conf.dist

* Mon Dec  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3-4m)
- update gitver to 1.6.5.5

* Fri Dec  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3-3m)
- update gitver to 1.6.5.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.3-1m)
- update
- update gitver to 1.6.4.3

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2.1-5m)
- update gitver to 1.6.4

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.2.1-4m)
- update gitver to 1.6.3.3

* Mon May 18 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2.1-3m)
- update gitver to 1.6.3.1

* Fri May  8 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2.1-2m)
- update gitver to 1.6.3

* Fri May  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2.1-1m)
- update to 0.8.2.1
- update gitver to 1.6.2.4

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.2-3m)
- rebuild against openssl-0.9.8k

* Wed Mar  4 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-2m)
- update gitver to 1.6.2

* Tue Feb 17 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2
- No NoSource
- update gitver to 1.6.1.3
- drop imported Patch0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-2m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-1m)
- import from Fedora to Momonga
- update gitver to 1.6.1

* Mon Jan 12 2009 Todd Zullinger <tmz@pobox.com> - 0.8.1-1
- Initial package
