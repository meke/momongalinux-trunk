%global		momorel 1

Name:           glfw
Version:        3.0.4
Release: %{momorel}m%{?dist}
Summary:        A cross-platform multimedia library
Summary(fr):    Une bibliothèque multimédia multi-plateforme
License:        "zlib"
URL:            http://www.glfw.org/index.html
Source0:	http://downloads.sourceforge.net/project/glfw/glfw/%{version}/glfw-%{version}.zip
NoSource:	0
Group:		System Environment/Libraries
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libX11-devel, libXi-devel, mesa-libGL-devel, mesa-libGLU-devel, libXrandr-devel
BuildRequires:  cmake
BuildRequires: 	geany

%description
GLFW is a free, Open Source, multi-platform library for OpenGL application
development that provides a powerful API for handling operating system specific
tasks such as opening an OpenGL window, reading keyboard, mouse, joystick and
time input, creating threads, and more.

%description -l fr_FR
GLFW est un logiciel gratuit, Open Source, multi-plate-forme de bibliothèque
pour l'application OpenGL développement qui fournit une API puissante pour la
manipulation du système d'exploitation spécifique des tâches telles que
l'ouverture d'une fenêtre OpenGL, la lecture du clavier, souris, joystick et
entrée du temps, les discussions de créer, et plus encore.


%package        devel
Summary:        Support for developing C application
Summary(fr):    Appui pour le développement d'application C
Requires:       %{name}%{?_isa} =  %{version}-%{release}
Requires:       xorg-x11-proto-devel
Requires:       pkgconfig

%description devel
The glfw-devel package contains header files for developing glfw
applications.

%description devel -l fr_FR
Le paquet glfw-devel contient les fichiers d'entêtes pour développer
des applications utilisant glfw.

%prep
%setup -q
find . -type f | xargs sed -i 's/\r//'

# temp geany config directory for allow geany to generate tags
mkdir -p geany_config

%build
%cmake -DCMAKE_INSTALL_PREFIX=%{_prefix} .
%make  all
# generate geany tags
geany -c geany_config -g glfw.c.tags $(find src \( ! -name CMakeFiles \) -type f \( -iname "*.c" -o -iname "*.h" \) \( ! -iname "win32*" \) \( ! -iname "cocoa*" \)
) include/GL/glfw3.h

%install
%make  install PREFIX=%{_prefix} LIBDIR=%{_lib} DESTDIR=%{buildroot}

# install geany tags
install -d %{buildroot}/%{_datadir}/geany/tags/
install -m0644 glfw.c.tags %{buildroot}/%{_datadir}/geany/tags/

%post   -p  /sbin/ldconfig
%postun -p  /sbin/ldconfig

%files
%doc README.md COPYING.txt
%{_libdir}/libglfw.so.*

%files devel
%{_includedir}/GLFW/glfw3.h
%{_includedir}/GLFW/glfw3native.h
%{_libdir}/libglfw.so
%{_libdir}/pkgconfig/glfw3.pc
%{_libdir}/cmake/glfw/glfwConfig.cmake
%{_libdir}/cmake/glfw/glfwConfigVersion.cmake
%{_libdir}/cmake/glfw/glfwTargets-noconfig.cmake
%{_libdir}/cmake/glfw/glfwTargets.cmake
%{_datadir}/geany/tags/glfw.c.tags

%changelog
* Fri Apr 18 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.4-1m)
- import from fedora

