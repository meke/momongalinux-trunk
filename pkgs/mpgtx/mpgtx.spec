%global momorel 9

Summary: MPEG ToolboX
Name: mpgtx
Version: 1.3.1
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
License: GPL
URL: http://mpgtx.sourceforge.net/
Source0: http://dl.sourceforge.net/mpgtx/mpgtx-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
mpgtx a command line MPEG audio/video/system file toolbox, that slices and joins audio and video files, including MPEG1, MPEG2 and MP3.


%prep
%setup -q

%build
./configure \
    --prefix=%{_prefix} \
    --manprefix=%{_datadir}
%make CFLAGS="%{optflags}"


%install
%{__rm} -rf %{buildroot}
%{__make} install \
    PREFIX=%{buildroot}%{_prefix} \
    manprefix=%{buildroot}%{_datadir}


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-, root, root, 0755)
%doc AUTHORS ChangeLog COPYING README TODO
%{_bindir}/*
%{_mandir}/man1/*.1*
%lang(de) %{_mandir}/de/man1/*.1*


%changelog
* Mon Sep 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-9m)
- fix URL of source0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- %%NoSource -> NoSource

* Mon Apr 09 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.1-1m)
- import from freshrpms.net

* Fri Jan 19 2007 Matthias Saou <http://freshrpms.net/> 1.3.1-1
- Initial RPM release.

