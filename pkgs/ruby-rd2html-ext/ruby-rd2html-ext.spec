%global momorel 5

%global rbname rd2html-ext
Summary: RD formatter
Name: ruby-%{rbname}

Version: 0.1.4
Release: %{momorel}m%{?dist}
Group: Applications/Text
License: GPL and Ruby
URL: http://www.rubyist.net/~rubikitch/computer/rd2html-ext/

Source0: http://www.rubyist.net/~rubikitch/archive/%{rbname}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: ruby

Requires: ruby-rdtool

%description
ruby-rd2html-ext

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q -n rd2html-ext-%{version}

%build

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
ruby setup.rb config \
    --rb-dir=%{buildroot}%{ruby_libdir}
ruby setup.rb install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr (-,root,root)
%doc sample rd2html-ext.rd GPL
%{ruby_libdir}/rd/*

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.4-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.4-1m)
- update 0.1.4

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.3-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.3-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-2m)
- %%NoSource -> NoSource

* Thu May 20 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.3-1m)
- verup

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.1.2-6m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.1.2-5m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.1.2-4m)
- rebuild against ruby-1.8.0.

* Sat Jan 18 2003 HOSONO Hidetomo <h@h12o.org>
- (0.1.2-3m)
- use buildroot macro instead of RPM_BUILD_ROOT environment variable
- change my mail address on the previous changelog entry,
  "h@h12o.org" to "h12o@h12o.org" :D

* Wed Aug 14 2002 Kenta MURATA <muraken2@nifty.com>
- (0.1.2-2m)
- BuildPreReq: ruby

* Thu Jul  4 2002 HOSONO Hidetomo <h12o@h12o.org>
- (ruby-rd2html-ext-0.1.2-1m)
- initial release
