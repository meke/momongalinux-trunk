%global         momorel 1
%global         qtver 4.8.4
%global         kdever 4.9.97
%global         kdelibsrel 1m
%global         qtdir %{_libdir}/qt4
%global         kdedir /usr
%global         libktorrent_ver 1.3.1
#global         rcver rc1
#global         prever 1

Summary:        BitTorrent program for KDE
Name:           ktorrent
Version:        4.3.1
Release:        %{?rcver:0.%{prever}.}%{momorel}m%{?dist}
License:        GPLv2
Group:          Applications/Internet
URL:            http://ktorrent.org/
Source0:        http://ktorrent.org/downloads/%{version}%{?rcver:%{rcver}}/%{name}-%{version}%{?rcver:%{rcver}}.tar.bz2
NoSource:       0
Patch0:         %{name}-%{version}-link.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       kdelibs >= %{kdever}-%{kdelibsrel}
Requires:       %{name}-libs = %{version}-%{release}
Requires:       libktorrent >= %{libktorrent_ver}
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  libktorrent-devel >= %{libktorrent_ver}
BuildRequires:  cmake
BuildRequires:  avahi-devel
BuildRequires:  strigi-devel >= 0.7.7
Obsoletes:      %{name}-devel

%description
KTorrent is a BitTorrent program for KDE.
The 2.0 release adds several big new features to KTorrent :
  * Support for distributed hash tables (mainline version)
  * Protocol encryption
  * Bandwith scheduling
  * Directory scanner to automatically load torrents in certain directories
  * Trackers can now be added to torrents
  * File prioritization for multi file torrents
We allready supported the following features in the previous version :
  * Downloads torrent files
  * Upload and download speed capping
  * Internet searching using various search engines, you can even add your own
  * UDP Trackers
  * Port forwarding with UPnP
  * IP blocking plugin
  * Importing of partially or fully downloaded files

%package libs
Summary: Runtime libraries for %{name}
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kdelibs >= %{kdever}-%{kdelibsrel}

%description libs
%{summary}.

%package -n kde-plasma-%{name}
Summary: ktorrent plasma applet
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}

%description -n kde-plasma-%{name}
%{summary}.

%prep
%setup -q -n %{name}-%{version}%{?rcver:%{rcver}}
%patch0 -p1 -b .link

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	-DWITH_SYSTEM_GEOIP:BOOL=ON \
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING ChangeLog
%{_kde4_bindir}/*
%{_kde4_libdir}/kde4/*.so
%exclude %{_kde4_libdir}/kde4/plasma_applet_%{name}.so
%{_kde4_datadir}/applications/kde4/*
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/icons/*/*/*/*
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/kde4/services/magnet.protocol
%exclude %{_kde4_datadir}/kde4/services/plasma-applet-%{name}.desktop
%{_kde4_datadir}/kde4/servicetypes/*
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/doc/HTML/en/%{name}

%files libs
%defattr(-, root, root)
%{_kde4_libdir}/*.so.*

%files -n kde-plasma-%{name}
%defattr(-, root, root)
%{_kde4_libdir}/kde4/plasma_applet_%{name}.so
%{_kde4_datadir}/kde4/services/plasma-applet-%{name}.desktop

%changelog
* Sat Jan 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to 4.3.1

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to 4.3.0

* Wed Aug 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3-0.1.1m)
- update to 4.3rc1

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to 4.2.0

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2-0.1.1m)
- update to 4.2rc1

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-0.20111230.1m)
- update to 4.2.0 git snapshot

* Wed Nov 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-1m)
- update to 4.1.3

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.2-1m)
- update to 4.1.2

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.1-1m)
- update to 4.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.0-2m)
- rebuild for new GCC 4.6

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to 4.1.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.5-1m)
- update to 4.0.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.4-2m)
- rebuild for new GCC 4.5

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.2-2m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-2m)
- rebuild against qt-4.6.3-1m

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Tue May 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to 4.0.0

* Tue May  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-0.1.1m)
- update to 4.0rc1

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-2m)
- touch up spec file

* Mon Feb  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.4-1m)
- update to 3.3.4

* Mon Jan 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.3-1m)
- update to 3.3.3

* Sun Dec 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-1m)
- update to 3.3.2
- separate sub packages

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.1-1m)
- version 3.3.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3-1m)
- update to 3.3

* Sat Oct 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3-0.1.1m)
- update to 3.3rc1

* Sun Oct 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.5-1m)
- update to 3.2.5

* Sun Oct  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.4-1m)
- update to 3.2.4

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-3m)
- import qt452 patch from debian unstable (3.2.3+dfsg.1-1)
-- http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=537023

* Sat Aug 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-2m)
- source tar ball has been updated

* Wed Aug 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Tue Jun  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Apr 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Tue Feb 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Sun Feb  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2-0.2.1m)
- update to 3.2rc1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-0.1.5m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2-0.1.4m)
- do not need STRIGI option with strigi-0.6.3-2m

* Fri Jan 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2-0.1.3m)
- pass STRIGI_*_LIBRARY_RELEASE to cmake

* Fri Jan  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2-0.1.2m)
- remove patch0 for KDE 4.2 beta2

* Tue Nov 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2-0.1.1m)
- update to 3.2beta1

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.4-1m)
- [SECURITY] CVE-2008-5905 CVE-2008-5906
- update to 3.1.4

* Sun Oct 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.3-1m)
- update to 3.1.3

* Wed Aug  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-1m)
- update to 3.1 official release

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-0.3.1m)
- update to 3.1rc1

* Tue May 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-0.2.1m)
- update to 3.1beta2

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-0.1.2m)
- rebuild against qt-4.4.0-1m

* Wed Apr 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-0.1.1m)
- update to 3.1beta1

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.1-2m)
- rebuild against gcc43

* Tue Mar 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Tue Feb 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Jan 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.2.1m)
- update to 3.0rc1

* Tue Jan 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-0.1.1m)
- update to 3.0beta1 for KDE4

* Fri Nov 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.4-1m)
- update to 2.2.4

* Fri Nov 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Wed Jul  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Thu Jun 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-0.2.1m)
- update to 2.2rc1

* Wed Jun 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2-0.1.1m)
- update to 2.2beta1

* Thu Apr 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.4-1m)
- update to 2.1.4

* Tue Apr  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Sun Mar 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1-2m)
- rebuild against kdelibs etc.

* Thu Feb  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-1m)
- update to 2.1 official release

* Sun Feb  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-0.2.2m)
- BuildPreReq: avahi-devel

* Thu Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-0.2.1m)
- update to 2.1rc1

* Sun Dec  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-0.1.1m)
- update to 2.1beta1

* Wed Nov 15 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-2m)
- remove dupullicated desktop file

* Tue Nov 14 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-1m)
- initial build for Momonga Linux 3
