%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 2m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global sopranover 2.9.4
%global strigiver 0.7.8
%global qimageblitzver 0.0.6
%global obso_name kdebase

Summary: K Desktop Environment 4 - core files
Name: kde-baseapps
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource :0

# fedora specific nsplugins paths
Patch0:   kdebase-4.6.1-nsplugins-paths.patch
# backwards compatibility hack for a config option changed by our old
Patch2:   kdebase-4.6.1-home-icon.patch
# fix disabling automatic spell checking in the Konqueror UI (kde#228593)
Patch3:   kdebase-4.6.1-konqueror-kde#228593.patch
# Password & User account becomes non responding
Patch4:   kde-baseapps-4.12.2-bz#609039.patch
# add x-scheme-handler/http for konqueror so it can be set
# as default browser in GNOME
Patch5:   kde-baseapps-4.9.2-konqueror-mimetyp.patch

## upstream patches

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires: kdelibs >= %{version}
Requires: kdepimlibs >= %{version}
Requires: kde-runtime >= %{version}

# for customized konsole
Requires: dejavu-lgc-sans-mono-fonts

# for KDE 4.6 -> KDE 4.7
Requires: konsole
Requires: kwrite

Requires: eject
Requires: %{name}-libs = %{version}-%{release}
# should not conflict with d3lphin, but does with old Dolphin
Conflicts:     dolphin <= 0.8.2
# FIXME: remove BRs which are only needed for stuff now in -runtime
BuildRequires: alsa-lib-devel
BuildRequires: bzip2-devel
# needed?
BuildRequires: cdparanoia
BuildRequires: cdparanoia-devel
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: cyrus-sasl-devel
BuildRequires: doxygen
BuildRequires: fontconfig-devel
BuildRequires: gettext
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: kde-workspace-devel >= %{version}
BuildRequires: kdepimlibs-devel >= %{version}
# needed?
BuildRequires: libart_lgpl-devel
# needed?
BuildRequires: libfontenc-devel
BuildRequires: libusb-devel
BuildRequires: OpenEXR-devel
# needed?
BuildRequires: openldap-devel
BuildRequires: openssl-devel
BuildRequires: pkgconfig
BuildRequires: qimageblitz-devel >= %{qimageblitzver}
# samba support
BuildRequires: libsmbclient-devel
BuildRequires: shared-desktop-ontologies-devel
BuildRequires: soprano-devel >= %{sopranover}
BuildRequires: strigi-devel >= %{strigiver}
BuildRequires: xine-lib-devel
# needed for phonon-xine VideoWidget, also need xine-lib built with libxcb support
BuildRequires: libxcb-devel
# needed?
BuildRequires: xorg-x11-font-utils
BuildRequires: xorg-x11-proto-devel
BuildRequires: zlib-devel
# extraneous?  add these back to kdelibs-devel Requires? maybe -- Rex
BuildRequires: gamin-devel
BuildRequires: giflib-devel
BuildRequires: libacl-devel
BuildRequires: pcre-devel
BuildRequires: glib2-devel
BuildRequires: libraw1394-devel >= 2.0.2

# rename
Obsoletes: %{obso_name} < %{version}
Provides:  %{obso_name} = %{version}-%{release}

# Obsolete some KDE3 packages
Obsoletes: kaquarium
Obsoletes: kicker-compiz
Obsoletes: kickpim
Obsoletes: kima
Obsoletes: ksmoothdock
Obsoletes: ksplash-engine-moodin
Obsoletes: tastymenu
Obsoletes: kdebase-kappfinder

# for KDE 4.6 -> KDE 4.7
Obsoletes: konq-plugins
Provides:  konq-plugins
Provides:  konqueror = %{version}-%{release}
Provides:  kde-baseapps = %{version}-%{release}

%description
Core runtime requirements and applications for the K Desktop Environment 4.
This package does not include the KDE 4 versions of applications which are
provided by KDE 3 because of file and configuration setting conflicts.

%package common
Summary:  Common files for %{name}
Group:    System Environment/Libraries
Conflicts: kde-baseapps < %{verion}
BuildArch: noarch

%description common
%{summary}

%package libs
Summary:  %{name} runtime libraries
Group:    System Environment/Libraries
Requires:  %{name}-common = %{version}-%{release}
Requires:  dolphin-libs = %{version}-%{release}
Requires:  konqueror-libs = %{version}-%{release}
Requires:  libkonq = %{version}-%{release}
Obsoletes: %{obso_name}-libs < %{version}
Provides:  %{obso_name}-libs = %{version}-%{release}

%description libs
%{summary}.

%package devel
Summary:   Header files for kdebase4
Group:     Development/Libraries
Requires:  %{name} = %{version}-%{release}
Requires:  kdelibs-devel
Requires:  kdepimlibs-devel
Obsoletes: %{obso_name}-devel < %{version}
Provides:  %{obso_name}-devel = %{version}-%{release}

%description devel
Header files for developing applications using %{name}.

%package -n dolphin
Summary: KDE File Manager
Group:   User Interface/Desktops
Requires: %{name}-common = %{version}-%{release}
Requires: dolphin-libs = %{version}-%{release}
Requires: kdialog = %{version}-%{release}
Requires: libkonq = %{version}-%{release}
Requires: kde-runtime >= %{version}

%description -n dolphin
This package contains the default file manager of KDE.

%package -n dolphin-libs
Summary: Dolphin runtime libraries
Group:   System Environment/Libraries
Requires: dolphin = %{version}-%{release}

%description -n dolphin-libs
%{summary}.

%package -n kdepasswd
Summary: Changes your UNIX password
Group:   User Interface/Desktops
Requires: %{name}-common = %{version}-%{release}
Requires: kde-runtime >= %{version}

%description -n kdepasswd
This application allows you to change your UNIX password.

%package -n kdialog
Summary:  Nice dialog boxes from shell scripts
Group:    User Interface/Desktops
Requires: %{name}-common = %{version}-%{release}
Requires: kde-runtime >= %{version}

%description -n kdialog
KDialog can be used to show nice dialog boxes from shell scripts.

%package -n keditbookmarks
Summary:  Bookmark organizer and editor
Group:    User Interface/Desktops
Requires: %{name}-common = %{version}-%{release}
Requires: keditbookmarks-libs = %{version}-%{release}
Requires: kde-runtime >= %{version}

%description -n keditbookmarks
%{summary}.

%package -n keditbookmarks-libs
Summary: Dolphin runtime libraries
Group:   System Environment/Libraries
Requires: keditbookmarks = %{version}-%{release}

%description -n keditbookmarks-libs
%{summary}.

%package -n kfind
Summary:  KDE Find File Utility
Group:    User Interface/Desktops
Requires: %{name}-common = %{version}-%{release}
Requires: kde-runtime >= %{version}

%description -n kfind
KFind allows you to search for directories and files.

%package -n konqueror
Summary:  KDE File Manager and Browser
Group:    User Interface/Desktops
Requires: %{name}-common = %{version}-%{release}
# dolphinpart for embedded filemanagement
Requires: dolphin = %{version}-%{release}
Requires: konqueror-libs = %{version}-%{release}
Requires: kdialog = %{version}-%{release}
Requires: libkonq = %{version}-%{release}
#Requires: kwebkitpart
Requires: kde-runtime >= %{version}

%description -n konqueror
Konqueror allows you to manage your files and browse the web in a
unified interface.

%package -n konqueror-libs
Summary: Konqueror runtime libraries
Group:   System Environment/Libraries
Requires: konqueror = %{version}-%{release}

%description -n konqueror-libs
%{summary}.

%package -n konqueror-devel
Summary:  Development files for konqueror
Group:    Development/Libraries
Requires: konqueror-libs = %{version}-%{release}
Requires: libkonq-devel
Requires: kdelibs-devel

%description -n konqueror-devel
%{summary}.

%package -n libkonq
Summary: Libkonq shared resources
Group:   System Environment/Libraries
Requires: %{name}-common = %{version}-%{release}
Requires: kdelibs >= %{version}

%description -n libkonq
%{summary}.

%package -n libkonq-devel
Summary:  Development files for Libkonq
Group:    Development/Libraries
Requires: libkonq = %{version}-%{release}
Requires: kdelibs-devel

%description -n libkonq-devel
%{summary}.

%package -n kde-plasma-folderview
Summary: FolderView plasma applet
Group:   User Interface/Desktops
Requires: %{name}-common = %{version}-%{release}
Requires: libkonq = %{version}-%{release}

%description -n kde-plasma-folderview
%{summary}.

%prep
%setup -q

%patch0 -p1 -b .nsplugins-paths
%patch2 -p1 -b .home-icon
%patch3 -p1 -b .kde#228593
%patch4 -p1 -b .bz#609039
%patch5 -p1 -b .mimetyp.patch

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        %{?kappfinder:-DBUILD_KAPPFINDER=BOOL:ON} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# konquerorsu only show in KDE
echo 'OnlyShowIn=KDE;' >> %{buildroot}%{_kde4_datadir}/applications/kde4/konquerorsu.desktop

# create/own some dirs
mkdir -p %{buildroot}%{_kde4_appsdir}/konqueror/{kpartplugins,icons,opensearch}

## unpackaged files
# libs for which there is no (public) api
rm -fv %{buildroot}%{_kde4_libdir}/lib{dolphin,kbookmarkmodel_,konqueror}private.so
# omit konqsidebarplugin api bits (for now), nothing uses it afaict
rm -fv %{buildroot}%{_kde4_libdir}/libkonqsidebarplugin.so
rm -fv %{buildroot}%{_kde4_includedir}/konqsidebarplugin.h

# move devel symlinks
mkdir -p %{buildroot}%{_kde4_libdir}/kde4/devel
pushd %{buildroot}%{_kde4_libdir}
for i in lib*.so
do
  case "$i" in
    libkonq.so)
      linktarget=`readlink "$i"`
      rm -fv "$i"
      ln -sf "../../$linktarget" "kde4/devel/$i"
      ;;
    *)
      ;;
  esac
done
popd

# fix documentation multilib conflict in index.cache
for f in konqueror dolphin ; do
   bunzip2 %{buildroot}%{_kde4_docdir}/HTML/en/$f/index.cache.bz2
   sed -i -e 's!name="id[a-z]*[0-9]*"!!g' %{buildroot}%{_kde4_docdir}/HTML/en/$f/index.cache
   sed -i -e 's!#id[a-z]*[0-9]*"!!g' %{buildroot}%{_kde4_docdir}/HTML/en/$f/index.cache
   bzip2 -9 %{buildroot}%{_kde4_docdir}/HTML/en/$f/index.cache
done

HTML_DIR=$(kde4-config --expandvars --install html)
if [ -d %{buildroot}${HTML_DIR} ]; then
for lang_dir in %{buildroot}${HTML_DIR}/* ; do
  if [ -d ${lang_dir} ]; then
    lang=$(basename ${lang_dir})
    echo "%lang(${lang}) ${HTML_DIR}/${lang}/*/" >> %{name}.lang
  fi
done
fi

%find_lang dolphin --with-kde --without-mo
%find_lang kdepasswd --with-kde --without-mo
%find_lang kfind --with-kde --without-mo
%find_lang konqueror --with-kde --without-mo

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
update-desktop-database -q &> /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
  update-desktop-database -q &> /dev/null ||:
fi

%posttrans -n dolphin
update-desktop-database -q &> /dev/null ||:

%postun -n dolphin
if [ $1 -eq 0 ] ; then
  update-desktop-database -q &> /dev/null ||:
fi

%post -n dolphin-libs -p /sbin/ldconfig

%postun -n dolphin-libs -p /sbin/ldconfig

%post -n konqueror
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:

%posttrans -n konqueror
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
update-desktop-database -q &> /dev/null ||:

%postun -n konqueror
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null ||:
  update-desktop-database -q &> /dev/null ||:
fi

%post -n konqueror-libs -p /sbin/ldconfig

%postun -n konqueror-libs -p /sbin/ldconfig

%post -n libkonq -p /sbin/ldconfig

%postun -n libkonq -p /sbin/ldconfig

%post -n kfind
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null ||:

%posttrans -n kfind
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%postun -n kfind
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
fi

%files
# empty metapackage

%files libs
# empty metapackage

%files common
%defattr(-,root,root,-)
%doc COPYING COPYING.DOC COPYING.LIB

%files -n dolphin -f dolphin.lang
%defattr(-,root,root,-)
%{_kde4_bindir}/dolphin
%{_kde4_libdir}/libkdeinit4_dolphin.so
%{_kde4_bindir}/servicemenu*
%{_kde4_appsdir}/dolphin
%{_kde4_datadir}/applications/kde4/dolphin.desktop
%{_kde4_libdir}/kde4/kcm_dolphin*.so
%{_kde4_libdir}/kde4/kio_filenamesearch.so
%{_kde4_datadir}/config.kcfg/dolphin_*.kcfg
%{_kde4_appsdir}/dolphinpart
%{_kde4_libdir}/kde4/dolphinpart.so
%{_kde4_datadir}/kde4/services/dolphinpart.desktop
%{_kde4_configdir}/servicemenu.knsrc
%{_kde4_datadir}/kde4/services/kcmdolphinnavigation.desktop
%{_kde4_datadir}/kde4/services/filenamesearch.protocol
%{_kde4_datadir}/kde4/services/kcmdolphingeneral.desktop
%{_kde4_datadir}/kde4/services/kcmdolphinservices.desktop
%{_kde4_datadir}/kde4/services/kcmdolphinviewmodes.desktop
%{_kde4_datadir}/kde4/servicetypes/fileviewversioncontrolplugin.desktop
# stuff from konqueror/ source dir thats not konq-specific
%{_kde4_libdir}/kde4/kcm_kio.so
%{_kde4_datadir}/kde4/services/smb.desktop
%{_kde4_datadir}/applications/kde4/Home.desktop

%files -n dolphin-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libdolphinprivate.so.4*

%files -n konqueror -f konqueror.lang
%defattr(-,root,root,-)
%{_kde4_bindir}/fsview
%{_kde4_bindir}/kfmclient
%{_kde4_libdir}/libkdeinit4_kfmclient.so
%{_kde4_bindir}/konqueror
%{_kde4_libdir}/libkdeinit4_konqueror.so
%{_kde4_bindir}/nspluginscan
%{_kde4_bindir}/nspluginviewer
%{_kde4_bindir}/servicemenudeinstallation
%{_kde4_bindir}/servicemenuinstallation
%{_kde4_datadir}/applications/kde4/kfmclient.desktop
%{_kde4_datadir}/applications/kde4/kfmclient_dir.desktop
%{_kde4_datadir}/applications/kde4/kfmclient_html.desktop
%{_kde4_datadir}/applications/kde4/kfmclient_war.desktop
%{_kde4_datadir}/applications/kde4/konqbrowser.desktop
%{_kde4_datadir}/applications/kde4/konquerorsu.desktop
%{_kde4_appsdir}/akregator
%{_kde4_appsdir}/domtreeviewer
%{_kde4_appsdir}/fsview
%{_kde4_appsdir}/kcmcss
%{_kde4_appsdir}/kcontrol
%{_kde4_appsdir}/khtml/kpartplugins
%{_kde4_appsdir}/konqsidebartng
%{_kde4_appsdir}/konqueror
%dir %{_kde4_appsdir}/kwebkitpart
%{_kde4_appsdir}/kwebkitpart/kpartplugins
%{_kde4_appsdir}/nsplugin
%{_kde4_iconsdir}/hicolor/*/apps/fsview.*
%{_kde4_iconsdir}/hicolor/*/apps/konqueror.*
%{_kde4_iconsdir}/oxygen/*/actions/*
%{_kde4_datadir}/kde4/services/kded/*.desktop
%{_kde4_datadir}/kde4/services/useragentstrings/
%{_kde4_datadir}/kde4/servicetypes/uasprovider.desktop
%{_kde4_datadir}/kde4/servicetypes/konqaboutpage.desktop
%{_kde4_datadir}/autostart/konqy_preload.desktop
%{_kde4_configdir}/konqsidebartngrc
%{_kde4_configdir}/translaterc
%{_kde4_libdir}/kde4/adblock.so
%{_kde4_libdir}/kde4/akregatorkonqfeedicon.so
%{_kde4_libdir}/kde4/autorefresh.so
%{_kde4_libdir}/kde4/babelfishplugin.so
%{_kde4_libdir}/kde4/dirfilterplugin.so
%{_kde4_libdir}/kde4/domtreeviewerplugin.so
%{_kde4_libdir}/kde4/fsviewpart.so
%{_kde4_libdir}/kde4/kcm_history.so
%{_kde4_libdir}/kde4/kcm_konq.so
%{_kde4_libdir}/kde4/kcm_konqhtml.so
%{_kde4_libdir}/kde4/kcm_kurifilt.so
%{_kde4_libdir}/kde4/kcm_performance.so
%{_kde4_libdir}/kde4/kded_konqy_preloader.so
%{_kde4_libdir}/kde4/khtmlkttsdplugin.so
%{_kde4_libdir}/kde4/khtmlsettingsplugin.so
%{_kde4_libdir}/kde4/kimgallery.so
%{_kde4_libdir}/kde4/konq_aboutpage.so
%{_kde4_libdir}/kde4/konq_shellcmdplugin.so
%{_kde4_libdir}/kde4/konq_sidebar.so
%{_kde4_libdir}/kde4/konq_sidebartree_bookmarks.so
%{_kde4_libdir}/kde4/konq_sidebartree_dirtree.so
%{_kde4_libdir}/kde4/konqsidebar_history.so
%{_kde4_libdir}/kde4/konqsidebar_places.so
%{_kde4_libdir}/kde4/konqsidebar_tree.so
%{_kde4_libdir}/kde4/konqsidebar_web.so
%{_kde4_libdir}/kde4/libkcminit_nsplugins.so
%{_kde4_libdir}/kde4/libnsplugin.so
%{_kde4_libdir}/kde4/minitoolsplugin.so
%{_kde4_libdir}/kde4/rellinksplugin.so
%{_kde4_libdir}/kde4/searchbarplugin.so
%{_kde4_libdir}/kde4/uachangerplugin.so
%{_kde4_libdir}/kde4/validatorsplugin.so
%{_kde4_libdir}/kde4/webarchiverplugin.so
%{_kde4_libdir}/kde4/webarchivethumbnail.so
%{_kde4_datadir}/config.kcfg/konqueror.kcfg
%{_kde4_datadir}/config.kcfg/validators.kcfg
%{_datadir}/dbus-1/interfaces/org.kde.FavIcon.xml
%{_datadir}/dbus-1/interfaces/org.kde.Konqueror.Main.xml
%{_datadir}/dbus-1/interfaces/org.kde.Konqueror.MainWindow.xml
%{_datadir}/dbus-1/interfaces/org.kde.konqueror.Preloader.xml
%{_datadir}/dbus-1/interfaces/org.kde.nsplugins.CallBack.xml
%{_datadir}/dbus-1/interfaces/org.kde.nsplugins.class.xml
%{_datadir}/dbus-1/interfaces/org.kde.nsplugins.instance.xml
%{_datadir}/dbus-1/interfaces/org.kde.nsplugins.viewer.xml
%{_kde4_datadir}/kde4/services/ServiceMenus/imageconverter.desktop
%{_kde4_datadir}/kde4/services/bookmarks.desktop
%{_kde4_datadir}/kde4/services/cache.desktop
%{_kde4_datadir}/kde4/services/cookies.desktop
%{_kde4_datadir}/kde4/services/ebrowsing.desktop
%{_kde4_datadir}/kde4/services/filebehavior.desktop
%{_kde4_datadir}/kde4/services/fsview_part.desktop
%{_kde4_datadir}/kde4/services/kcmhistory.desktop
%{_kde4_datadir}/kde4/services/kcmkonqyperformance.desktop
%{_kde4_datadir}/kde4/services/kcmperformance.desktop
%{_kde4_datadir}/kde4/services/khtml_appearance.desktop
%{_kde4_datadir}/kde4/services/khtml_behavior.desktop
%{_kde4_datadir}/kde4/services/khtml_filter.desktop
%{_kde4_datadir}/kde4/services/khtml_general.desktop
%{_kde4_datadir}/kde4/services/khtml_java_js.desktop
%{_kde4_datadir}/kde4/services/khtml_plugins.desktop
%{_kde4_datadir}/kde4/services/konq_aboutpage.desktop
%{_kde4_datadir}/kde4/services/konq_sidebartng.desktop
%{_kde4_datadir}/kde4/services/konqueror.desktop
%{_kde4_datadir}/kde4/services/netpref.desktop
%{_kde4_datadir}/kde4/services/proxy.desktop
%{_kde4_datadir}/kde4/services/useragent.desktop
%{_kde4_datadir}/kde4/services/webarchivethumbnail.desktop

%files -n konqueror-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libkonqsidebarplugin.so.*
%{_kde4_libdir}/libkonquerorprivate.so.4*

%files -n libkonq
%defattr(-,root,root,-)
%{_kde4_libdir}/libkonq.so.5*
%{_kde4_libdir}/kde4/kded_favicons.so
%{_kde4_libdir}/kde4/konq_sound.so
%{_kde4_appsdir}/kbookmark
%dir %{_kde4_appsdir}/konqueror
%dir %{_kde4_appsdir}/konqueror/pics
%{_kde4_appsdir}/konqueror/pics/arrow_*.png
%{_kde4_datadir}/kde4/services/kded/favicons.desktop
%{_kde4_datadir}/kde4/servicetypes/konqdndpopupmenuplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/konqpopupmenuplugin.desktop
%{_kde4_datadir}/templates/.source/*
%{_kde4_datadir}/templates/*.desktop

%files devel
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/devel/libkonq.so
%{_kde4_includedir}/knewmenu.h
%{_kde4_includedir}/konq_*.h
%{_kde4_includedir}/konqmimedata.h
%{_kde4_includedir}/kversioncontrolplugin*.h
%{_kde4_includedir}/libkonq_export.h

%files -n kdepasswd -f kdepasswd.lang
%defattr(-,root,root,-)
%{_kde4_bindir}/kdepasswd
%{_kde4_datadir}/applications/kde4/kdepasswd.desktop
%{_kde4_libdir}/kde4/kcm_useraccount.so
%{_kde4_datadir}/config.kcfg/kcm_useraccount.kcfg
%{_kde4_datadir}/config.kcfg/kcm_useraccount_pass.kcfg
%{_kde4_datadir}//kde4/services/kcm_useraccount.desktop
%dir %{_kde4_appsdir}/kdm
%dir %{_kde4_appsdir}/kdm/pics
%dir %{_kde4_appsdir}/kdm/pics/users
%{_kde4_appsdir}/kdm/pics/users/*

%files -n kdialog
%defattr(-,root,root,-)
%{_kde4_bindir}/kdialog
%{_datadir}/dbus-1/interfaces/org.kde.kdialog.ProgressDialog.xml

%files -n keditbookmarks
%defattr(-,root,root,-)
%{_kde4_bindir}/kbookmarkmerger
%{_kde4_bindir}/keditbookmarks
%{_kde4_libdir}/libkdeinit4_keditbookmarks.so
%{_kde4_datadir}/applications/kde4/keditbookmarks.desktop
%{_kde4_appsdir}/keditbookmarks
%{_kde4_datadir}/config.kcfg/keditbookmarks.kcfg
%{_kde4_appsdir}/kbookmark
%{_mandir}/man1/kbookmarkmerger.1.*

%files -n keditbookmarks-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libkbookmarkmodel_private.so.4*

%files -n kfind -f kfind.lang
%defattr(-,root,root,-)
%{_kde4_bindir}/kfind
%{_kde4_datadir}/applications/kde4/kfind.desktop
%{_mandir}/man1/kfind.1.*
%{_kde4_iconsdir}/hicolor/*/apps/kfind.*

%files -n kde-plasma-folderview
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/plasma_applet_folderview.so
%{_kde4_datadir}/kde4/services/plasma-applet-folderview.desktop

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sat Dec 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9.90-2m)
- remove bad patch for dolphin
- https://bugzilla.redhat.com/show_bug.cgi?id=886964

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.4-2m)
- rebuild for glib 2.33.2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0
- rename from kdebase to kde-baseapps

* Fri Jan  6 2012 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.97-2m)
- remove BR hal

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-2m)
- import upstream patches

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-2m)
- fix folderview icon sorting (kde227157)

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-2m)
- fix up Obsoletes: kdebase-kappfinder

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.3-4m)
- change BR from xine-lib to xine-lib-devel

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-3m)
- import upstream patch from Fedora devel

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sun May 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.3-2m)
- rebuild against libraw1394-2.0.2

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sun Apr 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

* Sun Apr 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-3m)
- change Requires from dejavu-lgc-fonts to dejavu-lgc-sans-mono-fonts

- * Sat Apr 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sat Apr 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Sat Mar 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.1-2m)
- remove Obsoletes: kdebase from kdebase-libs

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- import patch2 from Fedora devel
- update upstream patch

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0
- add patch100 from Fedra devel

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-2m)
- rebuild against strigi-0.6.2

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68
- remove Requires: kdebase from kdebase-libs

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.63-1m)
- update to 4.1.63

* Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.62-1m)
- update to 4.1.62

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.61-1m)
- update to 4.1.61

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-2m)
- import new patch from Fedora 9 (Patch102)

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-2m)
- remove Obsoletes: compizconfig-backend-kconfig

* Mon Apr  7 2008 NAARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3
- import Patch100 from Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-4m)
- rebuild against gcc43

* Tue Mar 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-3m)
- modify default profile of konsole

* Mon Mar 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-2m)
- good-bye ksmoothdock

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2
- apply upstream patch
- create lib subpackage

* Sat Feb 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-3m)
- add Obsoletes: kickpim

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-2m)
- obsolete some KDE3 packages

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Sun Jan 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to KDE4

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-5m)
- apply latest svn fix to avoid crashing nspluginviewer on x86_64
- http://bugs.kde.org/show_bug.cgi?id=154713

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-4m)
- apply latest svn fix
- http://bugs.kde.org/show_bug.cgi?id=155001

* Sun Dec  2 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-2m)
- added patch for gcc43

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8
- delete unused patches and update some patches

* Thu Sep 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-12m)
- [SECURITY] CVE-2007-4569
- import upstream security patch (post-3.5.7-kdebase-kdm.diff)

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-11m)
- [SECURITY] CVE-2007-3820, CVE-2007-4224, CVE-2007-4225
- update security patch (post-3.5.7-kdebase-konqueror-2.diff)

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-10m)
- [SECURITY] CVE-2007-3820, CVE-2007-4224, CVE-2007-4225
- import upstream security patch (post-3.5.7-kdebase-konqueror.diff)
- remove kdebase-3.5.7-konqueror-fix-spoofing.patch

* Wed Jul 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-9m)
- [SECURITY] CVE-2007-3820 Konqueror "data:" URI Scheme Address Bar Spoofing
- to resolve above issue, add patch500
- see http://secunia.com/advisories/26091/
- see also http://seclists.org/fulldisclosure/2007/Jul/0301.html

* Tue Jul 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-8m)
- import kdebase-3.5.7-bz#244906.patch from Fedora
- https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=244906

* Sun Jul 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-7m)
- add PreReq: coreutils

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-6m)
- modify startkde.momonga (delete copying kdeglobals.ja section)

* Thu Jul 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-5m)
- update kde-momonga-config.tar.bz2 (fix kickerrc)

* Tue Jul  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-4m)
- change Requires from dbus to dbus-x11

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-3m)
- modify /etc/pam.d/* for new pam

* Thu Jun  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- add Requires: bitstream-vera-fonts for monospace single-byte characters

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- delete unused upstream patches

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-8m)
- BuildPreReq: kdelibs-devel >= 3.5.6-16m (for relname: rainbowshoes)

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-7m)
- modify %%files for new filesystem
- sort BuildPreReq
- BuildPreReq: kdelibs-devel >= 3.5.6-15m (for relname: littlewing)

* Tue Apr 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-6m)
- install package_games_kids Icons

* Tue Mar 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-5m)
- install Momonga Icons

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-4m)
- import upstream patch to fix following problem
  #142806, startkde should check lnusertemp return values
- update startkde.momonga (apply above patch)

* Mon Feb 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-3m)
- update Xsession.momonga (add $DBUS_LAUNCH to startkde)

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against libmad libid3tag openldap libraw1394 libusb kdelibs

* Sun Jan 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- patch100 is still needed and patch117 - patch119 was moved to patch101 - patch103
- delete another unused upstream patches

* Sun Jan 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-16m)
- import upstream patch to fix following problem
  #94470, maximize window underruns panel

* Thu Jan 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-15m)
- import upstream patches to fix following problems
  #138873, kdesu crash after su password
  #115898, kdesu hangs, the window does not appear until "killall kdesud"

* Mon Jan 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-14m)
- import upstream patches to fix following problems
  #24735, close window doesnt work with open dialog box
  #63276, Konqueror doesn't remember size and position of its window between restarts

* Sat Jan 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-13m)
- import upstream patch to fix following problem
  #96605, History 'Number of Lines' - 1 is viewable

* Mon Jan  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-12m)
- import upstream patch to fix following problem
  #128696, Monitor Power Saving Settings Reverting

* Fri Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-11m)
- import upstream patches to fix following problems
  #117677, crash while switching show desktop state
  #138834, kwin crashes when launching application splash shot
  #139180, kwin crash when switching windows (3.5.5)

* Thu Dec 14 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-10m)
- import upstream patches to fix following problems
  Rev.612728, Don't steal focus from windows using globally active input focus.
  Rev.612721, Disable some debug output for focus stealing prevention.

* Sat Dec  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-9m)
- import upstream patches to fix following problems
  #138521, SIGSEGV crash after saving history twice
  #136958, konqueror crash after a lot of cut&paste operations in tree view

* Sat Nov 25 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-8m)
- import upstream patches to fix following problems
  #127012, kcheckpass doesn't refresh credentials via PAM
  #134734, Screensaver change to the ordinary X-Window-Logo when awaking from dpms

* Tue Nov 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-7m)
- rebuild against OpenEXR-1.4.0

* Tue Nov  7 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-6m)
- import upstream patch to fix following problem
  #136876, Symlinks in trash should show symlink size, not file size

* Mon Nov  6 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.5-5m)
- rebuild against lm_sensor

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- import upstream patch from FC6
- fix popup menu problem of konqueror (kdebase-3.5.5-kde#134816.patch)

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.5-3m)
- rebuild against expat-2.0.0-2m

* Sat Oct 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- import upstream patch to fix following problem (Patch101)
  #135250, desktop unusable due to flickering of windows if not "focus follows click"

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5
- remove merged upstream patches

* Wed Sep 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-13m)
- import kdebase-3.5.4.ksystraycmd-quoting.diff.gz from Slackware
  fix argument quoting
- import 5 upstream patches from Fedora Core devel
 +* Thu Sep 07 2006 Than Ngo <than@redhat.com> 6:3.5.4-7
 +- apply upstream patch
 +   fix #53642, Menubar is always visible after coming back from fullscreen
 +   fix #133665, crash in kiosk mode
 +* Fri Aug 25 2006 Than Ngo <than@redhat.com> 6:3.5.4-5
 +- apply upstream patch to fix kdedesktop crash, kde#132873
 +- fix kdm crash
 +* Mon Aug 21 2006 Than Ngo <than@redhat.com> 6:3.5.4-4
 +- fix #203221, konsole does not display bold characters

* Mon Sep 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-12m)
- import kdebase-3.5.4-antialias.patch from Ark Linux
 +* Sun Jun 25 2006 Bernhard Rosenkraenzer <bero@arklinux.org> 3.5.3-5ark
 +- Fix default XftAntialias setting

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-11m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-10m)
- BuildPreReq: hal-devel >= 0.5.7.1 -> 0.5.7

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-9m)
- rebuild against dbus-qt-0.70

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-8m)
- rebuild against dbus-qt-0.63-2m

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-7m)
- rebuild against dbus-0.92 dbus-qt-0.63
- update patch12 kdebase-3.5.3-dbus.patch

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-6m)
- rebuild against kdelibs-3.5.4-2m

* Sat Aug 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-5m)
- add Requires: truetype-fonts-ja-ipa

* Wed Aug  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-4m)
- import kdebase-3.5.4.halbackend.diff.gz from Slackware
  fix KDED crashing on startup when D-BUS is unavailable

* Tue Aug  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-3m)
- import kdebase-3.5.4.kicker-taskbar-resize.diff.gz from Slackware, thanks to JW

* Sat Aug  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-2m)
- import kdebase-3.5.4.video.redirect.diff.gz from Slackware for YouTube, thanks to JW

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4
- update startkde.momonga
- update kioslave_media_dbus-fuser.patch
- remove merged upstream patches

* Fri Jul 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-14m)
- import 2 upstream patches from Fedora Core devel
 +* Thu Jul 20 2006 Than Ngo <than@redhat.com> 6:3.5.3-15
 +- apply upstream patches,
 +    fix kde#130774, Strange lonesome icon in KInfocenter's start page
 +* Tue Jul 11 2006 Than Ngo <than@redhat.com> 6:3.5.3-9
 +- upstream patches,
 +    #kde127971 hard Drive device icons do not show on desktop

* Tue Jul 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-13m)
- import kdebase-3.5.3-khelpcenter-sort.patch from Fedora Core devel
 +* Sun Jul 16 2006 Than Ngo <than@redhat.com> 6:3.5.3-13
 +- fix 146377, khelpcenter's Browse Info Pages > Alphabetically should sort entries

* Sun Jul  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-12m)
- update kdebase-3.5.3-kioslave_media_dbus-fuser.patch
  /usr/bin/env fuser -> /sbin/fuser

* Fri Jul  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-11m)
- import 2 upstream patches from Fedora Core devel
 +* Thu Jul 06 2006 Than Ngo <than@redhat.com> 6:3.5.3-8
 +- fix #187228, kio_media_mounthelper fails with fuser
 +* Fri May 12 2006 Than Ngo <than@redhat.com> 6:3.5.2-8
 +- fix 190836, xmTextFieldClass widgets don't work properly

* Wed Jul  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-10m)
- fix setuid

* Wed Jun 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-9m)
- change AA initial settings "exclude range set checked" to false
- BuildPreReq: OpenEXR-devel >= 1.2.2-2m

* Thu Jun 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-8m)
- [SECURITY] post-3.5.0-kdebase-kdm.diff for "KDM symlink attack vulnerability"
  http://www.kde.org/info/security/advisory-20060614-1.txt

* Mon Jun 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-7m)
- update and apply dbus patches from Fedora Core devel
 +* Thu Jun 01 2006 Than Ngo <than@redhat.com> 6:3.5.3-1
 +- update kioslave_media_dbus patch
 +* Sun Mar 26 2006 Than Ngo <than@redhat.com> 6:3.5.2-1
 +- update dbus patch
- remove media_hal.diff
- import 4 upstream patches from Fedora Core devel
 +* Sat Jun 10 2006 Than Ngo <than@redhat.com> 6:3.5.3-5
 +- add several upstream patches

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-6m)
- add Momonga3.kcsrc, it's a copy of Momonga1.kcsrc and the best of Color Scheme on KDE3

* Wed Jun  7 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.5.3-5m)
- remove Requires: momonga-desktop

* Mon Jun  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-4m)
- import Xsession and xinitrc-* from xorg-x11-xinit-1.0.2-7m
- modify Xsession and xinitrc-* for kdm

* Sat Jun  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- add Settings to Categories of printers.desktop
- revise kups command

* Fri Jun  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- rebuild with avahi (enable Zeroconf support)

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3
- install kde.desktop for new gdm
- update startkde.momonga
- remove merged kwin.patch

* Tue May 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-9m)
- Requires: htdig (khelpcenter uses htdig)

* Thu May 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-8m)
- Requires: usermode (kdm uses /usr/bin/poweroff)

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.5.2-7m)
- rebuild against dbus-0.61

* Mon May  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-6m)
- import kcmsamba_log.diff from opensuse
 +* Mon Apr 17 2006 - ltinkl@suse.cz
 +- fix "KDE Information Center fails to provide log info for samba"
 +  (#98763)
- import kdebase-3.5.2-kwin.patch from Fedora Core devel
 +* Fri Apr 28 2006 Than Ngo <than@redhat.com> 6:3.5.2-5
 +- fix #189702, kwin crashes when switching windows with Alt-Tab
- clean up patches

* Fri Apr 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-5m)
- import use-full-hinting-by-default.diff from opensuse
 +* Wed Apr 05 2006 - dmueller@suse.de
 +- update hinting default to full (#157441)

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.2-4m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-3m)
- import media_hal.diff from opensuse
 +* Fri Mar 31 2006 - stbinner@suse.de
 +- update media_hal.diff to make it possible that applications like
 +  k3b can stop automounting (#160654)
 +* Tue Mar 28 2006 - coolo@suse.de
 +- update media_hal.diff to handle fstab entries more correctly
 +* Thu Mar 23 2006 - stbinner@suse.de
 +- update media_hal.diff to fix setting of mount properties for a
 +  removable device (#160002) and respect hal lockdown (#153241)
 +* Tue Mar 07 2006 - stbinner@suse.de
 +- update media_hal.diff to fix mounting (#154652)
 +* Wed Feb 15 2006 - coolo@suse.de
 +- update media_hal.diff to fix unmount (#149472)
 +* Mon Feb 06 2006 - dmueller@suse.de
 +- rediff media_hal.diff to fix crash on logout
 +* Mon Jan 30 2006 - stbinner@suse.de
 +- update media_hal.diff

* Wed Apr  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-2m)
- add ktaskbarrc to kde-momonga-config.tar.bz2

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2
- remove merged 2 patches
 - kdebase-3.5.1-kwin-systray.patch
 - kdebase-3.5.1-keyboardlayout.patch

* Mon Mar 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-5m)
- update kdmrc and Xsession.momonga for new X
- revise option of configure for new X
- move kdm/README
- import kdebase-3.5.1-kwin-systray.patch from Fedora Core devel
 +* Thu Feb 16 2006 Than Ngo <than@redhat.com> 6:3.5.1-4
 +- Systray icons not docked sometimes, apply patch to fix this issue #180314
- import kdebase-3.5.1-keyboardlayout.patch from Fedora Core devel
 +* Fri Feb 03 2006 Than Ngo <than@redhat.com> 6:3.5.1-2
 +- apply patch to fix broken xx_XX layouts in kxkb
- kscreensaver/kdm/kcheckpass use a separate PAM config file FC BTS #66902

* Sun Mar 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-4m)
- rebuild against xscreensaver-4.18-6m

* Wed Mar 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-3m)
- remove GLIBCXX_FORCE_NEW=1 from startkde.momonga

* Thu Feb  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-2m)
- export GLIBCXX_FORCE_NEW=1 in startkde.momonga for scim with gcc-4.0.2
  "GLIBCXX_FORCE_NEW=1" doesn't work with "ja_JP.EUC-JP", please use ja_JP.UTF-8
  gcc-4.1.0-0.8m works fine without GLIBCXX_FORCE_NEW=1
  GLIBCXX_FORCE_NEW=1 should be removed ASAP
- reserve dbus.patch
 +* Wed Feb 01 2006 Than Ngo <than@redhat.com> 6:3.5.1-1
 +- apply patch to fix kded segfaults for audio/blank cd's

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1
- remove kdebase-3.1.1-usb.patch
- change user icon to default2.png

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-8m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.0-7m)
- rebuild against openldap-2.3.11

* Sun Jan  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-6m)
- modify initial settings (HalBackendEnabled=true)

* Fri Dec 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-5m)
- rebuild against dbus-0.60

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-4m)
- execute sdr at first startup from kdm

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-3m)
- set default.face.icon for kdm

* Thu Dec  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- update kde-momonga-config.tar.bz2 (fix panel problem)

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5
- set kdm default session: kde
- move kdm files from %%{_datadir}/config to %%{_sysconfdir}/kde
- Requires: sdr, momonga-desktop for kdm
- add %%{_datadir}/config-sample/kdm/desktop.sample

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.5m)
- import and modify Xsession.momonga from xinitrc-3.37-4m

* Tue Nov 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.4m)
- add kdmrc and backgroundrc to kde-momonga-config.tar.bz2

* Mon Nov 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.3m)
- move printmgr.desktop from %%{_datadir}/applnk to %%{_datadir}/applications/kde
- BuildPreReq: desktop-file-utils

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- update Source4: kde-momonga-config.tar.bz2
- BuildPreReq: dbus-devel, dbus-qt, hal-devel
- Requires: dbus, dbus-qt, hal
- disable hidden visibility

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- update startkde.momonga
- remove monospace.patch
- remove konsole-keymap.patch
- sync with Fedora Core devel
 +* Mon Oct 24 2005 Than Ngo <than@redhat.com> 6:3.4.92-2
 +- add separate PAM configuration for kscreensaver #66902

* Tue Nov  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-3m)
- remove wallpapers from Source4: kde-momonga-config.tar.bz2
- Requires: momonga-backgrounds >= 1.0-3m

* Wed Oct 19 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.3-2m)
- enable ia64.

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3
- remove kdebase-3.4.2-kinfocenter.patch
- remove kdebase-3.4.2-uic.patch
- remove post-3.4.2-kdebase-kcheckpass.diff

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-5m)
- import kdebase-3.4.2-uic.patch from Fedora Core devel
 +* Wed Sep 21 2005 Than Ngo <than@redhat.com> 6:3.4.2-5
 +- fix uic build problem
- import kdebase-3.4.2-kinfocenter.patch from Fedora Core devel
 +* Mon Sep 05 2005 Than Ngo <than@redhat.com> 6:3.4.2-4
 +- apply upstream patch to fix kinfocenter opengl DRI/GLX crash
- add kinfocenterrc to kde-momonga-config-3.4.2.tar.bz2

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-4m)
- add --disable-rpath to configure
- install Source1: pam.d-kde
- update kdesktop-konsole.patch from Fedora Core devel
- set Konqueror version

* Mon Sep  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-3m)
- [SECURITY] post-3.4.2-kdebase-kcheckpass.diff for "kcheckpass local root vulnerability"
  http://www.kde.org/info/security/advisory-20050905-1.txt

* Sun Sep  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- modify initial settings %%{_datadir}/config/*rc
- remove "Clean up old kde /tmp files" section from startkde.momonga

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- update startkde.momonga
- remove nokdelibsuff.patch
- add %%doc

* Sat Jul 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.4.1-3m)
- ppc build fix.

* Sat Jun 26 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.1-2m)
- fixed build error gcc4. add admin-visibility.patch

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1
- update startkde.momonga
- update visibility.patch

* Mon May  9 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-8m)
- kdelibsuff isn't effective unless "make -f admin/Makefile.common cvs".

* Mon May  9 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.4.0-7m)
- ppc build fix.

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-6m)
- modify %%files section

* Fri Apr  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-5m)
- %%attr(2755,root,nobody) %%{_bindir}/kdesud
- modify %%files section

* Tue Apr  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-4m)
- cancel changes of 3.4.0-3m

* Mon Apr  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- support selinux enforcing mode
  import Source1: pam.d-kde from Fedora Core
  chmod 755 kcheckpass and kdesud
- import Source2: pam.d-kde-np from Fedora Core

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Sun Mar 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- update startkde.momonga
- update vroot.patch from Fedora Core
- update monospace.patch from Fedora Core
- import kdebase-3.4.0rc1-konsole-keymap.patch from Fedora Core
 +* Thu Mar 10 2005 Than Ngo <than@redhat.com> 6:3.4.0-0.rc1.5
 +- apply patch (Hans de Goede) to fix incompatibilities between konsole/xterm, #138191
- import kdebase-3.4.0-kdesktop-konsole.patch from Fedora Core
 +* Mon Mar 21 2005 Than Ngo <than@redhat.com> 6:3.4.0-2
 +- add konsole in desktop menu
- remove old patches
- remove Bluecurve.ktheme
- PreReq: XFree86-font-utils -> xorg-x11-font-utils
- BuildPreReq: perl, pkgconfig

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.2-8m)
- rebuild against libraw1394 >= 1.1.0.

* Thu Mar  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-7m)
- add Momonga Color Scheme
- rename Source110-113

* Mon Feb 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-6m)
- replace Source110-113

* Sun Feb 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-5m)
- import directory files from FC3
  Source110: mo-ServerConfig-More.directory
  Source111: mo-ServerConfig.directory
  Source112: mo-SystemConfig-More.directory
  Source113: mo-SystemConfig.directory
  Source113 is modified for Momonga icon
- remove Source102: rh-SystemConfig.directory.momonga
- update Source4: kde-momonga-config-3.2.3.tar.bz2
  fix permission of %%{_datadir}/config/ksplashrc
  change default splash screen to Momonga3
  %%{_datadir}/config/kde -> %%{_datadir}/config/.kde
- update Source100: startkde.momonga
  import prelink script from SUSE 9.2 supplementary

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-4m)
- remove %%{_datadir}/applnk/momonga
- add Source102: rh-SystemConfig.directory.momonga for System menu

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-3m)
- [SECURITY] post-3.3.2-kdebase-htmlframes2.patch for "Konqueror Window Injection Vulnerability"
  http://kde.org/info/security/advisory-20041213-1.txt
- import kdebase-3.3.2-cleanup.patch from Fedora Core
 +* Thu Feb 10 2005 Than Ngo <than@redhat.com> 6:3.3.2-0.4
 +- add cleanup patch file from Steve

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Sat Sep 11 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0
- clean up patches
- import Patch82,83,84 from Fedora Core

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-3m)
- apply the following patch
- http://www.kde.org/info/security/advisory-20040811-3.txt

* Mon Jul 12 2004 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.3-2m)
- add Momonga splash

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (3.2.3-2m)
- revised spec for rpm 4.2.

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release
- revised startkde.momonga (BugID 24)

* Sat Feb 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.0-3m)
- rebuild against lm_sensors-2.8.4-1m

* Tue Feb 17 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.0-2m)
- import knotify_without_arts-kdebase.diff to add option
  to completely disable arts from kde-core-devel ML
    http://lists.kde.org/?l=kde-core-devel&m=107696911417308&w=2

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- modified %files (many new files)
- following patches are temporally omitted (unappliedable or already omitted)
- - Patch1: kdebase-3.0-appnames.patch
- - Patch2: kdebase-3.0.0-resource.patch
- - Patch15: kdebase-3.1.3-dtfix.patch
- - Patch22: kdebase-2.1.1-sound.patch
- - Patch27: kdebase-3.0-ptsname.patch
- - Patch37: kdebase-3.1-krdb.patch
- - Patch39: kdebase-3.0.3-themes.patch
- - Patch41: kdebase-3.1-clock.patch
- - Patch42: kdebase-3.1-kicker.patch
- - Patch50: kdebase-3.1-desktop.patch
- - Patch52: kdebase-3.1-ansi.patch
- - Patch100: kdebase-3.0.0-genkdmconf.patch
- - Patch301: fontenginecpp.diff
- - Patch306: kdebase-3.1.2-xrandr.patch
- - Patch307: kdebase-3.1.2-add_kcmranrd.patch
- - Patch312: kcmrandr-20030812-messages.patch

- following patches are replaced by new patch from Fedora
- - Patch3: kdebase-3.0.0-vroot.patch -> Patch3: kdebase-3.1.93-vroot.patch
- - Patch6: kdebase-3.1.1-staticmotif.patch -> Patch6: kdebase-3.1.2-motif.patch
- - (not applied)Patch9: kdebase-3.0.0-keydefs.patch -> kdebase-3.1.4-keydefs.patch
- - (not applied)Patch13: kdebase-3.1-config.patch -> kdebase-3.1.95-config.patch
- - Patch33: kdebase-3.1.2-monospace.patch -> Patch33: kdebase-3.1.94-monospace.patch
- - (not used)Patch38: kdebase-3.1-paneliconResize.patch -> kdebase-3.1.3-panelicon.patch
- - Patch40: kdebase-3.0.3-keymap.patch -> kdebase-3.1.93-keymap.patch
- - Patch42: kdebase-3.1-kicker.patch -> kdebase-3.1.93-kicker.patch
- following patches are imported from Fedora
- - Patch65: kdebase-3.1.1-usb.patch
- - (not applied)Patch66: kdebase-3.1.1-klipper.patch
- - Patch67: kdebase-3.1.93-kdesktop.patch
- - Patch71: kdebase-3.1.3-konsole-double-esc.patch
- - (not applied)Patch72: kdebase-3.1.4-kickermenu.patch
- %{SOURCE21} is not used
- add --with-xinerama and --with-cdparanoia to ./configure

* Thu Feb  5 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.5-3m)
- Fix the problem of default KDE fonts
- - modified default font settings
    (/etc/kderc, kde-momonga-config-3.1.5.tar.bz2)
- - default font is set to Sans
    (which is an alias defined in /etc/fonts/fonts.conf and new patch in
	qt-3.2.3-4m made it enable to choose the alias font in KDE)
- - this fix the problem that default fonts in KDE were not choosen properly
- (--login has been removed from startkde)

* Tue Jan 27 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.1.5-2m)
- add --login to startkde

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Tue Jan  6 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-9m)
- change default value of ResizeMode from Transparent to Opaque because Transparent mode get hung-up
- rex momorel

* Fri Dec 5 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-8m)
- rebuild against for qt-3.2.3

* Thu Nov 20 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-7m)
- rebuild against lm_sensors-2.8.1-1m

* Fri Nov 14 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-6m)
- require and buildprereq libmad, libid3tag

* Fri Nov 7 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-5m)
- rebuild against for qt-3.2.3
- Sorry. wrong momorel.

* Wed Sep 24 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Tue Sep 23 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.3-4m)
- rewrite %%files again

* Tue Sep 23 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.3-3m)
- rewrite %%files

* Sat Sep 20 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.3-2m)
- add with_cups option
- add with_xrandr option
- remove --restore option from ksmserver section of startkde.momonga
- add /usr/bin/filesharelist
- add /usr/share/mimelnk/kdedevice


* Wed Jul 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3 (kde-3.1.3)
  - changes are
    * konqueror: Various fixes to the Info List View.
    * konqueror: Made Ctrl+Enter for new tab in URL combo working again.
    * kio_smb: Huge update for samba 3.0.
    * kicker: "Terminal Sessions" special button supports sessions with dots in filename
    * kicker: "Terminal Sessions" special button with same sort order as the one in Konsole.
    * konsole: Added Ctrl+Shift+N as alternate default shortcut for "New Session".
    * konsole: Fixed fixed-width with proportional-font drawing routine.
    * konsole: Fixed problems with mouse and paste support and detached windows.
    * konsole: Let new color schemes take effect once kcmkonsole tells to do so.
    * konsole: Wheel mouse fixes for Mode_Mouse1000.
- remove kdebase-2.1-konqurer-reload.patch which is not work for Momonga's xfs


* Thu Jul 17 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-4m)
- add fontenginecpp.diff that fixes wrong pointers dereference
  (from kde-core-devel)

* Sun Jul 13 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.2-3m)
- rm /usr/sbin/kappfinder

* Wed Jun 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.2-2m)
-  rebuild against cyrus-sasl2

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- changes are:
     kwin: Alt+Tab works while drag'n'drop (unless the application also grabs keyboard).
     kio_smtp: Doesn't eat 100% CPU when connection breaks and fixed a confusing error message when AUTH failed ("Unknown Command" vs. "Authorization failed")
     kscreensaver: Fixed issue where kdesktop_lock would stay running indefinitely in the background if it could not grab the keyboard/mouse, preventing the screen from being locked manually.
     kscreensaver: Screensavers are now stopped when asking for the password [#56803]
     kio_smb: Several bugfixes for the smbro-ioslave.
     kdesktop: fixed minicli layout problem with Qt 3.1.2
     kdm: fixed incorrect user window width with Qt 3.1.2
     Konqueror: Create DCOP interface for mainwindow when object begins to exist.
     Konqueror: Fixed tab open delays when it can't reach website.
     Konsole: Don't flicker when selecting entire lines.
     Konsole: Crash, selection and sort fixes in schema and session editors.
     Konsole: Fixed mouse-wheel in mouse mode.
     Konsole: Allow programs to resize windows if enabled.
     Konsole: Keep output steady when triple-click selecting.
     Konsole: Added "Print" menu command.
     kicker: Fixed kasbar only drawing last icon from a group.
- delete export _POSIX2_VERSION=199209 and add BuildPreReq: coreutils >= 5.0-1m

* Mon May 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1a-2m)
- invoke make -f admin/Make.common cvs

* Fri Apr 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1a-1m)
  update to 3.1.1a

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-3m)
- remove requires: qt-Xt

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    kcmshell: Restored backwards compatibility wrt KDE 3.0 module names
    klipper: Escape "&" everywhere.
    konsole:
      Removed "get prompt back"-hacks, don't assume emacs key shell bindings.
      Fixed usage of background images with spaces in the filename.
      Profile support fixes (disabled menubar etc.)
      Bookmarks invoked from "Terminal Sessions" kicker menu now set correct title.
      Fixed a problem with the "Linux" font that prevented it from being used with fontconfig.
    kdesktop: Made desktop menus pop up in unused Kicker space work.
    kicker: Fixed empty taskbar sometimes showing scroll buttons.
    konqueror:
      Various startup performance improvements
      Fix crash when sidebar tree expands hidden directory
      Fix crash when config button hidden from config button's menu
      Extensive fixes to Netscape plugins, fixing crashes and plugin support
      Changes to default preview settings, making the defaults safer on various platforms
    Java configuration module: Make it possible to properly configure Java in all cases
    Previews: Fixed a privacy issue where previews of HTML files could access files or hosts on the network.

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1-4m)
  rebuild against openssl 0.9.7a

* Thu Mar  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1-3m)
- [Momonga-devel.ja:01456]

* Sun Feb 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-2m)
- add BuildPreReq: openldap-devel
- add BuildPreReq: lm_sensors

* Sun Feb  2 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up.

* Tue Jan 21 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-4m)
- konsole_grantpty permission changed 0755 --> 4755.

* Mon Jan 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.5a-3m)
- fix required release of kdelibs %%{kderel} -> %%{kdelibsrel}

* Mon Jan 20 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-2m)
- rebuild against kdelibs-3.0.5a-2m

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Wed Oct 23 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-2m)
- fix atokx focus problem (Patch200).
- (See http://www.kde.gr.jp/ml/Kdeveloper/msg02535.html
-      http://www.turbolinux.co.jp/dcforum/DCForumID11/4235.html
-      http://searchqa.justsystem.co.jp/support/faq/latokx1/latxt022.html )

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Wed Aug 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.3-2m)
- rebuild against kdelibs-3.0.3-2m

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Tue Jul 23 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-7m)
- konqueror supports TAB for KDE-3.0.2.
- (Patch28: kdebase-3.0.2-konqtab.patch)

* Sat Jul 20 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (3.0.2-6m)
- Change Requres: libvorbis to libvorbis >= 1.0-2m
- Removed libogg from Requires: since libvorbis impiles libogg

* Sat Jul 20 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-5m)
- change BuildPrereq, openssl-devel >= 1.9.6a to >= 0.9.6a

* Sat Jul 20 2002 kourin <kourin@momonga-linux.org>
- (3.0.2-4m)
- fix BuildPreReq, libvorbis -> libvorbis-devel

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (3.0.2-3m)
- added BuildPreReq: libvorbis >= 1.0-2m

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.
- add startkde-momonga, kde-momonga-config-3.0.2.tar.gz

* Tue Jun 18 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-4k)
- change build options. ( use %{optflags} ).

* Mon Jun 10 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up to 3.0.1.

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-14k)
- mkfontdir tukattendaro korua.

* Mon May 13 2002 Toru Hoshina <t@Kondara.org>
- (3.0.0-12k)
- rebuild against openmotif 2.2.2.

* Tue Apr  9 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-10k)
  Konqueror TAB Support.

* Tue Apr  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-8k)
- applied kdebase-3.0-samba-add-TerminalCode-20020407.diff

* Sun Apr  7 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-6k)
- change startkde.kondara (fix twice startup problem)

* Fri Apr  5 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-4k)
- applied kdebase-3.0.0-genkdmconf.patch

* Thu Apr  4 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release.

* Sun Mar 24 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003004k)
- rebuild.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Sun Nov 25 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-14k)
- for AA etc...

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-12k)
- revised spec file.
- /etc/skel stuff moved.
- require truetype-font-ja for Japanese 'AA' user at this time.

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-10k)
- rebuild against libpng 1.2.0.

* Sat Oct 13 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- revised initial configuratiron.

* Mon Oct  8 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- Sorry, qt.fontguess missed!! oh my god.

* Sun Oct  7 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- ksplash and ksmserver avoid to use i18n() when QT_XFT is enabled,
  however konsole isn't quite fixed.....
- revise kde-kondara-config-2.2.1.tar.bz2 and kdebase-2.2.1-kondara.patch.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Mon Sep 17 2001 Toru Hoshina <t@kondara.org>
- (2.2.0-8k)
- just strip... sorry.

* Mon Sep 10 2001 Toru Hoshina <t@kondara.org>
- (2.2.0-6k)
- BuildRequire lesstif-devel to make sure recompiled versions of Konqueror
  support Netscape plugins.
- Require libogg libvorbis.

* Sat Aug 25 2001 Shingo Akagaki <dora@kondara.org>
- (2.2.0-4k)
- add Buildprereq: cdparanoia-devel

* Tue Aug 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)
- based on 2.2alphae.

* Thu May 31 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Sun Apr 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2alpha1-2k)

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.1-4k)
- rebuild against openssl 0.9.6.

* Tue Mar 20 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-4k]
- revised %files.

* Fri Mar 16 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-2k]

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.1-14k)
- rebuild against QT-2.3.0.

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-12k]
- backport 2.0.1-13k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-10k]
- backport 2.0.1-11k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-11k]
- add new patches.

* Tue Dec 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-9k]
- add kdebase-2.0.1-netscape_bookmark-kondara.patch.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-7k]
- rebuild against qt-2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Fri Dec 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-6k]
- rebuild against egcs++.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-5k]
- rebuild against new environment XFree86-4.0.1-39k.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-4k]
- rebuild against new environment gcc-2.95.2, glibc-2.2.

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-3k]
- rebuild against qt 2.2.2.

* Fri Nov 03 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-1k]
- update kdebase-2.0-kdmconfig.patch
- update kdebase-2.0-kwin-nofreeze-20001024.diff
- add GIF Support switch.
- release.

* Thu Oct 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.3k]
- add kdebase-2.0rc2-kwin-nofreeze-20001011.diff.
- modified Requires,BuildPrereq.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.2k]
- enable debug mode.

* Wed Oct 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- update 2.0rc2.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.

* Thu Sep 28 2000 Kenichi Matsubara <m@kondara.org>
- add kdebase-1.94-kwin-i18n-20000928.diff
- add kdebase-1.94-kwin-nofreeze-20000924.diff

* Mon Sep 25 2000 Toru Hoshina <t@kondara.org>
- fix kdm/configure.in.in, do not need to check /etc/X11/kdm/xdm-config.

* Mon Sep 25 2000 Kenichi Matsubara <m@kondara.org>
- little bugfix spec file for Alpha.

* Sun Sep 24 2000 Kenichi Matsubara <m@kondara.org>
- bugfix spec file [ %files sections.]

* Wed Sep 20 2000 Kenichi Matsubara <m@kondara.org>
- update 1.94.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Fri Jun 23 2000 Kenichi Matsubara <m@kondara.org>
- add kdebase-1.91-without-openssl-kondara.patch.

* Mon Jun 19 2000 Kenichi Matsubara <m@kondara.org>
- update 1.91.

* Tue May 16 2000 Kenichi Matsubara <m@kondara.org>
- initital release Kondara MNU/Linux.
