%global momorel 4

Summary: pthread library helper
Name: libpthread-stubs
Version: 0.3
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
%global xorgurl http://xcb.freedesktop.org/dist/
Source0: %{xorgurl}/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: xorg-x11-proto-devel >= 7.5

#Obsoletes: XFree86-libs, xorg-x11-libs

%description
The X protocol C-language Binding (XCB) is a replacement for Xlib featuring a small footprint, latency hiding, direct access to the protocol, improved threading support, and extensibility.

%prep
%setup -q

%build
%configure \
%make

%install
rm -rf --preserve-root %{buildroot}

%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/pkgconfig/pthread-stubs.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-2m)
- full rebuild for mo7 release

* Thu Jan 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-1m)
- update 0.3

* Sat Nov 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2-1m)
- update 0.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-2m)
- %%NoSource -> NoSource

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-1m)
- initial commit

