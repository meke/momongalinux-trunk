%global momorel 1

%define gupnp_ver 0.18.4
%define glib2_ver 2.33.10

Name:           gupnp-tools
Version:        0.8.5
Release:        %{momorel}m%{?dist}
Summary:	free replacements of Intel UPnP tools

Group:          Applications/System
License:	GPLv2
URL:            http://www.gupnp.org/
#Source0:        http://www.gupnp.org/sites/all/files/sources/%{name}-%{version}.tar.xz
Source0:        http://ftp.gnome.org/pub/GNOME/sources/gupnp-tools/0.8/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel >= %{glib2_ver}
BuildRequires:	gupnp-devel >= %{gupnp_ver}
BuildRequires:  gtk2-devel
BuildRequires:  gnome-icon-theme
BuildRequires:  libuuid-devel
BuildRequires:  gupnp-av-devel

%description
GUPnP Tools are free replacements of Intel UPnP tools that use GUPnP. They
provides the following client and server side tools which enable one to easily
test and debug one's UPnP devices and control points

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/gssdp-discover
%{_bindir}/gupnp-av-cp
%{_bindir}/gupnp-network-light
%{_bindir}/gupnp-universal-cp
%{_bindir}/gupnp-upload

%{_datadir}/applications/gupnp-av-cp.desktop
%{_datadir}/applications/gupnp-network-light.desktop
%{_datadir}/applications/gupnp-universal-cp.desktop

%{_datadir}/%{name}

%changelog
* Wed Jan  2 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.5-1m)
- update 0.8.5

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.4-3m)
- rebuild for gupnp-0.18.

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-2m)
- rebuild for glib 2.33.2

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.4-1m)
- update 0.8.4

* Thu Sep 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-2m)
- rebuild against gupnp-0.18

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.3-1m)
- update 0.8.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-4m)
- rebuild for new GCC 4.6

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-3m)
- rebuild against gupnp-0.15.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-2m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- initial build

