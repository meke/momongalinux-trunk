%global momorel 2

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

%define packagename BytecodeAssembler

Name:           python-peak-util-assembler
Version:        0.6
Release:        %{momorel}m%{?dist}
Summary:        Generate Python code objects by "assembling" bytecode

Group:          Development/Languages
License:        "PSF" or "ZPL"
URL:            http://pypi.python.org/pypi/BytecodeAssembler
Source0:        http://pypi.python.org/packages/source/B/%{packagename}/%{packagename}-%{version}.zip
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel
BuildRequires:  python-nose

Requires:       python-decoratortools >= 1.2
Requires:       python-peak-util-symbols >= 1.0

%description
peak.util.assembler is a simple bytecode assembler module that handles most
low-level bytecode generation details like jump offsets, stack size tracking,
line number table generation, constant and variable name index tracking, etc.
That way, you can focus your attention on the desired semantics of your
bytecode instead of on these mechanical issues.

In addition to a low-level opcode-oriented API for directly generating specific
Python bytecodes, this module also offers an extensible mini-AST framework for
generating code from high-level specifications.  This framework does most of
the work needed to transform tree-like structures into linear bytecode
instructions, and includes the ability to do compile-time constant folding.

%prep
%setup -q -n %{packagename}-%{version}

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%check
nosetests

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.txt
%{python_sitelib}/peak/util/*
%{python_sitelib}/%{packagename}-%{version}-py%{pyver}.egg-info
%{python_sitelib}/%{packagename}-%{version}-py%{pyver}-nspkg.pth

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6-2m)
- rebuild against python-2.7

* Thu Apr 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-1m)
- version 0.6
- update to enable build new python-peak-rules

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-3m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-2m)
- delete duplicate directory

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-1m)
- import from Rawhide

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.5-2
- Rebuild for Python 2.6

* Sun Aug  3 2008 Luke Macken <lmacken@redhat.com> - 0.5-1
- Initial package for Fedora
