%global momorel 2
%global fedora 19

Name: elfutils
Summary: A collection of utilities and DSOs to handle compiled objects
Version: 0.158
#%global baserelease 2
URL: https://fedorahosted.org/elfutils/
%global source_url https://fedorahosted.org/releases/e/l/elfutils/%{version}/
License: GPLv3+ and (GPLv2+ or LGPLv3+)
Group: Development/Tools

%if %{?_with_compat:1}%{!?_with_compat:0}
%global compat 1
%else
%global compat 0
%endif

%global portability             %{compat}
%global scanf_has_m             !%{compat}
%global separate_devel_static   1
%global use_zlib                0
%global use_xz                  0

%if 0%{?rhel}
%global portability             (%rhel < 6)
%global scanf_has_m             (%rhel >= 6)
%global separate_devel_static   (%rhel >= 6)
%global use_zlib                (%rhel >= 5)
%global use_xz                  (%rhel >= 5)
%endif
%if 0%{?fedora}
%global portability             (%fedora < 9)
%global scanf_has_m             (%fedora >= 8)
%global separate_devel_static   (%fedora >= 7)
%global use_zlib                (%fedora >= 5)
%global use_xz                  (%fedora >= 10)
%endif

%if %{compat} || %{!?rhel:6}%{?rhel} < 6
%global nocheck true
%else
%global nocheck false
%endif

%global depsuffix %{?_isa}%{!?_isa:-%{_arch}}

Source: %{?source_url}%{name}-%{version}.tar.bz2
NoSource: 0
Patch1: %{?source_url}elfutils-robustify.patch
Patch2: %{?source_url}elfutils-portability.patch
Patch3: elfutils-0.158-mod-e_type.patch
Patch4: elfutils-0.158-CVE-2014-0172.patch

%if !%{compat}
Release: %{momorel}m%{?dist}
%else
Release: %{momorel}m%{?dist}
%endif

Requires: elfutils-libelf%{depsuffix} = %{version}-%{release}
Requires: elfutils-libs%{depsuffix} = %{version}-%{release}

%if %{!?rhel:6}%{?rhel} < 6 || %{!?fedora:9}%{?fedora} < 10
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%endif

BuildRequires: gettext
BuildRequires: bison >= 1.875
BuildRequires: flex >= 2.5.4a
BuildRequires: bzip2
%if !%{compat}
BuildRequires: gcc >= 3.4
# Need <byteswap.h> that gives unsigned bswap_16 etc.
BuildRequires: glibc-headers >= 2.3.4-11
%else
BuildRequires: gcc >= 3.2
%endif

%if %{use_zlib}
BuildRequires: zlib-devel >= 1.2.2.3
BuildRequires: bzip2-devel
%endif

%if %{use_xz}
BuildRequires: xz-devel
%endif

%global _gnu %{nil}
%global _program_prefix eu-

%description
Elfutils is a collection of utilities, including ld (a linker),
nm (for listing symbols from object files), size (for listing the
section sizes of an object or archive file), strip (for discarding
symbols), readelf (to see the raw ELF file structures), and elflint
(to check for well-formed ELF files).


%package libs
Summary: Libraries to handle compiled objects
Group: Development/Tools
License: GPLv2+ or LGPLv3+
%if 0%{!?_isa:1}
Provides: elfutils-libs%{depsuffix} = %{version}-%{release}
%endif
Requires: elfutils-libelf%{depsuffix} = %{version}-%{release}

%description libs
The elfutils-libs package contains libraries which implement DWARF, ELF,
and machine-specific ELF handling.  These libraries are used by the programs
in the elfutils package.  The elfutils-devel package enables building
other programs using these libraries.

%package devel
Summary: Development libraries to handle compiled objects
Group: Development/Tools
License: GPLv2+ or LGPLv3+
%if 0%{!?_isa:1}
Provides: elfutils-devel%{depsuffix} = %{version}-%{release}
%endif
Requires: elfutils-libs%{depsuffix} = %{version}-%{release}
Requires: elfutils-libelf-devel%{depsuffix} = %{version}-%{release}
%if !%{separate_devel_static}
Requires: elfutils-devel-static%{depsuffix} = %{version}-%{release}
%endif

%description devel
The elfutils-devel package contains the libraries to create
applications for handling compiled objects.  libebl provides some
higher-level ELF access functionality.  libdw provides access to
the DWARF debugging information.  libasm provides a programmable
assembler interface.

%package devel-static
Summary: Static archives to handle compiled objects
Group: Development/Tools
License: GPLv2+ or LGPLv3+
%if 0%{!?_isa:1}
Provides: elfutils-devel-static%{depsuffix} = %{version}-%{release}
%endif
Requires: elfutils-devel%{depsuffix} = %{version}-%{release}
Requires: elfutils-libelf-devel-static%{depsuffix} = %{version}-%{release}

%description devel-static
The elfutils-devel-static package contains the static archives
with the code to handle compiled objects.

%package libelf
Summary: Library to read and write ELF files
Group: Development/Tools
License: GPLv2+ or LGPLv3+
%if 0%{!?_isa:1}
Provides: elfutils-libelf%{depsuffix} = %{version}-%{release}
%endif
Obsoletes: libelf <= 0.8.2-2

%description libelf
The elfutils-libelf package provides a DSO which allows reading and
writing ELF files on a high level.  Third party programs depend on
this package to read internals of ELF files.  The programs of the
elfutils package use it also to generate new ELF files.

%package libelf-devel
Summary: Development support for libelf
Group: Development/Tools
License: GPLv2+ or LGPLv3+
%if 0%{!?_isa:1}
Provides: elfutils-libelf-devel%{depsuffix} = %{version}-%{release}
%endif
Requires: elfutils-libelf%{depsuffix} = %{version}-%{release}
%if !%{separate_devel_static}
Requires: elfutils-libelf-devel-static%{depsuffix} = %{version}-%{release}
%endif
Obsoletes: libelf-devel <= 0.8.2-2

%description libelf-devel
The elfutils-libelf-devel package contains the libraries to create
applications for handling compiled objects.  libelf allows you to
access the internals of the ELF object file format, so you can see the
different sections of an ELF file.

%package libelf-devel-static
Summary: Static archive of libelf
Group: Development/Tools
License: GPLv2+ or LGPLv3+
%if 0%{!?_isa:1}
Provides: elfutils-libelf-devel-static%{depsuffix} = %{version}-%{release}
%endif
Requires: elfutils-libelf-devel%{depsuffix} = %{version}-%{release}

%description libelf-devel-static
The elfutils-libelf-static package contains the static archive
for libelf.

%prep
%setup -q

: 'compat=%compat'
: 'portability=%portability'
: 'separate_devel_static=%separate_devel_static'
: 'scanf_has_m=%scanf_has_m'

%patch1 -p1 -b .robustify

%if %{portability}
%patch2 -p1 -b .portability
sleep 1
find . \( -name Makefile.in -o -name aclocal.m4 \) -print | xargs touch
sleep 1
find . \( -name configure -o -name config.h.in \) -print | xargs touch
%else
%if !%{scanf_has_m}
sed -i.scanf-m -e 's/%m/%a/g' src/addr2line.c tests/line2addr.c
%endif
%endif

%patch3 -p1 -b .e_type
%patch4 -p1 -b .cve-2014-0172~

find . -name \*.sh ! -perm -0100 -print | xargs chmod +x

%build
# Momonga linux requires "-g" option for some test cases in make check stage
RPM_OPT_FLAGS="$RPM_OPT_FLAGS -g"

# Remove -Wall from default flags.  The makefiles enable enough warnings
# themselves, and they use -Werror.  Appending -Wall defeats the cases where
# the makefiles disable some specific warnings for specific code.
# Also remove -Werror=format-security which doesn't work without
# -Wformat (enabled by -Wall). We enable -Wformat explicitly for some
# files later.
RPM_OPT_FLAGS=${RPM_OPT_FLAGS/-Wall/}
RPM_OPT_FLAGS=${RPM_OPT_FLAGS/-Werror=format-security/}

%if %{compat}
# Some older glibc headers can run afoul of -Werror all by themselves.
# Disabling the fancy inlines avoids those problems.
RPM_OPT_FLAGS="$RPM_OPT_FLAGS -D__NO_INLINE__"
COMPAT_CONFIG_FLAGS="--disable-werror"
%else
COMPAT_CONFIG_FLAGS=""
%endif

trap 'cat config.log' EXIT
%configure --enable-dwz $COMPAT_CONFIG_FLAGS CFLAGS="$RPM_OPT_FLAGS -fexceptions"
trap '' EXIT
make -s %{?_smp_mflags}

%install
rm -rf ${RPM_BUILD_ROOT}
make -s install DESTDIR=${RPM_BUILD_ROOT}

chmod +x ${RPM_BUILD_ROOT}%{_prefix}/%{_lib}/lib*.so*
chmod +x ${RPM_BUILD_ROOT}%{_prefix}/%{_lib}/elfutils/lib*.so*

# XXX Nuke unpackaged files
(cd ${RPM_BUILD_ROOT}
 rm -f .%{_bindir}/eu-ld
)

%find_lang %{name}

%check
make -s %{?_smp_mflags} check || (cat tests/test-suite.log; %{nocheck})

%clean
rm -rf ${RPM_BUILD_ROOT}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post libelf -p /sbin/ldconfig

%postun libelf -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING COPYING-GPLV2 COPYING-LGPLV3 README TODO CONTRIBUTING
%{_bindir}/eu-addr2line
%{_bindir}/eu-ar
%{_bindir}/eu-elfcmp
%{_bindir}/eu-elflint
%{_bindir}/eu-findtextrel
%{_bindir}/eu-nm
%{_bindir}/eu-objdump
%{_bindir}/eu-ranlib
%{_bindir}/eu-readelf
%{_bindir}/eu-size
%{_bindir}/eu-stack
%{_bindir}/eu-strings
%{_bindir}/eu-strip
#%%{_bindir}/eu-ld
%{_bindir}/eu-unstrip
%{_bindir}/eu-make-debug-archive

%files libs
%defattr(-,root,root)
%{_libdir}/libasm-%{version}.so
%{_libdir}/libasm.so.*
%{_libdir}/libdw-%{version}.so
%{_libdir}/libdw.so.*
%dir %{_libdir}/elfutils
%{_libdir}/elfutils/lib*.so

%files devel
%defattr(-,root,root)
%{_includedir}/dwarf.h
%dir %{_includedir}/elfutils
%{_includedir}/elfutils/elf-knowledge.h
%{_includedir}/elfutils/libasm.h
%{_includedir}/elfutils/libebl.h
%{_includedir}/elfutils/libdw.h
%{_includedir}/elfutils/libdwfl.h
%{_includedir}/elfutils/version.h
%{_libdir}/libebl.a
%{_libdir}/libasm.so
%{_libdir}/libdw.so

%files devel-static
%defattr(-,root,root)
%{_libdir}/libasm.a
%{_libdir}/libdw.a

%files -f %{name}.lang libelf
%defattr(-,root,root)
%{_libdir}/libelf-%{version}.so
%{_libdir}/libelf.so.*

%files libelf-devel
%defattr(-,root,root)
%{_includedir}/libelf.h
%{_includedir}/gelf.h
%{_includedir}/nlist.h
%{_libdir}/libelf.so

%files libelf-devel-static
%defattr(-,root,root)
%{_libdir}/libelf.a

%changelog
* Sat Apr 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.158-2m)
- [SECURITY] CVE-2014-0172

* Sat Mar 29 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.158-1m)
- update to 0.158

* Tue Nov 19 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.157-1m)
- update to 0.157

* Thu Jul 18 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.155-1m)
- update to 0.155
- import two patches from fedora
- revise spec

* Sat Jun 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.154-1m)
- update to 0.154
- reimport from fedora
- fix test suite error

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.152-2m)
- rebuild for new GCC 4.6

* Wed Feb 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.152-1m)
- update 0.152

* Fri Feb 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.151-1m)
- update 0.151

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.148-4m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.148-3m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.148-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.148-1m)
- update to 0.148

* Sat May  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.146-1m)
- update to 0.146
- merge changes from fedora (elfutils-0_146-1_fc13)

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.145-1m)
- update to 0.145

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.144-1m)
- update to 0.144 with glibc212 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.142-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.142-1m)
- update to 0.142, which includes STB_GNU_UNIQUE support

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.141-1m)
- sync with Fedora 11 (0.141-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.137-2m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.137-1m)
- sync with Rawhide (0.137-3)

* Fri Apr 18 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.133-1m)
- update 0.133

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.131-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.131-3m)
- %%NoSource -> NoSource

* Sat Nov 24 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.131-2m)
- added elfutils-0.131-gcc43.patch

* Tue Nov 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.131-1m)
- updated 0.131

* Wed Oct 31 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.130-3m)
- updated a patch for gcc42 as elfutils-0.130-gcc42.patch
- removed unused patches

* Wed Oct 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.130-2m)
- add glibc-2.7 patch

* Thu Oct 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.130-1m)
- update 0.130

* Fri Sep  7 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.129-1m)
- update 0.129
-- new program; unstrip 
- sync with fc-devel (elfutils-0_129-2_fc8)
-- add two sub-packages; devel-static and libelf-devel-static
-- Replace Conflicts

* Sat May 12 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.127-2m)
- revised CFLAGS for x86_64
 
* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.127-1m)
- update 0.127

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.124-5m)
- -fgnu89-inline use gcc-4.2 only

* Thu Mar 15 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.124-4m)
- remove -fgnu89-inline

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.124-3m)
- fix gcc-4.2.
-- add -fgnu89-inline
-- elfutils-0.124-gcc42.patch

* Sun Oct 15 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.124-2m)
- replaced "ln" by "cp"

* Wed Oct 11 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.124-1m)
- version up (sync with elfutils-0_124-1_fc6)

* Fri May 19 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.119-1m)
- version up

* Tue Nov 30 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.97-1m)
- version up for gcc34
- cancel Patch0 (please revive if needed)

* Sun Jul 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.75-3m)
- fix unresolved symbols

* Sun Jul 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.75-2m)
- fix unresolved symbols

* Tue Apr 15 2003 Kenta MURATA <muraken2@nifty.com>
- (0.75-1m)
- first specfile from rawhide.
-* Mon Feb 24 2003 Elliot Lee <sopwith@redhat.com>
-- debuginfo rebuild
-
-* Thu Feb 20 2003 Jeff Johnson <jbj@redhat.com> 0.76-2
-- use the correct way of identifying the section via the sh_info link.
-
-* Sat Feb 15 2003 Jakub Jelinek <jakub@redhat.com> 0.75-2
-- update to 0.75 (eu-strip -g fix)
-
-* Tue Feb 11 2003 Jakub Jelinek <jakub@redhat.com> 0.74-2
-- update to 0.74 (fix for writing with some non-dirty sections)
-
-* Thu Feb  6 2003 Jeff Johnson <jbj@redhat.com> 0.73-3
-- another -0.73 update (with sparc fixes).
-- do "make check" in %%check, not %%install, section.
-
-* Mon Jan 27 2003 Jeff Johnson <jbj@redhat.com> 0.73-2
-- update to 0.73 (with s390 fixes).
-
-* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
-- rebuilt
-
-* Wed Jan 22 2003 Jakub Jelinek <jakub@redhat.com> 0.72-4
-- fix arguments to gelf_getsymshndx and elf_getshstrndx
-- fix other warnings
-- reenable checks on s390x
-
-* Sat Jan 11 2003 Karsten Hopp <karsten@redhat.de> 0.72-3
-- temporarily disable checks on s390x, until someone has
-  time to look at it
-
-* Thu Dec 12 2002 Jakub Jelinek <jakub@redhat.com> 0.72-2
-- update to 0.72
-
-* Wed Dec 11 2002 Jakub Jelinek <jakub@redhat.com> 0.71-2
-- update to 0.71
-
-* Wed Dec 11 2002 Jeff Johnson <jbj@redhat.com> 0.69-4
-- update to 0.69.
-- add "make check" and segfault avoidance patch.
-- elfutils-libelf needs to run ldconfig.
-
-* Tue Dec 10 2002 Jeff Johnson <jbj@redhat.com> 0.68-2
-- update to 0.68.
-
-* Fri Dec  6 2002 Jeff Johnson <jbj@redhat.com> 0.67-2
-- update to 0.67.
-
-* Tue Dec  3 2002 Jeff Johnson <jbj@redhat.com> 0.65-2
-- update to 0.65.
-
-* Mon Dec  2 2002 Jeff Johnson <jbj@redhat.com> 0.64-2
-- update to 0.64.
-
-* Sun Dec 1 2002 Ulrich Drepper <drepper@redhat.com> 0.64
-- split packages further into elfutils-libelf
-
-* Sat Nov 30 2002 Jeff Johnson <jbj@redhat.com> 0.63-2
-- update to 0.63.
-
-* Fri Nov 29 2002 Ulrich Drepper <drepper@redhat.com> 0.62
-- Adjust for dropping libtool
-
-* Sun Nov 24 2002 Jeff Johnson <jbj@redhat.com> 0.59-2
-- update to 0.59
-
-* Thu Nov 14 2002 Jeff Johnson <jbj@redhat.com> 0.56-2
-- update to 0.56
-
-* Thu Nov  7 2002 Jeff Johnson <jbj@redhat.com> 0.54-2
-- update to 0.54
-
-* Sun Oct 27 2002 Jeff Johnson <jbj@redhat.com> 0.53-2
-- update to 0.53
-- drop x86_64 hack, ICE fixed in gcc-3.2-11.
-
-* Sat Oct 26 2002 Jeff Johnson <jbj@redhat.com> 0.52-3
-- get beehive to punch a rhpkg generated package.
-
-* Wed Oct 23 2002 Jeff Johnson <jbj@redhat.com> 0.52-2
-- build in 8.0.1.
-- x86_64: avoid gcc-3.2 ICE on x86_64 for now.
-
-* Tue Oct 22 2002 Ulrich Drepper <drepper@redhat.com> 0.52
-- Add libelf-devel to conflicts for elfutils-devel
-
-* Mon Oct 21 2002 Ulrich Drepper <drepper@redhat.com> 0.50
-- Split into runtime and devel package
-
-* Fri Oct 18 2002 Ulrich Drepper <drepper@redhat.com> 0.49
-- integrate into official sources
-
-* Wed Oct 16 2002 Jeff Johnson <jbj@redhat.com> 0.46-1
-- Swaddle.
