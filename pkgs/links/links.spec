%global momorel 2
%global srcname %{name}-%{version}

Name:           links
Version:        2.4
Release:        %{momorel}m%{?dist}
Summary:        Web browser running in both graphics and text mode

Group:          Applications/Internet
License:        GPLv2+
URL:            http://links.twibright.com/
Source0:        http://links.twibright.com/download/%{srcname}.tar.bz2
NoSource:       0
Source1:        links.desktop
# Not sent upstream, since they would probably break my nose for this
Patch2:         links-2.4-google.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  bzip2-devel
BuildRequires:  zlib-devel
BuildRequires:  desktop-file-utils
BuildRequires:  gpm-devel
BuildRequires:  libX11-devel
BuildRequires:  autoconf automake
BuildRequires:  nss-devel
BuildRequires:  nss_compat_ossl-devel

Requires(preun): chkconfig
Requires(post): chkconfig
Requires(postun): chkconfig
Requires(post): coreutils
Requires(postun): coreutils

Provides:       webclient
Provides:       text-www-browser


%description
Links is a web browser capable of running in either graphics or text mode.
It provides a pull-down menu system, renders complex pages, has partial HTML
4.0 support (including tables, frames and support for multiple character sets
and UTF-8), supports color and monochrome terminals and allows horizontal
scrolling.


%prep
%setup -q -n %{srcname}
%patch2 -p1 -b .google


%build
iconv -f ISO-8859-1 -t UTF-8 AUTHORS >converted.AUTHORS
touch -r AUTHORS converted.AUTHORS
mv converted.AUTHORS AUTHORS

%configure --enable-graphics
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=${RPM_BUILD_ROOT}
mv $RPM_BUILD_ROOT/%{_bindir}/links $RPM_BUILD_ROOT/%{_bindir}/links2
mv $RPM_BUILD_ROOT/%{_mandir}/man1/links.1 $RPM_BUILD_ROOT/%{_mandir}/man1/links2.1
desktop-file-install --dir=${RPM_BUILD_ROOT}%{_datadir}/applications %{SOURCE1}
install -D -p Links_logo.png $RPM_BUILD_ROOT/%{_datadir}/pixmaps/links.png


%clean
rm -rf $RPM_BUILD_ROOT


%postun
[ $1 = 0 ] && exit 0
[ $(readlink %{_sysconfdir}/alternatives/links) = %{_bindir}/links2 ] &&
        %{_sbindir}/alternatives --set links %{_bindir}/links2
exit 0


%preun
[ $1 = 0 ] || exit 0
%{_sbindir}/alternatives --remove links %{_bindir}/links2


%post
%{_sbindir}/alternatives \
        --install %{_bindir}/links links %{_bindir}/links2 80 \
        --slave %{_mandir}/man1/links.1.gz links-man %{_mandir}/man1/links2.1.gz
[ $(readlink %{_sysconfdir}/alternatives/links) = %{_bindir}/links2 ] &&
        %{_sbindir}/alternatives --set links %{_bindir}/links2
exit 0


%files
%defattr(-,root,root,-)
%doc doc/* AUTHORS BUGS KEYS TODO README COPYING
%{_bindir}/links2
%{_mandir}/man1/links2.1*
%{_datadir}/pixmaps/links.png
%{_datadir}/applications/links.desktop


%changelog
* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-2m)
- rebuild against libtiff-4.0.1

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Fri Aug 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3-1m)
- update 2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-0.1.2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-0.1.1m)
- import from Fedora 13 and update to 2.3pre1

* Fri Nov 27 2009 Ondrej Vasik <ovasik@redhat.com> - 1:2.2-12
- add Requires(post) and Requires(postun) for coreutils (readlink)
  (#540434)
* Sat Oct 17 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.2-11
- Do not display textareas hidden by CSS

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1:2.2-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Apr 21 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.2-9
- Add epoch to beat elinks obsoletes

* Mon Apr 13 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.2-8
- Enable SSL certificates verification

* Sun Apr 12 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.2-7
- removing unnecessary BuildConflicts

* Fri Apr 10 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.2-6
- Cosmetic fixes
- Ship license

* Fri Apr 03 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.2-5
- Add SSL/TLS support by the means of NSS
- Add web browser provides

* Thu Apr 02 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.2-4
- Do not silence make
- Use bundled icon, place it in pixmaps directory
- Adjust summary, description and style
- Remove useless configure parameters
- Include X11 support
- Adjust menu entry text
- Add alternatives support

* Mon Nov 10 2008 johnhford@gmail.com - 2.2-3
- Do not mutilate timestamps on AUTHORS
- Removed vendor from desktop-file-install
- No longer gzipping manpage

* Mon Nov 10 2008 John Ford <johnhford.gmail@com> 2.2-2
- Modified %%build to ensure shared libraries are used
- Added missing requirements (desktop-file-utils)
* Sat Nov 08 2008 John Ford <johnhford.gmail@com> 2.2-1
- Initial Submission

