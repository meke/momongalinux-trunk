%global momorel 7

Summary: A MathML rendering library
Name: gtkmathview
Version: 0.8.0
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPLv3+
Source: http://helm.cs.unibo.it/mml-widget/sources/gtkmathview-%{version}.tar.gz
NoSource: 0
URL: http://helm.cs.unibo.it/mml-widget/
BuildRequires: glib2-devel >= 2.2
BuildRequires: gtk2-devel >= 2.2
BuildRequires: libxml2-devel >= 2.6.7
BuildRequires: libxslt >= 1.0.32
BuildRequires: popt >= 1.7 
BuildRequires: popt-devel >= 1.7 
BuildRequires: t1lib-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0: gtkmathview-0.8.0-gcc43.patch
Patch1: gtkmathview-0.8.0-includes.patch
Patch2: gtkmathview-0.8.0-gcc-4.4.4.patch

%description
GtkMathView is a C++ rendering engine for MathML documents. 
It provides an interactive view that can be used for browsing 
and editing MathML markup.

%package devel
Summary: Support files necessary to compile applications using gtkmathview
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel >= 2.2.1
Requires: gtk2-devel >= 2.2.1
Requires: libxml2-devel >= 2.6.7
Requires: popt >= 1.7.0 
Requires: pkgconfig

%description devel
Libraries, headers, and support files needed for using gtkmathview.

%prep
%setup -q
%patch0 -p1 -b .gcc43
%patch1 -p1 -b .includes
%patch2 -p1 -b .gcc-4.4.4

%build
automake-1.9
%configure --disable-static
%make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
rm -f $RPM_BUILD_ROOT/%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT/%{_mandir}/man1/mathml2ps.1
rm -f $RPM_BUILD_ROOT/%{_mandir}/man1/mathmlviewer.1

%files
%defattr(-,root,root)
%doc COPYING README AUTHORS CONTRIBUTORS BUGS LICENSE
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_sysconfdir}/gtkmathview/
%{_datadir}/gtkmathview/

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/mathview-core.pc
%{_libdir}/pkgconfig/mathview-frontend-libxml2.pc
%{_libdir}/pkgconfig/gtkmathview-custom-reader.pc
%{_libdir}/pkgconfig/gtkmathview-libxml2-reader.pc
%{_libdir}/pkgconfig/gtkmathview-libxml2.pc
%{_libdir}/pkgconfig/mathview-frontend-libxml2-reader.pc
%{_libdir}/pkgconfig/mathview-frontend-custom-reader.pc
%{_libdir}/pkgconfig/mathview-backend-svg.pc
%{_libdir}/pkgconfig/mathview-backend-gtk.pc
%{_libdir}/pkgconfig/mathview-backend-ps.pc
%{_includedir}/gtkmathview

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-7m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-4m)
- full rebuild for mo7 release

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-3m)
- build fix gcc-4.4.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Mon Jan 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.8-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.8-2m)
- rebuild against rpm-4.6

* Sat Apr 05 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.8-1m)
- import to Momonga (from Fedora, based on 0.7.6-7)
- add patch for gcc43, generated by gen43patch(v1)

* Fri Mar 14 2008 Doug chapman <doug.chapman@hp.com> - 0.7.6-7
- fix GCC 4.3 build errors (BZ 434485)
- require popt-devel for build (BZ 426136)

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.7.6-6
- Autorebuild for GCC 4.3

* Thu Oct 12 2006 Marc Maurer <uwog@abisource.com> 0.7.6-5.fc6
- Add pkgconfig to the -devel requires (bug 206451)

* Mon Sep 16 2006 Marc Maurer <uwog@abisource.com> 0.7.6-4.fc6
- Rebuild for FC 6

* Thu Feb 16 2006 Marc Maurer <uwog@abisource.com> 0.7.6-3.fc5
- Rebuild for Fedora Extras 5

* Sun Feb 05 2006 Marc Maurer <uwog@abisource.com> - 0.7.6-2.fc5
- Use %%{?dist} in the release name
- Omit static libs (part of bug 171971)
- s/gtkmathview/%%{name} (part of bug 171971)

* Sun Dec 11 2005 Marc Maurer <uwog@abisource.com> - 0.7.6-1
- Update to 0.7.6

* Sun Sep 25 2005 Marc Maurer <uwog@abisource.com> - 0.7.5-1
- Update to 0.7.5

* Mon Sep 12 2005 Marc Maurer <uwog@abisource.com> - 0.7.4-1
- Update to 0.7.4

* Tue Aug 30 2005 Marc Maurer <uwog@abisource.com> - 0.7.3-5
- Drop more unneeded Requires

* Tue Aug 30 2005 Marc Maurer <uwog@abisource.com> - 0.7.3-4
- Drop the explicit Requires

* Mon Aug 29 2005 Marc Maurer <uwog@abisource.com> - 0.7.3-3
- Use smaller lines in the Description field
- Remove the --disable-gmetadom and --without-t1lib flags
- Add a '/' to directories in the files section
- Remove the mathmlviewer man page

* Tue Aug 23 2005 Marc Maurer <uwog@abisource.com> - 0.7.3-2
- Add the proper Requires and Buildrequires
- Make the description field more descriptive
- Add CONTRIBUTORS BUGS LICENSE to the doc section
- Disable gmetadom and t1lib
- Remove the mathml2ps man page

* Sun Aug 14 2005 Marc Maurer <uwog@abisource.com> - 0.7.3-1
- Initial version
