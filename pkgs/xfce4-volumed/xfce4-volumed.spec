%global momorel 1

%global xfce4ver 4.8.0
%global major 0.1

Name:           xfce4-volumed
Version:        0.1.13
Release:        %{momorel}m%{?dist}
Summary:        Daemon to add additional functionality to the volume keys of the keyboard

Group:          User Interface/Desktops
License:        GPLv3+
URL:            https://launchpad.net/xfce4-volumed
Source0:        http://archive.xfce.org/src/apps/%{name}/0.1/%{name}-%{version}.tar.bz2
NoSource:       0
#Patch0:		xfce4-volumed-0.1.8-libnotify-0.7.2.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  desktop-file-utils
BuildRequires:  gstreamer-plugins-base-devel
BuildRequires:  keybinder-devel
BuildRequires:  libnotify-devel
BuildRequires:  xcb-util-devel >= 0.3.3
BuildRequires:  xfconf-devel >= %{xfce4ver}
Requires:       xfce4-mixer >= 4.6.1

%description
The xfce4-volumed adds additional functionality to the volume up/down and mute
keys of the keyboard. It makes the keys work without configuration and uses 
the XFCE 4 mixer's defined card and track for choosing which track to act on. 
The volume level is shown in a notification.

%prep
%setup -q
#%%patch0 -p1 -b .libnotify


%build
%configure
make %{?_smp_mflags} CFLAGS='%{optflags}'


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
desktop-file-install \
  --add-category="Utility" \
  --dir=$RPM_BUILD_ROOT/%{_datadir}/applications \
  $RPM_BUILD_ROOT/%{_sysconfdir}/xdg/autostart/%{name}.desktop
# one launcher is enough, we don't want to have a daemon in the menu
rm -rf $RPM_BUILD_ROOT/%{_datadir}/applications/


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README THANKS
%config %{_sysconfdir}/xdg/autostart/%{name}.desktop
%{_bindir}/%{name}


%changelog
* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.13-1m)
- update
- rebuild against xfce4-4.8
- add BuildRequires:  keybinder-devel

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.8-5m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.8-2m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.8-1m)
- import from Fedora to Momonga

* Tue Nov 17 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.8-1
- Update to 0.1.8

* Sun Nov 01 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.7-1
- Update to 0.1.7

* Wed Oct 28 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.5-1
- Update to 0.1.5

* Sat Sep 05 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.4-1
- Initial Fedora package
