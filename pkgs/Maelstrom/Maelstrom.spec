%global momorel 18

Summary:   Maelstrom
Name:      Maelstrom
Version:   3.0.6
Release: %{momorel}m%{?dist}
License: GPL
Group:     Amusements/Games
Source0:   http://www.devolution.com/~slouken/%{name}/src/%{name}-%{version}.tar.gz
Source1: Maelstrom.as
Patch0: dassa.patch
Patch1: unresolved.resolve.patch
Patch2: Maelstrom-3.0.5-installdir.patch
Patch3: Maelstrom-3.0.6-gcc34.patch
Patch4: Maelstrom-3.0.6-gcc4.patch
URL:       http://www.devolution.com/~slouken/Maelstrom/
BuildRequires: gcc-c++ >= 3.4.1-1m
BuildRequires: autoconf, automake
BuildRequires: SDL-devel >= 1.2.7-11m, SDL_net-devel
BuildRequires: libX11-devel, libXext-devel
BuildRequires: audiofile-devel >= 0.2.1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Maelstrom is a rockin' asteroids game ported from the Macintosh
Originally written by Andrew Welch of Ambrosia Software, and ported
to UNIX and then SDL by Sam Lantinga <slouken@devolution.com>
#'

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1 -b .gcc4

touch NEWS AUTHORS ChangeLog

%build
libtoolize --copy --force
aclocal-1.4
autoupdate-2.13
automake-1.4 -a -c 
autoconf
%configure --program-transform-name=""

make GAME_INSTALLDIR=/usr/share/Maelsrtom AUTOCONF=autoconf AUTOMAKE=automake-1.4 ACLOCAL=aclocal-1.4 AUTOHEADER=autoheader-2.13

%install
rm -rf %{buildroot}
%makeinstall

mkdir -p %{buildroot}%{_datadir}/afterstep/start/Games/
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/afterstep/start/Games/

mkdir -p %{buildroot}%{_datadir}/applications
cat > %{buildroot}%{_datadir}/applications/maelstrom.desktop <<__EOF__
[Desktop Entry]
Encoding=UTF-8
Categories=Application;Game;ArcadeGame;
Name=Maelstrom
Comment=Maelstrom
Exec=/usr/bin/Maelstrom
Icon=/usr/share/Maelstrom/icon.xpm
Terminal=false
Type=Application
__EOF__

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING* CREDITS README* Changelog Docs
%{_bindir}/Maelstrom
%{_bindir}/Maelstrom-netd
%{_datadir}/Maelstrom
%{_datadir}/afterstep/start/Games/Maelstrom.as
%{_datadir}/applications/maelstrom.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.6-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.6-17m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (3.0.6-16m)
- No NoSource

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.6-15m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.6-14m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.6-13m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.6-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.6-11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.6-10m)
- rebuild against gcc43

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.6-9m)
- use autoconf
- add gcc4 patch
- Patch4: Maelstrom-3.0.6-gcc4.patch

* Mon Nov 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.6-8m)
- remove %%{_datadir}/applnk/Games/Arcade/maelstrom.desktop
- revise %%{_datadir}/applications/maelstrom.desktop

* Wed Mar 23 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.6-7m)
- revised desktop file

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.6-6m)
- rebuild against SDL-1.2.7-11m

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.6-5m)
- rebuild against SDL-1.2.7-9m

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.0.6-4m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++
- add gcc34 patch

* Sun Aug 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.6-3m)
- rebuild against DirectFB-0.9.21

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (3.0.6-2m)
- revised spec for enabling rpm 4.2.

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.0.6-1m)
  update to 3.0.6

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.5-11m)
- rebuild against SDL-1.2.4-3m, SDL_net-1.2.4-3m

* Fri Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (3.0.5-10k)
- enable to build

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (3.0.5-8k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Thu Feb  7 2002 Shingo Akagaki <dora@kondara.org>
- (3.0.5-6k)
- add desktop links
- remove post script

* Thu Jan 10 2002 Shingo Akagaki <dora@kondara.org>
- (3.0.5-4k)
- automake autoconf 1.5

* Thu Oct 25 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.5-2k)
- update to 3.0.5

* Wed Apr 18 2001 Kenichi Matsubara <m@digitalfactory.co.jp>
- (3.0.1-16k)
- rebuild against SDL-1.2.0.

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (3.0.1-15k)
- rebuild against audiofile-0.2.1.

* Thu Feb  1 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.1-13k)
- added /usr/share/afterstep/start/Games/Maelstrom.as to %files section (#842)

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.1-11k)
- added Maelstrom.as for afterstep

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- (3.0.1-9k)
- rebuild against gcc 2.96.

* Thu Dec 28 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.1-7k)
- build against audiofile-0.2.0

* Fri Dec  1 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.1-5k)
- added unresolved.resolve.patch

* Mon Oct 30 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.0.1-1k)
- First Kondarization

* Tue Sep 21 1999 Sam Lantinga <slouken@devolution.com>
- first attempt at a spec file
