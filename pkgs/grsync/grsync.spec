%global momorel 6

Summary: GUI for rsync
Name: grsync
Version: 1.1.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
URL: http://www.opbyte.it/grsync/
Source0: http://www.opbyte.it/release/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: gettext
BuildRequires: glib2-devel >= 2.6
BuildRequires: gtk2-devel
BuildRequires: perl-XML-Parser
Requires: rsync

%description
Grsync is a GUI for rsync, the command line directory synchronization tool.
It supports only a limited set of rsync features, but can be effectively
used to synchronize local directories.

%prep
%setup

%build
%configure LIBS="-lm"
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR="%{buildroot}"
%find_lang %{name}

%clean
%{__rm} -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING INSTALL NEWS README
%{_bindir}/grsync
%{_bindir}/grsync-batch
%{_datadir}/%{name}
%{_datadir}/pixmaps/grsync.png
%{_datadir}/pixmaps/grsync-busy.png
%{_datadir}/applications/grsync.desktop
%{_datadir}/mime/packages/grsync.xml
%{_datadir}/icons/hicolor/*/*/*.png
%{_mandir}/man1/grsync.1*
%{_mandir}/man1/grsync-batch.1*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-3m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-2m)
- fix build

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-2m)
- %%NoSource -> NoSource

* Sun Jul 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-1m)
- import to Momonga from DAG

* Sun Jan 21 2007 Dag Wieers <dag@wieers.com> - 0.5.2-1
- Updated to release 0.5.2.

* Sun Aug 13 2006 Dries Verachtert <dries@ulyssis.org> - 0.5-1
- Updated to release 0.5.

* Thu Jun 29 2006 Dag Wieers <dag@wieers.com> 0.4.3-1
- Updated to release 0.4.3.

* Fri May 19 2006 Dries Verachtert <dries@ulyssis.org> - 0.4.2-1
- Updated to release 0.4.2.

* Sat May 06 2006 Dries Verachtert <dries@ulyssis.org> - 0.4.1-1
- Updated to release 0.4.1.

* Sun Apr 30 2006 Dag Wieers <dag@wieers.com> - 0.4-1
- Updated to release 0.4.

* Sun Apr 23 2006 Dries Verachtert <dries@ulyssis.org> - 0.3.2-1
- Updated to release 0.3.2.

* Wed Mar 01 2006 Dries Verachtert <dries@ulyssis.org> - 0.3-1
- Updated to release 0.3.

* Sun Jan 29 2006 Dries Verachtert <dries@ulyssis.org> - 0.2.1-1
- Initial package.
