%global momorel 9
%global srcname %{name}-%{version}

Summary: a full-text search program
Name: sylph-searcher

Version: 1.2.0
Release: %{momorel}m%{?dist}
License: BSD
URL: http://sylpheed.sraoss.jp/
Group: Applications/Text
Source0: http://sylpheed.sraoss.jp/sylpheed/misc/%{srcname}.tar.gz 
NoSource: 0
BuildRequires: glib2-devel >= 2.4.0
BuildRequires: gtk2-devel >= 2.4.0
BuildRequires: libsylph-devel >= 1.0.0
BuildRequires: momonga-rpmmacros >= 20040310-6m
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: mecab-devel >= 0.99
BuildRequires: postgresql-devel

Requires: glib2 >= 2.4.0
Requires: gtk2 >= 2.4.0
Requires: mecab >= 0.99
Requires: mecab-ipadic 
Requires: postgresql >= 8.2
Requires: libsylph >= 1.0.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Sylph-Searcher (tentative name) is a full-text search program for messages
stored in the mailboxes of Sylpheed, or generic MH folders.
It utilizes the full-text search feature of PostgreSQL 8.2.

%prep
%setup -q -n %{srcname}

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall transform='s,x,x,'

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING README*
%{_bindir}/syldbimport
%{_bindir}/syldbquery
%{_bindir}/sylph-searcher
%{_datadir}/%{name}/
%{_datadir}/locale/*/*/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-9m)
- rebuild for glib 2.33.2

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-8m)
- rebuild against mecab-0.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-7m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-6m)
- add BuildRequires

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Fri Sep  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-0.1.1m)
- update to 1.2.0beta1

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sat Nov  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- %%NoSource -> NoSource

* Sat Sep  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-2m)
- revise License (from GPL to BSD)

* Fri Jul 13 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.0-1m)
- up to 1.0.0

* Tue Jul 10 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.0-0.1.1m)
- up to 1.0.0beta1

* Sat Jun 30 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.5.0-1m)
- up to 0.5.0

* Fri Jun 29 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.4.0-2m)
- add sql

* Fri Jun 22 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.4.0-1m)
- up to 0.4.0

* Tue Jun 19 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.3.0-1m)
- up to 0.3.0

* Fri Jun 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-1m)
- initial build for Momonga Linux
