%global momorel 5

# Generated from heckle-1.4.3.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname heckle
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Heckle is unit test sadism(tm) at it's core
Name: rubygem-%{gemname}
Version: 1.4.3
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://www.rubyforge.org/projects/seattlerb
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(ParseTree) >= 2.0.0
Requires: rubygem(ruby2ruby) >= 1.1.6
Requires: rubygem(ZenTest) >= 3.5.2
Requires: rubygem(hoe) >= 2.3.0
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
Heckle is unit test sadism(tm) at it's core. Heckle is a mutation tester. It
modifies your code and runs your tests to make sure they fail. The idea is
that if code can be changed and your tests don't notice, either that code
isn't being covered or it doesn't do anything.
It's like hiring a white-hat hacker to try to break into your server and
making sure you detect it. You learn the most by trying to break things and
watching the outcome in an act of unit test sadism.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

sed -i -e 's|/usr/local/bin|/usr/bin|' %{buildroot}%{geminstdir}/bin/heckle

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/heckle
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.3-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.3-2m)
- full rebuild for mo7 release

* Sat Jan 02 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3-1m)
- Initial package for Momonga Linux
