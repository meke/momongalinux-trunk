%global momorel 4

Summary: Wrapper library for the Video Decode and Presentation API
Name: libvdpau
Version: 0.4.1
Release: %{momorel}m%{?dist}
License: MIT and see "COPYING"
Group: System Environment/Libraries
URL: http://freedesktop.org/wiki/Software/VDPAU
Source0: http://people.freedesktop.org/~aplattner/vdpau/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen
BuildRequires: graphviz
BuildRequires: libX11-devel
BuildRequires: libtool
BuildRequires: pkgconfig
BuildRequires: tetex

%description
VDPAU is the Video Decode and Presentation API for UNIX. 
It provides an interface to video decode acceleration and presentation
hardware present in modern GPUs.

%package devel
Summary: Header files and development libraries from libvdpau
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libX11-devel
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libvdpau.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/vdpau/%{name}_trace.la
rm -f %{buildroot}%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog
%{_libdir}/vdpau
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%doc %{_docdir}/%{name}
%{_includedir}/vdpau
%{_libdir}/pkgconfig/vdpau.pc
%{_libdir}/%{name}.so

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.1-4m)
- rebuild against graphviz-2.36.0-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.1-1m)
- update 0.4.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-3m)
- full rebuild for mo7 release

* Thu Jun  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4-2m)
- rebuild for gstreamer-plugins-bad

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-1m)
- update 0.4

* Wed Jan 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-2m)
- fix up %%install section

* Tue Dec 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3-1m)
- initial package for Momonga Linux
