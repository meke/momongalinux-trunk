Summary: Ruby/Bsearch -- a binary search library for Ruby
Name: ruby-bsearch

%global momorel 14

Version: 1.5
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://namazu.org/~satoru/ruby-bsearch/

Source0: http://namazu.org/~satoru/ruby-bsearch/%{name}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: ruby-devel >= 1.9.2

Requires: ruby

%description
Ruby/Bsearch is a binary search library for Ruby. It can search the FIRST or
LAST occurrence in an array with a condition given by a block.

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep

%setup -q

%build

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

mkdir -p %{buildroot}%{ruby_libdir}
install -m 644 bsearch.rb %{buildroot}%{ruby_libdir}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(- ,root,root)
%doc ChangeLog *.rd tests
%{ruby_libdir}/bsearch.rb

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5-14m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-11m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5-10m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-5m)
- %%NoSource -> NoSource

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (1.5-4m)
- merge from ruby-1_8-branch.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (1.5-3m)
- rebuild against ruby-1.8.0.

* Sun Dec 23 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.5-2k)

* Tue Oct 23 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.4-2k)

* Sat Sep 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3-2k)

* Fri Jul 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2-2k)
