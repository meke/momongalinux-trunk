%global         momorel 3

Name:           perl-MooseX-Types-Structured
Version:        0.30
Release:        %{momorel}m%{?dist}
Summary:        MooseX::Types::Structured - Structured Type Constraints for Moose
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Types-Structured/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/MooseX-Types-Structured-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-DateTime
BuildRequires:  perl-Devel-PartialDump >= 0.13
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Moose >= 1.08
BuildRequires:  perl-MooseX-Types >= 0.22
BuildRequires:  perl-MooseX-Types-DateTime
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Sub-Exporter >= 0.982
BuildRequires:  perl-Test-Exception >= 0.27
BuildRequires:  perl-Test-Simple >= 0.94
Requires:       perl-Devel-PartialDump >= 0.13
Requires:       perl-Moose >= 1.08
Requires:       perl-MooseX-Types >= 0.22
Requires:       perl-Scalar-Util
Requires:       perl-Sub-Exporter >= 0.982
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
A structured type constraint is a standard container Moose type constraint,
such as an ArrayRef or HashRef, which has been enhanced to allow you to
explicitly name all the allowed type constraints inside the structure. The
generalized form is:

%prep
%setup -q -n MooseX-Types-Structured-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/^perl(MooseX::Meta::TypeCoercion::Structured/d' | \
  sed -e '/^perl(MooseX::Meta::TypeConstraint::Structured/d' | \
  sed -e '/^perl(MooseX::Types::Structured::MessageStack/d' | \
  sed -e '/^perl(MooseX::Types::Structured::OverflowHandler/d'

EOF
%define __perl_requires %{_builddir}/MooseX-Types-Structured-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/MooseX/Meta/TypeCoercion/Structured.pm
%{perl_vendorlib}/MooseX/Meta/TypeCoercion/Structured
%{perl_vendorlib}/MooseX/Meta/TypeConstraint/Structured.pm
%{perl_vendorlib}/MooseX/Meta/TypeConstraint/Structured
%{perl_vendorlib}/MooseX/Types/Structured.pm
%{perl_vendorlib}/MooseX/Types/Structured
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-2m)
- rebuild against perl-5.14.0-0.2.1m

* Fri Apr 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.26-2m)
- rebuild for new GCC 4.6

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0,26

* Wed Dec 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.24-2m)
- rebuild for new GCC 4.5

* Wed Nov 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Thu Oct 07 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
