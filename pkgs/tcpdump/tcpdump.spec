%global momorel 1

Summary: A network traffic monitoring tool
Name: tcpdump
Version: 4.3.0
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
URL: http://www.tcpdump.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automake
BuildRequires: coreutils
BuildRequires: glibc-devel >= 2.2.5-12k
BuildRequires: kernel-headers >= 2.2.0
BuildRequires: libpcap-devel >= 1.2.0
BuildRequires: libtool
BuildRequires: momonga-rpmmacros >= 20040310-6m
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: sharutils
Requires: kernel >= 2.2.0
Requires(pre): shadow-utils

Source0: http://www.tcpdump.org/release/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: ftp://ftp.ee.lbl.gov/tcpslice-1.2a3.tar.gz 
NoSource: 1
Source2: ppi.h

Patch1: tcpdump-4.0.0-portnumbers.patch
Patch2: tcpdump-4.0.0-icmp6msec.patch
Patch3: tcpdump-3.9.8-gethostby.patch
Patch4: tcpslice-1.2a3-time.patch
Patch5: tcpslice-CVS.20010207-bpf.patch
Patch6: tcpslice-1.2a3-dateformat.patch

%define tcpslice_dir tcpslice-1.2a3

%description
Tcpdump is a command-line tool for monitoring network traffic.
Tcpdump can capture and display the packet headers on a particular
network interface or on all interfaces.  Tcpdump can display all of
the packet headers, or just the ones that match particular criteria.

Install tcpdump if you need a program to monitor network traffic.

%prep
%setup -q -a 1

%patch1 -p1 -b .portnumbers
%patch2 -p1 -b .icmp6msec
%patch3 -p1 -b .gethostby
cp %{SOURCE2} ppi.h

pushd %{tcpslice_dir}
%patch4 -p1 -b .time
%patch5 -p1 -b .bpf
%patch6 -p1 -b .dateformat
popd

find . -name '*.c' -o -name '*.h' | xargs chmod 644

%build
export CFLAGS="$RPM_OPT_FLAGS $(getconf LFS_CFLAGS) -fno-strict-aliasing"

pushd %{tcpslice_dir}
# update config.{guess,sub}
automake -a -f 2> /dev/null || :
%configure
make %{?_smp_mflags}
popd

%configure --with-crypto --with-user=tcpdump --without-smi
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_mandir}/man8
mkdir -p %{buildroot}%{_sbindir}

pushd %{tcpslice_dir}
install -m755 tcpslice %{buildroot}%{_sbindir}
install -m644 tcpslice.1 %{buildroot}%{_mandir}/man8/tcpslice.8
popd

install -m755 tcpdump %{buildroot}%{_sbindir}
install -m644 tcpdump.1 %{buildroot}%{_mandir}/man8/tcpdump.8

# fix section numbers
sed -i 's/\(\.TH[a-zA-Z ]*\)[1-9]\(.*\)/\18\2/' \
	%{buildroot}%{_mandir}/man8/*

%check
# make check

%clean
rm -rf %{buildroot}

%pre
/usr/sbin/groupadd -g 72 tcpdump 2> /dev/null
/usr/sbin/useradd -u 72 -g 72 -s /sbin/nologin -M -r \
	-d / tcpdump 2> /dev/null
exit 0

%files
%defattr(-,root,root)
%doc LICENSE README CHANGES CREDITS
%{_sbindir}/tcpdump
%{_sbindir}/tcpslice
%{_mandir}/man8/tcpslice.8*
%{_mandir}/man8/tcpdump.8*

%changelog
* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.3.0-1m)
- update to 4.3.0

* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.0-1m)
- update to 4.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.1-2m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.1-1m)
- update to 4.1.1
-- import patches from Rawhide (14:4.1.1-1)

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.8-10m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.8-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.9.8-8m)
- fix build on x86_64

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.8-7m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.8-6m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.8-5m)
- update Patch2 for fuzz=0

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.8-4m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9.8-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.8-2m)
- %%NoSource -> NoSource

* Mon Oct  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.8-1m)
- update to 3.9.8
- import Patch4 from Fedora devel

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.7-1m)
- update to 3.9.7 (sync with FC-devel)

* Sat Jul 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.5-3m)
- [SECURITY] CVE-2007-3798 tcpdump print-bgp.c Buffer Overflow Vulnerability
- import and modify patch from http://cvs.tcpdump.org/cgi-bin/cvsweb/tcpdump/print-bgp.c?r1=1.91.2.11&r2=1.91.2.12

* Thu Jun  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9.5-2m)
- separate libpcap and arpwatch

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.5-1m)
- sync with FC-devel
- update tcpdump tp 3.9.5
- update libpcap to 0.9.5
- update arpwatch to 2.1a15
- new patches has imported from FC-devel
- remove unused patches

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.4-3m)
- [SECURITY] CVE-2007-1218, import security patch from Mandriva

* Thu Jun  8 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9.4-2m)
- delete %%{_includedir}/net from files list

* Fri May  5 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.9.4-1m)
- update 3.9.4
- [SECURITY] CVE-2005-1280 CVE-2005-1279 CVE-2005-1278 CVE-2005-1267
- sync FC

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.8.3-5m)
- enable x86_64.

* Mon Oct 25 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.8.3-4m)
- add gcc34 patch

* Tue Jul 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.3-3m)
- delete %%define _ipv6		1
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Sat Apr 10 2004 Toru Hoshina <t@momonga-linux.org>
- (3.8.3-2m)
- The release number is applied for all of sub packages, however all packages
  aren't always updated at the same time, so we need independent release
  number for each sub package, otherwise must use epoch...

* Wed Apr 7 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.8.3-1m)
- update tcpdump to 3.8.3
- update libpcap to 0.8.3
- includes security fix. old patches are commented out.

* Thu Apr  1 2004 zunda <zunda at freeshell.org>
- (3.7.2-4m)
- arpwatch updated from 2.1a11 to 2.1a13

* Sun Jul 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.7.2-3m)
- rebuild against glibc-2.3.2                               

* Thu May  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.7.2-2m)
- fix include order in print-sctp.c
- remove INSTALL from %%files libpcap

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.7.2-1m)
  update to tcpdump 3.7.2 and libpcap 0.7.2

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.6.2-21m)
  rebuild against openssl 0.9.7a

* Thu Jun  6 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.6.2-20k)
  security fix. http://online.securityfocus.com/bid/4890
  
* Fri May 17 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.6.2-18k)
- rebuild against glibc-2.2.5-12k

* Wed May 15 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.6.2-16k)
- remove Patch1(libpcap-0.6.2-ifaddrs.patch)

* Tue May 14 2002 Toru Hoshina <t@kondara.org>
- (3.6.2-14k)
- security fix. jitterbug/1055
- ipv6 enabled at this time, but getifaddrs doesn't work, FIXME...

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.6.2-10k)
- arpwatch update to 2.1a11

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against openssl 0.9.6.

* Fri Apr 13 2001 Shingo Akagaki <dora@kondara.org>
- /etc/rc.d/init.d -> /etc/init.d

* Tue Mar  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.6.2-4k)
- errase IPv6 function with %{_ipv6} macro

* Fri Feb  9 2001 Kusunoki Masanori <nori@kondara.org>
- (3.6.2-3k)
- update to 3.6.2
- update libpcap to 0.6.2

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.6.1-7k)
- build against rpm-3.0.5-39k

* Thu Feb  1 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.6.1-5k)
- modified %files section

* Wed Jan 31 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.6.1-3k)
- update to 3.6.1
- update libpcap to 0.6.1

* Tue Nov 28 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to %_initscriptdir}

* Fri Oct 20 2000 MATSUDA, Daiki <dyky@df-usa.com>
- update arpwatch 2.1a10
- modified spec file a little with macro

* Wed Jul 06 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Sat Feb 19 2000 Tsutomu Yasuda <tom@kondara.org>
- bug fix libpcap

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-tcpdump-20000115

* Mon Jan 17 2000 Takaaki Tabuchi <tab@kondara.org>
- be able to rebuild non-root user.

* Sat Jan  8 2000 Kazuaki Yoshida <kazuaki@nifty.co.jp>
- 1st Release for "Kondara MNU/Linux".

* Tue Nov 30 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-tcpdump-19991115
