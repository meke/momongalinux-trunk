%global momorel 4
%global unstable 0
#global prever 1
%global srcver 4.4
%global qtver 4.8.5
%global qtrel 1m
%global kdever 4.12.97
%global kdelibsrel 1m
%global kdeedurel 1m
%global kdegraphicsrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%if 0%{?unstable}
%global betaver beta%{prever}
%global ftpdirver %{srcver}-%{betaver}
%global sourcedir unstable/kphotoalbum/%{ftpdirver}/src
%else
%global ftpdirver %{srcver}
%global sourcedir stable/kphotoalbum/%{ftpdirver}/src
%endif

Summary: KDE Photo Album 
Name: kphotoalbum
Version: %{srcver}
Epoch: 1
%if 0%{?unstable}
Release: 0.%{prever}.%{momorel}m%{?dist}
%else
Release: %{momorel}m%{?dist}
%endif
License: GPLv2
Group: Applications/Multimedia
URL: http://kphotoalbum.org/
%if 0%{?unstable}
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}-%{betaver}.tar.bz2
%else
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
%endif
NoSource: 0
## upstreamable patches
# disable mention of mplayer (which we can't ship), and needless scary warnings
Patch50:  kphotoalbum-4.3-mplayer_warning.patch
# default to xdg picturesPath instead of ~/Images
Patch51: kphotoalbum-4.3-xdg_picturesPath.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: marble-libs >= %{kdever}-%{kdeedurel}
Requires: kdegraphics-libs >= %{kdever}-%{kdegraphicsrel}
Requires: kipi-plugins
Requires: libkdcraw >= %{kdever}
Requires: libkexiv2 >= %{kdever}
BuildRequires: qt-devel >= %{qtver}-%{qtrel}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: marble-devel >= %{kdever}-%{kdeedurel}
BuildRequires: kdegraphics-devel >= %{kdever}-%{kdegraphicsrel}
BuildRequires: libkdcraw-devel >= %{kdever}
BuildRequires: libkexiv2-devel >= %{kdever}
BuildRequires: libkipi-devel >= %{kdever}
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: exiv2-devel >= 0.23
BuildRequires: gettext
BuildRequires: libjpeg-devel >= 8a

%description
A photo almbum tool.  focuses on three key points:
  * It must be easy to describe a number of images at a time. 
  * It must be easy to search for images. 
  * It must be easy to browse and View the images.

Can use kipi-plugins

%prep
%if 0%{?unstable}
%setup -q -n %{name}-%{version}-%{betaver}
%else
%setup -q
%endif

%patch50 -p1 -b .mplayer_warning
%patch51 -p1 -b .xdg_picturesPath

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING* ChangeLog INSTALL README TODO
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/kpa-backup.sh
%{_kde4_bindir}/open-raw.pl
%{_kde4_datadir}/applications/kde4/%{name}-import.desktop
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/applications/kde4/open-raw.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_configdir}/%{name}rc
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/*/*/*/%{name}.png
%{_kde4_iconsdir}/hicolor/*/actions/*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.4-4m)
- rebuild against KDE 4.13 RC

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.4-3m)
- rebuild against KDE 4.12.0

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.4-2m)
- rebuild against KDE 4.11 beta2

* Mon Feb 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.4-1m)
- update to 4.4

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.3-3m)
- add BuildRequires: libkipi-devel >= %%{kdever}

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.3-2m)
- rebuild with KDE 4.10 beta2
- import patches from Fedora

* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.3-1m)
- update to 4.3

* Sun Sep  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.3-0.1.1m)
- update to 4.3-beta1

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.2-3m)
- rebuild against exiv2-0.23

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.2-2m)
- rebuild with KDE 4.9 RC2

* Sun Apr  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.2-1m)
- update to 4.2 official release

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.2-0.2.1m)
- update to 4.2-rc2
- add Epoch

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-0.20101127.8m)
- rebuild against KDE 4.7.90

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-0.20101127.7m)
- rebuild against exiv2-0.22

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-0.20101127.6m)
- rebuildagainst kdeedu metapackage and marble

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-0.20101127.5m)
- rebuild against KDE 4.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-0.20101127.4m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-0.20101127.3m)
- rebuild against exiv2-0.21.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-0.20101127.2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-0.20101127.1m)
- KDE 4.5.80

* Sat Sep 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.1-0.20100918.1m)
- KDE 4.5.1

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-0.20100811.3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-0.20100811.2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-0.20100811.1m)
- KDE 4.5.0

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-0.20100726.1m)
- update to 20100726 svn snapshot for KDE 4.4.95
- and rebuild against kdeedu-4.4.95
- remove merged doc-translations.patch

* Sat Jul  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.5-0.20100703.1m)
- update to 20100703 svn snapshot for KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-0.20100609.2m)
- rebuild against qt-4.6.3-1m

* Wed Jun  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-0.20100609.1m)
- update to 20100609 svn snapshot for KDE 4.4.4

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-0.20100508.2m)
- rebuild against exiv2-0.20

* Sat May  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-0.20100508.1m)
- update to 20100508 svn snapshot for KDE 4.4.3

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-0.20100401.2m)
- rebuild against libjpeg-8a

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.2-0.20100401.1m)
- update to 20100401 svn snapshot for KDE 4.4.2

* Fri Mar 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.1-0.20100319.1m)
- update to 20100319 svn snapshot for KDE 4.4.1

* Sat Feb 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.0-0.20100212.1m)
- update to 20100212 svn snapshot for KDE 4.4.0

* Mon Jan 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.95-0.20100125.1m)
- update to 20100125 svn snapshot for KDE 4.3.95

* Sun Jan 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.90-0.20100110.1m)
- update to 20100110 svn snapshot for KDE 4.3.90

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-0.20091223-2m)
- rebuild against exiv2-0.19

* Wed Dec 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.85-0.20091223.1m)
- update to 20091223 svn snapshot for KDE 4.3.85

* Mon Dec 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.82-0.20091220.1m)
- update to 20091220 svn snapshot for KDE 4.3.82

* Thu Dec 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.80-0.20091210.1m)
- update to 20091210 svn snapshot for KDE 4.3.80

* Fri Dec  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.4-0.20091202.1m)
- update to 20091202 svn snapshot for KDE 4.3.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-0.20091104.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.3-0.20091104.1m)
- update to 20091104 svn snapshot for KDE 4.3.3

* Fri Oct  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-0.20091009.1m)
- update to 20091009 svn snapshot for KDE 4.3.2

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.3.1-0.20090904.2m)
- rebuild against libjpeg-7

* Fri Sep  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.1-0.20090904.1m)
- update to 20090904 svn snapshot for KDE 4.3.1

* Thu Aug  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-0.20090806.1m)
- update to 20090806 svn snapshot for KDE 4.3.0

* Tue Aug  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.98-0.20090804.1m)
- update to 20090804 svn snapshot for KDE 4.2.98

* Sun Jul 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.96-0.20090712.1m)
- update to 20090712 svn snapshot for KDE 4.2.96

* Tue Jul  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.95-0.20090707.1m)
- update to 20090707 svn snapshot for KDE 4.2.95

* Fri Jun 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.90-0.20090612.1m)
- update to 20090612 svn snapshot for KDE 4.2.90

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.4-0.20090606.1m)
- update to 20090606 svn snapshot for KDE 4.2.4

* Sun May 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.3-0.20090510.1m)
- update to 20090510 svn snapshot for KDE 4.2.3

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-0.20090406.1m)
- update to 20090406 svn snapshot for KDE 4.2.2

* Fri Mar 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.1-0.20090320.1m)
- update to 20090320 svn snapshot for KDE 4.2.1

* Sun Feb  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-0.20090201.1m)
- update to 20090201 svn snapshot for KDE 4.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-0.20090117.2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.96-0.20090117.1m)
- update to 20090117 svn snapshot

* Wed Dec 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-0.20081221.3m)
- build with new kdegraphics
- change Requires: from kipi-plugins4 to kipi-plugins

* Thu Dec 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-0.20081221.2m)
- rebuild against exiv2-0.18

* Sun Dec 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-0.20081221.1m)
- update to 20081221 svn snapshot

* Sat Nov 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.80-0.20081130.1m)
- update to 20081130 svn snapshot

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-0.20080831.2m)
- rebuild against kdegraphics-4.1.69-1m

* Sun Aug 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-0.20080831.1m)
- update to 20080831 svn snapshot
- remove info-rewind.patch

* Fri Jul  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-4.20080704.1m)
- update to 20080704 svn snapshot for KDE 4.1 beta 2+
- apply info-rewind.patch to enable build
- Requires: kipi-plugins4

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-4.20080627.1m)
- update to 20080627 svn snapshot for KDE 4.1 beta 2
- use kdegraphics-libs

* Mon Jun  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-4m)
- rebuild against exiv2-0.17

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-2m)
- rebuild against gcc43

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-1m)
- version 4.0.0

* Thu Jan 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-3m)
- BuildPreReq: libkdcraw-devel >= 0.1.3

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-2m)
- rebuild against exiv2-0.16
- License: GPLv2

* Sun Dec  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-1m)
- version 3.1.0

* Fri Apr 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.2-2m)
- rebuild against exiv2-0.14

* Sat Apr 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.2-1m)
- version 3.0.2

* Sat Apr 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-1m)
- version 3.0.1

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-1m)
- initial package for Momonga Linux
