%global momorel 16
%global pythonver 2.7
%global pythonverrel 2.7

Summary: XML libraries for python
Name: PyXML
Version: 0.8.4
Release: %{momorel}m%{?dist}
License: Apache
Group: Development/Libraries
URL: http://pyxml.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/pyxml/PyXML-%{version}.tar.gz 
NoSource: 0
Patch0: PyXML-0.7.1-intern.patch
Patch1: PyXML-0.8.4-cvs20041111-python2.4-backport.patch
Patch2: PyXML-memmove.patch
Patch3: PyXML-0.8.4-python2.6.patch
Requires: python >= %{pythonverrel}
BuildRequires: python-devel >= %{pythonverrel}
BuildRequires: expat-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
An XML package for Python.  The distribution contains a
validating XML parser, an implementation of the SAX and DOM
programming interfaces and an interface to the Expat parser.

%prep
%setup -q

%patch0 -p1 -b .intern
%patch1 -p1 -b .python2.4-backport
%patch2 -p1
%patch3 -p1

%build
# build PyXML with system expat
# Make sure we don't use local one
rm -rf extensions/expat
CFLAGS="$RPM_OPT_FLAGS" %{__python} -c 'import setuptools; execfile("setup.py")' build  --with-xslt --with-libexpat=%{_usr}

%install
rm -fr %{buildroot}
python -c 'import setuptools; execfile("setup.py")' install --skip-build --root=%{buildroot} --with-xslt

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ANNOUNCE CREDITS LICENCE README* TODO doc/*
%{_bindir}/xmlproc_parse
%{_bindir}/xmlproc_val
%{_libdir}/python%{pythonver}/site-packages/PyXML-*.egg-info
%{_libdir}/python%{pythonver}/site-packages/_xmlplus

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.4-16m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-13m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.4-12m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-10m)
- [SECURITY] CVE-2009-3720
- use system expat
- apply python26 patch from Rawhide (0.8.4-16)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-9m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.4-8m)
- rebuild against python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.4-7m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.4-6m)
- enable egginfo

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.4-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-4m)
- %%NoSource -> NoSource

* Fri Apr 06 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.8.4-3m)
- build with "--with-xslt"
- import patches from Fedora
- - PyXML-0.8.4-cvs20041111-python2.4-backport.patch
- - PyXML-memmove.patch

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-2m)
- rebuild against python-2.5

* Sun Oct 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-1m)
- update to 0.8.4 for 4Suite 1.0b1
- add %%files

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-3m)
- rebuild against python-2.4.2

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.3-2m)
- rebuild against python2.3

* Mon Mar 22 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.3-1m)
- import from Fedora Core 1

* Sun Nov  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.7.0-4m)
- accept python not only 2.2 but 2.2.*

* Sat Oct 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- change License: to Apache
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon May 20 2002 Kenta MURATA <muraken@kondara.org>
- (0.7.0-2k)
- sourceforge no ahou.

* Sat Apr  6 2002 Kenta MURATA <muraken@kondara.org>
- (0.7.0-2k)
- version up.

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6.5-4k)
- add BuildRequires,Requires Tag

* Thu Oct 25 2001 Toru Hoshina <t@kondara.org>
- (0.6.5-2k)
- rewind (TT) PyXML 0.6.6 doesn't match for printconf, still....

* Thu Oct 18 2001 Motonobu Ichimura <famao@kondara.org>
- (0.6.6-2k)
- up to 0.6.6
- use %{pythonver} for future.

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.6.5-4k)
- rebuild against gettext 0.10.40.

* Fri Sep 14 2001 Toru Hoshina <t@kondara.org>
- (0.6.5-2k)
- merge from rawhide.

* Tue Jul 24 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add python-devel to BuildRequires (#49820)

* Tue Jul 10 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Mark the locale-specific files as such

* Thu Jun  7 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Don't obsolete itself

* Mon May  7 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Initial build, it's no longer part of 4Suite


