%global         momorel 3

Name:           perl-POE
Version:        1.358
Release:        %{momorel}m%{?dist}
Summary:        Portable multitasking and networking framework for any event loop
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/POE/
Source0:        http://www.cpan.org/authors/id/R/RC/RCAPUTO/POE-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO >= 1.25
BuildRequires:  perl-IO-Pipely >= 0.005
BuildRequires:  perl-IO-Tty >= 1.08
BuildRequires:  perl-POE-Test-Loops >= 1.351
BuildRequires:  perl-PathTools >= 3.31
BuildRequires:  perl-Socket >= 1.7
BuildRequires:  perl-Storable >= 2.16
BuildRequires:  perl-Test-Harness >= 2.26
BuildRequires:  perl-Time-HiRes >= 1.59
Requires:       perl-PathTools >= 3.31
Requires:       perl-IO >= 1.25
Requires:       perl-IO-Pipely >= 0.005
Requires:       perl-IO-Tty >= 1.08
Requires:       perl-POE-Test-Loops >= 1.351
Requires:       perl-Socket >= 1.7
Requires:       perl-Storable >= 2.16
Requires:       perl-Test-Harness >= 2.26
Requires:       perl-Time-HiRes >= 1.59
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
POE is a framework for cooperative, event driven multitasking and
networking in Perl. Other languages have similar frameworks. Python has
Twisted. TCL has "the event loop".

%prep
%setup -q -n POE-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor --default
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES HISTORY README TODO
%{perl_vendorlib}/POE.pm
%{perl_vendorlib}/POE
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.358-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.358-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.358-1m)
- update to 1.358

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.356-1m)
- update to 1.356

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.354-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.354-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.354-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.354-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.354-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.354-1m)
- update to 1.354
- rebuild against perl-5.16.0

* Mon Mar 12 2012 NARTIA Koichi <pulsar@momonga-linux.org>
- (1.351-1m)
- update to 1.351

* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.350-1m)
- update to 1.350

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.312-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.312-1m)
- update to 1.312

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.311-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.311-2m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.311-1m)
- update to 1.311

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.310-1m)
- update to 1.310

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.299-2m)
- rebuild for new GCC 4.6

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.299-1m)
- update to 1.299

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.294-2m)
- rebuild for new GCC 4.5

* Fri Nov 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.294-1m)
- update to 1.294

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.293-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.293-1m)
- update to 1.293

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.292-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.292-1m)
- update to 1.292

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.291-1m)
- update to0 1.291

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.289-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.289-2m)
- rebuild against perl-5.12.0

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.289-1m)
- update to 1.289

* Sat Feb 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.287-1m)
- update to 1.287

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.286-1m)
- update to 1.286

* Thu Jan 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.284-1m)
- update to 1.284

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.283-1m)
- update to 1.283

* Thu Jan  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.282-1m)
- update to 1.282

* Fri Jan  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.281-1m)
- update to 1.281

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.280-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.280-1m)
- update to 1.280

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.269-1m)
- update to 1.269

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.268-1m)
- update to 1.268

* Thu Sep 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.267-1m)
- update to 1.267

* Sun Aug 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.266-1m)
- update to 1.266

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.020-1m)
- update to 1.020

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-1m)
- update to 1.007

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006-2m)
- version down to 1.006 for perl-Gungho (POE::Component::Client::DNS)
- rebuild against perl-5.10.1

* Wed Aug  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-1m)
- update to 1.007

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006-1m)
- update to 1.006

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.005-1m)
- update to 1.005

* Tue Mar 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.004-1m)
- update to 1.004

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.003-2m)
- rebuild against rpm-4.6

* Sun Jul  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003-1m)
- update to 1.003

* Fri Jun 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0003-1m)
- update to 1.0003

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0002-1m)
- update to 1.0002

* Sun Apr 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0001-1m)
- update to 1.0001

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0000-2m)
- rebuild against gcc43

* Thu Mar 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0000-1m)
- update to 1.0000

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9999-2m)
- %%NoSource -> NoSource

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9999-1m)
- update to 0.9999

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9989-2m)
- use vendor

* Sat Mar 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9989-1m)
- update to 0.9989

* Thu Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9917-1m)
- update to 0.9917

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9500-1m)
- update to 0.9500

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.38-1m)
- spec file was autogenerated
