%global momorel 5

%define userwrapper 0
#there is a problem with multi-threaded operations and samba 3.2 (F9+)
#as a workaround they are disabled by default
#see bug 445978 - https://bugzilla.redhat.com/show_bug.cgi?id=445978
%define userwrapper 1

Name:		fuse-smb
Summary:	FUSE-Filesystem to fast and easy access remote resources via SMB
Version:	0.8.7
Release:	%{momorel}m%{?dist}
License:	GPLv2+
Group:		System Environment/Base
URL:		http://www.ricardis.tudelft.nl/~vincent/fusesmb/
#moved to .rpmmacros
#Packager:	Marcin Zajaczkowski <mszpak ATT wp DOTT pl>
Source0:	http://www.ricardis.tudelft.nl/~vincent/fusesmb/download/fusesmb-%{version}.tar.gz
%if %{userwrapper}
Source1:	fusesmb.sh.fuse-smb
Source2:	README.fedora.fuse-smb
%endif
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#fuse-libs doesn't force to have fuse which is needed for fuse-smb
#explicit Requires statement is required
Requires:	fuse >= 2.3
#samba-common (fc<=6), libsmbclient (fc>6) and fuse-libs are implicity required by their .so files
#Requires:		fuse-libs >= 2.3
#Requires:		samba-common >= 4.0
#nmblookup from samba-client seems to be required, see bug 554188
Requires:	samba-client >= 4.0

#fuse(-devel) 2.3 is a minimal required version
BuildRequires:	fuse-devel >= 2.3
BuildRequires:	samba-client >= 4.0

%if 0%{?fedora} > 6
#starting with 3.0.24-3 in FC7+ libsmbclient is in a separate package
BuildRequires: libsmbclient-devel >= 4.0
%endif

%description
With SMB for Fuse you can seamlessly browse your network neighbourhood
as were it on your own filesystem.
It's basically smbmount with a twist. Instead of mounting one Samba share
at a time, you mount all workgroups, hosts and shares at once. Only when
you're accessing a share a connection is made to the remote computer.

%prep
%setup -q -n fusesmb-%{version}
%if %{userwrapper}
cp %{SOURCE1} fusesmb.sh
cp %{SOURCE2} README.fedora
%endif

%build
export CFLAGS="%{optflags} -I%{_includedir}/samba-4.0"
%configure
make %{?_smp_mflags}

%install
rm -fr %{buildroot}
make install DESTDIR=%{buildroot}
%if %{userwrapper}
mv %{buildroot}%{_bindir}/fusesmb %{buildroot}%{_bindir}/fusesmb-bin
cp fusesmb.sh %{buildroot}%{_bindir}/fusesmb
%endif

%clean
#build dir is removed by --clean option
rm -fr %{buildroot}

%files
%defattr(0644,root,root,0755)
%attr(0755,root,root) %{_bindir}/fusesmb
%attr(0755,root,root) %{_bindir}/fusesmb.cache
#NEWS is empty in 0.8.5 and causes rpmlint's warning
%doc AUTHORS ChangeLog COPYING INSTALL README TODO fusesmb.conf.ex
%{_mandir}/man*/fusesmb.*

%if %{userwrapper}
#original binary file
%attr(0755,root,root) %{_bindir}/fusesmb-bin
%doc README.fedora
%endif


%changelog
* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-5m)
- enable to build with samba-4.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.7-1m)
- import from Fedora

* Fri Jan 22 2010 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.7-8
- added samba-client dependency to provide nmblookup (bug 554188)

* Thu Sep 17 2009 Peter Lemenkov <lemenkov@gmail.com> - 0.8.7-7
- Rebuilt with new fuse

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.7-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.7-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jul 3 2008 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.7-4
- added workaround for problem with multi-threaded operations and samba 3.2
(F9+) - bug 445978

* Sat May 10 2008 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.7-3
- fixed conditional statement for better handle Fedora 10 version number

* Sat May 10 2008 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.7-2
- added explicit dependency on fuse package (thanks to Mikel Ward - #445316)

* Sat Feb 9 2008 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.7-1
 - updated to 0.8.7
 - specified licence type

* Thu Apr 19 2007 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.5-7
 - changed expression to determine Fedora version to work in mock

* Thu Apr 19 2007 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.5-6
 - added compatility with samba package >= 3.0.24-3 in FC7+

* Fri Jan 26 2007 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.5-5
 - bumped to next release to workaround problem with FC-5 tag in CVS

* Thu Jan 25 2007 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.5-4
 - fixed accidental spaces in file

* Wed Jan 17 2007 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.5-3
 - removed redundant requirements
 - removed redundant %%{optflags} (%%configure already uses it)

* Mon Jan 15 2007 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.5-2
 - started adaptation to Fedora Extras requirements
 - Packager tag removed from .spec file (now only in .rpmmacros)
 - gcc removed from BuildRequires section
 - added %%{optflags}
 - removed empty NEWS from package (rpmlint's warning)

* Thu May 26 2006 Marcin Zajaczkowski <mszpak ATT wp DOTT pl> - 0.8.5-1
 - initial release (should be compatible with fuse package from Fedora Extras)

