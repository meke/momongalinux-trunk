%global momorel 8

%global fontname        thibault
%global conf1           69-essays1743.conf
%global conf2           69-isabella.conf
%global conf3           69-rockets.conf
%global conf4           69-staypuft.conf

%define common_desc \
A collection of fonts from thibault.org,\
including Isabella, Essays1743, StayPuft,\
and Rockets.

Name:           %{fontname}-fonts
Version:        0.1
Release:        %{momorel}m%{?dist}

Summary:        Thibault.org font collection
Group:          User Interface/X
License:        LGPLv2+

URL:            http://www.thibault.org/fonts
Source0:        http://thibault.org/fonts/essays/Essays1743-1.0-ttf.tar.gz
Source1:        http://www.thibault.org/fonts/isabella/Isabella.tar.gz
Source2:        http://www.thibault.org/fonts/rockets/Rockets-ttf.tar.gz
Source3:        http://www.thibault.org/fonts/staypuft/StayPuft.tar.gz
Source4:        %{name}-essays1743-fontconfig.conf
Source5:        %{name}-isabella-fontconfig.conf
Source6:        %{name}-rockets-fontconfig.conf
Source7:        %{name}-staypuft-fontconfig.conf

#Not included due to legal concerns
#Engadget: A sort of modernistic font done to match the logo of http://www.engadget.com

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
BuildRequires:  fontforge >= 20061025-1

%description
%common_desc

%package common
Summary:        Common files for thibault (documentation)
Group:          User Interface/X
Requires:       fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.

%package -n %{fontname}-essays1743-fonts

Summary:  Thibault.org Montaigne's Essays typeface font

Requires: %{name}-common = %{version}-%{release}
Obsoletes: %{name}-essays1743 < 0.1-4

%description -n %{fontname}-essays1743-fonts
%common_desc

A font by John Stracke, based on the
typeface used in a 1743 English
translation of Montaigne's Essays.

%_font_pkg -n essays1743 -f %{conf1} Essays1743*.ttf

%package -n %{fontname}-isabella-fonts

Summary: Thibault.org Isabella Breviary calligraphic font

Requires: %{name}-common = %{version}-%{release}
Obsoletes: %{name}-isabella < 0.1-4

%description -n %{fontname}-isabella-fonts
%common_desc

This font is called Isabella because it is based on the
calligraphic hand used in the Isabella Breviary, made around 1497, in
Holland, for Isabella of Castille, the first queen of united Spain.

%_font_pkg -n isabella -f %{conf2} Isabella*.ttf

%package -n %{fontname}-rockets-fonts

Summary:  Thibault.org font, vaguely space themed

Requires: %{name}-common = %{version}-%{release}
Obsoletes: %{name}-rockets < 0.1-4

%description -n %{fontname}-rockets-fonts
%common_desc

This font is called Rockets because it's vaguely space
themed.  The A is, more or less, a 1950s SF rocket; the O is meant to
be Earth, with the Americas visible.  The other capitals are based on
curves from either A or O, to keep the theme consistent.

%_font_pkg -n rockets -f %{conf3} Rockets*.ttf

%package -n %{fontname}-staypuft-fonts

Summary: Thibault.org font, rounded and marshmellowy

Requires: %{name}-common = %{version}-%{release}
Obsoletes: %{name}-staypuft < 0.1-4

%description -n %{fontname}-staypuft-fonts
%common_desc

A rounded marshmellow type font. Good for frivolous things
like banners, and birthday cards.

%_font_pkg -n staypuft -f %{conf4} StayPuft*.ttf

%prep
mkdir -p isabella staypuft
tar xvzf %{SOURCE0}
tar xvzf %{SOURCE1} -C isabella
tar xvzf %{SOURCE2}
tar xvzf %{SOURCE3} -C staypuft

%build

pushd Essays1743
fontforge -lang=ff -c 'Open($1); Generate($2)' Essays1743.sfd ../Essays1743.ttf
fontforge -lang=ff -c 'Open($1); Generate($2)' Essays1743-Bold.sfd ../Essays1743-Bold.ttf
fontforge -lang=ff -c 'Open($1); Generate($2)' Essays1743-BoldItalic.sfd ../Essays1743-BoldItalic.ttf
fontforge -lang=ff -c 'Open($1); Generate($2)' Essays1743-Italic.sfd ../Essays1743-Italic.ttf
popd

pushd isabella
fontforge -lang=ff -c 'Open($1); Generate($2)' Isabella-first.sfd ../Isabella.ttf
popd

pushd rockets
fontforge -lang=ff -c 'Open($1); Generate($2)' Rockets.sfd ../Rockets.ttf
popd

pushd staypuft
fontforge -lang=ff -c 'Open($1); Generate($2)' StayPuft.sfd ../StayPuft.ttf
popd

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}

install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE4} \
        %{buildroot}%{_fontconfig_templatedir}/%{conf1}


install -m 0644 -p %{SOURCE5} \
        %{buildroot}%{_fontconfig_templatedir}/%{conf2}

install -m 0644 -p %{SOURCE6} \
        %{buildroot}%{_fontconfig_templatedir}/%{conf3}

install -m 0644 -p %{SOURCE7} \
        %{buildroot}%{_fontconfig_templatedir}/%{conf4}

for fconf in %{conf1} \
                %{conf2} \
                %{conf3} \
                %{conf4} ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done

%clean
rm -fr %{buildroot}

%files common
%defattr(0644,root,root,0755)
%doc Essays1743/COPYING Essays1743/README
%doc isabella/COPYING.LIB isabella/README.txt
%doc rockets/COPYING.LIB rockets/README.txt
%doc staypuft/COPYING.LIB staypuft/README.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-6m)
- full rebuild for mo7 release

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-5m)
- update Isabella

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1-1m)
- import from Fedora

* Sun Mar 15 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net> - 0.1-10
- Make sure F11 font packages have been built with F11 fontforge


#thibault
* Mon Mar 2 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 0.1-9
- Edited to censor to fit the whining nature of Fedora PC Freaks.

* Mon Mar 2 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 0.1-8
- Typo.

* Mon Mar 2 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 0.1-7
- Fontforge script errors fixed.

* Mon Mar 2 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 0.1-6
- Fixed error in fontforge script that was 
- caused by some f***er turning on python bindings.

* Mon Mar 2 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 0.1-5
- Fixed errors in previous modifications.
- Updated for latest policy changes.
- Deleted erroneous edits made by jkeating.

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jan 11 2009 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 0.1-3
- Modified spec file to comply with new policy changes.

* Sat Sep 06 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com> 0.1-2
- Rebuild for new fontforge release.

* Fri Jul 18 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com>
- Fixed minor issues found during review.

* Mon Jul 15 2008 Matt Domsch <mdomsch@fedoraproject.org>
- Rewrote spec file to comply with fedora's policies

* Thu Jul 10 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com>
- Removed the Engadget font due to legal concerns

* Wed Jul 09 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com>
- Fixed post and postun issues to stay in specs that Fedora requires.
- Fixed define issues.
- Created multiple source rpm
- Fixed issues with setup block

* Fri Jul 04 2008 Lyos Gemini Norezel <Lyos.GeminiNorezel@gmail.com>
- compiled collection of thibault.org fonts into single rpm

#rockets
* Sun Jan 29 2006 John Stracke <francis@thibault.org>
- based on specfile of Isabella-ttf.

#engadget
* Sun Aug 28 2005 John Stracke <francis@thibault.org>
- based on specfile of isabella-ttf.

#isabella
* Sun Jul 03 2005 John Stracke <francis@thibault.org>
- Finally getting rid of the Greek-letters-pretending-to-be-ligatures
  and moving them up to the Private Use Area.

* Sun Oct 31 2004 John Stracke <francis@thibault.org>
- adding reference to bold (added italic a bit ago)

#essays1743
* Sun Oct 31 2004 John Stracke <francis@thibault.org>
- adding reference to bold (added italic a bit ago)

* Wed Oct 06 2004 John Stracke <francis@thibault.org>
- based on specfile of RedHat 9 urw-fonts.

#isabella
* Wed Oct 06 2004 John Stracke <francis@thibault.org>
- based on specfile of RedHat 9 urw-fonts.

