%global		momorel 1
Name:		rply
Version:	1.1.1
Release: %{momorel}m%{?dist}
Summary:	A library to read and write PLY files
Group:		Development/Libraries
License:	MIT
URL:		http://w3.impa.br/~diego/software/rply/
Source0:	http://w3.impa.br/~diego/software/rply/rply-%{version}.tar.gz
NoSource: 	0
Source1:	rply_CMakeLists.txt
Source2:	RPLYConfig.cmake.in
Source3:	rply_cmake_export_cmakelists.txt
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	cmake >= 2.6.0


%description
RPly is a library that lets applications read and write PLY files.
The PLY file format is widely used to store geometric information, such as 3D
models, but is general enough to be useful for other purposes.

RPly is easy to use, well documented, small, free, open-source, ANSI C, 
efficient, and well tested. The highlights are:

* A callback mechanism that makes PLY file input straightforward;
* Support for the full range of numeric formats;
* Binary (big and little endian) and text modes are fully supported;
* Input and output are buffered for efficiency;
* Available under the MIT license for added freedom. 

%prep
%setup -q

# Add CMakeLists.txt file
cp %{SOURCE1} CMakeLists.txt

# Add CMake detection modules
mkdir -p CMake/export
mkdir -p CMake/Modules
cp %{SOURCE2} CMake/Modules/
cp %{SOURCE3} CMake/export/CMakeLists.txt

%build
%cmake -DCMAKE_BUILD_TYPE:STRING="Release"\
       -DCMAKE_VERBOSE_MAKEFILE=ON .

%make 

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc LICENSE
%doc manual/*
%{_libdir}/*.so.*
%{_bindir}/*


%package        devel
Summary:	Libraries and headers for rply
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel

Rply Library Header Files and Link Libraries

%files devel
%defattr(-,root,root)
%dir %{_includedir}/%{name}/
%{_includedir}/%{name}/*
%{_libdir}/*.so
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/rplyConfig.cmake

%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-1m)
- import from fedora

