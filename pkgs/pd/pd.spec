%global momorel 4
%global majorver 0.42
%global minorver 5

Summary: A real-time graphical programming environment for audio, video, and graphical processing
Name: pd
Version: %{majorver}.%{minorver}
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/Multimedia
URL: http://puredata.info/
Source0: http://dl.sourceforge.net/project/pure-data/pure-data/%{version}/%{name}-%{majorver}-%{minorver}.src.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-fftw3.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: tcl
Requires: tk
BuildRequires: alsa-lib-devel
BuildRequires: coreutils
BuildRequires: fftw-devel
BuildRequires: jack-devel
BuildRequires: portaudio-devel
BuildRequires: tcl-devel
BuildRequires: tk-devel

%description
PD (aka Pure Data) is a real-time graphical programming environment for audio, video, and graphical processing. It is the third major branch of the family of patcher programming languages known as Max (Max/FTS, ISPW Max, Max/MSP, jMax, etc.) originally developed by Miller Puckette and company at IRCAM. The core of Pd is written and maintained by Miller Puckette and includes the work of many developers, making the whole package very much a community effort.

Pd was created to explore ideas of how to further refine the Max paradigm with the core ideas of allowing data to be treated in a more open-ended way and opening it up to applications outside of audio and MIDI, such as graphics and video.

It is easy to extend Pd by writing object classes ("externals") or patches ("abstractions"). The work of many developers is already available as part of the standard Pd packages and the Pd developer community is growing rapidly. Recent developments include a system of abstractions for building performance environments; a library of objects for physical modeling; and a library of objects for generating and processing video in realtime.

Pd is free software and can be downloaded either as an OS-specific package, source package, or directly from CVS. Pd was written to be multi-platform and therefore is quite portable; versions exist for Win32, IRIX, GNU/Linux, BSD, and MacOS X running on anything from a PocketPC to an old Mac to a brand new PC. It is possible to write externals and patches that work with Max/MSP and Pd using flext and cyclone.

%package devel
Summary: Header files and static libraries from pd
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on pd.

%prep
%setup -q -n %{name}-%{majorver}-%{minorver}

%patch0 -p1 -b .fftw3

%build
pushd src
%configure \
	--enable-alsa \
	--enable-fftw \
	--enable-jack \
	--enable-portaudio
make depend
%make
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
pushd src
%makeinstall
popd

# install binaries
install -m 755 bin/%{name}-gui %{buildroot}%{_bindir}/
install -m 755 bin/%{name}-watchdog %{buildroot}%{_bindir}/
install -m 755 bin/%{name}.tk %{buildroot}%{_bindir}/

# unpack man files
pushd %{buildroot}%{_mandir}/man1
gzip -d %{name}*.1.gz
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc INSTALL.txt LICENSE.txt README.txt src/CHANGELOG.txt
%{_bindir}/%{name}
%{_bindir}/%{name}-gui
%{_bindir}/%{name}-watchdog
%{_bindir}/%{name}.tk
%{_bindir}/%{name}receive
%{_bindir}/%{name}send
%{_libdir}/%{name}
%{_mandir}/man1/%{name}*.1*

%files devel
%defattr(-,root,root)
%{_includedir}/m_%{name}.h

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.42.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.42.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.42.5-2m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.42.5-1m)
- version 0.42.5
- build with fftw, jack and portaudio

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.41-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.41-2m)
- rebuild against rpm-4.6

* Sun Jul 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.41-1m)
- initial package for aubio-0.3.2
