%global momorel 7

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}
Summary:        Which subproject of xml-commons
Name:           xml-commons-which
Version:        1.0
Release:        1.b2.0jpp.%{momorel}m%{?dist}
#Epoch:          1
License:        Apache
URL:            http://xml.apache.org/commons/
Source0:        xml-commons-1.0.b2.tar.gz
# svn export http://svn.apache.org/repos/asf/xml/commons/tags/xml-commons-1_0_b2/
# tar czvf xml-commons-1.0.b2.tar.gz xml-commons-1_0_b2
Source1:        xml-commons.which.script
Patch0:         xml-commons-which-Which_java.patch
Requires:       jpackage-utils >= 0:1.5
BuildRequires:  ant
BuildRequires:  jpackage-utils >= 0:1.5 
Group:          Applications/Text
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%if ! %{gcj_support}
BuildArch:      noarch
%endif

%if %{gcj_support}
BuildRequires:          java-gcj-compat-devel
Requires(post):         java-gcj-compat
Requires(postun):       java-gcj-compat
%endif

%description
The which subproject is an environment checking utility that scans 
your environment and reports common versions of xml-related files.

%package javadoc
Summary:        Javadoc for %{name} 
Group:          Documentation

%if %{gcj_support}
BuildRequires:          java-gcj-compat-devel
Requires(post):         java-gcj-compat
Requires(postun):       java-gcj-compat
%endif

%description javadoc
Javadoc for %{name}.

# -----------------------------------------------------------------------------

%prep
%setup -q -n xml-commons-1_0_b2 
%patch0
# remove all prebuilt stuff
rm -rf java/build

# remove all binary libs and prebuilt javadocs
rm -rf `find . -name "*.jar" -not -name 'xml-apis.jar' -o -name "*.gz"`

# -----------------------------------------------------------------------------

%build
#export OPT_JAR_LIST="./java/external/build/xml-apis.jar"
ant -f java/which.xml 

# -----------------------------------------------------------------------------

%install
rm -rf $RPM_BUILD_ROOT

# Jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p java/build/which.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-which-%{version}.jar

# Jar versioning
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

# Javadocs
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-which-%{version}
cp -pr java/build/docs/javadocs/* \
  $RPM_BUILD_ROOT%{_javadocdir}/%{name}-which-%{version}

ln -s %{name}-which-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}-which 

# Scripts
mkdir -p $RPM_BUILD_ROOT%{_bindir}
cp -p %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/xml-which

# -----------------------------------------------------------------------------

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

# -----------------------------------------------------------------------------

%if %{gcj_support}
%post
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc LICENSE.txt
%defattr(0644,root,root,0755)
%{_javadir}/%{name}-which*
%attr(0755,root,root) %{_bindir}/*

%if %{gcj_support}
%dir %attr(-,root,root) %{_libdir}/gcj/%{name}
%attr(-,root,root) %{_libdir}/gcj/%{name}/xml-commons-which-which-1.0.jar.*
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/*


# -----------------------------------------------------------------------------

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-1.b2.0jpp.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-1.b2.0jpp.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-1.b2.0jpp.5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1.b2.0jpp.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1.b2.0jpp.3m)
- rebuild against rpm-4.6

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1.b2.0jpp.2m)
- fix %%files javadoc to avoid conflicting main package

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1.b2.0jpp.1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1:1.0-1.b2.0jpp.2
- Autorebuild for GCC 4.3

* Mon Mar 19 2007 Matt Wringe <mwringe@redhat.com> - 1:1.0-0.b2.0jpp.2
- Create xml-commons-which-Which_java.patch patch to get around build
  failures due to enum being a reserved word in Java 5.0

* Tue Mar 06 2007 Matt Wringe <mwringe@redhat.com> - 1:1.0-0.b2.0jpp.1
- xml-commons split into xml-commons-apis and xml-commons-which
- bumped epoch due to previous xml-commons-which having the wrong 
  version (1.3.02 instead of 1.0-0.b2)

* Mon Aug 21 2006 Fernando Nasser <fnasser@redhat.com> - 0:1.3.02-0.b2.7jpp.10
- Add missing Requires for post and postun javadoc sections

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:1.3.02-0.b2.7jpp_9fc
- Rebuilt

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0:1.3.02-0.b2.7jpp_8fc
- rebuild

* Mon Mar  6 2006 Jeremy Katz <katzj@redhat.com> - 0:1.3.02-0.b2.7jpp_7fc
- stop scriptlet spew

* Wed Feb 22 2006 Rafael Schloming <rafaels@redhat.com> - 0:1.3.02-0.b2.7jpp_6fc
- Updated to 1.3

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0:1.0-0.b2.7jpp_5fc
- bump again for double-long bug on ppc(64)

* Wed Dec 21 2005 Jesse Keating <jkeating@redhat.com> 0:1.0-0.b2.7jpp_4fc
- rebuilt again

* Tue Dec 13 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Jul 15 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.7jpp_3fc
- Build on ia64, ppc64, s390 and s390x.
- Switch to aot-compile-rpm (also BC-compiles the which jar).

* Wed Jun 15 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.7jpp_2fc
- Remove all prebuilt stuff from the tarball.

* Thu May 26 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.7jpp_1fc
- Upgrade to 1.0-0.b2.7jpp.
- Remove now-unnecessary workaround for #130162.
- Rearrange how BC-compiled stuff is built and installed.

* Mon May 23 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_13fc
- Add alpha to the list of build architectures (#157522).
- Use absolute paths for rebuild-gcj-db.

* Thu May  5 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_12fc
- Add dependencies for %%post and %%postun scriptlets (#156901).

* Tue May  3 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_11fc
- BC-compile the API jar.

* Tue Apr 26 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_10fc
- Remove gcj endorsed dir support (#155693).

* Mon Apr 25 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_9fc
- Provide a default transformer when running under libgcj.

* Mon Apr 25 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_8fc
- Provide a default DOM builder when running under libgcj (#155693).

* Fri Apr 22 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_7fc
- Provide a default SAX parser when running under libgcj (#155693).

* Thu Apr 21 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_6fc
- Add gcj endorsed dir support.

* Tue Jan 11 2005 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_5fc
- Sync with RHAPS.

* Thu Nov  4 2004 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_4fc
- Build into Fedora.

* Thu Oct 28 2004 Gary Benson <gbenson@redhat.com> - 0:1.0-0.b2.6jpp_3fc
- Bootstrap into Fedora.

* Fri Oct 1 2004 Andrew Overholt <overholt@redhat.com> - 0:1.0-0.b2.6jpp_3rh
- add coreutils BuildRequires

* Thu Mar  4 2004 Frank Ch. Eigler <fche@redhat.com> - 0:1.0-0.b2.6jpp_2rh
- RH vacuuming part II

* Tue Mar  2 2004 Frank Ch. Eigler <fche@redhat.com> - 0:1.0-0.b2.6jpp_1rh
- RH vacuuming

* Thu Aug 26 2003 Ralph Apel <r.apel at r-apel.de> - 0:1.0-0.b2.7jpp
- Build with ant-1.6.2

* Mon May  5 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-0.b2.6jpp
- Fix non-versioned javadoc symlinking.

* Mon Apr 21 2003 Ville Skytta <ville.skytta at iki.fi> - 0:1.0-0.b2.5jpp
- Fix xml-which script.
- Include non-versioned javadoc symlinks.
- Add Epoch: 0.
- Fix jpackage-utils dependency versions.

* Thu Mar 13 2003 Nicolas Mailhot <Nicolas.Mailhot at jpackage.org> - 1.0-0.b2.4jpp
- For jpackage-utils 1.5

* Wed Nov 13 2002 Ville Skytta <ville.skytta at iki.fi> - 1.0-0.b2.3jpp
- Following upstream changes, resolver is now built from its own package.

* Sun Nov  3 2002 Ville Skytta <ville.skytta at iki.fi> 1.0-0.b2.2jpp
- Split resolver into its own subpackage.
- Fix Group, Vendor and Distribution tags.
- Use sed instead of bash 2 extension when symlinking jars.
- Add resolver and which shell scripts.

* Thu Jul 11 2002 Henri Gomez <hgomez@users.sourceforge.net> 1.0-0.b2.1jpp
- 1.0.b2
- get tarball from xml.apache.org
- add macro section

* Fri Jan 18 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b1.1jpp
- first jpp release
