%global momorel 9
%global _libdir %{_prefix}/lib/X11

Summary: A utility for converting FIG files (made by xfig) to other formats.
Name: transfig
Version: 3.2.5
Release: %{momorel}m%{?dist}
License: "see manpages"
Group: Applications/Multimedia
#Source0: http://www.xfig.org/software/xfig/%{version}/transfig.%{version}d.tar.gz
Source0: http://downloads.sourceforge.net/mcj/transfig.%{version}d.tar.gz
NoSource: 0
BuildRequires: libpng-devel >= 1.2.2, libjpeg-devel
BuildRequires: tetex-latex, imake
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: cmpskit

%description
The transfig utility creates a makefile which translates FIG (created
by xfig) or PIC figures into a specified LaTeX graphics language (for
example, PostScript(TM)).  Transfig is used to create TeX documents
which are portable (i.e., they can be printed in a wide variety of
environments).

Install transfig if you need a utility for translating FIG or PIC
figures into certain graphics languages.

%prep
%setup -q -n transfig.%{version}d

%build
xmkmf
make Makefiles \
     XFIGLIBDIR=%{_libdir}/xfig \
     FIG2DEV_LIBDIR=%{_libdir}/fig2dev

%ifarch alpha alphaev5
make EXTRA_DEFINES="-Dcfree=free" XFIGLIBDIR=%{_libdir}/xfig \
     FIG2DEV_LIBDIR=%{_libdir}/fig2dev
%else
make XFIGLIBDIR=%{_libdir}/xfig FIG2DEV_LIBDIR=%{_libdir}/fig2dev
%endif


%install
rm -rf --preserve-root %{buildroot}

mkdir -p %{buildroot}%{_libdir}/fig2dev
mkdir -p %{buildroot}%{_libdir}/xfig
make install \
    DESTDIR=%{buildroot} \
    XFIGLIBDIR=%{_libdir}/xfig \
    FIG2DEV_LIBDIR=%{_libdir}/fig2dev    
make install.man \
    DESTDIR=%{buildroot} \
    XFIGLIBDIR=%{_libdir}/xfig \
    FIG2DEV_LIBDIR=%{_libdir}/fig2dev

# documentation
pushd doc/manual
%make
latex manual
latex manual
popd

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES LATEX.AND.XFIG* NOTES README doc/manual/manual.dvi
%{_bindir}/fig2dev
%{_bindir}/fig2ps2tex
%{_bindir}/fig2ps2tex.sh
%{_bindir}/pic2tpic
%{_bindir}/transfig
%{_mandir}/man1/*
%{_libdir}/fig2dev
%{_libdir}/xfig

%changelog
* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.5-9m)
- update 3.2.5d

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.5-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.5-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.5-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.5-2m)
- rebuild against gcc43

* Sat Jun 02 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.5-1m)
- update to 3.2.5

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2.4-6m)
- revised installdir

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.4-5m)
- add Patch1: transfig-3.2.4-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.4-4m)
- revised spec for rpm 4.2.

* Tue Nov 25 2003 zunda <zunda at freeshell.org>
- (3.2.4-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Apr 29 2003 OZAWA Sakuro <crouton@ex-machina.jp>
- (3.2.4-2m)
- remove csh dependency by removing fig2ps2tex (csh dependent script),
  and renaming fig2ps2tex.sh (sh equivalent) to fig2ps2tex.

* Wed Feb 26 2003 Kenta MURATA <muraken2@nifty.com>.
- (3.2.4-1m)
- version up.
- included LaTeX manual.

* Sun Jul 28 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (3.2.3d-7m)
- m
- Requires: cmpskit

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.2.3d-6k)
- rebuild against libpng 1.2.2.

* Sat Dec 15 2001 Toru Hoshina <t@kondara.org>
- (3.2.3d-4k)
- bug fix. See jitterbug #991.

* Wed Nov  7 2001 Toru Hoshina <toru@df-usa.com>
- (3.2.3d-2k)
- version up.

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- (3.2.1-10k)
- add alphaev5 support.

* Fri Nov 24 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.2.1-9k)
- added XFIGLIBDIR.patch for resolving BITMAPDIR problems
- in addition, modified spec file

* Mon Oct  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.2.1-8k)
- BITMAPDIR is changed to /usr/X11R6/share/xfig/bitmaps

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (3.2.1-4).

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Wed Dec 30 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Tue Jul  7 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.2.1.

* Sat Jun 27 1998 Jeff Johnson <jbj@redhat.com>
- add %clean.

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Nov 13 1997 Otto Hammersmith <otto@redhat.com>
- fixed problem with Imakefile for fig2dev not including $(XLIB)
- build rooted.

* Fri Oct 24 1997 Otto Hammersmith <otto@redhat.com>
- recreated the glibc patch that is needed for an alpha build, missed it
  building on the intel.

* Tue Oct 21 1997 Otto Hammersmith <otto@redhat.com>
- updated version
- fixed source url

* Fri Jul 18 1997 Erik Troan <ewt@redhat.com>
- built against glibc
