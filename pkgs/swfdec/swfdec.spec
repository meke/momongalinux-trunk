%global momorel 6

%define	major_version	0.9
%define liboil_version	0.3.1
%define	gtk2_version	2.8.0
%define pango_version	1.16

Name:		swfdec
Version:	%{major_version}.2
Release:	%{momorel}m%{?dist}
Summary:	Flash animation rendering library

Group:		System Environment/Libraries
License:	LGPLv2+
URL:		http://swfdec.freedesktop.org/
Source0:	http://swfdec.freedesktop.org/download/%{name}/%{major_version}/%{name}-%{version}.tar.gz
#NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	alsa-lib-devel
BuildRequires:	glib2-devel >= 2.16
BuildRequires:	gstreamer-devel >= 0.10.11
BuildRequires:	gstreamer-plugins-base-devel >= 0.10.15
BuildRequires:	gtk2-devel >= %{gtk2_version}
BuildRequires:	liboil-devel >= %{liboil_version}
BuildRequires:	libsoup-devel >= 2.4.1-2m
BuildRequires:	pango-devel >= %{pango_version}

Requires(pre):	/sbin/ldconfig
Requires(post):	/sbin/ldconfig

%description
swfdec is a library for rendering Adobe Flash animations. Currently it handles
most Flash 3, 4 and many Flash 7 videos. 

%package	devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	liboil-devel >= %{liboil_version}
Requires:	pango-devel >= %{pango_version}
Requires:	pkgconfig

%description	devel
%{name}-devel contains the files needed to build packages that depend on
swfdec.

%package	gtk
Summary:	A library for easy embedding of Flash files in an application
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires(pre):	/sbin/ldconfig
Requires(post):	/sbin/ldconfig

%description	gtk
%{name}-gtk is a library for developers that allows one to easily embed
Flash videos and animations into their appplications. 

%package	gtk-devel
Summary:	Development files for swfdec-gtk
Group:		Development/Libraries
Requires:	%{name}-gtk = %{version}-%{release}
Requires:	%{name}-devel = %{version}-%{release}
Requires:	gtk2-devel >= %{gtk2_version}

%description	gtk-devel
%{name}-gtk is a library for developers that allows one to easily embed
Flash videos and animations into their appplications. This package contains
files necessary to build packages and appplications that use %{name}-gtk.

%prep
%setup -q
		
%build
%configure --disable-static \
    --enable-gtk \
    --enable-mad \
    --enable-ffmpeg \
    --enable-gstreamer \
    --disable-gtk-doc \
    --with-audio=alsa

# remove rpath from libtool
sed -i.rpath 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i.rpath 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install INSTALL='install -p'

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%post gtk
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun	-p /sbin/ldconfig

%postun gtk
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README 
%{_libdir}/libswfdec-%{major_version}.so.*
%exclude %{_libdir}/libswfdec-%{major_version}.la

%files	devel
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/%{name}/
%{_libdir}/pkgconfig/%{name}-%{major_version}.pc
%{_libdir}/libswfdec-%{major_version}.so
%dir %{_includedir}/%{name}-%{major_version}
%{_includedir}/%{name}-%{major_version}/swfdec/

%files	gtk
%defattr(-,root,root,-)
%{_libdir}/libswfdec-gtk-%{major_version}.so.*
%exclude %{_libdir}/libswfdec-gtk-%{major_version}.la

%files	gtk-devel
%defattr(-,root,root,-)
%{_libdir}/libswfdec-gtk-%{major_version}.so
%{_libdir}/pkgconfig/%{name}-gtk-%{major_version}.pc
%{_includedir}/%{name}-%{major_version}/swfdec-gtk/

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-1m)
- update to 0.8.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-2m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-2m)
- rebuild against gnutls-2.4.1 (libsoup)

* Thu Jun 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-2m)
- rebuild against x264
- --disable-gtkdoc(gtk-doc link old lirary...)

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4
- Secrity fix

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- import from Fedora

* Wed Feb 20 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.6.0-1
- Update to 0.6.0.
- Bump minimum version of gstreamer needed.
- Add BR for gstreamer-plugins-base-devel.

* Thu Feb 14 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.5.90-3
- Rebuild for new libsoup.

* Fri Feb  8 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.5.90-2
- Rebuild for gcc-4.3.

* Tue Jan 29 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.5.90-1
- Update to 0.5.90.
- Bump BR minimum versions for libsoup & pango.

* Wed Dec 19 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.5-2
- Build w/ pulse audio support.

* Mon Dec 17 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.5-1
- Update to 0.5.5.

* Fri Nov 16 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.4-2
- Add requires for pango-devel to devel pkg.
- Keep timestamp on installed files.

* Thu Nov 15 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.4-1
- Update to 0.5.4.
- Use valid license tag.
- Remove BR on ffmpeg & libmad, and only build gstreamer backend.

* Fri Oct 12 2007  Peter Gordon <peter@thecodergeek.com> - 0.5.3-1
- Update to new upstream release (0.5.3)

* Wed Oct 10 2007  Peter Gordon <peter@thecodergeek.com> - 0.5.2-1
- Update to new upstream release (0.5.2)

* Wed Aug 15 2007  Peter Gordon <peter@thecodergeek.com> - 0.5.1-1
- Update to new upstream release (0.5.1)

* Thu Jul  5 2007 kwizart <kwizart at gmail.com> - 0.4.5-1
- Update to 0.4.5 (bugfix)
- Add BR ffmpeg-devel libmad-devel (enabled in configure)
- Remove rpath (libtool method)

* Sat Apr 28 2007 Peter Gordon <peter@thecodergeek.com> - 0.4.4-1
- Update to new upstream release (0.4.4), which adds two new subpackages:
  swfdec-gtk and swfdec-gtk-devel.

* Sun Mar 25 2007 Peter Gordon <peter@thecodergeek.com> - 0.4.3-2
- Add js-devel to the BuildRequires to fix compilation in Mock.
  (Thanks to  Julian Sikorski; Livna bug #1453) 

* Sat Mar 24 2007 Peter Gordon <peter@thecodergeek.com> - 0.4.3-1
- Update to new upstream release (0.4.3), with lots of spec cleanups
- Spec file based heavily on Thomas Vander Stichele's 0.3.6 stuff.

* Sun Dec 03 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.3.6-0.gst.2
- fix pre/post scripts

* Sun Dec 03 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.3.6-0.gst.1
- new upstream
- remove swf_play
- add js-devel and gimp-devel buildrequires
- add gimp plugin

* Fri Jun 24 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.3.5-0.gst.1
- updated to new upstream

* Tue May 17 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.3.4-0.gst.1
- updated to new upstream

* Thu Mar 03 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.3.2-0.lvn.1
- updated to new liboil and upstream release

* Thu Nov 11 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- new upstream release

* Thu May 20 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.2.2-0.lvn.2
- require gcc-c++ for libtool
- fix pre/post req
- fix gtk loaders location
- work around FC2 packaging bug for SDL-devel

* Tue Mar 02 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.2.2-0.lvn.1: updated for rpm.livna.us (without mozilla plugin)

* Mon May 19 2003 Thomas Vander Stichele <thomas at apestaart dot org>
- Updated for 0.2.2

* Wed Feb 05 2003 Christian F.K. Schaller <Uraeus@linuxrising.org>
- Update spec to handle pixbuf loader
* Sat Oct 26 2002 Christian F.K. Schaller <Uraeus@linuxrising.org>
- First attempt at spec
