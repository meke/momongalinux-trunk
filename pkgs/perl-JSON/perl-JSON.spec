%global         momorel 3

Name:           perl-JSON
Version:        2.90
Release:        %{momorel}m%{?dist}
Summary:        JSON (JavaScript Object Notation) encoder/decoder
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/JSON/
Source0:        http://www.cpan.org/authors/id/M/MA/MAKAMAKA/JSON-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-JSON-XS >= 3.01
BuildRequires:  perl-Test-Simple
Requires:       perl-JSON-XS >= 3.01
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-JSON-PC

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
************************** CAUTION ********************************
* This is 'JSON module version 2' and there are many differences  *
* to version 1.xx                                                 *
* Please check your applications useing old version.              *
*   See to 'INCOMPATIBLE CHANGES TO OLD VERSION'                  *
*******************************************************************

%prep
%setup -q -n JSON-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(JSON::backportPP)/d'

EOF
%define __perl_requires %{_builddir}/JSON-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changes README
%{perl_vendorlib}/JSON
%{perl_vendorlib}/JSON.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.90-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.90-2m)
- rebuild against perl-5.18.2

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.90-1m)
- update to 2.90

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.61-1m)
- update to 2.61

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.59-1m)
- update to 2.59

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.58-2m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.58-1m)
- update to 2.58
- rebuild against perl-5.18.0

* Sat Apr  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.57-1m)
- update to 2.57

* Sat Apr  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.56-1m)
- update to 2.56

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.55-1m)
- update to 2.55

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.53-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.53-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.53-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.53-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.53-2m)
- rebuild against perl-5.14.2

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.53-1m)
- update to 2.53

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.52-2m)
- rebuild against perl-5.14.1

* Sun May 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.52-1m)
- update to 2.52

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.51-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.51-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.51-1m)
- update to 2.51

* Mon Dec 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.50-1m)
- update to 2.50

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.27-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.27-1m)
- update to 2.27

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.26-1m)
- update to 2.26

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.24-1m)
- update to 2.24
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-1m)
- update to 2.22

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.21-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-2m)
- rebuild against perl-5.12.0

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.21-1m)
- update to 2.21

* Sat Apr  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-1m)
- update to 2.20

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19-1m)
- update to 2.19

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.18-1m)
- update to 2.18
- Specfile re-generated by cpanspec 1.78.

* Thu Jan  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17-1m)
- update to 2.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-1m)
- update to 2.16

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-2m)
- rebuild against perl-5.10.1

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-1m)
- update to 2.15

* Wed Feb 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-1m)
- update to 2.14

* Sun Feb 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-1m)
- update to 2.13

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-3m)
- perl-JSON-PC was OBSOLETED

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.12-2m)
- rebuild against rpm-4.6

* Wed Jul 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-1m)
- update to 2.12

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Mon Apr 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-1m)
- update to 2.09

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-1m)
- update to 2.08

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.07-2m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 0.27

* Sun Feb 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-1m)
- update to 2.06

* Sat Jan  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Fri Jan  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Wed Dec 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02

* Sat Dec 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Thu Nov 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-2m)
- use vendor

* Sun Apr 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Wed Mar 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Tue Mar 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.07-1m)
- spec file was autogenerated
