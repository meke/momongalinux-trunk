%global momorel 1

Name:       cachefilesd
Version:    0.10.5
Release:    %{momorel}m%{?dist}
Summary:    CacheFiles userspace management daemon
Group:      System Environment/Daemons
License:    GPLv2+
URL:  		http://people.redhat.com/~dhowells/fscache/
Source0:    http://people.redhat.com/dhowells/fscache/cachefilesd-%{version}.tar.bz2
#0.10.5 was non-compressed
#NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automake, autoconf
BuildRequires: systemd-units
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires: selinux-policy-base

%description
The cachefilesd daemon manages the caching files and directory that are
that are used by network filesystems such a AFS and NFS to  
do persistent caching to the local disk.

%prep
%setup -q

%build
%ifarch s390 s390x sparcv9 sparc64
PIE="-fPIE"
%else
PIE="-fpie"
%endif
export PIE
CFLAGS="`echo $RPM_OPT_FLAGS $ARCH_OPT_FLAGS $PIE`"

make all


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_mandir}/{man5,man8}
mkdir -p %{buildroot}%{_localstatedir}/cache/fscache
make DESTDIR=%{buildroot} install

install -m 644 cachefilesd.conf %{buildroot}%{_sysconfdir}
install -m 644 cachefilesd.service %{buildroot}%{_unitdir}/cachefilesd.service

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable cachefilesd.service > /dev/null 2>&1 || :
    /bin/systemctl stop cachefilesd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart cachefilesd.service >/dev/null 2>&1 || :
fi

%files
%doc README
%doc howto.txt
%doc selinux/move-cache.txt
%doc selinux/*.fc
%doc selinux/*.if
%doc selinux/*.te
%config(noreplace) %{_sysconfdir}/cachefilesd.conf
/sbin/*
%{_unitdir}/*
%{_mandir}/*/*
%{_localstatedir}/cache/fscache

%changelog
* Fri Apr 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.5-1m)
- update 0.10.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.1-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-4m)
- apply glibc212 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-2m)
- move startup script from %%{_initrddir} to %%{_initscriptdir}

* Wed May 13 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9-1m)
- sync Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-2m)
- %%NoSource -> NoSource

* Wed May 16 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.7-1m)
- welcome to Momonga
