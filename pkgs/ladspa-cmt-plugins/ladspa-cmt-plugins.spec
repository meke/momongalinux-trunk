%global momorel 6

Summary: Computer Music Toolkit plugins for LADSPA.
Name: ladspa-cmt-plugins
Version: 1.16
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Multimedia
URL: http://www.ladspa.org/cmt/
Source0:  http://www.ladspa.org/download/cmt_src_%{version}.tgz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
BuildRequires: ladspa-devel

%description
The Computer Music Toolkit (CMT) is a collection of LADSPA plugins for use with software synthesis and recording packages on Linux. See the license before use.

The CMT is developed primarily by Richard W.E. Furse the principle designer of the LADSPA standard, with additional plugins by Jezar and David Bartold.

%prep
%setup -q -n cmt

%build
cd src
%make
cd ../

%install
cd plugins
install -d %{buildroot}%{_libdir}/ladspa
install -m 755 cmt.so %{buildroot}%{_libdir}/ladspa
cd ../

%files
%defattr(-,root,root)
%doc README doc
%{_libdir}/ladspa/cmt.so

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.16-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.16-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.16-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.16-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.16-1m)
- Initial Build for Momonga Linux


