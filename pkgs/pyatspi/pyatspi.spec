%global momorel 2

%global python_sitelib  %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: at-spi python binding
Name: pyatspi
Version: 2.5.5
Release: %{momorel}m%{?dist}
URL: http://developer.gnome.org/projects/gap/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.5/%{name}-%{version}.tar.xz
NoSource: 0

License: LGPLv2+
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 
Requires: at-spi

BuildRequires: dbus-devel
BuildRequires: glib2-devel
BuildRequires: dbus-glib-devel
BuildRequires: libxml2-devel
BuildRequires: atk-devel
BuildRequires: gtk3-devel
BuildRequires: pygobject >= 2.90.1
BuildArch:     noarch
Obsoletes: at-spi-python < %{version}
Provides: at-spi-python = %{version}

%description
at-spi python binding

%prep
%setup -q

%build
%configure --enable-gtk-doc --disable-schemas-install
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING README
%{python_sitelib}/%{name}

%changelog
* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.5-1m)
- update to 2.5.5

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (2.1.91-2m)
- add BuildRequires

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.91-1m)
- update to 2.1.91

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-2m)
- obsoletes at-spi

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.8-2m)
- full rebuild for mo7 release

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.8-1m)
- update to 0.1.8

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7

* Sun Feb 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.6-2m)
- fix build failure on x86_64
- set BuildArch: noarch

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.6-1m)
- initial build
