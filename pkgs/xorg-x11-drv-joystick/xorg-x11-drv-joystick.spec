%global momorel 1
%define tarball xf86-input-joystick
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/input

Summary:   Xorg X11 joystick input driver
Name:      xorg-x11-drv-joystick
Version: 1.6.1
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64 alpha sparc sparc64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 joystick input driver.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{tarball}-%{version}

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -regex ".*\.la$" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/joystick_drv.so
%{_mandir}/man4/joystick.4*

%files devel
%defattr(-,root,root,-)
%{_includedir}/xorg/joystick-properties.h
%{_libdir}/pkgconfig/xorg-joystick.pc

%changelog
* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-2m)
- rebuild against xorg-x11-server-1.13.0

* Sat Mar 17 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-1m)
- update 1.6.1

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update 1.6.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-6m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.0-5m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- update 1.5.0

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- update 1.4.1

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-2m)
- rebuild against xorg-x11-server 1.6

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update 1.4.0
- add devel package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-2m)
- rebuild against rpm-4.6

* Tue Sep  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-1m)
- update 1.3.3

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-1m)
- update 1.3.2
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- %%NoSource -> NoSource

* Thu Dec 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Mon Oct 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-2m)
- rebuild against xorg-x11-server-1.4

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sat May  5 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0.5-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0.5-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0.5-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.0.5-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.0.5-1
- Updated xorg-x11-drv-joystick to version 1.0.0.5 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.4-1
- Updated xorg-x11-drv-joystick to version 1.0.0.4 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.2-1
- Updated xorg-x11-drv-joystick to version 1.0.0.2 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.1-1
- Updated xorg-x11-drv-joystick to version 1.0.0.1 from X11R7 RC1
- Fix *.la file removal.

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-0
- Initial spec file for joystick input driver generated automatically
  by my xorg-driverspecgen script.
