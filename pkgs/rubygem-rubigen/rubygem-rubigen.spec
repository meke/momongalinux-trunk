# Generated from rubigen-1.5.8.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname rubigen

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: A framework to allow Ruby applications to generate file/folder stubs  (like the `rails` command does for Ruby on Rails, and the 'script/generate' command within a Rails application during development)
Name: rubygem-%{gemname}
Version: 1.5.8
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://drnic.github.com/rubigen
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(activesupport) >= 2.3.5
Requires: rubygem(activesupport) < 3.4.0
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
RubiGen - Ruby Generator Framework


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/install_rubigen_scripts
%{_bindir}/rubigen
%{_bindir}/ruby_app
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.8-2m)
- change activesupport Requrest version

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.8-1m)
- update 1.5.8

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.6-1m)
- update 1.5.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.5-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.5-1m)
- update 1.5.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-1m)
- Initial package for Momonga Linux
