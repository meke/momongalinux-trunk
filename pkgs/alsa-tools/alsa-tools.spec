%global momorel 1
%global libver 1.0.28
%global utilsver 1.0.28


%ifarch ppc ppc64
# sb16_csp doesn't build on PPC; see bug #219010
%{?!_without_tools:     %global builddirstools as10k1 echomixer envy24control hdspconf hdspmixer hwmixvolume rmedigicontrol sbiload sscape_ctl us428control hda-verb hdajackretask }
%else
%{?!_without_tools:     %global builddirstools as10k1 echomixer envy24control hdspconf hdspmixer hwmixvolume rmedigicontrol sbiload sb16_csp sscape_ctl us428control hda-verb hdajackretask }
%endif

%{?!_without_firmware:  %global builddirsfirmw hdsploader mixartloader usx2yloader vxloader }

Summary: Specialist tools for ALSA
Name: alsa-tools
Version: 1.0.28
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.alsa-project.org/
Source0: ftp://ftp.alsa-project.org/pub/tools/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: envy24control.desktop
Source2: envy24control.png
Source3: echomixer.desktop
Source4: echomixer.png
Source5: 90-alsa-tools-firmware.rules
# Resized version of public domain clipart found here:
# http://www.openclipart.org/detail/17428
Source6: hwmixvolume.png
Source7: hwmixvolume.desktop
Source9: hdajackretask.desktop
Patch0: %{name}-1.0.18-usx2yloader-udev.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: alsa-utils >= %{utilsver}
Requires: python-alsa
BuildRequires: alsa-lib-devel >= %{libver}
BuildRequires: autoconf
BuildRequires: automake
%if 0%{!?_without_tools:1}
BuildRequires: gtk+1-devel
BuildRequires: gtk2-devel
BuildRequires: fltk-devel >= 1.3.2
%endif

%description
This package contains several specialist tools for use with ALSA, including
a number of programs that provide access to special hardware facilities on
certain sound cards.

* as10k1 - AS10k1 Assembler
%ifnarch ppc ppc64
* cspctl - Sound Blaster 16 ASP/CSP control program
%endif
* echomixer - Mixer for Echo Audio (indigo) devices
* envy24control - Control tool for Envy24 (ice1712) based soundcards
* hdspmixer - Mixer for the RME Hammerfall DSP cards
* rmedigicontrol - Control panel for RME Hammerfall cards
* sbiload - An OPL2/3 FM instrument loader for ALSA sequencer
* sscape_ctl - ALSA SoundScape control utility
* us428control - Control tool for Tascam 428

%package firmware
Summary: ALSA tools for uploading firmware to some soundcards
Group: Applications/System
Requires: alsa-firmware
Requires: fxload
Requires: udev >= 151

%description firmware
This package contains tools for flashing firmware into certain sound cards.
The following tools are available:

* hdsploader   - for RME Hammerfall DSP cards
* mixartloader - for Digigram miXart soundcards
* vxloader     - for Digigram VX soundcards
* usx2yloader  - second phase firmware loader for Tascam USX2Y USB soundcards

%prep
%setup -q

%patch0 -p1 -b .usx2yudev

# for Patch0
pushd usx2yloader
autoreconf
popd

%build
mv seq/sbiload . ; rm -rf seq
for i in %{?builddirstools:%builddirstools} %{?builddirsfirmw:%builddirsfirmw}
do
  cd $i ; %configure
  %{__make} %{?_smp_mflags} || exit 1
  cd ..
done

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{_datadir}/{applications,pixmaps}

for i in %{?builddirstools:%builddirstools} %{?builddirsfirmw:%builddirsfirmw}
do
  case $i in
    echomixer)
      (cd $i ; %makeinstall ; install -m 644 %{SOURCE4} %{buildroot}%{_datadir}/pixmaps/ ; install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/applications/ ) || exit 1
      ;;
    envy24control)
      (cd $i ; %makeinstall ; install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/pixmaps/ ; install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/ ) || exit 1
      ;;
    hdspconf)
      (cd $i ; %makeinstall pixmapdir=%{buildroot}%{_datadir}/pixmaps desktopdir=%{buildroot}%{_datadir}/applications ) || exit 1
      ;;
    hdspmixer)
      (cd $i ; %makeinstall pixmapdir=%{buildroot}%{_datadir}/pixmaps desktopdir=%{buildroot}%{_datadir}/applications ) || exit 1
      ;;
    hwmixvolume)
      (cd $i ; %makeinstall ; install -m 644 %{SOURCE6} %{buildroot}%{_datadir}/pixmaps/ ; install -m 644 %{SOURCE7} %{buildroot}%{_datadir}/applications/ ) || exit 1
      ;;
    usx2yloader)
      (cd $i ; %makeinstall hotplugdir=%{buildroot}%{_sysconfdir}/hotplug/usb) || exit 1
      ;;
    hdajackretask)
      (cd $i ; %makeinstall ; install -m 644 %{SOURCE9} %{buildroot}%{_datadir}/applications/ ) || exit 1
      ;;
    *) (cd $i ; %makeinstall) || exit 1
   esac
   if [[ -s "${i}"/README ]]
   then
      if [[ ! -d "%{buildroot}%{_docdir}/%{name}-%{version}/${i}" ]]
      then
         mkdir -p "%{buildroot}%{_docdir}/%{name}-%{version}/${i}"
      fi
      cp "${i}"/README "%{buildroot}%{_docdir}/%{name}-%{version}/${i}"
   fi
   if [[ -s "${i}"/COPYING ]]
   then
      if [[ ! -d "%{buildroot}%{_docdir}/%{name}-%{version}/${i}" ]]
      then
         mkdir -p "%{buildroot}%{_docdir}/%{name}-%{version}/${i}"
      fi
      cp "${i}"/COPYING "%{buildroot}%{_docdir}/%{name}-%{version}/${i}"
   fi
   if [[ -s %{buildroot}%{_datadir}/applications/${i}.desktop ]] ; then
      desktop-file-install --dir %{buildroot}%{_datadir}/applications \
        %{buildroot}%{_datadir}/applications/${i}.desktop
   fi
done

# convert hotplug stuff to udev
rm -f %{buildroot}%{_sysconfdir}/hotplug/usb/tascam_fw.usermap
mkdir -p %{buildroot}/lib/udev
mv %{buildroot}%{_sysconfdir}/hotplug/usb/* %{buildroot}/lib/udev
mkdir -p %{buildroot}/lib/udev/rules.d
install -m 644 %{SOURCE5} %{buildroot}/lib/udev/rules.d

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%if 0%{!?_without_tools:1}
%files
%defattr(-,root,root,-)
%dir %{_docdir}/%{name}-%{version}
%doc %{_docdir}/%{name}-%{version}/as10k1
%doc %{_docdir}/%{name}-%{version}/echomixer
%doc %{_docdir}/%{name}-%{version}/envy24control
%doc %{_docdir}/%{name}-%{version}/hdspconf
%doc %{_docdir}/%{name}-%{version}/hdspmixer
%doc %{_docdir}/%{name}-%{version}/hwmixvolume
%doc %{_docdir}/%{name}-%{version}/rmedigicontrol
%doc %{_docdir}/%{name}-%{version}/sbiload
%doc %{_docdir}/%{name}-%{version}/hda-verb
%doc %{_docdir}/%{name}-%{version}/hdajackretask
%{_bindir}/as10k1
%{_bindir}/echomixer
%{_bindir}/envy24control
%{_bindir}/hdspconf
%{_bindir}/hdspmixer
%{_bindir}/hwmixvolume
%{_bindir}/rmedigicontrol
%{_bindir}/sbiload
%{_bindir}/sscape_ctl
%{_bindir}/us428control
%{_bindir}/hda-verb
%{_bindir}/hdajackretask
%{_datadir}/applications/echomixer.desktop
%{_datadir}/applications/envy24control.desktop
%{_datadir}/applications/hdspconf.desktop
%{_datadir}/applications/hdspmixer.desktop
%{_datadir}/applications/hwmixvolume.desktop
%{_datadir}/applications/hdajackretask.desktop
%{_datadir}/pixmaps/echomixer.png
%{_datadir}/pixmaps/envy24control.png
%{_datadir}/pixmaps/hdspconf.png
%{_datadir}/pixmaps/hdspmixer.png
%{_datadir}/pixmaps/hwmixvolume.png
%{_datadir}/sounds/opl3
%{_mandir}/man1/envy24control.1*

# sb16_csp stuff which is excluded for PPCx
%ifnarch ppc ppc64
%doc %{_docdir}/%{name}-%{version}/sb16_csp
%{_bindir}/cspctl
%{_mandir}/man1/cspctl.1*
%endif

%endif

%if 0%{!?_without_firmware:1}
%files firmware
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}/hdsploader
%doc %{_docdir}/%{name}-%{version}/mixartloader
%doc %{_docdir}/%{name}-%{version}/usx2yloader
%doc %{_docdir}/%{name}-%{version}/vxloader
/lib/udev/rules.d/*.rules
/lib/udev/tascam_fpga
/lib/udev/tascam_fw
%{_bindir}/hdsploader
%{_bindir}/mixartloader
%{_bindir}/usx2yloader
%{_bindir}/vxloader
%endif

%changelog
* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.28-1m)
- update to 1.0.28

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.27-1m)
- update to 1.0.27

* Sun Dec 30 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.26.1-2m)
- rebuild against fltk-1.3.2

* Thu Sep 27 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.26.1-1m)
- update to 1.0.26.1

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.25-1m)
- version 1.0.25

* Sat Jun 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.24.1-1m)
- version 1.0.24.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.23-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.23-1m)
- version 1.0.23
- build hwmixvolume
- import hwmixvolume.desktop and hwmixvolume.png from Fedora
- Requires: fxload and python-alsa

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.22-2m)
- update 55-alsa-tascam-firmware-loaders.rules for udev-151

* Thu Dec 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.22-1m)
- update 1.0.22

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-2m)
- apply glibc210 patch
-- http://article.gmane.org/gmane.linux.alsa.devel/63326

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.20-1m)
- version 1.0.20

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.18-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.18-1m)
- version 1.0.18
- update usx2yloader-udev.patch
- License: GPLv2+

* Mon Aug 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-1m)
- version 1.0.17

* Mon Aug 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.16-1m)
- import from Fedora
- a patch and an udev file are imported from cooker

* Mon May 19 2008 Tim Jackson <rpm@timj.co.uk> - 1.0.16-4
- Make it build cleanly on ppc and ppc64 by excluding sb16_csp

* Sun May 18 2008 Tim Jackson <rpm@timj.co.uk> - 1.0.16-3
- Really enable firmware subpackage

* Sun May 18 2008 Tim Jackson <rpm@timj.co.uk> - 1.0.16-2.fc9.1
- Release number bump for administrative reasons

* Sun May 18 2008 Tim Jackson <rpm@timj.co.uk> - 1.0.16-2
- Enable firmware subpackage - the accompanying alsa-firmware package is
  finally in Fedora

* Sat Mar 01 2008 Tim Jackson <rpm@timj.co.uk> - 1.0.16-1
- Update to upstream 1.0.16 (fixes #434473)

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.15-3
- Autorebuild for GCC 4.3

* Sat Jan 05 2008 Tim Jackson <rpm@timj.co.uk> - 1.0.15-2
- Update License tag to GPLv2+
- ExcludeArch ppc64 (bug #219010)

* Sat Jan 05 2008 Tim Jackson <rpm@timj.co.uk> - 1.0.15-1
- Update to upstream 1.0.15
- Add icon for envy24control
- Build echomixer

* Sat Dec 09 2006 Tim Jackson <rpm@timj.co.uk> - 1.0.12-4
- ExcludeArch ppc (#219010)

* Sun Nov 26 2006 Tim Jackson <rpm@timj.co.uk> - 1.0.12-3
- Add gtk2-devel BR

* Sun Nov 26 2006 Tim Jackson <rpm@timj.co.uk> - 1.0.12-2
- Own our docdir explicitly

* Sat Nov 25 2006 Tim Jackson <rpm@timj.co.uk> - 1.0.12-1
- Update to 1.0.12
- Resubmit to Fedora Extras 6
- Replace hotplug requirement with udev

* Mon Feb 13 2006 Thorsten Leemhuis <fedora[AT]leemhuis.info>
- Rebuild for Fedora Extras 5

* Tue Dec 06 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 1.0.10-1
- Update to 1.0.10

* Fri May 06 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 1.0.9-1
- Update to 1.0.9
- Use disttag
- Remove gcc4 patch

* Fri May 06 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 1.0.8-3
- prune ac3dec from sources

* Thu May 05 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 1.0.8-2
- don't build ac3dec -- use a52dec instead

* Wed Apr 06 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 1.0.8-1
- Update to 1.0.8

* Sun Mar 29 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0:1.0.6-2
- Add "--without tools" and "--with firmware" options
- Drop unneeded BR: automake

* Sun Jan 02 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0:1.0.6-0.fdr.1
- Update to 1.0.6 for FC3
- add new files in {_datadir}/sounds/
- add patch0 for as10k1 

* Sat Apr 03 2004 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0:1.0.4-0.fdr.1
- Update to 1.0.4

* Fri Jan 16 2004 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0:1.0.1-0.fdr.2
- Integrate Michaels patch that fixes:
- fix desktop files for fedora.us, adds buildreq desktop-file-utils
- fix %%install section for short-circuit installs

* Fri Jan 09 2004 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0:1.0.1-0.fdr.1
- Update to 1.0.1

* Sun Dec 14 2003 Thorsten Leemhuis <fedora[AT]leemhuis.info> 1.0.0-0.fdr.0.3.rc2
- exit if error during build or install
- fix install errors hdspconf, hdspmixer, usx2yloader
- Split package in alsa-tools and alsa-tools-firmware
- Integrate more docs

* Fri Dec 06 2003 Thorsten Leemhuis <fedora[AT]leemhuis.info> 1.0.0-0.fdr.0.2.rc2
- Update to 1.0.0rc2
- some minor corrections in style

* Thu Dec 04 2003 Thorsten Leemhuis <fedora[AT]leemhuis.info> 1.0.0-0.fdr.0.1.rc1
- Update to 1.0.0rc1
- Remove firmware files -- extra package now.
- Add description

* Wed Aug 13 2003 Dams <anvil[AT]livna.org> 0:tools-0.fdr.1
- Initial build.
