%global momorel 1
Name:		eekboard
Version:	1.0.7
Release: %{momorel}m%{?dist}
Summary:	Yet Another Virtual Keyboard

Group:		Applications/System
License:	GPLv3+
URL:		http://fedorahosted.org/eekboard/
Source0:	http://github.com/downloads/ueno/eekboard/eekboard-%{version}.tar.gz
NoSource: 0
Patch0:		eekboard-fix-crash.patch
BuildRequires:	libXtst-devel
BuildRequires:	gtk3-devel
BuildRequires:	libxklavier-devel
BuildRequires:	at-spi2-core-devel
BuildRequires:	intltool desktop-file-utils
BuildRequires:	gobject-introspection-devel
BuildRequires:	libcroco-devel
BuildRequires:	vala-tools

Provides:	eekboard-python = %{version}-%{release}
Obsoletes:	eekboard-python < 1.0.5-5

%description
eekboard is a virtual keyboard software package, including a set of
tools to implement desktop virtual keyboards.

%package libs
Summary:	Runtime libraries for eekboard
Group:		System Environment/Libraries
License:	LGPLv2+

%description libs
This package contains the libraries for eekboard

%package devel
Summary:	Development tools for eekboard
Group:		Development/Libraries
License:	LGPLv2+ and GFDL

Requires:	vala
Requires:	%{name}-libs = %{version}-%{release}

%description devel
This package contains the development tools for eekboard.


%prep
%setup -q
%patch0 -p1 -b .fix-crash


%build
%configure --disable-static --enable-atspi
%make 


%install
make install DESTDIR=%{buildroot} INSTALL="install -p"
rm -rf %{buildroot}%{_libdir}/*.la

# We don't install autostart file to avoid conflict with other OSK.
# Instead, install it under doc.
mkdir -p base-installed/examples
mv %{buildroot}%{_sysconfdir}/xdg/autostart/%{name}-autostart.desktop base-installed/examples

desktop-file-validate ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
if [ $1 -eq 0 ] ; then
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig


%files -f %{name}.lang
%doc base-installed/examples
%{_bindir}/eekboard-server
%{_bindir}/eekboard
%{_libexecdir}/eekboard-setup
%{_datadir}/dbus-1/services/eekboard-server.service
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/eekboard.png
%{_datadir}/icons/hicolor/scalable/apps/eekboard.svg
%{_datadir}/eekboard/
%{_datadir}/glib-2.0/schemas/*

%files libs
%doc AUTHORS COPYING README
%{_libdir}/libeek*.so.*
%{_libdir}/girepository-1.0/Eek*.typelib

%files devel
# LGPLv2+
%{_libdir}/libeek*.so
%{_includedir}/eek-0.90/
%{_includedir}/eekboard-0.90/
%{_datadir}/gir-1.0/Eek*.gir
%{_datadir}/vala/vapi/eek*.vapi
%{_datadir}/vala/vapi/eek*.deps
%{_libdir}/pkgconfig/eek*.pc
# GFDL
%{_datadir}/gtk-doc/html/*


%changelog
* Fri Jul 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.7-1m)
- import from fedora

