%global momorel 11
%global swigver 2.0.9
%global build_doc 0

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: Multi-platform PIM synchronization framework
Name: libopensync
Version: 0.39
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.opensync.org/
Group: System Environment/Libraries
Source0: http://opensync.org/download/releases/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.36-alt-python.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: swig = %{swigver}
BuildRequires: bison-devel
BuildRequires: cmake
BuildRequires: doxygen
BuildRequires: glib2-devel
BuildRequires: libxml2-devel
BuildRequires: pkgconfig
BuildRequires: python-devel >= 2.7
BuildRequires: sqlite-devel
BuildRequires: swig = %{swigver}
Obsoletes: libopensync-plugin-evolution2
Obsoletes: libopensync-plugin-gnokii
Obsoletes: libopensync-plugin-google-calendar
Obsoletes: libopensync-plugin-gpe
Obsoletes: libopensync-plugin-irmc
Obsoletes: libopensync-plugin-moto
Obsoletes: libopensync-plugin-opie
Obsoletes: libopensync-plugin-palm
Obsoletes: libopensync-plugin-python
Obsoletes: libopensync-plugin-sunbird
Obsoletes: multisync-gui

%description
OpenSync is a synchronization framework that is platform and distribution
independent.  It consists of several plugins that can be used to connect to
devices, a powerful sync-engine and the framework itself.  The synchronization
framework is kept very flexible and is capable of synchronizing any type of
data, including contacts, calendar, tasks, notes and files.

%package devel
Summary: Header files and static libraries from libopensync
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libopensync.

%package python
Summary: Python binding for libopensync
Group: Development/Libraries

%description python
Python bindings for libopensync

%prep
%setup -q

%patch0 -p2 -b .python

# need python-2.7
rm cmake/modules/FindPythonLibs.cmake

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
export CFLAGS=-DGLIB_COMPILATION
%cmake \
	-DOPENSYNC_LIBEXEC_DIR=%{_libexecdir} \
	-DCMAKE_SKIP_RPATH=YES \
	-DSWIG_DIR=%{_datadir}/swig/%{swigver}/ \
%if %{build_doc}
	-DBUILD_DOCUMENTATION:BOOL=ON \
%endif
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# directories for plugins
mkdir -p %{buildroot}%{_libdir}/%{name}1/formats
mkdir -p %{buildroot}%{_libdir}/%{name}1/plugins
mkdir -p %{buildroot}%{_libdir}/%{name}1/python-plugins
mkdir -p %{buildroot}%{_datadir}/%{name}1/defaults

mv %{buildroot}%{_libdir}/python{,2.7}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/osyncbinary
%{_bindir}/osyncdump
%{_bindir}/osyncplugin
%{_libdir}/%{name}1
%{_libdir}/%{name}.so.*
%{_libexecdir}/osplugin
%{_datadir}/%{name}1

%files devel
%defattr(-,root,root)
%doc CODING
%{_includedir}/%{name}1
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so

%files python
%defattr(-,root,root)
%{python_sitearch}/_opensync.so
%{python_sitearch}/opensync.py

%changelog
* Thu Mar 14 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-11m)
- rebuild against swig-2.0.9
- import libopensync-0.36-alt-python.patch from alt

* Sun Jul 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-10m)
- set Obsoletes: libopensync-plugin-evolution2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-9m)
- rebuild for glib 2.33.2

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.39-8m)
- build fix

* Tue Jun 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.39-7m)
- rebuild against swig-2.0.4

* Wed May  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.39-6m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.39-3m)
- full rebuild for mo7 release

* Sat Jan 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-2m)
- move OPENSYNC_PLUGINDIR from %%{_libdir}/opensync-1.0 to %%{_libdir}/%%{name}1
- move OPENSYNC_FORMATSDIR from %%{_datadir}/opensync-1.0/formats to %%{_libdir}/%%{name}1/formats
- move OPENSYNC_INCLUDE_DIR from %%{_includedir}/opensync-1.0 to %%{_includedir}/%%{name}1
- add ChangeLog to %%doc
- Obsoletes: few plugins and multisync-gui
- add a switch build_doc

* Thu Jan 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.39-1m)
- version 0.39

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-2m)
- rebuild against swig-1.3.39

* Fri Mar 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-1m)
- version 0.38

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-0.20081004.4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-0.20081004.3m)
- fix build on x86_64

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-0.20081004.2m)
- rebuild against python-2.6.1
- remove Obsoletes: multisync-gui

* Sat Oct  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-0.20081004.1m)
- update 0.38 svn snapshot (20081004)

* Thu Jun  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.36-3m)
- rebuild against swig-1.3.35

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.36-2m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.36-1m)
- version 0.36
- change License to LGPL
- remove python-lib-check-lib64.patch

* Sun May 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-3m)
- add a directory for plugins again

* Sun May 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-2m)
- add a directory for plugins

* Sun May 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-1m)
- initial package for kdepim-3.5.7
- import libopensync-python-lib-check-lib64.patch from cooker
- Summary and %%description are imported from cooker
