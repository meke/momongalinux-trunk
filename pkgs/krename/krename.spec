%global momorel 3
%global qtver 4.8.2
%global kdever 4.10.0
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Summary: A powerfull batch renamer for KDE
Name: krename
Version: 4.0.9
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/X
URL: http://www.krename.net/
Source0: http://dl.sourceforge.net/project/%{name}/KDE4%20%{name}-stable/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-4.0.8-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake >= 2.4.8-5m
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils
BuildRequires: exiv2-devel >= 0.23
BuildRequires: gettext
BuildRequires: libart_lgpl-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: taglib-devel
BuildRequires: zlib-devel

%description
Krename is a very powerful batch file renamer for KDE3 which can rename
a list of files based on a set of expressions. It can copy/move the files
to another directory or simply rename the input files.
Krename supports many conversion operations, including conversion of a 
filename to lowercase or to uppercase, conversion of the first letter
of every word to uppercase, adding numbers to filenames, finding and
replacing parts of the filename, and many more.
It can also change access and modification dates, permissions, and file
ownership.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
export CFLAGS="$RPM_OPT_FLAGS `pkg-config --cflags freetype2`"
export CXXFLAGS="$RPM_OPT_FLAGS `pkg-config --cflags freetype2`"
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop files
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --add-category X-KDE-Utilities-File \
  src/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README TODO
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/kde4/services/ServiceMenus/%{name}*.desktop
%{_kde4_iconsdir}/*/*/apps/%{name}.png
%{_datadir}/pixmaps/%{name}.png

%changelog
* Thu Feb 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.9-3m)
- fix build error

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.9-2m)
- rebuild against exiv2-0.23

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.9-1m)
- update to 4.0.9

* Sun Jan  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.8-1m)
- update to 4.0.8

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.7-4m)
- rebuild against exiv2-0.22

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.7-3m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.7-2m)
- rebuild against exiv2-0.21.1

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.7-1m)
- update to 4.0.7

* Thu Feb  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.6-1m)
- update to 4.0.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.5-2m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.5-1m)
- update to 4.0.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.4-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-2m)
- rebuild against qt-4.6.3-1m

* Fri Jun 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-4m)
- rebuild against exiv2-0.20

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-3m)
- touch up spec file

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-2m)
- rebuild against exiv2-0.19

* Mon Dec 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Sat Dec  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-1m)
- version 4.0.1
- update License
- remove needless install section of icons

* Wed Sep 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to 4.0.0

* Tue Jun  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.3-1m)
- update to 3.9.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-2m)
- rebuild against rpm-4.6

* Sun Oct 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.2-1m)
- update to 3.9.2

* Mon Jul 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.9.1-2m)
- install missing icons

* Sun Jul 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.9.1-1m)
- version 3.9.1
- install menu entry again
- add BPR: taglib-devel

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.0-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9.0-2m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.0-1m)
- update to 3.9.0 for KDE4

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.14-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.14-2m)
- %%NoSource -> NoSource

* Mon Mar 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.14-1m)
- version 3.0.14

* Mon Mar 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.13-2m)
- update desktop.patch (add Japanese message to krenameservicemenu.desktop)

* Fri Dec 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.13-1m)
- version 3.0.13

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.12-1m)
- version 3.0.12
- update desktop.patch
- good-bye desktop-file-utils

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.11-3m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m

* Wed Apr 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.11-2m)
- add X-KDE-Utilities-File to Categories of krename.desktop

* Wed Mar  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.11-1m)
- initial package for Momonga Linux (thanks, JW)
