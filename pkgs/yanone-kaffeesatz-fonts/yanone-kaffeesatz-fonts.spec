%global momorel 4

%global fontname yanone-kaffeesatz
%global fontconf 60-%{fontname}.conf

%global archivename YanoneKaffeesatz

Name:    %{fontname}-fonts
Version: 20100115
Release: %{momorel}m%{?dist}
Summary: Yanone Kaffeesatz decorative fonts

Group:     User Interface/X
License:   "CC-BY"
URL:       http://www.yanone.de/typedesign/kaffeesatz/
Source0:   %{url}%{archivename}.zip
Source1:   %{name}-fontconfig.conf
Source2:   %{url}%{archivename}.pdf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem

%description
Sans-serif decorative Latin OTF font by Jan Gerner, suitable for titles and
short runs of text.


%prep
%setup -q -c
install -m 0644 -p %{SOURCE2} .


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.otf

%doc *.pdf


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100115-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100115-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100115-2m)
- full rebuild for mo7 release

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100115-1m)
- update to 20100115

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080723-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080723-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20080723-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20080723-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080723-4
- prepare for F11 mass rebuild, new rpm and new fontpackages

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080723-3
- rpm-fonts renamed to fontpackages

* Fri Nov 14 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080723-2
- Rebuild using new rpm-font

* Mon Sep 1 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080723-1
- Upstream stealth update

* Fri Jul 11 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20061120-4
- Fedora 10 alpha general package cleanup

* Sun Nov 25 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20061120-3
- initial packaging
