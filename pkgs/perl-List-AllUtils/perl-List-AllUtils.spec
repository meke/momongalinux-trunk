%global         momorel 2

Name:           perl-List-AllUtils
Version:        0.08
Release:        %{momorel}m%{?dist}
Summary:        Combines List::Util and List::MoreUtils in one bite-sized package
License:        "Artistic 2.0"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/List-AllUtils/
Source0:        http://www.cpan.org/authors/id/D/DR/DROLSKY/List-AllUtils-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-base
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Temp
BuildRequires:  perl-List-MoreUtils >= 0.28
BuildRequires:  perl-List-Util >= 1.31
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Test-Warnings
Requires:       perl-base
Requires:       perl-List-MoreUtils >= 0.28
Requires:       perl-List-Util >= 1.31
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Are you sick of trying to remember whether a particular helper is defined
in List::Util or List::MoreUtils? I sure am. Now you don't have to
remember. This module will export all of the functions that either of those
two modules defines.

%prep
%setup -q -n List-AllUtils-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/List/AllUtils.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-2m)
- rebuild against perl-5.20.0

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-2m)
- rebuild against perl-5.18.2

* Wed Oct 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
