%global momorel 4

# Copyright (c) 2000-2008, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

Name:		backport-util-concurrent
Summary:	Backport of java.util.concurrent API, introduced in Java 5.0
Version:	3.1
Release:	%{momorel}m%{?dist}
URL:		http://backport-jsr166.sourceforge.net/
License:	Public Domain
Group:		Development/Libraries
Source0:        http://dl.sourceforge.net/project/backport-jsr166/backport-jsr166/3.1/backport-util-concurrent-%{version}-src.tar.bz2
NoSource:       0
Source1:	http://repo1.maven.org/maven2/backport-util-concurrent/backport-util-concurrent/3.1/backport-util-concurrent-3.1.pom

BuildRequires:	jpackage-utils >= 0:1.7.2
BuildRequires:	java-devel = 0:1.5.0
BuildRequires:	ant >= 0:1.6.5
BuildRequires:	junit
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:	noarch
Requires:	java >= 0:1.5.0
Requires:	jpackage-utils
Requires(post):		jpackage-utils >= 0:1.7.2
Requires(postun):	jpackage-utils >= 0:1.7.2

BuildRoot:		%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package is the backport of java.util.concurrent API, introduced in
Java 5.0, to Java 1.4. The backport is based on public-domain sources
from the JSR 166 CVS repository, and the dl.util.concurrent package.

%package javadoc
Group:			Documentation
Summary:		Javadoc for %{name}

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n %{name}-%{version}-src

find . -name '*.?ar' | xargs rm -f

build-jar-repository -s -p external \
		junit

%build
unset CLASSPATH
ant dist test

%install
rm -rf $RPM_BUILD_ROOT

# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 backport-util-concurrent-dist/%{name}.jar \
		$RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar \
		$RPM_BUILD_ROOT%{_javadir}/%{name}.jar


# pom
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP-%{name}.pom

%add_to_maven_depmap %{name} %{name} %{version} JPP %{name}

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr backport-util-concurrent-dist/doc/api/* \
		$RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%doc license.html
%doc README.html
%{_javadir}/*.jar
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-2m)
- full rebuild for mo7 release

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-1m)
- import from Fedora 13

* Thu Aug 27 2009 Andrew Overholt <overholt@redhat.com> 3.1-4
- Fix Source0 URL

* Mon Aug 24 2009 Andrew Overholt <overholt@redhat.com> 3.1-3
- Clarify Source0 URl
- Add jpackage-utils Requirement
- Fix %%files defattr
- Fix javadoc sub-package description

* Wed Aug 19 2009 Andrew Overholt <overholt@redhat.com> 3.1-2
- Update URL, Group, License
- Remove explicit "jpp" from Release
- Add doc files
- Remove javadoc ghost stuff

* Wed Mar 05 2008 Deepak Bhole <dbhole@redhat.com> 3.1-1jpp
- Import from JPackage + modify per Fedora requirements

* Fri Feb 08 2008 Ralph Apel <r.apel@r-apel.de> 0:3.1-1jpp
- Upgrade to 3.1
- Always test

* Wed Jun 14 2006 Ralph Apel <r.apel@r-apel.de> 0:2.1-1jpp
- Upgrade to 2.1 - no licensing issues any more
- Create option --with tests to optionally run tck

* Thu Jan 19 2006 Deepak Bhole <dbhole@redhat.com> 0:2.0.01-1jpp
- Initial build.
