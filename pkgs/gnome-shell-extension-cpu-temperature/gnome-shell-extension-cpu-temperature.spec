%global git e1777e6
%global uuid temperature@xtranophilist
%global github xtranophilist-gnome-shell-extension-cpu-temperature
%global git_date 20111230
%global momorel 3

Name:           gnome-shell-extension-cpu-temperature
Version:        0
Release:        0.%{git_date}.%{momorel}m%{?dist}
Summary:        A gnome-shell extension to show the current temperature of CPU

Group:          User Interface/Desktops
License:        GPLv3+
URL:            https://github.com/xtranophilist/gnome-shell-extension-cpu-temperature/
# https://github.com/xtranophilist/gnome-shell-extension-cpu-temperature/tarball/master/%{github}-%{git}.tar.gz
Source0:        %{github}-%{git}.tar.gz
Patch0:		version.patch
BuildArch:      noarch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       gnome-shell >= 3.2


%description
Gnome Shell Extensions that adds an applet on the panel which reveals
current CPU temperature in Degree Celsius and Fahrenheit.


%prep
%setup -q -n %{github}-%{git}
%patch0 -p1


%build
# Nothing to build


%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/gnome-shell/extensions/%{uuid}
install -Dp -m 0644 {extension.js,metadata.json} \
  %{buildroot}%{_datadir}/gnome-shell/extensions/%{uuid}/


%files
%defattr(-,root,root,-)
%doc README
%{_datadir}/gnome-shell/extensions/%{uuid}/


%changelog
* Fri Oct 19 2012 Masanobu Sato <satoshiga@momonga-linux.org>
- (0-0.20111230.3m)
- Update version patch for gnome 3.6

* Mon Sep 24 2012 Masanobu Sato <satoshiga@momonga-linux.org>
- (0-0.20111230.2m)
- add version patch for using gnome-3.5.90

* Fri Apr  6 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (0-0.20111230.1m)
- update

* Thu Aug 18 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (0-0.20110601.1m)
- initial build
