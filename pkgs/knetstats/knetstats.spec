%global momorel 12
%global artsver 1.5.10
%global artsrel 2m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: Network monitor in KDE
Name: knetstats
Version: 1.6.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
URL: http://knetstats.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-automake-version.patch
Patch2: %{name}-%{version}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libtool

%description
A simple KDE network monitor that show rx/tx LEDs or numeric information about
the transfer rate of any network interface in a system tray icon.

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .automake
%patch2 -p1 -b .autoconf265

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --remove-category Network \
  --add-category System \
  %{buildroot}%{_datadir}/applications/kde/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING README
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Tue Apr 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-12m)
- enable to build with new automake

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.2-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.2-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.2-9m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-8m)
- move knetstats.desktop from Network to System on menu

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-7m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-6m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-4m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-2m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-1m)
- version 1.6.2

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-7m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1-6m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-5m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-4m)
- %%NoSource -> NoSource

* Thu May  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-3m)
- change "Categories" of knetstats.desktop from "System" to "Network"

* Wed May  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-2m)
- change "Categories" of knetstats.desktop from "Network" to "System"

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-1m)
- initial package for Momonga Linux
