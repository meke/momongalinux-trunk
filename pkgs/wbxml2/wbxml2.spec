%global momorel 4
%global build_doc 0
%global srcname libwbxml

Summary: WBXML parser and compiler library
Name: wbxml2
Version: 0.10.7
Release: %{momorel}m%{?dist}
License: LGPL
URL: https://libwbxml.opensync.org/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/project/%{srcname}/%{srcname}/%{version}/%{srcname}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake
BuildRequires: expat-devel
BuildRequires: pkgconfig
BuildRequires: popt-devel
BuildRequires: zlib-devel

%description
wbxml2 is a library that includes a WBXML parser and a WBXML compiler.
Unlike wbxml, it does not depend on libxml2 but on expat, making it
faster and more portable. WBXML Library contains a library and its
associated tools to Parse, Encode and Handle WBXML documents. The WBXML
(Wireless Binary XML) format is a binary representation of XML, and it
has been defined by the Wap Forum.

%package devel
Summary: Header files and static libraries from wbxml2
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on wbxml2.

%prep
%setup -q -n %{srcname}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake \
%if %{build_doc}
	-DBUILD_DOCUMENTATION:BOOL=ON \
%endif
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# clean up doc
rm -rf %{buildroot}%{_docdir}/%{srcname}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING ChangeLog GNU-LGPL
%doc INSTALL NEWS README THANKS TODO
%{_bindir}/wbxml2xml
%{_bindir}/xml2wbxml
%{_libdir}/libwbxml2.so.*

%files devel
%defattr(-,root,root)
%doc MANIFEST RELEASE References
%{_includedir}/wbxml*.h
%{_libdir}/pkgconfig/libwbxml2.pc
%{_libdir}/libwbxml2.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.7-2m)
- full rebuild for mo7 release

* Sat Jan 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.7-1m)
- version 0.10.7
- change source from wbxml2 to FORKED libwbxml
- update URL
- add TODO to %%doc
- add MANIFEST, RELEASE and References to %%doc of devel
- remove fix build.patch
- add a switch build_doc
- good-bye autotools and hello cmake!

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-2m)
- %%NoSource -> NoSource

* Mon Jun 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-1m)
- initial package for libsyncml
- Summary and %%description are imported from opensuse
