%global momorel 11

Summary: XMMS - Skins
Name: xmms-skins
Version: 1.0.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://www.xmms.org/
Source0: %{name}.tar.bz2
Source1: xmmsskins-1.0.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: unzip
Requires: xmms
BuildRequires: bzip2
BuildRequires: gzip
BuildRequires: tar
BuildArchitectures: noarch

%description
Skins for xmms. Install this package; at next startup, xmms will see all the
skins. Browse with Options/Skin browser.

If you like skins, please consider installing the package xmms-kjofol-skins
which enables the import of skins for k-jofol.

%prep

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}/%{_datadir}/xmms/Skins

# install Fedora's skins
gzip -cd %{SOURCE1} | tar xf - -C %{buildroot}/%{_datadir}/xmms/Skins

# install cooker's skins
bzip2 -cd %{SOURCE0} | tar xf - -C %{buildroot}/%{_datadir}/xmms

# clean up duplicate themes
rm -f %{buildroot}/%{_datadir}/xmms/Skins/AbsoluteE_Xmms.zip
rm -f %{buildroot}/%{_datadir}/xmms/Skins/BlackXMMS.zip
rm -f %{buildroot}/%{_datadir}/xmms/Skins/UltrafinaSE.zip
rm -f %{buildroot}/%{_datadir}/xmms/Skins/blackstar.zip

cat > README << EOF
This package is a collection of skins for xmms.
Most of them come from http://www.xmms.org
If you would like even more of them you can visit sites like:
  http://www.skinz.org
  http://www.customize.org

If you like skins, please consider installing the package xmms-kjofol-skins
which enable the import of skins for k-jofol.
EOF

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README
%{_datadir}/xmms/Skins

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-7m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-6m)
- clean up duplicate themes

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-5m)
- merge Fedora's tar-ball of xmms-skins-1.2.10-15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-4m)
- rebuild against gcc43

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.0-3m)
- use %%momorel

* Thu Oct 25 2001 Toru Hoshina <t@kondara.org>
- (1.0.0-2k)
- merge from Mandrake. [1.0.0-11mdk]

* Thu Aug  2 2001 Guillaume Cottenceau <gc@mandrakesoft.com> 1.0.0-11mdk
- update Ayo skin

* Thu Aug  2 2001 Guillaume Cottenceau <gc@mandrakesoft.com> 1.0.0-10mdk
- add Ayo skin

* Thu Jul  5 2001 Guillaume Cottenceau <gc@mandrakesoft.com> 1.0.0-9mdk
- rebuild
- un-dadoustyle-specfile

* Thu Apr 05 2001 Vincent Danen <vdanen@mandrakesoft.com> 1.0.0-8mdk
- added some nice new skins

* Thu Aug 24 2000 Guillaume Cottenceau <gc@mandrakesoft.com> 1.0.0-7mdk
- added Packager tag

* Tue Jul 18 2000 Guillaume Cottenceau <gc@mandrakesoft.com> 1.0.0-6mdk
- BM
- macros

* Tue Jun 13 2000 Guillaume Cottenceau <gc@mandrakesoft.com> 1.0.0-5mdk
- added dependency to unzip
- added 4 of the top skins of http://www.xmms.org

* Mon Apr 17 2000 Guillaume Cottenceau <gc@mandrakesoft.com> 1.0.0-4mdk
- documentation

* Fri Mar 31 2000 Guillaume Cottenceau <gc@mandrakesoft.com> 1.0.0-3mdk
- new groups

* Thu Feb 03 2000 Pablo Saratxaga <pablo@mandrakesoft.com> 1.0.0-2mdk
- split skins into its own rpm spec file; so we can set it to noarch
