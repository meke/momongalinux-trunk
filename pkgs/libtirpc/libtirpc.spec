%global momorel 1

Name:           libtirpc
Version:        0.2.4
Release:        %{momorel}m%{?dist}
Summary:        Transport Independent RPC Library
Group:          System Environment/Libraries
License:        GPL
URL:            http://sourceforge.net/projects/libtirpc
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:        http://dl.sourceforge.net/sourceforge/libtirpc/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRequires:  automake
BuildRequires:  autoconf
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  libgssglue-devel

%description
This package contains SunLib's implementation of transport-independent
RPC (TI-RPC) documentation.  This library forms a piece of the base of 
Open Network Computing (ONC), and is derived directly from the 
Solaris 2.3 source.

TI-RPC is an enhanced version of TS-RPC that requires the UNIX System V 
Transport Layer Interface (TLI) or an equivalent X/Open Transport Interface 
(XTI).  TI-RPC is on-the-wire compatible with the TS-RPC, which is supported 
by almost 70 vendors on all major operating systems.  TS-RPC source code 
(RPCSRC 4.0) remains available from several internet sites.

%package devel
Summary:        Development files for the libtirpc library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
This package includes header files and libraries necessary for
developing programs which use the tirpc library.


%prep
%setup -q

# Remove .orig files
find . -name "*.orig" | xargs rm -f

%build
sh autogen.sh
autoreconf -fisv
%configure --enable-gss
make all

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/etc
make install DESTDIR=%{buildroot}
# Don't package .a or .la files
rm -f %{buildroot}%{_libdir}/*.{a,la}

# create the man diretory
mv %{buildroot}%{_mandir}/man3 %{buildroot}%{_mandir}/man3t

%post  -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog NEWS README
%{_libdir}/libtirpc.so.*
%config(noreplace)%{_sysconfdir}/netconfig

%files devel
%defattr(0644,root,root,755)
%dir %{_includedir}/tirpc
%dir %{_includedir}/tirpc/rpc
%dir %{_includedir}/tirpc/rpcsvc
%{_libdir}/libtirpc.so
%{_libdir}/pkgconfig/libtirpc.pc
%{_includedir}/tirpc/netconfig.h
%{_includedir}/tirpc/rpc/auth.h
%{_includedir}/tirpc/rpc/auth_des.h
%{_includedir}/tirpc/rpc/auth_gss.h
%{_includedir}/tirpc/rpc/auth_kerb.h
%{_includedir}/tirpc/rpc/auth_unix.h
%{_includedir}/tirpc/rpc/clnt.h
%{_includedir}/tirpc/rpc/clnt_soc.h
%{_includedir}/tirpc/rpc/clnt_stat.h
%{_includedir}/tirpc/rpc/des.h
%{_includedir}/tirpc/rpc/des_crypt.h
%{_includedir}/tirpc/rpc/nettype.h
%{_includedir}/tirpc/rpc/pmap_clnt.h
%{_includedir}/tirpc/rpc/pmap_prot.h
%{_includedir}/tirpc/rpc/pmap_rmt.h
%{_includedir}/tirpc/rpc/raw.h
%{_includedir}/tirpc/rpc/rpc.h
%{_includedir}/tirpc/rpc/rpc_com.h
%{_includedir}/tirpc/rpc/rpc_msg.h
%{_includedir}/tirpc/rpc/rpcb_clnt.h
%{_includedir}/tirpc/rpc/rpcb_prot.h
%{_includedir}/tirpc/rpc/rpcb_prot.x
%{_includedir}/tirpc/rpc/rpcent.h
%{_includedir}/tirpc/rpc/svc.h
%{_includedir}/tirpc/rpc/svc_auth.h
%{_includedir}/tirpc/rpc/svc_dg.h
%{_includedir}/tirpc/rpc/svc_soc.h
%{_includedir}/tirpc/rpc/types.h
%{_includedir}/tirpc/rpc/xdr.h
%{_includedir}/tirpc/rpcsvc/crypt.h
%{_includedir}/tirpc/rpcsvc/crypt.x
%{_mandir}/*/*

%changelog
* Thu Dec 26 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-1m)
- update to 0.2.4
-- nfs module of autofs 5.0.8+ requires this version

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1
- sync with Fedora devel (0.2.1-4)

* Sun Nov 15 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.10-10m)
- add Patch0: libtirpc-clint-x86_64.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.10-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.10-8m)
- rebuild against rpm-4.6

* Thu Dec 11 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.10-1m)
- update 0.1.10
- merge some fixes and changes from fc-devel (libtirpc-0_1_10-1_fc11)

* Sat Nov  1 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-7m)
- [SECURITY] CVE-2008-4619 CVE-2007-0165
- sync with Fedora 9 updates (0.1.7-21)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.7-6m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.7-5m)
- [SECURITY] CVE-2007-3999
- [SECURITY] http://secunia.com/advisories/29247/
- use libgssglue
- merged some fixes from fc-devel (libtirpc-0_1_7-17)

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.7-4m)
- %%NoSource -> NoSource

* Tue Jul 31 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-3m)
- merged some fixes from fc-devel (libtirpc-0_1_7-9_fc8)
--* Mon Jul 30 2007 <steved@redhat.com> 0.1.7-9
--- Fixed mutex lock problem in clnt_raw_create()
--- Ignore the return value of snprintf() and use strlen() instead
--  to bump the pointer in clnt_sperror()
--- A couple ntohs() were needed in bindresvport_sa()
--- Added IP_RECVERR processing with to clnt_dg_call() so
--  application will see errors instead of timing out
--- Make sure remote address (xp_rtaddr) is populated
--  with the correct type of address.
--- Change the order of network ids in /etc/netconfg
--  putting ipv4 ids before ipv6.

* Tue Jul 10 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-2m)
- sync with fc-devel (libtirpc-0_1_7-7_fc8)
--* Mon Jul  9 2007 <steved@redhat.com> 0.1.7-7
--- Fixed infinite loop in svc_run() (bz 246677)

* Sat May 19 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-1m)
- import from fc-devel's libtirpc-0.1.7-6
