%global momorel 16

Summary: Newsticker Plugin for GKrellM
Name: newsticker
Version: 0.3.5
Group: User Interface/Desktops
Release: 0.0.20030212.%{momorel}m%{?dist}
License: GPL
URL: http://gk-newsticker.sourceforge.net/
#Source0: http://telia.dl.sourceforge.net/gk-newsticker/gkrellm-newsticker-%{version}.tar.gz
#NoSource: 0
Source0: gkrellm-newsticker-%{version}-cvs.tar.gz
Patch0: gkrellm-newsticker-UTF8.patch
Patch1: gkrellm-newsticker-max-items.patch
Patch2: gkrellm-newsticker-curl716.patch
Patch3: gkrellm-newsticker-curl.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       gkrellm >= 2.1.7
BuildRequires:	gkrellm >= 2.1.7
BuildRequires: curl-devel >= 7.16.0
BuildRequires: openssl-devel >= 0.9.8a

%description
 This sucker scrolls headlines retrieved from news sites like Slashdot and
others. The user can easily integrate news sites into the plugin by using the
configuration dialog.
The plugin is able to read the widely-used RDF format. 

%prep
%setup -q -n gkrellm-newsticker
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1 -b .curl~

%build
make

%install
mkdir -p %{buildroot}%{_libdir}/gkrellm2/plugins
install -m 755 newsticker.so %{buildroot}%{_libdir}/gkrellm2/plugins

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changelog README AUTHORS
%{_libdir}/gkrellm2/plugins/newsticker.so

%changelog
* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-0.0.20030212.16m)
- fix build failure; add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-0.0.20030212.15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-0.0.20030212.14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.5-0.0.20030212.13m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.5-0.0.20030212.12m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.5-0.0.20030212.11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.5-0.0.20030212.10m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.5-0.0.20030212.9m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.5-0.0.20030212.8m)
- rebuild against gcc43

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.5--0.0.20030212.7m)
- rebuild against curl-7.16.0
- - add gkrellm-newsticker-curl716.patch

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.5-0.0.20030212.6m)
- rebuild against openssl-0.9.8a

* Sun Oct 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.5-0.0.20030212.5m)
- rebuild against curl-7.12.2
- install newsticker.so with '-m 755' instead of '-m 644'

* Sat May 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.3.5-0.0.20030212.4m)
- fix UTF8 handling

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.5-0.0.20030212.3m)
- rebuild against for XFree86-4.3.0

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.3.5-0.0.20030212.2m)
  rebuild against openssl 0.9.7a

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.5-0.0.20030212.1m)
- 0.3.5 cvs version

* Fri Nov 15 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3-3m)

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.0.3-2k)
- update to 0.0.3

* Fri Feb 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.0.2-6k)
- remade .ja patch

* Sun Dec  16 2001 Hiroyuki Ikezoe <zoe@kasumi.sakura.ne.jp>
- (0.0.2-4k)
- fixed problem reading long RDF-file.

* Sun Dec  16 2001 Hiroyuki Ikezoe <zoe@kasumi.sakura.ne.jp>
- (0.0.2-3k)
- ability to view kondara.rdf(http://www.kondara.org/kondara.rdf).

* Sat Dec  15 2001 Hiroyuki Ikezoe <zoe@kasumi.sakura.ne.jp>
- (0.0.2-2k)
- 1st release.
