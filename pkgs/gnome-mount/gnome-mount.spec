%global momorel 12

Summary: Mount replacement which uses HAL to do the mounting 
Name: gnome-mount
Version: 0.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source0: http://hal.freedesktop.org/releases/%{name}-%{version}.tar.gz
NoSource: 0
# usb memory use default utf-8 (euc-jp does not work)
Patch0: gnome-mount-0.8-utf-8.patch
Patch1: gnome-mount-0.8-libnotify-0.7.2.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: hal-devel >= 0.5.11
BuildRequires: gtk2-devel >= 2.12.9
BuildRequires: dbus-devel >= 1.2.1
BuildRequires: gcr-devel
BuildRequires: libgnomeui-devel
BuildRequires: nautilus-devel
BuildRequires: gettext
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: perl-XML-Parser
BuildRequires: gail-devel >= 1.20.1
Requires: hal >= 0.5.10
Requires: dbus >= 1.0.2
Requires(pre):          GConf2
Requires(post):         GConf2
Requires(preun):        GConf2


%description
A replacement for the mount, umount and eject commands that
uses HAL to do all the heavy lifting.

%package devel
Summary: Development files for gnome-mount
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Development files for gnome-mount

%prep
%setup -q 
%patch0 -p1 -b .utf-8
%patch1 -p1 -b .libnotify

%build
%configure \
    --enable-nautilus-extension \
    --disable-schemas-install \
    --enable-static=no
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
SCHEMAS="gnome-mount.schemas"
for S in $SCHEMAS; do
  gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/$S > /dev/null
done

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/gconf/schemas/gnome-mount.schemas
%{_datadir}/locale/*/*/*
%{_bindir}/gnome-eject
%{_bindir}/gnome-mount
%{_bindir}/gnome-umount
%{_datadir}/%{name}/gnome-mount-properties.glade
%{_libdir}/nautilus/extensions-?.0/libgnome-mount.so
%{_libdir}/nautilus/extensions-?.0/libgnome-mount.la
%{_mandir}/man1/%{name}.1.*

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/gnome-mount.pc

%changelog
* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-13m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-10m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8-9m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8-8m)
- add Requires: GConf2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.8-5m)
- define __libtoolize (build fix)

* Sat Mar 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-4m)
- update to 0.9
- add patch0 (usb memory use utf-8)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-3m)
- rebuild against rpm-4.6

* Fri Jun  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-2m)
- add BuildRequires: nautilus-devel

* Mon Apr 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-2m)
- rebuild against gcc43

* Thu Nov  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7-1m)
- update to 0.7

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Sat Feb 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-5m)
- rebuild against gail-1.9.2

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-4m)
- use hal-0.5.7 (SATA does not work)

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-3m)
- rebuild against dbus 0.92

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.4-2m)
- rebuild against expat-2.0.0-1m

* Tue Jun 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-1m)
- import from FC

* Fri Jun  9 2006 Matthias Clasen <mclasen@redhat.com> - 0.4-7
- Fix BuildRequires

* Mon May 15 2006 John (J5) Palmieri <johnp@redhat.com> - 0.4-6
- Patch from Brian Pepple <bdpepple@ameritech.net> Add BR for 
  gnome-keyring-devel & libgnomeui-devel.

* Wed Mar  1 2006 David Zeuthen <davidz@redhat.com> - 0.4-5
- Update for new patch in #183191

* Wed Mar  1 2006 Matthias Clasen <mclasen@redhat.com> - 0.4-4
- Fix a crash without media (#183191)

* Tue Feb 28 2006 Karsten Hopp <karsten@redhat.de> 0.4-3
- add some buildrequires

* Mon Feb 13 2006 David Zeuthen <davidz@redhat.com> - 0.4-2
- Fix mounting of drives that HAL cannot poll

* Mon Feb 13 2006 David Zeuthen <davidz@redhat.com> - 0.4-1
- Update to upstream version 0.4

* Mon Feb 13 2006 Jesse Keating <jkeating@redhat.com> - 0.4-0.cvs20060213.1.1
- rebump for build order issues during double-long bump

* Mon Feb 13 2006 David Zeuthen <davidz@redhat.com> - 0.4-0.cvs20060213.1
- fix build

* Mon Feb 13 2006 David Zeuthen <davidz@redhat.com> - 0.4-0.cvs20060213
- new upstream snapshot

* Tue Feb 07 2006 John (J5) Palmieri <johnp@redhat.com> - 0.4-0.cvs20060117.2
- fix build

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.4-0.cvs20060117.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 17 2006 John (J5) Palmieri <johnp@redhat.com> - 0.4-0.cvs20060117
- CVS snapshot of gnome-mount
- Added a devel sub package for apps that compile in gnome-mount support

* Tue Jan 17 2006 John (J5) Palmieri <johnp@redhat.com> - 0.3-2
- Install the schema in the post

* Wed Jan 11 2006 Christopher Aillon <caillon@redhat.com> - 0.3-1
- Update to 0.3

* Mon Jan 09 2006 John (J5) Palmieri <johnp@redhat.com> - 0.2-1
- initial import 
