%global momorel 4

Name: hunspell-si
Summary: Sinhala hunspell dictionaries
Version: 0.2.1
Release: %{momorel}m%{?dist}
Source: http://www.sandaru1.com/wp-content/uploads/2009/08/si-LK.tar.gz
Group: Applications/Text
URL: http://www.sandaru1.com/2009/08/29/sinhala-spell-checker-for-firefox/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch
Requires: hunspell

%description
Sinhala hunspell dictionaries.

%prep
%setup -q -c -n hunspell-si

%build
#nothing to build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p dictionaries/si-LK.aff $RPM_BUILD_ROOT/%{_datadir}/myspell/si_LK.aff
cp -p dictionaries/si-LK.dic $RPM_BUILD_ROOT/%{_datadir}/myspell/si_LK.dic

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE README
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-1m)
- import from Fedora 13

* Tue Mar 09 2010 Parag Nemade <pnemade AT redhat.com> - 0.2.1-2
- Resolves: rh#571710: Fix license tag in spec file with new upstream release
- Removed CREDITS file as upstream provides now README and LICENSE files.

* Wed Nov 25 2009 Caolan McNamara <caolanm@redhat.com> - 0.2-1
- initial version
