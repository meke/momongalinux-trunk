%global momorel 1

Summary: Load ICC profile calibration part to graphics card
Name: xcalib
Version: 0.8
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://xcalib.sourceforge.net/
Group: User Interface/X
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}/%{name}-source-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-makefile.patch
Patch1: %{name}.1.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: color-filesystem
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXxf86vm-devel
BuildRequires: pkgconfig
BuildRequires: xorg-x11-proto-devel

%description
The command line tool applies the 'vcgt'-tag of ICC profiles to your X-server
like MS-Windows or MacOS can to set your display to a calibrated state. 
  
Versions 0.5 and higher are also usable with Microsoft Windows. 
They can be used as a free alternative to other calibration loaders.

%prep
%setup -q

%patch0 -p1 -b .make~
%patch1 -p0 -b .man~

%build
make %{name} %{?_smp_mflags} CFLAGS="%{optflags}"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# preparing
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/color/icc
mkdir -p %{buildroot}%{_mandir}/man1

# install
install -m 755 %{name} %{buildroot}%{_bindir}/
install -m 644 gamma_*.icc %{buildroot}%{_datadir}/color/icc/
gzip -d %{name}.1.gz
install -m 644 %{name}.1 %{buildroot}%{_mandir}/man1/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING README*
%{_bindir}/%{name}
%{_datadir}/color/icc/gamma_1_0.icc
%{_datadir}/color/icc/gamma_1_0.icc
%{_datadir}/color/icc/gamma_2_2_bright.icc
%{_datadir}/color/icc/gamma_2_2.icc
%{_datadir}/color/icc/gamma_2_2_lowContrast.icc
%{_mandir}/man1/%{name}.1*

%changelog
* Fri Jul  5 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-1m)
- initial package for Momonga Linux
- import some parts of spec and patches from opensuse