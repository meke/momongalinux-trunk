%global momorel 11
%global qtver 4.7.0
%global qtdir %{_libdir}/qt4

%global qbsvver 047
%global qlcvver 043
%global qnlver  047a
%global qnuver  043a

Summary:	QtBSch3V is Circuit Editor.
Name:		qtbsch3v
Version:	0.47
# Use bsch ver
Release:	%{momorel}m%{?dist}
License:	GPLv2
Group:		Applications/Engineering
URL:		http://www.suigyodo.com/online/schsoft.htm
Source0:	http://www.suigyodo.com/online/e/qbsv%{qbsvver}.tgz
NoSource:	0
Source1:	http://www.suigyodo.com/online/e/qlcv%{qlcvver}.tgz
NoSource:	1
Source2:	http://www.suigyodo.com/online/qnl%{qnlver}.tgz
NoSource:	2
Source3:	http://www.suigyodo.com/online/qnu%{qnuver}.tgz
NoSource:	3
Source4:	Qt-BSch3V.desktop
Source5:	Qt-LCoV.desktop
Source6:	Qt-Nlist.desktop
Source7:	Qt-Nut.desktop
Patch0:		%{name}-%{version}-gcc43.patch
Patch1:		qtlcov-0.43-gcc43.patch
Patch2:		qtnlist-%{version}a-gcc43.patch
Patch3:		qtnut-0.43a-gcc43.patch
Patch4: 	qtbsch3v-0.47-gcc45.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	qt
BuildRequires:	qt-devel >= %{qtver}
BuildRequires:	desktop-file-utils

%description
Qt-BSch3V is Circuit Editor Toolset for qt.

Notice: Default Library is placed in /usr/share/qtbsch3v/LIBV

%prep
rm -rf --preserve-root %{buildroot}
%setup -q -a 0 -a 1 -a 2 -a 3 -c %{name}-%{version}
%patch0 -p0 -b .gcc43
%patch1 -p0 -b .gcc43
%patch2 -p0 -b .gcc43
%patch3 -p0 -b .gcc43
%patch4 -p1 -b .gcc45

# avoid readme(-j).txt conflicts
cp qbsv%{qbsvver}/COPYING ./COPYING
cp qbsv%{qbsvver}/readme.txt readme-qbsv.txt
cp qbsv%{qbsvver}/readme-j.txt readme-qbsv-j.txt
cp qlcv%{qlcvver}/readme.txt readme-qlcv.txt
cp qlcv%{qlcvver}/readme-j.txt readme-qlcv-j.txt
cp qnl%{qnlver}/readme.txt readme-qnl.txt
cp qnu%{qnuver}/readme.txt readme-qnu.txt

%build
# build qtbsch3v
cd qbsv%{qbsvver}/%{name}/
%{qtdir}/bin/qmake-qt4
make
make clean
cd ../../

# build qtlcov
cd qlcv%{qlcvver}/qtlcov/
%{qtdir}/bin/qmake-qt4
make
make clean
cd ../../

# build qtnlist
cd qnl%{qnlver}/qtnlist/
%{qtdir}/bin/qmake-qt4
make
make clean
cd ../../

# build qtnut
cd qnu%{qnuver}/qtnut/
%{qtdir}/bin/qmake-qt4
make
make clean
cd ../../

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_datadir}/%{name}/LIBV
mkdir -p %{buildroot}%{_datadir}/applications

install -m 755 qbsv%{qbsvver}/%{name}/%{name} %{buildroot}%{_bindir}
install -m 755 qlcv%{qlcvver}/qtlcov/qtlcov %{buildroot}%{_bindir}
install -m 755 qnl%{qnlver}/qtnlist/qtnlist %{buildroot}%{_bindir}
install -m 755 qnu%{qnuver}/qtnut/qtnut %{buildroot}%{_bindir}
install -m 644 qbsv%{qbsvver}/LIBV/* %{buildroot}%{_datadir}/%{name}/LIBV
install -m 644 qbsv%{qbsvver}/Radio.CE3 %{buildroot}%{_datadir}/%{name}
install -m 644 qnl%{qnlver}/nlfigs.png %{buildroot}%{_datadir}/%{name}

install -m 644 qbsv%{qbsvver}/%{name}/xbschicon.xpm %{buildroot}%{_datadir}/%{name}
install -m 644 qlcv%{qlcvver}/qtlcov/lcoicon.xpm %{buildroot}%{_datadir}/%{name}
install -m 644 qnl%{qnlver}/qtnlist/nlicon.xpm %{buildroot}%{_datadir}/%{name}
install -m 644 qnu%{qnuver}/qtnut/nu3w.xpm %{buildroot}%{_datadir}/%{name}

desktop-file-install --mode 644 --vendor "" %{SOURCE4} --dir %{buildroot}%{_datadir}/applications
desktop-file-install --mode 644 --vendor "" %{SOURCE5} --dir %{buildroot}%{_datadir}/applications
desktop-file-install --mode 644 --vendor "" %{SOURCE6} --dir %{buildroot}%{_datadir}/applications
desktop-file-install --mode 644 --vendor "" %{SOURCE7} --dir %{buildroot}%{_datadir}/applications

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING readme-qbsv-j.txt readme-qbsv.txt readme-qlcv-j.txt readme-qlcv.txt
%doc readme-qnl.txt readme-qnu.txt
%{_bindir}/%{name}
%{_bindir}/qtlcov
%{_bindir}/qtnlist
%{_bindir}/qtnut
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/Radio.CE3
%{_datadir}/%{name}/nlfigs.png
%{_datadir}/%{name}/xbschicon.xpm
%{_datadir}/%{name}/lcoicon.xpm
%{_datadir}/%{name}/nlicon.xpm
%{_datadir}/%{name}/nu3w.xpm
%dir %{_datadir}/%{name}/LIBV
%{_datadir}/%{name}/LIBV/*
%{_datadir}/applications/Qt-BSch3V.desktop
%{_datadir}/applications/Qt-LCoV.desktop
%{_datadir}/applications/Qt-Nlist.desktop
%{_datadir}/applications/Qt-Nut.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-10m)
- rebuild for new GCC 4.5

* Wed Oct 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-9m)
- add patch for gcc45

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-8m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.47-7m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-6m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.47-5m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.47-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.47-3m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.47-2m)
- fix build on WORKDIR

* Sun Oct 26 2008 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.47-1m)
- add gcc-4.3 build patch
- initial build for Momonga Linux
