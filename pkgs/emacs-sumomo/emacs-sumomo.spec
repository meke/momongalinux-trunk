%global momorel 31

%global apelver 10.8-6m

%global csig_version  3.8
%global weather_version 0.35
%global fortune_version 0.8
%global develock_version 0.39
%global session_version 2.2a

Summary: SUMO package fo MOmonga - elisp utilites for Emacs
Name: emacs-sumomo
Version: 1.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
Source0: http://www.osk.3web.ne.jp/~kshibata/c-sig/programs/c-sig-%{csig_version}.tar.gz
Source1: ftp://ftp.opaopa.org/pub/elisp/weather-%{weather_version}.tar.gz
Source3: ftp://ftp.jpl.org/pub/elisp/develock.el.gz
Source4: http://dl.sourceforge.net/sourceforge/emacs-session/session-%{session_version}.tar.gz
Source10: sumomo-init.el
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
BuildRequires: emacs-apel >= %{apelver}
Requires: emacs
Requires: emacs-apel >= %{apelver}
Obsoletes: elisp-kosumo kosumo-emacs kosumo-xemacs
Obsoletes: c-sig-emacs c-sig-xemacs 
Obsoletes: sumomo-emacs
Obsoletes: sumomo-xemacs
Obsoletes: elisp-sumomo
Provides: elisp-sumomo

%description
SUMO package fo MOmonga (sumomo) include some elisp utilities for Emacs.

%prep
%setup -q -c -a 0 -a 1 -a 4
(
  mkdir develock;
  cd develock;
  cp %{SOURCE3} .;
  gunzip develock.el.gz;
)
mv session session-%{session_version}

%build
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_emacs_sitelispdir}

ByteCompile() {
    pushd $1
    %{_emacs_bytecompile} *.el
    install -m 644 *.el *.elc %{buildroot}%{_emacs_sitelispdir}
    rm -f *.el*
    popd
}

rm -f weather-%{weather_version}/Makefile
rm -f fortune-%{fortune_version}/Makefile

for i in \
    c-sig-%{csig_version} \
    weather-%{weather_version} \
    develock \
    session-%{session_version}/lisp
        do ByteCompile $i
done

%install
mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE10} %{buildroot}%{_datadir}/config-sample/%{name}

mv session-%{session_version}/lisp/ChangeLog session-%{session_version}
rm -rf session-%{session_version}/lisp

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc c-sig-%{csig_version}
%doc weather-%{weather_version}
%doc session-%{session_version}
%dir %{_datadir}/config-sample/%{name}
%config(noreplace) %{_datadir}/config-sample/%{name}/sumomo-init.el
%{_emacs_sitelispdir}/*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-31m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-30m)
- delete fortune.el. it is now provides by emacs-common

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-29m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2-28m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-27m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-26m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-25m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-24m)
- full rebuild for mo7 release

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-23m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-22m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-21m)
- merge sumomo-emacs to elisp-sumomo
- kill sumomo-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-19m)
- update develock.el to 0.39
- License: GPLv2+

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-18m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-17m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-16m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-15m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-14m)
- update develock.el to 0.38

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-13m)
- rebuild against xemacs-21.5.29
-- session.el is excluded from sumomo-xemacs because of compile errors

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-12m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-11m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-10m)
- rebuild against rpm-4.6

* Sun Dec  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-9m)
- update weather.el to 0.35
- update fortune.el to 0.8

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-8m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-7m)
- rebuild against apelver 10.7-11m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-6m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-5m)
- update develock.el to 0.36

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-4m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-3m)
- rebuild against gcc43

* Sat Aug 18 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- update develock.el to 0.35

* Tue Jul 10 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-1m)
- bump up version to 1.2
-- add session.el
-- update develock.el to 0.34
- update Source10: sumomo-init.el
- no %%NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-27m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-26m)
- don't use Makefile because of make-3.81

* Mon Jun  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-25m)
- update weather.el to 0.33
-- add Patch10: weather-0.33-modify-template.patch
- update fortune.el to 0.6
- update develock.el to 0.33

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-24m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-23m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-22m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-21m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-20m)
- rebuild against apel-10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-19m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-18m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-17m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-16m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1-15m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1-14m)
- use %%{xe_sitedir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-13m)
- rebuild against emacs 22.0.50

* Thu Nov 25 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-12m)
- update apelver to 10.6-3m

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.1-11m)
- rebuild against emacs-21.3.50

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-10m)
- rebuild against emacs-21.3
- define apelver
- update apel version

* Mon Jan 13 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.1-9m)
- rebuild against elisp-apel-10.4-1m

* Sat Aug 17 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.1-8m)
- change package name from kosumo to sumomo
- rebuild against elisp-apel-10.3-21m

* Mon Jul 29 2002 smbd <smbd@momonga-linux.org>
- (1.1-7m)
- change source's URI

* Wed Mar 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.1-6k)
- rebuild against emacs-21.2

* Sun Mar 10 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (1.1-4k)
- Fixed a typo in 1.0-10k %changelog.

* Sun Mar 10 2002 Hidetomo Machi <mcHT@kondara.org>
- (1.1-2k)
- add develock.el
- use macro for each version

* Sat Jan 12 2002 Hidetomo Machi <mcHT@kondara.org>
- (1.0-10k)
- update weahter-0.29
- update fortune-0.4
- modify kosumo-init.el

* Sat Nov 10 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.0-8k)
- add configuration sample

* Mon Oct 29 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.0-6k)
- modified %BuildPrereq and %Requires (added xemacs-sumo)

* Mon Oct 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.0-4k)
- use BuildPreReq instead of BuildRequires

* Sun Oct 28 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.0-2k)
- Initial build
