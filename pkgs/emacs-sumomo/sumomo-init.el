;;; sumomo-init.el --- sumomo package
;;; -*- coding: iso-2022-7bit -*-

;; Copyright (C) 2002 Momonga Project

;; Author: Momonga Project
;; Maintainer: Momonga Project
;; Keywords: c-sig, weather, fortune, session
;; Version: $Id$

;; This file is part of dot.emacs

;;; Commentary:
;; For using this, you need to add this contents to ~/.emacs, or need to
;; load directly in ~/.emacs as follows.
;;   (load "/usr/share/config-sample/elisp-sumomo/sumomo-init")
;;

;;; Code:

;; c-sig
(autoload 'c-sig-version "c-sig" "c-sig" t)
(autoload 'add-signature "c-sig" "c-sig" t)
(autoload 'delete-signature "c-sig" "c-sig" t)
(autoload 'insert-signature-eref "c-sig" "c-sig" t)
(autoload 'insert-signature-automatically "c-sig" "c-sig" t)
(autoload 'insert-signature-randomly "c-sig" "c-sig" t)

(setq sig-insert-end t
      sig-save-to-sig-name-alist nil
      message-signature-file nil
      sig-replace-string t)

;; keybind
;; (for wanderlust)
(add-hook 'wl-mail-setup-hook
	  '(lambda ()
	     (define-key (current-local-map) "\C-c\C-w"
	       'insert-signature-eref)))

;; Weather
(autoload 'weather-from-http "weather" "Weather" t)
(autoload 'weather-insert-header "weather" "Weather" t)

(setq weather-where "$B0q>k8)(B"
      weather-replace nil
      weather-insert-user-agent t)

;; The cash of the weather are carried out at the moment of becoming on-line.
;; (for wanderlust)
(add-hook 'wl-plugged-hook
	  '(lambda () (and wl-plugged (weather-from-http "$B0q>k8)(B"))))

;; Fortune
(autoload 'fortune-from-http "fortune" "Fortune" t)
(autoload 'fortune-insert-header "fortune" "Fortune" t)

(setq fortune-format
      '("$B:#F|$N(B" seiza "$B$N%i%C%-!<%+%i!<$O(B" ("$B%i%C%-!<%+%i!<(B") "$B$G$9(B"))

;; The cash of the fortune-telling are carried out at the moment of becoming
;; on-line. (for wanderlust)
(add-hook 'wl-plugged-hook
	  '(lambda () (and wl-plugged (fortune-from-http "$B$$$F:B(B"))))

;; Session
(require 'session)
(setq session-save-file-coding-system 'iso-2022-7bit)
(add-hook 'after-init-hook 'session-initialize)

;;; sumomo-init.el ends here
