%global         momorel 1

Name:           perl-Acme-Kensiro
Version:        0.16
Release:        %{momorel}m%{?dist}
Summary:        Kensiro-sinsu
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Acme-Kensiro/
Source0:        http://www.cpan.org/authors/id/T/TO/TOKUHIROM/Acme-Kensiro-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.010001
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple >= 0.98
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
kensiro-sinsu.

%prep
%setup -q -n Acme-Kensiro-%{version}

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install --destdir=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE
%{perl_vendorlib}/Acme/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- rebuild against perl-5.20.0
- update to 0.16

* Mon May  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-2m)
- rebuild against perl-5.18.2

* Sun Dec 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sun Oct 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-2m)
- rebuild against perl-5.18.0

* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Fri Mar 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Wed Mar 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-2m)
- rebuild against perl-5.16.0

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.06-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.06-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-2m)
- rebuild against rpm-4.6

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.04-2m)
- rebuild against gcc43

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-1m)
- update to 0.04

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- Specfile autogenerated by cpanspec 1.74 for Momonga Linux.
