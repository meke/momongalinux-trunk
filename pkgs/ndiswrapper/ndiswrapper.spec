%global momorel 1

Summary: NdisWrapper binary loader utility
Name: ndiswrapper
Version: 1.58
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://ndiswrapper.sourceforge.net/
Group: Applications/System
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.bash-completion
Patch0: %{name}-1.44-cflags.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Some vendors refuses to release specs or even a binary 
linux-driver for their WLAN cards. This project tries 
to solve this problem by making a kernel module that 
can load Ndis (windows network driver API) drivers. 
We're not trying to implement all of the Ndis API 
but rather implement the functions needed to get 
these unsupported cards working.

%prep
%setup -q

%patch0 -p1 -b .cflags

%build
pushd utils
CFLAGS="%{optflags}" \
%make
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# preparing...
mkdir -p %{buildroot}%{_sysconfdir}/bash_completion.d
mkdir -p %{buildroot}%{_sysconfdir}/%{name}
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_mandir}/man8

# install
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/bash_completion.d/ndiswrapper
install -m 755 utils/loadndisdriver %{buildroot}/sbin/
install -m 755 utils/ndiswrapper %{buildroot}%{_sbindir}/
install -m 755 utils/ndiswrapper-buginfo %{buildroot}%{_sbindir}/
install -m 644 loadndisdriver.8 %{buildroot}%{_mandir}/man8/
install -m 644 ndiswrapper.8 %{buildroot}%{_mandir}/man8/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog INSTALL README
%config(noreplace) %{_sysconfdir}/bash_completion.d/ndiswrapper
%dir %{_sysconfdir}/ndiswrapper
/sbin/loadndisdriver
%{_sbindir}/ndiswrapper
%{_sbindir}/ndiswrapper-buginfo
%{_mandir}/man8/loadndisdriver.8*
%{_mandir}/man8/ndiswrapper.8*

%changelog
* Sat Oct 26 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.58-1m)
- version 1.58

* Mon Jan 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.57-1m)
- update 1.57

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.56-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.56-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.56-2m)
- full rebuild for mo7 release

* Wed Feb 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.56-1m)
- version 1.56
 - Use /etc/modprobe.d/ndiswrapper.conf, not /etc/modprobe.d/ndiswrapper.

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.55-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.55-1m)
- version 1.55

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.54-1m)
- version 1.54

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.54-0.1.3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.54-0.1.2m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Sun Dec  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.54-0.1.1m)
- update to 1.54-2.6.27.7

* Tue Jun  3 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.53-1m)
- update to 1.53

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.52-5m)
- rebuild against gcc43

* Mon Mar 24 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.52-4m)
- delete Source2: ndiswrapper.pm-utils

* Sun Mar 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.52-3m)
- add a man file of loadndisdriver

* Sun Mar 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.52-2m)
- fix Source2: ndiswrapper.pm-utils

* Sat Mar 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.52-1m)
- initial package for wireless LAN freaks using Momonga Linux
- import 2 sources and a patch from cooker
