%global momorel 9
%global svndate 20081107

Summary:	Free client libraries and binaries for the NX protocol
Name:		freenx-client
Version:	1.0
Release:	0.%{svndate}.%{momorel}m%{?dist}
License:	GPLv2+
Group:		Applications/Internet
URL:		http://freenx.berlios.de/
# Source0:	http://download.berlios.de/freenx/%%{name}-%%{version}.tar.bz2
Source0:	%{name}-%{version}-svn%{svndate}.tar.bz2
Source1:	qtnx.desktop
# NoSource:	0
Patch0:		freenx-client-0.9-fixes.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	ImageMagick
BuildRequires:	autoconf >= 2.59c, automake >= 1.10, libtool
BuildRequires:	gcc-c++
# temporary until we split nx
# BuildRequires:	%{_pkglibdir}/libXcomp.so.3
# Requires:	nxssh, nxproxy
BuildRequires:	dbus-devel
BuildRequires:	desktop-file-utils
BuildRequires:	doxygen
BuildRequires:	qt-devel >= 4.7.0
# Maybe this could be split later
Provides:	nxcl = %{version}-%{release}
# Provides:	nxcl-devel = %{version}-%{release}
Provides:	qtnx = %{version}-%{release}

%description
NX is an exciting new technology for remote display. It provides near
local speed application responsiveness over high latency, low
bandwidth links.

FreeNX-client contains client libraries and executables for connecting
to an NX server.

%package devel
Group:    Development/Libraries
Summary:  Development files for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
%{summary}.

%prep
%setup -q -n %{name}-%{version}-svn%{svndate}
#%%patch0 -p1 -b .fixes
cat >> qtnx/qtnx.pro << EOF
QMAKE_CXXFLAGS += -I`pwd`/nxcl/lib
LIBS += -L`pwd`/nxcl/lib -lnxcl
EOF
mv nxcl/README nxcl/README.nxcl
mv qtnx/README qtnx/README.qtnx

%build
cd nxcl
autoreconf -is
%configure --disable-static
make
cd ../qtnx
PATH=`pkg-config --variable=bindir Qt`:$PATH
qmake
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make -C nxcl install DESTDIR=%{buildroot} docdir=%{_defaultdocdir}/%{name}-%{version}/nxcl
install -p -m 0755 qtnx/qtnx %{buildroot}%{_bindir}/
install -p -m 0644 */README* %{buildroot}%{_defaultdocdir}/%{name}-%{version}/
desktop-file-install --vendor="freenx"                    \
  --dir=%{buildroot}%{_datadir}/applications              \
  %{SOURCE1}

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32}/apps
convert -scale 16x16 nxlaunch/src/pixmaps/nx-icon.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/nx.png
convert -scale 22x22 nxlaunch/src/pixmaps/nx-icon.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/nx.png
install -m 644 nxlaunch/src/pixmaps/nx-icon.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/nx.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/nx.png %{buildroot}%{_datadir}/pixmaps/nx.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc %{_defaultdocdir}/%{name}-%{version}

# could become nxcl
%{_bindir}/libtest
%{_bindir}/notQttest
%{_bindir}/nxcl
%{_bindir}/nxcmd
%{_libdir}/libnxcl.so.*

# could become qtnx
%{_bindir}/qtnx
%{_datadir}/applications/*qtnx.desktop
%{_datadir}/icons/hicolor/*/apps/nx.png
%{_datadir}/pixmaps/nx.png

%files devel
%{_includedir}/nxcl
%{_libdir}/libnxcl.so
%{_libdir}/libnxcl.la
%{_libdir}/pkgconfig/nxcl.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.20081107.9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.20081107.8m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.20081107.7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.20081107.6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.20081107.5m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.20081107.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.20081107.3m)
- rebuild against rpm-4.6

* Mon Nov 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.20081107.2m)
- add Icon to desktop file
- remove Graphics from Categories of desktop file
- package devel Requires: %%{name} = %%{version}-%%{release}
- add %%post and %%postun

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.20081107.1m)
- import from Fedora devel
- use svn snapshot for kdenetwork

* Thu Apr 10 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.9-7
- Fix description.
- Remove devel files and embedded *-devel Provides:.
- Create a desktop file for qtnx.

* Sat Mar 29 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.9-6
- Split off client part into freenx-client.

* Mon Dec 31 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.7.1-4
- Apply Jeffrey J. Kosowsky's patches to enable multimedia and
  file/print sharing support (Fedora bug #216802).
- Silence %%post output, when openssh's server has never been started
  before (Fedora bug #235592).
- Add dependency on which (Fedora bug #250343).

* Mon Dec 10 2007 Jon Ciesla <limb@jcomserv.net> - 0.7.1-3
- Fix syntax error in logrotate file, BZ 418221.

* Mon Nov 19 2007 Jon Ciesla <limb@jcomserv.net> - 0.7.1-2
- Added logrotate, BZ 379761.

* Mon Nov 19 2007 Jon Ciesla <limb@jcomserv.net> - 0.7.1-1
- Update to 0.7.1, many bugfixes, BZ 364751, 373771.

* Sun Sep 23 2007 Ville Skytta <ville.skytta at iki.fi> - 0.7.0-2
- Do not try to set up KDE_PRINTRC if ENABLE_KDE_CUPS is not 1, deal better
  with errors when it is (#290351).

* Thu Sep 6 2007 Jon Ciesla <limb@jcomserv.net> - 0.7.0-1
- CM = Christian Mandery mail@chrismandery.de,  BZ 252976
- Version bump to 0.7.0 upstream release (CM)
- Fixed download URL (didn't work, Berlios changed layout). (CM)
- Changed license field from GPL to GPLv2 in RPM. (CM)
- Fixed release.

* Mon Feb 19 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 0.6.0-9
- Update to 0.6.0.

* Sat Sep 17 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.4.4.

* Sat Jul 30 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.4.2.

* Sat Jul  9 2005 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 0.4.1.

* Tue Mar 22 2005 Rick Stout <zipsonic[AT]gmail.com> - 0:0.3.1
- Updated to 0.3.1 release

* Tue Mar 08 2005 Rick Stout <zipsonic[AT]gmail.com> - 0:0.3.0
- Updated to 0.3.0 release
- Removed home directory patch as it is now default

* Mon Feb 14 2005 Rick Stout <zipsonic[AT]gmail.com> - 0:0.2.8
- Updated to 0.2.8 release
- Fixes some security issues
- Added geom-fix patch for windows client resuming issues

* Thu Dec 02 2004 Rick Stout <zipsonic[AT]gmail.com> - 1:0.2.7
- Fixed package removal not removing the var session directories

* Tue Nov 23 2004 Rick Stout <zipsonic[AT]gmail.com> - 0:0.2.7
- Updated to 0.2.7 release
- fixes some stability issues with 0.2.6

* Fri Nov 12 2004 Rick Stout <zipsonic[AT]gmail.com> - 1:0.2.6
- Fixed a problem with key backup upon removal

* Fri Nov 12 2004 Rick Stout <zipsonic[AT]gmail.com> - 0:0.2.6
- Updated to 0.2.6 release
- Changed setup to have nx user account added as a system account.
- Changed nx home directory to /var/lib/nxserver/nxhome

* Thu Oct 14 2004 Rick Stout <zipsonic[AT]gmail.com> - 0:0.2.5
- updated package to 0.2.5 release
- still applying patch for netcat and useradd

* Fri Oct 08 2004 Rick Stout <zipsonic[AT]gmail.com> - 3:0.2.4
- Added nxsetup functionality to the rpm
- patched nxsetup (fnxncuseradd) script for occasional path error.
- Added patch (fnxncuseradd) to resolve newer client connections (netcat -> nc)
- Changed name to be more friendly (lowercase)
- Added known dependencies

* Thu Sep 30 2004 Rick Stout <zipsonic[AT]gmail.com> - 2:0.2.4
- Patch (fnxpermatch) to fix permissions with key generation

* Wed Sep 29 2004 Rick Stout <zipsonic[AT]gmail.com> - 1:0.2.4
- Initial Fedora RPM release.
- Updated SuSE package for Fedora
