%global momorel 2

%global fontname ipa-ex
%global fontconf 65-2-%{fontname}

%global pkgver      00103
%global archivename IPAexfont%{pkgver}

%global common_desc \
IPAex Fonts are JIS X 0213:2004 compliant TrueType based OpenType format \
outline fonts. \
\
In using IPA fonts, please comply with the terms and conditions set out \
in "IPA Font License Agreement v1.0" included in this package.\
\
Any use, reproduction or distribution of the IPA Font or any exercise of \
rights under "IPA Font License Agreement v1.0" by a Recipient constitutes \
the Recipient's acceptance of the License Agreement.

Name:           %{fontname}-fonts
Version:        001.03
Release:        %{momorel}m%{?dist}
Summary:        Japanese JIS X 0213:2004 compliant TrueType based OpenType format outline fonts by IPA

Group:          User Interface/X
License:        "IPA" 
URL:            http://ossipedia.ipa.go.jp/ipafont/
Source0:        http://info.openlab.ipa.go.jp/ipafont/fontdata/%{archivename}.zip
NoSource:       0
Source1:        %{fontname}-gothic-fontconfig.conf
Source2:        %{fontname}-mincho-fontconfig.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  fontpackages-devel

%description
%common_desc

%package common
Summary:        Common files of ipa-ex
Group:          User Interface/X
Requires:       fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.

%package -n %{fontname}-gothic-fonts
Summary:        IPAex gothic fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-gothic-fonts
%common_desc

This package contains the IPAex gothic fonts.

%package -n %{fontname}-mincho-fonts
Summary:        IPAex mincho fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-mincho-fonts
%common_desc

This package contains the IPAex mincho fonts.

%_font_pkg -n gothic -f %{fontconf}-gothic.conf ipaexg.ttf
%_font_pkg -n mincho -f %{fontconf}-mincho.conf ipaexm.ttf

%prep
%setup -q -n %{archivename}


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

# Repeat for every font family
install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-gothic.conf
install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-mincho.conf

for fconf in %{fontconf}-gothic.conf \
             %{fontconf}-mincho.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done


%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc IPA_Font_License_Agreement_v1.0.txt Readme_%{archivename}.txt


%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (001.04-2m)
- fix Fontconfig warning

* Thu Mar  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (001.03-1m)
- update 001.03

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (001.02-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (001.02-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (001.02-3m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (001.02-2m)
- the priority is 65-2

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (001.02-1m)
- initial packaging
