%global momorel 1
%global srcrel 135625
%global srcname moodbar_generator.amarokscript
%global amarokver 2.4.90

Summary: A script for amarok auto-generating moodbar files
Name: moodbar_generator
Version: 0.56
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Multimedia
URL: http://kde-apps.org/content/show.php/Moodbar+Generator?content=%{srcrel}
Source0: http://kde-apps.org/CONTENT/content-files/%{srcrel}-%{srcname}.tar.gz
# NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: amarok >= %{amarokver}
# _kde4 macros need cmake
BuildRequires: cmake
BuildRequires: coreutils

%description
This is an Amarok2 script.
Automatically generates moodbar files for currently playing song.

%prep
%setup -q -n %{name}

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{_kde4_appsdir}/amarok/scripts/%{name}
install -m 644 main.js %{buildroot}%{_kde4_appsdir}/amarok/scripts/%{name}/
install -m 644 script.spec %{buildroot}%{_kde4_appsdir}/amarok/scripts/%{name}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_kde4_appsdir}/amarok/scripts/%{name}

%changelog
* Wed Dec 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- version 0.56

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.53-2m)
- rebuild for new GCC 4.6

* Mon Jan 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- version 0.53

* Tue Jan 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- version 0.51

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- version 0.40

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- version 0.39

* Sat Jan  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- version 0.38

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- version 0.37

* Mon Jan  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- version 0.35

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.21-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.21-2m)
- full rebuild for mo7 release

* Fri May 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.21-1m)
- initial package for music freaks using Momonga Linux
