%global momorel 1

%{!?build_openmpi:%global build_openmpi 1}
%{!?build_mpich:%global build_mpich 1}
%define pv_maj 4
%define pv_min 1
%define pv_patch 0
%define pv_majmin %{pv_maj}.%{pv_min}

Summary:        Parallel visualization application for scientific data
Name:           paraview
Version:        %{pv_majmin}.%{pv_patch}
Release:        %{momorel}m%{?dist}
License:        BSD
Group:          Applications/Engineering
URL:            http://www.paraview.org/
Source0:        http://www.paraview.org/files/v%{pv_majmin}/ParaView-v%{version}-source.tar.gz
NoSource:       0
Source1:        paraview_22x22.png
Source2:        paraview.xml
# Patch to fix install locations
# http://paraview.org/Bug/view.php?id=13704
Patch0:         paraview-install.patch
#Patch to vtk (from vtk package) to use system libraries
Patch1:         vtk-6.1.0-system.patch
# Capitalize Protobuf so it finds FindProtobuf.cmake
# http://paraview.org/Bug/view.php?id=13656
Patch2:         paraview-4.0.1-Protobuf.patch
# Patch to vtk to use system netcdf library
Patch3:         vtk-6.1.0-netcdf.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post):  desktop-file-utils
Requires(postun): desktop-file-utils
Requires:       %{name}-data = %{version}-%{release}
BuildRequires:  cmake
%if %{build_openmpi}
BuildRequires:  openmpi-devel >= 1.6.3
%endif
%if %{build_mpich}
BuildRequires:  mpich-devel >= 3.0.4
%endif
BuildRequires:  qt-devel >= 4.8.5
BuildRequires:  chrpath
BuildRequires:  desktop-file-utils
BuildRequires:  doxygen
BuildRequires:  expat-devel
BuildRequires:  freetype-devel
BuildRequires:  gl2ps-devel >= 1.3.8
BuildRequires:  gnuplot
BuildRequires:  graphviz
BuildRequires:  hdf5-devel >= 1.8.12
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  libpng-devel
BuildRequires:  libtheora-devel
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  libxml2-devel
BuildRequires:  mesa-libOSMesa-devel >= 8.0
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  protobuf-devel >= 2.5.0
BuildRequires:  python-devel >= 2.7
BuildRequires:  readline-devel
BuildRequires:  tk-devel
BuildRequires:  vtk-devel >= 6.0.0
BuildRequires:  wget
BuildRequires:  zlib-devel
Obsoletes:      paraview-demos < %{version}-%{release}
Provides:       paraview-demos = %{version}-%{release}
Obsoletes:      paraview-doc < %{version}-%{release}
Provides:       paraview-doc = %{version}-%{release}

#-- Plugin: VRPlugin - Virtual Reality Devices and Interactor styles : Disabled - Requires VRPN
#-- Plugin: MantaView - Manta Ray-Cast View : Disabled - Requires Manta
#-- Plugin: ForceTime - Override time requests : Disabled - Build is failing
#-- Plugin: VaporPlugin - Plugin to read NCAR VDR files : Disabled - Requires vapor
%define paraview_cmake_options \\\
        -DCMAKE_BUILD_TYPE=RelWithDebInfo \\\
        -DCMAKE_CXX_COMPILER:FILEPATH=$CXX \\\
        -DCMAKE_C_COMPILER:FILEPATH=$CC \\\
        -DTCL_LIBRARY:PATH=tcl \\\
        -DTK_LIBRARY:PATH=tk \\\
        -DPARAVIEW_BUILD_PLUGIN_AdiosReader:BOOL=ON \\\
        -DPARAVIEW_BUILD_PLUGIN_CoProcessingScriptGenerator:BOOL=ON \\\
        -DPARAVIEW_BUILD_PLUGIN_EyeDomeLighting:BOOL=ON \\\
        -DPARAVIEW_BUILD_PLUGIN_ForceTime:BOOL=ON \\\
        -DPARAVIEW_ENABLE_PYTHON:BOOL=ON \\\
        -DPARAVIEW_INSTALL_THIRD_PARTY_LIBRARIES:BOOL=OFF \\\
        -DPARAVIEW_INSTALL_DEVELOPMENT:BOOL=ON \\\
        -DPARAVIEW_USE_SYSTEM_MPI4PY:BOOL=ON \\\
        -DVTK_CUSTOM_LIBRARY_SUFFIX="" \\\
        -DVTK_INSTALL_DATA_DIR=share/paraview \\\
        -DVTK_INSTALL_PACKAGE_DIR=share/cmake/paraview \\\
        -DVTK_PYTHON_SETUP_ARGS="--prefix=/usr --root=%{buildroot}" \\\
        -DVTK_USE_BOOST:BOOL=ON \\\
        -DVTK_USE_INFOVIS:BOOL=OFF \\\
        -DVTK_USE_N_WAY_ARRAYS:BOOL=ON \\\
        -DVTK_USE_OGGTHEORA_ENCODER:BOOL=ON \\\
        -DVTK_USE_SYSTEM_ICET=OFF \\\
        -DVTK_USE_SYSTEM_LIBRARIES=ON \\\
        -DVTK_USE_SYSTEM_HDF5=ON \\\
        -DHDF5_HL_LIBRARY:FILEPATH=%{_libdir}/libhdf5_hl.so \\\
        -DVTK_USE_SYSTEM_AUTOBAHN:BOOL=ON \\\
        -DVTK_USE_SYSTEM_LIBPROJ4=OFF \\\
        -DVTK_USE_SYSTEM_NETCDF=ON \\\
        -DVTK_USE_SYSTEM_QTTESTING=OFF \\\
        -DVTK_USE_SYSTEM_TWISTED:BOOL=ON \\\
        -DVTK_USE_SYSTEM_XDMF2=OFF \\\
        -DVTK_USE_SYSTEM_ZOPE:BOOL=ON \\\
        -DXDMF_WRAP_PYTHON:BOOL=ON \\\
        -DBUILD_DOCUMENTATION:BOOL=ON \\\
        -DBUILD_EXAMPLES:BOOL=ON \\\
        -DBUILD_TESTING:BOOL=OFF

%description
ParaView is an application designed with the need to visualize large data
sets in mind. The goals of the ParaView project include the following:

    * Develop an open-source, multi-platform visualization application.
    * Support distributed computation models to process large data sets.
    * Create an open, flexible, and intuitive user interface.
    * Develop an extensible architecture based on open standards.

ParaView runs on distributed and shared memory parallel as well as single
processor systems and has been successfully tested on Windows, Linux and
various Unix workstations and clusters. Under the hood, ParaView uses the
Visualization Toolkit as the data processing and rendering engine and has a
user interface written using a unique blend of Tcl/Tk and C++.

NOTE: This version has NOT been compiled with MPI support.

%package        data
Summary:        Data files for ParaView
Group:          Applications/Engineering
Requires:       %{name} = %{version}-%{release}
Requires(post):   desktop-file-utils
Requires(postun): desktop-file-utils
BuildArch:      noarch

%description    data
%{summary}.

%package        devel
Summary:        Development files for ParaView
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
%{summary}.

%if %{build_openmpi}
%package        openmpi
Summary:        Parallel visualization application
Group:          Applications/Engineering
Requires:       %{name}-data = %{version}-%{release}
Requires:       openmpi
Requires(post):   desktop-file-utils
Requires(postun): desktop-file-utils
Obsoletes:      %{name}-mpi < %{version}-%{release}
Provides:       %{name}-mpi = %{version}-%{release}

%description    openmpi
This package contains copies of the ParaView server binaries compiled with
OpenMPI.  These are named pvserver-mpi, pvbatch-mpi, etc.
You will need to load the openmpi-%{_arch} module to setup your path properly.

%package        openmpi-devel
Summary:        Development files for %{name}-openmpi
Group:          Development/Libraries
Requires:       %{name}-openmpi = %{version}-%{release}

%description    openmpi-devel
%{summary}.
%endif

%if %{build_mpich}
%package        mpich
Summary:        Parallel visualization application
Group:          Applications/Engineering
Requires:       %{name}-data = %{version}-%{release}
Requires:       mpich2
Obsoletes:      paraview-mpich2 < %{version}-%{release}
Provides:       paraview-mpich2 = %{version}-%{release}

%description    mpich

This package contains copies of the ParaView server binaries compiled with
mpich2.  These are named pvserver_mpich2, pvbatch_mpich2, etc.
You will need to load the mpich2-%{arch} module to setup your path properly.

%package        mpich-devel
Summary:        Development files for %{name}-mpich2
Group:          Development/Libraries
Requires:       %{name}-mpich2 = %{version}-%{release}
Obsoletes:      paraview-mpich2-devel < %{version}-%{release}
Provides:       paraview-mpich2-devel = %{version}-%{release}

%description    mpich-devel
%{summary}.
%endif

%prep
%setup -q -n ParaView-v%{version}
%patch0 -p1 -b .install
%patch1 -p0 -b .system
%patch2 -p1 -b .Protobuf
%patch3 -p0 -b .netcdf
# Install python properly
sed -i -s '/VTK_INSTALL_PYTHON_USING_CMAKE/s/TRUE/FALSE/' CMakeLists.txt
#Remove included thirdparty sources just to be sure
for x in vtkmpi4py vtkprotobuf
do
  rm -r ThirdParty/*/${x}
done
for x in autobahn vtkexpat vtkfreetype vtkgl2ps vtkhdf5 vtkjpeg vtklibxml2 vtknetcdf vtkoggtheora vtkpng vtksqlite vtktiff twisted vtkzlib zope
do
  rm -r VTK/ThirdParty/*/${x}
done

%build
export CC='gcc'
export CXX='g++'
export MAKE='make'
export CFLAGS="$RPM_OPT_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS"
export PATH=%{_qt4_bindir}:$PATH
mkdir momonga
pushd momonga
%cmake .. \
        -DVTK_INSTALL_INCLUDE_DIR:PATH=include/paraview \
        -DVTK_INSTALL_ARCHIVE_DIR:PATH=%{_lib}/paraview \
        -DVTK_INSTALL_LIBRARY_DIR:PATH=%{_lib}/paraview \
        -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=ON \
        -DQtTesting_INSTALL_LIB_DIR=%{_lib}/paraview \
        -DQtTesting_INSTALL_CMAKE_DIR=%{_lib}/paraview/CMake \
        %{paraview_cmake_options}
make VERBOSE=1 %{?_smp_mflags}
popd
%if %{build_openmpi}
mkdir momonga-openmpi
pushd momonga-openmpi
%{_openmpi_load}
%cmake .. \
        -DCMAKE_INSTALL_PREFIX:PATH=%{_libdir}/openmpi \
        -DVTK_INSTALL_INCLUDE_DIR:PATH=include/paraview \
        -DVTK_INSTALL_ARCHIVE_DIR:PATH=lib/paraview \
        -DVTK_INSTALL_LIBRARY_DIR:PATH=lib/paraview \
        -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=ON \
        -DQtTesting_INSTALL_LIB_DIR=lib/paraview \
        -DQtTesting_INSTALL_CMAKE_DIR=lib/paraview/CMake \
        -DPARAVIEW_USE_MPI:BOOL=ON \
        -DICET_BUILD_TESTING:BOOL=ON \
        -DMPI_COMPILER:FILEPATH=%{_libdir}/openmpi/bin/mpicxx \
        %{paraview_cmake_options}
# Fixup forward paths
sed -i -e 's,../%{_lib}/openmpi,..,' `find -name \*-forward.c`
make VERBOSE=1 %{?_smp_mflags}
%{_openmpi_unload}
popd
%endif
%if %{build_mpich}
mkdir momonga-mpich
pushd momonga-mpich
%{_mpich_load}
%cmake .. \
        -DCMAKE_INSTALL_PREFIX:PATH=%{_libdir}/mpich \
        -DVTK_INSTALL_INCLUDE_DIR:PATH=include/paraview \
        -DVTK_INSTALL_ARCHIVE_DIR:PATH=lib/paraview \
        -DVTK_INSTALL_LIBRARY_DIR:PATH=lib/paraview \
        -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=ON \
        -DQtTesting_INSTALL_LIB_DIR=lib/paraview \
        -DQtTesting_INSTALL_CMAKE_DIR=lib/paraview/CMake \
        -DPARAVIEW_USE_MPI:BOOL=ON \
        -DICET_BUILD_TESTING:BOOL=ON \
        -DMPI_COMPILER:FILEPATH=%{_libdir}/mpich/bin/mpicxx \
        %{paraview_cmake_options}
# Fixup forward paths
sed -i -e 's,../%{_lib}/mpich,..,' `find -name \*-forward.c`
make VERBOSE=1 %{?_smp_mflags}
%{_mpich_unload}
popd
%endif

%install
rm -rf %{buildroot}

#Fix permissions
find . \( -name \*.txt -o -name \*.xml -o -name '*.[ch]' -o -name '*.[ch][px][px]' \) -print0 | xargs -0 chmod -x

# Create some needed directories
install -d %{buildroot}%{_datadir}/applications
install -d %{buildroot}%{_datadir}/pixmaps
install -m644 %SOURCE1 %{buildroot}%{_datadir}/pixmaps
install -d %{buildroot}%{_datadir}/mime/packages
install -m644 %SOURCE2 %{buildroot}%{_datadir}/mime/packages

%if %{build_openmpi}
# Install openmpi version
pushd momonga-openmpi
make install DESTDIR=%{buildroot}

#Remove mpi copy of doc and man pages
rm -rf %{buildroot}%{_libdir}/openmpi/share/{doc,man,paraview}
ln -sf ../../../share/paraview %{buildroot}%{_libdir}/openmpi/share/

popd
%endif

%if %{build_mpich}
# Install mpich2 version
pushd momonga-mpich
make install DESTDIR=%{buildroot}

#Remove mpi copy of doc and man pages
rm -rf %{buildroot}%{_libdir}/mpich/share/{doc,man,paraview}
ln -sf ../../../share/paraview %{buildroot}%{_libdir}/mpich/share/

popd
%endif

#Install the normal version
pushd momonga
make install DESTDIR=%{buildroot}

#Create desktop file
cat > paraview.desktop <<EOF
[Desktop Entry]
Encoding=UTF-8
Name=ParaView Viewer
GenericName=Data Viewer
Comment=ParaView allows viewing of large data sets
Type=Application
Terminal=false
Icon=paraview_22x22
MimeType=application/x-paraview;
Categories=Application;Graphics;
Exec=paraview
EOF

desktop-file-install --vendor= \
       --add-category Education \
       --add-category Engineering \
       --dir %{buildroot}%{_datadir}/applications/ \
       paraview.desktop

#Cleanup only vtk conflicting binaries
rm %{buildroot}%{_bindir}/vtk{EncodeString,HashSource,Parse{Java,OGLExt},Wrap{Hierarchy,Java,Python,Tcl}}*
popd

# Strip build dir from VTKConfig.cmake (bug #917425)
find %{buildroot} -name VTKConfig.cmake | xargs sed -i -e '/builddir/s/^/#/'

%clean
rm -rf %{buildroot}

#Handle changing from directory to file
%pre
if [ -d %{_libdir}/paraview/paraview ]
then
  rm -r %{_libdir}/paraview/paraview
fi

%post
/sbin/ldconfig
update-desktop-database &> /dev/null ||:

%postun
/sbin/ldconfig
update-desktop-database &> /dev/null ||:

%post   data
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun data
update-mime-database %{_datadir}/mime &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc License_v1.2.txt
%{_bindir}/paraview
%{_bindir}/pvbatch
#{_bindir}/pvblot
%{_bindir}/pvdataserver
%{_bindir}/pvpython
%{_bindir}/pvrenderserver
%{_bindir}/pvserver
%{_bindir}/smTestDriver
%{_libdir}/%{name}

%files data
%defattr(-,root,root,-)
%{_datadir}/applications/paraview.desktop
%{_datadir}/pixmaps/paraview_22x22.png
%{_datadir}/mime/packages/paraview.xml
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_bindir}/vtk*
%{_includedir}/%{name}
%{_datadir}/cmake/*
%{_datadir}/doc/paraview-%{pv_majmin}

%if %{build_mpich}
%files openmpi
%defattr(-,root,root,-)
%doc License_v1.2.txt
%{_libdir}/openmpi/bin/[ps]*
%{_libdir}/openmpi/lib/%{name}

%files openmpi-devel
%defattr(-,root,root,-)
%{_libdir}/openmpi/bin/vtk*
%{_libdir}/openmpi/include/%{name}
%{_libdir}/openmpi/share/cmake
%{_libdir}/openmpi/share/%{name}
%endif

%if %{build_mpich}
%files mpich
%defattr(-,root,root,-)
%doc License_v1.2.txt
%{_libdir}/mpich/bin/[ps]*
%{_libdir}/mpich/lib/%{name}

%files mpich-devel
%defattr(-,root,root,-)
%{_libdir}/mpich/bin/vtk*
%{_libdir}/mpich/include/%{name}
%{_libdir}/mpich/share/cmake
%{_libdir}/mpich/share/%{name}
%endif

%changelog
* Sat Jan 18 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- rebuild against hdf5-1.8.12 and mpich-3.0.4
- buil with new vtk
- update to 4.1.0

* Wed Jul 17 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.98.1-2m)
- rebuild against protobuf-2.5.0
- sort BR

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.98.1-1m)
- update to 3.98.1

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.98.0-2m)
- rebuild against mpich2-1.5

* Wed Dec 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.98.0-1m)
- rebuild with openmpi-1.6.3
- rebuild with hdf5-1.8.10
- update to 3.98.0

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.1-2m)
- rebuild against mpich2-1.5rc1

* Tue Aug 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.1-1m)
- rebuild against openmpi-1.6
- rebuild against hdf5-1.8.9
- update to 3.14.1

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.0-3m)
- rebuild against libtiff-4.0.1

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.0-2m)
- rebuild against hdf5-1.8.8

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.0-1m)
- update to 3.14.0

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.12.0-7m)
- rebuild against mesa-8.0

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.0-6m)
- move some python related files to proper directory, instead of chrpath

* Sun Dec 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.0-5m)
- sync with Fedora devel (paraview-3.12.0-5)

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.0-4m)
- remove conflicting files with VTK

* Sat Dec  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.0-3m)
- revive devel subpackage
- add openmpi-devel and mpich2-devel subpackages
- BuildRequires: mpich2-devel >= 1.4.1p1-2m

* Tue Nov 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.0-2m)
- remove fenv.h hack

* Wed Nov 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.0-1m)
- update to 3.12.0

* Sun Oct  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.1-5m)
- enable to build with mpich2-1.4.1

* Wed Sep  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.1-4m)
- rebuild against mesa-libOSMesa-7.11

* Tue Jul  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.1-3m)
- rebuild against mpich2-1.4

* Mon May  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.1-2m)
- rebuild against python-2.7

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.1-1m)
- update to 3.10.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.10.0-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.10.0-1m)
- update to 3.10.0 (sync with Fedora devel)

* Tue Mar 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.1-5m)
- add patch for gcc4.6

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.1-4m)
- specify PATH for Qt4, DO NOT modify cmake options

* Thu Dec  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.8.1-3m)
- fix build

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.1-2m)
- rebuild for new GCC 4.5

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.1-1m)
- update to 3.8.1
- rebuild against mpich2-1.3
- drop devel sub package

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.0-3m)
- add BuildRequires: qt-assistant-adp-devel
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.8.0-1m)
- update to 3.8.0
- rename from mpi to openmpi subpackage
- add mpich2 subpackage
- remove doc subpackage
- set BuildArch: noarch to data subpackage
- sync with Fedora devel (3.8.0-3)

* Wed Jun 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-12m)
- remove Requires: qt-assistant-adp

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-11m)
- rebuild against qt-4.6.3-1m

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-10m)
- rebuild against qt-4.7.0 and qt-assistant-adp-4.6.2

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.4.0-9m)
- rebuild against readline6

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-8m)
- explicitly link libm

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-7m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-6m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.0-4m)
- rebuild against libjpeg-7

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-3m)
- rebuild against openssl-0.9.8k

* Wed Apr  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-2m)
- remove %%{_libdir}/paraview{,-mpi}/assistant-real
- Requires(*): shared-mime-info

* Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.0-1m)
- sync with Rawhide (3.4.0-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-3m)
- rebuild against python-2.6.1

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.2-2m)
- rebuild against hdf5-1.8.2

* Thu Jul 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-1m)
- version 3.2.2, cmake26 build fix
- import cmake2.6.patch from debian
 +- paraview (3.2.2-1) unstable; urgency=low
 +- [ Ondrej Certik ]
 +- cmake2.6.patch added to configure well with cmake2.6
- update qt.patch

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-3m)
- rebuild against qt-4.4.0-1m

* Thu May  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-2m)
- rebuild against openmpi-1.2.6

* Thu May 08 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- import to Momonga (from Fedora)

* Wed Mar 5 2008 - Orion Poplawski <orion@cora.nwra.com> - 3.2.1-5
- Rebuild for hdf5 1.8.0 using compatability API define and new patch

* Mon Feb 18 2008 - Orion Poplawski <orion@cora.nwra.com> - 3.2.1-4
- Add patch to compile with gcc 4.3

* Fri Jan 18 2008 - Orion Poplawski <orion@cora.nwra.com> - 3.2.1-3
- Add patch to fix parallel make
- Obsolete demos package (bug #428528)

* Tue Dec 18 2007 - Orion Poplawski <orion@cora.nwra.com> - 3.2.1-2
- Name ld.so.conf.d file with .conf extension
- Drop parallel make for now

* Mon Dec 03 2007 - Orion Poplawski <orion@cora.nwra.com> - 3.2.1-1
- Update to 3.2.1
- Use macros for version numbers
- Add patches to fix documentation install location and use assistant-qt4,
  not install copies of Qt libraries, and not use rpath.
- Install ld.so.conf.d file
- Fixup desktop files

* Thu Aug 23 2007 - Orion Poplawski <orion@cora.nwra.com> - 3.0.2-2
- Update license tag to BSD
- Fix make %%{_smp_mflags}
- Rebuild for ppc32

* Wed Jul 11 2007 - Orion Poplawski <orion@cora.nwra.com> - 3.0.2-1
- Update to 3.0.2
- Turn mpi build back on
- Add devel packages
- Remove demo package no longer in upstream
- Use cmake macros

* Thu Mar 08 2007 - Orion Poplawski <orion@cora.nwra.com> - 2.4.4-6
- Don't build mpi version until upstream fixes the build system

* Fri Dec 22 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.4-5
- Fix .so permissions
- Patch for const issue
- Patch for new cmake
- Build with openmpi

* Thu Dec 14 2006 - Jef Spaleta <jspaleta@gmail.com> - 2.4.4-4
- Bump and build for python 2.5

* Fri Oct  6 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.4-3
- Install needed python libraries to get around make install bug

* Wed Oct  4 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.4-2
- Re-enable OSMESA support for FC6
- Enable python wrapping

* Fri Sep 15 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.4-1
- Update to 2.4.4

* Thu Jun 29 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.3-8
- No OSMesa support in FC5
- Make data sub-package pull in main package (bug #193837)
- A patch from CVS to fix vtkXOpenRenderWindow.cxx
- Need lam-devel for FC6

* Fri Apr 21 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.3-7
- Re-enable ppc

* Mon Apr 17 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.3-6
- Exclude ppc due to gcc bug #189160

* Wed Apr 12 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.3-5
- Cleanup permissions

* Mon Apr 10 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.3-4
- Add icon and cleanup desktop file

* Mon Apr 10 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.3-3
- Add VTK_USE_MANGLE_MESA for off screen rendering
- Cleanup source permisions
- Add an initial .desktop file
- Make requirement on -data specific to version
- Don't package Ice-T man pages and cmake files

* Thu Apr  6 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.3-2
- Add mpi version

* Tue Apr  4 2006 - Orion Poplawski <orion@cora.nwra.com> - 2.4.3-1
- Initial Fedora Extras version
