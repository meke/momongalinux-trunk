%global momorel 14

Name:           perl-JSON-DWIW
Version:        0.47
Release:        %{momorel}m%{?dist}
Summary:        JSON converter that Does What I Want
License:        GPL or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/JSON-DWIW/
Source0:        http://www.cpan.org/authors/id/D/DO/DOWENS/JSON-DWIW-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
Other JSON modules require setting several parameters before calling the
conversion methods to do what I want. This module does things by default
that I think should be done when working with JSON in Perl. This module
also encodes and decodes faster than JSON.pm and JSON::Syck in my
benchmarks.

%prep
%setup -q -n JSON-DWIW-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
make test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Artistic README WhatsNew
%{perl_vendorarch}/auto/JSON/DWIW/*
%{perl_vendorarch}/JSON/DWIW*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-14m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-13m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-12m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-11m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-10m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-9m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-8m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-7m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.47-2m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.46-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Fri May 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.39-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Sat Sep 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Fri Jun 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Sat Feb 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.28-2m)
- rebuild against rpm-4.6

* Wed Nov 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-2m)
- rebuild against gcc43

* Thu Oct  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-1m)
- sorry, version down to 0.14
- version 0.15 build failure on x86_64

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
