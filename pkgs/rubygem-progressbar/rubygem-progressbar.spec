# Generated from progressbar-0.10.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname progressbar

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Ruby/ProgressBar is a text progress bar library for Ruby
Name: rubygem-%{gemname}
Version: 0.11.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/peleteiro/progressbar
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.3.6
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.3.6
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc
Provides: ruby-%{gemname}
Obsoletes: ruby-%{gemname}

%description
Ruby/ProgressBar is a text progress bar library for Ruby. It can indicate
progress with percentage, a progress bar, and estimated remaining time.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Sep 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.0-1m)
- update 0.11.0

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.0-1m)
- update 0.10.0

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8-11m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-8m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-7m)
- fix build on x86_64

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8-6m)
- rebuild against ruby-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8-3m)
- rebuild against gcc43

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.8-2m)
- /usr/lib/ruby

* Tue Jun 22 2004 kourin <kourin@momonga-linux.org>
- (0.8-1m)
- import

* Fri Feb  6 2004 Masaki Yatsu <yatsu@digital-genes.com>
- (0.8-1)
