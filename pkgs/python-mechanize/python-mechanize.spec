%global momorel 1
%global srcname mechanize
%global pythonver 2.7
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Stateful programmatic web browsing
Name: python-mechanize
Version: 0.2.5
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://wwwsearch.sourceforge.net/mechanize/
Source0: http://wwwsearch.sourceforge.net/%{srcname}/src/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python >= %{pythonver}
Requires: python-clientform
BuildRequires: coreutils
BuildRequires: python-devel >= %{pythonver}
BuildRequires: python-setuptools
BuildArch: noarch 

%description
tateful programmatic web browsing, after Andy Lester's Perl module
WWW::Mechanize.

The library is layered: mechanize.Browser (stateful web browser),
mechanize.UserAgent (configurable URL opener), plus urllib2 handlers.

Features include: ftp:, http: and file: URL schemes, browser history,
high-level hyperlink and HTML form support, HTTP cookies, HTTP-EQUIV and
Refresh, Referer [sic] header, robots.txt, redirections, proxies, and
Basic and Digest HTTP authentication.  mechanize's response objects are
(lazily-) .seek()able and still work after .close().

Much of the code originally derived from Perl code by Gisle Aas
(libwww-perl), Johnny Lee (MSIE Cookie support) and last but not least
Andy Lester (WWW::Mechanize).  urllib2 was written by Jeremy Hylton.

%prep
%setup -q -n %{srcname}-%{version}

%build
python setup.py build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install -O1 --single-version-externally-managed --root=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING.txt 
%doc INSTALL.txt README.txt docs/
%{python_sitelib}/%{srcname}
%{python_sitelib}/%{srcname}-%{version}-py?.?.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.5-1m)
- update 0.2.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.10-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.10-1m)
- initial package for python-libgmail
