%global momorel 2

Summary: The GNU data compression program
Name: gzip
Version: 1.6
Release: %{momorel}m%{?dist}
# info pages are under GFDL license
License: GPLv2+ and GFDL
Group: Applications/File
Source0: http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: gzip-1.3.12-openbsd-owl-tmp.patch
Patch1: gzip-1.3.5-zforce.patch
Patch4: gzip-1.3.13-rsync.patch
Patch5: gzip-1.3.5-cve-2006-4338.patch
Patch6: gzip-1.3.13-cve-2006-4337.patch
Patch7: gzip-1.3.5-cve-2006-4337_len.patch

Requires(post): info
Requires(preun): info
Requires: mktemp pager less
BuildRequires: texinfo
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gzip.org/

%description
The gzip package contains the popular GNU gzip data compression
program. Gzipped files have a .gz extension.

Gzip should be installed on your system, because it is a
very commonly used data compression program.

%prep
%setup -q
%patch0 -p1 -b .owl-tmp
%patch1 -p1 -b .zforce
%patch4 -p1 -b .rsync
%patch5 -p1 -b .4338
%patch6 -p1 -b .4337
%patch7 -p1 -b .4337l

%build
export DEFS="NO_ASM"
export CPPFLAGS="-DHAVE_LSTAT"
%configure 

%make
#make gzip.info

%install
rm -rf %{buildroot}
%makeinstall

# we don't ship it, so let's remove it from %{buildroot}
rm -f %{buildroot}%{_infodir}/dir
# uncompress is a part of ncompress package
rm -f ${RPM_BUILD_ROOT}/usr/bin/uncompress

%clean
rm -rf %{buildroot}

%post
if [ -f %{_infodir}/gzip.info* ]; then
    /sbin/install-info %{_infodir}/gzip.info %{_infodir}/dir || :
fi

%preun
if [ $1 = 0 ]; then
    if [ -f %{_infodir}/gzip.info* ]; then
        /sbin/install-info --delete %{_infodir}/gzip.info %{_infodir}/dir || :
    fi
fi

%files
%defattr(-,root,root,-)
%doc NEWS README AUTHORS ChangeLog THANKS TODO
%{_bindir}/*
%{_mandir}/*/*
%{_infodir}/gzip.info*

%changelog
* Sat Mar 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-2m)
- support UserMove env

* Tue Sep 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-1m)
- update 1.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-3m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4-2m)
- modify spec
- use original zless

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-1m)
- update 1.4

* Wed Apr 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.13-3m)
- fix CRC error (Patch19)
-- http://thread.gmane.org/gmane.comp.gnu.gzip.bugs/307 

* Sat Jan 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.13-2m)
- [SECURITY] CVE-2010-0001
- apply the upstream patch

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.13-1m)
- [SECURITY] CVE-2009-2624
- update to 1.3.13

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.12-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.12-4m)
- rebuild against rpm-4.6

* Sun Jan  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.12-3m)
- update Patch1,5,20 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.12-2m)
- rebuild against gcc43

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-1m)
- update to 1.3.12 (sync with FC-devel)
- some patches has replaced and imported
- delete unused patches and sources

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.9-2m)
- build fix for glibc-2.6

* Tue Feb 06 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.9-1m)
- update to 1.3.9
- delete Patch0: gzip-1.3.5-openbsd-owl-tmp.patch
  add    Patch0: gzip-1.3.9-openbsd-owl-tmp.patch
- delete Patch3: gzip-1.3.5-stderr.patch
  add    Patch3: gzip-1.3.9-stderr.patch
- delete Patch4: gzip-1.3.1-zgreppipe.patch
  add    Patch4: gzip-1.3.9-zgreppipe.patch
- delete Patch5: gzip-1.3-rsync.patch
  add    Patch5: gzip-1.3.9-rsync.patch
- delete Patch7: gzip-1.3.3-addsuffix.patch
  add    Patch7: gzip-1.3.9-addsuffix.patch
- delete Patch8: gzip-1.3.5-zgrep-sed.patch
- delete Patch9: gzip-1.3.5-gzip-perm.patch
- delete Patch10: gzip-1.3.5-gunzip-dir.patch
- delete Patch11: gzip-1.3.3-cve-2006-4334.patch
- delete Patch12: gzip-1.3.3-cve-2006-4335.patch
  add    Patch12: gzip-1.3.5-cve-2006-4335.patch
- delete Patch13: gzip-1.3.3-cve-2006-4336.patch
  add    Patch13: gzip-1.3.5-cve-2006-4336.patch
- delete Patch14: gzip-1.3.3-cve-2006-4338.patch
  add    Patch14: gzip-1.3.5-cve-2006-4338.patch
- delete Patch15: gzip-1.3.3-cve-2006-4337.patch
  add    Patch15: gzip-1.3.9-cve-2006-4337.patch
- add    Patch16: gzip-1.3.5-cve-2006-4337_len.patch
- add    Patch17: gzip-1.3.9-zdiff_arg.patch

* Thu Sep 21 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.5-3m)
- [SECURITY] CVE-2006-4334 CVE-2006-4335 CVE-2006-4336 CVE-2006-4337 CVE-2006-4338
- * Wed Sep  6 2006 Ivana Varekova <varekova@redhat.com> 1.3.3-16.rhel4
- - fix bug 204676 (patches by Tavis Ormandy)
-   - cve-2006-4334 - null dereference problem
-   - cve-2006-4335 - buffer overflow problem
-   - cve-2006-4336 - buffer underflow problem
-   - cve-2006-4338 - infinite loop problem
-   - cve-2006-4337 - buffer overflow problem

* Thu Jul 21 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.3.5-2m)
- [SECURITY] CAN-2005-0988 CAN-2005-1228

* Fri Apr 15 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.3.5-1m)
- up to 1.3.5

* Wed Feb  2 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3.3-6m)
  req less 
  
* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3.3-5m)
- revised spec for enabling rpm 4.2.

* Tue Nov 19 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (1.3.3-4m)
- require pager instead of less.

* Tue Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.3-3m)
- modified specfile based on rawhide

* Fri Mar 15 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.3.3-2k)
- update to 1.3.3

* Mon Jan 14 2002 WATABE Toyokazu <toy2@kondara.org>
- (1.3.2-4k)
- add gzip-1.3.2-overflow.patch, fixing security holes.
  http://www.securityfocus.com/bid/3712

* Thu Nov 22 2001 TABUCHI Takaaki <tab@kondara.org>
- (1.3.2-2k)
- version up to 1.3.2
- adapt gzip-1.2.4a-dirinfo.patch to 1.3.2, but Is this correct?

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Jun 20 2000 AYUHANA Tomonori <l@kondara.org>
- merge from RawHide (1.3-4)

* Wed Mar  1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- version up to man-pages-ja-GNU_gzip-20000115

* Thu Nov 25 1999 Masako Hattori <maru@kondara.org>
- version up to man-pages-ja-GNU_gzip-19991115

* Mon Nov 8 1999 Tenkou N. Hattori <tnh@kondara.org>
- remove translation_list.
- change to nosrc.rpm.

* Wed Nov 3 1999 Norihito Ohmori <nono@kondara.org>
- /usr/share/man -> %{_mandir}

* Fri Oct 29 1999 Norihito Ohmori <nono@kondara.org>
- add defattr

* Sun Oct 16 1999 Norihito Ohmori <nono@kondara.org>
- add Japanese Manual

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 14)

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- built against gliibc 2.1

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 09 1998 Cristian Gafton <gafton@redhat.com>
- added /usr/bin/gzip and /usr/bin/gunzip symlinks as some programs are too
  brain dead to figure out they should be at least trying to use $PATH
- added BuildRoot

* Wed Jan 28 1998 Erik Troan <ewt@redhat.com>
- fix /tmp races

* Sun Sep 14 1997 Erik Troan <ewt@redhat.com>
- uses install-info
- applied patch for gzexe

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Apr 22 1997 Marc Ewing <marc@redhat.com>
- (Entry added for Marc by Erik) fixed gzexe to use /bin/gzip

