%global momorel 6

%global debug_package %{nil}

Summary: A GUI for several command-line debuggers
Name: ddd
Version: 3.3.12
Release: %{momorel}m%{?dist}
License: GPLv3+ and LGPLv3+ and GFDL
Group: Development/Debuggers
URL: http://www.gnu.org/software/ddd/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source2: ddd.desktop
Source3: ddd.png
Patch0: ddd-3.3.11-lang.patch
Patch1: ddd-3.3.11-gcc44.patch
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXaw-devel
BuildRequires: libXext-devel, libXmu-devel, libXp-devel, libXpm-devel
BuildRequires: libXt-devel
BuildRequires: openmotif-devel
BuildRequires: ncurses-devel
BuildRequires: desktop-file-utils
BuildRequires: gcc-c++ >= 3.4.1-1m
Requires: openmotif
Requires(post): info
Requires(preun): info

%description
The Data Display Debugger (DDD) is a popular GUI for command-line
debuggers like GDB, DBX, JDB, WDB, XDB, the Perl debugger, and the
Python debugger. DDD allows you to view source texts and provides an
interactive graphical data display, in which data structures are
displayed as graphs. You can use your mouse to dereference pointers
or view structure contents, which are updated every time the program
stops. DDD can debug programs written in Ada, C, C++, Chill, Fortran,
Java, Modula, Pascal, Perl, and Python. DDD provides machine-level
debugging; hypertext source navigation and lookup; breakpoint,
watchpoint, backtrace, and history editors; array plots; undo and
redo; preferences and settings editors; program execution in the
terminal emulation window, debugging on a remote host, an on-line
manual, extensive help on the Motif user interface, and a command-line
interface with full editing, history and completion capabilities.

%prep
%setup -q
%patch0 -p1 -b .lang
%patch1 -p1 -b .gcc44~

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%makeinstall transform='s,x,x,'

mkdir -p %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/applications/ddd.desktop
mkdir -p %{buildroot}%{_datadir}/icons
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/icons

rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/ddd.info %{_infodir}/dir
/sbin/install-info %{_infodir}/ddd-themes.info %{_infodir}/dir
 
%preun
if [ "$1" = 0 ]; then
   /sbin/install-info --delete %{_infodir}/ddd.info %{_infodir}/dir
   /sbin/install-info --delete %{_infodir}/ddd-themes.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc doc/ddd.pdf
%{_bindir}/ddd
%{_datadir}/%{name}-%{version}
%{_mandir}/man1/ddd.1*
%{_infodir}/ddd.info*
%{_infodir}/ddd-themes.info*
%{_datadir}/applications/ddd.desktop
%{_datadir}/icons/ddd.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.12-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.12-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.12-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.12-3m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.12-1m)
- update to 3.3.12

* Sun Jan 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.11-8m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.11-7m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.11-6m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.11-5m)
- rebuild against gcc43

* Thu Mar 20 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.3.11-4m)
- del Buildrequires: libtermcap-devel

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.11-3m)
- %%NoSource -> NoSource

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.11-2m)
- rebuild against openmotif-2.3.0-beta2

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.3.11-1m)
- update to 3.3.11
- modify patch
- change Source0 URI

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.3.10-1m)
  update to 3.3.10

* Mon Oct 04 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.8-4m)
- %%makeinstall transform='s,x,x,'

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.8-3m)
- rebuild against gcc-c++-3.4.1

* Sun Jul 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.8-2m)
- 'install-info xxx.info' instead of 'install-info xxx.info.gz'

* Sat Jul 10 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.3.8-1m)
- version up and import specfile

* Thu Jul  8 2004 zunda <zunda at freeshell.org>
- (3.3.1-10m)
- ../usr/lib needs to be made before making install.
- %{_libdir}/* added to the file list: libtool or something changed?

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (3.3.1-9m)
- revised spec for enabling rpm 4.2.

* Tue May 14 2002 Toru Hoshina <t@kondara.org>
- (3.3.1-8k)
- reviced

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (3.3.1-6k)
- rebuild against openmotif-2.2.2-2k.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (3.3.1-4k)
- nigittenu

* Sun Nov  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.3.1-2k)
- update to 3.3.1
- add ddd-3.3-ldl.patch for linking libdl

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (3.3-6k)
- no more ifarch alpha.

* Tue Jan 23 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 3.2.98

* Wed Jan 17 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 3.2.95

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.2.93-5k)
- modified spec file with macros about docdir

* Thu Dec 21 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 3.2.93

* Mon Dec  4 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 3.2.92
- fix spec

* Thu Apr 22 1999 Alec Habig <habig@budoe.bu.edu>

- Applied glibc2.1 patch from J.H.M. Dassen <jdassen@wi.leidenuniv.nl>.
  Applied RH Motif 2.1 patch from Eugene Kanter <eugene@bgs.com>.
  3.1.4-3 released.

* Wed Mar 31 1999 Alec Habig <habig@budoe.bu.edu>

- Fixed minor specfile typos, 3.1.4-2 released

* Sun Mar 28 1999 Alec Habig <habig@budoe.bu.edu>

- Updated to ddd-3.1.4 bugfix release, linked against lesstif-0.88,
  3.1.4-1 released.

* Fri Mar 19 1999 Ferdy Hanssen <hanssen@cs.utwente.nl>

- Compiled and linked with Metrolink Motif 2.1.10 instead of Lesstif
- Made RPM relocatable and buildable anywhere

* Mon Jan 11 1999 Alec Habig <habig@budoe.bu.edu>

- Updated to ddd-3.1.3 bugfix release, 3.1.3-1 released.

* Wed Dec 23 1998 Alec Habig <habig@budoe.bu.edu>

- Updated to ddd-3.1.2 bugfix release, 3.1.2-1 released.

* Fri Dec 11 1998 Alec Habig <habig@budoe.bu.edu>

- MACHINE was undefined in post script, and there's no good way for the
  rpm to know what it should be at that point.  Since /usr/bin isn't ever
  shared between arch's, it's safe to remove the MACHINE from the executable
  name.
- 3.1.1-2 released

* Mon Dec 07 1998 Alec Habig <habig@budoe.bu.edu>

- Fixed post symlink bug
- Updated to ddd-3.1.1 bugfix release

* Thu Dec 03 1998 Alec Habig <habig@budoe.bu.edu>

- Changed Packager and Distribution fields to comply with Red Hat Contrib|Net
  protocols.
- Called this ddd-3.1-2

* Thu Dec 03 1998 Alec Habig <habig@budoe.bu.edu>

- Updated to v3.1.
- Now built vs. lesstif v0.87.0

* Thu Aug 18 1998 Alec Habig <habig@budoe.bu.edu>

- fixed unconditional removal of /usr/bin/ddd symlink so that
  rpm -U doesn't clobber it (thanks to Chris Siebenmann
  <cks@hawkwind.utcs.toronto.edu> for pointing this out!).
- linked statics against lesstif-0.86.0 from lesstif.org
- released version 3.0-5.

* Wed Jul 29 1998 Alec Habig <habig@budoe.bu.edu>

- Merged Peter's changes into the spec file
- updated docs about lesstif version to use
- linked statics against lesstif-0.85.3 from lesstif.org
- released version 3.0-4.

* Sat Jul  4 1998 Peter Rye <prye@picu-sgh.demon.co.uk>

- Slightly modified Alec's spec file to package ddd 3.0.
- Patch to config.sub to configure for my Alpha SX164.
- Changed `uname -m` hack and version information to avoid hardwiring this
  into the spec file.
- Reversed the chown bin.bin post install

* Thu Jun 26 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-3.0-3 release
- linked vs. lesstif-0.85-2 with patch from ddd page, to fix
  crash in breakpoint menu.
- fixed ddd symlink problem.

* Mon Jun 22 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-3.0-2 released
- minor updates to docs.

* Mon Jun 22 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-3.0-1 released
- build of the ddd-3.0 release

* Sun May 31 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-2.99.99-1 released
- build of the ddd-2.99.99 beta release

* Sun May 31 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-2.99.9-1 released
- build of the ddd-2.99.9 beta release

* Fri May 28 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-2.99.2-1 released
- this rpm now built for glibc on a RH-5.1 system
- lesstif v0.84 now used for this build, instead of RH Motif v2.0
- build of the ddd-2.99.2 beta release

* Tue May 12 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-2.99.1-1 released
- build of the ddd-2.99.1 beta release

* Wed May 06 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-2.99-1 released
- build of the ddd-2.99 beta release

* Tue Feb 03 1998 Alec Habig <habig@budoe.bu.edu>
- ddd-2.2.3-2 released
- typo in post-install for static package fixed

* Mon Feb 02 1998 Alec Habig <habig@budoe.bu.edu>
- changelog added to specfile
- changed installed binary names from *-gnulibc1-* to *-libc5-* to avoid
  confusion.  Dunno why autoconf wants to call this gnulibc.
- chown'd all the installed stuff to bin.bin
- gzip'd the docs' ChangeLog
- librx and termcap subdirs are not getting passed the RPM compile opts via
  configure.  The changed opts exist in the config.status files, but something
  in the sed scripts is broken because they are being ignored when Makefile.in
  is turned into Makefile.  Thus, patches put back on the Makefile.in's in
  these dirs.

* Fri Jan 30 1998 Alec Habig <habig@budoe.bu.edu>
- Changes from ddd-2.2 spec file to update to 2.2.3 - mostly name changes
- Removed patches to Makefiles in exchange for suppliing the RPM build opts via
  a configure parameter.
- replace the hardwired "i586" in the programs names with a call to uname.

