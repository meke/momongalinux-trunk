%global momorel 25
Summary: A tool which displays the status of serial port modem lines.
Name: statserial
Version: 1.1
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/System
Source: ftp://metalab.unc.edu/pub/Linux/system/serial/statserial-%{version}.tar.gz
Patch0: statserial-1.1-config.patch
Patch1: statserial-1.1-dev.patch
Patch2: statserial-1.1--n.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel
NoSource: 0

%description
The statserial utility displays a table of the signals on a standard
9-pin or 25-pin serial port and indicates the status of the
handshaking lines.  Statserial is useful for debugging serial port
and/or modem problems.

Install the statserial package if you need a tool to help debug serial
port or modem problems.

%prep
%setup -q
%patch0 -p1 -b .config
%patch1 -p1 -b .dev
%patch2 -p1 -b .-n

%build
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1

install -m 755 -s statserial %{buildroot}/usr/bin/statserial
install -m 444 statserial.1 %{buildroot}%{_mandir}/man1/statserial.1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/usr/bin/statserial
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-25m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-24m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-23m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-21m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-20m)
- rebuild against gcc43

* Thu Jul 25 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-19m)
- import rawhide patches

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.1-15).

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Oct 04 1999 Cristian Gafton <gafton@redhat.com>
- rebuilt against new glibc in the sparc tree

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 13)

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- build root
- include arch sparc

* Fri May 01 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Jul 18 1997 Erik Troan <ewt@redhat.com>
- built against glibc
