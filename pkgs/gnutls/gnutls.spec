%global momorel 1
Summary: A TLS protocol implementation
Name: gnutls
Version: 3.2.12.1
Release: %{momorel}m%{?dist}
# The libgnutls library is LGPLv2+, utilities and remaining libraries are GPLv3+
License: GPLv3+ and LGPLv2+
Group: System Environment/Libraries
BuildRequires: libgcrypt-devel >= 1.2.2
BuildRequires: gettext
BuildRequires: zlib-devel
BuildRequires: readline-devel
BuildRequires: libtasn1-devel >= 3.2
BuildRequires: lzo-devel
BuildRequires: libtool
BuildRequires: automake
BuildRequires: autoconf
BuildRequires: guile-devel >= 2.0.9
BuildRequires: p11-kit-devel >= 0.20.2
BuildRequires: nettle-devel >= 2.7
BuildRequires: unbound-devel
BuildRequires: guile-devel
URL: http://www.gnutls.org/
# XXX: lib/{auth_srp_rsa.c,auth_srp_sb64.c,auth_srp_passwd.c,auth_srp.c,gnutls_srp.c,ext_srp.c}
#      are tainted by SRP patent.
Source0: ftp://ftp.gnutls.org/gcrypt/gnutls/v3.2/%{name}-%{version}.tar.xz
NoSource: 0
Source1: libgnutls-config
Patch1: gnutls-3.2.6-rpath.patch
# Use random port in some tests to avoid conflicts during simultaneous builds on the same machine
Patch9: gnutls-3.1.10-tests-rndport.patch

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libgcrypt >= 1.2.2

%package c++
Summary: The C++ interface to GnuTLS
Requires: %{name}%{?_isa} = %{version}-%{release}

%package devel
Summary: Development files for the %{name} package
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: %{name}-c++%{?_isa} = %{version}-%{release}
Requires: libgcrypt-devel
Requires: pkgconfig
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%package utils
License: GPLv3+
Summary: Command line tools for TLS protocol
Group: Applications/System
Requires: %{name}%{?_isa} = %{version}-%{release}

%package dane
Summary: A DANE protocol implementation for GnuTLS
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%package guile
Summary: Guile bindings for the GNUTLS library
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: guile

%description
GnuTLS is a project that aims to develop a library which provides a secure 
layer, over a reliable transport layer. Currently the GnuTLS library implements
the proposed standards by the IETF's TLS working group.

%description c++
GnuTLS is a project that aims to develop a library which provides a secure
layer, over a reliable transport layer. Currently the GnuTLS library implements
the proposed standards by the IETF's TLS working group.
This package contains the C++ interface for the GnuTLS library.

%description devel
GnuTLS is a project that aims to develop a library which provides a secure
layer, over a reliable transport layer. Currently the GnuTLS library implements
the proposed standards by the IETF's TLS working group.
This package contains files needed for developing applications with
the GnuTLS library.

%description utils
GnuTLS is a project that aims to develop a library which provides a secure
layer, over a reliable transport layer. Currently the GnuTLS library implements
the proposed standards by the IETF's TLS working group.
This package contains command line TLS client and server and certificate
manipulation tools.

%description dane
GnuTLS is a project that aims to develop a library which provides a secure
layer, over a reliable transport layer. Currently the GnuTLS library implements
the proposed standards by the IETF's TLS working group.
This package contains library that implements the DANE protocol for verifying
TLS certificates through DNSSEC.

%description guile
GnuTLS is a project that aims to develop a library which provides a secure
layer, over a reliable transport layer. Currently the GnuTLS library implements
the proposed standards by the IETF's TLS working group.
This package contains Guile bindings for the library.

%prep
%setup -q -n %{name}-3.2.12
%patch1 -p1 -b .rpath
%patch9 -p1 -b .rndport

#for i in auth_srp_rsa.c auth_srp_sb64.c auth_srp_passwd.c auth_srp.c gnutls_srp.c ext_srp.c; do
#    touch lib/$i
#done

%build

export LDFLAGS="-Wl,--no-add-needed"

%configure --with-libtasn1-prefix=%{_prefix} \
           --disable-static \
           --disable-openssl-compatibility \
           --enable-guile \
           --enable-libdane \
           --disable-rpath

make

%install
rm -fr %{buildroot}
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_bindir}/srptool
rm -f %{buildroot}%{_bindir}/gnutls-srpcrypt
install -m 0755 %{SOURCE1} %{buildroot}%{_bindir}/libgnutls-config
install -m 0755 %{SOURCE1} %{buildroot}%{_bindir}/libgnutls-extra-config
rm -f %{buildroot}%{_mandir}/man1/srptool.1
rm -f %{buildroot}%{_mandir}/man3/*srp*
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/libguile*.a

%find_lang gnutls

%check
make check

%clean
rm -fr %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post c++ -p /sbin/ldconfig

%postun c++ -p /sbin/ldconfig

%post devel
if [ -f %{_infodir}/gnutls.info.bz2 ]; then
    /sbin/install-info %{_infodir}/gnutls.info %{_infodir}/dir || :
fi

%preun devel
if [ $1 = 0 -a -f %{_infodir}/gnutls.info.bz2 ]; then
   /sbin/install-info --delete %{_infodir}/gnutls.info %{_infodir}/dir || :
fi

%post guile -p /sbin/ldconfig

%postun guile -p /sbin/ldconfig

%files -f gnutls.lang
%defattr(-,root,root,-)
%{_libdir}/libgnutls.so.*
%{_libdir}/libgnutls-xssl.so.*
%doc ABOUT-NLS COPYING COPYING.LESSER ChangeLog INSTALL NEWS README THANKS AUTHORS

%files c++
%{_libdir}/libgnutlsxx.so.*

%files devel
%defattr(-,root,root,-)
%{_bindir}/libgnutls*-config
%{_includedir}/*
%{_libdir}/libgnutls*.so
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man3/*
%{_infodir}/gnutls*
%{_infodir}/pkcs11-vision*

%files utils
%defattr(-,root,root,-)
%{_bindir}/certtool
%{_bindir}/crywrap
%{_bindir}/danetool
%{_bindir}/ocsptool
%{_bindir}/psktool
%{_bindir}/p11tool
%{_bindir}/tpmtool
%{_bindir}/gnutls*
%{_mandir}/man1/*
%doc doc/certtool.cfg

%files dane
%defattr(-,root,root,-)
%{_libdir}/libgnutls-dane.so.*

%files guile
%defattr(-,root,root,-)
%{_libdir}/guile/2.0/*
%{_datadir}/guile/site/gnutls
%{_datadir}/guile/site/gnutls.scm

%changelog
* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.12.1-1m)
- [SECURITY] CVE-2014-0092
- update to 3.2.12.1

* Sat Mar  8 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.9-2m)
- fix %%files
- can not build this package under new kernel yet

* Fri Feb 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.9-1m)
- update to 3.2.9

* Thu Nov 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.6-1m)
- [SECURITY] CVE-2013-4466 CVE-2013-4487
- update to 3.2.6

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.23-2m)
- rebuild against guile-2.0.9

* Mon Feb 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.23-1m)
- [SECURITY] CVE-2013-1619
- update to 2.12.23

* Fri Jan  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.21-1m)
- update to 2.12.21
- rebuild against libtasn1-3.2

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.20-1m)
- update 2.12.20

* Mon Mar 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.18-1m)
- [SECURITY] CVE-2012-1569 CVE-2012-1573
- update to 2.12.18

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.17-1m)
- update to 2.12.17
- it seems 2.12.16 or 2.12.17 does not affected CVE-2012-1663, right?

* Wed Jan 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.16-1m)
- [SECURITY] CVE-2012-0390
- update to 2.12.16

* Thu Nov 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.14-1m)
- [SECURITY] CVE-2011-4128
- update to 2.12.14, bug fix release

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.12-1m)
- update 2.12.12

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.11-1m)
- update 2.12.11

* Sun Aug 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.9-1m)
- update 2.12.9, bug fix release
- fix BuildRequires

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.12.8-1m)
- update 2.12.8

* Tue Aug  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.7-1m)
- fix build failure
- merge with fedora's gnutls-2.12.7-2
-- fix problem when using new libgcrypt
-- split libgnutlsxx to a subpackage (#455146)
-- drop libgnutls-openssl (#460310)
- temporarily disable valgrind tests

* Mon Apr 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.2-2m)
- fix permissions of %%{_bindir}/libgnutls-{,extra-}config

* Wed Apr 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.1-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Nishio Fuotshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.6-6m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (2.8.6-5m)
- add gmp-devel to BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.6-4m)
- full rebuild for mo7 release

* Fri Jun 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-3m)
- [SECURITY] CVE-2009-3555 
- apply Patch3: gnutls-2.8.6-safe-renegotiation.patch

* Thu Jun  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.6-2m)
- fix file permissions
-- %%{_bindir}/libgnutls*-config should be executable

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.6-1m)
- update to 2.8.6
- split out guile package

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.6-7m)
- rebuild against readline6

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.6-6m)
- modify __os_install_post for new momonga-rpmmacros

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.6-5m)
- fix BuildRequires:

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.6-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.6-2m)
- [SECURITY] CVE-2009-2730
- import a security patch (Patch1) from Fedora 11 (2.6.6-2)

* Mon May  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.6-1m)
- [SECURITY] CVE-2009-1415 CVE-2009-1416 CVE-2009-1417
- update to 2.6.6

* Mon Mar 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-2m)
- update Source2: brp-compress to enable xz compression

* Sat Feb  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-1m)
- [SECURITY] CVE-2009-2409
- update to 2.6.4
-- %%define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-4m)
- rebuild against rpm-4.6

* Wed Nov 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-3m)
- do not bzip2 %%{_infodir}/*.png
- fix installation of info files

* Tue Nov 11 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.1-2m)
- [SECURITY] CVE-2008-4989 GNUTLS-SA-2008-3
- import a security patch (Patch1) from Gentoo

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-5m)
- %%NoSource -> NoSource

* Mon Mar 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-4m)
- rebuild against libgcrypt-1.2.4-2m

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-3m)
- retrived libtool library

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-2m)
- delete libtool library

* Mon Jan  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Sun Dec 10 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.4-1m)
- update to 1.4.4
- import 2 patches from FC
- [SECURITY]Variant of Bleichenbacher's crypto 06 rump session attack (CVE-2006-4790)

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.10-2m)
- rebuild against readline-5.0

* Thu May  4 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.10-1m)
- version up

* Sat Nov 12 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.9-2m)
- use install-info.
  file /usr/share/info/dir from install of gnutls-devel-1.2.9-1m
  conflicts with file from package info-4.8-5m.

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.9-1m)
- [SECURITY] CVE-2010-0731 (fixed in 1.2.1)
- up to 1.2.9

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.24-1m)
- import from Fedora
- version 1.0.24

* Tue Jan  4 2005 Ivana Varekova <varekova@redhat.com> 1.0.20-5
- add gnutls Requires zlib-devel (#144069)

* Mon Nov 08 2004 Colin Walters <walters@redhat.com> 1.0.20-4
- Make gnutls-devel Require libgcrypt-devel

* Tue Sep 21 2004 Jeff Johnson <jbj@redhat.com> 1.0.20-3
- rebuild with release++, otherwise unchanged.

* Tue Sep  7 2004 Jeff Johnson <jbj@redhat.com> 1.0.20-2
- patent tainted SRP code removed.

* Sun Sep  5 2004 Jeff Johnson <jbj@redhat.com> 1.0.20-1
- update to 1.0.20.
- add --with-included-opencdk --with-included-libtasn1
- add --with-included-libcfg --with-included-lzo
- add --disable-srp-authentication.
- do "make check" after build.

* Fri Mar 21 2003 Jeff Johnson <jbj@redhat.com> 0.9.2-1
- upgrade to 0.9.2

* Tue Jun 25 2002 Jeff Johnson <jbj@redhat.com> 0.4.4-1
- update to 0.4.4.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sat May 25 2002 Jeff Johnson <jbj@redhat.com> 0.4.3-1
- update to 0.4.3.

* Tue May 21 2002 Jeff Johnson <jbj@redhat.com> 0.4.2-1
- update to 0.4.2.
- change license to LGPL.
- include splint annotations patch.

* Tue Apr  2 2002 Nalin Dahyabhai <nalin@redhat.com> 0.4.0-1
- update to 0.4.0

* Thu Jan 17 2002 Nalin Dahyabhai <nalin@redhat.com> 0.3.2-1
- update to 0.3.2

* Wed Jan 10 2002 Nalin Dahyabhai <nalin@redhat.com> 0.3.0-1
- add a URL

* Wed Dec 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- initial package
