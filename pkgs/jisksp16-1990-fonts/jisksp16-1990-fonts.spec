%global momorel 6

%define	fontname	jisksp16-1990
%define	catalogue	%{_sysconfdir}/X11/fontpath.d

Name:		%{fontname}-fonts
Version:	0.983
Release:	%{momorel}m%{?dist}
Summary:	16x16 JIS X 0212:1990 Bitmap font
Group:		User Interface/X
License:	Public Domain

URL:		http://kanji.zinbun.kyoto-u.ac.jp/~yasuoka/ftp/fonts/
Source0:	http://kanji.zinbun.kyoto-u.ac.jp/~yasuoka/ftp/fonts/jisksp16-1990.bdf.Z

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	gzip mkfontdir xorg-x11-font-utils fontpackages-devel

Requires:	fontpackages-filesystem
Conflicts:	fonts-japanese <= 0.20061016-11.fc8
Provides:	jisksp16-1990 = 0.1-16
Obsoletes:	jisksp16-1990 <= 0.1-16

%description
This package provides 16x16 Japanese bitmap font for JIS X 0212:1990.
JIS X 0212:1990 is a character sets that contains the auxiliary kanji
characters.


%prep
%setup -c -T
gunzip -c %{SOURCE0} > jisksp16-1990.bdf

%build
%{_bindir}/bdftopcf jisksp16-1990.bdf | gzip -9c > jisksp16-1990.pcf.gz

%install
rm -rf $RPM_BUILD_ROOT

install -m 0755 -d $RPM_BUILD_ROOT%{_fontdir}
install -m 0755 -d $RPM_BUILD_ROOT%{catalogue}

install -m 0644 -p jisksp16-1990.pcf.gz $RPM_BUILD_ROOT%{_fontdir}/

%{_bindir}/mkfontdir $RPM_BUILD_ROOT%{_fontdir}

# Install catalogue symlink
ln -sf %{_fontdir} $RPM_BUILD_ROOT%{catalogue}/%{name}


%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg jisksp16-1990.pcf.gz

%verify(not md5 size mtime) %{_fontdir}/fonts.dir
%{catalogue}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.983-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.983-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.983-4m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.983-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.983-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.983-1m)
- import from Fedora

* Thu Mar 26 2009 Akira TAGOH <tagoh@redhat.com> - 0.983-4
- Update a spec file a bit.
- rebuild to correct autoprovides. (#491965)

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.983-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Sep 24 2007 Akira TAGOH <tagoh@redhat.com> - 0.983-2
- Use %%setup.

* Fri Aug 17 2007 Akira TAGOH <tagoh@redhat.com> - 0.983-1
- Split up from fonts-japanese.

