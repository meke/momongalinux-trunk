%global momorel 10

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

%define packagename PEAK-Rules
%define devrev 2646
%define module PEAK_Rules

%define docs README.txt AST-Builder.txt Code-Generation.txt Criteria.txt DESIGN.txt Indexing.txt Predicates.txt Syntax-Matching.txt

Name:           python-peak-rules
Version:        0.5a1.dev
# At earliest opportunity, move all non-numeric information to release.
# This would be proper:
# Version: 0.5
# Release:0.3.a1.dev%{devrev}%{?dist}
# But we can't do that yet because it breaks the upgrade path.
# When version hits 0.5.1 or 0.6 we can correct this.
Release:        %{momorel}m%{?dist}
Summary:        Generic functions and business rules support systems

Group:          Development/Languages
License:        "PSF" or "ZPL"
URL:            http://pypi.python.org/pypi/PEAK-Rules
Source0:        http://peak.telecommunity.com/snapshots/%{packagename}-%{version}-r%{devrev}.tar.gz
Patch0:         %{name}-setup.patch
Patch1:         %{name}-x86_64-doctest.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel
BuildRequires:  python-peak-util-assembler >= 0.6
BuildRequires:  python-peak-util-addons >= 0.6
BuildRequires:  python-peak-util-extremes >= 1.1
BuildRequires:  python-decoratortools >= 1.7

Requires:       python-peak-util-assembler >= 0.6
Requires:       python-peak-util-addons >= 0.6
Requires:       python-peak-util-extremes >= 1.1
Requires:       python-decoratortools >= 1.7

%description
PEAK-Rules is a highly-extensible framework for creating and using generic
functions, from the very simple to the very complex.  Out of the box, it
supports multiple-dispatch on positional arguments using tuples of types,
full predicate dispatch using strings containing Python expressions, and
CLOS-like method combining.  (But the framework allows you to mix and match
dispatch engines and custom method combinations, if you need or want to.)

%prep
%setup -q -n %{packagename}-%{version}-r%{devrev}
%patch0 -b .setup
%patch1 -p0 -b .test

%{__chmod} -x %{docs}

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%check
%{__python} setup.py test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc %{docs}
%{python_sitelib}/peak/rules
%{python_sitelib}/%{module}-%{version}_r%{devrev}-py%{pyver}.egg-info
%{python_sitelib}/%{module}-%{version}_r%{devrev}-py%{pyver}-nspkg.pth

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5a1.dev-10m)
- rebuild against python-2.7

* Thu Apr 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5a1.dev-9m)
- update to 20100803svn r2646

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5a1.dev-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5a1.dev-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5a1.dev-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a1.dev-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a1.dev-4m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a1.dev-3m)
- delete duplicate directory

* Sun Jan  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a1.dev-2m)
- apply Patch2 for 64 bit architectures

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a1.dev-1m)
- import from Rawhide

* Tue Dec 2 2008 Toshio Kuratomi <toshio@fedoraproject.org> - 0.5a1.dev-4.2582
- Update patch for some more doctest fixing under py2.6.

* Tue Dec 2 2008 Toshio Kuratomi <toshio@fedoraproject.org> - 0.5a1.dev-3.2582
- Update to latest development snapshot
- Enable test suite
- Patch so doctests pass

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.5a1.dev-2.2581
- Rebuild for Python 2.6

* Tue Oct 14 2008 Luke Macken <lmacken@redhat.com> - 0.5a1.dev-1.2581
- Revision bump to fix upgrade path

* Sat Oct 11 2008 Luke Macken <lmacken@redhat.com> - 0.5a1.dev-0.1.2581
- Update to the latest 0.5a1 development snapshot
- Fix the description

* Sun Aug  3 2008 Luke Macken <lmacken@redhat.com> - 0.5a1.dev-0.2569
- Initial package for Fedora
