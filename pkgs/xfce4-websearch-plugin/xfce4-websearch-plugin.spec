%global momorel 1

%global xfce4ver 4.10.0
%global srcdate 20120905

Name:		xfce4-websearch-plugin
Version:	0.1.1
Release:	0.2.%{srcdate}.%{momorel}m%{?dist}
Summary:	Websearch plugin from the XFCE panel

Group:		User Interface/Desktops
License:	GPL
URL:		http://goodies.xfce.org/
# Source0 was generated with the following commands:
# git clone git://git.xfce.org/panel-plugins/xfce4-websearch-plugin
# tar -cjf xfce4-websearch-plugin-`date +%G%m%d`git.tar.bz2 xfce4-websearch-plugin
Source0:	%{name}-20120905git.tar.bz2
#Source0:	http://goodies.xfce.org/releases/%{name}/%{name}-%{version}.tar.gz
Patch0:		%{name}-20070428svn2704-multilib.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=509294
# not yet in Xfce bugzilla, because the websearch component is missing
Patch1:		%{name}-20070428svn2704-desktop-file.patch
#Patch1: xfce4-websearch-plugin-autoconf.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	libxml2-devel
BuildRequires:	xfce4-dev-tools >= 4.6.0
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
Access searchengines from the XFCE panel.

%prep
%setup -qn %{name}
#%%patch1 -p1 -b .autoconf
./autogen.sh
%patch0 -b .multilib
%patch1 -b .path

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_libdir}/xfce4/panel-plugins/*.so
#%%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_libdir}/xfce4/panel-plugins/*.desktop

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.1-0.2.20120905.1m)
- update
- build against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-0.2.20070428.11m)
- rebuild against xfcd4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-0.2.20070428.10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-0.2.20070428.9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-0.2.20070428.8m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-0.2.20070428.7m)
- rebuild against xfcd4-4.6.2

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-0.2.20070428.6m)
- add patch1 for build fix

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-0.2.20070428.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-0.2.20070428.4m)
- rebuild against xfcd4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-0.2.20070428.3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-0.2.20070428.2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-0.20070428.1m)
- sync with fedora 0.1.1-0.6.20070428svn2704
- - source update
- - add Patch0: %{name}-20070428svn2704-multilib.patch

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-0.20060923.3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-0.20060923.2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-0.20060923.1m)
- import to Momonga from fc

* Thu Oct 05 2006 Christoph Wickert <fedora christoph-wickert de> - 0.1.1-0.2.20060923svn
- Bump release for devel checkin.

* Sat Sep 23 2006 Christoph Wickert <fedora christoph-wickert de> - 0.1.1-0.1.20060923svn
- Update to svn snapshot of Sep 23rd 2006 on Xfce 4.3.99.1.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 0.1.0-7
- Mass rebuild for Fedora Core 6.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 0.1.0-7
- Mass rebuild for Fedora Core 6.

* Tue Apr 11 2006 Christoph Wickert <fedora wickert at arcor de> - 0.1.0-6
- Require xfce4-panel.

* Thu Feb 16 2006 Christoph Wickert <fedora wickert at arcor de> - 0.1.0-5
- Rebuild for Fedora Extras 5.

* Thu Dec 01 2005 Christoph Wickert <fedora wickert at arcor de> - 0.1.0-4
- Add libxfcegui4-devel BuildReqs.
- Fix %%defattr.

* Mon Nov 14 2005 Christoph Wickert <fedora wickert at arcor de> - 0.1.0-3
- Initial Fedora Extras version.
- Rebuild for XFCE 4.2.3.
- disable-static instead of removing .a files.

* Fri Sep 23 2005 Christoph Wickert <fedora wickert at arcor de> - 0.1.0-2.fc4.cw
- Add libxml2 BuildReqs.

* Sat Jul 30 2005 Christoph Wickert <fedora wickert at arcor de> - 0.1.0-1%{?dist}%{?repo}
- Initial RPM release.
