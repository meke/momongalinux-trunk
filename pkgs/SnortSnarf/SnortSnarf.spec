%global momorel 4

Summary: Snort Alert Analyzer
Name: SnortSnarf
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
Source0: http://www.silicondefense.com/software/snortsnarf/SnortSnarf-%{version}.tar.gz
URL: http://www.silicondefense.com/software/snortsnarf/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: perl >= 5.8.5
BuildRequires: perl-Time-modules >= 2002.1001-3m
BuildRequires: perl-DBD-MySQL >= 4.012-2m
Provides: perl(BasicFilters), perl(BasicSorters), perl(KnownEquiv), perl(TimeFilters), perl(IPAddrContact)

%description
This program creates a set of HTML pages to allow you to quickly and
conveniently navigate around alerts output by the Snort intrusion
detection system (http://www.snort.org/).

%prep

%setup -q

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_datadir}/snortsnarf
cp -a snortsnarf.pl %{buildroot}%{_sbindir}/snortsnarf
cp -a include/* %{buildroot}%{_datadir}/snortsnarf
find cgi nmap2html sisr utilities -type f -exec chmod -x {} \;

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING Changes README Usage cgi nmap2html sisr utilities
%{_sbindir}/snortsnarf
%{_datadir}/snortsnarf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- full rebuild for mo7 release

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-1m)
- update 1.0

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0-0.20050314.1.4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-0.20050314.1.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0-0.20050314.1.2m)
- modify BuildRequires: perl-DBD-MySQL

* Wed May 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0-0.20050314.1.1m)
- update 20050314
- change Snort.org SID database link

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-0.20021111.1.11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0-0.20021111.1.10m)
- rebuild against gcc43

* Wed Jan  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0-0.20021111.1.9m)
- rebuild against perl-DBD-mysql-4.00-1m
- add BuildPreReq: perl--DBD-mmysql-4.00-1m
- add Patch2 to fix DBD-mysql module name

* Sun Aug 22 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0-0.20021111.1.8m)
- build against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.0-0.20021111.1.7m)
- remove Epoch from BuildPrereq

* Fri May 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20021111.1.6m)
- cancel NoSource since the primary site has gone...

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0-0.20021111.1.5m)
- rebuild against perl-5.8.2
- rebuild against perl-Time-modules >= 2002.1001-3m

* Tue Sep  9 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.0-0.20021111.1.4m)
- fix typo

* Fri Nov 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20021111.1.3m)
- append inc path

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20021111.1.2m)
- rebuild against perl-5.8.0

* Wed Nov 13 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20021111.1.1m)

* Mon Nov  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20021024.1.1m)

* Sat Oct  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.20020516.1.4m)
- revise 'SnortSnarf-020516.1-momonga.patch' for snort-1.9

* Fri Jul 12 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.00205161003m)
- add 'Usage' to %doc

* Wed Jul 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.00205161002m)
- disable empty sid entry and revise whitehats URI

* Wed Jul 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.0-0.00205161001m)
- version 020516.1
