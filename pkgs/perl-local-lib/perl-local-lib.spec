%global         momorel 1

Name:           perl-local-lib
Version:        2.000012
Release:        %{momorel}m%{?dist}
Summary:        Create and use a local lib/ for perl modules with PERL5LIB
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/local-lib/
Source0:        http://www.cpan.org/authors/id/H/HA/HAARG/local-lib-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-CPAN >= 1.82
BuildRequires:  perl-ExtUtils-CBuilder
BuildRequires:  perl-ExtUtils-Install >= 1.43
BuildRequires:  perl-ExtUtils-MakeMaker >= 6.31
BuildRequires:  perl-ExtUtils-ParseXS
BuildRequires:  perl-Module-Build >= 0.28
Requires:       perl-CPAN >= 1.82
Requires:       perl-ExtUtils-CBuilder
Requires:       perl-ExtUtils-Install >= 1.43
Requires:       perl-ExtUtils-MakeMaker >= 6.31
Requires:       perl-ExtUtils-ParseXS
Requires:       perl-Module-Build >= 0.28
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides a quick, convenient way of bootstrapping a user-local
Perl module library located within the user's home directory. It also
constructs and prints out for the user the list of environment variables
using the syntax appropriate for the user's current shell (as specified by
the SHELL environment variable), suitable for directly adding to one's
shell configuration file.

%prep
%setup -q -n local-lib-%{version}

%build
echo yes | %{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/POD2/DE/local/lib.pod
%{perl_vendorlib}/POD2/PT_BR/local/lib.pod
%{perl_vendorlib}/lib/core/only.pm
%{perl_vendorlib}/local/lib.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.000012-1m)
- rebuild against perl-5.20.0
- update to 2.000012

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.000011-1m)
- update to 2.000011

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.000010-1m)
- update to 2.000010

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.000008-1m)
- update to 2.000008

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.000004-2m)
- rebuild against perl-5.18.2

* Sat Jan  4 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.000004-1m)
- update to 2.000004

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.000003-1m)
- update to 2.000003

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008026-1m)
- update to 1.008026

* Sun Oct 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008023-1m)
- update to 1.008023

* Mon Sep 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008018-1m)
- update to 1.008018

* Sun Sep 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008017-1m)
- update to 1.008017

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008016-1m)
- update to 1.008016

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008011-3m)
- rebuild against perl-5.18.1

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008011-2m)
- rebuild against perl-5.18.1

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008011-1m)
- update to 1.008011

* Tue May 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008010-1m)
- update to 1.008010

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008009-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008009-2m)
- rebuild against perl-5.16.3

* Mon Feb 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008009-1m)
- update to 1.008009

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008007-1m)
- update to 1.008007

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008006-1m)
- update to 1.008006

* Tue Feb 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008005-1m)
- update to 1.008005

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008004-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008004-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008004-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008004-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008004-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008004-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.008004-2m)
- rebuild for new GCC 4.6

* Fri Feb 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008004-1m)
- update to 1.008004

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008003-1m)
- update to 1.008003

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008001-1m)
- update to 1.008001

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.008000-1m)
- update to 1.008000

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007000-1m)
- update to 1.007000

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006009-1m)
- update to 1.006009

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.006008-2m)
- rebuild for new GCC 4.5

* Wed Nov 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006008-1m)
- update to 1.006008

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.006007-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
