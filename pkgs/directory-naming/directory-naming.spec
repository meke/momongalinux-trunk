%global momorel 4

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

Name:           directory-naming
Version:        0.8
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Apache Directory Naming Component
License:        "ASL 2.0"
URL:            http://directory.apache.org
Group:          Development/Libraries

Source0:        directory-naming-0.8.tar.gz
# svn export -r 124846 http://svn.apache.org/repos/asf/directory/sandbox/dormant-subprojects/naming/ directory-naming-0.8

Source5:        http://repo1.maven.org/maven2/directory-naming/naming-core/0.8/naming-core-0.8.pom
Source6:        http://repo1.maven.org/maven2/directory-naming/naming-config/0.8/naming-config-0.8.pom
Source7:        http://repo1.maven.org/maven2/directory-naming/naming-factory/0.8/naming-factory-0.8.pom
Source8:        http://repo1.maven.org/maven2/directory-naming/naming-java/0.8/naming-java-0.8.pom
Source9:        http://repo1.maven.org/maven2/directory-naming/naming-management/0.8/naming-management-0.8.pom
Source10:       http://repo1.maven.org/maven2/directory-naming/naming-resources/0.8/naming-resources-0.8.pom

BuildRequires:  jpackage-utils >= 0:1.7.3
BuildRequires:  java-devel >= 0:1.4.2
BuildRequires:  ant >= 0:1.6.5
BuildRequires:  hsqldb
BuildRequires:  junit
BuildRequires:  ant-junit
BuildRequires:  jakarta-commons-beanutils
BuildRequires:  jakarta-commons-collections
BuildRequires:  jakarta-commons-dbcp
BuildRequires:  jakarta-commons-digester
BuildRequires:  jakarta-commons-lang
BuildRequires:  jakarta-commons-logging
BuildRequires:  jakarta-commons-pool
BuildRequires:  classpathx-mail
BuildRequires:  mx4j

Requires:  jakarta-commons-beanutils
Requires:  jakarta-commons-collections
Requires:  jakarta-commons-dbcp
Requires:  jakarta-commons-digester
Requires:  jakarta-commons-lang
Requires:  jakarta-commons-logging
Requires:  jakarta-commons-pool
Requires:  classpathx-mail
Requires:  mx4j
Requires(post):    jpackage-utils >= 0:1.7.3
Requires(postun):  jpackage-utils >= 0:1.7.3

BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Naming is a lightweight, in-memory JNDI service provider.  To
enable flexible deployment with limited dependencies, Naming is divided in 6
packages, each producing a separate jar artifact.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
%{summary}.

%prep
%setup -q
# remove all binary libs
for j in $(find . -name "*.jar"); do
    mv $j $j.no
done
sed -i "s/\r//" LICENSE.txt

%build
export CLASSPATH=$(build-classpath \
commons-beanutils \
commons-collections \
commons-dbcp \
commons-digester \
commons-lang \
commons-logging \
commons-pool \
hsqldb \
javamail \
junit \
mx4j/mx4j-jmx \
)

CLASSPATH=$CLASSPATH:$(pwd)/naming-config/target/classes:$(pwd)/naming-config/target/test-classes
CLASSPATH=$CLASSPATH:$(pwd)/naming-core/target/classes:$(pwd)/naming-core/target/test-classes
CLASSPATH=$CLASSPATH:$(pwd)/naming-factory/target/classes:$(pwd)/naming-factory/target/test-classes
CLASSPATH=$CLASSPATH:$(pwd)/naming-java/target/classes:$(pwd)/naming-java/target/test-classes
CLASSPATH=$CLASSPATH:$(pwd)/naming-management/target/classes:$(pwd)/naming-management/target/test-classes
CLASSPATH=$CLASSPATH:$(pwd)/naming-resources/target/classes:$(pwd)/naming-resources/target/test-classes

export OPT_JAR_LIST="junit ant/ant-junit"

ant -Dbuild.sysclasspath=only jar javadoc

%install
rm -rf %{buildroot}
# jars
%__mkdir_p %{buildroot}%{_javadir}/%{name}
for p in \
         naming-config \
         naming-core \
         naming-factory \
         naming-java \
         naming-management \
         naming-resources \
         ; do
%__install -m 644 $p/target/$p-%{version}.jar %{buildroot}%{_javadir}/%{name}/$p-%{version}.jar
done
(cd %{buildroot}%{_javadir}/%{name} && for jar in *-%{version}*; do %__ln_s ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# poms
%__mkdir_p %{buildroot}%{_datadir}/maven2/poms
%__install -m 644 %{SOURCE5}  %{buildroot}%{_datadir}/maven2/poms/JPP.%{name}-naming-core.pom
%add_to_maven_depmap %{name} naming-core %{version} JPP/%{name} naming-core
%__install -m 644 %{SOURCE6}  %{buildroot}%{_datadir}/maven2/poms/JPP.%{name}-naming-config.pom
%add_to_maven_depmap %{name} naming-config %{version} JPP/%{name} naming-config
%__install -m 644 %{SOURCE7}  %{buildroot}%{_datadir}/maven2/poms/JPP.%{name}-naming-factory.pom
%add_to_maven_depmap %{name} naming-factory %{version} JPP/%{name} naming-factory
%__install -m 644 %{SOURCE8}  %{buildroot}%{_datadir}/maven2/poms/JPP.%{name}-naming-java.pom
%add_to_maven_depmap %{name} naming-java %{version} JPP/%{name} naming-java
%__install -m 644 %{SOURCE9}  %{buildroot}%{_datadir}/maven2/poms/JPP.%{name}-naming-management.pom
%add_to_maven_depmap %{name} naming-management %{version} JPP/%{name} naming-management
%__install -m 644 %{SOURCE10} %{buildroot}%{_datadir}/maven2/poms/JPP.%{name}-naming-resources.pom
%add_to_maven_depmap %{name} naming-resources %{version} JPP/%{name} naming-resources

# docs
%__mkdir_p %{buildroot}%{_docdir}/%{name}-%{version}
%__cp LICENSE.txt  %{buildroot}%{_docdir}/%{name}-%{version}

%clean
%__rm -rf %{buildroot}

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(0644,root,root,0755)
%{_docdir}/%{name}-%{version}/LICENSE.txt
%{_javadir}/%{name}
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-2m)
- full rebuild for mo7 release

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8-1m)
- import from Fedora 13

* Thu Aug 20 2009 Alexander Kurtakov <akurtako@redhat.com> 0:0.8-7
- Fix Summary and description.

* Thu Aug 20 2009 Alexander Kurtakov <akurtako@redhat.com> 0:0.8-6
- Remove patch and sed lines -not needed.

* Wed Aug 19 2009 Alexander Kurtakov <akurtako@redhat.com> 0:0.8-5
- Remove gcj support.
- Build with build.sysclasspath=only.

* Sun May 17 2009 Fernando Nasser <fnasser@redhat.com> - 0.8-4
- Fix license

* Tue Mar 17 2009 Yong Yang <yyang@redhat.com> - 0.8-3
- rebuild with new maven2 2.0.8 built in bootstrap mode

* Thu Feb 05 2009 Yong Yang <yyang@redhat.com> 0.8-2
- Initial building on JPP6

* Wed Apr 09 2008 Deepak Bhole <dbhole@redhat.com> 0.8-1jpp.1
- Import from jpp, remove maven (1.x) build, update per Fedora guidelines

* Fri Nov 30 2007 Ralph Apel <r.apel@r-apel.de> 0:0.8-1jpp
- First release
