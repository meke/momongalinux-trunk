%global momorel 1
%global build_maemo 1

Summary: The Advanced Linux Sound Architecture (ALSA) Plugins
Name: alsa-plugins
Version: 1.0.28
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: System Environment/Libraries
URL: http://www.alsa-project.org/
Source0: ftp://ftp.alsa-project.org/pub/plugins/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-1.0.26-pulse-conf.patch
Source1: 50-a52.conf
Source2: 50-arcamav.conf
Source3: 50-jack.conf
Source4: 50-lavcrate.conf
Source5: 98-maemo.conf
Source6: 50-pcm-oss.conf
Source7: 10-samplerate.conf
Source8: 10-speex.conf
Source9: 50-upmix.conf
Source10: 97-vdownmix.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel >= %{version}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: dbus-devel
BuildRequires: faac-devel
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: jack-devel
BuildRequires: lame-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXdmcp-devel
BuildRequires: libcap-devel
BuildRequires: libogg-devel
BuildRequires: libsamplerate-devel
BuildRequires: libtheora-devel
BuildRequires: libtool
BuildRequires: libvorbis-devel
BuildRequires: libxcb-devel
BuildRequires: ncurses-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: speex-devel
BuildRequires: x264-devel
BuildRequires: zlib-devel

%description
The Advanced Linux Sound Architecture (ALSA) provides audio and MIDI
functionality to the Linux operating system.

This package includes plugins for ALSA.

%package a52
Summary: A52 compressed stream plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}
 
%description a52
This plugin converts S16 linear format to A52 compressed stream and
send to an SPDIF output.

%package arcamav
Summary: Arcam AV amplifier plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}

%description arcamav
This plugin exposes the controls for an Arcam AV amplifier
(see: http://www.arcam.co.uk/) as an ALSA mixer device.

%package jack
Summary: Jack PCM output plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: jack
Requires: alsa-utils >= %{version}

%description jack
This plugin converts the ALSA API over JACK (Jack Audio Connection
Kit, http://jackit.sf.net) API.  ALSA native applications can work
transparently together with jackd for both playback and capture.

    ALSA apps (playback) -> ALSA-lib -> JACK plugin -> JACK daemon
    ALSA apps (capture) <- ALSA-lib <- JACK plugin <- JACK daemon

This plugin provides the PCM type "jack"

%package lavcrate
Summary: External rate converter using libavcodec's resampler plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}
 
%description lavcrate
The plugin is an external rate converter using libavcodec's resampler.

%if %{build_maemo}
%package maemo
Summary: Maemo plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}

%description maemo
This plugin converts the ALSA API over PCM task nodes protocol. In this way,
ALSA native applications can run over DSP Gateway and use DSP PCM task nodes.
%endif

%package oss
Summary: Oss PCM output plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}
 
%description oss
This plugin converts the ALSA API over OSS API.  With this plugin,
ALSA native apps can run on OSS drivers.

This plugin provides the PCM type "oss".

%package pulseaudio
Summary: Alsa to PulseAudio backend
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}
Requires: pulseaudio

%description pulseaudio
This plugin allows any program that uses the ALSA API to access a PulseAudio
sound daemon. In other words, native ALSA applications can play and record
sound across a network. There are two plugins in the suite, one for PCM and
one for mixer control.

%package samplerate
Summary: External rate converter using libsamplerate plugin for ALSA
Group: System Environment/Libraries
License: GPLv2+
Requires: alsa-utils >= %{version}

%description samplerate
This plugin is an external rate converter using libsamplerate by Erik de
Castro Lopo.

%package speex
Summary: Speex preprocess plugin for ALSA
Group: System Environment/Libraries
License: GPLv2+
Requires: alsa-utils >= %{version}

%description speex
This plugin provides a pre-processing of a mono stream like denoise
using libspeex DSP API.

%package upmix
Summary: Upmixer channel expander plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}

%description upmix
The upmix plugin is an easy-to-use plugin for upmixing to 4 or
6-channel stream.  The number of channels to be expanded is determined
by the slave PCM or explicitly via channel option.

%package usbstream
Summary: USB stream plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}

%description usbstream
The usbstream plugin is for snd-usb-us122l driver. It converts PCM
stream to USB specific stream.

%package vdownmix
Summary: Downmixer to stereo plugin for ALSA
Group: System Environment/Libraries
License: LGPLv2+
Requires: alsa-utils >= %{version}

%description vdownmix
The vdownmix plugin is a downmixer from 4-6 channels to 2-channel
stereo headphone output.  This plugin processes the input signals with
a simple spacialization, so the output sounds like a kind of "virtual
surround".

%prep
%setup -q

%patch0 -p1 -b .pulse-conf

autoreconf -fi

%build
# workaround for ffmpeg
CFLAGS=`pkg-config libavcodec --cflags` \
LDFLAGS=`pkg-config libavcodec --libs` \
%configure \
%if %{build_maemo}
	--enable-maemo-plugin \
	--enable-maemo-resource-manager \
%endif
	--with-speex=lib
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install configuration files
install -d %{buildroot}%{_datadir}/alsa/alsa.conf.d
install -m 644 %{SOURCE1} %{SOURCE2} \
               %{SOURCE3} %{SOURCE4} \
               %{SOURCE5} %{SOURCE6} \
               %{SOURCE7} %{SOURCE8} \
               %{SOURCE9} %{SOURCE10} \
                   %{buildroot}%{_datadir}/alsa/alsa.conf.d
mv %{buildroot}%{_datadir}/alsa/alsa.conf.d/99-pulseaudio-default.conf.example \
	%{buildroot}%{_datadir}/alsa/alsa.conf.d/99-pulseaudio-default.conf

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/alsa-lib/*.la

%if ! %{build_maemo}
# removing the maemo plugins
rm -f %{buildroot}%{_libdir}/alsa-lib/libasound_module_ctl_dsp_ctl.so
rm -f %{buildroot}%{_libdir}/alsa-lib/libasound_module_pcm_alsa_dsp.so
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files a52
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/a52.txt
%{_libdir}/alsa-lib/libasound_module_pcm_a52.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/50-a52.conf

%files arcamav
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/README-arcam-av
%{_libdir}/alsa-lib/libasound_module_ctl_arcam_av.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/50-arcamav.conf

%files jack
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/README-jack
%{_libdir}/alsa-lib/libasound_module_pcm_jack.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/50-jack.conf

%files lavcrate
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/lavcrate.txt
%{_libdir}/alsa-lib/libasound_module_rate_lavcrate*.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/50-lavcrate.conf

%if %{build_maemo}
%files maemo
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/README-maemo
%{_libdir}/alsa-lib/libasound_module_ctl_dsp_ctl.so
%{_libdir}/alsa-lib/libasound_module_pcm_alsa_dsp.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/98-maemo.conf
%endif

%files oss
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/README-pcm-oss
%{_libdir}/alsa-lib/libasound_module_ctl_oss.so
%{_libdir}/alsa-lib/libasound_module_pcm_oss.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/50-pcm-oss.conf

%files pulseaudio
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/README-pulse
%{_libdir}/alsa-lib/libasound_module_conf_pulse.so
%{_libdir}/alsa-lib/libasound_module_ctl_pulse.so
%{_libdir}/alsa-lib/libasound_module_pcm_pulse.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/50-pulseaudio.conf
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/99-pulseaudio-default.conf

%files samplerate
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/samplerate.txt
%{_libdir}/alsa-lib/libasound_module_rate_samplerate.so
%{_libdir}/alsa-lib/libasound_module_rate_samplerate_best.so
%{_libdir}/alsa-lib/libasound_module_rate_samplerate_linear.so
%{_libdir}/alsa-lib/libasound_module_rate_samplerate_medium.so
%{_libdir}/alsa-lib/libasound_module_rate_samplerate_order.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/10-samplerate.conf

%files speex
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/speexdsp.txt doc/speexrate.txt
%{_libdir}/alsa-lib/libasound_module_pcm_speex.so
%{_libdir}/alsa-lib/libasound_module_rate_speexrate.so
%{_libdir}/alsa-lib/libasound_module_rate_speexrate_best.so
%{_libdir}/alsa-lib/libasound_module_rate_speexrate_medium.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/10-speex.conf

%files upmix
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/upmix.txt
%{_libdir}/alsa-lib/libasound_module_pcm_upmix.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/50-upmix.conf

%files usbstream
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL
%{_libdir}/alsa-lib/libasound_module_pcm_usb_stream.so

%files vdownmix
%defattr(-,root,root,-)
%doc COPYING COPYING.GPL doc/vdownmix.txt
%{_libdir}/alsa-lib/libasound_module_pcm_vdownmix.so
%config(noreplace) %{_datadir}/alsa/alsa.conf.d/97-vdownmix.conf

%changelog
* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.28-1m)
- update to 1.0.28

* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.27-2m)
- rebuild against ffmpeg

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.27-1m)
- update to 1.0.27

* Thu Sep 27 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.26-1m)
- update to 1.0.26

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.25-1m)
- version 1.0.25

* Sat Jun 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.24-1m)
- version 1.0.24
- update arcamav.conf, pulse-default.conf and speex.conf

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.23-5m)
- rebuild against ffmpeg

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.23-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.23-1m)
- version 1.0.23
- set build_maemo 1
- update speex.conf from Fedora
- import maemo.conf from Fedora

* Thu Dec 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.22-1m)
- update 1.0.22

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.20-2m)
- rebuild against ffmpeg-0.5.1-0.20090622

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.20-1m)
- version 1.0.20
- add libavutil.patch to enable build
- add 2 packages alsa-plugins-arcamav and alsa-plugins-speex
- import arcamav.conf from Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.18-4m)
- rebuild against rpm-4.6

* Sat Dec  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.18-3m)
- remove %%{_datadir}/alsa/pulse-default.conf

* Sat Dec  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.18-2m)
- move configuration files from %%{_syscondir}/alsa/pcm to %%{_datadir}/alsa/pcm

* Fri Dec  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.18-1m)
- version 1.0.18
- add a package usbstream
- clean up patches
- License: GPLv2+ and LGPLv2+

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.17-2m)
- rebuild against ffmpeg

* Mon Aug 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-1m)
- version 1.0.17
- import 2 patches from Fedora for pulseaudio

* Wed Apr 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.16-1m)
- version 1.0.16
- update configuration files
- import hints.patch from Fedora
 +* Tue Mar 25 2008 Lubomir Kundrak <lkundrak@redhat.com> - 1.0.16-4
 +- Kind of fix the plugins not to complain about the hints
- import pulseclose.patch from Fedora
 +* Sat Mar 08 2008 Lubomir Kundrak <lkundrak@redhat.com> - 1.0.16-1
 +- Do not assert fail when pulseaudio is unavailable (#435148)
- clean up old patches

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.15-4m)
- rebuild against gcc43

* Sun Mar 23 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.15-3m)
- add workaround for ffmpeg

* Thu Mar  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.15-2m)
- import pulsehint.patch from Fedora
 +* Tue Mar 04 2008 Lubomir Kundrak <lkundrak@redhat.com> - 1.0.15-4
 +- Be more heplful when there's PulseAudio trouble.
 +- This may save us some bogus bug reports
- import pulse-SND_PCM_STATE_PREPARED.patch from Fedora
 +* Fri Jan 18 2008 Eric Moret <eric.moret@epita.fr> - 1.0.15-2
 +- Fix pulse_hw_params() when state is SND_PCM_STATE_PREPARED (#428030)

* Thu Oct 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.15-1m)
- import from Fedora and modify spec file for Momonga Linux

* Thu Oct 18 2007 Lennart Poettering <lpoetter@redhat.com> - 1.0.14-6
- Merge the whole /etc/alsa/pcm/pulseaudio.conf stuff into
  /etc/alsa/pulse-default.conf, because the former is practically
  always ignored, since it is not referenced for inclusion by any other
  configuration file fragment (#251943)
  The other fragments installed in /etc/alsa/pcm/ are useless, too. But
  since we are in a freeze and they are not that important, I am not fixing
  this now.

* Wed Oct 17 2007 Lennart Poettering <lpoetter@redhat.com> - 1.0.14-5
- Split pulse.conf into two, so that we can load one part from
  form /etc/alsa/alsa.conf. (#251943)

* Mon Oct 1 2007 Lennart Poettering <lpoetter@redhat.com> - 1.0.14-4
- In the pulse plugin: reflect the XRUN state back to the application.
  Makes XMMS work on top of the alsa plugin. (#307341)

* Mon Sep 24 2007 Lennart Poettering <lpoetter@redhat.com> - 1.0.14-3
- Change PulseAudio buffering defaults to more sane values

* Tue Aug 14 2007 Eric Moret <eric.moret@epita.fr> - 1.0.14-2
- Adding pulse as ALSA "default" pcm and ctl when the alsa-plugins-pulseaudio
package is installed, fixing #251943.

* Mon Jul 23 2007 Eric Moret <eric.moret@epita.fr> - 1.0.14-1
- update to upstream 1.0.14
- use configure --without-speex instead of patches to remove a52

* Tue Mar 13 2007 Matej Cepl <mcepl@redhat.com> - 1.0.14-0.3.rc2
- Really remove a52 plugin package (including changes in
  configure and configure.in)

* Thu Feb 15 2007 Eric Moret <eric.moret@epita.fr> 1.0.14-0.2.rc2
- Adding configuration files
- Removing a52 plugin package

* Wed Jan 10 2007 Eric Moret <eric.moret@epita.fr> 1.0.14-0.1.rc2
- Initial package for Fedora
