%global momorel 18
%global artsver 1.5.10
%global artsrel 1m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr

Summary: A KDE FTP Client
Name: kasablanca
Version: 0.4.0.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Internet
URL: http://kasablanca.berlios.de/
Source0: http://download.berlios.de/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: desktop-file-utils
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: libart_lgpl-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: pkgconfig
BuildRequires: zlib-devel

%description
Kasablanca is a KDE ftp client.

Its main features are:

- advanced ftp operations like resumed and recursive transfers
- direct remote to remote transfer (fxp)
- ftps encryption on both control and data connection
- enqeueing of files and directories
- high configurability

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .lcrypto

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category Network \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL TODO
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/config.kcfg/kbconfig.kcfg
%{_datadir}/doc/HTML/en/%{name}
%{_datadir}/icons/*/*/apps/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0.2-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0.2-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0.2-16m)
- full rebuild for mo7 release

* Sun May  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0.2-15m)
- fix build with new gcc

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0.2-14m)
- touch up spec file

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0.2-13m)
- rebuild against openssl-1.0.0

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0.2-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0.2-11m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0.2-10m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0.2-9m)
- rebuild against openssl-0.9.8h-1m

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0.2-8m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0.2-7m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0.2-6m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0.2-5m)
- %%NoSource -> NoSource

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0.2-4m)
- use md5sum_src0

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0.2-3m)
- update desktop.patch (add DocPath)

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0.2-2m)
- remove Application from Categories of desktop file

* Sat Jul 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0.2-1m)
- initial package for Momonga Linux
