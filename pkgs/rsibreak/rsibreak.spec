%global momorel 3
%global qtver 4.8.2
%global kdever 4.9.0
%global kdebaserel 1m
%global kdebaseworkspacerel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
#global beta beta1
#global prever 1

Summary: A small utility which bothers you at certain intervals
Name: rsibreak
Version: 0.11
Release: %{?prever:0.%{prever}.}%{momorel}m%{?dist}
License: GPLv2
URL: http://kde-apps.org/content/show.php?content=29725
Group: Amusements/Graphics
Source0: http://www.rsibreak.org/files/%{name}-%{version}%{?beta:-%{beta}}.tar.bz2
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kde-baseapps >= %{kdever}-%{kdebaserel}
Requires: soprano
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kde-workspace-devel >= %{kdever}-%{kdebaseworkspacerel}
BuildRequires: cmake
BuildRequires: dbus-devel
BuildRequires: gettext
BuildRequires: libXScrnSaver-devel
BuildRequires: soprano-devel

%description
RSIBreak is a small utility which bothers you at certain intervals. The
interval and duration of two different timers can be configured. You can
use the breaks to stretch out or do the dishes. The aim of this utility
is to let you know when it is time to have a break from your computer.
This can help people to prevent Repetive Strain Injury.

%prep
%setup -q -n %{name}-%{version}%{?beta:-%{beta}}

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog HACKING INSTALL NEWS TODO
%{_kde4_bindir}/rsibreak
%{_kde4_libdir}/kde4/plasma_*_rsibreak.so
%{_kde4_datadir}/applications/kde4/rsibreak.desktop
%{_kde4_datadir}/autostart/rsibreak.desktop
%{_kde4_appsdir}/desktoptheme/default/widgets/rsibreak.svg
%{_kde4_appsdir}/rsibreak
%{_datadir}/dbus-1/interfaces/org.rsibreak.rsiwidget.xml
%{_kde4_docdir}/HTML/*/rsibreak
%{_kde4_iconsdir}/*/*/*/rsibreak.png
%{_kde4_iconsdir}/hicolor/*/actions/*.png
%{_kde4_datadir}/kde4/services/plasma-*-rsibreak.desktop
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-3m)
- change primary site URI and add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-2m)
- rebuild for new GCC 4.6

* Sat Jan 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-0.1.2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-0.1.1m)
- version 0.11-beta1

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-5m)
- rebuild against qt-4.6.3-1m

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-4m)
- explicitly link libX11

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-1m)
- version 0.10

* Fri Jul 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-0.2.1m)
- update to 0.10-beta2
- update desktop.patch

* Tue Jul 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-0.1.1m)
- update to version 0.10-beta1
- update desktop.patch
- remove merged kde42.patch
- remove fix-po-cmakelists.patch

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-5m)
- fix up broken po/CMakeLists.txt to enable build with new cmake

* Sat Mar 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-4m)
- fix up Requires

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-3m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-2m)
- fix build on KDE 4.2 RC

* Sat Nov 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-1m)
- version 0.9.0
- update desktop.patch

* Sun Oct 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.20080904.2m)
- rebuild against kdebase-workspace-4.1.71

* Thu Sep  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.20080904.1m)
- update to 20080904 svn snapshot

* Sun Jun  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-0.2.6m)
- source tarball was renamed for KDE 4.1 beta 1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-0.2.5m)
- rebuild against qt-4.4.0-1m
- source tarball was renamed

* Tue Apr  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-0.2.4m)
- update source tar ball

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-0.2.3m)
- rebuild against gcc43

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.2.2m)
- update Source0 (use stable/4.0.2/src/extragear)
- set ftpdirver 4.0.2

* Tue Feb 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.2.1m)
- update to 0.9.0-beta3

* Wed Jan 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-0.1.1m)
- revise version to 0.9.0 beta 2
- License: GPLv2

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-1m)
- version 4.0.0
- update desktop.patch

* Sun Apr  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-1m)
- initial package for Momonga Linux
