%global         momorel 1
%global         srcname ETL

Summary:        Extended Template Library
Name:           etl
Version:        0.04.15
Release:        %{momorel}m%{?dist}
License:        GPLv3
Group:          System Environment/Libraries
URL:            http://synfig.org/
Source0:        http://dl.sourceforge.net/sourceforge/synfig/%{srcname}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
ETL is a multi-platform class and template library designed to add new datatypes and functions
which combine well with the existing types and functions from the C++ Standard Template Library (STL).

%package devel
Summary:        Header files and libraries for a development with %{name}
Group:          Development/Libraries
#Requires:	%{name} = %{version}-%{release}

%description devel
ETL is a multi-platform class and template library designed to add new datatypes and functions which
combine well with the existing types and functions from the C++ Standard Template Library (STL).

This package contains header files and libraries for a development with %{name}.


%prep
%setup -q -n %{srcname}-%{version}

%build
%configure
%make

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install transform='s,x,x,'

# remove libtool la
rm -rf %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%files devel
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/ETL-config
%{_includedir}/ETL
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04.15-1m)
- update to 0.04.15

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.04.13-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.04.13-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.04.13-2m)
- full rebuild for mo7 release

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04.13-1m)
- update to 0.04.13

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.04.12-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.04.12-1m)
- update 0.04.12
-- need synfig

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.04.11-2m)
- rebuild against rpm-4.6

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.04.11-1m)
- import to Momonga for synfig
