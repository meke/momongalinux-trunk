%global momorel 1

%global build_tiny_server 0

%global pkgname xorg-server

Summary:   X.Org X11 X server
Name:      xorg-x11-server
Version:   1.13.4
Release:   %{momorel}m%{?dist}
URL:       http://www.x.org/
License:   MIT/X
Group:     User Interface/X
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: http://xorg.freedesktop.org/releases/individual/xserver/%{pkgname}-%{version}.tar.bz2 
NoSource: 0

Source4:   10-quirks.conf

Source10:   xserver.pamd

# "useful" xvfb-run script
Source20:  http://svn.exactcode.de/t2/trunk/package/xorg/xorg-server/xvfb-run.sh

# Trivial things to never merge upstream ever:
# This really could be done prettier.
Patch5002: xserver-1.4.99-ssh-isnt-local.patch

# don't build the (broken) acpi code
Patch6011: xserver-1.6.0-less-acpi-brokenness.patch

# ajax needs to upstream this
Patch6030: xserver-1.6.99-right-of.patch
#Patch6044: xserver-1.6.99-hush-prerelease-warning.patch

# Fix libselinux-triggered build error
# RedHat/Fedora-specific patch
Patch7013: xserver-1.12-Xext-fix-selinux-build-failure.patch

# needed when building without xorg (aka s390x)
Patch7017: xserver-1.12.2-xorg-touch-test.patch

Patch7022: 0001-linux-Refactor-xf86-En-Dis-ableIO.patch
Patch7023: 0002-linux-Make-failure-to-iopl-non-fatal.patch
Patch7024: 0003-xfree86-Change-the-semantics-of-driverFunc-GET_REQUI.patch
Patch7025: 0001-Always-install-vbe-and-int10-sdk-headers.patch

# do not upstream - do not even use here yet
Patch7027: xserver-autobind-hotplug.patch

Patch7052: 0001-xf86-return-NULL-for-compat-output-if-no-outputs.patch

# kernel doesn't use _INPUT_H anymore
Patch7060:  0001-xf86-Fix-build-against-recent-Linux-kernel.patch

# Fix non-PCI configuration-less setups
Patch7061:  v2-xf86-Fix-non-PCI-configuration-less-setups.patch

# Bug 878956 - After installation is complete, Alt+F4 is broken
Patch7063: 0001-linux-Prefer-ioctl-KDSKBMUTE-1-over-ioctl-KDSKBMODE-.patch

# mustard: make the default queue length bigger to calm abrt down
Patch7064: 0001-mieq-Bump-default-queue-size-to-512.patch

%global moduledir	%{_libdir}/xorg/modules
%global drimoduledir    %{_libdir}/dri
%global sdkdir		%{_includedir}/xorg

%ifarch s390 s390x
%global with_hw_servers 0
%else
%global with_hw_servers 1
%endif

%if %{with_hw_servers}
%global enable_xorg --enable-xorg
%else
%global enable_xorg --disable-xorg
%endif

BuildRequires: pkgconfig
BuildRequires: libpciaccess-devel >= 0.12.902
BuildRequires: xorg-x11-font-utils
BuildRequires: xorg-x11-util-macros >= 1.14.0
BuildRequires: xorg-x11-proto-devel >= 7.8
BuildRequires: xorg-x11-xtrans-devel
# FIXME: The version specification can be removed from here in the future,
# as it is not really mandatory, but forces a bugfix workaround on people who
# are using pre-rawhide modular X.
BuildRequires: libXfont-devel >= 0.99.2-3
BuildRequires: libXau-devel
BuildRequires: libxkbfile-devel
# libdmx-devel needed for Xdmx
BuildRequires: libdmx-devel
# libXdmcp-devel needed for Xdmx
BuildRequires: libXdmcp-devel
# libXmu-devel needed for Xdmx
BuildRequires: libXmu-devel
# libXext-devel needed for Xdmx
BuildRequires: libXext-devel
# libX11-devel needed for Xdmx
BuildRequires: libX11-devel
# libXrender-devel needed for Xdmx
BuildRequires: libXrender-devel
# libXi-devel needed for Xdmx
BuildRequires: libXi-devel
# libXres-devel needed for something that links to libXres that I never bothered to figure out yet
BuildRequires: libXres-devel
# libfontenc-devel needed for Xorg, but not specified by
# upstream deps.  Build fails without it.
BuildRequires: libfontenc-devel
# Required for Xtst examples
BuildRequires: libXtst-devel
# For Xdmxconfig 
BuildRequires: libXt-devel libXpm-devel libXaw-devel
BuildRequires: libXv-devel 
# To query fontdir from fontutil.pc
BuildRequires: xorg-x11-font-utils >= 1.0.0-1
# Needed at least for DRI enabled builds
BuildRequires: mesa-libGL-devel >= 8.1.0-0.20120707.2m
BuildRequires: libdrm-devel >= 2.3.0-1
BuildRequires: pixman-devel >= 0.21.8
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: audit-libs-devel >= 2.0.4
BuildRequires: systemd-devel >= 187

%global kdrive --enable-kdrive --enable-xephyr --disable-xsdl --disable-xfake --disable-xfbdev --disable-kdrive-vesa
%global xservers --enable-xvfb --enable-xnest %{kdrive} %{enable_xorg} --enable-dmx

# Removed old driver
# use evdev
Obsoletes: xorg-x11-drv-acecad <= 1.5.0
Obsoletes: xorg-x11-drv-aiptek <= 1.4.1
Obsoletes: xorg-x11-drv-elographics <= 1.3.0
Obsoletes: xorg-x11-drv-fpit <= 1.4.0
Obsoletes: xorg-x11-drv-hyperpen <= 1.4.1
Obsoletes: xorg-x11-drv-mutouch <= 1.3.0
Obsoletes: xorg-x11-drv-penmount <= 1.5.0

Obsoletes: xorg-x11-drv-ark <= 0.7.4
Obsoletes: xorg-x11-drv-chips <= 1.2.4
Obsoletes: xorg-x11-drv-s3 <= 0.6.3
Obsoletes: xorg-x11-drv-tseng <= 1.2.4

%description
X.Org X11 X server

# ----- Xorg --------------------------------------------------------
%package common
Summary: Xorg server common files
Group: User Interface/X

%description common
Common files shared among all X servers.

%if %{with_hw_servers}
%package Xorg
Summary: Xorg X server
Group: User Interface/X
Requires: system-setup-keyboard
Requires: udev >= 148
# NOTE: The X server invokes xkbcomp directly, so this is required.
Requires: xorg-x11-xkb-utils
# NOTE: The X server requires 'fixed' and 'cursor' font, which are provided
# by xorg-x11-fonts-base
Requires: xorg-x11-fonts-base
# NOTE: Require some basic drivers for minimal configuration. (#173060)
Requires: xorg-x11-drv-mouse xorg-x11-drv-keyboard xorg-x11-drv-vesa

Requires: xorg-x11-drv-void xorg-x11-drv-evdev
# virtuals.  XXX fix the xkbcomp fork() upstream.
Requires: xkeyboard-config
Requires: xorg-x11-server-common >= %{version}-%{release}
# These drivers were dropped in F7 for being broken, so uninstall them.
Obsoletes: xorg-x11-drv-elo2300 <= 1.1.0
Obsoletes: xorg-x11-drv-joystick <= 1.1.0
# Dropped from Mo6 for being broken, uninstall it.
# Obsoletes: xorg-x11-drv-magictouch <= 1.0.0.5
# Force sufficiently new libpciaccess
Conflicts: libpciaccess < 0.10.3

%description Xorg
X.org X11 is an open source implementation of the X Window System.  It
provides the basic low level functionality which full fledged
graphical user interfaces (GUIs) such as GNOME and KDE are designed
upon.

%endif

# ----- Xnest -------------------------------------------------------

%package Xnest
Summary: A nested server.
Group: User Interface/X
#Requires: %{name} = %{version}-%{release}
Obsoletes: XFree86-Xnest, xorg-x11-Xnest
# NOTE: This virtual provide should be used by packages which want to depend
# on an implementation nonspecific Xnest X server.  It is intentionally not
# versioned, since it should be agnostic.

%description Xnest
Xnest is an X server, which has been implemented as an ordinary
X application.  It runs in a window just like other X applications,
but it is an X server itself in which you can run other software.  It
is a very useful tool for developers who wish to test their
applications without running them on their real X server.

# ----- Xdmx --------------------------------------------------------

%package Xdmx
Summary: Distributed Multihead X Server and utilities
Group: User Interface/X
#Requires: %{name}-Xorg = %{version}-%{release}
Obsoletes: xorg-x11-Xdmx
# NOTE: This virtual provide should be used by packages which want to depend
# on an implementation nonspecific Xdmx X server.  It is intentionally not
# versioned, since it should be agnostic.

%description Xdmx
Xdmx is proxy X server that provides multi-head support for multiple displays
attached to different machines (each of which is running a typical X server).
When Xinerama is used with Xdmx, the multiple displays on multiple machines
are presented to the user as a single unified screen.  A simple application
for Xdmx would be to provide multi-head support using two desktop machines,
each of which has a single display device attached to it.  A complex
application for Xdmx would be to unify a 4 by 4 grid of 1280x1024 displays
(each attached to one of 16 computers) into a unified 5120x4096 display.

# ----- Xvfb --------------------------------------------------------

%package Xvfb
Summary: A X Windows System virtual framebuffer X server.
Group: User Interface/X
# xvfb-run is GPLv2, rest is MIT
License: MIT and GPLv2
Obsoletes: XFree86-Xvfb xorg-x11-Xvfb
# NOTE: This virtual provide should be used by packages which want to depend
# on an implementation nonspecific Xvfb X server.  It is intentionally not
# versioned, since it should be agnostic.

%description Xvfb
Xvfb (X Virtual Frame Buffer) is an X server that is able to run on
machines with no display hardware and no physical input devices.
Xvfb simulates a dumb framebuffer using virtual memory.  Xvfb does
not open any devices, but behaves otherwise as an X display.  Xvfb
is normally used for testing servers.

# ----- Xephyr -------------------------------------------------------

%package Xephyr
Summary: A nested server.
Group: User Interface/X
#Requires: %{name} = %{version}-%{release}
# NOTE: This virtual provide should be used by packages which want to depend
# on an implementation nonspecific Xephyr X server.  It is intentionally not
# versioned, since it should be agnostic.
Provides: Xephyr

%description Xephyr
Xephyr is an X server, which has been implemented as an ordinary
X application.  It runs in a window just like other X applications,
but it is an X server itself in which you can run other software.  It
is a very useful tool for developers who wish to test their
applications without running them on their real X server.  Unlike
Xnest, Xephyr renders to an X image rather than relaying the
X protocol, and therefore supports the newer X extensions like
Render and Composite.

# ----- sdk ---------------------------------------------------------

%if %{with_hw_servers}
%package devel
Summary: SDK for X server driver module development
Group: User Interface/X
Obsoletes: XFree86-sdk xorg-x11-sdk
Obsoletes: xorg-x11-server-sdk
Provides: xorg-x11-server-sdk
Requires: xorg-x11-util-macros >= 1.2.1
Requires: pixman-devel
Requires(pre): xorg-x11-filesystem >= 0.99.2-3


%description devel
The SDK package provides the developmental files which are necessary for
developing X server driver modules, and for compiling driver modules
outside of the standard X11 source code tree.  Developers writing video
drivers, input drivers, or other X modules should install this package.

%endif

%package source
Summary: Xserver source code required to build VNC server (Xvnc)
Group: Development/Libraries
 
%description source
Xserver source code needed to build VNC server (Xvnc)

# -------------------------------------------------------------------

%prep
%setup -q -n %{pkgname}-%{version}

%patch5002 -p1
%patch6011 -p1
%patch6030 -p1
#Patch6044 -p1
%patch7013 -p1
%patch7017 -p1
%patch7022 -p1
%patch7023 -p1
%patch7024 -p1
%patch7025 -p1
%patch7027 -p1
%patch7052 -p1
%patch7060 -p1
%patch7061 -p1
%patch7063 -p1
%patch7064 -p1

%build
%global default_font_path "catalogue:/etc/X11/fontpath.d,built-ins"

# --with-rgb-path should be superfluous now ?
# --with-pie ?
autoreconf -v --install || exit 1
export CFLAGS="${RPM_OPT_FLAGS} -fno-omit-frame-pointer"
%configure --enable-maintainer-mode %{xservers} \
	--disable-static \
	--with-pic \
	--with-int10=x86emu \
	--with-default-font-path=%{default_font_path} \
	--with-module-dir=%{moduledir} \
	--with-xkb-output=%{_localstatedir}/lib/xkb \
	--with-os-name="Momonga Linux 8" \
	--with-os-vendor="Momonga Project" \
	--with-dri-driver-path=%{drimoduledir} \
	--without-dtrace \
	--enable-install-libxf86config \
	--enable-xselinux --enable-record \
	--enable-config-udev \
	${CONFIGURE}
%make

%install

rm -rf %{buildroot}
make install DESTDIR=%{buildroot} moduledir=%{moduledir}

%if %{with_hw_servers}
rm -f %{buildroot}%{_libdir}/xorg/modules/libxf8_16bpp.so
mkdir -p %{buildroot}%{_libdir}/xorg/modules/{drivers,input}

mkdir -p %{buildroot}%{_datadir}/xorg
install -m 0444 hw/xfree86/common/{vesa,extra}modes %{buildroot}%{_datadir}/xorg/

mkdir -p %{buildroot}%{_sysconfdir}/pam.d
install -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/pam.d/xserver

mkdir -p %{buildroot}%{_datadir}/X11/xorg.conf.d
install -m 644 %{SOURCE4} %{buildroot}%{_datadir}/X11/xorg.conf.d

# make sure the (empty) /etc/X11/xorg.conf.d is there, system-setup-keyboard
# relies on it more or less.
mkdir -p %{buildroot}%{_sysconfdir}/X11/xorg.conf.d

#mkdir -p %{buildroot}%{_bindir}
#install -m 755 %{SOURCE30} %{buildroot}%{_bindir}

%endif

# Make the source package
%global xserver_source_dir %{_datadir}/xorg-x11-server-source
%global inst_srcdir %{buildroot}/%{xserver_source_dir}
mkdir -p %{inst_srcdir}/{doc/xml{,/dtrace},Xext,xkb,GL,hw/{xquartz/bundle,xfree86/common}}
mkdir -p %{inst_srcdir}/{hw/dmx/doc,doc/man}
#cp {,%{inst_srcdir}/}doc/xml/xmlrules.in
#cp {,%{inst_srcdir}/}doc/xml/xmlrules-noinst.in
#cp {,%{inst_srcdir}/}doc/xml/xmlrules-inst.in
#cp {,%{inst_srcdir}/}doc/xml/xserver.ent.in
#cp {,%{inst_srcdir}/}doc/xml/Xserver-spec.xml
#cp {,%{inst_srcdir}/}doc/xml/dtrace/Xserver-DTrace.xml
cp {,%{inst_srcdir}/}hw/xquartz/bundle/cpprules.in
#cp {,%{inst_srcdir}/}doc/man/Xserver.man
cp {,%{inst_srcdir}/}doc/smartsched
#cp {,%{inst_srcdir}/}hw/dmx/doc/doxygen.conf.in
cp xkb/README.compiled %{inst_srcdir}/xkb
cp hw/xfree86/xorgconf.cpp %{inst_srcdir}/hw/xfree86
cp hw/xfree86/common/{vesamodes,extramodes} %{inst_srcdir}/hw/xfree86/common

install -m 0755 %{SOURCE20} %{buildroot}%{_bindir}/xvfb-run

find . -type f | egrep '.*\.(c|h|am|ac|inc|m4|h.in|pc.in|man.pre|pl|txt)$' |
xargs tar cf - | (cd %{inst_srcdir} && tar xf -)
# SLEDGEHAMMER
find %{inst_srcdir}/hw/xfree86 -name \*.c -delete

# Remove unwanted files/dirs
{
    rm -f %{buildroot}%{_libdir}/X11/Options
    rm -f %{buildroot}%{_bindir}/in?
    rm -f %{buildroot}%{_bindir}/ioport
    rm -f %{buildroot}%{_bindir}/out?
    rm -f %{buildroot}%{_bindir}/pcitweak
    rm -f %{buildroot}%{_mandir}/man1/pcitweak.1*
    find %{buildroot} -type f -name '*.la' | xargs rm -f -- || :
%if !%{with_hw_servers}
    rm -f %{buildroot}%{_libdir}/pkgconfig/xorg-server.pc
    rm -f %{buildroot}%{_datadir}/aclocal/xorg-server.m4

%endif
}

%clean
rm -rf --preserve-root %{buildroot}

%files common
%defattr(-,root,root,-)
%{_mandir}/man1/Xserver.1*
%{_libdir}/xorg/protocol.txt
%dir %{_localstatedir}/lib/xkb
%{_localstatedir}/lib/xkb/README.compiled

%if %{with_hw_servers}

%files Xorg
%defattr(-,root,root,-)
%config %attr(0644,root,root) %{_sysconfdir}/pam.d/xserver
%{_bindir}/X
%attr(4711, root, root) %{_bindir}/Xorg
%{_bindir}/cvt
%{_bindir}/gtf
%dir %{_datadir}/xorg
%{_datadir}/xorg/vesamodes
%{_datadir}/xorg/extramodes
%dir %{_libdir}/xorg
%dir %{_libdir}/xorg/modules
%dir %{_libdir}/xorg/modules/drivers
%dir %{_libdir}/xorg/modules/extensions
%{_libdir}/xorg/modules/extensions/libglx.so
%dir %{_libdir}/xorg/modules/input
%{_libdir}/xorg/modules/libfbdevhw.so
%dir %{_libdir}/xorg/modules/multimedia
%{_libdir}/xorg/modules/multimedia/bt829_drv.so
%{_libdir}/xorg/modules/multimedia/fi1236_drv.so
%{_libdir}/xorg/modules/multimedia/msp3430_drv.so
%{_libdir}/xorg/modules/multimedia/tda8425_drv.so
%{_libdir}/xorg/modules/multimedia/tda9850_drv.so
%{_libdir}/xorg/modules/multimedia/tda9885_drv.so
%{_libdir}/xorg/modules/multimedia/uda1380_drv.so
%{_libdir}/xorg/modules/libexa.so
%{_libdir}/xorg/modules/libfb.so
%{_libdir}/xorg/modules/libint10.so
%{_libdir}/xorg/modules/libshadow.so
%{_libdir}/xorg/modules/libshadowfb.so
%{_libdir}/xorg/modules/libvbe.so
%{_libdir}/xorg/modules/libvgahw.so
%{_libdir}/xorg/modules/libwfb.so
%{_mandir}/man1/gtf.1*
%{_mandir}/man1/Xorg.1*
%{_mandir}/man1/cvt.1*
%{_mandir}/man4/fbdevhw.4*
%{_mandir}/man4/exa.4*
%{_mandir}/man5/xorg.conf.5*
%{_mandir}/man5/xorg.conf.d.5.*
%dir %{_sysconfdir}/X11/xorg.conf.d
%dir %{_datadir}/X11/xorg.conf.d
%{_datadir}/X11/xorg.conf.d/10-evdev.conf
%{_datadir}/X11/xorg.conf.d/10-quirks.conf
%endif

%files Xnest
%defattr(-,root,root,-)
%{_bindir}/Xnest
%{_mandir}/man1/Xnest.1*

%files Xdmx
%defattr(-,root,root,-)
%{_bindir}/Xdmx
%{_bindir}/dmxaddinput
%{_bindir}/dmxaddscreen
%{_bindir}/dmxinfo
%{_bindir}/dmxreconfig
%{_bindir}/dmxresize
%{_bindir}/dmxrminput
%{_bindir}/dmxrmscreen
%{_bindir}/dmxtodmx
%{_bindir}/dmxwininfo
%{_bindir}/vdltodmx
#%{_bindir}/xdmx
%{_bindir}/xdmxconfig
%{_mandir}/man1/Xdmx.1*
%{_mandir}/man1/dmxtodmx.1*
%{_mandir}/man1/vdltodmx.1*
%{_mandir}/man1/xdmxconfig.1*

%files Xvfb
%defattr(-,root,root,-)
%{_bindir}/Xvfb
%{_bindir}/xvfb-run
%{_mandir}/man1/Xvfb.1*

%files Xephyr
%defattr(-,root,root,-)
%{_bindir}/Xephyr
%{_mandir}/man1/Xephyr.1*

%if %{with_hw_servers}
%files devel
%defattr(-,root,root,-)
%{_libdir}/libxf86config.a
%{_libdir}/pkgconfig/xorg-server.pc
%dir %{_includedir}/xorg
%{sdkdir}/*.h
%{_datadir}/aclocal/xorg-server.m4
%endif

%files source
%defattr(-, root, root, -)
%{xserver_source_dir}

%changelog
* Wed May  1 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.4-1m)
- update 1.13.4

* Sun Mar 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.3-1m)
- update 1.13.3

* Fri Jan 25 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.2-1m)
- update 1.13.2

* Sat Dec 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.1-1m)
- update 1.13.1

* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.0.902-1m)
- update 1.13.0.902

* Mon Sep 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.0-2m)
- OpenGL redraw fix

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.13.0-1m)
- update 1.13.0

* Sat Aug  4 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.12.3-3m)
- rebuild against systemd-187

* Sun Jul 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.12.3-2m)
- change Requires from xkbdata to xkeyboard-config

* Mon Jul  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.3-1m)
- update 1.12.3

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.2.901-1m)
- update 1.12.2.901 (1.12.3-RC1)
- fixed any crash bug 

* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.2-1m)
- update 1.12.2
- change OS Name "Momonga Linux 8"

* Sat Apr 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.1-1m)
- update 1.12.1

* Wed Apr 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.0.902-1m)
- update 1.12.0.902

* Thu Mar  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.0-1m)
- update 1.12.0

* Sun Feb 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.99.903-1m)
- update 1.11.99.903(1.12-RC3)

* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.99.902-1m)
- update 1.11.99.902(1.12-RC2)

* Sat Jan 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.99.901-4m)
- update HEAD patch

* Thu Jan 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.99.901-3m)
- update HEAD patch

* Thu Jan 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.99.901-2m)
- add HEAD patch
-- fixed any leak

* Fri Jan  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.99.901-1m)
- update 1.11.99.901

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.3-2m)
- enable to build on x86_64

* Mon Dec 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.3-1m)
- update 1.11.3

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.2-1m)
- update 1.11.2

* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.1.902-1m)
- update 1.11.1.902

* Sun Oct 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.1-5m)
- delete a workaround patch
-- see BTS #387 for details

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.11.1-4m)
- [SECURITY] CVE-2011-4028 CVE-2011-4029 

* Mon Oct 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.1-3m)
- fix momorel
- add a workaround patch for Momonga Linux BTS #387

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.1-2m)
- add patches

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.1-1m)
- update 1.11.1

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.11.0-1m)
- update 1.11.0

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.99.902-1m)
- update 1.10.99.902

* Fri Jul 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.99.902-0.1m)
- update 1.10.99.902-snapshot

* Sun Jul 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.99.901-4m)
- add xorg-server-HEAD.patch

* Sun Jun 26 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (1.10.99.901-3m)
- revise BR version

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.99.901-1m)
- update 1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.5-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.5-1m)
- update to 1.9.5

* Sat Feb  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.4-1m)
- update to 1.9.4

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.3-1m)
- update to 1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.2-3m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2-2m)
- import fedora patches

* Fri Jul  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2-1m)
- update 1.8.2

* Mon Jun 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-4m)
- add BuildRequires

* Sun May 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-3m)
- add BuildRequires

* Sun May 30 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.1-2m)
- Requires: system-setup-keyboard udev

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.1-1m)
- update 1.8.1

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0-5m)
- import fedora patches

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0-4m)
- import fedora patches

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-3m)
- rebuild against openssl-1.0.0

* Sun Apr  4 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.0-2m)
- fix BR: xorg-x11-proto-devel >= 7.5-10
- fix BR: mesa-libGL-devel >= 7.8

* Fri Apr  2 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.0-1m)
- update 1.8.0

* Sun Feb 28 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.5-1m)
- update 1.7.5

* Fri Jan  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4-1m)
- update 1.7.4

* Tue Dec 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3-2m)
- rebuild against audit-2.0.4

* Thu Dec  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3-1m)
- update 1.7.3

* Tue Dec  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-1m)
- update 1.7.2

* Thu Nov 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.1-3m)
- revised BR pixman-devel version

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.1-1m)
- update to 1.7.1

* Tue Oct 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.5-2m)
- remove BuildRequires: liblbxutil-devel

* Mon Oct 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.5-1m)
- update 1.6.5

* Fri Oct  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-4m)
- update Xorg-7.5 patch

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-3m)
- support Xorg-7.5

* Wed Sep 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-2m)
- fix Xserver crash
-- add xorg-x11-server-1.6.4-DCA.patch

* Tue Sep 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.4-1m)
- update 1.6.4

* Mon Aug  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3-1m)
- update 1.6.3

* Sat Jul 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2-3m)
- drop xserver-1.6.1-nouveau.patch to avoid miss detection of nvidia driver

* Thu Jul  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2-2m)
- import patch from fedora

* Thu Jul  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2-1m)
- update 1.6.2

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-2m)
- now --without-dtrace
- apply automake111 patch

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-1m)
- update 1.6.1

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-3m)
- import fedora patches(1.6.0-19)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-2m)
- rebuild against openssl-0.9.8k

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update 1.6.0

* Wed Feb 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-5m)
- apply backport fix for new GNOME to avoid 100% CPU issue

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-4m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-3m)
- update Patch5009 for fuzz=0

* Wed Jan 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.3-2m)
- add fedora patches

* Tue Jan 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.3-1m)
- update 1.5.3

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-1m)
- update 1.5.2

* Thu Sep 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update 1.5.1
- import Fedora Patch
-* Thu Sep 25 2008 Dave Airlie <airlied redhat com> 1.5.1-2
-- fix crash with x11perf on r500 modesetting

* Fri Sep 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-2m)
- add EXA master-upgrade patch

* Thu Sep  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update 1.5.0

* Tue Sep  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.99.906-6m)
- remove enable-selinux.patch for rhgb and firstboot on STABLE_5

* Fri Aug 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.906-5m)
- disable dri2 module
- import any fedora patch

* Wed Aug  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.906-4m)
- update 1.5-upstream patch

* Tue Aug  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.906-3m)
- add 10-x11-keymap.fdi, fedora-setup-keyboard
- evdev keyboard map fix patches

* Mon Aug  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.906-2m)
- update 1.5-branch upstream patch

* Fri Jul 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.906-1m)
- update 1.5RC6(1.4.99.906)

* Sun Jul 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.905-3m)
- update upstream patch.

* Thu Jul 10 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.99.905-2m)
- update upstream patch.

* Thu Jul  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.99.905-1m)
- update 1.4.99.905
- update upstream patch. 

* Sat Jun 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.99.902-6m)
- update upstream patch. fix many bug(include 2 memory leak)
- remove BPR mesa-source

* Wed Jun 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.99.902-5m)
- update upstream patch. fix many bugfix 

* Sat Jun 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.99.902-4m)
- add upstream patch. include many bugfix and CVE patches
-- CVE-2008-1377
-- CVE-2008-1379
-- CVE-2008-2360
-- CVE-2008-2361
-- CVE-2008-2362

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.99.902-3m)
- rebuild against openssl-0.9.8h

* Thu Jun  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.99.902-2m)
- support mesa-7.1rc1

* Fri May 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.99.902-1m)
- update 1.4.99.902

* Mon May  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.99.901-1.20080503.2m)
- Obsoletes and Provides: xorg-x11-server-sdk

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.99.901-1.20080503.1m)
- update 1.5.0-20080503
- need rhgb

* Tue Apr 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0.90-8m)
- fix requires (xorg-x11-font-util-devel -> xorg-x11-font-utils)

* Sun Apr  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0.90-7m)
- add requires

* Fri Apr  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0.90-6m)
- fix BuildRequires

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.0.90-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0.90-4m)
- %%NoSource -> NoSource

* Sun Feb  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0.90-3m)
- add BuildReq xorg-x11-font-util-devel
- remove unused file

* Thu Jan  3 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0.90-2m)
- add patch to fix X freezing bug
-- see http://bugs.freedesktop.org/show_bug.cgi?id=13511

* Thu Dec 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0.90-1m)
- update 1.4.0.90

* Sat Sep 15 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-5m)
- revised required version "xorg-x11-proto-devel >= 7.3"

* Thu Sep 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-4m)
- enable dbus-support
- add xorg-xserver-server-dbus.patch, xorg-x11-server-1.4.0-xephyr-only.patch

* Wed Sep 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-3m)
- enable any patches
- cleanup spec

* Wed Sep 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-2m)
- enable HAL support

* Mon Sep 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-1m)
- update 1.4

* Sun Jul  8 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0.0-4m)
- use xkeyboard-config

* Sun Jul  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0.0-3m)
- import any patch from Fedora

* Sun Apr 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0.0-2m)
- rebuild against mesa-6.5.3

* Mon Apr 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0.0-1m)
- update 1.3.0.0
- Require mesa-sources-6.5.3-0.2

* Sat Apr  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.99.905-1m)
- update 1.2.99.905(1.3-rc5)

* Tue Apr  3 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildRequires: libfontenc-devel

* Tue Mar 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.99.903-1m)
- update 1.2.99.903(1.3-rc3)

* Wed Feb 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.99.902-1m)
- update 1.2.99.902(1.3-rc2)

* Wed Feb 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- support mesa-6.5.3
- import Fedora patches

* Tue Jan 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0

* Wed Jan 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.99-903-6m)
- SECURITY FIX
- CVE IDs: CVE-2006-6101 CVE-2006-6102 CVE-2006-6103
-- add patch9

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.99.903-5m)
- revised requires version "xorg-x11-proto-devel >= 7.2"

* Thu Dec 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.99.903-4m)
- revised requires version "xorg-x11-proto-devel >= 7.1"
- xorg-x11-server Requested 'kbproto >= 1.0.3' 

* Wed Dec 27 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.1.99.903-3m)
- enable build with DRI option for ppc64

* Tue Dec 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.99.903-2m)
- broken AIGLX 
-- add xorg-server-1.1.99.901-GetDrawableAttributes.patch

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.99.903-1m)
- update 1.1.99.903

* Mon Sep 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-6m)
- add any patch
-- xorg-x11-server-1.1.1-aiglx-happy-vt-switch.patch
-- xorg-x11-server-1.1.1-aiglx-locking.patch
-- xorg-x11-server-1.1.1-mode-sort-kung-fu.patch
-- xorg-x11-server-1.1.1-pci-paranoia.patch
- require Mesa-6.5.1

* Mon Sep 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-5m)
- delete Patch105:  xorg-x11-server-1.1.1-enable-composite.patch
  for avoid Mozilla crash bug

* Mon Aug 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-4m)
- support mesa-6.5.1
- sync fc-devel

* Tue Jul 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-3m)
- remove composite enable patch.
-- instability mozilla

* Mon Jul 17 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-2m)
- OpenGL compositing manager feature/optimization patches.
-- Patch2000:  xorg-x11-server-1.1.0-no-move-damage.patch
-- Patch2001:  xorg-x11-server-1.1.0-dont-backfill-bg-none.patch
-- Patch2002:  xorg-x11-server-1.1.0-gl-include-inferiors.patch
-- Patch2003:  xorg-x11-server-1.1.0-tfp-damage.patch
-- Patch2004:  xorg-x11-server-1.1.0-mesa-copy-sub-buffer.patch
-- Patch2005:  xorg-x11-server-1.1.1-enable-composite.patch

* Sun Jul  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1
- --enable-glx-tls

* Fri Jun 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-4m)
- add AIGLX patch
-- Patch2000: xorg-x11-server-1.1.0-gl-include-inferiors.patch
-- Patch2001: xorg-x11-server-1.1.0-convolution-filter-fix.patch
-- Patch2002: xorg-x11-server-1.1.0-tfp-damage.patch

* Tue Jun 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-3m)
- Security advisory http://xorg.freedesktop.org/releases/X11R7.1

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- delete duplicated dir

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Sat May 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.99.903-1m)
- update 1.0.99.903

* Thu May  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.99.902-2m)
- add patch9 from http://xorg.freedesktop.org/releases/X11R7.0/patches/
-- CVE-2006-1526

* Sun Apr 30 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.99.902-1m)
- update 1.0.99.902(Xorg-7.1RC2)

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.99.901-1m)
- update 1.0.99.901(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- To trunk

* Tue Mar 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-0.1m)
- version up
-- Security Fix: CVE-2006-0745

* Wed Mar  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-7.1m)
- commentout obsolete

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-7m)
- import to Momonga

* Wed Feb 22 2006 Jeremy Katz <katzj@redhat.com> - 1.0.1-7
- install randrstr.h as part of sdk as required for building some drivers

* Tue Feb 21 2006 Mike A. Harris <mharris@redhat.com>
- Added xorg-server-1.0.1-backtrace.patch which enables the Xorg server's
  built in backtrace support by default, as it was inadvertently disabled in
  7.0.
#'

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> 1.0.1-6.1
- bump again for double-long bug on ppc(64)

* Wed Feb  8 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-6
- Added xorg-x11-server-1.0.1-Red-Hat-extramodes.patch which is a merger of
  XFree86-4.2.99.2-redhat-custom-modelines.patch and
  xorg-x11-6.8.2-laptop-modes.patch from FC4 for (#180301)
- Install a copy of the vesamodes and extramodes files which contain the list
  of video modes that are built into the X server, so that the "rhpxl" package
  does not have to carry around an out of sync copy for itself. (#180301)

* Tue Feb  7 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-5
- Updated "BuildRequires: mesa-source >= 6.4.2-2" to get fix for (#176976)

* Mon Feb  6 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-4
- Fix brown paper bag error introduced in rpm post script in 1.0.1-4.

* Mon Feb  6 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-3
- Added xorg-x11-server-1.0.1-composite-fastpath-fdo4320.patch with changes
  suggested by ajax to fix (fdo#4320).
- Cosmetic cleanups to satiate the banshees.

* Sun Feb  5 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-2
- Added xorg-x11-server-1.0.1-fbpict-fix-rounding.patch from CVS HEAD.
- Added xorg-x11-server-1.0.1-SEGV-on-null-interface.patch which prevents a
  SEGV on null interfaces (#174279,178986)

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated to xserver 1.0.1 from X11R7.0

* Thu Dec 22 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-3
- Added "Provides: libxf86config-devel = %{version}-%{release}" to sdk package.

* Wed Dec 21 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-2
- Added xserver-1.0.0-parser-add-missing-headers-to-sdk.patch to provide the
  necessary libxf86config.a headers to be able to use the library. (#173084)

* Sat Dec 17 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated to xserver 1.0.0 from X11R7 RC4
- Removed the following patches, which are now integrated upstream:
  - xorg-server-0.99.3-rgb.txt-dix-config-fix.patch,
  - xorg-server-0.99.3-fbmmx-fix-for-non-SSE-cpu.patch
- Changed manNx directories to manN to match upstream defaults.
- Added libxf86config.a to sdk subpackage.
- Updated build dependency of "mesa-libGL-devel >= 6.4.1-1"
- Added "BuildRequires: xorg-x11-font-utils >= 1.0.0-1" to be able to query
  the fontdir from fontutil.pc which is implemented currently by a custom
  patch.
- Enable xtrap, xcsecurity, xevie, and lbx on all builds, not just DRI builds.
- Fix sdk installation path, so that drivers can find the files again.
- Update file manifest, to deal with X server modules that have moved to
  a subdir, etc.

* Mon Nov 28 2005 Kristian Hogsberg <krh@redhat.com>
- Add a few missing BuildRequires.

* Fri Nov 25 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-9
- Added "Requires: xorg-x11-drivers >= 0.99.2-4" as a dependency of the Xorg
  subpackage, to ensure that anaconda installs all of the drivers during OS
  installs and upgrades, as requested by Jeremy Katz.

* Fri Nov 25 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-8
- Added xorg-server-0.99.3-rgb.txt-dix-config-fix.patch which fixes the
  --with-rgb-path option to actually *work*.
- Updated libdrm dep to 1.0.5

* Wed Nov 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-7
- Update xorg-x11-server-utils dep to 0.99.2-5 to ensure rgb.txt is installed
  in correct location - _datadir/X11/rgb
- Added --with-rgb-path configure option to specify _datadir/X11/rgb so the
  X server finds the rgb.txt database properly, for bugs (#173453, 173435,
  173428, 173483, 173734, 173737, 173594)
- Added xorg-server-0.99.3-fbmmx-fix-for-non-SSE-cpu.patch to prevent SSE/MMX
  code from being activated on non-capable VIA CPU. (#173384,fdo#5093)

* Thu Nov 17 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-6
- Add the missing rpm pre script from monolithic xorg-x11 packaging,
  clean it up a bit, reorder it for slight performance gain.
- Add some perl magic to pre script to remove RgbPath from xorg.conf,
  in order to fix bug (#173036, 173435, 173453, 173428)
- Add more perl magic to pre script to update ModulePath to the new
  location if it is specified in xorg.conf.
- Added xorg-x11-server-0.99.3-init-origins-fix.patch ported from monolithic
  xorg-x11 package to fix Xinerama bug.
- Added xorg-redhat-die-ugly-pattern-die-die-die.patch to kill the ugly grey
  stipple once again for bug (#173423).
- Added "BuildRequires: libdrm-devel" for DRI enabled builds.

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.3-5
- Xorg server should be suid for users to be able to run startx (#173064)

* Mon Nov 14 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-4
- Added temporary "BuildRequires: libXfont-devel >= 0.99.2-3" and
  "Requires: libXfont-devel >= 0.99.2-3" to ensure early-testers of
  pre-rawhide modular X have installed the work around for (#172997).
- Added implementation specific "Requires: xkbdata" to Xorg subpackage, as
  we want to ensure the xkb data files are present, but allow us the option
  of easily switching implementations to "xkeyboard-config" at a future
  date, if we decide to go that route.
- Re-enable _smp_mflags during build.
- Added "Requires: xorg-x11-drv-vesa" to Xorg subpackage (#173060)

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.3-3
- provide Xserver
- add another requires for basic bits

* Sun Nov 13 2005 Jeremy Katz <katzj@redhat.com> - 0.99.3-2
- add some deps to the Xorg subpackage for base fonts, keyboard and mouse 
  drivers, and rgb.txt that the server really wont work without

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.3-1
- Update to xorg-server-0.99.3 from X11R7 RC2.
- Add xorg-server.m4 to sdk subpackage, and "X" symlink to Xorg subpackage.

* Thu Nov 10 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-4
- Added "Requires: xkbcomp" for Xorg server, as it invokes it internally.

* Wed Nov 9 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-3
- Added "BuildRequires: libXtst-devel" for Xtst examples.

* Mon Nov 7 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Added versioning to Xorg virtual Provide, to allow config tools and driver
  packages to have version based requires.

* Thu Oct 27 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Update to xorg-server-0.99.2 from X11R7 RC1.
- Add "BuildRequires: xorg-x11-util-macros >= 0.99.1".
- Add "BuildRequires: mesa-source >= 6.4-4" for DRI builds.
- Added dmx related utilities to Xdmx subpackage.
- Individually list each X server module in file manifest.
- Hack man1 manpages to be installed into man1x.
- Add the following ./configure options --disable-dependency-tracking,
  --enable-composite, --enable-xtrap, --enable-xcsecurity, --enable-xevie,
  --enable-lbx, --enable-dri, --with-mesa-source, --with-module-dir,
  --with-os-name, --with-os-vendor, --with-xkb-output, --disable-xorgcfg
- Added getconfig, scanpci et al to Xorg subpackage
- Added inb, inl, inw, ioport, outboutl, outw, pcitweak utils to Xorg package
  conditionally, defaulting to "off".  These utilities are potentially
  dangerous and can physically damage hardware and/or destroy data, so are
  not shipped by default.
- Added "BuildRequires: libdmx-devel" for dmx utilities
- Added "BuildRequires: libXres-devel" for Xres examples
- Added {_libdir}/xserver/SecurityPolicy to Xorg subpackage for XSECURITY

* Mon Oct  3 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2.cvs20050830.2
- Fix license tag to be "MIT/X11"
- Change Xdmx subpackage to Obsolete xorg-x11-Xdmx instead of xorg-x11-Xnest

* Sun Oct  2 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2.cvs20050830.1
- Update BuildRequires for new library package naming (libX...)
- Use Fedora Extras style BuildRoot tag
- Invoke make with _smp_mflags to take advantage of SMP systems

* Tue Aug 30 2005 Kristian Hogsberg <krh@redhat.com> 0.99.1-2.cvs20050830
- Go back to %spec -n, use new cvs snapshot that supports overriding
  moduledir during make install, use %makeinstall.
- Drop %{moduledir}/multimedia globs.

* Fri Aug 26 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2.cvs20050825.0
- Added build dependency on xorg-x11-libfontenc-devel, as the build fails
  half way through without it, even though upstream dependencies do not
  specify it as required.

* Tue Aug 23 2005 Kristian Hogsberg <krh@redhat.com> 0.99.1-1
- Initial spec file for the modular X server.
