%global momorel 3
%define pkgname rendercheck
Summary: X.Org X11 %{pkgname}
Name: xorg-x11-%{pkgname}
Version: 1.4
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: libXrender-devel

%description
%{pkgname}

%prep
%setup -q -n %{pkgname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
%{_bindir}/%{pkgname}
%{_mandir}/man1/%{pkgname}.1.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4-1m)
- update to 1.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-2m)
- rebuild against gcc43

* Sat Mar  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-2m)
- %%NoSource -> NoSource

* Wed Jan 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Sat Nov  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1-1m)
- initial build
