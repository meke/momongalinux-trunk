%global momorel 3

Name:           homura-backgrounds
Version:        8.0.0
Release:        %{momorel}m%{?dist}
Summary:        Homura desktop backgrounds

Group:          Applications/Multimedia
License:        CC-BY-SA
URL:            http://www.momonga-linux.org

Source0:        %{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
Obsoletes:	momonga-backgrounds
Obsoletes:	natsuki-backgrounds

# for %%_kde4_* macros
BuildRequires:  cmake
Requires:       %{name}-gnome = %{version}-%{release}
Requires:       %{name}-kde = %{version}-%{release}


%description
This package contains desktop backgrounds for the Homura theme.
Pulls in both Gnome and KDE themes.

%package        single
Summary:        Single screen images for Homura Backgrounds
Group:          Applications/Multimedia
Obsoletes:	natsuki-backgrounds-single

%description    single
This package contains Single screen images for Homura Backgrounds.

%package        kde 
Summary:        Homura Wallpapers for KDE 
Group:          Applications/Multimedia
Provides:       system-backgrounds-kde

Requires:       %{name}-single = %{version}-%{release} 
Requires:       momonga-logos >= 7.994
Obsoletes:	natsuki-backgrounds-kde

%description    kde 
This package contains KDE desktop wallpapers for the Homura theme.

%package        gnome 
Summary:        Homura Wallpapers for Gnome 
Group:          Applications/Multimedia

Requires:       %{name}-single = %{version}-%{release} 
# FIXME: Which provides I should use?
Provides:       system-backgrounds
Provides:       system-backgrounds-gnome
Obsoletes:	natsuki-backgrounds-gnome

%description    gnome 
This package contains Gnome desktop wallpapers for the Homura theme.


%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc

%files single
%defattr(-,root,root,-)
%doc COPYING Credits
#There'll be also dual wallpapers in dual subpackage in the future, 
# hence the %%dir ownership is separated
%dir %{_datadir}/backgrounds/homura
%dir %{_datadir}/backgrounds/homura/default
%{_datadir}/backgrounds/homura/default/normalish
%{_datadir}/backgrounds/homura/default/standard
%{_datadir}/backgrounds/homura/default/wide
%dir %{_datadir}/backgrounds/homura/balloon
%{_datadir}/backgrounds/homura/balloon/normalish
%{_datadir}/backgrounds/homura/balloon/standard
%{_datadir}/backgrounds/homura/balloon/wide

%files kde
%defattr(-,root,root,-)
%{_kde4_datadir}/wallpapers

%files gnome
%defattr(-,root,root,-)
%{_datadir}/backgrounds/homura/default/homura1-*.xml
%{_datadir}/backgrounds/homura/default/homura2-*.xml
%{_datadir}/gnome-background-properties/desktop-backgrounds-homura.xml
%{_datadir}/backgrounds/homura/balloon/homura-balloon-night.xml
%{_datadir}/backgrounds/homura/balloon/homura-balloon-pastel.xml
%{_datadir}/gnome-background-properties/desktop-backgrounds-homura-balloon.xml


%changelog
* Thu Oct 25 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.0.0-3m)
- add "Homura Balloon" by tetsureba, thanks a lot ;)

* Fri Feb 10 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (8.0.0-2m)
- add jpeg images
- modify homura image properties

* Fri Feb 10 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.0.0-1m)
- update backgrounds package for Momonga Linux 8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0.0-7m)
- rebuild for new GCC 4.5

* Sun Sep  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0.0-6m)
- add links of hayabusa to package kde

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.0-5m)
- full rebuild for mo7 release

* Wed Aug 18 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (7.0.0-4m)
- add Obsoletes: momonga-backgrounds

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0.0-3m)
- own %%{_kde4_datadir}/wallpapers

* Sat Aug 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.0.0-2m)
- add two links for KDE

* Sat Aug 14 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (7.0.0-1m)
- Initial backgrounds package for Momonga Linux 7(Natsuki)
