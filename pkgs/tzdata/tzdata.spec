%global momorel 1

Summary: Timezone data
Name: tzdata
Version: 2014d
%define tzdata_version %{version}
%define tzcode_version %{version}
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Base
URL: https://www.iana.org/time-zones
Source0: ftp://ftp.iana.org/tz/releases/tzdata%{tzdata_version}.tar.gz
Source1: ftp://ftp.iana.org/tz/releases/tzcode%{tzcode_version}.tar.gz
NoSource: 0
NoSource: 1
# Add new patches here...
# Patch1: your-new-patch.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gawk, glibc, perl
BuildRequires: java-devel
BuildRequires: glibc-common >= 2.5.90-7
Conflicts: glibc-common <= 2.3.2-63
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package contains data files with rules for various timezones around
the world.

%package java
Summary: Timezone data for Java
Group: System Environment/Base
Source3: javazic.tar.gz
Source4: javazic-1.8-37392f2f5d59.tar.xz
Patch100: javazic-fixup.patch
Patch101: javazic-exclusion-fix.patch

%description java
This package contains timezone information for use by Java runtimes.

%prep
%setup -q -c -a 1

# Add new patches here...
# %patch1 -p1

mkdir javazic
tar zxf %{SOURCE3} -C javazic
pushd javazic
%patch100
%patch101

# Hack alert! sun.tools may be defined and installed in the
# VM. In order to guarantee that we are using IcedTea/OpenJDK
# for creating the zoneinfo files, rebase all the packages
# from "sun." to "rht.". Unfortunately, gcj does not support
# any of the -Xclasspath options, so we must go this route
# to ensure the greatest compatibility.
mv sun rht
find . -type f -name '*.java' -print0 \
    | xargs -0 -- sed -i -e 's:sun\.tools\.:rht.tools.:g' \
                         -e 's:sun\.util\.:rht.util.:g'
popd

tar xf %{SOURCE4}

echo "%{name}%{tzdata_version}" >> VERSION

%build
FILES="africa antarctica asia australasia europe northamerica southamerica
       pacificnew etcetera backward"

mkdir zoneinfo/{,posix,right}
zic -y ./yearistype -d zoneinfo -L /dev/null -p America/New_York $FILES
zic -y ./yearistype -d zoneinfo/posix -L /dev/null $FILES
zic -y ./yearistype -d zoneinfo/right -L leapseconds $FILES

grep -v tz-art.htm tz-link.htm > tz-link.html

# Java 6/7 tzdata
pushd javazic
javac -source 1.5 -target 1.5 -classpath . `find . -name \*.java`
popd

java -classpath javazic/ rht.tools.javazic.Main -V %{version} \
  -d javazi \
  $FILES javazic/tzdata_jdk/gmt javazic/tzdata_jdk/jdk11_backward

# Java 8 tzdata
pushd javazic-1.8
javac -source 1.7 -target 1.7 -classpath . `find . -name \*.java`
popd

java -classpath javazic-1.8 build.tools.tzdb.TzdbZoneRulesCompiler \
    -srcdir . -dstfile tzdb.dat \
    $FILES `find -type f javazic-1.8/tzdata_jdk/`

%install
rm -fr $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_datadir}
cp -prd zoneinfo $RPM_BUILD_ROOT%{_datadir}
install -p -m 644 zone.tab iso3166.tab $RPM_BUILD_ROOT%{_datadir}/zoneinfo
cp -prd javazi $RPM_BUILD_ROOT%{_datadir}/javazi
mkdir -p $RPM_BUILD_ROOT%{_datadir}/javazi-1.8
install -p -m 644 tzdb.dat $RPM_BUILD_ROOT%{_datadir}/javazi-1.8/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_datadir}/zoneinfo
%doc README
%doc Theory
%doc tz-link.html

%files java
%defattr(-,root,root)
%{_datadir}/javazi
%{_datadir}/javazi-1.8

%changelog
* Mon Jun  9 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2014d-1m)
- update to 2014d for tzdata and tzcode
- sync with fc21
- patches from fc21

* Fri Mar 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2012b-1m)
- update tzdata to 2012b
- update tzcode to 2012b

* Sun Nov  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2011n-1m)
- update tzdata to 2011n

* Tue Oct 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2011l-1m)
- update tzdata to 2011l
- update tzcode to 2011i
- time zone database will be updated ICANN

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2011g-1m)
- update tzdata to 2011g
- update tzcode to 2011g

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2011d-2m)
- rebuild for new GCC 4.6

* Tue Mar 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2011d-1m)
- update tzdata to 2011d
- update tzcode to 2011d

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2011a-1m)
- update tzdata to 2011a
- update tzcode to 2011a

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2010n-2m)
- rebuild for new GCC 4.5

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2010n-1m)
- update tzdata to 2010n
- update tzcode to 2010n

* Mon Oct  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2010m-1m)
- update tzdata to 2010m
- update tzcode to 2010m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2010j-2m)
- full rebuild for mo7 release

* Thu Jul  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2010j-1m)
- update tzdata to 2010j
- update tzcode to 2010j

* Tue Apr 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2010h-1m)
- update tzdata to 2010h
- update tzcode to 2010f

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2009u-1m)
- update tzdata to 2009u
- update tzcode to 2009t

* Tue Nov 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2009s-1m)
- update tzdata to 2009s
- update tzcode to 2009r

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2009g-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2009g-1m)
- update 2009g

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2008i-2m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2008i-1m)
- update ti 2008i
- sync with Fedora devel

* Mon Jun 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2008c-1m)
- update to 2008c
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2007j-3m)
- rebuild against gcc43

* Wed Jan  9 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2007j-2m)
- stop NoSource

* Sun Dec 30 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2007j-1m)
- sync Fedora

* Sat Nov 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2007i-1m)
- sync Fedora

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2007f-1m)
- update to 2007f
- sync with FC

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2006b-1m)
- version up

* Thu Mar 10 2005 Yasuo Ohgaki <yoghaki@ohgaki.net>
- (2005c-1m)
- version up

* Fri Dec 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2003d-1m)
- import from fedora

* Mon Oct  6 2003 Jakub Jelinek <jakub@redhat.com> 2003d-1
- 2003d

* Thu Sep 25 2003 Jakub Jelinek <jakub@redhat.com> 2003c-1
- 2003c
- updates for Brasil (#104840)

* Mon Jul 28 2003 Jakub Jelinek <jakub@redhat.com> 2003a-2
- rebuilt

* Mon Jul 28 2003 Jakub Jelinek <jakub@redhat.com> 2003a-1
- initial package
