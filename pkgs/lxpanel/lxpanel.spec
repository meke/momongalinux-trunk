%global momorel 6

Summary:        A lightweight X11 desktop panel
Name:           lxpanel
Version:        0.5.6
Release:        %{momorel}m%{?dist}

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.org/
Source0:        http://downloads.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
# distro specific patches
Patch100:       lxpanel-0.5.4-default.patch
Patch101:       lxpanel-0.3.8.1-nm-connection-editor.patch

# conflict alarm with unistd.h
Patch200:       lxpanel-0.5.6-alarm.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires:  docbook-utils
BuildRequires:  gettext
BuildRequires:  gtk2-devel 
BuildRequires:  intltool
BuildRequires:  libXpm-devel
BuildRequires:  startup-notification-devel
# required for alsa mixer plugin
BuildRequires:  alsa-lib-devel
# required for netstatus plugin
BuildRequires:  wireless-tools-devel
BuildRequires:  menu-cache-devel >= 0.3.0

%description
lxpanel is a lightweight X11 desktop panel. It works with any ICCCM / NETWM 
compliant window manager (eg sawfish, metacity, xfwm4, kwin) and features a 
tasklist, pager, launchbar, clock, menu and sytray.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gtk2-devel 
Requires:       libXpm-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q
%patch100 -p1 -b .default
%patch101 -p1 -b .system-config-network
%patch200 -p1 -b .alarm

%build
%configure LIBS="-lX11 -lgmodule-2.0" CPPFLAGS=-DGLIB_COMPILATION
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/lxpanel*
%{_datadir}/lxpanel/
%{_libdir}/lxpanel/
%{_mandir}/man1/lxpanel*

%files devel
%defattr(-,root,root,-)
%{_includedir}/lxpanel/
%{_libdir}/pkgconfig/lxpanel.pc

%changelog
* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.6-6m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.6-4m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.6-3m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.6-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.6-1m)
- update to 0.5.6

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.3-3m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.3-1m)
- update to 0.5.3

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1 based on Fedora 11 (0.4.0-1)

* Tue Mar  3 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.8.1-1m)
- import from Fedora to Momonga

* Thu Aug 28 2008 Sebastian Vahl <fedora@deadbabylon.de> 0.3.8.1-2
- re-create patches for rpmbuild's fuzz=0

* Tue Jul 08 2008 Sebastian Vahl <fedora@deadbabylon.de> 0.3.8.1-1
- new upstream version: 0.3.8.1

* Fri Jul 04 2008 Sebastian Vahl <fedora@deadbabylon.de> 0.3.8-1
- new upstream version: 0.3.8
- new BR in this version: intltool

* Sun Jun 15 2008 Sebastian Vahl <fedora@deadbabylon.de> 0.3.7-1
- new upstream version: 0.3.7

* Mon May 05 2008 Sebastian Vahl <fedora@deadbabylon.de> 0.3.5.4-1
- new upstream version: 0.3.5.4
- update lxpanel-default.patch

* Sun Mar 31 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.9.0-1
- new upstream version: 0.2.9.0

* Wed Mar 26 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.8-2
- BR: docbook-utils

* Thu Mar 20 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.8-1
- new upstream version: 0.2.8
- add lxpanel-0.2.8-manpage.patch

* Thu Mar 13 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.7.2-1
- new upstream version: 0.2.7.2
- update lxpanel-default.patch

* Mon Feb 25 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.6-1
- new upstream version: 0.2.6
- update lxpanel-default.patch

* Sat Feb 09 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.4-6
- rebuild for new gcc-4.3

* Thu Aug 16 2007 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.4-5
- Change License to GPLv2+

* Mon Jan 08 2007 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.4-4
- Fixed some minor issues from the review process (#219930)

* Sun Dec 17 2006 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.4-3
- BR: startup-notification-devel
- Added Patch1 from Chung-Yen to fix wrong starters in default config
- Removed pcmanfm.desktop from the default config for the moment

* Fri Dec 01 2006 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.4-2
- BR: gettext

* Wed Nov 29 2006 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.4-1
- New upstream version: 0.2.4

* Sun Nov 05 2006 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.2-1
- New upstream version: 0.2.1

* Fri Nov 03 2006 Sebastian Vahl <fedora@deadbabylon.de> - 0.2.0-1
- New upstream version: 0.2.0

* Wed Oct 25 2006 Sebastian Vahl <fedora@deadbabylon.de> - 0.1.1-2
- Rebuild for FC6

* Thu Oct 14 2006 Sebastian Vahl <fedora@deadbabylon.de> - 0.1.1-1
- Initial Release
