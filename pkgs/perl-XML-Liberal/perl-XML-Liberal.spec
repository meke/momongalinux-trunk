%global         momorel 6

Name:           perl-XML-Liberal
Version:        0.30
Release:        %{momorel}m%{?dist}
Summary:        Super liberal XML parser that parses broken XML
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-Liberal/
Source0:        http://www.cpan.org/authors/id/M/MI/MIYAGAWA/XML-Liberal-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Carp
BuildRequires:  perl-Class-Accessor
BuildRequires:  perl-Encode
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Filter-Util-Call
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-HTML-Tagset
BuildRequires:  perl-List-Util
BuildRequires:  perl-Module-Pluggable-Fast >= 0.16
BuildRequires:  perl-UNIVERSAL-require
BuildRequires:  perl-XML-LibXML >= 2.0014-2m
Requires:       perl-Carp
Requires:       perl-Class-Accessor
Requires:       perl-Encode
Requires:       perl-Filter-Util-Call
Requires:       perl-HTML-Parser
Requires:       perl-HTML-Tagset
Requires:       perl-List-Util
Requires:       perl-Module-Pluggable-Fast >= 0.16
Requires:       perl-UNIVERSAL-require
Requires:       perl-XML-LibXML >= 2.0014-2m
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
XML::Liberal is a super liberal XML parser that can fix broken XML stream
and create a DOM node out of it.

%prep
%setup -q -n XML-Liberal-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/XML/Liberal
%{perl_vendorlib}/XML/Liberal.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.16.3

* Sat Jan 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-15m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-14m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-13m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-4m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- rebuild against perl-5.10.1

* Fri Mar 21 2009 Narita Koichi <pulsar@momonga-linux>
- (0.21-1m)
- update to 0.21

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20-3m)
- rebuild against rpm-4.6

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-2m)
- NoSource again

* Wed Nov 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20
- No NoSource

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.18-2m)
- rebuild against gcc43

* Sat Mar  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.16-2m)
- use vendor

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-1m)
- spec file was autogenerated
