%global         momorel 5

Name:           ikiwiki
Version:        3.20130212
Release:        %{momorel}m%{?dist}
Summary:        A wiki compiler

Group:          Applications/Internet
License:        GPLv2+
URL:            http://ikiwiki.info/
Source0:        http://ftp.debian.org/debian/pool/main/i/%{name}/%{name}_%{version}.tar.gz
NoSource:       0
Patch0:         ikiwiki-3.00-libexecdir.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  perl-Text-Markdown
BuildRequires:  perl-Mail-Sendmail
BuildRequires:  perl-HTML-Scrubber
BuildRequires:  perl-XML-Simple
BuildRequires:  perl-Time-Date
BuildRequires:  perl-Time-Date
BuildRequires:  perl-HTML-Template
BuildRequires:  perl-CGI-FormBuilder
BuildRequires:  perl-CGI-Session
BuildRequires:  perl-File-MimeInfo
BuildRequires:  perl-HTML-Parser >= 3.65-2m
BuildRequires:  gettext
BuildRequires:  po4a

Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires:       perl-Text-Markdown
Requires:       perl-Mail-Sendmail
Requires:       perl-HTML-Scrubber
Requires:       perl-XML-Simple
Requires:       perl-CGI-FormBuilder
Requires:       perl-CGI-Session
Requires:       perl-File-MimeInfo
Requires:       python-docutils

%define cgi_bin %{_libexecdir}/w3m/cgi-bin

%description
Ikiwiki is a wiki compiler. It converts wiki pages into HTML pages
suitable for publishing on a website. Ikiwiki stores pages and history
in a revision control system such as Subversion or Git. There are many
other features, including support for blogging, as well as a large
array of plugins.

%prep
%setup -q -n %{name}
%patch0 -p1 -b .libexecdir

# Filter unwanted Provides:
%{__cat} << \EOF > %{name}-prov
#!/bin/sh
%{__perl_provides} $* |\
  %{__sed} -e '/perl(IkiWiki.*)/d'
EOF

%define __perl_provides %{_builddir}/%{name}/%{name}-prov
%{__chmod} +x %{__perl_provides}

# Filter Requires, all used by plugins
# - Monotone: see bz 450267
%{__cat} << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  %{__sed} -e '/perl(IkiWiki.*)/d' \
           -e '/perl(Monotone)/d'
EOF

%define __perl_requires %{_builddir}/%{name}/%{name}-req
%{__chmod} +x %{__perl_requires}

# goes into the -w3m subpackage
%{__cat} << \EOF > README.fedora
See http://ikiwiki.info/w3mmode/ for more information.
EOF

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor PREFIX=%{_prefix}
# parallel builds currently don't work
%{__make} 

%install
%{__rm} -rf %{buildroot}
%{__make} pure_install DESTDIR=%{buildroot} W3M_CGI_BIN=%{cgi_bin}
%find_lang %{name}

# move external plugins
%{__mkdir_p} %{buildroot}%{_libexecdir}/ikiwiki/plugins
%{__mv} %{buildroot}%{_prefix}/lib/ikiwiki/plugins/* \
        %{buildroot}%{_libexecdir}/ikiwiki/plugins

# remove shebang
%{__sed} -e '1{/^#!/d}' -i \
        %{buildroot}%{_sysconfdir}/ikiwiki/auto.setup \
        %{buildroot}%{_sysconfdir}/ikiwiki/auto-blog.setup \
        %{buildroot}%{_libexecdir}/ikiwiki/plugins/proxy.py

%clean
%{__rm} -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%{_bindir}/ikiwiki*
%{_sbindir}/ikiwiki*
%{_mandir}/man1/ikiwiki*
%{_mandir}/man8/ikiwiki*
%{_datadir}/ikiwiki
%dir %{_sysconfdir}/ikiwiki
%config(noreplace) %{_sysconfdir}/ikiwiki/*
# contains a packlist only
%exclude %{perl_vendorarch}
%{perl_vendorlib}/IkiWiki*
%exclude %{perl_vendorlib}/IkiWiki*/Plugin/skeleton.pm.example
%{_libexecdir}/ikiwiki
%doc README debian/changelog debian/NEWS html
%doc IkiWiki/Plugin/skeleton.pm.example

%package w3m
Summary:        Ikiwiki w3m cgi meta-wrapper
Group:          Applications/Internet
Requires:       w3m
Requires:       %{name} = %{version}-%{release}

%description w3m
Enable usage of all of ikiwiki's web features (page editing, etc) in
the w3m web browser without a web server. w3m supports local CGI
scripts, and ikiwiki can be set up to run that way using the
meta-wrapper in this package.
#'

%files w3m
%defattr(-,root,root,-)
%doc README.fedora
%{cgi_bin}/ikiwiki-w3m.cgi

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20130212-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20130212-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20130212-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20130212-2m)
- rebuild against perl-5.18.0

* Mon Mar 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20130212-1m)
- update to 3.20130212

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20120725-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20120725-2m)
- rebuild against perl-5.16.2

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20120725-1m)
- update to 3.20120725

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20120516-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20120516-2m)
- rebuild against perl-5.16.0

* Fri May 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20120516-1m)
- [SECURITY] CVE-2012-0220
- update to 3.20120516

* Mon Mar 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20120203-1m)
- update to 3.20120203

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20110905-2m)
- rebuild against perl-5.14.2

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20110905-1m)
- update to 3.20110905

* Fri Jul 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20110715-1m)
- update to 3.20110715

* Sun Jul 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20110707-1m)
- update to 3.20110707

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20110328-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20110328-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20110328-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20110328-1m)
- [SECURITY] CVE-2011-1401
- update to 3.20110328

* Mon Jan  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20101231-1m)
- update to 3.20101231

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20100623-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20100623-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.20100623-2m)
- full rebuild for mo7 release

* Thu Jun 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20100623-1m)
- update to 3.20100623

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20100504-2m)
- rebuild against perl-5.12.1

* Wed May  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20100504-1m)
- update to 3.20100504

* Sun Apr 18 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.20100312-3m)
- add BuildRequires:  perl-HTML-Parser >= 3.65-2m

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20100312-2m)
- rebuild against perl-5.12.0

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.20100312-1m)
- [SECURITY] CVE-2010-1195
- update to 3.20100312

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20100302-1m)
- update to 3.20100302

* Fri Jan 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20100102.3-1m)
- update to 3.20100102.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.14159265-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.14159265-1m)
- [SECURITY] CVE-2009-2944
- update to 3.14159265

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.141-2m)
- rebuild against perl-5.10.1

* Fri Jun 26 2009 NARITA Koichi <pulsar@mmonga-linux.org>
- (3.141-1m)
- update to 3.141

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.13-1m)
- sync with Fedora 11 (3.12-1)
- update to 3.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.53.3-2m)
- rebuild against rpm-4.6

* Wed Nov 19 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.53.3-1m)
- update to 2.53.3
-- 2.53.2 is no longer available

* Thu Oct 30 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.53.2-1m)
- update to 2.53.2

* Sat Jul 12 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.53-1m)
- update to 2.53

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.51-1m)
- import from Fedora to Momonga

* Sun Jul  6 2008 Thomas Moschny <thomas.moschny@gmx.de> - 2.51-1
- Update to 2.51.
- Save iconv output to a temporary file.

* Sun Jun 15 2008 Thomas Moschny <thomas.moschny@gmx.de> - 2.50-1
- Update to 2.50.
- Move ikiwiki-w3m.cgi into a subpackage.
- Add ikiwiki's own documentation.
- Remove duplicate requirement perl(File::MimeInfo).
- Minor cleanups.

* Mon Jun  2 2008 Thomas Moschny <thomas.moschny@gmx.de> - 2.48-1
- Update to 2.48.

* Wed May 28 2008 Thomas Moschny <thomas.moschny@gmx.de> - 2.47-1
- Update to 2.47.

* Tue May 13 2008 Thomas Moschny <thomas.moschny@gmx.de> - 2.46-1
- Update to 2.46.

* Sat May 10 2008 Thomas Moschny <thomas.moschny@gmx.de> - 2.45-1
- New package.
