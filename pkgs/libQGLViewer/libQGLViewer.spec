%global         momorel 1

Name:           libQGLViewer
Version:        2.3.17
Release:        %{momorel}m%{?dist}
Summary:        Qt based OpenGL generic 3D viewer library
Group:          System Environment/Libraries
License:        "GPLv2 with exceptions" or "GPLv3 with exceptions" 
URL:            http://www.libqglviewer.com/
Source0:        http://www.libqglviewer.com/src/%{name}-%{version}.tar.gz
NoSource:       0
# QGLViewer/VRender/gpc.cpp uses exit(0) to "abort" from a failure of malloc
# Use abort() instead.
Patch0:         libQGLViewer-2.3.1-exit.patch
# libQGLViewer .pro files explicitely remove "-g" from compile flags. Make
# them back.
Patch1:         libQGLViewer-2.3.6-dbg.patch
# fix build failure when using kde 4.8.0 or later
Patch2:         libQGLViewer-2.3.9-kde480.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel

%description
%{name} is a C++ library based on Qt that eases the creation of OpenGL
3D viewers. It provides some of the typical 3D viewer functionality, such
as the possibility to move the camera using the mouse, which lacks in most
of the other APIs. Other features include mouse manipulated frames,
interpolated keyFrames, object selection, stereo display, screenshot saving
and much more. It can be used by OpenGL beginners as well as to create
complex applications, being fully customizable and easy to extend.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       qt-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package doc
Summary: API documentation, demos and example programs for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
%{summary}.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .exit
%patch1 -p1 -b dbg
%patch2 -p1 -b .kde480~

# Fix the encoding of several files, so that they all use utf-8.
for f in doc/*.html doc/examples/*.html \
        examples/contribs/terrain/quadtree.cpp \
        examples/contribs/terrain/viewer.cpp \
        examples/contribs/quarto/piece.cpp \
        examples/contribs/dvonn/dvonnwindowimpl.cpp \
        examples/luxo/luxo.cpp \
        examples/contribs/quarto/jeu.cpp \
        examples/contribs/terrain/terrain.cpp \
        examples/contribs/quarto/rules.txt;
do
  cp -a $f $f.bak
  rm -f $f
  sed -e 's/charset=iso-8859-1/charset=utf-8/' < $f.bak | iconv -f l1 -t utf8 > $f;
  touch -r $f.bak $f
  rm -f $f.bak
done

%build
cd QGLViewer
%{_qt4_qmake} \
          LIB_DIR=%{_libdir} \
          DOC_DIR=%{_defaultdocdir}/%{name}-%{version} \
          INCLUDE_DIR=%{_includedir} \
          TARGET_x=%{name}.so.%{version}
# The TARGET_x variable gives the SONAME. However, qmake behavior is not
# correct when the SONAME is customized: it create wrong symbolic links
# that must be cleaned after the installation.

make %{?_smp_mflags}

cd ../designerPlugin
%{_qt4_qmake} LIB_DIR=../QGLViewer
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd QGLViewer
make -f Makefile -e INSTALL_ROOT=%{buildroot} install_target install_include
find %{buildroot} -name '*.la' -exec rm -f {} ';'
rm %{buildroot}%{_libdir}/libQGLViewer.prl

# Clean symbolic links
rm %{buildroot}%{_libdir}/libQGLViewer.so.?.? \
   %{buildroot}%{_libdir}/libQGLViewer.so.%{version}\\*

cd ../designerPlugin
make -e INSTALL_ROOT=%{buildroot} install

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README LICENCE CHANGELOG GPL_EXCEPTION
%{_libdir}/libQGLViewer.so.%{version}

%files devel
%defattr(-,root,root,-)
%{_includedir}/QGLViewer/
%{_libdir}/libQGLViewer.so
%{_qt4_plugindir}/designer/libqglviewerplugin.so

%files doc
%defattr(-,root,root,-)
%doc doc
%doc examples

%changelog
* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.17-1m)
- update to 2.3.17

* Mon Dec 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.9-3m)
- fix build failure with kde 4.8

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.9-2m)
- force rebuild to resolve dependency of package devel
- correct License

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.9-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.6-4m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.6-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.6-2m)
- full rebuild for mo7 release

* Sat Jul  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.6-1m)
- update to 2.3.6

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-3m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-1m)
- import for CGAL
