%global momorel 10

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-cherrypy2
Version:        2.3.0
Release:        %{momorel}m%{?dist}
Summary:        A pythonic, object-oriented web development framework
Group:          Development/Libraries
License:        BSD
URL:            http://www.cherrypy.org/
Source0:        http://download.cherrypy.org/cherrypy/%{version}/CherryPy-%{version}.tar.gz
NoSource:       0
Source1:        README.fedora
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0:         python-cherrypy-tutorial-doc.patch
Patch1:         python-cherrypy-regression-test.patch
Patch2:         python-cherrypy-2.3.0-EINTR.patch
Patch3:         python-cherrypy-2.3.0-py26-test.patch

BuildArch:      noarch

BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel >= 0.6c9-2m

Requires: python-setuptools

%description
CherryPy allows developers to build web applications in much the same way 
they would build any other object-oriented Python program. This usually 
results in smaller source code developed in less time.

This is a compat package for programs which still need the 2.x branch of
CherryPy.
%prep
%setup -q -n CherryPy-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

%{__sed} -i 's/\r//' CHANGELOG.txt README.txt CHERRYPYTEAM.txt cherrypy/tutorial/README.txt
cp -p %{SOURCE1} .

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} -c 'import setuptools; execfile("setup.py")' bdist_egg

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{python_sitelib}
easy_install -m --prefix $RPM_BUILD_ROOT%{_usr} -d $RPM_BUILD_ROOT%{python_sitelib} dist/*.egg
# %{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT
find $RPM_BUILD_ROOT%{python_sitelib}/ -type f -exec chmod -x \{\} \;

%check
cd cherrypy/test
%{__python} test.py

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGELOG.txt CHERRYPYTEAM.txt README.txt README.fedora
%doc cherrypy/tutorial
%{python_sitelib}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-10m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.0-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.0-5m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.0-4m)
- rebuild against python-2.6.1
- import py26-test.patch from Fedora

* Fri May  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.0-3m)
- enable build on x86_64

* Thu May  1 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.3.0-2m)
- enable easy_install command

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.0-1m)
- import from Fedora

* Wed Jan 16 2008 Toshio Kuratomi <toshio@fedoraproject.org> 2.3.0-3
- Merge changes from current python-cherrypy package.
- Update to 2.3.0.

* Wed Jan 16 2008 Toshio Kuratomi <toshio@fedoraproject.org> 2.2.1-9
- Initial cherrypy2 build.

* Sun Jan  6 2008 Toshio Kuratomi <toshio@fedoraproject.org> 2.2.1-8
- Fix a security bug with a backport of http://www.cherrypy.org/changeset/1775
- Include the egginfo files as well as the python files.

* Sat Nov  3 2007 Luke Macken <lmacken@redhat.com> 2.2.1-7
- Apply backported fix from http://www.cherrypy.org/changeset/1766
  to improve CherryPy's SIGSTOP/SIGCONT handling (Bug #364911).
  Thanks to Nils Philippsen for the patch.

* Mon Feb 19 2007 Luke Macken <lmacken@redhat.com> 2.2.1-6
- Disable regression tests until we can figure out why they
  are dying in mock.

* Sun Dec 10 2006 Luke Macken <lmacken@redhat.com> 2.2.1-5
- Add python-devel to BuildRequires

* Sun Dec 10 2006 Luke Macken <lmacken@redhat.com> 2.2.1-4
- Rebuild for python 2.5

* Mon Sep 18 2006 Luke Macken <lmacken@redhat.com> 2.2.1-3
- Rebuild for FC6
- Include pyo files instead of ghosting them

* Thu Jul 13 2006 Luke Macken <lmacken@redhat.com> 2.2.1-2
- Rebuild

* Thu Jul 13 2006 Luke Macken <lmacken@redhat.com> 2.2.1-1
- Update to 2.2.1
- Remove unnecessary python-abi requirement

* Sat Apr 22 2006 Gijs Hollestelle <gijs@gewis.nl> 2.2.0-1
- Update to 2.2.0

* Wed Feb 22 2006 Gijs Hollestelle <gijs@gewis.nl> 2.1.1-1
- Update to 2.1.1 (Security fix)

* Tue Nov  1 2005 Gijs Hollestelle <gijs@gewis.nl> 2.1.0-1
- Updated to 2.1.0

* Sat May 14 2005 Gijs Hollestelle <gijs@gewis.nl> 2.0.0-2
- Added dist tag

* Sun May  8 2005 Gijs Hollestelle <gijs@gewis.nl> 2.0.0-1
- Updated to 2.0.0 final
- Updated python-cherrypy-tutorial-doc.patch to match new version

* Wed Apr  6 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 2.0.0-0.2.b
- Removed CFLAGS

* Wed Mar 23 2005 Gijs Hollestelle <gijs[AT]gewis.nl> 2.0.0-0.1.b
- Initial Fedora Package
