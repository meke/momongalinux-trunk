%global momorel 3

Summary: A Linux video stream processing utility
Name: transcode
Version: 1.1.7
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://www.transcoding.org/
Source0: https://bitbucket.org/france/transcode-tcforge/downloads/transcode-%{version}.tar.bz2
NoSource: 0
Patch1: %{name}-1.1.7-ffmpeg.patch
Patch2: %{name}-1.1.7-ffmpeg-0.10.patch
Patch3: %{name}-1.1.7-ffmpeg2-1.patch
Patch10: %{name}-1.0.0beta3-ppc64.patch
Patch50: 10_freetype.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: a52dec
Requires: gawk
Requires: mjpegtools >= 1.9.0
BuildRequires: ImageMagick-devel >= 6.8.8.10
BuildRequires: SDL-devel >= 1.2.11-2m
BuildRequires: a52dec-devel >= 0.7.4-3m
BuildRequires: alsa-lib-devel >= 1.0.13-2m
#BuildRequires: avifile >= 0.7.38
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: freetype-devel >= 2.5.3
BuildRequires: libquicktime-devel
BuildRequires: lame-devel >= 3.97-2m
BuildRequires: lcms-devel >= 1.15-4m
BuildRequires: libXv-devel
BuildRequires: libXxf86dga-devel
BuildRequires: libXxf86vm-devel
BuildRequires: libdvdread-devel >= 4.1.2
BuildRequires: libdv-devel >= 1.0.0-2m
BuildRequires: libfame-devel >= 0.9.1-6m
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libogg-devel >= 1.1.3-2m
BuildRequires: libpostproc >= 0.10.3
BuildRequires: libtheora-devel
BuildRequires: libvorbis-devel >= 1.1.2-2m
BuildRequires: libxml2-devel
BuildRequires: lzo-devel >= 2.01-2m
BuildRequires: mpeg2dec-devel >= 0.4.0
BuildRequires: mplayer
BuildRequires: mjpegtools-devel >= 2.1.0
BuildRequires: x264-devel >= 0.0.721-0.20081021

%description
transcode is a linux text-console utility for video stream processing,
running on a platform that supports shared libraries and threads.  Decoding
and encoding is done by loading modules that are responsible for feeding
transcode with raw video/audio streams (import modules) and encoding the
frames (export modules). It supports elementary video and audio frame
transformations, including de-interlacing or fast resizing of video frames
and loading of external filters.

A number of modules are included to enable import of DVDs on-the-fly, MPEG
elementary (ES) or program streams (VOB), MPEG video, Digital Video (DV),
YUV4MPEG streams, NuppelVideo file format and raw or compressed
(pass-through) video frames and export modules for writing DivX;-), DivX
4.02/5.xx, XviD, Digital Video, MPEG-1/2 or uncompressed AVI files with
MPEG, AC3 (pass-through) or PCM audio.  More file formats and codecs for
audio/video import are supported by the avifile library import module, the
export with avifile is restricted to video codecs only, with MPEG/PCM or AC3
(pass-through) audio provided by transcode. Limited Quicktime export support
and DVD subtitle rendering is also avaliable.

It's modular concept is intended to provide flexibility and easy user
extensibility to include other video/audio codecs or file types.  A set of
tools is available to extract, demultiplex and decode the sources into raw
video/audio streams for import, non AVI-file export modules for writing
single frames (PPM) or YUV4MPEG streams, auto-probing and scanning your
sources and to enable post-processing of AVI files, including header fixing,
merging multiple files or splitting large AVI files to fit on a CD.

More information and usage examples can be found on the original author's
home page at

  http://www.theorie.physik.uni-goettingen.de/~ostreich/transcode/

Written by Thomas streich <ostreich@theorie.physik.uni-goettingen.de>
Currently maintained by Tilmann Bitterberg <transcode@tibit.org>
See the Authors file for contributions from the linux community.
See the file COPYING for license details.

%prep
%setup -q

#%patch1 -p0 -b .new_new_ffmpeg
#%patch2 -p0 -b .ffmpeg-0.10
%patch3 -p1 -b .ffmpeg2
%ifarch ppc64
%patch10 -p1 -b .ppc64~
%endif
%patch50 -p1 -b .freetype

%build
autoreconf -vfi
CFLAGS="%{optflags} `pkg-config libavcodec --cflags`" \
LIBS="-llzo2 `pkg-config libavcodec --libs`" \
%configure \
	--with-mod-path=%{_libdir}/transcode \
	--enable-alsa \
	--enable-freetype2 \
	--enable-lame \
	--enable-x264 \
	--enable-ogg \
	--enable-vorbis \
	--enable-libdvdread \
	--enable-libdv \
	--enable-lzo \
	--enable-a52 \
	--enable-libxml2 \
	--enable-mjpegtools \
	--enable-sdl \
	--enable-imagemagick \
	--enable-libquicktime \
	--enable-libjpeg \
	--enable-iconv \
	--enable-libmpeg2 \
	--enable-v4l \
	--enable-theora \
	--enable-libpostproc \
	--enable-xvid=no \
	--with-lzo-includes=%{_includedir}/lzo \
	--with-libavcodec-includes=%{_includedir}/ffmpeg/libavcodec

# todo: split libpostproc from mplayer
# this is dup mplayer and ffmpeg
#
# tmp disable	--enable-libmpeg3 \

# Parallel build shall fail!
# please use make -j1
%undefine _smp_mflags
%make

%install
%__rm -rf %{buildroot}
%makeinstall pkgdir=%{buildroot}%{_libdir}/transcode MOD_PATH=%{buildroot}%{_libdir}/transcode transform='s,x,x,'

find %{buildroot} -name "*.la" -delete

%clean
%__rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README TODO docs/*
%{_bindir}/*
%{_libdir}/transcode
%{_mandir}/man1/*
%{_docdir}/transcode

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-3m)
- rebuild against ImageMagick-6.8.8.10

* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-2m)
- rebuild against mjpegtools-2.1.0
- add Patch50 from debian

* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.7-1m)
- rebuild against ffmpeg

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-13m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-12m)
- rebuild against ImageMagick-6.8.2.10

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-11m)
- rebuild against ImageMagick-6.8.0.10

* Thu May 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-10m)
- [BUILD FIX] apply two gentoo patches to enable build

* Thu May 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-9m)
- [CAN NOT BUILD]
- rebuild against ffmpeg-0.10.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-8m)
- rebuild against ImageMagick-6.7.2.10

* Sat Jun  4 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-7m)
- rebuild against mjpegtools-2.0.0

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-6m)
- rebuild for ffmpeg-0.6.1-0.20110514

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-5m)
- rebuild for new GCC 4.6

* Tue Dec  7 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (1.1.5-4m)
- disable xvid, becase xvid is nonfree

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-1m)
- update 1.1.5

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-7m)
- rebuild against ImageMagick-6.6.2.10

* Sat May  1 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-6m)
- add BuildRequires

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-5m)
- rebuild against libjpeg-8a

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-4m)
- rebuild against ImageMagick-6.5.9.10

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-1m)
- update 1.1.4 release

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.7-0.991.2m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.7-0.991.2m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.7-0.991m)
- update 1.0.7-rc1 (newer ffmpeg API support)

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-8m)
- disable ImageMagick at this time...

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-7m)
- rebuild against x264

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.5-6m)
- rebuild against libdvdread-4.1.2

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-5m)
- rebuild against x264

* Sat Apr 26 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.5-4m)
- apply Patch200 to make it compile with kernel-2.6.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-3m)
- rebuild against gcc43

* Sat Mar 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-2m)
- add workaround for ffmpeg

* Sun Mar  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- %%NoSource -> NoSource

* Thu Dec 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-2m)
- rebuild against libvorbis-1.2.0-1m

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3
- rebuild against ImageMagick-6.3.5-2m
- update and rename patch2
- remove patch3, patch100, patch101, patch102, patch105

* Sun Jun 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-10m)
- change SOURCE URL

* Thu Apr 05 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.2-9m)
- rebuild against latest ffmpeg
- import patches from freshrpms.net
- - Patch100: transcode-1.0.2-filter_compare-fixes-try1.patch
- - Patch101: transcode-1.0.2-filter_logo-hangup-try1.patch
- - Patch102: transcode-1.0.x-filter-patch.txt
- - Patch104: transcode-1.0.2-libmpeg3.patch
- - Patch105: transcode-1.0.2-libavcodec.patch


* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-8m)
- rebuild against mjpegtools-1.9.0
- clean up spec file

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-7m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.2-6m)
- enable ppc64

* Tue Oct 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-5m)
- add patch4 (for autoconf-2.60)

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0.2-4m)
- rebuild against expat-2.0.0-1m

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.2-3m)
- rebuild against ffmpeg-cvs20060517
- - add Patch3: transcode-1.0.2-latestffmpeg.patch
- enable libpostproc, theola, avifile, v4l, libfame again

* Tue Feb 14 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.2-2m)
- rebuild against ImageMagick-6.2.6-1m

* Sun Dec 18 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.0.2-1m)
- update to 1.0.2
- rebuild against ImageMagick

* Thu Jul 21 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.0-0.3.3m)
- rebuild against lzo-2.01

* Sat Jun 11 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.0-0.3.2m)
  rebuild against lzo-2.00

* Tue May 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-0.3.1m)
- update to 1.0.0beta3
- add BuildRequires: ffmpeg-devel >= 0.4.9
- add BuildRequires: mpeg2dec >= 0.4.0
- delete patch0 and patch1

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.6.12-8m)
  undefined __libtoolize
  
* Sat Feb 19 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.12-7m)
- version down for ffmpeg
- add Patch1: transcode-%%{version}-decode_dv.patch
- use -fno-unit-at-a-time

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.14-2m)
- enable x86_64.

* Fri Feb 04 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.14-1m)
- update to 0.6.14
- add BuildRequires: ffmpeg >= 0.4.9
- delete --with-default-xvid=xvid4 for compile error
- adapt gcc34 patch(Patch0)
- add %%undefine _smp_mflags

* Fri Feb 04 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.12-6m)
- rebuild against xvid-1.1.0-beta1
- add --with-default-xvid=xvid4

* Mon Oct 25 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.6.12-5m)
- append CFLAGS "-fno-unit-at-a-time" for compilation problems
- add patch for gcc-3.4

* Wed Jul 7 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.6.12-4m)
- rpm42 compatibility
- patch by Yoshi Doi

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.12-3m)
- rebuild against libdv-0.102

* Sun May  2 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.12-2m)
- rebuild agaist xvid-1.0.0rc4 (apiversion 4)

* Thu Apr 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.12-1m)
- version 0.6.12 has been released

* Thu Jan  8 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.11-2m)
- add BuildPrereq: mplayer
- - for building mplayer postprocessing filter plugin(filter_pp.so)

* Sat Nov  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.11-1m)
- major feature enhancements

* Sun Oct 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.10-2m)
- rebuild against ImageMagkck-5.5.7

* Thu Sep 11 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.6.10-1m)
- version 0.6.10
- Minor feature enhancements

* Thu Sep  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.9-2m)
- remove implicit requirements

* Thu Sep  4 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.6.9-1m)
- first import to Momonga

* Thu Aug 21 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.6.9-0.0.1m)
- version 0.6.9

* Thu Jul 24 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.6.8-0.0.1m)

* Sat May 31 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.6.7-0.0.1m)

* Mon Feb 3 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.6.3-0.0.1m)
- version 0.6.3
- enable avifile support

* Mon Jan 20 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.6.3-0.0.0.20021219.1m)
- build for Momonga Linux
- --with-avifile-mods=no because "yes" causes a compile error by avifile
- --enable-warnings=no and Patch0 because trivial error stops a build process...

* Fri Nov 8 2002 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.6.2-0.20021024.1m)
- modified src.rpm of version 0.6.0 at freshrpms.net

* Fri Aug  2 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.6.0 final!

* Thu Aug  1 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.6.0rc4... doesn't compile :-(
- Added libdv, libogg and libvorbis dependencies.

* Fri Jun 14 2002 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.6.0rc1.
- Spec file updates and fixes.

* Mon Dec 24 2001 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.5.3.

* Sun Dec  2 2001 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Update to 0.5.1.

* Sun Nov 25 2001 Matthias Saou <matthias.saou@est.une.marmotte.net>
- Major spec file cleanup.

* Wed Jul 11 2001 Thomas Ostreich
- update to transcode v0.3.3
- small changes suggested by VM

* Tue Jul 10 2001 Thomas Ostreich
- update to transcode v0.3.2
- added pkgdir in install section

* Tue Jul 10 2001 Volker Moell <moell@gmx.de>
- Wrote this specfile; first build
