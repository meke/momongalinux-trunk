%global momorel 1

%define wimax 1
%define openconnect 0
%define openvpn 0
%define polkit 1
%define bluetooth 1

Name: connman
Release: %{momorel}m%{dist}
Version: 1.19
Summary: Connection Manager
Group: System Environment/Base
License: GPLv2
URL: http://www.connman.net/
Source0: http://www.kernel.org/pub/linux/network/%{name}/%{name}-%{version}.tar.gz
Nosource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(libudev) >= 136
BuildRequires: dhclient
BuildRequires: gnutls-devel >= 3.2.0
%if %{?wimax} 
BuildRequires: wimax-daemon
%endif

Requires: dbus
Requires: wpa_supplicant >= 0.5.7
Requires: libeap libnl3
%if %{?bluetooth}
Requires: bluez
%endif

%description
Connection Manager (connman) provides a daemon for managing Internet connections
within embedded devices running the Linux operating system.

%package devel
Summary: Development files for Connection Manager
Group: Development/Libraries
Requires: %{name} >= %{version}

%description devel
connman-devel contains development files for use with connman.

%prep
%setup -q -n %{name}-%{version}

%build
%configure --prefix=/usr \
	   --disable-static \
	   --enable-ethernet \
	   --enable-wifi \
	   --enable-loopback \
	   --enable-threads \
	   --with-gnu-ld \
	   --enable-client \
	   --enable-iospm \
	   --enable-nmcompat \
	   --enable-pacrunner \
	   --enable-l2tp \
	   --enable-pptp \
	%if %{?bluetooth}
	   --enable-bluetooth \
	%endif
	%if %{?openconnect}
	   --enable-openconnect \
	%endif
	%if %{?openvpn}
	   --enable-openvpn \
	%endif
	%if %{?wimax}
	   --enable-iwmx \
	%endif
	%if %{?polkit}
	   --enable-polkit \
	%endif
	   --disable-static

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%make_install

%find_lang %{name} || echo -n >> %{name}.lang

mkdir -p %{buildroot}/%{_datadir}/doc/%{name}-%{version}
for f in `ls %{buildroot}/%{_datadir}/doc/`; do
	if [ -f %{buildroot}/%{_datadir}/doc/$f ]; then
		mv %{buildroot}/%{_datadir}/doc/$f %{buildroot}/%{_datadir}/doc/%{name}-%{version}
	fi
done

# --disable-static fails, remove static libs 
rm -f %{buildroot}%{_libdir}/%{name}/plugins/*.la
rm -f %{buildroot}%{_libdir}/%{name}/plugins-vpn/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel -p /sbin/ldconfig

%postun devel -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL ChangeLog NEWS README
%{_sbindir}/*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/plugins
%dir %{_libdir}/%{name}/plugins-vpn
%dir %{_libdir}/%{name}/scripts
%{_libdir}/%{name}/plugins/*.so
%{_libdir}/%{name}/plugins-vpn/*.so
%{_libdir}/%{name}/scripts/*
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}-vpn.service
%config %{_sysconfdir}/dbus-1/system.d/*.conf
%{_datadir}/dbus-1/system-services/net.%{name}.vpn.service
%if %{?polkit}
%{_datadir}/polkit-1/actions/net.%{name}.policy
%{_datadir}/polkit-1/actions/net.%{name}.vpn.policy
%endif
%{_mandir}/man5/*
%{_mandir}/man8/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}/*.h
%{_libdir}/pkgconfig/*.pc

%changelog
* Wed Nov 27 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15
- rebuild against gnutls-3.2.0

* Tue Feb  5 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6-3m)
- rebuild against iptables-1.4.17-1m

* Wed Sep 05 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.6-m2)
- added Requires: Libnl3 libeap

* Wed Sep 05 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.6-m1)
- Initial Momomga release, imported from opensuse
