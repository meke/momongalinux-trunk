%global momorel 9

Summary: 2D data plotting program
Name: xgraph
Version: 12.1
Release: %{momorel}m%{?dist}
URL: http://www.isi.edu/nsnam/xgraph/
Source: http://www.isi.edu/nsnam/dist/xgraph-%{version}.tar.gz
NoSource: 0
Patch0: xgraph-man.patch
Patch1: xgraph-12.1-glibc210.patch
License: BSD
Group: Applications/Engineering
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Xgraph is a popular two-dimensional plotting program that accepts data in a
form similar to the unix program graph and displays line graphs, scatter
plots, or bar charts on an X11 display.  These graphs are fully annotated
with a title, axis numbering and labels, and a legend.  Zooming in on regions
of a graph is supported.  The program can automatically generate hardcopy
output to Postscript printers and HPGL plotters.

%prep
%setup -q
%patch0 -p0 -b .man
%patch1 -p1 -b .glibc210
mv xgraph.man xgraph.1

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/xgraph
%{_mandir}/man1/xgraph.1*
%doc README*
%doc examples

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (12.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (12.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (12.1-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (12.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (12.1-5m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (12.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (12.1-3m)
- rebuild against gcc43

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (12.1-2m)
- revised installdir

* Sun Jul  6 2003 Hiroyuki KOGA <kuma@momonga-linux.org>
- (12.1-1m)
- first import
