%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-camomile
Version:        0.8.3
Release:        %{momorel}m%{?dist}
Summary:        Unicode library for OCaml

Group:          Development/Libraries
# Several files are MIT and UCD licensed, but the overall work is LGPLv2+
# and the LGPL/GPL supercedes compatible licenses.
# https://www.redhat.com/archives/fedora-legal-list/2008-March/msg00005.html
License:        LGPLv2+
URL:            http://sourceforge.net/projects/camomile/
Source0:        http://downloads.sourceforge.net/camomile/camomile-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}, ocaml-findlib-devel, ocaml-ocamldoc, ocaml-camlp4-devel


%description
Camomile is a Unicode library for ocaml. Camomile provides Unicode
character type, UTF-8, UTF-16, UTF-32 strings, conversion to/from
about 200 encodings, collation and locale-sensitive case mappings, and
more.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%package        data
Summary:        Data files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    data
The %{name}-data package contains data files for developing
applications that use %{name}.


%prep
%setup -q -n camomile-%{version}


%build
./configure --prefix=%{_prefix} --datadir=%{_datadir} --libdir=%{_libdir}
make
make dochtml
make man
strip tools/*.opt


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install prefix=$RPM_BUILD_ROOT%{_prefix} DATADIR=$RPM_BUILD_ROOT%{_datadir}


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README
%{_libdir}/ocaml/camomile
%if %opt
%exclude %{_libdir}/ocaml/camomile/*.a
%exclude %{_libdir}/ocaml/camomile/*.cmxa
%exclude %{_libdir}/ocaml/camomile/*.cmx
%endif
%exclude %{_libdir}/ocaml/camomile/*.mli


%files devel
%defattr(-,root,root,-)
%doc README dochtml/*
%if %opt
%{_libdir}/ocaml/camomile/*.a
%{_libdir}/ocaml/camomile/*.cmxa
%{_libdir}/ocaml/camomile/*.cmx
%endif
%{_libdir}/ocaml/camomile/*.mli


%files data
%defattr(-,root,root,-)
%doc README
%{_datadir}/camomile/


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.3-1m)
- update to 0.8.3
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-2m)
- full rebuild for mo7 release

* Fri Jun 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.7.1-7
- Rebuild for OCaml 3.10.2

* Fri Mar 21 2008 Richard W.M. Jones <rjones@redhat.com> - 0.7.1-6
- ExcludeArch ppc64 (#438486).

* Mon Mar 17 2008 Richard W.M. Jones <rjones@redhat.com> - 0.7.1-5
- Definitive license.
- Move ./configure into the build section.
- Remove a superfluous comment in the install section.
- Fix rpmlint error 'configure-without-libdir-spec'.
- Scratch build in Koji.

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 0.7.1-4
- License is LGPLv2+ (no OCaml exception).

* Wed Mar  5 2008 Richard W.M. Jones <rjones@redhat.com> - 0.7.1-3
- Remove ExcludeArch ppc64.

* Tue Feb 12 2008 Richard W.M. Jones <rjones@redhat.com> - 0.7.1-2
- Added BR ocaml-camlp4-devel.
- Rename /usr/bin/*.opt as /usr/bin.

* Wed Aug 08 2007 Richard W.M. Jones <rjones@redhat.com> - 0.7.1-1
- Initial RPM release.
