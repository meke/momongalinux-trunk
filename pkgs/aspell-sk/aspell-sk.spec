%global momorel 5

%define aspellversion 6
%define lang sk
%define langrelease 2
%define aspellname aspell%{aspellversion}-%{lang}

Name:           aspell-%{lang}
Version:        2.01
Release:        %{momorel}m%{?dist}
Summary:        Slovak dictionaries for Aspell

Group:          Applications/Text
License:        GPLv2 or LGPLv2 or MPLv1.1
URL:            http://sk-spell.sk.cx/aspell-sk
Source0:        http://www.sk-spell.sk.cx/files/%{aspellname}-%{version}-%{langrelease}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  aspell >= 0.60
Requires:       aspell >= 0.60

%define debug_package %{nil}                                                    

%description
Provides the word list/dictionaries for the following: Slovak


%prep
%setup -q -n %{aspellname}-%{version}-%{langrelease}


%build
sh configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc doc/* Copyright README
%{_libdir}/aspell-*/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.01-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.01-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.01-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.01-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.00-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.00-1m)
- import from Fedora

* Sat Feb 9 2008 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 2.00-4
- rebuild against gcc4.3

* Thu Nov 1 2007 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 2.00-3
- update upstream
- updated license to upstream
- inspired by aspell-en (used aspellversion macro)

* Thu Sep 06 2007 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.52-3
- changed requirement to aspell >= 12:0.60 because of there worldlist
  incompatibility
- debug_package set to nil to prevent empty debuginfo package
- configure is called with sh interpreter to prevent rpmlint errors

* Thu Sep 06 2007 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.52-2
- added macros to Source tag
- cleanups

* Sun Aug 26 2007 Jan ONDREJ (SAL) <ondrejj(at)salstar.sk> - 0.52-1
- initial release
- configure rename to reconfigure to skip rpmlint errors
