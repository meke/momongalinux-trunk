%global         momorel 1
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         qtver 4.8.5
%global         kdever 4.12.0
%global         kdelibsrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         ftpdirver 1.1
%global         sourcedir %{release_dir}/%{name}/%{ftpdirver}/src

Summary:        Lightweight scanning program
Name:           skanlite
Version:        1.1
Release:        %{momorel}m%{?dist}
License:        GPLv2
Group:          Applications/Productivity
URL:            http://kde-apps.org/content/show.php/Skanlite?content=109803
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires:  gettext
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  kdegraphics-devel >= %{kdever}

%description
Skanlite is a light-weight scanning application based on libksane.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# validate desktop file
desktop-file-install \
        --dir="%{buildroot}%{_datadir}/applications/kde4" \
        %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop


%find_lang %{name} --with-kde

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-, root, root)
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop

%changelog
* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Tue Feb 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Mon Dec 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-2m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-1m)
- update to 0.7

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- update to 0.6

* Fri Nov 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-1m)
- import from Fedora devel

* Fri Feb 12 2010 Rex Dieter <rdieter@fedoraproject.org> - 0.4-1
- skanlite-0.4-kde-4.4.0

* Wed Nov 25 2009 Rex Dieter <rdieter@fedoraproject.org> - 0.3-6 
- rebuild (kdegraphics)
- use %%find_lang --with-kde

* Tue Sep 01 2009 Sebastian Vahl <svahl@fedoraproject.org> - 0.3-5
- KDE 4.3.1

* Tue Aug 11 2009 Sebastian Vahl <fedora@deadbabylon.de> - 0.3-4
- KDE 4.3.0

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jun 23 2009 Sven Lankes <sven@lank.es> - 0.3-2
- SPEC-Fixes from review

* Mon Jun 22 2009 Sven Lankes <sven@lank.es> - 0.3-1 
- Update to current upstream version
- Update license tag

* Thu Jan 08 2009 Teemu Rytilahti <tpr@d5k.net> - 0.2-2
- use source package from the kde's ftp-site instead of the svn

* Wed Jan 07 2009 Teemu Rytilahti <tpr@d5k.net> - 0.2-1
- initial package
