%global momorel 8
%global realname libglade

Summary: The libglade library for loading user interfaces.
Name: libglade2
Version: 2.6.4
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://www.jamesh.id.au/software/libglade/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/2.6/%{realname}-%{version}.tar.bz2
NoSource: 0
# http://bugzilla.gnome.org/show_bug.cgi?id=121025
Patch1: libglade-2.0.1-nowarning.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python >= 2.6
BuildRequires: gtk-doc
BuildRequires: atk-devel >= 1.26.0
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: libxml2-devel >= 2.7.3
BuildRequires: pango-devel >= 1.24.0
BuildRequires: fontconfig

Provides: %{realname}
Obsoletes: %{realname}

%description
The libglade library allows you to load user interfaces which are
stored externally into your program. This allows for alteration of the
interface without recompilation of the program. The interfaces can
also be edited with GLADE. Currently libglade supports all of the
widgets in current releases, keyboard accelerators and automatic
signal connection.

%package devel
Summary: The files needed for libglade application development.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Requires: libxml2-devel
Requires: python

Provides: %{realname}-devel
Obsoletes: %{realname}-devel

%description devel
The libglade-devel package contains the libraries, include files,
etc., that you can use to develop libglade applications.

%prep
%setup -q -n %{realname}-%{version}
%patch1 -p1 -b .nowarning

%build
%configure --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
%__mkdir_p %{buildroot}/%{_libdir}/libglade/2.0

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS ChangeLog NEWS README COPYING
%{_libdir}/lib*.so.*
%exclude %{_libdir}/lib*.la
%{_libdir}/libglade
%{_datadir}/xml/libglade

%files devel
%defattr(-, root, root)
%doc test-libglade.c
%{_bindir}/libglade-convert
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/pkgconfig/*.pc
%{_prefix}/include/libglade-2.0
%doc %{_datadir}/gtk-doc/html/libglade

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-8m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.4-5m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.4-4m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.4-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Mar 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.3-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.2-2m)
- rebuild against gcc43

* Sun Aug 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Thu Jul  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.0-3m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0-2m)
- rename libglade -> libglade2

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.1-3m)
- comment out unnessesaly autoreconf and make check

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.1-2m)
- enable gtk-doc

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.1-1m)
- version up.
- GNOME 2.12.1 Desktop

* Mon Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.1-1m)
- version 2.4.1
- GNOME 2.8 Desktop

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (2.4.0-2m)
- enable x86_64.

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0 has been released
- import libglade-2.0.1-nowarning.patch from FC (http://bugzilla.gnome.org/show_bug.cgi?id=121025)

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.3.6-2m)
- adjustment BuildPreReq

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.3.6-1m)
- version 2.3.6
- GNOME 2.6 Desktop

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.0.1-2m)
- pretty spec file.

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-2m)
- rebuild against for XFree86-4.3.0

* Sat Aug 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-7m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.12-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.12-2k)
- version 1.99.12

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.11-2k)
- version 1.99.11

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-6k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-2k)
- version 1.99.10

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-2k)
- version 1.99.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-8k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-6k)
- rebuild against for pango-1.0.0.rc2

* Thu Mar  7 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-6k)
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-4k)
- modify dependency list

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-2k)
- version 1.99.8

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-14k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-12k)
- change require python2 to python

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-10k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-8k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-6k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-4k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-2k)
- version 1.99.7
* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-6k)
- rebuild against for glib-1.3.13

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-2k)
- version 1.99.6

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.5-10k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.5-8k)
- rebuild against for atk-0.10

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.99.5-4k)
- requires: python2

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (1.99.5-2k)
- port from Jirai
- version 1.99.5

* Wed Sep 26 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.99.1-3k)
- 2.0.0

* Tue Aug 28 2001 Shingo Akagaki <dora@kondara.org>
- (0.16-18k)
- rebuild against for gnome-print-0.29
- disable gnome-db
- create libglade-gnomedb package in other spec

* Sun Aug 26 2001 Shingo Akagaki <dora@kondara.org>
- (0.16-16k)
- arrangement spec file

* Mon Aug 20 2001 Shingo Akagaki <dora@kondara.org>
- rebuild against for gal-0.10

* Sat Jul 21 2001 Shingo Akagaki <dora@kondara.org>
- add Buildprereq: pkgconfig

* Fri Jul 20 2001 Shingo Akagaki <dora@kondara.org>
- rebuild against gnome-db-0.2.10

* Wed Jul 18 2001 Shingo Akagaki <dora@kondara.org>
- rebuild against gal-0.9.1

* Mon Jun 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- rebuild against gal-0.8

* Sat May  5 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against gnome-print-0.25.

* Sun Mar 25 2001 Motonobu Ichimura <famao@kondara.org>
- now re-link to libxml (too early to use libxml2 I think)

* Fri Feb 16 2001 Motonobu Ichimura <famao@kondara.org>
- version 0.16
- added patch to handle if using xml2

* Thu Nov 30 2000 Kenichi Matsubara <m@kondara.org>
- (0.15-3k)
- Obsoletes libglade-0.14-newbonobo.patch.

* Wed Nov 29 2000 Shingo Akagaki <dora@kondara.org>
- version 0.15

* Mon Nov 06 2000 Shingo Akagaki <dora@kondara.org>
- compile with bonobo
 
* Sat Nov 04 2000 Motonobu Ichimura <famao@kondara.org>
- compile without bonobo
- dora will fix this problem in the future ;-P)

* Thu Sep 15 2000 Motonobu Ichimura <famao@kondara.org>
- up to 0.14

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Mon Jun 19 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- up tp 0.13

* Thu Mar 16 2000 Shingo Akagaki <dora@kondara.org>
- up to 0.12

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Sun Dec 05 1999 Motonobu Ichimura <famao@kondara.org>
- up to 0.11

* Wed Dec 01 1999 Shingo Akagaki <dora@kondara.org>
- update to 0.9

* Tue Sep 07 1999 Elliot Lee <sopwith@redhat.com>
- Updated RHL 6.1 package to libglade-0.5

* Sun Nov  1 1998 James Henstridge <james@daa.com.au>

- Updated the dependencies of the devel package, so users must have gtk+1-devel.

* Sun Oct 25 1998 James Henstridge <james@daa.com.au>

- Initial release 0.0.1
