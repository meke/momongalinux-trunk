%global momorel 5

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%global svnrev r2618

Name:           python-ruledispatch
Version:        0.5a1
Release:        0.1.%{momorel}m%{?dist}
Summary:        A generic function package for Python

Group:          Development/Languages
License:        "Python" or "ZPLv2.1"
URL:            http://www.turbogears.org/
Source0:        http://files.turbogears.org/eggs/RuleDispatch-%{version}.dev-%{svnrev}.tar.gz
Patch0:         python-ruledispatch-0.5a0-py26keyword.patch
Patch1:         python-ruledispatch-ez_setup.patch
Patch2:         python-ruledispatch-depr-py26.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7 python-setuptools-devel
BuildRequires:  python-protocols >= 1.0
BuildRequires:  python-peak-util-extremes >= 1.1
Requires:       python-protocols >= 1.0
Requires:       python-peak-util-extremes >= 1.1

%description
Rule-based Dispatching and Generic Functions.

%prep
%setup -q -n RuleDispatch-%{version}.dev-%{svnrev}
%patch0 -p 1 -b .keyword
%patch1 -p0 -b .ez_setup
%patch2 -p1 -b .depr

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT
mv $RPM_BUILD_ROOT%{python_sitearch}/dispatch/*.txt .
chmod a-x *.txt


%check
%{__python} setup.py test

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc combiners.txt TODO.txt 
%{python_sitearch}/RuleDispatch-%{version}.dev_%{svnrev}-py*.egg-info
%dir %{python_sitearch}/dispatch
%{python_sitearch}/dispatch/*.py
%{python_sitearch}/dispatch/_d_speedups.so
%{python_sitearch}/dispatch/*.pyc
%dir %{python_sitearch}/dispatch/tests
%{python_sitearch}/dispatch/tests/*.py
%{python_sitearch}/dispatch/tests/*.pyc
%{python_sitearch}/dispatch/*.pyo
%{python_sitearch}/dispatch/tests/*.pyo

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5a1-0.1.5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5a1-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5a1-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5a1-0.1.2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a1-0.1.1m)
- sync with Fedora 13 (0.5a1-0.17.svnr2506)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a0-0.5.svnr2306.7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a0-0.5.svnr2306.6m)
- rebuild against rpm-4.6

* Thu Jan  2 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5a0-0.5.svnr2306.5m)
- add Patch0: python-ruledispatch-0.5a0-py26keyword.patch

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5a0-0.5.svnr2306.4m)
- rebuild against python-2.6.1-2m

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5a0-0.5.svnr2306.3m)
- rebuild against python-setuptools-0.6c9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5a0-0.5.svnr2306.2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5a0-0.5.svn2305.1m)
- import from Fedora

* Thu May  3 2007 Luke Macken <lmacken@redhat.com> 0.5a0-0.5.svn2305
- 0.5a0.dev-r2306

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> 0.5a0-0.4.svnr2115
- Rebuild for new python

* Sun Sep  3 2006 Luke Macken <lmacken@redhat.com> 0.5a0-0.2.svnr2115
- Rebuild for FC6

* Mon Aug 14 2006 Luke Macken <lmacken@redhat.com> 0.5a0-0.1.svnr2115
- Include the pyo files instead of ghosting them

* Thu Jul  6 2006 Luke Macken <lmacken@redhat.com> 0.5a0-0.1.svnr2115
- Use sitearch instead of sitelib in the files
- Remove python-abi requirement
- Ghost the .pyo files
- Add the URL

* Wed Apr 19 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 0.5a0-0.svnr2115
- Intial RPM release
