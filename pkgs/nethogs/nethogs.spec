%global momorel 4

Summary:        A tool resembling top for network traffic
Name:           nethogs
Version:        0.7.0
Release:        %{momorel}m%{?dist}
Group:          Applications/Internet
License:        GPL+
URL:            http://nethogs.sourceforge.net
Source0:        http://downloads.sourceforge.net/project/%{name}/%{name}/0.7/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       ncurses 
BuildRequires:  libstdc++-devel
BuildRequires:  ncurses-devel
BuildRequires:  libpcap-devel >= 1.1.1
BuildRequires:  gcc-c++

%description
NetHogs is a small "net top" tool.

Instead of breaking the traffic down per protocol or per subnet, like
most such tools do, it groups bandwidth by process and does not rely
on a special kernel module to be loaded.

So if there's suddenly a lot of network traffic, you can fire up
NetHogs and immediately see which PID is causing this, and if it's
some kind of spinning process, kill it.

%prep
%setup -q -n %{name}

%build
%make

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_sbindir}
install -m 0755 nethogs %{buildroot}%{_sbindir}/

mkdir -p %{buildroot}%{_mandir}/man8
install -m 0644 nethogs.8 %{buildroot}%{_mandir}/man8/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changelog DESIGN README
%{_sbindir}/nethogs
%doc %{_mandir}/man*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-0.20080627.4m)
- rebuild against libpcap-1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-0.20080627.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-0.20080627.2m)
- apply gcc44 patch

* Mon Mar  2 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7-0.20080627.1m)
- import from Fedora to Momonga

* Fri Feb 26 2009 Manuel "lonely wolf" Wolfshant <wolfy@fedoraproject.org> - 0.7-6.20080627cvs
- Adjust the patch in order to compile with gcc4

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7-5.20080627cvs
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Sep 21 2008 Ville Skytta <ville.skytta at iki.fi> - 0.7-4.20080627cvs
- Fix Patch0:/%%patch mismatch.

* Fri Jul 08 2008 Anderson Silva <ansilva@redhat.com> 0.7-3.20080627cvs
- Fix for debuginfo package provided by Ville Skytta.

* Fri Jun 27 2008 Anderson Silva <ansilva@redhat.com> 0.7-2.20080627cvs
- Patch provided by Marek Mahut to compile under Fedora 9
- Removed BuildArch restrictions

* Fri Jun 27 2008 Anderson Silva <ansilva@redhat.com> 0.7-1.20080627cvs
- Stable package of nethogs
