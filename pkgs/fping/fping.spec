%global momorel 10
%global subver	b2_to-ipv6

Summary: 	Fast pallarel ping
Name: 		fping
Version:	2.4
Release: %{momorel}m%{?dist}
License: 	BSD
Group: 		Applications/System
URL: 		http://www.fping.com/
Source0: 	http://www.fping.com/download/%{name}-%{version}%{subver}.tar.gz
# NoSource: 0
Patch1:		fping-2.4b2_to-ipv6-build.patch
Patch2:		fping-2.4b2_ipv6-fix.diff

BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
fping is a ping(1) like program which uses the Internet Control
Message Protocol (ICMP) echo request to determine if a host is
up. fping is different from ping in that you can specify any number of
hosts on the command line, or specify a file containing the lists of
hosts to ping. Instead of trying one host until it timeouts or
replies, fping will send out a ping packet and move on to the next
host in a round-robin fashion. If a host replies, it is noted and
removed from the list of hosts to check. If a host does not respond
within a certain time limit and/or retry limit it will be considered
unreachable.

Unlike ping, fping is meant to be used in scripts and its output is
easy to parse.

%prep
%setup -q -n %{name}-%{version}%{subver}
%patch1 -p0
%patch2 -p1 -b .ipv6

%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc  COPYING ChangeLog INSTALL README
%{_sbindir}/*
%{_mandir}/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-5m)
- rebuild against gcc43

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-4m)
- NoSource to Source

* Wed May 17 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (2.4-3m)
- Import fping-2.4b2_ipv6-fix.diff from Fedora Extras

* Fri Apr 21 2006 Takaaki Tabuchi <tab@momonga-linux.org>
- (2.4-2m)
- fix files
- use global
- use buildroot

* Wed Jan 28 2004  <yohgaki@momonga-linux.org> - 
- Initial build.

