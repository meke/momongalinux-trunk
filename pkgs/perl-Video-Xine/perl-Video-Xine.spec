%global         momorel 4

Name:           perl-Video-Xine
Version:        0.26
Release:        %{momorel}m%{?dist}
Summary:        Perl interface to libxine
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Video-Xine/
Source0:        http://www.cpan.org/authors/id/S/ST/STEPHEN/Video-Xine-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= v5.8.5
BuildRequires:  perl-DateTime
BuildRequires:  perl-DateTime-Format-Duration
BuildRequires:  perl-ExtUtils-CBuilder
BuildRequires:  perl-ExtUtils-PkgConfig
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple >= 0.01
BuildRequires:  perl-X11-FullScreen
BuildRequires:  xine-lib >= 1.2.4
Requires:       perl-DateTime
Requires:       perl-DateTime-Format-Duration
Requires:       perl-ExtUtils-CBuilder
Requires:       perl-ExtUtils-PkgConfig
Requires:       perl-Module-Build
Requires:       perl-Test-Simple >= 0.01
Requires:       perl-X11-FullScreen
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
A perl interface to xine, the Linux movie player. More properly, an
interface to libxine, the development library. Requires installation of
libxine. It has been tested up to xine version 1.2.2.

%prep
%setup -q -n Video-Xine-%{version}

%build
%{__perl} Build.PL installdirs=vendor optimize="%{optflags}"
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{_bindir}/*
%{perl_vendorarch}/auto/Video/Xine
%{perl_vendorarch}/Video/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-3m)
- rebuild against perl-5.18.2

* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.26-2m)
- rebuild against xine-lib-1.2.4

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-2m)
- reuild against perl-5.18.1

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Sat Jun 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Tue Jun 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-20m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-19m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-18m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-17m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-16m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-15m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-14m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-13m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-11m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-10m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18-9m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-8m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-7m)
- rebuild against perl-5.12.0

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18-6m)
- change BR from xine-lib to xine-lib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-4m)
- add BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18-2m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-2m)
- rebuild against gcc43

* Tue Jan 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Mon Jan 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Sun Jan 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Wed Jan 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Tue Jan 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Thu Jan 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- Specfile autogenerated by cpanspec 1.74 for Momonga Linux.
