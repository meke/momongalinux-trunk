%global momorel 4

Summary: The GNU disk partition manipulation program
Name:    parted
Version: 3.1
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/System
URL: http://www.gnu.org/software/parted/index.shtml

Source0: http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
NoSource: 0

Patch0: parted-3.0-libparted-copy-pmbr_boot-when-duplicating-GPT-disk.patch
Patch1: parted-3.1-libparted-check-PMBR-before-GPT-partition-table-8052.patch
Patch2: parted-3.1-tests-add-t0301-overwrite-gpt-pmbr.sh.patch
Patch3: parted-3.1-libparted-Fix-endian-error-with-FirstUsableLBA.patch
Patch4: parted-2.1-libparted-use-dm_udev_wait-698121.patch
Patch5: parted-3.1-libparted-use-largest_partnum-in-dm_reread_part_tabl.patch
patch6: parted-3.1-test-creating-20-device-mapper-partitions.patch
Patch7: parted-3.1-libparted-preserve-the-uuid-on-dm-partitions.patch
Patch8: parted-3.1-tests-Make-sure-dm-UUIDs-are-not-erased.patch
Patch9: parted-3.1-libparted-reallocate-buf-after-_disk_analyse_block_s.patch
Patch10: parted-3.1-tests-cleanup-losetup-usage.patch
Patch11: parted-3.1-libparted-add-support-for-implicit-FBA-DASD-partitions.patch
Patch12: parted-3.1-libparted-add-support-for-EAV-DASD-partitions.patch
Patch13: parted-3.1-libparted-don-t-canonicalize-dev-md-paths.patch
Patch14: parted-3.1-libparted-mklabel-eav.patch
Patch15: parted-3.1-avoid-dasd-as-default-file-image-type.patch
Patch16: parted-3.1-libparted-mklabel-edev.patch
Patch17: parted-3.1-tests-rewrite-t6001-to-use-dev-mapper.patch
Patch18: parted-3.1-libparted-Recognize-btrfs-filesystem.patch
Patch19: parted-3.1-tests-Add-btrfs-and-xfs-to-the-fs-probe-test.patch
Patch20: parted-3.1-libparted-Flush-parent-device-on-open-962611.patch

# NOTE: Started numbering patches to their order is obvious
Patch21: 0021-tests-Remove-rmmod-loop-from-t8001-1040504.patch
Patch22: 0022-tests-Increase-size-of-container-filesystem-in-t4100.patch
Patch23: 0023-tests-Disable-t4100-dvh-test-for-now-1036093.patch
Patch24: 0024-libparted-make-sure-not-to-treat-percentages-as-exac.patch
Patch25: 0025-tests-Use-mkfs.xfs-to-create-files-1036152.patch

# Momonga
Patch10000: parted-3.1-no-test.patch


Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: e2fsprogs-devel
BuildRequires: readline-devel
BuildRequires: ncurses-devel
BuildRequires: gettext-devel
BuildRequires: texinfo
BuildRequires: device-mapper-devel
BuildRequires: libselinux-devel
BuildRequires: libuuid-devel
BuildRequires: libblkid-devel >= 2.17
BuildRequires: gnupg
BuildRequires: git
BuildRequires: autoconf automake

Requires(post): glibc
Requires(post): info
Requires(preun): info
Requires(postun): glibc

%description
The GNU Parted program allows you to create, destroy, resize, move,
and copy hard disk partitions. Parted can be used for creating space
for new operating systems, reorganizing disk usage, and copying data
to new hard disks.


%package devel
Summary:  Files for developing apps which will manipulate disk partitions
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The GNU Parted library is a set of routines for hard disk partition
manipulation. If you want to develop programs that manipulate disk
partitions and filesystems using the routines provided by the GNU
Parted library, you need to install this package.

%prep
%setup -q

%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1
%patch14 -p1
#%%patch15 -p1
%patch15 -p1
%patch17 -p1
%patch18 -p1
%patch19 -p1
%patch20 -p1
%patch21 -p1
%patch22 -p1
%patch23 -p1
%patch24 -p1
%patch25 -p1

%patch10000 -p1 -b .no_test

%build
autoreconf
autoconf
CFLAGS="$RPM_OPT_FLAGS -Wno-unused-but-set-variable"; export CFLAGS
%configure --enable-selinux --disable-static
# Don't use rpath!
%{__sed} -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
%{__sed} -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
V=1 %{__make} %{?_smp_mflags}


%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

# Remove components we do not ship
%{__rm} -rf %{buildroot}%{_libdir}/*.la
%{__rm} -rf %{buildroot}%{_infodir}/dir
%{__rm} -rf %{buildroot}%{_bindir}/label
%{__rm} -rf %{buildroot}%{_bindir}/disk

%find_lang %{name}


%check
export LD_LIBRARY_PATH=$(pwd)/libparted/.libs
make check


%clean
%{__rm} -rf %{buildroot}


%post
/sbin/ldconfig
if [ -f %{_infodir}/parted.info* ]; then
    /sbin/install-info %{_infodir}/parted.info %{_infodir}/dir || :
fi

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/parted.info %{_infodir}/dir || :
fi

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING ChangeLog NEWS README THANKS TODO doc/API doc/FAT
%{_sbindir}/parted
%{_sbindir}/partprobe
%{_mandir}/man8/parted.8.*
%{_mandir}/man8/partprobe.8.*
%{_libdir}/libparted*.so.*
%{_infodir}/parted.info.*

%files devel
%defattr(-,root,root)
%{_includedir}/parted
%{_libdir}/libparted*.so
%{_exec_prefix}/%{_lib}/libparted.so
%{_exec_prefix}/%{_lib}/pkgconfig/libparted.pc


%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-4m)
- import RHEL7 patches

* Tue May 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-3m)
- import fedora patches

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1-2m)
- suport userMove env

* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1-1m)
- version up 3.1

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-2m)
- import fedora patched

* Wed Sep  7 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0-1m)
- version up 3.0

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3-1m)
- update 2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-4m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-3m)
- rebuild against readline6

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-2m)
- import fedora patches

* Sun Mar 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-1m)
- update 2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.8-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.8-9m)
- import fedora patches(1.8.8-13)

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.8-8m)
- fix build

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.8-7m)
- add libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.8-6m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.8-5m)
- buildfix (new autoconf)
- add SOURCE1 extentions.m4

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.8-4m)
- update Patch200 for fuzz=0
- License: GPLv3+

* Mon Nov 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8,8-3m)
- fix force bz2 man and info

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8,8-2m)
- fix install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.6-5m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.6-4m)
- remove BPR libtermcap-devel

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.6-3m)
- %%NoSource -> NoSource

* Tue Nov 27 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.6-2m)
- added a patch for gcc43

* Sun Jun 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.6-1m)
- sync FC-devel
- update 1.8.6

* Wed Feb 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.2-2m)
- update 1.8.2

* Tue Oct 17 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.1-2m)
- update reiser4 patch

* Sat Oct 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.7.1-1m)
- update to 1.7.1
- sync with FC (1.7.1-16.fc6)
- * Wed Oct 04 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-16
- - Don't throw PED_EXCEPTION_ERROR in ped_geometry_read() if accessing
-   sectors outside of partition boundary, since returning false will
-   shift ped_geometry_check() to the correct sectors.
- * Wed Aug 23 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-15
- - Fixed gpt patch (*asked_already -> asked_already, whoops)
- * Tue Aug 22 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-14
- - Improve error message returned by _parse_header() on GPT-labeled disks
-   so users actually have an idea of how to correct the problem
- - Fix off-by-one error with LastUsableLBA and PartitionEntryLBA overlap
-   to prevent possible data corruption when using non-parted GPT editing
-   tools
- * Mon Aug 21 2006 Peter Jones <pjones@redhat.com> - 1.7.1-13
- - Don't use the "volume name" as the device node name on dm device
-   partitions, it isn't really what we want at all.
- * Thu Aug 17 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-12
- - Updated O_DIRECT patch to work around s390 problems
- - Update LastUsableLBA on GPT-labeled disks after LUN resize (#194238)
- - Fix exception when backup GPT table is not in the correction location
-   and parted tries to move it (#194238)
- * Tue Aug 15 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-11
- - Expand error buffer to 8192 bytes in vtoc_error()
- - Do not apply O_DIRECT patch on S/390 or S/390x platforms
- * Mon Aug 14 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-10
- - Removed bad header file patch (#200577)
- * Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.7.1-9.1
- - rebuild
- * Wed Jul  5 2006 Peter Jones <pjones@redhat.com> - 1.7.1-9
- - add ped_exception_get_handler()
- * Mon Jun 26 2006 Florian La Roche <laroche@redhat.com> - 1.7.1-8
- - remove info files in preun
- * Thu Jun 22 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-7
- - PED_SECTOR_SIZE -> PED_SECTOR_SIZE_DEFAULT
- * Thu Jun 22 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-6
- - Roll dasd patches together
- - Use O_DIRECT to prevent first partition corruption on GPT disks
- * Thu Jun 15 2006 Jeremy Katz <katzj@redhat.com> - 1.7.1-5
- - fix segfaults with dasd devices
- * Wed Jun  7 2006 Jeremy Katz <katzj@redhat.com> - 1.7.1-4
- - move .so symlink to -devel subpackage
- * Sun May 28 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-3
- - Rebuild
- * Sun May 28 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-2
- - Removed mac-swraid patch (added upstream)
- - Updated device-mapper patch for parted-1.7.1
- * Sat May 27 2006 David Cantrell <dcantrell@redhat.com> - 1.7.1-1
- - Upgraded to parted-1.7.1
- * Fri May 19 2006 David Cantrell <dcantrell@redhat.com> - 1.7.0-1
- - Upgraded to parted-1.7.0
- 
* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.25.1-2m)
- rebuild against readline-5.0

* Tue May 16 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- add BuildRequire gettext-devel

* Tue May 16 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.6.25.1-1m)
- update to 1.6.25.1
- sync with 1.6.25.1-1 in Fedora Core
- Changelog in Fedora Core is following...
- * Thu Apr 13 2006 David Cantrell <dcantrell@redhat.com> - 1.6.25.1-1
- - Upgraded to parted-1.6.25.1
- - BuildRequires libtool
- * Tue Mar 14 2006 Jeremy Katz <katzj@redhat.com> - 1.6.25-8
- - fix ppc swraid
- - BR gettext-devel
- * Wed Feb 22 2006 Peter Jones <pjones@redhat.com> - 1.6.25-7
- - close /proc/devices correctly
- * Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.6.25-6.1
- - bump again for double-long bug on ppc(64)
- * Tue Feb  7 2006 Peter Jones <pjones@redhat.com> 1.6.25-6
- - Fix dm partition naming.
- * Tue Feb  7 2006 Jesse Keating <jkeating@redhat.com> 1.6.25-5.2
- - rebuilt for new gcc4.1 snapshot and glibc changes
- * Fri Dec  9 2005 Jesse Keating <jkeating@redhat.com>
- - rebuilt
- * Fri Dec  2 2005 Peter Jones <pjones@redhat.com> 1.6.25-5
- - rebuild for new device-mapper
- * Thu Dec  1 2005 Peter Jones <pjones@redhat.com> 1.6.25-4
- - change device-mapper code to call dm_task_update_nodes() after
-   tasks which change device nodes.
- * Wed Nov 16 2005 Peter Jones <pjones@redhat.com> 1.6.25-3
- - fix /proc/devices parser bug
- * Tue Nov 15 2005 Peter Jones <pjones@redhat.com> 1.6.25-2
- - add support for partitions on dm devices
- * Wed Nov 09 2005 Chris Lumens <clumens@redhat.com> 1.6.25-1
- - Updated to 1.6.25.
- - Update DASD, iseries, and SX8 patches.
- * Tue Aug 30 2005 Chris Lumens <clumens@redhat.com> 1.6.24-1
- - Updated to 1.6.24.
- * Mon Jul 18 2005 Chris Lumens <clumens@redhat.com> 1.6.23-2
- - Add buildreq for texinfo.
- * Fri Jul 08 2005 Chris Lumens <clumens@redhat.com> 1.6.23-1
- - Updated to 1.6.23.
- - Get rid of separate Mac patches that are now included in upstream.
- - Update DASD and AIX patches.
- * Tue Jun 07 2005 Chris Lumens <clumens@redhat.com> 1.6.22-3
- - Modified Apple_Free patch to take care of the case where the partitions
-   are unnamed, causing many errors to be printed (#159047).
- * Thu May 05 2005 Chris Lumens <clumens@redhat.com> 1.6.22-2
- - Added upstream patch to display certain Apple_Free partitions (#154479).

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.22-1m)
  update to 1.6.22

* Tue Feb 22 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.21-1m)
  update to 1.6.21

* Tue Dec 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.19-2m)
  reiser4 support for anaconda(subset)

* Tue Dec  7 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.19-1m)
  update to 1.6.19, based FC development

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.3-34m)
- rebuild against python2.3

* Wed Aug 25 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (1.6.3-33m)
- add patch10 for kernel 2.6

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.6.3-32m)
- remove Epoch

* Mon Apr 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6.3-31m)
- imported from FC1 again. python module is neseccary for anaconda.

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6.5-4m)
- revised spec for rpm 4.2.

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org<\>
- (1.6.5-3m)
- put source into repository

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.5-2m)
- add URL tag

* Sat Mar  1 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.5-1m)
  update to 1.6.5

* Wed Dec 18 2002 TAKAHASHI Tamotsu
- (1.6.4-1m)

* Sat Oct 26 2002 Kikutani Makoto <poo@momonga-linux.org>
- (1.6.3-1m)
  update to 1.6.3

* Thu Jul 25 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.2-1m)
  update to 1.6.2

* Tue Apr 30 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (1.6.0-2k)
- decided to libtoolize
- configure --disable-dynamic-loading CC=gcc

* Sat Mar 23 2002 Toru Hoshina <t@kondara.org>
- (1.4.24-6k)
- libunicode is no longer used.

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.4.24-4k)
- just rebuild for parted-python

* Thu Feb  7 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.4.24-2k)
  update to 1.4.24

* Sun Jan 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.4.22-2k)
- version 1.4.22

* Wed Nov  7 2001 Shingo Akagaki <dora@kondara.org>
- (1.4.20-6k)
- remove python module

* Mon Nov  5 2001 Toru Hoshina <t@kondara.org>
- (1.4.20-4k)
- forgot python module (TT)

* Thu Oct 25 2001 Toru Hoshina <t@kondara.org>
- (1.4.20-2k)
- 1st release.
- merge from rawhide [1.4.20-0.1pre3]

* Thu Oct 11 2001 Matt Wilson <msw@redhat.com> 1.4.20-0.1pre3
- new dist from CVS with new autoconf and automake
- gpt is in 1.4.20, removed patch1 (gpt support)
- partstatic is in 1.4.20, removed patch2 (partstatic patch)

* Tue Aug 28 2001 Matt Wilson <msw@redhat.com> 1.4.16-8
- new dist from cvs with changes to the python binding: register
  DEVICE_I20 and DEVICE_ATARAID, check to make sure that a partition
  exists in the PedDisk when using it to find ped_disk_next_partition

* Tue Aug 21 2001 Matt Wilson <msw@redhat.com> 1.4.16-7
- really disable pc98 support (SF #51632)

* Fri Aug 17 2001 Matt Wilson <msw@redhat.com> 1.4.16-6
- added a patch (Patch1) to link the c library in dynamically, the
  rest of the libs statically for the parted binary (MF #49358)

* Tue Aug  7 2001 Matt Wilson <msw@redhat.com>
- made a new dist from CVS that includes binding for
  disk.get_partition_by_sector and accessing the name of a disk type

* Mon Aug  6 2001 Matt Wilson <msw@redhat.com> 1.4.16-4
- created a new dist from CVS that fixes ext3 detection when
  _probe_with_open is needed (#50292)

* Fri Jul 20 2001 Matt Wilson <msw@redhat.com>
- rewrite scsi id code (#49533)

* Fri Jul 20 2001 Matt Wilson <msw@redhat.com>
- added build requires (#49549)

* Tue Jul 17 2001 Matt Wilson <msw@redhat.com>
- 1.4.16
- regenerated gpt patch against 1.4.16, incorporated
  parted-1.4.15-pre1-gpt-printf.patch into the same patch, removed Patch1

* Tue Jul 10 2001 Matt Wilson <msw@redhat.com>
- added a new dist tarball that contains python wrappers to get disk types

* Tue Jul 10 2001 Tim Powers <timp@redhat.com>
- run ldconfig on un/install

* Tue Jul 10 2001 Matt Wilson <msw@redhat.com>
- added a fix from clausen for border case when there is an extended
  on the last cyl

* Mon Jul  9 2001 Matt Wilson <msw@redhat.com>
- 1.4.15

* Thu Jul  5 2001 Matt Wilson <msw@redhat.com>
- added patch from Arjan to enable ataraid support

* Wed Jul  4 2001 Matt Wilson <msw@redhat.com>
- imported 1.4.15-pre2 into CVS and made a new dist tarball

* Tue Jun 26 2001 Matt Wilson <msw@redhat.com>
- added a new dist tarball that contains a check in python code to
  make sure that a partition exists within a disk before trying to
  remove it from the disk
- also changed _probe_with_open to make the first probed filesystem win

* Tue Jun 26 2001 Bill Nottingham <notting@redhat.com>
- fix filesystem type reading on GPT disks

* Tue Jun 26 2001 Matt Wilson <msw@redhat.com>
- added another fix for ext2/ext3
- added Patch4 to move the crc32 function into its own namespace so
  we don't colide with zlib when both are in the same executable space

* Mon Jun 25 2001 Matt Wilson <msw@redhat.com>
- added a new dist tarball from CVS that includes
  ext3 probing

* Wed Jun  6 2001 Matt Wilson <msw@redhat.com>
- updated dist with binding for partition.geom.disk

* Tue Jun  5 2001 Matt Wilson <msw@redhat.com>
- make a new dist tarball that has new python binding changes

* Tue May 29 2001 Bill Nottingham <notting@redhat.com>
- add major numbers for cciss 
- add libunicode-devel buildprereq

* Sun May 27 2001 Matthew Wilson <msw@redhat.com>
- added type, heads, and sectors to the python binding for PedDevice

* Fri May  4 2001 Matt Wilson <msw@redhat.com>
- added parted-1.4.11-gpt-pmbralign.patch from Matt Domsch

* Wed May  2 2001 Matt Wilson <msw@redhat.com>
- include python binding
- enable shared library (for python binding, we want fpic code)
  with --enable-shared
- build parted binary static with --enable-all-static
- don't run libtoolize on this.

* Wed May 02 2001 Bill Nottingham <notting@redhat.com>
- update to 1.4.11
- add EFI GPT patch from Matt Domsch (<Matt_Domsch@dell.com>)
- don't run autoconf, it relies on a newer non-released version
  of autoconf...

* Fri Feb 23 2001 Trond Eivind Glomsrod <teg@redhat.com>
- langify

* Wed Jan 17 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.4.7

* Thu Dec 14 2000 Bill Nottingham <notting@redhat.com>
- rebuild because of broken fileutils

* Fri Nov 03 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.2.12

* Wed Nov 01 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.2.11

* Tue Oct 17 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.2.10

* Sun Sep 10 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.2.9

* Tue Aug 29 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- fix bug when just hitting "return" with no user input

* Sun Aug 20 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- 1.2.8
- blksize patch not needed anymore
- move changelog to the end of the spec file

* Wed Aug 16 2000 Matt Wilson <msw@redhat.com>
- 1.2.7
- patched configure script to ignore the 2.4 blkpg.h header (fixes #15835).

* Fri Aug  4 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.2.6

* Sat Jul 22 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.2.5
- add more docu

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jun 12 2000 Matt Wilson <msw@redhat.com>
- initialization of spec file.
